---
title: Some other websites worth recommending
description: 
published: true
date: 2024-02-09T04:25:44.102Z
tags: 
editor: markdown
dateCreated: 2023-02-23T22:30:29.670Z
---

# Test of all_videos page formatting:

<details> <summary>
2018-09-20: Let's talk about burning shoes (<a href="https://youtube.com/watch?v=mfDasT0zSpg">watch</a> || <a href="/videos/2018/09/20/Lets_talk_about_burning_shoes.md">transcript &amp; editable summary</a>)

Beau questions the logic behind burning symbols of protest and challenges the true meaning of freedom.

</summary>

### AI summary (High error rate! Edit errors on [video page](/videos/2018/09/05/Lets_talk_about_burning_shoes.md))

"If you're that mad and you can't wear a pair of Nikes because of a commercial, take them and drop them off at a shelter."
"You're loving that symbol of freedom more than you love freedom."
"Y'all have a good day."

- Beau wants to burn some shoes today because it's raining and he feels the need to burn something.
- He mentions not owning Nikes due to personal reasons but his son has a pair.
- Beau questions the significance of burning shoes as a form of protest against Nike.
- He suggests donating unwanted Nikes to shelters or thrift stores near military bases for those in need.
- Beau challenges the idea of burning symbols without considering the workers behind the products.
- He draws parallels between burning Nikes and burning the American flag as symbolic acts.
- Beau questions the integrity of those who are quick to disassociate with Nike over a commercial but ignore their history of unethical practices.
- He criticizes people who claim to value freedom but turn a blind eye to issues like sweatshops and slave labor.
- Beau concludes by pointing out the irony of loving the symbol of freedom more than actual freedom itself.

The full transcript provides a thought-provoking reflection on performative activism and the true essence of freedom, encouraging individuals to reexamine their actions and beliefs.

### Actions

for consumers, activists, ethical shoppers:

Donate unwanted shoes to shelters or thrift stores near military bases (suggested)
Educate oneself on the ethical practices of companies before supporting them (implied)
</details>

<details> <summary>
2018-09-20: Let's talk about what it s like to be a black person in the US (<a href="https://youtube.com/watch?v=WD8mWq0Hdcw">watch</a> || <a href="/videos/2018/09/20/Lets_talk_about_what_it_s_like_to_be_a_black_person_in_the_US.md">transcript &amp; editable summary</a>)

Unexpected reach of a Nike video led to deep reflections on cultural identity, heritage, and the ongoing impacts of slavery on collective identity, urging understanding beyond surface-level statistics.

</summary>

### AI summary (High error rate! Edit errors on [video page](/videos/2018/09/05/Lets_talk_about_burning_shoes.md))

"Your entire cultural identity was ripped away."
"They have black pride because they don't know."
"That's a whitewash of the reality."
"How much of who you are as a person comes from the old country."
"It's just a thought."

- Unexpected wide reach of a Nike video led to insults and reflections.
- Insult about IQ compared to a plant sparked thoughts on understanding being Black in the U.S.
- White allies may grasp statistics but not the true experience of being Black.
- Deep dive into cultural identity, heritage, and pride.
- Historical context of slavery and its lasting impacts on cultural identity.
- Reflections on cultural identity being stripped away and replaced.
- Food, cuisine, and cultural practices as remnants of slavery.
- The ongoing impact of slavery on cultural scars.
- Call to acknowledge history and the need for healing.
- Hope for a future where national identities are left behind.
- Acknowledgment of uncomfortable history and the lack of pride for some.
- Recognition of the significance of the Black Panther movie in providing pride.
- Encouragement to truly understand the depth of the issue beyond surface-level talks.
- Acknowledgment of cultural identity loss and the need for reflection and understanding.

The full transcript delves deep into the ongoing effects of slavery on cultural identity, urging a reflective understanding beyond statistics and surface-level knowledge.

### Actions

for white allies:

Acknowledge and understand the deep-rooted cultural impacts of historical events (implied).
</details>

<details> <summary>
2018-09-20: Let's talk about the September 11th attacks and how we lost the war on terror (<a href="https://youtube.com/watch?v=XO6sdC3AlQ8">watch</a> || <a href="/videos/2018/09/20/Lets_talk_about_the_September_11th_attacks_and_how_we_lost_the_war_on_terror.md">transcript &amp; editable summary</a>)

Beau warns against the dangerous normalization of loss of freedoms post-9/11, urging for grassroots community action to reclaim freedom and resist government overreach.

</summary>

### AI summary (High error rate! Edit errors on [video page](/videos/2018/09/05/Lets_talk_about_burning_shoes.md))

"We didn't know it at the time, though, we were too busy, too busy putting yellow ribbons on our cars, looking for a flag to pick up and wave, or picking up a gun, traveling halfway around the world to kill people we never met."
"You can't dismantle the surveillance state or the police state by voting somebody into office."
"You have to defeat an overreaching government by ignoring it."
"The face of tyranny is always mild at first. And we're there right now."
"If your community is strong enough, what happens in DC doesn't matter because you can ignore it."

- Recalls his experience on September 11th, watching the tragic events unfold live on TV.
- Expresses how a casualty of that day was the erosion of freedom and rights, not listed on any memorial.
- Talks about the erosion of freedoms post-9/11 due to government actions in the name of security.
- Mentions the strategic nature of terrorism and how it aims to provoke overreactions from governments.
- Points out the dangerous normalization of loss of freedoms in society.
- Urges for a grassroots approach to reclaiming freedom, starting at the community level.
- Advocates for teaching children to question and resist the normalization of oppressive measures.
- Emphasizes the importance of self-reliance in preparation for natural disasters and reducing dependency on the government.
- Suggests counter-economic practices like bartering and cryptocurrency to reduce government influence.
- Encourages surrounding oneself with like-minded individuals who support freedom and each other.
- Calls for supporting and empowering marginalized individuals who understand the loss of freedom.
- Talks about the effectiveness of U.S. Army Special Forces in training local populations as a force multiplier.
- Stresses the need for community building to resist government overreach and tyranny.

The full transcript provides deeper insights into the erosion of freedoms post-9/11 and the strategic nature of terrorism, urging individuals to take concrete actions at the community level to safeguard freedom and resist tyranny.

### Actions

for community members:

Teach children to question and resist oppressive measures (implied)
Prioritize self-reliance for natural disasters and reduce dependency on the government (implied)
Practice counter-economic activities like bartering and cryptocurrency (implied)
Surround yourself with like-minded individuals who support freedom (implied)
Support and empower marginalized individuals who understand the loss of freedom (implied)
Build community networks to resist government overreach (implied)
</details>

<details> <summary>
2018-09-20: Let's talk about the Dallas shootin (<a href="https://youtube.com/watch?v=6-d4C-sIlaI">watch</a> || <a href="/videos/2018/09/20/Lets_talk_about_the_Dallas_shootin.md">transcript &amp; editable summary</a>)

Beau questions the official narrative of a shooting in Dallas, pointing out potential motives and legal consequences, while criticizing police actions and lack of justice for victims, and connecting it to the Black Lives Matter movement.

</summary>

### AI summary (High error rate! Edit errors on [video page](/videos/2018/09/05/Lets_talk_about_burning_shoes.md))

"There's two kinds of liars in the world."
"I'm not a lawyer, but that sure sounds like motive to me."
"If this is the amount of justice they get, they don't. They don't."
"Not one cop has crossed the thin blue line to say that's wrong."
"It doesn't matter what else you do."

- Expresses skepticism towards the official narrative of the shooting in Dallas, referencing experience with liars and motives.
- Raises concerns about the shooting being a potential home invasion due to new information about noise complaints.
- Points out the potential legal consequences in Texas for killing someone during a home invasion.
- Questions the lack of intervention or opposition within the police department regarding potential corruption and cover-ups.
- Connects the lack of justice in cases like this to the Black Lives Matter movement and the value placed on black lives.
- Criticizes the actions of the police in searching the victim's home posthumously for justifications.

The emotional impact and depth of analysis provided by Beau during his commentary.

### Actions

for community members, justice seekers, activists.:

Speak out against police corruption and cover-ups within your community (implied).
Support movements like Black Lives Matter by advocating for justice and equality (implied).
</details>

<details> <summary>
2018-09-20: Let's talk about 3 letters that can make rural whites understand the fear minorities have of cops (<a href="https://youtube.com/watch?v=J5DBrOBIgNM">watch</a> || <a href="/videos/2018/09/20/Lets_talk_about_3_letters_that_can_make_rural_whites_understand_the_fear_minorities_have_of_cops.md">transcript &amp; editable summary</a>)

Exploring the distrust towards law enforcement, Beau urges unity and accountability to address the common issue of unaccountable men with guns.

</summary>

### AI summary (High error rate! Edit errors on [video page](/videos/2018/09/05/Lets_talk_about_burning_shoes.md))

"We can hold them accountable one way or the other, the ballot box or the cartridge box."
"It's unaccountable men with guns. We can work together and we can solve that."
"We've got to start talking to each other. We got the same problems."

- Explains the distrust that minorities have for law enforcement compared to white country folk.
- Illustrates how in rural areas, law enforcement is more accountable and part of a tighter community.
- Mentions the ability to hold law enforcement accountable through the ballot box or the cartridge box.
- Points out that minority groups lack institutional power to hold unaccountable men with guns in law enforcement accountable.
- Draws parallels between the distrust felt towards law enforcement by minorities and white country folk towards agencies like ATF and BLM.
- Addresses the frequency of unjust killings and lack of accountability in law enforcement.
- Encourages taking action by getting involved in local elections to ensure accountability.
- Suggests starting at the local level by electing good police chiefs to prevent corruption in federal agencies.
- Advocates for people from different backgrounds to unite and address the common issue of unaccountable men with guns in law enforcement.
- Urges individuals to care enough to take action and work together to solve the problem.

The full transcript provides a detailed explanation of the distrust towards law enforcement, suggestions for accountability through community action, and the importance of unity in addressing systemic issues.

### Actions

for community members, activists:

Elect good police chiefs in local elections to prevent corruption (suggested)
Advocate for accountability in law enforcement by participating in local elections (implied)
Start dialogues with individuals from different backgrounds to address common issues (implied)
</details>



# Lauren's suggestions

Math of various stuff, such as communities and networks: https://explorabl.es/

A guide to connecting to other people in small communities; good at only using [[soft words]] and being thorough: https://www.microsolidarity.cc/
