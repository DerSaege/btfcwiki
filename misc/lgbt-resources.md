---
title: LGBT Resources list
description: 
published: true
date: 2024-02-02T23:20:53.097Z
tags: 
editor: markdown
dateCreated: 2024-01-31T18:08:55.996Z
---

# if last change older than 6mo, list may be out of date

see right sidebar.

# General:

- PFLAG is a general LGBTQ+ support and advocacy group.  Their site also has a listing of some important crisis support and intervention hotlines:
    - [pflag.org/findachapter/](https://pflag.org/findachapter/)
    - [pflag.org/resource/support-hotlines/](https://pflag.org/resource/support-hotlines/)
- Veterans for Equality is not currently active beyond socials in most of the country, but
  [veteransforequality.co](https://www.veteransforequality.co) and facebook.com/groups/406316048039271/
- Signal is an encrypted messenger app that grants something close to actual privacy: [signal.org/](https://signal.org/)
  - Make sure to that notifications are set up to not show message details ([example screenshot](/signal-messenger-privacy-tips-features-tricks-android-iphone-14_935adec67b324b146ff212ec4c69054f.webp) of where the setting can be found) since those are not protected.

# Progressive defense

## **If you contact any chapter thereof, it's a good idea to not use personally connected email channels for panopticon reasons.**
Checking with multiple is suggested if you think any is relevant to your needs. 
- Pink Pistols is a gun rights group with the express purpose of supporting and protecting the LGBTQ+ community.
  [pinkpistols.org/find-a-local-chapter/](https://www.pinkpistols.org/find-a-local-chapter/)
- Operation Blazing Sword is a network of LGBTQ+-supportive firearms instructors who offer free basic training.
  [blazingsword.org/instructor-list-page/](https://www.blazingsword.org/instructor-list-page/)

### by location:
- **texas**: [blackcatriflegroup.com/](https://blackcatriflegroup.com/) (local to Texas afawk)
- John Brown Gun Club:
    - **Wisconsin**: [john-brown-gun-club.org/](https://www.john-brown-gun-club.org/)
    - **Puget Sound**: [psjbgc.org/](https://web.archive.org/web/20221118220614/https://psjbgc.org/) 
      (puget sound is active, they just have a weird obfuscation around their site)
    - **other chapters**: [john-brown-gun-club.org/branches](https://www.john-brown-gun-club.org/branches)

# Legislation tracking:
- Coverage of anti-trans legislation, general risk map and state legislative trends:
  [erininthemorn.substack.com/](https://erininthemorn.substack.com/)
- Spreadsheet (most live documentation):
  [docs.google.com/spreadsheets/d/1x8FDh50ouBldIpxnCdSb745pyYISYLLKp9lJligOQZk/edit#gid=0](https://docs.google.com/spreadsheets/d/1x8FDh50ouBldIpxnCdSb745pyYISYLLKp9lJligOQZk/edit#gid=0)
- Website:
  [translegislation.com/](https://translegislation.com/)

# Trans resources:
- General healthcare resource lists:
  [transhealthproject.org/resources/trans-health-care-providers/](https://transhealthproject.org/resources/trans-health-care-providers/)
- Relocation funding help:
  [hrc.org/resources/emergency-funds-for-relocating-families](https://www.hrc.org/resources/emergency-funds-for-relocating-families)
- Informed consent HRT clinic map:
  [google.com/maps/d/viewer?mid=1DxyOTw8dI8n96BHFF2JVUMK7bXsRKtzA&hl=en_US&ll=37.8803431320255%2C-81.74642342305575&z=4](https://www.google.com/maps/d/viewer?mid=1DxyOTw8dI8n96BHFF2JVUMK7bXsRKtzA&hl=en_US&ll=37.8803431320255%2C-81.74642342305575&z=4)
- A place for Marsha official:
  [aplaceformarshaofficial.org/](https://www.aplaceformarshaofficial.org/)
- Trans resistance network:
  [transresistancenetwork.wordpress.com/](https://transresistancenetwork.wordpress.com/)
- Relocation services and security - Rainbow passage (my yellow brick road has merged with them):
  [rainbowpassage.org/](https://rainbowpassage.org/)
- The gender affirming letter access project (GALAP):
  [galap.org/](https://www.galap.org/)
  and if you're a mental health professional, consider offering your services via GALAP.
