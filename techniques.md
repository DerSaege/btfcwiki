---
title: Conversation Techniques
description: 
published: true
date: 2024-02-28T06:53:46.576Z
tags: 
editor: markdown
dateCreated: 2024-02-27T01:20:28.628Z
---



the core is active listening skills
abandon the belief that you can change people's minds
people change their own minds
the goal needs to be to get people to the point where they can have one on one conversations that are skillful and build relationships, so the question is how do you get there

# the spread of good conversations?

I've been talking to folks about how to be more effective at spreading activist engagement.

apparently the biggest political divide in the US has again been measured to be politically engaged vs politically disengaged; politically engaged people get the most attention on social media, so politically disengaged people think of social media as being what politics is.[citation needed] [possible citation](https://www.pewresearch.org/politics/2024/01/09/tuning-out-americans-on-the-edge-of-politics/)

since that is kind of an unwelcome flavor to a lot of disengaged people they avoid it.
then, engaged people get upset because they care a lot and their disengaged friends don't seem to.
so they pressure them to engage; pressure typically makes people retaliate against being pressured by doing the opposite of what they were pressured to do, and this is no exception.

there's also good research on what to do instead.

groups that focus on it tend to end up finding more or less the same strategy. some that I know of are braver angels, changing the conversation, deep canvassing - read any of their how-to manuals and you'll be in a good spot. [beau talks about similar stuff](/), but imo not halfway often enough. he mostly just does it. he's a good example of one side of it but he can't give examples of doing it interactively, which is the most important version.




# candidate: braver angels

**workshops ([in person](https://braverangels.org/attend-a-workshop/) and [online](https://braverangels.org/online/workshops/))** are designed to teach those skills and give practice time.

**there are [braver angels e-learning courses](https://braverangels.org/what-we-do/take-an-ecourse/)**, originally intended to be background work before a workshop, but stand on their own.

**issues with this project**: there are probably other online activities besides online workshops that could contribute if braver angels' insights were presented in a way that works...
... but, [leeja miller tried to make a video about this but most people objected in comments](https://www.youtube.com/watch?v=a263OuotbWw). The objections are fairly well known and you likely have some yourself.


## the surprising idea is that trust comes before facts.
people think we can't talk to people who don't hold the same things as facts. that somehow we need to get on the same page of facts before we can proceed. while there is a logic to that, it doesn't match what actually happens interpersonally. trust comes before facts: you decide which facts to accept based on who you trust. so if you want to open up someone's views, you have to build trust. that goes for me writing this and you reading this too.

## braver angels teaches a behavior they call CAPP: clarify, acknowledge, pivot, perspective
1. **Clarify** what the person is saying in a way that shows them you're listening, without adding anything into what they're saying. No saying what you think at all.
2. **Acknowledge** the values implied by what they said. Again, no saying what you think at all. Not saying what you think is what trips people up!
3. **Pivot**: signal of a change in topic from their view to your view. **eg**, ask them if you can tell your point of view. eg, say "I have a different point of view"... but that's all; you say that you have a different point of view, not what it's is. Then wait for them to say go ahead.
    - You're getting their consent to switch topic. might be nonverbally communicated, but you're looking for a clear signal that they're ready to hear what you have to say. You must not proceed until you get that confirmation.
    - answer any reservations they may have - eg, "you're not going to try to change my mind?" -> "I don't want to try to change your mind, I just want to share what I think" - but if you copycat this example rather than answering honestly it won't work.
    - having gotten that "ok, sure, go ahead", then and only then move on to...
4. **Perspective**: ...you start telling your own perspective. Talk about your life experiences whenever possible: the things you have observed firsthand.
    - not making overarching judgements about what's true in the world; you don't have that common frame of reference yet. this is one of the big sticking points people have with this protocol
    - avoid getting overly heated but if you do have a lot of emotion you may acknowledge it, eg, "this subject is very close to my heart". if you're going to do that it makes a big difference to say why: tell the story of experiences you've had that make it so emotional to you.
    - you almost certainly have observed other people's experiences; it's fine to discuss your observations about others, and your observations about their observations, but it's all from your perspective.
    - if there are facts that you must reference, you may, but you must keep the first person as much as possible, and it typically will need to take the form, "[I observed] [person], who I trust, say [this thing], and I [believed them]". you may have to talk about why you believed them, and you should expect them to not share your trust in that person

## doing this online:


- there are a lot of ameliorating actions from nonverbal communication that are possible in person that don't work online

- in pure text one on one you'd have to add in words that serve the function of the nonverbal communication, eg
    - stating your intention, eg "I'm not trying to trick you here I'm just really curious" (if and only if that's true)
    - "forgive me if I'm missing the point, but it sounds to me like..."
    - people are just gonna have a higher rate of misunderstanding online because of words sounding harsher due to ambient expectation of the bad use by liars, so you need to know the other person's culture and text communication style enough to know how to be friendly
        - which could be a big problem in many cases
- in one to many (youtube) or many to many (twitter/bluesky), some guessing:
    - show doing it one to one? still make it one to one but in public so to speak?
    - you could do how-to videos
        - there's interesting opportunity to find examples of conversations gone badly or gone well in online media and rework them to show what it would look like done better or worse
    - claim: this sort of social repair sort of necessarily takes place one to one?
        - "the backbone of organizing is one on one conversations" is a thing I've heard in union organizing

# candidate: changing the conversation

[look up changing the conversation's guides and fill me out pls]

# candidate: deep canvassing

[look up deep canvassing's guides and fill me out pls]

# funnels (this part needs editing)
in order to change the world with this we need to greatly increase the spread rate of types of activism that work for talking to disengaged people
it's what I've been thinking the wiki could be used for but there are a lot of severe problems with it from that perspective
when you're thinking about something like "change how politically engaged people talk to disengaged people" it helps to think in something vaguely like a marketing funnel
![image1.png](/image1.png)
^ this is something capitalists use to market to people
they call it a funnel, but it actually looks more like this on analytics pages
![image2.png](/image2.png)
people come in via some source, encounter your thing (in this case, "you should talk to people using techniques that really work"), and then there are a series of steps they have to go through before they end up doing whatever you're hoping they do (in this case, "actually learn the skills and apply them")

these marketing funnels are meant for centralized usage
but they also work for peer to peer stuff
I mean, I guess they're not exactly only meant for centralized usage. that first one ends in "advocacy"
there are big dropoffs any time you ask the people who are encountering your thing to do another difficult step
those are the red bars, which in the case of web analytics just means leaving the page
in the case of activism it probably means something like "goes back to doing politics the way they were before" or "goes back to being disengaged"