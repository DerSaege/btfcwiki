---
title: Home page
description: 
published: true
date: 2024-08-08T00:54:31.562Z
tags: 
editor: markdown
dateCreated: 2023-02-14T05:27:49.941Z
---

wiki will be staying up 🙂

# Beau's email

"com gmail @ beau for question" backwards. (There are spam bots on the internet, unfortunately, so we put it in a funky way on the wiki for now)


# Anyway, it's just a searchable video wiki

Right now you can search for videos up to the current video and it should work pretty great. Better than youtube search! You can also sign up to edit: either join the BeauTFC patreon discord, then sign in with discord; or sign in with gitlab.

See [ALL videos (big page)](/videos/all_videos) for a list of all videos, or if your computer has trouble with that one, just [all 2024 videos](/videos/2024/all_videos).

# Newly added pages:

- [lgbt resources list](/misc/lgbt-resources)
- [conversation techniques](/techniques)

## Some particularly cool beau discussions:

- [(2021) Soft language and sensationalism](/videos/2021/09/13/Lets_talk_about_soft_language_and_sensationalism)
- [(2021) what spears can teach us about creating change](/videos/2021/09/17/Lets_talk_about_what_spears_can_teach_us_about_creating_change); see also, newer, [(2022) spears and relief logistics](/videos/2022/10/07/Lets_talk_about_spears_and_relief_logistics)
- [(2022) a positive note for the weekend](/videos/2022/10/15/Lets_talk_about_a_positive_note_for_the_weekend)
- [(2021) burn out from helping those in need](/videos/2021/05/14/Lets_talk_about_burn_out_from_helping_those_in_need)
- [(2023) learning about institutions from board games](/videos/2023/01/29/Lets_talk_about_learning_about_institutions_from_board_games)
- [(2021) what I didn't talk about and a way to reach people](/videos/2021/05/22/Lets_talk_about_what_I_didn_t_talk_about_and_a_way_to_reach_people)
- [(2023) feedback, critiques, and cattle ranchers](/videos/2023/02/05/Lets_talk_about_feedback_critiques_and_Cattle_ranchers)
- [(2023) maintaining hope](/videos/2023/02/26/Lets_talk_about_maintaining_hope)
- [(2022) civic engagement, roe, electoral politics](/videos/2022/05/10/Lets_talk_about_civic_engagement_Roe_and_electoral_politics)
- [(2018) the September 11th attacks and how we lost the war on terror](/videos/2018/09/11/Lets_talk_about_the_September_11th_attacks_and_how_we_lost_the_war_on_terror)
- [(2023) a $100 oversimplification](/videos/2023/02/01/Lets_talk_about_a_100_oversimplification)
- [(2020) societal change and pronouns](/videos/2020/11/21/Lets_talk_about_societal_change_and_pronouns)
- [(2021) the importance of understanding basic philosophies](/videos/2021/09/18/Lets_talk_about_the_importance_of_understanding_basic_philosophies)
- [(2018) buying a gun because of fear of political violence](/videos/2018/10/30/Lets_talk_about_buying_a_gun_because_of_fear_of_political_violence)
- [(2021) acceptance and reaching rural americans](/videos/2021/06/11/Lets_talk_about_acceptance_and_reaching_rural_Americans)
- [(2019) 14 characteristics, 10 stages, and where you are](/videos/2019/07/01/Lets_talk_about_14_characteristics_10_stages_and_where_you_are)
- [(2022) the us already learning from ukraine and what we should...](/videos/2022/04/18/Lets_talk_about_the_US_already_learning_from_Ukraine_and_what_we_should)
- [(2020) why people believe certain theories](/videos/2020/05/10/Lets_talk_about_why_people_believe_certain_theories)
- [(2022) bias and narratives](/videos/2022/12/29/Lets_talk_about_bias_and_narratives)
- [(2023) the male scale](/videos/2023/02/04/Lets_talk_about_the_male_scale)
- [(2020) aoc, performative posts, and social change](/videos/2020/10/08/Lets_talk_about_AOC_performative_posts_and_social_change)
- [(2022) good news on climate and why we got it](/videos/2022/09/26/Lets_talk_about_good_news_on_Climate_and_why_we_got_it)
- [(2022) community networking 101](/videos/2022/05/08/Lets_talk_about_community_networking_101)
- [(2022) community networks and what you can do](/videos/2022/05/09/Lets_talk_about_community_networks_and_what_you_can_d)
- [(2022) turnkey community networks](/videos/2022/05/10/Lets_talk_about_turnkey_community_networks)

# Nov 2023 topic map of videos

Good for finding things by vibe. Eg, try using the search button on the top left to search by title or fulltext, then browse things near those dots.

[Guide to how to use this map](https://docs.nomic.ai/atlas/capabilities/data-interface) // [view beau videos map](https://atlas.nomic.ai/data/laurenpinschannels/beau-videos-wip-no-share/map) // or view another map of [60k videos from various progressive channels](https://atlas.nomic.ai/map/ed14732d-27ff-4a99-81b9-cd40bd30b5ad/b6c681be-54ea-4922-ae2d-7d9389921d64)

[![screen_shot_2024-02-23_at_0.45.09_am.jpg](/screen_shot_2024-02-23_at_0.45.09_am.jpg)](https://atlas.nomic.ai/data/laurenpinschannels/beau-videos-wip-no-share/map)


# To do (Sign in and you can contribute!):

(warning: wiki admins be able, in principle, to see your email when you sign up. We promise to not do anything bad (or anything at all) with your email; Unfortunately, the way [wiki.js works](https://docs.requarks.io/auth) makes it a lot of work to change the email requirement.)

#### wiki edits:

- Edit the 4k or so crappy ai summaries!
    - Of particular interest would be focusing in on the most important actions.
        - If a video doesn't suggest actions, the ai still probably suggested actions; remove those, prolly.
        - If a video did suggest actions, the ai didn't necessarily get the right ones.
    - Same deal for quotes. Not all videos have a quote for the ages. For those, the quotes section should be empty.
    - Probably about 50% of the bullet point summaries are too low quality to keep. Some of them are so bad that just deleting them is better than nothing.
- Make a list of the most timeless beau videos
    - Progress: lots of them linked above! Are there more?
- Make an overview page of how to contribute
    - Progress: mentioned above!
- Make an overview of what to do if the wiki goes down!
    - cliff notes: see
- Make topic pages with links to videos on that topic and a synthesis of what those videos have to say
    - This could be seeded automatically a number of ways, lauren has ideas, talk to her in discord
- add page with list of links to other cool stuff.
    - cool little how-to website about communities: https://www.microsolidarity.cc/
    - other cool little "how to" websites.
    
#### wiki admin:

- anyone: figure out some sort of monthly funding thing so lauren isn't paying about $15/mo to host stuff.
- lauren or anyone technical: add a guide for how to run the update script that pulls new videos.
    - short version: check out https://gitlab.com/ds9fan/beau-subtitles-wiki-base/ which is the "database" repo that auto-transcribes and then uploads to https://gitlab.com/DerSaege/btfcwiki/-/tree/wiki?ref_type=heads which is where the actual editable wiki data is hosted
- anyone technical: test python automatic video subtitler on windows
- Reach out to other youtubers to see about hosting their stuff here? In the hope of transcribing a few more channels and adding those listings as well.
- get the wiki registered with search engines at all (it will maybe happen naturally eventually but it hasn't been mentioned much in public yet; that said, wiki.js tends to get realllly low search rankings on google)
    - probably depends on swapping out wiki.js with something else
- push the modified wiki.js code to gitlab. (it's a pretty small change, disables emails in git commits)
- pick a custom icon for the site rather than using default wiki.js icon
- Retitle "just a video wiki" to something a little more memorable?

crazy ideas:

- make wiki.js render [[wikilinks]] correctly, using the same rules as obsidian.
- try hosting the wiki on something like jamstack or obsidian publish with better navigation, and put the wiki.js instance behind an "edit" button. (wiki.js is kinda janky for purposes besides editing, compared to how good the best stuff is. good enough for now, though.)

Done!:

- ***Done**: automatically update!*
- ***Done**: translate video script to python!*
- ***Done**: make index pages for all videos!*