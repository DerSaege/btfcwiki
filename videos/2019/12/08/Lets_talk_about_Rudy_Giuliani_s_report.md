---
title: Let's talk about Rudy Giuliani's report
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Jz-IYX0GHWU) |
| Published | 2019/12/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau sets the stage to talk about Giuliani's report and shares a hypothetical story about a cop convinced of someone's wrongdoing.
- The cop, obsessively watching the person's house, discovers stolen property inside.
- Both the cop and the person with stolen property get arrested after separate trials, illustrating accountability.
- Beau expresses curiosity about Giuliani's report, wanting to see the evidence against the Bidens.
- He mentions that if Biden abused his office, he should face consequences, but the same accountability should apply to Trump.
- Beau stresses the importance of holding leaders accountable for any abuse of power, regardless of political affiliation.
- He questions the timing of bringing up evidence during Biden's trial instead of the President's impeachment.
- Beau argues that evidence should not be used to muddy the waters but to ensure accountability.
- He concludes by urging for accountability for both Biden and Trump if wrongdoing is proven.

### Quotes

- "But it does not absolve the administration of any of their alleged crimes, even if they were right. It's not how this works."
- "Either what we believe is good and true is good and true, or we don't have any order. We don't have any laws."
- "If Biden abused his office for personal gain, he should go down. And if you believe that, you have to believe the same is true for Trump."

### Oneliner

Beau delves into Giuliani's report, advocates for accountability across political lines, and questions the timing of evidence presentation during trials.

### Audience

Citizens, voters, activists

### On-the-ground actions from transcript

- Hold leaders accountable for abuses of power (implied)
- Advocate for equal accountability across political affiliations (implied)

### Whats missing in summary

The passionate delivery and nuanced arguments made by Beau can be best experienced by watching the full transcript.


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight, we're gonna talk about Giuliani's report.
Imagine that.
I want you to imagine something else, too.
Tell you a story.
There's a cop.
And he's convinced, absolutely convinced,
that this other guy is dealing installing property.
Convinced.
Can't prove it, can't catch him.
But he's convinced of it.
So he starts keeping an eye on him.
To be honest, it kind of turns into an obsession for him.
Eventually, he's looking around the guy's house,
peeking in the windows, seeing if he can see something
that he can claim was in plain view.
One day, while he's doing this, the apartment manager
is out there with one of those chainsaws
on the end of a pole, trimming trees.
He's trimming trees, and that chainsaw falls,
Pops off the pole, falls, comes down, takes it off.
Takes it off.
Cop walks up to him and says, I got a tourniquet and a phone.
But I need your master key, though.
Of course, the guy gives it to him.
And then, cop goes in the house.
And sure enough, sure enough, there's stolen property there.
What happens next?
Cop goes to jail.
That's what happens next.
The cop goes to jail.
Now, because the police are aware of the activity, the
whole department is kind of keeping an eye on the guy.
And eventually, they get the evidence, and they arrest him
too.
That's how this works.
They hold two separate trials.
Wouldn't make sense to do it any other way.
Let's go back to Giuliani's report for a second.
I want to start off by saying, I want to see it.
I want to see what he turned up.
Not going to lie.
I'm curious.
I have said that when I looked into it, couldn't find anything illegal, but I
thought it was shady.
I am open to the idea that the Biden's broke the law and I have no
loyal to the, to the Bidens.
Biden is not my man.
I don't care.
So I want to see it.
But it does not absolve the administration of any of their alleged crimes, even if they were
right. It's not how this works. It's not how this works. And it doesn't make any sense to bring it
up during the president's impeachment. That's the part I really don't get. Because if this is real
evidence, well then we should see arrests. And that evidence would kind of be held quiet and
close hold. You wouldn't want to tell them right up front so they could, you know, develop a good
defense. You certainly wouldn't want to show it in a different trial. Unless, of course, the whole
intention is not to hold the Bidens responsible, but is to muddy the waters and the evidence
and is superficial and wouldn't really hold up to any scrutiny, that's another option.
But I do want to see it after the impeachment, because the truth of the matter is,
if Biden abused his office for personal gain, he should go down.
And if you believe that, you have to believe the same is true for Trump.
And if you believe it for Trump, you have to believe it for Biden.
It's the way that works.
Either what we believe is good and true is good and true, or we don't have any order.
We don't have any laws.
We don't have anything.
We just have corruption at the top, if they're not going to be held accountable.
So I do want to see it during the Biden's trial, not during the President's.
And the fact that the administration seems to have convinced their base that this type
of abuse of power is worth this kind of crazy investigation only shows that if that abuse
of power occurs, the people have to hold their leaders accountable.
If they turned up something on Biden, he should be held accountable, but it does not absolve
of the president, of the alleged soliciting the assistance of foreign
nationals, the abuse of power, obstruction of justice, none of it, it
doesn't change what the president did just because he was right, anyway, I
would suggest that if Biden is guilty, it's just all the more important to
hold Biden and Trump accountable.
Anyway, it's just a thought.
Y'all have a good night

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}