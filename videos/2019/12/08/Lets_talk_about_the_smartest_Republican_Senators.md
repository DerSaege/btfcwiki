---
title: Let's talk about the smartest Republican Senators....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=9aFvSldIoRk) |
| Published | 2019/12/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the political implications of Republican senators considering voting to impeach and convict Trump.
- Speculating on the potential consequences for these senators in terms of primary challenges and general elections.
- Pointing out the impact of a single-issue voter base whose goal is to remove Trump from office.
- Drawing parallels to the 2016 election and the potential effects on voter turnout based on the impeachment vote.
- Arguing that removing Trump from the equation may actually benefit Republicans in the long run.
- Warning about the dangers of a second term for Trump and the possible repercussions for the Republican Party.
- Advocating for putting aside loyalty to the party in order to save it from potential long-term damage.

### Quotes

- "I think that they understand what happened in 2016."
- "If you want to save the party, you've got to get rid of Trump."
- "Removing Trump is actually the safest play for Republicans."
- "It's gonna be easy to defeat somebody in a primary whose whole purpose in running is that you honored your oath."
- "Long-term, the worst possible thing in the world for the Republican party is a Trump second term."

### Oneliner

Exploring the political implications of Republican senators potentially voting to impeach Trump, with a focus on voter turnout, party loyalty, and the long-term consequences for the GOP.

### Audience

Politically engaged citizens

### On-the-ground actions from transcript

- Mobilize voters to understand the long-term implications of supporting candidates who prioritize country over party (implied).
- Engage in political discourse within your community to raise awareness about the potential impact of removing Trump from office (implied).

### Whats missing in summary

Insight into the historical context of the 2016 election and its relevance to current political decision-making processes.

### Tags

#Impeachment #RepublicanParty #PoliticalStrategy #VoterTurnout #TrumpAdministration


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're talking about the campaign trail and those handful of
Republican senators that are considering voting to impeach and convict Trump.
And we're just going to talk about it from a political aspect.
We're not going to talk about people honoring their oaths, the legality or
morality behind it just the politics because I got a feeling those people
might have a have a better understanding of what happened in 2016 they may be the
smartest people in the impeachment vote room see the president's coat towels
they aren't as long as they once were we saw that as he endorsed candidates and
They lost.
Can't energize the base.
Got a feeling that politics is a lot like that one scene from that gangster movie.
Only as good as your last envelope.
Doesn't appear that Trump's envelopes are very full lately.
So what happens?
Republican Senator votes to convict.
worst-case scenario, they get primaried by some far-right loon who's a Trump
loyalist. They're the incumbent senator. Big deal. It's gonna be easy to defeat
somebody in a primary whose whole purpose in running is that you honored
your oath. That should be an easy, that should be a pretty easy victory. Now on
the other hand, there's a lot of voters out there that are single-issue of
voters this time, and that goal of that single issue could be summed up with Trump out.
If they don't vote to impeach, Trump's on the ballot.
Those voters show up to vote against Trump.
They'll vote against all the Republicans, including them.
However if they vote to impeach, well those voters will stay home, Trump's gone.
energizing element, it's gone, which means the Republicans stand a better chance of
re-election because, well, no matter what, the Republicans are going to vote for
another Republican when it comes down to it.
I think that they understand what happened in 2016.
See, a lot of Democrats stayed home in 2016.
They didn't go vote because they saw what the DNC did to Bernie and they're like,
I'm done. So they didn't care. And then on the other side, you had a whole bunch of
people that showed up to vote against Hillary Clinton, not necessarily for
Donald Trump. Increased the Republican turnout because people really didn't want
her. Fair enough. See that same thing might play out in 2020. Smart Republican
senators may realize that their best chance for victory, and to stay in office, is to
get Trump off the ballot or remove that single issue that has so many people motivated.
If you do that, you keep a lot of Democrats, a lot of independents, a lot of never Trumpers
at home so they don't vote.
If they don't vote against Trump, they're not going to vote against the Republican senators.
They're not going to be there.
It also helps eliminate the idea of vote blue no matter who, because those people who do
show up to vote against Trump, they're going to vote against every single Republican, because
they're going to see them as enablers, as they are, so setting aside legality
and morality, just looking at it from the political side of things, removing
Trump is actually the safest play for Republicans because long-term, the worst
possible thing in the world for the Republican party is a Trump second term.
because then President Trump in his second term is going to have to deal
with the giant messes that President Trump created in his first term. And I
think all Republicans know that he is not equal to the task. He's made a lot of
errors, foreign policy decisions, economic decisions, that the effects of which
haven't been felt yet and they're going to be felt in that next turn. And if
If Trump's in office, he's just going to make them worse, and it will haunt the Republican
Party for decades.
It's funny.
All of these people screaming about party loyalty, and at the end of the day, if you
want to save the party, you've got to get rid of Trump.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}