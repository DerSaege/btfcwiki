---
title: Let's talk about a misunderstood Constitutional premise and a coin....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=m3RGizuRbK8) |
| Published | 2019/12/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of separation of church and state, not explicitly stated in the Constitution but implied.
- Describes how the United States was designed to be a secular nation where no law favored or opposed any religion.
- Mentions a treaty signed in 1797 with Tripoli stating that the U.S. government is not founded on the Christian religion.
- Emphasizes that while some founders were Christian, atheist, or deist, the goal was to avoid a theocracy.
- Addresses contemporary issues like prayer in schools, displaying religious monuments, and marriage laws in the context of separation of church and state.

### Quotes

- "The United States was designed to be a secular nation."
- "It was supposed to be secular. Does that mean that our founders weren't Christians? No."
- "When you are working for the government as an agent of government, you have no religious freedom."
- "The wall is up. Separation of church and state."
- "At the end of the day, I think it'll probably go back to what's in the Civil Rights Act."

### Oneliner

Beau explains the concept of separation of church and state and how it applies to various contemporary issues, stressing the secular foundation of the United States.

### Audience

Americans, policymakers

### On-the-ground actions from transcript

- Understand and advocate for the separation of church and state (implied)
- Ensure that public accommodations are available to everybody, regardless of religious beliefs (implied)
- Educate others on the historical context and intent behind the secular foundation of the United States (implied)

### Whats missing in summary

The full transcript provides a detailed explanation of the historical context and implications of the separation of church and state, offering insights into how this concept influences modern-day debates and policies.

### Tags

#SeparationOfChurchAndState #SecularNation #Constitution #ReligiousFreedom #CivilRights


## Transcript
Well, howdy there, internet people, it's Beau again.
So tonight we're going to talk about something
I've been asked a lot.
But any time I'm asked about it, it's very vague
and I don't really know what people want to hear
because it's a huge topic.
And then a couple days ago, I got a question
that kind of brought it all into focus.
Somebody said they were trying to find it in the Constitution
and couldn't.
I bet you couldn't because it's not there.
So tonight we're going to talk about the separation of church and state.
It's a philosophical concept.
It's a concept of jurisprudence.
It's not a constitutional one overtly.
It's implied.
It is implied, but it's not overtly stated anywhere in the Constitution.
They didn't think they needed to.
At the time, the people who wrote the Constitution, it was a given.
It was just accepted.
The United States was going to be a secular nation.
It's enshrined in a way in the First Amendment.
Congress shall make no law respecting the establishment of a religion or the free exercise
thereof.
Now, if you read the debate over how that was going to be phrased, it becomes very,
very clear. No law in favor or against, so nobody could be forced to participate
in a religious activity and Congress couldn't stop you from participating in
a religious activity. So what it meant, it's a real simple concept. When people
say the separation of church and state, especially the Supreme Court, they're
normally referring to Jefferson's letter to some Baptists. And that's what gets cited. However,
Madison used it. Everybody used it. The U.S. was designed to be a secular nation. No law in
favor or against any religion. That was the idea. When you tell people this they
will often say the United States was founded as a Christian nation. Okay
shortly after the Constitution was written and adopted, so shortly after that
the same people were still around, a treaty was adopted. We've learned in
other videos, treaties are law. The supremacy clause of the Constitution dictates that.
So anything that's in a treaty is U.S. law.
In 1797, the United States signed a treaty with Tripoli. Article 11 states,
as the government of the United States of America is not in any sense founded on
the Christian religion. So the people who wrote the Constitution were around when
this came into being. It wasn't. The idea that the United States was supposed to
be a Christian nation is demonstrably false. It's wrong. It's not true. It was
supposed to be secular. Does that mean that our founders weren't Christians? No.
Some of them were. Some were atheists. Some were deists. What it means is they
understood the dangers of theocracy and they didn't want it. They wanted nothing
So how does this apply to our hot button issues of today?
Kids praying in schools. Can children pray at school?
Absolutely. Can a teacher lead them?
No. What if the teacher's off doing it on their off time?
Yeah, if it's voluntary on the student's part.
If it's voluntary, there's nothing stopping the school from allowing a
If it's voluntary, there's nothing stopping a school from allowing a priest to come in
as long as they do it for every religion and every denomination.
And it's completely voluntary.
That's the key part.
It has to be free of coercion.
What about, let's see, Ten Commandment Monuments at courthouses?
Intent matters there.
If it's the Ten Commandments next to the Code of Hammurabi, it's a display of jurisprudence
history, no big deal.
If it's a religious monument out front trying to show that it's a Christian nation, well,
then it's an issue because you're violating the Constitution and oddly
enough, depending on your doctrine, you're probably also violating the Ten
Commandments. You might want to read those. What about people getting married?
Legally, if you can enter into a contract, you should be able to get married.
Period. Full stop. That should be the end of the qualifications right there.
Now does that mean that the government can force the first self-righteous church of wherever
to marry people it doesn't want to?
No.
No law for or against.
What if you're a clerk and your sincerely held religious beliefs say that you think
these people shouldn't get married?
You don't have to issue them a marriage license because you're religious freedom.
Nope.
When you are working for the government as an agent of government, you have no religious
freedom. The wall is up. Separation of church and state. What about people who
want to use their religious beliefs as an excuse to not cater to certain
people? That's where it gets tricky. Government cannot force people to
abandon their religious principles in a private setting. However, government can
regulate public accommodations. So that, constitutionally, that's a gray area. It
hasn't really been decided. There's been a few cases about it, but it's not
concrete yet. At the end of the day, I think it'll probably go back to what's
in the Civil Rights Act, what's defined as a public accommodation there will have to
be available to everybody, regardless of your sincerely held religious beliefs.
What about, in God we trust on our money?
That's one I go back and forth on.
It's not really enforcing a religion on anybody, it's very generic, it doesn't require anything
of you, I think the founders would probably look at that and be like, that's not really
what we wanted, but we're not going to be real mad about it.
I personally like what was on our first coin, the first coin the U.S. put into circulation.
It didn't say, in God we trust.
didn't say e pluribus unum. It summed up the entire bill of rights because the
entire bill of rights really it's your right to be left alone. It's really what
it boils down to and that coin the Franklin sent well it just said mind
your business. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}