---
title: Let's talk about UPS trucks....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=o-DYbOGMVBI) |
| Published | 2019/12/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- UPS truck incident prompts Beau to address unusual events in Miami involving law enforcement.
- Miami cops have a reputation, but not for tactical incompetence.
- Pursuit of UPS truck ends tragically with suspects, driver, and bystander killed.
- UPS trucks are equipped with sensors for tracking and monitoring.
- Tactical approach to the situation raises questions about law enforcement actions.
- Primary objective in such situations should be preserving human lives.
- Law enforcement's use of bystander vehicles as cover raises ethical concerns.
- Ineffective and inaccurate fire directed at the vehicle during the incident.
- Critique on the failure of planning, logistics, and training in the UPS truck pursuit.
- Beau concludes with reflections on the need for better preparedness and training in law enforcement.

### Quotes

- "Speed, surprise, and violence of action."
- "The primary objective is the preservation of human life."
- "Planning, logistics, and training failed here massively."

### Oneliner

Miami UPS truck pursuit raises questions about law enforcement tactics and the importance of preserving human life over property.

### Audience

Law enforcement officials

### On-the-ground actions from transcript

- Reassess training and tactics in law enforcement (implied)

### Whats missing in summary

The emotional impact on the victims' families and communities. 

### Tags

#Miami #LawEnforcement #UPS #Tactics #PreservingLife #Training


## Transcript
Well, howdy there, internet people, it's Beau again.
So tonight, we're going to talk about UPS trucks.
I don't normally like talking about stuff like this quickly.
However, half the messages, more than half the messages,
I got from my were asking about this event, OK?
I want to start off by saying that there
are things that could change some of what I'm about to say.
The only reason I'm willing to say that up front is because this is out of character
for this area.
Cops in Miami have a reputation.
It's not a good reputation.
It's not a reputation you would want for your department.
But that reputation does not include them being tactically stupid.
That isn't part of it.
They have a whole other set of problems they have to deal with.
But when it comes to situations like this, they're normally very deliberate.
So this was surprising happening there.
There are other jurisdictions where, I mean, it would have been normal.
Coming out of there, it's very odd.
So I am willing to leave the door open to there being a piece of information that hasn't
been made public yet.
Okay, so what happened?
The UPS driver and his truck got snatched after an attempted jewelry heist.
The pursuit involved 30 to 40 vehicles.
It ended when the truck had to stop because of traffic on the road at an intersection.
At which point, the officers exited their vehicles and engaged.
And the end result of that was the suspects, the driver, and a bystander gone.
Let me tell you what I know about UPS trucks.
They are full of sensors.
Full.
UPS knows when that back door opens.
UPS knows when they put their seatbelt on.
UPS knows where that truck is.
Maybe that failed.
Maybe that was the reason for the pursuit, the way it occurred.
Under normal circumstances, in a situation like this, it probably would have been better
to back off just out of sight and allow them to think they've escaped.
And then once they're away from all of the other bystanders, close in again, at which
point you've regained that element of surprise. What do you need to win something like this?
Speed, surprise, and violence of action. You can't have surprise when there's 30 blinking
lights in the rear view. In a situation like this, the primary objective is the preservation
of human life, not getting arrests, not recovering property, nothing except saving lives.
Now when they exited the vehicle, when they exited their vehicles and went to move in,
they used bystander vehicles as cover, and using a vehicle as cover is normal.
normal. However, it's normally your own vehicle or an unoccupied one. Why
do you get behind cover so it can absorb fire? When you get behind an occupied
vehicle you are using them as a human shield. They can't leave, they can't go
anywhere. That's why the truck wasn't moving. The fire directed at the vehicle
was less than accurate. It was an immense amount of fire and it was all over the
place. There were a lot of issues here, some of which cannot be explained away
way by information that we don't know yet there's a saying everybody wants to
be an operator until it's time to do operator stuff now normally when people
say that what they mean is people think they want to do this until the first
time it gets loud and then they realize this isn't really for them kind of a
everybody wants to be a door kicker until they get to the door and realize
that on the other side of it isn't paper this time. When I say it, I mean something
else. I say it with the understanding that doing that type of stuff, the final
1% is pulling the trigger. The 99% before that is planning, logistics,
and training. Planning, logistics, and training failed here massively. There
There could be information that comes out that mitigates some of that, but not much.
Anyway it's just a thought.
y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}