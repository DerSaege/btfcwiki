---
title: Let's talk about white vans and legends....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Ck2iZM3stFg) |
| Published | 2019/12/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the urban legend about white vans in the United States where people with white vans abduct others for organs.
- Mentions that the story originated in Baltimore and caused panic, prompting the Baltimore PD to issue a statement debunking it.
- Emphasizes that the core of such stories is often about increasing situational awareness and being cautious.
- Points out the importance of being aware of your surroundings, especially when vulnerable like when putting a child in a car seat.
- Gives an example of a firefighter spreading a false meme about smoke detectors to encourage people to replace batteries for safety.
- Expresses that while many of these viral stories are untrue, they often carry real lessons that can benefit society.
- Criticizes the loss of the ability to distinguish facts from fiction and the missed opportunities to learn from fictional stories.
- Encourages looking beyond the story itself to find the lesson embedded within most viral tales.
- Concludes by urging viewers to focus on the lessons rather than the sensationalism of viral stories on social media.

### Quotes

- "Most of these stories, yeah they're not true, but the lesson is, the lesson's real and most of them are a positive benefit to society."
- "Somewhere not just did we lose the ability to tell facts from fiction, we lost the ability to learn from fiction, and that's sad."
- "So when you see one of these stories, one of these viral stories on Facebook or whatever, don't really think about the story. Think about the lesson."

### Oneliner

Be more aware of the lessons behind viral stories, as even if the story is fake, the lesson is real and valuable.

### Audience

Internet users

### On-the-ground actions from transcript

- Verify information before spreading it on social media (implied)
- Be cautious and aware of your surroundings, especially in vulnerable situations like parking lots (implied)

### Whats missing in summary

Beau's engaging storytelling and reflective message on the impact of viral stories.

### Tags

#UrbanLegends #SituationalAwareness #SocialMedia #FictionVsFact #InternetSafety


## Transcript
Well howdy there internet people, it's Beau again.
So tonight we're going to talk about white vans, stories, and I guess something we lost along the way.
I got a message asking me to debunk something.
And after reading it, I'm kind of like, do I really want to?
It's false.
But, it's also true.
So the story is that within the United States there's a network of people, and apparently
they all own white vans, and if you happen to pull up next to one of these vans in a
parking lot. Maybe it's dark and stormy night or maybe it's broad daylight but if
you park next to one you get out they're gonna drag you inside and you're gonna
wake up in a bathtub full of ice missing a kidney or on the boat from taken or
something else horrible. This story originated in Baltimore it's caused such
Such an issue there that Baltimore PD's actually put out a statement about it, a team of journalists
has investigated it.
It's not a thing, it's not actually happening.
But the story's going around on Facebook, and it's causing a little bit of panic.
So the thing is, what is that at its core?
What is that story, it's an urban legend, it's what it is.
It's a story designed to teach you something.
You need to be more situationally aware.
That's true.
Everybody needs to be situationally aware.
And you are vulnerable getting in and out of a car.
You know, if you're a mom and you're
putting your kid in a car seat, in the back seat,
You're very vulnerable.
You're not paying attention to what's going on around you.
Your backs to the parking lot.
Yeah, you should be more situationally aware.
Most stories like this have a lesson like that in them.
I know a firefighter who put out a meme, which I guess today passes for an urban legend at
times.
He'd had a bad night, and he had dealt with something he never wanted to deal with again.
So he created a meme that said that smoke detectors with low batteries were likely to
cause fires, got shared thousands of times, completely untrue, completely untrue.
But what was he trying to do?
He's trying to save people's lives, trying to get you to replace the batteries in your
smoke detector.
Most of these stories, yeah they're not true, but the lesson is, the lesson's real and most
of them are a positive benefit to society.
And when it comes to the idea of using a story to talk about something else, really
They can't get mad at anybody for doing that, that's kind of my whole shtick.
Somewhere not just did we lose the ability to tell facts from fiction, we lost the ability
to learn from fiction, and that's sad.
sad because true stories they can give you facts. Fictional stories well they
can give you truth if you look for it. So when you see one of these stories, one of
these viral stories on Facebook or whatever, don't really think about the
story. Think about the lesson, because most of them have one. So in this case, no, the
The story is not real, but the lesson is.
Anyway, it's just a thought.
Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}