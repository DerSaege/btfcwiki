---
title: Let's talk about shelters, sales, smells, and internet people....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=elHndoJW2aI) |
| Published | 2019/12/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau Gadd says:

- Beau organized a livestream on YouTube where donations were earmarked for Christmas presents for teens in a shelter.
- Five bags were prepared, each containing a tablet, a Netflix card, a tablet case, earbuds, and a board game.
- The tablets and Netflix cards were included to provide the teens with their own space, especially if they have younger siblings monopolizing the TV.
- The goal was quickly accomplished through collective effort; people donated money and time to make it happen.
- Additional items like cleaning supplies were also provided to the shelter, which are often overlooked but necessary for PTSD sufferers.
- Beau questioned why companies don't donate products like diapers and soap to domestic violence shelters to avoid triggering bad memories.
- He suggested that companies could benefit from such donations through tax deductions, advertising, and building brand loyalty.
- Beau emphasized the importance of individual actions contributing to collective goals, showcasing the power of community effort.
- He expressed gratitude towards the internet community for their swift action in addressing the initial problem and going beyond it.
- Beau reflected on the world we live in, where companies could do more to support shelters, but also praised the internet community for their quick and effective response.

### Quotes

- "Y'all did this in about an hour."
- "Seems like a win-win-win-win."
- "Shows the value of individual action in pursuit of a collective goal."

### Oneliner

Beau Gadd organized a livestream to provide Christmas presents for teens in a shelter, showcasing the power of collective action and urging companies to support domestic violence shelters.

### Audience

Community members, donors.

### On-the-ground actions from transcript

- Donate products like diapers and soap to domestic violence shelters for survivors (implied).

### Whats missing in summary

The full transcript provides a detailed account of how collective action can make a significant impact on community issues beyond initial goals.

### Tags

#CommunityAction #CollectiveEffort #SupportShelters #Donations #InternetCommunity


## Transcript
Well, howdy there, internet people, it's Bo Gadd.
Y'all made a mess.
So if you don't know, we did a live stream on YouTube
and all the money that was donated, sent in was earmarked
for a specific purpose.
That specific purpose, these five bags back here.
We wanted to get Christmas presents for some teens
that are in a bad way.
They're in a shelter.
So, each one of these bags has a tablet, a Netflix card, a case for the tablet, earbuds,
and a board game of some kind.
We got the tablets and the Netflix card because a lot of them are going to have younger siblings
who will probably be monopolizing the TV.
This gives them a space of their own, a place that they can kind of retreat to to process
what's going on. I would imagine this is probably pretty difficult for them. So, this gives
them their own area, even if it's only a small screen in front of them. This is what we set
out to do. We accomplished that goal pretty quickly, because a whole bunch of people came
together. People got in the comments, in the chat, commenting, increased interaction, brought
more people in. People donated. Everybody, whether you sent money or not, donated your
time. And it made this happen. So once we got this, we took care of some other items
that the shelter needed, which cleaning supplies. They get food all the time via food drives,
but a lot of times people don't bring cleaning supplies. While I was buying this stuff, I
wondering why I had to. One of the things you learn about PTSD is that scent is a
very very strong trigger of memory. I got a bunch of kids. Every time we have a
kid, Huggies, Infamils, Similac, Loves, Pampers, they shower us with products in
in hopes of building that brand loyalty.
I don't know why anybody has to donate these to a domestic violence shelter.
Seems like these companies are missing out.
And I'm not just saying the multi-billion dollar corporations should be out there solving the problems of the world.
should but in this instance it can be self-serving because do you really think
somebody's gonna want their clothes to smell like they're abuser? Do you really
think they're gonna want to use the same soap? Probably not. Those scents are going
to carry bad memories. If a company has a new scent it seems like the ideal place
to send it to get it out on the market. Not just are most of these places charitable institutions
so it would be tax deductible. It's advertising, you're doing something good and you're building
brand loyalty. Seems like a win-win-win-win. No domestic violence shelter should ever need
any of this. These companies should be providing it. But they're not because that's the world
we live in. At the same time, we also live in a world with the internet people. Y'all
did this in about an hour. Set out to solve one problem, solve two, and did it pretty
quickly, it shows the value of individual action in pursuit of a collective goal.
and I can't thank you enough. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}