---
title: Let's talk with Emerican Johnson of Non-Compete....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=FObbjvpVVKA) |
| Published | 2019/12/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- American Johnson introduces himself as an anarcho-communist running the YouTube channel Noncompete, engaging with various leftist ideologies critical of capitalism.
- The channel, despite discussing complex topics like means of production, uses puppets and Legos to attract a younger audience, prompting concerns regarding COPPA guidelines.
- COPPA, aimed at protecting children's privacy online, fined YouTube $127 million for data harvesting from children, threatening content creators with penalties for not collecting data.
- American Johnson criticizes the FTC's COPPA enforcement, arguing that it contradicts the First Amendment by restricting free speech based on potential appeal to children.
- Concerns arise about potential FTC lawsuits affecting creators globally, like American Johnson's partner Luna, a Vietnamese citizen creating adult-focused content.
- American Johnson advocates for alternative social media platforms like Mastodon and PeerTube, promoting decentralized, community-owned networks as replacements for capitalist platforms.
- Encouraging viewers to join and create channels on these alternative platforms, American Johnson stresses the importance of supporting and building solidarity within the working class.
- In Vietnam, a culture of skepticism towards hierarchy and authority fosters local problem-solving and community-focused governance.
- American Johnson's call to action revolves around organizing, building solidarity, and fostering class consciousness as key solutions to societal challenges.
- He invites viewers to subscribe to his channel for updates on transitioning to PeerTube and participating in the YouTube walkout on the 10th, advocating for collective action and utilizing available tools to enact change.

### Quotes

- "The real solution to all of our problems is organizing together and building solidarity with the working class."
- "Unions have become a shadow of what they used to be. I think they're starting to come back, fortunately."
- "Let's use them, let's fight together, and let's change the world."

### Oneliner

Beau says the solution to societal problems lies in organizing together, promoting alternative social media platforms, and advocating for class consciousness and solidarity within the working class.

### Audience

Creators and activists

### On-the-ground actions from transcript

- Join Mastodon or PeerTube to support decentralized platforms (suggested)
- Start a channel on existing instances like PeerTube (suggested)
- Participate in the YouTube walkout on the 10th (implied)

### Whats missing in summary

The importance of collective action, utilizing alternative platforms, and fostering class consciousness are key takeaways from this transcript.


## Transcript
Well, howdy there, internet people.
Let's go again.
And tonight, we've got American Johnson with Noncompete.
OK, so give our viewers a little bit of insight.
Who are you?
That's a good question.
My name is American Johnson.
That's my real fake name, as I like to say.
I run the YouTube channel Noncompete.
You can find it at youtube.com slash Noncompete.
I consider myself an anarcho-communist, but we talk to leftists from around the world
from various different strains.
My partner who runs the channel with me is a Marxist-Leninist Vietnamese woman who's
sort of in the Ho Chi Minh school of the left, but we talk to social democrats, we talk to
all kinds, anybody who's basically critical of capitalism and wants to see radical change
in the way our systems are run and operated, that's what we're all about.
So I'm looking at your YouTube channel here.
I'm a little confused that this is obviously made for kids.
I see Legos and.
Yeah, we're all about trying to lure kids to our channel
by talking about old dead guys with beards talking
about the means of production.
That's our child-focused content.
That's our whole thing.
We do have puppets.
Yeah, so if you're not familiar with the channel we have like puppet shows where we have like
horses and goats and
Superheroes, you know struggling with their meager existence in capitalism and trying to find ways to overthrow
the the government
And I do a few videos with Legos as well
I have a series called how anarchism works where you know, we basically just use Legos as like a sort of like just
b-roll
so, you know to illustrate some of the points we're talking about and
And you know, I have but but I have never to my knowledge
I've never had a I might have children watching but I've never had any kind of interaction with a viewer who is a child.
So
But according to the YouTube Kappa guidelines, we've been given
Because it's colorful and fun
It's apparently for children, I guess I'll go ahead and kind of just quickly explain what Kappa is
It's an old law from I think 1998
Right and I think it's it's around the time when I was under 13 actually right right around the time
I turned 13 is when COPPA came out and it's basically it's it's not a terrible law
The whole idea is to protect the privacy of children on the internet and to keep companies from
harvesting data
From children which I think is you know that makes sense
I don't want you know, I don't have kids
But if I did I wouldn't want big corporations harvesting data of them and that sort of thing
And so that's what it really comes down to is the harvesting of data from children
Now, Google, through YouTube, has been aggressively harvesting data from children, specifically
on the platform of YouTube, and the FTC basically decided to finally do something about it.
But they worked out an agreement where they did fine YouTube about $127 million, I believe,
and that's basically it.
And then they worked out an agreement where they would punish content creators who don't
collect data.
If you violate this COPPA thing, according to what the FTC is saying, they will sue you for a $42,000-something fine.
If that happens to me, I've already decided, there's no way I have $42,000. I'm never going to have $42,000.
I'm going to see what I can do about fighting back in court if that should happen.
people who are saying that so basically in January the FTC is gonna roll out
some like more specific legislation or I guess I don't know they don't make
legislation because they're an agency so I guess it would be a whatever you call
it I'm not a lawyer yeah yeah but they're gonna yeah they're gonna make
more specific rulings and apparently some people are saying that it's gonna
be hopefully make more sense I mean to me it's like and as like a leftist
anarchist who completely opposes everything the USA stands for I still
know that we do have the First Amendment and this flies completely in the face of
the First Amendment because it doesn't say you know the freedom of speech shall
not be abridged unless it might be appealing to children so I can't see
this standing up in any kind of you know court situation at all like you know if
it's pretty easy to demonstrate you know that what I'm practicing on YouTube is
protected speech and if the FTC wants to try to limit that speech because I use
puppets in my videos I just can't see that standing up so I don't know if
anybody with the ACLU or anybody like that wants to reach out but if it goes
down that path where I get sued by the FTC I'm fully prepared to fight against
it you know the other thing is I live in Vietnam and like so my partner Luna okay
she's a like I was saying earlier she's a Vietnamese woman she lives here in
Vietnam she's a Vietnamese citizen she's never visited the USA she she's made
some videos that are like like for instance she she does a video where she
tells the Vietnamese version of the story of Cinderella which is a very
violent dark telling of the story but she like illustrated it she's very she
does these very beautiful drawings and I could definitely see somebody could say
like well that might be appealing to children even though she wasn't
targeting children at all when she made it like her audience is all adults from
what we know and but you know if she get it what would happen to her I mean
she's Vietnamese so well from what I understand the FTC would find YouTube
and if it gets into that kind of situation like I'm just trying to think
of head think ahead here as a creator as somebody who's a full-time youtuber you
know this is this is what I do for a living this is what Luna does for a
living I can only imagine YouTube aggressively airing on the side of like
shutting channels down and deleting content or making it on its inaccessible
if they get into the situation.
We need to start building our own alternative platforms
and supporting alternative platforms.
The Mastodon is actually an alternative to Twitter,
but it's a federated platform,
and it sounds complicated, but it's actually pretty simple.
So what that basically means is that
it's a social media network
that is composed of a lot of smaller social media.
They call them instances,
but you could just think of them as like a server.
It's probably the easiest way to wrap your head around it.
So anybody could start a Mastodon server
and then link it up with all the other Mastodon servers.
And then so what that does is it, A,
it puts the ownership of the social media
platform in the hands of the actual people who use it.
It's a lot more granular.
So if you're on a server and you don't like, so for instance,
well, let me give you a real world example.
There was a fascist group of people
who started their own instance on this platform.
And it was very pro-fascist, free speech,
absolutist kind of thing.
And so all the other instances in the Mastodon network,
which we call the Fediverse because it's federated,
all those other instances just basically banned that server.
So if that makes sense.
If you're running a server and you
don't like what's happening on another server,
you could just disconnect from that server
while staying connected to all the rest of them.
It's a great solution.
It's a distributed system.
It puts a lot of ownership in the hands of us, the users.
And now there is a maturing platform called PeerTube.
It is a direct competitor to YouTube.
It's built on the same kind of a system.
And so we have all these different instances
of PeerTube that we can join, or we can even build our own,
like, leftist strands of PeerTube servers.
But I think that ultimately, anybody
who's critical of capitalism, a goal of ours
should be to eliminate these capitalist social media
platforms and replace them with more open source, more community owned and
controlled and operated platforms.
If somebody has to start making these first steps, I'm going to be doing it.
And I encourage anybody else who's out there watching or making
content to check out PeerTube.
It's a work in progress.
It's always getting better all the time.
I am not technologically savvy enough to set up my own instances of this stuff.
So explain how it works.
How hard is it?
What do I have to do?
If I were to build an instance all by myself, you know, all by my lonesome,
I'm not so sure that I could handle like the security and keeping it updated and all that kind of stuff
So I would want to work with somebody who's like more of an experienced developer to just you know, keep it all
Maintained
it
So I think that the the real way to do this effectively would be to start finding groups of people
Who have various skill sets that we you know where we could work together
But what anybody could do is build a channel on an pre-existing instance
So I mean if you want to set up a channel on a pre-existing instance, it's as easy as building a YouTube channel
I mean you just make an account upload some videos put in your keywords and all that
It's exactly like basically operating a YouTube channel. So I would say as a community we should be working to build
more instances  but then as individuals we should just be like
creating accounts creating channels and
and most of all, watching stuff.
These systems are not designed to be addictive.
They're not designed to manipulate you.
They are not designed to emotionally control you
the way that Twitter and Facebook.
Well, there's a foot in that, man.
Yeah, right, exactly.
I mean, so Twitter and Facebook,
they invest millions of dollars.
They hire some of the greatest psychological experts
in the world to get you hooked
and to manipulate you and to control you
and to make you do things that will make other people a lot
of money.
These systems don't have those things.
So right out of the box, the first time
you log on to Mastodon or PeerTube,
you're going to find that it's not
as habit-forming as Twitter or Facebook.
And it's kind of weird.
The way it feels to me, and I'm a little, like I said earlier,
I'm 35 years old.
I was around at kind of the beginning of the big internet
boom in the mid to late 90s.
It feels more like that.
What's Vietnam like?
There's much more of a focus on family and friends
and enjoying life.
And you'll see people, I mean, I guess
what I really love about Vietnam is that the people work really
hard, but they're very happy.
And you don't see that kind of, I mean,
it's creeping in actually, especially
with the office culture.
So a lot of white collar jobs are
moving into the sort of Japanese Korean model,
where they're asking people to come in on Saturdays
and work overtime every day with unpaid
and all that kind of thing.
Hopefully they'll be able to avoid that
from getting too strong here.
But the organic culture in Vietnam for 1,000 years
has been very anarchistic.
They're very skeptical of hierarchy and authority,
believe it or not.
There's a saying in Vietnam,
which I think encapsulates what I'm talking about.
They say that the rule of the village
overrides the rule of the king.
So everything is kind of locally focused here.
If you look at the central government of Vietnam,
it is a little bureaucrat, it's very bureaucratic.
It's very bureaucratic, it's very top-down, hierarchical.
But then if you go into like a village,
and I've been to village,
like kind of like town council meetings sort of things,
and they solve problems together.
I'm gonna blindside you here.
Other than the peer-to-peer stuff
and the different social media outlets,
give me a solution to a problem.
Give me something.
You're talking about COPPA specifically?
In general, anything.
Something that people listening or watching to this right now
can be like, hey, I can do that.
Well, the real solution to all of our problems
is organizing together
and building solidarity with the working class.
I mean, that will always be the solution to our problems.
The problem is, the left in the United States of America in particular, but also I would
say around the world, has been gutted.
Unions have become a shadow of what they used to be.
Unions used to be very powerful, like central to the United States of American way of life.
Through the decades, the conservatives have just gutted unions.
I think they're starting to come back, fortunately.
I think we're starting to awaken again class consciousness with people partly just because of desperation
I think so many people are living such desperate meager existences that
We're looking for ways to overcome them and the unions a great way to do that
The whole point is that we learn how to work together. We learn as we go. We find connections with each other
We network together TLDR. The key is
Organization and building class consciousness parting shot
You can do that.
Tell them something.
Plug something.
Whatever you got.
Well, yeah, of course I'll tell you to subscribe.
Ironically, I will tell you to subscribe to my YouTube channel
at youtube.com slash not compete.
Even if you are fired up and ready to get rid of YouTube
and switch to PeerTube, I don't have a PeerTube instance yet.
Because again, it takes time to do this kind of stuff.
So what I'm going to be doing is my plan
is to roll out the PeerTube instance, or at least
my channel, in early 2020.
And I will be documenting that whole experience
on my YouTube channel at youtube.com slash non-compete.
I'll be giving updates.
I will be putting up tutorials for how to start your own PeerTube
instance.
And I will also be participating in the YouTube walkout
on the 10th.
So definitely, yeah, if you want to learn more about that,
you can check out youtube.com slash non-compete.
involved. It's understandable that we would have a fear of these unknowns, that we would
have trepidation about trying new things, and I understand why a lot of people might
feel like we're destined to failure, but I believe people have done amazing things in
the past. If you look at the history of the left, if you look at the history of the working
class, we have done phenomenal things in the past. There's no reason we can't do it now.
We have this technology, we have these tools that our predecessors did not have, so let's
Let's use them, let's fight together, and let's change the world.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}