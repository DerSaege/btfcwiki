---
title: Let's talk about what to expect next in the impeachment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=HrNG9cSCsBU) |
| Published | 2019/12/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau Gyan says:

- Criticizes the theatrics surrounding the impeachment process, labeling it as utter nonsense.
- Expresses concern over Republican representatives comparing impeachment to Pearl Harbor and the crucifixion of Christ.
- Points out the flawed argument of the lack of due process, especially when made by elected representatives.
- Explains the impeachment process, clarifying that the trial takes place in the Senate, not the House.
- Shares a historical anecdote about women gaining political power in Argonia in the late 1800s through a prank.
- Draws parallels between the current political situation and the waiting game for new evidence to sway opinions on impeachment.
- Emphasizes the importance of the Senate considering what the American people will accept before voting on impeachment.

### Quotes

- "It is utter nonsense."
- "Today I watched as Republican representatives compared the impeachment to Pearl Harbor and the crucifixion of Christ."
- "These are representatives on the House floor saying this, as if they don't actually understand how the process works."
- "The moral of this story is that right now, both Democrats and Republicans, they're waiting for that morning."
- "The Senate has to figure out what the American people will accept before they cast their vote."

### Oneliner

Beau Gyan criticizes the theatrics of impeachment, questions understanding of due process, and draws parallels to history and current political waiting games.

### Audience

Politically engaged citizens

### On-the-ground actions from transcript

- Understand the impeachment process and how it differs between the House and the Senate (implied)
- Stay informed about political proceedings and hold elected representatives accountable for their statements (implied)
- Advocate for transparency and accountability in political processes (implied)

### Whats missing in summary

A deeper insight into the historical context and implications of political theatrics in contemporary decision-making processes. 

### Tags

#Impeachment #PoliticalTheatrics #DueProcess #Accountability #HistoricalParallels


## Transcript
Buddy there, internet people, it's Bo Gyan.
So today, I guess we've got to talk about that impeachment nonsense some more.
And that's what it is.
It is now nonsense.
I'm not talking about the grounds.
There are certainly grounds to impeach the president.
I'm not talking about the process.
I'm talking about the theatrics that are now being thrown out.
It is utter nonsense.
And the big question is, is the Senate going to convict?
The pundits are saying that it's highly unlikely and that we don't have precedent.
We do have precedent for something this ridiculous.
We really do.
But I would agree it's probably unlikely.
Today I watched as Republican representatives compared the impeachment to Pearl Harbor and
the crucifixion of Christ.
I didn't see that coming, I got to be honest, I didn't see that coming, but what bothered
me more than the just ridiculous rhetoric was the continued complaint that the president
didn't have due process during the House's proceedings.
It bothers me because these are representatives saying this.
These are representatives saying this, not just random people on Twitter.
These are representatives on the House floor saying this, as if they don't actually
understand how the process works.
The impeachment in the House is the indictment.
There's nothing in the Constitution requiring the House to even notify them that it's
happening.
the trial takes place in the Senate.
I can understand a newly-minted representative
not knowing this when they arrive.
However, once an impeachment proceeding begins,
you kind of figure it's something
that you would brush up on.
The only options are either they don't know
the basics of their job, or they're intentionally misleading
their constituents.
They're playing on the gullibility of their voters.
I have to assume they're lying, but I could be wrong about that.
But at the end of the day, we do have precedent.
In the late 1800s in a town in Kansas called Argonia, women had just gotten a little bit
of political power.
They could have a say in political issues now.
Men didn't like it.
They didn't want women to worry their pretty little heads about such things.
So they orchestrated a prank.
And that prank was to take the local temperance movement, the Women's Christian Temperance
Union, take their platform, their preferred candidates, and nominate them but switch out
the candidate for mayor and replace it with a woman, Susanna Salter.
Miss Salter didn't know she was on the ballot until after the polls opened.
Did no campaigning.
But when, after the joke had been played, some people asked her if she would accept
if she was elected.
And she said yes.
What she won in a landslide.
The moral of this story is that right now, both Democrats and Republicans, they're waiting
for that morning.
Both are hoping that some new evidence is going to come out.
Democrats are hoping that more evidence of the president's misdeeds will surface and
cement the drive for removal and force the Republicans to remove the President.
That's what they're hoping for.
The Republicans are hoping for either the American public to grow weary of trying to
get to the truth or maybe some genuinely believe there is some exonerating evidence out there.
I mean if they don't know how their job works, it's very likely they didn't read any of this.
So there could be a few that earnestly hope the president is innocent.
They're waiting for that morning, they're waiting for those townspeople to walk up and
say, hey, would you accept?
And that's really what it's boiling down to.
The Senate has to figure out what the American people will accept before they cast their
vote. At least that should be their goal. We don't know if it will be. Anyway, it's
It's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}