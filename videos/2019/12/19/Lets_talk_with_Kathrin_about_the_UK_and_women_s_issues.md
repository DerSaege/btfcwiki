---
title: Let's talk with Kathrin about the UK and women's issues....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-h3WjmwGVqM) |
| Published | 2019/12/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduction of Catherine and her work as an intersectional anti-capitalist YouTuber covering social and political issues from an intersectional anti-capitalist perspective.
- Overview of the UK election results where the Conservative Party secured a significant majority while Labour faced a major defeat, with reasons including media bias and their stance on Brexit.
- Impact of the election results on Scotland and Northern Ireland, with the SNP gaining seats and potential calls for a second referendum for Scotland to leave England.
- Speculation on the impact of Brexit on the border between North and South Ireland, considering varying statements made by Boris Johnson regarding a hard border.
- Delving into Catherine's video on body positivity, critiquing how mainstream representations still reinforce normative beauty standards and capitalist consumption.
- Explanation of patriarchy as a system where men hold power, discussing how masculinity and femininity traits are valued unequally in society.
- Exploration of how patriarchy harms men, leading to issues like suppressed emotions, mental health problems, and harmful gender expectations.
- Connection between capitalism and patriarchy, detailing how capitalism historically oppressed women and continues to subjugate them for profit.
- Vision of a post-patriarchal society where all hierarchies are dismantled, allowing individuals to live without gendered norms and restrictions.

### Quotes

- "So many ways in which patriarchy harms men just as much well not just as much but definitely harms men as well."
- "Capitalism is inherently based off of the oppression of women."
- "I don't think that we can get rid of men dominating women without also getting rid of men, like humans dominating nature and dominating animals."

### Oneliner

Beau introduces Catherine, covers the UK election results, Brexit's impact on Ireland, body positivity's flaws, patriarchy's effects on men, and the ties between capitalism and patriarchy.

### Audience

Intersectional activists

### On-the-ground actions from transcript

- Advocate for gender equality and challenge societal norms through education and awareness (implied).
- Support organizations promoting body positivity and representation for all body types (implied).
- Engage in community dialogues about patriarchy and its impact on society (implied).

### Whats missing in summary

Exploration of deeper topics beyond the transcript, such as the podcast content and potential future collaborations.

### Tags

#IntersectionalActivism #UKPolitics #ElectionResults #BodyPositivity #Patriarchy #Capitalism #GenderEquality #SocialChange #CommunityAction #Awareness


## Transcript
I like your background, by the way.
Thank you.
Well, how do you do, internet people?
Let's bow again.
And today it's the battle of the accents.
We have Catherine from YouTube.
She's from the UK.
And she's going to tell us a little bit about what's
going on over there and then touch on some other subjects.
Catherine, start off by telling us a little bit about yourself.
Sure.
So yes, my name's Catherine.
I am an intersectional anti-capitalist.
I make YouTube videos about politics,
social and political ecology, veganism, feminism,
basically anything that relates to social
and political issues from an intersectional
anti-capitalist perspective.
I also work in the climate change mitigation field,
and I'm an advocate for children
who have experienced child abuse.
So yeah.
That is a huge list.
Yeah, so yeah, I've definitely I looked over your YouTube channel and there's a lot of stuff on it. That's uh,
Like I like the way you phrase things in your titles. It definitely makes people want to click without being being click
baity. It's nice  Okay, so the
The one question that everybody wants me to ask anybody from over there, tell us about
the election.
Oh, God, yeah.
Yeah, so first of all, I guess, for people outside of the UK who don't really know how
UK elections work, basically, there are 650 seats in the House of Commons and the House
of Commons is where we all the important political government decisions get made.
And in order for a party to have a majority in the government, they need 326 seats.
And so the Conservative Party, which is the right-wing party in the UK, they got 365 seats,
which is a huge majority.
And the Labour Party got 203 seats, seats is like the members of parliament are elected
in each constituency.
And 203 seats is the worst results that Labour have had since 1935.
So it was pretty shocking, especially since in 2017, they got, I think, 40% of the total
number of votes, total number of seats in the House of Commons.
So it was pretty bad.
So yeah, people are generally saying the reasons for why this has happened relates to one,
media bias, secondly, the referendum, the position labour had on the referendum. A lot of people
are saying that labour were too left-wing, which I think is not the case at all. But
yeah, I think one of the main reasons for why they failed did have a lot to do with
the media bias, because in the UK, 80% of the media is owned by just five billionaires.
So people like Rupert Murdoch and the Barty brothers who have a vested interest in making
sure people like Labour don't get in power because it would mess with their profit margins and their
overall politics and even the BBC which is supposed to be really impartial in the UK have also been
accused of being more in favour and reporting more favourably on Boris than on Corbyn generally,
which I would also agree to. There was a mass anti-Corbyn smear campaign going on throughout
the media and generally so much focus on his anti apparent anti-semitism, but relatively little
about Boris being racist, sexist and homophobic. So yeah, I think the media was a big issue, but I
do think that the Labour government should definitely take into account the fact that
their referendum position wasn't very effective at all. They basically, they did a poll in January
by my gov and it was asking people who had previously supported Labour why they now weren't
supporting Labour anymore and two-thirds of the answers related to Brexit and Labour had a policy
on Brexit which was basically that they wanted another referendum in six months time and the
Corbyn didn't make his position on the referendum clear at all, he didn't say whether he was pro or
against it. And I think in contrast, Boris had a very clear get Brexit done stance, which
a lot of people were in favour of. So basically, Labour lost so many leave voters because they
massively overestimated how popular a second referendum would be. But I don't really know
how much better it would have been if they had had a pro-Brexit stance, because then I still think
they would have lost a lot of leave voters who wanted the, you know, the super get Brexit done
stance of Boris was very appealing to a lot of people. So, yeah, I don't know how much that would
have helped. But yeah, and a lot of people are arguing that, well, Labour needs to be
left-left wing in the future. That was the main problem. But if it were really the case that
people wanted a left-left Labour, I think a lot more people would have voted for the liberal
Democrats, but they're kind of the more centrist left in between Conservatives and Labour in the UK.
They only got 11 seats overall, so I think it's pretty clear that a more centrist left isn't what
people want. People didn't vote for Labour because they thought the top five percent of people are
having enough of their taxes taken. They didn't vote for Labour because they thought it was bad
that we should give the NHS more money or do something about climate change. I think it was
much more to do with the media and the referendum position of Corbyn. So how does all of this impact
Scotland and Northern Ireland now that there's kind of a leave movement there? Yeah, so I think
The SNP got a lot of seats, I think it was 48 seats in the election and they were really,
really popular in Scotland and I think Nicholas Sturgeon, the leader of the SNP party,
Scottish National Party has called or is about to call for a second referendum or another second,
another referendum in Scotland to leave England and it's pretty positive that it's now going to
happen. I think the UK is not going to be the UK anymore. I think Scotland is going to leave
England and maybe even Wales is going to leave England. But I'm not so sure about Wales,
what's happening there, because the DUP, which is the main, like the Welsh party, Democratic Union
party didn't get many seats. So I don't think that that would happen. But yeah, I think Scotland is
is definitely going to leave England for sure.
And then the other question that we're
getting, especially even here, which
is odd to see this much interest in UK politics,
is do you think Brexit will result
in a hard border between the North and South and Ireland?
It's really hard to say because I
don't know because Boris has taken different chances
on this at one minute.
saying that there will be a border like you've been suggesting there was would be like a wall
between the south and the north but i do but i actually don't think that that will happen i
think that the northern island and south i that they'll still stay it will stay as it is but it's
it's difficult to say because i think there will be some conflict and in ireland but i don't know
enough about irish politics to know what's going on at the moment over there but yeah okay and just
So everybody knows at home, I kind of completely almost surprised her with all of those questions.
That's not really what we're here to talk about. So pick one of your videos and just kind of
delve us into where to where you take them. Like body positivity is a scam, is one that I saw.
And I think that that's a good kind of segue into the other stuff we want to talk about.
Yeah, so I guess we were going to talk about patriarchy and relating to body image and
body positivity. So I guess when it comes to, I guess what I was going into in the body,
in the video on body positivity, it wasn't about saying it wasn't like a fatphobic video,
even though the title might sound like that. It was basically talking about how today things
like body positivity are used in feminism, that we have this type of feminism that is advocating for
things like body positivity, which is like greater representation of certain bodies is supposed to be
the solution to the fact that we have these certain beauty ideals that are so pervasive and leading
to women being psychologically, physically, emotionally depleted. And basically this movement
has come about where there's greater, where the non-normative bodies are being shown more
in advertising and social media. But the thing is, I think for the most part, it's quite a
false solution because the majority of the time, the bodies that are being shown are very normatively
attractive bodies still, even if they are plus size, they tend to have hourglass figures,
conventionally attractive facial features and kind of curves in all the hypersexualized places.
So they're not actually really challenging body image issues, because by saying that
these models are representative of fat bodies, they're just contributing to more shame towards
bodies that are plus-sized, because the representation of them is still not accurate.
And even when they are accurate representations, most of the time they're used to advertise
different products such as like new clothing lines or even dieting pills and things like that
and that just is contributing to more capitalism more consumerism which are actually at the root
of a lot of patriarchy and sexism in our society. You know traditionally feminists radical feminists
have recognized that we need to tackle things like capitalism. Capitalism is at heart of
patriarchy, and we shouldn't be promoting more capitalism, and a lot of the images of body positive models and
body positive advocates in advertising, they're often reproducing the
typical male gaze, so they'll often look into the camera in typically seductive ways or position themselves
so as to invite the male gaze on certain areas of your body and things like that, and
And it's very much reproducing patriarchal male gaze.
Or they'll be wearing lots of makeup, or fake tan, or lots of new fashion items.
And I'm not saying there's anything inherently wrong with that, but it's kind of, there's
a contradiction when you're trying to subvert a certain norm, but then you're simultaneously
reproducing it, reproducing the idea of what women should look like and dress like.
So I think it actually ends up reproducing the exact same things that they're actually
trying to subvert in the end. Okay so for those that are not up to speed on it let's get a good
definition of patriarchy. Sure so there's different definitions of patriarchy so the first one tends
to be that a system of governments or society where men hold the power and women are largely
excluded from it. A second one relates to where men are like the head of the family or the
oldest male child is head of the family and rate and descent is reckoned through the male line.
And then there's also like a system where that is designed around patriarchal lines and patriarchal
values. And I think that there's a common misconception that patriarchy is just about
men against women, but it's actually more about masculinity and femininity, and these are terms
that relate to traits that are seen as characteristic of men or women. So masculinity
like refers to aggressiveness and domination and individualism and competition and violence
and things like that, and femininity is traits associated with softness and kindness, compassion
and caring, and we currently live in a system and society where we overvalue traits relating
to masculinity and undervalue feminine traits, so masculinity is seen to dominate femininity,
and that's the inherent problem, it's not that there's anything inherent in men that
makes them bad or inherent in women that makes them subordinates, it's about the way, the
characteristics that we value.
patriarchy hurt men? Yes, I think again this is a common misconception that
feminism is just about women but actually I mean one of the most
common problems with patriarchy is that men are taught to suppress so many
emotions, not allowed to cry, not allowed to express when they're hurting and
things like that, or they're only allowed to express it through anger or
regression. And this leads to so many issues, mental health
issues, they have a much higher rate of suicide than women, they
have much higher rate of alcohol abuse, men are much more likely
to end up in prison, they're much less likely to be taken
seriously if they have domestic or sexual experience, domestic
or sexual assault, they are much more likely to have to go to
war and fight in war, even if they don't want to. I think
fatherhood is often very difficult for men, because
there's certain ideas that they're not supposed to spend too
much time with their kids or play too much with their kids. Then there's also these gendered
expectations that they should go out to work all the time and take care of the family, which can
harm a lot of people who aren't able to do that or don't want to do that, and also these expectations
of what it is to be a real man. If you don't conform to these ideas of, you know, a tall,
muscley hyper masculine guy then it can be incredibly harmful for you so yeah
there's multiple ways in which patriarchy harms men just as much well
not just as much but definitely harms men as well. And how is capitalism and
patriarchy how are they tied? So some people say that there isn't a link
between patriarchy and capitalism because patriarchy preceded capitalism, but whilst it
is true that patriarchy preceded capitalism, basically capitalism profoundly transformed
the nature of women's subordination and oppression. So, prior to the 15th century women had a lot
more rights than after the 15th century when capitalism was institutionalized but from the
the 15th to 18th century capitalism was basically used as a means to subordinate and oppress women.
So they did this by, for example, they decriminalized rape as a means of defusing
workers protests, they institutionalized prostitution as a means of defusing worker
solidarity, things like women were forced into domestic and reproductive labor against
their will, so they could be relegated more into their home instead of working. And all of these
things basically meant that the institutionalizing of capitalism, the very foundations upon which
capitalism was built, was built off of the oppression of women. And I think that we can
still see that today. So anytime that women do get any rights under our capitalist system it's mainly
as a result of the fact that we need it for production and consumption. So, you know,
during World War Two, when they needed more females to work because men were out at war,
women were given the right to work. But then as soon as in the 1950s, when they no longer needed
women to work, they started to introduce in advertising this idea of domestic goddess and
femininity correlating with staying at home and doing domestic chores, and this served the consumer
purpose of getting women to buy household products which they needed in order to make money, but it
also helped to bring women back into the home, back into domestic labour and reproductive labour.
Then it went on to a change to beauty ideals that we see today which also helped to
to continue the subordination of women today and yeah, it just continues and again the
beauty ideal serves the consumer purpose by selling us fast fashion and the beauty industry
and dieting industry and all of these industries which help keep capitalism alive and the more
women see themselves as subordinate and objects to the male gaze, the more it keeps us from
revolting or challenging the system. And there is no incentive inherent in capitalism to do
anything about patriarchy. So no matter how much we try and fight for rights, or, yeah, for the
rights or equality for women, it's never going to be successful under a capitalist system,
because capitalism is inherently based off of the oppression of women. And it has no incentive to do
anything about it. Yeah. So what does a post-patriarchal society look like?
I guess it would look like where both men and women have complete equality, but I think it would
have to do with getting rid of all hierarchies in general. I don't think that we can get rid of
of men dominating women without also getting rid
of men, like humans dominating nature and dominating animals.
And I generally, I think, what would it look like,
I guess, where everyone would have the ability
to do with their lives whatever they wanted to.
There wasn't any kind of gendered norms
that you, as a woman, have to do this,
or you, as a man, have to do this.
whole idea of I guess we might even get rid of these ideas of certain sexes or not sexes but
of genders in general. Yeah, I don't know if that was a good answer. No, no, any answer is a good
answer. It's just, it's one, this is a topic here in the US, these terms are just so loaded. I mean,
yeah, they really are. And it's one of these things that it's hard to get people to even
discuss it, because a lot of them don't have definitions. They don't really grasp the ideas.
And it's, you know, one of those things about the messenger, and I'm not really the right person
to deliver that message at times. So, for those of you watching on YouTube, this is kind of the
introduction this is this is going to be the end of it because we're going to
delve into a lot of deeper topics and topics that wouldn't really fit on the
YouTube channel in the podcast so the link to that will be below if you're
listening to this on the podcast, we will be right back in just a moment.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}