---
title: Let's talk about why being "Anti-Trump" is wrong and solutions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=jwNHtA3hsUs) |
| Published | 2019/12/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Cable news is opinionated, telling viewers what to be against instead of presenting just news.
- Opposition should be based on moral obligation, not just rhetoric.
- Being against something creates resistance, not solutions.
- The nation is polarized because people focus on being against things rather than for them.
- Root causes of many issues are a lack of access to basic necessities and education.
- Lack of education leads to fear and opposition to things people don't understand.
- Democrats positioning themselves as anti-Trump is not sufficient to bring about real change.
- Solutions need to come from individuals, not just political leaders.
- Being anti-something is defensive, but being for something is proactive and necessary for progress.
- It's time to push for radical ideas that address poverty, basic necessities, and education to create real solutions and avoid repeating the same arguments in the future.

### Quotes

- "Being against something doesn't create solutions, just creates resistance."
- "We need to move forward with the more radical ideas, those that will create solutions."
- "The best defense is a good offense."
- "We need to be for ending poverty."
- "Teaching people to question. Narratives."

### Oneliner

Beau urges for a shift from being against to being for, advocating for radical solutions addressing poverty, basic necessities, and education to combat polarization and stagnation.

### Audience

Activists, Community Leaders

### On-the-ground actions from transcript

- Advocate for education reform to encourage critical thinking and questioning of narratives (implied).
- Support initiatives that address poverty and provide basic necessities in communities (implied).
- Shift focus from being against something to being for solutions in local activism and organizing (implied).

### Whats missing in summary

The full transcript provides a deeper exploration of the negative impact of polarization and the importance of grassroots action in creating real change. Viewing the full transcript offers a comprehensive understanding of Beau's call for proactive solutions and education to address societal issues.

### Tags

#GrassrootsActivism #PoliticalPolarization #EducationReform #CommunityBuilding #SocialChange


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about framing, we're going to talk about solutions, we're
going to talk about being against something rather than being for something.
Last couple of days I was with my in-laws.
My father-in-law is a conservative.
Not a nut conservative, but generally leans conservative.
That means Fox News was on.
Now, I don't know if y'all know this,
I don't watch cable news, like at all.
Every once in a while, if I have to,
I'll find a clip on YouTube if I need to see something.
But I don't watch cable news
because I don't like getting yelled at, really.
And that's what it is now.
It's, there are very few news programs that are just news.
It's opinion, it's telling you what to be against,
and that's how it's framed.
telling you what to be mad about, telling you what to oppose.
And there are some things out there that can be rightly framed as you have to oppose this.
No matter what, you've got to oppose this.
There's a lot of things like that.
But they're moral issues,
not political ones.
There are a lot of things out there that, yeah,
you gotta fight it.
You gotta fight against it no matter what.
But the thing is, if it's something that you
have to fight against, you have a moral obligation
to fight against it, you have a moral obligation
to actually defeat it, not just oppose it in rhetoric.
And that's where we're at.
We're at this point where we motivate people out of fear.
We tell them what to be afraid of.
We tell them what to be against.
rather than telling them what they could be for,
being against something doesn't create solutions,
just creates resistance.
You get two forces that are diametrically opposed,
pushing against each other, and you never get a solution.
That creates a static line on a battlefield, same thing.
You get half the country going one way,
half the country going the other.
They push against each other right there.
And it doesn't move much.
It's why we have the same hot button issues
that we've had since the 70s.
Because we're not moving anywhere.
There's no progress.
It's static.
It's how we create a polarized nation like we have.
It's time to stop being against things
and start being for them.
Start pushing for solutions.
And this has to go beyond rhetoric and slogans.
There are a lot of special interest groups
that will describe themselves as pro-something.
Are you though, really?
Or is it you're anti-something
and you're just framing it this way?
Most times that's what it is.
The thing is, those hot button issues,
those things we've been arguing about for 50 years,
They have the same three root causes, the same three root causes, and in most of them,
it doesn't matter if you're for or against it, you want it to go away.
You want this to no longer be an issue.
So rather than attack the behavior, attack the root cause.
What are the root causes?
a lack of access to basic necessities and a lack of acceptance. And that lack
of acceptance comes straight from a lack of education. And when I say acceptance
I don't mean that just in the social sense, although it's true there too.
People oppose those of other lifestyles because they're afraid of them. They
aren't educated on the topic, they don't understand it, you fear what you don't
know. And yeah, that certainly applies here. But think about people who deny climate issues.
Have you ever met anybody that actually sat down and read any of the data on this and looked at
any of it for any length of time that didn't believe it was true? No. It's education. So
So you've got poverty, access to basic necessities, and education.
Those are the three root causes.
You solve almost everything if you address those three things.
But that's not where we're going.
Democrats in general right now are positioning themselves as the anti-Trump.
Cool, yeah.
We need that, definitely.
We need to go the other direction, fact, absolutely.
But that's not enough. It's not enough to change the country, to make things better,
and it's probably not enough to win the election either.
I think that's a failing on their part.
We don't need more rhetoric. We need solutions.
The thing is, those solutions are going to have to come from us, and by us I mean me and you.
I mean me and you. Contrary to the way it's framed they're not our leaders
they're certainly not our betters as I like to say on this channel. A lot of
times they don't even understand the basic facts behind what they're
legislating. The solutions are going to have to come from us from the ground
level. Always, that is always true. So we can't waste any more time being anti
something. We've got to be for something. Being anti-something is a good defense.
It's a good defense. It can slow things down, but the best defense is a good
offense. We need to seize the initiative. We have a very politically aware
populace right now. People are involved because it's gotten so crazy. People are
paying attention. Now is the time to move forward with the more radical ideas,
those that will create solutions. We need to be for ending poverty. However, we have
to do it. I'm looking at those people that worry about pitchforks right now. We
have to be for providing basic necessities and we have to be for
education, real education. Not just standardized testing to create good
work or drones. Education. Teaching people to question. Narratives. How things are
framed. We have to move forward or we're gonna be here and in 2050 people will be
arguing about the same stuff we're arguing about right now. Those of us on
the bottom kicking down at each other. And those up at the top laughing. Anyway, it's
just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}