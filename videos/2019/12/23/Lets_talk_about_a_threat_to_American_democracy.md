---
title: Let's talk about a threat to American democracy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=BBhBNMsOqkc) |
| Published | 2019/12/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump campaign alleging political action committees are taking in money insincerely.
- Trump supporters not easily manipulated by silly slogans or memes.
- Allegation of almost $50 million flowing to these groups, mostly from contributions less than $200.
- Surprising that a massive demographic could be tricked easily.
- Public surprise at the lack of outcry to reform campaign finance despite $50 million involved in politics.
- $50 million enough to sway a candidate's vote or even buy one.
- Lack of accountability in methods used to influence votes.
- Concern that money given to Trump's campaign or political action committees may not serve contributors' best interests.
- Administration not interested in making America great or representing the electorate.
- No meaningful difference between giving money directly to Trump's campaign or political action committees.

### Quotes

- "Trump supporters getting conned? No, no, no, no. Trump supporters are not known for being gullible."
- "That's a massive demographic to be tricked that easily."
- "There's probably not a meaningful difference, not to the person who made the contribution."

### Oneliner

The Trump campaign alleges political action committees are taking in money insincerely, raising concerns about the lack of accountability and potential impact on American democracy.

### Audience

American voters

### On-the-ground actions from transcript

- Verify the authenticity of donation requests before contributing (implied).
- Advocate for campaign finance reform and increased transparency (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the alleged insincerity in fundraising for political campaigns and its implications on American democracy.


## Transcript
Well howdy there internet people, it's Beau again.
So tonight we have to talk about a very serious threat to American democracy, a significant one.
The Trump campaign is currently crying foul.
They are saying, suggesting, that there are a bunch of political action committees out
there that are less than sincere about helping the president win re-election, and that they
They are taking in money that they don't really use for the cause.
Now to be fair, I've gone to a couple of these websites and most of them have an disclaimer
saying they are not affiliated with the Trump campaign.
But the Trump campaign is nevertheless suggesting that they're almost conning people into giving
them money, that the person making the contribution would believe is going to help the president
reelected himself. Make America great again and all that. See, and this is where I have a problem with it.
Trump supporters getting conned? No, no, no, no. Trump supporters are not known for being gullible.
These are not people who can be emotionally manipulated with a silly slogan and a Facebook meme
or a 30-second soundbite. That's ridiculous. To suggest otherwise would mean that Trump supporters
really don't pay attention and we know that's not true. So the whole allegation
is surprising to me but it is more surprising when you find out the amount
of money that's alleged to have flowed to these groups is almost 50 million
dollars and that these contributions came in, the overwhelming majority of
them, were less than 200 bucks. You do the math on that. You're talking about
hundreds of thousands of people seems odd seems off seems unbelievable almost
that's a massive demographic to be tricked that easily and that is
certainly what the Trump campaign is kind of suggesting has happened whether
or not I believe it it doesn't matter the allegation has been made so we need
to do our part if you get one of these robocalls if you get one of these memes
understand that it may not actually be in support of the president, and it would
be horrible to give your money to somebody who said they were gonna do
something and then not do it and have no intention of doing it when what you
really meant to do was give your money to somebody who said they were going to
do something, who didn't do it and had no intention of doing it. That would be
pretty bad. See, here's the real surprise though, the public surprise anyway. Fifty
million dollars. You notice there's no cry to reform campaign finance, get rid of these
political action committees, get rid of the dark money. Of course not. Even with fifty
million dollars being whisked away, there's more money than that to be made.
That should give you a good idea of the scope.
Real good clear picture of how much money is involved in our politics.
Some would suggest that 50 million dollars is enough to sway a candidate's vote.
Others might suggest that it's enough to flat out buy one.
And in this case, it's not even enough to propose reform when it's lost.
When it's diverted.
That is a significant threat to American democracy because votes are up for sell.
With methods that, as shown by this, well there's not a whole lot of accountability.
And for every one of those $200 donations, there's somebody out there with a million
that's looking to get something in return.
But I wouldn't expect any outcry from the administration to change this.
Because they're not really interested in making America great.
They're not interested in representing those people who elect them.
The sad part here is the difference between giving the money to Trump's campaign directly
and giving the money to one of these political action committees.
There's probably not a meaningful difference, not to the person who made the contribution.
Either way, it's probably going to go to somebody that doesn't have their best interests at
heart. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}