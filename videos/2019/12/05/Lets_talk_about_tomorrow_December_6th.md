---
title: Let's talk about tomorrow, December 6th....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=c9XbsR7x7CY) |
| Published | 2019/12/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- December 6th should be a national holiday, marking the end of a dark American institution.
- The Emancipation Proclamation did not end the institution; it only freed slaves in conflict zones.
- Lincoln's strategic decision as commander in chief aimed to destabilize the South.
- The 13th Amendment was necessary to prevent the resurgence of the institution after the war.
- Lincoln pushed for the 13th Amendment, which was ratified on December 6th, 1865.
- Some states were slow to ratify the amendment, with Kentucky and Mississippi waiting until 1976 and 1995, respectively.
- The day marks the closing of a dark chapter in American history, yet it is not appropriately commemorated.
- It took another hundred years for further steps towards equality after the 13th Amendment.
- People who defended slavery until the end are remembered for their stance.
- Current events may lead to individuals being similarly remembered for supporting indefensible causes.

### Quotes

- "December 6th should be a national holiday, marking the end of a dark American institution."
- "The Emancipation Proclamation did not end the institution; it only freed slaves in conflict zones."
- "The day marks the closing of a dark chapter in American history, yet it is not appropriately commemorated."
- "People who defended slavery until the end are remembered for their stance."
- "Current events may lead to individuals being similarly remembered for supporting indefensible causes."

### Oneliner

December 6th should be a national holiday, marking the end of a dark American institution, but its significance is not appropriately commemorated.

### Audience

History enthusiasts, activists

### On-the-ground actions from transcript

- Advocate for December 6th to be recognized as a national holiday (suggested)
- Educate others about the importance of the 13th Amendment and its significance in American history (suggested)

### Whats missing in summary

The full transcript delves into the historical significance of December 6th and the 13th Amendment, shedding light on overlooked aspects of American history.


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about tomorrow, December 6th.
It's a day that I think should be a national holiday.
It isn't.
It marks the end of one of the darkest institutions
in American history.
And I know people are thinking, the Emancipation Proclamation.
No, no.
The Emancipation Proclamation did not
that institution, it simply freed the slaves. And it only applied within the
conflict zone. People used that as a way to attack Lincoln. See, he didn't really
care about slaves, he only freed him in this in this little area. No, it's because
he understood, at least somewhat, that there were limitations on his power. He's
not a king, he can't walk out and just speak legislation into the air. He did
that as commander in chief, it was a strategic decision that was made to destabilize the
South and also move towards his goal.
It didn't apply in border states, it didn't apply in areas that the Union had already
taken control over because they weren't in active rebellion.
So his war powers didn't matter.
areas had to go through the normal process.
And that brings us to the 13th Amendment.
Because without it, the Emancipation Proclamation didn't mean anything, nothing.
Because as soon as it was over, it could have sprouted back up.
There was no legislation to stop it from reoccurring.
So what did the 13th Amendment say?
That no amendment shall be made to the Constitution which will authorize or give to Congress the
power to abolish or interfere within any state with the domestic institutions thereof, including
that of persons held to labor or servitude by the laws of said state.
History people are like, that's not what it says.
No it's not, but it could have been.
This was the 13th Amendment that was proposed in December of 1860.
Right up until the last moment, there were those who were attempting to defend the indefensible.
They were trying to enshrine a state's ability to maintain that institution in the Constitution.
Didn't happen.
Didn't pass.
The real 13th Amendment, the one that was ratified, says, neither slavery nor involuntary
servitude except as punishment for crime, whereof the party shall have been duly convicted,
shall exist within the United States or any place subject to their jurisdiction, and then
Congress shall have the power to create appropriate legislation to make that happen.
very different things. Luckily we got the right 13th Amendment, but to speak to
Lincoln not really caring about it, this amendment was proposed first in 1863 and
1864, end of 1863, beginning of 1864, and it didn't pass. It failed to pass in the
House. So Lincoln made it part of the platform and forced the House, kind of
bullied him into it. And then once it passed the House and the Senate, it went out to the
states to be ratified. It was ratified and became an amendment to the Constitution on
December 6th, 1865. Now, a lot of states were slow to ratify it. For example, Kentucky didn't
ratify it until 76. Mississippi didn't ratify it until 95. And understand I'm
not talking about 1876 and 1895. I'm talking about 1976 and 1995. I mean their
ratification really didn't matter once once the number had been reached,
but it's interesting that it took that long. So one of the things is that this
day is not remembered the way it should be. This was the closing of the darkest
chapter in American history and we don't really do anything for it. We probably should.
And it was a stepping stone. It was another hundred years before
there were another set of steps taken to achieve equality, but this was a really
important one and we don't mark it. The other thing to remember is that right up
Until the end, there were people who attempted to defend the indefensible and they're remembered
for it.
I have a feeling we're going to see a lot of people get remembered that way due to current
events as well.
Anyway, it's just a thought.
have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}