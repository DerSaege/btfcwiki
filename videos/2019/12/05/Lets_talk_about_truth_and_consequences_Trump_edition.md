---
title: Let's talk about truth and consequences, Trump edition....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ob5Hhbn4oVs) |
| Published | 2019/12/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Trump is considering sending an additional 14,000 troops to the Middle East to prepare for conflict with Iran.
- The decision stems from the need to energize his supporters for the election season.
- The consequences of discounting foreign policy experts and abandoning allies, like the Kurds, are coming to light.
- Iran, similar to other countries in the region, has a Kurdish population historically friendly to the U.S.
- The president's focus appears to be on power and re-election rather than the well-being of troops.
- The move towards conflict seems driven by political motives rather than genuine concern.
- Trusting a businessman over generals may have severe consequences for American troops.
- The cost of prioritizing political gain over alliances and military expertise may result in significant losses.

### Quotes

- "He cares about power. He doesn't care about the troops."
- "The cost of believing that is going to be paid by them."
- "When you don't see them at the gas station for a while, you're going to know why."

### Oneliner

President Trump considers sending 14,000 troops to the Middle East, prioritizing election strategy over the well-being of troops and allies.

### Audience

Voters, Activists, Military Families

### On-the-ground actions from transcript

- Reach out to elected officials to voice opposition to escalating conflicts (implied)

### Whats missing in summary

Context on the potential consequences of prioritizing political gain over military expertise. 

### Tags

#Consequences #ElectionStrategy #ForeignPolicy #Allies #Troops #Iran


## Transcript
Well howdy there internet people, it's Beau again.
So tonight I guess we're going to talk about consequences, election strategy, and public
statements versus action.
While we were all watching the glorious and very entertaining proceedings in DC, reports
began to surface.
And they should come as no surprise because there's only one way a president has embattled
as President Trump is, can survive the election season.
And that's if he can mobilize those flag waivers, those people with yellow ribbons, those bumper
sticker patriots, those, thank you for your service.
That crowd has to be energized, and there's only one way to do it.
So it should come as no surprise that reports are now surfacing that the president is considering
sending an additional 14,000 troops to the Middle East to prepare for the seemingly inevitable
conflict he is intent on provoking with Iran.
I've done a video explaining in detail how Iran is different than Iraq or Afghanistan.
I'll put it in the comments section, there's no reason to go over it again, but we're going
to need a lot more than 14,000 troops and we're kind of hard up for help and I can't
imagine why.
See this is that moment when all of those people who discounted the foreign policy experts,
generals, all of the people that actually understand the Middle East, those people
that were like, yeah, don't worry, let's just go ahead and sell the Kurds out.
Leave them twisting in the wind, hang them out to dry, it doesn't matter, what are
they going to do for us?
This is the moment when you get to find out, and you're going to find out with a
bunch of flag-covered coffins that could have been avoided.
Iran, like a lot of other countries in the region, has a Kurdish population, a population
of people that has historically been very friendly to the United States.
I wonder if they're going to help in this seemingly inevitable war.
Probably not.
Why would they risk themselves?
There's nothing to gain.
Not just did we sell them out.
The president just congratulated Turkey on what a wonderful job they're doing in the
north.
So we sell them out after convincing them to take down their defenses.
And then we congratulate their opposition.
He needs this.
He needs this for re-election, but it's not going to be an October surprise that's over
in a few days, but it doesn't matter because he doesn't care.
He cares about power.
He doesn't care about the troops.
He never intended on bringing them home, none of them ever came back, just moved them to
a different area, because he needs a new conflict to secure his re-election.
So all of those kids, those young infantrymen, that you love to shake their hands and say
thank you for your service.
When you don't see them at the gas station for a while, you're going to know why.
It's because the American people trusted a businessman rather than generals.
It's because they were willing to sell out their allies, their friends, those people
who have saved American lives daily for 15 years because the president thought he could
get a good deal.
The cost of believing that is going to be paid by them.
So when they're missing, you can visit your local cemetery.
Anyway it's just a thought.
have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}