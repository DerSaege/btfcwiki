---
title: Let's talk about the global Green New Deal and 100% clean energy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=5eQQX3LBEhE) |
| Published | 2019/12/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the concept of the global Green New Deal as a massive infrastructure project to achieve 100% clean energy by 2050.
- Emphasizes that 95% of the technology needed for this project is already available commercially.
- Mentions that the remaining theoretical technology pertains to long-distance and ocean travel.
- Reveals the $73 trillion price tag spread across 143 countries, with potential to create 28.6 million more jobs than those lost.
- Asserts that the project will pay for itself within seven years, even if it takes longer.
- Notes the feasibility of the project both technologically and economically, with potential for completion by 2030 if resistance to change is overcome.
- Addresses human resistance to change as a significant factor in delaying the implementation of such projects.
- Raises the issue of inevitable opposition studies funded by oil companies that may slow down progress.
- Urges for immediate action due to the pressing need to combat climate change and pollution.
- Ends with a thought-provoking question on why not start the transition away from fossil fuels now.

### Quotes

- "It's a mortgage on the planet."
- "We are addicted to fossil fuels."
- "We are resistant to change."
- "We're going to have to do this at some point. Why not do it now?"
- "Y'all have a good night."

### Oneliner

Beau introduces the monumental global Green New Deal, suggesting immediate action to combat climate change and pollution, despite human resistance to change.

### Audience

Climate activists, policymakers, environmental advocates.

### On-the-ground actions from transcript

- Advocate for and support policies that prioritize transitioning to clean energy sources (implied).
- Raise awareness about the urgency of addressing climate change and pollution in communities (implied).
- Support initiatives that create job opportunities in the clean energy sector (implied).

### Whats missing in summary

The full transcript includes detailed insights on the economic feasibility, potential job creation, and challenges posed by human resistance to change when implementing monumental projects like the global Green New Deal.


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight, we're going to talk about the Green New Deal.
Not the one you're thinking of.
The global Green New Deal, the huge Green New Deal, the really,
really big Green New Deal.
Some researchers from Stanford who can only be described as
really ambitious put out a roadmap for 143 countries to
reach 100% clean energy, wind, water, and solar by 2050.
That is ambitious by anybody's reckoning.
The technology is already there.
95% of it is commercially available.
The only stuff that is still theoretical
is the stuff that deals with long distance and ocean travel.
Even if that isn't figured out, that's
That's a pretty small piece of the pie, really.
The price tag for this monumental global public works project and infrastructure endeavor
is $73 trillion with a T. But it is spread out over 143 countries.
importantly, it's going to create 28.6 million jobs more than are lost, 28.6
million on top of those that are lost. And that, combined with the economic
activity and the savings, this will pay for itself in seven years. Let's say
they're just really really wrong about that and it takes 20 years to do it, 20
to pay itself off. It's a mortgage on the planet. Even if you think that climate
change isn't a thing, pollution is bad. I think we can all agree on that. Here's a
solution to it. Here's a solution to it. The messed up part about this, it is
technologically feasible, it is economically feasible. So much so that if those were the
only concerns, they say this could be done by 2030. But those aren't the only concerns
because the planet is populated by people, humans, who are resistant to change. That
That extra 20 years is allotted to convince us that we need to do it.
Because it hasn't been out that long, this study.
But it won't be long until some oil company funds a study to attack it.
And we love things that confirm our own biases.
And studies like that, well they can just drag us around by the nose with them.
Because we want to believe them.
Because we are resistant to change.
We're going to have to do this at some point.
Why not do it now?
Why not start now?
We are addicted to fossil fuels.
going to be very reluctant to get off of them, even with a road map, but I think everybody
knows what happens if you don't deal with an addiction.
Anyway, it's just a thought.
y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}