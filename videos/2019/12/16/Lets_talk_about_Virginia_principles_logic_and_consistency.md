---
title: Let's talk about Virginia, principles, logic, and consistency....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=5CH4RbTbLdw) |
| Published | 2019/12/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- American political thought lacks principles, logic, and consistency.
- Virginia is proposing sweeping legislation, but local jurisdictions are resisting by creating Second Amendment sanctuaries.
- Beau's principles dictate that for something to be a crime, there must be a victim.
- He supports the local jurisdictions' decision to not comply with the proposed laws.
- Beau warns against deploying the National Guard to force compliance, as it could escalate and result in real victims.
- He mentions the importance of supporting other forms of sanctuary jurisdictions based on consistent principles.
- Beau argues that the Constitution reserves certain powers to the states and people, not the federal government.
- He stresses the need for logical consistency in principles, rather than being swayed by political propaganda.
- Beau encourages individuals to take a stand based on their true principles, even if it may lead to conflict.
- He questions whether individuals are truly acting on their principles or simply following directives.

### Quotes

- "If there's not a victim, there's not a crime."
- "The mere possession of something doesn't necessarily create a victim."
- "Either what you believe to be right and true is right and true or you don't have any principles."
- "You guys want to take this stand, more power to you, seriously."
- "But I would ask if they're your principles, or you're just being told what to do."

### Oneliner

Beau in Virginia challenges the lack of principles in political thought, supporting Second Amendment sanctuaries and urging consistency in beliefs.

### Audience

Virginia residents, Activists

### On-the-ground actions from transcript

- Support and join Second Amendment sanctuaries (exemplified)
- Advocate for consistent principles in political decision-making (exemplified)
- Engage in local governance to uphold community values (exemplified)

### Whats missing in summary

The full transcript provides a deeper exploration of the importance of individual principles and logical consistency in political decisions.


## Transcript
Well, howdy there, internet people, it's Beau again.
So tonight we're gonna talk about principles,
logic and consistency in Virginia.
Principles, logic and consistency are three things
that have been sorely lacking
in American political thought as of late.
We're gonna talk about this
because a whole lot of people have asked me my opinion
on what's going on up there.
My principles dictate that, in order for something to be a crime, there has to be a victim.
If there's not a victim, there's not a crime.
It's pretty simple.
I like to try to give freedom a chance.
I also believe that nobody knows how to run your life better than you, so the local jurisdiction
is going to be more responsive to the will of the people in that area.
simple, not complex, not complex ideas. Okay, so what's going on up there? The
state is proposing some pretty sweeping legislation. Local jurisdictions are
saying no, we're not going to do it, we're not going to abide by these laws, we are
going to create Second Amendment sanctuaries. Okay, because of my
principles, I have to say, the mere possession of something doesn't necessarily create a victim.
Therefore, if these guys want to run around and everybody in the county wants to carry an Uzi,
that's on them. I would also say that the local jurisdiction probably best represents the will of
the people in that jurisdiction. So yeah, the far right wing Virginians have my
support from afar on this one. Kind of thoughts and prayers things. You guys
want to do this go right ahead. I don't think the state should deploy the
National Guard to force compliance in this. I think that's a really bad idea
Indian can cause things to spiral out of control, which would probably create real victims.
Now there's a whole bunch of right-wingers going, yay, right now.
All right, now's the consistency part.
If these are your principles and you believe this, you have to support other forms of sanctuary
jurisdictions whether we're talking about other forms of prohibition or
immigration. The mere existence of a person does not create a victim. The
people in the local community decided what they wanted. Right now I'm sure if
somebody is saying you know there's no amendment saying we have the right to
keep and bear illegals. You're right but that's not how most the Constitution
works. The Constitution is a permission slip from the states to the feds. This is what you're
allowed to do. This is where you have power. If the power is not specifically listed in the
Constitution under the 9th and 10th amendments, it's reserved to the states and people at no
point in the Constitution is there a power granted to the feds to determine who comes in and out of
of a state. It doesn't exist. So even from the constitutional aspect, if you want to
remain logically consistent and you want to say these are your principles, you have to
stick to them. Otherwise, they're not your principles. You're being drug around by the
nose by some politicians that have found a talking point that resonates with you. It's
propaganda. Either what you believe to be right and true is right and true or you don't
have any principles. You just have whatever your betters tell you. This is what you need
to believe. And in this case, this is what you really need to take a stand for. This
This could get ugly, this could get ugly real fast.
It shouldn't, it shouldn't, but it could.
And it's not going to be those politicians egging you along, it's not going to be those
special interest groups from outside of the state or outside of the county, that have
to pay the price for it, it's going to be the little guy.
You guys want to take this stand, more power to you, seriously.
But I would ask if they're your principles, or you're just being told what to do, and
if they are your principles, are you remaining consistent with your beliefs?
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}