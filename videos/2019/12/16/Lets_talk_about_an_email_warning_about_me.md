---
title: Let's talk about an email warning about me....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=GWhQPNqqenM) |
| Published | 2019/12/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Received an email warning a shelter about his appearance before delivering charity supplies.
- Stereotype: resembling the image of a person you wouldn't want to buzz in - big beard, tattoos.
- Stereotypes can be dangerous as they may lead to missing signs of domestic violence.
- Domestic violence affects everyone regardless of race, ethnicity, gender, or socioeconomic class.
- Dangerous to perceive domestic violence as only happening in certain areas, leading to overlooking signs.
- Shelter previously printed shirts to dispel the stereotype associated with Beau's appearance.
- Domestic abusers don't have a specific uniform.
- Urges to be mindful of stereotypes and not overlook signs of domestic violence.
- Domestic violence is intersectional and can happen anywhere.
- Importance of dispelling harmful stereotypes surrounding domestic violence.

### Quotes

- "There is no uniform for a domestic abuser."
- "Domestic violence is truly intersectional."
- "We really need to remember there is no uniform for a domestic abuser."

### Oneliner

Beau warns about dangerous stereotypes surrounding domestic violence and the importance of not overlooking signs due to preconceived notions.

### Audience

Community members, advocates

### On-the-ground actions from transcript

- Support and volunteer at domestic violence shelters (implied)
- Educate others on the intersectionality of domestic violence (implied)
- Challenge and dispel harmful stereotypes about domestic violence (implied)

### Whats missing in summary

Importance of educating others to recognize signs of domestic violence and dispelling harmful stereotypes to ensure support for all victims.

### Tags

#DomesticViolence #Stereotypes #Intersectionality #CommunitySupport #Awareness


## Transcript
Well, howdy there, internet people, it's Beau again.
So tonight we're going to talk about stereotypes
and an email that was sent about me.
So as you all know, we did the charity drive for the shelter.
And when I took the supplies over there,
a well-meaning person sent an email giving them
a heads up about me.
enough you've ever been to a DV shelter, but you've got to be buzzed in.
Even just at the offices, you have to be buzzed in.
There are a lot of bad people out there, so there is a lot of security at these places.
The email was warning them that I look exactly like the stereotype of the person you wouldn't
want to buzz in big beard tattoos and there's no nice way to say that quote.
That's funny. It is. It's funny. It's not very flattering, but it's funny. But
there's a problem with it. There's a problem with it. That stereotype, mm-hmm.
something else. What's this? It's a tank top, right? But if it's white and ribbed, what's
the slang term for it? A wife beater. Because there's that image, there is that stereotype
of a guy that looks like me, wearing one of these, standing at the door as the police
show up. Because that's the image. That's the image that gets pushed in movies. And
that is a very, very dangerous stereotype. The reason it's dangerous is that it means
you may miss signs. See, domestic violence is truly intersectional. It affects everybody.
race, every ethnicity, every gender, every socioeconomic class, it affects everyone.
And if it's portrayed and viewed as something that only happens in the trailer parks, what
happens when it's happening right next door in the picket fence neighborhood?
You may miss the signs.
may write it off to something else because it doesn't happen here. It's a
very dangerous concept and the funny part about this to me is that years ago
this exact shelter went out of their way to dispel this image. They actually printed
shirts like this that said this is not a wife beater or this is a tank top
something like that to drive that point home. At the end of the day, we really
need to remember there is no uniform for a domestic abuser. Anyway, it's just a
thought.
have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}