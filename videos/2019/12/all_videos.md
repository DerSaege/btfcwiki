# All videos from December, 2019
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2019-12-31: We've talked about everything: A recap moving into 2020.... (<a href="https://youtube.com/watch?v=mEryako3A-U">watch</a> || <a href="/videos/2019/12/31/We_ve_talked_about_everything_A_recap_moving_into_2020">transcript &amp; editable summary</a>)

Beau challenges blind patriotism, addresses gun violence, and advocates for community empowerment.

</summary>

"Guns don't kill people. Governments do."
"We are a violent country. We always have been."
"If you make your community strong enough, it doesn't matter who gets elected."

### AI summary (High error rate! Edit errors on video page)

The president's decision to cut aid to Central American countries out of fear.
Concern over the wall working both ways to confine Americans.
Criticism of the younger generation's lack of exposure to accurate history.
Pointing out the propaganda in history textbooks and the internet's raw information.
Criticizing blind patriotism that supports harmful actions.
Addressing the tactic of using fear to win elections and gain power.
Challenging the unfounded fears and stereotypes around asylum seekers.
Criticizing the dehumanization and mistreatment of migrants at the border.
Addressing the issue of civilian gun violence and the role of governments.
Arguing against the effectiveness of an assault weapons ban.
Exploring the glorification of violence and its impact on society.
Advocating for understanding the historical trauma of minority communities.
Pointing out the importance of arming minority groups for protection.
Addressing the fear and division around the changing demographics of America.
Criticizing the lack of accountability and excessive use of force by law enforcement.
Drawing parallels between legal actions and moral implications in history.

Actions:

for community members, activists,
Contact your representative to address foreign policy impacting migration (implied).
Advocate for understanding historical trauma in minority communities (implied).
Strengthen your community to be self-sufficient regardless of political leadership (implied).
</details>
<details>
<summary>
2019-12-31: Let's talk about Ice T, hobbies, and power.... (<a href="https://youtube.com/watch?v=l9IzxwTAFC8">watch</a> || <a href="/videos/2019/12/31/Lets_talk_about_Ice_T_hobbies_and_power">transcript &amp; editable summary</a>)



</summary>

"Politics isn't a hobby. Politics is power."
"We stop letting politics be a hobby. We start letting it be a calling."
"Stick to what you're truly passionate about and work towards bettering that particular topic."

### AI summary (High error rate! Edit errors on video page)

Beau starts by discussing ICE-T's tweet about a Napoleon quote and a flaming queue image, which led to controversy.
Politics is described as a hobby that some are deeply steeped in, mentioning a far-right theory related to the Trump administration.
Politics should not just be a hobby but a powerful force that requires action to convert beliefs into tangible change.
Beau encourages moving from politics as a hobby to a calling, suggesting taking tangible actions once a month to make a real-world impact.
He stresses the importance of channeling passion into action to see significant changes in society, especially in the year 2020.

Actions:

for activists, community members,
Organize a tangible action once a month to affect real change (suggested)
Channel passion into making a difference in a chosen topic (implied)
</details>
<details>
<summary>
2019-12-29: Let's talk about the global Green New Deal and 100% clean energy.... (<a href="https://youtube.com/watch?v=5eQQX3LBEhE">watch</a> || <a href="/videos/2019/12/29/Lets_talk_about_the_global_Green_New_Deal_and_100_clean_energy">transcript &amp; editable summary</a>)

Beau introduces the monumental global Green New Deal, suggesting immediate action to combat climate change and pollution, despite human resistance to change.

</summary>

"It's a mortgage on the planet."
"We are addicted to fossil fuels."
"We are resistant to change."
"We're going to have to do this at some point. Why not do it now?"
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Introduces the concept of the global Green New Deal as a massive infrastructure project to achieve 100% clean energy by 2050.
Emphasizes that 95% of the technology needed for this project is already available commercially.
Mentions that the remaining theoretical technology pertains to long-distance and ocean travel.
Reveals the $73 trillion price tag spread across 143 countries, with potential to create 28.6 million more jobs than those lost.
Asserts that the project will pay for itself within seven years, even if it takes longer.
Notes the feasibility of the project both technologically and economically, with potential for completion by 2030 if resistance to change is overcome.
Addresses human resistance to change as a significant factor in delaying the implementation of such projects.
Raises the issue of inevitable opposition studies funded by oil companies that may slow down progress.
Urges for immediate action due to the pressing need to combat climate change and pollution.
Ends with a thought-provoking question on why not start the transition away from fossil fuels now.

Actions:

for climate activists, policymakers, environmental advocates.,
Advocate for and support policies that prioritize transitioning to clean energy sources (implied).
Raise awareness about the urgency of addressing climate change and pollution in communities (implied).
Support initiatives that create job opportunities in the clean energy sector (implied).
</details>
<details>
<summary>
2019-12-29: Let's talk about Aleksandr Solzhenitsyn and themes.... (<a href="https://youtube.com/watch?v=1yVS-FLHnc4">watch</a> || <a href="/videos/2019/12/29/Lets_talk_about_Aleksandr_Solzhenitsyn_and_themes">transcript &amp; editable summary</a>)

Beau delves into the universal themes of propaganda, ideology, and power in Solzhenitsyn's "The Gulag Archipelago," urging readers to grasp the insights applicable to all governments and ideologies.

</summary>

"Without evildoers, there would have been no archipelago."
"No government is best, no ideology is best, that they can all be corrupted, and that they can all create evildoers who believe they are doing good."
"There's a lot in it that you'll have to research the history of along the way. But it's worth it."

### AI summary (High error rate! Edit errors on video page)

Alexander Solzhenitsyn published The Gulag Archipelago in 1973, a significant work detailing the brutal Soviet prison system.
Solzhenitsyn wrote the book in sections, not carrying all of it at once due to security concerns.
The book is often seen as a criticism of state communism under Stalin but Beau believes it goes beyond that.
Beau points out the universal nature of the book's themes on propaganda, legality, and morality in all governments and ideologies.
The work examines how collective ideologies can justify actions that individuals wouldn't justify on their own.
Solzhenitsyn understood the dangers of absolute power and unjust hierarchies in governments.
Beau encourages reading the book for its insights into how governments operate and create evildoers who believe they are doing good.
Despite being a Russian book with historical references, Beau recommends it as a valuable read for understanding government dynamics.
Beau notes the parallel between the number of incarcerated individuals in the US today and those in the gulags.
The book's message and subtext make it a valuable read beyond its historical context.

Actions:

for readers, history enthusiasts, book lovers,
Read "The Gulag Archipelago" to understand governmental dynamics (suggested)
Research the historical context of the book's themes (suggested)
Share the insights from the book with others (implied)
</details>
<details>
<summary>
2019-12-28: Let's talk about what it means to be a Republican.... (<a href="https://youtube.com/watch?v=5bUwzgIt4yw">watch</a> || <a href="/videos/2019/12/28/Lets_talk_about_what_it_means_to_be_a_Republican">transcript &amp; editable summary</a>)

Exploring the historical Republican values through Eisenhower's presidency, contrasting past policies with the present party stance.

</summary>

"It's also worth noting that he used his farewell address to warn the United States about something called the Military Industrial Complex."
"But it meant that you hate all of his policies."
"At this time, Republicans still pretended, at least, to care about those people who voted for them."
"That's the reality of it."
"He was a great Republican president in history. Just remember, it's history."

### AI summary (High error rate! Edit errors on video page)

Explains the historical context of the Republican party and what it used to mean.
Mentions the transition from Roosevelt to Eisenhower, showcasing the values and policies of these presidents.
Talks about how Eisenhower continued and expanded on the New Deal policies initiated by Truman.
Describes Eisenhower's actions in office, including strengthening Social Security, increasing the minimum wage, and initiating major public works projects like the Interstate.
Emphasizes Eisenhower's understanding and empathy towards those in need, reflected in policies like the Department of Health, Education, and Welfare.
Notes Eisenhower's support for civil rights, mentioning his involvement in desegregation efforts like the Little Rock Nine.
Acknowledges Eisenhower's faults, such as authorizing CIA activities against communism.
Points out Eisenhower's warning about the Military Industrial Complex in his farewell address.
Reminds the audience of the contrast between Eisenhower's policies and the current stance of the Republican party.
Encourages reflecting on history and understanding the evolution of political ideologies.

Actions:

for history enthusiasts, political analysts.,
Study and understand historical political ideologies (implied).
Analyze and compare past and present policies of political parties (implied).
</details>
<details>
<summary>
2019-12-27: Let's talk about the state and finding the money for it.... (<a href="https://youtube.com/watch?v=veVGLwT5vxw">watch</a> || <a href="/videos/2019/12/27/Lets_talk_about_the_state_and_finding_the_money_for_it">transcript &amp; editable summary</a>)

Beau addresses the funding double standard, linking societal well-being to supporting the marginalized; failing to fund projects for the disadvantaged is akin to killing the state.

</summary>

"When you hear that, just assume it's something that rich people don't want to do."
"Those social safety nets that keep the elderly, the disabled, and just the general poor, keep them alive. Well, those people, they are the state."
"Failing to find the money for it is literally killing the state."

### AI summary (High error rate! Edit errors on video page)

Addresses the question of finding money for projects benefiting lower socioeconomic classes in the U.S.
Points out the double standard in funding when it comes to projects that benefit wealthy classes vs. the general populace.
Mentions Chuck DeVore's statement on progressive policies in California.
Talks about the state's role in serving the people and the dangers of blending corporate interests with government.
Explains the term fascism in the context of a state serving corporate interests.
Notes the lack of funding for education, especially if opposed by wealthier classes.
Gives an example of funding for a project benefiting the wealthy, the Slavery Abolition Act in the UK.
Mentions the extensive loan taken out for reparations to slave owners, paid off in 2015.
Emphasizes that policies benefiting the marginalized actually benefit society as a whole.
Concludes by suggesting that failing to fund projects for the disadvantaged is equivalent to killing the state.

Actions:

for advocates for social justice,
Support and advocate for policies that benefit the lower socioeconomic classes (suggested)
Educate others about the importance of funding social safety nets and infrastructure projects (suggested)
</details>
<details>
<summary>
2019-12-24: Let's talk about how Barbie can change the world.... (<a href="https://youtube.com/watch?v=7KnV-0L6_Ew">watch</a> || <a href="/videos/2019/12/24/Lets_talk_about_how_Barbie_can_change_the_world">transcript &amp; editable summary</a>)

Beau explains how Barbie's evolution towards diversity can positively impact young minds and perceptions on race relations, showcasing the power of representation in shaping attitudes.

</summary>

"Barbie is woke now."
"We win."
"That is going to alter her mindset about race relations in general."
"Even if it is an act, if the results are real, does it matter that it was an act?"
"When brands do this, they are probably doing it for all the wrong reasons. But the results will end up being right."

### AI summary (High error rate! Edit errors on video page)

Explains how Barbie has evolved to represent diverse body types, skin tones, and career choices.
Recounts a heartwarming encounter of a little girl searching for a marine biologist Barbie for her sister.
Emphasizes the positive impact of diverse representation in dolls on shaping young minds.
Points out the significance of a white girl playing with a black Barbie in altering perceptions on race relations.
Expresses optimism that despite current bigotry, positive change will prevail through such representations.

Actions:

for parents, educators, activists,
Buy diverse dolls for children, exemplified
Encourage diverse career aspirations in children, exemplified
</details>
<details>
<summary>
2019-12-24: Let's talk about airplanes, helmets, studies, and statistics.... (<a href="https://youtube.com/watch?v=f615wgvz90M">watch</a> || <a href="/videos/2019/12/24/Lets_talk_about_airplanes_helmets_studies_and_statistics">transcript &amp; editable summary</a>)

Beau explains the flaws in studies and statistics using historical and modern examples, stressing the importance of a complete dataset for accurate interpretations.

</summary>

"When you're missing half of your data set, you can't extrapolate."
"Statistics and studies are useful. They can give you a clear picture of some things."
"If you only look at little bits and pieces or you choose to interpret it however you want, you don't really pay attention to the methodology used and how it's collected."

### AI summary (High error rate! Edit errors on video page)

Illustrates flaws in studies and statistics using historical and modern examples.
Navy commissioned study during World War II on aircraft damage.
Mathematician Wald recommended armor where there were no holes on planes.
Example of new helmet during World War I saving lives but doctors misinterpreted data.
Flaw: analyzing data assuming complete dataset when it's not.
Mention of the misconception about demographic X committing the most crime.
Statistics can be misinterpreted, like the Bureau of Justice Statistics example.
Emphasizes the importance of having a complete dataset for accurate interpretations.
Mentions climate models inaccuracies due to incomplete datasets.
Scientists' estimations improve as they gather more data.

Actions:

for data analysts, researchers, students.,
Verify data sources before making conclusions (implied).
Ensure data completeness for accurate analysis (implied).
</details>
<details>
<summary>
2019-12-23: Let's talk about why being "Anti-Trump" is wrong and solutions.... (<a href="https://youtube.com/watch?v=jwNHtA3hsUs">watch</a> || <a href="/videos/2019/12/23/Lets_talk_about_why_being_Anti-Trump_is_wrong_and_solutions">transcript &amp; editable summary</a>)

Beau urges for a shift from being against to being for, advocating for radical solutions addressing poverty, basic necessities, and education to combat polarization and stagnation.

</summary>

"Being against something doesn't create solutions, just creates resistance."
"We need to move forward with the more radical ideas, those that will create solutions."
"The best defense is a good offense."
"We need to be for ending poverty."
"Teaching people to question. Narratives."

### AI summary (High error rate! Edit errors on video page)

Cable news is opinionated, telling viewers what to be against instead of presenting just news.
Opposition should be based on moral obligation, not just rhetoric.
Being against something creates resistance, not solutions.
The nation is polarized because people focus on being against things rather than for them.
Root causes of many issues are a lack of access to basic necessities and education.
Lack of education leads to fear and opposition to things people don't understand.
Democrats positioning themselves as anti-Trump is not sufficient to bring about real change.
Solutions need to come from individuals, not just political leaders.
Being anti-something is defensive, but being for something is proactive and necessary for progress.
It's time to push for radical ideas that address poverty, basic necessities, and education to create real solutions and avoid repeating the same arguments in the future.

Actions:

for activists, community leaders,
Advocate for education reform to encourage critical thinking and questioning of narratives (implied).
Support initiatives that address poverty and provide basic necessities in communities (implied).
Shift focus from being against something to being for solutions in local activism and organizing (implied).
</details>
<details>
<summary>
2019-12-23: Let's talk about a threat to American democracy.... (<a href="https://youtube.com/watch?v=BBhBNMsOqkc">watch</a> || <a href="/videos/2019/12/23/Lets_talk_about_a_threat_to_American_democracy">transcript &amp; editable summary</a>)

The Trump campaign alleges political action committees are taking in money insincerely, raising concerns about the lack of accountability and potential impact on American democracy.

</summary>

"Trump supporters getting conned? No, no, no, no. Trump supporters are not known for being gullible."
"That's a massive demographic to be tricked that easily."
"There's probably not a meaningful difference, not to the person who made the contribution."

### AI summary (High error rate! Edit errors on video page)

Trump campaign alleging political action committees are taking in money insincerely.
Trump supporters not easily manipulated by silly slogans or memes.
Allegation of almost $50 million flowing to these groups, mostly from contributions less than $200.
Surprising that a massive demographic could be tricked easily.
Public surprise at the lack of outcry to reform campaign finance despite $50 million involved in politics.
$50 million enough to sway a candidate's vote or even buy one.
Lack of accountability in methods used to influence votes.
Concern that money given to Trump's campaign or political action committees may not serve contributors' best interests.
Administration not interested in making America great or representing the electorate.
No meaningful difference between giving money directly to Trump's campaign or political action committees.

Actions:

for american voters,
Verify the authenticity of donation requests before contributing (implied).
Advocate for campaign finance reform and increased transparency (implied).
</details>
<details>
<summary>
2019-12-20: Let's talk about a misunderstood Constitutional premise and a coin.... (<a href="https://youtube.com/watch?v=m3RGizuRbK8">watch</a> || <a href="/videos/2019/12/20/Lets_talk_about_a_misunderstood_Constitutional_premise_and_a_coin">transcript &amp; editable summary</a>)

Beau explains the concept of separation of church and state and how it applies to various contemporary issues, stressing the secular foundation of the United States.

</summary>

"The United States was designed to be a secular nation."
"It was supposed to be secular. Does that mean that our founders weren't Christians? No."
"When you are working for the government as an agent of government, you have no religious freedom."
"The wall is up. Separation of church and state."
"At the end of the day, I think it'll probably go back to what's in the Civil Rights Act."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of separation of church and state, not explicitly stated in the Constitution but implied.
Describes how the United States was designed to be a secular nation where no law favored or opposed any religion.
Mentions a treaty signed in 1797 with Tripoli stating that the U.S. government is not founded on the Christian religion.
Emphasizes that while some founders were Christian, atheist, or deist, the goal was to avoid a theocracy.
Addresses contemporary issues like prayer in schools, displaying religious monuments, and marriage laws in the context of separation of church and state.

Actions:

for americans, policymakers,
Understand and advocate for the separation of church and state (implied)
Ensure that public accommodations are available to everybody, regardless of religious beliefs (implied)
Educate others on the historical context and intent behind the secular foundation of the United States (implied)
</details>
<details>
<summary>
2019-12-19: Let's talk with Kathrin about the UK and women's issues.... (<a href="https://youtube.com/watch?v=-h3WjmwGVqM">watch</a> || <a href="/videos/2019/12/19/Lets_talk_with_Kathrin_about_the_UK_and_women_s_issues">transcript &amp; editable summary</a>)

Beau introduces Catherine, covers the UK election results, Brexit's impact on Ireland, body positivity's flaws, patriarchy's effects on men, and the ties between capitalism and patriarchy.

</summary>

"So many ways in which patriarchy harms men just as much well not just as much but definitely harms men as well."
"Capitalism is inherently based off of the oppression of women."
"I don't think that we can get rid of men dominating women without also getting rid of men, like humans dominating nature and dominating animals."

### AI summary (High error rate! Edit errors on video page)

Introduction of Catherine and her work as an intersectional anti-capitalist YouTuber covering social and political issues from an intersectional anti-capitalist perspective.
Overview of the UK election results where the Conservative Party secured a significant majority while Labour faced a major defeat, with reasons including media bias and their stance on Brexit.
Impact of the election results on Scotland and Northern Ireland, with the SNP gaining seats and potential calls for a second referendum for Scotland to leave England.
Speculation on the impact of Brexit on the border between North and South Ireland, considering varying statements made by Boris Johnson regarding a hard border.
Delving into Catherine's video on body positivity, critiquing how mainstream representations still reinforce normative beauty standards and capitalist consumption.
Explanation of patriarchy as a system where men hold power, discussing how masculinity and femininity traits are valued unequally in society.
Exploration of how patriarchy harms men, leading to issues like suppressed emotions, mental health problems, and harmful gender expectations.
Connection between capitalism and patriarchy, detailing how capitalism historically oppressed women and continues to subjugate them for profit.
Vision of a post-patriarchal society where all hierarchies are dismantled, allowing individuals to live without gendered norms and restrictions.

Actions:

for intersectional activists,
Advocate for gender equality and challenge societal norms through education and awareness (implied).
Support organizations promoting body positivity and representation for all body types (implied).
Engage in community dialogues about patriarchy and its impact on society (implied).
</details>
<details>
<summary>
2019-12-19: Let's talk about what to expect next in the impeachment.... (<a href="https://youtube.com/watch?v=HrNG9cSCsBU">watch</a> || <a href="/videos/2019/12/19/Lets_talk_about_what_to_expect_next_in_the_impeachment">transcript &amp; editable summary</a>)

Beau Gyan criticizes the theatrics of impeachment, questions understanding of due process, and draws parallels to history and current political waiting games.

</summary>

"It is utter nonsense."
"Today I watched as Republican representatives compared the impeachment to Pearl Harbor and the crucifixion of Christ."
"These are representatives on the House floor saying this, as if they don't actually understand how the process works."
"The moral of this story is that right now, both Democrats and Republicans, they're waiting for that morning."
"The Senate has to figure out what the American people will accept before they cast their vote."

### AI summary (High error rate! Edit errors on video page)

Criticizes the theatrics surrounding the impeachment process, labeling it as utter nonsense.
Expresses concern over Republican representatives comparing impeachment to Pearl Harbor and the crucifixion of Christ.
Points out the flawed argument of the lack of due process, especially when made by elected representatives.
Explains the impeachment process, clarifying that the trial takes place in the Senate, not the House.
Shares a historical anecdote about women gaining political power in Argonia in the late 1800s through a prank.
Draws parallels between the current political situation and the waiting game for new evidence to sway opinions on impeachment.
Emphasizes the importance of the Senate considering what the American people will accept before voting on impeachment.

Actions:

for politically engaged citizens,
Understand the impeachment process and how it differs between the House and the Senate (implied)
Stay informed about political proceedings and hold elected representatives accountable for their statements (implied)
Advocate for transparency and accountability in political processes (implied)
</details>
<details>
<summary>
2019-12-18: Let's talk about that TV show and independent journalism.... (<a href="https://youtube.com/watch?v=VsRR4Bwx1lE">watch</a> || <a href="/videos/2019/12/18/Lets_talk_about_that_TV_show_and_independent_journalism">transcript &amp; editable summary</a>)

Beau stresses the importance of independent journalists disrupting narratives and the need for projects like the reality TV show he was approached for, urging people to follow independent voices.

</summary>

"It's something this country desperately needs."
"I've seen that clip a hundred times and I laugh every time because that is independent journalism in a nutshell right there."

### AI summary (High error rate! Edit errors on video page)

Shared a post on social media and received many questions about it.
Approached by a production company in LA to create a reality TV show about independent journalists.
Flew to LA and covered events there and in Vegas with a diverse crew.
Mentioned crew members like Carrie Wedler, Ford, and Derek Brose.
Discussed the possibility of the reality TV show happening in the future.
Stressed the importance of independent journalists disrupting corporate news narratives.
Urged for the need of such projects in the current polarized environment.
Noted that many people do not follow independent journalists, leading to a lack of nuance in debates.
Shared details about his nickname, accent flexibility, and beard trimming for TV.
Encouraged learning different accents for better interaction with people from various regions.
Expressed comfort in being himself on his channel rather than using a non-regional accent.
Recommended watching a specific clip involving Derek getting shot for a glimpse into independent journalism.

Actions:

for journalism enthusiasts, media consumers,
Support independent journalism by following and sharing content (suggested)
Encourage nuance in debates by exposing oneself to various independent journalistic viewpoints (implied)
</details>
<details>
<summary>
2019-12-18: Let's talk about Trump's love letter.... (<a href="https://youtube.com/watch?v=aZl2UDMV-wM">watch</a> || <a href="/videos/2019/12/18/Lets_talk_about_Trump_s_love_letter">transcript &amp; editable summary</a>)

Beau Gyan dissects Trump's letter to Pelosi and speculates on its intended audience, warning Senate Republicans of potential fallout post-Trump.

</summary>

"It's not enough to make a Republican cast their vote for a Democrat, but they certainly might vote for somebody else in a primary who wants a Senator who was tricked."
"Neither one of those sound like very appealing legacies."
"And I think he may be a little worried about that."
"Doesn't take much to get through it."
"So he can remain unscathed while the republicans in the senate suffer the wrath after he's gone."

### AI summary (High error rate! Edit errors on video page)

Analyzes Donald Trump's letter to Nancy Pelosi, questioning the motive behind it.
Considers various possible reasons for Trump's actions, ruling out typical motivations like preserving his legacy or energizing his base.
Suggests that the letter may be intended to rally Senate Republicans, especially in light of public opinion showing a split on Trump's impeachment.
Points out that Senate Republicans may be staying supportive due to Trump's campaign efforts and warns of potential repercussions when more information about Trump's actions surfaces post-presidency.
Raises ethical concerns about Trump's spending on golf and personal profit from taxpayers.
Speculates about how Senate Republicans defending Trump till the end may impact their reputations in the future.
Notes that staying with Trump might affect the re-election prospects of Senate Republicans.
Surmises that there might be internal divisions within the Republican party, despite outward displays of unity.

Actions:

for political analysts, concerned citizens,
Reach out to Senate Republicans to express concerns about their continued support for Trump (implied)
Stay informed about political developments and hold elected officials accountable (implied)
</details>
<details>
<summary>
2019-12-17: Let's talk about the electoral college, power, and Jules Verne.... (<a href="https://youtube.com/watch?v=0Yqf-EHlTus">watch</a> || <a href="/videos/2019/12/17/Lets_talk_about_the_electoral_college_power_and_Jules_Verne">transcript &amp; editable summary</a>)

Beau delves into the Electoral College, advocating for higher standards and participation, suggesting a tougher process for presidential candidates.

</summary>

"It's not about education, it's about participation."
"You want to sit in the most powerful chair in the world? Prove yourself out on the campaign trail."
"We've had enough clowns stumble into that office and they've caused a lot of damage along the way."
"Let's set the standards higher not lower."
"I'm not certain of how to fix this problem but the one thing I am sure about is that the answer is not making it easier to sit in the most powerful office in the world."

### AI summary (High error rate! Edit errors on video page)

Talks about the Electoral College and voting, prompted by his son's question about it.
Shares a joke about John Quincy Adams approving an expedition to the center of the Earth.
Explains his son's concerns about how to prevent unsuitable candidates from reaching the Oval Office.
Mentions the issue of candidates campaigning only in major cities if the Electoral College is eliminated.
Expresses disappointment at his son's elitist view associating better education with people in major cities.
Comments on the importance of participation over geography in elections.
Acknowledges flaws in the current system where votes matter more in swing states.
Suggests that candidates currently win elections by dividing the country and playing one side against the other.
Proposes a harder process for candidates to prove themselves and unite people before becoming president.
Advocates for introducing ranked choice voting to address concerns about voting for third-party candidates.
Emphasizes the need to set higher standards for presidential candidates to make America better.

Actions:

for voters, political activists,
Advocate for ranked choice voting to provide more options for voters (suggested).
Participate in campaigns or initiatives that aim to reform the electoral process (implied).
</details>
<details>
<summary>
2019-12-17: Let's talk about homelessness in the wake of the Supreme Court decision.... (<a href="https://youtube.com/watch?v=Rb-MW0LmbEA">watch</a> || <a href="/videos/2019/12/17/Lets_talk_about_homelessness_in_the_wake_of_the_Supreme_Court_decision">transcript &amp; editable summary</a>)

Supreme Court non-ruling decriminalizes homelessness, forcing companies to become humanitarians and invest in shelters and affordable housing nationwide.

</summary>

"In essence, it decriminalized homelessness."
"They're going to have to chip into the communities they're profiting from."
"We have the technology to solve this problem, we just need the will."
"If you're an advocate for housing for all, if you are an advocate for people who are in poverty, if you're an advocate for the homeless, now is your time."
"You can't criminalize this without an alternative."

### AI summary (High error rate! Edit errors on video page)

Supreme Court non-ruling on Boise versus Martin decriminalized homelessness.
Companies pushing to criminalize homelessness driven by desire to keep homeless away from storefronts.
Companies now have to become humanitarians and contribute to communities they profit from.
Need for homeless shelters and affordable housing will increase.
Ruling currently applies on West Coast but expected to spread across the US.
Predicted consequences: pushing homeless into areas covered by ruling, blaming politicians for increase in homelessness.
Companies previously funding criminalization of homelessness may now support shelters and affordable housing.
Technology exists to solve homelessness issue, lack of will is the obstacle.
Advocates for housing, poverty, and homelessness urged to step up and take action.
Courts now require alternatives before criminalizing homelessness.

Actions:

for advocates, community members,
Advocate for housing for all, poverty, and homeless (exemplified)
Push companies to contribute to communities (implied)
Support organizations providing shelters and affordable housing (implied)
</details>
<details>
<summary>
2019-12-17: Let's talk about DOD's PR disaster.... (<a href="https://youtube.com/watch?v=_xzcgzFraF0">watch</a> || <a href="/videos/2019/12/17/Lets_talk_about_DOD_s_PR_disaster">transcript &amp; editable summary</a>)

Department of Defense posted a glowing biography of a war criminal, sparking confusion and concern, revealing a management blunder, not a moral lapse.

</summary>

"They didn't explain what was going on. It was shocking, to say the least."
"When we see social media blunders they may not always be what they appear..."
"The mistake was likely due to a management error, not a moral one."

### AI summary (High error rate! Edit errors on video page)

Department of Defense posted a glowing biography of a literal war criminal without explanation.
The post was shocking and lacked context, causing confusion and concern.
The mistake was likely due to a management error, not a moral one.
The narrative may have been intended to be posted in parts throughout a commemoration.
The post has been deleted, but there may have been plans for more posts.
The incident serves as a reminder that social media blunders may not always be what they seem.
The importance of proper context and clarity in social media communications.

Actions:

for social media managers,
Contact Department of Defense to express concerns about the post (suggested)
</details>
<details>
<summary>
2019-12-16: Let's talk about an email warning about me.... (<a href="https://youtube.com/watch?v=GWhQPNqqenM">watch</a> || <a href="/videos/2019/12/16/Lets_talk_about_an_email_warning_about_me">transcript &amp; editable summary</a>)

Beau warns about dangerous stereotypes surrounding domestic violence and the importance of not overlooking signs due to preconceived notions.

</summary>

"There is no uniform for a domestic abuser."
"Domestic violence is truly intersectional."
"We really need to remember there is no uniform for a domestic abuser."

### AI summary (High error rate! Edit errors on video page)

Received an email warning a shelter about his appearance before delivering charity supplies.
Stereotype: resembling the image of a person you wouldn't want to buzz in - big beard, tattoos.
Stereotypes can be dangerous as they may lead to missing signs of domestic violence.
Domestic violence affects everyone regardless of race, ethnicity, gender, or socioeconomic class.
Dangerous to perceive domestic violence as only happening in certain areas, leading to overlooking signs.
Shelter previously printed shirts to dispel the stereotype associated with Beau's appearance.
Domestic abusers don't have a specific uniform.
Urges to be mindful of stereotypes and not overlook signs of domestic violence.
Domestic violence is intersectional and can happen anywhere.
Importance of dispelling harmful stereotypes surrounding domestic violence.

Actions:

for community members, advocates,
Support and volunteer at domestic violence shelters (implied)
Educate others on the intersectionality of domestic violence (implied)
Challenge and dispel harmful stereotypes about domestic violence (implied)
</details>
<details>
<summary>
2019-12-16: Let's talk about Virginia, principles, logic, and consistency.... (<a href="https://youtube.com/watch?v=5CH4RbTbLdw">watch</a> || <a href="/videos/2019/12/16/Lets_talk_about_Virginia_principles_logic_and_consistency">transcript &amp; editable summary</a>)

Beau in Virginia challenges the lack of principles in political thought, supporting Second Amendment sanctuaries and urging consistency in beliefs.

</summary>

"If there's not a victim, there's not a crime."
"The mere possession of something doesn't necessarily create a victim."
"Either what you believe to be right and true is right and true or you don't have any principles."
"You guys want to take this stand, more power to you, seriously."
"But I would ask if they're your principles, or you're just being told what to do."

### AI summary (High error rate! Edit errors on video page)

American political thought lacks principles, logic, and consistency.
Virginia is proposing sweeping legislation, but local jurisdictions are resisting by creating Second Amendment sanctuaries.
Beau's principles dictate that for something to be a crime, there must be a victim.
He supports the local jurisdictions' decision to not comply with the proposed laws.
Beau warns against deploying the National Guard to force compliance, as it could escalate and result in real victims.
He mentions the importance of supporting other forms of sanctuary jurisdictions based on consistent principles.
Beau argues that the Constitution reserves certain powers to the states and people, not the federal government.
He stresses the need for logical consistency in principles, rather than being swayed by political propaganda.
Beau encourages individuals to take a stand based on their true principles, even if it may lead to conflict.
He questions whether individuals are truly acting on their principles or simply following directives.

Actions:

for virginia residents, activists,
Support and join Second Amendment sanctuaries (exemplified)
Advocate for consistent principles in political decision-making (exemplified)
Engage in local governance to uphold community values (exemplified)
</details>
<details>
<summary>
2019-12-15: Let's talk about two questions and civil rights.... (<a href="https://youtube.com/watch?v=28caQlP6WQI">watch</a> || <a href="/videos/2019/12/15/Lets_talk_about_two_questions_and_civil_rights">transcript &amp; editable summary</a>)

Exploring why white people who care often have Irish ancestry, examining Civil Rights Movement parallels, and stressing the need to end systemic issues before focusing on reparations.

</summary>

"The problem is in the US, it's not over."
"We can't say, 'Pull yourself up by your bootstraps' until those institutional issues are gone."
"The primary thing is we've got to make it stop first."

### AI summary (High error rate! Edit errors on video page)

Exploring the question of why white people who seem to care or understand are often Irish or of Irish ancestry.
Providing a timeline of the Civil Rights Movement in the 1960s, detailing protests, police response, bombings, and government infiltration.
Drawing parallels between the Black Panthers in the U.S. and the Irish Republican Army in Europe during the same timeline.
Noting the camaraderie between Irish and black communities in the American South due to shared experiences of oppression and intermarriage.
Mentioning that Irish-Americans in the South tend to have strong family bonds and pride in their heritage.
Questioning when personal responsibility should kick in, considering ongoing systemic issues like disproportionate sentencing and redlining.
Stating that until institutional and systemic issues are eradicated, expecting individuals to solely overcome challenges is unreasonable.
Emphasizing the need to first stop systemic issues before taking steps like reparations to make amends.
Criticizing proposed reparations figures as initially insulting but acknowledging recent talks of higher figures being more appropriate.
Stressing the importance of awareness that the fight against systemic issues is ongoing and subtle, requiring continued efforts to overcome.

Actions:

for activists, community members,
Advocate for ending systemic issues (implied)
Support reparations initiatives (implied)
Stay informed and engaged in ongoing fights against systemic racism (implied)
</details>
<details>
<summary>
2019-12-15: Let's talk about solving the problem of homeless vets.... (<a href="https://youtube.com/watch?v=OiJaAbW_ZQk">watch</a> || <a href="/videos/2019/12/15/Lets_talk_about_solving_the_problem_of_homeless_vets">transcript &amp; editable summary</a>)

Beau addresses homelessness among veterans, proposing a solution that involves utilizing existing facilities, honoring treaty obligations, and prioritizing genuine care over political biases.

</summary>

"All it takes is for those people who say they care about homeless vets to love homeless vets more than they hate brown people."
"The solution is there."
"The facilities are already there."
"What are we waiting for?"
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Addresses the issue of homeless veterans and the common response of prioritizing their needs over other social safety net concerns.
Mentions the need for facilities across states to house homeless vets, providing them with necessities like hygiene, food, and shelter.
Points out that there are already facilities capable of housing homeless vets, with room for more people.
Suggests honoring treaty obligations to free up space in facilities currently holding asylum seekers and migrants.
Proposes putting individuals with criminal records or deemed dangerous in county jails, while utilizing current incarcerated individuals for assistance.
Challenges viewers to take action by contacting their senators instead of just discussing the issue online.
Emphasizes the importance of prioritizing the well-being of homeless vets over political agendas.
Advocates for a comprehensive solution to homelessness rather than temporary fixes.
Urges for support in reintegrating homeless individuals back into society through assistance with housing costs.
Calls for a shift in focus towards loving homeless vets more than harboring negative sentiments towards certain groups.

Actions:

for advocates, activists, supporters,
Call your senator to advocate for better facilities to support homeless veterans (suggested)
Support efforts to reintegrate homeless individuals back into society by assisting with housing costs (implied)
</details>
<details>
<summary>
2019-12-14: Let's talk with Greta Thunberg.... (<a href="https://youtube.com/watch?v=-NBbsDmxNjQ">watch</a> || <a href="/videos/2019/12/14/Lets_talk_with_Greta_Thunberg">transcript &amp; editable summary</a>)

Beau confronts Greta Thunberg on her advocacy, mocking her tech skills, but ends up praising her resilience and urging continued activism and learning.

</summary>

"I could have swore they said she's come by."
"You're in this problem right now because a lot of us, that's what we did. We didn't do anything."
"You keep watching documentaries. You be better than us."

### AI summary (High error rate! Edit errors on video page)

Welcomes Greta Thunberg as a special guest, prepared for a confrontational interview due to her controversial figure.
Questions Greta's lack of life experience to advocate for climate change and mocks her inability to work basic technology.
Mistakes a Halloween decoration for Greta and continues the playful banter about generational differences in technology use.
Applauds Greta for facing criticism from powerful figures like Putin and Trump, encouraging her not to chill out but to keep advocating and learning.
Urges Greta to watch documentaries and be better than older generations who didn't take action on climate change.
Responds with silence to Beau's initial mocking about technology, prompting him to realize his mistake.
Listens as Beau acknowledges the unjust bullying she faces online and praises her for standing strong against powerful critics.

Actions:

for climate activists, youth advocates.,
Watch documentaries on climate change and take action to be better advocates (implied).
</details>
<details>
<summary>
2019-12-14: Let's talk about Biden's qualifications.... (<a href="https://youtube.com/watch?v=iFNr_LR164k">watch</a> || <a href="/videos/2019/12/14/Lets_talk_about_Biden_s_qualifications">transcript &amp; editable summary</a>)

Addressing the false claims around Hunter Biden's qualifications, Beau exposes the double standards and political narratives at play.

</summary>

"He has the qualifications. They're right there."
"At the end of the day, Trump in some ways has been a benefit because he's really laid bare the out-and-out lies and corruption in our government."
"Maybe this type of corruption is just accepted here and the idea is that yeah, George Bush was trying to curry favor with Biden and you know, here it's okay, but over there, well, we have something different."

### AI summary (High error rate! Edit errors on video page)

Addressing Hunter Biden's qualifications and the repeated claims of his lack of qualifications for board positions.
Mentioning George Bush's appointment of Hunter Biden to a board of directors and confirmation by the Senate.
Noting Hunter Biden's past role on the Amtrak board of directors and his qualifications, including his education and work experience.
Pointing out the false narrative perpetuated by some Republicans regarding Hunter Biden's qualifications.
Exploring the idea of different standards being applied to Ukraine compared to the US government.
Suggesting that the repeated falsehoods about Hunter Biden are believed by a base that does not fact-check.
Contemplating whether there is a double standard in scrutinizing appointments based on political affiliations.
Acknowledging the potential benefit of President Trump's administration in exposing lies and corruption in government.

Actions:

for fact-checkers, political observers,
Fact-check repeated claims and correct misinformation (implied)
</details>
<details>
<summary>
2019-12-11: Let's talk with Emerican Johnson of Non-Compete.... (<a href="https://youtube.com/watch?v=FObbjvpVVKA">watch</a> || <a href="/videos/2019/12/11/Lets_talk_with_Emerican_Johnson_of_Non-Compete">transcript &amp; editable summary</a>)



</summary>

"The real solution to all of our problems is organizing together and building solidarity with the working class."
"Unions have become a shadow of what they used to be. I think they're starting to come back, fortunately."
"Let's use them, let's fight together, and let's change the world."

### AI summary (High error rate! Edit errors on video page)

American Johnson introduces himself as an anarcho-communist running the YouTube channel Noncompete, engaging with various leftist ideologies critical of capitalism.
The channel, despite discussing complex topics like means of production, uses puppets and Legos to attract a younger audience, prompting concerns regarding COPPA guidelines.
COPPA, aimed at protecting children's privacy online, fined YouTube $127 million for data harvesting from children, threatening content creators with penalties for not collecting data.
American Johnson criticizes the FTC's COPPA enforcement, arguing that it contradicts the First Amendment by restricting free speech based on potential appeal to children.
Concerns arise about potential FTC lawsuits affecting creators globally, like American Johnson's partner Luna, a Vietnamese citizen creating adult-focused content.
American Johnson advocates for alternative social media platforms like Mastodon and PeerTube, promoting decentralized, community-owned networks as replacements for capitalist platforms.
Encouraging viewers to join and create channels on these alternative platforms, American Johnson stresses the importance of supporting and building solidarity within the working class.
In Vietnam, a culture of skepticism towards hierarchy and authority fosters local problem-solving and community-focused governance.
American Johnson's call to action revolves around organizing, building solidarity, and fostering class consciousness as key solutions to societal challenges.
He invites viewers to subscribe to his channel for updates on transitioning to PeerTube and participating in the YouTube walkout on the 10th, advocating for collective action and utilizing available tools to enact change.

Actions:

for creators and activists,
Join Mastodon or PeerTube to support decentralized platforms (suggested)
Start a channel on existing instances like PeerTube (suggested)
Participate in the YouTube walkout on the 10th (implied)
</details>
<details>
<summary>
2019-12-11: Let's talk about shelters, sales, smells, and internet people.... (<a href="https://youtube.com/watch?v=elHndoJW2aI">watch</a> || <a href="/videos/2019/12/11/Lets_talk_about_shelters_sales_smells_and_internet_people">transcript &amp; editable summary</a>)

Beau Gadd organized a livestream to provide Christmas presents for teens in a shelter, showcasing the power of collective action and urging companies to support domestic violence shelters.

</summary>

"Y'all did this in about an hour."
"Seems like a win-win-win-win."
"Shows the value of individual action in pursuit of a collective goal."

### AI summary (High error rate! Edit errors on video page)

Beau organized a livestream on YouTube where donations were earmarked for Christmas presents for teens in a shelter.
Five bags were prepared, each containing a tablet, a Netflix card, a tablet case, earbuds, and a board game.
The tablets and Netflix cards were included to provide the teens with their own space, especially if they have younger siblings monopolizing the TV.
The goal was quickly accomplished through collective effort; people donated money and time to make it happen.
Additional items like cleaning supplies were also provided to the shelter, which are often overlooked but necessary for PTSD sufferers.
Beau questioned why companies don't donate products like diapers and soap to domestic violence shelters to avoid triggering bad memories.
He suggested that companies could benefit from such donations through tax deductions, advertising, and building brand loyalty.
Beau emphasized the importance of individual actions contributing to collective goals, showcasing the power of community effort.
He expressed gratitude towards the internet community for their swift action in addressing the initial problem and going beyond it.
Beau reflected on the world we live in, where companies could do more to support shelters, but also praised the internet community for their quick and effective response.

Actions:

for community members, donors.,
Donate products like diapers and soap to domestic violence shelters for survivors (implied).
</details>
<details>
<summary>
2019-12-10: Let's talk about reports, reactions, and responses.... (<a href="https://youtube.com/watch?v=c1ZFPh4LRyc">watch</a> || <a href="/videos/2019/12/10/Lets_talk_about_reports_reactions_and_responses">transcript &amp; editable summary</a>)

People are rightfully angry after being lied to, but learning from past mistakes and supporting the truth can prevent unnecessary military actions and protect American soldiers.

</summary>

"Be mad, be angry, that's fine, but learn from it."
"Supporting the truth will protect American soldiers more than any politician in DC ever could."
"Iran needs a political response, not a military one."
"This is a moment where the American people can learn from the graves of thousands of American troops."
"The U.S. runs the greatest war machine the world has ever known."

### AI summary (High error rate! Edit errors on video page)

People are rightfully angry after being lied to for 18 years by multiple administrations, leading to immeasurable costs in money and lives.
Rather than formulating an educated response, the U.S. reacted emotionally to the events in the Middle East in 2001, resulting in unnecessary invasions.
The U.S. government is currently pushing the idea that the Taliban wants to make a deal, but experts suggest otherwise.
The Taliban are strategic and waiting out the U.S., with any deal likely not being honored, leading to unnecessary deaths.
Beau points out that learning from past mistakes is critical, such as abandoning the Kurds against expert advice.
There are concerns about a potential march to war with Iran, despite experts warning against it.
Supporting the truth is more beneficial to American soldiers than blind patriotism, and curbing unnecessary military actions is vital.
Beau stresses the need for American citizens to hold their government accountable and make informed decisions about military involvement.

Actions:

for american citizens,
Hold the government accountable by demanding transparency and truthfulness (exemplified)
Educate yourself and others on foreign policy decisions and their consequences (suggested)
Advocate for political responses over military actions in international conflicts (exemplified)
</details>
<details>
<summary>
2019-12-09: Let's talk about Miss Universe, waste, and wins.... (<a href="https://youtube.com/watch?v=KJpwfxdXUq4">watch</a> || <a href="/videos/2019/12/09/Lets_talk_about_Miss_Universe_waste_and_wins">transcript &amp; editable summary</a>)

Beau describes a surprising and uplifting encounter at a remote gas station, showcasing progressive views on acceptance and equality, marking it as a win towards a better world.

</summary>

"I can't believe in this day and age, there are still places where people just can't be themselves."
"I'm assuming you're not picturing us. If you're not picturing us and you are picturing them, you might want What?"
"And sometimes we just got to take the wins where we can get them."

### AI summary (High error rate! Edit errors on video page)

Describes his visit to a remote gas station in the country.
Talks about the unique setup of the gas station with a barbecue stand, picnic tables, and a little diner.
Advises on how to navigate interactions at such busy places, comparing it to being a new inmate in prison.
Shares an unexpected and positive interaction he witnessed between three guys at the gas station.
Recounts their open-mindedness towards discussing LGBTQ+ issues, particularly around Miss Universe competition.
Expresses surprise and admiration for the guys' progressive views on acceptance and equality.
Questions society's ability to let people be themselves in this day and age.
Appreciates the guys' inclusive commentary as a win and a step in the right direction for the world.
Acknowledges the importance of recognizing and celebrating small victories for progress.
Concludes with a reflective message on valuing individuals beyond surface judgments and stereotypes.

Actions:

for all individuals,
Celebrate small victories for progress (implied)
Engage in open-minded and inclusive conversations (implied)
</details>
<details>
<summary>
2019-12-08: Let's talk about the smartest Republican Senators.... (<a href="https://youtube.com/watch?v=9aFvSldIoRk">watch</a> || <a href="/videos/2019/12/08/Lets_talk_about_the_smartest_Republican_Senators">transcript &amp; editable summary</a>)

Exploring the political implications of Republican senators potentially voting to impeach Trump, with a focus on voter turnout, party loyalty, and the long-term consequences for the GOP.

</summary>

"I think that they understand what happened in 2016."
"If you want to save the party, you've got to get rid of Trump."
"Removing Trump is actually the safest play for Republicans."
"It's gonna be easy to defeat somebody in a primary whose whole purpose in running is that you honored your oath."
"Long-term, the worst possible thing in the world for the Republican party is a Trump second term."

### AI summary (High error rate! Edit errors on video page)

Exploring the political implications of Republican senators considering voting to impeach and convict Trump.
Speculating on the potential consequences for these senators in terms of primary challenges and general elections.
Pointing out the impact of a single-issue voter base whose goal is to remove Trump from office.
Drawing parallels to the 2016 election and the potential effects on voter turnout based on the impeachment vote.
Arguing that removing Trump from the equation may actually benefit Republicans in the long run.
Warning about the dangers of a second term for Trump and the possible repercussions for the Republican Party.
Advocating for putting aside loyalty to the party in order to save it from potential long-term damage.

Actions:

for politically engaged citizens,
Mobilize voters to understand the long-term implications of supporting candidates who prioritize country over party (implied).
Engage in political discourse within your community to raise awareness about the potential impact of removing Trump from office (implied).
</details>
<details>
<summary>
2019-12-08: Let's talk about Rudy Giuliani's report (<a href="https://youtube.com/watch?v=Jz-IYX0GHWU">watch</a> || <a href="/videos/2019/12/08/Lets_talk_about_Rudy_Giuliani_s_report">transcript &amp; editable summary</a>)

Beau delves into Giuliani's report, advocates for accountability across political lines, and questions the timing of evidence presentation during trials.

</summary>

"But it does not absolve the administration of any of their alleged crimes, even if they were right. It's not how this works."
"Either what we believe is good and true is good and true, or we don't have any order. We don't have any laws."
"If Biden abused his office for personal gain, he should go down. And if you believe that, you have to believe the same is true for Trump."

### AI summary (High error rate! Edit errors on video page)

Beau sets the stage to talk about Giuliani's report and shares a hypothetical story about a cop convinced of someone's wrongdoing.
The cop, obsessively watching the person's house, discovers stolen property inside.
Both the cop and the person with stolen property get arrested after separate trials, illustrating accountability.
Beau expresses curiosity about Giuliani's report, wanting to see the evidence against the Bidens.
He mentions that if Biden abused his office, he should face consequences, but the same accountability should apply to Trump.
Beau stresses the importance of holding leaders accountable for any abuse of power, regardless of political affiliation.
He questions the timing of bringing up evidence during Biden's trial instead of the President's impeachment.
Beau argues that evidence should not be used to muddy the waters but to ensure accountability.
He concludes by urging for accountability for both Biden and Trump if wrongdoing is proven.

Actions:

for citizens, voters, activists,
Hold leaders accountable for abuses of power (implied)
Advocate for equal accountability across political affiliations (implied)
</details>
<details>
<summary>
2019-12-07: Let's talk with Howie Hawkins about the Green Party.... (<a href="https://youtube.com/watch?v=CxlGzHwE1dU">watch</a> || <a href="/videos/2019/12/07/Lets_talk_with_Howie_Hawkins_about_the_Green_Party">transcript &amp; editable summary</a>)

Howie Hawkins advocates for eco-socialist solutions, a Green New Deal by 2030, and grassroots organizing to build a major Green Party base, challenging the Democratic establishment and calling for electoral reform.

</summary>

"If we're gonna have a left and alternative in this country, we gotta have our own independent voice and our own independent power."
"There's no such thing as an unorganized socialist. You gotta get in an organization."
"Strength in numbers. When we're working together, we get our ideas tested."

### AI summary (High error rate! Edit errors on video page)

Introduction of Howie Hawkins, a Green Party candidate with a background in social movements and environmental activism.
Howie Hawkins shares his history of being a teamster, his involvement in forming the Green Party, and his previous statewide campaigns.
Emphasis on building the Green Party as a major party and advocating for eco-socialist solutions to pressing issues like climate change, inequality, and nuclear disarmament.
Howie Hawkins details his Green New Deal proposal, contrasting it with the Democrats' version and stressing the urgency to address climate change by 2030.
Importance of third-party representation in elections to push progressive agendas and hold major parties accountable.
Howie Hawkins explains the dangers of the current nuclear arms race, advocating for disarmament and referencing past treaties.
Addressing job concerns in the transition to a green economy, ensuring a just transition and providing millions of jobs.
Expansion on the Economic Bill of Rights proposed by the Green Party, including job guarantees, income above poverty, affordable housing, universal rent control, and healthcare for all.
Advocacy for open borders and ending mass surveillance, supporting whistleblowers like Edward Snowden.
The need for grassroots organizing to bridge divides among working-class individuals and build a mass party for working people.
Focus on electoral reform, advocating for a national popular vote with ranked-choice voting to eliminate the Electoral College.
Challenges faced by progressive Democrats and the potential for a Green Party to attract disenchanted voters, particularly non-voters.

Actions:

for progressive activists, third-party supporters,
Contact the Howie Hawkins campaign via howiehawkins.us to volunteer, donate, and help with ballot access efforts (suggested).
Host house parties for grassroots fundraising and awareness about Howie Hawkins' campaign (suggested).
Engage in electioneering canvassing activities to reach out to potential supporters and collect information for future engagement (suggested).
</details>
<details>
<summary>
2019-12-06: Let's talk about white vans and legends.... (<a href="https://youtube.com/watch?v=Ck2iZM3stFg">watch</a> || <a href="/videos/2019/12/06/Lets_talk_about_white_vans_and_legends">transcript &amp; editable summary</a>)

Be more aware of the lessons behind viral stories, as even if the story is fake, the lesson is real and valuable.

</summary>

"Most of these stories, yeah they're not true, but the lesson is, the lesson's real and most of them are a positive benefit to society."
"Somewhere not just did we lose the ability to tell facts from fiction, we lost the ability to learn from fiction, and that's sad."
"So when you see one of these stories, one of these viral stories on Facebook or whatever, don't really think about the story. Think about the lesson."

### AI summary (High error rate! Edit errors on video page)

Addresses the urban legend about white vans in the United States where people with white vans abduct others for organs.
Mentions that the story originated in Baltimore and caused panic, prompting the Baltimore PD to issue a statement debunking it.
Emphasizes that the core of such stories is often about increasing situational awareness and being cautious.
Points out the importance of being aware of your surroundings, especially when vulnerable like when putting a child in a car seat.
Gives an example of a firefighter spreading a false meme about smoke detectors to encourage people to replace batteries for safety.
Expresses that while many of these viral stories are untrue, they often carry real lessons that can benefit society.
Criticizes the loss of the ability to distinguish facts from fiction and the missed opportunities to learn from fictional stories.
Encourages looking beyond the story itself to find the lesson embedded within most viral tales.
Concludes by urging viewers to focus on the lessons rather than the sensationalism of viral stories on social media.

Actions:

for internet users,
Verify information before spreading it on social media (implied)
Be cautious and aware of your surroundings, especially in vulnerable situations like parking lots (implied)
</details>
<details>
<summary>
2019-12-06: Let's talk about UPS trucks.... (<a href="https://youtube.com/watch?v=o-DYbOGMVBI">watch</a> || <a href="/videos/2019/12/06/Lets_talk_about_UPS_trucks">transcript &amp; editable summary</a>)

Miami UPS truck pursuit raises questions about law enforcement tactics and the importance of preserving human life over property.

</summary>

"Speed, surprise, and violence of action."
"The primary objective is the preservation of human life."
"Planning, logistics, and training failed here massively."

### AI summary (High error rate! Edit errors on video page)

UPS truck incident prompts Beau to address unusual events in Miami involving law enforcement.
Miami cops have a reputation, but not for tactical incompetence.
Pursuit of UPS truck ends tragically with suspects, driver, and bystander killed.
UPS trucks are equipped with sensors for tracking and monitoring.
Tactical approach to the situation raises questions about law enforcement actions.
Primary objective in such situations should be preserving human lives.
Law enforcement's use of bystander vehicles as cover raises ethical concerns.
Ineffective and inaccurate fire directed at the vehicle during the incident.
Critique on the failure of planning, logistics, and training in the UPS truck pursuit.
Beau concludes with reflections on the need for better preparedness and training in law enforcement.

Actions:

for law enforcement officials,
Reassess training and tactics in law enforcement (implied)
</details>
<details>
<summary>
2019-12-05: Let's talk about truth and consequences, Trump edition.... (<a href="https://youtube.com/watch?v=ob5Hhbn4oVs">watch</a> || <a href="/videos/2019/12/05/Lets_talk_about_truth_and_consequences_Trump_edition">transcript &amp; editable summary</a>)

President Trump considers sending 14,000 troops to the Middle East, prioritizing election strategy over the well-being of troops and allies.

</summary>

"He cares about power. He doesn't care about the troops."
"The cost of believing that is going to be paid by them."
"When you don't see them at the gas station for a while, you're going to know why."

### AI summary (High error rate! Edit errors on video page)

President Trump is considering sending an additional 14,000 troops to the Middle East to prepare for conflict with Iran.
The decision stems from the need to energize his supporters for the election season.
The consequences of discounting foreign policy experts and abandoning allies, like the Kurds, are coming to light.
Iran, similar to other countries in the region, has a Kurdish population historically friendly to the U.S.
The president's focus appears to be on power and re-election rather than the well-being of troops.
The move towards conflict seems driven by political motives rather than genuine concern.
Trusting a businessman over generals may have severe consequences for American troops.
The cost of prioritizing political gain over alliances and military expertise may result in significant losses.

Actions:

for voters, activists, military families,
Reach out to elected officials to voice opposition to escalating conflicts (implied)
</details>
<details>
<summary>
2019-12-05: Let's talk about tomorrow, December 6th.... (<a href="https://youtube.com/watch?v=c9XbsR7x7CY">watch</a> || <a href="/videos/2019/12/05/Lets_talk_about_tomorrow_December_6th">transcript &amp; editable summary</a>)

December 6th should be a national holiday, marking the end of a dark American institution, but its significance is not appropriately commemorated.

</summary>

"December 6th should be a national holiday, marking the end of a dark American institution."
"The Emancipation Proclamation did not end the institution; it only freed slaves in conflict zones."
"The day marks the closing of a dark chapter in American history, yet it is not appropriately commemorated."
"People who defended slavery until the end are remembered for their stance."
"Current events may lead to individuals being similarly remembered for supporting indefensible causes."

### AI summary (High error rate! Edit errors on video page)

December 6th should be a national holiday, marking the end of a dark American institution.
The Emancipation Proclamation did not end the institution; it only freed slaves in conflict zones.
Lincoln's strategic decision as commander in chief aimed to destabilize the South.
The 13th Amendment was necessary to prevent the resurgence of the institution after the war.
Lincoln pushed for the 13th Amendment, which was ratified on December 6th, 1865.
Some states were slow to ratify the amendment, with Kentucky and Mississippi waiting until 1976 and 1995, respectively.
The day marks the closing of a dark chapter in American history, yet it is not appropriately commemorated.
It took another hundred years for further steps towards equality after the 13th Amendment.
People who defended slavery until the end are remembered for their stance.
Current events may lead to individuals being similarly remembered for supporting indefensible causes.

Actions:

for history enthusiasts, activists,
Advocate for December 6th to be recognized as a national holiday (suggested)
Educate others about the importance of the 13th Amendment and its significance in American history (suggested)
</details>
<details>
<summary>
2019-12-04: Let's talk about Fred Hampton's legacy after 50 years.... (<a href="https://youtube.com/watch?v=3aK7hY_67Ok">watch</a> || <a href="/videos/2019/12/04/Lets_talk_about_Fred_Hampton_s_legacy_after_50_years">transcript &amp; editable summary</a>)

50 years ago, an incident in Chicago altered history, revealing systemic resistance to change but also the unstoppable power of ideas.

</summary>

"Nobody can tell you this is what happened that day."
"When an idea's time has come, nothing will stop it."

### AI summary (High error rate! Edit errors on video page)

Recalls an incident from 50 years ago at 2337 West Monroe Street in Chicago that altered American history.
Mentions an up-and-coming activist named Fred Hampton who could have created significant social change.
Expresses a belief that the incident was a deliberate effort to destabilize and remove black leadership in the United States.
Encourages people to look into the event themselves and analyze all the evidence.
Suggests two possible conclusions: isolated bad actions or a targeted effort against black leadership.
Believes the system lashed out against those seeking change, showing systemic resistance to change.
Despite the tragic event, notes that it led to a massive outcry and political changes.
Points out the election impact following the incident, leading to Chicago's first black mayor.
Draws a connection from the past events to the election of President Obama.
Emphasizes that when the time is right, no system can stop an idea from manifesting.

Actions:

for activists, historians, social change advocates,
Research and learn about historical events like the one mentioned (suggested)
Get involved in local politics and elections to create change (implied)
</details>
<details>
<summary>
2019-12-03: Let's talk about logical fallacies.... (<a href="https://youtube.com/watch?v=M2WKnNkrCdI">watch</a> || <a href="/videos/2019/12/03/Lets_talk_about_logical_fallacies">transcript &amp; editable summary</a>)

Beau gives a crash course on logical fallacies through real-life stories, urging for better aircraft maintenance and critical thinking in debates.

</summary>

"We really need to pay our aircraft maintenance crews more."
"Just because one of these fallacies was used does not mean the argument is wrong."
"The middle ground is not always where you want to end up."
"The world is a bad place sometimes."
"It's just a thought y'all have a good Good night."

### AI summary (High error rate! Edit errors on video page)

Tells a true story about a trip where several planes had issues which leads to the point of needing to pay aircraft maintenance crews more.
Mentions misleading vividness, where a detailed story doesn't actually support the claim being made.
Explains logical fallacies including appealed hypocrisy, straw man, genetic fallacy, appeal to belief, appeal to tradition, appeal to authority, ad hominem, argument from ignorance, cherry-picking data, slippery slope, Nirvana Fallacy, moving the goalpost, false dilemma, fallacy of composition, divine fallacy, argument to moderation, and argument from fallacy.
Provides examples for each logical fallacy and explains how they can lead to flawed arguments.
Emphasizes the importance of understanding logical fallacies especially in the context of debates and arguments.
Ends by suggesting that the information on logical fallacies will be valuable in the coming year.

Actions:

for debaters and critical thinkers.,
Learn about logical fallacies and how to identify them in arguments (suggested).
Educate others on common logical fallacies and how they can weaken arguments (suggested).
</details>
<details>
<summary>
2019-12-02: Let's talk about interviewing Presidential candidates.... (<a href="https://youtube.com/watch?v=bQqZBzBksVA">watch</a> || <a href="/videos/2019/12/02/Lets_talk_about_interviewing_Presidential_candidates">transcript &amp; editable summary</a>)

Beau plans to interview presidential candidates to understand their support, preferring a conversational approach to reveal candidates' true selves, ultimately questioning the concentration of power in political office.

</summary>

"You should not be trusted with that much power."
"It's almost as if nobody should be trusted with that much power."
"No big mystery there."
"The office has too much."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Planning to interview presidential candidates to understand their base of support.
Interested in learning who the candidates are and why they have support.
Prefers a conversational approach over hardball questions to get to the truth.
Believes that keeping candidates talking will eventually reveal their true selves.
Values offhand comments over prepared sound bites for genuine insights.
Acknowledges valid reasons to disqualify every candidate from holding office.
Questions the concentration of power in the hands of political officeholders.
Leaves viewers with a thought-provoking reflection on trust and power.

Actions:

for viewers,
Reach out to neighbors to understand their support for political candidates (implied)
Engage in meaningful, conversational dialogues to uncover truths (implied)
Question the concentration of power in political office and its implications (implied)
</details>
<details>
<summary>
2019-12-02: Let's talk about genius, robots, futures, and impeachment.... (<a href="https://youtube.com/watch?v=Eb2krF7Ydho">watch</a> || <a href="/videos/2019/12/02/Lets_talk_about_genius_robots_futures_and_impeachment">transcript &amp; editable summary</a>)

Beau talks about Isaac Asimov's genius, his accurate predictions, the importance of holding leaders accountable, and makes a chilling prediction for the future.

</summary>

"He saw all this coming."
"If we don't start holding those in the White House accountable 50 years from now, we won't be talking about impeachment because we won't have a presidency."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic of discussing a genius.
Talks about Isaac Asimov, a genius known for science fiction.
Mentions that Asimov made predictions in 1964 about life in 2014, some of which have come true.
Talks about Asimov's interest in history and politics, particularly Watergate.
Criticizes the idea of excusing criminal actions based on historical precedents.
Calls for holding leaders accountable.
Mentions World AIDS Day and Asimov's death due to AIDS on April 6, 1992.
Makes a prediction about the future if leaders are not held accountable.

Actions:

for citizens, voters, activists.,
Hold leaders accountable (implied).
Advocate for transparency and accountability in government (suggested).
</details>
<details>
<summary>
2019-12-01: Let's talk with Marianne Williamson.... (<a href="https://youtube.com/watch?v=OXlXGl7_BmQ">watch</a> || <a href="/videos/2019/12/01/Lets_talk_with_Marianne_Williamson">transcript &amp; editable summary</a>)

Marianne Williamson advocates for reparations, peace-building efforts, and a nonviolent political revolution to address historical wrongs and corporate influence on government policies.

</summary>

"Purify your own heart."
"What reparations do is they carry an inherent mea culpa, an inherent acknowledgement of the historical wrong that was done."
"We need a nonviolent political revolution in this country."

### AI summary (High error rate! Edit errors on video page)

Introduces Marianne Williamson, known for being a self-help guru and running for president.
Marianne talks about her background, career, and involvement during the AIDS epidemic.
Describes the despair and love experienced during the AIDS crisis.
Marianne founded organizations to provide support services to people living with HIV.
Shares the story of Project Angel Food, delivering meals to homebound people with AIDS.
Connects her work during the AIDS crisis to her current political campaign.
Advocates for harnessing goodness and decency for political purposes.
Talks about the need for a Department of Peace to focus on peace-building efforts.
Proposes a Department of Children and Youth to address vulnerabilities and challenges faced by American children.
Advocates for reparations as a way to address historical wrongs and economic disparities.
Calls for purifying hearts and promoting forgiveness and respect for differing opinions.
Addresses the influence of corporate interests on government policies and the need for a nonviolent political revolution.

Actions:

for activists, advocates, voters,
Support organizations providing non-medical services to those living with HIV (exemplified).
Advocate for the establishment of a Department of Peace and a Department of Children and Youth (suggested).
Join movements advocating for reparations to address economic disparities and historical wrongs (implied).
Educate oneself and others on the impact of corporate influence on government policies (exemplified).
</details>
<details>
<summary>
2019-12-01: Let's talk about opening a can and the future.... (<a href="https://youtube.com/watch?v=kpgsAJjMiu0">watch</a> || <a href="/videos/2019/12/01/Lets_talk_about_opening_a_can_and_the_future">transcript &amp; editable summary</a>)

Beau challenges the notion that young people can't be trusted to vote based on their ability to use a can opener, advocating for a shift in perspective towards youth resilience and capability.

</summary>

"Can't trust young people to vote."
"Don't listen to those young people who did not give up even though they didn't understand something."
"Y'all should lay off the kids because at the end of the day whether or not they open that can doesn't matter they'll figure it out."
"Most people today have an automatic can opener and even then most of the brands you don't need it anymore."
"Let's listen to the people who will laugh and record a video because they don't understand what they did."

### AI summary (High error rate! Edit errors on video page)

Beau introduces a video of a young man struggling to use a can opener during Thanksgiving dinner, sparking a tutorial on can opener usage.
The accompanying message suggests young people can't be trusted to vote because they struggle with basic tasks like using a can opener.
Beau showcases various pop-top cans, indicating the prevalence of can designs that don't require a traditional can opener.
He demonstrates how to use a can opener, expressing disbelief at the notion that young people can't figure it out.
Beau criticizes the message that undermines young voters, pointing out the hypocrisy of older generations who mock youth for not knowing certain skills.
Despite the ridicule, the young man in the video perseveres, in contrast to the elders who filmed and laughed at him.
Beau questions why there's a tendency to blame young people for their supposed incompetence rather than acknowledging their strengths and resilience.
He challenges the idea that older generations are inherently wiser and more capable of making sound decisions for the future.
Beau suggests that young people, despite facing ridicule, will figure things out on their own while being undermined by those who are supposed to guide them.
The message conveyed is to not underestimate young people's abilities based on isolated incidents of struggle or ignorance.

Actions:

for young voters,
Support and encourage young voters (implied)
Challenge stereotypes and biases against young people (implied)
</details>
