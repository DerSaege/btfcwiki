---
title: Let's talk with Howie Hawkins about the Green Party....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=CxlGzHwE1dU) |
| Published | 2019/12/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduction of Howie Hawkins, a Green Party candidate with a background in social movements and environmental activism.
- Howie Hawkins shares his history of being a teamster, his involvement in forming the Green Party, and his previous statewide campaigns.
- Emphasis on building the Green Party as a major party and advocating for eco-socialist solutions to pressing issues like climate change, inequality, and nuclear disarmament.
- Howie Hawkins details his Green New Deal proposal, contrasting it with the Democrats' version and stressing the urgency to address climate change by 2030.
- Importance of third-party representation in elections to push progressive agendas and hold major parties accountable.
- Howie Hawkins explains the dangers of the current nuclear arms race, advocating for disarmament and referencing past treaties.
- Addressing job concerns in the transition to a green economy, ensuring a just transition and providing millions of jobs.
- Expansion on the Economic Bill of Rights proposed by the Green Party, including job guarantees, income above poverty, affordable housing, universal rent control, and healthcare for all.
- Advocacy for open borders and ending mass surveillance, supporting whistleblowers like Edward Snowden.
- The need for grassroots organizing to bridge divides among working-class individuals and build a mass party for working people.
- Focus on electoral reform, advocating for a national popular vote with ranked-choice voting to eliminate the Electoral College.
- Challenges faced by progressive Democrats and the potential for a Green Party to attract disenchanted voters, particularly non-voters.

### Quotes

- "If we're gonna have a left and alternative in this country, we gotta have our own independent voice and our own independent power."
- "There's no such thing as an unorganized socialist. You gotta get in an organization."
- "Strength in numbers. When we're working together, we get our ideas tested."

### Oneliner

Howie Hawkins advocates for eco-socialist solutions, a Green New Deal by 2030, and grassroots organizing to build a major Green Party base, challenging the Democratic establishment and calling for electoral reform.

### Audience

Progressive activists, third-party supporters

### On-the-ground actions from transcript

- Contact the Howie Hawkins campaign via howiehawkins.us to volunteer, donate, and help with ballot access efforts (suggested).
- Host house parties for grassroots fundraising and awareness about Howie Hawkins' campaign (suggested).
- Engage in electioneering canvassing activities to reach out to potential supporters and collect information for future engagement (suggested).

### Whats missing in summary

The full transcript provides detailed insights into Howie Hawkins' background, policy proposals, challenges faced by third-party candidates, and the importance of grassroots organizing for political change. Viewing the full transcript offers a comprehensive understanding of his platform and strategies for political engagement.

### Tags

#HowieHawkins #GreenParty #EcoSocialism #GrassrootsOrganizing #ThirdParty #ElectoralReform


## Transcript
Well, howdy there internet people, it's Bo again.
Today, we've got another candidate for us
from the Green Party and I'm going to let him
introduce himself.
Go ahead, sir, how you doing?
Hi, my name's Howie Hawkins.
I'm a recently retired teamster,
been active in social movements
the 1960s, civil rights, anti-war environment. I was one of the people that helped form the Green
Party in 1984. The Greens have run me statewide in New York State four times against Hillary
Clinton for US Senate in 2006, and in three gubernatorial races, 2010, 2014, 2018. In 2010,
I was the first candidate in this country to call for a Green New Deal for climate action and basic
economic rights. And in 2014, we got 5% of the vote, which is the most a third party
candidate has ever got on the left in New York, except for two socialists right after
World War One, they got half a percent more than me. So I've run, you know, large scale
campaigns. And here I am running for the Green Party nomination for president, which wasn't
my idea. Much people drafted me. So I like to say it's not just me out here. It's a collective
leadership is a group that's diverse in terms of race and gender and geography and sexual
orientation and age. And we have two major objectives. One is to build the Green Party
into a real major party in this country, take steps toward that during this campaign. And
the other is to advance eco-socialist solutions to pressing problems. And there are three
life and death issues that we're emphasizing. Excuse me. One is the climate crisis. Second
is growing inequality because inequality kills. We now have a 20-year life expectancy gap
between our richest and poorest counties. And if you break it down by census tract, it's
even greater. And then the third life or death issue is this new nuclear arms race. We're
modernizing our nuclear forces, both strategic and tactical. We're getting out of nuclear
treaties, so we're calling for a pledge of no first use, unilaterally disarming to arming
to a minimum credible deterrent and on the basis of those tension-reducing initiatives
go to the nuclear powers around the world and say, we got to get rid of these weapons
of mass destruction and negotiate complete neutral nuclear disarmament, which we've been
obligated to try to do since the 1970 Nuclear Non-Proliferation Treaty.
And there are now 122 nations that have agreed to the wording of a treaty for the prohibition
of nuclear weapons in 2017, and the campaign for the abolition of nuclear weapons got the
the Nobel Peace Prize for that in 2017.
And very few Americans even know about it.
And none of the presidential candidates
are talking about it.
So if for no other reason,
that's why we need the Greens in this election.
All right, so for those people who wonder
what good a third party is gonna do,
what do you have to say for them?
Why are you running?
Well, if progressives, we're on a progressive side of things,
if progressives and socialists don't run their own candidates,
they're gonna vote for the Democrats
and they get lost in the sauce.
Your vote as a Democrat,
you don't know if it's a socialist Sanders vote
or a corporatist Clinton vote.
It's all there mixed up.
So the left disappears, our arguments disappear.
We don't speak for ourselves.
We kind of try to play political ventriloquism,
and get a Joe Biden or somebody to say
and do what we would do,
but they'll never do that.
So if we're gonna have a left and alternative
in this country, we gotta have our own independent voice
and our own independent power.
And it may be relatively small vote at first,
but when I got that 5% for governor in New York in 2014,
Andrew Cuomo, he was getting ready to run for president.
He wanted more votes than his daddy got, Mario Cuomo.
He wanted more votes than he got in 2010,
and he got less votes.
And he looked around and he saw this,
Howie Hawkins, his teamster, got 5% of the vote.
What was he talking about?
And I was talking about a ban on fracking,
a $15 minimum wage, tuition-free public higher education,
and paid family leave.
And Cuomo had to move on all those
in order to compete for our votes.
So you don't have to win the office
to have leverage in the political process
and move the ball down the field.
So that's why we need a third party.
Right, at the very least, you can get a major party
to pick up at least pieces of your platform
if you have a good showing.
Now, you brought up nuclear proliferation.
Maybe give some insight into pulling out of open skies
and all of that, because we've talked about it on the channel.
But I'm not sure that it's something
that everybody understands on the level of this
what was going on and this is what can happen now. I'm not sure what your Open Skies reference is
to. It's a one of the treaties it's a I think it was called Open Skies yeah Open Skies it's
a treaty that Trump pulled out of between the U.S. and Russia. It allowed basically limited
intermediate-range nuclear weapons. Yeah, okay. I hadn't heard that referred to as open skies,
but that is really important, because back in the 80s when we had this nuclear freeze movement,
Reagan and Gorbachev really reduced the number of weapons and almost went to
complete disarmament, it was broached. That's when the missiles in Europe, the intermediate-range
missiles were pointed right at each other with such a short reaction time. You really didn't
have time to react. And then we were worried that somebody had launched a preemptive strike
because of tensions rising, and they wanted to get them off before the other side did,
so they didn't lose their own. And that's where we're at right now with this nuclear modernization
program, multi-trillion dollar, multi-decade program. The United States is updating their
strategic forces and their tactical nukes. They're reducing the size of tactical nukes
to make them easier to use in conventional battlefields, as if you start throwing tactical
nukes around and it's not going to escalate to strategic nukes. And in the strategic nukes,
they're hypersonic. They're six times faster than the ones we got. And again, that puts us in a
situation like we have with that missile crisis in the 80s. There won't be time to react. So
somebody may shoot them off because they're afraid the other side will first. And that's why the
bulletin of the atomic sinus has the doomsday clock at two minutes to midnight now, which is
as high as it's ever been. And this is really a dangerous situation. The rest of the world is
very concerned about it. That's why they adopted this treaty for the Prohibition of Nuclear Weapons
two years ago. We're not even talking about it in this country. When I asked people how many people
knew that the campaign for the abolition of nuclear weapons got the Nobel Peace Prize,
I have yet to find anybody, and I've talked to thousands of people who even knew that.
Our media is not covering it, our candidates are not. It's a bipartisan policy to back this
nuclear modernization program, which is a race to doomsday. We've got to stop it.
Now as far as the Green New Deal, how does yours vary from the one that the Democrats adopted?
They didn't adopt it, that's the problem. This has been the signature issue of the Green Party
for the last decade, and for us it's a combination of a green economy reconstruction program to
convert the economy to reducing greenhouse gas emissions to zero and producing all energy
by renewable sources by 2030.
Because that's what the climate science carbon budgets say we got to do in the rich countries,
to have any hope of avoiding real climate catastrophe.
And the Democrats, right after the 2018 election, you know, Alexandria Ocasio-Cortez and the
Sunrise Movement, they sat in Speaker Pelosi's office and said, we want a Green New Deal
fuel. And Pelosi took what they proposed, which is a select committee, and sliced, diced,
and threw that down at a disposal. So they came back with a nonbinding resolution. But
it took the brand and diluted the content the Green Party's been talking about. It extended
the deadline from 2030 to 2050. It eliminated the ban on fracking and new fossil fuel infrastructure.
If we build out fossil fuel infrastructure, we're going to be burning fossil fuels for
for three, four, or five decades and were cooked.
It eliminated phasing out nuclear power.
And it stopped talking about cutting military budget
in a major way to put that money into a Green New Deal.
So those would be pretty big changes.
And even then, Pelosi won't even let the House vote on it.
And when it got to the Senate, McConnell said,
well, half you Democrats are running for president.
Let's get you on the record.
And Schumer, the Democratic leader said,
oh, this is a trick, I want you all to vote present.
And all the good little Democrats voted present
and said, Florida voted Republicans against it.
So first of all, the Democrats are not gonna get us
a Green New Deal.
Now Bernie Sanders, I will say, has a serious proposal.
All the others are not serious.
They're talking one to $3 trillion over 10 years,
and it's mostly tax incentives and subsidies to business.
And that's just not gonna get it done.
We have a eco-socialist budget for a Green New Deal.
You can find it on my website, howiehawkins.us.
And we costed it out to do that 100% clean energy by 2030
at $27 trillion over 10 years.
Bernie's at 16.3 trillion.
And that's because he's extended the deadline to 2050
because we think his numbers are pretty good.
And he did bring back those policies
that I said were eliminated.
The ban on fracking the new fossil fuel infrastructure,
the elimination of nuclear power
and cutting the military budget,
although he's not so specific on that.
But we think he's got a serious proposal.
We'll give him that.
It's just too slow.
We say 2030, he says 2050.
So, you know, the Democrats have taken, like I said,
they took the brand and diluted the content,
except for Sanders, there's nothing much worth talking about
what they're talking about.
All right.
So a little bit more about your history,
because I'm sure some people are gonna listen to this
and they're just gonna say, you know, here's some hippie.
I was looking at your bio,
you've got some other background as well
that might appeal to the other side a little bit.
Yeah, well, back in the 60s,
we distinguished between the political radicals
and the hippies.
And sometimes we'd all show up at the anti-war demos,
but we were coming from very different
kind of cultural backgrounds.
You know, I've been a construction worker
teams through my adult life. When my draft number came up during the Vietnam War, I enlisted in the
Marine Corps to join the GI movement against the war, which was very strong at that time. It sounds
kind of crazy to enlist in the Marine Corps, but when I got there, it was officer training. It was
off-campus training because I was drafted out of college. And I'll tell you, the veterans, they
didn't want to go back to Vietnam. They thought that was the wrong war. I mean, there was some
young guys they wanted to be. We didn't have Ramro yet. It was John Wayne back then. You know, they
They wanted to be the next John Wayne.
But the veterans knew that that was not a just war, not a war we should be fighting.
You know, it was their country and what the hell were we doing there?
So even in the Marine Corps, there was, you know, the Vietnam syndrome,
as it was called at that time.
So, you know, hippies didn't do that kind of thing, you know, but I have.
So, you know, I'm nothing against hippies, but I'm more,
you know, blue collar kind of guy.
Now, as far as one of the big complaints that we hear about the Green New Deal is, but what
about jobs?
Now, you know, if we do this, we're going to eliminate fossil fuel jobs.
What happens to those people?
Blue collar workers?
Well, we provide for a just transition to the jobs that will be created by the Green
New Deal. And I costed this out with a political economist named John Wren, who's been working
on this for a decade. And we come up with 30 plus million jobs to build this green infrastructure
over the next decade. The problem will not be technology, it will not be finding the
money. The problem will be finding enough people to do the work. We will have full employment
building this out. Jobs is not the problem. The corporations always say it's jobs versus
environment and they're full of it like they are with a lot of their false advertising and you know
the fossil fuel industry promoting fake science saying there wasn't a problem when they knew
themselves their own scientists was saying there is a climate problem coming so you know jobs is
not the issue we got plenty of jobs with the Green New Deal. So what are some of the other
are key pieces of the platform that you would want to expand on and tell people about?
Well we have a lot on besides the three life and death issues I talked about. Let me just
elaborate on how the Green New Deal deals with the inequality issue because we call
for an economic bill of rights. This is something that's been on the public agenda since Franklin
Roosevelt as president called for it in his last two State of the Union addresses in 1944
in 1945, and he wanted Congress to enact this, and they never have. And what those rights
include is a job guarantee. So you get a public job if you can't get a private job providing
public services or public works that the community is defined as needed. An income above poverty
guarantee, and that can be built into the tax structure. So if your income is below
the poverty line, you get a check every month to bring you up to the poverty line until
you can get to the point where you make enough money, you're over the poverty line. It would
provide affordable housing for all, which for us is we got to expand public housing
and do it the right way, multi-income, diverse, rather than segregated housing for people
of color away from public transportation and every other resource. They do that in Europe
and it cross-subsidizes, it costs less, it's the quickest way to build affordable housing.
What we've been doing since about 1970 is subsidizing private developers with tax breaks
and it's more expensive that way. We also need immediately universal rent control because the
rent is too damn high and we did that during World War II when we converted to building arms to
fight the fascists and we weren't building housing and the housing rates started going up so the fed
said we're just going to you know have rent control for the duration. So that's the immediate
relief. Next thing is comprehensive health care for all and that's a medicare for all type program.
program, we actually want to do a national health service like they do in England, rather
than just national health insurance, so the doctors work in public clinics and hospitals
on the public salary, and there are democratic boards elected locally that federate at the
state and national level, so we have community control, and we have better cost control because
you don't have income-maximizing organizations like hospitals and clinics, and of course
the drug companies trying to feed off the trough of public insurance.
And then next is a secure retirement.
And I'm going to have a statement out tomorrow.
The Teamsters have a presidential forum.
I'm the only Teamster running for president.
They didn't invite me.
But as a Teamster, they asked me to send in a video question, which I did.
I said, my pension was cut 20% because of the law both parties passed in 2014 called
the Multi-employer Pension Reform Act.
It took away our protections under the Employment, Retirement and Income Security Act of 1974,
before they couldn't take away our earned benefits. My pension was cut 20 percent because
of that law. So I asked those candidates after explaining that, I'm so mad about it, I'm
running for president, but I want to know what you're going to do. And we'll see on
Saturday. So I got a statement coming out about that. But I think the immediate thing
beyond those of us who still have defined benefit pensions, which is a shrinking minority,
we need to double Social Security benefits by raising the cap, eliminating the cap on
income that pays the Social Security tax and some other progressive tax reforms.
The baby boomer generation now, one-third of them are still paying their home mortgage.
2.8 million of them are still paying student loans.
Wages have been stagnant our whole adult working life since the early 70s, but the cost of
housing, healthcare, and college in particular have escalated well beyond the inflation rate.
So most baby boomers don't have any savings.
And the average social security benefits is about $15,000 a year, you can't live on that.
So that's the way we're going to get retirement security.
So that's the economic bill of rights.
Beyond that, we need to address some of the real abuses we see, like the atrocities on
the border.
The children got to be reconnected with their families.
Our policy is let's have open borders with Mexico and the Central American countries,
like Europe has, the European Union within their countries.
You know, the border move, as some of the Chicanos like to say, we didn't move across
the border.
The border moved across us after the war in 1848.
And a lot of those people have family on both sides that go back and forth.
You know, let's have the borders open for shopping, recreation, employment, residence.
And you know, if people are afraid everybody in Central America will suddenly come here,
they need to ask themselves why everybody in Greece didn't go somewhere else in Europe
when Greece was a basket case, most people don't want to move. They want to make a living
where they can. And many of these immigrants just want to come here and make some money
and send it back to their families back home. So we got to just relax about the immigrants.
This is a nation of immigrants. Let's make it a strength instead of something we're afraid
of. That's a big issue. I mean, there are a lot of issues. We got to stop the mass surveillance.
I think Edward Snowden is a real hero. And I would pardon all the whistleblowers who
charged under the Espionage Act, which in itself is totally unjust. You can't defend yourself with
a public interest argument that the information you released was in the public interest. That's
not even permitted. So it's a kangaroo court kind of situation. But I pardon them all,
but if I was president, I'd bring Edward Snowden into the administration because the way he sees
the world, the biggest conflict now is between democracy and democratic rights and freedom
versus authoritarian regimes. And these mass surveillance systems like the United States
set up, there's surveillance all through the NSA, that's what Snowden exposed. But look at China
with the social credit system, where they're ranking everybody based on what they buy,
and they got all this facial recognition technology. I mean, it makes George Orwell's 1984 look like
a beginner's course in repression. So I think that's a huge issue. We got to end mass surveillance,
only surveillance when you get a warrant from the court and not go on. I mean, there are a lot of
issues. There are dozens of them, but those are some of the highlights. I got a question for you
because it's a question I get a lot and I never have a good answer. How do people of your mindset
reach out to other blue collar workers, because we're seeing more and more blue collar swing
right. And I get asked that a lot. I don't have a good answer. I'm hoping you do.
Well, I think those of us who are progressives on the left, we need to get off our computers,
off of Facebook, off of Twitter, and go out and talk to real people and build real organizations
is where people meet and talk to each other.
You look at the working class, and we have common interests.
But we're very divided.
It's by race.
We're more segregated, actually, than we were coming out
of World War II, just physically.
The segregation is more distant because of sprawling suburbs.
So people know each other less.
But we're also divided by occupational hierarchies.
Wage workers are in some are white collar, some are blue
some are in unions, some are not.
The ones that are not, we resent the ones that are.
Some are government,
they got better protections than private sector.
Small business is different than big business.
We got all these divisions.
And of course the bosses are always telling us,
it's those other workers that are your problem.
You know, those immigrants, those minorities,
you know, those people who got a union
or those people that don't got a union.
And they divide us and we need to find a way for us
to sit down and talk to each other, get to know each other.
And that to me is building a mass party of working people.
And that is something that takes a lot of footwork
and knocking on doors.
We gotta be organizers, not just preaching at people,
here's the answer, but knocking on a door and listening
and hearing what their issues are
and then working with them on those issues,
building trust and relationships across race lines,
across geographical lines, occupational lines.
You know, even, we got working class people
are administered by the state, even the welfare system or the prison system. And they don't see
themselves, I mean, they're in a situation where the people who are administering them are also
working class people. How do we bridge that gap? So that's where I think a party on the left can
play a big role. You know, the unions, since they have automatic dues check off, the bureaucracy is
kind of detached from the members. Back in the old days, the shop steward had to go around and ask
the workers for the dues every month and the worker could kind of hold that money back and say
well what about my agreements or you know what about this and what about that they really had
them listen now they kind of detached so you know getting our unions more reflective of the rank and
file and including them is another issue and I think an organized party could organize people
to do that so we need to be organizers as well as activists and we need to be in the streets not just
on the internet. Community organizing. That's my, that's the answer. And believe me, my
audience is gonna laugh when they hear that because that's the answer I have. But I was
hoping for some, you know, some stroke of brilliance on that one. So if there is one
thing that an individual can do, one, you know, by themselves to change the world, make
it a better place, what do you got? Well, find one other person and then another person.
There's strength in numbers.
And when we're working together, we get our ideas tested.
We have to speak up for ourselves.
We can prove our speaking ability, our ability
to articulate our positions.
You need to be in an organization.
So I think the most important thing an individual can do
is get involved in an organization,
an organization that's democratic.
I'm not talking about move on where you wait to get
your orders online by some staff that some billionaires hired,
and then they decide for us what we're gonna do.
We need to be deciding for ourselves.
And we're gonna make mistakes,
but we're gonna learn from no mistakes.
We gotta be engaged that way.
So I would tell individuals,
there's no such thing as an unorganized socialist.
You gotta get in an organization.
Sounds good.
Yeah, I think I caught a delay there for a second.
So what do you think your chances are of at least driving some of these core issues into
the candidates, you know, into their platforms?
Well, this is a very tough year.
You know, Trump has got people scared, stupid.
They don't really want to talk about issues, they just want to get Trump out of there.
That's people from the center to the left.
So it's really hard to get traction.
On the other hand, you know, our campaign aims to get on all 51 ballots, and we are
not just going for the Green Party nomination, I already have the Socialist Party nomination.
They don't have any ballot lines, it's more like an endorsement, but that's good.
And then there's about eight or so progressive state parties seeking their nomination too.
So hopefully we'll build a united, independent left out of this campaign.
And by the time we come out of the Green Party convention in early July, we're going to be
on the ballot and then, you know, the media will start to pay attention and Democrats
start to pay attention. Of course, the first thing it will say, why are you spoiling the
election? Go away. And, you know, our answer is no, we're improving the election. You're
not talking about the nuclear arms race. You botched the Green New Deal. You're really
not for Medicare for All and make our case. And the other thing we'll say, and this is
an issue that needs to be raised, is that if you want to end the problem of splitting
the vote and quote-unquote spoiling elections, the Green Party has been giving you the answer
for two decades since the 2000 election, and that is get rid of the electoral college and
have a national popular vote by rank choice voting. Rank choice voting, you rank the choices
one, two, three, and if nobody gets a majority in the first round, the last place candidate
is eliminated and their vote is transferred to their second choice, and you keep that
process up until somebody gets the majority of the votes. That way people can vote for
candidate they really want without fearing they'll help their worst enemy. With that kind of election,
nobody's going to be asking the Greens why we're in an election. They're going to be asking,
what's your position? That's where we want to get to. And I'll be damned if we're going to let the
Democrats chase us away from the debate, because they won't take up the solution we've been
offering them for 20 years. Join with us to fight for that change. A national popular vote with
with Ranked Choice Willing.
All right, I should point out after thinking about it,
Open Skies was the surveillance thing.
You're right, that's why you didn't know the name.
I'm on the wrong, I was off on that.
Open Skies was ringing a bell, but I couldn't.
Yeah.
It was surveillance of nukes, not actually
the intermediate one.
OK, so I guess at the end of the day,
You've got a platform that a lot of progressives,
a lot of people who consider themselves Democrats,
want to embrace.
But it seems like your main opposition
is the Democratic establishment.
I mean, do you think that's true?
Well, in terms of those that have power,
yeah, it's the Democratic establishment.
I think we do have a problem with progressive Democrats.
The Democratic party is really two parties.
got a progressive wing that Bernie Sanders probably best
represents, although some of them are supporting Warren and
some others. And then you got the establishment wing, which
is very corporate. You know, Biden is the epitome of that.
But there are a lot of the others. Buttigieg is getting a
lot of money from that quarter, both Wall Street and Silicon
Valley. And I'm just hearing today about military
contractors. So, you know, I think the future of the Green
party is two places. One is those progressive Democrats who finally get fed up with getting
their Democrats in, and then they don't get what they want. We just had that in New York.
For years, we've had a Republican Senate, and the Democrats, they passed for like 20
years in a row a single-payer bill in the assembly, which the Democrats controlled.
Well last year, the Democrats got control of the Senate, and then they couldn't pass
single-payer in the assembly, because now it was for real. And the insurance companies
were filling their coffers, and now the Democrats didn't get it done. And progressives, you
know, they will see that. So there's about half the Democratic Party ought to be with
the Greens. But the biggest block of voters, the ones that win almost every election, are
the non-voters. And that's mostly working-class people, people of color and young people,
who are alienated by both parties and feel like nobody knows what their issues are, cares
about them, even knows what they're going through. And those are the people, like we
were talking about before, we got to reach by community organizing, you know, building
relationships by, you know, community action, and bringing them back into the political
process. That is the future majority base of a Green Party that's a major party.
Oh, I definitely agree with that for almost any problem.
All right, so parting shot.
You got anything you want to say that we didn't cover?
Do you have a website you want to send people to,
petition that you need them to sign, anything like that?
Yeah, well, the place to go for all that is howiehawkins.us,
H-O-W-I-E, H-A-W-K-I-N-S, dot U-S.
put that in your search engine, and you'll get our website. And on the website, you can find,
statements that we've been making, and news about the campaign, how to volunteer, how to donate.
What we need right now is it depends on the state. There are 51 jurisdictions. We have
ballot status in 21. We've got 30 more to go. And it depends on your state. But you contact
the campaign, we can put you in touch for how to get ballot access in your state if
you don't have it. We can get you set up to do the kind of election nearing canvassing
we need to do, and that is talking to people, getting their names so we can stay in touch
with them and getting them ready for the election a year from now. Grassroots fundraising, hosting
house parties. I mean, one of the goals of this campaign is everybody volunteers, we're
going to help you find a way you can contribute, besides money, you know, we,
everybody needs to do that, you know, it's like, if we don't fund us, nobody
will, because we're not taking corporate money.
It's gotta be lots of small people contributing, you know, what they can.
So howiehawkins.us, I think that's where to follow up.
All right.
So if you're watching on YouTube, this will be the end of the show.
if you're on the podcast, there might be something coming on after this. I'm honestly not sure.
Either way, it's just a thought and y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}