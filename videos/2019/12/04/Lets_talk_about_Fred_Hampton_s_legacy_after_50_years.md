---
title: Let's talk about Fred Hampton's legacy after 50 years....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=3aK7hY_67Ok) |
| Published | 2019/12/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recalls an incident from 50 years ago at 2337 West Monroe Street in Chicago that altered American history.
- Mentions an up-and-coming activist named Fred Hampton who could have created significant social change.
- Expresses a belief that the incident was a deliberate effort to destabilize and remove black leadership in the United States.
- Encourages people to look into the event themselves and analyze all the evidence.
- Suggests two possible conclusions: isolated bad actions or a targeted effort against black leadership.
- Believes the system lashed out against those seeking change, showing systemic resistance to change.
- Despite the tragic event, notes that it led to a massive outcry and political changes.
- Points out the election impact following the incident, leading to Chicago's first black mayor.
- Draws a connection from the past events to the election of President Obama.
- Emphasizes that when the time is right, no system can stop an idea from manifesting.

### Quotes

- "Nobody can tell you this is what happened that day."
- "When an idea's time has come, nothing will stop it."

### Oneliner

50 years ago, an incident in Chicago altered history, revealing systemic resistance to change but also the unstoppable power of ideas.

### Audience
Activists, Historians, Social Change Advocates

### On-the-ground actions from transcript

- Research and learn about historical events like the one mentioned (suggested)
- Get involved in local politics and elections to create change (implied)

### Whats missing in summary
The emotional impact and significance of understanding historical events for societal progress.

### Tags
#SocialChange #SystemicResistance #BlackLeadership #HistoricalEvent #Activism


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we're going to talk about something that happened
50 years ago today at 2337 West Monroe Street in Chicago.
I believe that what happened that day altered the course of
American history.
It really did.
I don't think that incident is given the credit that it
There was an up-and-coming activist named Fred Hampton.
Many people, myself included, believed that if he had been left to his own devices, he
would have created a massive impact on American society, would have created social change.
talking Martin Luther King levels of social change, but he was not left to his own devices.
What happened that day is debated by some, and nobody can really tell you what to think.
Nobody can tell you this is what happened that day.
But what happened that day is extremely important, and I think it's one of those things that
everybody should know about and I implore you to look into it yourself.
Really look at all of the evidence.
You'll come to one of two conclusions more than likely.
One is that it was just a few bad apples doing their thing.
The other is that it was part of a concerted effort to destabilize and remove black leadership
the United States.
And that can be disheartening when you look at this and you think about it, you realize
that if that's what it was, well, they can do that to anybody at any time for any reason.
I do believe that's what it was.
I believe that it was the system lashing out against those that wanted to change it.
Systems are reluctant to change.
And that can be disheartening for people who want systemic change today.
But rather than focusing on what happened that day, I want to talk about something else.
Because even though they didn't leave him to his own devices, it still changed history.
Still changed history, at least I think so.
Because after this, there was a massive outcry, huge.
That state's attorney, gone during the next election.
And I think it was that moment when black Americans realized how much political power
they had.
wasn't too long after that, Chicago had its first black mayor, Mayor Washington.
I think that had they jerked him out of bed and said, hey, this is what we're about to
do to you, but this is what's going to happen, he would have been all right with it.
Because I think you can draw a direct line from the events of this day to getting rid
of Hanrahan, to the first black mayor in Chicago, to the inauguration of President Obama.
I think Hampton would have been okay with that trade.
And I think that we all need to realize that it doesn't matter how much the system resists
it.
When an idea's time has come, nothing will stop it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}