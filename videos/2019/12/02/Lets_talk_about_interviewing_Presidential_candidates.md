---
title: Let's talk about interviewing Presidential candidates....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=bQqZBzBksVA) |
| Published | 2019/12/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Planning to interview presidential candidates to understand their base of support.
- Interested in learning who the candidates are and why they have support.
- Prefers a conversational approach over hardball questions to get to the truth.
- Believes that keeping candidates talking will eventually reveal their true selves.
- Values offhand comments over prepared sound bites for genuine insights.
- Acknowledges valid reasons to disqualify every candidate from holding office.
- Questions the concentration of power in the hands of political officeholders.
- Leaves viewers with a thought-provoking reflection on trust and power.

### Quotes

- "You should not be trusted with that much power."
- "It's almost as if nobody should be trusted with that much power."
- "No big mystery there."
- "The office has too much."
- "Y'all have a good night."

### Oneliner

Beau plans to interview presidential candidates to understand their support, preferring a conversational approach to reveal candidates' true selves, ultimately questioning the concentration of power in political office.

### Audience

Viewers

### On-the-ground actions from transcript

- Reach out to neighbors to understand their support for political candidates (implied)
- Engage in meaningful, conversational dialogues to uncover truths (implied)
- Question the concentration of power in political office and its implications (implied)

### Whats missing in summary

The full transcript provides deeper insights into the dynamics of political interviews and the implications of concentrated power in office.


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we're going to talk about the interviews, who I'm
going to hopefully interview, why, how, and what the
responses to the different suggested
candidates really tell us.
Because that might be surprising.
Who?
Literally anybody running for president.
I mean, yeah, there might be an exception.
I'm probably not going to interview some dude with a red armband, but just about anybody else.
Why? Because these people have a large base of support from our neighbors, from people we know.
Support each one of these candidates. I want to know why. I want to know who they are, a little bit about them.
It's really that simple. No big mystery there.
How? And this is a question that when it is thrown up, it is thrown up in a
confrontational manner most times. The way I did with Marianne Williamson
conversationally, I know that's not real journalism. A real journalist should
engage in hardball questions. Maybe. A real journalist's job is to get to the truth.
hopefully. I learned a long time ago that when you're dealing with people who are
trained to withhold the truth, you don't ask them pointed questions because
they're already prepared for that. They're ready for that. They know what
they're gonna say. You don't push them that way. Politicians have prepared
answers to questions you don't know to ask yet about scandals that haven't been
uncovered. They're ready for that. You want to get to the truth when you're
dealing with somebody who is trained to withhold it, you don't have to be abrasive.
You just have to keep them talking and eventually they'll tell you.
Even if they're not trying to withhold the truth, the key questions people won't answer,
they'll come up with Ms. Williams and one of the big questions that everybody had was
her views on medicine because it's come up.
I didn't ask a question about it.
one, but go back and listen to that video. She answered it in an offhand way. That offhand
comment is going to be more reliably accurate about as to how she truly feels than any sound
bite ready statement. So no, you won't see me engaging in hardball questions. It will
be conversational and it'll take time, like that one.
But one of the things I notice is that any time a candidate comes up, somebody else comes
along and says, no, don't interview that candidate.
They did X, or they said they were going to do Y, or whatever, and there's a valid reason
to excuse every single candidate from every single party from holding that office.
You're right.
That's what we find out.
When we look at the information as a whole, presented by everybody, when you take each
piece of information that's presented, that's the conclusion.
Every single one of them, there's a reason to say no.
You should not be trusted with that much power, yeah.
It's almost as if nobody should be trusted with that much power and that the office has
too much. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}