---
title: Let's talk about genius, robots, futures, and impeachment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Eb2krF7Ydho) |
| Published | 2019/12/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the topic of discussing a genius.
- Talks about Isaac Asimov, a genius known for science fiction.
- Mentions that Asimov made predictions in 1964 about life in 2014, some of which have come true.
- Talks about Asimov's interest in history and politics, particularly Watergate.
- Criticizes the idea of excusing criminal actions based on historical precedents.
- Calls for holding leaders accountable.
- Mentions World AIDS Day and Asimov's death due to AIDS on April 6, 1992.
- Makes a prediction about the future if leaders are not held accountable.

### Quotes

- "He saw all this coming."
- "If we don't start holding those in the White House accountable 50 years from now, we won't be talking about impeachment because we won't have a presidency."

### Oneliner

Beau talks about Isaac Asimov's genius, his accurate predictions, the importance of holding leaders accountable, and makes a chilling prediction for the future.

### Audience

Citizens, voters, activists.

### On-the-ground actions from transcript

- Hold leaders accountable (implied).
- Advocate for transparency and accountability in government (suggested).

### Whats missing in summary

The emotional impact of discussing the genius of Isaac Asimov alongside the call for accountability in leadership.

### Tags

#Genius #IsaacAsimov #Predictions #Accountability #Leadership


## Transcript
Well, howdy there, internet people, it's Beau again.
So tonight we're going to talk about a genius.
We're talking about a genius for two reasons, yesterday and today.
Today, because on December 2nd, 1950, a collection of his short stories
was published under the title iRobot.
The stories were written individually and then cobbled together
this kind of clumsy framework of somebody telling the stories to a reporter. If you're going to
read it, just skip that part and read the stories individually. That's what he's known for, science
fiction. Isaac Asimov. And he should be. He should be. But he was a genius in a lot of other ways.
In 1964 he made a bunch of predictions about what life was going to be like today, well five years
ago. He's talking about 50 years in the future, so 2014. Communications will become sight sound
and you will see as well as hear the person you telephone. Yeah. Satellites hovering in space will
make it possible to direct dial any spot on earth. Electro-luminescent panels will be
in common use. Ceilings and walls will glow softly and in a variety of colors that will
change at the touch of a button. LED lighting. Mock turkey and pseudo steak will be served.
It won't be bad at all, but there will be considerable psychological resistance to such
Wall screens will have replaced the ordinary set.
An experimental fusion power plant or two will already exist.
Large solar power stations will be in operation.
Only unmanned ships will have landed on Mars, though a manned expedition will be in the
works.
The population of the US will be 350 million, he's a little off there.
Vehicles with robot brains can, it's a Tesla car.
Said all this in 1964, think about what the world was really like then.
And he saw all this coming.
And this is what he's known for, science fiction, stuff like this.
He actually published books on all sorts of things.
He was a student of history and he followed politics very closely.
He followed Watergate very closely.
I was not impressed by the argument that it has spared the nation an ordeal.
To my way of thinking, the ordeal was necessary to make certain it would never happen again.
He's talking about Nixon basically walking free after resigning, just like those other
predictions he was right.
That type of behavior has increased in frequency and gets excused just like cheerleaders, people
just cheering it along.
And they use all sorts of excuses.
Well the other guy did this and each generation of president goes a little bit further, becomes
a little bit more criminal in nature.
We cannot use the past to excuse criminal actions of today.
If this line of thinking continues, just remember, next time it may not be the guy you cheer
lead for.
At some point in time, this has to stop.
We have to hold our leaders accountable.
I said yesterday and today, today is when his book was published.
Yesterday was World AIDS Day.
On April 6, 1992, he died of AIDS.
Nowhere near the mind that he was.
I'm going to make a prediction of my own for 50 years from now.
If we don't start holding those in the White House accountable 50 years from now, we won't
be talking about impeachment because we won't have a presidency.
Anyway, it's just a thought.
have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}