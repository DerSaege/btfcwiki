---
title: Let's talk about Miss Universe, waste, and wins....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KJpwfxdXUq4) |
| Published | 2019/12/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Describes his visit to a remote gas station in the country.
- Talks about the unique setup of the gas station with a barbecue stand, picnic tables, and a little diner.
- Advises on how to navigate interactions at such busy places, comparing it to being a new inmate in prison.
- Shares an unexpected and positive interaction he witnessed between three guys at the gas station.
- Recounts their open-mindedness towards discussing LGBTQ+ issues, particularly around Miss Universe competition.
- Expresses surprise and admiration for the guys' progressive views on acceptance and equality.
- Questions society's ability to let people be themselves in this day and age.
- Appreciates the guys' inclusive commentary as a win and a step in the right direction for the world.
- Acknowledges the importance of recognizing and celebrating small victories for progress.
- Concludes with a reflective message on valuing individuals beyond surface judgments and stereotypes.

### Quotes

- "I can't believe in this day and age, there are still places where people just can't be themselves."
- "I'm assuming you're not picturing us. If you're not picturing us and you are picturing them, you might want What?"
- "And sometimes we just got to take the wins where we can get them."

### Oneliner

Beau describes a surprising and uplifting encounter at a remote gas station, showcasing progressive views on acceptance and equality, marking it as a win towards a better world.

### Audience

All individuals

### On-the-ground actions from transcript

- Celebrate small victories for progress (implied)
- Engage in open-minded and inclusive conversations (implied)

### Whats missing in summary

The full transcript provides a deeper insight into the importance of acceptance, equality, and celebrating progress in unexpected places.

### Tags

#Acceptance #Equality #Progress #Inclusivity #Community


## Transcript
Well howdy there internet people, it's Bo again.
So tonight, we're gonna start the week off
with something lighthearted.
Uplifting, almost.
In case you don't know, I live out in the country.
Today, I was out in the sticks, like way out there.
And when you get that far out, gas stations
are something out of a movie.
And the one I pulled into, it's a gas
station convenience store like normal got a little barbecue stand out front
there's picnic tables people eat and lunch inside they sell hardware auto
parts feed there's a little diner it's one of those places now just before sun
up at noon and just before sundown these places are packed because there's
nothing else out there and if you come up on them at any other time it looks
like something that's really a plot device at the beginning of a horror
movie. If you ever find yourself in one of these places when it's busy, I suggest
you act like an inmate your first day in the joint. Just don't make eye contact
with anybody and try to keep to yourself. Not because they're horrible people but
because they're gonna want to talk to you for like an hour and inevitably
they're going to say something that is going to make you just cringe.
So I'm standing in a line and there's three guys there drinking their coffee and their
sandwiches.
I'm assuming they're regulars.
And they start talking.
They're talking about the Miss Universe competition.
And the guy's like, you know that girl from Myanmar, she's real pretty, but she won them
Alphabet girls.
Here we go.
He's like, you know, but I guess in her home country, it's illegal, and she's like open
about it, trying to make it better, because I guess the law can still get you there for
that.
I can't believe in this day and age, there are still places where people just can't be
themselves.
What?
I have to admit, that was not the commentary I was expecting.
And his buddy chimes in, and he's like, you know, I don't care what people do.
at home. I just wish they'd keep it there, you know, when." And he says a guy's name.
You know, when he starts talking about his boyfriend, it creeps me out. I get to picturing
it and I don't like it. Third guy pipes up and he's like, you know, that's not fair.
I talk about my wife all the time. Don't tell me to keep it at home. I'm assuming you're
not picturing us. If you're not picturing us and you are picturing them, you might want
What?
You know?
Like, I'm just standing there slack-jawed.
And I guess my gaze caught their attention because the guy's like, they're a problem?
Everything okay?
I'm like, man, that was just not what I was expecting to hear in here.
And he's like, oh, I get it.
Because I'm wearing a real tree, I've got to be a bigot, right?
I'm pleasantly surprised.
So I go ahead and I'm checking out and they've got her pulled up on their phone and they're
talking about her.
Man, she's pretty.
What a waste.
Almost made it out of there because there it is.
What a waste.
Waste of what?
Looked her up when I got home.
And not just does she push for equal rights, she pushes for better treatment of children,
all kinds of stuff.
It doesn't seem like a wasted life, it's that one item, that one thing that is so defining.
And when you take that away, you can't objectify it, it's a waste.
Didn't say anything, because frankly I'm counting today as a win.
And at the end of the day, that commentary from those guys in that location, oh that's
a win.
That is a step in the right direction for the world at large.
And sometimes we just got to take the wins where we can get them.
Anyway, it's just a thought.
have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}