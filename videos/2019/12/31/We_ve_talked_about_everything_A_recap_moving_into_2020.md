---
title: 'We''ve talked about everything: A recap moving into 2020....'
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=mEryako3A-U) |
| Published | 2019/12/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The president's decision to cut aid to Central American countries out of fear.
- Concern over the wall working both ways to confine Americans.
- Criticism of the younger generation's lack of exposure to accurate history.
- Pointing out the propaganda in history textbooks and the internet's raw information.
- Criticizing blind patriotism that supports harmful actions.
- Addressing the tactic of using fear to win elections and gain power.
- Challenging the unfounded fears and stereotypes around asylum seekers.
- Criticizing the dehumanization and mistreatment of migrants at the border.
- Addressing the issue of civilian gun violence and the role of governments.
- Arguing against the effectiveness of an assault weapons ban.
- Exploring the glorification of violence and its impact on society.
- Advocating for understanding the historical trauma of minority communities.
- Pointing out the importance of arming minority groups for protection.
- Addressing the fear and division around the changing demographics of America.
- Criticizing the lack of accountability and excessive use of force by law enforcement.
- Drawing parallels between legal actions and moral implications in history.

### Quotes

- "Guns don't kill people. Governments do."
- "We are a violent country. We always have been."
- "If you make your community strong enough, it doesn't matter who gets elected."

### Oneliner

Beau challenges blind patriotism, addresses gun violence, and advocates for community empowerment.

### Audience

Community members, activists

### On-the-ground actions from transcript

- Contact your representative to address foreign policy impacting migration (implied).
- Advocate for understanding historical trauma in minority communities (implied).
- Strengthen your community to be self-sufficient regardless of political leadership (implied).

### Whats missing in summary

The full transcript provides a deep dive into societal issues, historical perspectives, and the importance of community resilience.

### Tags

#Patriotism #GunViolence #CommunityEmpowerment #HistoricalTrauma #Accountability


## Transcript
So the president said something and the media didn't really comment on it.
They didn't, at least not to my satisfaction.
Um, to me it was terrifying.
He said he's cutting off aid to these countries in Central America
because they're not doing their job. They're not stopping their people from coming here,
in case you're still not getting it.
The man who wants to build a wall along our southern border and militarize it
said that he believed it was his duty as a head of state
to keep you in the United States if need be.
I've said it from the beginning, that wall works both ways.
It will be used to keep Americans in.
This isn't the America I remember.
I don't recognize America anymore.
You're not hearing it from people younger than me.
We learned our history in schools.
Learned about how great we were in those textbooks,
those big, thick textbooks.
They didn't get that.
They didn't get that propaganda,
and they didn't learn history, not the way we did.
They didn't get it from a thick, government-approved textbook,
a sanitized version.
They got it from the internet, raw.
That's why when we say, you know, I don't recognize America, I can't believe we're
taking kids from families.
And they show up, really, because here's a picture of us doing it to natives.
Here's a picture of us doing it to slaves.
No, this is as American as apple pie.
Now I know there's still a whole lot of people who consider themselves to be patriots that
support this man. If you believe in the ideals of freedom and liberty, you can't. If you've
chosen to, you've sold out your ideals and your principles and your country for a red
hat and a cute slogan. It's that fear. Everybody's afraid of everything. So that's all a politician
needs to do is tap into that fear. That's how you win an election. You gather a group
of low information, middle income, middle Americans, and you tell them what to be afraid
of and how you're going to save them from it.
Then you get the votes and you get the power, then you can do whatever you want and they'll
defend it.
We're going to talk about the fears of these asylum seekers, that group coming up.
We're going to address some of those concerns and then we're going to talk about what I
think the real fear is.
Because when you hear about it, you see it in the comments section, it's always the same
thing. There could be ISIS in that group, MS-13. They could be drug mules. They
could have diseases. They're gonna bankrupt us. We can't take that many
people. It's gonna overwork our medical infrastructure. They could all be
rapists and killers and murderers and armed robbers. Why hadn't any of that
happened yet. These giant groups of asylum seekers have been coming up for
15 years. 15 years. You would think that if that is a serious concern, I mean, there
would be some kind of evidence of that occurring so far. It's almost like those
fears are unfounded. Whether you came legally or illegally is irrelevant. They
are doing it legally. That's how the law works. Now if you have that big of a
problem with them coming here I would suggest you call DC and get a hold of
your representative. Maybe talk to them about how our foreign policy impacts
these countries. Talk to them about how our drug war just funds the gangs down
there. Maybe do that instead of blaming the victim. I was busy answering the
comments in my last video on YouTube. All the ones saying that no we're not
really going down this road to fascism. We're not headed towards genocide. This
would never happen here. They're not concentration camps. We're not
dehumanizing people. I was busy replying to all of that when news broke that
The border guards were laughing about telling a mother to drink out of a toilet, but they're
laughing about it.
Because that dehumanization has already started.
It's already started.
They're not real people, they've been drinking out of toilets like a dog.
And that's part of it.
That is part of this trend, it's part of the stages.
This is what makes it really easy when that order comes down.
We need to evacuate these people and by evacuate I mean into a ditch.
If you can sit there and laugh at a mother being told to drink out of a toilet, you'll
probably be okay with putting a bullet in the back of her head.
What people need to remember is that civilian gun violence is nothing.
It is nothing in the grand scheme of things.
If you look at the statistics, it's a blip.
Guns don't kill people.
Governments do.
These same people who a few weeks ago were talking about concentration camps and our
march towards fascism are advocating disarming.
360 million guns in civilian hands is a deterrent.
It is a deterrent against tyranny.
And I know somebody out there is like,
you can't fight a drone or a tank or an A-10 with a rifle.
That's not how insurgencies work.
And that's the reason you need to want,
you should really want this deterrent
because if the deterrent isn't there
and you have to actually act, it's horrible.
You don't fight the A-10 or the tank or the drone.
You find their family.
Wycliffe Middle School, one dad, five wounded.
Blackville Hilda High School, two dead, one wounded.
Pearl High School, three dead, seven wounded.
Heath High School, three dead, five wounded.
Westside Middle School, five dead, 10 wounded.
Thurston High School, four dead, 25 wounded.
Columbine High School, 15 dead, 21 wounded.
What do all of these have in common?
All of these shootings took place
when the United States had an assault weapons ban in place.
This is about a third of them.
It's not even all of them.
The assault weapons ban is an emotional reaction.
It's a band-aid on a bullet wound, though.
It's not going to do any good.
It didn't last time.
The assault weapons ban isn't going to do
what you think it will.
And I know somebody's like, well, it's not going to hurt.
Yeah, it will.
The reason weapons like the AR-15 get used so often, the AR-15 is the most popular rifle
in the United States.
So when a kid decides he wants to shoot up a school, guess what's in the house?
This would be a lot like banning the Camry because it gets used in a lot of drunk driving
accidents because it's a popular car.
Doesn't make any sense.
It won't hurt.
Yeah, it will.
Most of the weapons that are classified as assault weapons by legislation, they are low
to intermediate power like the AR-15.
If you take those off, get rid of them, and let's assume somehow you're magically going
to make this work and people are just going to give them up, it will be replaced with
the most likely candidate is a.30-06, which is a very high-powered rifle.
So we go back to our roots of mass shootings.
The first one that people think of is Charles Whitman, Texas clock tower.
Killed 16 people, wounded 32.
His primary weapon, a Remington 700 bolt action rifle with no detachable magazine.
It's not the design that does it.
It's not the design.
That's a talking point.
Reality says otherwise.
What about other countries?
Something that pops up a lot.
Other countries did not have more guns than people.
The United States does.
Mental health checks.
This is a big one because who would be against that?
Well tell me what one is.
Where everybody's agreeing to it but nobody knows what they're talking about.
What kind of mental health?
does that preclude you from purchasing a firearm it stigmatizes mental health now
to the gun people just stop this is your fault this is your fault it's not my
fault yeah it is guns just a tool right how many photos you have on Facebook of
of you holding a hammer, a screwdriver, or a Salzall, pretending like it makes you a
man? None, right? Just that rifle. Just that one tool. Glorifying that violence. Why I
needed to fight back against the government, sure. You could, but you won't. Glorify
violence for everything. Everything, every little infraction becomes something worthy
of death. Well, these protesters are making me late. Let's try to pass legislation so
we can run them over and kill them. Step on my flag, I'll shoot you. And then you wonder
why some kid walks into a school and starts killing people. We are a violent country.
We always have been. Even these solutions. We sit there and people who propose this are
We want peace, we don't want guns, sure you do.
How do you think any of this is going to get enforced?
I can't understand what it's like to be black in the US.
I can't understand what it's like to have my cultural identity stripped away and
replaced with the knowledge that when my granddad was a kid,
people didn't think he was human.
Or that what is seen as my cuisine is because we
got good cooking trash, that's going to affect a people, a collective people on a
very deep level. You know, when you say get over it, we're a long way away from
that. We are a long way from getting over slavery. Yeah, the physical scars, they've
they've healed, they have. Cultural ones are still there because it's gone, was stripped
away, just vanished in the air. I really want you to think about how much of who you are
as a person comes from the old country. And then imagine what it would be like if you
didn't have that and if what it was replaced with wasn't love of the red
white and blue because the red white and blue didn't love you. So I got questions
a few actually from black people who want to start gun clubs for black people
and I told that story to remind the Second Amendment crowd that it's not the
It is not the same, you know, you say, well, they don't have to do that, it's their right.
It's our right.
If they do the same things we do, they'll end up dead.
Right now, they're trying to flex their rights, advocate for themselves and just get law enforcement
used to the idea that a black person can be armed, and that's not a reason to shoot them.
The intent of the Second Amendment, maybe not when it was written because it didn't
apply to them. But it's to arm minority groups. When it was written it was more about ideological
minorities. As we include more and more people in America's blanket of freedom, today it
is more important for a racial minority to be armed than just about anybody else. The
The second amendment is there to protect them.
The skin tone fundamentally change a person that much.
I don't understand the fear of the browning of America.
Don't understand it because it doesn't make any sense logically.
And then I don't understand it because, well, it's inevitable.
I don't know why you'd be afraid of something that is going to happen.
This isn't a it might happen in the future type of thing.
It's going to happen on a long enough timeline.
It's going to happen everywhere.
will become darker, dark will become lighter, eventually we're all gonna look
like Brazilians. I personally can't wait, I mean I'll be gone, but can you imagine
how much harder politicians are gonna have to work to keep us divided, keep us
kicking down. It's gonna be a lot harder to say, oh you're better than those
people when they look just like you. See, minority groups, by definition, they're a
minority. They can't swing an election. They don't have that institutional power.
They don't have the votes. They're a minority. So they can't do anything about
it. They got unaccountable men with guns running around. A lot of these police
chiefs aren't even elected. So, trying to figure out how to relate that that
distrust and that fear to white country folk and I figured it out. Three letters.
A, T, F. All of a sudden you got that distrust, you got that fear, got that
anger. You probably know somebody they did dirty. You might know somebody they
killed didn't need killing. Say you live a little bit further out west and ATF
doesn't do it for you. BLM got that distrust, got that fear, got that anger.
Why? Because they're unaccountable men with guns. Can't do anything about it. See
here's the thing we got to remember. They got a Ruby Ridge or a Lavoie Fennecom
happening every week. Every week there's some unjust killing and they can't do
anything about it.
Being a cop, not really that dangerous. Not a lot of deaths really. It's not even
in the top 10. Loggers, fishermen, roofers, delivery drivers all have more
dangerous jobs. But they don't know that. They've bought into their own propaganda.
So, you hear things like, well, I want to go home at the end of my shift.
Why don't you do that and stay there?
Let's say you roll up on a kid selling a dime bag.
He takes off running.
That's completely normal.
That is completely normal.
That's not a reason to shoot him.
One out of 10 people were killed while unarmed.
That's pretty high.
That is pretty high.
Now, I know there's some people saying, well, that's 10%.
That's not really that bad, is it?
Let's put in another perspective.
Let's say you order a pizza once a week, and five times a year, the guy brings you the
wrong order.
You're probably going to stop ordering from there.
You hold a pizza delivery driver more accountable for the accuracy of your order than you do
a cop for the accuracy of his bullets.
Every video I see of you guys in a shootout, you star ski and hutch it out into the middle
of the road popping off rounds.
don't hit the guy. I are 48 rounds, suspect hit three times. That's 45 rounds
that could have killed some kid, could have killed some single mother, could
have killed some elderly guy. If you're no longer a public servant, your job is
strictly to enforce the law. It means you're personally responsible on a moral
level for every law in the books. If you don't agree with it and you're willing
to enforce it. Well then you're just a hired goon. There are a lot of unjust
laws out there. When you run across somebody that is ideologically motivated
and they are breaking the law but the crime has no victim, keep rolling. We know
you want to go home at the end of your shift. Everybody does. That's
completely understandable. The unarmed people that were killed, they wanted to
go home to. You chose this profession. They probably didn't choose to interact
with you. Violence against police? It's not a dangerous job. There is no war on
cops. There should be no warrior cops. So some suicide bomber fills his truck or
his van full of explosives and drives into a barracks in the middle of the
night, kills a bunch of sleeping troops. What's the difference between that and a
guy who's sitting in Texas flying a drone over Yemen and blowing up a training
camp? Morally they're the same thing. Opening fire on a village for no reason
whatsoever just gives the insurgency propaganda, helps them recruit people to
kill his friends.
You have to keep a hard look for those who confuse legality and morality, because everything
the Taliban did, well that was legal under their laws.
Everything Saddam did was legal under their laws.
Everything the Islamic State did was legal under their laws.
Everything Nazi Germany did was legal under their laws.
Genocide of the natives, slavery, segregation.
legal under our laws you know it doesn't matter if you're in the
holler of some West Virginia hillside or you're in Kansas whatever you got more
in common with a black guy from the inner city then you're ever gonna have
in common with your representative up in DC please if we're gonna have any forward
movement in this country, what passes for the left and the right in this country, have
to start talking.
What you're going to find out is if you actually start talking to the right-wing voters, you're
going to find out that they have a lot of the same problems you have.
They just believe a different set of politicians is going to fix it.
The reality is you're both wrong.
If you make your community strong enough, it doesn't matter who gets elected, it doesn't
matter who's sitting in the Oval Office, it doesn't matter if you hate Trump or Obama,
whoever it is, they're in power.
You're going to be fine because your community can take care of itself.
Rule 303.
You have the means at hand.
You have the responsibility to act.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}