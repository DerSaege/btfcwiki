---
title: Let's talk about Ice T, hobbies, and power....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=l9IzxwTAFC8) |
| Published | 2019/12/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau starts by discussing ICE-T's tweet about a Napoleon quote and a flaming queue image, which led to controversy.
- Politics is described as a hobby that some are deeply steeped in, mentioning a far-right theory related to the Trump administration.
- Politics should not just be a hobby but a powerful force that requires action to convert beliefs into tangible change.
- Beau encourages moving from politics as a hobby to a calling, suggesting taking tangible actions once a month to make a real-world impact.
- He stresses the importance of channeling passion into action to see significant changes in society, especially in the year 2020.

### Quotes

- "Politics isn't a hobby. Politics is power."
- "We stop letting politics be a hobby. We start letting it be a calling."
- "Stick to what you're truly passionate about and work towards bettering that particular topic."

### Oneliner

Beau says: Politics isn't a hobby; it's power. Let's turn passion into action for real change in 2020.

### Audience

Activists, Community Members

### On-the-ground actions from transcript

- Organize a tangible action once a month to affect real change (suggested)
- Channel passion into making a difference in a chosen topic (implied)

### Whats missing in summary

The full transcript provides detailed insights on the shift needed from viewing politics as a hobby to recognizing it as a powerful force that requires action for tangible change. Viewing the full transcript can provide a deeper understanding of the examples and perspectives shared by Beau.

### Tags

#Politics #Activism #Passion #Action #Change


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about ICE-T.
That's a phrase I never thought I'd say.
We're going to talk about ICE-T, hobbies, and power,
because the three things converge today.
ICE-T, the musician, tweeted a meme.
It was a quote widely attributed to Napoleon,
something to the effect of never interrupt
opposition when they're destroying themselves or making mistakes. Something along those lines.
He tweeted it because he liked the quote.
The problem is the image in the background was a flaming queue.
Dun dun dun. See we all know what that means. Most of us, I would assume.
Because politics is a hobby for us.
This is a fringe theory. It is a fringe theory.
theory. He had no reason to know what it was and it was very entertaining to watch him
deal with the people who were attempting to drag him or claiming that he was spreading
it or whatever because he responded in typical iced tea fashion which I can't repeat it here
but it's pretty entertaining. And it was also really funny to me being familiar with his
music to watch people say that he needed to pay closer attention to politics.
I'm guessing you've never heard body count.
That's some pretty political music at times.
But see, the thing is, because it's a hobby, we become steeped in it and we're aware of
these fringe theories.
Now if you're not aware of it, it is a far right-wing theory that suggests somebody in
the Trump administration is dropping hints and clues as to what's going to
happen next and the name originates because of his cue clearance. By the way
guys, that's not a military thing. That's the Department of Energy. But aside
from that, it's basically Nostradamus for the internet age. There are little
statements made that could be interpreted in a whole bunch of
different ways and if you want to believe it, you can find the way to
to believe it. If you don't, you can find a way not to believe it. They're very vague
and very cryptic. We know about it because it's a hobby. Politics is a hobby. There's
nothing wrong with that in and of itself. There isn't. In fact, in many ways, it's
a good thing. It gets you interested. It gets you believing in something. But at the same
time, your beliefs are worthless unless they're converted into action, tangible action. And
And I can't be too judgmental about this, I do it myself all the time.
Get online, somebody says something, and you end up in a four hour conversation.
Been busy all day, got nothing done.
We should probably stop that.
Because politics isn't a hobby.
Politics is power.
It's about power.
And it doesn't matter where on the political spectrum you lie, that's what it's about.
If you are a far-right believer in Q, it's about consolidating that power and getting
it in the right hands, those benevolent dictators that would exist if you're somebody on the
other end wants to decentralize power and give it to everybody, break it up so there
is no monopoly on it.
It's a game.
It's a tug of war, but it's one of the few games where the points are measured in human
lives and the quality of those lives. So what I would suggest is that as we move
into 2020, we stop letting politics be a hobby. We start letting it be a calling
and set the goal of once a month doing something tangible, a real world, to
affect change. Whatever it is you believe in, whatever it is you're passionate
about. Because at the end of the day, if you are truly passionate about a topic
you probably know a lot about it. You're probably going to make better decisions
than somebody who's just going off of, you know, a little bit of what they've
heard. So stick to what you're truly passionate about and work towards
bettering that particular topic. If we all do that, we will see real change. 2020
is a year where we need to get out there. We need to seize the initiative. If you
We want progress now is the time we need to seize the initiative and no retreat.
Anyway it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}