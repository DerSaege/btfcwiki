---
title: Let's talk about the state and finding the money for it....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=veVGLwT5vxw) |
| Published | 2019/12/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the question of finding money for projects benefiting lower socioeconomic classes in the U.S.
- Points out the double standard in funding when it comes to projects that benefit wealthy classes vs. the general populace.
- Mentions Chuck DeVore's statement on progressive policies in California.
- Talks about the state's role in serving the people and the dangers of blending corporate interests with government.
- Explains the term fascism in the context of a state serving corporate interests.
- Notes the lack of funding for education, especially if opposed by wealthier classes.
- Gives an example of funding for a project benefiting the wealthy, the Slavery Abolition Act in the UK.
- Mentions the extensive loan taken out for reparations to slave owners, paid off in 2015.
- Emphasizes that policies benefiting the marginalized actually benefit society as a whole.
- Concludes by suggesting that failing to fund projects for the disadvantaged is equivalent to killing the state.

### Quotes

- "When you hear that, just assume it's something that rich people don't want to do."
- "Those social safety nets that keep the elderly, the disabled, and just the general poor, keep them alive. Well, those people, they are the state."
- "Failing to find the money for it is literally killing the state."

### Oneliner

Beau addresses the funding double standard, linking societal well-being to supporting the marginalized; failing to fund projects for the disadvantaged is akin to killing the state.

### Audience

Advocates for social justice

### On-the-ground actions from transcript

- Support and advocate for policies that benefit the lower socioeconomic classes (suggested)
- Educate others about the importance of funding social safety nets and infrastructure projects (suggested)

### Whats missing in summary

The full transcript provides historical context and examples to drive home the point about funding disparities and societal impacts.


## Transcript
Well, howdy there, Internet people, it's Beau again.
So today we're going to talk about finding the money.
It's a question that comes up any time somebody proposes
something that is going to benefit the lower socioeconomic
classes in this country.
Even just the general populace, infrastructure projects, the
Green New Deal, any form of social safety net.
Well, where are we going to find the money for that?
It's not a question that gets asked when it is going
benefit the wealthier classes or the corporate interests in this country.
Former California Assemblyman Chuck DeVore told Fox News that progressive left policies
were killing the state.
Killing the state of California.
In the United States, the state is supposed to serve the people.
That's the idea.
If you want a state that benefits corporate interests, that blends corporate and government
interests and everybody serves the state, there's a word for that.
It's called fascism.
That's literally the definition.
People advocate for it more and more because they don't seem to know what it is because
one of those things we can never find the money for is education.
If it's something that the wealthier classes oppose, well, those representatives in our
government that have accepted their money will find a way to resist it.
And that's what they'll say.
We don't have the money for it.
Where are we going to find the money?
It's really what that means.
When you hear that, just assume it's something that rich people don't want to do.
example of this and the idea that the government will always find the money for it if it's
something that rich people want to do is the Slavery Abolition Act 1833 in the United Kingdom.
It ended slavery throughout the empire and included twenty million pounds for reparations.
Twenty million pounds today, that doesn't sound like a lot.
Back then, that was closer to 18 billion pounds, about 5% of the GDP, about 40% of what the
Treasury took in in a year, and they found the money for it.
Found the money for it, they took out a loan.
They felt it was really important to do this, to include these reparations, so they took
out a loan, a loan that wasn't paid off until 2015 just a few years ago. I know
you're probably thinking I didn't know the UK did reparations it's because it
wasn't to the slaves that would benefit the lower socioeconomic classes it was
to the slave owners. They took out a loan that took almost 200 years to pay
off because when it benefits the wealthier people, finding the money for
it's never an issue. I would remind those people that say that these policies will
kill the state, that the homeless, those kids that need free lunch, those members of the
general populace that would like their children educated, those infrastructure projects, they
all benefit the little guy.
Those social safety nets that keep the elderly, the disabled, and just the general poor, keep
them alive. Well, those people, they are the state. And failing to find the money
for it is literally killing the state. Anyway, it's just a thought. Y'all have a
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}