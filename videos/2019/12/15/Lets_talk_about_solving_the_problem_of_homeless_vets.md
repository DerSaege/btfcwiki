---
title: Let's talk about solving the problem of homeless vets....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=OiJaAbW_ZQk) |
| Published | 2019/12/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the issue of homeless veterans and the common response of prioritizing their needs over other social safety net concerns.
- Mentions the need for facilities across states to house homeless vets, providing them with necessities like hygiene, food, and shelter.
- Points out that there are already facilities capable of housing homeless vets, with room for more people.
- Suggests honoring treaty obligations to free up space in facilities currently holding asylum seekers and migrants.
- Proposes putting individuals with criminal records or deemed dangerous in county jails, while utilizing current incarcerated individuals for assistance.
- Challenges viewers to take action by contacting their senators instead of just discussing the issue online.
- Emphasizes the importance of prioritizing the well-being of homeless vets over political agendas.
- Advocates for a comprehensive solution to homelessness rather than temporary fixes.
- Urges for support in reintegrating homeless individuals back into society through assistance with housing costs.
- Calls for a shift in focus towards loving homeless vets more than harboring negative sentiments towards certain groups.

### Quotes

- "All it takes is for those people who say they care about homeless vets to love homeless vets more than they hate brown people."
- "The solution is there."
- "The facilities are already there."
- "What are we waiting for?"
- "It's just a thought."

### Oneliner

Beau addresses homelessness among veterans, proposing a solution that involves utilizing existing facilities, honoring treaty obligations, and prioritizing genuine care over political biases.

### Audience

Advocates, Activists, Supporters

### On-the-ground actions from transcript

- Call your senator to advocate for better facilities to support homeless veterans (suggested)
- Support efforts to reintegrate homeless individuals back into society by assisting with housing costs (implied)

### Whats missing in summary

The detailed statistics and specific examples provided by Beau in the full transcript are missing from this summary. Watching the full video can provide a deeper understanding of the issue and proposed solutions.

### Tags

#Homelessness #Veterans #SocialSafetyNet #Advocacy #CommunitySupport


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight, we're gonna talk about a problem
I've been hearing about for 20 years and it gets brought up
anytime somebody wants to bring up anything
about any form of social safety net, people are like,
hey, well, we gotta deal with our homeless vets first.
And since those people who pretend to care about them
are obviously not going to propose a solution,
I've got one of my own, scope of the problem.
On the average night, there are 40,056 homeless vets.
All right?
So we need facilities in every state,
because they're not all geographically-centered.
We need them spread out everywhere that can house them,
provide hygiene, food, shelter, everything they need,
recreation.
Okay, so, that network of facilities already exists and as of last year, it was capable
of housing 40,520 people.
So more than.
It's there.
It is there.
So all we have to do is honor our treaty obligations, let the asylum seekers and the migrants out,
show up for their court date, then all of those facilities are open.
They're all there.
There's room for everybody.
Now if they actually have a criminal record or are a danger to society, you put them in
a county jail where they belong.
And I know people are like, well what about the guards?
It's the US.
We now have more people incarcerated than were incarcerated in Stalin's gulags.
They're not going to go without work.
They could get retrained to help the vets because it's normally a lot of those people
saying that we need to help them.
The solution is there.
It's there.
So what are we waiting for?
All it takes is for those people who are all over the comments section screaming about
homeless vets to instead of being in YouTube video comment sections or on Twitter or on
Facebook or wherever, instead of talking to random people on the internet, call your senator.
involved. Now, I know that's unlikely because just like the Constitution, most
of them only care about homeless vets when it suits their needs, but that's all
it's going to take. The facilities are already there and people say, well you
know they're better off in those jails and those detention centers
than they are in their home country. Wouldn't that apply to people on the
streets? Wouldn't that apply to the homeless? It can even help them with what we're paying
since we're using private facilities, a lot of them. We could even help reintegrate them,
help them pay first and last month's rent, get them off the streets, actually solve the
problem instead of a band-aid. The solution is there. All it takes is for those people
who say they care about homeless vets to love homeless vets more than they hate brown people.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}