---
title: Let's talk about two questions and civil rights....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=28caQlP6WQI) |
| Published | 2019/12/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the question of why white people who seem to care or understand are often Irish or of Irish ancestry.
- Providing a timeline of the Civil Rights Movement in the 1960s, detailing protests, police response, bombings, and government infiltration.
- Drawing parallels between the Black Panthers in the U.S. and the Irish Republican Army in Europe during the same timeline.
- Noting the camaraderie between Irish and black communities in the American South due to shared experiences of oppression and intermarriage.
- Mentioning that Irish-Americans in the South tend to have strong family bonds and pride in their heritage.
- Questioning when personal responsibility should kick in, considering ongoing systemic issues like disproportionate sentencing and redlining.
- Stating that until institutional and systemic issues are eradicated, expecting individuals to solely overcome challenges is unreasonable.
- Emphasizing the need to first stop systemic issues before taking steps like reparations to make amends.
- Criticizing proposed reparations figures as initially insulting but acknowledging recent talks of higher figures being more appropriate.
- Stressing the importance of awareness that the fight against systemic issues is ongoing and subtle, requiring continued efforts to overcome.

### Quotes
- "The problem is in the US, it's not over."
- "We can't say, 'Pull yourself up by your bootstraps' until those institutional issues are gone."
- "The primary thing is we've got to make it stop first."

### Oneliner
Exploring why white people who care often have Irish ancestry, examining Civil Rights Movement parallels, and stressing the need to end systemic issues before focusing on reparations.

### Audience
Activists, Community Members

### On-the-ground actions from transcript
- Advocate for ending systemic issues (implied)
- Support reparations initiatives (implied)
- Stay informed and engaged in ongoing fights against systemic racism (implied)

### Whats missing in summary
The full transcript delves deeper into the historical context of racial dynamics and the importance of acknowledging ongoing systemic issues to achieve true equality. 

### Tags
#CivilRights #SystemicRacism #Reparations #IrishHeritage #CommunityPolicing


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're going to talk about a question I got more than a year ago and a question
I got last night because, well, one kind of grows right out of the other.
More than a year ago, a black guy asked me, why is it that any time there's a white person
who even vaguely seems to care or understand, they turn out to be Irish or of Irish ancestry.
And oddly enough, that very specific question kind of came up again. Before we get into
it, we have to go through a timeline of the Civil Rights Movement. It's important once
we get to the answer to understand exactly what went down.
Okay so it's the 1960s.
Oppressed, want civil rights, feel like you should already have civil rights, I mean there
was a civil war fought over this.
But there's issues.
So protests, marches, police respond the way police respond.
gas, batons, water cannons, bullets. It escalates. There are bombings, assassinations. Government
agencies infiltrate movements to destabilize them and take out the leaders. Leaders get
arrested, end up behind bars. On the upside, they write some of their best work behind
bars. While all of this is going on, there's a group emerging. They're left-leaning. They're
known for wearing black berets and aviator sunglasses, and they start to run those neighborhoods.
If you're in the U.S., you're picturing the Black Panthers.
If you're one of our European viewers, you're probably picturing the Irish Republican Army.
It's the same timeline.
Exact same timeline.
The causes are very, very different.
And the IRA got a whole lot more violent.
But the causes or the timeline is the same.
That was the source of that initial camaraderie, I think.
Now in the South, and I believe this is probably true in the Southern United States, I don't
think if this guy was from Boston he would be saying this, but in the Southern U.S.,
Yeah, I would agree.
Irish people are probably the least racist.
But there is a reason for that.
It is very hard to find an Irish-American in the South
that is not also native, because when the Irish came to the US,
they weren't really looked upon very nicely.
When they came down here, they intermarried.
Broke down those racial barriers.
Broke down those racial barriers.
Created a little bit more understanding.
And then there's two kind of humorous reasons
that I think people see this.
One is simply there's a lot of them.
I mean, it's a cliche.
It's a stereotype.
But Irish-Americans have a lot of kids.
That, in my experience, has been very, very true.
The other thing is that Irish-Americans
are very, very quick to tell you that they're Irish.
So it may just be that the white people that seem to care,
when you find out their background,
it's Irish, because Irish people are very, very proud
that ancestry. So that's the answer to that question. Now the question that grew
out of that has to do with the outcomes. The timelines in the 60s, 70s, very very
similar, very similar. The outcomes extremely different and the
question is, when does personal responsibility start to come in? You know, personal responsibility
versus societal pressure. I would say one generation after it ends. The problem is in
the US, it's not over. Yeah, we've got the Civil Rights Act, but anytime there's a study,
Who is disproportionately sentenced?
Who is disproportionately stopped randomly?
Who is disproportionately stopped and frisked?
Yeah we've got the Fair Housing Act, but redlining still happens.
It's not gone.
Those institutional issues, those systemic issues, they're still there.
Until those are gone, we can't say, oh, pull yourself up by your bootstraps.
It's all on you, because it might not be.
Yeah, there are cases where somebody's personal actions can help eliminate some of that, can,
might, but that doesn't mean they won't become a victim of the systemic issues in this country.
there. They're still there. They can try to dodge it, but that doesn't mean they
won't get hit. So the first step would be ending it. That's the first step,
stopping it. I think a wise second step would be making amends of some kind. Um, and that
topic's coming up. That topic is coming up by actual viable candidates for the White
House. When we talked about reparations on this channel last time, the largest number
thrown out by any politician that had any kind of clout was a hundred billion. We broke
it down, we did the math. Man, that's insulting. That is actually a really
insulting number when you actually do the math on it. That number now,
presidential candidates are talking about 500 billion. That's getting closer
to what I think would be in the ballpark. At least it's not insulting anymore.
But, again, I'm not the one you have to ask.
I do think that that's an important step to be taken once it ends, but the primary thing
is we've got to make it stop first.
And being aware that it's not over, that that fight is still going on, is really important
to eventually winning it.
The problem is that from the outside looking in, it's gotten so subtle that we kind of
shrug it off, and we can't do that.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}