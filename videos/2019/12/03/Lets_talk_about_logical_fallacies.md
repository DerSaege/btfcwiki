---
title: Let's talk about logical fallacies....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=M2WKnNkrCdI) |
| Published | 2019/12/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Tells a true story about a trip where several planes had issues which leads to the point of needing to pay aircraft maintenance crews more.
- Mentions misleading vividness, where a detailed story doesn't actually support the claim being made.
- Explains logical fallacies including appealed hypocrisy, straw man, genetic fallacy, appeal to belief, appeal to tradition, appeal to authority, ad hominem, argument from ignorance, cherry-picking data, slippery slope, Nirvana Fallacy, moving the goalpost, false dilemma, fallacy of composition, divine fallacy, argument to moderation, and argument from fallacy.
- Provides examples for each logical fallacy and explains how they can lead to flawed arguments.
- Emphasizes the importance of understanding logical fallacies especially in the context of debates and arguments.
- Ends by suggesting that the information on logical fallacies will be valuable in the coming year.

### Quotes

- "We really need to pay our aircraft maintenance crews more."
- "Just because one of these fallacies was used does not mean the argument is wrong."
- "The middle ground is not always where you want to end up."
- "The world is a bad place sometimes."
- "It's just a thought y'all have a good Good night."

### Oneliner

Beau gives a crash course on logical fallacies through real-life stories, urging for better aircraft maintenance and critical thinking in debates.

### Audience

Debaters and critical thinkers.

### On-the-ground actions from transcript

- Learn about logical fallacies and how to identify them in arguments (suggested).
- Educate others on common logical fallacies and how they can weaken arguments (suggested).

### Whats missing in summary

In-depth examples and nuances of each logical fallacy discussed.

### Tags

#LogicalFallacies #Debates #CriticalThinking #AircraftMaintenance #Education


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're gonna talk about logical fallacies.
It's, we wanted to do a series on this for a while,
and somebody brought it up, so we're gonna go ahead
and kind of do a crash course on it tonight.
But before we get into it, I'm gonna tell you a story.
True story.
I had to go on this trip, did not want to go on it,
had a bad feeling about it,
and required a couple of flights to get to where I was going.
First flight went okay until we landed, then the tire separated from the plane on landing.
Now come to find out that's actually safe, as long as it's just one tire.
But it certainly doesn't feel safe when it's happening.
Get off the plane, change planes, get on the new plane.
And this one's a jet, and as we are taxiing down the runway, smoke starts kind of pouring
out of the plane.
And the guy sitting next to me is like, man, I hope he doesn't take off.
I'm like, man, I didn't even realize that was an option.
And so they pull us back around and we end up switching planes.
The next plane we get on is propeller driven and is so old, there's ashtrays in the plane.
And I'm not talking about the one mandated in the bathroom.
I'm talking about in the armrests of the chairs.
And the point of all of this is that we really need to pay our aircraft maintenance crews
more.
And that is misleading vividness.
It's something we see a lot of.
The actual story that has all of the details and actually gets your attention has nothing
to do with the claim.
In fact, there is nothing in that story to suggest that any of those planes were in the
United States. We see this a lot when people are trying to villainize a certain
group. Well you know this person from demographic X did this horrible thing
and they tell it in detail and then they'll use another logical fallacy to
suggest that well they're all like that which is fallacy of composition but
But we'll get there.
Anyway, so one we see a lot of, especially on social media,
is appealed hypocrisy.
And this has a bunch of different names,
but appealed hypocrisy is the easiest to pronounce.
So this is where the proponent, the person that's
putting forth an argument, fails to live up to the standard
or is perceived to fail to live up to their own ideas.
Well, you know those climate change activists
drove there in an SUV, and that's fine,
but that does not actually address their argument
about climate change.
Therefore, it's a logical fallacy.
Another one is a straw man.
This is where somebody takes an argument
and rephrases it to weaken it, and then
attacks the weakened argument.
A good sign that you're about to see a straw man
is when somebody says, so what you're saying is,
mm, no, no, that's not what I said.
So sometimes you will see people deconstruct an argument,
and that's good.
Take it piece by piece.
But a lot of times when people do that,
they only attack the weakest piece, and that is a straw man.
The genetic fallacy.
Assuming that something is bad,
because it came from something bad, and we see this a lot.
A good example of that would be the military developed
the internet, therefore it's a weapon of war.
No, it's not what it means.
The final product that's relevant today
may not have anything to do with where it came from.
Appeal to belief or appeal to majority, this is basically bandwagon.
Something is true because everybody believes it's true, like the world being flat.
Appeal to tradition, this is one I hate and we see a lot of this.
You support a conclusion because it's always been done that way and that is the worst reason
in the world to do something a certain way is because it's always been done that way.
Appeal to authority, you find this a lot in the back the blue crowd.
The argument is assumed to be true because a person in authority said it.
Now this isn't the same as deferring to a subject matter expert, this is somebody that
has authority and this is normally somebody that has authority and force. So
an agent of government, a higher power, that is an appeal to authority. Ad hominem,
this is when you discredit the person rather than the idea. Example, Bo has an
ugly beard and he's wrong. That's not actually an ad hoc. That's just an
insult. Insults are okay when you're arguing. Bo is wrong because he has an
ugly beard. That's an ad hoc. Argument from ignorance. This is assuming that
something is true because it can't be proven false and this is a really
dangerous one, because once people start falling for this, this is when you see
them start to embrace a lot of fringe theories. Donald Trump is really a giant
lizard, but you won't be able to prove that because, you know, there's the secret
society. Once you go down that road, it's very hard to come back. Cherry-picking
data. That's pretty obvious. That's when politicians normally will select specific
data that has already been determined to give them the outcome they want. And we
see a lot of this in a lot of debates. You can have the same study and both
sides will try to use it because they're only using a piece of it. Slippery slope.
Now, slippery slope is not the same as a chain of events. A slippery slope is, well,
if we let these people get married, well, then people will marry dogs. It's normally
absurd and there is no causal relationship between step one and step
two. That's different from, if we pull funding from this program, people will go
hungry. There's a chain of events there. The Nirvana Fallacy, and this is one that
I cannot beat out of myself. Solutions to problems have to be perfect. If they're
not perfect, you dismiss them. You're not supposed to do that. I have a problem
with that. It's still something I do. Moving the goalpost. You see this a lot
on on in the comment section will prove that Trump spent a hundred million
dollars on golf well here's a source prove Obama didn't spend more as soon as
the requirement for evidence starts being increased just walk away at that
point you know you're dealing with somebody that doesn't actually care
about information. False dichotomy, false dilemma. Two choices are presented and
they're presented as if they're the only two choices. You either like chicken or
steak. There's no vegans in this world. We see this a lot from politicians
because they try to simplify things and they will paint one option as horrible
and the option they favor as it's really your only choice. Now there's always
other choices. Fallacy of composition. It's where you assume that something is
true for the whole thing because it's true for a part of it. You normally find
this with people that are trying to disparage a demographic. You know this
person of demographic X did this horrible thing therefore they're all
like that. It's not true. Divine fallacy. This is when something incredible happens.
Something shocking. Something just amazing. The desire to attribute it to something supernatural
or a phenomenon that is beyond our control. Anything that isn't present in the real world.
that can be proven. There's a lot of things that happen where people just don't want to
accept that the world is a bad place sometimes, so they attribute it to a lot of French theories,
and that is divine fallacy. Argument to moderation, and that is where you assume that compromise
is correct. It's not always correct. You can have two opposing sides and one of them is
going to be right. The middle ground is not always where you want to end up and this is
especially true with arguments recently about where we want to go as a nation. And then
Then on top of all of these, you have argument from fallacy, also known as the fallacy fallacy.
Just because one of these fallacies was used does not mean the argument is wrong.
Somebody can cherry pick data and appeal to tradition and use misleading vividness to
tell you that Windows is the most popular operating system.
because they used fallacious arguments doesn't mean that the argument is wrong.
Okay so I know this is a little dry but I have a feeling we're going to need this
information in the coming year. Anyway it's just a thought y'all have a good
Good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}