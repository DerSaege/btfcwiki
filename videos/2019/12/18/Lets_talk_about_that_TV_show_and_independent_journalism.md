---
title: Let's talk about that TV show and independent journalism....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=VsRR4Bwx1lE) |
| Published | 2019/12/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Shared a post on social media and received many questions about it.
- Approached by a production company in LA to create a reality TV show about independent journalists.
- Flew to LA and covered events there and in Vegas with a diverse crew.
- Mentioned crew members like Carrie Wedler, Ford, and Derek Brose.
- Discussed the possibility of the reality TV show happening in the future.
- Stressed the importance of independent journalists disrupting corporate news narratives.
- Urged for the need of such projects in the current polarized environment.
- Noted that many people do not follow independent journalists, leading to a lack of nuance in debates.
- Shared details about his nickname, accent flexibility, and beard trimming for TV.
- Encouraged learning different accents for better interaction with people from various regions.
- Expressed comfort in being himself on his channel rather than using a non-regional accent.
- Recommended watching a specific clip involving Derek getting shot for a glimpse into independent journalism.

### Quotes

- "It's something this country desperately needs."
- "I've seen that clip a hundred times and I laugh every time because that is independent journalism in a nutshell right there."

### Oneliner

Beau stresses the importance of independent journalists disrupting narratives and the need for projects like the reality TV show he was approached for, urging people to follow independent voices. 

### Audience

Journalism enthusiasts, media consumers

### On-the-ground actions from transcript

- Support independent journalism by following and sharing content (suggested)
- Encourage nuance in debates by exposing oneself to various independent journalistic viewpoints (implied)

### Whats missing in summary

The full transcript provides insights into the challenges faced by independent journalists and the necessity of disrupting mainstream news narratives for a more nuanced understanding of current events. 

### Tags

#IndependentJournalism #RealityTV #Narratives #MediaBias #Nuance


## Transcript
Well, howdy there, internet people, it's Beau again.
So I shared something on my social media,
some of my social media, and I got
a bunch of questions about it.
And I have to answer them.
About a year and a half, two years ago, myself
and a bunch of other independent journalists
were approached by a production company out of LA
to create a reality TV show about independent journalists.
The idea was to take a group of us
who have varying viewpoints and different tactics as far as how we do our job, stick
us together, and then follow us around with camera crews like the TV show Cops.
So they flew us out to LA and we covered some events in LA and in Vegas.
The crew is one of the reasons I did it.
It included Carrie Wedler, who you've seen on this channel.
She does the compilation videos on this channel, Ford, who you have seen on this channel, and
you have seen his footage, I promise you.
If you live in Houston, you probably know the name Derek Brose from the recent mayoral
race.
Fun fact, he's an independent journalist.
You watch the clip that I'll link below, you'll get to see him with long flowing hair
like Fabio.
Nick Burnaby, Robert Caputo, it was a really cool crew and had a whole lot of fun doing
it.
The most common question that comes up when people see it now is, is this still a possibility?
Is this something that could happen?
Yeah, it is.
I actually talked to the production company yesterday or the day before yesterday.
It is something that could happen.
The reason that it is a hard sell for them is the reason it's so important that if not
this project, one similar to it, happens.
Most major media companies have a news component of their own, profiling independent journalists
who have a really nasty habit of destroying corporate news narratives.
That's a hard sell.
That's hard to convince any normal TV station to, well, here, put this show on that's going
to just completely disrupt your news, your news component.
But as polarized as the debate is in the United States, a group of people out there shattering
narratives and breaking narratives is more important than ever.
The project is still something that I really support and I really hope that if not this
one, one similar to it takes off.
It's something this country desperately needs.
Most people watching this, you guys probably follow independent journalists on YouTube
or Twitter or Facebook, wherever.
There's a whole bunch of people who don't.
So they never get those narratives shattered.
They never get to see them broke.
And that leads to a lack of nuance in all of the debates and discussions in this country.
Now to some of the less important questions, yeah, Bo's a nickname, yes, I did quote church
up to be on TV, I trim my beard, and yeah, I have a non-regional accent that I can switch
into pretty much any point in time so I don't sound like an extra from Justified.
It's very useful when you're doing interviews with people who might think that people from
south are stupid, so you don't want to let on the accent. People have asked how you do it.
People from the south are like, wow, I wish I could do that. It's just like a foreign language.
You can learn it. You just have to practice it. That's all there is to it. And if you have a
job that requires you to interact with people from other parts of the country,
history, it's turned out really useful for me, so I would recommend doing it.
It's sad that you would need to do that, but at the same time, it's the world we live in.
And people have asked that if this show takes off, would I still use the non-regional accent
like I did in The Sizzle?
Probably not, mainly because of this channel.
I've become a lot more comfortable with just me, rather than trying to hide it, so I guess
thank you for that. If you do watch the clip, I highly recommend you stick around
to the part where Derek gets shot. I've seen that clip a hundred times and I laugh
every time because that is independent journalism in a nutshell right there.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}