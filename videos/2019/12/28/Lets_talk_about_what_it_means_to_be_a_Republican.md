---
title: Let's talk about what it means to be a Republican....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=5bUwzgIt4yw) |
| Published | 2019/12/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the historical context of the Republican party and what it used to mean.
- Mentions the transition from Roosevelt to Eisenhower, showcasing the values and policies of these presidents.
- Talks about how Eisenhower continued and expanded on the New Deal policies initiated by Truman.
- Describes Eisenhower's actions in office, including strengthening Social Security, increasing the minimum wage, and initiating major public works projects like the Interstate.
- Emphasizes Eisenhower's understanding and empathy towards those in need, reflected in policies like the Department of Health, Education, and Welfare.
- Notes Eisenhower's support for civil rights, mentioning his involvement in desegregation efforts like the Little Rock Nine.
- Acknowledges Eisenhower's faults, such as authorizing CIA activities against communism.
- Points out Eisenhower's warning about the Military Industrial Complex in his farewell address.
- Reminds the audience of the contrast between Eisenhower's policies and the current stance of the Republican party.
- Encourages reflecting on history and understanding the evolution of political ideologies.

### Quotes

- "It's also worth noting that he used his farewell address to warn the United States about something called the Military Industrial Complex."
- "But it meant that you hate all of his policies."
- "At this time, Republicans still pretended, at least, to care about those people who voted for them."
- "That's the reality of it."
- "He was a great Republican president in history. Just remember, it's history."

### Oneliner

Exploring the historical Republican values through Eisenhower's presidency, contrasting past policies with the present party stance.

### Audience

History enthusiasts, political analysts.

### On-the-ground actions from transcript

- Study and understand historical political ideologies (implied).
- Analyze and compare past and present policies of political parties (implied).

### Whats missing in summary

Deeper insights into Eisenhower's presidency and the implications of his policies on contemporary politics.

### Tags

#Republican #Eisenhower #History #PoliticalIdeologies #PresidentialPolicies


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we're going to talk about what it meant to be a
Republican, what it used to mean.
See, I tweeted out that older video.
Let's talk about that new liberal deal.
Now, to avoid giving too many spoilers, I'm just going to
say that a few people pointed out that after Roosevelt, the
greatest generation, they voted in a Republican, a guy named
Eisenhower, in case you haven't heard.
First, I'd like to point out that between Roosevelt and
Eisenhower, there was a guy named Truman.
But Truman was kind of an extension of FDR, so fair
enough, but here's a fun little historical fact.
Eisenhower was pretty much an extension of FDR.
In fact, Truman, probably the reason he's so forgettable,
His numbers were pretty, pretty awful.
It was a surprise that he actually won his campaign.
And the people that truly supported the New Deal, those
Roosevelt insiders, well, they were worried.
They were worried that Truman just wasn't up to the task.
So they decided to reach out and get a different candidate,
steal the nomination from it.
You know who they reached out to?
Eisenhower.
Yeah, they reached out to Eisenhower because, well, Eisenhower was a pretty big supporter
of it.
That's what it used to mean to be a Republican.
When Eisenhower finally got in office, he continued almost all of the new and fair deal,
fair deal being Truman's.
He strengthened Social Security.
He increased the minimum wage.
He initiated the largest public works project in history, even bigger than anything in the
New Deal.
You might have heard of it.
It created 41,000 miles of road.
It's called the Interstate.
You know him being a general, when it came time for him to leave his mark on Washington
and the bureaucracy there, it was time for him to create a new department.
You know he created some military branch or something, right?
Now, he created something called the Department of Health, Education, and Welfare, a Republican,
because as part of the greatest generation, he understood what it was like to see people
suffer.
They didn't do that.
They banded together.
They didn't look at the poor and the disabled and say, oh, well, they must have done something
to deserve that.
They didn't live their life right.
They could just pick themselves up by their bootstraps, work harder, no.
I wanted to give people a hand up.
As far as civil rights go, there was a little bit of tension at the time.
Tension could be personified in a group called the Little Rock Nine.
They were trying to get into a school, recently desegregated, played out a whole lot like
that scene from Forrest Gump.
He had to send federal troops down to make sure everything went okay.
That person was Eisenhower.
He signed two other pieces of civil rights legislation that I know of.
There may be more.
I can only think of two.
You know, him being a general, he was obviously one of those guys that wanted
a yellow ribbon on your bumper sticker, I guess back then, tied around your tree.
The whole time, because that's how you keep people motivated, right?
No.
In fact, he ended the Korean War as soon as possible, and during his entire tenure, he
only deployed troops into the combat setting once in 1958 to Lebanon, although he did authorize
airstrikes to save some French troopers at a place called Dien Bien Phu.
That isn't to say he's without fault.
He did authorize the CIA to do some pretty messed up stuff in the name of combating communism.
But, overall, when you look back, yeah, he's a pretty great Republican president and
Republicans can lay claim to him, but they must acknowledge that everything that
made him great, they now stand against.
That's the reality of it.
When people talk about the party switching, it was right after this.
It was right after this.
At this time, Republicans still pretended, at least, to care about those people who voted
for them, rather than just care about the rich, and convince all of those poor people
that it was somebody else's fault.
It wasn't the people making the decisions, it's the brown person, the poor person, the
person on welfare, that's who it is.
That's the reason you're having a bad time.
That was an Eisenhower's thing.
So yeah, lay claim to him.
But it meant that you hate all of his policies.
It's also worth noting that he used his farewell address to warn the United States about something
called the Military Industrial Complex.
See he was worried that the armaments industry insiders there in DC, that they had too much
influence, and that, well, we just end up with a larger and larger military, more and
more defense spending, perpetual wars.
He was right.
He tried to warn people about this, and it was his farewell address that he did it.
The speech was two years in the making, because he understood that the idea behind the United
states is that the state serves the people, not the people serving the state.
So yeah, he was a great Republican president in history.
Just remember, it's history.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}