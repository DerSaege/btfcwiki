---
title: Let's talk with Greta Thunberg....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-NBbsDmxNjQ) |
| Published | 2019/12/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Welcomes Greta Thunberg as a special guest, prepared for a confrontational interview due to her controversial figure.
- Questions Greta's lack of life experience to advocate for climate change and mocks her inability to work basic technology.
- Mistakes a Halloween decoration for Greta and continues the playful banter about generational differences in technology use.
- Applauds Greta for facing criticism from powerful figures like Putin and Trump, encouraging her not to chill out but to keep advocating and learning.
- Urges Greta to watch documentaries and be better than older generations who didn't take action on climate change.

Greta Thunberg says:
- Responds with silence to Beau's initial mocking about technology, prompting him to realize his mistake.
- Listens as Beau acknowledges the unjust bullying she faces online and praises her for standing strong against powerful critics.

### Quotes
- "I could have swore they said she's come by."
- "You're in this problem right now because a lot of us, that's what we did. We didn't do anything."
- "You keep watching documentaries. You be better than us."

### Oneliner
Beau confronts Greta Thunberg on her advocacy, mocking her tech skills, but ends up praising her resilience and urging continued activism and learning.

### Audience
Climate activists, youth advocates.

### On-the-ground actions from transcript
- Watch documentaries on climate change and take action to be better advocates (implied).

### Whats missing in summary
The full transcript provides a playful yet insightful look at the generational differences in activism and the importance of continuous advocacy for climate change.

### Tags
#ClimateChange #Activism #GenerationalDifferences #YouthAdvocacy #Documentaries


## Transcript
Well howdy there internet people, it's Beau again.
Tonight we have a very very special guest with us here.
We have Time's Person of the Year, the young activist upstart who's been running all over
the world warning us of the dangers of climate change.
We have Greta Thunberg here in the studio with us.
Now normally I don't do adversarial interviews but given that she is such a controversial
figure, tonight's conversation is going to be a little bit more confrontational and we're going
to start off with one of those tough questions. Greta, there's a whole lot of people who would
say that you really just don't have the life experience to be running around the world telling
your elders how to live. What do you have to say to that? Nothing. Cat got your tongue. It's because
This guy's got millennial kryptonite over here.
Gen Z.
Gen Z, millennial, whatever.
Why don't you just chill out?
Go watch an old-fashioned movie or something.
Got millennial kryptonite over here.
Ever seen one of these before?
Bet you don't even know how it works.
Can't work a can opener telling us how to run the world, huh?
Go on, take it.
No?
Just gonna sit there and stare at me.
Okay.
What about this?
Ever seen one of these?
You wring the ribbon on this,
you know how it works, hit the buttons,
little letters pop up on paper instead of your tablet.
Change film in this camera.
Can you do that?
My question to you is how are we supposed to trust
your opinion on how younger generations feel
when you can't even work basic technology?
Grandpa, that's not Greta.
For the last time,
That is a left over Halloween decoration.
Well that's embarrassing.
I could have swore they said she's come by.
I don't have my glasses.
Without my glasses I really can't change the film in this.
Can you do that for me?
Why don't you just use your phone?
I can't get past the little connect-the-dot watch.
Okay boomer.
You've got to, you have amassed quite a collection of detractors, heads of state, grown men bullying
a teenager on the internet.
It's fantastic.
They include Vladimir Putin, President Donald Trump, that fascist from Brazil.
You know, it's sad that you're judged by your enemies as much as your friends.
I would say you're in pretty good standing right now.
So as an older millennial, I would suggest you don't chill out.
We're in this problem right now because a lot of us, that's what we did.
We didn't do anything.
We just chilled out.
So what I would suggest is not that you go watch an old-fashioned movie.
You keep watching documentaries.
You be better than us.
Well, be best.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}