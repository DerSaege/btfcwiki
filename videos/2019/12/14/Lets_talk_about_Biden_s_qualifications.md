---
title: Let's talk about Biden's qualifications....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=iFNr_LR164k) |
| Published | 2019/12/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing Hunter Biden's qualifications and the repeated claims of his lack of qualifications for board positions.
- Mentioning George Bush's appointment of Hunter Biden to a board of directors and confirmation by the Senate.
- Noting Hunter Biden's past role on the Amtrak board of directors and his qualifications, including his education and work experience.
- Pointing out the false narrative perpetuated by some Republicans regarding Hunter Biden's qualifications.
- Exploring the idea of different standards being applied to Ukraine compared to the US government.
- Suggesting that the repeated falsehoods about Hunter Biden are believed by a base that does not fact-check.
- Contemplating whether there is a double standard in scrutinizing appointments based on political affiliations.
- Acknowledging the potential benefit of President Trump's administration in exposing lies and corruption in government.

### Quotes

- "He has the qualifications. They're right there."
- "At the end of the day, Trump in some ways has been a benefit because he's really laid bare the out-and-out lies and corruption in our government."
- "Maybe this type of corruption is just accepted here and the idea is that yeah, George Bush was trying to curry favor with Biden and you know, here it's okay, but over there, well, we have something different."

### Oneliner

Addressing the false claims around Hunter Biden's qualifications, Beau exposes the double standards and political narratives at play.

### Audience

Fact-checkers, political observers

### On-the-ground actions from transcript

- Fact-check repeated claims and correct misinformation (implied)

### Whats missing in summary

Full context and depth of analysis.

### Tags

#HunterBiden #Qualifications #PoliticalNarratives #DoubleStandards #Corruption


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're going to talk about Hunter Biden's qualifications, the younger Biden.
We're going to talk about him, talk about his qualifications because it keeps coming up.
The idea is that he is just completely unqualified to sit on a board of directors and therefore
the only reason anybody would appoint him would be to curry political favor with his daddy.
And therefore, I mean, we got to lock them up, I guess.
That's the cool chant right now.
OK, so when are we arresting George Bush?
Because George Bush appointed him to a board of directors,
and it was confirmed by the Senate.
Hunter Biden sat on the Amtrak board of directors
for years, four years.
I guess that was to curry political favor, maybe.
When this gets brought up, Republican plenents will say something along the lines of,
he got that job because they said that he rode on Amtrak trains a lot.
And that's true, that's cute.
It was said in the confirmation,
Hunter spent a lot of time on Amtrak trains.
That's the quote.
It's really there, Senator Carpenter.
However, that little bit immediately followed.
Hunter will be an excellent addition to the Amtrak board.
He is a graduate of Georgetown and Yale.
He has served as Senior Vice President at MB&A America
and as Executive Director of E-Commerce Policy Coordination
at the United States Department of Commerce.
Five years ago, and it goes on.
So I guess the idea is that we're holding Ukraine
to a higher standard.
Republican president appointed this man to a board of directors and it was
confirmed by the Senate. So this Ukrainian energy company needs to have
more stringent requirements for its board than the US government. Cool. We
can roll with that I guess. Makes sense. Why are we talking about it? Because a
whole bunch of current Republicans repeat it and repeat it and repeat it
it and therefore their base who does not fact-check believes it. It's not true, it's not true.
He has the qualifications. They're right there. In addition to the qualifications he had to
get him on the Amtrak board, the fact that he sat on the Amtrak board would be yet another
qualification for him to sit on the board of the energy company. Maybe they are holding
Ukraine to a higher standard.
Maybe this type of corruption is just accepted here and the idea is that yeah, George Bush
was trying to curry favor with Biden and you know, here it's okay, but over there, well,
we have something different.
We're going to hold them to a higher standard or maybe he is qualified and this is all just
It's made up because they know their base will believe their lies.
At the end of the day, Trump in some ways has been a benefit because he's really laid
bare the out-and-out lies and corruption in our government.
Anyway, it's just a thought.
Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}