---
title: Let's talk about how Barbie can change the world....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=7KnV-0L6_Ew) |
| Published | 2019/12/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains how Barbie has evolved to represent diverse body types, skin tones, and career choices.
- Recounts a heartwarming encounter of a little girl searching for a marine biologist Barbie for her sister.
- Emphasizes the positive impact of diverse representation in dolls on shaping young minds.
- Points out the significance of a white girl playing with a black Barbie in altering perceptions on race relations.
- Expresses optimism that despite current bigotry, positive change will prevail through such representations.

### Quotes

- "Barbie is woke now."
- "We win."
- "That is going to alter her mindset about race relations in general."
- "Even if it is an act, if the results are real, does it matter that it was an act?"
- "When brands do this, they are probably doing it for all the wrong reasons. But the results will end up being right."

### Oneliner

Beau explains how Barbie's evolution towards diversity can positively impact young minds and perceptions on race relations, showcasing the power of representation in shaping attitudes.

### Audience

Parents, educators, activists

### On-the-ground actions from transcript

- Buy diverse dolls for children, exemplified
- Encourage diverse career aspirations in children, exemplified

### Whats missing in summary

The full transcript captures a heartwarming story about the impact of diverse representation in dolls on young minds and attitudes towards race relations, advocating for positive change through inclusive representation.

### Tags

#Representation #Diversity #Barbie #PositiveChange #InclusiveRepresentation #YouthInfluence


## Transcript
Well howdy there internet people, it's Bo again.
So tonight we're going to talk about how Barbie can change the world.
This isn't an ad.
So I mentioned offhand to my buddy that I bought my daughter some Barbies.
And he's like, man, as woke as you are, I can't believe you'd do that, reinforcing
unrealistic body image and all that. It would have been cool if he meant that, but he said it as a joke.
I'm gonna take this opportunity to let everybody know that Barbie is woke now. I mean as woke as
any large multinational corporation can be. The Barbie that we knew, if you're my age, oh that's
gone. That is gone. The unrealistic body image is gone. Yeah, there are still some dolls
that have hourglass figures, but there's also some that are pear-shaped. There's varying
body styles among the dolls. They range in skin tone from super light to super dark.
There's no more, you know, just three.
There's a Barbie in a wheelchair.
It's just very different.
The jobs that Barbie has now are no longer like homemaker Barbie.
Before that I got my daughter, and this plays into the rest of the story, the first one
of course was photojournalist Barbie because, I mean, come on.
And then astrophysicist Barbie, wildlife conservationist Barbie, and marine biologist Barbie.
As I gathered them, right about the time I got them all, a little girl walks up with
her mom in tow, and blonde hair, blue eyed, white little girl, white mom, it's important.
And she is in an utter panic looking for marine biologist Barbie.
So I stand there just in case she can't find it.
And she wants it because her little sister wants it for Christmas.
And her little sister, all she wants to be is a marine biologist.
It's all she's ever wanted to be.
It's why she goes to Gulf World all the time, blah, blah, blah, blah.
She finds one, turns around, hands it to her mom, who thinks nothing of putting this doll
in the cart.
You know what that tells me?
We win.
That's what it tells me.
It wasn't that long ago that mom might have offered some resistance to this doll.
No, no hesitation whatsoever.
If you are currently dealing with the systemic racism in the United States, I'm sure the
fact that some little white girl in the backwoods of Florida is going to be happy playing with
a black Barbie doll, that probably doesn't mean a whole lot.
But look at it a different way.
This is who she wants to be.
This is who she's going to act out her adventures with.
This is going to be her hero.
This is all she wanted.
That is going to alter her mindset about race relations in general.
So yeah, we are seeing a lot more bigotry right now.
But it will fade.
That's why I don't criticize woke brands as much as a lot of people.
Yeah, most of them, it's performative, it's an act, it's to get social capital.
Who cares if that's the effect?
Who cares?
Even if it is an act, if the results are real, does it matter that it was an act?
Say it doesn't.
I would say it doesn't.
When brands do this, they are probably doing it for all the wrong reasons.
But the results will end up being right.
That's why people fought to wake brands up for so long.
Because that little girl's hero is going to be that.
It's gonna be her and I think a couple days when she opens it up on Christmas
she's gonna be super excited for a black Barbie doll. I can tell you that it was
not long ago when that wouldn't have been the case. Anyway it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}