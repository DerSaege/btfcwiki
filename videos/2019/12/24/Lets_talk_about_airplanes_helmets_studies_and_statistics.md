---
title: Let's talk about airplanes, helmets, studies, and statistics....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=f615wgvz90M) |
| Published | 2019/12/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Illustrates flaws in studies and statistics using historical and modern examples.
- Navy commissioned study during World War II on aircraft damage.
- Mathematician Wald recommended armor where there were no holes on planes.
- Example of new helmet during World War I saving lives but doctors misinterpreted data.
- Flaw: analyzing data assuming complete dataset when it's not.
- Mention of the misconception about demographic X committing the most crime.
- Statistics can be misinterpreted, like the Bureau of Justice Statistics example.
- Emphasizes the importance of having a complete dataset for accurate interpretations.
- Mentions climate models inaccuracies due to incomplete datasets.
- Scientists' estimations improve as they gather more data.

### Quotes

- "When you're missing half of your data set, you can't extrapolate."
- "Statistics and studies are useful. They can give you a clear picture of some things."
- "If you only look at little bits and pieces or you choose to interpret it however you want, you don't really pay attention to the methodology used and how it's collected."

### Oneliner

Beau explains the flaws in studies and statistics using historical and modern examples, stressing the importance of a complete dataset for accurate interpretations.

### Audience

Data analysts, researchers, students.

### On-the-ground actions from transcript

- Verify data sources before making conclusions (implied).
- Ensure data completeness for accurate analysis (implied).

### Whats missing in summary

The full transcript provides more detailed examples and explanations on the importance of complete datasets for accurate statistical interpretations.

### Tags

#Studies #Statistics #DataAnalysis #Interpretation #CompleteDataset


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk
about studies, statistics, how we consume information, and what airplanes and
helmets can teach us about that. Most studies and statistics have the same
flaw. They have the same flaw. So we're going to talk about some historical examples that
illustrate that and then we're going to talk about some modern ones, some studies you've probably heard.
During World War II, the Navy commissioned a study dealing with their aircraft. The way it worked was
basically the aircraft came back, it landed, came back from action. These guys ran out and counted the
holes in it, took a diagram, drew where the holes were. They did this to a whole bunch of planes
and created a composite diagram showing where most damage occurred. They used
that to make a recommendation on where the Navy should put more armor on the
planes. They sent this over to a guy named Wald who was part of the
statistical research group up at Columbia. He's a mathematician. He looked at it
He's like, you're gonna armor the wrong spots. You need to put it where there's
no holes. It's where you need to put the armor. Why? What did he know and that
everybody else that looked at it didn't? He paid attention to how they gathered
information. The plane comes back and they count the holes. It doesn't include the planes
that didn't make it back. He took that and realized that those areas where there was
no damage, that meant if the plane got hit there, it was going down. So that's where
he recommended they armored. They went with him. A similar example was the
introduction of a new helmet. I think this was during World War I. Doctors
started complaining, this helmet isn't worth anything at all. Head injuries
are way, way up. The reality, the helmet was really, really good. All those new
head injury patients well before they never even would have made it to the
hospital. It was saving lives it was doing its job. Both of those have the
same flaw. The person analyzing it assumed they had a complete data set and
they didn't. I'll give you one you've heard more than likely. Demographic X commits the
most crime. I'll go ahead and tell you now, there is no study, literally none, that says
that. None. Somebody right now is going to the Bureau of Justice Statistics. Read it
carefully. That's not what that study says. It says that Demographic X is arrested the
most doesn't say who commits the most somebody certainly believes you can
extrapolate not really not really go over to the figures on the percentages
of cases that are solved you'll find out some information that may not let you
sleep well at night when you're talking about the most severe crime in the US
about 60% are solved, 40% are unsolved, but even that isn't a complete data set
because that only includes incidents where a case was opened. What about those
people just disappeared? You can use that statistic to determine a lot of things.
It could mean a bunch of different things. It could mean that maybe it's
It's that, well, demographic X is just really bad at committing crimes, and they get caught more.
Maybe they lack the economic means to facilitate the cover-up.
Maybe they live in urban areas where it's harder to get rid of the evidence.
Could mean a lot of things.
And what it doesn't mean is that they commit the most.
It's not there.
When you're missing half of your data set, you can't extrapolate.
Give you another one you'll hear a lot.
These climate models, I've been hearing about this since the 70s, they're always wrong.
The reality is that when these models are produced, scientists don't have a complete
data set.
They're estimating years in the future how much pollution is going to be there.
When your gas mileage increases, it cuts pollution.
When a state starts checking emissions, it cuts pollution.
more mass transit is used, it cuts pollution. All of these things cut
pollution and increase the timeline. When scientists have gone back to these
models and plugged in the actual information, it turns out they are incredibly
accurate. Statistics and studies are useful. They can give you a clear picture
of some things. If you look at enough of them you can get the big picture, but if
you only look at little bits and pieces or you choose to interpret it however
you want, you don't really pay attention to the methodology used and how it's
collected. Well, as the old saying goes, there are three kinds of lies. Anyway, it's
It's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}