---
title: Let's talk about the electoral college, power, and Jules Verne....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=0Yqf-EHlTus) |
| Published | 2019/12/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the Electoral College and voting, prompted by his son's question about it.
- Shares a joke about John Quincy Adams approving an expedition to the center of the Earth.
- Explains his son's concerns about how to prevent unsuitable candidates from reaching the Oval Office.
- Mentions the issue of candidates campaigning only in major cities if the Electoral College is eliminated.
- Expresses disappointment at his son's elitist view associating better education with people in major cities.
- Comments on the importance of participation over geography in elections.
- Acknowledges flaws in the current system where votes matter more in swing states.
- Suggests that candidates currently win elections by dividing the country and playing one side against the other.
- Proposes a harder process for candidates to prove themselves and unite people before becoming president.
- Advocates for introducing ranked choice voting to address concerns about voting for third-party candidates.
- Emphasizes the need to set higher standards for presidential candidates to make America better.

### Quotes

- "It's not about education, it's about participation."
- "You want to sit in the most powerful chair in the world? Prove yourself out on the campaign trail."
- "We've had enough clowns stumble into that office and they've caused a lot of damage along the way."
- "Let's set the standards higher not lower."
- "I'm not certain of how to fix this problem but the one thing I am sure about is that the answer is not making it easier to sit in the most powerful office in the world."

### Oneliner

Beau delves into the Electoral College, advocating for higher standards and participation, suggesting a tougher process for presidential candidates.

### Audience

Voters, Political Activists

### On-the-ground actions from transcript

- Advocate for ranked choice voting to provide more options for voters (suggested).
- Participate in campaigns or initiatives that aim to reform the electoral process (implied).

### Whats missing in summary

Deeper insights into the potential consequences of maintaining or changing the Electoral College system.

### Tags

#ElectoralCollege #Voting #PresidentialCandidates #RankedChoiceVoting #PoliticalReform


## Transcript
Well, howdy there, Internet people, it's Bo again.
So tonight, we're gonna talk about voting
and the Electoral College.
We're gonna talk about it because my son asked about it.
I told him a joke about John Quincy Adams,
and that joke is that John Quincy Adams
once approved an expedition to the center of the Earth,
like Jules Verne style.
Go up to the Arctic, dive down,
meet the people that live in the center of the Earth,
begin trading with them,
and thus create a more prosperous United States.
If you're waiting for the punchline to this joke,
it's that it's true.
That actually happened.
This, of course, led my son to question,
how do we stop people like that from getting in the Oval Office
to begin with?
And the Electoral College comes up and he says,
we've got to get rid of this because right now people
are winning the election and they don't have the support
of most people.
I explained that if we get rid of the Electoral College,
Well, then presidential candidates will only campaign in major cities, and those will be
the only voices that are heard.
Much to my dismay, my son says, well, isn't it true that people who live in major cities
tend to be better educated?
I'm like, wow, that is incredibly elitist.
It's not about education, it's about participation.
We wouldn't say that about any other demographic qualifier.
We can't do it with geography.
Yeah it is true that the strongest argument against democracy is a 10 minute conversation
with your average voter, but this is the system we've embraced in the United States.
If we're going to get rid of that, we have to get rid of a lot more of the constitution
than just how we select our president.
He says that if it's about participation, we definitely have to change it because right
now your vote only matters if you live in a swing state. Yeah, that's true, that's
true, and that's how the debate gets framed. Even by the media, that's how it
gets framed. Electoral college versus popular vote. Democrats tend to want to
go to the popular vote because it would benefit them. Republicans want to stay
with the Electoral College because it benefits them. As my good friend Dr.
Zoidberg used to say, why not both? When this system was devised, the Office of the
President of the United States was not the most powerful office in the world. It
didn't control the world's most powerful military. It didn't have the ability to
force project anywhere on the planet in 24 hours. It wasn't the de facto leader
of the world.
I think that making it easier to get into this position is probably the wrong move.
We should probably be making it harder.
If we're going to change it, let's make it more difficult.
You have to get the Electoral College and the popular vote, you have to get both.
I would take it a step further and say you have to get two-thirds of the popular vote.
Right now, candidates win elections by dividing the country, by playing one side against the
other, getting people to kick down.
That's how they win.
You want to sit in the most powerful chair in the world?
Prove yourself out on the campaign trail.
Show us that you can unite people.
I would take it a step further and introduce ranked choice voting as well.
Right now there's a whole bunch of people who are scared to vote for a person who better
represents them because they're not a part of a major party.
And if you vote for Jill Stein, well that's really a vote for that person you truly don't
like.
be that way. Introduce ranked choice voting and make it more difficult to get
into that office. We've had enough clowns stumble into that office and they've
caused a lot of damage along the way. You want to make America great? Let's set the
standards higher not lower. I'm not certain of how to fix this problem but
the one thing I am sure about is that the answer is not making it easier to
sit in the most powerful office in the world. Anyway it's just a thought. Y'all
have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}