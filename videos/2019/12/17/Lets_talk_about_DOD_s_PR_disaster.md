---
title: Let's talk about DOD's PR disaster....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_xzcgzFraF0) |
| Published | 2019/12/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Department of Defense posted a glowing biography of a literal war criminal without explanation.
- The post was shocking and lacked context, causing confusion and concern.
- The mistake was likely due to a management error, not a moral one.
- The narrative may have been intended to be posted in parts throughout a commemoration.
- The post has been deleted, but there may have been plans for more posts.
- The incident serves as a reminder that social media blunders may not always be what they seem.
- The importance of proper context and clarity in social media communications.

### Quotes

- "They didn't explain what was going on. It was shocking, to say the least."
- "When we see social media blunders they may not always be what they appear..."
- "The mistake was likely due to a management error, not a moral one."

### Oneliner

Department of Defense posted a glowing biography of a war criminal, sparking confusion and concern, revealing a management blunder, not a moral lapse.

### Audience

Social media managers

### On-the-ground actions from transcript

- Contact Department of Defense to express concerns about the post (suggested)

### Whats missing in summary

The impact of the incident on affected military units and individuals. 

### Tags

#SocialMediaBlunders #DepartmentOfDefense #WarCriminal #ManagementMistake #Context #Communication


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about the anatomy of a PR disaster.
Yeah we're going to talk about the Department of Defense post from yesterday because they
sure stepped in it.
Okay so if you don't know, we are currently commemorating the 75th anniversary of the
Battle of the Bulge.
If you know nothing of World War II history, understand the Battle of the Bulge was incredibly
important.
It was a turning point.
Beyond that, from a military sciences standpoint, it's an incredibly historic battle.
Seventy-five years later, infantry commanders should still study it because there's a lot
they can learn.
It would be in the military's best interest to keep people interested in this topic.
So what did they post?
a very stunning photo of the bad guy
and a glowing biography of him.
Now, I want to be very, very clear when I say this.
When I say bad guy, I don't mean just that he was the opposition.
I don't mean that he was on the enemy team.
I mean, dude's a literal war criminal.
And they posted that without explanation, really.
They didn't explain what was going on.
It was shocking, to say the least.
So shocking, I called a friend of mine that's in one of the
units affected, and I asked him, I'm like, see, I'll
wear an armbands now?
What's going on?
And he tells me the rumor as to how this went down.
So I want you to imagine your boss comes to you and says,
hey, we need you to write a narrative of this event, be
interesting, be informative, be entertaining, and you do it.
You have seen the author of The Da Vinci Code, Dan Brown's
master class on writing.
You know you have to define your villain first,
because that's going to play throughout the whole story.
So that's what you do.
And you write your narrative.
And it's a long narrative.
So the social media people see it,
and they're like, well, that's too long.
And they decide to break it up and post it day after day throughout the commemoration.
And then you get to look in horror as the first day of the commemoration is profiling
the villain, the bad guy.
That's the rumor.
That's how this happened according to the rumor mill up there.
So yes, this was definitely a huge mistake.
Luckily, it was a management mistake, not a moral one.
It certainly appeared, the post has been deleted now, but it certainly appeared that the Department
of Defense was endorsing an ideology that's really not good and was extremely troubling
given the current political climate in the United States.
I don't know if they plan to still release the rest of the narrative, but there should
be more posts coming unless because of the uproar they decided to scrap the whole thing
which I wouldn't blame them for at this point.
But the takeaway from this is that when we see social media blunders they may not always
be what they appear because this certainly appeared to be incredibly troubling and it
turns out it was just normal army management.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}