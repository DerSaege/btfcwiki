---
title: Let's talk about homelessness in the wake of the Supreme Court decision....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Rb-MW0LmbEA) |
| Published | 2019/12/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Supreme Court non-ruling on Boise versus Martin decriminalized homelessness.
- Companies pushing to criminalize homelessness driven by desire to keep homeless away from storefronts.
- Companies now have to become humanitarians and contribute to communities they profit from.
- Need for homeless shelters and affordable housing will increase.
- Ruling currently applies on West Coast but expected to spread across the US.
- Predicted consequences: pushing homeless into areas covered by ruling, blaming politicians for increase in homelessness.
- Companies previously funding criminalization of homelessness may now support shelters and affordable housing.
- Technology exists to solve homelessness issue, lack of will is the obstacle.
- Advocates for housing, poverty, and homelessness urged to step up and take action.
- Courts now require alternatives before criminalizing homelessness.

### Quotes

- "In essence, it decriminalized homelessness."
- "They're going to have to chip into the communities they're profiting from."
- "We have the technology to solve this problem, we just need the will."
- "If you're an advocate for housing for all, if you are an advocate for people who are in poverty, if you're an advocate for the homeless, now is your time."
- "You can't criminalize this without an alternative."

### Oneliner

Supreme Court non-ruling decriminalizes homelessness, forcing companies to become humanitarians and invest in shelters and affordable housing nationwide.

### Audience

Advocates, Community Members

### On-the-ground actions from transcript

- Advocate for housing for all, poverty, and homeless (exemplified)
- Push companies to contribute to communities (implied)
- Support organizations providing shelters and affordable housing (implied)

### Whats missing in summary

The full transcript provides detailed insights on the legal implications of decriminalizing homelessness and the potential shift towards humanitarian efforts by companies and individuals.

### Tags

#Homelessness #Decriminalization #Humanitarianism #SupremeCourt #Advocacy #AffordableHousing


## Transcript
Well, howdy there, Internet people, it's Bo again.
So tonight we're going to talk about why we're about to see a whole bunch of
companies out on the West Coast become humanitarian.
It stems from a non-ruling from the Supreme Court.
The Supreme Court declined to hear a case coming out of the Ninth Circuit.
And that case was the city of Boise versus Martin.
Now what the ruling held was that
Ticketing somebody who was sleeping on public property, when there wasn't a shelter available,
well that's cruel and unusual punishment.
In essence, it decriminalized homelessness, that's what it did.
It's got some far-reaching, far-reaching consequences that are going to come out of
And from where I sit, they're pretty cool.
So the push to criminalize homelessness, to criminalize poverty,
it's been around a while, and it's getting more and more drastic.
And it's been driven by companies, mainly,
that don't want homeless people near their storefronts, it's what it boils down to.
In some cases, it's wealthier citizens who don't want homeless people around their properties
as well.
But at the end of the day, what you're going to find out now is these people who wanted
to criminalize them, lock them up, just get them out of their sight, kick the can further
down the road, or in some cases, beat the people further down the road into another
jurisdiction, they're not going to be able to do it.
So the only alternative, if they want to keep their storefronts free of people they see
as undesirable, well, they're going to have to become humanitarians.
They're going to have to chip into the communities they're profiting from.
We're going to need homeless shelters, affordable housing, these things.
something that's going to need to come.
And I have a feeling this ruling will spread across the US.
Right now, this only applies on the West Coast.
But all jurisdictions need to look at this very carefully,
because it's going to head east.
This is going to migrate.
So people need to start making budgetary provisions for it.
So what we're going to end up with
is we're going to see a number of things.
One, we'll start to see those that are outside
of where this ruling applies.
Those jurisdictions are gonna start pushing people
into where it does apply.
And then they'll blame the politicians over in those areas
for an increase in homelessness.
We can expect that to happen,
but we can also expect companies
that had been funding the push to criminalize homelessness
suddenly have a change of heart and to work for shelters and work for affordable housing.
There's a nonprofit called New Story and they teamed up with a construction technology company
out of Austin, Texas, I believe, called Icon.
They can 3D print.
They can 3D print houses now in 24 hours.
two-bedroom, one-bath. Very inexpensive. We have the technology to solve this
problem, we just need the will. All of a sudden, out of less than pure motives,
people are going to find the will. So that political capital to make this
happen is going to be there. If you're an advocate for housing for all, if you are
an advocate for people who are in poverty, if you're an advocate for the homeless, now
is your time.
Now is your time to step up and really get in the game.
Because we've crossed a threshold where the courts are now saying you can't criminalize
this without an alternative.
You can do it if there are shelters available and they choose not to use them and there
empty beds, well yeah I guess that could be a crime. But you can't do it if you
have no place to send them. The Supreme Court surprisingly has taken a very
liberal stance, a stance that is going to send the US using corporations down a
path towards providing housing for everyone. It's a very interesting
development. It's something we should watch. Anyway, it's just a thought. Y'all
Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}