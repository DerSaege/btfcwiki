---
title: Let's talk about reports, reactions, and responses....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=c1ZFPh4LRyc) |
| Published | 2019/12/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- People are rightfully angry after being lied to for 18 years by multiple administrations, leading to immeasurable costs in money and lives.
- Rather than formulating an educated response, the U.S. reacted emotionally to the events in the Middle East in 2001, resulting in unnecessary invasions.
- The U.S. government is currently pushing the idea that the Taliban wants to make a deal, but experts suggest otherwise.
- The Taliban are strategic and waiting out the U.S., with any deal likely not being honored, leading to unnecessary deaths.
- Beau points out that learning from past mistakes is critical, such as abandoning the Kurds against expert advice.
- There are concerns about a potential march to war with Iran, despite experts warning against it.
- Supporting the truth is more beneficial to American soldiers than blind patriotism, and curbing unnecessary military actions is vital.
- Beau stresses the need for American citizens to hold their government accountable and make informed decisions about military involvement.

### Quotes

- "Be mad, be angry, that's fine, but learn from it."
- "Supporting the truth will protect American soldiers more than any politician in DC ever could."
- "Iran needs a political response, not a military one."
- "This is a moment where the American people can learn from the graves of thousands of American troops."
- "The U.S. runs the greatest war machine the world has ever known."

### Oneliner

People are rightfully angry after being lied to, but learning from past mistakes and supporting the truth can prevent unnecessary military actions and protect American soldiers.

### Audience

American citizens

### On-the-ground actions from transcript

- Hold the government accountable by demanding transparency and truthfulness (exemplified)
- Educate yourself and others on foreign policy decisions and their consequences (suggested)
- Advocate for political responses over military actions in international conflicts (exemplified)

### Whats missing in summary

The full transcript provides a deeper understanding of the consequences of government lies and emotional reactions in foreign policy decisions, urging citizens to be informed and proactive in preventing unnecessary military actions.

### Tags

#GovernmentAccountability #ForeignPolicy #Truth #AmericanSoldiers #MilitaryActions


## Transcript
Well, howdy there, internet people, it's Beau again.
So we gotta talk about that report, that report.
People are angry.
You should be, you should be angry.
Lied to for 18 years by multiple administrations.
The cost of those lies is immeasurable in money and lives.
capital. It's immeasurable. You have every right to be angry. But should you be
surprised? No. It happens all the time. It happens all the time. Beyond that, when
all this kicked off in 2001, the experts, those people that understand the Middle
East, those people that understand the area, those people that are familiar with
with doing these sort of things.
They made suggestions.
They formulated an advised response.
And that response was a series of covert actions with no footprint designed to destabilize
command and control of the bad guys.
That was the plan.
That was the response.
But we didn't use a response.
We reacted.
Rather than looking at it from an educated perspective, looking at it from the view of
taking in all of the facts and trying to decide what was best going to accomplish our goals,
we reacted emotionally, viscerally.
And we had an invasion.
We had to invade somebody, right?
that's the answer, but it wasn't. Once the invasion happened, what happened? The boots
on the ground, what did they have to do? Formulate a plan to disrupt command and control of the
bad guys. But that was after we had already made a whole bunch of new enemies.
The part that infuriates me about this is not the anger.
Should be angry.
Shouldn't be surprised.
But people are so angry they're not really learning from it.
Can't let that anger cloud your ability to learn from this.
Because what makes me mad is that it's being presented as, during the Bush and Obama administrations
this happened.
You were lied to.
You're being lied to right now.
Right now, U.S. government is pushing the idea that the Taliban wants to cut a deal.
No, they don't.
No they don't.
The United States government has already announced its intention to withdraw.
U.S. doesn't matter anymore, they're irrelevant.
Whatever deal is made isn't going to be honored, and anybody with half a brain should know
that.
The Taliban, contrary to the propaganda, are not stupid mountain people.
They're pretty savvy.
There's a reason Afghanistan is known as the place where empires go to die.
They're just waiting us out and then they'll resume their operations.
The deal is irrelevant and all the time spent waiting trying to make this perfect deal that
will not be honored is more time that people are going to die unnecessarily.
kicking the football down the road over the bodies of Americans in innocence.
But we can learn from this.
We can learn from this.
Right now, these same types of situations are happening.
What did the experts say about abandoning the Kurds?
Don't do it.
It's a horrible idea.
US foreign policy for decades to come. But the politicians, they didn't want a
response. They wanted a reaction. And there we are. Decisions made. And in the
background right now you can hear the drums beating that march to war with Iran.
What do the experts say? It's unnecessary. Don't do it. DOD's own war
games show the U.S. losing, but it doesn't matter because we need a reaction, not a response.
So when the time comes, they'll wave those flags, those bumper stickers will appear in
the gas stations, and people will put them on their cars because they don't want to be
seen as not supporting the troops. Before you support the troops, you have to
support the truth. The truth will protect American soldiers more than any
politician in DC ever could. If you supported the truth, they wouldn't go
anywhere. They wouldn't need support, moral support, from back here. While
While they're somewhere they don't need to be fighting for their lives.
Be mad, be angry.
You're lied to, you're still being lied to.
Be mad, be angry, that's fine, but learn from it.
We are in charge.
The United States runs the greatest war machine the world has ever known.
There's a lot of responsibility that comes with that.
We as American citizens have to be willing to curtail our government when they want to
use it.
We allowed that behemoth to be built and here we are.
When they want to use it, we just wave our flags and a lot of times it's unnecessary.
deployment of that many troops in Afghanistan was not only unnecessary, it was unadvisable.
The same thing is true of Iran.
Iran needs a political response, not a military one, but those political responses, they don't
get votes the way images of Abrams being loaded up to go over do.
We have a responsibility.
This is a moment where the American people can learn from the graves of thousands of
American troops.
So there's not more.
Anyway it's just a thought.
Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}