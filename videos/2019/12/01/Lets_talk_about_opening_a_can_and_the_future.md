---
title: Let's talk about opening a can and the future....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=kpgsAJjMiu0) |
| Published | 2019/12/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces a video of a young man struggling to use a can opener during Thanksgiving dinner, sparking a tutorial on can opener usage.
- The accompanying message suggests young people can't be trusted to vote because they struggle with basic tasks like using a can opener.
- Beau showcases various pop-top cans, indicating the prevalence of can designs that don't require a traditional can opener.
- He demonstrates how to use a can opener, expressing disbelief at the notion that young people can't figure it out.
- Beau criticizes the message that undermines young voters, pointing out the hypocrisy of older generations who mock youth for not knowing certain skills.
- Despite the ridicule, the young man in the video perseveres, in contrast to the elders who filmed and laughed at him.
- Beau questions why there's a tendency to blame young people for their supposed incompetence rather than acknowledging their strengths and resilience.
- He challenges the idea that older generations are inherently wiser and more capable of making sound decisions for the future.
- Beau suggests that young people, despite facing ridicule, will figure things out on their own while being undermined by those who are supposed to guide them.
- The message conveyed is to not underestimate young people's abilities based on isolated incidents of struggle or ignorance.

### Quotes

- "Can't trust young people to vote."
- "Don't listen to those young people who did not give up even though they didn't understand something."
- "Y'all should lay off the kids because at the end of the day whether or not they open that can doesn't matter they'll figure it out."
- "Most people today have an automatic can opener and even then most of the brands you don't need it anymore."
- "Let's listen to the people who will laugh and record a video because they don't understand what they did."

### Oneliner

Beau challenges the notion that young people can't be trusted to vote based on their ability to use a can opener, advocating for a shift in perspective towards youth resilience and capability.

### Audience

Young voters

### On-the-ground actions from transcript

- Support and encourage young voters (implied)
- Challenge stereotypes and biases against young people (implied)

### Whats missing in summary

Full understanding of Beau's passionate defense of young people's capabilities and resilience.

### Tags

#YouthEmpowerment #VotingRights #Stereotypes #GenerationalDivide #Resilience


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight, we're going to talk about a video I saw
and a message that accompanied it.
The video was a young man in a kitchen,
looks like Thanksgiving dinner.
And he's trying to open a can.
He's got a can opener, doesn't know how to use it.
Yeah, that's a problem, and we'll fix that tonight.
I'll show everybody how to use a can opener.
The message that accompanied it was
that we can't trust young people to vote
because they don't know how to use a can opener.
We're going to talk about that message too.
OK, so got my can opener, got a can of Campbell's.
Wow, that's pop top.
Crazy.
Chef Boyardee.
Pop top.
Hormel.
Pop top.
Dole, Pop Top, man it seems like the major brands don't, I mean, Wal-Mart, Pop Top, crazy.
These are all out of my pantry.
I was able to find one, so to show everybody this all important skill that is still very
You got it.
You just clamp it right there, and if you can hold it, press down as hard as you can,
turn the wheel.
That's it.
You can also do it this way.
That's all there is to it.
I'm willing to bet that I explained that to you in less time than it would take you to
explain to your parents how to get on Discord.
So now that that major issue is solved, let's talk about that other message.
The major issue is solved, let's talk about that other message, the message that went along with it.
Can't trust young people to vote.
See, maybe I didn't see the same thing that everybody else saw in that video,
because I saw a young man using something he obviously didn't know how to use,
not giving up, while his entire family laughed at him, the elders who were supposed to guide him,
recorded a video to put on the internet so other people could laugh at him. But
he didn't stop. And the idea is that we should let those elders vote because
they know what's going on. Those would be the people that make the wise decisions.
Those will be the people to guide us into the future because they care about
about the future. They've obviously shown that by making sure that their kid has a pretty
basic skill for us, for our age. Like, I guess they didn't do that. But those are the people
we want taking care of us, guiding us into the future. Don't listen to those young people
who would not give up even though they didn't understand something.
Let's listen to the people who will laugh and record a video because they don't understand what they did.
I don't know why there's this fascination with blaming young people.
understand that any of their failings are our fault and I don't know that this
is a failing. Most people today have an automatic can opener and even then most
the brands you don't need it anymore but I mean I guess it's cool to make fun of
a teenager.
might should lay off the kids because at the end of the day whether or not they
open that can doesn't matter they'll figure it out and they will do it while
those people who are supposed to guide them laugh and record them so other
people can laugh at.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}