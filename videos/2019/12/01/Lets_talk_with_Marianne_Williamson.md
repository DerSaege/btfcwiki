---
title: Let's talk with Marianne Williamson....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=OXlXGl7_BmQ) |
| Published | 2019/12/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces Marianne Williamson, known for being a self-help guru and running for president.
- Marianne talks about her background, career, and involvement during the AIDS epidemic.
- Describes the despair and love experienced during the AIDS crisis.
- Marianne founded organizations to provide support services to people living with HIV.
- Shares the story of Project Angel Food, delivering meals to homebound people with AIDS.
- Connects her work during the AIDS crisis to her current political campaign.
- Advocates for harnessing goodness and decency for political purposes.
- Talks about the need for a Department of Peace to focus on peace-building efforts.
- Proposes a Department of Children and Youth to address vulnerabilities and challenges faced by American children.
- Advocates for reparations as a way to address historical wrongs and economic disparities.
- Calls for purifying hearts and promoting forgiveness and respect for differing opinions.
- Addresses the influence of corporate interests on government policies and the need for a nonviolent political revolution.

### Quotes

- "Purify your own heart."
- "What reparations do is they carry an inherent mea culpa, an inherent acknowledgement of the historical wrong that was done."
- "We need a nonviolent political revolution in this country."

### Oneliner

Marianne Williamson advocates for reparations, peace-building efforts, and a nonviolent political revolution to address historical wrongs and corporate influence on government policies.

### Audience

Activists, Advocates, Voters

### On-the-ground actions from transcript

- Support organizations providing non-medical services to those living with HIV (exemplified).
- Advocate for the establishment of a Department of Peace and a Department of Children and Youth (suggested).
- Join movements advocating for reparations to address economic disparities and historical wrongs (implied).
- Educate oneself and others on the impact of corporate influence on government policies (exemplified).

### Whats missing in summary

The full transcript provides a detailed insight into Marianne Williamson's background, her work during the AIDS epidemic, advocacy for peace-building efforts, reparations, and the need for a nonviolent political revolution. Watching the full interview can provide a comprehensive understanding of her viewpoints and proposed solutions. 

### Tags

#MarianneWilliamson #AIDSepidemic #Reparations #Peacebuilding #CorporateInfluence #PoliticalRevolution


## Transcript
You went away.
Well, I have to say hello people, it's Bo again.
And we're here tonight with somebody known
for being on the dance floor with a tambourine on her hip
or maybe having an Alanis Morissette song written
about her.
But you probably know her because she's
running for president.
We have Marianne Williamson.
How are you doing today?
I'm doing great, thank you.
Thank you for having me on.
So this would probably be a good time for you
to tell everybody all the stuff that the media really
doesn't put out about your background.
Because when you look you up, basically, you're
told that you're a self-help guru.
And that's all we get.
Well, I have had a 35-year career.
And today, we're talking a World AIDS Day.
And when my career began in the early 80s,
I was giving lectures in Los Angeles.
And very, very soon after I began lecturing,
the AIDS epidemic exploded onto the scene.
And at first, and for quite a while, Western Medicine,
not that it wasn't trying, it certainly was,
but they kept playing cards and coming up empty.
It was definitely at that time an incurable situation.
If people were told that they had contracted the virus,
it was an almost certain death sentence.
And also, it took a while before the organized religious
institutions had much to say either,
working through whatever they had to work through.
I, at the time, was a young woman
talking over at a place called the Philosophical Research
Society in Los Angeles.
And I was talking about spiritual matters,
very non-denominational.
But I was talking specifically about a God who loves us
no matter what and about how, when we love each other,
miracles happen.
And so gay men, particularly in Los Angeles,
everyone affected by the virus,
but particularly the audience of gay men,
the population of gay men in LA,
began to come to my lectures.
And it became something that we did,
we were together all the time
because it was like being in a war zone.
It was like, it was a moment of such despair and desperation.
And many of these people were young.
This had followed on the heels
you know, a fabulous time in, you know, party time that all of a sudden crashed into a terrible,
terrible, terrible thing. Also, I think it's almost hard for people to believe this now.
We certainly have vestiges of this, but at that time, you wouldn't even believe how many young
men who would come to me, they had a double-edged horror on their hands. They had to not only
tell their parents that they were dying, but they had to tell their parents they were gay,
which believe it or not, was as horrifying a prospect as having to reveal the disease.
So there was so much despair and so much pain, but at the same time, there was so much love
with which we all held on to each other. So it was a major part of the beginnings of my
work as a nonprofit activist also because in addition to giving the lectures and what
would then become my writing career, I founded an organization called the LA Center for Living
and we also had one in New York called the New York Center for Living and what these
were about were about giving non-medical support services to people living with HIV.
So let's say you have been diagnosed with this illness, what are you supposed to do
all day. People were alone. So we rented a house where people could just come. They could
get reiki. They could get massage. And let me tell you something. At that moment when
there was so much misinformation about AIDS and so much fear about AIDS, the very fact
they could get a massage from someone. And these were all free services. And we would
feed people and we would sit around and we would watch movies together and have support
groups together. It was really quite a phenomenon. And then we started one in New York as well.
And then one day, I came to the house and I said, where's John or whatever?
And they said, well, John's not here today.
And I said, well, why isn't John here today?
Well, John's not here today because he couldn't get out of bed, so he couldn't come over.
And I would say, well, how's he going to eat?
And he said, well, we don't know, because people were making food at the house every
day.
So I would say, well, we better take a meal over to John.
And then that turned into a project called Project Angel Food, where we delivered meals
to homebound people with AIDS and other life-challenging illnesses, and as of today, Project Angel Food
has served over 12 million meals.
It definitely kind of leads into the whole idea behind your campaign.
There definitely seems to be a hill-the-country aspect to it, and I'm just kind of wondering
if they play into each other.
Well they more than play into each other.
are each other. Because what I learned during the AIDS crisis was that I learned not only
how sad life can be, but I also learned how good people are. I saw it in Los Angeles,
I saw it in New York, and I saw it in all of the places where they were doing this kind
of work. People really showed up. I mean, one of the great sort of untold stories in
a way about that period of time is not just all the people who rejected gay people and
rejected AIDS patients, but also how many people accepted and accepted AIDS patients.
And so much of the stuff that has occurred that grew out of that human rights campaign
and gay marriage equality and all of those things were really seeded by the way people
showed up at that time.
Particularly one of the things that was happening in Los Angeles was that Los Angeles is so
defined by the entertainment industry. And the entertainment industry is so full of obviously
very vibrant, obviously very talented gay people. So Hollywood really showed up. So
I saw there and then continued with the rest of my nonprofit work. We are good people.
Americans are good people. I'm not saying we're better than other people. We're not
better than other people, but we are a good and decent people. But you, as you well know,
you know, I know you're working, you and I are certainly aligned on this. You wouldn't know how
good and decent we are when you look sometimes at American public policy. And so the problem we
have is the divergence between the goodness and the decency and the dignity of the average American
and who is following the dictates of their conscience on a daily basis
versus the activity of a government that has been so hijacked and corrupted by, you know, the economic, you know,
market forces that are soulless and untethered to any ethical and moral consideration that that public policy then
displays
no conscience whatsoever, no compassion whatsoever. And the way I have seen it, and the way I've seen it play out over
the decades
decades is that so many of the people whose hearts are filled with love have just been
so turned off to politics for that reason.
So then some of the most conscience-based people, most integrists people have been chronically
disengaged from politics, which only made politics get more toxic, which only made the
best people disengaged, which only made it more toxic, which brought us to here.
And I think that there's been a mass awakening.
That's why the popularity of such people is yourselves.
But now we have to harness the goodness and the decency for political purposes.
Because what we did in the nonprofit sector can be done in the public sector.
It's the same thing.
I think that public policy should act according to the dictates of basic decency and conscience
in living with other, you know, human and other sentient beings, um, just like individual behavior
should. And I think that people listen and hear us on the level that we speak from. So nobody can
say, well, we've tried that and it didn't work because in fact we haven't tried it, not in this
country. We've never sought to harness real compassion for political purposes. Yeah. When
one of the things you said in one of the debates was you mentioned something along the lines of,
of, you know, Trump didn't win on ideas. He, he won by saying,
make America great. And when I was looking at the reactions of
people, it almost, it seemed like they thought you were just
saying, Oh, Republicans are dumb. But to me, that read
that. He got people excited about an idea, a slogan, even if
it didn't really mean anything. Do you think that your work can
help achieve kind of a counterbalance to that?
Well, I certainly wasn't saying, nor do I believe that Republicans are dumb.
I think that there are high-minded conservative values, and I think that there are high-minded
liberal values. Eisenhower, President Eisenhower was a Republican president, said that American
mind at its best is both liberal and conservative. It doesn't matter what side of the political
spectrum you're on, neither side, and no group, no socioeconomic group, no ethnic group, no
political group has a monopoly on human values.
And so I believe that the counter force to mean-spiritedness wherever it comes from,
the left or the right, the counterbalance is a conversation that gets to the heart of
the matter where we just, we want to be good people and we want to treat people right.
And we want a government, you know, if you have a representative government, a government
that is supposedly there to quote unquote represent the will of the people, representing
the will of the people should mean representing the consciousness of the people.
So if we are good and decent people, whether we're on the left or the right, which I believe
that we are the majority of Americans, good and decent people, and if our government were
were to reflect that through public policy,
then absolutely that would be a counter force
to the craziness and the falsity
and the mean spiritedness of this moment.
And this brings us to the Department of Peace, right?
Yes, we now spend $760 billion on our military budget.
Now, my father fought in World War II.
I'm a fan of the US military.
Obviously, we need a strong military.
I don't think anybody doubts that.
Before World War II, we didn't even have a standing army.
Obviously, we do now.
We need that.
That goes without saying to me.
However, I think what too many Americans probably
don't realize is how much our military budget reflects
hundreds of billions of dollars over and above what
the military says that they need.
So to me, it's like with your physical body.
I see the military like a good surgeon.
Does America need a really fine surgeon on hand all the time?
Yeah, we do.
I mean, we do.
But a sane person tries to avoid surgery if possible.
So you don't just take medicine.
You also have to cultivate your health.
You have to take care of your diet,
you have to take care of exercise, et cetera.
If you don't take care of your body,
then sickness is almost inevitable.
Well, we have to see at the same time,
you can't just cultivate preparedness for war.
And preparedness for war must be cultivated,
but we must also prepare for peace.
Donald Rumsfeld was the Secretary of Defense
under George Bush.
He said, we must wage peace.
James Mattis, who was the Secretary of Defense
under Trump until he left the Defense Department,
he said, if you don't fully fund the State Department,
I will have to buy more ammunition.
So we have to cultivate peace, not just prepare for war.
And that's why, so what we have reflected in our budget
is we have 760 billion that goes to the military budget,
but then the State Department is 40 billion.
And then within that, we have the USAID,
which is a long-term development and humanitarian assistance.
And then in addition to that, 17,
also within the 40 billion,
is less than one billion that we spend
on what are called the peace-building agencies.
I think a lot of people don't even understand
that peace-building agencies exist.
There are actually factors having to do
with educating children, economic opportunities for women,
reduction of violence against women,
the basic reduction of unnecessary human suffering.
When those factors are addressed,
statistically we have a higher incidence of peace
and we have a lower incidence of conflict.
America needs to decide, do we want a violent society,
do we want a nonviolent society?
That's really what we need to get to,
and that's why Department of Peace coordinates
and addresses things like restorative justice
and conflict resolution and violence prevention
in the schools and trauma-informed education.
There are things that we can do on a domestic basis
as well as on an international basis
to increase the probability of peace.
We should have a peace academy,
just like we have a military academy.
And the people in our government
who are referred to as the peace builders,
in my administration, they would have an equal seat of power
at the National Security Council table.
Well, that makes sense.
I mean, there are a lot of studies
from the military side of things,
showing that the civil affairs guys
and the guys that go out and build bridges
to build schools and stuff like that.
They keep more fighters off the battlefield
than anybody would have gone.
That is so true.
And all of this that I said,
you're pointing out something really important here
because nothing I said was a critique of the military.
These are political decisions.
The corruption here is not military corruption.
It is political corruption.
It has to do with expenditures and activities
that are not for military purposes.
They are for the purpose of increasing short-term profits
defense contractors and what what happens and I've you know heard this from military people all the
time we we burden our military with problems that shouldn't even they shouldn't even be put into the
hands of the military they should have been nipped in the bud years and years before they
shouldn't even been created we create I mean look at ISIS it was our invasion of of of Iraq that did
that. So then the military ends up having to clean up a mess that the politicians created.
Yeah, yeah. So another one of your new ideas, the Department of Children and Youth. What is that?
We have millions of American children who live with vulnerabilities and challenges that are,
it's like a huge invisible sea of unnecessary human suffering. We have 13 million hungry
children in the United States.
And we have a 100,000 homeless children in the United States.
We have children.
It's hard to even fathom.
We have millions of children in the United States who go to school, the elementary school,
and ask the teachers if they have some food for them.
We have millions of American children.
We have, on this one I don't think we have millions, but we definitely have elementary
school children on suicide watch in this country.
And we do have millions of children who go to classrooms
where there are not even the adequate school supplies
with which to teach a child to read.
And if a child cannot learn to read by the age of eight,
then the chances of high school graduation
are drastically reduced
and the chances of incarceration are drastically increased.
So the question can't just be how to create jobs.
It also has to be how to create an education
and how to fully actualize the intellectual
and cultural capacity of every child.
I want a massive realignment of investment
in the United States in the direction of our children.
And so, because so much happens before the age of 10.
So I want every school in the United States
to be a palace of learning and culture and the arts.
Because if you set a child up to succeed
before the age of 10, so much is handled.
So much is handled.
So we need trauma-informed education.
We need community wraparound services.
We need mental health counselors.
Right now, we have one mental health counselor
for every 1,500 children in the United States
and the public schools.
We're not even having, I don't know if you have children,
but I don't know if, we're not even having
a national conversation about what is PTSD.
Because the psychologists now say
that millions of American children in our schools
are suffering a form of PTSD
that is no less severe than the PTSD
of our returning veterans from Afghanistan and Iraq.
Now think about this, think about your workplace
and everybody should think about,
let's say the workplace, wherever you work, okay?
So imagine if where you go every day,
there was now an established pattern around the country
of people coming in and doing a mass shooting.
Imagine if you work at a department store,
but let's say that there was a pattern now
people coming up and shooting up people at a department store.
This would give you a level of chronic PTSD.
And these kids are now in our schools on these lockdowns.
So I think that we need to recognize the emotional and psychological problems of so many children
and we can help these children.
And so there are many things that we can do to address the violence prevention and issues
such as that in our schools, among our children,
we could have mindfulness in the schools.
These are vulnerabilities and challenges that go beyond
the capacity of the Department of Education and Health
and Human Services to deal with. That's true of both
the Department of Peace and Department of Children and Youth. A lot of it is coordinating.
It's not creating new things. It's just like we did with the
Department of Homeland Security, we recognized after 9-11 that we had, there was all this
tragic, you know, I realize now when I'm talking, there are people who do not remember
because they're too young to actually remember that day necessarily, but one of the really
tragic things that happened at 9-11 was, I think it was Mohammed Atta's computer, and
because the FBI wasn't in communication with the CIA.
It was just a simple lack of communication
that could have made all the difference.
So Homeland Security was created to just coordinate.
And that's what we need to do
in terms of creating a more peaceful society.
And that's what we need to do
in terms of helping these children
coordinate some of these efforts that are already happening,
but they're not connected to each other.
And some of the problem solving is not,
we fund the problem creators.
We do not fund the problem solvers.
We fund the problem creators
because they represent corporate interests,
all that stuff that you talk about so eloquently
all the time.
So one of the more controversial series of videos
that I did was actually on reparations.
And I based it on a number and it was the highest number
I could find thrown out by any politician.
And it's one fifth of the number that you're proposing.
So I'm curious how you see reparations working.
And basically, what's the end goal for reparations?
It doesn't look like it's just a payout.
No, it's not a payout.
I don't think the average American is a racist.
That's not my experience or my belief.
But I do think that the average American
is woefully undereducated about the history of race.
So I think my experience in talking
in the whitest states in America is that when
people hear the history, you know, a lot of it we learned,
but we learned it in the seventh grade.
We don't really remember, or we weren't taught that much of it.
And like we were talking in the beginning, you and I,
we're decent people.
So let's look at this.
The first slave ship was brought over in 1619,
and slavery did not end in this country until 1865.
That's just shy of 250 years.
So at that time, General Tecumseh Sherman
promised 40 acres and a mule
to every former slave family of four.
And historians believe that there were, at that time,
between four and five million people who were emancipated.
So the promise of the 40 acres and a mule was rescinded.
And what followed, except for 12 years of reconstruction,
the North sent down, the US government sent
down federal troops to the South to ensure
that slavery would not be reinstituted,
but they left after 12 years.
So except for those 12 years,
you had following 250 years of slavery,
another hundred years of institutionalized violence
towards black people,
because the Southern legislature passed
what were called the black code laws
to ensure that there would be subpar economic
and social and political opportunities for black people.
So that was not addressed.
You have slavery over in 1865,
and it wasn't until 1965, 100 years.
It's staggering.
100 years before the Civil Rights Movement, the Civil Rights Act in 1964 dismantled segregation,
and in 1965, the Voting Rights Act gave equal access to the ballot, but a lot of that's
been chipped away at because of the chipping away of the Voting Rights Act in 2013.
The point is that the economic gap that existed when slavery ended, that has never been closed.
Now Germany has paid $89 billion in reparations to Jewish organizations since World War II.
And one of the things that's interesting about that to me is that, even though obviously
It doesn't mean the Holocaust didn't happen.
It has gone far towards establishing reconciliation between Germany and the Jews of Europe.
Many people now acknowledge this issue of the black, of the gap.
Now there was a study recently, you know, if you have a black person in America who
has equal educational and professional achievement to a white person, they still statistically
make less money. And what has been established is that if black families made as much wealth
as white families, our economy would be $1.5 trillion larger. So it would help everybody.
Now if you actually take the 4 to 5 million slaves and then family of four and multiply
By that, 48% of the world, we'd be talking about trillions of dollars.
Well, that's not going to happen.
But I believe anything less than $100 billion is an insult.
It's like when you're selling a house and somebody comes in so low, you don't even
call them back.
I think, I believe $500 billion is politically feasible because I think we're a good people.
And I think that we need politicians who say to people, instead of just what this does
for you or what this does for you, we need a politics, and that's what I believe my campaign
is that says to people, let's do the right thing and the whole country will be better
off.
So my idea, my plan is for a council, a reparations council, and this would be, let's say, anywhere
between 35 and 70 people who are black leaders from culture, business, academia, et cetera.
And it is their job to disperse the $500 billion over a period of 20 years.
And the stipulation on the part of the U.S. government is that the money is only to be
used for purposes of economic and educational renewal.
So if the reparations council uses the money towards historically black colleges, if they
use it for venture capital, de-gentrification efforts, that's their business.
If I owe you money, I don't get to tell you how to spend it,
as long as it's within that stipulation
of economic and educational renewal.
And the difference between race-based policies
and reparations is that race-based policies
address the economic issue,
but they don't address the moral issue.
They don't provide for the kind of psychological
and emotional healing that this country needs
in regards to race.
We just take this toxic baton of racial turbulence
and we just pass it generation to generation.
What reparations do is they carry an inherent mea culpa,
an inherent acknowledgement of the historical wrong
that was done, the willingness on the part of a generation
to recognize it as a debt, and a willingness to pay it.
I just, to me, it's such a win-win.
It's such a win-win on every level,
including economic, by the way.
You compare, you know, you look at the 2017 tax cut, right?
Two trillion dollars, where 83 cents of every dollar
went to the very richest corporations and individuals.
And we don't even have, nobody has given any evidence
that that will ever even pay for itself,
much less stimulate the economy.
Whereas something like this absolutely does.
So that's the plan.
Now, how do you think you're going to be able to get, I mean, when reparations comes up,
now I know you said in the past that you don't believe that the average American is racist.
And I don't know, maybe it's because I'm down here in the South.
I have a little bit of a view most times.
What did you say to that?
I said, you know, maybe it's that I'm down here in the South.
I tend to have a different view of that most times.
We run a race a lot down here.
I'm curious, that number, and you're right,
about a billion dollars, that's what I use
to do all the math on.
And when you break it down, it's an insult.
500 billion, that definitely seems more than the ballpark.
But even at 100 billion, I was watching people
just freak out over the money.
And I'm just, I'm wondering how you're gonna convince
the average white guy in the South
that this is something we need to do.
Well, one of the things I know about your work,
I don't, when I watch you,
I don't feel like you figured out what you should say.
That's not, that's not what I feel from you.
It's not about my figuring out what people want to hear.
That's the corruption of our politics.
Politicians figuring out what you want to hear,
figuring out what they can get away with,
that's not leadership.
If we're not going to have a big enough number,
then we shouldn't do it.
Because you have to have a younger generation.
If we pay out $100 billion, which is a lot of money,
but then you have a younger generation of Black people
that still says, oh, yeah, we didn't really do much,
Where did that get us?
If it's not a big enough number,
then it doesn't carry the force,
either economically or morally
or spiritually or emotionally or psychologically.
It has to be big enough that it's the level of statement
and the level of repair.
Now, you talk about the South.
I come from Houston, Texas.
Not only do I come from Houston,
I was born and raised there.
I mean, I know about racial attitudes.
I mean, hello, Texas.
But I have also seen how the world has changed.
I remember as a little girl when I used to go to,
there was a building that's still there in Houston
called the Medical Towers.
And we'd go to the doctor.
And I remember seeing a sign between the elevators,
colored restrooms downstairs.
And I remember asking my mother why that was,
and my mother explaining things to me.
And I remember when, after the Civil Rights Act though,
that was gone, that was part of what was outlawed,
that kind of thing, because that's segregation.
I remember hearing a man call an adult black man,
I heard a white man calling an adult black man
who was working in a restaurant say, come here boy,
and asking my father what that was about
and my father explaining to me.
But my parents explained this to me
in terms of how wrong it was.
And the good thing is this country has made strides.
You know, there are a lot more lovers
than haters in this country.
It's just that the haters are active politically
and they have megaphones and they're on social media.
So you're right, there are people out there who are racist
And there are people out there who don't relate to what I'm talking about.
But there are also millions and millions of people out there who are not racist and who
do know what I'm talking about.
And the way I'm, my whole campaign is about saying what I think leaders should do is to
say what the leader thinks needs to be said.
And then not, because once you start just saying what you think you need to say in order
to get elected, then that's got us to where we are.
So I'm saying what I believe needs to be said and people, hey, they're going to vote for
it or not.
And also, let's remember, the president doesn't have a magic wand.
So when I say reparations, you know, plan 500 billion, that doesn't mean I'm going
to have a magic wand or make it happen the next day.
It's what I'm proposing is what I believe is a good idea and it begins a national conversation
and we take it somewhere.
All right.
Okay, so something I ask everybody that comes on the show
is what is one thing the average person can do
to make the world better, to provide the solution,
just one individual action that a person on their own can do?
Purify your own heart.
There's so much meanness to that.
There's so much meanness on the left and on the right.
There's so much, something really terrible has happened.
You know, it's like when we don't agree with someone
these days, it's like not only do we not agree,
but they should shut up.
That's like an emotional fascism that's in the air.
And I think we all need to be more forgiving.
And we all need to be more respectful of people
who don't share our opinion.
You know, there's a smug self-righteous intolerance
on the left is no less dangerous to the emotional fabric
of this country than a smug self-righteous
intolerant person on the right.
You know, I'm sure that there are people
who have heard the conversation I've had today,
who might not agree with my conclusions,
but still, a conversation can add to your understanding,
it can add to your insight,
it can add to your thinking whether or not you agree.
I mean, there are many things
that a high-minded conservative might talk about,
and in the end, I don't agree with the ultimate conclusion,
And I'm like, yeah, my thinking is broader
because I heard it talked about that way.
Nobody has a monopoly on values
and nobody owns this country, we all do.
And meanwhile, there's this left versus right thing
that we're all caught up in.
And the real enemy of democracy,
which is this corporatism and this corporate aristocracy,
democracy, which has market forces unfettered to any ethical or moral consideration for
people or planet, is just corrupting our government, undermining our values, and eating this country
alive.
While we're over here having these ultimately unimportant fights, not recognizing where the
real assault on our democracy is coming from, because it's not from high-minded conservatism
or high-minded liberalism.
coming from something that's really dark. So, that's how I see those things. You know,
we have a politics of people-pleasing. We have politicians, you know, they do focus
groups and this is what we should say to get elected. What is going on here? That's just
the degradation, once again, you know. That's why I like shows like yours. You're just a
truth teller. You're just letting it fall where it falls. Somebody agrees with you or
I don't agree with you.
That's what I think politics should be.
OK, so we're closing in on the end of the show here.
Partly in the shot.
And this is the part where you can get any point out.
I know you're in Iowa now.
Do a big push there.
Anything you want to plug, anything you need to,
anything along those lines.
Well, in Iowa, we have a corporate aristocracy.
And it's made up of health insurance companies
and big pharmaceutical companies and gun manufacturers
and food companies and chemical companies
and agribusiness and defense contractors and oil and gas.
And the way our country works,
and all of them have a legitimate role to play
in our country.
I mean, it's not like they don't have legitimate roles
to play in our country.
But what we have done since the 1980s
is given their short-term profits, undo influence,
our government does more to advocate for their short-term profits for their stockholders than it
does to advocate for our people and our health and our safety and our planet. Now in Iowa, one of the
places where this is the most dangerous and destructive has to do with the role of agribusiness
and the way it has devastated the farming sector. And we have in Iowa, you have farmers who are
committing suicide, you have an explosion of bankruptcies among farmers, you have terrible
devastation of the soil, lack of crop diversification. All of this started back in
the 80s because before then, in the 70s, but before then, if a farmer didn't have a good yield one
year, it went to the bank. The bank was liable to be a local bank that would most probably say,
Well, we understand you had a bad year. Don't worry about it. We'll extend the loan. You'll you'll pay us next year
But what happened starting in the 80s was that the banking became so much a handmade into corporate agribusiness
So you have all these people never even visited this land. They know nothing about farming
They know nothing about land and all it's about is about creating short-term profit and this is
Devastated the farming sector, but the thing is and once again you talk about this all the time
It's one underlying problem and that is the corporate takeover of the US government
It looks like it's different problems.
It looks like it's the opioid crisis over there
because of predatory practices on the part of big pharma.
It's the Boeing disaster over there
because of the FAA looking away
while Boeing cut corners over there.
It's devastation among farmers over here
because of agribusiness and what it's done to farmers.
But it's really, it looks like different problems.
It's really only one problem.
And that is what has happened
when your government has so sold out,
that whether it's the FAA not looking at Boeing
or the FDA not watching out what was going on
with big pharma and the opioid crisis,
or whether it's the USDA and the FDA
being in the pockets of agribusiness
rather than the government doing what it used to do,
which is really supporting the farmer.
This is what we need to recognize,
that we need a nonviolent political revolution
in this country,
because the corporate aristocracy represents,
It's an economic tyranny, and it is hurting people,
and it is undermining our democracy.
Our government is supposed to represent
the will of the people, not just represent the short-term profit
motivation of huge corporate conglomerates, which
have no sense of ethical or moral responsibility to people.
And our entire political establishment is in on it.
Our entire political establishment
is so corrupted because of the money
that it's time for the people to step in.
And that's what you do and that's what I do.
And there are a lot of us, there are a lot of us,
there are a lot of us in there.
And I think ultimately it's gonna lead to a good thing.
I think so too.
All right, so that's gonna wrap up the show,
the interview portion of the podcast.
If you're watching on YouTube, this will be the end of it.
And I really wanna thank you for coming on.
Oh, I wanna thank you.
you're busy. You gotta go do. Thank you. I learned from you
and I think you're great and you're you're doing something
really important as you know. Thank you so much. Thank you.
All right, yeah, you have a good night.
Bye.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}