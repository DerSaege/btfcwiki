---
title: Let's talk about what outrage on Fox News can accomplish....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=3CMfrhAScfY) |
| Published | 2019/05/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing outrage and a comment on YouTube about discussing unpleasant happenings.
- Explaining terrorism as a PR campaign with violence to provoke outrage.
- Describing how outrage leads to justifying and condoning extreme actions like drone strikes and torture.
- Linking overreactions to an increase in terrorism and insurgency.
- Connecting foreign fighters in Iraq to the abuse of detainees, Guantanamo Bay, and Abu Ghraib.
- Noting that outlets like Fox News contribute to radicalizing Middle Easterners through outrage.
- Criticizing Fox News for using fear, bigotry, and ignorance to stoke outrage and ratings.
- Emphasizing that being outraged plays into the hands of terrorist organizations.
- Suggesting that Fox News inadvertently aids in the recruitment of terrorists.
- Advocating for discussing sensitive topics in a calm and clinical manner to avoid perpetuating outrage.

### Quotes

- "Being outraged is not good when you are talking about countering terrorism."
- "I don't want them to win."
- "The reason I talk about these things in a very clinical manner so as not to provoke outrage."
- "Fox News and outlets of their ilk have done more to help that small percentage of radicalized Middle Easterners than their own propaganda networks have."
- "The Islamic State, Al-Qaeda, these guys, they should have been cutting Fox News a check the entire time because they were their greatest recruitment tool."

### Oneliner

Beau addresses how outrage fuels terrorism, criticizing outlets like Fox News for inadvertently aiding radicalization by stoking fear and bigotry.

### Audience

Media Consumers

### On-the-ground actions from transcript

- Fact-check news sources and avoid outlets that sensationalize and provoke outrage (implied).
- Support media outlets that report calmly and objectively on sensitive issues (implied).

### Whats missing in summary

Beau's passionate plea for media consumers to be critical of sources and avoid falling into the trap of outrage-driven narratives.

### Tags

#Outrage #Terrorism #MediaConsumption #FoxNews #Radicalization


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're going to talk about outrage.
There's a comment on YouTube, and basically what it said was the average American is terrified
of this very small percentage of radicalized Middle Easterners.
And the problem is that I only talk about the good that people do, I don't talk about
the messed up things.
person feels that it would be better if I did.
Well first, I do talk about the unpleasant happenings in the Middle East.
I do it in a very clinical fashion, and there's a reason for that.
What is terrorism?
It's a PR campaign with violence, it's what it is.
What's the goal?
The goal is to provoke outrage.
Why?
Because when outrage occurs in the target, in this case, the United States, the average
citizen begins to justify and condone things they shouldn't.
Drone strikes that have 90% civilian casualties, torture, waterboarding, the abuse of detainees.
These things, because they're scary.
That outrage produces an overreaction.
But overreaction creates more terrorists.
That's how terrorism becomes insurgency.
Why do I talk about these things in a clinical manner so as not to provoke outrage?
Because I don't want them to win.
Iraq you probably heard about foreign fighters. Foreign fighters and the reason
they were a hot topic was because they were exceptional fighters. They really
were. Very small percentage of opposition forces were responsible for about half
Half of American KIAs, half of American dead, came from these guys.
But where did they come from?
All over the world, why?
What was the number one reason cited?
The abusive detainees, Guantanamo Bay, Abu Ghraib, that's where they came from.
That outrage worked.
of a reaction, brought in the foreign fighters that killed American service people.
It's what happened.
This isn't arguable, this is fact.
Now with that in mind, I would suggest that Fox News and outlets of their ilk have done
more to help that small percentage of radicalized Middle Easterners than their own propaganda
networks have.
Because they pursue that outrage daily in hopes of getting ratings and it works.
It works because they use that visceral imagery.
They play on bigotry.
They play on fear.
They play on ignorance.
They make you scared, and they make you willing to condone things you shouldn't.
Creates that overreaction, and in turn creates more insurgence, and in turn kills the people
most Fox News viewership would say they care about, but they don't.
They don't even care enough to learn a little bit about the subject.
Being outraged is not good when you are talking about countering terrorism.
That is literally their goal.
I mean in all honesty, the Islamic State, Al-Qaeda, these guys, they should have been
cutting Fox News a check the entire time because they were their greatest recruitment tool.
So the reason I discuss these things in a very mellow manner, very clinical, is because
I don't want them to win.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}