---
title: Let's talk about why rapists are going to be moving to Alabama....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ViDLhi-QvfM) |
| Published | 2019/05/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Alabama's legislature is compared to the Taliban due to a bill introduced by Dicky Drake to address false accusations of sex crimes.
- Filing a false report of a sex crime is already a crime in Alabama, but the penalties may be increased to 10 years in prison for the accuser if the accusation is proven false.
- The bill could lead to victims of rape being imprisoned if their accusations are proven false, even though studies show that false accusations of rape are in the single digits.
- Beau questions the flawed criminal justice system in Alabama and suggests that putting rape victims on trial is not the solution.
- The bill may create an environment where rapists flock to Alabama, as there is a risk of innocent victims being imprisoned based on false accusations.
- Beau expresses disbelief at the flawed logic behind the bill and its potential consequences on rape victims and the justice system in Alabama.

### Quotes

- "A careful rapist who doesn't leave physical evidence always walk and their victim will go to prison because Dickie Drake's friend, his ex-wife, said something."
- "So he gets to rape her and then put her in prison?"
- "You're saying the criminal justice system is broke, but you're just making the assumption that it's only broke one way."

### Oneliner

Beau questions Alabama's flawed legislation that could lead to innocent victims of rape being imprisoned based on false accusations.

### Audience

Legislators, advocates, activists

### On-the-ground actions from transcript

- Advocate for comprehensive justice system reform in Alabama (implied)
- Support organizations working to protect victims of sexual assault and improve reporting mechanisms (implied)

### Whats missing in summary

The emotional impact and urgency of addressing the dangerous implications of Alabama's proposed legislation.

### Tags

#Alabama #Legislation #SexCrimes #CriminalJustice #Victims #Advocacy


## Transcript
Well howdy there internet people, it's Bo again.
So tonight we've got to talk about Alabama-stan.
Saying Alabama-stan because the legislature there is the Taliban.
A gentleman named Dicky Drake introduced a bill and the state of purpose of the bill
is to cut down on false accusations of sex crimes.
Filing a false report in Alabama, it's already a crime.
But I guess the penalties aren't stiff enough.
So if you make an allegation of a sex crime and it's proven false, guys found not guilty,
the accuser, looking at 10 years in prison, keep in mind a convicted rapist, the average
sentence in the US is 9.8 years.
They may also be liable for the accused's legal fees.
So when some bumpkin deputy mishandles evidence and breaks a chain of custody and the rapist walks, she goes to prison.
Or when maybe a person just has a credibility problem.
Maybe it's a child on a youth basketball team.
Mr. Drake, aren't you involved with that?
So when one of those kids gets molested and comes forward
nobody believes them, you're gonna throw them in jail.
This is a solution to a problem that really doesn't exist.
You know, studies put the percentages of false accusations of rape in the single digits.
So it's not gonna accomplish fixing that.
rapists already one of the most under-reported crimes in the country.
It's not going to solve that, but let me tell you what it is going to do.
It's going to make every rapist in the country move to Alabama.
Oh I would not be sending my daughter to Auburn or the University of Alabama.
No way.
Because that's just a target rich environment now.
A careful rapist who doesn't leave physical evidence.
always walk and their victim will go to prison because Dickie Drake's friend, his ex-wife,
said something.
So he decided to introduce this bill.
That's what happened and that's not even hidden.
He said that.
He's introducing this bill because one of his buddies, I guess the guy's ex-wife, claimed
that he abused their kid or something.
So now, every rapist in the country is going to move to Alabama, 10 years.
If the criminal justice system in Alabama is this flawed, this tragically flawed that
false allegations are that much of a serious problem, wouldn't the solution be to address
the criminal justice system and not put rape victims on trial?
Wouldn't that make more sense?
I mean, the idea here is that these false accusations, well, they're running amok and
they're ruining people's lives and they could send somebody to prison, well, okay, so the
criminal justice system is broke.
You're saying the criminal justice system is broke, but you're just making the assumption
that it's only broke one way and that guilty rapists don't walk, so he gets to rape her
and then put her in prison?
I honestly can't believe that this even made it.
I can't believe one of this guy's aides didn't just look at him like, are you out of your
mind?
Do you not understand what this is going to do?
I understand you're 70 something years old, but people travel.
You know, in today's age, people travel.
But somebody of that ilk, when you create an environment in which they can basically
operate freely, with no real risk of being reported, you don't think they're gonna move?
I would get out of Alabama.
I really would.
I would not send my daughter to a university there because the frat boys, oh man, they're
going to love this. They're going to love this. I mean, you know they've got the best
lawyers. That's really what a courtroom in Alabama decides, isn't it? Who's got the best
lawyer. Not who's telling the truth. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}