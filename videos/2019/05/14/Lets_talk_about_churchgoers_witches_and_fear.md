---
title: Let's talk about churchgoers, witches, and fear....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=cV9YO7E_vgo) |
| Published | 2019/05/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Tells an anecdote about a civics teacher whispering words to divide students into groups based on seating, leading to unfair outcomes.
- The teacher labeled himself as a "witch" in the seating chart, symbolizing the government's role in instilling fear and control.
- Talks about fear and false accusations, particularly related to sexual assault and terrorism.
- Mentions a dark video concept addressing the Alabama law, not shared due to its nature.
- Expresses how fear is manipulated by those in power to divide and control people.
- Compares fear of terrorists, school shooters, illegal immigrants, and other issues with statistical likelihoods.
- Points out the manipulation of fear in elections and society to maintain control and division.
- Urges reflection on who benefits from keeping individuals afraid and divided in society.

### Quotes

- "They whisper in your ear and tell you what to be afraid of."
- "Panic struck, terror."
- "Nobody whispered that spell into your ear."
- "Keep us kicking down or punching left and right rather than punching up."
- "Turn them into cowards."

### Oneliner

Beau talks about how fear is used to control and divide people, urging reflection on who benefits from keeping individuals afraid and divided in society.

### Audience

Citizens, Activists

### On-the-ground actions from transcript

- Challenge fear-mongering narratives (implied)
- Support mental health initiatives to combat fear and anxiety (implied)
- Advocate for policies based on facts, not fear (implied)

### Whats missing in summary

The emotional impact and Beau's engaging storytelling style are missing from the summary.

### Tags

#Fear #Manipulation #Government #FalseAccusations #Community #Activism


## Transcript
Well howdy there internet people, it's Bo again.
Tonight I'm gonna start off with an anecdote.
Telling it for two reasons.
The first is that I just saw somebody try to tell it
and butcher it, and there is nothing worse
than putting a lot of thought into a great concept
and having it relayed poorly.
And the second is, it's very timely.
Fits with a lot of things we've been discussing
this channel. So there's a civics teacher and it's the second day of class and this
guy's job is to talk about government. It's what it is. It's what he's
supposed to do. So he's standing up at the front of the class and he says okay
at the end of the week you guys are gonna have to divide yourselves into
groups of four or more. Now, I'm going to walk around the room and I'm going to
whisper a word in your ear. If you follow instructions, some of you may get an A
in this class and never have to do anything else. Some of you may fail. The
word I'm going to whisper in your ear is determined by where you chose to sit
today. It's all done by a seating chart. So he walks around the room, he starts
whispering. Gets back up to the front of class and he says, okay, if I whispered
the word witch into your ear, your job is to get into a group with two or more
churchgoers. If you do that you get an A. If I whisper churchgoer into your ear
and you end up with a group, or in a group with a witch in it, you fail.
Now predictably the class erupts talking about how unfair it is, but begrudgingly
over the next week, they divide themselves up into little groups.
They're mad, but they do it.
The end of the week, everybody's sitting there in their little groups, and he says,
okay, if I whisper churchgoer into your ear, raise your hand.
And everybody does.
Now when I just heard this anecdote told, it was kind of like a gotcha moment.
There were no witches at Salem.
Nah.
I think it was a little bit deeper than that.
I think the guy was a little bit smarter than that.
The only person labeled a witch on that seating chart was him.
His job is to teach about how government works.
That's what they do, isn't it?
They tell you what to be afraid of.
They whisper in your ear and tell you what to be afraid of.
Tell you the penalties if you don't do what they say, if you don't ostracize these people,
if you don't take the correct stance.
They get you so scared,
you never stop to think about
why you're even participating in it.
I think it's very timely because of
a lot of the discussions that take place here.
Because that's what most of them are about.
At the root, we wouldn't even be talking about them
if it wasn't for fear.
Fear of the other.
fear of this thing that might happen, but the reality is that there's not many witches.
Now it was a common theme in the comment section of that video on the proposed Alabama law
about false accusations.
A whole bunch of guys worried, terrified, afraid
of those witches
making false accusations.
The uh...
the poetic justice of using
something related to Salem
as an example and the lead into this is not lost on me.
But that's what they're worried about.
Should they be?
Really? Or is that a witch?
The reality is, guys, unless you walk around terrified
that you're going to get raped or sexually assaulted,
probably shouldn't worry much about false accusations,
because you are more likely to be sexually assaulted
than you are to be falsely accused.
That's the reality.
But somebody whispered in your ear,
And you believed it.
Panic struck, terror.
And while we're on this topic, some of you may have already seen it, there's a video
that's out.
It wasn't exactly leaked, I just didn't want to put it on this channel because it was a
little dark.
That was a concept, not a refined finished product, so I don't want to hear any critiques
about the production value on it, did a little PSA about that Alabama law and how that will
be used.
But I'll put the link in the comment section, just to understand it is dark.
It wasn't something I felt comfortable uploading here, even as just a concept.
Okay, now back to the topic at hand.
pretty afraid of terrorists too. They're witches. They are. You're more likely to
be killed by a cop than a terrorist. Probably worried about school shooters.
Not saying you shouldn't be. It could happen. Could happen. Your child is more
likely to die by suicide. Do you worry about that? No. Because nobody's told you that that's
your fear. Nobody whispered that spell into your ear. The witch, the government, those
in authority, the head of the class, they didn't tell you that. Illegals, right? Undocumented
workers. They are certainly a witch today. They even have the same propaganda. They are
poisoning us from within. They are violent. They do all these horrible things. The reality
is they are more law-abiding than the average citizen and you are more likely to be sexually
assaulted by a cop than an illegal.
Iraq, you know, active duty in Iraq, death, big fear, reality, more vets killed themselves
during that time period, then were killed by enemy action.
Fear, what can be used by those at the top to keep us fighting with each other just like
that class.
Keep us kicking down or punching left and right rather than punching up.
Fear, that's what it takes today to win an election in the US.
You've got to tap into what people are afraid of and make them more scared.
Turn them into cowards.
They want to hide behind a wall.
That's, I mean, there's nothing more symbolic than that.
It's ridiculous.
And the thing is, most of them truly believe that they're the best, brightest, and bravest.
They're afraid of somebody who speaks a different language.
As we're coming up and trying to move forward
in this country, you might want to think about
who's trying to keep you terrified, keep you afraid,
and those people who are trying to charge ahead through the fear through
the things that are supposed to be our downfall. You can determine from that who
want to follow. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}