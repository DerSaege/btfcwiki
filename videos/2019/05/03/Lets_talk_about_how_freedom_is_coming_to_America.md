---
title: Let's talk about how freedom is coming to America....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NowVx9S1Lgs) |
| Published | 2019/05/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Describes a world with a higher level of freedom.
- Mentions various instances where this freedom is emerging.
- Talks about societal change and resistance against injustice.
- Points out different acts of solidarity and unity.
- Emphasizes on the importance of standing up for what's right, even if it doesn't directly affect individuals.
- Encourages the building of parallel power structures.
- Raises the question of individual participation in the ongoing fight for change.
- Addresses the global impact of positive ideas spreading.
- Concludes by wishing everyone a good night.

### Quotes

- "It's coming from the people who are standing in the streets facing off against MRAPs and tear gas over yet another unjust killing."
- "It's coming from anybody who stands up for what's right, even though it doesn't directly impact them."
- "Good ideas don't really require force."
- "It's coming from those people within the existing power structures who are attempting to dismantle them."
- "First we take DC, then we'll worry about Beijing."

### Oneliner

A call to action for freedom through societal change, solidarity, and standing up for what's right, regardless of direct impact.

### Audience

Activists, Community Members

### On-the-ground actions from transcript

- Stand up against injustice by joining protests and demonstrations (exemplified).
- Engage in acts of solidarity and unity with marginalized communities (suggested).
- Contribute to building parallel power structures within communities (exemplified).
- Educate oneself and others about ongoing social issues (implied).

### Whats missing in summary

The full transcript provides a detailed and inspiring look at how individuals are contributing to a more free and just society through various acts of resistance, solidarity, and unity.

### Tags

#Freedom #SocietalChange #Solidarity #Activism #CommunityBuilding


## Transcript
Well, howdy there, internet people, it's Bo again.
After that video where I described the world I wanted,
a whole bunch of people were like, hey, that's great.
How do we get there?
How do we achieve that level of freedom?
Oh, that level of freedom's coming.
It's coming.
It's coming from the feeling that society isn't right.
Right now, it's crawling through a tunnel under Trump's wall.
It's at a wedding chapel where a Klansman's grandkid
marrying their black partner. It's coming from the people who are standing in the streets
facing off against MRAPs and tear gas over yet another unjust killing. It's coming from
the decriminalization of marijuana and the pardoning of those people who were convicted
beforehand. It's coming from the straight guy who shows up at the Pride rally to act
as a buffer against the bigots. It's coming from the trans person who's hanging out with
the rednecks to learn how to shoot so they don't have to rely on a police force that
doesn't care about them, it's coming from those people who look at a flag-covered coffin
and see the waste.
It's coming from the cultural exchange that takes place over the internet, because that
exchange kills nationalism and prejudice and xenophobia.
It's coming from the gardens that are popping up in Section 8 housing, and from the farmer
who's donating a portion of his harvest to the food bank.
It's coming from the bookworm in the hijab whose best friend happens to be the blonde
hair blue eyed cheerleader.
It's coming from the boy scout who took a knee.
It's coming from the black girl who's tutoring the kid with the rubble flag sticker on his
truck.
It's coming.
It's coming from the labor organizer who believes everybody who's working deserves a decent life.
It's coming from the Jews who are standing up for the Palestinians.
It's coming from the natives who are imparting their wisdom to the descendants of the people
who almost annihilated them.
It's coming from the grandparents who are filling in for parents because of the strain
modern society places on them.
It's coming from those pioneering for universal education and universal healthcare.
It's coming from the scientists who are working on renewable energy.
It's coming from the high school dropout who just can't take it anymore and masks up in
face of corporate greed. It's coming from anybody who stands up for what's right, even though it
doesn't directly impact them. It's coming from those people attempting to build parallel power
structures. It's coming from those people who refuse to bow to any order but our own, any
institutions but our own. It's coming from those people within the existing power structures who
attempting to dismantle them. It's coming from the fifth column. The question is
where are you? Because that fight, it's happening whether or not you're there.
And I know there were a lot of people asking about well what about other
countries? That's great for here, but what about other countries? First we take
DC, then we'll worry about Beijing. It'll spread. Good ideas don't really require
force. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}