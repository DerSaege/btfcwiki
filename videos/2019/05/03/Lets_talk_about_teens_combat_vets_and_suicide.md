---
title: Let's talk about teens, combat vets, and suicide....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=j74Y57R15Qs) |
| Published | 2019/05/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- May's Mental Health Awareness Month.
- Beau addresses the heavy topic of suicide.
- Beau shares shocking statistics on suicide attempts by young girls.
- Suicide among teens is a significant issue.
- The rise in successful suicides is alarming.
- Beau provides the National Suicide Hotline number.
- Suicide is a prevalent issue among teens.
- The lack of national focus on suicide is questioned.
- Beau criticizes the lack of attention to suicide compared to school shootings.
- The impact of popular media like "13 Reasons Why" on suicide is discussed.
- Suicide is often a result of multiple factors, not just one reason.
- Beau talks about the struggles teens face in today's society.
- The importance of valuing and supporting youth is emphasized.

### Quotes

- "As Americans, we want to know, well, what's the reason this is happening? It's not a reason."
- "Life is never without the means to dismiss itself."
- "We have a society that is creating a suicide epidemic among teens."
- "There's not going to be an easy fix to this one, guys."
- "It's just a thought."

### Oneliner

May's Mental Health Awareness Month: Beau shares shocking stats on teen suicide, questioning the lack of national focus and urging to value youth.

### Audience

Parents, educators, policymakers

### On-the-ground actions from transcript

- Call the National Suicide Hotline at 1-800-273-8255 or text 741-741 for immediate help (exemplified)
- Take interest in the well-being of teens contemplating suicide; it can make a life-saving difference (exemplified)

### Whats missing in summary

Full context and emotional depth from Beau's message.

### Tags

#MentalHealth #SuicidePrevention #TeenSuicide #YouthSupport #CommunityAction


## Transcript
Well howdy there internet people, it's Bo again.
It's mental health month.
So tonight we're going to talk about suicide.
I'm telling you that now.
I'm also telling you I'm not a mental health professional.
I talked to one
when I decided to do this video and she basically told me that I can't.
Because you're not allowed to be descriptive.
You can't paint a picture
suicide. That's actually apparently very bad. So I was at a loss. How am I gonna
talk about this? And then I realized maybe the most effective thing to do
would be to tell you why I want to talk about it. Because I ran across some
numbers, some studies, and it blew me away. It blew me away. Now as I go through these
numbers, I want you to keep in mind the 22 a day. Now if you're not familiar with
that number, it's a number of veteran suicides, okay? It's national, national
discussion because of this. Okay, so here are the numbers. This is what caught my
attention. From 2010 to 2017 girls age 10 to 12 who attempted to poison
themselves, the percentage rose 268%. Ages 13 to 15 rose 143%. This trend
continues until you reach age 24. Huge jumps, huge jumps. But we don't know what
that means, you know. The percentage going up, well yeah that's noticeable. This is
definitely not statistically insignificant, but it doesn't give you an
idea of the scope of the problem. So in 2018, girls aged 10 to 18 who attempted
to poison themselves, that number is 60,000. If you were to break that down
per day, it would be 164. Now I know somebody out there right now is going
going, attempted poisoning, drama queen, cry for help. First, shut up. Second, if it is
a cry for help, they need help. Not to be made fun of. Not to be marginalized.
I'll give you another number to help sink this in. 2009 to 2017, the number of successful
Suicides rose 33 percent.
Pause real quick.
If you are contemplating this, 1-800-273-8255,
that's the National Suicide Hotline,
1-800-273-8255, or you can text
741-741.
It's important,
because 16 percent of teens
of teens will consider suicide.
It's not a national topic though, maybe that's how we should frame it.
You're more likely to lose your kid to suicide than you are a school shooting.
So why aren't we talking about it?
Why is this not a national issue?
Can't be politicized, that's the real reason.
Who are we going to blame?
That's the reason.
That's the reason it isn't really being discussed.
As Americans, we want to know, well, what's the reason this is happening?
It's not a reason.
It's not how this works.
And that's why I hate the show 13 Reasons Why.
Not just as that glorifies suicide.
paints it as an event
rather than the mess that it is.
Normally suicide doesn't occur because of a single thing.
It's a whole bunch of little things
that when taken together seem insurmountable.
And among teens I would imagine
that the fact that they don't have much agency
of their own. They can't fix the problems themselves sometimes.
It leaves them at a loss.
They don't have any control.
But life is never without the means to dismiss itself.
As far as 13 reasons why, there's never a reason why, never.
It will get better, I promise.
Now communities I'm close to have been dealing with this for a while, for a while.
And these are solution-oriented guys, it is mostly men, and they got nothing, not really.
Puppies and pot, that's what seems to be working for this community, service dogs and cannabis.
Probably not the route we should suggest to teens, but at the same time, I think we need
to pause and realize teens today are facing the same issues that combat vets are.
This is the world we created.
This is our society.
That's depressing.
There's never a reason why.
And there are a lot of things that can be done and it's going to have to be a lot of
things, not one.
There's no silver bullet for this.
It's not that, you know, you can blame it on music or TV or too much time on the internet.
It's not that.
It's that we have a society that doesn't value our youth.
That's the main issue.
We don't.
We treat them as a burden.
Thai pod eating losers rather than the resource that they are.
They're the future and they're poisoning themselves.
And it's not even a topic of discussion because, well, they don't matter, it's the way they're
seen.
We don't put that effort into it.
There's a study that shows that if a non-parent adult figure takes an interest in the well-being
of somebody who's in this situation, it can literally be the difference between life and
death.
There's a whole bunch of information out there about this, but there's not one solution.
Just like it's not one reason.
It's not one event.
We have a society that is creating a suicide epidemic among teens.
There's not going to be an easy fix to this one, guys.
It's going to take a lot.
But before any of that can start, we have to start valuing the youth of this country.
Anyway, there's never a reason why.
It's just a thought.
So have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}