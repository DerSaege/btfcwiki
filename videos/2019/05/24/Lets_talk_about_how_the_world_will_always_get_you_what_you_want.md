---
title: Let's talk about how the world will always get you what you want....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=jxc-7SE8Ev8) |
| Published | 2019/05/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the rarity and unique features of a Glock 18C, a fully automatic firearm with a rapid cyclic rate of 1200 rounds per minute.
- Describes the strict regulations that essentially made the Glock 18C non-existent for civilians in the United States.
- Mentions the illegal conversion kits that can turn a semi-automatic Glock into a fully automatic one, which are now being tracked by the ATF.
- Points out that prohibition often creates demand for restricted items, as seen with the Glock 18C conversion kits.
- Argues against prohibition of certain items like firearms, drugs, and abortion, stating that it only increases their desirability without a clear victim.

### Quotes

- "Prohibition doesn't work with guns. It doesn't work with drugs. It's not going to work with abortion."
- "Focusing on prohibiting items like this is always a losing proposition."
- "When you take something that is, in essence, a victimless crime, possessing this, there's no victim."

### Oneliner

Beau explains how prohibition fuels demand for restricted items like firearms, showing why it fails without victims.

### Audience

Policy Makers, Activists

### On-the-ground actions from transcript

- Monitor and advocate for sensible gun control policies (implied)
- Support community programs that address root causes of violence (implied)

### Whats missing in summary

Detailed examples and further explanations on the impact of prohibition on demand and availability of restricted items.

### Tags

#Firearms #Prohibition #GunControl #Demand #Regulations


## Transcript
Well howdy there internet people, it's Beau again.
So tonight we're going to talk about how the world will always get you what you want.
There was a firearm, a pistol, it's called a Glock 18C.
It was fully automatic.
If you don't know anything about firearms, it was a machine gun.
Bullets kept coming out as long as you held the trigger down.
As opposed to a semi-automatic where you pull the trigger and one bullet comes out until
you let it go and pull it again.
So because of the laws that were already on the books and the time this thing hit market
and a whole bunch of other things, this was basically just legislated out of existence.
You couldn't get them on the normal civilian market.
They weren't really available on the black market, mainly because of the demand.
It didn't exist.
Nobody really wanted this thing.
Not the only way you could realistically get one was if you were a class 3 firearms dealer,
a legal firearms dealer, and you had a buddy who was a sheriff or a cop, a police chief,
not just a normal cop or deputy.
And he wrote a letter saying that he would like to sample this weapon.
Then you could get your hands on one, but you really couldn't do anything with it.
There are a couple of other ways if you were lucky enough to find one that was made before
certain date and you had a federal fire there were there were a whole bunch of
things going on but realistically there was no way for the average person within
the United States who was not military, a cop, or a licensed firearms dealer to get
their hands on one. There wasn't a huge demand for them because it's a gimmick. I
don't know a lot of shooters that are like you know I want a pistol but I want
to shoot 1,200 rounds a minute.
That was the cyclic rate on this thing.
Now a normal automatic firearm, fully automatic firearm, they're not illegal in the United
States.
That's a common misconception.
You can get them, but you've got to jump through some hoops and they are really expensive.
We're talking luxury sedan expensive if you do it legally.
If you do it illegally, well they're about a tenth of that price and there's no weight.
You can pick them up the same day.
So the key thing I want you to remember is that this thing, for all intents and purposes
within the United States, it was gone.
It did not exist in civilian hands until now.
The ATF is currently trying to track down more than 3,000 conversion kits that were
were made to turn a normal semi-automatic Glock, the kind that every cop in the world
has and you can buy at any gun store, into the fully automatic one.
This was something that didn't have a market, but now the ATF knows there were at least
3,000 conversion kits that were sold.
You're thinking, kit like that, that's got to be a high dollar item.
1995, shipped to your door, ordered it on the internet.
You didn't need any special connections, didn't need to know anybody in the underworld.
No big deal.
Somebody figured out how to make it cheap.
In this case, the prohibition created the demand.
Nobody wanted the thing before then.
But when it was prohibited, it became cool.
So the market was created.
And that market was filled.
Who's buying these?
I don't know.
The ones they have recovered that they have talked about, well, they're a bunch of felons.
That's where they came from.
That two got their hands on them, which, like in most cases with guns, especially when you
prohibit something, that two ends up still being able to get their hands on them.
But the only people that would be interested in something like this, realistically, are
people engaged in a mass shooting drive-by, something like that, where you want to pump
out a bunch of rounds in a very short amount of time, and you need something concealable,
or that one idiot at the range that thinks that if he has every firearm in the world,
his member is longer.
That's it.
There's no real use for this thing.
But now it's flooded the market.
Now it's everywhere within the United States.
You'll be able to get them because it was banned.
Nobody wanted it before then.
It's almost like prohibition doesn't work.
It never has.
It never will.
When you take something that is, in essence, a victimless crime, possessing this, there's
no victim.
There's no victim to owning it.
And you make it illegal, all you do is make people want it.
That's what's happened here.
Prohibition doesn't work with guns.
It doesn't work with drugs.
It's not going to work with abortion.
It is what it is.
There will always be somebody willing to fill that market and when you're talking about
something that is a moral gray area for most people, it's going to be filled very quickly.
This isn't a crime that has a victim.
You can't name the person that's harmed by this.
That's the problem.
Focusing on prohibiting items like this is always a losing proposition.
In fact, you're going to do this.
You're going to create the demand for it.
You're going to make it more accessible to those you don't want to have them.
That's what's going to happen and that is what happened.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}