---
title: Let's talk about the effects of the tariff on Mexico....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=fjR5mi532e0) |
| Published | 2019/05/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's tariffs on Mexico, 5% on everything, will increase the cost of fruits and veggies from Mexico, ultimately paid by the American consumer.
- Adrian Wiley, a Florida man, criticized Trump's tariffs in a unique way, shedding light on the impact on consumers.
- Despite the humor, the tariffs will lead restaurants and grocers to seek alternative sources for produce.
- The tariffs may inadvertently benefit American farmers, creating a competitive edge and increasing demand for their products.
- This increased demand will require more labor, potentially incentivizing illegal border crossings.
- Trump's belief that Mexico should prevent its citizens from leaving undermines the concept of asylum and individual freedom.
- Trump's push for a border wall contradicts his stance on freedom of movement, as walls work both ways.

### Quotes

- "You actually end up paying for this."
- "Congratulations President, you have just increased demand and incentive to cross the border illegally."
- "Walls work both ways y'all need to remember that."

### Oneliner

Trump's tariffs on Mexico will raise costs for American consumers, potentially benefiting farmers but also incentivizing illegal border crossings, undermining asylum claims, and revealing contradictions in border policies.

### Audience

American consumers and those concerned about border policies.

### On-the-ground actions from transcript

- Support local farmers by purchasing their produce (suggested)
- Advocate for fair trade policies that benefit consumers and farmers (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the impact of Trump's tariffs on Mexico, American consumers, farmers, and border policies.


## Transcript
Well, howdy there, internet people, it's Bo again.
So I guess we gotta talk about Trump's tariffs.
The new ones he's slapping on Mexico, 5% on everything.
I don't think I could put it any better than Adrian Wiley.
He was a candidate for governor here in Florida.
And let me tell you, he is Florida man, down to a T.
This is a guy who, he didn't like the Real ID Act.
So he surrendered his driver's license, drove around trying to get arrested for driving
without a license.
Anyway, cops wouldn't arrest him because they knew what he was up to.
They can recognize Florida man when they see one.
They didn't want to spend the next five years going to testify in court as he fought it.
To this day, I don't know that he has a license or if he ever got arrested or not.
But anyway, he put it into perfect terms.
We get about half our fruits and veggies from Mexico.
So the cost of these things is going to go up 5%
because that's how tariffs work.
The consumer, you, actually end up paying for this.
Now the American consumer is going to be upset by this.
It is going to make restaurants and grocers
look for other sources.
So for once, there's a good thing that comes from one of Trump's temper tantrums.
The American farmer is going to have a competitive edge.
That's cool.
There's going to be increased demand for the American farmer, which means they're going
to need more labor.
Who are they going to hire?
Congratulations President, you have just increased demand and incentive to cross the border illegally.
You are a very stable genius sir.
Now while all that's funny, that's all well and good, I just want to point out the reason
he's doing this is because he believes that the Mexican government is not
keeping people in their country well enough. He is once again asserting that
it is government's job to stop you from leaving. It is once again he is saying
government has the authority to keep you in your home country no matter what. Even
if you're gonna go to another country and claim asylum, which by the way that's
illegal but either way that's what he is asserting. Now I want to remind everybody
this is the guy who wants to build a wall along our southern border. Walls
work both ways y'all need to remember that. Anyway it's just a thought y'all
I'll, uh, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}