---
title: Let's talk about crying babies, Trump, and the poverty level....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=c_VIOZWhR6o) |
| Published | 2019/05/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recounts a story from when he was 20 about a friend's ex showing up with four kids, only one of whom the friend knew, and leaving them with no food or supplies.
- Stayed with the kids while his friend went to get baby formula, learning that a hungry baby won't stop crying no matter how much you try to soothe them.
- Criticizes the Trump administration for redefining poverty without actually improving people's situations through the Chained CPI method.
- Explains how Chained CPI manipulates statistics to make it seem like people are lifted out of poverty when in reality they lose benefits and struggle.
- Points out that with the current economy, even a slight increase in income can disqualify individuals from receiving necessary benefits.
- Shares statistics on the financial struggles of Americans, such as the inability to find good-paying jobs, pay credit card balances, or afford healthcare.
- Notes that while the economy may seem great for some, the majority of Americans are living paycheck to paycheck and cannot handle emergencies.
- Expresses the importance of not just focusing on the top earners but addressing the needs of the bottom percentile who significantly impact society.
- Suggests that cutting benefit programs leads to an increase in crime as people resort to illegal activities to survive.
- Advocates for real solutions to address the financial hardships faced by a large portion of the population instead of manipulating data for political gain.

### Quotes

- "Babies don't stop crying when they're hungry. What are they going to do?"
- "These benefit programs, entitlement programs, as people sometimes like to call them, that's just crime insurance at this point because if you get rid of them, you're going to see an uptick in crime."
- "Maybe it would be better if we didn't cook the books and we actually took some steps to try to fix the fact that 80% of Americans are living paycheck to paycheck."

### Oneliner

Beau shares a personal story, criticizes the manipulation of poverty statistics, and advocates for real solutions to address financial struggles faced by Americans.

### Audience

Concerned citizens

### On-the-ground actions from transcript

- Advocate for policies that genuinely address poverty and financial hardships (implied)

### Whats missing in summary

The emotional impact of witnessing a hungry baby crying and the urgency to address the financial struggles of a majority of Americans.

### Tags

#Poverty #Economy #FinancialStruggles #ChainedCPI #CommunityConcerns


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight, we're gonna start off with a story.
When I was about 20, went over to my buddy's house,
we were gonna go out that night, his ex showed up,
who has never been nominated for Mother of the Year,
with four kids, only one of which he knew.
It was a little guy.
She brought nothing with them, no food, juice cups, anything like that, and she took off.
Now five minutes later, of course the youngest one is hungry, the little guy.
Now at this point in my life I would not even know what to look for going to the store to
buy a baby formula. So I stayed there with the kids when he ran to town. To town and
back was about 40-45 minutes back then. There's one thing I learned that day. It doesn't matter
how much you try to play with him, how much you hold him, talk to him. If a baby is hungry,
He will not stop crying.
That's what I learned that day.
So good news, though.
The Trump administration is going to pull a whole bunch of people out of poverty.
They're not actually going to do anything to better their situation.
They're just going to redefine what poverty is.
After two hours of reading, what I've basically found out is that they're going to go to something
called Chained CPI. They want to. In English what Chained CPI is, it means
that cost of living increases, inflation, things like this. They're not really
counted the way they should be. So it appears that a whole bunch of people
came out of poverty when in reality they're in the exact same situation they
were they're just now not getting benefits so it's a great way to not only
give you a great false statistic for your campaign but also you know go ahead
and kick the little guy who's in trouble. The way this would work right now if you
got a family of four, I think $25,900 is poverty level.
So with the adjustments, if you're making that, you're no longer getting benefits.
More importantly, with the way our economy is actually going, if you make $26,000 a year,
Next year, because your wages won't go up, but cost of living will, you won't get help
either.
The money that you've paid in for these programs, you're not going to get back.
Now $25,900 is our current poverty level for that.
However if you ask the average American what you need to be out of poverty with a family
they're going to tell you $55,000 a year. Twice as much. 48% of Americans feel
they cannot find a good job with decent wages. 42% cannot pay their credit card
balance. 137 million are struggling to pay for health care and 80% are living
paycheck to paycheck. That is the actual state of the economy. I know there's a
whole bunch of news articles saying that the economy is doing great and it is.
for those people at the top, for everybody else not so much, for 80% of
Americans. I read something saying that most Americans could not handle a $400
emergency. So that's where we're really at and rather than actually attempting to
you solve these problems, we're just going to redefine it,
going to cook the books, like some kind man who found a way
to run a casino out of business.
See, the problem with looking at the top
is that they're not the important ones.
And I don't mean that in the philosophical sense
of you judge a country by how it treats its worst off.
I mean that in the sense of those at the bottom shape a lot of things, a lot of things.
Babies don't stop crying when they're hungry.
What are they going to do?
Probably won't be legal, whatever it is, and can you blame them?
I mean really?
That baby's not going to stop crying.
have to find a way to feed him. Armed robbery? Dealing drugs? What? I'm not sure
that I can really fault it either. I understand it's not right, but I dealt
with it for 45 minutes and remember it all this time later.
These programs, because of the way our economy is, because our capitalist class is not acting
like capitalists, they're not actually investing and creating jobs, although we constantly
call them job creators, these benefit programs, entitlement programs, as people sometimes
like to call them, that's just crime insurance at this point because if you get rid of them,
you're going to see an uptick in crime.
You're going to have to.
They have to find a way to feed that crying baby.
Maybe it would be better if we didn't cook the books and we actually took some steps
to try to fix the fact that 80% of Americans are living paycheck to paycheck.
That might be a better starting point than just trying to come up with a fake statistic
for your campaign.
Anyway it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}