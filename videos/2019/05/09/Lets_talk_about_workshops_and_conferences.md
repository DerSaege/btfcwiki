---
title: Let's talk about workshops and conferences....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Pj4RAw_HX1g) |
| Published | 2019/05/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Workshops and conferences are common among like-minded individuals to exchange ideas and tactics.
- Workshops typically piggyback on activist events, making it challenging for outsiders to know about them.
- Conferences are widely advertised but can be cost-prohibitive, lasting a week and held in remote locations.
- Beau has been hesitant to attend conferences due to high costs and exclusivity, feeling they cater to a niche group.
- A new conference in a major U.S. city with diverse speakers, affordable tickets, and a clear purpose of community action has caught Beau's interest.
- The upcoming conference focuses on practical actions for community building and force multiplication.
- Beau will be speaking at the conference on July 21st, encouraging those who resonate with his videos to attend.
- The conference will feature speakers like G. Edward Griffin, independent journalists, and activists discussing various topics.
- Attendees can use the promo code BOWE for a discount on tickets and a chance to upgrade to VIP by sharing the event on social media.
- Beau invites his audience to join him at the conference and hopes it will be an eye-opening experience for many.

### Quotes

- "All of a sudden, I'm out of excuses not to do it, and I guess I'm going to Vegas, baby."
- "If you watch the videos about the world I want or how freedom will come to America, and you're sitting there going, yeah, yeah, you need to go."
- "Here you go. Y'all have a good night. Hope to see you there."

### Oneliner

Beau invites his community to an inclusive and purpose-driven conference focusing on community action and networking, offering discounts and VIP upgrades for attendees who share on social media.

### Audience

Community members, activists

### On-the-ground actions from transcript

- Attend the community-focused conference in a major U.S. city (Suggested)
- Use promo code BOWE for a ticket discount and a chance to upgrade to VIP by sharing on social media (Implied)

### Whats missing in summary

Details on the specific topics and speakers at the conference

### Tags

#CommunityAction #Networking #Conference #Activism #VIPUpgrade #Discount


## Transcript
Well howdy there internet people, it's Beau again.
So tonight we're going to talk about workshops and conferences.
People of my ideological bent, we hold workshops all the time.
I probably did 10 a year up to recently.
Problem with them is why they are great for exchanging ideas, learning new tactics, stuff
like this, they tend to be pop-up and done on the fly at other activist events.
They piggyback on a protest or a march or whatever.
What that means is if you're not already within the community, you don't even know what happened.
And then we do these conferences, and these conferences are pretty widely advertised,
but I was certain I was never going to have to do one even though I've been invited a
a few times because, well, I don't really like the way they're done.
And this isn't to fault the guys that do them.
I understand why they're set up the way they are, all of that.
It's just not for me.
One of the problems I've had with them is that they are cost prohibitive.
Tickets are several hundred dollars.
They last a week.
And they're normally in a remote location, and when you take all of this together, what
that means is that if you go to one of these things, you're already in the club.
You're already pretty committed to this set of beliefs.
So I'm not going to be winning a lot of hearts and minds there.
So it didn't, never seemed like a good use of my time, and I got a buddy that goes to
one that happens in Acapulco every year and it's a week long and he drops four
to seven thousand dollars on these trips. If you're going to spend that kind of
cash you're already very committed. Now not all that was on the conference some
of that is because well he's kind of goofball but the point remains the other
Another issue I've had with them is that they cater to a niche.
It's not wide enough for the whole community.
For people who are engaged in using cryptocurrency, or for people who think like me, but are very
conservative socially, I know that sounds weird, but it exists, and most of them lack
a purpose beyond networking, you know, and again, I just never really felt like I needed
to go to them.
And then this week I get a call and some activists have decided to put one together in a major
U.S. city that is close enough to other major U.S. cities for people to drive in.
If you want to do the VIP granddaddy ticket set or whatever, it's less than $200.
It's only a couple days long.
The speaker list is very diverse.
And it has a purpose.
purpose is, what are things you can do to start tomorrow in
your own community?
Basically, all that network building and force
multiplication that we talk about in these videos, well,
that's the theme of the conference.
So all of a sudden, I'm out of excuses not to do it, and I
guess I'm going to Vegas, baby.
I'll be speaking on July 21st, so who should go to this thing?
If you watch the videos about the world I want or how freedom will come to America,
and you're sitting there going, yeah, yeah, you need to go, because you're already part
of the community, you just don't know it yet, I have recently found out that there are a
a lot of disaffected three percenters who are watching me who really like the general
idea but the problem is I'm such a whiny touchy feely snowflake kind of guy that you're just
not interested.
You need to go because there are going to be some other speakers there that you're interested
in.
If you have fallen down the rabbit hole a few times you probably read a book called
a creature from Jekyll Island.
The author of that, G. Edward Griffin, he's going to be there.
If you're interested in independent journalism,
there's going to be just a giant list of them.
So let's see.
Larkin Rose will be there, G. Edward Griffin.
Carrie Wedler, who you guys have seen in videos on this channel,
she's going to be there as well.
The news to share is sending one of their guys, I was hoping it was going to be Alex,
but last time I saw him he was holding a sign out of the window of the Venezuelan embassy
that says journalists need cigarettes.
I guess they're sending Ford Fisher, and I mean, he's kind of okay, long-running joke.
David Rodriguez, who's going to talk about homeschooling and unschooling, these types
things, and then just this giant list of independent journalists.
There's going to be, of course, with this community, there's going to be a party.
I think that for some people, this might be really eye-opening.
Now I'm going to put a link below to order tickets and get a little bit more information
about it.
If you use the promo code BOWE, you'll get a discount of some kind, I want to say it's
10%.
And the main difference between the VIP thing and the regular tickets, from what I understand,
is that there's going to be a party that you get to go to and free wine or something like
that.
You'll have to read it.
In addition to that, if you share this video with the hashtag BoVegas2019 on your social
media and you have bought the entrance ticket, you stand a chance of being upgraded to the
VIP thing.
So this was a little bit more of a commercial, I guess, than one of my normal videos, but
people have asked, you know, when I'm going on the road,
I'm like, well, probably not.
Here you go.
So anyway, it's just a thought.
Y'all have a good night.
Hope to see you there.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}