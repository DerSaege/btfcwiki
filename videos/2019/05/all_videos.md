# All videos from May, 2019
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2019-05-31: Let's talk about the effects of the tariff on Mexico.... (<a href="https://youtube.com/watch?v=fjR5mi532e0">watch</a> || <a href="/videos/2019/05/31/Lets_talk_about_the_effects_of_the_tariff_on_Mexico">transcript &amp; editable summary</a>)

Trump's tariffs on Mexico will raise costs for American consumers, potentially benefiting farmers but also incentivizing illegal border crossings, undermining asylum claims, and revealing contradictions in border policies.

</summary>

"You actually end up paying for this."
"Congratulations President, you have just increased demand and incentive to cross the border illegally."
"Walls work both ways y'all need to remember that."

### AI summary (High error rate! Edit errors on video page)

Trump's tariffs on Mexico, 5% on everything, will increase the cost of fruits and veggies from Mexico, ultimately paid by the American consumer.
Adrian Wiley, a Florida man, criticized Trump's tariffs in a unique way, shedding light on the impact on consumers.
Despite the humor, the tariffs will lead restaurants and grocers to seek alternative sources for produce.
The tariffs may inadvertently benefit American farmers, creating a competitive edge and increasing demand for their products.
This increased demand will require more labor, potentially incentivizing illegal border crossings.
Trump's belief that Mexico should prevent its citizens from leaving undermines the concept of asylum and individual freedom.
Trump's push for a border wall contradicts his stance on freedom of movement, as walls work both ways.

Actions:

for american consumers and those concerned about border policies.,
Support local farmers by purchasing their produce (suggested)
Advocate for fair trade policies that benefit consumers and farmers (implied)
</details>
<details>
<summary>
2019-05-30: Let's talk about Hulk, China, rare metals, and Trump keeping his promises.... (<a href="https://youtube.com/watch?v=cx6XjvMYrWE">watch</a> || <a href="/videos/2019/05/30/Lets_talk_about_Hulk_China_rare_metals_and_Trump_keeping_his_promises">transcript &amp; editable summary</a>)

China issues a warning to the US amidst trade tensions, signaling potential economic impacts and environmental concerns, with long-lasting repercussions.

</summary>

"China has issued a catchphrase that has led to shooting wars throughout history."
"The decisions that Trump is making are going to have long, long lasting and far reaching impacts in the economy, and they're not good ones."
"We're all gonna end up paying for this."

### AI summary (High error rate! Edit errors on video page)

China has a history of issuing a catchphrase, "don't say we didn't warn you," that has led to shooting wars.
The catchphrase has been recently issued by China to the United States, but it's likely related to the trade war rather than escalating to an actual war.
China is considering cutting off or reducing exports of rare earth minerals in response to Trump's trade war.
Rare earth minerals are vital for high-tech goods and China controls about 85% of the market.
The trade deficit is not inherently bad, but Trump is using tariffs as a solution, which ultimately affects American consumers.
Trump's focus on keeping campaign promises may lead to ineffective solutions that impact the economy negatively.
The trade war won't bring back American manufacturing jobs but might shift them to other countries like Vietnam.
The decision to move jobs abroad is driven by economic factors and the cost benefits of operating in countries with less stringent environmental regulations.
Mining rare earth minerals has severe environmental impacts, as seen in China where they are currently mined.
Trump's decisions regarding the trade war will have lasting negative impacts on the economy and future presidents will likely face the consequences.

Actions:

for global citizens, policymakers,
Research the environmental impacts of mining rare earth minerals (suggested)
Advocate for environmentally responsible practices in mining (exemplified)
</details>
<details>
<summary>
2019-05-29: Let's talk about a third of migrants lying about their kids.... (<a href="https://youtube.com/watch?v=oYhj2GMahHo">watch</a> || <a href="/videos/2019/05/29/Lets_talk_about_a_third_of_migrants_lying_about_their_kids">transcript &amp; editable summary</a>)

Beau debunks the false narrative that a third of migrants lied about their children, exposing flawed statistics and calling out the harmful impact of sharing misinformation online.

</summary>

"If you wouldn't lie to stop a kid from getting raped, you're a pretty horrible person."
"This is made up and 145,000 people shared one version of this stupid story."

### AI summary (High error rate! Edit errors on video page)

Beau addresses a headline claiming that a third of migrants lied about their family status with their children, expressing concern and calling for an investigation.
He points out that people are sharing the headline without critically examining it.
Beau clarifies that the 30% mentioned in the headline was rounded up to a third, but the actual percentage was 30%.
The sample used in the study was not representative of all migrants, but specifically those suspected by ICE of lying about family relationships.
ICE's DNA testing method to verify family relationships does not account for step parents, legal guardians, or other complex family situations.
He criticizes ICE for being wrong 70% of the time in cases where they suspected migrants of lying about family relationships.
Beau questions the ethics of sharing a false narrative that demonizes migrants and perpetuates harmful stereotypes.
He underscores the dangers faced by unaccompanied minors who end up in the custody of Health and Human Services, where reports of sexual abuse are prevalent.
Beau concludes by debunking the manufactured story and criticizing the 145,000 people who shared it without verifying its accuracy.

Actions:

for online activists,
Fact-check and verify information before sharing online (implied)
Advocate for the protection of unaccompanied minors and migrant families (implied)
Combat misinformation by educating others on how to critically analyze news stories (implied)
</details>
<details>
<summary>
2019-05-28: Let's talk about Tank Man and somebody.... (<a href="https://youtube.com/watch?v=BF3NU0bgDk8">watch</a> || <a href="/videos/2019/05/28/Lets_talk_about_Tank_Man_and_somebody">transcript &amp; editable summary</a>)

Beau describes a scenario resembling Tiananmen Square, urging viewers to recognize their potential as change-makers in the face of adversity.

</summary>

"If there's ever a metaphor for the power of the state versus the individual, it's that image."
"A million people with popular support were all waiting for somebody. You are somebody."

### AI summary (High error rate! Edit errors on video page)

Describes a scenario where graduates are worried about job prospects due to a changing economy and negative media portrayal.
Mentions attacks on freedom of press, assembly, and speech, with intellectuals being demonized.
Portrays a protest that grows exponentially and faces logistical problems like food and hygiene issues.
Describes government response with tanks, armored personnel carriers, and a quarter million troops.
Points out the role of surrounding suburbs in nonviolently blocking the government from reaching the protesters.
Notes infighting among movement leadership and missed opportunities for reform.
Describes a government crackdown on protesters with tanks and rifles, resulting in casualties.
Talks about the iconic Tank Man image from Tiananmen Square, symbolizing the power struggle between the state and the individual.
Emphasizes the courage and defiance displayed by Tank Man in standing up to the tanks.
Concludes by urging viewers to recognize their own potential as change-makers in the face of adversity.

Actions:

for young activists,
Support movements for freedom and democracy (exemplified)
Stand up against government oppression (exemplified)
Be prepared to take courageous action in the face of tyranny (exemplified)
</details>
<details>
<summary>
2019-05-27: Let's talk about film cameras, social media, and political debate.... (<a href="https://youtube.com/watch?v=cVQ8xAZt2pg">watch</a> || <a href="/videos/2019/05/27/Lets_talk_about_film_cameras_social_media_and_political_debate">transcript &amp; editable summary</a>)

Beau draws parallels between old film cameras and social media filters, criticizes groupthink in political discourse, challenges rigid labels in the abortion debate, and calls for nuanced opinions beyond social media slogans.

</summary>

"It's overexposed so there's no independent thought, there's no ideas being developed in the dark room."
"Pro-life and pro-choice, right? Means nothing. Those terms mean nothing."
"We want a 30-second sound bite on the nightly news and a tweet."
"If we can't even accomplish that, you can give up putting it into practice."
"It might be time to form opinions in private before introducing them to social media."

### AI summary (High error rate! Edit errors on video page)

Draws parallels between old film cameras and social media filters, likening overexposure in both to the loss of depth and nuance.
Criticizes how social media shapes political discourse, particularly among young people who are constantly under peer scrutiny for online behavior.
Points out how groupthink and lack of independent thought harm debates and limit the development of ideas.
Gives an example of a law in Alabama that, despite its origins in bigotry, actually makes it easier for same-sex couples to marry.
Challenges the rigid labels and group mentalities in the abortion debate, illustrating how they stifle nuanced understanding and critical thinking.
Analyzes a controversial image on social media related to abortion, showcasing extreme reactions from both sides.
Raises awareness about the importance of exploring gray areas in debates and forming well-thought-out opinions beyond simplistic slogans and hashtags.

Actions:

for social media users,
Form opinions in private before sharing on social media (implied)
Challenge groupthink by engaging in nuanced debates (implied)
Encourage independent thought and depth in online discourse (implied)
</details>
<details>
<summary>
2019-05-26: Let's talk about the dos and don'ts of Memorial Day.... (<a href="https://youtube.com/watch?v=i987Ke9CzPE">watch</a> || <a href="/videos/2019/05/26/Lets_talk_about_the_dos_and_don_ts_of_Memorial_Day">transcript &amp; editable summary</a>)

A guide on navigating Memorial Day sensitively, from recognizing diverse veteran perspectives to honoring the fallen and advocating against unjust wars.

</summary>

"Memorial Day is a day to remember the fallen. It's a day to remember those lost. It's not a happy day. It is not a happy day."
"Just leave it at that. I'm sorry for your loss."
"Make sure that those who are active duty today get to celebrate Veterans Day and not get remembered on Memorial Day."
"You've got a voice, use it."
"We stop losing people, we don't need to remember."

### AI summary (High error rate! Edit errors on video page)

A veteran asked Beau to provide a do's and don'ts of Memorial Day, but Beau initially felt he wasn't the right person for it.
Veterans already familiar with Memorial Day's significance, but the general public may not be aware due to lack of discourse by politicians.
Beau acknowledges the diversity of perspectives among veterans, contrary to a singular portrayal.
Armed Forces Day, Veterans Day, and Memorial Day serve different purposes: active duty appreciation, all who served recognition, and honoring the fallen, respectively.
Memorial Day is a solemn occasion to commemorate those who have died in service.
Beau advises against saying "Happy Memorial Day" as it may not be appropriate given the solemnity of the occasion.
Younger veterans from recent conflicts may still be dealing with emotional wounds, so sensitivity is key.
Beau stresses the importance of not asking certain questions to veterans, particularly about combat experiences or killing others.
Gold Star families, who have lost a family member in service, should be acknowledged with a simple "I'm sorry for your loss" without trying to relate their loss to a cause.
Beau suggests allowing veterans to enjoy Memorial Day without feeling the need to relate to their experiences.
Some veterans believe kneeling during the National Anthem on Memorial Day is a way to honor those unjustly killed by law enforcement.
Beau encourages advocating against unjust wars and ensuring that active-duty personnel get celebrated on Veterans Day instead of being remembered on Memorial Day.

Actions:

for community members,
Acknowledge Gold Star families with a simple "I'm sorry for your loss." (implied)
Advocate against unjust wars and ensure active duty personnel are celebrated on Veterans Day. (implied)
</details>
<details>
<summary>
2019-05-25: Let's talk about baking a cake.... (<a href="https://youtube.com/watch?v=FoP7q6zrS-Q">watch</a> || <a href="/videos/2019/05/25/Lets_talk_about_baking_a_cake">transcript &amp; editable summary</a>)

Beau humorously navigates a disagreement about baking a birthday cake, turning absurd with comparisons to cake murder and Hitler.

</summary>

"Well, okay, so what you're saying then is that all the cakes that made it that had less than perfect ingredients. We should just round all of them up and throw them away too. Cake murderer."
"Hitler didn't like cakes either."
"That's not even a thing, and that's not even a cake! It will be if you take responsibility for your actions."
"You obviously didn't love it. Actually I did love it. It was strawberry cheesecake and I don't want any more cake."
"You can shove this icing where the sun doesn't shine."

### AI summary (High error rate! Edit errors on video page)

Beau introduces the scenario of being married to someone who behaves like people in the comment section, illustrating a pro-choice female married to a pro-life male dynamic.
The humor and absurdity escalate as the couple's disagreement unfolds over baking a cake for the wife's birthday.
The husband pushes for baking the cake, disregarding his wife's concerns about allergies and her lack of desire for cake.
The wife tries to end the cake debate multiple times, but the husband insists on baking it, leading to a comical argument.
The husband's persistence leads to extreme statements comparing the wife to a cake murderer and Hitler for not wanting to bake the cake.
The disagreement culminates in a ridiculous exchange about icing and the wife leaving the situation.

Actions:

for couples,
Bake a cake for a loved one's birthday (exemplified)
Share cakes with neighbors (implied)
</details>
<details>
<summary>
2019-05-24: Let's talk about how the world will always get you what you want.... (<a href="https://youtube.com/watch?v=jxc-7SE8Ev8">watch</a> || <a href="/videos/2019/05/24/Lets_talk_about_how_the_world_will_always_get_you_what_you_want">transcript &amp; editable summary</a>)

Beau explains how prohibition fuels demand for restricted items like firearms, showing why it fails without victims.

</summary>

"Prohibition doesn't work with guns. It doesn't work with drugs. It's not going to work with abortion."
"Focusing on prohibiting items like this is always a losing proposition."
"When you take something that is, in essence, a victimless crime, possessing this, there's no victim."

### AI summary (High error rate! Edit errors on video page)

Explains the rarity and unique features of a Glock 18C, a fully automatic firearm with a rapid cyclic rate of 1200 rounds per minute.
Describes the strict regulations that essentially made the Glock 18C non-existent for civilians in the United States.
Mentions the illegal conversion kits that can turn a semi-automatic Glock into a fully automatic one, which are now being tracked by the ATF.
Points out that prohibition often creates demand for restricted items, as seen with the Glock 18C conversion kits.
Argues against prohibition of certain items like firearms, drugs, and abortion, stating that it only increases their desirability without a clear victim.

Actions:

for policy makers, activists,
Monitor and advocate for sensible gun control policies (implied)
Support community programs that address root causes of violence (implied)
</details>
<details>
<summary>
2019-05-23: Let's talk about how men are from memes and women are from very long posts.... (<a href="https://youtube.com/watch?v=bSuCtoAZDcw">watch</a> || <a href="/videos/2019/05/23/Lets_talk_about_how_men_are_from_memes_and_women_are_from_very_long_posts">transcript &amp; editable summary</a>)

Beau sheds light on the influence of social media posts on real-life perceptions and warns against the limitations of sharing memes, advocating for meaningful communication over superficial interactions.

</summary>

"Your social media posts are seen by people in your real life, and it shapes their opinion of you."
"Stop using memes. It limits your thought."
"She saw something. She understood something that you didn't get."
"Stop letting other people define your thoughts like that because this is what happens."
"It's an important lesson to learn."

### AI summary (High error rate! Edit errors on video page)

Speaking in generalities, discussing the impact of social media posts on real-life perceptions.
Sharing personal experiences related to the Kavanaugh hearings and the abortion debate.
Emphasizing the importance of being mindful of the messages conveyed through social media comments.
Explaining how a seemingly harmless meme on abortion led to a relationship ending.
Pointing out that sharing memes can limit one's thought process and influence others' opinions.
Advising against using memes and encouraging open, direct discussions instead.
Warning against the implications of sharing content without fully understanding its message.
Stressing the significance of being aware of how one's online actions shape others' perceptions in real life.
Encouraging deeper understanding and communication rather than relying on superficial social media posts.
Concluding with a reminder to be thoughtful about the messages shared online.

Actions:

for social media users,
Have open, direct discussions instead of relying on memes (suggested)
Be mindful of the messages shared online and their potential impact on real-life perceptions (implied)
</details>
<details>
<summary>
2019-05-22: Let's talk about the Russian plan, undertows, and national security.... (<a href="https://youtube.com/watch?v=4x9cflMxahQ">watch</a> || <a href="/videos/2019/05/22/Lets_talk_about_the_Russian_plan_undertows_and_national_security">transcript &amp; editable summary</a>)

Beau explains a Russian plan to trigger an insurgency in the US using black Americans reveals systemic racial issues needing urgent attention, urging action against racism.

</summary>

"You want to make America great? You got to stop racism."
"No self-appraisal will ever be as valuable as your opposition's appraisal."
"Addressing racial disparities in this country is no longer just the moral thing to do. It's a national security issue."
"Countering racism is now a national security issue."
"They identified them two years ago."

### AI summary (High error rate! Edit errors on video page)

Beau starts by sharing a personal story about taking his kid to the beach and facing an undertow, using it as an analogy to explain a complex situation.
The Russian documents aren't a government plan but were created by a group of civilian contractors who pitched it to their handler and then to the government.
The plan originated from a group of intelligence professionals contracted by the Internet Research Agency (IRA) or possibly Wagner.
The plan is to trigger an insurgency within the United States using black Americans, causing racial discord.
The strategy targets a specific demographic - black Americans who make up about 12% of the population and have high incarceration rates and economic disparities.
Beau explains the importance of choosing a group with a high incarceration rate for radicalization.
By alienating and persecuting black Americans further, it becomes easier to radicalize them.
Beau stresses that the question should be why intelligence professionals chose this demographic, pointing out systemic racial issues that need addressing.
Addressing racial disparities is not just a moral issue but a national security concern as targeted groups are easier to radicalize.
Beau concludes by stating that countering racism is now a national security issue and urges for action against it.

Actions:

for policy makers, activists,
Address systemic racial issues in your community (implied)
Advocate for policies that counter racism and support marginalized communities (implied)
</details>
<details>
<summary>
2019-05-21: Let's talk about pardoning war criminals.... (<a href="https://youtube.com/watch?v=ppRdK82vjfI">watch</a> || <a href="/videos/2019/05/21/Lets_talk_about_pardoning_war_criminals">transcript &amp; editable summary</a>)

Beau addresses specific cases of war criminals, expressing doubts about pardons and advocating for professional conduct, stressing Memorial Day's significance for the fallen.

</summary>

"Memorial Day is not Veterans Day. Memorial Day is for those who were killed."
"If you want to behave like that, there's a whole bunch of places, whole bunch of locations, whole bunch of firms that you can go to and you can work like that and you'll end up at the end of the machete just like everybody else."
"As a contractor, you are supposed to be a professional, not open fire on crowds, ever, period, full stop, end of story."

### AI summary (High error rate! Edit errors on video page)

Addressing the issue of pardoning war criminals after returning from a break on Facebook.
Responding to messages about opinions on specific cases involving contractors.
Describing a situation where an Army lieutenant interrogated a suspected bomb maker and ended up killing him in a physical altercation.
Mentioning the ambiguity surrounding evidence in the case and the lieutenant's intention.
Pointing out that incidents like these, although not right, do happen.
Explaining that the lieutenant had already served his time in prison before being pardoned.
Expressing understanding towards the lieutenant's emotional reaction leading to the death.
Moving on to discussing potential pardons for a Navy chief accused of heinous acts in villages.
Emphasizing the significance of SEALs turning in the accused Navy chief as a grave indicator.
Expressing disbelief in the Navy chief's deservingness of a pardon due to his actions aiding the insurgency.
Mentioning another case involving an Army Major killing a suspected bomb maker, casting doubt on the need for a pardon.
Touching on the case of Marines urinating on opposition KIAs, condemning their actions but suggesting that they have already faced consequences.
Acknowledging the impact of the Marines' actions in aiding insurgency propaganda.
Stating that criminal justice consequences may not match the burden of living with the outcomes of their actions.
Concluding with strong disapproval of a contractor behaving unprofessionally by opening fire on crowds.
Arguing that the contractor should have faced justice under Iraqi jurisdiction rather than being brought back to the US.
Asserting that such behavior is unacceptable in a professional setting.

Actions:

for war critics and advocates,
Contact Iraqi authorities to address the contractor's actions under their jurisdiction (suggested)
Educate on the distinctions between Memorial Day and Veterans Day to honor the fallen appropriately (exemplified)
Share Beau's message to raise awareness about the implications of pardoning war criminals (implied)
</details>
<details>
<summary>
2019-05-20: Let's talk about rights, kicking down, and punching up.... (<a href="https://youtube.com/watch?v=Zx0w0k0xurU">watch</a> || <a href="/videos/2019/05/20/Lets_talk_about_rights_kicking_down_and_punching_up">transcript &amp; editable summary</a>)

Beau reacts to a message questioning his support for abortion, urging people to care about the rights of others beyond themselves and warning against the manipulation of division for power.

</summary>

"Stop caring about things that impact you directly and only those things."
"You vote each other's rights away."
"Stop kicking down. Only punch up."

### AI summary (High error rate! Edit errors on video page)

Reacts to a message questioning his support for abortion due to having six kids.
Expresses surprise and introspection at the hypocrisy pointed out by the message.
Lists various causes he supports, despite not personally embodying all of them.
Urges people to care about the rights of others beyond those that directly impact them.
Warns against falling into the trap of division created by propaganda and those in power.
Criticizes the manipulation of people's rights in the name of democracy.
Acknowledges living by far-right ideals in his personal life but opposes forcing them on others.
Advocates for freedom by not using force to restrict others based on personal beliefs.
Condemns the idea of government having ultimate control over individuals' lives as oppressive.
Calls for a shift in mindset to stop fighting amongst each other and focus on real freedom.

Actions:

for activists, advocates, citizens,
Challenge yourself to care about causes that may not directly affect you (exemplified)
Advocate for the rights of marginalized groups in your community (exemplified)
Refrain from imposing personal beliefs on others through force (exemplified)
</details>
<details>
<summary>
2019-05-19: Let's talk about how I like my coffee, a new skill, and the lessons of history.... (<a href="https://youtube.com/watch?v=t8RNlkIkvCM">watch</a> || <a href="/videos/2019/05/19/Lets_talk_about_how_I_like_my_coffee_a_new_skill_and_the_lessons_of_history">transcript &amp; editable summary</a>)

Beau talks about his coffee preferences, learning about conducting abortions, and how a new law in Alabama might actually increase access and affordability while undermining government authority.

</summary>

"I like my coffee like I like my markets."
"It's almost like prohibition doesn't work."
"Never give an order that won't be followed."

### AI summary (High error rate! Edit errors on video page)

Shares how he likes his coffee, drawing parallels with his preference for markets.
Talks about living on the Alabama line and learning how to conduct abortions out of curiosity.
Mentions the misconception around back alley abortions and how things have changed.
Points out that 97% of abortions occur before 20 weeks, mostly done with a pill.
Explains the multi-use nature of the abortion pill and its availability.
Notes the advancement in technology making abortion procedures safer.
Mentions the availability of tools for the procedure at hardware stores.
Talks about the affordability of ultrasound machines and how they enhance safety.
Suggests that the new law in Alabama might increase access to abortions and make them cheaper.
Points out the lack of a paper trail or protesters when abortions are performed at home.
Mentions how this law could undermine government authority and resemble the failure of prohibition.
Concludes by stating that leadership should not give orders that won't be followed.

Actions:

for community members,
Contact local healthcare providers for information on abortion services (suggested)
Research affordable and safe abortion options in your area (implied)
</details>
<details>
<summary>
2019-05-17: Let's talk about choice, life, and statistics.... (<a href="https://youtube.com/watch?v=jgGpFYEMiw0">watch</a> || <a href="/videos/2019/05/17/Lets_talk_about_choice_life_and_statistics">transcript &amp; editable summary</a>)

Beau debunks myths, presents statistics, and challenges the notion of bodily autonomy in the abortion debate, urging society to prioritize ethics over morals or religion.

</summary>

"97%, remember that number."
"Consent for sex is not consent to carry a child."
"Poverty is a lack of cash. It's not a lack of character."
"Bodily autonomy, it is not your body."
"When does life exist and the rights of that new life trump the rights of the woman?"

### AI summary (High error rate! Edit errors on video page)

Addresses choice, life, and statistics related to abortion, debunking false information dominating headlines.
Mentions the religious issue surrounding abortion and references a passage in Numbers from the Bible.
Talks about the misconception around heartbeats in embryos and the size of embryos at six weeks.
Emphasizes the statistic that 97% of abortions occur before 20 weeks.
Argues against viewing abortion as a consequence, stating it's more of a punishment due to legislative restrictions.
Challenges the idea that consent for sex equals consent to carry a child.
Counters the belief that women use abortion as birth control, presenting statistics on abortion frequency.
Addresses the misconception that tax dollars fund abortions, clarifying the role of the Hyde Amendment.
Links abortion rates to poverty and criticizes the lack of focus on real societal issues.
Centers the abortion debate on bodily autonomy and challenges the notion of men's rights over women's bodies.

Actions:

for advocates for reproductive rights,
Advocate for comprehensive sex education in states with limited education on the topic (implied).
Support organizations that provide resources for women facing unplanned pregnancies (implied).
Challenge misconceptions and stigma surrounding abortion through education and open dialogues (implied).
</details>
<details>
<summary>
2019-05-16: Let's talk about what car accidents, child support, and bodily autonomy.... (<a href="https://youtube.com/watch?v=jDbn0sZN0co">watch</a> || <a href="/videos/2019/05/16/Lets_talk_about_what_car_accidents_child_support_and_bodily_autonomy">transcript &amp; editable summary</a>)

Beau draws parallels between bodily autonomy and obligations, stressing the simplicity of respecting individual choices.

</summary>

"It's her body. It's really that simple."
"Guys this is what it sounds like when you say, well if I don't have a say in whether or not she keeps the baby, then I shouldn't have to pay child support."

### AI summary (High error rate! Edit errors on video page)

Driving and texting led to a four car pile up, resulting in an old man on life support.
Beau suggested to the judge that the old man be taken off life support, but the family disagreed.
Beau believes he is not obligated to pay for the medical bills in this situation.
He draws a parallel to the argument of not wanting to pay child support if one doesn't have a say in the decision to keep the baby.
Beau points out the lack of understanding of bodily autonomy in the country.
He stresses that bodily autonomy is fundamental and simple—it's her body.
Beau leaves with the message that it's just a thought and wishes everyone a good night.

Actions:

for advocates for bodily autonomy,
Respect and support bodily autonomy (implied)
</details>
<details>
<summary>
2019-05-15: Let's talk about what picking a color can teach us about gender.... (<a href="https://youtube.com/watch?v=ijooHoBoey8">watch</a> || <a href="/videos/2019/05/15/Lets_talk_about_what_picking_a_color_can_teach_us_about_gender">transcript &amp; editable summary</a>)

Beau challenges gender norms, questions societal constructs, and advocates for acceptance and understanding of diverse gender identities.

</summary>

"Gender, that's what's between your ears."
"At the end of the day, sex, well that's between your legs."
"It's just a thought."
"Throughout history, what is viewed as acceptable within the societal norms for a gender has changed."
"Gender fluid people, people that want to cross over, maybe even to the point of identifying as a different gender."

### AI summary (High error rate! Edit errors on video page)

Painting yard toys and questioning gender norms.
Challenging societal constructs of pink for girls and blue for boys.
Introduces the concept of gender neutrality and non-binary identities.
Exploring historical gender norms like colors and clothing.
Examining the fluidity of gender norms and societal expectations.
Acknowledging the existence of gender fluid individuals.
Addressing the misconception of only two genders in the U.S.
Stating that accommodating gender identity isn't a national issue.
Criticizing the stigmatization and control of gender identities.
Comparing the treatment of gender identity to mental illness and lack of accommodations.
Questioning the inconsistency in accommodating mental illnesses.
Criticizing attempts to legislate and control people's sex lives.
Distinguishing between sex and gender, asserting it's not others' business.

Actions:

for all individuals,
Embrace gender diversity by educating yourself and others (implied).
Support and advocate for gender-neutral spaces and inclusive policies (implied).
Challenge gender stereotypes in daily interactions and choices (implied).
</details>
<details>
<summary>
2019-05-15: Let's talk about Iran.... (<a href="https://youtube.com/watch?v=QqXzZKtCFGc">watch</a> || <a href="/videos/2019/05/15/Lets_talk_about_Iran">transcript &amp; editable summary</a>)

A warning against blindly supporting a potentially disastrous and manufactured war driven by political agendas rather than national interest.

</summary>

"They're not fighting for freedom. They're fighting so Trump can be more reelectable."
"Before you say that you support the troops, you have to support the truth."
"This is a manufactured war."

### AI summary (High error rate! Edit errors on video page)

A young guy, just back from boot camp, is excited about the prospect of war with Iran, despite Trump's mixed signals.
The young guy missed out on Iraq and Afghanistan and is eager to go to war.
He's underestimating the potential troop numbers needed for a conflict with Iran.
The conflict was sparked by alleged sabotaged shipping boats in the Gulf of Oman, blamed on Iran without concrete evidence.
Senator Rubio and Tom Cotton are pushing for aggressive action against Iran.
There's a discrepancy between the threat level reported by Operation Inherent Resolve and CENTCOM regarding Iran.
Acting Secretary of Defense Shanahan discussed a plan to send 100,000 to 120,000 troops to Iran with Trump.
Beau warns that a war with Iran won't be like Iraq and could lead to disastrous consequences.
Iran's military is well-prepared for insurgency and technologically advanced, posing a significant challenge for any military action.
Beau points out that the real motivation behind a potential conflict with Iran may be to boost Trump's reelection chances, not national security.
He stresses that going into Iran half-heartedly will only lead to more conflict and loss of life without solving the root issues.
Beau questions the motives behind supporting a war that benefits Saudi interests over American lives.
He calls for supporting the truth rather than blindly backing a manufactured war.

Actions:

for concerned citizens, anti-war activists.,
Contact elected officials to express opposition to military action against Iran (implied).
Support organizations advocating for diplomatic solutions and peace (implied).
</details>
<details>
<summary>
2019-05-14: Let's talk about churchgoers, witches, and fear.... (<a href="https://youtube.com/watch?v=cV9YO7E_vgo">watch</a> || <a href="/videos/2019/05/14/Lets_talk_about_churchgoers_witches_and_fear">transcript &amp; editable summary</a>)

Beau talks about how fear is used to control and divide people, urging reflection on who benefits from keeping individuals afraid and divided in society.

</summary>

"They whisper in your ear and tell you what to be afraid of."
"Panic struck, terror."
"Nobody whispered that spell into your ear."
"Keep us kicking down or punching left and right rather than punching up."
"Turn them into cowards."

### AI summary (High error rate! Edit errors on video page)

Tells an anecdote about a civics teacher whispering words to divide students into groups based on seating, leading to unfair outcomes.
The teacher labeled himself as a "witch" in the seating chart, symbolizing the government's role in instilling fear and control.
Talks about fear and false accusations, particularly related to sexual assault and terrorism.
Mentions a dark video concept addressing the Alabama law, not shared due to its nature.
Expresses how fear is manipulated by those in power to divide and control people.
Compares fear of terrorists, school shooters, illegal immigrants, and other issues with statistical likelihoods.
Points out the manipulation of fear in elections and society to maintain control and division.
Urges reflection on who benefits from keeping individuals afraid and divided in society.

Actions:

for citizens, activists,
Challenge fear-mongering narratives (implied)
Support mental health initiatives to combat fear and anxiety (implied)
Advocate for policies based on facts, not fear (implied)
</details>
<details>
<summary>
2019-05-12: Let's talk about why rapists are going to be moving to Alabama.... (<a href="https://youtube.com/watch?v=ViDLhi-QvfM">watch</a> || <a href="/videos/2019/05/12/Lets_talk_about_why_rapists_are_going_to_be_moving_to_Alabama">transcript &amp; editable summary</a>)

Beau questions Alabama's flawed legislation that could lead to innocent victims of rape being imprisoned based on false accusations.

</summary>

"A careful rapist who doesn't leave physical evidence always walk and their victim will go to prison because Dickie Drake's friend, his ex-wife, said something."
"So he gets to rape her and then put her in prison?"
"You're saying the criminal justice system is broke, but you're just making the assumption that it's only broke one way."

### AI summary (High error rate! Edit errors on video page)

Alabama's legislature is compared to the Taliban due to a bill introduced by Dicky Drake to address false accusations of sex crimes.
Filing a false report of a sex crime is already a crime in Alabama, but the penalties may be increased to 10 years in prison for the accuser if the accusation is proven false.
The bill could lead to victims of rape being imprisoned if their accusations are proven false, even though studies show that false accusations of rape are in the single digits.
Beau questions the flawed criminal justice system in Alabama and suggests that putting rape victims on trial is not the solution.
The bill may create an environment where rapists flock to Alabama, as there is a risk of innocent victims being imprisoned based on false accusations.
Beau expresses disbelief at the flawed logic behind the bill and its potential consequences on rape victims and the justice system in Alabama.

Actions:

for legislators, advocates, activists,
Advocate for comprehensive justice system reform in Alabama (implied)
Support organizations working to protect victims of sexual assault and improve reporting mechanisms (implied)
</details>
<details>
<summary>
2019-05-11: Let's talk about Ben Shapiro, getting destroyed, and something important.... (<a href="https://youtube.com/watch?v=2YmDv2GsEvA">watch</a> || <a href="/videos/2019/05/11/Lets_talk_about_Ben_Shapiro_getting_destroyed_and_something_important">transcript &amp; editable summary</a>)

Beau addresses a divisive interview incident with Ben Shapiro, stressing the need to move beyond political labels for constructive dialogues to foster real progress.

</summary>

"just say that you're on the left."
"It's all about that now, that bumper sticker politics, right, left, red and blue."
"If you actually start talking to the right-wing voters, you're going to find out that they have a lot of the same problems you have."
"It's going to be up to you."
"Anyway, it's just a thought y'all have a good night"

### AI summary (High error rate! Edit errors on video page)

Beau addresses the incident involving Ben Shapiro and Andrew Neal, focusing on a key moment during their interview.
Ben Shapiro uses terms like "radical left" to create division right from the start of the interview.
Neil points out that new ideas in the US are emerging from what is considered the left, particularly the Democrats.
Shapiro talks about the conservative intelligentsia having debates but doesn't present any new ideas himself.
When asked about abortion, Shapiro reacts defensively and accuses Neal of bias.
Shapiro dismisses Neil as being part of the "other team" and questions his motives.
Neil's main point is that pundits like Shapiro are worsening the quality of debate in American media by immediately labeling others as the enemy.
Beau notes that this divisive tactic is common on both the American right and left but more prevalent on the right.
Beau clarifies the difference between liberal, left, and Democrat, stressing that they are not interchangeable terms.
Beau challenges the notion of a true leftist party in the US and points out the lack of socialist ideologies in mainstream politics.
The core takeaway is the tendency to shut down opposing views based on political labels rather than engaging in meaningful debate.
Beau criticizes the conservative movement for lacking new ideas and focusing on maintaining the status quo.
Beau encourages bridging the gap between left and right by engaging in constructive dialogues to address shared problems.
The importance of individuals taking the initiative to drive positive change in the political landscape is emphasized.
Beau concludes by stressing the need for genuine communication and understanding between differing political ideologies for progress to be achieved.

Actions:

for political activists,
Start engaging in meaningful dialogues with individuals from different political ideologies (implied)
Foster understanding and communication by reaching out to right-wing voters to identify shared concerns (implied)
</details>
<details>
<summary>
2019-05-09: Let's talk about workshops and conferences.... (<a href="https://youtube.com/watch?v=Pj4RAw_HX1g">watch</a> || <a href="/videos/2019/05/09/Lets_talk_about_workshops_and_conferences">transcript &amp; editable summary</a>)

Beau invites his community to an inclusive and purpose-driven conference focusing on community action and networking, offering discounts and VIP upgrades for attendees who share on social media.

</summary>

"All of a sudden, I'm out of excuses not to do it, and I guess I'm going to Vegas, baby."
"If you watch the videos about the world I want or how freedom will come to America, and you're sitting there going, yeah, yeah, you need to go."
"Here you go. Y'all have a good night. Hope to see you there."

### AI summary (High error rate! Edit errors on video page)

Workshops and conferences are common among like-minded individuals to exchange ideas and tactics.
Workshops typically piggyback on activist events, making it challenging for outsiders to know about them.
Conferences are widely advertised but can be cost-prohibitive, lasting a week and held in remote locations.
Beau has been hesitant to attend conferences due to high costs and exclusivity, feeling they cater to a niche group.
A new conference in a major U.S. city with diverse speakers, affordable tickets, and a clear purpose of community action has caught Beau's interest.
The upcoming conference focuses on practical actions for community building and force multiplication.
Beau will be speaking at the conference on July 21st, encouraging those who resonate with his videos to attend.
The conference will feature speakers like G. Edward Griffin, independent journalists, and activists discussing various topics.
Attendees can use the promo code BOWE for a discount on tickets and a chance to upgrade to VIP by sharing the event on social media.
Beau invites his audience to join him at the conference and hopes it will be an eye-opening experience for many.

Actions:

for community members, activists,
Attend the community-focused conference in a major U.S. city (Suggested)
Use promo code BOWE for a ticket discount and a chance to upgrade to VIP by sharing on social media (Implied)
</details>
<details>
<summary>
2019-05-08: Let's talk about crying babies, Trump, and the poverty level.... (<a href="https://youtube.com/watch?v=c_VIOZWhR6o">watch</a> || <a href="/videos/2019/05/08/Lets_talk_about_crying_babies_Trump_and_the_poverty_level">transcript &amp; editable summary</a>)

Beau shares a personal story, criticizes the manipulation of poverty statistics, and advocates for real solutions to address financial struggles faced by Americans.

</summary>

"Babies don't stop crying when they're hungry. What are they going to do?"
"These benefit programs, entitlement programs, as people sometimes like to call them, that's just crime insurance at this point because if you get rid of them, you're going to see an uptick in crime."
"Maybe it would be better if we didn't cook the books and we actually took some steps to try to fix the fact that 80% of Americans are living paycheck to paycheck."

### AI summary (High error rate! Edit errors on video page)

Recounts a story from when he was 20 about a friend's ex showing up with four kids, only one of whom the friend knew, and leaving them with no food or supplies.
Stayed with the kids while his friend went to get baby formula, learning that a hungry baby won't stop crying no matter how much you try to soothe them.
Criticizes the Trump administration for redefining poverty without actually improving people's situations through the Chained CPI method.
Explains how Chained CPI manipulates statistics to make it seem like people are lifted out of poverty when in reality they lose benefits and struggle.
Points out that with the current economy, even a slight increase in income can disqualify individuals from receiving necessary benefits.
Shares statistics on the financial struggles of Americans, such as the inability to find good-paying jobs, pay credit card balances, or afford healthcare.
Notes that while the economy may seem great for some, the majority of Americans are living paycheck to paycheck and cannot handle emergencies.
Expresses the importance of not just focusing on the top earners but addressing the needs of the bottom percentile who significantly impact society.
Suggests that cutting benefit programs leads to an increase in crime as people resort to illegal activities to survive.
Advocates for real solutions to address the financial hardships faced by a large portion of the population instead of manipulating data for political gain.

Actions:

for concerned citizens,
Advocate for policies that genuinely address poverty and financial hardships (implied)
</details>
<details>
<summary>
2019-05-07: Let's talk about the next 11-year-old girl.... (<a href="https://youtube.com/watch?v=eLpe5mMvVSI">watch</a> || <a href="/videos/2019/05/07/Lets_talk_about_the_next_11-year-old_girl">transcript &amp; editable summary</a>)

An 11-year-old girl in Ohio, pregnant after allegedly being raped, faces the consequences of legislation removing her choice, while societal focus shifts from critical issues like testing rape kits.

</summary>

"That's the society we live in, in part because the legislatures of this country are more interested in regulating stuff like this than they are making sure that rape kits get tested."
"It's just a thought, y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

An 11-year-old girl in Ohio, allegedly raped by a 26-year-old, is pregnant after the state removed her choice through legislation.
The focus on regulating issues like abortion rather than testing rape kits contributes to societal problems.
The 11-year-old girl is at a critical developmental stage, facing emotional turmoil, peer pressure, and self-consciousness.
She may struggle with eating disorders, substance abuse, and self-harm during this period.
Laws in Ohio may prevent the perpetrator from getting parental rights if convicted of rape or sexual battery.
Poorly written laws aiming to restrict abortion may lead to unsafe practices and disproportionately affect economically disadvantaged individuals.
Beau challenges the notion that a six-week-old embryo is equivalent to a living child by presenting a stark moral dilemma.
He underscores the complex shades of gray in the world that an 11-year-old is beginning to grasp, contrasting with black-and-white legislation.
Beau questions the heavy burden placed on an 11-year-old in such circumstances.

Actions:

for legislators, activists, advocates,
Advocate for comprehensive sex education in schools to empower young individuals (suggested)
Support organizations providing resources for survivors of sexual violence (exemplified)
</details>
<details>
<summary>
2019-05-06: Let's talk about what outrage on Fox News can accomplish.... (<a href="https://youtube.com/watch?v=3CMfrhAScfY">watch</a> || <a href="/videos/2019/05/06/Lets_talk_about_what_outrage_on_Fox_News_can_accomplish">transcript &amp; editable summary</a>)

Beau addresses how outrage fuels terrorism, criticizing outlets like Fox News for inadvertently aiding radicalization by stoking fear and bigotry.

</summary>

"Being outraged is not good when you are talking about countering terrorism."
"I don't want them to win."
"The reason I talk about these things in a very clinical manner so as not to provoke outrage."
"Fox News and outlets of their ilk have done more to help that small percentage of radicalized Middle Easterners than their own propaganda networks have."
"The Islamic State, Al-Qaeda, these guys, they should have been cutting Fox News a check the entire time because they were their greatest recruitment tool."

### AI summary (High error rate! Edit errors on video page)

Addressing outrage and a comment on YouTube about discussing unpleasant happenings.
Explaining terrorism as a PR campaign with violence to provoke outrage.
Describing how outrage leads to justifying and condoning extreme actions like drone strikes and torture.
Linking overreactions to an increase in terrorism and insurgency.
Connecting foreign fighters in Iraq to the abuse of detainees, Guantanamo Bay, and Abu Ghraib.
Noting that outlets like Fox News contribute to radicalizing Middle Easterners through outrage.
Criticizing Fox News for using fear, bigotry, and ignorance to stoke outrage and ratings.
Emphasizing that being outraged plays into the hands of terrorist organizations.
Suggesting that Fox News inadvertently aids in the recruitment of terrorists.
Advocating for discussing sensitive topics in a calm and clinical manner to avoid perpetuating outrage.

Actions:

for media consumers,
Fact-check news sources and avoid outlets that sensationalize and provoke outrage (implied).
Support media outlets that report calmly and objectively on sensitive issues (implied).
</details>
<details>
<summary>
2019-05-05: Let's talk about American kids learning Arabic numerals.... (<a href="https://youtube.com/watch?v=0uGvZCg0nN4">watch</a> || <a href="/videos/2019/05/05/Lets_talk_about_American_kids_learning_Arabic_numerals">transcript &amp; editable summary</a>)

Beau questions the resistance to teaching Arabic numerals, advocating for a society that values knowledge and continuous learning.

</summary>

"The fact that we have become a nation where knowledge is looked down upon, where gaining knowledge of any kind is looked down upon, it's a bad sign."
"Education is never wasted. Knowledge is never wasted."
"So anytime this topic comes up, the answer should pretty much always be yes."
"It is how we end up with a society where 60 to 85 percent of people answer a poll they know nothing about."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Criticizes the US government for planning to teach American kids Arabic numerals.
Mentions polls on Facebook showing 60% to 85% against learning Arabic numerals.
Expresses agreement with the majority, questioning the need for Arabic numerals.
Believes that the push for teaching Arabic numerals is part of a plot and mentions "Shakira Law."
Points out that Arabic numerals are the numbers commonly used today.
Condemns basing opinions on misinformation and lack of context.
States that knowledge should not be looked down upon and advocates for continuous learning.
Argues that disregarding knowledge simply because of its Arab origin is ridiculous.
Emphasizes that education and knowledge are never wasted.
Encourages Americans to be open to learning, especially when it comes to new topics or ideas.

Actions:

for americans,
Educate yourself about the origins and significance of Arabic numerals (implied)
Challenge misconceptions and misinformation about learning new things (implied)
Embrace a culture of continuous learning and curiosity (implied)
</details>
<details>
<summary>
2019-05-04: Let's talk about subtle ways to change the behavior of others.... (<a href="https://youtube.com/watch?v=UUtgVyiEwVE">watch</a> || <a href="/videos/2019/05/04/Lets_talk_about_subtle_ways_to_change_the_behavior_of_others">transcript &amp; editable summary</a>)

Beau shares strategies to address bigotry, focusing on changing actions rather than beliefs, leveraging influence wisely, and recognizing the power individuals hold in shifting societal norms.

</summary>

"If bigots had trouble getting dates, it [bigotry] would disappear."
"Women do not understand the power they wield in this particular battle."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Recounts an encounter with a friend in South Florida who expressed bigoted and racist views, causing a moment of realization about institutional racism.
Addresses questions received about dealing with bigots in one's life, focusing on changing their actions rather than beliefs.
Advises not to waste time trying to change older bigots, as influencing younger generations has a longer-lasting impact.
Suggests exposing a racist boss to potential consequences like losing business due to public backlash.
Warns about dealing with a partner who exhibits bigotry as a defense mechanism, urging careful consideration before addressing the issue.
Encourages women to recognize the power they hold in combatting bigotry by making it socially unacceptable, thus changing behavior over time.
Advocates for challenging racist jokes by pretending not to understand them, forcing individuals to confront the racism behind the humor and potentially leading to a reduction in such behavior.

Actions:

for individuals combating bigotry,
Leave a newspaper article exposing racist behavior for a boss to see, potentially impacting their actions (suggested).
Challenge racist jokes by pretending not to understand them, forcing individuals to confront their racism (implied).
</details>
<details>
<summary>
2019-05-03: Let's talk about teens, combat vets, and suicide.... (<a href="https://youtube.com/watch?v=j74Y57R15Qs">watch</a> || <a href="/videos/2019/05/03/Lets_talk_about_teens_combat_vets_and_suicide">transcript &amp; editable summary</a>)

May's Mental Health Awareness Month: Beau shares shocking stats on teen suicide, questioning the lack of national focus and urging to value youth.

</summary>

"As Americans, we want to know, well, what's the reason this is happening? It's not a reason."
"Life is never without the means to dismiss itself."
"We have a society that is creating a suicide epidemic among teens."
"There's not going to be an easy fix to this one, guys."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

May's Mental Health Awareness Month.
Beau addresses the heavy topic of suicide.
Beau shares shocking statistics on suicide attempts by young girls.
Suicide among teens is a significant issue.
The rise in successful suicides is alarming.
Beau provides the National Suicide Hotline number.
Suicide is a prevalent issue among teens.
The lack of national focus on suicide is questioned.
Beau criticizes the lack of attention to suicide compared to school shootings.
The impact of popular media like "13 Reasons Why" on suicide is discussed.
Suicide is often a result of multiple factors, not just one reason.
Beau talks about the struggles teens face in today's society.
The importance of valuing and supporting youth is emphasized.

Actions:

for parents, educators, policymakers,
Call the National Suicide Hotline at 1-800-273-8255 or text 741-741 for immediate help (exemplified)
Take interest in the well-being of teens contemplating suicide; it can make a life-saving difference (exemplified)
</details>
<details>
<summary>
2019-05-03: Let's talk about how freedom is coming to America.... (<a href="https://youtube.com/watch?v=NowVx9S1Lgs">watch</a> || <a href="/videos/2019/05/03/Lets_talk_about_how_freedom_is_coming_to_America">transcript &amp; editable summary</a>)

A call to action for freedom through societal change, solidarity, and standing up for what's right, regardless of direct impact.

</summary>

"It's coming from the people who are standing in the streets facing off against MRAPs and tear gas over yet another unjust killing."
"It's coming from anybody who stands up for what's right, even though it doesn't directly impact them."
"Good ideas don't really require force."
"It's coming from those people within the existing power structures who are attempting to dismantle them."
"First we take DC, then we'll worry about Beijing."

### AI summary (High error rate! Edit errors on video page)

Describes a world with a higher level of freedom.
Mentions various instances where this freedom is emerging.
Talks about societal change and resistance against injustice.
Points out different acts of solidarity and unity.
Emphasizes on the importance of standing up for what's right, even if it doesn't directly affect individuals.
Encourages the building of parallel power structures.
Raises the question of individual participation in the ongoing fight for change.
Addresses the global impact of positive ideas spreading.
Concludes by wishing everyone a good night.

Actions:

for activists, community members,
Stand up against injustice by joining protests and demonstrations (exemplified).
Engage in acts of solidarity and unity with marginalized communities (suggested).
Contribute to building parallel power structures within communities (exemplified).
Educate oneself and others about ongoing social issues (implied).
</details>
<details>
<summary>
2019-05-01: Let's talk about the candidate who can beat Trump.... (<a href="https://youtube.com/watch?v=xfGxX9EXHsg">watch</a> || <a href="/videos/2019/05/01/Lets_talk_about_the_candidate_who_can_beat_Trump">transcript &amp; editable summary</a>)

The Democratic establishment should focus on policies, not stealing votes from Trump, and represent the people by supporting a platform that appeals to those who stayed home.

</summary>

"Don't co-sign evil because you don't like the other candidate."
"The moral thing to do is stay home."
"Represent the people of this country. In other words, do your job."

### AI summary (High error rate! Edit errors on video page)

Questions the Democratic establishment's focus on which candidate can beat Trump rather than policies
Criticizes the idea of running a candidate to appeal to Trump's voters, moving policies to the right
Points out that supporting a candidate who appeals to bigots is unacceptable
Encourages people who don't support Trump's policies to stay home rather than vote for them
Urges the Democratic establishment to listen to younger progressives who prioritize moral values
Notes that younger demographics tend to be more progressive, and running a pro-war candidate may lead them to stay home
Observes that Trump didn't receive a majority of votes in any age demographic until 50 years old or older
Warns against trying to steal votes from Trump by appealing to his supporters
Emphasizes the importance of supporting a candidate with a platform that appeals to eligible voters who stayed home
Calls on the Democratic establishment to represent the people and do their job

Actions:

for voters, democratic establishment,
Support a candidate with a platform that appeals to eligible voters who stayed home (suggested)
Encourage the Democratic establishment to prioritize policies over stealing votes from Trump (implied)
Represent the people by supporting candidates that truly resonate with the electorate (suggested)
</details>
<details>
<summary>
2019-05-01: Let's talk about #850Strong and Northwest Florida still waiting on congress.... (<a href="https://youtube.com/watch?v=EZ3Q4LnlOiY">watch</a> || <a href="/videos/2019/05/01/Lets_talk_about_850Strong_and_Northwest_Florida_still_waiting_on_congress">transcript &amp; editable summary</a>)

Hurricane Michael relief efforts face significant delays, especially in aiding Puerto Rico and brown Spanish-speaking communities, showcasing political negligence and a call for accountability in resource allocation.

</summary>

"And now, here we are more than 200 days later and we got nothing."
"You guys might want to get your act together."
"Y'all might be making a mistake here."
"There's a whole bunch of brown people that speak Spanish in Florida, Tambien."
"You guys might want to get your act together."

### AI summary (High error rate! Edit errors on video page)

Hurricane Michael relief efforts revisited due to lack of Congressional action after more than 200 days.
Republicans trying to shift blame onto Democrats for the lack of progress in assisting Northwest Florida.
Despite the hurricane occurring before a change in control, no significant aid has been provided.
Previous hurricanes like Katrina and Andrew received Congressional action within days to weeks.
Republicans are hesitant to allocate funds to Puerto Rico, seen as part of the U.S., leading to accusations of bigotry.
Florida contributes more to the federal government than it receives but struggles to get assistance.
The delay in aid extends to other affected regions like California wildfires and Florence victims.
Criticism towards Republicans for withholding aid from Spanish-speaking brown communities.
Reminder to voters to take note of the neglect during the next election, especially in states like Florida.
Call for accountability and efficient allocation of resources in providing necessary assistance.

Actions:

for voters, community members,
Vote in elections to hold accountable those responsible for delayed aid (implied).
</details>
