---
title: Let's talk about pardoning war criminals....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ppRdK82vjfI) |
| Published | 2019/05/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the issue of pardoning war criminals after returning from a break on Facebook.
- Responding to messages about opinions on specific cases involving contractors.
- Describing a situation where an Army lieutenant interrogated a suspected bomb maker and ended up killing him in a physical altercation.
- Mentioning the ambiguity surrounding evidence in the case and the lieutenant's intention.
- Pointing out that incidents like these, although not right, do happen.
- Explaining that the lieutenant had already served his time in prison before being pardoned.
- Expressing understanding towards the lieutenant's emotional reaction leading to the death.
- Moving on to discussing potential pardons for a Navy chief accused of heinous acts in villages.
- Emphasizing the significance of SEALs turning in the accused Navy chief as a grave indicator.
- Expressing disbelief in the Navy chief's deservingness of a pardon due to his actions aiding the insurgency.
- Mentioning another case involving an Army Major killing a suspected bomb maker, casting doubt on the need for a pardon.
- Touching on the case of Marines urinating on opposition KIAs, condemning their actions but suggesting that they have already faced consequences.
- Acknowledging the impact of the Marines' actions in aiding insurgency propaganda.
- Stating that criminal justice consequences may not match the burden of living with the outcomes of their actions.
- Concluding with strong disapproval of a contractor behaving unprofessionally by opening fire on crowds.
- Arguing that the contractor should have faced justice under Iraqi jurisdiction rather than being brought back to the US.
- Asserting that such behavior is unacceptable in a professional setting.

### Quotes

- "Memorial Day is not Veterans Day. Memorial Day is for those who were killed."
- "If you want to behave like that, there's a whole bunch of places, whole bunch of locations, whole bunch of firms that you can go to and you can work like that and you'll end up at the end of the machete just like everybody else."
- "As a contractor, you are supposed to be a professional, not open fire on crowds, ever, period, full stop, end of story."

### Oneliner

Beau addresses specific cases of war criminals, expressing doubts about pardons and advocating for professional conduct, stressing Memorial Day's significance for the fallen.

### Audience

War critics and advocates

### On-the-ground actions from transcript

- Contact Iraqi authorities to address the contractor's actions under their jurisdiction (suggested)
- Educate on the distinctions between Memorial Day and Veterans Day to honor the fallen appropriately (exemplified)
- Share Beau's message to raise awareness about the implications of pardoning war criminals (implied)

### Whats missing in summary

Deeper insights on the implications of pardoning war criminals and the importance of professional conduct in military and contractor roles.

### Tags

#WarCriminals #Pardons #MilitaryJustice #ProfessionalConduct #MemorialDay


## Transcript
Well, howdy there, Internet people.
It's Bo again.
So I guess we're going to talk about pardoning war criminals
tonight, since I am back from my forced vacation from Facebook.
I've been going through the messages.
There's a whole lot of people asking about it.
The problem is they're asking about the pardons.
As if it's one group, they're not.
Each incident has its own characteristics.
You can't lump them all together like that.
And then a whole lot of the people that sent messages specifically asked about my opinion
on the contractor, and we will get to that.
Okay, so the guy that's already been pardoned, he was an Army lieutenant.
Now the story is, two of his guys get killed by a bomb, army intel is like, this is the
bomb maker we think, and somehow this suspected bomb maker ends up in his custody.
He takes him out to somewhere secluded to interrogate him, I believe that, I really
do.
I don't think he took him out there with the intention of killing him.
I think he was trying to confirm what Army intelligence believed.
He shouldn't have been doing it, but I think that's what he was doing.
Then this is where it gets murky.
According to him, there was a physical altercation.
He was worried the guy was going to get his weapon and he killed him.
According to the government, well, he killed him.
And that's really kind of, they've kind of left that very vague.
However, it appears that the government withheld evidence at his trial.
So we don't really know what went down there.
Things like this happen.
They do.
It's not right.
He never should have had this guy in his custody, period, period.
Anybody who was surprised by this, anyway, they happen.
It's not right, but it happens.
You've got the guy who you believe killed two of your buddies.
You're not going to cut him a lot of slack if he starts to resist.
So we need to keep that in mind.
The other thing we need to keep in mind is that this guy's already done his time.
He was out.
He was on a supervised release, parole, whatever it is they have.
He had already done his time in prison.
So in effect, this pardon cuts his probation short, which happens all the time without
a pardon.
That's not a big deal.
And it gives him a clean slate.
I find it very unlikely that this guy is going to re-offend.
To me, it seems like an incident in which his emotions got the better of him and somebody
died.
He went to prison.
He's out.
I don't really have a problem with this one.
I don't have a problem with this pardon.
Now on Memorial Day, apparently the president wants to pardon more people and those that
he is considering is a Navy chief.
One of them is a Navy chief who is alleged to have opened fire with his machine gun on
villages multiple times without a discernible target.
his sniper rifle to kill a young girl and an old man, and stabbed a teenaged captive.
That's a pattern.
That is a pattern.
This is a very different situation.
The guy was a SEAL, which means he should understand that opening fire on a village
for no reason whatsoever just gives the insurgency propaganda, helps them recruit people to kill
his friends.
The only thing you really need to know about this case, other SILs turned him in.
Other SILs gave him up.
If you know nothing about the Spec Ops community, the Special Operations community, you think
the police have a wall of silence.
These guys do not talk.
They don't turn each other in.
For other Seals to have turned this guy in, this is probably just the tip of the iceberg.
My belief is that this guy is probably not deserving of a pardon.
Creating propaganda like that for the opposition is to me unforgivable.
The next case is an Army Major, another suspected bomb maker in custody.
Kills him and they throw him in a fire pit.
This seems a little different.
An Army Major captures a bomb maker, and that's a feather in his cap.
That's a win for him.
That makes him look good.
For it to go down the way it did, it's almost like they weren't sure, but they didn't want
to let him go. So they killed him. I can't really warrant a pardon there either. I don't
have all the details like I do in some of these others, but from what I have heard,
no, this guy does not need to be out walking around. Then the Marines that were urinating
opposition KIAs. That type of stuff happens. It's wrong. The worst thing they
did, in my opinion, is they produced evidence that they did it. And that
evidence got out. And then it produced that propaganda that helped the
insurgency recruit people to kill their friends. Now these guys they're done
they're all out I'm sure by now. The only one I know of that was involved in
this since he's been done he helped stop a contract killing in Tennessee. Helped
the Tennessee Bureau of Investigation stopped a hitman from being hired.
Again this is one of those things, they're done, their punishment's over, I want to give
them a clean slate.
I understand that.
And the reality is because that propaganda got spread so far and so wide, they got to
live with that.
They know the outcome of what happened.
They know what occurred because of what they did, and they have to live with it.
Nothing the criminal justice system is going to be able to do to them is going to match
that, nothing.
Letting them go on with their lives, I don't really have a problem with it, I don't.
Now, onto the contractor.
A lot of the people that asked about this one specifically,
I went to your profile, and I noticed that a lot of you
worked for firms that I recognize,
think you were hoping to get somebody's voice out there
to maybe speak in favor of this.
You chose the wrong dude.
As a contractor, you are supposed to be a professional, not open fire on crowds, ever,
period, full stop, end of story.
If you want to behave like some cut-rate merc, go work for some tin pot dictator.
As far as I'm concerned, that sentence is too light, I don't think he should have been
brought back here.
They should have let the Iraqis handle it.
That would have been justice.
He was not part of the US military.
The Iraqi government, in my opinion, had jurisdiction over that.
They should be able to make the call on what happens to him.
You want to behave like that.
There's a whole bunch of places, whole bunch of locations, whole bunch of firms that you
can go to and you can work like that and you'll end up at the end of the machete
just like everybody else. That is not something that happens in a
professional organization. Now I will, as far as the urinating on the opposition
KIAs, I'm gonna link a video. If you have a dark sense of humor and foul language
does not bother you, this pretty much gives my whole opinion on it. If either one of those
things do bother you, don't watch it. Now, the fact that he wants to pardon these guys
on Memorial Day, some of these people created propaganda that recruited more insurgents
I created some of the people who are supposed to be honored on Memorial Day.
Memorial Day is not Veterans Day.
Memorial Day is for those who were killed.
I think it's in pretty poor taste
to do it then.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}