---
title: Let's talk about subtle ways to change the behavior of others....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=UUtgVyiEwVE) |
| Published | 2019/05/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recounts an encounter with a friend in South Florida who expressed bigoted and racist views, causing a moment of realization about institutional racism.
- Addresses questions received about dealing with bigots in one's life, focusing on changing their actions rather than beliefs.
- Advises not to waste time trying to change older bigots, as influencing younger generations has a longer-lasting impact.
- Suggests exposing a racist boss to potential consequences like losing business due to public backlash.
- Warns about dealing with a partner who exhibits bigotry as a defense mechanism, urging careful consideration before addressing the issue.
- Encourages women to recognize the power they hold in combatting bigotry by making it socially unacceptable, thus changing behavior over time.
- Advocates for challenging racist jokes by pretending not to understand them, forcing individuals to confront the racism behind the humor and potentially leading to a reduction in such behavior.

### Quotes

- "If bigots had trouble getting dates, it [bigotry] would disappear."
- "Women do not understand the power they wield in this particular battle."
- "Y'all have a good night."

### Oneliner

Beau shares strategies to address bigotry, focusing on changing actions rather than beliefs, leveraging influence wisely, and recognizing the power individuals hold in shifting societal norms.

### Audience

Individuals combating bigotry

### On-the-ground actions from transcript

- Leave a newspaper article exposing racist behavior for a boss to see, potentially impacting their actions (suggested).
- Challenge racist jokes by pretending not to understand them, forcing individuals to confront their racism (implied).

### Whats missing in summary

Beau's full transcript provides nuanced insights and practical strategies for addressing bigotry in personal and professional settings.

### Tags

#Bigotry #SocialChange #Influence #CommunityPolicing #AntiRacism


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're gonna start off with a story.
This friend, he grew up in South Florida.
In an area of South Florida that white guys
normally do not grow up in.
This guy is not a bigot.
But I see him out one night,
and I sit down at the table with him,
there's two other guys there.
And I hear this man say some of the most bigoted,
racist stuff. I never would have imagined him saying, to be honest. Now I've known him long enough
to know that he's got his Curious George patch on upside down. There's something going on, and I
have to wait and see how it ends. But he's, you know, he's like, yeah, you know, went into bid for that
job. It was me and two Cubans. You know who got that? The guys are laughing. And he goes through and he
outlines a whole bunch of scenarios like that and they're laughing and talking
about how true it is and then he's like man I love being white white privilege
is great and for that brief moment you could see it on their face they couldn't
deny that institutional racism existed now I have no idea if this was actually
an effective tactic. I never saw these guys again. Could have just been a brief
thing, but for that moment, oh they knew it was real and there was no way they
could deny it because they'd been talking about it and saying it was real all
night. Now the reason I told you this is because over the last couple weeks I've
had questions come in from different people, very different
situations but they're all the same question. How do we deal with bigots that
are in our lives that we can't get rid of? It's tough. That is tough. I'm trying
to give you some advice on these mainly because there were so many and understand
the advice I'm giving is more about changing how they act rather than what
they believe.
In an ideal world, we'd all be out there
winning hearts and minds.
At the end of the day, if you got a good hold on them,
their hearts and minds will follow.
You just have to change the way they act, at least around you.
First guy, and I'm putting the ages together here.
You didn't say, but from the timeline you gave,
this guy sounds to be 60 plus years old.
and you have to deal with them.
You've made some efforts to limit that contact,
but you live in a town of 3,000 people,
oh yeah, y'all are gonna meet again.
How do you reach out to somebody who is 60-plus years old
and a bigot?
You don't.
No, don't even waste your time with it.
Seriously.
It has been my experience that when you were talking
about racists that are that old, the amount of time it takes
to create any real change in them is substantial.
And if you were to take that time
and devote it to reaching out to 20-year-olds,
you could reach 10 20-year-olds in the same amount of time.
That 60-year-old, he's gonna be dead in 20 years.
Those 20-year-olds, they will be influencing people
long after you're gone.
You got limited resources, use them wisely.
On this one, just let nature take its course.
Put your effort where it's going to make the most good,
just from math.
Just looking at the math on this, if you waste that time
trying to reach out to this guy, you are 580 years in the hole.
That's how much influence could be created.
580 years.
Anyway, next guy, his boss, is openly racist.
But he needs the job.
And he's a white guy and he doesn't know how to deal with it
because he just doesn't like hearing it.
Okay, this is easy.
This one is easy because the business deals with the public.
Leave out a newspaper article the next time
If somebody loses their business because they said something stupid on Facebook and word
got out that they were racist, leave the newspaper open to that article.
Share it on Facebook if he's following you on social media.
Just get it out there and put it in his head that he can lose his business because of these
stupid ideas.
Again, you're not changing hearts and minds, but if you got him by the wallet, his actions
The next one is tough, the next one is tough, the next one is very different because it
is not run-of-the-mill bigotry, the woman is dating a veteran and he has a lot of negative
things to say about Arabs and Muslims.
Based on the years you said he was there and where you said he was, this is probably a
defense mechanism.
It's really common, extremely common throughout history.
Most normal people, there's a small subset that this doesn't apply to, but most normal
people need to dehumanize their opposition because if they don't and the opposition,
those are people, that means everything they did, they did to people, not their favorite
racial slur.
The question I'd ask you is, did this only come up because he was around some of his
older army buddies that he doesn't see so often, and you just heard it all, and now
you're concerned, is this something that's only going to come up every once in a while,
or is this something you're going to have to deal with every day?
I would weigh this one out really carefully, because when you open this can of worms, you're
probably going to need professional help along the way.
When you take away that crutch, and that's what it is, it's a crutch.
in this case is a crutch. It helps them justify actions. You know you didn't do
that to a person, you did that to whatever. I'd make sure first you're
gonna stay with this guy forever and second that you're ready for it because
because this will get nasty.
This is gonna be a rough road.
Um...
The next one...
You know, you said the South.
I don't even consider that the South, but I do understand that in a lot of ways
that is more the South than a lot of the South.
She says that anytime she hears the N-word, she corrects it
as politely as possible
and that this causes problems at times.
Seems like she's looking for another way to do it.
This is, again, this is not a great tactic,
and I'm sure that...
I'm sure there will be some people who are not happy with this, but
I'm of the firm belief
that women do not understand the power they wield
in this particular battle.
If women tomorrow coast to coast decided that bigotry was unattractive,
oh it would be gone in a generation.
If bigots had trouble getting dates,
it would disappear.
Now I know
that's basically encouraging your own objectification. I understand that.
it's a tactic
and it's one that I do believe would work. I'm assuming that if you are as
passionate as you've seen
in the message
about this
your uh...
your friends
your other women friends
are probably
the same way
and
if it became
known that these are qualities
you don't find attractive in men,
you wouldn't see them anymore.
You really wouldn't.
They would disappear. Again,
you're not really changing hearts and minds,
but you're changing the way they act.
And if they have to conceal the way they act, then, well,
eventually, they're going to stop thinking that way.
It's a lot like racist jokes.
You know, where I'm at,
there's not a lot of overt racism anymore.
It's jokes.
And the best thing I have found to deal with that is to
pretend like you don't get the joke.
Because the reason they're hiding it in a joke is that
they're not comfortable being openly racist.
So when they finish the punch line, you're like, I don't
get it.
Explain it.
And you have to drag it out of them.
And they have to openly admit the racist
undertones of the joke.
then it's not funny anymore and then they also don't get to hide behind it.
What happens is they stop telling the jokes, maybe not everywhere, but around you.
And this is one of those things where it's a huge battle and every little bit helps.
If you can shut them up for five minutes, that's five minutes of peace that somebody
else might get. I mean, there are a lot of ways to deal with racists. There are. All
of these seem to be looking for more subtle ways. So those are the options I give you.
I do love the fact that we are seeing more and more people who are not racist become
anti-racist, because that's what it's going to take, and even if it's just little things
like this, it can make an impact.
Anyways, just a thought.
Y'all have a good night.
Y'all have a good night.
Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}