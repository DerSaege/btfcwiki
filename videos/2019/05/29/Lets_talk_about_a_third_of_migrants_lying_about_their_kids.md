---
title: Let's talk about a third of migrants lying about their kids....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=oYhj2GMahHo) |
| Published | 2019/05/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses a headline claiming that a third of migrants lied about their family status with their children, expressing concern and calling for an investigation.
- He points out that people are sharing the headline without critically examining it.
- Beau clarifies that the 30% mentioned in the headline was rounded up to a third, but the actual percentage was 30%.
- The sample used in the study was not representative of all migrants, but specifically those suspected by ICE of lying about family relationships.
- ICE's DNA testing method to verify family relationships does not account for step parents, legal guardians, or other complex family situations.
- He criticizes ICE for being wrong 70% of the time in cases where they suspected migrants of lying about family relationships.
- Beau questions the ethics of sharing a false narrative that demonizes migrants and perpetuates harmful stereotypes.
- He underscores the dangers faced by unaccompanied minors who end up in the custody of Health and Human Services, where reports of sexual abuse are prevalent.
- Beau concludes by debunking the manufactured story and criticizing the 145,000 people who shared it without verifying its accuracy.

### Quotes

- "If you wouldn't lie to stop a kid from getting raped, you're a pretty horrible person."
- "This is made up and 145,000 people shared one version of this stupid story."

### Oneliner

Beau debunks the false narrative that a third of migrants lied about their children, exposing flawed statistics and calling out the harmful impact of sharing misinformation online.

### Audience

Online activists

### On-the-ground actions from transcript
- Fact-check and verify information before sharing online (implied)
- Advocate for the protection of unaccompanied minors and migrant families (implied)
- Combat misinformation by educating others on how to critically analyze news stories (implied)

### What's missing in summary

The emotional impact of spreading false narratives and the importance of empathy and fact-checking in online discourse.

### Tags

#Immigration #FactChecking #ICE #MigrantRights #Misinformation


## Transcript
Well, howdy there, internet people, it's Bo again.
So we've got to talk about that headline, because people have been sending it to me.
And then I had a journalist friend ask me to debunk it.
And okay, so here we go, a third of migrants lied about their familiar status with their
children, wow, a third, that's really high.
That needs an investigation.
We need to take a look at that.
So I mean it's one of those headlines that's just grabbing people and they're sharing it
like crazy without actually looking into it at all.
One of them from the Daily Mail carrying this idea has 145,000 shares.
145,000 people bought into this stupidity enough to share it and co-sign on it.
Look at what's happening.
Okay.
So let's talk about the realities of this.
Was it a third?
No.
It was 30%.
I'll give that one to them.
They rounded it up.
Cool, that's fine.
No big deal.
No harm, no foul there, really.
Was it of all migrants?
No.
Not all migrants come with kids.
So no, it's not.
Was it of all migrants with kids?
No, no, of course not.
They didn't test everybody.
it a scientific sample from different locations? No. Was it even just a random sample of people
going through a specific location to be processed? No. No, it wasn't. It was of those that ICE
believed were lying. So if ICE believed you were lying, and they suspected that enough,
they put you in this thing and they tested your DNA, and 30% of those were, kind of,
But even that's not true.
We'll get to that.
So even at this point, the real headline is out of those people suspected of bringing
a child that wasn't there, ICE was wrong 70% of the time, but it's really more than that
because the DNA test does not account for step parents.
And when asked about this, ICE said, well, we would look for documents.
They would look for documents, but those numbers are already in the total.
Doesn't account for those who have legal guardianship.
Doesn't account for those who were asked by a parent to bring their child here.
Doesn't account for those who were just walking along and saw a 15 or 16 year old kid who
happened down there at 15, 16 years old in some of these countries.
It's so rough that that's pretty much an adult.
child may have decided to make that trip on their own. And then they get here, and maybe
the adult actually knows what's happening in the U.S., and when they see this kid about
to be taken into custody, they're like, no, no, no, no, that's my kid. Because these unaccompanied
minors and where they end up. They end up in the custody of Health and Human Services
where there are thousands of reports of sexual abuse. If you wouldn't lie to stop a kid
from getting raped, you're a pretty horrible person.
So no, not a third of migrants faked their kids.
Not a third of those with kids lied about it.
It's an entirely manufactured story.
The reality is, out of those that ICE believe were lying, they were wrong.
The overwhelming majority of the time, a ridiculously high percentage, by their own flawed study,
If you take it into account for the things that they didn't account for, they're almost
always wrong.
This is made up and 145,000 people shared one version of this stupid story.
This is why we can't have nice things.
Anyway, it's just a thought.
Y'all have a good night

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}