---
title: Let's talk about rights, kicking down, and punching up....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Zx0w0k0xurU) |
| Published | 2019/05/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Reacts to a message questioning his support for abortion due to having six kids.
- Expresses surprise and introspection at the hypocrisy pointed out by the message.
- Lists various causes he supports, despite not personally embodying all of them.
- Urges people to care about the rights of others beyond those that directly impact them.
- Warns against falling into the trap of division created by propaganda and those in power.
- Criticizes the manipulation of people's rights in the name of democracy.
- Acknowledges living by far-right ideals in his personal life but opposes forcing them on others.
- Advocates for freedom by not using force to restrict others based on personal beliefs.
- Condemns the idea of government having ultimate control over individuals' lives as oppressive.
- Calls for a shift in mindset to stop fighting amongst each other and focus on real freedom.

### Quotes

- "Stop caring about things that impact you directly and only those things."
- "You vote each other's rights away."
- "Stop kicking down. Only punch up."

### Oneliner

Beau reacts to a message questioning his support for abortion, urging people to care about the rights of others beyond themselves and warning against the manipulation of division for power.

### Audience

Activists, Advocates, Citizens

### On-the-ground actions from transcript

- Challenge yourself to care about causes that may not directly affect you (exemplified)
- Advocate for the rights of marginalized groups in your community (exemplified)
- Refrain from imposing personal beliefs on others through force (exemplified)

### Whats missing in summary

The full transcript includes Beau's reflections on societal issues, the manipulation of division for power, and advocating for true freedom beyond personal beliefs.

### Tags

#Abortion #Activism #Rights #Freedom #Division


## Transcript
Well, howdy there, internet people, it's Bo again.
So I got a message, and we're gonna talk about it.
Very direct, very direct message.
It said, Bo, you've got six kids.
How in the F do you support abortion?
Man, wow.
I have to admit, that one got to me.
really got me thinking. Made me realize when I was up against, you know, that
display of hypocrisy just being pointed out like that. Not mine. Not talking about
mine. How do you respond to a question like that? How do I respond to a question
like that. Black lives matter. I support LGBTQ rights, support refugees, the homeless, suicidal
teens, women's rights. I'm against poaching. I know this may come as a shock to a lot of
you, but I am not a gay, black, trans, female, homeless, refugee, rhino with suicidal ideation.
You want to make the world a better place, you want to change the world, you want to
make America great, stop being so selfish.
Stop caring about things that impact you directly and only those things.
Start caring about the rights of others.
Because here's the thing, there will always be that class of people who will create the
propaganda, to manufacture the division, to play one side against the other, and get the
normal folk fighting.
And they'll capitalize on that, and that's how they stay in power.
They get you fighting, and each side considers it a win when they take a right from the other
team.
Nobody's winning, everybody's losing, except for those people in power, except for those
in control because they gain more control over your life every time a right is taken.
Every time they can inch just a little bit further into your life.
That's how a tyranny of the majority is manufactured under the guise of a democracy.
You vote each other's rights away.
And yeah, yeah, I do in a lot of ways live the far right ideal.
That's how I run my life.
Really is.
The thing is, I would never force that on anybody.
My life and my views on things, well that's not a basis for law.
I know that's a crazy concept in today's world, but what I think is right for me personally,
that's really not a good enough reason to employ men with guns to restrict the actions
of others. That's not freedom. That's tyranny. That's oppression. You know, we sit here and
fight over things like this. And meanwhile, those at the top, they just laugh. Because
Because they realize that half the population has bought into the idea that the government
is infallible, that the government should have final say over how you run your life,
down to the smallest detail.
That's not freedom.
You want freedom, you want to make America great, it's a pretty simple rule.
Stop kicking down.
Only punch up.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}