---
title: Let's talk about the dos and don'ts of Memorial Day....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=i987Ke9CzPE) |
| Published | 2019/05/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A veteran asked Beau to provide a do's and don'ts of Memorial Day, but Beau initially felt he wasn't the right person for it.
- Veterans already familiar with Memorial Day's significance, but the general public may not be aware due to lack of discourse by politicians.
- Beau acknowledges the diversity of perspectives among veterans, contrary to a singular portrayal.
- Armed Forces Day, Veterans Day, and Memorial Day serve different purposes: active duty appreciation, all who served recognition, and honoring the fallen, respectively.
- Memorial Day is a solemn occasion to commemorate those who have died in service.
- Beau advises against saying "Happy Memorial Day" as it may not be appropriate given the solemnity of the occasion.
- Younger veterans from recent conflicts may still be dealing with emotional wounds, so sensitivity is key.
- Beau stresses the importance of not asking certain questions to veterans, particularly about combat experiences or killing others.
- Gold Star families, who have lost a family member in service, should be acknowledged with a simple "I'm sorry for your loss" without trying to relate their loss to a cause.
- Beau suggests allowing veterans to enjoy Memorial Day without feeling the need to relate to their experiences.
- Some veterans believe kneeling during the National Anthem on Memorial Day is a way to honor those unjustly killed by law enforcement.
- Beau encourages advocating against unjust wars and ensuring that active-duty personnel get celebrated on Veterans Day instead of being remembered on Memorial Day.

### Quotes

- "Memorial Day is a day to remember the fallen. It's a day to remember those lost. It's not a happy day. It is not a happy day."
- "Just leave it at that. I'm sorry for your loss."
- "Make sure that those who are active duty today get to celebrate Veterans Day and not get remembered on Memorial Day."
- "You've got a voice, use it."
- "We stop losing people, we don't need to remember."

### Oneliner

A guide on navigating Memorial Day sensitively, from recognizing diverse veteran perspectives to honoring the fallen and advocating against unjust wars.

### Audience

Community members

### On-the-ground actions from transcript

- Acknowledge Gold Star families with a simple "I'm sorry for your loss." (implied)
- Advocate against unjust wars and ensure active duty personnel are celebrated on Veterans Day. (implied)

### Whats missing in summary

The full transcript covers the nuances of Memorial Day observance, including the diverse perspectives of veterans and the importance of sensitivity in honoring the fallen.

### Tags

#MemorialDay #Veterans #HonorTheFallen #Respect #Advocacy


## Transcript
Well, howdy there, internet people, let's bow again.
Had a vet ask me to do a do's and don'ts of Memorial Day.
Like, no, I am not the right guy to do this.
And he said, no, you're the perfect guy to do this.
Veteran channels tend to only reach veterans.
Veterans already know this.
Politicians have no interest in talking about it.
Therefore, people don't know.
I'm not sure.
Rule 303, you've got the means.
So here we are.
OK, so the first thing I want to address,
and this will be certainly shown in the comments section,
is that veterans are not a singularity.
They don't all think the same way, contrary
to the way they're portrayed.
In fact, I am certain that there is one bullet point here
that will demonstrate that very clearly.
OK, Armed Forces Day, Veterans Day, and Memorial Day.
Three very different things.
Armed Forces Day, active duty.
Veterans Day, anybody who served.
Memorial Day, people who died.
That's what the days are for.
All of a sudden, saying Happy Memorial Day
seems really weird, right?
OK, so keep that in mind.
going to have your barbecue or whatever, you're going to invite your vet buddies over. Happy
Memorial Day may not be the appropriate thing to say. It's a day to remember the fallen.
It's a day to remember those lost. It's not a happy day. It is not a happy day.
More importantly for today's vets, if you're younger and you're inviting people over who
are in Iraq or Afghanistan, these are still wounds.
They're not scars yet.
So every day to them is Memorial Day.
They're remembering it every day.
So just bear that in mind.
When you have these vets over there, there are two questions you don't ask, just don't.
The first is, did you see combat, did you see any action?
Something along these lines.
When you ask this question, you're going to see the guy...
It isn't because that particular question necessarily caused him any distress or hurt
any distress, it's because they know what's coming next.
you kill anyone? Don't ask that. Just don't. There's a limited number of situations. You've
got a person who went over and saw nothing. How could that cause them any distress? They
may feel guilty. I know that sounds weird. They may feel like they didn't do their part.
all but some are gonna have that feeling. Then you have the guy or woman who went
over saw something and didn't kill anyone. Maybe they're remembering
somebody on Memorial Day. Maybe they feel like they let that person down. They
probably don't want to talk about it either. Then you got the guy who killed
someone and feels bad obviously doesn't want to talk about it. Then you've got
the person who killed someone and is completely apathetic to it, just doesn't
care. And either they're part of that 4% that we've talked about in other
videos or they haven't processed it yet, may not want to bring it up. And then
the one person you can run into that I can assure you will make you stop asking
this question forever is the person who killed someone and liked it because they
exist and they're never who you think they are. This guy's probably a school
teacher now but he's got a few beers in him. Did you kill anyone? Next thing you know
you're looking at a kill book and you will never look at him the same way
again. Just don't ask this question, ever. Okay, the Gold Star families, now these
flags they come out, these lapel pins they show up particularly on Memorial
Day. What this means and the flag, it's a white flag with a red border and then in
the middle of it there's a gold star with a blue border. This means they lost
somebody in their immediate family. That's what it means and sometimes it's
a lapel pin. Now you probably want to say something. I'm sorry for your loss. Leave
it at that. Just leave it at that. In older times it might have been
inappropriate and well-received to say, you know, he died for a good cause or she died
for a good cause.
Not all Gold Star families today are going to respond well to that.
They don't all believe that because of the wars in which they were lost.
That may provoke a conversation you might not want to have at a barbecue.
So I'm sorry for your loss.
The next thing, and this is kind of odd, it may seem odd, it's really not.
Just have the barbecue, have the event, don't try to relate.
And there's one of the people I talked to, he said it in the perfect way, I would rather
Whether they buy their mattress, have their barbecue, and have a good time, then try to
relate because they can't.
In at least one of these two ways, I can have a good time too.
So just remember the following.
Leave those that are here kind of out of it.
Now the next one, they want it addressed and man, okay, they're certain that people will
kneel for the fallen during the national anthem.
According to them, now this was a certain subset of vets, it's the perfect day to do
it, I'm certain right now somebody is typing a comment and that's fine, y'all can hash
this out in the comments section I'm not even getting involved in this conversation.
Their logic is that when people are kneeling during the National Anthem today they are
kneeling for those killed unjustly by law enforcement.
To them it is remembering the fallen and that is more in line with the holiday holiday is
more aligned with the day than a barbecue. At least it's for the same purpose I guess.
Okay so what is the best thing you can do for Memorial Day? Make sure that those who
are active duty today get to celebrate Veterans Day and not get remembered on Memorial Day.
require your silent consent. If it is an unjust or unnecessary war, don't give it.
You've got a voice, use it. That is the most important thing that you can do,
is to make this holiday null. We stop losing people, we don't need to remember
Okay, so now you know, knowing is half the battle, the other half is violence.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}