---
title: Let's talk about what picking a color can teach us about gender....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ijooHoBoey8) |
| Published | 2019/05/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Painting yard toys and questioning gender norms.
- Challenging societal constructs of pink for girls and blue for boys.
- Introduces the concept of gender neutrality and non-binary identities.
- Exploring historical gender norms like colors and clothing.
- Examining the fluidity of gender norms and societal expectations.
- Acknowledging the existence of gender fluid individuals.
- Addressing the misconception of only two genders in the U.S.
- Stating that accommodating gender identity isn't a national issue.
- Criticizing the stigmatization and control of gender identities.
- Comparing the treatment of gender identity to mental illness and lack of accommodations.
- Questioning the inconsistency in accommodating mental illnesses.
- Criticizing attempts to legislate and control people's sex lives.
- Distinguishing between sex and gender, asserting it's not others' business.

### Quotes

- "Gender, that's what's between your ears."
- "At the end of the day, sex, well that's between your legs."
- "It's just a thought."
- "Throughout history, what is viewed as acceptable within the societal norms for a gender has changed."
- "Gender fluid people, people that want to cross over, maybe even to the point of identifying as a different gender."

### Oneliner

Beau challenges gender norms, questions societal constructs, and advocates for acceptance and understanding of diverse gender identities.

### Audience

All individuals

### On-the-ground actions from transcript

- Embrace gender diversity by educating yourself and others (implied).
- Support and advocate for gender-neutral spaces and inclusive policies (implied).
- Challenge gender stereotypes in daily interactions and choices (implied).

### Whats missing in summary

Beau's engaging storytelling and historical insights provide a compelling perspective on the fluidity of gender norms and the importance of accepting diverse gender identities.

### Tags

#Gender #Identity #Norms #Acceptance #Equality


## Transcript
Well, howdy there internet people, it's Bo again. Today's gonna be a pretty short video
It's daytime look at that and
I'm out in the yard. I'm painting some yard toys for my kit. I need your help. I need to know what color to paint them
You choose What?
You're waiting for more information
Are these colors associated with something?
Pink and blue, huh?
Why? Is there a biological reason that blue is for boys and pink is for girls?
None. None whatsoever. That's gender. It's a social construct. It's the norm.
It's all it is. It's nothing to do with biological sex and I've realized that
that people have a problem distinguishing the two.
What if I painted it this?
Gender neutral, right?
Non-binary, how about that?
Also gonna give my oldest boy a knife.
Which one should I give him?
Does it change anything?
Are these different?
Other than the color?
Nope.
but the societal norm says that he should get the blue one.
I personally
am non-binary
in my knife lifestyle choices.
Throughout history
what is viewed as acceptable
within the societal norms for a gender
has changed.
You think about it today, a dress, well that's for women,
it's for the feminine gender,
but what about a kilt?
Well that's masculine, societal norm, that's all it is, it doesn't change anything.
It's really irrelevant.
All of this conversation is just about people deviating from the societal norm.
Who cares?
Deep down who cares?
Funny thing, pink used to be the boys' color.
Red shirts were masculine.
When they would fade, the men would take them and give them to the women because sewing
is women's work, unless of course it's for sale or on a ship, then it's men's work.
Because it's a very different kind of sewing, not that it's just the societal norm.
So when they would fade, they'd give them to the women, and the women would take them
and make boys' clothes out of them.
If what society deems is appropriate for a gender is fluid, how can you be surprised
that there are gender fluid people, people that want to cross over, maybe even to the
point of identifying as a different gender.
Now in the U.S., we've grown very accustomed to having two genders, so much so that people
think that that's the way it's always been.
It's not true, it's not true.
Historically, a lot of societies have had more than two genders.
In the U.S. it just so happened that we had two main ones that lined up with biological
sex.
All there is to it.
This isn't a national issue, it shouldn't be.
It's just people, I mean there's nothing pressing, there's nothing damaging to the country because
somebody wants to present themselves as more feminine or more masculine or even go all
the way, identify as somebody else, or maybe even have a surgery. There's no damage to
the country from this. The damage comes when somebody whispers, whispers in your ear, witch.
And you find yourself fighting, kicking down instead of punching up. It's a good way to
divide people and control them? I know somebody out there right now is saying it's a mental
illness. I'd love to have that conversation at some point, but this is supposed to be
a short video. So we're just going to say sure, fine, whatever. It's a mental illness.
So why aren't we making the accommodations we would for other mental illnesses? We don't
make accommodations for mental illnesses. That's ridiculous. We cure them. So I'm
going to expect you to throw the vet out next time you see him in the restaurant
with his service dog. We don't make accommodations for mental illnesses. Or is
it you just don't want to make them for people you don't like? See the thing is
with a lot of this that's going on it boils down to something that is just
innate in the American character, we want to legislate and control people's sex lives.
That's what it really boils down to.
It's not your business.
It is not your business.
At the end of the day, sex, well that's what's between your legs.
Gender, that's what's between your ears.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}