---
title: Let's talk about Iran....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=QqXzZKtCFGc) |
| Published | 2019/05/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A young guy, just back from boot camp, is excited about the prospect of war with Iran, despite Trump's mixed signals.
- The young guy missed out on Iraq and Afghanistan and is eager to go to war.
- He's underestimating the potential troop numbers needed for a conflict with Iran.
- The conflict was sparked by alleged sabotaged shipping boats in the Gulf of Oman, blamed on Iran without concrete evidence.
- Senator Rubio and Tom Cotton are pushing for aggressive action against Iran.
- There's a discrepancy between the threat level reported by Operation Inherent Resolve and CENTCOM regarding Iran.
- Acting Secretary of Defense Shanahan discussed a plan to send 100,000 to 120,000 troops to Iran with Trump.
- Beau warns that a war with Iran won't be like Iraq and could lead to disastrous consequences.
- Iran's military is well-prepared for insurgency and technologically advanced, posing a significant challenge for any military action.
- Beau points out that the real motivation behind a potential conflict with Iran may be to boost Trump's reelection chances, not national security.
- He stresses that going into Iran half-heartedly will only lead to more conflict and loss of life without solving the root issues.
- Beau questions the motives behind supporting a war that benefits Saudi interests over American lives.
- He calls for supporting the truth rather than blindly backing a manufactured war.

### Quotes

- "They're not fighting for freedom. They're fighting so Trump can be more reelectable."
- "Before you say that you support the troops, you have to support the truth."
- "This is a manufactured war."

### Oneliner

A warning against blindly supporting a potentially disastrous and manufactured war driven by political agendas rather than national interest.

### Audience

Concerned citizens, anti-war activists.

### On-the-ground actions from transcript

- Contact elected officials to express opposition to military action against Iran (implied).
- Support organizations advocating for diplomatic solutions and peace (implied).

### Whats missing in summary

The emotional impact of sending young Americans to war for political gain rather than national security.

### Tags

#War #Iran #Trump #PoliticalAgendas #ManufacturedConflict


## Transcript
Well, howdy there, internet people, it's Bo again.
I ran into this guy, just got back from boot, he's excited.
He is excited, because Trump's talking off and on
about sending troops to Iran, talking about war with Iran.
Saying he doesn't want it, but we're ready for it.
Sending a carrier group out there.
This kid, he is excited, and he is a kid.
He's young enough to be excited about going to war, and he's excited because he missed
Iraq and Afghanistan.
He's like, yeah, man, they're talking about 120,000 troops.
I'm like, man, they're going to need more than that.
He's like, no, we're not going to have those problems.
It's not like Iraq.
We already got the Arab linguists and everything.
That's the perfect example.
That shows, I understand this kid is not brass.
I get that.
he's making the same mistakes. They don't speak Arabic. They speak Farsi. They're trying
to draw a parallel to Iraq. 120,000 troops. Anyway. So in case you don't know what's popped
all this off, some shipping boats were allegedly sabotaged. I said allegedly because we haven't
seen any evidence much less evidence that I ran did it but we've blamed them
they've they're denying it but we blame them and Rubio Senator Rubio is out
there saying well it's all on them they get to decide I guess whether or not we
attack them we attack them because it's not they attacked us none of those boats
were ours, two of them were Saudi, one was UAE, and I think one was from Norway.
But it's all on them.
No Senator, it's on you.
You're the one that's going to vote for it.
Then you got Tom Cotton out there saying, well, we'll end the war in two strikes.
We?
What is this we?
You're going to be in D.C.
You're going to send them off, and then maybe you'll write a letter to one of their families.
You probably have a staffer write the letter to be honest.
There's no we in this, sir.
So on the ground, the number two guy in charge of Operation Inherent Resolve, he's a major
general, he's British GECA, I think is his name.
He said that the threat level from Iran has not increased.
in damage control, out comes a Navy captain, spokesman for CENTCOM.
He comes out and says, well that runs contrary to our credible evidence and our intelligence.
That alone presents its own issue because that means one of two things.
Either CENTCOM is not distributing intelligence that could save American lives or they're
They're making it up.
Those are the only options.
New York Times has reported that acting Secretary of Defense Shanahan reviewed a plan with the
White House about sending 100,000 to 120,000 troops to Iran.
Trump himself is saying that he doesn't want a war and going back and forth with it, but
But at the same time, he's promised them a devastating war and a bad problem, and said
that he was willing to spend way more than 120,000 troops.
Well, that's good, because you're going to have to.
You are going to have to.
This is not Iraq, 120,000 is less than we sent to Iraq, and we didn't pacify Iraq.
I know some of the out there is going, well, it's a different country.
It is.
It is a different country.
The Iranian military trains for insurgency.
It's part of their war plan.
Not the way Iraq did a couple of drills.
No, they plan on it.
They're also more technologically advanced.
And then there's the big problem, the numbers issue.
Iraq had a population of 38 million.
Iran about 80, 120,000 troops just is not going to cut it.
And I know, well, they don't want regime change.
No, I know they don't want that.
What they want is to put the U.S. on a wartime footing
before the election,
because they know that wartime presidents
are more reelectable.
That's what they want.
They don't care that American troops are gonna die
so they can get some extra votes.
But the reality is, you cannot go into Iran half-heartedly.
You go in, it's going to be regime change, and it's going to be another decade-long
insurgency.
Over what?
When this happens, remember this, it started because of Saudi profits.
That's what this is over.
We were not attacked, had nothing to do with us.
the Saudis were. Maybe. We don't even know that Iran did it. And we certainly can't trust
U.S. intelligence at this point. That's where we're at. If we go into Iran half-heartedly,
well as Trump said, it'll be a devastating war. We'll have a bad problem. But even knowing
all of this, when the time comes, people will wave those flags, put that yellow ribbon on
their truck, bumper sticker patriots, and not say a word as their kids and grandkids
are sent off to die for Saudi prophets.
They're not fighting for freedom.
They're fighting so Trump can be more reelectable.
and let your kid die for that.
Before you say that you support the troops,
you have to support the truth.
And in this case,
this is a manufactured war.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}