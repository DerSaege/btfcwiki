---
title: Let's talk about how I like my coffee, a new skill, and the lessons of history....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=t8RNlkIkvCM) |
| Published | 2019/05/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Shares how he likes his coffee, drawing parallels with his preference for markets.
- Talks about living on the Alabama line and learning how to conduct abortions out of curiosity.
- Mentions the misconception around back alley abortions and how things have changed.
- Points out that 97% of abortions occur before 20 weeks, mostly done with a pill.
- Explains the multi-use nature of the abortion pill and its availability.
- Notes the advancement in technology making abortion procedures safer.
- Mentions the availability of tools for the procedure at hardware stores.
- Talks about the affordability of ultrasound machines and how they enhance safety.
- Suggests that the new law in Alabama might increase access to abortions and make them cheaper.
- Points out the lack of a paper trail or protesters when abortions are performed at home.
- Mentions how this law could undermine government authority and resemble the failure of prohibition.
- Concludes by stating that leadership should not give orders that won't be followed.

### Quotes

- "I like my coffee like I like my markets."
- "It's almost like prohibition doesn't work."
- "Never give an order that won't be followed."

### Oneliner

Beau talks about his coffee preferences, learning about conducting abortions, and how a new law in Alabama might actually increase access and affordability while undermining government authority.

### Audience

Community members

### On-the-ground actions from transcript

- Contact local healthcare providers for information on abortion services (suggested)
- Research affordable and safe abortion options in your area (implied)

### Whats missing in summary

Beau's detailed explanations and examples are missing in this summary.

### Tags

#Abortion #Alabama #Access #Technology #Government


## Transcript
Well, howdy there, internet people, it's Bo again.
So, tonight we're gonna talk about how I like my coffee.
I know there's a lot of jokes out there, you know,
I like my coffee like I like my women.
No, not me.
I like my coffee like I like my markets.
Free, organic, strong, preferably fair trade, and black.
You know, I live right on the Alabama line.
So today I did something out of pure curiosity, didn't have any intention on actually using
this new skill of mine, but I was really curious how it was going to be done.
So today I learned how to conduct abortions, I'm not joking.
Found out some really interesting stuff, really interesting stuff.
The pro-choice crowd might be looking at this law all wrong.
This new Alabama law might have misread it.
Because we have this image of the back alley abortion
that came from the 60s.
Things have changed a lot, a lot.
As we talked about in that other video,
97% of abortions occur before 20 weeks,
right after the screenings are done.
That's pretty much a cut-off point.
So that's when they happen.
Now, even using the very, very safe standards of a hospital,
for half that time, abortion's done with a pill.
It's done with a pill.
And I know, people are like, well,
that pill's going to be banned.
Yeah, for that purpose.
But the thing is, it's a multi-use pill.
It's prescribed for different stuff.
In fact, if you're a big boy, I'm pretty sure
that you could slam a bunch of little Debbie cakes
for those overseas real sugary, sweet snacks,
walk into the doc's office,
and he'll probably prescribe it to you.
So, it seems like it's gonna be pretty easy.
Now after that point, assuming they'll probably push it.
These aren't doctors doing this.
So they'll push it out to 12 or 13 weeks using that method.
That last seven weeks gets a little bit dicier,
but nothing like it was in the 60s,
because technologies change.
The tools are pretty much the same.
You can pick them up, I don't know, $100, $150
at Lowe's or Hobby Lobby, which is really funny,
given Hobby Lobby's stance on birth control.
Yeah, those tools are the same,
but see, something's changed.
Something big's changed.
Technology is advanced.
Back in the 60s, it's not like, you know,
the guy's gonna bring over a sonogram machine
to do an ultrasound, right?
Nah, couldn't do that.
But you can today.
I know what you're thinking,
Beau, that's crazy.
You know, I mean, those things
Gotta be expensive.
Yeah, I looked.
They're on eBay.
Cost about as much as an Xbox.
And that is going to make that procedure a whole lot safer.
It's crazy.
It is crazy.
So the worry was that it was going to limit access.
Probably not.
Probably not.
In fact, it may increase access.
because
well now
the guy's gonna come to you
and come to your house
think about that in the comfort of your own home
it's probably going to be cheaper
because well there's not as many regulations to follow
it's uh...
it's weird
there's no paper trail on it nothing that's gonna
used to blackmail you in the future, since it's happening at your home, there's no protesters
to deal with.
Nobody's screaming at you, throwing stuff at you, like good Christian folk.
And it undermines the belief in the infallibility of government.
That little rebellion starts right there.
Might be looking at this law all wrong.
It's almost like prohibition doesn't work.
It's almost like that's what all of recorded history has taught us and we're just ignoring
it.
We're just pretending like that's not true and that's not the lesson.
It's almost like this law will make things cheaper, increase access, limit regulation,
limit people whining about it, and undermine the authority of the government in Alabama.
It's one of the first rules of leadership.
Never give an order that won't be followed.
Anyway, it's just a thought.
have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}