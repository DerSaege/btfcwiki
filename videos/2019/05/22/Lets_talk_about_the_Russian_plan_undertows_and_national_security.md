---
title: Let's talk about the Russian plan, undertows, and national security....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=4x9cflMxahQ) |
| Published | 2019/05/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Beau starts by sharing a personal story about taking his kid to the beach and facing an undertow, using it as an analogy to explain a complex situation.
- The Russian documents aren't a government plan but were created by a group of civilian contractors who pitched it to their handler and then to the government.
- The plan originated from a group of intelligence professionals contracted by the Internet Research Agency (IRA) or possibly Wagner.
- The plan is to trigger an insurgency within the United States using black Americans, causing racial discord.
- The strategy targets a specific demographic - black Americans who make up about 12% of the population and have high incarceration rates and economic disparities.
- Beau explains the importance of choosing a group with a high incarceration rate for radicalization.
- By alienating and persecuting black Americans further, it becomes easier to radicalize them.
- Beau stresses that the question should be why intelligence professionals chose this demographic, pointing out systemic racial issues that need addressing.
- Addressing racial disparities is not just a moral issue but a national security concern as targeted groups are easier to radicalize.
- Beau concludes by stating that countering racism is now a national security issue and urges for action against it.

### Quotes

- "You want to make America great? You got to stop racism."
- "No self-appraisal will ever be as valuable as your opposition's appraisal."
- "Addressing racial disparities in this country is no longer just the moral thing to do. It's a national security issue."
- "Countering racism is now a national security issue."
- "They identified them two years ago."

### Oneliner

Beau explains a Russian plan to trigger an insurgency in the US using black Americans reveals systemic racial issues needing urgent attention, urging action against racism.

### Audience

Policy Makers, Activists

### On-the-ground actions from transcript

- Address systemic racial issues in your community (implied)
- Advocate for policies that counter racism and support marginalized communities (implied)

### Whats missing in summary

Insight into the potential long-term effects of systemic racism if not addressed urgently.

### Tags

#RussianDocuments #RacialDiscord #SystemicRacism #NationalSecurity #CommunityAction


## Transcript
Well, howdy there, internet people, it's Bo again.
????????????, ?????? ?????????
???????? ?????????? Bo.
So we're gonna talk about those Russian documents.
But before we do, I wanna tell you a story.
Here in the last couple weeks, I took my kid to the beach.
He'd never experienced an undertow before.
Now I was right next to him, it wasn't particularly strong,
so I let him try to fight it.
I let him struggle with it,
try to swim directly to the beach.
up until the point where he almost wore himself out. Then I explained you have to
go at an angle. You can't fight something like that. You won't win. Okay so now
these documents. First thing you have to understand is that this isn't a Russian
government plan. The way this works, and I'm pretty familiar with this process, is
a group of civilian contractors come up with a plan and they submit it to their
handler and then their handler takes it to the government pitches it to the
government if the government likes it they fund it if not it dies in this case
it was the Internet Research Agency apparition it may be Wagner probably IRA
to be honest but I don't remember to be honest while I was looking at it so
that's where the plan originated that's how it came to be a group of
intelligence professionals that are contracted came up with this. What is the plan? To trigger
an insurgency within the United States using black Americans. That's the plan. Now that's
not how the media is describing it yet, but that's the plan. They're saying it's to trigger
racial discord trigger yeah because we don't have that already so the US does
this all the time now when you're setting something like this up you need
to look for certain things you need to look for a population an ethnic minority
that is 10 to 15 percent of the population in Syria the Kurds were 10
percent in Iraq they were 15 to 20 the reason you want 10 to 15 is because that
makes them effective enough to cause a problem but not so effective that they
win you don't actually want that and 15 is the cap the Kurds in Iraq were a
little bit over that almost lost control almost had a Kurdish state so you need
need that, you need that percentage, black Americans are about 12 by the way, you need
a group with a high incarceration rate, group that is economically disadvantaged, not really
represented in government, they need to be the out group, they need to be persecuted,
oppressed in some way.
That's what you need to do this.
You want the incarceration rate high because those people are easier to radicalize.
Somebody has been to prison, it is easier to play on that gripe, play on that experience
to get them to move against their government.
I mean, that's why it's there.
We did it in Syria.
We used people that were incarcerated in Syria to help found the moderate rebels.
So this isn't anything new, it's not, but in the coverage I'm seeing something very
dangerous already.
What are we going to do about the black nationalist threat?
There's not one yet, but that question will create one.
That question is part of the plan.
You have to alienate this group even further.
See right now in the U.S., you know, black people, they're an ethnic minority and they're
looked down upon.
What if you add that they could be possible traders?
And now they're even further alienated, makes them that much easier to radicalize.
That is not the question to ask, unless you want it to happen.
The question to ask is, why did a group of intelligence professionals select this demographic
to target?
Because we have systemic racial issues in this country that need to be addressed.
That's why.
really that simple. That's why they were chosen. It's not because they're evil or
they're not good Americans. It's because they're on the bottom and they've been
on the bottom and nobody seems to care. But here are these people saying, hey
we'll train you. Maybe we can get you your own country.
It may not seem like such a bad deal.
So when you hear this conversation taking place and people talking about it as if this
is the fault of black Americans, you need to correct them because that is very dangerous.
is very dangerous. I've looked at the plan. It's ambitious. Probably wouldn't work this time,
once you establish that generational trend of trying to go for independence and that militancy
as it stays within that community generation after generation, it'll work. It'll work,
it'll take time, but it'll work. So, here's the interesting thing about this.
Addressing racial disparities in this country, oh, it's no longer just the moral thing to do.
It's always been the moral thing to do, but now it's a national security issue.
issue, because if these Russian contractors figured this out and they've targeted them,
every intelligence agency in the world has done the same thing.
They know that within the United States, this group, because of their treatment, will be
easy to radicalize.
Think about this, really think about this.
You can play whatever conservative trump card you want to throw out right now.
Doesn't matter.
No self-appraisal will ever be as valuable as your opposition's appraisal.
And the opposition just told you, we treat these people horribly.
That's what they're saying.
They're saying that because of the government's treatment of black Americans, they will be
a demographic that can be turned.
You want to make America great?
You got to stop racism.
Congratulations.
The best part about all this is that this plan was drawn up prior to Trump's administration.
So everything that's happened in the last two years, that's just made it worse.
They identified them two years ago.
Yeah.
So countering racism is now a national security issue.
Anyway it's just a thought.
I'll have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}