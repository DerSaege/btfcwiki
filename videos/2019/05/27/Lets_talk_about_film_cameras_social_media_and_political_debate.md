---
title: Let's talk about film cameras, social media, and political debate....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=cVQ8xAZt2pg) |
| Published | 2019/05/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Draws parallels between old film cameras and social media filters, likening overexposure in both to the loss of depth and nuance.
- Criticizes how social media shapes political discourse, particularly among young people who are constantly under peer scrutiny for online behavior.
- Points out how groupthink and lack of independent thought harm debates and limit the development of ideas.
- Gives an example of a law in Alabama that, despite its origins in bigotry, actually makes it easier for same-sex couples to marry.
- Challenges the rigid labels and group mentalities in the abortion debate, illustrating how they stifle nuanced understanding and critical thinking.
- Analyzes a controversial image on social media related to abortion, showcasing extreme reactions from both sides.
- Raises awareness about the importance of exploring gray areas in debates and forming well-thought-out opinions beyond simplistic slogans and hashtags.

### Quotes

- "It's overexposed so there's no independent thought, there's no ideas being developed in the dark room."
- "Pro-life and pro-choice, right? Means nothing. Those terms mean nothing."
- "We want a 30-second sound bite on the nightly news and a tweet."
- "If we can't even accomplish that, you can give up putting it into practice."
- "It might be time to form opinions in private before introducing them to social media."

### Oneliner

Beau draws parallels between old film cameras and social media filters, criticizes groupthink in political discourse, challenges rigid labels in the abortion debate, and calls for nuanced opinions beyond social media slogans.

### Audience

Social media users

### On-the-ground actions from transcript

- Form opinions in private before sharing on social media (implied)
- Challenge groupthink by engaging in nuanced debates (implied)
- Encourage independent thought and depth in online discourse (implied)

### Whats missing in summary

The full transcript provides a deeper exploration of how oversimplification and group mentalities hinder meaningful discourse on social media, urging individuals to develop nuanced opinions beyond surface-level slogans.

### Tags

#SocialMedia #PoliticalDiscourse #Groupthink #Nuance #AbortionDebate


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight, we're going to talk about what we can learn from old film cameras.
You know, everything that you see today on social media, the apps,
the filters that get used, you can do with one of these.
You really could.
You just needed to know how.
You needed to know the finer points, the details of how to do it.
You know, that common beauty filter lightens the face and gets rid of the blemishes.
You can do it with one of these by overexposing the area you wanted to do that to, putting
too much light on it, shining too much light on it, overexposing it, gets rid of those
blemishes, washes them out, washes out the finer details.
It's funny that we see those images all over social media because that is exactly what
social media does to political discourse.
You know, young people today, they've grown up with everything they do being put online...everything.
And it's reviewed for appropriateness by their peer group.
If they start to step out on the line, don't worry, their peer group is there to push them
right back where they belong.
We see it as something that just happens to young people.
This is the reason they're lacking in substance.
I don't know, I don't know about that.
Happens to everybody.
Happens to everybody in different ways.
debate gets destroyed this way. Groupthink develops. Essays become tweets,
limited in character. I'm not talking about the number of letters. I'm talking
about depth, nuance, the finer details. All washed out because it's overexposed.
overexposed. It's overexposed so there's no independent thought, there's no ideas
being developed in the dark room, so to speak, privately, than to be shown later.
Because we put all of our thoughts online and then kind of work them into where we
need them to be.
You know, we can see this in every debate, in every debate, how the sides, the group
think plays out.
In the latest travesty of a law to come out of Alabama, it's so pronounced that it's
almost funny.
So they're going to redo the way they are recording marriages in the state.
Now those in opposition to this, they know it came from a bad place.
This bill came from a bigoted place.
It was judges who didn't want to sign off on same sex marriages.
That's what created this bill, really did.
If you look at it and you look at the sides that people are taking, those who are supporting
this bill, well, they're against same-sex marriage.
Those who are in opposition to it are foreign, but the bill, if you actually really look
If you look at it, it makes it easier to have a same-sex marriage in Alabama, it really does.
Basically it just takes the judge out of the process.
It makes it easier if you are a same-sex couple in a county with a bigot judge, well now you
don't have to deal with them.
That's the effect of the law.
But because of the way political discourse in this country happens, people who are opposed
to same-sex marriage, they're supporting a law to make it easier.
And there are some people who, because they know this came from a place of bigotry, are
opposing it.
That's a genetic fallacy.
Take the win here.
It's going to be easier.
It's going to increase access.
Who cares if bigots were too stupid to write the law right?
The end result is good.
You can see it there.
You can of course see it in the abortion debate.
Pro-life and pro-choice, right?
Means nothing.
Those terms mean nothing.
They really don't, there's no defined position for these terms.
They're just slogans to help people group together.
And if you start to deviate from the norm of what
is accepted from your chosen group, oh, don't worry.
Your peer group will be there to push you back where you belong.
And there is no clearer example of this
than an image that is floating around on social media
right now.
It is a fetus nine weeks into development
on a piece of gauze.
Man, when that image goes up, the pro-life crowd goes nuts.
The mother is all kinds of horrible things.
The doctor should have left it.
You know, it's nuts.
One version of it that's being shared,
there's a reply from somebody named Vanessa,
not going to share their last name,
because I'm certain she is already
getting enough death threats from the pro-life crowd.
Pro-life doesn't mean pro-life in all instances.
It's just a sloat.
Same thing with pro-choice
Her reply
was that she would gladly
stomp on it until it was unrecognizable with a smile on her face
Wow!
That takes the uh...
edgiest comment of the day award
but see this is where it gets interesting
from an image of an ectopic pregnancy.
So those who want the doctor to leave the fetus where it is,
well, they would have killed two people.
And Vanessa, whether she knew it or not,
she's actually taking the pro-life stance.
Ectopic pregnancy means that it was in the tube.
Well, extra uterine.
The fetus was not viable to begin with, would not survive.
and, if left, would kill the mom.
So in a situation like this, what you see is that all of these debates, not just abortion,
but all of them, have this huge gray area.
But it's only gray if we don't overexpose it.
So we do that.
We choose to do that because we're Americans and we like things to be simple.
We don't want to get involved in the finer points.
We want a 30-second sound bite on the nightly news and a tweet.
We want a hashtag to identify with rather than a well-thought-out position.
That's where we're at.
It might be time to form opinions in private before introducing them to social media.
Before groupthink can take hold.
Because otherwise, we're not going to go anywhere.
People talk about wanting revolution.
Requires revolutionary thought.
It requires thought outside of the accepted norms.
If we can't even accomplish that, you can give up putting it into practice.
It'll never happen.
Need a paradigm shift in the way we talk about controversial subjects.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}