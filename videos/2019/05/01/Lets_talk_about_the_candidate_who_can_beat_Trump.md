---
title: Let's talk about the candidate who can beat Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xfGxX9EXHsg) |
| Published | 2019/05/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Questions the Democratic establishment's focus on which candidate can beat Trump rather than policies
- Criticizes the idea of running a candidate to appeal to Trump's voters, moving policies to the right
- Points out that supporting a candidate who appeals to bigots is unacceptable
- Encourages people who don't support Trump's policies to stay home rather than vote for them
- Urges the Democratic establishment to listen to younger progressives who prioritize moral values
- Notes that younger demographics tend to be more progressive, and running a pro-war candidate may lead them to stay home
- Observes that Trump didn't receive a majority of votes in any age demographic until 50 years old or older
- Warns against trying to steal votes from Trump by appealing to his supporters
- Emphasizes the importance of supporting a candidate with a platform that appeals to eligible voters who stayed home
- Calls on the Democratic establishment to represent the people and do their job

### Quotes

- "Don't co-sign evil because you don't like the other candidate."
- "The moral thing to do is stay home."
- "Represent the people of this country. In other words, do your job."

### Oneliner

The Democratic establishment should focus on policies, not stealing votes from Trump, and represent the people by supporting a platform that appeals to those who stayed home.

### Audience

Voters, Democratic establishment

### On-the-ground actions from transcript

- Support a candidate with a platform that appeals to eligible voters who stayed home (suggested)
- Encourage the Democratic establishment to prioritize policies over stealing votes from Trump (implied)
- Represent the people by supporting candidates that truly resonate with the electorate (suggested)

### Whats missing in summary

The full transcript provides a detailed analysis of the pitfalls of focusing on beating Trump rather than policies and advocates for supporting candidates based on moral values and platforms that resonate with voters. 

### Tags

#DemocraticEstablishment #Trump #Policies #Progressives #Voters


## Transcript
Howdy there, Internet people, it's Bo again.
So tonight, we're gonna talk about
that all-important question, which candidate can beat Trump?
I know it's the all-important question
because it's all the Democratic establishment
we'll talk about, that's all they'll say.
Should we run a Latino, a gay guy, a woman?
Maybe we should play it safe and just run an old white guy.
It's a scary way to think about it.
a scary, scary way to think about it, because it really just moves everything to the right,
doesn't it?
Doesn't it mean you're adopting the policies of Trump to help appeal to those voters?
See what you say is which candidate can beat Trump, but the way you're going about it,
What I hear is who can appeal to his voters.
The problem with this is that I don't know anybody in my circle of friends that would
even remotely support a candidate that appealed to those bigots.
I know.
I know right now somebody out there is going, Beau, that's harsh, you know, just because
you voted for Trump doesn't mean that you're a bigot.
You're right.
You're right.
That's unfair.
But you did have to make a decision and you decided that bigotry, misogyny, xenophobia
and hatred in general weren't deal-breakers for you.
I don't see a lot of the people I know supporting a candidate that appeals to that.
I don't support any of those things.
I just didn't want Hillary to win.
Then you stay home.
You stay home.
That's the moral thing to do.
Don't co-sign evil because you don't like the other candidate.
That doesn't even make sense.
Everything that Trump has done, the changes in taxes that hit the little guy, the tariffs,
those kids in cages that got sexually abused, you did that if you voted for it.
You did it.
That's how it works.
You co-signed all of that.
You empowered him.
The moral thing to do is stay home.
Really hope the democratic establishment is listening to that because younger progressives,
they know that.
That's what happened in 2016.
If you run a pro-war corporate-owned candidate, they're going to lose because the younger
people will stay home.
They'll do the moral thing, just like they did in 2016.
If you look at the demographics from the voters, you'll find something really interesting.
Trump didn't get a majority of people in any age demographic until you hit 50 years
old or older.
50 year olds well they're 54 now doesn't change anything but there's a whole
bunch of kids who are far more progressive they can vote now but will
they not if you run Trump light you know it said that if Hitler ran as a
a Republican and Stalin ran as a Democrat, one of them would be president.
I don't think that's a fair analogy though.
It's more like if Hitler ran as a Republican and the Democrats went and
sought out Himmler, somebody that kind of appealed to those people.
If the only platform the candidate has is that they're not Trump,
Trump, they're going to lose.
They're going to lose.
Don't try to steal votes from Trump.
Anybody that voted for Trump, they're going to vote for him again.
Because to change their vote means they have to change their mind.
They have to admit that they were duped by that mango Mussolini, that they were wrong,
that they were tricked.
That's really hard to admit.
So they're going to vote the same way.
But by trying to appeal to them, you're going to alienate all of those people that won't
real change.
They're not going to show up.
They'll do the moral thing and stay home.
It's almost as if the democratic establishment is just dead set on another four years of
Trump.
Don't look at the physical characteristics of the candidate and try to figure out who
can steal votes from Trump.
Support the candidate that's got a platform that can appeal to the 42% of eligible voters
that did the moral thing and stayed home.
Get a platform people can rally behind.
Represent the people of this country.
In other words, do your job.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}