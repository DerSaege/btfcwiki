---
title: 'Let''s talk about #850Strong and Northwest Florida still waiting on congress....'
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=EZ3Q4LnlOiY) |
| Published | 2019/05/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Hurricane Michael relief efforts revisited due to lack of Congressional action after more than 200 days.
- Republicans trying to shift blame onto Democrats for the lack of progress in assisting Northwest Florida.
- Despite the hurricane occurring before a change in control, no significant aid has been provided.
- Previous hurricanes like Katrina and Andrew received Congressional action within days to weeks.
- Republicans are hesitant to allocate funds to Puerto Rico, seen as part of the U.S., leading to accusations of bigotry.
- Florida contributes more to the federal government than it receives but struggles to get assistance.
- The delay in aid extends to other affected regions like California wildfires and Florence victims.
- Criticism towards Republicans for withholding aid from Spanish-speaking brown communities.
- Reminder to voters to take note of the neglect during the next election, especially in states like Florida.
- Call for accountability and efficient allocation of resources in providing necessary assistance.

### Quotes

- "And now, here we are more than 200 days later and we got nothing."
- "You guys might want to get your act together."
- "Y'all might be making a mistake here."
- "There's a whole bunch of brown people that speak Spanish in Florida, Tambien."
- "You guys might want to get your act together."

### Oneliner

Hurricane Michael relief efforts face significant delays, especially in aiding Puerto Rico and brown Spanish-speaking communities, showcasing political negligence and a call for accountability in resource allocation.

### Audience

Voters, Community Members

### On-the-ground actions from transcript

- Vote in elections to hold accountable those responsible for delayed aid (implied).

### Whats missing in summary

The emotional impact of neglected communities waiting for aid and the urgency for immediate action to rectify the situation.

### Tags

#HurricaneMichael #CongressionalInaction #AidDelay #PoliticalNegligence #CommunitySupport


## Transcript
Well, howdy there, internet people, it's Bo again.
So I guess we gotta talk about Hurricane Michael.
It's a new video, don't worry.
I know it does make sense, that happened a while ago.
Y'all remember the videos about me going down there
and doing relief work.
We gotta talk about it again because, well,
there's still no Congressional action.
There is still no Congressional action.
And the Republican party is trying to pin it
on the Democrats, of course.
I mean, and that would make sense.
that would be important to do because Northwest Florida,
oh, this is Trump country, all right?
So you definitely had to do something about that.
The problem is, I know we sound like this,
but we're not stupid.
We know the hurricane happened in October
before you guys lost control.
And nothing got done.
Nothing.
What could be done in that little amount of time,
month or so? A lot. A lot. Katrina got their congressional action in 10 days.
Gustav, 17. Andrew, which was the last cat 5 to hit, took a whopping 34. So yeah,
something could have been done. And now, here we are more than 200 days later and
we got nothing. And the reason is because, well, the Republicans don't want to give
Puerto Rico any money and well I mean I understand that you know brown people
that speak Spanish and all you wonder why everybody sees the Republican Party
as the party of bigots this is why I've heard it referred to as foreign aid guys
that's part of the United States it is and yet they don't all pay federal
income tax but they pay Social Security tax which you guys raid all the time
for your projects, Medicare, payroll taxes, the state taxes, whole bunch of
other stuff. They pay federal taxes, but you don't want to give them any aid. Now
Florida itself, we give more to the federal government than we take, but we
can't we can't get any help. And because of the Republicans just being ignorant
And about the situation in Puerto Rico, you know, kind of like when Trump said that he
was talking to the President of the U.S. Virgin Islands, not realizing he is the President
of the U.S. Virgin Islands, you know, we're hit here in Florida.
We're getting nothing.
Two hundred days later, more than six months, as well as people affected by the wildfires
in California, and people hit by Florence, they're waiting on help too, and they're
getting nothing.
And it's being held up because you don't want to give brown people that speak Spanish
help.
You guys remember that during the next election, because there's a whole bunch of brown people
that speak Spanish in Florida, Tambien.
remember that. Y'all might be making a mistake here, because everybody knows y'all can come
up with money anytime you need to do a PR stunt and send troops down to the border or
anything else. You guys might want to get your act together. Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}