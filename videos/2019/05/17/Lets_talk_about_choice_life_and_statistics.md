---
title: Let's talk about choice, life, and statistics....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=jgGpFYEMiw0) |
| Published | 2019/05/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses choice, life, and statistics related to abortion, debunking false information dominating headlines.
- Mentions the religious issue surrounding abortion and references a passage in Numbers from the Bible.
- Talks about the misconception around heartbeats in embryos and the size of embryos at six weeks.
- Emphasizes the statistic that 97% of abortions occur before 20 weeks.
- Argues against viewing abortion as a consequence, stating it's more of a punishment due to legislative restrictions.
- Challenges the idea that consent for sex equals consent to carry a child.
- Counters the belief that women use abortion as birth control, presenting statistics on abortion frequency.
- Addresses the misconception that tax dollars fund abortions, clarifying the role of the Hyde Amendment.
- Links abortion rates to poverty and criticizes the lack of focus on real societal issues.
- Centers the abortion debate on bodily autonomy and challenges the notion of men's rights over women's bodies.

### Quotes

- "97%, remember that number."
- "Consent for sex is not consent to carry a child."
- "Poverty is a lack of cash. It's not a lack of character."
- "Bodily autonomy, it is not your body."
- "When does life exist and the rights of that new life trump the rights of the woman?"

### Oneliner

Beau debunks myths, presents statistics, and challenges the notion of bodily autonomy in the abortion debate, urging society to prioritize ethics over morals or religion.

### Audience

Advocates for reproductive rights

### On-the-ground actions from transcript

- Advocate for comprehensive sex education in states with limited education on the topic (implied).
- Support organizations that provide resources for women facing unplanned pregnancies (implied).
- Challenge misconceptions and stigma surrounding abortion through education and open dialogues (implied).

### Whats missing in summary

In-depth exploration of the societal implications and ethical considerations in the abortion debate.

### Tags

#Abortion #ReproductiveRights #BodilyAutonomy #MythsDebunked #Ethics


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're gonna talk about choice, life,
and some statistics.
We're gonna give you some real information here
about the topic that is dominating the headlines.
There's a lot of information out there
that is just plain false, and we're gonna go through it.
You will not really find out my personal opinion on this
at this point, mainly because it's irrelevant.
Okay, but before we can get into the statistics
and all that stuff and the truth behind the matter,
we have to address that other issue.
Because it's the US, we have to address the religious issue.
So, okay, friends, I'd like you to join me,
open your Bible to Numbers, chapter five, verse 15.
Start reading, yeah, that is the description
an abortion in the Bible. The reason there's an abortion in the Bible was
because the man, the husband believed the wife cheated on him. If infidelity is a
just reason, certainly rape an incest star. Just a thought though. This is the
problem going to a church. They give sermons where they just jump around in
the Bible and they never actually read the whole thing. You miss out on some
some pretty important things and you may walk away thinking something is true that isn't.
Okay now let's talk about heartbeats.
You know six weeks, okay ventricles don't form until 16 weeks.
Maybe heartbeat in the heartbeat law isn't what you think it is.
You might want to look into that.
At that point, at six weeks, the size of the embryo is about the size of a sweet pea.
So a third the size of a Florida mosquito.
Give you a statistic here.
I really want you to remember, 97% of abortions
occur before 20 weeks, or at 20 weeks or before.
97%, remember that number.
20 weeks, 97%.
OK, so some of the arguments that we hear against this
is that actions have consequences.
They do.
They certainly do.
But this isn't a natural consequence anymore.
It's a manufactured one.
It's not a consequence, it's a punishment.
The legislature has removed the ability to deal with this.
So it's not actually a consequence anymore.
This would be very similar to, in the old days, if you cut
off your hand, well, you just cut off your hand.
Today, we have the ability to reattach it.
Well, the legislature thinks you need to be more careful
with your saws all.
So they pass a law saying the doctor can't reattach it.
Oh, it's just a consequence.
No, it's a punishment.
OK, well, you knew it was a possibility.
You know, a car accident's a possibility
when you get in a car.
And this seems to be something that people don't understand.
Consent for sex is not consent to carry a child.
That is evidenced by the number of abortions there are.
Consent is a very simple concept.
But apparently, we're losing it in this country.
We don't understand it.
OK, this is where we run into the idea
that women are using this as birth control.
When you go in for an abortion, they actually
ask, how many have you had?
8% have had three or more.
92% have had zero, one, or two.
So that really isn't happening, not to the degree
that people seem to believe it is, it's not a thing.
Well, convenience abortions are 99% of them.
That's true.
That is true.
But the devil is in the details.
How is convenience defined?
Basically, 1% of abortions save a life.
Everything else is convenience.
So if the child would be born with a horrible defect
and spend its two-week life in incredible pain,
well, that's convenience.
Definitions matter.
Well, we've got an abortion pandemic.
We're just going crazy.
OK.
2017, there were 879,000 abortions.
Seems like a big number.
Doesn't seem like a big number when
You think that in 2015 there were 903,000 and in 2014 there were 926,000 and in 2011
there were 1.06 million and in 2008 there were 1.21 million.
That's right the numbers are going down.
Why because the internet has allowed young people to get sex education in states that
only teach abstinence or don't offer it.
it's important. Okay, so can you screen a child at six weeks to find out about
defects that are going to occur? No, you can't. You know when that happens? The
first round, the one that tells you whether or not the chance is there at the
big chance, that happens at 11 to 13 weeks. At 15 to 20 weeks, there's that
number well that's when you find out for sure. It's when you find out if they have
spina bifida or downs or whatever. 97% of abortions occur 20 weeks or before it's
almost like there's a reason for it. Okay now the other idea that's out
there is that this is all just young people running around hot-rodding in
their cars and making out in the backseat, right? It's not true either. 31% of
abortions are for women over the age of 30. Crazy, crazy, isn't it? In fact, if you
were to add 25 to 29, well that's another 27.6%. So 25 and older, that's more than
half. It's not what you think it is. The top three reasons for having an abortion?
Not ready. Those are young people. The second one is not having the money. You can't afford
it. It's almost like poverty plays into this a whole lot. We don't want to address poverty
though. We just want to legislate, you know, abortion, punish people. And then the third
reason is they're done having kids. That's right. You know, those good mothers that actually
had their children, that's 59% of women who have an abortion. 59% already have kids. The
stigma that is created in the propaganda, it's just not true. It's just not true. Now,
the other thing that pops up a lot is, I don't see why my tax dollars should pay for this.
My federal tax dollars are going to this.
No, they're not.
They haven't since 1976 when the Hyde Amendment was passed.
But my Republican congressmen said we're funding Planned Parenthood.
Well, they probably did.
Because Planned Parenthood does a whole lot more than abortion.
Federal tax dollars do not go to pay for abortions.
They don't.
But if that's really an issue,
my evangelical friends,
Are you ready to cut off aid to Israel?
Because state assistance for abortions, oh, that's widely available there.
Widely available.
So all the money we give them in aid, well, that enables them to pay for that.
You ready to do that?
No, no.
You just want to punish the people you can look down on here.
You don't actually care about the actual issue.
Now, here's an interesting stat.
Abortion rates are three times higher among people who receive Medicaid, almost like it
has to do with poverty.
Poverty is a lack of cash.
It's not a lack of character.
This is why this is happening.
It has to do with money.
It has to do with the society we have, but we don't want to address any actual issue.
want a magic bullet because we're the US. We don't actually want to have to think about
it and come up with any kind of real solution. Well, we'll just pass a law. Great. It's not
going to change anything. Now, the real issue when it comes down to all of this is bodily
autonomy. And that's where all of this really lies. Now, I did that little short video earlier.
Anytime I do something like that, just so you know, I'm researching, I need to know
who I'm talking to, I need to know what the real objections are, so I read the comments.
A lot of men saying, well, men don't get a choice.
You're right, it's not your body.
That's how that works.
Bodily autonomy, it is not your body.
But I have to pay for child support.
Fantastic job equating women to property right there.
That's brilliant.
Your cash is the same as her body?
Are you insane?
That's not how that works.
That isn't how that works, guys.
But that's where this whole issue stems from, treating women as property.
Because we've got to keep them in their place, right?
I was going to write all of this up in an article, but then I remembered, you know,
there's a lot of numbers and facts and statistics in this and Alabama's 50th in education kind
of seemed pointless at that point.
But there is the hot debate right now about whether people like me should continue to
do workshops and events.
I will tell you, the Dothan workshop, it was aborted.
We're going to take it to another state.
That's what you want us to do, right?
So the real question here isn't any of this.
It's, you know, when does life exist, right?
No, it's not.
When does life exist and the rights of that new life trump the rights of the woman?
That's the real question.
that's how you have to measure society by ethics not your morals not your
religion but when is it okay to put a gun to a woman's head and say this is
what you're gonna do with your body. That's the question you have to answer now
I have my opinion what's yours anyway it's just a thought y'all have a good
time.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}