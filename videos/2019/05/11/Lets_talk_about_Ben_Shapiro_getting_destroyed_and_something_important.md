---
title: Let's talk about Ben Shapiro, getting destroyed, and something important....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2YmDv2GsEvA) |
| Published | 2019/05/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses the incident involving Ben Shapiro and Andrew Neal, focusing on a key moment during their interview.
- Ben Shapiro uses terms like "radical left" to create division right from the start of the interview.
- Neil points out that new ideas in the US are emerging from what is considered the left, particularly the Democrats.
- Shapiro talks about the conservative intelligentsia having debates but doesn't present any new ideas himself.
- When asked about abortion, Shapiro reacts defensively and accuses Neal of bias.
- Shapiro dismisses Neil as being part of the "other team" and questions his motives.
- Neil's main point is that pundits like Shapiro are worsening the quality of debate in American media by immediately labeling others as the enemy.
- Beau notes that this divisive tactic is common on both the American right and left but more prevalent on the right.
- Beau clarifies the difference between liberal, left, and Democrat, stressing that they are not interchangeable terms.
- Beau challenges the notion of a true leftist party in the US and points out the lack of socialist ideologies in mainstream politics.
- The core takeaway is the tendency to shut down opposing views based on political labels rather than engaging in meaningful debate.
- Beau criticizes the conservative movement for lacking new ideas and focusing on maintaining the status quo.
- Beau encourages bridging the gap between left and right by engaging in constructive dialogues to address shared problems.
- The importance of individuals taking the initiative to drive positive change in the political landscape is emphasized.
- Beau concludes by stressing the need for genuine communication and understanding between differing political ideologies for progress to be achieved.

### Quotes

- "just say that you're on the left."
- "It's all about that now, that bumper sticker politics, right, left, red and blue."
- "If you actually start talking to the right-wing voters, you're going to find out that they have a lot of the same problems you have."
- "It's going to be up to you."
- "Anyway, it's just a thought y'all have a good night"

### Oneliner

Beau addresses a divisive interview incident with Ben Shapiro, stressing the need to move beyond political labels for constructive dialogues to foster real progress.

### Audience

Political Activists

### On-the-ground actions from transcript

- Start engaging in meaningful dialogues with individuals from different political ideologies (implied)
- Foster understanding and communication by reaching out to right-wing voters to identify shared concerns (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the interview incident between Ben Shapiro and Andrew Neal, shedding light on the importance of constructive dialogues and bridging political divides for societal progress.

### Tags

#BenShapiro #AndrewNeal #PoliticalDialogues #Division #Progress


## Transcript
Well, howdy there, internet people, it's Bo again.
So we gotta talk about that Ben Shapiro thing, because something important happened, something
that really can help illustrate something.
Now, the important thing was not Ben Shapiro, just, sorry to use the term, getting destroyed
by Andrew Neal, but something happened early on in that, before he took off his mic and
ran away.
OK, so while I'm starting, Americans, you probably
don't know who Andrew Neal is.
You may not, anyway.
Go ahead, and while I'm talking, head over to Wikipedia
and just read.
OK, so the interview starts off with a simple question.
And Ben goes on to talk about how the media in the US
is very liberal.
In fact, he used the term radical left.
And he's getting that term out early,
because it helps divide everything.
It helps divide everything.
Now, the reality is that, for whatever reason,
Fox News is the most trusted news network in the US.
According to the polls, that's what it is.
And then Neil goes on and kind of points out
that the new ideas are coming out of the left in the US, what is considered the left, using
that term, coming from the Democrats.
And Shapiro, I'm never going to make it through this without a laugh, so I'm just going to
... Shapiro goes on this kind of rant to say that the conservative intelligentsia, which
which he is apparently a part of, they have robust debate about what should be done about
the medical system and whether or not climate change is a threat.
Now keep in mind he didn't actually point out any new ideas they have, just said that
they talk about it.
And then he gets posed one kind of tough question, not really, but I mean I guess it could be
if you were used to people that just play ball with you.
Um, he asks him, Neal asks him about abortion.
And Ben just goes off the rails,
tells him to be honest in his own biases.
I hope by now you have read a little bit
about who this guy is.
Um, he goes on to say that, you know,
if you're on the left, just say that you're on the left.
Getting it out there early,
don't believe anything this guy says,
he's part of the other team.
And then he says, it's pretty evident from your questions who you are.
So he doesn't know.
He doesn't know that, I mean, this is kind of like somebody in the US calling Rush Limbaugh
on the left.
Now the whole point Neil was trying to point out was that these type of pundits are coarsening
debate in American media.
This may be one of the only things Andrew Neal and I will ever agree on.
They do.
They do, because they use that divide.
As soon as something gets thrown out that is in opposition, oh, well, you're part of
the other team.
And this happens on the American right and the American left, but it more so happens
on the right, and as evidence of this, scroll through my videos, there's a dozen on gun
gun ownership, advocating gun ownership, talking about guns, and you won't find anything that's
anti-Second Amendment in any of my videos.
I have never been called right wing by anybody.
This is really more of a phenomenon that occurs within the right.
That's where it happens the most.
It does happen in the left.
And I'm sitting here using these terms because they used them.
And it's something I'm gonna clear up here in a minute.
And then Asbin continues to just lose it.
He goes on to say, I'm popular and no one has ever heard of you.
I've heard of you.
I've heard of you, Andrew.
I know who you are.
If you're not gonna go look up his Wikipedia, this is like Rupert.
I mean, I think Rupert Murdoch actually hand-selected this
dude for a job once. He's not anybody's definition of anything remotely related
to the left. I mean, I know he was fairly instrumental in getting the UK
involved in Iraq. I mean, he is not anybody's idea of a leftist, and
that's another problem we have in the US. Because, and it happens so
subtly, I mean I wound up doing it in this video, equating liberal, left, and
Democrat. Those are not the same thing. They're not. We don't have a leftist
party in the US. We have some individual politicians that espouse a
few leftist ideas, but even those are pretty soft. We don't have leftists,
We don't have socialists.
Doesn't matter how many times the Republicans say it.
I have yet to hear any mainstream candidate
advocate seizing the means of production, OK?
Or worker control of the means of production.
It's not.
We don't have that in the US.
So the idea that we even have a left is funny.
Democrats are center right.
They are.
they're to the left of republicans
but that's not saying much
uh...
so but the key thing
that we need to take away from this video i mean other than the laughs
that tendency
to shut down
and immediately say
well no this person's a leftist i don't have to listen to them
even if you're a political plenip with millions of followers
you can just take your microphone off
Throw it down, not listen, not debate, not discuss anything, because they're from the
other team and it's all about that now, that bumper sticker politics, right, left, red
and blue.
And the reality is, Neil was right, the conservatives in the United States and the conservatives
generally everywhere, it's kind of in the definition.
They don't have new ideas.
That is the purview of the liberal movement within whatever country it is in the United
States that passes for the Democrats.
You're not going to get a lot of new ideas from the Republican Party.
Their whole modus operandi is to maintain the status quo, to enforce more government
regulation.
They pride themselves against small government.
Name one Republican administration in which the government shrank.
It's not the case.
So that's what I hope people really see.
Is they see the guy that is known for destroying somebody he is handpicked to destroy.
And then the second he encounters any resistance, oh well this person's a leftist and I don't
have to listen to him.
I don't have to answer.
And the reason, just so you know if you're not going to watch the video, I will put this
video in the links, the reason he left is because the guy started quoting him.
That's what it boiled down to.
He didn't want to answer for the things that he had said that were inflammatory over the
years.
Now I'm kind of kicking myself for doing this video because I don't like giving these guys
a nod.
I don't like helping to platform them in any way.
I'm sure that everybody, the only reason I did this country, everybody has heard of Ben
Shapiro and how he destroys people.
But please, if we're going to have any forward movement in this country, what passes for
of the left and the right in this country have to start talking and what you're going
to find out is if you actually start talking to the right wing voters, you're going to
find out that they have a lot of the same problems you have.
They just believe a different set of politicians is going to fix it and the reality is you're
both wrong.
They're not.
It's going to be up to you.
It is going to be up to you.
Anyway, it's just a thought y'all have a good night

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}