---
title: Let's talk about the next 11-year-old girl....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=eLpe5mMvVSI) |
| Published | 2019/05/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- An 11-year-old girl in Ohio, allegedly raped by a 26-year-old, is pregnant after the state removed her choice through legislation.
- The focus on regulating issues like abortion rather than testing rape kits contributes to societal problems.
- The 11-year-old girl is at a critical developmental stage, facing emotional turmoil, peer pressure, and self-consciousness.
- She may struggle with eating disorders, substance abuse, and self-harm during this period.
- Laws in Ohio may prevent the perpetrator from getting parental rights if convicted of rape or sexual battery.
- Poorly written laws aiming to restrict abortion may lead to unsafe practices and disproportionately affect economically disadvantaged individuals.
- Beau challenges the notion that a six-week-old embryo is equivalent to a living child by presenting a stark moral dilemma.
- He underscores the complex shades of gray in the world that an 11-year-old is beginning to grasp, contrasting with black-and-white legislation.
- Beau questions the heavy burden placed on an 11-year-old in such circumstances.

### Quotes

- "That's the society we live in, in part because the legislatures of this country are more interested in regulating stuff like this than they are making sure that rape kits get tested."
- "It's just a thought, y'all have a good night."

### Oneliner

An 11-year-old girl in Ohio, pregnant after allegedly being raped, faces the consequences of legislation removing her choice, while societal focus shifts from critical issues like testing rape kits.

### Audience

Legislators, Activists, Advocates

### On-the-ground actions from transcript

- Advocate for comprehensive sex education in schools to empower young individuals (suggested)
- Support organizations providing resources for survivors of sexual violence (exemplified)

### Whats missing in summary

The emotional impact of legislation on young individuals and the societal implications beyond the immediate case.

### Tags

#Ohio #AbortionLegislation #SexualViolence #RapeSurvivors #YouthEmpowerment


## Transcript
Well howdy there internet people, it's Bo again.
We've got kind of a heavy subject tonight.
In Ohio, there's an 11 year old girl.
She was allegedly raped by a 26 year old.
She's pregnant.
So she's going to be a mom.
I can say that with certainty
because the governor
and the legislature of the state of Ohio
took that choice away from her.
They removed that option, took it right off the table.
Now, I don't want to talk about her specifically.
Because we don't know.
She may want to carry this child.
She may be from a family of means.
And everything's going to work out.
It's going to be fine.
It's possible.
But there will be the next 11-year-old.
Because that's the society we live in, in part
because the legislatures of this country
are more interested in regulating stuff like this
than they are making sure that rape kits get tested.
So at 11 years old, that next 11-year-old,
while her brain is still developing,
she's beginning to understand
that the world isn't black and white and that there are shades of gray to it.
But she may not fully understand the consequences of her actions.
She's got a good grasp on right and wrong, but the emotional turmoil that she's going
through disrupts the execution of that sometimes.
to understand that her behavior impacts others, beginning to understand that.
She wants to spend more time with her friends, less time with her family.
She's self-conscious about her appearance, her body.
It's about the time that eating disorders, substance abuse, self-harm, those things start
to show up.
She finds it hard to resist peer pressure.
This period has been described as a rollercoaster of distress and happiness.
She needs 9 to 11 hours of sleep a night.
Not the ideal candidate for motherhood.
Not the ideal candidate to be dealing with an infant.
Man, that's a lot.
That's a lot when you really think about it.
Now, there is some good news for her though.
Some good news for that next 11-year-old.
At least if she's in Ohio, because it isn't true of all states.
As long as this guy's convicted of rape or sexual battery
and she goes through court, well, he won't get parental rights.
But if he's not convicted or the charge gets knocked back or she's in another state,
well he might.
She'll deal with him the rest of her life.
This is the result
of poorly written laws
with bad intent that are really
set out to
play to a base rather than accomplish anything because you're not going to
legislate abortion
out of existence, it's not going to happen.
I mean, you're going to succeed in making it unsafe,
creating a black market, and it's
going to disproportionately affect those that
are disadvantaged economically,
because those with means, well, they'll just leave the state.
They'll just go on vacation.
Yeah, so it will be mainly the poor kids
are having kids. Makes sense. That's definitely a way to build a great society.
I know somebody out there right now is going, you know, there's another life at
stake. They wrote this law in Ohio to basically stop anything before or after
six weeks. Six weeks. The embryos less than an inch long. It's not the same. And to prove
it's not the same, I will use the same example I have used before. If I have a jar with a
hundred of those embryos in it, and I've got a toddler named Billy in this hand, I'm hanging
them both off a cliff, you get to pick which one I drop. The only question I have is what
kind of ice cream are you going to buy Billy? Because everybody's going to tell me to drop
that jar. It's not the same. It is not the same. In this case, there is another kid at
at stake, 11 years old, an 11-year-old who is beginning, unlike a lot of the country,
to understand that the world is not black and white. There's a whole bunch of shades
of gray out there. And when you cast it as black and white, this is what happens. Anyway,
Anyway, it seems like a whole lot to put on an 11 year old, to me.
It's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}