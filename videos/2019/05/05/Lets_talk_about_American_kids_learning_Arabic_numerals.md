---
title: Let's talk about American kids learning Arabic numerals....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=0uGvZCg0nN4) |
| Published | 2019/05/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Criticizes the US government for planning to teach American kids Arabic numerals.
- Mentions polls on Facebook showing 60% to 85% against learning Arabic numerals.
- Expresses agreement with the majority, questioning the need for Arabic numerals.
- Believes that the push for teaching Arabic numerals is part of a plot and mentions "Shakira Law."
- Points out that Arabic numerals are the numbers commonly used today.
- Condemns basing opinions on misinformation and lack of context.
- States that knowledge should not be looked down upon and advocates for continuous learning.
- Argues that disregarding knowledge simply because of its Arab origin is ridiculous.
- Emphasizes that education and knowledge are never wasted.
- Encourages Americans to be open to learning, especially when it comes to new topics or ideas.

### Quotes

- "The fact that we have become a nation where knowledge is looked down upon, where gaining knowledge of any kind is looked down upon, it's a bad sign."
- "Education is never wasted. Knowledge is never wasted."
- "So anytime this topic comes up, the answer should pretty much always be yes."
- "It is how we end up with a society where 60 to 85 percent of people answer a poll they know nothing about."
- "Y'all have a good night."

### Oneliner

Beau questions the resistance to teaching Arabic numerals, advocating for a society that values knowledge and continuous learning.

### Audience

Americans

### On-the-ground actions from transcript

- Educate yourself about the origins and significance of Arabic numerals (implied)
- Challenge misconceptions and misinformation about learning new things (implied)
- Embrace a culture of continuous learning and curiosity (implied)

### Whats missing in summary

The full transcript provides a detailed exploration of the resistance towards teaching Arabic numerals and the importance of valuing knowledge regardless of its origin or association.

### Tags

#Education #Knowledge #Learning #CulturalAwareness #Community


## Transcript
Well, howdy there, internet people.
It's Bo again.
We've got to talk about something serious tonight.
Apparently, the US government is planning on teaching
American kids Arabic numerals.
I want to know why.
All over Facebook, there's polls going on about it.
The good thing is American people are starting to wake
up, because it's 60% to 85% saying, no, we don't need to
learn this. And I agree with that. I don't know why we can't just use the numbers we
use today. I mean, when you really think about this, why would we need to do that? I mean,
that's kind of scary. This is obviously, you know, it's a plot. It's got to be. It's got
to be a plot. I mean, and then you hear them talking about it saying that, you know, it's
the basis of mathematics and science everywhere, that's just that
taquita that they use when they're talking. I mean this has got to be a plot
from that Al Jaber group. It's how they gonna bring about Shakira Law. If you
don't know that the numbers on your keyboard are Arabic numerals, that's okay.
There's nothing wrong with that. There's nothing wrong with not knowing
something however basing your opinion off of one word and not actually knowing
the context of what is being asked that that's that that is bad especially when
it is supposedly something that is going to influence policy the numbers we use
are Arabic numerals. That's what they are. But because it has that word Arabic in it,
the immediate reaction from people was no, no, it's got to be bad. Because the
propaganda has gotten so thick since 9-11 that everything associated with the
Middle East is seen as horrible, it has to be evil.
We need to walk that back.
We need to walk that back and we need to do it quick.
Generally speaking, any time the question is, should Americans learn X?
The answer is yes, always, always.
The fact that we have become a nation where knowledge is looked down upon, where gaining
knowledge of any kind is looked down upon, it's a bad sign.
It is a bad sign.
The Middle East has contributed a lot to civilization as a whole, and disregarding something simply
And because it's Arab in origin, it's ridiculous.
It's a genetic fallacy.
It is how we end up with a society where 60 to 85 percent of people answer a poll they
know nothing about.
That's how we got here, was by saying, no, we don't need to learn that.
good is that going to do us? Probably a lot. Education is never wasted. Knowledge
is never wasted. So anytime this topic comes up, the answer should pretty much
always be yes. Yes, Americans do need to learn this. Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}