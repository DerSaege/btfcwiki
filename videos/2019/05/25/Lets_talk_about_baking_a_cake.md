---
title: Let's talk about baking a cake....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=FoP7q6zrS-Q) |
| Published | 2019/05/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces the scenario of being married to someone who behaves like people in the comment section, illustrating a pro-choice female married to a pro-life male dynamic.
- The humor and absurdity escalate as the couple's disagreement unfolds over baking a cake for the wife's birthday.
- The husband pushes for baking the cake, disregarding his wife's concerns about allergies and her lack of desire for cake.
- The wife tries to end the cake debate multiple times, but the husband insists on baking it, leading to a comical argument.
- The husband's persistence leads to extreme statements comparing the wife to a cake murderer and Hitler for not wanting to bake the cake.
- The disagreement culminates in a ridiculous exchange about icing and the wife leaving the situation.

### Quotes

- "Well, okay, so what you're saying then is that all the cakes that made it that had less than perfect ingredients. We should just round all of them up and throw them away too. Cake murderer."
- "Hitler didn't like cakes either."
- "That's not even a thing, and that's not even a cake! It will be if you take responsibility for your actions."
- "You obviously didn't love it. Actually I did love it. It was strawberry cheesecake and I don't want any more cake."
- "You can shove this icing where the sun doesn't shine."

### Oneliner

Beau humorously navigates a disagreement about baking a birthday cake, turning absurd with comparisons to cake murder and Hitler.

### Audience

Couples

### On-the-ground actions from transcript

- Bake a cake for a loved one's birthday (exemplified)
- Share cakes with neighbors (implied)

### Whats missing in summary

The full transcript provides a comical illustration of an escalating disagreement over baking a birthday cake, reflecting on communication and compromise in relationships.

### Tags

#Relationships #Humor #BirthdayCake #Disagreement #Compromise


## Transcript
well howdy there internet people it's Bo again so tonight's video might get cut
short it's my wife's birthday I'm waiting for her to get home but in the
meantime have you ever thought what it would be like if you were married to
somebody who behaved the way people do in the comment section like especially
if you were a pro-choice female married to a pro-life male hi honey happy
birthday I made you a cake. Thanks that's not a cake. Well it will be if you take
responsibility for your actions and bake it. Baby it's late I don't even want
cake. Well you should have thought about that before you had a birthday everybody
knows that cakes follow birthdays. Um and did you forget I'm allergic to milk? I
I mean, I could get really sick or die if I eat that.
You know, a good birthday girl would risk anything for her cake.
Babe, it's late.
Let's just not have cake.
Do you know how many people would love to have this cake?
Babe, it's OK.
I already ate cake at work.
And oh, so because you already had a cake,
you're just going to kill this cake.
Just get rid of it.
I mean, I feel sorry for that first cake.
You obviously didn't love it.
Actually I did love it.
It was strawberry cheesecake and I don't want any more cake.
Let's just make this decision now.
We're not having cake tonight.
We're not having cake.
Fine.
You don't want the cake?
Bake the cake and give it to the neighbors.
It's my time and my oven and I don't want to bake a cake.
I'm not baking a cake tonight.
I paid for this.
Isn't it half mine?
Babe you bought that mix like when we moved into the house. I mean, it's expired by now. It probably won't even bake
properly
Well, okay, so what you're saying then is that all the cakes that made it that had less than perfect ingredients
We should just round all of them up and throw them away too. Cake murderer
Oh, there's options other than cake. I mean we have other stuff
Hitler didn't like cakes either
Did you just compare me to Hitler?
That's, you've obviously lost your mind, I'm leaving.
Well you know, if you try to leave and not make this cake, I'll call the cops and extradite
you back here for cake murder.
That's not even a thing, and that's not even a cake!
It will be if you take responsibility for your actions.
Look, I've already accepted way too much responsibility in our lives right now and I'm not baking
a cake tonight.
I'm not accepting-
Murderer!
I'm not taking responsibility for baking a cake!
Murderer!
It's gonna be a thing.
Fine, give me the cake pan.
No.
Ha ha ha.
I'll send you a check for half of the icing.
I'm out.
Ha ha ha.
You can shove this icing where the sun doesn't shine.
It's just a thought.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}