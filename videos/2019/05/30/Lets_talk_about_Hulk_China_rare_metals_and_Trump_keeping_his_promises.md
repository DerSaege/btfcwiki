---
title: Let's talk about Hulk, China, rare metals, and Trump keeping his promises....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=cx6XjvMYrWE) |
| Published | 2019/05/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- China has a history of issuing a catchphrase, "don't say we didn't warn you," that has led to shooting wars.
- The catchphrase has been recently issued by China to the United States, but it's likely related to the trade war rather than escalating to an actual war.
- China is considering cutting off or reducing exports of rare earth minerals in response to Trump's trade war.
- Rare earth minerals are vital for high-tech goods and China controls about 85% of the market.
- The trade deficit is not inherently bad, but Trump is using tariffs as a solution, which ultimately affects American consumers.
- Trump's focus on keeping campaign promises may lead to ineffective solutions that impact the economy negatively.
- The trade war won't bring back American manufacturing jobs but might shift them to other countries like Vietnam.
- The decision to move jobs abroad is driven by economic factors and the cost benefits of operating in countries with less stringent environmental regulations.
- Mining rare earth minerals has severe environmental impacts, as seen in China where they are currently mined.
- Trump's decisions regarding the trade war will have lasting negative impacts on the economy and future presidents will likely face the consequences.

### Quotes

- "China has issued a catchphrase that has led to shooting wars throughout history."
- "The decisions that Trump is making are going to have long, long lasting and far reaching impacts in the economy, and they're not good ones."
- "We're all gonna end up paying for this."

### Oneliner

China issues a warning to the US amidst trade tensions, signaling potential economic impacts and environmental concerns, with long-lasting repercussions.

### Audience

Global citizens, policymakers

### On-the-ground actions from transcript

- Research the environmental impacts of mining rare earth minerals (suggested)
- Advocate for environmentally responsible practices in mining (exemplified)

### Whats missing in summary

The full transcript provides a detailed analysis of the potential consequences of the US-China trade war and the environmental impacts of mining rare earth minerals.

### Tags

#USChinaTradeWar #RareEarthMinerals #EconomicImpact #EnvironmentalConcerns #GlobalTrade


## Transcript
Well, howdy there, internet people, it's Bo again.
You wouldn't like me when I'm angry.
Now, if I'm Bruce Banner, you know what's about to happen.
I'm gonna turn green, my clothes are gonna rip,
I'm gonna get all buff and turn into the Incredible Hulk.
It's a catchphrase, it's a warning.
China has one of those two.
Theirs is, don't say we didn't warn you.
They've issued this catchphrase twice throughout history.
Both times it turned into a shooting war.
They issue it through the People's Daily, which is a state run newspaper over there.
They just issued it to the United States.
I don't think it's going to turn into a shooting war.
It seems pretty clear from the context this is about the trade war.
And they're done with Trump's trade war.
They're done with it.
They're going to cut off or drastically reduce exports of rare earth minerals.
That certainly seems like their next move, kind of what they said.
They're done with the tariffs because unlike Trump, they understand them.
They're not going to hurt their own people.
Look, on one hand, you have committees of people who are economists and successful businessmen,
international relations experts, people who blended a communist and a capitalist system
to create an economic superpower.
On the other hand, you have a dude who found a way to bankrupt a casino.
Who do you think is going to come out on top here?
Okay rare earth minerals.
What are they?
China controls about 85% of the market.
They are one of 17 elements.
They're soft, malleable metals.
And they're used in a lot of high-tech goods.
The device you're watching this on, they're probably used in that.
They're used in guidance systems.
All of a sudden the Pentagon's like, we need to find a replacement.
And that's what's going on.
This is going to have long-reaching impact.
And the real problem here is that Trump is trying to keep his campaign promises.
That's the problem.
And I know people are like, why is that a problem?
Because he made those promises to appeal to people who didn't understand the issues.
So the solutions that made them feel good probably aren't very effective.
Okay why are we fighting this trade war?
Because we have a trade deficit.
Okay.
So why are we fighting the trade war?
People associate trade deficit with the deficit.
Therefore they inherently think it's bad.
Trade deficits are not inherently bad.
All it means is that you're buying more from somebody than you're selling to them.
I explained this to my 13 year old today.
And he's like, so we have a trade deficit with the grocery store.
Yes.
But we need that stuff.
Yes.
I mean, if we don't buy that stuff, then, you know, everything else doesn't work.
Yes.
Trade deficit is not inherently bad, but how are we going to fight this trade war?
With tariffs.
Yeah, tax their goods.
Makes sense, until you realize that cost is passed on to the American consumer.
Just right, raises the price of everything.
And the response is, well, it'll bring American manufacturing jobs back, and no, it won't.
They'll move from China to Vietnam.
That's what they'll do.
They're not going to come back here.
Foreigners did not steal your jobs.
A CEO made a decision, a business decision, an economic one.
cheaper for them to operate in the global south where they can pollute and destroy the
environment at will than it is to operate within the United States and be environmentally
responsible.
That's why.
Because nobody cares what happens to the people in the global south.
So we don't boycott them if they're not environmentally responsible there.
That's why.
They're not coming back here, but let's say they do, and they could in this case, rare
earth minerals.
They may have to.
Okay, I think we have some in Nevada and California.
You go take that job.
You handle that thorium.
I forgot to mention that.
This stuff's radioactive, has to be washed with hydrochloric acid.
Make sure you have a sip of the water from that area too.
While you're looking into this subject, go ahead and research the environmental impacts
of mining rare earth minerals.
Go ahead and take a look.
what it did in the areas in China, where they manufacture it, where they mine it.
It's not a good thing.
It's not something you want going on here.
Anyway, the fact is at the end of the day, the decisions that Trump is making are going
to have long, long lasting and far reaching impacts in the economy, and they're not good
ones.
are not good ones. We're all gonna end up paying for this. I feel sorry for the
next president. I really do because they're gonna have to fix all this and
they're gonna end up catching the blame because that's when a lot of it's gonna
actually start to happen. When it does, well, don't say we didn't warn you.
Anyway, it's just a thought y'all have a good night

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}