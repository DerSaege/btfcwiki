---
title: Let's talk about how men are from memes and women are from very long posts....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=bSuCtoAZDcw) |
| Published | 2019/05/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Speaking in generalities, discussing the impact of social media posts on real-life perceptions.
- Sharing personal experiences related to the Kavanaugh hearings and the abortion debate.
- Emphasizing the importance of being mindful of the messages conveyed through social media comments.
- Explaining how a seemingly harmless meme on abortion led to a relationship ending.
- Pointing out that sharing memes can limit one's thought process and influence others' opinions.
- Advising against using memes and encouraging open, direct discussions instead.
- Warning against the implications of sharing content without fully understanding its message.
- Stressing the significance of being aware of how one's online actions shape others' perceptions in real life.
- Encouraging deeper understanding and communication rather than relying on superficial social media posts.
- Concluding with a reminder to be thoughtful about the messages shared online.

### Quotes

- "Your social media posts are seen by people in your real life, and it shapes their opinion of you."
- "Stop using memes. It limits your thought."
- "She saw something. She understood something that you didn't get."
- "Stop letting other people define your thoughts like that because this is what happens."
- "It's an important lesson to learn."

### Oneliner

Beau sheds light on the influence of social media posts on real-life perceptions and warns against the limitations of sharing memes, advocating for meaningful communication over superficial interactions.

### Audience

Social media users

### On-the-ground actions from transcript

- Have open, direct discussions instead of relying on memes (suggested)
- Be mindful of the messages shared online and their potential impact on real-life perceptions (implied)

### Whats missing in summary

Detailed examples of how social media posts can influence personal relationships and perceptions. 

### Tags

#SocialMedia #Communication #Relationships #Perceptions #Memes


## Transcript
Well, howdy there internet people, it's Bo again.
Before we even start, I want to say yes, I understand, I'm talking in generalities.
Not everything I'm going to say is true of all women or all men.
Go ahead and hashtag it, not all men, not all women, whatever.
I am speaking in generalities, but there's an important lesson to be learned here.
I've talked about it in other videos.
And up until this point, I didn't really
have an experience I could clearly detail,
because the last time I talked about this
was during the Kavanaugh hearings.
And the experiences were more private.
They couldn't be discussed the way we were going to do this.
Guys, men in particular, listen up.
Your social media posts are seen by people in your real life, and it shapes their opinion
of you.
So your pizza cutter comments, all edge and no point, they may not say things that you
want them to say about you.
During the Kavanaugh hearing, we had a bunch of guys just commenting, well, she's a liar.
She's making it up.
the women who they don't know were sexually assaulted who are in their life.
We're looking at them like because it echoed what was said about them or the reason they
didn't come forward.
Now during the abortion debate got a lot of the same thing happening.
So I had this conversation and basically it starts with, hey, is one of my friends still
single?" And I'm like, yeah, he's a single dad with five kids and one of them has special
needs, of course he's still single. Again, I understand, not all women. It was a joke.
But I thought you were dating guy X. She's like, no, not anymore. What happened? We had
an argument over abortion.
But y'all are both pro-life.
Well, it wasn't really about abortion.
He shared a meme.
Oh do tell.
What was this meme that ended a two year relationship?
Breaking news.
Women don't like the government forcing them to be a mother.
They only want government to force men to be fathers.
Right now, guys are like, well, that's about child support.
And that's where the train of thought stops, right?
Women are more intuitive.
They see things.
They read things that we don't see.
Now once it was sent to me, yeah, I completely got it.
Cutting a monthly check, that's not being a father, guys.
That is not being a father.
child support does not make you a father. Now if you are a woman who is very
interested in having children, somebody who shares this meme, oh they're no
longer a suitable candidate because they're gonna phone it in. They're gonna
do the bare minimum. And I know there's guys out there right now going, well it's
just a meme. Stop letting other people define your thoughts like that because
this is what happens. You may not agree with that statement. You may not really
mean that, but you co-signed it. You shared it. You put it out there with your picture
and your name on it. And then what normally happens is somebody challenges
it in the comment section or in the replies. And then you double down and
Defend it.
And the people in your life see that.
They see it.
And it shapes their opinion of you.
Stop using memes.
It limits your thought.
That's why on the Facebook page, you can't share images.
If you want to discuss something, discuss it.
Don't.
it what somebody else thought and put on a picture. So, somewhere there's a guy saying
oh man we broke up. Crazy psycho. I mean she was arguing with me about child support. We
don't even have kids yet. Psycho X. She saw something. She understood something that you
didn't get. She read the subtext of what you were saying, even though you may not
have meant it. It's an important lesson to learn. Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}