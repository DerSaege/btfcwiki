---
title: Let's talk about Brexit, Ireland, and a hard border....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=br9YiOB0Ddo) |
| Published | 2019/02/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the division of the island of Ireland between North and South, controlled by the UK and Republic of Ireland respectively.
- Describes the significance of Brexit in relation to the open border between Northern Ireland and the Republic of Ireland.
- Mentions the US's stance on backing a post-Brexit trade deal that includes an open border on the Irish island.
- Raises concerns about potential violence if a hard border is implemented, specifically mentioning Republican violence.
- Differentiates Irish Republicans from American Republicans, noting the political ideologies of each.
- Advises individuals living near potential conflict areas to stay completely out of any involvement to ensure safety.
- Talks about the increased accessibility of explosives due to online materials, making it easier for individuals to manufacture them.
- Mentions the historical practice of giving warnings before bomb attacks and the potential for modern technology to aid in issuing alerts.
- Warns against getting involved in areas where violence might erupt and advises staying away from such hotbeds.
- Comments on the changing dynamic regarding targeting the royal family and British establishment in potential attacks.

### Quotes

- "I mean they make Bernie Sanders look like Trump."
- "Stay out of whatever area develops as a hotbed, just stay out of it."
- "This is just throwing a match on the powder keg."

### Oneliner

Beau explains the implications of Brexit on the Irish border, warning about potential violence and advising complete non-involvement for safety.

### Audience

Irish residents, Brexit observers

### On-the-ground actions from transcript

- Stay completely out of potential conflict areas (implied)
- Remove any nationalist symbols from visible locations (implied)
- Keep any political affiliations to yourself (implied)
- Pay attention to warnings through modern technology (implied)

### Whats missing in summary

The full transcript provides a detailed insight into the potential consequences of Brexit on the Irish border and the historical context of violence in the region. Viewing the full content offers a comprehensive understanding of the situation and the precautions that individuals can take.

### Tags

#Brexit #Ireland #Border #Violence #PoliticalImplications


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we're leaving the U.S.
We're going to talk about Brexit, specifically how it relates to Ireland
and the border. Okay, so crash course for Americans real quick. The island of
Ireland is divided between North and South. The North is under the control of
the UK, the South is the Republic of Ireland. The Republic of Ireland is part
of the EU. The UK wants to leave the EU. This presents an issue because that border between
Northern Ireland and the Republic of Ireland is supposed to be soft. It is supposed to
be open, more or less. Part of the reason for Brexit was to stem migration and open
borders, what they're calling open borders.
So it creates a very unique political situation.
The US, and this was actually public now, they've told the UK they will not back any
post-Brexit trade deal that doesn't include an open border on the Irish island.
the UK is probably not going to be happy about that.
The message I got was very concerned if a hard border happens about a potential upswing
in violence.
Yeah, yeah.
It's not a maybe, it's yes.
If there is a hard border, you can guarantee there will be Republican violence.
It's going to happen.
Americans, Irish Republicans are nothing like American Republicans. Generally
Irish Republicans are to the left of anything we have in this country. I mean
they make Bernie Sanders look like Trump. So bear that in mind. Now your message
said you live on an old route. It's probably not an old route. It's probably
just dormant. If you really want to stay out of it, you have to stay out of it
completely. That means if you see something say nothing okay and that goes
for anything. Remove any form of nationalist symbol on either side from
your vehicle from the outside of your home. I don't know which passport you
have but you keep that to yourself. Don't get involved at all. If you
were to see something on that route and say something they will kill you or
kneecap you.
The other thing you have to keep in mind is that during the last big upsurge in violence,
it took a little bit of training to make the explosives.
You had to have been involved in the movement.
That's not true anymore because of Islamic groups putting training material online.
It's available to anybody.
So it's not going to be fertilizer and coffee grinders anymore.
They're going to have a whole new set of explosives and they can be done, they can be manufactured
by people who are operating alone.
That makes it a little bit more scary.
The good news is that typically the Republican side was pretty good about giving warning
about bombs.
They really were.
I would imagine with today's technology there will be some form of text message on your
cell phone when a warning comes through.
Pay attention to them.
Stay out of whatever area develops as a hotbed, just stay out of it.
There's no reason to go there.
Whatever that area is, there's no reason to go there.
Now another change in dynamic was during the last period, the royal family and the British
establishment was off limits.
Today there's enough dissatisfaction with them that the Republican command may not see
it as something to keep off limits.
So there may be activity in England, which would be aside from a few attacks.
That didn't happen, but it may be over there.
Although I would definitely expect violence along any hard border, I would just hope that
it doesn't happen because it's not a maybe. It will. It will cause violence and we're
at that point, it's generational, you know, 800 years and every generation, sometimes
it skips a generation, but there's an upsurge in violence. We're hitting that point right
now anyway. This is just throwing a match on the powder keg. So anyway, good news, you
asked is there any good that can come with this? Yeah. Any time a government entity fractures
and gets smaller, it's more responsive to the people. Now if you do hold sympathies
to an idea of a united ireland this may cause it because the brits may decide that they don't want
they don't want to deal with it anymore you know it's funny all of this time where it's
been by uh bullets and bombs it may be decided by balance anyway that's just a thought have a good
Good night, stay safe.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}