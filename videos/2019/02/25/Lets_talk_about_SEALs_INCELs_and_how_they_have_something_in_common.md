---
title: Let's talk about SEALs, INCELs, and how they have something in common....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=lLZfKs5N8jQ) |
| Published | 2019/02/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the topic of Dick Marcinko, the founder of SEAL Team 6, and addresses his controversial reputation.
- Questions the conviction of Marcinko, suggesting it may have been a result of personal vendettas.
- Comments on Marcinko's arrogance based on personal interactions but acknowledges his military accolades.
- Explores Marcinko's unique approach to building SEAL Team 6 by selecting individuals who had to struggle, showcasing psychological fortitude.
- Draws parallels between Marcinko's team-building strategy and insights into the incel community.
- Observes intelligence, self-awareness, misogyny, and racism within the incel groups.
- Identifies three types of incels: physically unattractive, psychological or cognitive issues, and those with negative personality traits.
- Notes that violent rhetoric and suicidal talk predominantly stem from the third group of incels.
- Encourages incels to potentially use their experiences and psychological resilience for positive purposes.
- Urges incels to avoid resorting to violence or isolation, suggesting that improving society benefits not only them but future generations.

### Quotes

- "He looked for people that had suffered, that had that psychological fortitude."
- "Killing people, killing yourself, it's not the answer."
- "You're not going to make it better for the people after you. You're going to make it worse."

### Oneliner

Beau delves into the controversial figure of Dick Marcinko, incel community insights, and the importance of psychological resilience in facing challenges and improving society.

### Audience

Incels, Community Builders

### On-the-ground actions from transcript

- Reach out to support groups or mental health professionals for assistance (suggested)
- Engage in positive community activities to channel experiences and resilience positively (implied)
- Advocate for societal change to prevent isolation and violence (implied)

### Whats missing in summary

Insights on the impact of societal stigmas and the importance of empathy in understanding and addressing the struggles faced by different groups in society.

### Tags

#DickMarcinko #SEALTeam6 #IncelCommunity #PsychologicalResilience #SocietalChange


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight, we're going to start off talking about a guy
named Dick Marcinko.
Now, this is the guy that built SEAL Team 6.
And when you think of SEALs, you're
probably thinking of SEALs.
The image of a SEAL comes from them.
Okay, now just to go ahead and head this off in the comments section, yes, I am aware.
He was convicted of a crime, I'm aware of that.
He made a lot of enemies over the course of his career and it certainly appears to me
from looking at it that this was a case of those enemies getting even.
It looks like a case of, here's the man, now show me the crime.
At the end of the day, he got convicted and sent to prison for a $100,000 kickback scheme
in a unit that had a black budget.
It doesn't even make sense.
He's a pretty bright guy.
If he wanted to skim a little bit of money, there were a whole lot of ways to do it that
didn't leave a paper trail.
I'm not really buying it.
And the other comment that always comes up about him is that he's arrogant.
Most of the people who say that have never met him.
I have, twice.
Once I shook his hand and was in his presence for 20-30 minutes.
Once I actually talked to him for a little bit.
And because of that, I can say with 100% certainty, yeah, he is.
He is a pretty arrogant dude.
At the same time, Silver Star, four Bronze Stars, Legion of Merit, have built the most
elite fighting unit on the planet.
Yeah, maybe he's a little arrogant.
Maybe he earned it.
And part of it, I think, is his brand, to be honest.
I think he actually plays up his arrogance to be honest, but whatever.
What I want to talk about is him building cell team six because there was something,
he did something that I found very unique and very, very interesting.
When he was pulling guys to go to this unit, when he was picking the very first guys for
this unit. He didn't pull guys that were at the top of their class at Buds. Buds is
Sill School for those that don't know. He did not, that's not where he got. He looked
for the guys that were, that had to struggle to make it through it because he knew that
they had suffered. He knew that they had the psychological fortitude to make it through
Buds, and therefore would be what he needed in combat.
It was amazing.
That's an interesting thought, and it's not one that I would second-guess, really.
It makes sense.
And for those that don't know, Buds is, if it's not the hardest course in the military,
it's in the top three.
It's a meat grinder.
I've known quite a few guys that went and didn't make it, and these are tough guys.
So that was something that always stuck with me because this guy is widely regarded as
the ultimate team builder.
And that's who he looked for.
So for the last, well, not even few, but for quite a while,
I've been stalking the incel boards.
Because of that, I know you guys are watching this.
You guys seem to post anything that has to do with you.
And while you're watching it, take it for what it is.
Take it for what it is.
All right, so this is what I have kind of observed.
These guys are intelligent, extremely intelligent.
On a general level, when you're talking
about the average person on these boards in this group
that self-identifies as an in-cell, they're smart.
They're extremely self-aware and very critical of themselves.
There's a lot of misogyny, a lot of it.
Some of it can be written off as a defense mechanism.
There's a lot of racism.
But not all of it is supremist racism.
Some of it is...
It's weird because I've never seen it anywhere else.
I knew a couple military contractors.
One was from Ireland and one was Colombian,
of Colombian descent.
There are derogatory terms for people from these countries that both end in ick.
And that's what their men called them.
Their men called them that.
God help anyone else who did.
Comradery almost.
And that's a lot of what I see.
Now don't get me wrong.
There is a lot of real racism there, supremacist racism, but at the same time there's also
that camaraderie and it kind of blends, it's weird.
Aside from that, those are general observations, aside from that there's three kinds of incel.
Outside looking in at first it seems like it's one group, one group of guys.
It's not, there's three different groups.
The first group are guys who are ugly.
And I don't mean that in the sense of, well, they're not really attractive.
I mean, they're guys who are so unattractive, it doesn't matter how good their personality
is, odds are they're never going to have a meaningful relationship.
It's going to take a really strong woman to get past it.
And when I told somebody I was going to say that, they're like, you can't say that about
people.
I don't think they're going to take offense to it.
I think there may be a group of them that are actually happy somebody finally said it.
So that's the first group.
The second group, they look normal.
You would not look at them and say, you know, well, this guy's an incel from a picture.
But they have a psychological issue or some cognitive problem that puts them in the same
boat. Realistically, they're never going to date. It doesn't matter if you look like
the dude from Frontier if you have Tourette's and walk around screaming profanity all the
time. That makes dating really hard. So those are the first two groups. And then the third
group, there's nothing wrong with them.
They don't have those same characteristics.
They have bad disposition or bad personality, things that can be changed.
For the first two groups, not a lot of self-work is going to fix the problem.
There's a point where there's only so much that can be done.
And some of these guys are genuinely past that.
Some of them are low self-esteem and maybe think they're worse off than they are.
But some of them are genuinely past that.
But that third group, they're just not datable because they're not good people.
But here's the interesting part.
This is the part that blew my mind, because as self-aware as they are, I hadn't seen
any of them comment on this.
Guess which group most of the violent rhetoric and talk of suicide comes from?
The third group.
I have to believe that it has something to do with why Marcinko chose the guys at the
bottom of the class.
The psychological fortitude that it takes for those first two groups to have made it
this far and not already take the easy way out, it says something about that.
Guys I wish I had some sage advice for you.
I don't.
I don't.
of your assessments, they're accurate. They're accurate. I don't know a way where you're
going to get what you want. I don't know anything for some of you that you're right. You're
never going to get the picket fence, man. And I feel for you. I really do. Life has
dealt you a pretty raw deal.
But at the same time, keep in mind that when the man that is widely regarded as one of
the greatest small unit warriors of the 20th century was looking for people that were capable
of
the impossible.
He looked for guys like you.
He looked for people that had suffered,
that had that psychological fortitude.
Maybe you can put it to some good use.
Maybe you can use that.
Now I know what you're saying, why would you want to help society when society has
done nothing for you?
There's going to be people
like you
after you
and if you don't change society for the better
it's going to be the same for them
killing people killing yourself
it's not the answer
it's not going to change society
the only thing it's going to do
is add
level of fear to people like you.
So it's going to add yet another layer of isolation to them.
You're not going to make it better for the people after you.
You're going to make it worse.
Anyway, it's just a thought.
I'll have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}