---
title: Let's talk about a hypothetical question, military readiness, and the wall....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=h7WkKRmj-1c) |
| Published | 2019/02/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Poses a hypothetical question about a list of facilities being targeted by a foreign power and their potential goals and strategies.
- Mentions critical facilities like maintenance facilities for the F-35, dry dock facilities at Pearl Harbor, and training facilities for special operations.
- Suggests that targeting these facilities is an attempt to degrade readiness before a war.
- Reveals that the list of facilities in question are actually Department of Defense programs being sacrificed for border wall construction.
- Criticizes the idea of sacrificing vital military programs for a partial border wall, which falls short of fulfilling promises to rebuild the military.
- Expresses concern about the negative impact on military readiness and logistics due to diverting funding from vital programs.

### Quotes

- "They're crippling our air because if these aircraft aren't maintained, and they don't fly. It's that simple."
- "What we're doing is we're going to give up our ability to defend against an actual invasion to build part of a wall to stop an invasion he made up."
- "Two hundred thirty-four miles is not building the wall. It's not. It's a start."

### Oneliner

Beau poses a hypothetical scenario about targeting critical military facilities, revealing they are actually sacrifices for a partial border wall, jeopardizing military readiness.

### Audience

Policy Makers, Activists

### On-the-ground actions from transcript

- Contact policymakers to advocate for prioritizing military readiness over border wall construction (suggested).
- Join advocacy groups working to protect vital Department of Defense programs (exemplified).

### Whats missing in summary

The full transcript provides detailed insights into the potential consequences of sacrificing military programs for border wall construction.

### Tags

#MilitaryReadiness #BorderWall #DefensePrograms #DepartmentOfDefense #HypotheticalScenario #FundingPriorities


## Transcript
Well, howdy there internet people, it's Bo again.
So I had a friend send me a list, and along with this list he posed a hypothetical question.
He said if a foreign power was targeting these facilities, what do you think their goal is?
What's their strategy?
I love questions like this, by the way.
I'm going to give you guys the list, and you guys can tell me what you think it is.
Okay, maintenance facilities in Miramar, California for the F-35.
That is our state of the art fighter, state of the art aircraft.
Vehicle maintenance facilities in Kuwait, dry dock facilities at Pearl Harbor, F-35
hangars at Luke Air Force Base in Arizona, F-35 hangars at Eglin Air Force Base in Florida,
a staging area in Poland, a mobility center in Greece, the training facilities that the
SEALs use, and the training facilities at BRAG used by what you guys would call Delta Force.
Man, that's a list. So they're crippling our air because if these aircraft aren't maintained,
and they don't fly. It's that simple. They can't be properly maintained, they can't
fly. The dry dock facilities at Pearl Harbor are pretty important to the Pacific fleet
and the training facilities for the special operations community are very important. That's
all they do. They train constantly. So, my answer, they're trying to degrade our readiness.
This is something you would do before a war.
These are small operations you would do to degrade the ability of the opposition to respond.
That's what it is.
Okay, so what is the list really?
These are DOD programs that are on the chopping block so Trump can build 234 miles of wall
along a 1,954 mile border.
I hate to break it to you guys,
the wall's still not getting built.
It's still just a section of it.
This is what we may have to give up to get 234 miles.
In addition to this, there were a bunch of other things,
a bunch of other DOD programs that might get hit,
but they also want to take $2.5 billion
dollars from DOD's drug interdiction program. I personally don't care. I'm not a supporter
of the war on drugs. However, if you bring up in your speech that the wall will help
stop drugs, it won't. But it was in the speech. You might not want to take two and a half
billion dollars from one of the only successful drug interdiction programs. It seems a little
There's no other word for it.
You know, Trump had two main campaign promises.
Build the wall and rebuild our military.
Two hundred thirty-four miles is not building the wall.
It's not.
It's a start.
This is not rebuilding the military.
It is degrading it.
It is hurting its ability to respond.
The F-35, by the way, is a $1.5 trillion program.
Each aircraft cost around $90 million bucks.
But I guess we're just going to set them out on the tarmacs.
No hangars.
No maintenance facilities.
Not going to build them.
Now he's saying, oh, well, we'll just build them a year later.
That's not how DOD works.
That is not how DOD works.
Construction projects on posts and bases, they're done like 10 years out.
They're planned way ahead and they're done that way for a reason.
You're going to have fallout from this.
If this isn't built, then this can't be built, then this can't be done.
These planes can't come here because we don't have the facilities to house them.
Even though we've already kind of let the pilots trained on these aircraft go, you can't
do this with DoD.
When you're talking about an organization that large, the logistics involved is immense.
If you have not watched one of the latest videos I did about the new immigration numbers,
please go watch it.
This, what we're doing is we're going to give up our ability to defend against an actual
invasion to build part of a wall to stop an invasion he made up.
is insanity. It is insanity. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}