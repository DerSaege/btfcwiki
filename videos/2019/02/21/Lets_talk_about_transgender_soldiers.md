---
title: Let's talk about transgender soldiers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=99Jw8ci4Uvk) |
| Published | 2019/02/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Responds to a question about transgender people in the military impacting readiness and the importance of their service.
- Approximately 1000 to 15,000 transgender individuals serve in the military, a small percentage.
- Military readiness is based on worldwide deployability and unit cohesion.
- RAND Corporation's study found no breakdown in unit cohesion in countries allowing transgender individuals to serve.
- Concerns about worldwide deployability due to hormone treatments requiring refrigeration, similar to insulin dependence.
- Loss of time for a transgender person in labor years is minimal, not a significant readiness issue.
- Beau compares the minimal impact of transgender individuals on readiness to pregnancy sidelining soldiers for nine months.
- Acknowledges a legitimate readiness concern for a small percentage of transgender individuals requiring long-term hormone treatment.
- Beau argues that turning away willing recruits impacts readiness more than allowing transgender individuals to serve.
- Draws parallels to historical military acceptance movements like desegregation and acceptance of gay individuals.
- Points out the military's role in driving social acceptance and the importance of diverse representation in the armed forces.
- Condemns the ban on transgender individuals serving in the military as politically motivated, not related to readiness.
- Expresses disappointment at the mistreatment and politicization of soldiers who have served.
- Mentions the California National Guard's decision to allow transgender individuals to serve despite DOD policy, expressing concerns about potential issues.

### Quotes

- "It is upsetting that soldiers who have fought are being discarded in this way by the government."
- "This small percentage of soldiers has become a political pawn."
- "At the end of the day, this ban, it has nothing to do with readiness, nothing."
- "The military has always been on the cutting edge of social acceptance."
- "I served with a black guy. They're all right."

### Oneliner

Beau breaks down the insignificant impact of transgender individuals on military readiness and stresses the importance of their service for societal acceptance and diversity.

### Audience

Advocates for inclusive military policies.

### On-the-ground actions from transcript

- Support efforts to allow transgender individuals to serve openly in the military (exemplified).
- Advocate for policies that prioritize diversity and inclusion in the armed forces (suggested).

### Whats missing in summary

The full transcript provides a detailed analysis of the impact of transgender individuals on military readiness and the historical context of diversity in the armed forces.

### Tags

#Military #TransgenderRights #Diversity #Inclusion #SocialAcceptance #Readiness


## Transcript
Well howdy there internet people, it's Beau again.
So almost immediately after that last video going up, I got a question sent to me and
I'm going to answer it.
Do transgender people impact military readiness and why is it so important for them to serve
anyway?
There are roughly, depending on the estimate, 1,000 to 15,000 transgender people serving
in the military.
This is a small percentage.
This is a very small percentage.
Readiness itself, there's a couple of components to it, worldwide deployability and unit cohesion.
Those are the two things that really matter.
Anytime you want to know something about the military, go to Rand Corporation.
They've done a study on it.
Whatever it is you want to know, they've done a study on it and they did it right.
So Unicohesion.
RAND Corporation looked at the UK, Australia, Israel, and Canada.
Four countries that allow transgender people to serve have policies that can be researched
and shared data.
What they found out was that there was no breakdown in Unicohesion.
None, I mean, none, okay?
So that one's out, move on from that.
The next one is worldwide deployability.
Now what this means is there are a lot of conditions
that can stop you from being deployed.
You can't be broke and be sent overseas, it's that simple.
It's why insulin dependent diabetics can't join.
The refrigeration required doesn't exist down range,
so you don't get to go.
therefore you can't be in the military.
Okay.
Rand Corporation did a study on this too.
They determined that in labor years,
the loss of time for a transgender person
would be 0.0015.
Over a 20-year career,
I think this amounts to like three months.
It's nothing, that's nothing.
A pregnancy takes a soldier out for nine months.
That's not a readiness issue.
That is not a readiness issue.
There weren't any.
Now there is one legitimate readiness concern,
and it only affects a small percentage
of this small percentage we're talking about.
But there is a hormone treatment that requires refrigeration.
Just like insulin dependence, if you have to be on this for more than nine months, there's
a readiness issue.
If you don't, there's no readiness issue.
And that's probably how DOD should look at it.
They should look at it in terms of readiness.
That's their argument.
The reality is, it is probably more damaging to readiness to turn away willing recruits
when they're missing their recruitment goals a lot.
Now the other part of that question, why is it so important for them to serve anyway?
I can't answer for them, I can't.
I can tell you why it would be important to me though.
In 1948 the military desegregated before desegregation in the South.
While segregation was still going strong in the South, black soldiers were welcomed into
the most elite units in the U.S. Army with open arms, and they had to go home and couldn't
drink from the same water fountain.
But that slowly changed in a large part to people saying, you know, I served with a black
guy.
They're all right.
In 1993, Don't Ask, Don't Tell came out.
This was basically what it said was you could be in the military and be gay, but you couldn't
tell anybody you were gay.
By 2004, states started allowing gays to get married.
By 2011, you could serve in the military openly if you were gay.
2015, every state in the country, they can get married.
Contrary to the image of the conservative soldier, the military has always been on the
cutting edge of social acceptance.
If you want to get your demographic that is seen as an outgroup accepted by American society,
in the military is critical because, yeah, I served with one of them, you know, they're
all right, and then there's also the fact that nobody wants to talk bad about a soldier
in this country.
That's why it's important.
That's why it would be important to me.
I don't know why it's important to them, and I'm not going to speak to it in that way.
At the end of the day, this ban, it has nothing to do with readiness, nothing.
It has to do with playing to the base, playing to the political party that is in power, playing
to their base.
That's what it has to do with.
This small percentage of soldiers has become a political pawn, and there's so many jokes
to make right there.
It is upsetting that soldiers who have fought are being discarded in this way by the government.
It's not surprising, but it's upsetting.
I have heard that the California National Guard has basically said, too bad, I don't
care about DOD's policy, we're going to let them serve.
There's a lot of things that can go wrong with that.
I applaud the effort, but that may go real bad.
At the end of the day, this has nothing to do with readiness.
It has to do with bigotry, but it's just a thought.
Y'all, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}