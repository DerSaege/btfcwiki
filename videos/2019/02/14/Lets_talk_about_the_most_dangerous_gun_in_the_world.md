---
title: Let's talk about the most dangerous gun in the world....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=9c8Zpm4HXMI) |
| Published | 2019/02/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau answers a question about getting a safe rifle for his son, sharing a personal story about firearm safety.
- A friend accidentally shoots a metal plate hanging on the wall with a .22 pistol, illustrating the danger of firearms.
- Beau stresses that all firearms are designed to kill and that safety must be a top priority.
- He mentions starting with an air rifle before introducing a .22 due to its misconception of being safe.
- Despite being tiny, a .22 bullet is dangerous, moving at high speeds and causing significant harm.
- Even experienced individuals can make mistakes with firearms, as seen in the story Beau shares.
- Beau talks about accidental discharges and how they can happen to anyone who handles guns.
- Emphasizes the importance of gun safety measures like keeping the gun pointed in a safe direction and finger off the trigger.
- Beau shares a humorous incident where a mistake with a gun led to beer being thrown but thankfully no serious harm.
- Concludes by stating that there are no truly safe firearms, only varying levels of danger.

### Quotes

- "Safe and firearm do not go together."
- "All firearms will kill."
- "There is no safe firearm, but there are a lot of dangerous ones."

### Oneliner

Beau stresses the inherent danger of firearms and the critical importance of gun safety measures to prevent accidents.

### Audience

Parents, gun owners

### On-the-ground actions from transcript

- Teach firearm safety to children and beginners, starting with air rifles (implied)
- Emphasize the importance of gun safety measures like keeping the gun pointed in a safe direction and finger off the trigger (implied)

### Whats missing in summary

The full transcript provides a detailed insight into firearm safety and the potential risks associated with owning and handling guns.


## Transcript
Well, howdy there internet people it's Bo again, so I got a question and I'm gonna answer it
Bo I want to get my son a rifle I
Want to get him something safe. So if there's an accident, it won't do much damage
My friends have told me to get him a 22. Is this good advice?
twice. I've kept this plate for more than a decade. There's a hole in it, right here.
Metal plate. The plate used to hang on my dining room wall. One of my friends came over and
there's a bunch of us sitting at the dining room table. And these are men that are experienced
with firearms. We're sitting there talking and he's got it out showing everybody this
new pistol. Bam! Didn't hit anybody. Hit that plate on the wall. Bullet hit the plate, went
through the plate, went through the drywall, through the insulation, clipped the stud,
threw the siding on the house lodged in a privacy fence about 30 yards away.
It was 22.
Safe and firearm do not go together.
All firearms will kill.
It wouldn't be much good if it didn't.
That's what it's designed to do.
So that sentence gave me pause.
If you're concerned about that, there's a whole lot of steps you need to take.
If he has not had an air rifle yet, start there, for the love of God.
The.22, it's a common misconception because it's tiny, it is a tiny bullet,.22 of an
inch wide, little, little baby round.
is it's moving at 1200 to 1700 feet per second. Creates a wound channel. A lot like getting
stabbed with an ice pick. Not safe. Definitely not safe. And anybody can have an accident.
The guy that blew a hole in my wall fired tens of thousands of rounds.
Very well trained.
He made a mistake.
It happens.
When you're talking about shooting, there are two types of people.
Those that have had accidental discharges and those that will.
On a long enough timeline, it's going to happen.
You can't count on the gun being safe.
The gun's tool, beyond just the weapon, has to be kept pointed in a safe direction at
all times.
Finger off the trigger.
And if you double up on all these, you keep the safety on, you make sure it's clear, you
keep it pointed in a safe direction at all times, you keep your finger off the trigger.
If you mess up on one, like he did, it's still pointed in a safe direction.
not pointed at anybody. Scares everybody in the room and did get beer thrown at him.
But nobody got hurt besides his pride. There is no safe firearm, but there are a lot of
dangerous ones. The most dangerous firearm in the world is the one that you're pretty
sure is unloaded.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}