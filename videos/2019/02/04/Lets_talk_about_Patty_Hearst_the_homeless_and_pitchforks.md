---
title: Let's talk about Patty Hearst, the homeless, and pitchforks....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=mk7T5yS7_jI) |
| Published | 2019/02/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Patty Hearst was kidnapped 45 years ago today, and her family gave up $2 million in negotiations with the SLA.
- The $2 million given up by the Hearst family was to be turned into food and distributed to the needy, not handed over to the kidnappers.
- Patty Hearst's grandfather, adjusted for inflation, was worth about $30 billion when he died.
- Beau questions the morality of extreme wealth in society, using Jeff Bezos as an example.
- Bezos earned $78.5 billion last year, but most of it is not liquid cash.
- Beau imagines what Bezos could do if he decided to end homelessness in the U.S., estimating it to cost $5.7 billion.
- Beau suggests the idea of taxing the ultra-wealthy at a higher rate or holding them socially responsible.
- He points out the extreme income inequality and questions when it becomes a moral imperative to act against it.
- Beau criticizes the entitlement and lack of empathy shown by the wealthy in the United States.
- He warns that if things don't change, people may resort to drastic actions to address inequality.

### Quotes

- "One guy can end homelessness and give them $200 a week. It's crazy. It's insane."
- "When does it become moral to act? The system as it is, is turning the working class into the working poor and millionaires into billionaires."
- "The entitled nature of the wealthy in the United States now."
- "We still know where the pitchforks are. And if things don't change, those pitchforks are coming."
- "Y'all have a good night."

### Oneliner

45 years after Patty Hearst's kidnapping, Beau questions the morality of extreme wealth through a Jeff Bezos example, proposing an end to homelessness in the US for a fraction of his wealth.

### Audience

Social justice advocates

### On-the-ground actions from transcript

- Advocate for policies that address income inequality and homelessness (implied)
- Support initiatives that tax the ultra-wealthy at a higher rate (implied)
- Raise awareness about the moral implications of extreme wealth accumulation (implied)

### Whats missing in summary

The full transcript provides a detailed examination of income inequality and societal morality through the lens of extreme wealth, urging action against growing disparities.

### Tags

#IncomeInequality #WealthDistribution #SocialJustice #EndHomelessness #TaxTheRich


## Transcript
Well, howdy there, internet people, it's Bo again.
So 45 years ago today, Patty Hearst was kidnapped.
A lot of people know that story, but they mainly know the part about her then
robbing a bank with the people who kidnapped her.
But there's a lot to that story.
In order to open negotiations with the SLA, the Hearst family,
had to give up $2 million, and that's normally reported in the story.
They had to give up $2 million.
What it isn't normally reported is that that 2 million did not go to the kidnappers.
It was to be turned into food and distributed to the needy.
When her grandfather died, he was worth, adjusted for inflation, about $30 billion.
In today's money, that 2 million is worth almost 11 million, 10.8 million.
So why am I bringing all this up?
other than her kidnapping happened today 45 years ago, because the conversation is still
happening. It's been 45 years and nothing's changed. The question is still arising, is
it moral to be a billionaire in this society? Now when we talk about this, it gets confusing
because the numbers are so big we can't wrap our heads around it. I mean it's just
It's insane when you're talking about billions upon billions of dollars that is in the hands
of this ultra-wealthy 1%.
So we're going to do it a different way.
We're going to take one dude, and we're going to use Bezos, a guy from Amazon, because he's
at the top.
He's at the top of the list this year.
According to Business Insider, he earned $78.5 billion this year, or last year.
That's a lot of money, so much we don't know what it means.
The other thing we do need to point out when we're talking about this is that he doesn't
actually have $78.5 billion, like chilling in some Scrooge McDuckie in vault somewhere.
A lot of that's on paper, it's the value of his company and so on and so forth.
But we're not talking about all of the ultra-wealthy either, we're talking about one guy.
So let's say that one guy decided to do something moral, decided to do something good, and this
is the guy from Amazon, he's not going to settle for anything small, he's going to
do something amazing.
So he's going to talk to his ultra wealthy buddies, and they're going to end homelessness
across the United States.
No more homeless people.
And to top it off, they're going to give each one of the 550,000 homeless people $200 a
week to cover incidentals.
So how much money is he going to have to borrow to get that?
None.
He's not going to have to call anybody.
The cost to end homelessness in the US is estimated to be a little bit more than $20
billion to give $200 a week to that 550,000 homeless people.
It's going to be $5.7 billion.
So he could end homelessness and give every homeless person in the country $200 a week
for a year and it would take him from $78 billion down to about $50 billion.
Crazy.
One guy.
So then the question becomes, is it moral to tax them at a higher rate?
that's up to you to decide really. Now I personally like the idea of just destroying
the company if somebody, if it's not socially responsible, just stop shopping with them.
But that's not always possible in a lot of these situations. A lot of these companies
don't rely on consumers for money. His does, but most don't.
So one guy can end homelessness and give them $200 a week.
It's crazy.
It's insane.
When you actually put it into numbers, that is an insane amount of inequality.
And the question is, as that income inequality grows, when does it become self-defense?
When does it become moral to act?
The system as it is, is turning the working class into the working poor and millionaires
into billionaires.
They're paying the lowest wages they can get away with and reaping the profits.
There was nothing more telling than that.
There was nothing like a let them eat cake moment in modern history.
More so than the Commerce Secretary saying that he doesn't understand why furloughed
federal employees were going to food banks because while their jobs are federally protected,
they should just borrow the money.
They should charge it and pay 18% interest because the federal government can't get
their act together.
That is the entitled nature of the wealthy in the United States now.
So the thing is, the 70% tax that's being proposed, stuff like that, that's nothing.
That's nothing.
Because we're reaching that critical point where it's not going to be solved with taxes.
When people get hungry and people get desperate, they act.
or not they act. The ultra wealthy probably need to remember that we still know where
the pitchforks are. And if things don't change, those pitchforks are coming. Anyway, it's
It's just a fault.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}