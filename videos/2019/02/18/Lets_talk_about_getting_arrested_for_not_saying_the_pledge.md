---
title: Let's talk about getting arrested for not saying the pledge....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=4-Y0vE97JMc) |
| Published | 2019/02/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A sixth-grade student in Polk County, Florida was arrested for refusing to stand for the Pledge of Allegiance at Lawton Childs Middle Academy.
- The student was charged with disrupting a school function and resisting arrest without violence.
- The substitute teacher at the school questioned the student's refusal to stand, leading to a confrontation about the perceived racism of the flag and national anthem.
- Despite settled law that students do not lose their constitutional rights at school, the student faced charges for disrupting the school function.
- Beau argues that the disruption was caused by the teacher's attempt to coerce the student into pledging allegiance, not by the student's peaceful protest.
- The student was defending his constitutionally protected rights and should not have been arrested for doing so.
- Beau criticizes the teacher's actions as coercive and questions why the student faced consequences while the teacher was let go by the school board.
- Beau calls for the immediate dropping of charges against the student and stresses the importance of defending his rights against government overreach.

### Quotes

- "Indoctrinating kids creates nationalists, not patriots."
- "If a contract is coerced or forced, it's meaningless."
- "His rights were violated by a government employee."

### Oneliner

A student in Florida arrested for refusing to stand for the Pledge of Allegiance sparks debate on constitutional rights and coercive behavior in schools.

### Audience

Students, Teachers, Activists

### On-the-ground actions from transcript

- Defend the student's rights by advocating for the immediate dropping of charges (suggested).
- Support initiatives that protect students' freedom of expression in educational settings (implied).

### Whats missing in summary

Full understanding of the legal implications and consequences of coercing students to pledge allegiance.

### Tags

#StudentRights #ConstitutionalRights #Coercion #Florida #Education


## Transcript
Well, howdy there Internet people, it's Bo again.
So a sixth grade student in Polk County, Florida going to the Lawton Childs Middle Academy
was arrested after refusing to stand for the Pledge of Allegiance.
Now I should say before we get into this, all of this is preliminary.
or initial reports. Normally I have my own little sneaky methods of getting to
the root of the issue. In this case, the victim is a minor and for once everybody's
doing what they're supposed to. Nobody's talking. So some of this may change as
more information comes out about what happened, but at the end of the day it
appears he was charged with disrupting a school function and resisting arrest
without violence okay so by public accounts of what we know there's a
substitute teacher at the school he didn't stand she asked him why he told
her that he felt the flag was racist and her flag was flagging national anthem
racist. Something along those lines. From there, she asks him why he came there. We
can only infer from the quotes that he is not, was not born in the U.S. and he
says well they brought me here and she tells him he could always go back to
wherever he came from. Something along those lines. Wow. So she eventually calls
the school office. The school administrator shows up and asks him to
leave 20 times according to reports. Then the cops show and arrest him. Okay so
this is settled law. Students do not shed their constitutional rights at the
schoolhouse gate Tinker versus Des Moines. This is already settled. He had
had a right to not stand for the Pledge. It's protected. He doesn't have to. Now Tinker
Des Moines, what it says is basically as long as the protest is not causing a disruption,
it is protected. Fair enough. But he was charged with disrupting the school function. The problem
is it wasn't the protest that disrupted the function. Him sitting there did nothing.
That doesn't stop the educational process at all. The teacher engaging in this conversation
and then proceeding to attempt to coerce a pledge out of him.
That's what disrupted the educational process. So anything beyond that point
He's defending his rights, he's defending his constitutionally protected rights.
Maybe he did get loud, maybe he didn't comply, he didn't have to, he didn't have to.
You know, you want this kid to say a pledge to the Republic.
Why?
The Republic is showing no allegiance to him.
There's nothing worse than this.
You know, in simple contract law, and that's what a pledge is, if a contract is coerced
or forced, it's meaningless.
Indoctrinating kids creates nationalists, not patriots.
And that's what we've run into here today because when the administrator found out what
had happened in that classroom and when a teacher tells a student basically he can always
go back, who really should have been asked to leave that classroom?
The kid or the teacher?
It's funny because the school board has now said that, well, she won't be working with
them anymore.
won't be teaching in the district. They know she was wrong. They know it. This kid is still
facing misdemeanor charges. And I know, it's a misdemeanor, big deal, he's a juvenile.
Doesn't matter. Doesn't matter. His rights were violated by a government employee. That
teacher worked for the government. Was trying to coerce a pledge out of him. To what it
it appears. Otherwise, I mean, what's the appropriate response? You're not going to
stand up? Oh, okay. Doesn't seem like what happened. The police department that
arrested him was Lakeland PD. Now, again, the charges disrupting a school
function, defending his constitutionally protected rights, resisting arrest without violence,
defending his constitutionally protected rights.
These charges need to be dropped immediately.
There's no sense in this.
None.
If there is not an outcry to defend this kid, what's their
left to pledge allegiance to. Really.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}