---
title: Let's talk about vaccines, jaywalking, and abortion
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Wu4s1GkWIgk) |
| Published | 2019/02/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Urges viewers to watch the whole video even if they disagree with the content.
- Traces back the history of vaccines to 1902 when smallpox vaccination was made mandatory in France.
- Describes how the first vaccine came about through Jenner's experimentation with cowpox and smallpox.
- Acknowledges different perspectives on vaccine history, including ethical concerns and conspiracy theories.
- Mentions the practice of inoculation in Africa, India, and China before Jenner's discovery.
- Emphasizes the concept of risk versus reward in vaccination.
- Clarifies that adverse reactions to vaccines don't always mean death.
- Encourages anti-vaxxers to research and make informed decisions.
- Notes that parents not following post-vaccination instructions can complicate adverse reactions.
- Explains how vaccines work, addressing concerns about unvaccinated children posing a risk.
- Compares principles of bodily autonomy in vaccination and abortion.
- Asserts the importance of individual freedom in making healthcare choices.
- Raises ethical concerns about mandatory vaccines despite advocating for vaccination.
- Draws parallels between government control in vaccine mandates and abortion regulations.

### Quotes

- "Don't. Watch the whole thing."
- "Risk versus reward, that's what it's all about."
- "Every law is backed up by penalty of death."
- "Either we have bodily autonomy or we do not."
- "Either we're that servant's eight-year-old boy, or we do maintain control over our own destiny."

### Oneliner

Beau challenges viewers to watch his controversial take on vaccines, exploring their history, risks, rewards, and the ethical debate around bodily autonomy in healthcare decisions.

### Audience

Health-conscious individuals

### On-the-ground actions from transcript

- Research vaccines thoroughly before making decisions (suggested)
- Follow post-vaccination instructions from healthcare providers (exemplified)

### Whats missing in summary

In watching the full video, viewers can gain a deeper understanding of the historical context, ethical considerations, and individual freedoms surrounding vaccination debates.

### Tags

#Vaccines #History #Ethics #BodilyAutonomy #Healthcare


## Transcript
Well, howdy there, internet people, it's Beau again.
Normally, if you watch the first 30 seconds of one of my
videos, you watch the whole thing.
That's what the analytics tell me.
I have a feeling this is going to be a little bit different,
because this is going to challenge everybody.
You're probably going to find something in this that you're
not going to agree with, maybe so much so that you want to
turn it off.
Don't.
That's my challenge to you.
Don't.
Watch the whole thing.
Everybody wants me to talk about vaccines, so we're going to.
Today's a good day to do it.
Today in 1902 is when France made smallpox vaccination mandatory.
So the first vaccine was smallpox.
And how it came about was there was a guy named Jenner, and well, he had a thing for
milkmaids.
When he was younger, he heard a milkmaid say that she'd never have an ugly face because
she got cowpox and therefore wouldn't get smallpox.
He did this idea and intrigued him.
Later on in life, he took his servant's eight-year-old boy and infected him with cowpox.
On the ninth day, the kid almost died.
But then he got better.
He recovered.
And once he recovered, he tried to infect him with smallpox and it didn't work.
The first vaccine was born.
That's how it came about.
You can look at that story in a lot of ways.
You can look at that in the sense of the wealthy
experimenting, and it can be used to reinforce that idea
that big pharma's out to get you.
Maybe they are.
Or you can look at it in the sense of somebody
doing something that's pretty unethical in the hopes
of a small risk creating a great reward.
Those are the two ways to look at it.
Now, I want to touch on the fact that that's
White History Month, all right?
For hundreds of years before this in Africa, India,
and China, there were various kinds of inoculations
for smallpox.
What they would do, this is gross by the way,
they would take the scab from a lesion,
they would powder it and blow it up each other's noses.
You had about a 2% chance of dying from this.
Of course, you have a 35% chance
of dying from smallpox.
So one out of three, one out of 50.
Risk versus reward, that's what it's all about.
It's math, it's numbers.
People talk about adverse reactions.
And the first thing I want to talk about before we even
get into this is that adverse reaction does not mean death.
Not in all cases.
It does not mean death.
An adverse reaction can be swelling.
It can be a lot of things.
It doesn't always mean death.
and that's what a lot of the memes
in the anti-vaxx community infer.
That's not accurate.
It's risk versus reward.
I went through and looked at all of the vaccines
and looked at them through that lens, risk versus reward.
I found one that I believe the reward
does not warrant the risk.
I'm not gonna tell you which one
because if you are an anti-vaxxer,
I want you to go out and research it
in that way for yourself.
I will tell you it's one that's not extremely common.
Now a lot of the adverse reactions are complicated by parents, not following what the doctor
says to do.
You know, when you get your kids vaccinated, and spoiler alert, yeah, my kids have been
vaccinated, the doc tells you, you know, don't leave for 10 or 15 minutes or whatever after
getting the vaccination.
I can sit in the waiting room as I'm sitting there waiting
and watch parents walk right out the door.
They're monitoring for a certain type of shock.
I have a feeling that if parents aren't willing to follow that simple
instruction, they're probably not going to be monitoring for fever
or any of the other things the doc tells them to do.
That complicates those adverse reactions a lot.
The other statement you hear is that all of these things
can be cured with modern medicine.
you know, you're not going to die from it anymore.
Modern medicine and hygiene took care of that.
Cool, let me stab you in the gut.
I mean, with modern medicine,
it's not typically going to kill you.
That's not an argument.
That doesn't even make sense.
And then there's always the question of,
well, why is my unvaccinated child
a danger to the vaccinated if vaccines work?
Well, Jenny, that's not how vaccines work.
A vaccine targets a specific disease or specific strain.
It's a virus.
Viruses, when they enter a body, when they infect somebody,
they kind of attach to the DNA in it,
and there's a chance of mutation.
There's a chance of mutation.
If it mutates far enough, everybody else's vaccines
don't work.
That's one reason.
then there's also the problem of the immunosuppressed.
There's also the problem of the infants that are too young to be vaccinated.
These are all things that come into play.
And it's funny to me because of where the anti-vax community tends to lie,
not always, but tends to lie on the political spectrum.
They're the same people that will rail against socialism.
And their main argument is, well, if socialism is in effect,
then people don't have any incentive to work because they can rely on
other people to take care of them. That's what you're doing with the vaccines. Now
when you combine all of this, modern medicine takes care of it, then you can
just basically isolate yourself. If you agree to spend your entire life in your
hometown, never go anywhere, never go out and see the world and always stay by
modern medical infrastructure. Yeah, that's probably true, but in order to believe that,
you also have to believe that there's never going to be a situation in which modern medical
infrastructure is unavailable to you. And that's funny because a lot of the anti-vax community
carries the belief that society is going to collapse soon, or that there's going to be a
civil war. I got some bad news for you about disease during wartime. It doesn't even have
to be anything that extreme, though. You know, we just went through Hurricane Michael. That
was months ago. Hospitals still aren't up and running right. For the longest time, all
we had was an ER. Had we had an outbreak during that time, God help us.
Okay, so now that I've said all of this, all very pro-vax stuff, I'm not in favor of mandatory
vaccines. Everything I just talked about is logical and moral. Those are the
arguments. Ethics is how you run society. I cannot lend my support to put a gun to
somebody's head and say this is what you're gonna do with your body. I can't
do that. I don't believe that's right and people it's not that severe. It is that
severe. Every law is backed up by penalty of death. Every single one. If you're
You're jaywalking.
You don't pay the ticket you get.
The cops issue a warrant for you if you refuse to be thrown in a cage because you crossed
the street in the wrong spot and you resist, they will kill you.
Every law is backed up by penalty of death.
So I believe in people having that freedom to choose.
I believe in bodily autonomy.
However, there's also the fact that freedom has consequences if you don't vaccinate your
kid and the school board won't let them go to school, that's a consequence.
one that kind of makes sense, maybe open a school for unvaxxed kids, homeschool, whatever.
If you do open a school for unvaxxed kids, God help you if there's an outbreak.
So there's where we're at, and then this is where it gets confusing for a lot of people.
These same principles, bodily autonomy, they apply to abortion too.
And I know somebody's going to say, no, no, no, no, no, no, no, that's somebody else's
body.
If I'm holding a fertilized egg or a fetus a week into development off the side of a
cliff and I got a toddler named Billy in this hand and you get to pick which one I drop,
I'm willing to bet that Billy is always going to be safe.
It's not the same.
Now once the fetus gets to the point where it can sustain life on its own, you probably
have a moral argument there, but you still don't have an ethical one. You still can't
point a gun at somebody and say, this is what you're going to do with your body. You can't
do it. Not ethically, anyway. So, at the end of the day, the options are pretty simple.
Either we have bodily autonomy or we do not. Either the government is in control of us
to that degree, and we are slaves, or we're free, we're modern citizens. Either we're
that servant's eight-year-old boy, or we do maintain control over our own destiny.
That's how it works out. Now, normally I'm pretty active in the comments section. That's
Probably not going to happen today.
It's not a debate.
It's just a thought.
Anyway, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}