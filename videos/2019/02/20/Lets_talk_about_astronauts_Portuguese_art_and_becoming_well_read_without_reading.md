---
title: Let's talk about astronauts, Portuguese art, and becoming well read without reading....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=qGwlNQ-EViw) |
| Published | 2019/02/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau starts by sharing a story about a guy who meets an astronaut on a plane and tries to strike up a conversation with him.
- The astronaut's interest in Portuguese art despite being an astronaut is explained by his desire to learn and expand his knowledge.
- Beau addresses a common question he receives about how to become well-read without having the time to read.
- He recommends an app called LibriVox, which offers thousands of audio books for free, allowing people to listen while cooking or commuting.
- Beau underscores the importance of reading for education, understanding, and gaining insights from different perspectives.
- He mentions that banned books are often the best books and recommends exploring them for a richer reading experience.
- Beau clarifies that his endorsement of LibriVox is not sponsored and encourages people to find ways to achieve their goals.

### Quotes

- "Just because I'm sure somebody will ask, no, this isn't a commercial for LibriVox, they did not pay me, they don't even know I'm talking about it right now."
- "There's always a way to accomplish your goal."
- "Banned books are the best books."

### Oneliner

Beau shares a story about meeting an astronaut and advocates for using LibriVox to become well-read despite a busy schedule, stressing the importance of reading banned books.

### Audience

Book lovers, busy individuals

### On-the-ground actions from transcript

- Download the LibriVox app and listen to audiobooks during daily activities (suggested)
- Make time for reading by incorporating it into cooking or commuting routines (implied)

### Whats missing in summary

Beau's engaging storytelling and passion for reading are best experienced in the full transcript.

### Tags

#Reading #Books #Education #LibriVox #Learning


## Transcript
Well howdy there internet people, it's Bo again.
So tonight I'm going to start off with a story.
Now if you tuned in early on in this series of videos on Facebook, you've probably already
heard it.
But it bears repeating.
I heard this story when I was a kid and it stuck with me.
It was really something.
So this guy, he's just enamored by space.
about it. And he loves astronauts. Well one day he gets on a plane and the stewardess
walks this guy back and sits him down next to him. And it's an astronaut. One of the
famous ones too. We're talking Buzz Aldrin or somebody like that. Neil Armstrong maybe.
I can't remember exactly who. So this guy, he's just overcome. He's ecstatic. His hero
was sitting next to him. So of course he keeps trying to start a conversation with him and
of course he's nervous so he keeps making a fool of himself. But at one point he looks
out the window and he comments on how small the houses are. And the astronaut looks out
the window and he tells them roughly how high up they are. He tells them their altitude
based on the fact that he said it because the structures on the earth are still visible.
And the guy just loved that.
He's like, I'm calling it the ground and this dude's calling it the earth.
That's why he's an astronaut.
And he keeps trying to spark up a conversation with him, but it never works, never goes anywhere.
Eventually, the astronaut pulls out a book on Portuguese art, that's what it was on.
The guy looks at him and he's like, you're an astronaut, why are you reading a book about
Portuguese art?
Because I don't know anything about it.
That's why he's an astronaut.
One of the most common questions I get in my inbox is, what do you read?
I guess because of the variety of topics that I talk about, people assume I'm well read,
and that's true.
I read all the time, all the time, and there were periods in my life when I had a whole
lot of time to read.
I'm lucky though, I'm very lucky in the sense that my job and my life gives me that time
to read, requires it in a way.
The average person in the U.S. doesn't have that kind of time, they don't have leisure
time to sit down and read.
We live busy lives.
So, the question I got was, how do you become well read if you don't have time to read?
There's an app for that.
There is an app for that.
If you're on Droid, it's free.
It's called LibriVox.
It's a non-profit.
I don't know if it's on iPhone or if it costs on iPhone or not, but thousands, thousands
of audio books, everything, modern stuff, the stuff that's out of print, everything
from Moby Dick to Sherlock Holmes to the Art of War, it's all on there.
So you can put that app on your phone and you can listen to it while you're cooking
dinner or on your commute to work. And if you did it just those two times, knock out
thirty or forty books a year. And it's important because it's beyond just the education and
understanding and being able to discuss the books. In all of those books there's something
you can take away from it. In every book. Just like that story. It's just a guy talking
to another guy on an airplane, but there was something profound in it.
Almost every book is like that, and every book on LibriVox, there's certainly something
you can take away from it.
Learning about different periods and times and different places and seeing the eyes or
seeing the world through the eyes of the author, of all of these different authors, it helps
you understand what's going on today in a lot of ways.
It gives you a better understanding of the world.
It helps with education versus credentialing.
So it's definitely something that I would recommend.
Just remember when you're doing it, banned books are the best books.
The National Library Association, they put out a list every year of the most challenged
books, you know, the ones that people tried to remove from libraries and stuff like that.
Those are always the best.
And just because I'm sure somebody will ask, no, this isn't a commercial for LibriVox,
they did not pay me, they don't even know I'm talking about it right now.
If you want to become well-read, you probably don't have the time.
But this is a way to get that.
There's always a way.
There's always a way to accomplish your goal.
Here's a cheat and it works.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}