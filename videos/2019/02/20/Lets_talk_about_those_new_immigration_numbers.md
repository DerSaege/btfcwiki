---
title: Let's talk about those new immigration numbers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=eUrR0eFsWvI) |
| Published | 2019/02/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Distinguishes between the Center for Migration Studies and the Center for Immigration Studies, cautioning against using the latter as a source.
- Presents a new study from the Center for Migration Studies with seven years of hard data on undocumented workers entering the United States.
- Notes that most undocumented workers enter the U.S. via visa overstays rather than border crossings.
- Breaks down the annual numbers: $320,000 from visa overstays and $190,000 from border crossings, questioning President Trump's claims of an invasion based on secret intelligence.
- Debunks the crisis at the border narrative, pointing out that the undocumented population in the U.S. fell by 1.1 million from 2010 to 2017, with more leaving than arriving.
- Provides insights into the lengthy wait times for legal immigration, using the example of a Mexican individual with ties to the U.S. needing to apply for a visa back in 1997.
- Suggests that addressing wait times for legal immigration could be a practical starting point if there are concerns about illegal immigration.
- Encourages focusing on areas within our control, like wait times, if there is a perceived crisis at the border.

### Quotes

- "There is no crisis at the border. It doesn't exist."
- "That's why people don't just get in line and come here legally."
- "If we're going to pretend that there is some crisis, maybe we should start with the stuff we have control over, which is the wait times."

### Oneliner

Beau debunks the border crisis narrative, reveals data on undocumented population decline, and underscores the lengthy wait times for legal immigration as real issues to address.

### Audience

Citizens concerned about immigration policies

### On-the-ground actions from transcript

- Advocate for reducing wait times for legal immigration processes (suggested)
- Share information on visa application processing times to raise awareness (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of immigration data and challenges the narrative of a crisis at the border, advocating for a focus on addressing wait times for legal immigration processes.

### Tags

#Immigration #BorderCrisis #UndocumentedWorkers #VisaOverstays #LegalImmigration


## Transcript
Well, howdy there internet people, it's Beau again and I'm excited.
We've got us a new study to talk about.
From the Center for Migration Studies of New York and that's not to be confused with the
Center for Immigration Studies, which is not a source I would use.
Dig into their background and you'll find out why.
Center for Migration Studies released a new study and it has seven years of hard data.
As always, visa overstays, not border crossings, are how most undocumented workers are entering
the United States. People that would be unaffected by a border wall, $320,000 per year.
a year via visa overstays $190,000 per year from border crossings and I know, $190,000.
That sounds like a lot.
And we'll even give President Trump the benefit of the doubt.
He says that he has secret intelligence or whatever that shows that it's on the rise
and shows that we're being invaded even though he won't show anybody the numbers or tell
anybody where he got them or whatever even though they would greatly help his
case and there is absolutely no national security reason to keep them a secret.
We're gonna pretend that he's not lying and we're gonna go ahead and bump his
numbers up to 200,000. We'll say 200,000 a year border crossings. We're
obviously being invaded. We're gonna be overrun at any moment. That amounts to
zero percent of the population. 0.06. Statistically insignificant. Does not
matter. There is no crisis at the border. It doesn't exist. But the best little
tidbit out of this study is the total undocumented population inside the
United States from 2010 to 2017.
Okay.
What's happening?
It fell by 1.1 million as every study, except for the ones that Trump won't
show us, shows there are more undocumented people leaving than there are coming.
So for every 190,000 border crossers you have, you have
347,000 leaving over that seven-year period. There is no crisis at the border.
It doesn't exist. It's manufactured to scare people. It's all it is. Okay, and then
I was able to get something else. I was able to get a visa bulletin. People have
asked before and I've told them it takes a really long time. That's why people don't just get in line
to come here legally. So I now have the hard data and we're going to talk about how long it
actually takes and we're going to not even put you in the craziest category. We're going to say that
you're from Mexico, that you're unmarried, over 21, and your parents are U.S. citizens.
So you're not the worst off by any means. You have ties to the US. You're not married. You're there.
Okay, you're in a good state as far as these things go.
So if you wanted your visa or your application to be processed this month, when would you have had to apply?
August 1st of 1997, more than 20 years, that's why people don't just get in line.
It's not like you can just walk up and say, hey, I want to become a U.S. citizen.
It doesn't work like that, 20 years, and that's if your parents are U.S. citizens.
If you don't have a corporate sponsor or family in the United States, something like
that, I don't even know.
I have no clue how long it would take.
I'm not even sure that it's possible in one lifetime.
That's why.
That's why people don't just get in line and come here legally.
If you really feel, despite all the statistics we just talked about, that illegal immigration
is a huge issue, even though there's more leaving than coming, that would probably be
the best place to start.
That would be the best place to start, fixing the wait times.
Normally, I make people Google or duck, duck, go, go, whatever, research for yourself to
get this information.
It's hard to find, so I'm going to go ahead and I'll put this in the comment sections.
If we're going to pretend that there is some crisis, maybe we should start with the stuff
we have control over, which is the wait times.
Anyways, just a thought.
Y'all, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}