---
title: Let's talk about fake hate crimes....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Bs4GfJYGnBA) |
| Published | 2019/02/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the topic of fake hate crimes, which was repeatedly requested by viewers.
- Initially found the subject uninteresting until looking into it due to right-wing rage pages.
- Questions the intensity of outrage over the fake hate crime incident compared to other similar cases.
- Criticizes the focus on partisan divides rather than individual beliefs.
- Condemns the blinding racism in only caring about this specific incident.
- Expresses disinterest in discussing topics that cannot be used to make a wider point.
- Points out the inconsistency and divisiveness caused by outrage-driven news coverage.
- Suggests that people prioritize ideological consistency over political affiliations.
- Raises concerns about innocent people possibly being arrested due to false complaints.
- Comments on the lack of utility in focusing solely on outrage without broader implications.

### Quotes

- "If you only care about this guy and what he did, this instance it's blindingly racist."
- "One of the biggest problems in this country is that people are more concerned about red versus blue than they are their own beliefs."
- "That's what they do, you know, they tell you why you need to be mad."
- "I think people call the cops too much anyway, and I don't even watch his show."
- "Can't imagine what the difference is. This isn't the 1960s."

### Oneliner

Beau questions the intense outrage over fake hate crimes, criticizing partisan divides and advocating for ideological consistency.

### Audience

Viewers

### On-the-ground actions from transcript

- Challenge partisan divides by prioritizing individual beliefs and ideological consistency (implied).

### Whats missing in summary

Deeper insights into the societal impact of focusing on outrage-driven news coverage and the importance of ideological consistency in addressing divisive issues.

### Tags

#FakeHateCrimes #PartisanDivides #IdeologicalConsistency #Outrage #Racism


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about fake hate crimes.
Not really sure why.
I've had a lot of people ask for this video repeatedly.
Some were nice, some weren't.
I had one guy tell me I wouldn't dare talk about this subject.
So when the story first broke I didn't really follow it.
I thought it was odd that the hate crime just stopped.
Normally hate crimes don't have a happy ending.
But it's not impossible.
Maybe the attackers saw somebody coming and took off running.
Who knows?
The police, will they investigate the complaint earnestly?
They find new evidence, they follow that evidence and follow our leads.
They begin to suspect that he filed a false complaint and they arrest him.
This is completely uninteresting to me because that's how it's supposed to work.
So it doesn't make, I didn't care.
So I didn't understand the repeated request for this video.
So I had to go over to the right-wing rage pages to find out why I was supposed to be
angry.
That's what they do, you know, they tell you why you need to be mad.
And you're right guys, I didn't know that he should be charged with a hate crime and
suffer a lengthy prison sentence because of this, because he put people's lives in jeopardy.
He called the cops and they could have just found two random white guys wearing MAGA hats
and arrested them.
Innocent people could have gone to jail.
They could have gotten beat or hurt.
Man, you're right, I didn't think of that.
I didn't think of that.
That doesn't happen any other time.
That's not a common occurrence.
So let's give him ten years.
Let's give him ten years.
I think people call the cops too much anyway, and I don't even watch his show.
So let's just throw him under the bus.
Ten years.
But I'm assuming that in the cells next to him will be Permit Patty and Barbecue Becky.
Maybe even the president for putting out that ad, full page ad, calling for the execution
of people who later turned out to be innocent.
I mean, if we're going to worry about state violence, let's do it.
But now it's not so appetizing.
it's not so rage-inducing. You don't care about those cases, just the one, right?
Can't imagine what the difference is. This isn't the 1960s. We don't have two
sets of laws for people based on their skin tone. If I don't talk about a
subject, it is not because I fear talking about it, it is because it's
uninteresting to me. I can't use it to make a wider point, and normally the
The outrage of the day isn't useful for making a wider point, it's only useful for dividing
people along partisan lines, turning them into drones, and making them ideologically
inconsistent.
That's what they're good for.
That's what those, the massive coverage of events like this, that's what it's good for.
One of the biggest problems in this country is that people are more concerned about red
versus blue than they are their own beliefs. If people could remain ideologically consistent
it would be a whole lot easier on this. If you only care about this guy and what he did
this instance it's blindingly racist. It really is. Anyway, it's just a thought. Y'all have
Good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}