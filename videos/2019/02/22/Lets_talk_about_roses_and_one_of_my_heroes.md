---
title: Let's talk about roses and one of my heroes....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=iboTt4la7RI) |
| Published | 2019/02/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces a historical figure who had a significant impact during WWII.
- The figure, Sophie Scholl, was part of the White Rose Society, which advocated passive resistance against the Nazi regime.
- Sophie, along with her brother Hans, was part of an anti-Hitler family but took a different path towards resistance.
- The White Rose Society wrote leaflets framed in biblical and philosophical arguments to reach a wider audience.
- Despite not engaging in violent resistance like blowing up bridges or taking out trains, Sophie believed in the power of passive resistance.
- Sophie's execution at 21 years old left a lasting impact, showcasing bravery and standing up for righteous causes.
- Her message emphasized the importance of meeting people where they are in life's journey and the difference between legality and morality.
- Sophie's actions taught that everyone can contribute to resistance efforts in their unique ways.
- She believed in the power of passive resistance, even in small acts like slowing down work to obstruct the Nazi regime.
- Sophie's defense during her trial reflected her belief that someone had to start expressing dissent, inspiring others to do the same.


### Quotes

- "How can we expect righteousness to prevail when there is hardly anybody willing to give himself up individually to a righteous cause?"
- "If through us, thousands of people are awakened and stirred to action."
- "Even in a sea of brown shirts there can be white roses."
- "Don't be as good at your job as you could be. Slow down at work."
- "Somebody after all had to make a start. What we wrote and said is also believed by others. They just don't dare express themselves as we did."

### Oneliner

Beau introduces Sophie Scholl from the White Rose Society, showcasing her impact through passive resistance against the Nazi regime and the importance of individual action for righteous causes.

### Audience

History enthusiasts, activists

### On-the-ground actions from transcript

- Learn more about historical figures like Sophie Scholl and the White Rose Society (suggested)
- Educate others on the difference between legality and morality (implied)
- Engage in passive resistance in everyday actions to stand up against injustices (implied)

### Whats missing in summary

The emotional depth and historical context conveyed in the full transcript

### Tags

#WWII #SophieScholl #WhiteRoseSociety #Resistance #PassiveResistance


## Transcript
Well howdy there internet people, let's bow again.
So a few days ago, somebody mentioned a historical figure in a conversation and I didn't know
how to respond.
I had to leave the conversation.
Timing was very fitting though.
So today we're going to talk about one of my heroes, and even though you may have never
heard of her in a less than roundabout way, she's the reason you're watching this.
Her brother, Hans, well he was a member of Hitler Youth.
She was a member of a similar organization.
Her dad, well he was a critic of Hitler.
She was a little bit of an artist though, so she hung out with musicians, philosophers,
other degenerates of that ilk.
She almost didn't make it through school, became a kindergarten teacher.
Eventually she wound up at the University of Munich.
Sure enough, there's Hans, he's there studying medicine, I think, and they had this circle
of friends and they'd sit around talk about philosophy and music and this
little network well it morphed and it became what was known as the White Rose
Society and they started writing leaflets and encouraging passive
resistance to the Nazi regime.
They framed their arguments in these leaflets, framed them biblically and philosophically,
making sure that they could reach everybody.
After the sixth flier, they got caught.
As they were walking her to the guillotine, her last words were,
How can we expect righteousness to prevail when there is hardly anybody willing to give
himself up individually to a righteous cause?
It's such a fine, sunny day, and I have to go.
What does my death matter?
If through us, thousands of people are awakened and stirred to action.
She was 21 when she was executed.
That execution took place today, February 22nd, 76 years ago.
What did she teach us though?
What can we learn from her life and what she did?
This entire series of videos.
First, that new information, exposure to new ideas, can and should change the way you think.
That there is a difference between legality and morality.
The patriotism and nationalism aren't the same.
Rule 303, if you've got the means, you've got the responsibility.
She left a historical mark that even in a sea of brown shirts there can be white roses.
She taught us the importance of meeting people where they're at in life's journey.
It's hard to quantify what she did militarily for the war effort.
When you think of resistance during World War II, you think of blowing up bridges, taking
out trains, collecting Nazi scalps.
She didn't do any of that, didn't do any of that.
She encouraged passive resistance because she understood that not everybody can pick
up a gun.
Not everybody is going to become a radical or a militant.
But everybody can engage in passive resistance.
What does that mean?
Don't be as good at your job as you could be.
Slow down at work.
You work in a factory, maybe the Nazis don't have as much ammunition.
Maybe that family of Jews gets away.
She taught us a whole lot.
Twenty-one.
When she was executed, even the Nazi guards commented on the bravery she displayed.
During her trial, her entire defense consisted of one statement.
Somebody after all had to make a start.
What we wrote and said is also believed by others.
They just don't dare express themselves as we did.
Sophie had way more than just a thought.
y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}