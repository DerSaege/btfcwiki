---
title: Let's talk about teaching the Bible in school....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=lft9g73Smbw) |
| Published | 2019/02/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Trump wants to put the Bible back in school.
- Christian students at school didn't like the idea much.
- The modern church is indicted for not fostering religious tolerance.
- Beau believes in teaching religious texts in schools if all are taught equally.
- Teaching philosophy is education; enforcing moral code is indoctrination.
- Philosophy, including religious philosophy, helps people think critically.
- Beau points out similarities in teachings across different religions.
- The downfall of Christianity in the US is due to hypocrisy, especially in politics.
- Christianity is on the decline due to blatant hypocrisy.
- Beau doubts the political expedience of disregarding Jesus' teachings.
- Public schools turning into religious institutions is unlikely to happen.
- Beau mentions the Church of Satan demanding their texts to be taught in schools.
- Teaching philosophy, not just religious philosophy, can benefit the American people.
- Education should focus on teaching how to think, not what to think.

### Quotes

- "Teaching philosophy is education; enforcing moral code is indoctrination."
- "The downfall of Christianity in the US is due to blatant hypocrisy."
- "The ultimate irony is that today's Church of Satan embodies most of the ideals of Christianity better than most Christian churches."
- "The goal of education should not be to teach you what to think. It should be to teach you how to think."

### Oneliner

President Trump's proposal to put the Bible back in schools sparks a reflection on religious tolerance, philosophy, and the decline of Christianity due to hypocrisy.

### Audience

Educators, Religious Leaders

### On-the-ground actions from transcript

- Advocate for equal representation of all religious texts in school curriculum (implied)
- Support teaching philosophy, including religious philosophy, in educational institutions (implied)

### Whats missing in summary

The full transcript offers a deeper exploration of the importance of philosophical education and religious tolerance.


## Transcript
Well, howdy there internet people, it's Bo again.
So I guess President Trump wants to put the Bible back in school.
I had a kid ask me about it yesterday.
I asked him what the students at his school thought, because they were talking about it.
He said that the Christian kids didn't like the idea much, and I thought that was funny.
I asked him why.
He said that they feel they get enough of that on Sunday, and that the non-Christian
kids, well, they didn't seem to care.
And if there has ever been an indictment of the modern church, that's it.
I'm all for it, in a way.
I have taken a lot of good from the Bible.
I think most people could.
It's a great text.
It's a great book.
One of my favorite passages, I'm going to paraphrase it a little bit here, says that
We were all given a law and a way of life.
And that if God had wanted, He could have made us all part of one community, one nation.
But He wanted to see who was the most pious among us.
So we're supposed to compete with each other in doing good.
And that when we return to God, we will be informed about all the ways in which we differed.
Definitely speaking about religious tolerance.
Of course, that's not from the Bible.
That's from the Quran.
I've taken a lot of good from that book too.
I've learned a lot from that as well.
So I have no problem with religious texts being taught in school, as long as we're
teaching all of them.
as long as we're focusing on the philosophical aspects of it
and not trying to enforce the moral code within it.
One of those, teaching the philosophy,
well, that's education.
The other, that's indoctrination.
That's wrong.
It is wrong.
Philosophy is something that is sorely lacking in this country.
People don't think.
And philosophy, not just religious philosophy,
but all philosophy, helps you do that, especially studying various types, helps you come up
with your own. So I'm all for it, in a way. But I have to wonder how today's Christian
would feel if it was another religious text that somebody was trying to force into a school.
You know, none of you believes until he wishes for his brother what he wishes for himself.
It's also from Islam.
That which is hateful to you, do not do to your fellow.
That is the whole Torah.
Everything else is just commentary.
Do not hurt others in ways you yourself would find hurtful.
That's Buddhism.
This is the sum of duty.
Do nothings to others that would cause pain if it would cause pain to you.
That's Hinduism.
In Christianity, we call that the golden rule, right?
So I'm all for teaching.
I'm all for teaching it because I think that it might show the similarities more than the
differences.
It might be one less thing for us to be divided over if people actually understood them, but
I have to say the reason for the downfall of Christianity in the United States, oh it's
not because people don't know the Bible, the problem is they know it better than most Christians
and they see the hypocrisy, especially when it comes to politics.
You would not want another religion doing this to you.
So why would you do it to another one?
Why would you do it to other people?
We're just going to forget the teachings of Jesus simply because it's politically expedient?
That is why Christianity is on the decline.
The hypocrisy is so blatant you could choke on it.
If you don't follow your own code of ethics, why on earth would you expect someone else
to?
Now, to set everybody's mind at ease, this isn't going to happen.
It's really not.
This has been rolled on over and over and over again.
It's just not going to occur.
They're not going to turn public schools into religious institutions.
And if some school district does, I assure you the Church of Satan will show up.
And this isn't a joke.
you're not familiar with them, look into them. It's kind of interesting. They'll
show up and they'll demand that their holy texts be taught too. And they'll
take them to court and they'll win because that's how the law works. And they have
done this quite a few times in the past. I'm not gonna speak for
their church so to speak but it's definitely worth taking a look at their
actions and the ultimate irony is that today's Church of Satan embodies most
of the ideals of Christianity better than most Christian churches. Anyway
Philosophy is definitely something that the American people can benefit from.
It is definitely something that should be taught, not just religious philosophy, but all philosophy.
Because the goal of education should not be to teach you what to think.
It should be to teach you how to think.
And philosophy is critical for that.
Anyway, it's just a thought y'all y'all have a good day

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}