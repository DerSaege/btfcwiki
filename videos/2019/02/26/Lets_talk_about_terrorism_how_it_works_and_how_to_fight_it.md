---
title: Let's talk about terrorism, how it works, and how to fight it....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=L_EQcqzJvz4) |
| Published | 2019/02/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Defines terrorism as the unlawful use of violence or the threat of violence to influence people beyond the immediate area of attack to achieve a political, religious, ideological, or monetary goal.
- Notes that terrorism is carried out by non-state actors, distinguishing it from acts committed by governments.
- Mentions state-directed terrorism, where weak states use terrorist groups to achieve their objectives.
- Provides a real-life playground analogy to illustrate the dynamics of terrorism, insurgency, and regime change.
- Suggests that in-person assassinations of competent terrorist leadership are the most effective military strategy.
- Criticizes ineffective methods like drone strikes and invasions, which often lead to more militants and insurgency.
- Recommends addressing terrorists' grievances to prevent attacks and foster resolution before resorting to violence.
- Emphasizes the importance of understanding and engaging with terrorists to prevent future attacks.

### Quotes

- "Terrorism is not a slur. It's a strategy."
- "That's how terrorism works, start to finish, and it is so predictable it occurs on playgrounds."
- "None of the ways we tried after 9-11."
- "It's going to happen. So we can address it now or address it later. It's that simple."
- "Violence isn't widespread yet. It's isolated within a subsection of this group."

### Oneliner

Beau explains terrorism as the unlawful use of violence to achieve goals through influence beyond the immediate area, suggesting engaging with terrorists to prevent attacks and addressing grievances.

### Audience

Community members, policymakers, activists

### On-the-ground actions from transcript

- Talk about grievances with potential terrorists (suggested)
- Engage with individuals prone to radicalization (suggested)
- Address potential terrorists before resorting to violence (exemplified)

### Whats missing in summary

The full transcript provides a detailed analysis of terrorism, suggesting unconventional methods to combat it and prevent future attacks through understanding and engagement.

### Tags

#Terrorism #Prevention #Engagement #Community #NonStateActors #Grievances


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we're gonna talk about terrorism.
What is it?
Seriously, right now, write your definition
in the comments section.
You probably don't know we've been at war
with terror for a decade and a half, don't know.
It's been too propagandized, too emotionalized.
People don't know what it is anymore.
You can go with the old Webster's dictionary definition.
definition, rule by terror, rule by violence. It's not modern, doesn't make
sense. Attacks on in a sense, well that's just war, it happens. Terrorism is the
unlawful use of violence or the threat of violence to influence those people
beyond the immediate area of attack to achieve a political, religious,
ideological, or monetary goal. That's what terrorism is, that's the best
definition, and it's a mouthful, but each clause is important, unlawful.
Terrorism is the purview of the non-state actor.
Governments don't commit acts of terrorism.
War crimes, genocide, ethnic cleansing, oppression, tyranny, yeah, all of those things they commit,
but it's not terrorism.
It's not terrorism.
Terrorism is the purview of the non-state actor.
If you really start to research into this, you will find something called state-directed
terrorism that exists. That is where a militarily weak state uses a terrorist
group to achieve its goals. Okay, so to violence or the threat of violence, you
don't actually have to commit a crime or commit an act of violence to employ
terrorism. Melling white powder to somebody, melling flour to somebody can
can be just as devastating because of the belief of what it is, it doesn't have to
be actual violence.
Then to influence those people beyond the immediate area of attack, that's the part
that most people have a problem understanding.
For Americans, it's easy.
What was the difference between Pearl Harbor and the attack on the Pentagon on 9-11?
Pentagon was terrorism when it wasn't, why?
Pearl Harbor was a state actor, it was Japan.
Pentagon wasn't.
Pearl Harbor was militarily effective.
The goal was to take out the Pacific fleet.
The goal of the Pentagon attack was to send a message.
Nobody thought the attack on the Pentagon was going to seriously degrade the US war
fighting effort, was to send a message.
And that's what it is.
That's the critical part of terrorism.
That's what differentiates it from most other things.
To achieve a political, religious, ideological, or monetary goal, that's pretty much anything.
Odds are one of your heroes was a terrorist.
If you look up to the founding fathers, I've got some bad news for you about the stuff
they did before Lexington.
Terrorism is not a slur.
It's a strategy.
Why do people use it?
What does it look like?
use it because it works. It's worked for thousands of years. It's very predictable.
What does it look like? When I was a kid on a playground in the 80s, I was too old to
actually play with the swings and slide and stuff like that, but too young to be going
anywhere else. So me and my buddies, we hung out at this picnic table. True to form in
the 80s, you know, there was no anti-bullying campaigns and parents weren't around. There
There was a bully on the playground, two cronies, that's what the 80s was like, it was just
part of going to the playground was the bully.
So there's the establishment, the bully, the two cronies, that's the homeland.
Then you got the empire, which is the playground, and then you got the person they're targeting.
And normally they set the example with a minority, doesn't necessarily have to be a racial minority,
just somebody that's a little bit different. In this case, the kid wore glasses. That was it.
And he suffered the brunt of their bullying. Well, one day he just had enough.
It wasn't that he had been particularly picked on hard that day. He had just had enough.
He picks up this rock and smashes the lead bully in the face,
busts his nose, blacks his eye, all nine yards.
And like the true insurgent terrorist that he was,
This kid took off and disappeared, ran, didn't see him again for days.
He's at home playing Atari, might as well be hiding in a cave in Afghanistan.
What happens next?
Because up until this point, everybody understands terrorism.
The next phases of this are very predictable, though.
The establishment, well, now they're worried.
They don't want anybody else to get that idea, so
they pick on everybody a little bit harder.
There's a clamp down, there's a security clamp down.
The lead bully, he's worried about being usurped from within, so he's got to worry
about the homeland, so he makes sure to kind of pick on them a little bit too, keep them
in their place a little bit harder than he used to.
What's the next phase?
Insurgency.
Those kids on the playground, and they did, one day they all have enough.
They gang up and they beat him up, throw him out of the empire.
Empire collapses.
The establishment retreats to the homeland.
He hangs out at his house with his two buddies.
That security clampdown, though, that's still in effect.
That is still in effect.
And eventually they get tired of him curtailing them, curtailing their freedoms, picking on
them.
And they beat him up, revolution complete.
Those kids, the two cronies, they go back to the playground, regime change.
And for a while everything's fine and they're friendly.
And then eventually they start picking on them again, because that is the cycle.
That's how terrorism works, start to finish, and it is so predictable it occurs on playgrounds.
So then the obvious question is, well, how do you fight terrorism?
None of the ways we tried after 9-11.
The most effective way militarily to do it is the use of in-person assassinations of
competent leadership of terrorist groups.
You leave the incompetent guys, you leave them alone, because then the organization
still exists, you don't get splinter groups, acts as a lightning rod, attracts all of the
militants, but because they have poor leadership they become combat ineffective, they can't
anything. There's a whole bunch of groups that have been neutralized in this
fashion. That's what works. Drone strikes against the leadership that caused 90%
collateral damage. That just creates more terrorists. That just makes people pick
up arms. Invasion, stuff like this, it just fosters the insurgency. And
eventually what happens is the insurgency either wins or its grievances
get addressed by the new government and we end up sitting down and talking to them anyway.
So non-militarily, what can you do?
All of this stuff only works if the audience, those beyond the immediate area of attack,
can identify in some way with the terrorist.
In that case, and that's what's going to have to happen anyway after the insurgency,
why not just do that ahead of time?
Why not start off identifying, trying to talk about their grievances?
Because what you may find out is that it's very easy to resolve.
Or what you may find out is that they don't really have a firm grasp on it.
Maybe this is something that is beyond the control of those people that are targeting.
Maybe it's just the way life is.
Or maybe it's an unseen enemy, somebody the terrorist isn't aware of, a force that the
terrorist doesn't know exists.
And in some cases, the establishment and the terrorists may have the same opposition, that
same unseen enemy.
So reaching out before the attacks seemed preferable.
Got a lot of feedback over that in cell video.
Go back and watch it again.
Yeah we can continue to leave these guys in isolation and leave them in an echo chamber
that is full of hatred and violence and suicide.
We can do that.
But then we don't get to be surprised when there's another attack.
it's going to happen. So we can address it now or address it later. It's that simple.
Now with the insults, violence is not actually ingrained in the ideology yet. It's not like
Nazism. Violence is part of that. There's no way to talk to them early and get them
to stop. With this movement, violence isn't widespread yet. It's isolated within a subsection
of this group. So what can we do? We just write the rest of them off and let them be
influenced by this subsection? Or do we bring them into the narrative, bring them to the
table and actually start trying to talk to him and address it anyway it's just
Just a thought.
y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}