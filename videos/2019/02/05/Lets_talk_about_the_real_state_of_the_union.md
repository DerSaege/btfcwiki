---
title: Let's talk about the real state of the union....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=eegrx5LlUBo) |
| Published | 2019/02/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The United States is as politically divided today as during desegregation.
- Washington, D.C. is not in a good state.
- Trump's wall dominates headlines despite not being a top priority for most Americans.
- Families are still being torn apart for crossing a border.
- Government and large corporations preach conservation while harming the environment.
- $3.4 billion is spent on lobbying and corruption.
- News focuses on manipulating emotions rather than informing.
- The drug war continues, imprisoning innocent individuals.
- Fragile masculinity is evident in men's reactions to a razor ad.
- Teachers are forced to prepare for combat.
- Systemic racism persists in society.
- Youth are mocked and discredited.
- Hate crimes are increasing, and Nazis are present in the streets.
- Americans are fighting for safety and freedom for others, even at personal risk.
- Eight companies produce more pollution than the entire U.S. population.
- Communities are growing stronger, decreasing the power of DC politicians.
- Independent journalists strive to separate fact from fiction.
- Masculine men openly criticize toxic masculinity.
- Teachers are implementing diverse methods to combat violence in schools.
- People of all races are uniting against systemic racism.
- Youth are at the forefront of societal battles.

### Quotes

- "We don't defeat tyranny and oppression by complying with it while we fight it."
- "We defeat it by ignoring it while we quietly create our own systems to replace it."
- "The state of those people who care? Well, they're in a state of defiance."

### Oneliner

The United States faces political division, fragile masculinity, systemic racism, and rising hate crimes while individuals quietly rebel and build new systems for change.

### Audience

Americans

### On-the-ground actions from transcript

- Joining together with community members to combat hate crimes and systemic racism (exemplified)
- Supporting independent journalists striving for truth (exemplified)
- Implementing diverse methods to combat violence in schools (exemplified)

### Whats missing in summary

The full transcript provides a detailed insight into the current state of the United States, urging individuals to take action against oppression and division.

### Tags

#PoliticalDivision #SystemicRacism #CommunityAction #Defiance #Change


## Transcript
Howdy there, Internet people.
It's Bo again.
I said today we're going to talk about the state of the union.
There is no unity.
None.
The United States today is as politically divided as it was during desegregation.
Washington, D.C. isn't the best.
And Brightus is the best of the worst.
Trump's wall dominates the headlines, even though 69% of Americans feel it isn't a priority.
Families are still getting ripped apart for walking across a line in the sand.
The government and large corporations telling you to conserve while constantly attacking
and destroying the environment.
There's $3.4 billion spent on lobbying and corruption in this country.
The news, well, it tells you how to feel rather than informing you.
The drug war, it's still raging.
Innocent people who harm nobody are getting locked in cages.
Masculinity has become so fragile that men are posting photos of themselves holding guns
on Facebook over a razor ad, teachers, well, they've got to be prepared for combat.
Systemic racism?
Yeah, that's still a thing.
There's a concerted effort to mock the youth of this country, discredit them.
Hate crimes are on the rise, and there are literal Nazis in the street.
the State of the Union today. But what about the State of the Union tomorrow?
Americans are sharing memes on Facebook talking about what sugar does to wet
concrete and they're buying land along the border to tie up that wall in court.
Americans are still fighting for others ability to seek safety and freedom here
even if it means they end up in cuffs. We've researched for ourselves and
and realized that eight companies
produce more pollution than the entire citizenry of the United States.
Those DC politicians had become less and less powerful as communities
strengthened themselves from within.
And even though many were purged on social media,
independent journalists the world over are still fighting,
trying to separate fact from fiction. Men who are actually masculine have found it
within themselves to openly mock those who believe masculinity is the
suppression of others. Teachers, you know, they've started a whole bunch new
diverse ways to combat violence in schools. People of all races are starting
to join together and speak out against that systemic racism. Today's youth, yeah,
they're on the front line of most of these battles. As, you know, the older
generations portray him as weak, while they wait for Trump to build a wall for
them to hide behind. In the Pacific Northwest and the Southeastern United
States, right-wing three-percenters and the John Brown Gun Club, as left-wing as
they come, are joining together to keep Nazis off the street, even if it means
with guns.
We don't defeat tyranny and oppression by complying with it while we fight it.
We defeat it by ignoring it while we quietly create our own systems to replace it.
The state of the Union?
I don't have a clue.
The state of those people who care?
Well, they're in a state of defiance.
anyway it's just a thought now it's gonna get a little different here you
know during Trump's State of the Union he offered to sell the ability to have
your name scrolled he gave money to his campaign you would have your name on his
live stream I made a joke said if you donated three dollars and fifty cents to
a domestic violence shelter that I support I would say your name during this
video was a joke I made on my personal profile. It was a joke until it wasn't
and people started sending money. So, Greed Media, Aaron Raymond, James Woods,
I'm paying you to never say my name ever, Kelly Bauer in the Center for Animal
Health and Welfare, Jason Arnold, Billy Rod, Peter Kropotkin, Gary Ferner, Jay
Martin, Justin Swanstrom, Chadwick Wingett, Laura Jenkins, Laurel I. Kautz, Peggy Martinez,
Priscilla Hernandez, Pamela Lockwood, and one cheeky fellow who calls himself Scooter
McGruder. Thank you guys, and do not think I didn't notice that y'all sent a lot more
$3.50 each. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}