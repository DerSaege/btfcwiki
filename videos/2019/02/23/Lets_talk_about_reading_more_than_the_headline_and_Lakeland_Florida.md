---
title: Let's talk about reading more than the headline and Lakeland, Florida....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=CjNgYXH2UoA) |
| Published | 2019/02/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses the situation in Lakeland, Florida where a child who did not stand for the pledge is facing punishment despite headlines stating otherwise.
- The child's actions are protected by the West Virginia State Board of Education versus Barnett Supreme Court case law, allowing students to choose not to stand for the pledge.
- A substitute teacher attempted to shame or coerce the child into standing for the pledge, leading to interference with the educational process.
- The school administrator intervened and told the child to leave the class, which goes against the Tinker versus Des Moines ruling that students cannot be punished for peaceful protests that don't disrupt education.
- Law enforcement got involved, with a police officer backing up the school's decision to remove the child from the class.
- The child was arrested for disrupting the school and resisting arrest without violence, but questions arise about the validity of these charges.
- Beau planned activism involving playing the National Anthem on loop at the school board meeting but was overruled by local activists.
- The current plan of activism involves targeting campaign contributors and businesses associated with government officials for boycotts.
- Despite headlines suggesting the child may not be prosecuted, the situation is far from resolved, and punishing the child for exercising his rights undermines the value of the flag.
- Beau concludes by noting the importance of upholding constitutionally protected rights, regardless of how the child is punished.

### Quotes

- "You're punishing him for exercising his constitutionally protected rights."
- "If you do that, that flag is worthless. It means nothing."
- "You are making that flag worthless."
- "It's not worth a pledge."
- "It isn't."

### Oneliner

Beau dissects the situation in Lakeland, Florida where a child faces punishment for not standing during the pledge, exposing the attempt to spin headlines and uphold constitutional rights.

### Audience

Activists, Advocates, Parents

### On-the-ground actions from transcript

- Contact local activists to support their efforts in addressing the situation in Lakeland, Florida (implied).
- Participate in boycotts of businesses associated with government officials who are involved in the case (implied).
- Stay informed about legal developments in the case and support the child's legal counsel (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the events in Lakeland, Florida, shedding light on attempts to manipulate narratives and the importance of protecting constitutional rights in educational settings.

### Tags

#Lakeland #Florida #ConstitutionalRights #Activism #Punishment #Schools


## Transcript
Well, howdy there, internet people, it's Bo again.
So I guess we have to talk about Lakeland, Florida again and why it's important to read
more than just the headline.
So the headline comes out, the child who did not stand for the pledge will most likely
not be prosecuted.
Hey, we won.
No, no, that is political spin.
That is an attempt by them to defuse the situation.
The reality is, he's still going to be punished.
It says that most likely he will be sent to a diversion program.
So he's going to be punished for exercising his constitutionally protected rights.
If you're just tuning in, what went down more or less is there's a substitute teacher
and a child in the class does not want to stand for the pledge.
He doesn't have to.
West Virginia State Board of Education versus Barnett, this is established case law from
the Supreme Court.
The decision's been around, I don't know, like 80 years, something ridiculously long.
So she attempts to kind of shame him or coerce him into saying the pledge.
This is when the interference with the educational process seems to have occurred, when she attempted
to violate his rights, that's what it seems like.
After telling him that he can go back to wherever he came from, or something along those lines,
she gets fed up and she calls the school administrator who comes down and tells him to leave the
class, Tinker versus Des Moines.
He cannot be punished for a protest at school as long as it does not interfere with the
educational process. Him remaining seated did not interfere with the
educational process. Okay, now enter law enforcement. The gun of the government.
The cop tells him to leave the class backing up what the school is
trying to do.
Is it lawful for him to do that?
That's going to be the big question when this either goes to trial and then in the lawsuit
afterward.
From where I'm standing, it wasn't, but hey, I'm not a lawyer.
Okay, so he arrests this kid for disrupting the school and resisting arrest without violence.
I'm not sure how either one of those charges are going to stick, especially the disruption
of the school.
His protest didn't by all accounts.
Okay, and then there's the other little matter here.
Any elements of offense in Florida
for resisting arrest without violence,
the officer has to be lawfully detaining him.
Was he?
Okay, so that's what the court case
is gonna look like, basically.
Now, people have asked me to give an update on this before and I said I couldn't because I was probably going to be
involved in some activism.  I'm not going to. The local activists do not want to. They've kind of overruled what I
wanted to do.  So my idea was to go down there with the National Anthem on loop and show the school board what
a disruption is.
But I was overruled.
Okay, so I guess, and they have said I can say this, it looks like the current plan of
activism is to go after the campaign contributors to basically everybody in
the government down there. Any business that contributed to any of them, they're
going to arrange boycotts and stuff like that. That appears to be the activist
side of it. The legal side of it, I do know that the child has counsel, has
retained counsel and that that's where we're at but the key thing we have to
notice here is is that headline most likely not be prosecuted sounds like
they're dropping the charges that's not what was happening and this is what
we've got to be very careful of in today's world in many cases the local
papers are, I don't want to say in bed with, but they have ties to the government officials.
And in a lot of papers, basically they just print whatever the press release is.
That isn't what happened here, but it was still framed in a way, and that headline especially
was framed in a way to seem to attempt to defuse this situation and make it seem as
as though the law enforcement down there has righted their wrong.
It's not what's happened.
It's not what has happened.
It doesn't matter how you punish the child.
The end of the day, you're punishing him for exercising his constitutionally protected
rights.
That's where this chain of events started.
And if you do that, that flag is worthless.
It means nothing.
It's not worth a pledge.
It isn't.
You're confirming everything this child believes.
You are making that flag worthless.
Anyway, it's just a thought.
It's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}