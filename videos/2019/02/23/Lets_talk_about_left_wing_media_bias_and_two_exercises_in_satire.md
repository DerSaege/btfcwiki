---
title: Let's talk about left wing media bias and two exercises in satire....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8V1RnieO7mw) |
| Published | 2019/02/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- AOC Press tweets were perceived as real, causing outrage, but it's actually a parody account.
- The reaction to the tweets reveals how the right-wing uses gullibility for political gain.
- People's susceptibility to confirmation bias fuels polarization.
- AOC receives disproportionate attention compared to other deserving Congress members.
- Beau praises a Congresswoman with an impressive resume who remains largely ignored.
- The media's left-wing bias is evident in its coverage or lack thereof.
- The overlooked Congresswoman has a notable background in science and international relations.
- Despite significant achievements, the media fails to give her the attention she deserves.
- The media neglects stories about her involvement in controversies surrounding Flint and Standing Rock.
- Beau criticizes the media's focus on sensationalism over substantive contributions.

### Quotes

- "The right-wing rage machine in this country knows their audience is dumb."
- "People are susceptible to confirmation bias."
- "The media's left-wing bias is evident."
- "A Congresswoman with an impressive resume remains largely ignored."
- "The media neglects stories about her involvement in controversies surrounding Flint and Standing Rock."

### Oneliner

AOC Press tweets cause outrage, revealing political manipulation, media bias, and the overlooked accomplishments of deserving Congress members.

### Audience

Media Consumers

### On-the-ground actions from transcript

- Support and amplify deserving Congress members who are overlooked in media coverage (suggested).
- Challenge and fact-check sensationalist news to combat misinformation (suggested).

### Whats missing in summary

The full transcript provides a detailed analysis of media bias, political manipulation, and the overlooked achievements of deserving public figures.


## Transcript
Well, howdy there, internet people, it's Beau again.
So, I guess we gotta talk about AOC.
She's lost her mind.
Lost her mind.
I don't know if you've seen those tweets.
Coming from AOC Press, insane stuff.
Saying that returning ISIS brides are more important
than our own veterans.
Stuff like that.
I mean, shocking stuff.
stuff that should just be unbelievable. Completely unbelievable. So unbelievable
that you would have to doubt that it was real. And you should doubt that it's real
because it's not. AOC Press is a parody account. It even says that it's a parody
account. But that hasn't stopped hundreds of people from replying to these tweets
calling her an idiot and that's I mean that's funny you're too dense to realize
you're witnessing satire and man anyway and it is funny but it belies a couple
of huge issues a couple of huge issues the it shows very clearly that the
right-wing rage machine in this country, well, they know their audience is dumb.
They know their audience is stupid.
They're playing on it.
They're counting on the gullibility of these people.
They're using low-information voters to swing elections.
The person that can't tell that these things are false, their vote counts just as much
yours. It's terrifying. Terrifying. The other thing that it shows is the polarization in
this country and how susceptible people are to confirmation bias. If you don't like somebody
and hear something bad about them, well, you're going to believe it, even if it's ridiculous
on its face. But the other thing it shows is the left-wing bias in this country and
the media especially because you know everybody's talking about AOC, some stupid
waitress let's be honest. Meanwhile there's that other girl and I can't
remember her name, but she's up there on Capitol Hill. She just got elected too.
And she's got this really impressive resume, but nobody's talking about her.
You know, every year there's a competition, science and engineering, in D.C., 1,500
people. She placed second for something in microbiology. She's got a graduated
And top of her class, I want to say cum laude, with a degree in international relations and
economics, nobody's talking about her, I guess that's not noteworthy.
She has an asteroid name after her, I mean that's cool.
And when she was 19, because she's bilingual, when she was interning in another congressman's
office, she had to deal with immigration stuff.
At 19 years old, she's talking to people whose families have been kidnapped by ICE, trying
to calm them down.
She's not worth talking about though, it's crazy, it's just insane.
And it shows the left-wing bias in this country, anyway.
You know what, that same girl, come to think of it, as a private citizen, she even dipped
her toes into the controversies surrounding Flint and Standing Rock.
Haven't seen any coverage about that, though, it's crazy.
And it's all because it's left-wing bias in the media, it's that simple.
I mean, you don't even see that on Fox News, they're not even talking about her, it's
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}