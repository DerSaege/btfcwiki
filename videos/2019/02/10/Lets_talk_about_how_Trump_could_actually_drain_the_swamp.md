---
title: Let's talk about how Trump could actually drain the swamp....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=7a-f3kptCQ4) |
| Published | 2019/02/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau questions President Trump's campaign promise to "drain the swamp of campaign finances" and mentions reaching out to him.
- He refers to AOC taking Congress to task over campaign finance issues and suggests that the President could work across the aisle for real reform.
- Beau criticizes Trump's true intentions, pointing out that as a businessman, he may not actually care about draining the swamp but was cutting out the middleman.
- He contrasts AOC's efforts in addressing campaign finance with Trump's lack of action and engagement.
- Beau signs off with a casual goodbye, wishing the viewers a good night.

### Quotes

- "He doesn't care about draining the swamp. He was just cutting out the middleman."
- "But you know she's down there actually bringing it up and talking about it and instead of reaching out to her he's up there swimming with alligators."

### Oneliner

Beau questions Trump's commitment to draining the swamp, contrasting AOC's actions with his inaction and lack of engagement.

### Audience

Political observers, reform advocates

### On-the-ground actions from transcript

- Reach out to elected officials for campaign finance reform (implied)
- Stay informed and engaged in political issues (implied)

### Whats missing in summary

The full transcript provides additional context on President Trump's campaign promises and AOC's efforts in Congress.


## Transcript
Well, howdy there, internet people.
It's Bo again.
I just tweeted to the president.
I hadn't heard back yet.
I'm sure it's just a matter of time, though.
I had to ask.
I mean, he's art of the deal.
He's for D-Chess.
He's the guy at making deals and seeing things
from all angles, so I had to ask.
One of his chief campaign promises,
I don't know if y'all remember,
He said that he was going to drain the swamp of campaign finances.
Now you've got this new Congress person coming in, AOC, taking Congress to task over it,
and she's a darling of the Democratic Party, darling of the left.
It would seem like somebody that was good at making deals would understand that all
he has to do is reach across the aisle and say, hey, if you draft real campaign finance
reform legislation, I'll support it. That bill would be unstoppable if he
actually wanted to drain the swamp. But see people didn't really listen to what
he was saying. He said that businessmen, they pretty much owned politicians. He's
a businessman. He doesn't care about draining the swamp. He was just cutting
out the middleman. But you know she's down there actually bringing it up and
talking about it and instead of reaching out to her he's up there swimming with
alligators. Anyways, just a follow up. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}