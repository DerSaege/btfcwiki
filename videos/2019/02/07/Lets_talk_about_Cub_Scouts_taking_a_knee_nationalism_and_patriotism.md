---
title: Let's talk about Cub Scouts taking a knee, nationalism, and patriotism....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ixedV66SfMY) |
| Published | 2019/02/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A ten-year-old Cub Scout took a knee during the Pledge of Allegiance at a city council meeting, sparking varied reactions.
- Some people struggle to fit their standard responses to this unique situation.
- The attempt to blend racism and nationalism has led to young people associating the American flag with racism.
- The rejection of racism by the youth will result in them rejecting the flag as well.
- The young Scout's actions are rooted in independence, self-reliance, and free thought taught by scouting.
- Patriotism involves correcting your country when it's wrong, not blind obedience.
- Beau expresses gratitude towards the young Scout for embodying true patriotism at a young age and standing against racism.
- Beau shares his Scout background and praises the Scout for his independent and thoughtful act.
- The Scout's action symbolizes independent thought, self-reliance, and preparedness for future decisions.
- Beau sees hope in the younger generation dismantling racist symbols and standing up for what's right.

### Quotes

- "Patriotism is correcting your country when it's wrong."
- "Scouts are doing just fine."
- "This country can build that monument. And these kids are gonna tear it down."

### Oneliner

A ten-year-old Cub Scout's act challenges racism, embodying true patriotism and independent thought, symbolizing hope for dismantling divisive symbols.

### Audience

Youth advocates

### On-the-ground actions from transcript

- Support youth empowerment in community groups (implied)
- Advocate for inclusive and anti-racist education in scouting organizations (implied)
- Encourage independent thought and standing up against discrimination in youth (implied)

### Whats missing in summary

The full transcript captures the power of youth in challenging racism and nationalism, offering hope for a more inclusive future.

### Tags

#YouthEmpowerment #AntiRacism #Patriotism #CommunityAction #Scouting


## Transcript
Well, howdy there, internet people, it's Bo again.
So somebody took a knee during a city council meeting
during the Pledge of Allegiance.
And it's funny, because nobody knows how to react.
You know, normally, we'd hear the standard lines.
Oh, he's just some NFL baby crying,
just some black thug that doesn't like the cops.
Yeah, it's hard to say that about a white,
ten-year-old Cub Scout.
I'm loving it. I'm loving watching people try to
try to fit their normal responses into this.
I'll tell you some of the things I've already seen.
The first one is that there's a better time for him to exercise his
First Amendment rights, and they're real polite about it.
Can't imagine what the difference is.
There's not really a better time, to be honest.
You know, you guys have done everything you can.
There's a segment of this country that's done everything it can
to blend
racism and nationalism.
To put those two things together. To hide your racism in the American flag.
When you do that, you can't be surprised
when young people start to associate
the American flag with racism.
It's what you want it.
The problem is that they reject the racism, which means they're going to reject that flag along with it.
They're going to interrupt your nationalist ceremonies to prove their point, and it is the perfect time to do it.
The other thing I've already seen is, well, he's only ten. He doesn't know any better.
an interview and he said that he he didn't like discrimination which he
defined as being mean to a person of another color. It is not the perfect
intersectional definition of discrimination that I'm sure we would
all like but at 10 years old oh I'll take it and yeah now he understands he
understands completely his moral compass is intact it may not be knowing right
and wrong, that's not something that has to come with age. Not caring about right
and wrong is something that comes with age. No, his age is pretty much irrelevant
to this. This is only happening because the Cub Scouts have gone soft. Cub Scouts
are there, the scouting in general, is there to teach independence, self-reliance,
Self-reliance, free thought, independent thought, being able to do things on your own, being
prepared for the difficult decisions you're going to have to make as you get older.
That's what it is.
One kid in that pack, one kid took a knee.
Oh no, the scouts are functioning just fine because that is independent thought.
That is self-reliance and that is being prepared for the decisions he's going to have to make.
Scouts are doing just fine.
Well, the scouting motto says God and country.
It does, it most certainly does.
It says God and country, not God and nationalism.
Patriotism is correcting your country when it's wrong.
It's not blind obedience, that's nationalism.
That kid has it down, he's got it.
He's embodying the Scouts and he's embodying patriotism at 10.
And then I've already seen people say that if you're not a Scout, you don't have any
business talking about this.
Well, guess what?
I was a Scout.
My eldest was a Scout.
I'm sure my youngest boys will be Scouts.
Liam, son, you did your good deed for the week, dude.
It was fantastic.
I personally want to thank you, because you reassured me of something that I believed.
There are people in this country that want to hang on to this.
They want to build this racist monument down on the border.
They can.
And they can milk it for all it's worth for about the next eight years, and then y'all
are going to be in.
Yeah, this country can build that monument.
And these kids are gonna tear it down.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}