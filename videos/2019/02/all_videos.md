# All videos from February, 2019
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2019-02-27: Let's talk about fake hate crimes.... (<a href="https://youtube.com/watch?v=Bs4GfJYGnBA">watch</a> || <a href="/videos/2019/02/27/Lets_talk_about_fake_hate_crimes">transcript &amp; editable summary</a>)

Beau questions the intense outrage over fake hate crimes, criticizing partisan divides and advocating for ideological consistency.

</summary>

"If you only care about this guy and what he did, this instance it's blindingly racist."
"One of the biggest problems in this country is that people are more concerned about red versus blue than they are their own beliefs."
"That's what they do, you know, they tell you why you need to be mad."
"I think people call the cops too much anyway, and I don't even watch his show."
"Can't imagine what the difference is. This isn't the 1960s."

### AI summary (High error rate! Edit errors on video page)

Addresses the topic of fake hate crimes, which was repeatedly requested by viewers.
Initially found the subject uninteresting until looking into it due to right-wing rage pages.
Questions the intensity of outrage over the fake hate crime incident compared to other similar cases.
Criticizes the focus on partisan divides rather than individual beliefs.
Condemns the blinding racism in only caring about this specific incident.
Expresses disinterest in discussing topics that cannot be used to make a wider point.
Points out the inconsistency and divisiveness caused by outrage-driven news coverage.
Suggests that people prioritize ideological consistency over political affiliations.
Raises concerns about innocent people possibly being arrested due to false complaints.
Comments on the lack of utility in focusing solely on outrage without broader implications.

Actions:

for viewers,
Challenge partisan divides by prioritizing individual beliefs and ideological consistency (implied).
</details>
<details>
<summary>
2019-02-26: Let's talk about terrorism, how it works, and how to fight it.... (<a href="https://youtube.com/watch?v=L_EQcqzJvz4">watch</a> || <a href="/videos/2019/02/26/Lets_talk_about_terrorism_how_it_works_and_how_to_fight_it">transcript &amp; editable summary</a>)

Beau explains terrorism as the unlawful use of violence to achieve goals through influence beyond the immediate area, suggesting engaging with terrorists to prevent attacks and addressing grievances.

</summary>

"Terrorism is not a slur. It's a strategy."
"That's how terrorism works, start to finish, and it is so predictable it occurs on playgrounds."
"None of the ways we tried after 9-11."
"It's going to happen. So we can address it now or address it later. It's that simple."
"Violence isn't widespread yet. It's isolated within a subsection of this group."

### AI summary (High error rate! Edit errors on video page)

Defines terrorism as the unlawful use of violence or the threat of violence to influence people beyond the immediate area of attack to achieve a political, religious, ideological, or monetary goal.
Notes that terrorism is carried out by non-state actors, distinguishing it from acts committed by governments.
Mentions state-directed terrorism, where weak states use terrorist groups to achieve their objectives.
Provides a real-life playground analogy to illustrate the dynamics of terrorism, insurgency, and regime change.
Suggests that in-person assassinations of competent terrorist leadership are the most effective military strategy.
Criticizes ineffective methods like drone strikes and invasions, which often lead to more militants and insurgency.
Recommends addressing terrorists' grievances to prevent attacks and foster resolution before resorting to violence.
Emphasizes the importance of understanding and engaging with terrorists to prevent future attacks.

Actions:

for community members, policymakers, activists,
Talk about grievances with potential terrorists (suggested)
Engage with individuals prone to radicalization (suggested)
Address potential terrorists before resorting to violence (exemplified)
</details>
<details>
<summary>
2019-02-25: Let's talk about SEALs, INCELs, and how they have something in common.... (<a href="https://youtube.com/watch?v=lLZfKs5N8jQ">watch</a> || <a href="/videos/2019/02/25/Lets_talk_about_SEALs_INCELs_and_how_they_have_something_in_common">transcript &amp; editable summary</a>)

Beau delves into the controversial figure of Dick Marcinko, incel community insights, and the importance of psychological resilience in facing challenges and improving society.

</summary>

"He looked for people that had suffered, that had that psychological fortitude."
"Killing people, killing yourself, it's not the answer."
"You're not going to make it better for the people after you. You're going to make it worse."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic of Dick Marcinko, the founder of SEAL Team 6, and addresses his controversial reputation.
Questions the conviction of Marcinko, suggesting it may have been a result of personal vendettas.
Comments on Marcinko's arrogance based on personal interactions but acknowledges his military accolades.
Explores Marcinko's unique approach to building SEAL Team 6 by selecting individuals who had to struggle, showcasing psychological fortitude.
Draws parallels between Marcinko's team-building strategy and insights into the incel community.
Observes intelligence, self-awareness, misogyny, and racism within the incel groups.
Identifies three types of incels: physically unattractive, psychological or cognitive issues, and those with negative personality traits.
Notes that violent rhetoric and suicidal talk predominantly stem from the third group of incels.
Encourages incels to potentially use their experiences and psychological resilience for positive purposes.
Urges incels to avoid resorting to violence or isolation, suggesting that improving society benefits not only them but future generations.

Actions:

for incels, community builders,
Reach out to support groups or mental health professionals for assistance (suggested)
Engage in positive community activities to channel experiences and resilience positively (implied)
Advocate for societal change to prevent isolation and violence (implied)
</details>
<details>
<summary>
2019-02-25: Let's talk about Brexit, Ireland, and a hard border.... (<a href="https://youtube.com/watch?v=br9YiOB0Ddo">watch</a> || <a href="/videos/2019/02/25/Lets_talk_about_Brexit_Ireland_and_a_hard_border">transcript &amp; editable summary</a>)

Beau explains the implications of Brexit on the Irish border, warning about potential violence and advising complete non-involvement for safety.

</summary>

"I mean they make Bernie Sanders look like Trump."
"Stay out of whatever area develops as a hotbed, just stay out of it."
"This is just throwing a match on the powder keg."

### AI summary (High error rate! Edit errors on video page)

Explains the division of the island of Ireland between North and South, controlled by the UK and Republic of Ireland respectively.
Describes the significance of Brexit in relation to the open border between Northern Ireland and the Republic of Ireland.
Mentions the US's stance on backing a post-Brexit trade deal that includes an open border on the Irish island.
Raises concerns about potential violence if a hard border is implemented, specifically mentioning Republican violence.
Differentiates Irish Republicans from American Republicans, noting the political ideologies of each.
Advises individuals living near potential conflict areas to stay completely out of any involvement to ensure safety.
Talks about the increased accessibility of explosives due to online materials, making it easier for individuals to manufacture them.
Mentions the historical practice of giving warnings before bomb attacks and the potential for modern technology to aid in issuing alerts.
Warns against getting involved in areas where violence might erupt and advises staying away from such hotbeds.
Comments on the changing dynamic regarding targeting the royal family and British establishment in potential attacks.

Actions:

for irish residents, brexit observers,
Stay completely out of potential conflict areas (implied)
Remove any nationalist symbols from visible locations (implied)
Keep any political affiliations to yourself (implied)
Pay attention to warnings through modern technology (implied)
</details>
<details>
<summary>
2019-02-24: Let's talk about a question from a teacher and the next generation of activists.... (<a href="https://youtube.com/watch?v=4lSzdINAfSU">watch</a> || <a href="/videos/2019/02/24/Lets_talk_about_a_question_from_a_teacher_and_the_next_generation_of_activists">transcript &amp; editable summary</a>)

A teacher seeks guidance on a student's defiance, sparking Beau's insights on diverse self-advocacy tactics and the role of activism in teaching students their rights.

</summary>

"How do we create the next generation of people that will look at an injustice and say, no, not today."
"If you don't assert your rights, you don't have any."
"It wasn't until this message that I realized we don't have to. People like Lakeland PD are going to do it for us."
"There's always going to be somebody that's going to say whatever you did was wrong. The question is, was it effective?"
"Just let them know that they can. Just let them know that it is okay to advocate for yourself, to stand up for your rights."

### AI summary (High error rate! Edit errors on video page)

A teacher asked about a student refusing to leave with the principal, leading to police involvement.
Questions arise about compliance, respect, and self-advocacy for students.
Beau questions the training and support available to the student in advocating for their rights.
Beau believes in diverse tactics for self-advocacy and questions the concept of a "right way."
He expresses concerns about the complex nature of this teachable moment and its potential impact.
Beau underscores the importance of teaching students about their rights and self-advocacy.
He acknowledges the role of the Lakeland PD in sparking defiance and activism in students.
Beau mentions the national attention drawn to the incident in Lakeland, Florida, and its impact on students' awareness of rights.
Respectful activism may still provoke criticism, but effectiveness is key.
Beau encourages individuals to determine their level of involvement in activism and self-advocacy.

Actions:

for teachers, students, activists,
Teach students about their rights and how to self-advocate (exemplified)
Encourage diverse tactics for self-advocacy (exemplified)
Spark awareness and activism among students through real-life examples (exemplified)
</details>
<details>
<summary>
2019-02-23: Let's talk about reading more than the headline and Lakeland, Florida.... (<a href="https://youtube.com/watch?v=CjNgYXH2UoA">watch</a> || <a href="/videos/2019/02/23/Lets_talk_about_reading_more_than_the_headline_and_Lakeland_Florida">transcript &amp; editable summary</a>)

Beau dissects the situation in Lakeland, Florida where a child faces punishment for not standing during the pledge, exposing the attempt to spin headlines and uphold constitutional rights.

</summary>

"You're punishing him for exercising his constitutionally protected rights."
"If you do that, that flag is worthless. It means nothing."
"You are making that flag worthless."
"It's not worth a pledge."
"It isn't."

### AI summary (High error rate! Edit errors on video page)

Beau addresses the situation in Lakeland, Florida where a child who did not stand for the pledge is facing punishment despite headlines stating otherwise.
The child's actions are protected by the West Virginia State Board of Education versus Barnett Supreme Court case law, allowing students to choose not to stand for the pledge.
A substitute teacher attempted to shame or coerce the child into standing for the pledge, leading to interference with the educational process.
The school administrator intervened and told the child to leave the class, which goes against the Tinker versus Des Moines ruling that students cannot be punished for peaceful protests that don't disrupt education.
Law enforcement got involved, with a police officer backing up the school's decision to remove the child from the class.
The child was arrested for disrupting the school and resisting arrest without violence, but questions arise about the validity of these charges.
Beau planned activism involving playing the National Anthem on loop at the school board meeting but was overruled by local activists.
The current plan of activism involves targeting campaign contributors and businesses associated with government officials for boycotts.
Despite headlines suggesting the child may not be prosecuted, the situation is far from resolved, and punishing the child for exercising his rights undermines the value of the flag.
Beau concludes by noting the importance of upholding constitutionally protected rights, regardless of how the child is punished.

Actions:

for activists, advocates, parents,
Contact local activists to support their efforts in addressing the situation in Lakeland, Florida (implied).
Participate in boycotts of businesses associated with government officials who are involved in the case (implied).
Stay informed about legal developments in the case and support the child's legal counsel (implied).
</details>
<details>
<summary>
2019-02-23: Let's talk about left wing media bias and two exercises in satire.... (<a href="https://youtube.com/watch?v=8V1RnieO7mw">watch</a> || <a href="/videos/2019/02/23/Lets_talk_about_left_wing_media_bias_and_two_exercises_in_satire">transcript &amp; editable summary</a>)

AOC Press tweets cause outrage, revealing political manipulation, media bias, and the overlooked accomplishments of deserving Congress members.

</summary>

"The right-wing rage machine in this country knows their audience is dumb."
"People are susceptible to confirmation bias."
"The media's left-wing bias is evident."
"A Congresswoman with an impressive resume remains largely ignored."
"The media neglects stories about her involvement in controversies surrounding Flint and Standing Rock."

### AI summary (High error rate! Edit errors on video page)

AOC Press tweets were perceived as real, causing outrage, but it's actually a parody account.
The reaction to the tweets reveals how the right-wing uses gullibility for political gain.
People's susceptibility to confirmation bias fuels polarization.
AOC receives disproportionate attention compared to other deserving Congress members.
Beau praises a Congresswoman with an impressive resume who remains largely ignored.
The media's left-wing bias is evident in its coverage or lack thereof.
The overlooked Congresswoman has a notable background in science and international relations.
Despite significant achievements, the media fails to give her the attention she deserves.
The media neglects stories about her involvement in controversies surrounding Flint and Standing Rock.
Beau criticizes the media's focus on sensationalism over substantive contributions.

Actions:

for media consumers,
Support and amplify deserving Congress members who are overlooked in media coverage (suggested).
Challenge and fact-check sensationalist news to combat misinformation (suggested).
</details>
<details>
<summary>
2019-02-22: Let's talk about roses and one of my heroes.... (<a href="https://youtube.com/watch?v=iboTt4la7RI">watch</a> || <a href="/videos/2019/02/22/Lets_talk_about_roses_and_one_of_my_heroes">transcript &amp; editable summary</a>)

Beau introduces Sophie Scholl from the White Rose Society, showcasing her impact through passive resistance against the Nazi regime and the importance of individual action for righteous causes.

</summary>

"How can we expect righteousness to prevail when there is hardly anybody willing to give himself up individually to a righteous cause?"
"If through us, thousands of people are awakened and stirred to action."
"Even in a sea of brown shirts there can be white roses."
"Don't be as good at your job as you could be. Slow down at work."
"Somebody after all had to make a start. What we wrote and said is also believed by others. They just don't dare express themselves as we did."

### AI summary (High error rate! Edit errors on video page)

Beau introduces a historical figure who had a significant impact during WWII.
The figure, Sophie Scholl, was part of the White Rose Society, which advocated passive resistance against the Nazi regime.
Sophie, along with her brother Hans, was part of an anti-Hitler family but took a different path towards resistance.
The White Rose Society wrote leaflets framed in biblical and philosophical arguments to reach a wider audience.
Despite not engaging in violent resistance like blowing up bridges or taking out trains, Sophie believed in the power of passive resistance.
Sophie's execution at 21 years old left a lasting impact, showcasing bravery and standing up for righteous causes.
Her message emphasized the importance of meeting people where they are in life's journey and the difference between legality and morality.
Sophie's actions taught that everyone can contribute to resistance efforts in their unique ways.
She believed in the power of passive resistance, even in small acts like slowing down work to obstruct the Nazi regime.
Sophie's defense during her trial reflected her belief that someone had to start expressing dissent, inspiring others to do the same.

Actions:

for history enthusiasts, activists,
Learn more about historical figures like Sophie Scholl and the White Rose Society (suggested)
Educate others on the difference between legality and morality (implied)
Engage in passive resistance in everyday actions to stand up against injustices (implied)
</details>
<details>
<summary>
2019-02-21: Let's talk about transgender soldiers.... (<a href="https://youtube.com/watch?v=99Jw8ci4Uvk">watch</a> || <a href="/videos/2019/02/21/Lets_talk_about_transgender_soldiers">transcript &amp; editable summary</a>)

Beau breaks down the insignificant impact of transgender individuals on military readiness and stresses the importance of their service for societal acceptance and diversity.

</summary>

"It is upsetting that soldiers who have fought are being discarded in this way by the government."
"This small percentage of soldiers has become a political pawn."
"At the end of the day, this ban, it has nothing to do with readiness, nothing."
"The military has always been on the cutting edge of social acceptance."
"I served with a black guy. They're all right."

### AI summary (High error rate! Edit errors on video page)

Responds to a question about transgender people in the military impacting readiness and the importance of their service.
Approximately 1000 to 15,000 transgender individuals serve in the military, a small percentage.
Military readiness is based on worldwide deployability and unit cohesion.
RAND Corporation's study found no breakdown in unit cohesion in countries allowing transgender individuals to serve.
Concerns about worldwide deployability due to hormone treatments requiring refrigeration, similar to insulin dependence.
Loss of time for a transgender person in labor years is minimal, not a significant readiness issue.
Beau compares the minimal impact of transgender individuals on readiness to pregnancy sidelining soldiers for nine months.
Acknowledges a legitimate readiness concern for a small percentage of transgender individuals requiring long-term hormone treatment.
Beau argues that turning away willing recruits impacts readiness more than allowing transgender individuals to serve.
Draws parallels to historical military acceptance movements like desegregation and acceptance of gay individuals.
Points out the military's role in driving social acceptance and the importance of diverse representation in the armed forces.
Condemns the ban on transgender individuals serving in the military as politically motivated, not related to readiness.
Expresses disappointment at the mistreatment and politicization of soldiers who have served.
Mentions the California National Guard's decision to allow transgender individuals to serve despite DOD policy, expressing concerns about potential issues.

Actions:

for advocates for inclusive military policies.,
Support efforts to allow transgender individuals to serve openly in the military (exemplified).
Advocate for policies that prioritize diversity and inclusion in the armed forces (suggested).
</details>
<details>
<summary>
2019-02-21: Let's talk about a hypothetical question, military readiness, and the wall.... (<a href="https://youtube.com/watch?v=h7WkKRmj-1c">watch</a> || <a href="/videos/2019/02/21/Lets_talk_about_a_hypothetical_question_military_readiness_and_the_wall">transcript &amp; editable summary</a>)

Beau poses a hypothetical scenario about targeting critical military facilities, revealing they are actually sacrifices for a partial border wall, jeopardizing military readiness.

</summary>

"They're crippling our air because if these aircraft aren't maintained, and they don't fly. It's that simple."
"What we're doing is we're going to give up our ability to defend against an actual invasion to build part of a wall to stop an invasion he made up."
"Two hundred thirty-four miles is not building the wall. It's not. It's a start."

### AI summary (High error rate! Edit errors on video page)

Poses a hypothetical question about a list of facilities being targeted by a foreign power and their potential goals and strategies.
Mentions critical facilities like maintenance facilities for the F-35, dry dock facilities at Pearl Harbor, and training facilities for special operations.
Suggests that targeting these facilities is an attempt to degrade readiness before a war.
Reveals that the list of facilities in question are actually Department of Defense programs being sacrificed for border wall construction.
Criticizes the idea of sacrificing vital military programs for a partial border wall, which falls short of fulfilling promises to rebuild the military.
Expresses concern about the negative impact on military readiness and logistics due to diverting funding from vital programs.

Actions:

for policy makers, activists,
Contact policymakers to advocate for prioritizing military readiness over border wall construction (suggested).
Join advocacy groups working to protect vital Department of Defense programs (exemplified).
</details>
<details>
<summary>
2019-02-20: Let's talk about those new immigration numbers.... (<a href="https://youtube.com/watch?v=eUrR0eFsWvI">watch</a> || <a href="/videos/2019/02/20/Lets_talk_about_those_new_immigration_numbers">transcript &amp; editable summary</a>)

Beau debunks the border crisis narrative, reveals data on undocumented population decline, and underscores the lengthy wait times for legal immigration as real issues to address.

</summary>

"There is no crisis at the border. It doesn't exist."
"That's why people don't just get in line and come here legally."
"If we're going to pretend that there is some crisis, maybe we should start with the stuff we have control over, which is the wait times."

### AI summary (High error rate! Edit errors on video page)

Distinguishes between the Center for Migration Studies and the Center for Immigration Studies, cautioning against using the latter as a source.
Presents a new study from the Center for Migration Studies with seven years of hard data on undocumented workers entering the United States.
Notes that most undocumented workers enter the U.S. via visa overstays rather than border crossings.
Breaks down the annual numbers: $320,000 from visa overstays and $190,000 from border crossings, questioning President Trump's claims of an invasion based on secret intelligence.
Debunks the crisis at the border narrative, pointing out that the undocumented population in the U.S. fell by 1.1 million from 2010 to 2017, with more leaving than arriving.
Provides insights into the lengthy wait times for legal immigration, using the example of a Mexican individual with ties to the U.S. needing to apply for a visa back in 1997.
Suggests that addressing wait times for legal immigration could be a practical starting point if there are concerns about illegal immigration.
Encourages focusing on areas within our control, like wait times, if there is a perceived crisis at the border.

Actions:

for citizens concerned about immigration policies,
Advocate for reducing wait times for legal immigration processes (suggested)
Share information on visa application processing times to raise awareness (implied)
</details>
<details>
<summary>
2019-02-20: Let's talk about astronauts, Portuguese art, and becoming well read without reading.... (<a href="https://youtube.com/watch?v=qGwlNQ-EViw">watch</a> || <a href="/videos/2019/02/20/Lets_talk_about_astronauts_Portuguese_art_and_becoming_well_read_without_reading">transcript &amp; editable summary</a>)

Beau shares a story about meeting an astronaut and advocates for using LibriVox to become well-read despite a busy schedule, stressing the importance of reading banned books.

</summary>

"Just because I'm sure somebody will ask, no, this isn't a commercial for LibriVox, they did not pay me, they don't even know I'm talking about it right now."
"There's always a way to accomplish your goal."
"Banned books are the best books."

### AI summary (High error rate! Edit errors on video page)

Beau starts by sharing a story about a guy who meets an astronaut on a plane and tries to strike up a conversation with him.
The astronaut's interest in Portuguese art despite being an astronaut is explained by his desire to learn and expand his knowledge.
Beau addresses a common question he receives about how to become well-read without having the time to read.
He recommends an app called LibriVox, which offers thousands of audio books for free, allowing people to listen while cooking or commuting.
Beau underscores the importance of reading for education, understanding, and gaining insights from different perspectives.
He mentions that banned books are often the best books and recommends exploring them for a richer reading experience.
Beau clarifies that his endorsement of LibriVox is not sponsored and encourages people to find ways to achieve their goals.

Actions:

for book lovers, busy individuals,
Download the LibriVox app and listen to audiobooks during daily activities (suggested)
Make time for reading by incorporating it into cooking or commuting routines (implied)
</details>
<details>
<summary>
2019-02-19: Let's talk about vaccines, jaywalking, and abortion (<a href="https://youtube.com/watch?v=Wu4s1GkWIgk">watch</a> || <a href="/videos/2019/02/19/Lets_talk_about_vaccines_jaywalking_and_abortion">transcript &amp; editable summary</a>)

Beau challenges viewers to watch his controversial take on vaccines, exploring their history, risks, rewards, and the ethical debate around bodily autonomy in healthcare decisions.

</summary>

"Don't. Watch the whole thing."
"Risk versus reward, that's what it's all about."
"Every law is backed up by penalty of death."
"Either we have bodily autonomy or we do not."
"Either we're that servant's eight-year-old boy, or we do maintain control over our own destiny."

### AI summary (High error rate! Edit errors on video page)

Urges viewers to watch the whole video even if they disagree with the content.
Traces back the history of vaccines to 1902 when smallpox vaccination was made mandatory in France.
Describes how the first vaccine came about through Jenner's experimentation with cowpox and smallpox.
Acknowledges different perspectives on vaccine history, including ethical concerns and conspiracy theories.
Mentions the practice of inoculation in Africa, India, and China before Jenner's discovery.
Emphasizes the concept of risk versus reward in vaccination.
Clarifies that adverse reactions to vaccines don't always mean death.
Encourages anti-vaxxers to research and make informed decisions.
Notes that parents not following post-vaccination instructions can complicate adverse reactions.
Explains how vaccines work, addressing concerns about unvaccinated children posing a risk.
Compares principles of bodily autonomy in vaccination and abortion.
Asserts the importance of individual freedom in making healthcare choices.
Raises ethical concerns about mandatory vaccines despite advocating for vaccination.
Draws parallels between government control in vaccine mandates and abortion regulations.

Actions:

for health-conscious individuals,
Research vaccines thoroughly before making decisions (suggested)
Follow post-vaccination instructions from healthcare providers (exemplified)
</details>
<details>
<summary>
2019-02-18: Let's talk about getting arrested for not saying the pledge.... (<a href="https://youtube.com/watch?v=4-Y0vE97JMc">watch</a> || <a href="/videos/2019/02/18/Lets_talk_about_getting_arrested_for_not_saying_the_pledge">transcript &amp; editable summary</a>)

A student in Florida arrested for refusing to stand for the Pledge of Allegiance sparks debate on constitutional rights and coercive behavior in schools.

</summary>

"Indoctrinating kids creates nationalists, not patriots."
"If a contract is coerced or forced, it's meaningless."
"His rights were violated by a government employee."

### AI summary (High error rate! Edit errors on video page)

A sixth-grade student in Polk County, Florida was arrested for refusing to stand for the Pledge of Allegiance at Lawton Childs Middle Academy.
The student was charged with disrupting a school function and resisting arrest without violence.
The substitute teacher at the school questioned the student's refusal to stand, leading to a confrontation about the perceived racism of the flag and national anthem.
Despite settled law that students do not lose their constitutional rights at school, the student faced charges for disrupting the school function.
Beau argues that the disruption was caused by the teacher's attempt to coerce the student into pledging allegiance, not by the student's peaceful protest.
The student was defending his constitutionally protected rights and should not have been arrested for doing so.
Beau criticizes the teacher's actions as coercive and questions why the student faced consequences while the teacher was let go by the school board.
Beau calls for the immediate dropping of charges against the student and stresses the importance of defending his rights against government overreach.

Actions:

for students, teachers, activists,
Defend the student's rights by advocating for the immediate dropping of charges (suggested).
Support initiatives that protect students' freedom of expression in educational settings (implied).
</details>
<details>
<summary>
2019-02-17: Reporting from the Mexican border.... (<a href="https://youtube.com/watch?v=oJpmdtW7N0Q">watch</a> || <a href="/videos/2019/02/17/Reporting_from_the_Mexican_border">transcript &amp; editable summary</a>)

Reporting from the Mexican border on Trump's national emergency declaration due to asylum seekers; frustration and disbelief at the situation.

</summary>

"He declared a national emergency over people crossing the border to legally claim asylum."
"I am so done with this network."

### AI summary (High error rate! Edit errors on video page)

Reporting live from the Mexican border for the fifth column.
President Trump declared a national emergency due to an invasion.
No signs of any opposition forces at the border.
Pentagon has not specified which country is invading.
Refugees seen fleeing in terror.
Assumption made that opposition forces must be close by.
New information reveals it was a family of six, not an invasion.
National emergency declared over people crossing the border to claim asylum legally.
Beau questions who sent him there and expresses frustration with the situation.
Beau is done with the network.

Actions:

for media consumers, activists.,
Contact network to express frustration (implied).
</details>
<details>
<summary>
2019-02-17: Let's talk about that shooting in Houston again.... (<a href="https://youtube.com/watch?v=-NJVLls-II4">watch</a> || <a href="/videos/2019/02/17/Lets_talk_about_that_shooting_in_Houston_again">transcript &amp; editable summary</a>)

Police union threats, fabricated warrants, and demonization of victims reveal systemic issues demanding an unbiased investigation.

</summary>

"Don't paint us all with a broad brush, isolated incident, a few bad apples, same song and dance we always hear."
"And please stop demonizing the victim."
"You need to bring somebody in that doesn't have loyalty, that doesn't want to cover for their buddy, that isn't part of that thin blue line."
"In court that [evidence] would be suppressed."
"What makes you think he didn't have pot and coke."

### AI summary (High error rate! Edit errors on video page)

Police union president in Houston made veiled threats after shooting incident.
Warrant used for the raid was entirely fabricated.
Confidential informants who were supposed to buy heroin from the house never did.
One officer had heroin in his car.
Houston PD targeted an independent journalist in 2016, leading to trumped-up charges.
Journalists cultivated sources within the Houston Police Department after the incident.
Chief of police claims isolated incident with a few bad apples, demonizes the victim.
Private face tells officers to tear apart cases, suggesting it's not an isolated incident.
Beau suggests bringing in the FBI for a thorough investigation.
Raises questions about the source of heroin, integrity of officers, and demonization of the victim.
Points out discrepancies in the evidence used for the raid.
Calls for an end to demonizing the victim and a thorough, unbiased investigation.
Questions the credibility of officers involved in the raid.
Criticizes the use of evidence that wouldn't hold up in court to justify the raid.
Raises suspicions about other possible illegal substances in the officer's possession.

Actions:

for journalists, activists, community members,
Contact local journalists or media outlets to raise awareness of the fabricated warrant and biased practices within the Houston Police Department (suggested).
Reach out to community organizations advocating for police accountability and transparency to support unbiased investigations (implied).
</details>
<details>
<summary>
2019-02-16: Let's talk about national emergencies.... (<a href="https://youtube.com/watch?v=Coh3j4EH-DY">watch</a> || <a href="/videos/2019/02/16/Lets_talk_about_national_emergencies">transcript &amp; editable summary</a>)

Beau warns of the dangerous abuse of power and erosion of democracy through the misuse of national emergencies, urging Americans to take action before it's too late.

</summary>

"This is how every democracy, every Republican history committed suicide."
"No one person should have this much power. This is dictatorial rule."
"The average American needs to start leading themselves."
"Pandora's box is being opened and it's extremely dangerous."
"If the Senate and House of Representatives, the Supreme Court, do not put an end to this immediately. That's it. It's over."

### AI summary (High error rate! Edit errors on video page)

Expresses frustration over the abuse of national emergencies for political gain.
Describes attempts to create a video addressing the issue, including initial anger and satire.
Criticizes the current national emergency declaration as dangerous and unnecessary.
Points out that border apprehensions are at a 41-year low, indicating no crisis.
Emphasizes the dangerous precedent set by one person having too much power.
Explains the limitations of a national emergency declaration in creating legislation.
Raises concerns about potential misuse of power, such as seizing firearms or factories.
Warns about the erosion of democracy if checks and balances are not upheld.
Urges Americans to be more vigilant and proactive in protecting their rights.
Calls for action from groups traditionally focused on constitutional rights.

Actions:

for american citizens,
Contact your Senators and Representatives to urge them to uphold checks and balances in government (implied)
Join or support organizations focused on defending constitutional rights and democracy (implied)
Attend marches or protests advocating for governmental accountability and transparency (implied)
</details>
<details>
<summary>
2019-02-14: Let's talk about the problem with sanctuary cities.... (<a href="https://youtube.com/watch?v=I3Nkjkoxock">watch</a> || <a href="/videos/2019/02/14/Lets_talk_about_the_problem_with_sanctuary_cities">transcript &amp; editable summary</a>)

The problem with sanctuary cities is the necessity they exemplify in protecting undocumented individuals from deportation when seeking help after being victims of violence, countering misconceptions and advocating for federal responsibility over immigration enforcement.

</summary>

"Because if the cops, local cops, have to run everybody that they won't run into through ICE's database, I mean let's just set aside the whole fact of local law enforcement walking around like the Gestapo asking for papers."
"They're not real people. They can't go to the cops."
"Statistically proven, illegal immigrants commit less violent crime than native-born."
"No local department should be enforcing laws outside of this jurisdiction."
"There's no reason for your local deputy or your local cop to walk around like the Gestapo."

### AI summary (High error rate! Edit errors on video page)

A man in Florida was lured into a trailer, tied up, beaten, robbed, and threatened by individuals who ultimately let him go after warning him not to go to the police because he was undocumented.
The attackers were white, which may surprise some listeners.
Sanctuary cities exist to protect undocumented individuals from being deported when seeking help from law enforcement after being victims of violent crimes.
Some local police departments resist cooperating with ICE due to concerns about inaccuracies in ICE's database and potential liability issues.
An example is shared of a man wrongfully detained by ICE for three weeks due to a database error.
Even a veteran carrying military ID and a passport was detained by ICE, demonstrating the flaws in the system.
The argument against sanctuary cities fostering non-assimilation is countered by the fact that undocumented workers statistically commit less violent crime than native-born individuals.
Private prisons may be pushing for a focus on immigration enforcement as a new source of profit once marijuana legalization reduces the need for housing non-violent drug offenders.
Beau argues against local law enforcement acting like immigration enforcers, stating that immigration is a federal issue and should remain as such.

Actions:

for advocates for immigrant rights,
Advocate for the protection of undocumented individuals and support sanctuary city policies (implied)
Educate your community on the importance of sanctuary cities in protecting victims of violent crimes regardless of immigration status (implied)
Support initiatives that push for federal responsibility over immigration enforcement (implied)
</details>
<details>
<summary>
2019-02-14: Let's talk about the most dangerous gun in the world.... (<a href="https://youtube.com/watch?v=9c8Zpm4HXMI">watch</a> || <a href="/videos/2019/02/14/Lets_talk_about_the_most_dangerous_gun_in_the_world">transcript &amp; editable summary</a>)

Beau stresses the inherent danger of firearms and the critical importance of gun safety measures to prevent accidents.

</summary>

"Safe and firearm do not go together."
"All firearms will kill."
"There is no safe firearm, but there are a lot of dangerous ones."

### AI summary (High error rate! Edit errors on video page)

Beau answers a question about getting a safe rifle for his son, sharing a personal story about firearm safety.
A friend accidentally shoots a metal plate hanging on the wall with a .22 pistol, illustrating the danger of firearms.
Beau stresses that all firearms are designed to kill and that safety must be a top priority.
He mentions starting with an air rifle before introducing a .22 due to its misconception of being safe.
Despite being tiny, a .22 bullet is dangerous, moving at high speeds and causing significant harm.
Even experienced individuals can make mistakes with firearms, as seen in the story Beau shares.
Beau talks about accidental discharges and how they can happen to anyone who handles guns.
Emphasizes the importance of gun safety measures like keeping the gun pointed in a safe direction and finger off the trigger.
Beau shares a humorous incident where a mistake with a gun led to beer being thrown but thankfully no serious harm.
Concludes by stating that there are no truly safe firearms, only varying levels of danger.

Actions:

for parents, gun owners,
Teach firearm safety to children and beginners, starting with air rifles (implied)
Emphasize the importance of gun safety measures like keeping the gun pointed in a safe direction and finger off the trigger (implied)
</details>
<details>
<summary>
2019-02-13: Let's talk about what a concept of Japanese art can teach us about masculinity.... (<a href="https://youtube.com/watch?v=2Px9F0p2TCE">watch</a> || <a href="/videos/2019/02/13/Lets_talk_about_what_a_concept_of_Japanese_art_can_teach_us_about_masculinity">transcript &amp; editable summary</a>)

Beau shares a Japanese concept, Shibumi, to redefine masculinity as effortless perfection, separate from conforming to charts or molds.

</summary>

"Maybe masculinity can't be quantified the way people try."
"Effortless perfection. Now that is a hard idea to get your head around."
"When you see somebody trying to be masculine, you see somebody trying to be a tough guy, happens you immediately know they're not."

### AI summary (High error rate! Edit errors on video page)

Critiques resources aimed at helping young American males find masculinity on the internet as ridiculous.
Recalls a time working in a call center with an old Japanese man during the overnight shift.
Describes drinking sake with the old Japanese man who talked about a Japanese concept called Shibumi.
Explains Shibumi as subtle, understated, part of nature but separate from it, in control of nature, and representing perfection without effort.
Suggests that masculinity cannot be quantified or charted and should involve maintaining control over natural characteristics effortlessly.
Emphasizes the importance of effortless perfection in masculinity and the need to accept oneself rather than mimicking role models.

Actions:

for young american males,
Embrace your natural characteristics and accept yourself as you are (implied).
Refrain from trying to force yourself to conform to societal molds of masculinity (implied).
</details>
<details>
<summary>
2019-02-12: Let's talk about why I don't like Facebook memes.... (<a href="https://youtube.com/watch?v=GIZ14Szfe10">watch</a> || <a href="/videos/2019/02/12/Lets_talk_about_why_I_don_t_like_Facebook_memes">transcript &amp; editable summary</a>)

Beau conducts a meme demonstration, revealing misattributed quotes and advocating for meaningful discourse, contrasting Trump and AOC's stances on gun control.

</summary>

"This is why I don't like memes."
"Boils things down to talking points, eliminates discussion. It's a slogan."
"It completely destroys any meaningful conversation."
"Donald Trump is not pro-Second Amendment."
"Don't believe everything you read or see on the internet, George Washington."

### AI summary (High error rate! Edit errors on video page)

Expresses dislike for Facebook memes and conducts a demonstration to illustrate his point.
Shares a meme featuring AOC with a controversial quote about guns.
Reads through comments on the shared meme, showcasing derogatory and misinformed remarks.
Clarifies that the quote attributed to AOC actually belongs to Donald Trump.
Criticizes memes for oversimplifying complex issues and hindering meaningful discourse.
Advocates for engaging in thoughtful and informed conversations.
Contrasts Trump and AOC's stances on the Second Amendment and due process.
Points out Trump's actions regarding firearms regulations.
Mentions AOC's position statement on gun violence and the Second Amendment.
Concludes with a quote from George Washington about not believing everything on the internet.

Actions:

for social media users,
Fact-check memes before sharing (suggested)
Engage in informed and meaningful dialogues about political issues (implied)
</details>
<details>
<summary>
2019-02-10: Let's talk about how Trump could actually drain the swamp.... (<a href="https://youtube.com/watch?v=7a-f3kptCQ4">watch</a> || <a href="/videos/2019/02/10/Lets_talk_about_how_Trump_could_actually_drain_the_swamp">transcript &amp; editable summary</a>)

Beau questions Trump's commitment to draining the swamp, contrasting AOC's actions with his inaction and lack of engagement.

</summary>

"He doesn't care about draining the swamp. He was just cutting out the middleman."
"But you know she's down there actually bringing it up and talking about it and instead of reaching out to her he's up there swimming with alligators."

### AI summary (High error rate! Edit errors on video page)

Beau questions President Trump's campaign promise to "drain the swamp of campaign finances" and mentions reaching out to him.
He refers to AOC taking Congress to task over campaign finance issues and suggests that the President could work across the aisle for real reform.
Beau criticizes Trump's true intentions, pointing out that as a businessman, he may not actually care about draining the swamp but was cutting out the middleman.
He contrasts AOC's efforts in addressing campaign finance with Trump's lack of action and engagement.
Beau signs off with a casual goodbye, wishing the viewers a good night.

Actions:

for political observers, reform advocates,
Reach out to elected officials for campaign finance reform (implied)
Stay informed and engaged in political issues (implied)
</details>
<details>
<summary>
2019-02-09: Let's talk about 1916, 1984, thought, and rebellion.... (<a href="https://youtube.com/watch?v=4i9gbymKMjw">watch</a> || <a href="/videos/2019/02/09/Lets_talk_about_1916_1984_thought_and_rebellion">transcript &amp; editable summary</a>)

In 1916, Irish rebellion sparked by seizing Dublin's General Post Office led to a legacy of independent thought, urging individuals to challenge oppressive systems with new ideas.

</summary>

"Real rebellion starts with thought, independent thought, free thought, looking around and analyzing things for yourself."
"For the first time, ideas travel faster than bullets."
"Create real change. Put a new idea in somebody's head."
"That's how you fight a system."
"Anyway, it's just a thought, y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

In 1916, 1200 men seized the General Post Office in Dublin, igniting the Irish Movement for Independence against British rule.
The Irish rebellion, though militarily a failure, sparked a legacy of independent thought and resistance.
The act of rebellion often begins with independent thought and questioning the status quo.
Winston's first act of rebellion was not writing a diary but realizing the falsehood of the system around him.
Rebellion starts with independent thought, analyzing situations, and questioning norms.
Real change in society can be brought about by spreading ideas rather than resorting to violence.
Ideas have the power to create change without the need for force or coercion.
Sparking independent thought and defending ideas can lead to significant transformations in society.
Encourages individuals to think for themselves, spark meaningful dialogues, defend their ideas, and create genuine change.
The key to fighting oppressive systems lies in challenging them through independent thought and spreading new ideas.

Actions:

for activists, free thinkers, change-makers,
Spark independent thought by encouraging others to question norms and think critically (implied).
Defend your ideas in dialogues and spark meaningful exchanges to create real change (implied).
Engage in open, honest dialogues without relying on memes or slogans to foster independent thought (implied).
</details>
<details>
<summary>
2019-02-08: Let's talk about what happens "if you don't get an education".... (<a href="https://youtube.com/watch?v=aCvYfWCcMPc">watch</a> || <a href="/videos/2019/02/08/Lets_talk_about_what_happens_if_you_don_t_get_an_education">transcript &amp; editable summary</a>)

Beau questions the link between education and labor devaluation, calls for a more representative democracy that prioritizes common ground over privilege.

</summary>

"If you're working a blue collar, no collar job, and got your name on your shirt that you've done something wrong."
"Education doesn't mean education anymore."
"More welders, less lobbyists, more scientists, less CEOs, more teachers and less lawyers."
"Let's create a system where you actually have something in common with the person governing your life."
"They're taking care of their own and they're leaving us out in the cold."

### AI summary (High error rate! Edit errors on video page)

Education often tied to devaluing labor in the United States, reinforcing class ideas.
Misconception that blue-collar jobs equate to failure in achieving the American dream.
Dangerous concept when education is confused with credentialing, favoring those with money.
Public education system not what it used to be; emphasis on credentialing over true education.
Those with money tend to receive the best credentialing, leading to power imbalances in a democracy.
Education system in the country may need a revolution due to current flaws.
Educators' decisions influenced by distant policymakers who may not understand teaching.
Calls for a more diverse representation in governance, including more waitresses and fewer millionaires.
Emphasizes the importance of creating a representative democracy where the governed have common ground with their representatives.
Critique on how current system prioritizes the wealthy and powerful over the general population.

Actions:

for educators, policymakers, activists.,
Advocate for educational reforms that prioritize true learning over credentialing (implied).
Support and amplify voices calling for more diverse representation in governance (implied).
Engage in local politics to ensure representation that mirrors community diversity (implied).
</details>
<details>
<summary>
2019-02-07: Let's talk about Cub Scouts taking a knee, nationalism, and patriotism.... (<a href="https://youtube.com/watch?v=ixedV66SfMY">watch</a> || <a href="/videos/2019/02/07/Lets_talk_about_Cub_Scouts_taking_a_knee_nationalism_and_patriotism">transcript &amp; editable summary</a>)

A ten-year-old Cub Scout's act challenges racism, embodying true patriotism and independent thought, symbolizing hope for dismantling divisive symbols.

</summary>

"Patriotism is correcting your country when it's wrong."
"Scouts are doing just fine."
"This country can build that monument. And these kids are gonna tear it down."

### AI summary (High error rate! Edit errors on video page)

A ten-year-old Cub Scout took a knee during the Pledge of Allegiance at a city council meeting, sparking varied reactions.
Some people struggle to fit their standard responses to this unique situation.
The attempt to blend racism and nationalism has led to young people associating the American flag with racism.
The rejection of racism by the youth will result in them rejecting the flag as well.
The young Scout's actions are rooted in independence, self-reliance, and free thought taught by scouting.
Patriotism involves correcting your country when it's wrong, not blind obedience.
Beau expresses gratitude towards the young Scout for embodying true patriotism at a young age and standing against racism.
Beau shares his Scout background and praises the Scout for his independent and thoughtful act.
The Scout's action symbolizes independent thought, self-reliance, and preparedness for future decisions.
Beau sees hope in the younger generation dismantling racist symbols and standing up for what's right.

Actions:

for youth advocates,
Support youth empowerment in community groups (implied)
Advocate for inclusive and anti-racist education in scouting organizations (implied)
Encourage independent thought and standing up against discrimination in youth (implied)
</details>
<details>
<summary>
2019-02-06: Let's talk about domestic violence and why women don't "just leave".... (<a href="https://youtube.com/watch?v=3YH26YcE6hc">watch</a> || <a href="/videos/2019/02/06/Lets_talk_about_domestic_violence_and_why_women_don_t_just_leave">transcript &amp; editable summary</a>)

Beau explains the misconceptions around domestic violence shelters and the critical support they provide for survivors leaving abusive relationships.

</summary>

"One of the comments was, I don't even understand why these things exist, why can't women just leave?"
"It affects everybody, every class, but it's not as simple as just leaving."
"Women are 500 times more likely to be killed if they're in one of these relationships when they're just leaving."
"Any obstacle that's standing in the way, they get rid of it."
"It's something that can be done."

### AI summary (High error rate! Edit errors on video page)

Addresses misconceptions about domestic violence shelters and the challenges faced by survivors.
Domestic violence affects everyone regardless of gender, race, or class.
Personal connections drive Beau's involvement in the fight against domestic violence.
Shares personal stories of friends who were victims of domestic violence.
Women are 500 times more likely to be killed when leaving abusive relationships.
Domestic violence shelters play a critical role in reducing risks and providing support.
Financial control by abusers complicates leaving abusive relationships.
Challenges include finding a safe place to go, caring for pets, and potential job changes.
Domestic violence extends to the workplace, with a high percentage of women being killed by intimate partners.
Beau supports Shelter House of Northwest Florida for their innovative solutions and stigma-fighting initiatives.

Actions:

for supporters of domestic violence survivors.,
Donate money or old cell phones to organizations supporting domestic violence survivors (suggested).
Contact Shelter House of Northwest Florida to inquire about ways to help (suggested).
Support initiatives like providing kits for rape victims in hospitals (suggested).
</details>
<details>
<summary>
2019-02-05: Let's talk about the real state of the union.... (<a href="https://youtube.com/watch?v=eegrx5LlUBo">watch</a> || <a href="/videos/2019/02/05/Lets_talk_about_the_real_state_of_the_union">transcript &amp; editable summary</a>)

The United States faces political division, fragile masculinity, systemic racism, and rising hate crimes while individuals quietly rebel and build new systems for change.

</summary>

"We don't defeat tyranny and oppression by complying with it while we fight it."
"We defeat it by ignoring it while we quietly create our own systems to replace it."
"The state of those people who care? Well, they're in a state of defiance."

### AI summary (High error rate! Edit errors on video page)

The United States is as politically divided today as during desegregation.
Washington, D.C. is not in a good state.
Trump's wall dominates headlines despite not being a top priority for most Americans.
Families are still being torn apart for crossing a border.
Government and large corporations preach conservation while harming the environment.
$3.4 billion is spent on lobbying and corruption.
News focuses on manipulating emotions rather than informing.
The drug war continues, imprisoning innocent individuals.
Fragile masculinity is evident in men's reactions to a razor ad.
Teachers are forced to prepare for combat.
Systemic racism persists in society.
Youth are mocked and discredited.
Hate crimes are increasing, and Nazis are present in the streets.
Americans are fighting for safety and freedom for others, even at personal risk.
Eight companies produce more pollution than the entire U.S. population.
Communities are growing stronger, decreasing the power of DC politicians.
Independent journalists strive to separate fact from fiction.
Masculine men openly criticize toxic masculinity.
Teachers are implementing diverse methods to combat violence in schools.
People of all races are uniting against systemic racism.
Youth are at the forefront of societal battles.

Actions:

for americans,
Joining together with community members to combat hate crimes and systemic racism (exemplified)
Supporting independent journalists striving for truth (exemplified)
Implementing diverse methods to combat violence in schools (exemplified)
</details>
<details>
<summary>
2019-02-04: Let's talk about Patty Hearst, the homeless, and pitchforks.... (<a href="https://youtube.com/watch?v=mk7T5yS7_jI">watch</a> || <a href="/videos/2019/02/04/Lets_talk_about_Patty_Hearst_the_homeless_and_pitchforks">transcript &amp; editable summary</a>)

45 years after Patty Hearst's kidnapping, Beau questions the morality of extreme wealth through a Jeff Bezos example, proposing an end to homelessness in the US for a fraction of his wealth.

</summary>

"One guy can end homelessness and give them $200 a week. It's crazy. It's insane."
"When does it become moral to act? The system as it is, is turning the working class into the working poor and millionaires into billionaires."
"The entitled nature of the wealthy in the United States now."
"We still know where the pitchforks are. And if things don't change, those pitchforks are coming."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Patty Hearst was kidnapped 45 years ago today, and her family gave up $2 million in negotiations with the SLA.
The $2 million given up by the Hearst family was to be turned into food and distributed to the needy, not handed over to the kidnappers.
Patty Hearst's grandfather, adjusted for inflation, was worth about $30 billion when he died.
Beau questions the morality of extreme wealth in society, using Jeff Bezos as an example.
Bezos earned $78.5 billion last year, but most of it is not liquid cash.
Beau imagines what Bezos could do if he decided to end homelessness in the U.S., estimating it to cost $5.7 billion.
Beau suggests the idea of taxing the ultra-wealthy at a higher rate or holding them socially responsible.
He points out the extreme income inequality and questions when it becomes a moral imperative to act against it.
Beau criticizes the entitlement and lack of empathy shown by the wealthy in the United States.
He warns that if things don't change, people may resort to drastic actions to address inequality.

Actions:

for social justice advocates,
Advocate for policies that address income inequality and homelessness (implied)
Support initiatives that tax the ultra-wealthy at a higher rate (implied)
Raise awareness about the moral implications of extreme wealth accumulation (implied)
</details>
<details>
<summary>
2019-02-03: Let's talk about Trump's gift to Putin.... (<a href="https://youtube.com/watch?v=WP_BGUp-ScQ">watch</a> || <a href="/videos/2019/02/03/Lets_talk_about_Trump_s_gift_to_Putin">transcript &amp; editable summary</a>)

Trump's action towards Putin was a gift, removing the only obstacle for Russia to develop weapon systems that threaten European allies and jeopardizing European security.

</summary>

"Trump's action towards Putin was a gift, not punishment or leverage."
"The move jeopardizes European security and could lead to an arms race."
"Removing the treaty allows Russia to openly develop weapon systems that threaten European allies."
"Russia, on the other hand, needed these missiles as NATO expanded closer to its borders."
"Trump's decision signals a disregard for treaties and sets a dangerous precedent."

### AI summary (High error rate! Edit errors on video page)

Trump's action towards Putin was a gift, not punishment or leverage.
The treaty negotiated in 1987 banned land-based short and intermediate-range missiles.
The treaty heavily favored the United States over the Soviet Union.
The U.S. did not need land-based missiles due to geographical factors.
Russia, on the other hand, needed these missiles as NATO expanded closer to its borders.
Removing the treaty allows Russia to openly develop such missiles.
Obama's administration understood the limitations in dealing with Putin's Russia.
Trump's action has removed the only obstacle for Russia to develop weapon systems that threaten European allies.
The move jeopardizes European security and could lead to an arms race.
Trump's decision signals a disregard for treaties and sets a dangerous precedent.

Actions:

for politically engaged citizens,
Contact political representatives to express concerns about the potential repercussions of abandoning treaties (implied)
Stay informed about international treaties and their implications on global security (implied)
</details>
<details>
<summary>
2019-02-02: Let's talk about teaching the Bible in school.... (<a href="https://youtube.com/watch?v=lft9g73Smbw">watch</a> || <a href="/videos/2019/02/02/Lets_talk_about_teaching_the_Bible_in_school">transcript &amp; editable summary</a>)

President Trump's proposal to put the Bible back in schools sparks a reflection on religious tolerance, philosophy, and the decline of Christianity due to hypocrisy.

</summary>

"Teaching philosophy is education; enforcing moral code is indoctrination."
"The downfall of Christianity in the US is due to blatant hypocrisy."
"The ultimate irony is that today's Church of Satan embodies most of the ideals of Christianity better than most Christian churches."
"The goal of education should not be to teach you what to think. It should be to teach you how to think."

### AI summary (High error rate! Edit errors on video page)

President Trump wants to put the Bible back in school.
Christian students at school didn't like the idea much.
The modern church is indicted for not fostering religious tolerance.
Beau believes in teaching religious texts in schools if all are taught equally.
Teaching philosophy is education; enforcing moral code is indoctrination.
Philosophy, including religious philosophy, helps people think critically.
Beau points out similarities in teachings across different religions.
The downfall of Christianity in the US is due to hypocrisy, especially in politics.
Christianity is on the decline due to blatant hypocrisy.
Beau doubts the political expedience of disregarding Jesus' teachings.
Public schools turning into religious institutions is unlikely to happen.
Beau mentions the Church of Satan demanding their texts to be taught in schools.
Teaching philosophy, not just religious philosophy, can benefit the American people.
Education should focus on teaching how to think, not what to think.

Actions:

for educators, religious leaders,
Advocate for equal representation of all religious texts in school curriculum (implied)
Support teaching philosophy, including religious philosophy, in educational institutions (implied)
</details>
