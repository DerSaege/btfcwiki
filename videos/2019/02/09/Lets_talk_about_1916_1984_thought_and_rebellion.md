---
title: Let's talk about 1916, 1984, thought, and rebellion....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=4i9gbymKMjw) |
| Published | 2019/02/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- In 1916, 1200 men seized the General Post Office in Dublin, igniting the Irish Movement for Independence against British rule.
- The Irish rebellion, though militarily a failure, sparked a legacy of independent thought and resistance.
- The act of rebellion often begins with independent thought and questioning the status quo.
- Winston's first act of rebellion was not writing a diary but realizing the falsehood of the system around him.
- Rebellion starts with independent thought, analyzing situations, and questioning norms.
- Real change in society can be brought about by spreading ideas rather than resorting to violence.
- Ideas have the power to create change without the need for force or coercion.
- Sparking independent thought and defending ideas can lead to significant transformations in society.
- Encourages individuals to think for themselves, spark meaningful dialogues, defend their ideas, and create genuine change.
- The key to fighting oppressive systems lies in challenging them through independent thought and spreading new ideas.

### Quotes

- "Real rebellion starts with thought, independent thought, free thought, looking around and analyzing things for yourself."
- "For the first time, ideas travel faster than bullets."
- "Create real change. Put a new idea in somebody's head."
- "That's how you fight a system."
- "Anyway, it's just a thought, y'all have a good day."

### Oneliner

In 1916, Irish rebellion sparked by seizing Dublin's General Post Office led to a legacy of independent thought, urging individuals to challenge oppressive systems with new ideas.

### Audience

Activists, Free Thinkers, Change-Makers

### On-the-ground actions from transcript

- Spark independent thought by encouraging others to question norms and think critically (implied).
- Defend your ideas in dialogues and spark meaningful exchanges to create real change (implied).
- Engage in open, honest dialogues without relying on memes or slogans to foster independent thought (implied).

### Whats missing in summary

The full transcript provides a detailed historical context and insight into the power of independent thought and spreading ideas to challenge oppressive systems effectively.

### Tags

#Independence #Rebellion #IndependentThought #Change #Activism


## Transcript
Well howdy there internet people, it's Beau again.
So on Easter of 1916, about 1200 men marched into the streets of Dublin and they took over
the General Post Office, it's kind of the center of British rule in Ireland.
That act is seen as the beginning of the Irish Movement for Independence which was eventually
successful. Militarily was a great heroic epic of failure. Men marching in step
towards slaughter. They fought an Alamo style battle. They got inside, the Brits
surrounded them, opened up on them, drove them out. Then the Brits took the leaders,
stood them up against the wall, those that could stand. One guy named Connolly,
They had to strap to a chair, and they shot him.
They shot him to kill that rebellion.
It didn't end well for them.
In 1984, Winston's first act of rebellion,
if you're taking a high school test, was writing a diary.
It was the first thing that he did
rebel against the system he found around him.
Neither one of those are really the first act of rebellion though.
Long before any man walked into that post office, long before any of them picked up
a rifle, and fun fact for frequent viewers, a lot of the rifles used that day were caliber
.303.
But before any of that happened, they thought, they thought for themselves, they looked around
said this isn't right. And it was that independent thought. A lot of the guys
that were executed after the battle, they were poets. That's what they were known
for prior to the battle, was independent thought. And that thought sparked
rebellion. Winston, his first act of rebellion was not writing a diary. He was
looking around him and realizing that freedom wasn't slavery.
His first act of rebellion occurred inside that five-inch space inside his head.
And then from there, a small act of rebellion.
It's how it always works.
It is how it always works.
Real rebellion starts with thought, independent thought, free thought, looking around and
analyzing things for yourself.
A lot of people on Facebook have commented,
I can't post pictures, it's because I hate memes.
Some spark discussion, which can lead to independent thought,
but most are just slogans.
And anything that can be taught with a meme
can be countered by one.
That doesn't cause real independent thought.
not really. Both of those incidents, they teach us something else too. When Winston
sat down and started writing his diary, he knew it wasn't going to end well for him.
In fact, he said as much. He said, they get you. They always get you. And I don't think
that the men that walked into the general post office that day had any delusions about
what was going to happen. I don't think they thought they were going to win, pretty sure
they knew they weren't, but both of them knew when you're faced with a system like that,
you don't have to beat them, not up front, you just have to fight them. All those men
they executed after the battle, every one of them they shot brought ten more to the
cause because that idea had taken hold.
That's what happens.
An idea, if you discuss it freely and you defend it properly, well then the other person
believes it and that idea spreads like wildfire.
For the first time in history, we're faced with something unique.
There's two ways to cause real change in society.
You can put a bullet in somebody's mind, or you can put a new idea in it.
For the first time, ideas travel faster than bullets.
Probably the route we should focus on.
free ideas, they create change if they take hold. They don't have to be forced on anybody
at the point of a gun. Good ideas typically don't require force. So today, as an act of
rebellion, think. Think for yourself. About anything. And then without a meme,
without anything like that, try to spark a conversation with it. Defend that idea.
Create real change. Put a new idea in somebody's head. It doesn't matter what
the idea is. That's how you fight a system. That is how you fight a system.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}