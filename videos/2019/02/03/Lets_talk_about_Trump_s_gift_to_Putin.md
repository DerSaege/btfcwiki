---
title: Let's talk about Trump's gift to Putin....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=WP_BGUp-ScQ) |
| Published | 2019/02/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's action towards Putin was a gift, not punishment or leverage.
- The treaty negotiated in 1987 banned land-based short and intermediate-range missiles.
- The treaty heavily favored the United States over the Soviet Union.
- The U.S. did not need land-based missiles due to geographical factors.
- Russia, on the other hand, needed these missiles as NATO expanded closer to its borders.
- Removing the treaty allows Russia to openly develop such missiles.
- Obama's administration understood the limitations in dealing with Putin's Russia.
- Trump's action has removed the only obstacle for Russia to develop weapon systems that threaten European allies.
- The move jeopardizes European security and could lead to an arms race.
- Trump's decision signals a disregard for treaties and sets a dangerous precedent.

### Quotes

- "Trump's action towards Putin was a gift, not punishment or leverage."
- "The move jeopardizes European security and could lead to an arms race."
- "Removing the treaty allows Russia to openly develop weapon systems that threaten European allies."
- "Russia, on the other hand, needed these missiles as NATO expanded closer to its borders."
- "Trump's decision signals a disregard for treaties and sets a dangerous precedent."

### Oneliner

Trump's action towards Putin was a gift, removing the only obstacle for Russia to develop weapon systems that threaten European allies and jeopardizing European security.

### Audience

Politically engaged citizens

### On-the-ground actions from transcript

- Contact political representatives to express concerns about the potential repercussions of abandoning treaties (implied)
- Stay informed about international treaties and their implications on global security (implied)
  
### Whats missing in summary

The full transcript provides a detailed analysis of the consequences of Trump's decision on international relations, especially regarding Russian missile development and European security.

### Tags

#InternationalRelations #Treaties #Trump #Putin #EuropeanSecurity


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about Trump's gift to Putin, and it was a gift, make no
mistake about it.
It was not punishment, it was not obtaining leverage, it was a gift.
To understand how much of a gift you have to understand what the treaty really was,
the media is doing a really bad job of explaining that.
Clinton has indicated for the last decade that he wanted out of this treaty, and Trump
just opened the door for him.
So the treaty was negotiated by people who actually knew how to negotiate on behalf of
the U.S. back in 1987, and they didn't just write a book about making deals.
They made them.
In this case, the United States walked away with everything, and the Soviet Union walked
away with nothing.
To use Putin's terminology, the leaders of the Soviet Union were naive, and he was right.
That's a pretty good summation of that treaty.
It was entirely in the U.S.'s favor, all of it.
So what it was, it did two things, it banned short and intermediate range missiles that
were land-based, and that's the critical part, land-based.
Intermediate range means, I want to say it's 3,480 miles, it's less than 3,500 miles, how
about that?
It's that or less.
Go ahead and pull up a map and take a look.
The US doesn't use them.
Why?
Because we have the Atlantic and the Pacific
that we have to cross over.
There's no targets within that range.
You can find some minor targets in Russia
that could be reached from intermediate range
land-based missiles in Alaska.
But that's it.
It is not a piece of our strategic deterrence.
in any way, shape, or form.
Geography eliminated that for us.
We didn't need it.
So of course, we're totally willing to give it up
back in 87, because we didn't need it.
Russia, however, does.
Our intermediate range missiles are Seaborne.
They're in subs, or they're launched from the sky.
Russia then, or the Soviet Union then, and Russia today,
don't have that kind of infrastructure.
They can't do that.
The US Air Force controls the sky all over the world.
And that's not national pride showing.
That's just objective statement of fact.
There is not an Air Force on the planet that can touch the United States Air Force.
So we don't need land-based missiles.
We have no use for them whatsoever.
Russia does because as NATO expands and picks up more and more former Warsaw Pact countries,
their opposition gets closer and closer to their borders.
So that means they need them.
That's why Putin wanted them.
Now somebody's going to say, well, they were developing them anyway.
Probably.
I mean, yeah, I don't, I haven't seen the intelligence firsthand, but I'd be willing
to bet they were.
All major powers ignore their treaties.
But by removing it and letting them out of the treaty the way he did, they now no longer
have to hide it.
And when you're doing something in violation of a treaty, it gets really expensive.
It's really hard to do because you have to keep it secret.
They don't have to do that anymore.
Now somebody else will say, well, Obama could have fixed this problem.
No, he couldn't.
Not really.
Obama at least understood, his administration, understood that they were no match for Putin.
And that's, Putin is like our Air Force. When it comes to the international scene,
community organizer from Chicago is not going to stand a chance against
a real ex-KGB agent. And he wasn't former KGB the way, you know, George Bush
Sr. was ex-CIA. It wasn't a political job. He was actually out in the field. So Obama's
administration did the right thing. They passed. They were not the
administration to tackle this, neither is Trump's. The difference was that Obama at
least knew his limitations. Trump is either... I'll leave that out of it. It's a gift. We'll
leave it at that. This certainly appears to be a gift to Putin. And at the end of
the day we walk away with removing the only obstacle they have to developing weapon systems
that threaten our European allies.
We have thrown Europe under the bus and that's really all there is to it.
Now the scary part about this is what happens next.
arms race is very possible and God help us all.
Trump is not Kennedy, Trump is not Reagan.
Trump cannot handle brinksmanship.
Can you imagine that on Twitter?
The other thing that it did was that it gave countries like North Korea and Iran, why should
they follow any treaties?
The US doesn't.
Russia didn't pull out of this.
We did.
This was a mistake on a grand scale, and it's going to have repercussions for a very, very
long time.
So that's kind of a glimpse into it, into the underbelly of it.
There's the political stuff that's going on that everybody's talking about, but the
The end result, the hard facts of it, is that the U.S. opened the door and got rid of the
only obstacle that was standing in Russia's way of developing weapon systems that can
annihilate entire cities in Europe.
And we got nothing for it.
And we will get nothing for it.
Because contrary to what Trump's administration seems to believe, Putin has no interest in
being in this treaty.
The idea is, well, we're going to pull out if you don't come back into compliance within
six months.
They don't want to be in the treaty.
They have no interest in being in it.
And that's part of what I was talking about with Putin.
When US politicians want to blow off steam and they sit around and have a couple beers,
they play poker.
He plays chess.
And it just showed.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}