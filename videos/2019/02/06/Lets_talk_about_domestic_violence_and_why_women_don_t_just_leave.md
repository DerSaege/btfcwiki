---
title: Let's talk about domestic violence and why women don't "just leave"....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=3YH26YcE6hc) |
| Published | 2019/02/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses misconceptions about domestic violence shelters and the challenges faced by survivors.
- Domestic violence affects everyone regardless of gender, race, or class.
- Personal connections drive Beau's involvement in the fight against domestic violence.
- Shares personal stories of friends who were victims of domestic violence.
- Women are 500 times more likely to be killed when leaving abusive relationships.
- Domestic violence shelters play a critical role in reducing risks and providing support.
- Financial control by abusers complicates leaving abusive relationships.
- Challenges include finding a safe place to go, caring for pets, and potential job changes.
- Domestic violence extends to the workplace, with a high percentage of women being killed by intimate partners.
- Beau supports Shelter House of Northwest Florida for their innovative solutions and stigma-fighting initiatives.

### Quotes

- "One of the comments was, I don't even understand why these things exist, why can't women just leave?"
- "It affects everybody, every class, but it's not as simple as just leaving."
- "Women are 500 times more likely to be killed if they're in one of these relationships when they're just leaving."
- "Any obstacle that's standing in the way, they get rid of it."
- "It's something that can be done."

### Oneliner

Beau explains the misconceptions around domestic violence shelters and the critical support they provide for survivors leaving abusive relationships.

### Audience

Supporters of domestic violence survivors.

### On-the-ground actions from transcript

- Donate money or old cell phones to organizations supporting domestic violence survivors (suggested).
- Contact Shelter House of Northwest Florida to inquire about ways to help (suggested).
- Support initiatives like providing kits for rape victims in hospitals (suggested).

### Whats missing in summary

The emotional impact and personal connection Beau shares in advocating for domestic violence survivors.

### Tags

#DomesticViolence #SupportSurvivors #Donate #CommunitySupport #Awareness


## Transcript
Well, howdy there, internet people, it's Bo again.
So in that last video, I mentioned domestic violence
shelters, and as some of you know, I read the comments
on the shares and see what people say.
One of the comments was, I don't even understand
why these things exist, why can't women just leave?
Well, first, it's not just women, that's,
domestic violence affects everybody, every race,
every sex, every gender, everybody.
It affects everybody, every class, but it's not as simple as just leaving.
It is not as simple as just leaving.
A lot of the people who get involved in this fight get involved in it for personal reasons.
I happen to know there is a connection between one of the people who donated and Yvonne Lopez,
who was shot three times in the back of her head while she was making Christmas cookies.
I got involved because my best friend in high school, Amber, was killed by her husband while
she was just leaving. She becomes part of that statistic that the Mississippi Coalition
Against Domestic Violence talks about. Women are 500 times more likely to be killed if
they're in one of these relationships when they're just leaving. These organizations,
these domestic violence shelters, they facilitate that. They lower that risk dramatically and
they solve a lot of the problems. A lot of the problems, people don't even realize are
problems. If you're gonna leave, you need money and the abuser typically controls
the finances. It's part of the pattern. You may have kids and that makes finding
a place to go harder. You may have pets. You can't leave the pet because the
abuser is going to kill it. There's all of these things that play into it and
And even beyond that, you may have to change jobs.
From 2003 to 2008, 78% of the women killed at work were murdered by a former or current
intimate partner.
Three quarters.
Think about all the ways you can get murdered at work.
Three quarters of them were killed because of domestic violence.
Domestic violence is a huge issue in the United States.
Fifteen percent of all violent crime is intimate partner violence.
It's huge.
One of the organizations that I am familiar with and that I support is the Shelter House
of Northwest Florida.
They're extremely forward thinking.
When they ran across the pet issue, well, I guess we need to get a facility to house
the pets.
Go figure.
You know?
They just solve the problem.
Any obstacle that's standing in the way, they get rid of it.
And they address the way people think about domestic violence as well.
Years ago they made a t-shirt, it was a tank top, and all it said was, this is not a wife
beater.
Then you think about it.
That's what they're called, you know, those white tank tops.
What's it do?
It gives the impression that domestic violence is something that happens to lower class people
the projects or in trailer parks. It's not true. It happens to everybody of every
socioeconomic class. It was their way of kind of countering that stigma, and I
thought it was great. So a lot of these networks, they operate like safe
house networks. The woman makes contact, the abused makes contact, they
They roll out, pick them up, take them to a secure safe house, secure them, and then
let them restart their life.
Because in a lot of ways, it's a complete reset.
And that's one of the reasons I support them the way I do.
A lot of people also asked how to get involved, how to donate.
There's going to be a link on whatever social media platform you're seeing this on, either
in the description or in the comments.
There will be a link in that you'll find a way to donate that can fit you.
Whether it be money, which of course they need, or an old cell phone.
You know, if the abuser controls the finances, that cell phone gets cut off.
No communication.
Can make it hard to get a job.
Can make it hard to contact those people who do care about you.
It's something that can be done.
In one of our videos we talked about Bezos and what he could do if he cared.
Same thing.
Odds are you have an old cell phone sitting in a drunk drawer, mail it to them.
It means nothing to you could save someone's life.
Another question is, you know, why can't they go to a friend's house?
Because the abused knows the abuser is violent.
You're not going to visit that on your extended family or friends.
He's going to come looking for you.
20% of those people killed in murder-suicides are a family member or a friend of someone
suffering intimate partner violence.
That's one.
So Shelterhouse of Northwest Florida, give them a call or look at the link.
They also make these kits that are put at hospitals for rape victims.
Inside the kit is, there's a few things, but to me the most important is there's fresh
clothes.
If you go to a hospital after being sexually assaulted and undergo a rape kit, now you're
in a gown, you don't want to put those other clothes back on.
These kits can help take a traumatic experience that gets prolonged by the medical procedures
and help alleviate that.
It's something really simple, but it can mean a lot.
And then there's methods to sponsor a family.
There's all kinds of things.
There's certainly something for you.
And then, again, this affects everybody.
It affects everybody.
isn't there should be no stigma about this and just remember that normally by
the time the abused calls the police they've been assaulted 35 times anyway
It's just thought y'all y'all have a good night

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}