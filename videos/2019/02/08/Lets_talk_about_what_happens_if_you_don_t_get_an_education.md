---
title: Let's talk about what happens "if you don't get an education"....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=aCvYfWCcMPc) |
| Published | 2019/02/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Education often tied to devaluing labor in the United States, reinforcing class ideas.
- Misconception that blue-collar jobs equate to failure in achieving the American dream.
- Dangerous concept when education is confused with credentialing, favoring those with money.
- Public education system not what it used to be; emphasis on credentialing over true education.
- Those with money tend to receive the best credentialing, leading to power imbalances in a democracy.
- Education system in the country may need a revolution due to current flaws.
- Educators' decisions influenced by distant policymakers who may not understand teaching.
- Calls for a more diverse representation in governance, including more waitresses and fewer millionaires.
- Emphasizes the importance of creating a representative democracy where the governed have common ground with their representatives.
- Critique on how current system prioritizes the wealthy and powerful over the general population.

### Quotes

- "If you're working a blue collar, no collar job, and got your name on your shirt that you've done something wrong."
- "Education doesn't mean education anymore."
- "More welders, less lobbyists, more scientists, less CEOs, more teachers and less lawyers."
- "Let's create a system where you actually have something in common with the person governing your life."
- "They're taking care of their own and they're leaving us out in the cold."

### Oneliner

Beau questions the link between education and labor devaluation, calls for a more representative democracy that prioritizes common ground over privilege.

### Audience

Educators, policymakers, activists.

### On-the-ground actions from transcript

- Advocate for educational reforms that prioritize true learning over credentialing (implied).
- Support and amplify voices calling for more diverse representation in governance (implied).
- Engage in local politics to ensure representation that mirrors community diversity (implied).

### Whats missing in summary

The full transcript provides a deeper exploration of the flaws within the education system and the need for a more inclusive and representative democracy.

### Tags

#Education #Representation #Democracy #Inequality #PolicyChange


## Transcript
Well, howdy there, internet people, it's Bo again.
You know, if you don't get an education,
you're going to end up digging ditches
or working as a waitress.
You ever heard something like that?
Might even said something like that.
I did talk, thought about it.
Very dangerous idea, very dangerous concept
in a representative democracy.
It reinforces the class ideas.
You think about it, does that statement
teach you to value education or does it teach you to devalue labor? There's this
theme, this idea in the United States that if you're working a blue collar, no
collar job, and got your name on your shirt that you've done something wrong,
that you've made a mistake, you're not living the American dream, and that if
you're in that situation, well maybe you should just leave the decision-making to
your betters. That concept gets more dangerous when we start confusing
education and what it means. Education doesn't mean education anymore. The
device you're watching this on has access to the entire collection of human
knowledge. Education doesn't have to take place in a classroom. Education doesn't have
to take place in a building. It can take place anywhere. And despite the best efforts of
those on the ground teaching in classrooms, the public education system isn't what it
used to be. Most times when people say get an education, what they really mean is
get credentialing. And when you confuse the two, man, that gets dangerous.
Because what does it mean? You follow it out. If education means credentialing
and credentialing does have to come through an institution, who gets it?
Those people with money. Who gets the best credentialing? Those people with the
most money. Who gets into positions of power? Those people with the most money.
To represent a democracy, who do they represent? Not you. They represent
themselves. And then we're all surprised when they pass legislation, you know,
giving themselves tax breaks for their second yacht and get rid of yours. The
education system probably needs a revolution in this country, but that's
That's beside the point.
The point is the educators that are in the classroom through no fault of their own are
having their decisions made by people a few hundred miles away in the state capitol or
thousands of miles away in DC, most of whom have no idea what it takes to teach because
it isn't a representative democracy.
And we see that now with a lot of the maverick congress people that are coming up.
Oh, well she was just a waitress.
Well I think we all watched just a waitress school, Congress on Ethics.
Pretty impressive.
If you haven't seen it, you need to watch it.
Say we need more waitresses.
Less millionaires.
more welders, less lobbyists, more scientists, less CEOs, more teachers and less lawyers.
If we're going to have a representative democracy, let's make it representative.
Let's create a system where you actually have something in common with the person governing
your life.
The idea of a representative democracy isn't just that you have someone there to represent
you.
It's that there's something like you.
And we don't have that anymore.
They're taking care of their own and they're leaving us out in the cold.
Anyway, it's just a thought.
I'll have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}