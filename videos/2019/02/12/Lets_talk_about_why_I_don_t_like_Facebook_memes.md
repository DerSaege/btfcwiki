---
title: Let's talk about why I don't like Facebook memes....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=GIZ14Szfe10) |
| Published | 2019/02/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Expresses dislike for Facebook memes and conducts a demonstration to illustrate his point.
- Shares a meme featuring AOC with a controversial quote about guns.
- Reads through comments on the shared meme, showcasing derogatory and misinformed remarks.
- Clarifies that the quote attributed to AOC actually belongs to Donald Trump.
- Criticizes memes for oversimplifying complex issues and hindering meaningful discourse.
- Advocates for engaging in thoughtful and informed conversations.
- Contrasts Trump and AOC's stances on the Second Amendment and due process.
- Points out Trump's actions regarding firearms regulations.
- Mentions AOC's position statement on gun violence and the Second Amendment.
- Concludes with a quote from George Washington about not believing everything on the internet.

### Quotes

- "This is why I don't like memes."
- "Boils things down to talking points, eliminates discussion. It's a slogan."
- "It completely destroys any meaningful conversation."
- "Donald Trump is not pro-Second Amendment."
- "Don't believe everything you read or see on the internet, George Washington."

### Oneliner

Beau conducts a meme demonstration, revealing misattributed quotes and advocating for meaningful discourse, contrasting Trump and AOC's stances on gun control.

### Audience

Social media users

### On-the-ground actions from transcript

- Fact-check memes before sharing (suggested)
- Engage in informed and meaningful dialogues about political issues (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of the dangers of misinformation spread through memes and the importance of engaging in substantial dialogues for a better-informed society.


## Transcript
Well, howdy there, internet people, it's Bo again.
So I mentioned that I don't like Facebook memes.
A lot of people ask me why.
Today I conducted a demonstration.
I shared a meme I saw on Facebook.
It's a picture of AOC.
It's got the quote, I like to take the guns early,
take the guns first, do process second.
So I shared it.
Then I waited a few hours, and I went to the shares, people who shared it from me, and
I looked at the comments section to see what people had said about it.
I'm going to read some of the comments to you, most of them not going to read them verbatim
because some of them are pretty bad.
Truly the most ignorant member of Congress due process second LOL is your part of the
female anatomy, hurting because Bronx now repped by dumbest broad in history.
Female dog can't read, needs to read a word I'm assuming is supposed to be constitution.
I don't know, I can't read it.
Commie socialists don't believe in due process, go figure.
Drag this traitor out in the street.
This dumb, a lot of bad things, MAGA.
Now I liked this meme and I shared this meme because the second time this happened to her,
another quote that went around from her was, owning guns isn't a right, if it was it would
be in the Constitution.
And there are still people who believe that she said that.
She didn't say either one of these things.
In fact, the one that I shared, this quote,
I like to take the guns early, take the guns first,
due process second, that's Donald Trump, MAGA.
Now, when people were informed of who actually said this,
no longer the dumbest broad in history,
all of a sudden they wanted context.
This is why I don't like memes.
Boils things down to talking points
eliminates discussion. It's a slogan. It invigorates the base, but it doesn't
create any meaningful discussion. It limits it. Those who want to believe the
information presented in it do, and those who don't, find some way to discredit it.
It completely destroys any meaningful conversation, and meaningful
conversation is something that we need in this country desperately. So let's
Let's have some.
Donald Trump is not pro-Second Amendment.
That quote proves it.
It really does.
If you're willing to disarm somebody without due process, that is not a pro-Second Amendment
stance.
He's already enacted more regulations on firearms than Obama did.
So what is her actual stance?
Well, we don't really know and voted on anything yet.
But in her position statement, she says there are ways to curb gun violence without running
a file of the Second Amendment.
So to her it's at least a concern.
Whereas with Donald Trump, we'll worry about due process later.
So it certainly appears that the silly commie socialist is more second amendment than the
president.
So anyway, all of this reminds me of one of my favorite quotes, don't believe everything
you read or see on the internet, George Washington.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}