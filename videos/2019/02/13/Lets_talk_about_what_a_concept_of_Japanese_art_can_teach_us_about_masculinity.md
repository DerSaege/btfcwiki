---
title: Let's talk about what a concept of Japanese art can teach us about masculinity....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2Px9F0p2TCE) |
| Published | 2019/02/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Critiques resources aimed at helping young American males find masculinity on the internet as ridiculous.
- Recalls a time working in a call center with an old Japanese man during the overnight shift.
- Describes drinking sake with the old Japanese man who talked about a Japanese concept called Shibumi.
- Explains Shibumi as subtle, understated, part of nature but separate from it, in control of nature, and representing perfection without effort.
- Suggests that masculinity cannot be quantified or charted and should involve maintaining control over natural characteristics effortlessly.
- Emphasizes the importance of effortless perfection in masculinity and the need to accept oneself rather than mimicking role models.

### Quotes

- "Maybe masculinity can't be quantified the way people try."
- "Effortless perfection. Now that is a hard idea to get your head around."
- "When you see somebody trying to be masculine, you see somebody trying to be a tough guy, happens you immediately know they're not."

### Oneliner

Beau shares a Japanese concept, Shibumi, to redefine masculinity as effortless perfection, separate from conforming to charts or molds.

### Audience

Young American Males

### On-the-ground actions from transcript

- Embrace your natural characteristics and accept yourself as you are (implied).
- Refrain from trying to force yourself to conform to societal molds of masculinity (implied).

### Whats missing in summary

Exploration of the importance of embracing authenticity and effortless perfection in redefining masculinity.

### Tags

#Masculinity #Authenticity #Shibumi #Acceptance #SelfLove


## Transcript
Well howdy there internet people, it's Beau again. So all over the internet there
are charts and books for sale and guides and YouTube videos designed to help the
young American male become a man and find his masculinity. Most are utterly
ridiculous. Rather than talking about those, we're gonna talk about Japanese
art and a Japanese concept.
Years ago, seems like a lifetime ago, I drew a rotation working in what amounts to a call
center that you hope the phone never rings.
Basically two guys sitting in a room staring at each other for hours on end.
As luck would have it, my turn of doing this for a month, I got the overnight shift.
with this old Japanese guy, and I mean old. When we were off, we hung out with each other.
There weren't a lot of friendly people in the area. So we did what a lot of men do.
We drank way too much, constantly. Six, seven hours at a time, playing chess and just getting
just getting hammered. One night we're drinking sake and he picks up the cup in front of him
and he is just out of his mind drunk and he starts rambling about how perfect it is while
pointing out all of the imperfections in it. And then he grabs my cup and my cup is perfect
too but it has different imperfections. They're perfect because they don't conform even though
he could tell they were made by the same artist and he goes on to tell me that
it's a Japanese concept called Shibumi and this term this idea doesn't have a
definition in Japanese it embodies characteristics but it's a fluid
definition and after telling me there is no definition for it he goes on to try
to define it. I want you to picture a very drunk old Japanese guy trying to
define a Japanese term that does not have a definition in Japanese to an
American that has no clue what he's talking about. He says that it's subtle,
understated, it's a part of nature but separate from it and in control of it,
exercising control over it. And then he says the phrase that sticks in my mind, he says
it's perfection without effort. Effortless perfection. Now that is a hard idea to get
your head around. The second you try, you fail. Subtle, understated, a part of nature
and accepting of nature, but separate from it and in control of it, and effortless. That's
masculinity. Maybe masculinity can't be quantified the way people try. Maybe it can't be charted.
Yeah, there are natural characteristics that you're going to embrace, but you're going
to maintain control over them and you're going to be separate from them. And it's going to
be effortless. It's going to be effortless because you're not going to try to force yourself
fit some mold, it's going to be perfect because you're not going to conform to a chart.
Maybe your masculinity is going to be different than mine.
If you think about it, that effortless perfection part, that's really important.
Really important to the overall concept.
When you see somebody trying to be masculine, you see somebody trying to be a tough guy,
happens you immediately know they're not because it doesn't fit it's not who
they are if they were that guy they wouldn't be trying it would just be
natural it would be effortless so maybe instead of trying to look for role
models to mimic and try to force yourself to become like them maybe the
secret for the young American male in search of masculinity is to accept what
it is in you.
Anyways, it's just a fault. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}