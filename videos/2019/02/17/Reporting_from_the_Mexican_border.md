---
title: Reporting from the Mexican border....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=oJpmdtW7N0Q) |
| Published | 2019/02/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Reporting live from the Mexican border for the fifth column.
- President Trump declared a national emergency due to an invasion.
- No signs of any opposition forces at the border.
- Pentagon has not specified which country is invading.
- Refugees seen fleeing in terror.
- Assumption made that opposition forces must be close by.
- New information reveals it was a family of six, not an invasion.
- National emergency declared over people crossing the border to claim asylum legally.
- Beau questions who sent him there and expresses frustration with the situation.
- Beau is done with the network.

### Quotes

- "He declared a national emergency over people crossing the border to legally claim asylum."
- "I am so done with this network."

### Oneliner

Reporting from the Mexican border on Trump's national emergency declaration due to asylum seekers; frustration and disbelief at the situation.

### Audience

Media consumers, activists.

### On-the-ground actions from transcript

- Contact network to express frustration (implied).

### What's missing in summary

The full context and background details of the situation at the Mexican border. 

### Tags

#BorderReporting #NationalEmergency #AsylumSeekers #Refugees #MediaCoverage


## Transcript
Howdy there, internet people, it's Bo again, reporting live from the Mexican
border for the fifth column, uh, President Trump has declared a national
emergency due to an invasion, we've been down here most of the night, we have not
seen any signs of any opposition forces, the Pentagon has yet to, uh, tell us
exactly which country is invading, uh, however, we have seen what appears to be
refugees, fleeing in terror from somebody.
We can only assume that the opposition forces have to be close by, and that's why they're
heading this way.
Hang on, we have some new information.
I'm sorry, say again, it sounded like you said that was the invasion?
It was a family of six, confirm?
He declared a national emergency over people crossing the border to legally claim asylum.
Who sent me down here?
You get that idiot Justin on the phone.
I am so done with this network.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}