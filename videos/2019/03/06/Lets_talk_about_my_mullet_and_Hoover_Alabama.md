---
title: Let's talk about my mullet and Hoover, Alabama....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Ulf3hTtoNOw) |
| Published | 2019/03/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recalls a photo from his past where he was skipping school with older kids in Stewart County, Tennessee.
- Mentions witnessing young men and women in Hoover, Alabama fondly discussing ethnic cleansing on the internet.
- Draws attention to a series of racist incidents in Hoover, Alabama, including a girl using racial slurs and a teacher's inappropriate behavior.
- Describes a case in Hoover where the police mistakenly shot the wrong person, noting a history of racial issues in the area.
- Points out that Hoover, Alabama is named after a figure associated with white nationalist movements and a synagogue bombing suspect.
- Attributes the recent rise in racism to a reaction against having a black president and white people feeling threatened.
- Compares the current surge in racism to a passing trend, like a mullet haircut, suggesting it will eventually fade.
- Expresses concern for the young individuals in Hoover who may face consequences for their community's racist views, instilled by older generations.
- Notes the danger of nationalism fostering unfounded pride and hatred towards others.
- Encourages young people to be critical of outdated ideologies perpetuated by elders in their community.

### Quotes

- "It's a fad, triggered because we had a black president and a bunch of white people felt like they were losing their place in the world."
- "This uptick in racism is a fad, triggered because we had a black president and a bunch of white people felt like they were losing their place in the world."
- "Society, they know that their elders in their community aren't always right. These kids know that. And they should have been able to discern what's right and wrong."
- "It's the community's fault, but it's gonna be these kids that pay."

### Oneliner

Beau addresses the rise in racism triggered by societal shifts, pointing out Hoover, Alabama's history of racial issues and urging young people to challenge outdated community ideologies.

### Audience

Community members, Youth

### On-the-ground actions from transcript

- Address outdated ideologies in your community by initiating open dialogues and challenging discriminatory beliefs (implied).
- Educate yourself and others on the harmful impacts of nationalism and racism to foster a more inclusive society (implied).

### Whats missing in summary

The full transcript provides a detailed examination of the roots of racism, particularly focusing on the impact of community influences on individual beliefs and behaviors.

### Tags

#Racism #Community #Youth #Nationalism #History


## Transcript
Well howdy there internet people, it's Bo again.
You know there's a photo of me taking a lot of skipping school with a bunch of kids, a
lot older than me, had no business skipping school with them to be honest.
I was in Stewart County, Tennessee on a dirt road hanging out the side of a Camaro, wearing
denim on denim, red denim shirt, blue jeans. I had a mullet, white trash to find in a single
photo. All I needed was a can of bush beer. Got photos not on the internet though. You
know what is on the internet? A bunch of young men and women in Hoover, Alabama, speaking
fondly of ethnic cleansing. That's what's on the internet. They're going to have to
pay for these statements the rest of their life because the internet is forever. It is
forever. It shouldn't come as a surprise, though, that it's coming out of Hoover, Alabama.
You remember that girl that was swinging on the gym bars talking about how much she loved
the N-word, and y'all can all go F yourselves?
Hoover Alabama.
You remember that teacher that told the students to turn down Tupac, used a racial slur in
the process?
Hoover Alabama.
You remember the outrage over the parents at the football game saying their kind shouldn't
be on the playing field.
Hoover, Alabama.
Remember when Bradford got shot in the mall?
Cops said they got the right guy, the active shooter.
And they had to admit they got the wrong guy, killed the
wrong person.
Hoover, Alabama.
Should not come as a surprise at all.
Hoover, Alabama is named after a guy.
guy who Pulitzer Prize-winning journalist Diane McWhorter tied to white
nationals movements. He was a suspect in a synagogue bombing, which who the town is named after.
So this should not come as a surprise. It's still shocking, though. It shouldn't be.
It's a fad, it's a fad.
This uptick in racism is a fad, triggered because we had a black president and a bunch
of white people felt like they were losing their place in the world.
It's a fad, just like that mullet.
The difference is when I walk into a job interview, they know the mullet's gone.
For the rest of their lives, these kids, they walk into a job interview, you still believe
in ethnic cleansing.
This community did these young men and women a great disservice.
Because the stuff they're saying didn't come from them, did not come from these kids.
talking points, they're old. They're old. That came from the community. It's what
this community is creating. It's what this community is creating. Community
won't have to pay for it though. Be these kids. They'll probably be alright if they
never leave Hoover, Alabama. But if they ever want to get out of that armpit, join
in the rest of the world, they're going to have to answer for this because the internet
is forever.
This video will be around.
The nationalism used to create these statements, it's what nationalism does, teaches you to
take pride in things you had nothing to do with and hate people you've never met.
Live in Hoover, Alabama.
Have a problem with Jewish people?
Really?
How many could you possibly have met there?
I don't even think there's a synagogue.
This tide of racism is going to ebb.
This video won't.
Young people today need to remember that the elders in their community, they don't know
how to work their phone, they don't know how to work their computer.
Maybe there's a lot of things they don't know.
they got left behind and they're still clinging to ideologies that are obsolete.
On one hand I feel bad for them because everybody has some belief that they're
ashamed of that they held when they were a teenager if they've grown at all as a
person. But on the other hand, they should have known better. They should have known
better. Society, they know that their elders in their community aren't always
right. These kids know that. And they should have been able to discern what's
right and wrong. It's the community's fault, but it's gonna be these kids that pay
Anyway, it's just a fault.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}