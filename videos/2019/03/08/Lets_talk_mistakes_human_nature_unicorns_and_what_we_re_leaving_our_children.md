---
title: Let's talk mistakes, human nature, unicorns, and what we're leaving our children....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=iFj6f7ooW-w) |
| Published | 2019/03/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Society takes a long time to learn from mistakes and tends to double down without questioning.
- The war on drugs has resulted in wasted lives, resources, and money, with plants ultimately winning.
- Society embraces a system that encourages greed and exploitation.
- Political leaders often participate in corruption, rising through ranks to Washington D.C.
- Rare "unicorns" who genuinely care about people get corrupted in the political system.
- Beau rejects the current political system and believes in people living together without borders or government.
- He plants seeds for a future society he won't see, aiming for a stateless society doing no harm.
- Beau criticizes societal conditioning that discourages thinking globally and acting locally.
- Youth today are either angry and rebellious or apathetic due to the bleak future laid out for them.
- Technology offers immense potential to solve global issues, but society clings to outdated systems.

### Quotes

- "We embrace a system that encourages the worst instincts humanity has to offer."
- "People always say, think globally and act globally."
- "Simply, we plant the seeds today for trees that we will never sit under."

### Oneliner

Society's resistance to change, corrupt political systems, and the need for individuals to pave the way towards a better future, as Beau advocates for a stateless society based on cooperation rather than competition.

### Audience

Activists, Reformers, Individuals

### On-the-ground actions from transcript

- Plant seeds for a future society based on cooperation and doing no harm (suggested)
- Encourage critical thinking and acting locally to bring about change (suggested)
- Advocate for a society without borders or government, focused on people living together (implied)

### Whats missing in summary

Beau's call for individuals to reject the current flawed systems, think globally while acting locally, and work towards a cooperative society free from greed and corruption.

### Tags

#Politics #Society #Change #Activism #Future


## Transcript
Well howdy there internet people, it's Beau again.
So tonight we're going to talk about mistakes.
You know when a person makes a mistake, a smart person, they tend to learn from it.
wise person learns from somebody else's mistakes. But society takes us a real long time to learn
anything. And in the meantime, we just double down because we don't question what we're
told. The war on drugs is a perfect example. If you think about it, we've had millions
of years, people's lives, spent in a cage, trillions of dollars spent, untold amounts
of resources expended to fight some plants, and the plants are winning.
We embrace a system that encourages the worst instincts humanity has to offer, greed, and
we cheer as the planet is plundered, people in other countries are plundered, and well,
It's just the way it's got to be, because it's human nature.
And we say that, greed is human nature, without any hint of irony, as our children sleep next
to 100-pound dogs, and we go and ride horses on the weekend, because man is apparently
the animal, incapable of change. It's just human nature. And because of that, we embrace
a political system that gives us the best of the worst as leaders. Those who are great
at politics, they rise up through the ranks from the local to the state level and eventually
they find their way to Washington D.C. where they participate in an orgy of corruption
unlike anything we've ever seen.
But it's just human nature.
Every once in a while we get that unicorn that shows up.
that isn't in it for themselves, that really cares about the people.
What do we do with that uniform?
We send it to DC, where it spends the rest of its life being corrupted, slaughtered,
fighting a never-ending battle that bears no fruit. But we did our part. We voted.
You know, we did our part.
We expect things to change.
We look for hope and change.
Look for somebody to make America great, but we only send the people up there that are
in it for themselves or those rare unicorns.
I want no part of it to know, I don't want to run for office.
I don't believe that any person living knows how to run my life better than I do.
And I know I'm not wise enough to run your life.
I want no pardon.
When people ask me about my ideology, what I think we should be working towards, and
I tell them, you know, eventually, long after I'm dead, we should be hoping for a society
without borders, a society without government, a society of just people living together,
doing no harm. And they laugh because of human nature, again. Human nature won't allow that.
Eventually, somebody in this stateless society will carve out their own country, create a
government, rule over other people. Somebody else will do it. And eventually
we'll be right back to where we started. The people that say this always think
that they've made a great argument. But what that tells me is that deep down
they know the current system is wrong because their strongest argument
against the idea of a stateless society, is that one day it might become what we
have now.
If that's the worry,
what does that say about what we have now?
You know and people ask well, how are we going to achieve this? How are we going to do this?
It's up to you
It can give you guidelines
Give you ideas, but it's up to you if somebody was to give you marching orders
Well that would defeat the whole point
I mean simply, we plant the seeds today for trees that we will never sit under.
People always say, think globally and act globally.
We think that means recycling, everything, it means everything.
But that's hard, it requires a lot of effort, it requires a lot of thought, it requires
action.
And we've been conditioned to rock the boat and that that's what's important and let
our betters decide how we should live our lives.
Should we look at the youth of today, and they're either angry or apathetic.
They're either angry and trying to tear down societal norms, or they just don't care.
Of course they are.
They look at their future, the future that we've laid out for them, and they see the
past.
There's nothing new.
There's no revolutionary idea, there's nothing for them to pursue.
They are to be a cog in the wheel, hop on the conveyor belt, and let society mold them.
We live in a world where technology is advancing so quickly, most of the problems that the
world has can be solved if we spend less time trying to kill each other over flags or books.
The device that you're watching this on has the entire base of human knowledge on it.
You can access anything.
A person today can get a better education via their phone than what was possible 100
years ago, with 12 years of school, but we still cling to these old systems. We don't
revolutionize, we don't move with it, we let the technology be used to govern us, control
us, rather than using it to liberate ourselves.
We have to move towards some kind of new idea, some kind of new promise for the future generations
or we are going to leave them with nothing.
Leave them with growing inequality, a polluted planet, constant war, a surveillance state.
This is what we offer our children.
We can do that or we can lay the groundwork so one day all people really are created equal.
And rather than fighting for the scraps from those in power's table, we just have a really
long table, and everybody's eating.
It's outside a gas station, and this woman there, she's in a minivan.
Got her kid in her car, and the people at the gas station are kind of trying to chase
her off, being a little rude about it.
I asked what was going on, and she said, all I wanted was a little change, and you and
me both. But that is the world we have. People living in their cars. When we have the resources
anything as long as we cease to compete and we start to cooperate as long as we
abandon the idea of needing a ruler. Don't need a leader, you're a leader.
If we have to make a change, we leave nothing behind anyway it's just a thought
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}