---
title: Let's talk about why people believe conspiracy theories (and debunk one)....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=86d-fgSK_oo) |
| Published | 2019/03/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Expresses his interest in conspiracy theories as great thought exercises.
- Refutes the conspiracy theory about the New Zealand shooting being staged by the CIA.
- Points out the flaws in the theory, such as the CIA's unlikely involvement and the lack of evidence.
- Explains the physics of gunshot wounds, debunking misconceptions about bullet impact.
- Mentions the lack of blood in the conspiracy theory and questions its validity.
- Conducts a demonstration to debunk claims about bullet impact on a windshield.
- Debunks the theory using evidence from the video, like the presence of a magazine on the floor.
- Criticizes the theory for its lack of credibility and reliance on false evidence.
- Explains why people may believe in conspiracy theories for comfort and a sense of control.
- Links the belief in the conspiracy theory to political motivations, particularly among Donald Trump supporters.

### Quotes

- "If you don't believe that, the alternative is that somebody really did walk in and murder 50 people."
- "There is no solace believing that the CIA controls the world. It's not really true."
- "The world is chaotic. Bad things happen. Murders happen. That's what it's about."
- "The world's a scary place, and without somebody pulling the strings, it's downright terrifying."
- "It's either they don't want to admit that they've elected this guy, or they need something to believe in."

### Oneliner

Beau debunks a conspiracy theory surrounding the New Zealand shooting, illustrating why people grasp onto such narratives for comfort and control in a chaotic world.

### Audience

Skeptics, Truth-Seekers

### On-the-ground actions from transcript

- Fact-check conspiracy theories (implied)
- Challenge misinformation online (implied)

### Whats missing in summary

The transcript provides a critical analysis of a specific conspiracy theory but lacks broader insights into combating misinformation and promoting critical thinking.


## Transcript
Well howdy there internet people, it's Beau again.
So tonight we're going to talk about conspiracy theories and why people believe them.
For those that don't know, I'm a professional paranoid.
Humpty Dumpty didn't fall, he was pushed.
I love conspiracy theories, they're great thought exercises.
Most are utter garbage.
Some are true.
And at some point I'll do a video talking about the true ones.
But tonight we're going to tear one apart, we're going to tear one apart that is gaining
ground coming out of the right wing circles about the New Zealand shooting.
It didn't happen.
It was all made up.
Those were actors.
It was a staged event.
The CIA staged it to make Americans give up their guns.
Okay, so let's just start with the concept.
The CIA, in order to believe that this theory is even plausible, you have to believe that
the CIA has a problem killing innocent Muslims.
Probably don't believe that, because it's not true.
They don't.
If the CIA wanted to create this effect, they wouldn't stage it.
They would just conduct it.
It's a whole lot easier to send people in to go kill a bunch of unarmed people than
it is to fake it.
So on its face, this is stupid.
But we're going to go through the evidence that is presented in these videos and blogs.
We can go through each one and just kind of make fun of it for a little bit.
And then we're going to explain why people believe it.
Okay.
That's not how people react when they're shot.
Yeah, it is.
exactly how they react when they're shot. Contrary to every video and blog that I have seen talking
about this, the AR-15 is not high powered. It is not high caliber. It's.223 for those overseas,
5.56 millimeter..223 that is.223 of an inch wide. That's what that means. It's low intermediate
power. It doesn't send people flying across the room. They don't get knocked down by
the force. That's Hollywood. That doesn't happen in real life. It's a tiny bullet moving
very very fast. It goes into you. That's what it does. The injury wounds aren't big enough.
The entry wound should be less than a quarter of an inch.
That's how big they are.
The giant injury wounds you see on movies, that doesn't happen in real life.
There are things that can change it, such as it hitting bone instead of soft tissue,
or if it passes through something else first and sends the bullet wobbling, you'll get
a larger injury wound.
But no, they're supposed to be very small.
a small bullet. There's not enough blood. Based on what? What kind of wound is it?
Well, it's a gunshot wound. That doesn't tell me anything. Is it a sucking chest
wound? Is it in the liver? The kidney? Did it hit an artery? If you don't know the
answers to those questions and you don't, you can't say that. It's just something
you made up. You should be able to see the holes in the wall from where he
missed. Okay, for that one we're going to conduct a little demonstration. There's
a wood block back there. Blue dot, red dot, brown dot. You can see them, right? This
camera is higher resolution than the live stream and you can see those. Problem
is those are an inch and a half wide. You can't see the purple dot. The purple
eye by the way right there well that's a quarter of an inch that's why you don't
see it okay in the car he fires out the windshield and it should have shattered
gone everywhere that's Hollywood that doesn't happen in real life if it's low
velocity you can get some spidering but windshields are safety glass they're
designed to not shatter this was point-blank it wasn't low velocity now
I'm not gonna have somebody shoot my truck so put a link in the comment
section it's a 50 caliber BMG shooting a windshield 50 caliber BMG is about the
most powerful thing you can get on the civilian market guess what doesn't
happen. It doesn't shatter and blow out. That doesn't happen in real life. It takes a lot of
rounds to do that. The most damning evidence. There's a magazine on the floor. There is. If
you haven't watched the video, I suggest you don't. But guy walks in, he's got a shotgun. Boom, boom,
boom, drops it, pulls up the AR-15. He shoots, then he turns, bang, bang, bang, and then
he comes back. Now, if something was slung when he turned, guess where it would land?
Right where that magazine is. Or maybe he went to pull another magazine out and dropped
it and when he was moving, his foot hit it and sent it down the hallway. All of these
things are more believable than the CIA staging this massive event so they don't have to
kill innocent Muslims.
What percentage of drone strikes are collateral damage?
They don't care.
On the premise, on the face, the whole premise is false.
Then the military precision with which he moved means he's an operator.
No, no, there was no military precision.
That was amateur hour.
That should have taken half that time, especially considering you're not encountering any real
resistance because you're gunning down a bunch of unarmed innocent people.
No, there was no military precision.
The accuracy of his fire, it wasn't accurate.
Hitting the person doesn't mean it's accurate.
Accurate is anyway.
That is explained by that thing sitting on top of the rifle, those are his optics.
nothing there's no great conspiracy here that's the entirety of their evidence oh there is one
other thing there's a meme floating around there's actually few variations of it but uh it's got a
bloody scene of a carpet and then another carpet and it says the carpets not even the same they're
faking it well the i looked up the photo uh it's from years ago when they used a mosque as a triage
center after an airstrike. Yeah, of course the carpets aren't the same. They're from different
events on different continents. That's just something some liar threw together.
Okay, so why do people believe this stuff? I mean, we ripped it apart in minutes. Why do people believe it?
Because it's comforting. It is comforting to believe that there's a plan, that there's somebody out there
pulling the strings. If you don't believe that, the alternative is that somebody really
did walk in and murder 50 people. There is no man behind the curtain, guys. The world
is chaotic. Bad things happen. Murders happen. That's what it's about. There's no solace
believing that the CIA controls the world. It's not really true. Now the other reason that this
is being widely believed is because it's being widely circulated from certain websites. Here's
an interesting little tidbit. They're all Donald Trump supporters. Why does that matter? Because
he's named in the manifesto. So if they can convince you that this didn't happen, well then
Then Donald Trump's not responsible for the murder of 50 people.
That's why people want to believe it.
And that's why some people do.
It's either they don't want to admit that they've elected this guy, or they need something
to believe in, they need something, somebody out there in control of all of this chaos.
The world's a scary place, and without somebody pulling the strings, it's downright terrifying.
But the reality is, it's just a scary place.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}