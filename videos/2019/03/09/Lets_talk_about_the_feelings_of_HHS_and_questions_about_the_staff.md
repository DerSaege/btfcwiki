---
title: Let's talk about the feelings of HHS and questions about the staff....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=CK2wZLdNIIg) |
| Published | 2019/03/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Congress investigates sexual abuse reports from migrant children in U.S. custody.
- A congressperson misspoke about responsibility for abuse, hurting Health and Human Services' feelings.
- Health and Human Services clarifies it was not all sexual abuse but also misconduct against children.
- Beau stresses that regardless of contractors, the responsibility lies with Health and Human Services.
- Health and Human Services demands an apology for the congressperson's misspeaking.
- Beau questions which staff member waived background checks, recommended shelters, and oversaw children's welfare.
- He questions who placed children at the mercy of others and made the decision to hinder investigations.
- Beau compares the situation to journalists' work despite criticism, pointing out the duty to act.
- He concludes by suggesting that staff hindering investigations should be replaced.
- Beau delivers a critical message on accountability and prioritizing the safety of children in care.

### Quotes

- "It doesn't matter if your feelings are hurt."
- "Which one of your staff should replace the person responsible for this decision?"
- "Probably don't deserve to be in the position you're in."
- "We had a job to do. So do you."
- "Y'all have a good night."

### Oneliner

Congress investigates migrant children abuse, Health and Human Services prioritizes feelings over accountability, Beau calls for action and accountability.

### Audience

Congressional oversight committees

### On-the-ground actions from transcript

- Replace staff hindering investigations (suggested)
- Prioritize child safety over personal feelings (exemplified)
- Demand accountability from Health and Human Services (implied)

### Whats missing in summary

The full transcript provides additional context and depth to the criticism of Health and Human Services for prioritizing feelings over accountability in cases of child abuse.

### Tags

#Congress #MigrantChildren #Accountability #ChildSafety #Oversight


## Transcript
Well, howdy there Internet people, it's Bo again.
So Congress was looking into the thousands of sexual abuse reports filed by migrant children
who are in U.S. custody.
And I guess when talking to the person over the Office of Refugee Resettlement, which
is under the Department of Health and Human Services, a congressperson misspoke.
They misspoke.
And instead of saying that contractors were responsible for some of this abuse, I guess
he said employees or staff, well this hurt Health and Human Services, it hurt their feelings.
It hurt their feelings.
And they also felt that they needed to clarify that it wasn't all sexual abuse.
Some of it was just sexual misconduct against children.
That doesn't make it better.
And I would say that it doesn't matter if you farmed them out like so many horses to
be bred into somebody else's stable.
It's still your responsibility.
Now, even though your feelings are hurt, this is a little note that was sent to a reporter.
Health and Human Services would be happy to meet with him, the congressperson that misspoke,
once he corrects the hearing record from last week and provides an apology to the dedicated
men and women working tirelessly to protect and improve the lives of unaccompanied alien
children in our care.
Our care.
I thought they weren't in your care.
I thought it was contractors.
Isn't that why your feelings are hurt?
Since this is important to differentiate between staff and contractors, I have some questions
of my own.
Which one of your staff was responsible for waiving the FBI fingerprint background check?
Which one of your staff was responsible for recommending these shelters?
Which one of your staff was responsible for overseeing the welfare of the children you
placed there. If there wasn't a person tasked with that specific responsibility,
which one of your staff was responsible for placing these children at these
people's mercy? Which one of your staff was responsible for this idiotic
statement? And which one of your staff thought the right move would be to
hinder a look into child sexual abuse, I'm sorry, misconduct, because your feelings were
hurt.
The President of the United States said that journalists were the enemy of the people,
and that's why you didn't have newspapers for weeks on end, and there were no news reports
on cable TV news.
all stopped working because our feelings were hurt. No, of course not. That's
idiotic. We had a job to do. So do you. It doesn't matter if your feelings are
hurt. My last question would be which one of your staff should replace the person
responsible for this decision? Because if somebody feels that their feelings are
are more important, and they're willing to hinder a look into child sexual abuse
because of their feelings, probably don't deserve to be in the position you're in.
Anyway, it's just a fault.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}