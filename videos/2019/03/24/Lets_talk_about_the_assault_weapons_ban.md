---
title: Let's talk about the assault weapons ban....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=sIIK--yFdoA) |
| Published | 2019/03/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Lists multiple school shootings under the assault weapons ban to illustrate the stakes.
- Points out the ineffectiveness of the assault weapons ban, citing that it didn't work previously.
- Argues that there's no such thing as an "assault weapon" in firearms vocabulary, labeling it a political term.
- Mentions that popular rifles like the AR-15 will still be present even with an assault weapons ban.
- Raises concerns that banning certain weapons could lead to the use of more powerful firearms like the .30-06.
- Notes that the U.S. has more guns than people, making it a unique case in terms of gun ownership.
- Shifts the focus from firearms to the individuals using them, discussing the importance of secure weapon storage.
- Criticizes the glorification of violence and the lack of emphasis on conflict resolution and coping skills.
- Shares a personal story of learning to shoot at a young age, stressing the importance of understanding firearms as tools for a specific purpose.
- Concludes with a thought on the consequences of actions and presentations in society.

### Quotes

- "It's not going to do any good because in the firearms vocabulary, there's no such thing as an assault weapon."
- "Glorify violence for everything, every little infraction, become something worthy of death."
- "It's a tool with one purpose, to kill. That's what a firearm is for."

### Oneliner

Beau questions the effectiveness of an assault weapons ban, pointing out the deeper issues surrounding gun violence and individual responsibility.

### Audience

Gun policy advocates

### On-the-ground actions from transcript

- Secure your firearms to prevent unauthorized access (implied)
- Teach conflict resolution and coping skills alongside firearm safety (implied)

### Whats missing in summary

The detailed examples and personal anecdotes shared by Beau provide a nuanced perspective on gun violence and the broader societal issues contributing to it.

### Tags

#GunViolence #AssaultWeaponsBan #FirearmSafety #IndividualResponsibility #ConflictResolution


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're gonna talk about the assault weapons ban.
I'm gonna start off by reading a list.
I want you to stick with me as I read this list.
It's not a short list,
but it is important to illustrate the stakes.
Wycliffe Middle School, one dead, five wounded.
Blackville Hilda High School, two dead, one wounded.
21 wounded, Santana High School 2 dead, 13 wounded, McDonough High School 1 dead, 3 wounded.
What do all of these have in common?
All of these shootings took place when the United States had an assault weapons ban in
place.
This is about a third of them, it's not even all of them.
The Assault Weapons Band is an emotional reaction, it's a Band-Aid on a bullet wound though,
it's not going to do any good, it didn't last time, it did not last time.
It's not going to do any good because in the firearms vocabulary, there's no such thing
as an assault weapon. It's a made-up political term. From that alone, it should tell you
that this isn't going to work. It does not exist. It's not a real thing. The things that
are classified as assault weapons, like the AR-15, it's all cosmetic. I did a three-part
series on guns, gun control, and school shootings. I'm going to link it. Please watch it. Doesn't
No matter where you fall on this, if you're anti-gun,
you need to watch the first two.
If you are a pro-gun, you need to watch the third one.
And right now, I'm sure there's some pro-gun people
out there going, yeah, you're proving your point.
You just sit down and wait.
The assault weapons ban isn't gonna do
what you think it will.
And I know somebody's like, well, it's not gonna hurt.
Yeah, it will.
What is the reason weapons like the AR-15 get used so often?
The AR-15 is the most popular rifle in the United States.
So when a kid decides he wants to shoot up a school, guess what's in the house?
This would be a lot like banning the Camry because it gets used in a lot of drunk driving accidents because it's
a popular car.
Doesn't make any sense.
Well, it won't hurt. Yeah, it will.
weapons that are classified as assault weapons by legislation, they are low to
intermediate power like the AR-15. If you take those off, get rid of them, and
let's assume somehow you're magically going to make this work and people are
just going to give them up, it will be replaced with the most likely candidate
is a.30-06, which is a very high-powered rifle.
It worked in other countries, not really.
Not the way you think, but even, let's say that it did.
Other countries didn't have more guns than people, the United States does.
We are a very special case when it comes to guns.
At the end of the day, it isn't the firearm.
Semi-automatic firearms have been available for the civilian public for more than a hundred
years.
This is a relatively new phenomenon.
the gun people just stop this is your fault this is your fault it's not my
fault yeah it is guns just a tool right how
many photos you have on Facebook of you holding a hammer screwdriver or a
sawzall, pretending like it makes you a man.
None, right?
Just that rifle.
Just that one tool.
Glorifying that violence.
Why I needed to fight back against the government, sure.
You could, but you won't.
LARPing on the weekend and posting photos of it on Facebook proves you won't.
And then there's the memes.
Every time I see one, it drives me nuts.
When my gun sits there in the corner, it's never jumped up and killed anybody.
My question is, why is your weapon not secured?
Out of these shootings that I named, how many were done?
Because the shooter got access to a weapon that wasn't secured.
Almost all of them.
Glorify violence for everything, everything, every little infraction, become something
worthy of death.
These protesters are making me late.
Let's try to pass legislation so we can run them over and kill them.
Step on my flag, I'll shoot you.
And then you wonder why some kid walks into a school and starts killing people.
I taught my kid to shoot at 12 years old, that's cool.
When did you teach him conflict resolution, coping skills?
No, maybe he's not ready for a firearm.
I learned at that age, I learned at 12, 11, something like that.
I can tell this story because the person that did it is gone.
I'm fairly certain if you did this today, CPS would come take your kids.
I learned to shoot on a Marlin Model 60, 22 caliber rifle.
He handed it to me and had me shoot a shoebox.
Shot it for 10 minutes.
Then he went and got the shoebox, took the duct tape off of it, and showed me the
squirrel inside. From the very beginning for me it actually was a tool. It's a tool
with one purpose, to kill. That's what a firearm is for. Stop pretending like
holding one. Makes you masculine. It doesn't. But it gives the idea that the
that God makes the man.
And this is the result.
You can't blame this on libtards, it's not what it is.
It's the reaction to what you do, the image you present.
Anyway, it's just a thought now. Have a good night

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}