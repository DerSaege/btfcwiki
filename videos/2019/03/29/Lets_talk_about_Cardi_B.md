---
title: Let's talk about Cardi B....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=dcXfi5amUyE) |
| Published | 2019/03/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the controversy surrounding Cardi B and her past actions.
- Questions the victims of Cardi B's actions, implying they are partly to blame.
- Expresses disbelief that Cardi B needed to drug anyone, given her appearance.
- Raises doubts about the credibility of the victims and suggests they may have a history of blacking out.
- Criticizes those who compare Cardi B's actions to Bill Cosby's, arguing that it dehumanizes women.
- Warns against damaging relationships by making such comparisons.
- Urges people to stop equating drugging and robbing with drugging and raping.
- Ends with a message to have a good day.

### Quotes

- "I guess she kind of admitted that when she was younger, didn't have a lot of options or whatever, she enticed men to come back with her because they were going to pay her for sex, and then she drugged them and robbed them."
- "Not adding up to me at all."
- "You're telling me that she had to drug you? You? You gotta be kidding me."
- "They're property. They're not people."
- "Two things aren't the same."

### Oneliner

Beau questions victims, doubts credibility, and warns against harmful comparisons in the Cardi B controversy.

### Audience

Onlookers, individuals, allies

### On-the-ground actions from transcript

- Confront harmful comparisons in your social circles (suggested)
- Challenge victim-blaming narratives in discussions (implied)

### Whats missing in summary

The full context and nuance of Beau's perspective can be better understood by watching the complete transcript. 

### Tags

#CardiB #Controversy #VictimBlaming #Dehumanization #SocialAwareness


## Transcript
Well, howdy there, I don't know people, it's Bo again. So, I guess we got to talk about that Cardi B thing.
Because that's crazy. That is crazy.
I guess she kind of admitted that when she was younger, didn't have a lot of options or whatever,
she enticed men to come back with her because they were going to pay her for sex, and then she drugged them and robbed
them.
Man, that's nuts.
Didn't see that coming.
She was such a wholesome role model.
None of us knew that she had had a life like that when she was younger.
We expect people to be flawless.
The thing about this is I've got some questions.
I don't really want to justify her doing this.
I've got some questions for the victims because something didn't add up to me.
Not adding up to me at all.
So I want to start off with why were you there?
That's not a respectable place for a respectable person.
What were you even doing there?
Flashing money around a former gang member.
Man, what did you think was going to happen?
Go home alone with them?
Kind of asking for this if you want to be honest.
I kind of had this one coming.
Why'd you wait so long to report it?
I mean, let's be honest.
I mean, now she's rich and powerful.
You're just trying to ruin this woman's life.
I don't know.
I'm not buying it.
The other thing I just don't get, I don't really
understand it at all, is she's a pretty girl.
You're telling me that she had to drug you?
You?
You gotta be kidding me.
Man, I don't believe that.
No jury's going to believe that.
You've got a credibility problem, brother.
I don't see anybody believing it at all.
One last question.
Two.
You have a history of drinking until you pass out, black out, don't know what happened.
If we talk to your friends, and we will just to conduct a thorough investigation, are they
going to tell us stories about when you did that?
really a regret case, not a robbery case?
What were you wearing?
Guys, please stop comparing this to the Bill Cosby case.
You may not get the subtext of the message that you're sending with that comparison,
but trust me the women in your life do.
They're property.
They're not people.
In one case, somebody was drugged and robbed.
In the other case, somebody was drugged and raped.
To compare the two and act as if they're some kind of moral equivalent, while that means
that women are things, something you take, something you steal, they're not people that
are assaulted or violated.
That comparison probably needs to stop.
Not just is it ridiculous, it's probably damaging relationships in your real life.
Women around you are seeing this and they're like, wow, didn't know he thought like that.
They may not even be doing it consciously, but believe me, that connection is being made.
Two things aren't the same.
Anyway, it's just a thought.
y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}