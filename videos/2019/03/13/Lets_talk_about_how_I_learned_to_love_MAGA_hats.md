---
title: Let's talk about how I learned to love MAGA hats....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2kqqTHs397Y) |
| Published | 2019/03/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about his changed view on MAGA hats after a revealing interaction.
- Mentions his intention to buy a MAGA hat for the video but couldn't find one.
- Raises the topic of an app to help MAGA supporters find harassment-free places.
- Shares a story of a Trump supporter facing negative reactions and treatment in a Trump-supporting area.
- Describes microaggressions faced by the Trump supporter, including stares and sneers.
- Recounts instances of verbal harassment directed at the Trump supporter for wearing a MAGA hat.
- Shares an incident where the manager of a bar asked the Trump supporter to speak more friendly about Trump.
- Mentions a situation in a store where the Trump supporter was served but told not to wear the hat again.
- Suggests that these negative experiences have made the Trump supporter hesitant to wear the hat.
- Draws parallels between the treatment of the Trump supporter and other marginalized groups.
- Encourages using the MAGA hat as a teaching tool to understand societal dynamics.
- Points out that the younger generation might learn empathy through experiencing such treatment.
- Mentions safe spaces as places free from harassment.
- Concludes with a thought for the viewers to ponder.

### Quotes

- "Love it. It is a fantastic teaching tool."
- "By the way, those harassment free zones you're looking for, those are safe spaces."
- "I'm like, okay, do tell."
- "Those stares, those sneers, those are the micro aggressions."
- "There is a whole lot to unpack there."

### Oneliner

Beau talks about his changed view on MAGA hats and shares eye-opening experiences of a Trump supporter facing microaggressions and negative treatment in a Trump-supporting area, suggesting using the hat as a teaching tool for understanding societal dynamics.

### Audience

Community members

### On-the-ground actions from transcript

- Have open and empathetic dialogues with individuals facing discrimination (implied).
- Encourage introspection and empathy in younger generations towards marginalized groups (implied).
- Create safe spaces free from harassment in communities (implied).

### Whats missing in summary

The full video provides more in-depth insights into the societal dynamics and the impact of negative treatment on individuals wearing MAGA hats.


## Transcript
Well, howdy there, internet people, it's Bo again.
So we gotta talk about MAGA hats tonight.
I had one conversation, and it changed my entire view of them.
I was actually gonna buy one tonight to wear during this, but I couldn't find one I could
buy.
I'm not joking, this isn't one of my sarcastic lead-ins, I am dead serious.
If you don't know, there's an app out now so the MAGA hack crowd can find harassment-free
places that they can go and eat or drink or shop or whatever.
I found this funny and I made a post on my personal social media saying that I had just
marked the punk bar over in Fort Walton as a MAGA-friendly establishment.
If there is a place around here where you're pretty much guaranteed to catch a brick to
the forehead for wearing one of those hats, it's there.
Had a guy message me and tell me that he was disappointed in me because I don't know what
it's like to wear one of those hats.
I'm like, okay, do tell.
So he tells me five little things.
Completely changed my view.
Completely.
Instantly so he says it and first thing you need to know was that we are in MAGA country down here
This is this is I mean, this is a Trump supporting area
But if he puts his hat on and goes into town people will stare at him, sneer
They don't say anything they don't do anything, but they just look at him like he's trash
And I'm like, nah, I know I'm one of them
Then he told me this story about coming out of the Target and he was in the parking lot
pushing his buggy and somebody rolled by and yelled, Bigot.
He didn't know if he was going to be able to make it to his car before they made it
to him if they decided to stop and get out.
That's a little scary.
He was in a bar talking about Trump in a positive light and I guess he said MAGA a couple of
of times, and the manager of the establishment came over to him and told him to speak in
a way that was friendlier to the rest of the guests.
He went into the little Mexican store down in Panama City, and they served him, but then
made sure to tell him not to wear it in there again.
He said that so many things like that had happened to him over the last year or so.
doesn't even want to wear it anymore. There is a whole lot to unpack there. So
let's get started. Those stares, those sneers, those are the micro aggressions.
You've been saying don't exist. They are very real and now you've experienced
them. Being on a parking lot, that whole story, I would imagine that's a whole
lot like the people that roll by Home Depot and yell, I'm calling immigration, you got
papers. It's just a joke. You're not hurting anybody. Didn't mean to scare anyone. I know
two translators for the Army. They were out there speaking Arabic and they had somebody
come up to them and tell them to speak American.
That little incident in the Mercado, you're just dressed different.
You're not hurting anybody, are you?
Pull your pants up, boy.
And now, so many things like that have happened, and you're worried about your treatment at
the hands of society, so you want to hide who you are.
Well, I will allow the LGBTQ community to give you tips on leading a closeted life.
Oh yeah, I love the MAGA hat now.
Love it.
It is a fantastic teaching tool.
You use it to illustrate so many things about society.
Of course, the difference is you can take that stupid hat off anytime you want to.
Really was going to buy one tonight, but I only buy American.
So, that hat, it's not going to help with the older crowd, they're not going to change.
But the younger people that are experiencing this, after a little bit of introspection,
they're probably going to be a whole lot more sympathetic to a whole lot of people.
By the way, those harassment free zones you're looking for, those are safe spaces.
Anyway it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}