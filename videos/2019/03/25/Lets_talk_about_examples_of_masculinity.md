---
title: Let's talk about examples of masculinity....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=UHZ9VYJARL4) |
| Published | 2019/03/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Young men are questioning masculinity and its definition in the absence of a checklist or skill set.
- Masculinity is individualized and not determined by others.
- Beau shares three masculine role models: Teddy Roosevelt, Sheriff Andy Taylor, and John F. Kennedy, each showcasing different aspects of masculinity.
- Teddy Roosevelt exemplified American masculinity through courage, individualism, and advocacy for the downtrodden.
- Sheriff Andy Taylor humanized conflict resolution and was active in his community without carrying a gun.
- John F. Kennedy demonstrated principles, uplifted the oppressed, and had a vision for positive change.
- Beau defines masculinity as not using aggression to subjugate others but to uplift and improve oneself.
- Real masculinity embraces challenges and focuses on personal growth rather than dominating others.
- Masculinity is a journey of self-improvement and defining what matters to oneself.
- It ultimately boils down to an individual's definition, irrespective of others' opinions.

### Quotes

- "Masculinity isn't a trait, it's a journey."
- "Real masculinity likes a challenge."
- "Masculinity is something you're going to define yourself."

### Oneliner

Young men question masculinity without a checklist, Beau shares diverse role models, defining masculinity as a journey of self-improvement and personal definition.

### Audience

Men, individuals exploring masculinity

### On-the-ground actions from transcript

- Define masculinity for yourself (exemplified)
- Focus on self-improvement and uplifting others (implied)
- Embrace challenges and strive for personal growth (implied)

### Whats missing in summary

The full transcript provides a deep dive into questioning and redefining masculinity, offering diverse perspectives and encouraging personal reflection.

### Tags

#Masculinity #RoleModels #SelfImprovement #Individuality #PersonalGrowth


## Transcript
Well howdy there internet people, it's Bo again, so
I've got a few questions from young men about masculinity
And really
The question when you get down to the root of it, it's what is it because we don't know anymore
And it's funny because there is no checklist
It's not a skill set, you know?
Running a chainsaw, being able to change your own oil, and shooting a gun doesn't make
you masculine.
It's not what it is.
These questions were prompted by the videos that came out around the time of that Gillette
ad.
Around that same time, there was a meme floating around on Facebook that had my picture next
to Ben Shapiro's, because he was whining about how poorly men are treated.
I had a quote from each of us, and the caption said, you know, who would you trust to explain
masculinity to you?
Something to that effect.
The best response I saw was a guy who said, if either one of these guys gets to determine
your masculinity, you're not.
Liked that.
Masculinity is not a team sport.
It's very individualized.
I know that doesn't answer the question.
I'm going to tell you three masculine role models that I have.
The first, he started his career off as a police chief.
He used to go undercover, dress like a poor citizen and try to catch his cops doing something
wrong, being corrupt, taking a bribe, taking advantage of people, whatever.
If he did, he'd beat them up and fire them.
legends as he beat the guy up, stripped him naked through him in a dumpster, and then
fired him.
Later on, he was working in the secretary of the Navy's office, and he kind of started
a war.
But rather than sitting in the comfort and safety of that office, he went and fought
on the front lines, got the Medal of Honor for it later.
he was going to give a speech, would be assassin, walked up, shot him in the chest.
He gave the speech, bullet in his chest.
He was a huge conservationist, an explorer, he was a feminist.
When he was president, he had a boxing ring installed at the White House, it was almost
blinded in a fight.
He was the first president to invite a black man to come have dinner at the White House.
Won the Nobel Peace Prize back when it actually mattered.
That was Teddy Roosevelt.
Definitely the pinnacle of American masculinity when it comes to the individualism and the
desire to uplift those who are downtrodden and the willingness to put his own rear on
the line when it mattered.
Another guy looked up to, a little bit different.
He was a sergeant during World War II in France and Africa.
After the war, he came back to his little small town.
His wife died.
He became sheriff, spent most of his time looking after his boy.
He was unique because Hollywood was going to make a movie about him.
They're going to call the movie The Sheriff Without a Gun or something along those lines
because he didn't carry one.
He had one in his car, but he didn't walk around with one.
He was very active in his community, and he tried to solve problems by humanizing the
two opposing parties in each other's eyes, using some very unique conflict resolution.
That was Sheriff Andy Taylor from the Andy Griffith Show.
what you would think of as a tough guy, but definitely a masculine figure.
That wholesome masculine.
Now on the other end of things, John F. Kennedy, always look up to him, saying principles though.
He did put his rear on the line, but at the same time he was trying to uplift as president.
He was trying to free the oppressed.
Took a lot of vision for some of the things that he proposed, some of the things he was
actually able to accomplish.
To be honest, I always kind of had a crush on Jackie in Maryland, so, I mean, he had
that going for him, too.
Three very, very different people, all very masculine.
It's not a checklist.
There is no guidebook for it.
It's you.
The constants are that you're not trying to use your natural aggressiveness to
to subjugate others
trying to lift them up
you know today a lot of those who
talk about masculinity
they want to keep others down
real masculinity doesn't care because real masculinity likes a challenge
if you help them get up and they're better than you
well that just means you have to get better
and that's what it's about it's not about subjugating other people it's
about improving yourself
masculinity isn't a trait it's a journey
I know that's not really an answer to the questions
there may not be one
masculinity is uh...
something you're going to define
yourself.
What matters to you?
That's what's going to matter.
Anybody else's opinion of it is irrelevant.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}