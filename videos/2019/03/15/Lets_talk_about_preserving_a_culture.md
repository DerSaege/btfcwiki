---
title: Let's talk about preserving a culture....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=JCmSbWPKWrI) |
| Published | 2019/03/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the concept of white culture and its perceived destruction.
- Questions the existence of a unified white culture compared to specific ethnic cultures.
- Shares a personal story about a Creek man who identifies as white due to ancestry.
- Emphasizes that culture and skin tone are not inherently linked.
- Talks about his family's Danish ancestry and how culture transcends physical appearance.
- Predicts a future where racial blending leads to a homogenized appearance.
- Argues that linking culture solely to skin tone ensures its destruction.
- Advocates for preserving meaningful cultures beyond just physical characteristics.

### Quotes

- "Culture is not a thing."
- "Culture and skin tone are not linked."
- "Even once that happens, I'd be willing to bet that those people in the Northeastern United States have a very different culture than those people in Central Africa."
- "The blending of races is an inevitability."
- "If the culture becomes solely about skin tone, why should it remain?"

### Oneliner

Beau questions the concept of white culture, dismantles the link between culture and skin tone, and advocates for preserving meaningful cultures beyond physical characteristics.

### Audience

Cultural enthusiasts, anti-racism advocates

### On-the-ground actions from transcript

- Challenge stereotypes and misconceptions about culture (suggested)
- Encourage cultural exchange and understanding in communities (implied)

### Whats missing in summary

The full transcript provides a nuanced exploration of the intersection between culture, ancestry, and skin tone, challenging preconceived notions and advocating for the preservation of meaningful cultural practices beyond physical appearances.

### Tags

#WhiteCulture #CulturalIdentity #Preservation #Ancestry #SkinTone #RacialBlending


## Transcript
Well howdy there internet people, it's Bo again.
Birth rates, birth rates, birth rates.
So if you've read the
manifesto
purported to come from the shooter in New Zealand, you know what I'm talking about if you
haven't, I wouldn't really recommend you read it.
But uh...
there's a concept in it
that's worth talking about.
And that concept is the idea that, well, white culture is being destroyed.
Seems like something that should bother me, but I've spent a while thinking about it.
What is white culture?
I don't know.
I have no idea what it even is.
I know what Irish culture is, German culture, Russian culture.
Don't know what white culture is, especially when you're talking about people that have
been thrown all over the world via colonization.
What is the unified white culture?
Yelling at sports games?
A love of red meat?
What is it?
Can't really say because it's not a thing.
It doesn't exist.
culture is not a thing. There's this guy. He's Creek. Through a twist of fate, he's
more on a Lakota Road than a Creek Road. But nevertheless, he's Creek and not in the
way of his second cousin's twice-removed great uncle married a Cherokee princess. No,
He's creaky, he's on the rolls, he's very active in his community, he grows his own
medicine in the backyard.
His chinupa has a place of honor in his library.
Before he moved into the new house, his wife made sure that it was smudged.
He's been through 150, 200 sweats.
Had an inepi in his backyard until the hurricane.
He's white.
Skin tone.
He's white because his grandmother married a white guy.
So what does that mean for this conversation?
I mean this guy looks like me because he is me, but what it shows is that skin tone and
and culture aren't linked. Not really. Not in the way that I think a lot of people believe.
My wife, she's a Danish ancestry. Also blonde hair, blue eyed. So of course our kids are
blonde hair blue-eyed. To be honest, our family photo looks like an SS recruitment
poster. Even have a German Shepherd. But those little blonde hair blue-eyed kids
can sit around that fire and tell you about the trickster. They understand why
it's aligned the way it is. They can tell you about the medicine well and tell you
what the various medicines are used for. Culture and skin tone are not linked. It's a good
thing though. It's important that it's not. Not to the extreme that I think a lot of people
believe anyway. Because on a long enough timeline, well we're all going to look like Brazilians.
will become dark, dark will become light, we're all pretty much going to look the same.
But even once that happens, I'd be willing to bet that those people in the Northeastern
United States have a very different culture than those people in Central Africa.
culture is not skin tone. When you link skin tone to culture and that's how you
define it, it's funny in a way because it's the only way I know of to be 100%
certain that that culture is destroyed. The blending of races is an
inevitability. It is going to happen. I mean skin tones are dynamic anyway.
That's why we have different ones today. Because we moved around. We changed. And
they will change again. It is what it is. It doesn't alter the culture. It doesn't
alter the culture of people to have a different skin tone. If the culture is worthy, well,
even after skin tone change, that culture will remain. If that culture becomes solely
about skin tone, why should it remain?
What value does it give to those people later on?
I can understand wanting to preserve Russian culture, Irish culture, German culture, whatever.
I can get that.
I don't understand wanting to preserve white culture, because I don't know what it is.
Because it doesn't exist.
It's not a thing.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}