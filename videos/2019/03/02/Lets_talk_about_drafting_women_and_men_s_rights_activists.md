---
title: Let's talk about drafting women and men's rights activists....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NHO8eBkeykc) |
| Published | 2019/03/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Men's rights activists are viewed negatively by society and the activist community for reasons like not wanting to pay child support or using their children as leverage.
- An MRA group in Texas filed a lawsuit claiming that the draft was discriminatory and won in federal court.
- The Supreme Court used the argument of women not being allowed in combat as a reason to disallow them from the draft, a ruling from the 80s.
- MRAs believe that if women were subject to the draft, they'd be more vocal in ending it, leading to its sooner end.
- Beau argues that the draft is immoral, using government violence to force people to fight is wrong, and not a modern-age responsibility.
- Beau points out the inconsistency of opposing the draft while advocating to draft women to fight, exposing them to government violence.
- He stresses the importance of ideological consistency in building a movement and seeking equality in freedom, not oppression.
- Beau criticizes the strategy employed by MRAs as akin to seeking equality in oppression, contrasting it with seeking freedom.
- Despite the controversy around the draft, a military commission is likely to advise the Department of Defense to end the Selective Service.
- Beau concludes by condemning the approach of taking hostages to achieve goals, urging for a stand without resorting to harming the innocent.

### Quotes
- "You seek equality in freedom."
- "You've taken hostages in an attempt to get what you want. You're willing to hurt the innocent, those that aren't complicit in it, to get what you want."
- "If you want to take a stand, take a stand. Don't take hostages."

### Oneliner
Men's rights activists challenge the draft's discrimination but risk harming innocents in their strategy, while advocating for equality in freedom.

### Audience
Activists, Advocates, Critics

### On-the-ground actions from transcript
- Advocate for ideological consistency within movements (implied)
- Stand against harmful tactics and advocate for equality without resorting to violence or harm (implied)

### Whats missing in summary
The full transcript provides a comprehensive breakdown of the controversy surrounding men's rights activists and the draft, delving into the strategy's implications and advocating for ideological consistency within movements. For a deeper understanding of the topic and Beau's perspectives, watching the full video is recommended.

### Tags
#Men'sRightsActivists #SelectiveService #Draft #GenderEquality #EqualityInFreedom


## Transcript
Well, howdy there internet people, it's Bo again.
So today we're going to talk about men's rights activists and selective service.
That's the draft in the United States, military conscription.
Men's rights activists are generally looked down upon by American society at large and
widely looked down upon within the activist community.
They are seen as a bunch of guys who don't want to pay child support, want to use their
child as leverage to control their ex-wife or former domestic abusers.
That is widely how they are looked at.
True or not, that's the image.
Why do they have this image?
We're about to see.
So an MRA, a men's rights activist group in Texas, filed suit in federal court and
claimed that the draft was discriminatory.
They believed that women should face the same repercussions as men for noncompliance.
Now of course they won.
They won in court.
The old Supreme Court ruling was from the 80s, back when women were not allowed in combat.
Today, that has changed.
The Supreme Court relied on the fact that women weren't allowed in combat as a reason
to disallow them from the draft.
Since that fact has changed, the federal court has said, well, you're right now.
Do men's rights activists actually want women in the draft?
Do they want to ship them off to war?
Not according to the MRAs I've talked to.
The idea is it's a strategy.
They believe that if women were subject to the draft, they would be more vocal in calling
out to end it, and therefore the draft would end sooner.
That's the idea.
For whatever reason, that's the strategy.
So, if you want to end the draft as a whole, you must understand that the draft is immoral.
The idea of taking someone, using the threat of government violence to get them to fight
for you is wrong, that it's not a responsibility that should be held in the modern age.
This is something that occurred during feudalism.
So the idea of using the threat of government violence to get someone to fight for you is
wrong.
That's what they just did.
That's their strategy.
While opposing a draft, they turn around and draft women to fight for them.
Meanwhile, exposing them to that government violence, you know, the idea, and a lot of
them were like, well, no, no, no, it's not that, you know, the DOD will never draft women,
really, because their policies have changed.
That's why the ruling changed, it was in your argument.
If there was a draft, women could certainly be included now because they're no longer
prohibited from military service and combat.
Ideological consistency is very, very important, especially when you're trying to build a movement.
So using the same tactic as your opposition, using the same tactic that you're trying to
get rid of, probably isn't advisable if you want to build a movement.
Now if you just want to be edgy, whiny little boys, hey, look, you won one.
But the reality is, you just put millions of lives at risk, in theory.
You don't seek equality in oppression.
You seek equality in freedom.
Can you imagine Martin Luther King walking out and saying, I have a dream that one day
all white people will be treated as badly as we are.
Of course not.
That's stupid.
But that was the strategy just employed.
That's what was just used.
Now in reality, none of this matters because despite it apparently being a very, very hot
topic for them and something they feel necessary to file suit in federal court over, they obviously
aren't keeping up with the topic.
The National Commission on Military, National, and Public Service was convened in 2016.
Their report is due out I think early next year.
The guy that's running the show on this commission, he said that the key question they're trying
to figure out is why do we even need a draft at all?
Why does he have this opinion?
Because it's cost $1.1 billion and we haven't used it since 1973.
So there's a military commission.
There's a commission that is going to be advising DOD, apparently.
We haven't seen the final report yet, but it certainly looks like they're going to
advise DOD to kill Selective Service, to kill the draft.
Now DOD can't overrule this, but if they do, what are the odds they're going to care about
women that are now upset and calling for it to end?
Not much.
So either it was going to end on its own, or it isn't going to end and you've exposed
millions of women to the threat of government violence.
Now there is the argument that with equal rights comes equal responsibility.
And that's true.
That is true.
If it's actually responsibility, do you really feel that it's your responsibility to lay
down your life for the state whenever they call?
get no say in the matter. I don't think so. Again, that sounds like something that belongs
in feudal times. What the men's rights activist community has done is taken hostages. That's
what they've done. They're attempting to take hostages. They wonder why they're seen
as domestic abusers, and then they do this. You didn't do what I wanted, so here comes
the violence, just follow it up. Go ahead and make a post. You know, look at what you
made me do. That's why this image exists. You've taken hostages in an attempt to get
what you want. You're willing to hurt the innocent, those that aren't complicit in it,
to get what you want. If you want to take a stand, take a stand. Don't take hostages.
Anyway, it's just a fault.
all. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}