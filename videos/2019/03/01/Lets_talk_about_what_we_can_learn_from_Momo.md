---
title: Let's talk about what we can learn from Momo
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=S7L6wCzscDg) |
| Published | 2019/03/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Momo, a meme causing panic, is actually a joke known to kids for a year and a half.
- Children understand it's a joke and find parents' reactions hilarious.
- The fear around Momo reveals a lack of parental involvement in children's lives.
- Parents often use technology as a babysitter, unaware of what's on their kids' phones.
- Modern society values work over family time, leading to disconnection.
- Beau suggests that Momo's scare might have helped people focus more on their kids.
- He points out statistics on teen struggles like suicide attempts, drug use, and pregnancies.
- These real-life issues are much scarier than the Momo meme.
- Beau implies that the moral of the Momo story is to be more involved with our children.
- He encourages viewers to prioritize spending time with their kids.

### Quotes

- "We don't really spend enough time with our kids."
- "We sell hours of our life every day to some faceless entity at the expense of our families."
- "Maybe Momo is a good thing. Maybe like most good stories, it has a moral."
- "Become a little bit more involved if you can."
- "So maybe Momo is a good thing. Maybe like most good stories, it has a moral."

### Oneliner

Momo meme panic reveals lack of parental involvement, urging focus on real teen struggles over fictional scares.

### Audience

Parents, caregivers, families

### On-the-ground actions from transcript

- Spend quality time with your kids regularly (suggested)
- Monitor your children's online activities (suggested)

### Whats missing in summary

The full transcript provides a deeper exploration of the societal issues surrounding parental involvement and children's exposure to technology, along with statistical comparisons to illustrate the real dangers faced by teens.

### Tags

#Parenting #Children #Meme #TeenStruggles #FamilyTime


## Transcript
Well, howdy there, internet people.
It's Bo again.
And it is a dark and stormy night.
And Momo is coming.
OK, so for those who don't know, there
has been a Momo scare in the United States.
There were news reports, warnings from police,
warnings from schools.
Momo was going to hack into your kid's phone,
them to kill themselves. Momo is going to contact your kid over a whatsapp and
issue challenges and those challenges get progressively stranger and then they
culminate in the child killing themselves. Momo will splice into YouTube
children shows and tell your toddler to throw himself in the trash can. Okay so
so what is Momo really? It's a meme. It's a meme. It's been around, first time I heard
about it from my kid was about a year and a half ago. About a year and a half ago. And
they know it's a joke. It's creepypasta. It's a ghost story for the 21st century. Instead
of being told around the glow of the campfire, it's told around the glow of the Kindle or
the iPad.
And the kids, they know it's a joke.
They do.
Listen to my oldest talking to his buddies today about it.
They thought it was hilarious that parents were freaking out because this is old news
to them.
So why was it so scary?
Because like all great ghost stories, it revealed something about ourselves that was much scarier
than the ghost.
We don't really spend enough time with our kids, you know?
We have no idea what's on our kids' phones.
use YouTube, children's shows as a babysitter. Maybe it happened. We wouldn't know. Instinctively
we know this is wrong. We know that the society we have created that allows this and demands
this is certainly isn't a condemnation of parents. This is the way modern society is.
We don't spend enough time with our kids, we're not involved, generally.
And it is, it's a societal thing.
We sell hours of our life every day to some faceless entity, most times, suffering this
greed that we will never question at the expense of our families.
So in a way, I want to thank Momo, because it probably helped a lot of people realize
what's important.
Probably helped a lot of people focus on their kids just a little bit more, and even just
a little bit more is a lot.
I'm going to tell you some things that are scarier than MOMO.
In the next 24 hours in the United States, 1,439 teens will attempt suicide, 2,795 teen
girls will become pregnant, 15,006 teens will try drugs for the first time, 3,506 will run
away and too will be murdered. It's a whole lot scarier than Momo in a trill.
So, maybe Momo is a good thing. Maybe like most good stories, it has a moral and that
moral is to become a little bit more involved if you can. Anyways, just a thought. Y'all
So have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}