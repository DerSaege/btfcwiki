---
title: Let's talk about pirates and emperors....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=JonuekkDeGM) |
| Published | 2019/03/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses questions about terrorism and non-state actors.
- Talks about a terrorist action committed by the government.
- Emphasizes the importance of definitions in understanding propaganda.
- References St. Augustine's story about Alexander the Great and a pirate to make a point about morality and legality.
- Compares street gangs collecting protection money to taxation.
- Draws parallels between a suicide bomber and a drone operator in terms of morality.
- Argues that legality does not equate to morality.
- Mentions historical instances where atrocities were legal under respective laws.
- Stresses the role of power in determining what is legal and not morality or social consensus.
- Encourages watching a related video to further understand the topic.

### Quotes

- "The technological advance doesn't somehow make it more moral. Just makes it legal."
- "Everything the Taliban did, well that was legal under their laws."
- "There's no difference in real life. It's the same thing."
- "Size doesn't matter. At least not in this case."
- "It's power."

### Oneliner

Beau addresses the blurred lines between legality and morality, drawing parallels between different actions and urging a critical examination of power dynamics.

### Audience

Ethical thinkers, justice advocates.

### On-the-ground actions from transcript

- Watch the suggested video to deepen understanding (suggested).

### Whats missing in summary

The tone and nuances of Beau's delivery can be best experienced by watching the full video.


## Transcript
Well, howdy there, Internet people, it's Bo again.
So we had a whole lot of questions about terrorism being the purview of the non-state actor.
It was a concept people were having a hard time with.
This is a terrorist action committed by this government.
Not really.
Not by definition.
This is why definitions are important when it comes to propaganda and understanding the
conditioning that a lot of people suffer from.
So, because of that, tonight we're not going to talk about St. Patrick, we're going to
talk about St. Augustine.
St. Augustine told a story about Alexander the Great.
He called a pirate, he had the pirate brought before him.
And Alexander was like, I'm going to paraphrase here, dude, what are you doing?
You're disturbing the piece of the seas.
And the pirate's like, dude, what are you doing?
You're disturbing the piece of the whole planet.
But because I do it with one ship, I'm a thief, a pirate, a robber.
Because you have a fleet, well, you're an emperor."
Pretty smart, isn't it?
Morally there's no difference.
If you're attempting to use force to plunder, there's no difference.
It's the same thing.
That's why it gets very disturbing when people equate legality and morality.
When people say, well, it's legal.
Do it legally.
It means nothing.
What's the difference between a pirate and a privateer?
Ones authorized by the state.
They do the exact same thing.
A lot of street gangs and certainly organized crime collect protection money.
Some of that protection money goes back into the community.
They help the elderly or whatever.
What's the difference between that and taxation?
If you don't pay it, something bad is going to happen to you.
It's the same thing morally.
Some suicide bomber fills his truck or his van full of explosives and drives into a barracks
in the middle of the night, kills a bunch of sleeping troops.
What's the difference between that and a guy who's sitting in Texas flying a drone over
Yemen and blowing up a training camp?
Morally, they're the same thing.
They are the same thing.
The technological advance doesn't somehow make it more moral.
Just makes it legal.
Just makes it more sanitary.
There's no difference in real life.
It's the same thing.
It's just as barbaric, just one's easier to sanitize.
So we have to keep a hard look for those who confuse legality and morality because everything
the Taliban did, well that was legal under their laws.
Everything Saddam did was legal under their laws.
Everything the Islamic State did was legal under their laws.
Everything Nazi Germany did was legal under their laws.
Genocide of the natives, slavery, segregation, all legal under our laws.
The only difference between the two, it's not morality.
It's not some social contract that makes it seem like they have the consensus of the
people. In most cases they don't. It's power. And despite the video that I'm
going to put in the comments section, which I strongly suggest you watch, it
explores this topic in an unusual way. Size doesn't matter. At least not in this
case. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}