---
title: Let's talk about that new liberal plan....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=qUZiVPYTxYY) |
| Published | 2019/03/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing a new liberal plan with various components, including cutting military pay by 15% and creating employment opportunities for conservation work.
- The plan includes funding for renewable energy, railway projects, and paying artists for artwork to decorate public buildings.
- There is a focus on employee rights, strengthening unions, and the establishment of entitlement programs for different groups.
- A significant aspect of the plan is the proposal for a living wage for all American workers.
- Beau criticizes the economic impact of the plan and instead focuses on its historical context and implications.
- He challenges the myth that all World War II veterans were conservatives and dissects the reality of the greatest generation's values.
- The transcript delves into issues of bigotry, refugee support, and the caring nature of the greatest generation during hard times.
- Beau contrasts the values of the greatest generation with the modern American conservative ideology, pointing out discrepancies in their beliefs and actions.
- He argues that American history is complex, with flawed leaders and mistakes, challenging idealized notions of historical figures.
- The transcript concludes by reiterating the fearlessness and compassion of the greatest generation and contrasting it with the attitudes of the modern American conservative.

### Quotes

- "Living with a myth."
- "Nothing like the American conservative today."
- "Fearless is what made the greatest generation great."

### Oneliner

Beau critically examines a new liberal plan, challenges historical myths, and contrasts the values of the greatest generation with the modern American conservative ideology.

### Audience

American citizens

### On-the-ground actions from transcript

- Challenge historical myths about political ideologies (implied)
- Advocate for employee rights and a living wage (implied)
- Support conservation efforts and renewable energy initiatives (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of a new liberal plan, historical comparisons, and a critical examination of the modern American conservative ideology.

### Tags

#LiberalPlan #HistoricalComparison #ConservativeIdeology #EmployeeRights #LivingWage


## Transcript
Well, howdy there Internet people, it's Bo again.
So we gotta talk about this new liberal plan.
Wow, I've got the outline of it laid out here in front of me.
So first, conservatives, hold your laughter till the end, I don't want anybody making
my jokes before I get to them.
Liberals, we gotta talk about this.
I'm gonna go through an overview of it, and then I'm gonna hit a couple things in detail,
I'm going to burst some bubbles, but I need you to stick with me.
So the first part of the plan, we're going to cut the pay of the military by 15%.
We're going to employ 3 million people to do hippie conservationist work, planting trees
and cutting paths on parks, that sort of thing.
Whole bunch of money for renewable energy, hydroelectric dams.
There's the railway project.
going to pay artists to create 2,500 paintings and 17,000 sculptures and I
guess we're going to decorate public buildings with them or something. I don't
know that's not actually in the plan. There's a whole section on employee
rights one of which we're going to come back to. There's a section on strengthening
unions, they're gonna buy 10 million, 10 million acres of farmland and turn it
into parks and preserves. There's new entitlement programs for the old, the
sick, the blind, kids, anybody who wants a handout. There's a whole new section and
a whole new way of doing public housing because that's worked out so well for us.
us. But the big one in the employee rights section is a living wage, like a real living wage, like the
one everybody's been talking about. Yeah, it's in there. Basically, it's guaranteeing that anybody
who's working in the United States earns a decent living. The guy who drafted this, he's on record
as saying that no business that pays less than a living wage has any right to continue to exist
in the United States. Wow that's that is I guess if you support that kind of
thing you would call this ambitious. Everybody else would probably call it
expansive. I'm not going to talk about the economic impact of this. I'm sure
there's probably already a hundred videos on YouTube talking about that. I
I want to talk about it in a different way.
Can you imagine what people throughout American history would think of this plan?
Can you imagine what the guys who hoisted the flag on Iwo Jima and stormed the beaches
at Normandy, can you imagine what they would do to this guy if they caught him?
They'd vote him into office for a fourth term.
This isn't a new plan, this is the new deal.
So after that last video, conservatives were mad at me.
Apparently, they have laid claim to the men who stormed the beaches at Normandy, and we
certainly shouldn't equate them to any kind of liberal.
And looking back, you're right.
They're nothing like the modern liberal.
This is straight socialism.
It is.
And this isn't even all of it.
These are just the highlights I picked out that I could remember.
Yeah.
So the modern American conservative is living with a myth.
And it's a myth created by Hollywood.
See when you want to make a war movie, who's going to go watch it?
Right wingers, right?
guys you know really love to watch that stuff. When you watch a movie do you want
the characters to be completely unrelatable? No of course not you want
them to be like you. So Hollywood made them like them and that created the myth
And the myth is that all of these men that were part of the greatest generation, those
that stormed the beaches at Normandy, well, they were all conservatives.
The reality?
More than half of them voted for this dude.
It's a myth.
It is a myth.
And it's a myth that just won't die, apparently.
So let's talk about some other things just to make sure that the point gets across.
What about bigotry?
I mean it was the 40s, of course there was bigotry.
What about among the men that stormed the beaches of Normandy?
1945 the war ended.
There was some rumbling in the ranks.
1948, three short years later? Oh, the military's desegregated. Why? Because when you actually put
your rear on the line and you're getting shot at, skin tone really doesn't matter that much anymore.
Dude's got the same uniform I got on, we're on the same team.
Nothing like the American conservative today. What about refugees?
After World War II? After World War II. This is after the fighting is over.
So these people would be what the American conservative of today would call an economic refugee.
They're not fleeing violence. Oh, they took hundreds of thousands of them.
And then when they just couldn't take any more, you think they turned the military?
Did we use our military to stop them from coming, put them on the border, put flotillas
out?
No, we helped them move.
The U.S. Navy, just between Europe and Australia, ran more than 150 voyages moving refugees.
Nothing like the modern American conservative.
The greatest generation was great, not because they stormed the beaches at Normandy.
They were great because they were fearless.
Yeah, they were fearless when they were facing a machine gun nest on Utah Beach.
But they were also fearless when it came to new technology and new ideas.
That made them great.
The other thing that made them great is that they cared about each other.
See they had been through hard times during the Depression, they had seen suffering, and
then during World War II they saw suffering.
And when you see it for yourself it tends to change your perspective on things, at least
it should if you're human.
So now, the conservatives in the US do not get to lay claim.
Just remember, they're sitting there smoking their Lucky Strikes in that photo, holding
their Thompsons, foot on a dead Nazi, M1 in the air, half the dudes in that photo voted
for this, straight socialism.
No, the modern American conservative does not get to lay claim to this because, let's
see, environmental protection, conservation, mass transit, arts, employee rights, unions,
entitlement programs, public housing, living wage, they're literally trying to destroy
all of it.
nothing like them. Nothing. It's a myth. It is a myth. Living with a myth. And while
we're on the topic of myths, because I know this is going to come up, well you
know FDR did X, Y, and Z. It could be a list. He did a lot of bad things. Yep, he
did. He did. See this here, what we're talking about is American history. American
history is populated by people and people are flawed. People make mistakes.
People do bad things. Some of what FDR did is bad in hindsight. Bad by today's
standards. Some of it was pretty horrible by the standards of the day. Yeah, all
that's true. They voted for him four times. Four times. American leaders are
not perfect. That's another myth. We're talking about American history. If you
want to talk about infallible leaders or how all World War II veterans were super
conservative, you need to go over to the American mythology section because
that's where you're going to find that, because it's not backed up by fact. It's not true.
Socialism took on a nasty connotation in the United States during the Cold War. Prior to
that? Literally the only president elected four times. Anyway, just remember fearless
is what made the greatest generation great. I can assure you they would not hide behind
a wall and cower behind a wall because they were afraid of some men, women, and children
looking for a better life.
These are men who left cover and charged unprotected towards machine gun nests.
They are nothing like the American conservative of today.
Anyways just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}