---
title: Let's talk about the dangers of being moderate....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=CGeKdAWnnxc) |
| Published | 2019/03/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Moderates are dangerous and could be the downfall of the country.
- The Democratic Party is not truly leftist but slightly right, particularly evident in their foreign policy.
- Moderates trying to find compromise can enable extreme positions.
- Moderates are moving dangerously to the right, tolerating positions unthinkable in the past.
- Democrats are catering to far-right administrations and moving right to chase moderate votes.
- The moderate position has shifted significantly over time.
- Moderates are swayed by finding a middle ground between extreme positions, moving the country further right.
- Child sexual abuse in immigration camps is a result of the moderate position.
- Far-right extremists are taking advantage of moderates not researching or fact-checking.
- Moderates need to re-evaluate their morals and beliefs to prevent further dangerous compromises.

### Quotes

- "You're talking about murdering people. The moderate position of today? Well, can we just kill some of them? Trying to find that middle ground. It's dangerous."
- "Moderates in this country need to get their act together."
- "Are we a nation of ethnic cleansers? Are we a nation that enables child sexual abuse? Is that what we are? Is that moderate?"
- "Call me crazy. Maybe I'm an extremist."
- "What's the next compromise?"

### Oneliner

Moderates moving to the right enable extreme positions, endangering society by compromising on fundamental morals and ethics.

### Audience

Moderates, Activists, Voters

### On-the-ground actions from transcript

- Re-evaluate your morals and beliefs on society's ethics (implied).
- Research and fact-check claims instead of blindly seeking a middle ground (implied).

### Whats missing in summary

The full transcript provides a detailed exploration of how moderates' tendency to compromise dangerously shifts society's ethical boundaries.

### Tags

#Moderates #PoliticalEthics #ExtremePositions #FarRight #SocietyEthics


## Transcript
Well, howdy there Internet people, it's Bo again. So today we're going to talk about the dangers of being a moderate.
Moderates will be the death of this country, today's version of a moderate.
Before we get too far into this, I want to just go ahead and say that if you equate Democrats with leftists and
Republicans as rightists, and you think the middle is between the two, you've already lost the plot.
and the reason you've lost the plot is because of moderates. You can ask our
European viewers, I'm sure that they will be more than happy to explain to you
that the United States does not have a viable leftist party. It doesn't exist. There
are a few Democrats who cross center and go into leftist territory on a few
issues, but the Democratic Party as a whole is slightly to the right. This is
clearly exemplified in their foreign policy. They're not leftists. Okay, so how
did this happen? It's easy and now that the right wing, the far right, has figured
this out it's terrifying because they understand how it's working and now
they're exploiting it. The moderate of today, rather than holding to a moderate
position is
Attempting to find compromise to be the peacemaker to find that ever elusive middle ground
Let me show you how this works pretend for a moment. I am a right-wing extremist
Let's commit ethnic cleansing. I don't like all these brown people. We got to get rid of them. A
Moderate position is whoa whoa whoa whoa whoa?
No, and if you do that
that, we're going to have to resist you with violence.
The other extreme is not resisting ethnic cleansing, that's the moderate, that should
be the default.
The other extreme is just for suggesting this, you're already in crosshairs.
Those are the extremes.
The moderate position is no, we do not kill people, forcibly remove people based on their
ethnicity. We are a nation of immigrants. There should be no ethnic supremacy. It
shouldn't occur here. It does, but it shouldn't. That should be the default.
It's not. It's not. Why? Because the right wing has made wilder and wilder claims
and it's moved that discussion thanks to the moderates who don't hold fast to
their principles, they attempt to find that middle ground.
The moderates of today are tolerating positions that would have been unthinkable in the 1980s.
The idea of a militarized border in the U.S. during the height of the Cold War would have
been unthinkable. In fact, a right-wing president spoke out against it. Mr. Gorbachev, tear
down this wall. And the nation cheered. Why? Because that's a moderate position. But
today, you have the Democratic Party. Well, they're throwing bones to a far right-wing
administration, and they're using a different
qualifier to target people rather than saying, oh, well, we're going to get rid
of brown people.
Well, we're going to target people based on immigration status.
And if you cross a line in the sand legally, asylum seeking is legal.
It doesn't matter how many times the president says it isn't, it is, we're
going to take your kids away and turn them over to people who will sexually
abuse them.
This is not a moderate position, this is the position of a monster.
But in the interest of chasing those moderate votes, who are so easily swayed by trying
to find the middle ground between extreme positions, the Democrats move further and
further to the right.
that's what's happened. You know, you take it to the next step. Well, let's commit genocide.
The default, the moderate position is no, of course not, and if you attempt it we will
resist you with violence. You're talking about murdering people. The moderate position
of today? Well, can we just kill some of them? Trying to find that middle ground. It's dangerous.
It's dangerous. It is so dangerous that the moderate conservatives have lost the plot
completely. You look at the memes that are shared on patriotic pages that appeal to the
moderate conservative. There's the typical snowflake jokes and stuff like that, and there's
the ever-present meme that compares today's man with the man of the 1940s. And there's
always the joke, you know, well they wouldn't be able to handle it. They wouldn't be able
to handle it. They wouldn't be able to handle storing the beaches of Normandy. That's what
the generation of men in the 40s did. Yeah, they did. To kill fascists. To kill fascists.
That's why they did it. They stormed the beaches. They got in gliders. They jumped out of airplanes.
To kill fascists. Never forget that part. And those same pages, without any hint of irony,
their ultra far right wing fascist ideology because the moderate conservative has moved
further and further to the right.
They talk about the greatest generation and they've betrayed it.
We've come to the time when the moderate conservative stands up and becomes a volunteer
safety advocate for Nazis.
They're just expressing their beliefs.
The greatest generation?
Yeah.
Nazis walk into a bar, they don't get up.
The moderate position, both moderate liberals and moderate conservatives are moving dangerously
far to the right in the attempt to find that middle ground.
There are thousands of reports of child sexual abuse in what amounts to concentration camps.
Children ripped from their parents and turned over to people who abuse them or allow them
to be abused.
And the nation is silent.
Why?
Because that's the moderate position.
I mean, you know, they really should have done it legally.
It is legal.
That's the other scary part about this.
The far right, they've realized that
because the average moderate does not research on their own
and they just attempt to find the middle ground,
they can make insane claims.
And they know they won't be researched.
They won't be fact-checked.
Claiming asylum is legal.
Crossing the border without authorization
to claim asylum
it's literally in the law
they can claim asylum
at a port of entry which
is being blocked
they're telling them that they can't come in
and that they need to go get on a list
in violation of our own laws
or you can claim it once you're inside the country
and there's a specific part of it that says
your immigration status doesn't matter
That's actually the law.
So no, crossing a line in the sand
does not warrant having your children taken and raped.
Call me crazy.
Maybe I'm an extremist.
Moderates in this country need to get their act together.
They need to get their act together.
They need to sit down and think about what their morals truly are,
what they truly believe society's ethics should be.
Are we a nation of ethnic cleansers?
Are we a nation that enables child sexual abuse?
Is that what we are?
Is that moderate?
What's the next compromise?
Anyway, it's just a thought.
Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}