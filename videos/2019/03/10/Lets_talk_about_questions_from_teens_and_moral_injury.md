---
title: Let's talk about questions from teens and moral injury....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2_OX5v9z5MA) |
| Published | 2019/03/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Teens are angry and apathetic but know they should get involved in societal issues.
- They face a sense of hopelessness and futility, making it difficult to take action.
- Beau warns against looking up to him as a role-model.
- Beau introduces the concept of moral injury, often associated with war zones.
- Moral injury stems from failing to prevent or witnessing transgressions of morals.
- It can result from witnessing everyday injustices like racism or environmental degradation.
- Similar to PTSD, moral injury can lead to suicide, demoralization, and self-harm.
- Ways to cope include belief in a just world view and building self-esteem through small daily actions.
- Beau advises starting with small actions to combat issues causing moral injury, like sitting with a kid wearing a hijab.
- He stresses the importance of developing skills gradually in activism and finding where you are most useful.
- Effective activists share a common motivation that breaks through demoralization and self-handicapping behavior.
- Motivations of activists can stem from various sources like monetary addiction, religion, ideology, or ego.
- Beau encourages holding onto motivations as a reason to persist in activism.
- True believers who are ideologues are the most dangerous but also the most committed in the fight for justice.

### Quotes

- "Get in the fight for 28 days. That's all it takes."
- "Don't go all militant right away. Figure out what you're good at."
- "The most dangerous people in the world are true believers."
- "Hold onto your motivation, it's going to be the reason you make it through 28 days."
- "True believers are all ideologues."

### Oneliner

Teens face anger and apathy but can combat moral injury by starting small actions, finding their role in activism, and holding onto motivations.

### Audience

Teens and young activists

### On-the-ground actions from transcript
- Start small actions to combat issues causing moral injury (suggested)
- Sit with a kid that faces discrimination at school (suggested)
- Use social media to raise awareness about societal issues (suggested)
- Find where you are most useful in activism (suggested)
- Hold onto your motivations as a reason to persist in activism (suggested)

### Whats missing in summary

The importance of persistence, gradual skill development, and finding motivation in activism.

### Tags

#Youth #Activism #MoralInjury #FindingMotivation #SocialChange


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're going to talk about a question I've got.
It's actually like eight, maybe nine different questions
posed by teens within the last couple of days.
But it was all the same question,
had three components to it.
The first was that they were the angry and apathetic I
talked about when I was sitting by the fire. They know though that they should
get involved. They look around and they see all of this stuff. They're angry. They
want to get involved. The second component is that they can't. They feel it's
hopeless, futile, that they start and then blow it off. They just can't get in
the fight. And the third component was that for whatever reason you look up to
me, don't do that. So in this vein I want to talk about something called moral
injury. Now most times when this is discussed the context is people in war
zones. It's what it's normally presented as. That's when it's normally talked
But what it stems from is perpetrating failing to prevent or bearing witness to a gross transgression
of your morals.
That's what it is.
So yeah, one extreme would be a guy witnessing a war crime and failing to stop it, but another
version of it would be seeing the racism every day, the environmental degradation, the homeless,
all of the things in those messages, and not being able to do anything about it.
It's a lot like PTSD. It doesn't just occur in soldiers.
So, what happens when you suffer a moral injury?
What occurs?
Suicide, demoralization, self-harm, self-handicapping behavior, demoralization, feeling that it's
hopeless, futile, self-handicapping behavior, starting and then blowing it off.
Sounds familiar, right?
What are some things that can help you out?
One is a belief in the just world view,
or the just world hypothesis.
If your moral injury occurred because you realize
the world is not just, that's not gonna help you.
You have realized that bad things happen to good people,
and bad people aren't always punished.
So you can cross that one off the list.
other thing that can help is self-esteem. How do you build self-esteem? Age-old
question, right? Well in this context it's pretty simple. Get in the fight for 28
days. That's all it takes. Nothing big. Just every day do something to combat
whatever it is that is causing you that moral injury. You do what you can, when
you can, where you can, for as long as you can.
Doesn't have to be anything huge.
If racism, one of the ones brought up in the messages, is your focal point, sit with a
kid that wears the hijab at school.
Use your Snapchat to talk about something a little bit more serious.
Make a tweet, a Facebook post.
Doesn't have to be huge.
In fact, in the beginning, I would suggest you not attempt anything huge.
battles big enough to matter but small enough to win so you don't get
discouraged you don't get more demoralized and after 28 days it's not a
challenge it's a habit and as you go and continue on this path you'll develop
more skills to tackle the bigger things and you'll figure things out you'll meet
other people along the way that are on the same path.
You know, one of the things that I've noticed is that when people first get involved in
activism, whatever you want to call it, they want to jump right in and they want to go
do something crazy.
Don't do that.
Don't do that.
Find out where your skills are first.
You know, when I went to Ferguson with, well, let's just call them other seasoned activists,
there was a young man that wanted to go and went like, no, this one's probably not for
you.
And he was a little mad at the time.
Today, he's extremely influential.
He writes, though.
He writes.
Your skill set determines where you fall in this type of thing, your disposition.
Don't go all militant right away.
Don't go buy a beret and mask up just yet.
Figure out what you're good at, where you're going to be most useful.
I talked about Carrie Wedler the other day in a video.
I have never seen her at any street protest, any militant action whatsoever that she was
not covering for a news outlet.
I mean that she's an ineffective activist, of course not.
She reaches millions of people and has edited for two of the largest indie outlets out there.
She creates thought.
Probably a little more important than, you know, me dodging tear gas.
One is a statement.
other changes people's minds. So just be leery of that. Now the other thing that
I want to address is, you know, feeling like I've got it together. Okay? Now that
you know what moral injury is, go back and watch the videos again. I would
imagine that if you talk to a psychologist and told them the story of
me putting my hand on a gun and basically daring a cop to shoot, they
might call that self-harm. You'll see a trend and it's a trend that exists
throughout the activist community. The most effective activists I know, they
They have one thing and only one thing in common, this, this, that's it, it's what
motivates them.
They just broke through the demoralization and the lack of self-esteem and the self-handicapping.
They got past it.
You can too.
You can too.
Those people in the world, their motivations can be summed up by Marie, M-A-R-I-E, monetary
addiction, religion, ideology, or ego.
The true gems, those people that will never break, that will just stay in the fight no
matter what. They are all eyes. They're all ideologues. And normally that comes
from seeing something and it crystallizing and you responding to it.
The most dangerous people in the world are true believers. So don't run away
from this. You know, most of you, when I pushed for specifics, you regard it. And
that makes sense. Most people who have suffered a moral injury, most people who
are ideologues, their motivations are often complicated and most times secret.
Believe me, I understand.
Whatever it is, hang on to it though.
Remember it, because it's going to be the reason you make it through 28 days.
Anyway, it's just a thought.
the fall. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}