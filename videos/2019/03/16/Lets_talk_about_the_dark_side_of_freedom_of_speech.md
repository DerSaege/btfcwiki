---
title: Let's talk about the dark side of freedom of speech....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=d5pJOE-nzFU) |
| Published | 2019/03/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Mentioned ethnic cleansing in a recent video, sparking reactions in the comment section.
- People are talking about ethnic cleansing but using more subtle rhetoric.
- Individuals have the right to express bigoted views, but it's your obligation to speak out against it.
- There is a dangerous belief that atrocities only happen in countries with brown people.
- Gives an example from the 1990s where mass rape and murder occurred in a well-developed country.
- Warns that ethnic cleansing can happen in the United States and has already shown its face through mass shootings.
- Talks about the dangerous propaganda tactics used by certain groups to manipulate well-meaning individuals.
- Stresses the importance of waking up to the reality of the situation to prevent further atrocities.
- Ethnic cleansing doesn't always mean mass murder but can involve making certain groups uncomfortable or forcing them to leave.
- Emphasizes that history doesn't repeat but rhymes, indicating current dangerous trends.

### Quotes

- "It can happen here."
- "You have an obligation to speak over them."
- "Genocide. That's how it ends."
- "History doesn't repeat, but it rhymes."
- "The only thing that's going to change it is you."

### Oneliner

Beau stresses the importance of speaking out against dangerous rhetoric to prevent ethnic cleansing in the United States, warning that history's rhymes are already echoing.

### Audience

Activists, Advocates, Community Members

### On-the-ground actions from transcript

- Speak out against bigotry and dangerous rhetoric (implied)
- Educate others about the dangers of subtle forms of ethnic cleansing (implied)
- Advocate for inclusive and diverse communities (implied)
- Support organizations working to prevent hate crimes and discrimination (implied)

### Whats missing in summary

Beau's emotional delivery and depth of analysis can be best appreciated by watching the full transcript.

### Tags

#EthnicCleansing #SpeakingOut #Prevention #CommunityAction #History


## Transcript
Well, howdy there internet people it's Bo again
So tonight we're going to talk about how it can't happen here
You know, when I mentioned ethnic cleansing in a recent video, comment section, a lot
of people saying that, well, nobody's mentioning ethnic cleansing, nobody's talking about that.
That's crazy.
That can't happen here anyway.
It can.
There are people talking about it.
They don't use that term and they're more subtle in their rhetoric.
But that is what they're talking about.
Make no mistake about it.
It can happen here.
And for the last couple of years, you've had a lot of libertarians and liberals and certainly
conservatives.
Well that's their First Amendment right, we can't censor them.
First Amendment protects them from the government, not from you, not from you.
They do have a right, they do.
They can say all the bigoted idiotic things they want and they have a right.
And you have an obligation to speak over them.
You have an obligation to shut them down.
You have an obligation to speak up every time they bring up this nonsense.
Because it can happen here.
You know when people say that, it is either American exceptionalism at work or it's latent
racism.
Because when people say it can't happen here, what they really mean, that only happens in
countries with brown people.
When you mention genocide, people think of Africa, they think of Rwanda, or maybe they
think of Nazi Germany, but that was a long time ago and that was something different.
doesn't happen among white people. Oh yeah it does, yeah it does. There was a country, it was a tourist
destination on the Mediterranean in the 1990s, pretty well developed, had you know
those big floodlights that they have when they're doing construction, had
had those on the roads to the beaches because they were having to build it up because it
was really coming into its own.
And then the country broke up and those floodlights got drug behind a building so the mass rape
and murder could continue through the night.
When Yugoslavia broke up, Bosnia, right?
It can happen here, and it will.
It will, because of those people who refuse to see horror on horror's face.
And that is what we are dealing with.
they're dealing with for pure unadulterated evil, if you've taken that
stance that well they have a right and I have to sit here and be silent and maybe
even cheerlead for them a little bit to help secure their right, you put around
into the magazines that were used in that shooting. Because to them they don't
see it as you being ideologically consistent. They see it as you being too
much of a coward to admit that you agree with them. That's what they see. And for
For some, that's probably true, but for others, they've become those people that are too dumb
to take their own side in an argument.
It can happen here.
It will.
It has.
A lot of the mass shootings that have occurred in the United States, they have the same links.
this time it's harder for them to spin. They can't spin it off the way they have in other
cases. See, that movement of people, and I use the term people loosely, they're playing
on the stupidity and gullibility of the well-meaning liberal and the ideological libertarian and
the conservative who's really just in it so their party can be in power, even though
So their party and their president, as outlined in that manifesto, is a symbol of white identity.
They know what they're doing.
If you read that manifesto and you look at the tailored messages to the various groups
that are inside of it, as a student of propaganda, I will tell you that is a brilliant piece
of propaganda.
It really is.
And unless people wake up to what we're really dealing with and what we're really facing,
it's going to work, it's going to work.
And then you will find out, yeah, it can happen here.
It can.
Ethnic cleansing doesn't always take the form of mass murder.
And cleansing can just be removing them, making it uncomfortable for them to be there, making
them leave.
It can be a lot of things.
But it always ends one way.
Genocide.
That's how it ends.
single time and it is where we are headed
people wonder if Trump's gonna leave office peacefully if there will be a
peaceful transition yes that depends on whether or not he has support to stay in
it.
Whether or not he has support depends on whether or not you speak up.
This has to be made unacceptable.
History doesn't repeat, but it rhymes.
And I will tell you right now, we are in tune with a whole lot of very bad situations.
You know, people say, well, this is how it starts.
No, it's really not.
This is a couple steps in.
We are a couple steps in already.
And the only thing that's going to change it is you.
Because the government will end up being complicit in it.
It's how it works.
How it always works.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}