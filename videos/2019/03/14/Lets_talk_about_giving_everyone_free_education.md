---
title: Let's talk about giving everyone free education....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=rHuXHJPxeZs) |
| Published | 2019/03/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Critiques the current education system for prioritizing credentialing over actual education.
- Mentions a bribery scandal where rich individuals paid to get their kids into elite schools, questioning the purpose of education in that scenario.
- Talks about the university system and its lack of overhaul despite advancements in technology.
- Mentions computer-based training (CBTs) in the military and how it simplifies the learning process.
- Points out that the government has no interest in breaking down class barriers or providing free education.
- Encourages individuals or groups to set up alternative education platforms in states with lax regulations.
- Suggests creating online certifications and vocational training programs funded through ads.
- Emphasizes the need for individuals to take action in revolutionizing education since the government won't prioritize it.

### Quotes

- "Education's easy. Right now, you've got access to the entire base of humanity's knowledge."
- "It's not about education, it's about status, it's about class, it's about a way of dividing us normal folk from our betters."
- "The government could have done this a long time ago, this training, the CBTs and this technology has been around for a very long time."
- "If the government isn't gonna do it, we just can't have it then I guess you do it."
- "You want a revolution? Start one."

### Oneliner

Beau criticizes the current education system's focus on credentialing over true education, urging individuals to take action in revolutionizing education themselves.

### Audience

Education reformists

### On-the-ground actions from transcript

- Set up alternative education platforms in states with lax regulations on educational institutions (suggested)
- Create online certifications and vocational training programs funded through ads (suggested)

### Whats missing in summary

The full transcript provides in-depth insights into the flaws of the current education system, the government's lack of interest in revolutionizing education, and the call for individuals to take charge in creating change.

### Tags

#Education #Revolution #Credentialing #Government #AlternativeEducation


## Transcript
Well, howdy there Internet of Big Elizabeth again. So today we are finally going to talk
about how to stage a revolution in education. Put down your guns. Okay, why doesn't the
government give us free education? Subsidize it at least, something. Help us out. Throw
us a bone, they do. They do. They give us education. Education's easy. Right now, you've
got access to the entire base of humanity's knowledge. Education. It's not
what you want though. You're not talking about education, you're talking about
credentialing. You want a degree. See, people don't go to college to get an
education and right now there's a whole bunch of college professors out there
with their eyebrows raised. No, they go to get a degree. They go to get a degree.
it's not about education. The education you provide is a byproduct. What they want is to be
separated. It's a method of separating classes. It's not about education. I'm not saying you guys
don't educate though. Okay, you want proof, I would assume. This bribery scandal, okay?
A bunch of rich people, allegedly, have paid off all kinds of people to get their kids
into elite schools.
That doesn't even make sense.
If you really think about it, if this is about education, it doesn't make sense.
Because how are they going to be able to handle the course load?
Oh, they're going to have to cheat there too.
Then why are they trying to get them into elite schools?
Why wouldn't they just try to get them into a school that they could actually handle it?
Because it's not about education, it's about status, it's about class, it's about a way
of dividing us normal folk from our betters.
That's what it is about.
This system, the university system as it stands, has been around more or less since what, 1100
I think, maybe longer.
You would think the government would have overhauled it, come up with a little bit more
of an efficient way to create an educated population, if you believe that's in their
interest.
Or at least a better trained workforce, that seems more like what the government would
be interested in.
So why haven't they done it?
Why haven't they embraced new technology and come up with a method of teaching people
that doesn't require centuries-old method of doing it.
They did.
They did, they use it.
But you don't get to.
Okay, so there's a thing in the military called CBTs,
computer-based training.
The way this works is you take your ID card,
you walk up to a computer,
you slide your ID card in a CAC reader, an ID card reader.
You take your course, you pull your ID card out,
done. It's that simple. Military uses it for all kinds of things, not just minor
stuff. I know guys that took their SEER training via CBT, which is terrifying.
SEER, for those that don't know, that's what to do if you get captured. Okay,
pretty important stuff. Okay, so the technology exists. Why haven't they used
it because they have no interest in breaking down class barriers. They have
no interest in an educated population and DOD certainly has no interest in
everybody having free college. I mean that's their major recruitment tool. Why?
Because they can say, hey, we'll give you free college. It's a way out. It gets you
to another class. Government has no interest in this. None. You know, I know
know somebody out there is going right now they're like well people could cheat
if they did that they're cheating now they're cheating now it's about
credentialing that's what it's about so if the government isn't gonna do it we
just can't have it then I guess you do it one of you a group of you one of y'all
lives in a state with lax regulations on educational institutions. I'm certain, set
it up. You may not be able to grant degrees, you know, the state likes to
keep a tight hold on that power, maintaining those class structures, but
maybe you can grant two-year certifications, four-year certifications.
It's just a website, that's all it is, it's an advanced website.
And yeah, you wouldn't be able to do every major out there.
There are still some fields that need hands-on training.
I personally would not like it if my surgeon got his entire training via the computer.
But it would work for a whole lot of majors.
It would certainly work for all general classes, certainly work for career development, for
vocational training, and it would be cheap, cheap.
You could fund this entire thing via ads, it wouldn't be hard.
But the government isn't going to do it.
The government has no interest in creating an educated population, no interest in breaking
down class barriers, and I mean they don't have any interest in any of this.
It is not in their best interest that no government is ever going to give you the tools to understand
what they're doing.
You've got to give them yourself, you can provide them to other people, you want a revolution?
Start one.
This is something that is important to the entire nation.
The government could have done this a long time ago, this training, the CBTs and this
technology has been around for a very long time, and to set this up, it would probably
cost less than a year's worth of grants.
But they haven't done it, they haven't done it, you're going to have to do it.
It's going to be hard, a lot of work, a lot of work, but anything worthy requires a lot
of work.
Anyways, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}