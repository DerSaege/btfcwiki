---
title: Let's talk about the land of the free and the home of the brave....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=jz6EcnjDxu8) |
| Published | 2019/03/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Secretary of Homeland Security requested authority to deport unaccompanied minors crossing the border, transitioning from baby prisons to baby deportations.
- Five-month-olds have been in legal proceedings, requiring crayons for them to draw since they can't write a statement.
- Administration policies are making the journey more dangerous, like a real-life Hunger Games.
- Reuniting deported children with their families poses challenges as their families might be the reason they fled.
- There's a moral dilemma in sending children back to dangerous situations or taking more drastic measures.
- Calls for militarization of the border to protect against children reveal underlying fears of losing dominance.
- Fear of the browning of America drives some to extreme measures, like calling for military intervention against children.
- The pervasive fear of losing dominance leads to irrational calls for military protection from perceived threats.
- The irony of calling for military intervention against children in the "land of the free and home of the brave."
- Beau shares a powerful anecdote about a Palestinian woman facing off against an IDF trooper, questioning if a similar scene will unfold on American soil.
- Deporting children may prevent them from being sexually abused in custody, but it doesn't address the root problem.
- Beau urges America to confront its contradictions in claiming to be the land of the free while seeking military protection from toddlers.

### Quotes

- "We want to maintain the idea that we are still the land of the free and the home of the brave."
- "Your ignorance of American foreign policy and the impacts it has doesn't mean it's not really our problem, it just means you're too ignorant to know it."
- "Land of the free, home of the brave."
- "Fear of white people who are afraid to lose their place in this country."
- "We cannot continue to call for the U.S. military to protect us from toddlers."

### Oneliner

Secretary of Homeland Security seeks to deport unaccompanied minors, revealing deeper fears and contradictions in the "land of the free and home of the brave."

### Audience

Advocates for migrant rights

### On-the-ground actions from transcript

- Advocate for humane treatment and fair legal proceedings for unaccompanied minors (implied)
- Educate others on the impacts of American foreign policy on migration (implied)
- Challenge calls for militarization of borders and protection against children (implied)

### Whats missing in summary

The full transcript provides a detailed exploration of the moral and ethical implications of deporting unaccompanied minors and the underlying fears driving extreme responses.

### Tags

#Migration #HumanRights #Fear #Militarization #AmericanIdentity


## Transcript
Well, howdy there Internet people, it's Bo again.
So the Secretary of Homeland Security tweeted to Congress, because we rule by tweet now,
asking for authority to deport unaccompanied minors that cross the border.
So we're moving from baby prisons to baby deportations is what's happening.
This isn't new though.
We've had a five-month-old in proceedings recently.
are now making sure they have crayons in their briefcases when they go to meet
their client so the child can draw a picture of whatever horrible thing
happened to them that made them flee because they're too young to write a
statement. This is where we're at. And she's saying that the trek has become
more dangerous. Well, yeah, it has because of this administration's policies. Attempting
corral them into the most dangerous portions of the border to deter them from coming. You cannot
deter the drive for freedom or safety. It doesn't work that way. Yes, it's more dangerous. It's
becoming a real-life Hunger Games. And those kids that make it, although they're fighters,
they'd be great Americans if you gave them a chance, but we're going to deport them. How?
How? Who are we going to reunite them with?
Are you sure that their family isn't the reason they're fleeing?
Do you care?
Drive them down there, put them on a plane?
Why not?
Why even go to that trouble though?
Outcome will be the same.
DHS ordered a couple million rounds of ammo, right?
There's holes in the desert.
There's no moral difference between sending them back to die
doing it yourself, just a PR one.
And that's what it is.
We want to maintain the idea that we are still the land of the free and the home of the brave.
And then you look at the replies to that tweet.
Americans saying, well, this isn't our problem.
These children aren't our problem.
Your ignorance of American foreign policy and the impacts it has in these foreign countries
what it does on the ground doesn't mean that it's not really our problem, it just means
you're too ignorant to know it.
A lot of these cases are direct results of our foreign policy.
Wash our hands of it, right?
Because we're a pro-life country.
So you've got those and then you have people asking for the military, the National Guard
in the militarization of the border, Americans asking for a militarized border to protect
them from children.
Land of free, home of brave.
My question to these requests would be if the Department of Homeland Security cannot
secure the homeland without calling on the greatest military machine the world has ever
known, what good is it? Why do we even need that agency? Seems like it's completely ineffective
to me. If you can't deal with kids, how are you going to deal with real threats? Because
there is no threat. We all know that. It doesn't matter what propaganda you push. We know there's
no threat. The military has said there's no threat. There's no threat. It's just fear.
And it's that fear of the browning of America.
That's what it boils down to.
It's the fear of white people who are afraid to lose their place in this country.
And it's a self-fulfilling prophecy.
You know, the statistics that they're all pointing at now, I don't even know if they're
true because I don't really look up these stats anymore because they're never true,
is that white women are
more frequently
marrying outside of their race than they used to be.
That we're going to become a minority in our own country. I can actually believe that.
I can believe that white women
are marrying outside of their race more frequently
because all of these comments,
all these replies,
asking for
the military
to go face off against children,
they came from white people.
Generally speaking, cowardice is not an attractive quality in a man, doesn't really blow the
skirts up, and that's what this is.
You need the U.S. military to protect you from toddlers.
Land of the free, home of the brave, right?
One of the most interesting chats I've ever had was I had the privilege of speaking to
ahead to Mimi. She's a Palestinian. She was a kid at the time,
young woman today, and
she was the subject of a very poignant photo
that came out of that conflict. There she is, she's a child,
standing there with this look of utter defiance
on her face. And there's an IDF trooper
pointing an M203 at her chest. That's a grenade launcher
on the bottom of an M16. That's the photo.
And regardless of your opinion of that conflict, that became an icon of it.
I have to wonder how long we have until there's a photo of an American serviceman pointing
an automatic rifle at a child on our own border because our citizens have become terrified
of brown children.
Land of the free, home of the brave.
There is one upside to this though.
I guess if they deport them, they are less likely to be sexually abused in custody.
That problem hasn't been solved, has it?
America needs to take a long hard look at itself.
We cannot continue to say that we are the land of the free and the home of the brave
and call for the U.S. military to protect us from toddlers.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}