---
title: Let's talk about the fingerprints of history....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=89hi45P4Z6k) |
| Published | 2019/03/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- History books are created from the fingerprints of history, like letters, photographs, and government records.
- Historians shape history books to appeal to readers, often judging the past by today's standards.
- People of the future will judge us based on the standards of tomorrow, wondering why we didn't act on issues like the environment or political systems.
- Digital records today will shape how future generations view us, similar to how Thomas Jefferson is viewed for his actions and inactions.
- Society today is often busy but accomplishing little, confusing motion with progress.
- Our actions today will leave behind fingerprints for future generations to judge us by.

### Quotes

- "They're going to wonder why we didn't act for the environment."
- "We spend a lot of time being entertained by stuff that is mindless."
- "We know that a lot of the stuff going on around us is wrong. But we're not doing much to solve it."
- "Our fingerprints will be there for them to look at and the society we leave them."
- "Society today is often busy but accomplishing little, confusing motion with progress."

### Oneliner

Beau examines how history books are shaped by the fingerprints of history and how future generations will judge us based on our actions and inactions today.

### Audience

History enthusiasts, future generations

### On-the-ground actions from transcript

- Document your actions for future generations to learn from (implied)
- Actively work towards solving societal issues (implied)

### Whats missing in summary

The full transcript provides a reflective look at how history is shaped and how our present actions will be judged by future generations.


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're going to talk about the fingerprints of history.
When you read a history book today, it's a story.
The people that put it together, they try to turn it into some cohesive theme, even
has plot points.
They're normally people that are used to describe everybody that existed at the time.
And those plot points are put together and they're distilled from the information they
have.
The fingerprints that were left behind, letters, photographs, memos, government records, receipts,
whatever they can get their hands on.
That's how history books are created.
And then, they're shaped.
So they appeal to the reader somewhat, you know.
I've got a collection of history books from all over the world, and it's interesting to
see the different viewpoints on different historical events. It is definitely entertaining
because each demographic of today, they judge the past by today's standards. A good example
of that is Thomas Jefferson. Today, he's a slaver. That's what he's known as. He's
that guy that didn't free his own slaves when he died. He was also one of the first abolitionists,
but he was very flawed. He didn't put his money where his mouth was, literally. He said
slavery was a hideous blot but he didn't act on it. People wonder why. Future historians
are going to have a lot more fingerprints assuming that the digital record survives,
the one you're creating right now. But that digital record goes way beyond just your Facebook
post or Twitter post or whatever. All of your purchases, they're all tracked. There's a
record of everything. And if that survives, there's not going to be much guesswork in
It'll all that data will be cataloged and it'll be refined.
Rather than a thick history book that weighs a ton, probably a slideshow type thing.
Your history book was probably broken up into 10 year periods during more modern history
in 20 year periods as you get further back and then 50, 100, it gets broken up based
on the amount of information.
They'll be able to do it year by year, month by month if they want it.
The thing is, the people of the future, they're going to wonder the same thing about us.
They're going to look back and they're going to judge us by the standards of tomorrow.
They're going to wonder why we didn't act for the environment.
They're going to wonder why our political system is the way it is, which is going to
directly relate to why their political system is the way it is.
You'll probably wonder why we spent so much time being entertained by nothing.
We spent a lot of time being entertained by stuff that is mindless.
I would hope that the society of the future looks at that as a bad thing.
May not, I don't know.
But I definitely think that we as a people are going to be judged the same way Thomas
Jefferson is.
Because we know that a lot of the stuff going on around us is wrong.
But we're not doing much to solve it.
It's like any other fingerprint.
That digital fingerprint, it's placing you here.
When future generations look back on it, it could be evidence used to prosecute you.
Or it could be evidence used to exonerate you.
going to wonder why we didn't do a lot of things because the information is out here.
Just like back then, Jefferson knew slavery was wrong and yeah, he tried in his own way
to solve some of it, but he probably could have done a whole lot more.
And that's how we're going to be viewed.
Because we spend a lot of time, a lot of time, confusing motion with accomplishment.
busy all day and getting absolutely nothing done. And that's where we're at.
The society of tomorrow will judge us and it's going to be the society that's
built on whatever we leave behind. Our fingerprints will be there for them to
look at and the society we leave them. Well, it'll be very evident to them.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}