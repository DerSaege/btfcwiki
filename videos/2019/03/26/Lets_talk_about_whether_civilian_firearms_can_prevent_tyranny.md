---
title: Let's talk about whether civilian firearms can prevent tyranny....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=dK2fH7JNRhM) |
| Published | 2019/03/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the idea that Jews having guns during the Holocaust could have prevented it.
- Explaining the importance of having the right skills and knowledge along with guns.
- Mentioning that guns in themselves do not prevent tyranny; it's about how they are used.
- Stating that untrained individuals are ineffective in combat and are likely to perish.
- Pointing out the discrepancy between people's romanticized views of insurgency and the harsh reality.
- Describing the strategy of insurgency to make the government overreact and clamp down on civil liberties.
- Emphasizing that armed revolution is a last resort and a reflection of societal failure.
- Concluding with a reminder of the gravity and horror of armed revolutions.

### Quotes

- "You don't fight a tank. You don't fight a drone. You go find the tank driver's family or the drone operator's family."
- "Advocating for armed revolution doesn't make you tough. It's an indictment of it."
- "It's too hard to teach that violence is just easier, and it is."
- "At some point in the future, we may need another armed revolution. But if they do, it's because we failed them."

### Oneliner

Beau addresses the limitations and realities of armed revolutions, underscoring the need for societal change and the potential consequences of violence.

### Audience

Society members

### On-the-ground actions from transcript

- Educate others on the complex realities and consequences of armed revolutions (suggested).
- Advocate for peaceful societal change through education and awareness (implied).

### Whats missing in summary

The full transcript provides a deep dive into the complex dynamics of armed revolutions, urging for critical thinking and societal improvements to avoid resorting to violence in the future.

### Tags

#ArmedRevolution #Insurgency #SocietalChange #GovernmentTyranny #Holocaust


## Transcript
Hey internet people, it's Cari.
So I've been talking with someone suggesting that Jews having guns during the Holocaust
could have prevented it.
Bo, thoughts?
Well, guns aren't magic, they're tools.
Here.
My pocket survival kit.
I can survive in the woods for a week or more with this thing.
And I can go survive in the woods for a week.
Yeah, I don't know how to use that.
Well, survival kits aren't magic either.
Okay, it's not guns themselves that could have prevented the Holocaust, but guns in
hands of the right people. So does this mean private gun ownership is worthless
in stopping government tyranny? Well, if the only thing the gun owner knows how
to do with his gun is poke holes in paper at the range, no, it's not gonna do
any good at all. It's worthless. If the gun owner knows a little bit about
unconventional warfare, insurgency, cover, concealment, how to move, those kinds of
things, then yeah, it can be used to preserve liberty. Most don't. Not really.
And untrained people, they do one thing really, really well in combat, die.
How is a rifle going to combat a tank or a drone?
Well, that's the rub.
That is the rub right there.
People who typically have the attitude of, well, my rifle's going to protect us and serve
freedom and this is how we're going to keep the republic.
They have a very romanticized version of what insurgency and revolution looks like.
They want to be the hero in real life.
That's not how any of this works.
You don't fight a tank.
You don't fight a drone.
You go find the tank driver's family or the drone operator's family.
That's how it really works.
That's not war.
That's murder.
War is murder.
Don't you think that kind of terrorist attack would cause the government to take more drastic
measures?
Well, that's the point of insurgency.
The insurgents, their whole job is to make the government overreact, is to make the government
respond in a way that is too much.
When they start drone strikes and they kill a bunch of civilians, the family members of
those civilians, well they take up arms.
When they clamp down on civil liberties, makes the people of the area feel like they're living
under tyranny, then they see the opposition government for what it is.
This is why terrorist activities have grown since the war on terror started, including
in non-conflict countries.
It seems like insurgencies put the government in a no-win situation.
It is.
And so are the civilians.
They're in a no-win situation, too.
The idea of armed revolution is not one to be tossed out half-heartedly.
It's a horrible, horrible thing.
Armed revolutions have been proven necessary in the past, and they may be proven necessary
in the future, if we fail to create a fair and just society, they'll probably be necessary.
Advocating for armed revolution doesn't make you tough.
It doesn't make you a proponent of real change.
It's not a display of your character.
It's an indictment of it.
What you're really saying is that reason and thought are just too difficult.
It's too hard to teach.
that violence is just easier, and it is. At some point in the future, we may need another
armed revolution. Our kids may need it. But if they do, it's because we failed them.
Anyways, just follow up.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}