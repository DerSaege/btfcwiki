---
title: Let's talk about prohibition....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xQTVRzcnIIU) |
| Published | 2019/03/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Prohibition failed because people wanted alcohol, creating a market for it despite laws.
- To address issues like alcohol or abortion, focus on education, changing culture, and treatment.
- The drug war is ineffective; there is a market for drugs that education and treatment can address.
- Manufacturing firearms is easier than commonly thought, with designs like the Luddy being made at home.
- Countries like the UK have found fully automatic weapons made at home despite stricter gun laws.
- Addressing gun violence requires education on firearm use, mental health treatment, and changing toxic masculinity.
- Toxic masculinity is a key factor in gun violence and needs to be addressed through cultural change.
- Focus on treating the root causes of crime like poverty and lack of options rather than banning firearms.
- Prohibition of firearms will not eliminate the market but drive it underground, leading to illegal firearms trade.
- The illegal firearms market thrives due to supply and demand, showing prohibition's ineffectiveness.

### Quotes

- "Prohibition doesn't work."
- "To address issues like alcohol or abortion, focus on education, changing culture, and treatment."
- "The drug war is ineffective; there is a market for drugs that education and treatment can address."
- "Focus on treating the root causes of crime like poverty and lack of options rather than banning firearms."
- "The illegal firearms market thrives due to supply and demand, showing prohibition's ineffectiveness."

### Oneliner

Beau explains how prohibition fails due to demand, advocating for education, culture change, and treatment to address societal issues like gun violence and drug use.

### Audience

Advocates for social change

### On-the-ground actions from transcript

- Educate others on the ineffectiveness of prohibition through real-life examples (exemplified)
- Support mental health initiatives to address underlying issues of gun violence (implied)
- Challenge toxic masculinity by promoting healthier views of masculinity in your community (suggested)

### Whats missing in summary

In-depth analysis of the impact of addressing root causes like poverty and mental health on reducing crime rates.

### Tags

#Prohibition #GunViolence #ToxicMasculinity #Education #SocialChange


## Transcript
Well, howdy there, Internet people, it's Bo again.
So tonight we're going to talk about prohibition.
It didn't work, why?
People wanted alcohol, it's that simple.
There was a market for it.
Didn't matter what law the government passed, didn't matter what the government said, people
started making it.
Eventually the law was rescinded.
How do we address some of the issues associated with alcohol?
Education, a change in culture and treatment.
Pretty simple formula.
Abortion.
People want to prohibit abortion.
Doesn't matter what your moral stance or belief on this is, you know there's a market for
it.
Somebody's going to provide that service.
it just makes it unsafe. How do you actually deal with it? Education, a change
in culture, and treatment. You know, birth control, condoms, Plan B, explaining where
babies come from. It's no longer the swinging 60s. These things. That's what
actually cuts down on the numbers of abortions. The drug war. I don't know what
decade we're in now but it is safe to say the plants have won. There's a market
for it. Doesn't matter how you feel about drugs. Some people want them. They're
gonna get them. How are we actually gonna deal with it? Education, a change in
culture, and treatment. It's a pretty simple formula. It's having this
conversation with somebody today who is rabidly anti-gun. I asked what the
difference was and he said because you can't grow a gun and then it clicked
and I promise this will be the last video I make on this topic for a while
but anyway it clicked it all came into focus so I pulled out my phone and I
started texting other anti-gun people I know asking them how hard they think it
is to manufacture a firearm, and you need a lot of equipment.
It's the consensus.
100% of my massive sample size of four
believed that it was extremely hard to manufacture a gun.
So what I want you to do is Google,
don't actually go to any of the search results
unless you're familiar with the laws in your country,
LUTY, L-U-T-Y, firearm.
That's it.
What you will see in the search results
is a bunch of schematics for different firearms.
Now, when people save the Luddy, they're
talking about that first one.
They're talking about the fully automatic submachine gun.
The interesting thing about the Luddy
is that it was designed to be made, manufactured at home,
with no training, using only things bought
at your local hardware store.
completely untraceable stuff, seamless tubing and stuff like that.
It's not hard.
It is certainly easier to manufacture a Luddy than it is to make meth.
Every redneck in the country knows how to make one of those things.
If they don't know how to make that, they know how to make a Sten.
Same basic idea, just one looks like a laser gun.
And these are fully automatic weapons.
Now, Luddy designed a bunch of stuff, but the Luddy is that submachine gun.
And when I brought this up, he said, well, why didn't they have this problem in the UK?
Luddy was from Leeds, he lived in the UK, the Luddy was designed in the UK.
That's why he had the criteria that he did when he designed it.
They have been found.
Weapons of that design have been found in Australia.
And those are countries that don't have America's idiotic gun culture.
There's a market for it.
People are going to get them.
How do we actually address the problem?
Same thing.
Education, explaining that a firearm is a tool, it's a tool used to kill.
That's it.
It's not a toy.
Stop playing with it.
deserves. You don't walk around playing with your chainsaw. Treatment. We have a
massive mental health issue in this country that is ignored. It's completely
ignored and then we wonder why people lose it and start killing people. They
have no coping skills. They don't understand the issues they're facing.
They can't figure out why they feel the way they do because most times when they
admit that they do have a problem, they get a prescription drug, they don't
actually get any help with the issue. And then a change in culture, and that's the
big one. That is the big one. We have got to adjust the view of masculinity. Toxic
masculinity is literally killing us. You know, it's that idea that the gun
makes the man. It doesn't. And we have to find a way to address that. And you can
do it through humor, you can do it any way you want, you know? You want to be funny?
Grab your favorite power tool, make a photo of you, post it up like Rambo with it. And
anytime you see somebody post one of their Rambo photos with their plate carrier and
their AR, post yours underneath it. Remind people that masculinity is not all destructive,
There are creative aspects to it.
We need to bring that back.
It's a bigger problem than I think a lot of folks realize.
And I think once we do that, we've hit the turning point.
And then, once that's out of the way, we can start focusing on the other treatments that
we need to do.
We need to focus on the treatments that will stem crime.
the causes of crime, you know, poverty, hopelessness, a lack of options. This is why crime occurs.
Banning firearms will have the same result as every other form of prohibition in this
country. Because at the end of the day, simply possessing a firearm is a victimless crime.
The market is still going to be there and people are going to do whatever it takes.
They can build it with the stuff from the hardware store, they can buy one illegally,
and that's a perfect example.
The National Firearms Act effectively banned fully automatic weapons.
You can still get one if you jump through the hoops, but an M16 with the military version
of an AR-15, it's about 20 grand.
Same thing for an AK.
You can pick up an illegal AK for $2,000.
So what does that tell you?
There are more illegal ones out there, supply and demand, than there are illegal.
Prohibition doesn't work.
The market will find a way.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}