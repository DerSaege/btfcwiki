# All videos from March, 2019
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2019-03-30: Let's talk about the land of the free and the home of the brave.... (<a href="https://youtube.com/watch?v=jz6EcnjDxu8">watch</a> || <a href="/videos/2019/03/30/Lets_talk_about_the_land_of_the_free_and_the_home_of_the_brave">transcript &amp; editable summary</a>)

Secretary of Homeland Security seeks to deport unaccompanied minors, revealing deeper fears and contradictions in the "land of the free and home of the brave."

</summary>

"We want to maintain the idea that we are still the land of the free and the home of the brave."
"Your ignorance of American foreign policy and the impacts it has doesn't mean it's not really our problem, it just means you're too ignorant to know it."
"Land of the free, home of the brave."
"Fear of white people who are afraid to lose their place in this country."
"We cannot continue to call for the U.S. military to protect us from toddlers."

### AI summary (High error rate! Edit errors on video page)

Secretary of Homeland Security requested authority to deport unaccompanied minors crossing the border, transitioning from baby prisons to baby deportations.
Five-month-olds have been in legal proceedings, requiring crayons for them to draw since they can't write a statement.
Administration policies are making the journey more dangerous, like a real-life Hunger Games.
Reuniting deported children with their families poses challenges as their families might be the reason they fled.
There's a moral dilemma in sending children back to dangerous situations or taking more drastic measures.
Calls for militarization of the border to protect against children reveal underlying fears of losing dominance.
Fear of the browning of America drives some to extreme measures, like calling for military intervention against children.
The pervasive fear of losing dominance leads to irrational calls for military protection from perceived threats.
The irony of calling for military intervention against children in the "land of the free and home of the brave."
Beau shares a powerful anecdote about a Palestinian woman facing off against an IDF trooper, questioning if a similar scene will unfold on American soil.
Deporting children may prevent them from being sexually abused in custody, but it doesn't address the root problem.
Beau urges America to confront its contradictions in claiming to be the land of the free while seeking military protection from toddlers.

Actions:

for advocates for migrant rights,
Advocate for humane treatment and fair legal proceedings for unaccompanied minors (implied)
Educate others on the impacts of American foreign policy on migration (implied)
Challenge calls for militarization of borders and protection against children (implied)
</details>
<details>
<summary>
2019-03-29: Let's talk about Cardi B.... (<a href="https://youtube.com/watch?v=dcXfi5amUyE">watch</a> || <a href="/videos/2019/03/29/Lets_talk_about_Cardi_B">transcript &amp; editable summary</a>)

Beau questions victims, doubts credibility, and warns against harmful comparisons in the Cardi B controversy.

</summary>

"I guess she kind of admitted that when she was younger, didn't have a lot of options or whatever, she enticed men to come back with her because they were going to pay her for sex, and then she drugged them and robbed them."
"Not adding up to me at all."
"You're telling me that she had to drug you? You? You gotta be kidding me."
"They're property. They're not people."
"Two things aren't the same."

### AI summary (High error rate! Edit errors on video page)

Addresses the controversy surrounding Cardi B and her past actions.
Questions the victims of Cardi B's actions, implying they are partly to blame.
Expresses disbelief that Cardi B needed to drug anyone, given her appearance.
Raises doubts about the credibility of the victims and suggests they may have a history of blacking out.
Criticizes those who compare Cardi B's actions to Bill Cosby's, arguing that it dehumanizes women.
Warns against damaging relationships by making such comparisons.
Urges people to stop equating drugging and robbing with drugging and raping.
Ends with a message to have a good day.

Actions:

for onlookers, individuals, allies,
Confront harmful comparisons in your social circles (suggested)
Challenge victim-blaming narratives in discussions (implied)
</details>
<details>
<summary>
2019-03-28: Let's talk about prohibition.... (<a href="https://youtube.com/watch?v=xQTVRzcnIIU">watch</a> || <a href="/videos/2019/03/28/Lets_talk_about_prohibition">transcript &amp; editable summary</a>)

Beau explains how prohibition fails due to demand, advocating for education, culture change, and treatment to address societal issues like gun violence and drug use.

</summary>

"Prohibition doesn't work."
"To address issues like alcohol or abortion, focus on education, changing culture, and treatment."
"The drug war is ineffective; there is a market for drugs that education and treatment can address."
"Focus on treating the root causes of crime like poverty and lack of options rather than banning firearms."
"The illegal firearms market thrives due to supply and demand, showing prohibition's ineffectiveness."

### AI summary (High error rate! Edit errors on video page)

Prohibition failed because people wanted alcohol, creating a market for it despite laws.
To address issues like alcohol or abortion, focus on education, changing culture, and treatment.
The drug war is ineffective; there is a market for drugs that education and treatment can address.
Manufacturing firearms is easier than commonly thought, with designs like the Luddy being made at home.
Countries like the UK have found fully automatic weapons made at home despite stricter gun laws.
Addressing gun violence requires education on firearm use, mental health treatment, and changing toxic masculinity.
Toxic masculinity is a key factor in gun violence and needs to be addressed through cultural change.
Focus on treating the root causes of crime like poverty and lack of options rather than banning firearms.
Prohibition of firearms will not eliminate the market but drive it underground, leading to illegal firearms trade.
The illegal firearms market thrives due to supply and demand, showing prohibition's ineffectiveness.

Actions:

for advocates for social change,
Educate others on the ineffectiveness of prohibition through real-life examples (exemplified)
Support mental health initiatives to address underlying issues of gun violence (implied)
Challenge toxic masculinity by promoting healthier views of masculinity in your community (suggested)
</details>
<details>
<summary>
2019-03-26: Let's talk about whether civilian firearms can prevent tyranny.... (<a href="https://youtube.com/watch?v=dK2fH7JNRhM">watch</a> || <a href="/videos/2019/03/26/Lets_talk_about_whether_civilian_firearms_can_prevent_tyranny">transcript &amp; editable summary</a>)

Beau addresses the limitations and realities of armed revolutions, underscoring the need for societal change and the potential consequences of violence.

</summary>

"You don't fight a tank. You don't fight a drone. You go find the tank driver's family or the drone operator's family."
"Advocating for armed revolution doesn't make you tough. It's an indictment of it."
"It's too hard to teach that violence is just easier, and it is."
"At some point in the future, we may need another armed revolution. But if they do, it's because we failed them."

### AI summary (High error rate! Edit errors on video page)

Addressing the idea that Jews having guns during the Holocaust could have prevented it.
Explaining the importance of having the right skills and knowledge along with guns.
Mentioning that guns in themselves do not prevent tyranny; it's about how they are used.
Stating that untrained individuals are ineffective in combat and are likely to perish.
Pointing out the discrepancy between people's romanticized views of insurgency and the harsh reality.
Describing the strategy of insurgency to make the government overreact and clamp down on civil liberties.
Emphasizing that armed revolution is a last resort and a reflection of societal failure.
Concluding with a reminder of the gravity and horror of armed revolutions.

Actions:

for society members,
Educate others on the complex realities and consequences of armed revolutions (suggested).
Advocate for peaceful societal change through education and awareness (implied).
</details>
<details>
<summary>
2019-03-25: Let's talk about examples of masculinity.... (<a href="https://youtube.com/watch?v=UHZ9VYJARL4">watch</a> || <a href="/videos/2019/03/25/Lets_talk_about_examples_of_masculinity">transcript &amp; editable summary</a>)

Young men question masculinity without a checklist, Beau shares diverse role models, defining masculinity as a journey of self-improvement and personal definition.

</summary>

"Masculinity isn't a trait, it's a journey."
"Real masculinity likes a challenge."
"Masculinity is something you're going to define yourself."

### AI summary (High error rate! Edit errors on video page)

Young men are questioning masculinity and its definition in the absence of a checklist or skill set.
Masculinity is individualized and not determined by others.
Beau shares three masculine role models: Teddy Roosevelt, Sheriff Andy Taylor, and John F. Kennedy, each showcasing different aspects of masculinity.
Teddy Roosevelt exemplified American masculinity through courage, individualism, and advocacy for the downtrodden.
Sheriff Andy Taylor humanized conflict resolution and was active in his community without carrying a gun.
John F. Kennedy demonstrated principles, uplifted the oppressed, and had a vision for positive change.
Beau defines masculinity as not using aggression to subjugate others but to uplift and improve oneself.
Real masculinity embraces challenges and focuses on personal growth rather than dominating others.
Masculinity is a journey of self-improvement and defining what matters to oneself.
It ultimately boils down to an individual's definition, irrespective of others' opinions.

Actions:

for men, individuals exploring masculinity,
Define masculinity for yourself (exemplified)
Focus on self-improvement and uplifting others (implied)
Embrace challenges and strive for personal growth (implied)
</details>
<details>
<summary>
2019-03-24: Let's talk about the assault weapons ban.... (<a href="https://youtube.com/watch?v=sIIK--yFdoA">watch</a> || <a href="/videos/2019/03/24/Lets_talk_about_the_assault_weapons_ban">transcript &amp; editable summary</a>)

Beau questions the effectiveness of an assault weapons ban, pointing out the deeper issues surrounding gun violence and individual responsibility.

</summary>

"It's not going to do any good because in the firearms vocabulary, there's no such thing as an assault weapon."
"Glorify violence for everything, every little infraction, become something worthy of death."
"It's a tool with one purpose, to kill. That's what a firearm is for."

### AI summary (High error rate! Edit errors on video page)

Lists multiple school shootings under the assault weapons ban to illustrate the stakes.
Points out the ineffectiveness of the assault weapons ban, citing that it didn't work previously.
Argues that there's no such thing as an "assault weapon" in firearms vocabulary, labeling it a political term.
Mentions that popular rifles like the AR-15 will still be present even with an assault weapons ban.
Raises concerns that banning certain weapons could lead to the use of more powerful firearms like the .30-06.
Notes that the U.S. has more guns than people, making it a unique case in terms of gun ownership.
Shifts the focus from firearms to the individuals using them, discussing the importance of secure weapon storage.
Criticizes the glorification of violence and the lack of emphasis on conflict resolution and coping skills.
Shares a personal story of learning to shoot at a young age, stressing the importance of understanding firearms as tools for a specific purpose.
Concludes with a thought on the consequences of actions and presentations in society.

Actions:

for gun policy advocates,
Secure your firearms to prevent unauthorized access (implied)
Teach conflict resolution and coping skills alongside firearm safety (implied)
</details>
<details>
<summary>
2019-03-23: Let's talk about the fingerprints of history.... (<a href="https://youtube.com/watch?v=89hi45P4Z6k">watch</a> || <a href="/videos/2019/03/23/Lets_talk_about_the_fingerprints_of_history">transcript &amp; editable summary</a>)

Beau examines how history books are shaped by the fingerprints of history and how future generations will judge us based on our actions and inactions today.

</summary>

"They're going to wonder why we didn't act for the environment."
"We spend a lot of time being entertained by stuff that is mindless."
"We know that a lot of the stuff going on around us is wrong. But we're not doing much to solve it."
"Our fingerprints will be there for them to look at and the society we leave them."
"Society today is often busy but accomplishing little, confusing motion with progress."

### AI summary (High error rate! Edit errors on video page)

History books are created from the fingerprints of history, like letters, photographs, and government records.
Historians shape history books to appeal to readers, often judging the past by today's standards.
People of the future will judge us based on the standards of tomorrow, wondering why we didn't act on issues like the environment or political systems.
Digital records today will shape how future generations view us, similar to how Thomas Jefferson is viewed for his actions and inactions.
Society today is often busy but accomplishing little, confusing motion with progress.
Our actions today will leave behind fingerprints for future generations to judge us by.

Actions:

for history enthusiasts, future generations,
Document your actions for future generations to learn from (implied)
Actively work towards solving societal issues (implied)
</details>
<details>
<summary>
2019-03-22: Let's talk about the training incident in Indiana..... (<a href="https://youtube.com/watch?v=eeAvhhppe6I">watch</a> || <a href="/videos/2019/03/22/Lets_talk_about_the_training_incident_in_Indiana">transcript &amp; editable summary</a>)

Beau criticizes the mock execution of teachers during an active shooter drill in Indiana, questioning the effectiveness and ethics of such training and advocating for a more proactive approach to school safety.

</summary>

"I wonder where the school shooters get the idea from us, from us, because violence is easy."
"Just kill them, two in the chest, one in the head. Blood makes the grass grow green."
"If we keep this posture, we can't be surprised when there are more small caskets because of an event at a school."

### AI summary (High error rate! Edit errors on video page)

Explains an incident in Indiana where teachers were mock executed during an active shooter drill.
Clarifies that the incident was not part of the ALICE training program and expresses doubt that it was authorized by them.
Criticizes the lack of effective training programs for active shooter situations and the mentality needed for such training.
Describes how teachers were taken into a room, lined up against a wall, and shot in the back with a high-powered airsoft gun as part of the drill.
Questions the effectiveness of using fear and pain in training teachers to respond to active shooter situations.
Raises concerns about whether teachers were aware they could fight back during the drill.
Criticizes the reactive nature of the training and suggests a more proactive approach involving psychologists to identify warning signs.
Condemns the use of violence as a solution and points out the potential impact on students' mental health and well-being.
Urges for a reevaluation of current training methods for teachers in dealing with school shootings.

Actions:

for teachers, educators, school administrators,
Advocate for comprehensive and trauma-informed training programs for teachers (implied)
Support initiatives that focus on proactive measures such as identifying warning signs of potential violence (implied)
</details>
<details>
<summary>
2019-03-22: Let's talk about a critique of the reparations video.... (<a href="https://youtube.com/watch?v=lmJY61rbsqI">watch</a> || <a href="/videos/2019/03/22/Lets_talk_about_a_critique_of_the_reparations_video">transcript &amp; editable summary</a>)

Beau engages in a thoughtful response to a reparations video, proposing a refundable tax credit over tax exemption as a more feasible and beneficial approach to reparations, fostering unexpected allies through tax credits.

</summary>

"A Nazi wants to talk, let's talk."
"Rather than tax exemption, what about a tax credit, a refundable tax credit more importantly?"
"Taxation is theft, man."
"You will find people that would march against you in the street supporting this just based on the idea that it's cutting down on government extortion."
"This is something that if with the right amount of organizing could be done pretty quickly."

### AI summary (High error rate! Edit errors on video page)

Beau was watching YouTube videos while feeling under the weather and stumbled upon a video critiquing reparations, prompting him to respond.
The video he watched critiqued the reparations proposal, focusing on the $100 billion amount mentioned, which Beau believes is not sufficient given the true value of slavery.
Beau appreciates the video's approach of sparking a constructive conversation and wants to contribute to the discourse.
He suggests a tweak to the proposed tax exemption for reparations, advocating for a refundable tax credit instead to benefit those at the bottom economically.
Beau sees tax credits as more politically feasible than direct dollar reparations funds, as it could garner support from unexpected allies who oppose taxes.
Implementing tax credits for reparations is seen as more achievable and effective compared to other reparations plans, making it a practical and timely solution.
Beau values the importance of turning criticisms into productive dialogues, which is the essence of his channel.
He concludes by encouraging further reflection and engagement on the topic of reparations.

Actions:

for tax reform advocates,
Advocate for implementing refundable tax credits for reparations (suggested)
Organize grassroots movements around the idea of tax credits for reparations (implied)
</details>
<details>
<summary>
2019-03-19: Let's talk about why people believe conspiracy theories (and debunk one).... (<a href="https://youtube.com/watch?v=86d-fgSK_oo">watch</a> || <a href="/videos/2019/03/19/Lets_talk_about_why_people_believe_conspiracy_theories_and_debunk_one">transcript &amp; editable summary</a>)

Beau debunks a conspiracy theory surrounding the New Zealand shooting, illustrating why people grasp onto such narratives for comfort and control in a chaotic world.

</summary>

"If you don't believe that, the alternative is that somebody really did walk in and murder 50 people."
"There is no solace believing that the CIA controls the world. It's not really true."
"The world is chaotic. Bad things happen. Murders happen. That's what it's about."
"The world's a scary place, and without somebody pulling the strings, it's downright terrifying."
"It's either they don't want to admit that they've elected this guy, or they need something to believe in."

### AI summary (High error rate! Edit errors on video page)

Expresses his interest in conspiracy theories as great thought exercises.
Refutes the conspiracy theory about the New Zealand shooting being staged by the CIA.
Points out the flaws in the theory, such as the CIA's unlikely involvement and the lack of evidence.
Explains the physics of gunshot wounds, debunking misconceptions about bullet impact.
Mentions the lack of blood in the conspiracy theory and questions its validity.
Conducts a demonstration to debunk claims about bullet impact on a windshield.
Debunks the theory using evidence from the video, like the presence of a magazine on the floor.
Criticizes the theory for its lack of credibility and reliance on false evidence.
Explains why people may believe in conspiracy theories for comfort and a sense of control.
Links the belief in the conspiracy theory to political motivations, particularly among Donald Trump supporters.

Actions:

for skeptics, truth-seekers,
Fact-check conspiracy theories (implied)
Challenge misinformation online (implied)
</details>
<details>
<summary>
2019-03-18: Let's talk about pirates and emperors.... (<a href="https://youtube.com/watch?v=JonuekkDeGM">watch</a> || <a href="/videos/2019/03/18/Lets_talk_about_pirates_and_emperors">transcript &amp; editable summary</a>)

Beau addresses the blurred lines between legality and morality, drawing parallels between different actions and urging a critical examination of power dynamics.

</summary>

"The technological advance doesn't somehow make it more moral. Just makes it legal."
"Everything the Taliban did, well that was legal under their laws."
"There's no difference in real life. It's the same thing."
"Size doesn't matter. At least not in this case."
"It's power."

### AI summary (High error rate! Edit errors on video page)

Addresses questions about terrorism and non-state actors.
Talks about a terrorist action committed by the government.
Emphasizes the importance of definitions in understanding propaganda.
References St. Augustine's story about Alexander the Great and a pirate to make a point about morality and legality.
Compares street gangs collecting protection money to taxation.
Draws parallels between a suicide bomber and a drone operator in terms of morality.
Argues that legality does not equate to morality.
Mentions historical instances where atrocities were legal under respective laws.
Stresses the role of power in determining what is legal and not morality or social consensus.
Encourages watching a related video to further understand the topic.

Actions:

for ethical thinkers, justice advocates.,
Watch the suggested video to deepen understanding (suggested).
</details>
<details>
<summary>
2019-03-17: Let's talk about terrorism and propaganda.... (<a href="https://youtube.com/watch?v=CMRuMaRUq8Q">watch</a> || <a href="/videos/2019/03/17/Lets_talk_about_terrorism_and_propaganda">transcript &amp; editable summary</a>)

Beau explains terrorism as a strategy to provoke overreactions, urges keeping terrorist groups unrelatable to combat radicalization and violence.

</summary>

"Hang them from lamp posts. Nobody cares, they're fascists."
"You've got to keep that wall up. It's the most important thing you, as an average citizen, can do to combat terrorism."
"It's extremely important that you do this. Because if this group becomes relatable to the average person, people start to radicalize."
"The founding fathers were terrorists, textbook."
"Point out the flaws in the ideology and make sure they stay unrelatable."

### AI summary (High error rate! Edit errors on video page)

Defines terrorism as a strategy to provoke an overreaction from the establishment.
Explains how terrorists aim to make themselves relatable to gain support.
Talks about fascists being hard to offend or make relatable.
Analyzes a manifesto's brilliance in making the author relatable through nods to different groups.
Mentions the ideological journey from left to right and doubts its authenticity.
Points out the existence of a libertarian to fascist pipeline.
Urges libertarians to address and clean out radical elements within their group.
Emphasizes the importance of not getting defensive when discussing these issues.
Stresses the need to keep terrorist groups marginalized and unrelatable.
Connects the importance of keeping terrorists unrelatable to preventing radicalization and violence.
Mentions the founding fathers as historical examples of terrorists.
Suggests that pointing out flaws in terrorist ideologies and keeping them unrelatable is critical in combating terrorism.

Actions:

for average citizens, libertarians.,
Address and clean out radical elements within libertarian groups (implied).
Keep terrorist groups marginalized and unrelatable to prevent radicalization and violence (implied).
Point out flaws in terrorist ideologies to combat terrorism (implied).
</details>
<details>
<summary>
2019-03-16: Let's talk about the dark side of freedom of speech.... (<a href="https://youtube.com/watch?v=d5pJOE-nzFU">watch</a> || <a href="/videos/2019/03/16/Lets_talk_about_the_dark_side_of_freedom_of_speech">transcript &amp; editable summary</a>)

Beau stresses the importance of speaking out against dangerous rhetoric to prevent ethnic cleansing in the United States, warning that history's rhymes are already echoing.

</summary>

"It can happen here."
"You have an obligation to speak over them."
"Genocide. That's how it ends."
"History doesn't repeat, but it rhymes."
"The only thing that's going to change it is you."

### AI summary (High error rate! Edit errors on video page)

Mentioned ethnic cleansing in a recent video, sparking reactions in the comment section.
People are talking about ethnic cleansing but using more subtle rhetoric.
Individuals have the right to express bigoted views, but it's your obligation to speak out against it.
There is a dangerous belief that atrocities only happen in countries with brown people.
Gives an example from the 1990s where mass rape and murder occurred in a well-developed country.
Warns that ethnic cleansing can happen in the United States and has already shown its face through mass shootings.
Talks about the dangerous propaganda tactics used by certain groups to manipulate well-meaning individuals.
Stresses the importance of waking up to the reality of the situation to prevent further atrocities.
Ethnic cleansing doesn't always mean mass murder but can involve making certain groups uncomfortable or forcing them to leave.
Emphasizes that history doesn't repeat but rhymes, indicating current dangerous trends.

Actions:

for activists, advocates, community members,
Speak out against bigotry and dangerous rhetoric (implied)
Educate others about the dangers of subtle forms of ethnic cleansing (implied)
Advocate for inclusive and diverse communities (implied)
Support organizations working to prevent hate crimes and discrimination (implied)
</details>
<details>
<summary>
2019-03-15: Let's talk about preserving a culture.... (<a href="https://youtube.com/watch?v=JCmSbWPKWrI">watch</a> || <a href="/videos/2019/03/15/Lets_talk_about_preserving_a_culture">transcript &amp; editable summary</a>)

Beau questions the concept of white culture, dismantles the link between culture and skin tone, and advocates for preserving meaningful cultures beyond physical characteristics.

</summary>

"Culture is not a thing."
"Culture and skin tone are not linked."
"Even once that happens, I'd be willing to bet that those people in the Northeastern United States have a very different culture than those people in Central Africa."
"The blending of races is an inevitability."
"If the culture becomes solely about skin tone, why should it remain?"

### AI summary (High error rate! Edit errors on video page)

Exploring the concept of white culture and its perceived destruction.
Questions the existence of a unified white culture compared to specific ethnic cultures.
Shares a personal story about a Creek man who identifies as white due to ancestry.
Emphasizes that culture and skin tone are not inherently linked.
Talks about his family's Danish ancestry and how culture transcends physical appearance.
Predicts a future where racial blending leads to a homogenized appearance.
Argues that linking culture solely to skin tone ensures its destruction.
Advocates for preserving meaningful cultures beyond just physical characteristics.

Actions:

for cultural enthusiasts, anti-racism advocates,
Challenge stereotypes and misconceptions about culture (suggested)
Encourage cultural exchange and understanding in communities (implied)
</details>
<details>
<summary>
2019-03-14: Let's talk about giving everyone free education.... (<a href="https://youtube.com/watch?v=rHuXHJPxeZs">watch</a> || <a href="/videos/2019/03/14/Lets_talk_about_giving_everyone_free_education">transcript &amp; editable summary</a>)

Beau criticizes the current education system's focus on credentialing over true education, urging individuals to take action in revolutionizing education themselves.

</summary>

"Education's easy. Right now, you've got access to the entire base of humanity's knowledge."
"It's not about education, it's about status, it's about class, it's about a way of dividing us normal folk from our betters."
"The government could have done this a long time ago, this training, the CBTs and this technology has been around for a very long time."
"If the government isn't gonna do it, we just can't have it then I guess you do it."
"You want a revolution? Start one."

### AI summary (High error rate! Edit errors on video page)

Critiques the current education system for prioritizing credentialing over actual education.
Mentions a bribery scandal where rich individuals paid to get their kids into elite schools, questioning the purpose of education in that scenario.
Talks about the university system and its lack of overhaul despite advancements in technology.
Mentions computer-based training (CBTs) in the military and how it simplifies the learning process.
Points out that the government has no interest in breaking down class barriers or providing free education.
Encourages individuals or groups to set up alternative education platforms in states with lax regulations.
Suggests creating online certifications and vocational training programs funded through ads.
Emphasizes the need for individuals to take action in revolutionizing education since the government won't prioritize it.

Actions:

for education reformists,
Set up alternative education platforms in states with lax regulations on educational institutions (suggested)
Create online certifications and vocational training programs funded through ads (suggested)
</details>
<details>
<summary>
2019-03-13: Let's talk about how I learned to love MAGA hats.... (<a href="https://youtube.com/watch?v=2kqqTHs397Y">watch</a> || <a href="/videos/2019/03/13/Lets_talk_about_how_I_learned_to_love_MAGA_hats">transcript &amp; editable summary</a>)

Beau talks about his changed view on MAGA hats and shares eye-opening experiences of a Trump supporter facing microaggressions and negative treatment in a Trump-supporting area, suggesting using the hat as a teaching tool for understanding societal dynamics.

</summary>

"Love it. It is a fantastic teaching tool."
"By the way, those harassment free zones you're looking for, those are safe spaces."
"I'm like, okay, do tell."
"Those stares, those sneers, those are the micro aggressions."
"There is a whole lot to unpack there."

### AI summary (High error rate! Edit errors on video page)

Talks about his changed view on MAGA hats after a revealing interaction.
Mentions his intention to buy a MAGA hat for the video but couldn't find one.
Raises the topic of an app to help MAGA supporters find harassment-free places.
Shares a story of a Trump supporter facing negative reactions and treatment in a Trump-supporting area.
Describes microaggressions faced by the Trump supporter, including stares and sneers.
Recounts instances of verbal harassment directed at the Trump supporter for wearing a MAGA hat.
Shares an incident where the manager of a bar asked the Trump supporter to speak more friendly about Trump.
Mentions a situation in a store where the Trump supporter was served but told not to wear the hat again.
Suggests that these negative experiences have made the Trump supporter hesitant to wear the hat.
Draws parallels between the treatment of the Trump supporter and other marginalized groups.
Encourages using the MAGA hat as a teaching tool to understand societal dynamics.
Points out that the younger generation might learn empathy through experiencing such treatment.
Mentions safe spaces as places free from harassment.
Concludes with a thought for the viewers to ponder.

Actions:

for community members,
Have open and empathetic dialogues with individuals facing discrimination (implied).
Encourage introspection and empathy in younger generations towards marginalized groups (implied).
Create safe spaces free from harassment in communities (implied).
</details>
<details>
<summary>
2019-03-12: Let's talk about the dangers of being moderate.... (<a href="https://youtube.com/watch?v=CGeKdAWnnxc">watch</a> || <a href="/videos/2019/03/12/Lets_talk_about_the_dangers_of_being_moderate">transcript &amp; editable summary</a>)

Moderates moving to the right enable extreme positions, endangering society by compromising on fundamental morals and ethics.

</summary>

"You're talking about murdering people. The moderate position of today? Well, can we just kill some of them? Trying to find that middle ground. It's dangerous."
"Moderates in this country need to get their act together."
"Are we a nation of ethnic cleansers? Are we a nation that enables child sexual abuse? Is that what we are? Is that moderate?"
"Call me crazy. Maybe I'm an extremist."
"What's the next compromise?"

### AI summary (High error rate! Edit errors on video page)

Moderates are dangerous and could be the downfall of the country.
The Democratic Party is not truly leftist but slightly right, particularly evident in their foreign policy.
Moderates trying to find compromise can enable extreme positions.
Moderates are moving dangerously to the right, tolerating positions unthinkable in the past.
Democrats are catering to far-right administrations and moving right to chase moderate votes.
The moderate position has shifted significantly over time.
Moderates are swayed by finding a middle ground between extreme positions, moving the country further right.
Child sexual abuse in immigration camps is a result of the moderate position.
Far-right extremists are taking advantage of moderates not researching or fact-checking.
Moderates need to re-evaluate their morals and beliefs to prevent further dangerous compromises.

Actions:

for moderates, activists, voters,
Re-evaluate your morals and beliefs on society's ethics (implied).
Research and fact-check claims instead of blindly seeking a middle ground (implied).
</details>
<details>
<summary>
2019-03-12: Let's talk about that new liberal plan.... (<a href="https://youtube.com/watch?v=qUZiVPYTxYY">watch</a> || <a href="/videos/2019/03/12/Lets_talk_about_that_new_liberal_plan">transcript &amp; editable summary</a>)

Beau critically examines a new liberal plan, challenges historical myths, and contrasts the values of the greatest generation with the modern American conservative ideology.

</summary>

"Living with a myth."
"Nothing like the American conservative today."
"Fearless is what made the greatest generation great."

### AI summary (High error rate! Edit errors on video page)

Analyzing a new liberal plan with various components, including cutting military pay by 15% and creating employment opportunities for conservation work.
The plan includes funding for renewable energy, railway projects, and paying artists for artwork to decorate public buildings.
There is a focus on employee rights, strengthening unions, and the establishment of entitlement programs for different groups.
A significant aspect of the plan is the proposal for a living wage for all American workers.
Beau criticizes the economic impact of the plan and instead focuses on its historical context and implications.
He challenges the myth that all World War II veterans were conservatives and dissects the reality of the greatest generation's values.
The transcript delves into issues of bigotry, refugee support, and the caring nature of the greatest generation during hard times.
Beau contrasts the values of the greatest generation with the modern American conservative ideology, pointing out discrepancies in their beliefs and actions.
He argues that American history is complex, with flawed leaders and mistakes, challenging idealized notions of historical figures.
The transcript concludes by reiterating the fearlessness and compassion of the greatest generation and contrasting it with the attitudes of the modern American conservative.

Actions:

for american citizens,
Challenge historical myths about political ideologies (implied)
Advocate for employee rights and a living wage (implied)
Support conservation efforts and renewable energy initiatives (implied)
</details>
<details>
<summary>
2019-03-10: Let's talk about questions from teens and moral injury.... (<a href="https://youtube.com/watch?v=2_OX5v9z5MA">watch</a> || <a href="/videos/2019/03/10/Lets_talk_about_questions_from_teens_and_moral_injury">transcript &amp; editable summary</a>)

Teens face anger and apathy but can combat moral injury by starting small actions, finding their role in activism, and holding onto motivations.

</summary>

"Get in the fight for 28 days. That's all it takes."
"Don't go all militant right away. Figure out what you're good at."
"The most dangerous people in the world are true believers."
"Hold onto your motivation, it's going to be the reason you make it through 28 days."
"True believers are all ideologues."

### AI summary (High error rate! Edit errors on video page)

Teens are angry and apathetic but know they should get involved in societal issues.
They face a sense of hopelessness and futility, making it difficult to take action.
Beau warns against looking up to him as a role-model.
Beau introduces the concept of moral injury, often associated with war zones.
Moral injury stems from failing to prevent or witnessing transgressions of morals.
It can result from witnessing everyday injustices like racism or environmental degradation.
Similar to PTSD, moral injury can lead to suicide, demoralization, and self-harm.
Ways to cope include belief in a just world view and building self-esteem through small daily actions.
Beau advises starting with small actions to combat issues causing moral injury, like sitting with a kid wearing a hijab.
He stresses the importance of developing skills gradually in activism and finding where you are most useful.
Effective activists share a common motivation that breaks through demoralization and self-handicapping behavior.
Motivations of activists can stem from various sources like monetary addiction, religion, ideology, or ego.
Beau encourages holding onto motivations as a reason to persist in activism.
True believers who are ideologues are the most dangerous but also the most committed in the fight for justice.

Actions:

for teens and young activists,
Start small actions to combat issues causing moral injury (suggested)
Sit with a kid that faces discrimination at school (suggested)
Use social media to raise awareness about societal issues (suggested)
Find where you are most useful in activism (suggested)
Hold onto your motivations as a reason to persist in activism (suggested)
</details>
<details>
<summary>
2019-03-09: Let's talk about the feelings of HHS and questions about the staff.... (<a href="https://youtube.com/watch?v=CK2wZLdNIIg">watch</a> || <a href="/videos/2019/03/09/Lets_talk_about_the_feelings_of_HHS_and_questions_about_the_staff">transcript &amp; editable summary</a>)

Congress investigates migrant children abuse, Health and Human Services prioritizes feelings over accountability, Beau calls for action and accountability.

</summary>

"It doesn't matter if your feelings are hurt."
"Which one of your staff should replace the person responsible for this decision?"
"Probably don't deserve to be in the position you're in."
"We had a job to do. So do you."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Congress investigates sexual abuse reports from migrant children in U.S. custody.
A congressperson misspoke about responsibility for abuse, hurting Health and Human Services' feelings.
Health and Human Services clarifies it was not all sexual abuse but also misconduct against children.
Beau stresses that regardless of contractors, the responsibility lies with Health and Human Services.
Health and Human Services demands an apology for the congressperson's misspeaking.
Beau questions which staff member waived background checks, recommended shelters, and oversaw children's welfare.
He questions who placed children at the mercy of others and made the decision to hinder investigations.
Beau compares the situation to journalists' work despite criticism, pointing out the duty to act.
He concludes by suggesting that staff hindering investigations should be replaced.
Beau delivers a critical message on accountability and prioritizing the safety of children in care.

Actions:

for congressional oversight committees,
Replace staff hindering investigations (suggested)
Prioritize child safety over personal feelings (exemplified)
Demand accountability from Health and Human Services (implied)
</details>
<details>
<summary>
2019-03-09: Let's talk about being armed and black.... (<a href="https://youtube.com/watch?v=zL_IX8yX_JU">watch</a> || <a href="/videos/2019/03/09/Lets_talk_about_being_armed_and_black">transcript &amp; editable summary</a>)

Beau recounts a tense encounter at a shooting range, offering advice for black gun clubs on engaging with law enforcement and challenging stereotypes to survive potentially lethal encounters.

</summary>

"What if I had been black? You wouldn't be hearing this story right now."
"If they tell you to put your hands on the roof of the car the second you get pulled over, do it."
"They'll kill you. They will kill you."

### AI summary (High error rate! Edit errors on video page)

Recounts a tense encounter at a shooting range with a rookie cop checking guns.
Describes how the situation was de-escalated by a sergeant using humor.
Raises the question of how the situation might have played out differently if he were black.
Offers advice for black gun clubs on engaging with law enforcement and setting rules of engagement.
Emphasizes the importance of complying with law enforcement to avoid escalation.
Advises against carrying certain types of firearms to avoid reinforcing stereotypes.
Suggests diversifying attire to challenge stereotypes about armed black individuals.
Recommends setting an example of compliant behavior during encounters with law enforcement.
Addresses questions about standardizing equipment in community defense scenarios.
Explains the historical significance and popularity of the 1911 pistol among white gun enthusiasts.
Urges the inclusion of a first aid kit in the car for emergencies.
Points out the heightened danger and need for caution for black individuals exercising their gun rights.

Actions:

for black gun club members,
Organize an event with top law enforcement officials to establish rules of engagement (suggested)
Comply with law enforcement instructions during encounters (implied)
Include a diverse range of attire during events to challenge stereotypes (exemplified)
</details>
<details>
<summary>
2019-03-08: Let's talk mistakes, human nature, unicorns, and what we're leaving our children.... (<a href="https://youtube.com/watch?v=iFj6f7ooW-w">watch</a> || <a href="/videos/2019/03/08/Lets_talk_mistakes_human_nature_unicorns_and_what_we_re_leaving_our_children">transcript &amp; editable summary</a>)

Society's resistance to change, corrupt political systems, and the need for individuals to pave the way towards a better future, as Beau advocates for a stateless society based on cooperation rather than competition.

</summary>

"We embrace a system that encourages the worst instincts humanity has to offer."
"People always say, think globally and act globally."
"Simply, we plant the seeds today for trees that we will never sit under."

### AI summary (High error rate! Edit errors on video page)

Society takes a long time to learn from mistakes and tends to double down without questioning.
The war on drugs has resulted in wasted lives, resources, and money, with plants ultimately winning.
Society embraces a system that encourages greed and exploitation.
Political leaders often participate in corruption, rising through ranks to Washington D.C.
Rare "unicorns" who genuinely care about people get corrupted in the political system.
Beau rejects the current political system and believes in people living together without borders or government.
He plants seeds for a future society he won't see, aiming for a stateless society doing no harm.
Beau criticizes societal conditioning that discourages thinking globally and acting locally.
Youth today are either angry and rebellious or apathetic due to the bleak future laid out for them.
Technology offers immense potential to solve global issues, but society clings to outdated systems.

Actions:

for activists, reformers, individuals,
Plant seeds for a future society based on cooperation and doing no harm (suggested)
Encourage critical thinking and acting locally to bring about change (suggested)
Advocate for a society without borders or government, focused on people living together (implied)
</details>
<details>
<summary>
2019-03-07: Let's talk about Omar, pro-Israeli lobbying, and nuance.... (<a href="https://youtube.com/watch?v=L7UdEb8V8bM">watch</a> || <a href="/videos/2019/03/07/Lets_talk_about_Omar_pro-Israeli_lobbying_and_nuance">transcript &amp; editable summary</a>)

Be accurate with your words when discussing Israel's policies and influence to avoid anti-Semitism accusations and acknowledge American Jews' diverse perspectives.

</summary>

"You have that obligation."
"Jews did not bomb Gaza. Israel did."
"American Jews who speak out against Israel. Wow."

### AI summary (High error rate! Edit errors on video page)

Talks about Omar's tweet, Israeli lobbying, campaign contributions, and anti-Semitism.
Mentions the importance of using OpenSecrets.org to track campaign contributions and lobbying money.
Points out that Omar's tweet about APEC donating to congresspeople was factually inaccurate.
Clarifies that AIPAC doesn't give campaign contributions but lobbies with a significant amount of money.
Emphasizes the influence of pro-Israeli groups in Congress through campaign contributions.
Questions why Democrats didn't support Omar, suggesting their ties to pro-Israeli groups.
Explains how Israeli lobbying groups strategically spread their money around to influence both parties.
Stresses the importance of discussing Israel's policies without coming off as anti-Semitic.
Advises being precise with terminology when discussing Israel and its policies.
Mentions the challenges faced by American Jews who speak out against Israel.

Actions:

for advocates and activists.,
Contact your politicians to demand transparency on campaign contributions and lobbying influences (implied).
Support American Jews who speak out against Israeli policies by amplifying their voices and stories (implied).
</details>
<details>
<summary>
2019-03-06: Let's talk about my mullet and Hoover, Alabama.... (<a href="https://youtube.com/watch?v=Ulf3hTtoNOw">watch</a> || <a href="/videos/2019/03/06/Lets_talk_about_my_mullet_and_Hoover_Alabama">transcript &amp; editable summary</a>)

Beau addresses the rise in racism triggered by societal shifts, pointing out Hoover, Alabama's history of racial issues and urging young people to challenge outdated community ideologies.

</summary>

"It's a fad, triggered because we had a black president and a bunch of white people felt like they were losing their place in the world."
"This uptick in racism is a fad, triggered because we had a black president and a bunch of white people felt like they were losing their place in the world."
"Society, they know that their elders in their community aren't always right. These kids know that. And they should have been able to discern what's right and wrong."
"It's the community's fault, but it's gonna be these kids that pay."

### AI summary (High error rate! Edit errors on video page)

Recalls a photo from his past where he was skipping school with older kids in Stewart County, Tennessee.
Mentions witnessing young men and women in Hoover, Alabama fondly discussing ethnic cleansing on the internet.
Draws attention to a series of racist incidents in Hoover, Alabama, including a girl using racial slurs and a teacher's inappropriate behavior.
Describes a case in Hoover where the police mistakenly shot the wrong person, noting a history of racial issues in the area.
Points out that Hoover, Alabama is named after a figure associated with white nationalist movements and a synagogue bombing suspect.
Attributes the recent rise in racism to a reaction against having a black president and white people feeling threatened.
Compares the current surge in racism to a passing trend, like a mullet haircut, suggesting it will eventually fade.
Expresses concern for the young individuals in Hoover who may face consequences for their community's racist views, instilled by older generations.
Notes the danger of nationalism fostering unfounded pride and hatred towards others.
Encourages young people to be critical of outdated ideologies perpetuated by elders in their community.

Actions:

for community members, youth,
Address outdated ideologies in your community by initiating open dialogues and challenging discriminatory beliefs (implied).
Educate yourself and others on the harmful impacts of nationalism and racism to foster a more inclusive society (implied).
</details>
<details>
<summary>
2019-03-05: Let's talk about the unlisted penalty behind every law.... (<a href="https://youtube.com/watch?v=ws3w2EqpioE">watch</a> || <a href="/videos/2019/03/05/Lets_talk_about_the_unlisted_penalty_behind_every_law">transcript &amp; editable summary</a>)

Beau explains how even seemingly trivial laws can escalate to violence, raising questions about the justification of laws that could lead to death.

</summary>

"Every law, no matter how trivial, is backed up by penalty of death."
"At the end of the road, whatever law that is being suggested, they will kill somebody over."
"Arrest is a violent act."

### AI summary (High error rate! Edit errors on video page)

Laws, no matter how trivial, are backed by the threat of death.
Death is rarely listed as a penalty for not complying with the law.
Using jaywalking as an example, Beau illustrates the escalation of consequences for not obeying the law.
Non-violent actions can lead to violent arrests by law enforcement.
The use of force continuum can ultimately lead to death.
Beau questions the justification of laws that could result in someone's death.
He points out that many deaths involve seemingly trivial offenses.
The power of government violence is a real and concerning issue.
The potential for fatal outcomes exists for any law enforcement interaction.
Beau expresses reluctance to support laws that could lead to lethal consequences.

Actions:

for policy advocates,
Question the justification of laws that could lead to lethal outcomes (suggested)
Advocate for police reform and de-escalation training (implied)
</details>
<details>
<summary>
2019-03-04: Let's talk about reparations.... (<a href="https://youtube.com/watch?v=v5tsHSrG-_4">watch</a> || <a href="/videos/2019/03/04/Lets_talk_about_reparations">transcript &amp; editable summary</a>)

Reparations aim to address generational wealth disparities rooted in slavery, proposing a $100 billion trust for low-interest loans to empower individuals economically.

</summary>

"Everybody in the United States has personally benefited from the institution of slavery."
"Reparations are not about seeking justice for past wrongs but about remedying the lasting impacts of historical injustices."
"It's not a handout by any means. It's a loan. It's a loan and it's going to solve a problem."

### AI summary (High error rate! Edit errors on video page)

Reparations are a topic that impacts hundreds of millions of people and must be discussed in general terms.
The wealth and foundation of the United States were built on slavery, benefiting all individuals currently residing in the country.
Generational wealth, passed down through families, plays a significant role in economic opportunities and success.
Reparations are not about taxing specific groups but should come from the general government fund.
Direct financial disbursements as reparations may not address the root issues of generational wealth disparity.
One proposed solution involves creating a trust with $100 billion for low-interest loans to help individuals start businesses or access education.
This approach aims to empower individuals to control their economic destiny and address the ongoing effects of slavery.
Reparations are not about seeking justice for past wrongs but about remedying the lasting impacts of historical injustices.
Delayed justice for slavery can never fully be achieved, but reparations can help alleviate some of its effects.
Reparations, in the form of loans, aim to empower individuals and address economic disparities.

Actions:

for americans,
Create and support trusts for low-interest loans to empower marginalized communities (suggested)
Advocate for reparations initiatives that address economic disparities (implied)
</details>
<details>
<summary>
2019-03-03: Let's talk about "not racist" people and Cherokee Princesses.... (<a href="https://youtube.com/watch?v=inKPdS7l1PQ">watch</a> || <a href="/videos/2019/03/03/Lets_talk_about_not_racist_people_and_Cherokee_Princesses">transcript &amp; editable summary</a>)

Beau explains the importance of actively opposing racism and changing behavior to combat societal prejudices, offering tactics for confronting and challenging hidden racism.

</summary>

"To change society you have to change the way people act."
"It's time for those people who are not racist to start being anti-racist."
"You don't change their heart, you don't change their mind, you don't change the way they think, but you change the way they act."
"It's time for those people who are not racist to actively oppose racism."
"Most effective thing I've ever seen is when the racist dad becomes the grandpa to a mixed kid."

### AI summary (High error rate! Edit errors on video page)

Sharing a message received about being banned for using racist slurs on social media.
Explaining his stance on banning racist slurs on his platform.
Emphasizing the need to change societal behavior to combat racism.
Encouraging people to move from not being racist to actively being anti-racist.
Debunking the myth of having a Cherokee princess in one's family tree.
Providing a historical tidbit about the mistranslation of Cherokee princesses.
Suggesting a tactic to confront hidden racism through jokes by pretending not to understand them.
Stating that confronting hidden racism can change behavior, even if not beliefs.
Advocating for changing the behavior of racist family members by not tolerating their behavior.
Mentioning a powerful example of changing a racist individual's mindset through becoming a grandparent to a mixed-race child.

Actions:

for social activists, anti-racism advocates,
Challenge hidden racism by pretending not to understand jokes that perpetuate harmful stereotypes (implied)
Change the behavior of racist family members by not tolerating their racist comments or jokes (implied)
Actively oppose racism by standing up against discriminatory behavior in social circles (implied)
</details>
<details>
<summary>
2019-03-02: Let's talk about drafting women and men's rights activists.... (<a href="https://youtube.com/watch?v=NHO8eBkeykc">watch</a> || <a href="/videos/2019/03/02/Lets_talk_about_drafting_women_and_men_s_rights_activists">transcript &amp; editable summary</a>)

Men's rights activists challenge the draft's discrimination but risk harming innocents in their strategy, while advocating for equality in freedom.

</summary>

"You seek equality in freedom."
"You've taken hostages in an attempt to get what you want. You're willing to hurt the innocent, those that aren't complicit in it, to get what you want."
"If you want to take a stand, take a stand. Don't take hostages."

### AI summary (High error rate! Edit errors on video page)

Men's rights activists are viewed negatively by society and the activist community for reasons like not wanting to pay child support or using their children as leverage.
An MRA group in Texas filed a lawsuit claiming that the draft was discriminatory and won in federal court.
The Supreme Court used the argument of women not being allowed in combat as a reason to disallow them from the draft, a ruling from the 80s.
MRAs believe that if women were subject to the draft, they'd be more vocal in ending it, leading to its sooner end.
Beau argues that the draft is immoral, using government violence to force people to fight is wrong, and not a modern-age responsibility.
Beau points out the inconsistency of opposing the draft while advocating to draft women to fight, exposing them to government violence.
He stresses the importance of ideological consistency in building a movement and seeking equality in freedom, not oppression.
Beau criticizes the strategy employed by MRAs as akin to seeking equality in oppression, contrasting it with seeking freedom.
Despite the controversy around the draft, a military commission is likely to advise the Department of Defense to end the Selective Service.
Beau concludes by condemning the approach of taking hostages to achieve goals, urging for a stand without resorting to harming the innocent.

Actions:

for activists, advocates, critics,
Advocate for ideological consistency within movements (implied)
Stand against harmful tactics and advocate for equality without resorting to violence or harm (implied)
</details>
<details>
<summary>
2019-03-01: Let's talk about what we can learn from Momo (<a href="https://youtube.com/watch?v=S7L6wCzscDg">watch</a> || <a href="/videos/2019/03/01/Lets_talk_about_what_we_can_learn_from_Momo">transcript &amp; editable summary</a>)

Momo meme panic reveals lack of parental involvement, urging focus on real teen struggles over fictional scares.

</summary>

"We don't really spend enough time with our kids."
"We sell hours of our life every day to some faceless entity at the expense of our families."
"Maybe Momo is a good thing. Maybe like most good stories, it has a moral."
"Become a little bit more involved if you can."
"So maybe Momo is a good thing. Maybe like most good stories, it has a moral."

### AI summary (High error rate! Edit errors on video page)

Momo, a meme causing panic, is actually a joke known to kids for a year and a half.
Children understand it's a joke and find parents' reactions hilarious.
The fear around Momo reveals a lack of parental involvement in children's lives.
Parents often use technology as a babysitter, unaware of what's on their kids' phones.
Modern society values work over family time, leading to disconnection.
Beau suggests that Momo's scare might have helped people focus more on their kids.
He points out statistics on teen struggles like suicide attempts, drug use, and pregnancies.
These real-life issues are much scarier than the Momo meme.
Beau implies that the moral of the Momo story is to be more involved with our children.
He encourages viewers to prioritize spending time with their kids.

Actions:

for parents, caregivers, families,
Spend quality time with your kids regularly (suggested)
Monitor your children's online activities (suggested)
</details>
