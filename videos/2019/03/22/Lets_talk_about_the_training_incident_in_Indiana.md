---
title: Let's talk about the training incident in Indiana.....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=eeAvhhppe6I) |
| Published | 2019/03/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains an incident in Indiana where teachers were mock executed during an active shooter drill.
- Clarifies that the incident was not part of the ALICE training program and expresses doubt that it was authorized by them.
- Criticizes the lack of effective training programs for active shooter situations and the mentality needed for such training.
- Describes how teachers were taken into a room, lined up against a wall, and shot in the back with a high-powered airsoft gun as part of the drill.
- Questions the effectiveness of using fear and pain in training teachers to respond to active shooter situations.
- Raises concerns about whether teachers were aware they could fight back during the drill.
- Criticizes the reactive nature of the training and suggests a more proactive approach involving psychologists to identify warning signs.
- Condemns the use of violence as a solution and points out the potential impact on students' mental health and well-being.
- Urges for a reevaluation of current training methods for teachers in dealing with school shootings.

### Quotes

- "I wonder where the school shooters get the idea from us, from us, because violence is easy."
- "Just kill them, two in the chest, one in the head. Blood makes the grass grow green."
- "If we keep this posture, we can't be surprised when there are more small caskets because of an event at a school."

### Oneliner

Beau criticizes the mock execution of teachers during an active shooter drill in Indiana, questioning the effectiveness and ethics of such training and advocating for a more proactive approach to school safety.

### Audience

Teachers, educators, school administrators

### On-the-ground actions from transcript

- Advocate for comprehensive and trauma-informed training programs for teachers (implied)
- Support initiatives that focus on proactive measures such as identifying warning signs of potential violence (implied)

### Whats missing in summary

The emotional impact on teachers and students involved in such drills and the potential long-term consequences on mental health and well-being.

### Tags

#SchoolSafety #TeacherTraining #ViolencePrevention #ProactiveApproach #EducationSafety


## Transcript
Well, howdy there Internet people, it's Beau again.
So tonight we're going to talk about what happened in Indiana.
There was an active shooter drill, training program, and teachers were mock executed during
it.
Before we get into the actual topic, I want to be very clear about something.
I've seen a lot of people say, and reports even, saying that this was the ALICE training
program.
No, no, I'm familiar with that program.
I've seen a lot of their training material, I know people that have been through it.
Maybe this lesson is in their program, this execution of it is certainly not.
I would be extremely surprised to find out that this was in any way, shape, or form authorized
by them.
And the reason I want to say that is there's not a lot of good active shooter programs
out there.
There are not a lot of training programs out there for this topic.
You don't get a lot of high-speed combat instructors drawing up curriculums for this because they
sit down, they write up the curriculum, and then they get the middle flash of the English
comp teacher after she puts two in the chest of one of her students, then she puts another
went into his face and go, safety shot, because that's the mentality you need.
And that's not the mentality you want in a teacher.
The ALICE program does a pretty good job of giving teachers skills they can use without
trying to turn them into killers.
And I don't want to see a good program drug through the mud.
It's not perfect, but it's an impossible task.
better than most. So I don't want this incident to be linked to them. That's not a fair or
wise thing to do. Okay, so what went down? Teachers were taken four at a time into a
room, put up against a wall, and then shot in the back with an airsoft gun. And apparently
this was not one of the cheap airsoft guns you get at Walmart, this was a pretty high
power one, it drew blood in some cases.
For those that don't know, it's a plastic BB gun, it shoots little plastic pellets pretty
quickly though.
So the lesson that they were trying to pass along was that if you just cower you're going
to die because teachers don't know this.
I can understand using this form of training to do it because everybody knows that if you
inflict pain on somebody, then they'll remember.
That's why the Army, when they're teaching people how to use cover, they make everybody
stand up and they shoot them with some ammunition.
Everybody that was in the Army is like, what?
No, they don't do that because they're not stupid.
The average person has no idea what it's like to be shot.
They have a fear, they have a fear of death, but the pain, that's intangible.
They have no idea what it's like.
But those teachers do now.
They know it's going to be a whole lot worse than what they went through.
So what does that do?
What does that fear do?
Is it more likely that they're going to respond or is that fear going to trigger panic and
limit their ability to self-help?
Seems like the more likely option.
This was a very, very poorly thought out exercise.
The other question I would ask, were the teachers aware that they could assault their instructors
and fight back?
Were they instructed that they could do that?
Because if not, this is just some sadistic joke.
Now there's another issue with this.
Do we have a problem with teachers not responding?
Has that been going on where teachers are leaving students to die?
teachers aren't doing everything that they can.
Because from what I remember, they're literally using their own bodies to shield their students.
Meanwhile, law enforcement is waiting outside.
Maybe if this is really the way you guys think that you need to teach people, wouldn't it
have been wiser to take the children of cops and line them up and shoot them with an airsoft
gun and make their parents watch?
I mean, because then they'll respond, you see how stupid this sounds?
The whole idea behind this is the problem in itself.
This is all reactive, it's reactive violence.
You want to train the teachers to be warriors, you can't.
Not and still haven't been teachers.
That's the problem with this.
It's reactive.
They're describing this training as being proactive, no, it's not.
It's not in any way, shape, or form because for any of this to be used, somebody has to
walk into a school and start killing kids.
That's reactive.
That's not proactive.
You want to be proactive, you bring in a psychologist and have them instruct the teachers on how
to look for warning signs.
That's proactive.
But no, we have a problem, and rather than try to solve it, we're going to use violence.
Man, I wonder where the school shooters get the idea from us, from us, because violence
is easy.
That's the easy way out.
Just kill them, two in the chest, one in the head.
Blood makes the grass grow green.
That's the solution.
If we keep this posture, we can't be surprised when there are more small caskets because
of an event at a school.
We really need to rethink what we're doing here.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}