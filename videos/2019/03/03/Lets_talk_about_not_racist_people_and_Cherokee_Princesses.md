---
title: Let's talk about "not racist" people and Cherokee Princesses....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=inKPdS7l1PQ) |
| Published | 2019/03/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Sharing a message received about being banned for using racist slurs on social media.
- Explaining his stance on banning racist slurs on his platform.
- Emphasizing the need to change societal behavior to combat racism.
- Encouraging people to move from not being racist to actively being anti-racist.
- Debunking the myth of having a Cherokee princess in one's family tree.
- Providing a historical tidbit about the mistranslation of Cherokee princesses.
- Suggesting a tactic to confront hidden racism through jokes by pretending not to understand them.
- Stating that confronting hidden racism can change behavior, even if not beliefs.
- Advocating for changing the behavior of racist family members by not tolerating their behavior.
- Mentioning a powerful example of changing a racist individual's mindset through becoming a grandparent to a mixed-race child.

### Quotes

- "To change society you have to change the way people act."
- "It's time for those people who are not racist to start being anti-racist."
- "You don't change their heart, you don't change their mind, you don't change the way they think, but you change the way they act."
- "It's time for those people who are not racist to actively oppose racism."
- "Most effective thing I've ever seen is when the racist dad becomes the grandpa to a mixed kid."

### Oneliner

Beau explains the importance of actively opposing racism and changing behavior to combat societal prejudices, offering tactics for confronting and challenging hidden racism.

### Audience

Social activists, anti-racism advocates

### On-the-ground actions from transcript

- Challenge hidden racism by pretending not to understand jokes that perpetuate harmful stereotypes (implied)
- Change the behavior of racist family members by not tolerating their racist comments or jokes (implied)
- Actively oppose racism by standing up against discriminatory behavior in social circles (implied)

### Whats missing in summary

The full transcript provides a detailed exploration of confronting hidden racism and advocating for active anti-racism efforts in society.

### Tags

#AntiRacism #ConfrontRacism #ChangeSociety #HiddenRacism #FamilyConversations


## Transcript
Well howdy there internet people, it's Bo again.
So tonight, I'm going to start off by reading you a message I got, at least the parts of
it I can read.
Did you effing ban me, slur for homosexual, I should have expected this from a slur for
a person who has relations with a black person, you're supposed to be a free speech advocate
slur for homosexual.
I didn't get to respond to you because you blocked me on Facebook before I could, so
yes, yes, I banned you.
racist slurs will get you banned on my social media. That's really one of the
very few rules that I have. I love debate. I love arguments. They're fantastic. The
expressions of new thoughts and ideas. This isn't an idea and it's not a new
thought. I see no reason to give people like you a platform so you don't get one
with me. Take it somewhere else. You know, a very wise person once said that to change
society you have to change the law. I disagree. To change society you have to change the way
people act. And by allowing those comments, it makes it seem like I find it tolerable.
I don't. I don't.
You know, a lot of people have trouble confronting racism.
You know, there's a lot of people who,
well, I'm not racist.
It's time for those people, it's past time,
for those people who are not racist
to start being anti-racist.
There have been people who are not racist for a really long time,
But racism is still very, very prevalent.
I'm going to give you a historical tidbit.
Somewhere you probably know somebody who believes that in their family tree they have a Cherokee
princess.
You don't.
They don't.
Whoever.
I promise.
Cherokee did not have princesses. Period. Now historians and linguists write this up
to a mistranslation of chief's daughter or an important political woman. See, I like
what the genealogists found, and I'm going with this theory because it makes a whole
lot more sense. When they started looking for people's Cherokee princess
ancestor, what they found out was that a lot of times she wasn't even Cherokee. So
what happened, it certainly appears, is that your not-racist white ancestor
married a native woman, but she wasn't from one of the civilized tribes. That was
the thing. So rather than deal with the confrontation, he lied and said she was
Cherokee. And then to help alleviate the racial tensions, well he made a royalty.
That's probably where the story comes from. It's the version, the
explanation that I like anyway. I think it makes a whole lot more sense than a
series of mistranslations by different translators that match. I'm not buying
that. So the problem with confrontation has been around for a very very long time
with not racist people and it still exists today. So I'm gonna give you a
tactic. I wish I could say that I came up with this. I did and I've been using it
for a couple years now because it's brilliant and it works. Most racism you
find today is hidden in jokes. It's couched in jokes. The punch lines. Stuff
like that. You don't have to confront them and you know I use this a lot in
in social situations where I can't be as abrasive as I sometimes am.
But just pretend like you don't get the joke.
Make them explain it.
And then watch.
And eventually you'll enjoy doing this.
Because it's funny.
It really is.
I mean, I don't get it.
Well see, it's funny because there was this famine and a bunch of Irish people died so
it works with sexism too.
I don't understand.
Well see, you know, she didn't do the dishes and domestic abuse.
I don't get it.
And just keep saying it.
And watch.
You don't change their heart, you don't change their mind, you don't change the way they think, but you change the way
they act.  At least around you, that's all you have control over.
And if everybody's doing this and letting them know that it's not tolerable, that you're not going to give them that
platform, it goes away.  It goes away.
And this is something that anybody can do, even if you are deathly afraid of confrontation, you can do this.
You can do this.
It works with your racist family members too.
It changes the way they act around you and you don't have to have the long drawn out
fight.
Let's be honest.
Your 60, 70 year old relative, they're probably racist, at least to some degree.
Not all of them, but some of them.
And you're not going to change them.
They've been that way for a long time.
It is ingrained, but you can change the way they act around you.
For them to actually change, it's going to take something profound.
Most effective thing I've ever seen is when the racist dad becomes the grandpa to a mixed
kid.
All of a sudden, he's not so racist.
At least he doesn't show it.
And maybe it actually changes the way he thinks.
But it's time for those people who are not racist to actively oppose racism.
And if this is all you can do, this is all you can do.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}