---
title: Let's talk about the unlisted penalty behind every law....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ws3w2EqpioE) |
| Published | 2019/03/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Laws, no matter how trivial, are backed by the threat of death.
- Death is rarely listed as a penalty for not complying with the law.
- Using jaywalking as an example, Beau illustrates the escalation of consequences for not obeying the law.
- Non-violent actions can lead to violent arrests by law enforcement.
- The use of force continuum can ultimately lead to death.
- Beau questions the justification of laws that could result in someone's death.
- He points out that many deaths involve seemingly trivial offenses.
- The power of government violence is a real and concerning issue.
- The potential for fatal outcomes exists for any law enforcement interaction.
- Beau expresses reluctance to support laws that could lead to lethal consequences.

### Quotes

- "Every law, no matter how trivial, is backed up by penalty of death."
- "At the end of the road, whatever law that is being suggested, they will kill somebody over."
- "Arrest is a violent act."

### Oneliner

Beau explains how even seemingly trivial laws can escalate to violence, raising questions about the justification of laws that could lead to death.

### Audience

Policy advocates

### On-the-ground actions from transcript

- Question the justification of laws that could lead to lethal outcomes (suggested)
- Advocate for police reform and de-escalation training (implied)

### Whats missing in summary

The emotional impact and detailed examples can be better understood in the full transcript.


## Transcript
Well howdy there internet people, it's Bo again.
So tonight we're going to talk about a concept I have brought up a couple of times and every
time I bring it up there's a question about it so we're going to kind of delve into it
a little bit deeper tonight.
In the United States in particular, but this is true in most countries, every law, no matter
how ridiculous, no matter how stupid, no matter how trivial it seems, is backed up by penalty
of death.
Every single one.
And the confusion sets in because death is very rarely listed as a penalty for not complying
with the law.
During the vaccine video, people were like, nobody's going to hold a gun to someone's
head and make them take a vaccine.
the draft video, people. It's not government violence. They're just making them fill out a form.
Okay, so what I want you to do is think of the stupidest law you can possibly imagine,
like the most ridiculous one in your jurisdiction, whether it's collecting rainwater or having solar
panels on your roof, whatever the dumbest law you can think of. I always like to use jaywalking
for those overseas, crossing the street in the wrong spot. I like to use it
because it's a law in most jurisdictions and it's stupid. So you cross the street
in the wrong spot. Cop sees you, writes you a ticket. You're not gonna pay this
ticket. You're not gonna be punished for crossing the street, okay? So you don't
pay the ticket. What happens next? First they try to encourage you, extort you into
complying. They may suspend your license, they may send you a letter with a court
date, whatever. Okay, so you don't go to the court date. What happens? The judge gets
mad, writes a bench warrant for your arrest, and now the suspended license and
the bench warrant are at the same place. You know, normally they don't dispatch
somebody to go arrest you for a bench warrant. They wait till you get pulled over.
So you get pulled over. You're sitting there in the car. What happens then?
At this point, all you have done is cross the street in the wrong spot and then
refuse to be punished for it. That's it. You've committed no act of violence
whatsoever. What happens? They try to arrest you. They're going to take you
you, out of your car, physically take you out of the car, cuff you, throw you in their
car and then throw you in a cage. You've done nothing violent. You're already having
violence visited upon you. Arrest is a violent act.
So what's next? Let's say you don't let them arrest you. You refuse to be punished for
crossing the street in the wrong spot and you don't comply. Now if you have a decent
officer, it will start off with pepper spray. But at the end of the use of force continuum,
it's death. They kill you. You resist hard enough, they kill you. Every law, doesn't
matter which law you're talking about, at the end of the road is a cop with a gun. So
And that's why I'm extremely reluctant to support many laws.
Because I don't ask, would it be better off for people if somebody did this?
I ask, is it okay to kill the person if they don't?
Because that's what's going to happen.
And I know that seems ridiculous.
But if you look at a lot of the deaths that have occurred, they're over pretty ridiculous
things.
when cops kill somebody who's resisting. Selling loose cigarettes can get you
killed. Ask Eric Garner. So then when I talk about the power of a government gun
or government violence that's what I'm talking about and then that's why a lot
of a lot of the positions I take may differ because this this is a very real
reality that this is a very real thing at the end of the day at the end of the
road whatever law that is being suggested they will kill somebody over
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}