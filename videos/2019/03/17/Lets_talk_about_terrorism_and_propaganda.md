---
title: Let's talk about terrorism and propaganda....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=CMRuMaRUq8Q) |
| Published | 2019/03/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Defines terrorism as a strategy to provoke an overreaction from the establishment.
- Explains how terrorists aim to make themselves relatable to gain support.
- Talks about fascists being hard to offend or make relatable.
- Analyzes a manifesto's brilliance in making the author relatable through nods to different groups.
- Mentions the ideological journey from left to right and doubts its authenticity.
- Points out the existence of a libertarian to fascist pipeline.
- Urges libertarians to address and clean out radical elements within their group.
- Emphasizes the importance of not getting defensive when discussing these issues.
- Stresses the need to keep terrorist groups marginalized and unrelatable.
- Connects the importance of keeping terrorists unrelatable to preventing radicalization and violence.
- Mentions the founding fathers as historical examples of terrorists.
- Suggests that pointing out flaws in terrorist ideologies and keeping them unrelatable is critical in combating terrorism.

### Quotes

- "Hang them from lamp posts. Nobody cares, they're fascists."
- "You've got to keep that wall up. It's the most important thing you, as an average citizen, can do to combat terrorism."
- "It's extremely important that you do this. Because if this group becomes relatable to the average person, people start to radicalize."
- "The founding fathers were terrorists, textbook."
- "Point out the flaws in the ideology and make sure they stay unrelatable."

### Oneliner

Beau explains terrorism as a strategy to provoke overreactions, urges keeping terrorist groups unrelatable to combat radicalization and violence.

### Audience

Average citizens, libertarians.

### On-the-ground actions from transcript

- Address and clean out radical elements within libertarian groups (implied).
- Keep terrorist groups marginalized and unrelatable to prevent radicalization and violence (implied).
- Point out flaws in terrorist ideologies to combat terrorism (implied).

### Whats missing in summary

Importance of understanding terrorist strategies and the role of relatability in radicalization.

### Tags

#Propaganda #Terrorism #Radicalization #CombattingViolence #IdeologicalJourney


## Transcript
Well howdy there internet people, it's Beau again.
So today we've got to talk about propaganda, specifically how propaganda relates to terrorism.
A couple of weeks ago I did a video on terrorism, what it is and how it works.
I strongly suggest you go back and watch it, but the key takeaways that are important to
this conversation is that terrorist is not a slur.
This guy is a terrorist, but it's not because he walked into a room and killed a bunch of
innocent people.
He's a terrorist because he's using a strategy and that strategy is to provoke an overreaction
from the establishment.
Now when that overreaction occurs, and it always does, if the general populace identifies
more with the terrorist group than the establishment, well then they start siding with the terrorist.
That's how it works.
That's what terrorism is designed to do.
Fascists have a very hard time with this, because they're not relatable.
Fascists are not relatable people.
There's not a whole lot that can be done to fascists that is going to offend the general
population.
Hang them from lamp posts.
Nobody cares, they're fascists.
The reason this manifesto was such a brilliant piece of propaganda was because it attempted
to make him relatable. There's little nods to various groups throughout it like the Navy
Seal copypasta. For those that don't know that whole bit about being a Navy Seal, it's
a text meme that's been circulated forever. So it was a nod to that group. Subscribe to
Poodie Pie. It's a nod to that group. His ideological journey, which I'm fairly
certain is manufactured, but we'll get to that. It was a nod to these various
groups. Poodie Pie. Is he a racist? I don't know. I've never watched anything
that he's ever done. I know that there's some kind of debate over it, but for the
sake of conversation we're gonna say he's not, because it doesn't matter. It
it doesn't matter
because he was mentioned
there will be a backlash against him
it's inevitable
what do his fans
then have to do
they have to make a choice
do they
defend him
and if they do who do they begin to identify with
that's how terrorism works that's how it spreads that's how people become
radicalized
Now the ideological journey from left to right is, I doubt that it's true.
I doubt that it's true because it's a very odd journey and the other thing that's very
unique is that they all have militant groups in them.
It's almost as if they were specifically chosen in hopes of radicalizing the militants within
each one of these groups.
The libertarians are mentioned.
And there is a libertarian to fascist pipeline that exists.
Seriously, you guys need to clean out your house.
Y'all need to get your stuff in order.
Good luck in the next election.
Hi, the Libertarian Party, we're the people that brought you the crying Nazi and the New
Zealand shooter.
Right now, there's a whole bunch of libertarians getting defensive.
Don't do that.
That's the whole point.
Don't do that.
Everything I just said, it's true.
But the way I presented it, it was to make you get defensive.
Don't do that.
What's your response?
Yeah, he was a libertarian.
He didn't start killing people until he became a fascist.
He didn't start hurting people until he embraced white nationalism.
You've got to keep that wall up.
It's the most important thing you, as an average citizen, can do to combat terrorism.
You have to keep the terrorist group marginalized.
You have to keep them unrelatable.
That's what's important.
It's extremely important that you do this.
Because if this group becomes relatable to the average person, people start to radicalize.
They start to identify with the terrorist group more than the establishment.
And in this case, that's a really, really bad thing.
Sometimes terrorism can be used for good.
Go back and watch the other video.
It's a strategy, it's a strategy.
The founding fathers were terrorists, textbook.
This is not a situation like that.
This is a situation in which if people begin to identify with this group of people there
will be lots of bloodshed of innocence.
what's going to happen. So we have to make sure that they stay
unrelatable and point out the flaws in their
ideology. I had somebody tell me when I talked
about white culture, he said the white culture really meant
western civilization and that's you know a drive towards
more independence and you know individualism.
Well that's kind of true. I don't see how collectivizing yourself by race,
race, by skin tone, supports that. It's actually in direct contradiction to it.
Point out the flaws in the ideology and make sure they stay unrelatable. That is what you
can do as the average person to stop this form of terrorism.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}