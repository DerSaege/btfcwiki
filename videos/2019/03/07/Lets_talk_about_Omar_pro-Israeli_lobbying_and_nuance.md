---
title: Let's talk about Omar, pro-Israeli lobbying, and nuance....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=L7UdEb8V8bM) |
| Published | 2019/03/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about Omar's tweet, Israeli lobbying, campaign contributions, and anti-Semitism.
- Mentions the importance of using OpenSecrets.org to track campaign contributions and lobbying money.
- Points out that Omar's tweet about APEC donating to congresspeople was factually inaccurate.
- Clarifies that AIPAC doesn't give campaign contributions but lobbies with a significant amount of money.
- Emphasizes the influence of pro-Israeli groups in Congress through campaign contributions.
- Questions why Democrats didn't support Omar, suggesting their ties to pro-Israeli groups.
- Explains how Israeli lobbying groups strategically spread their money around to influence both parties.
- Stresses the importance of discussing Israel's policies without coming off as anti-Semitic.
- Advises being precise with terminology when discussing Israel and its policies.
- Mentions the challenges faced by American Jews who speak out against Israel.

### Quotes

- "You have that obligation."
- "Jews did not bomb Gaza. Israel did."
- "American Jews who speak out against Israel. Wow."

### Oneliner

Be accurate with your words when discussing Israel's policies and influence to avoid anti-Semitism accusations and acknowledge American Jews' diverse perspectives.

### Audience

Advocates and activists.

### On-the-ground actions from transcript

- Contact your politicians to demand transparency on campaign contributions and lobbying influences (implied).
- Support American Jews who speak out against Israeli policies by amplifying their voices and stories (implied).

### Whats missing in summary

Deeper insights into the nuances of discussing Israel's policies and lobbying influence while avoiding anti-Semitic implications.

### Tags

#Israel #Lobbying #AntiSemitism #CampaignContributions #Advocacy


## Transcript
Well howdy there internet people, it's Bo again.
So today I guess we have to talk about Omar's tweet, Israeli lobbying, campaign contributions
and anti-Semitism.
So before we get started, I'm going to be throwing out a lot of numbers, normally I
make you guys research and find this stuff on your own because I think it's important.
In this case, this information is kind of hard to find unless you know where to look.
OpenSecrets.org.
They track campaign contributions and money spent on lobbying.
If you want to know who owns your politician, this is where you find out.
Okay, so Omar's tweet basically said, uh, APEC donated to a bunch of congresspeople.
The general gist of what the tweet was.
That was wrong.
That was wrong.
Was wrong of her to tweet that because it was factually inaccurate.
APAC doesn't give campaign contributions at all.
If you go to the individuals that are inside that organization, you can find about $20,000
donated in 2018 to various politicians.
That's nothing.
The grand scheme of things, $20,000 is nothing,
especially when you're talking about an organization that
lobbies to the tune of $3.5 million in 2018.
She was factually inaccurate.
AIPAC does exert a lot of influence.
Now as far as campaign contributions, pro-Israeli groups donated about $15 million in 2018 alone.
A midterm election, that's a lot of money.
So she was factually inaccurate, but the idea that pro-Israeli groups exert a lot of influence
Congress, oh that's true. That's a true statement. So why did the Democrats sell
her out? Why didn't they back her up and just, well she misspoke? Because they get
a lot of the money. In fact the number one recipient in 2018 was Robert
Nadez, Democrat from New Jersey, got more than half a million dollars.
Right now Republicans are like, ha, Ted Cruz was number two.
Israeli lobbying groups and groups that donate, they're not stupid.
They're just like every other special interest group.
They spread their money around.
They spread their money around.
So it doesn't matter who wins, they win.
So how do you discuss this without coming off as an anti-Semite?
Pretty simply.
I've been a very, very vocal critic of Israel's policies for years.
Never been called an anti-Semite.
It's never happened.
happened. Why? Because when I discuss Israel's policies, I discuss Israel's
policies, not Jews. We're about four minutes into this video. First time the
word Jew was spoken. Why? Because it's not Jews. That's a very imprecise
term. It's factually inaccurate. You know, one of the terms I hate most is Jewish
settlers because it's not Jewish settlers. They're Israeli settlers. Jews all over the
world do not get held responsible for the actions of Israel. That's a government. Government
policies. You can't hold everybody responsible for that. It's an overgeneralization. It's
kind of like saying Muslims are terrorists. It's not true. Even if you
were to get more specific, Wahhabi and Salafi, it's more accurate but it's still
not all of them. It's imprecise. That overgeneralization comes off as racism.
Why? Because it kind of is, even if it's unintentional. So you have to be very
careful with that, especially when you're talking about a demographic that
is probably still pretty tender to being overgeneralized, especially when it comes
to saying that they're influencing a government. That did not work out well
for them last time. So they are probably very leery of even allowing that idea
to come out. What Israel does is, and pro-Israeli groups, it's not unheard of. It's unusual
for a country to get away with doing it. But lobbying like this, this is just the American
way. It's not right, but it's the American way. The other thing you have to be very aware
of is that a lot of American Jews identify with Israel.
They see that Star of David on the flag and they're like, well that's part of me.
And that's something you need to be aware of because they associate the Israel of today,
state of Israel, with the Israel from a long, long time ago, and they connect the two.
That's something you have to be careful with, and you do.
You have to be tender with the subject.
There's a lot of nuance in it, but the key thing you have to do is that you have to be
accurate with your language.
Jews did not bomb Gaza.
Israel did.
It's a government.
Not everybody around the world is held responsible for that.
You know who gets it worse than anybody, though?
American Jews who speak out against Israel.
Wow.
They get it hard.
In fact, I'm going to link a video from a friend of mine
Carrie. She's from the West Coast. We share a lot of the same beliefs, but
her presentation is a little bit different. It may not be everybody's cup of
tea, but it kind of explains her journey coming from being one of those
Americans who looked at the flag of Israel, saw that star of David, and was
like, hey, that's me, and then looked into Israel's policies and was like, I want
nothing to do with this and then talked to other American Jews and well you'll watch
the video.
So that's the controversy in a nutshell.
A lot of it had to do with imprecise language, a lot of it had to do with the scars of history
reopening.
And then a lot of it had to do with just plain corruption.
That's the real reason Democrats aren't backing her up, because they probably took money.
So you don't have a right to criticize governments around the world.
You don't have a right to criticize money and politics.
You don't have a right to criticize special interest groups that are representing foreign
governments influencing your government. You don't have that right. You have that obligation.
It's very important that you do it, but you also have an obligation to be accurate with
language. Anyways, just a thought. Y'all, have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}