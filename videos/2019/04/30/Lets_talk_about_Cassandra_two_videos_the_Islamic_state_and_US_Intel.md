---
title: Let's talk about Cassandra, two videos, the Islamic state, and US Intel....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KDIsogkOc2k) |
| Published | 2019/04/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Cassandra Complex: feeling ignored like Cassandra, gifted with prophecy nobody listens to.
- ISIS isn't defeated: holding land isn't a metric for potency, they can go on the offensive.
- US intelligence failure: lack of foresight, listening to signal intercepts and satellite photos.
- Technology vs. terrorism: spying tech doesn't provide intent, Baghdadi avoids electronics.
- Baghdadi's intelligence: US knew he was smart, used him unknowingly to build the Islamic State.
- Baghdadi's video: propaganda showing humility or giving orders? Alarming Al Qaeda style attacks expected.
- Declaring ISIS defeated: dangerous move, lowers expectations, makes them the underdog.
- Foreign fighters' threat: potential attacks in countries listed in video folders.
- Need for human intelligence: shift focus from technology to understanding intent.

### Quotes

- "Holding land requires resources, and when you take that away, they can go on the offensive."
- "US intelligence failure, listening to signal intercepts and satellite photos."
- "Technology doesn't provide intent, Baghdadi avoids electronics."
- "Declaring ISIS defeated has made them the underdog again."
- "Y'all have a good night."

### Oneliner

Beau outlines the dangers of declaring ISIS defeated, warns of future attacks, and calls for a shift towards human intelligence over technology in counter-terrorism efforts.

### Audience

Counter-terrorism experts

### On-the-ground actions from transcript

- Prioritize human intelligence over technology in counter-terrorism efforts (implied).
- Stay vigilant and prepared for potential Al Qaeda style attacks in the future (implied).

### Whats missing in summary

In-depth analysis of the implications of relying on technology over human intelligence in counter-terrorism efforts.

### Tags

#CounterTerrorism #ISIS #USIntelligence #HumanIntelligence #FutureAttacks


## Transcript
Well howdy there internet people, it's Beau again.
So tonight's going to be a little bit different.
This is more like my therapy session to be honest, because just like every other expert
on political violence or counter-terrorism in the world, I feel like I got Cassandra
Complex right now.
If you're not familiar with it, Cassandra was a beautiful woman, daughter of the King
of Troy, and well Apollo loved her.
He gave her the gift of prophecy.
When she rejected his advances, he said, well, you've got the gift of prophecy, but nobody's
going to listen to you.
And that's the way everybody in that field feels right about now, because politicians
and those in the establishment who stand to gain from defeating ISIS are still claiming
ISIS is defeated when it's not and that just makes them more dangerous.
Baghdadi the leader, he released a video today, well yesterday by the time you're watching
this and the take coming out of the establishment circles was sickening.
They saw what they wanted to see in it, not what was there.
Now on April 6th, I released a video, A Toast to the Defeat of ISIS.
I suggest you go watch it, if you don't, I give you a synopsis, ISIS isn't dead.
Using the land held by an insurgent organization that's really a terrorist organization at
heart as a metric of determining their potency is stupid.
It is stupid.
Holding land requires resources, and when you take that away and they don't have to
expend those resources to defend that land, well then they can go on the offensive.
Their leadership also disappears, blends away.
They spread out, head into other areas of operation to begin work.
Islamic State has shown repeatedly that it can attack far, far beyond its footprint.
And then I discussed those that were killed after the President said that the Islamic
State was defeated.
And then I discussed the rumor coming out of Iraq about attacks on churches, April 6th,
before the Easter attacks.
Now there was a time when, yeah, I was up to speed on all of this and I was really good.
That was a long time ago, that was more than a decade ago.
So my question, and the question everybody should be asking, and to be clear, I'm not
the only person who pointed all of this out. Every political violence expert in the world
that I saw comment on this said the exact same thing. But the question everybody should
be asking is if the has-been that's been out of that field of study for 10 years could
list down to the letter what was going to happen.
Where was US intelligence?
What was the establishment doing?
This was an utter failure of US intelligence, complete failure.
And what they were doing, they were listening to signal intercepts and looking at satellite
photos.
That's what they were doing because that's what it's all about now.
That is what it is all about.
And that technology, it's great for spying on you, it doesn't help with terrorism.
Number one, it doesn't provide intent, it doesn't let you know what they're going to
do.
A rumor coming out of Iraq, that lets you know what they're going to do.
The $50 informant in Basra is worth more than that $50 million satellite.
So that's one reason.
It does not give you intent.
Just lets you know what's happening, but by then it's too late.
The other reason it doesn't help is that Baghdadi's smart.
He doesn't use electronics.
None of these guys do.
And our policy is to kill terrorists
when we catch them in the open, call in a drone.
That's great, except for the fact that he's probably
communicating with his organization via courier.
Really, really hard to question somebody
after a Hellfire missile hits them.
See, the thing is, though, the US establishment
knows that Baghdadi is smart. They know he's smart
because they had him in custody 2003-2004
somewhere around there. He got in turn to Camp Bukka
and they knew he was smart. They used him. Well, they thought they were using him.
He was really using them. They shuttled him around
to different prisons to help keep the peace. Now in reality he's building the
Islamic State.
We don't know why exactly he got released, but we do know that by 2010, he's running
the Islamic State, that's what we know.
He just released that video and the take coming out of Washington is that, well, he's so humble
now because of this horrific defeat that he's suffered and he's talking about these operatives
he has in this country, in these various countries, as a way to show that they're
still transnational. And then he had this great propaganda where he's handed these
these files that have the names of other countries on them. Yeah, maybe. Maybe that's
it. Or maybe the guy who doesn't use electronics just got you to give
everybody his orders. Yeah, that video is very different. Baghdadi is a very
boisterous man. And yeah, he's aged since the last time we saw him. But that
doesn't change his personality. What's he doing in this video? Sitting,
traditional Arab style. Looks familiar, doesn't it? Really does. Up against a wall,
framed looks exactly like a bin Laden video doesn't it and right beside him
what's there a cut-down AK exactly like the bin Laden videos so what we can
expect is a bunch of Al Qaeda style attacks that's what we can expect in the
future bombings machine gun attacks probably some more of the vehicles
was driving into crowds. That's what's coming. Where? Those countries that he listed are
probably the base of operations, not the target. They could be the target, but that'd be pretty
bold even for him. My guess is that's where these guys are currently at training to conduct
these attacks. Just a guess. And then those file folders, the people that he has fighting
for him that aren't terrorist quality, they don't have that tradecraft training. They're
just normal insurgents. And they now know where to go. Listed on those file folders.
That's where we're headed.
That video showed intent, and the read coming out of Washington is that he's beat.
Possibly the dumbest thing the President of the United States has done since he took office,
and that's a long list to be at the top of, is declare the Islamic State defeated, because
Now he's the underdog again.
He doesn't have to stage any spectacular attacks.
He did, but he doesn't have to anymore.
Because you've lowered the expectations for him.
He made him the underdog.
It's Jahad.
Those foreign fighters, they're coming.
They're coming to every country that was on one of those folders.
And you guys, in your quest for propaganda to show him as being humble, drew attention
to it.
You made sure that everybody saw his orders.
It might be time to step back from technology and start looking at intent again.
Get some real human intelligence going on.
Unless, of course, that's not really the goal anymore.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}