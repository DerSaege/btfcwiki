---
title: Let's talk about the advice I give my sons....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=qxM3txDfMSg) |
| Published | 2019/04/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Received questions on advice for sons on living life.
- Emphasizes that there's no right way to live life and to ignore critics.
- Stresses the importance of embracing both good and bad experiences.
- Shares a personal anecdote about a defining canoeing adventure at 17.
- Encourages having adventures while young.
- Advises not to regret the things done in life but the risks avoided.
- Urges to take risks as anything worthwhile involves some risk.
- Warns against living a safe, predictable life within the bell curve.
- Advocates for being selective with friends and valuing true friendship.
- Emphasizes the significance of attitude in facing life's challenges.
- Encourages continuous self-improvement even in difficult times.
- Provides advice on love, advocating for following one's heart over peers' influence.
- Reminds to remain curious and constantly seek knowledge to grow.
- Advises developing skill sets for independence and freedom.
- Encourages asking the right questions and taking actions based on beliefs.
- Urges uplifting all people, avoiding putting others down for personal gain.
- Reminds that individual choices shape the world and to be mindful of this impact.
- Mentions being prepared for deep tests in life and how life experiences lead up to them.
- The advice for his daughter mirrors the advice given for sons.

### Quotes

- "Embrace them all, learn from them all, experience them all."
- "It's the risks I didn't take that I regret."
- "Your beliefs mean absolutely nothing if you don't act on them."
- "Don't let others frame your thought."
- "Make sure you consider that when you're making them."

### Oneliner

Beau advises on embracing experiences, taking risks, valuing attitude, and shaping the world through actions and choices.

### Audience

Young adults

### On-the-ground actions from transcript

- Embrace both good and bad experiences (implied).
- Have adventures while young (implied).
- Take risks in life (implied).
- Be selective with friends and value true friendship (implied).
- Keep moving forward in challenging situations (implied).
- Follow your heart in love and ignore peer influence (implied).
- Remain curious and seek knowledge constantly (implied).
- Develop skill sets for independence and freedom (implied).
- Ask the right questions and act on beliefs (implied).
- Avoid putting others down for personal gain (implied).

### Whats missing in summary

Guidance on embracing experiences, taking risks, valuing attitude, making impactful choices, and shaping the world through actions.

### Tags

#LifeAdvice #TakingRisks #EmbracingExperiences #Friendship #SelfImprovement


## Transcript
Well, howdy there, internet people, it's Bo again.
So, I've had questions, message to me from young men,
asking me what advice I give my kids, my sons.
Well, first, there's no right way to live your life.
Somebody will always have something to say about it.
Ignore them.
They mean nothing.
I guess it's pretty important to understand that you're not
getting out of life alive.
If you think you are, we probably
need to talk about that rather than general life advice.
In that vein, don't shirk away from adventures.
I'd suggest the experiences of life
are more important than the material.
Good and bad experiences.
Embrace them all.
learn from them all, experience them all.
Some experiences have an expiration date.
When I was 17 or so, I canoed a couple hundred miles down this river.
It was pretty defining.
I tried to recreate that experience later in life.
I've been on that river a dozen times since.
It was never the same.
That adventure took place when the world was still new to me.
hadn't seen as much. So have your adventures when you're young. They'll mean
more. I don't regret many things I've done in my life. I regret the things I
didn't do. It's the risks I didn't take that I remember. Anything worthwhile will
have some risk associated with it. It could be physical, it could be emotional,
It could be psychological, or it could be that all devastating blow to your ego.
Don't go through your life without getting hurt.
Only experiencing the life available within the bell curve is just a waste of oxygen.
Take advice, but let nobody, especially your current friends, determine who you get to
be.
The only thing worse than the decisions you'll make when you're younger is the decisions
made by a group of people your age.
Be selective with who you call friend.
For the term to have any meaning, you have to be willing to put it all on the line for
them, and then for you.
Your attitude is more important than anything.
If you live a full life, you'll find yourself down, you'll find yourself beat up, you'll
find yourself in bad situations. Keep moving forward. Never stop improving yourself even
when it seems pointless. When it comes to love, love who you want, how you want. Don't
let your heart ignore your head or your head ignore your heart, but certainly ignore your
friends. Don't let your peers influence who you love. They don't have it any more
figured out than you do. Using the consensus of people you won't see in five years or
ten years to determine who you spend the rest of your life with? Well that's just stupid.
At some point, somebody will rip your heart out and stomp on it. Cool. If it never happens,
failed at love. At some point, somebody you are completely infatuated with will not be
interested in you. Move on. I don't even chase my whiskey. Remain forever curious about everything.
No knowledge is wasted, and that knowledge that you gain should change you. If you can't
Don't look back on the person you were five years ago and see a marked change.
You've wasted those five years.
Beyond the philosophical and political, strive to develop your skill set as much as possible.
Skills beget independence and independence begets freedom.
The answers are only important if you ask the right question.
Don't let others frame your thought.
Always ask the bigger question that's being ignored.
And it doesn't matter what you believe.
Your beliefs mean absolutely nothing if you don't act on them.
Make the move to uplift all people.
Don't be afraid of competition.
Putting someone else down doesn't actually elevate your position.
And remember that you will change the world.
It's not some motivational clich??.
The choices you make will shape the world.
Make sure you consider that when you're making them.
At some point in your life you'll be tested on a deep level.
Remember just as every other moment in your life, your entire life prepared you for it.
It all led up to that.
The advice for my daughter, it's the same.
Anyway, it's just a thought.
Y'all have a good night!

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}