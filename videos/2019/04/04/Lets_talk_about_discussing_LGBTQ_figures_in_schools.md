---
title: Let's talk about discussing LGBTQ figures in schools....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=VlYjoyyNX4w) |
| Published | 2019/04/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A book was read in school, and a parent got outraged because the historical figure was gay.
- The outrage stemmed from discomfort with discussing gay people in school.
- Beau questions what will happen when children learn about uncomfortable topics like genocide or nuclear war if discussing a historical figure being gay is uncomfortable.
- Beau provides examples of significant historical figures who were gay, such as Alexander the Great, Julius Caesar, Michelangelo, Sally Ride, and many others.
- He argues that excluding the mention of gay people in history erases significant contributions and diminishes the richness of historical knowledge.
- Beau points out that homosexuality has existed for thousands of years and doesn't need to be "normalized" as it is already a normal part of society.
- He offers to have a face-to-face talk show to address the issue with those uncomfortable discussing gay historical figures.
- Beau underscores the importance of recognizing the presence and contributions of gay people throughout history, urging people to accept it as a part of reality.

### Quotes

- "History would get really, really, really bare if we didn't talk about gay people."
- "It's almost like they're here and you need to get used to it."

### Oneliner

A parent's outrage over a gay historical figure prompts Beau to challenge the discomfort with discussing LGBTQ+ history, stressing its integral role in shaping our past and present.

### Audience

Teachers, parents, students

### On-the-ground actions from transcript

- Educate yourself and others on LGBTQ+ history (implied)
- Advocate for inclusive and diverse historical education in schools (implied)

### Whats missing in summary

The emotional impact and depth of understanding LGBTQ+ history and its significance in educational settings.

### Tags

#LGBTQ+ #History #Education #Inclusivity #Acceptance


## Transcript
Well, howdy there, internet people, it's Beau again.
So a book was read in the school and a parent got mad, outraged, actually.
That's the word in the headline, outraged.
Which is fitting because this is the outrage of the week, something for people to argue
about and make it seem like it's new and this is just something that is just coming up because
of the liberal gay agenda.
Apparently the parent was uncomfortable because the historical figure was gay.
Okay.
Uncomfortable.
I cannot imagine what's going to happen when your child actually starts learning about
history.
The extermination of entire peoples, nuclear war, genocide, slavery.
things are uncomfortable to talk about. The fact that somebody was gay, not so much. Not
so much. But let's just, for the sake of argument, attempt to apply this standard.
We're not going to talk about gay people in school. Okay. Alexander the Great, so we're
not going to talk about the Greeks. Julius Caesar can't talk about the Romans. Michel
Plangelo, Leonardo da Vinci, there goes the Renaissance, Sally Ride, first female astronaut.
Alan Turing, not going to talk about technology.
Florence Nightingale was gay.
Walt Whitman, Oscar Wilde, not going to talk about the arts.
History would get really, really, really bare if we didn't talk about gay people.
about gay people in school will not normalize them. They've been around
thousands of years. It's not something that needs to be normalized. Something
that has existed that long, it's pretty normal. If you'd like, I'd be happy to go
on like a talk show and talk to you about this in person. I doubt you'd go on
Ellen though.
That list, it's just one I came up with off the top of my head.
It's almost like homosexuality's been around a really, really long time.
It's almost like there's been a whole lot of homosexual people that have done some pretty
amazing things. It's almost like they are part of history, and if you're going to
discuss history, you're going to discuss anything of importance, you're going to
run across some gay people. It's almost like they're here and you need to get
used to it. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}