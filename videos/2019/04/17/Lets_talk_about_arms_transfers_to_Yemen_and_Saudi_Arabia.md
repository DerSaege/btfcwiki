---
title: Let's talk about arms transfers to Yemen and Saudi Arabia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=g5xEa-nOdCI) |
| Published | 2019/04/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Introduces the topic of Yemen and focuses on the legal framework of U.S. arms transfers.
- Explains the importance of understanding the legalities separate from Trump's veto on a resolution.
- Points out the attempt by the U.S. government to establish a moral framework within arms export laws.
- Describes the dire humanitarian crisis in Yemen, comparing it to one of the worst since World War II.
- Talks about the indiscriminate killing of civilians, especially children dying of starvation.
- Examines U.S. laws like the Arms Export Control Act, Foreign Assistance Act, Presidential Policy Directive 27, and CATP regarding arms transfers.
- States that the U.S. is violating its own laws by transferring arms to Saudi Arabia for use in Yemen.
- Addresses the constitutional issue of Congress's authority over making war and supplying arms in conflicts.
- Raises questions about allowing a president to bypass laws and stresses the importance of upholding the nation's laws.

### Quotes

- "We're violating our own laws by doing it."
- "Do we allow a president to just run roughshod over laws?"
- "It was illegal then, and it's illegal now, and it needs to stop."

### Oneliner

Beau delves into Yemen's crisis, dissecting U.S. arms transfer laws, revealing violations, and questioning presidential authority.

### Audience

Policy makers, activists

### On-the-ground actions from transcript

- Contact your representatives to urge them to uphold laws on arms transfers (implied)
- Support organizations advocating for human rights and peace in Yemen (implied)

### Whats missing in summary

Deeper insights into the moral implications of U.S. arms transfers to Saudi Arabia for the Yemen conflict.

### Tags

#Yemen #USLaws #ArmsTransfers #HumanRights #ForeignPolicy


## Transcript
Well, howdy there, I don't know people, it's Bo again.
So tonight, we're going to talk about Yemen.
What we are not going to talk about is good guys and bad guys, because it doesn't matter.
We're going to talk about the legal framework that allows the U.S. to transfer arms, because
that's what's important.
And this is important completely separate from Trump's veto of the resolution.
It really is.
But as that veto gets discussed, we need to know about these frameworks and understand
whether or not we're breaking our own laws anyway.
I know there's going to be somebody out there going, Bo, you don't care about the law.
You care about morality.
You always talk about legality versus morality.
I do.
Oddly enough, as you're about to find out, US arms export laws are one of the few places
where the U.S. government attempted to establish a moral framework within the laws.
It's really bizarre, but you'll find that out as we go along. Before we get talking about that,
you have to understand a little bit about the political situation. Now in, I want to say 1990,
I could be off by a year or two, North and South Yemen became Yemen. The fighting started then.
It came to a boiling point and reached U.S. news and became a topic in 2014.
The government the U.S. believes is legitimate is in exile. They're not in the country.
The Saudis are actually running the war for them. That's who's getting the weapons. That matters.
Now, what's going on on the ground in Yemen?
It may be the worst humanitarian crisis since World War II.
It's a toss-up between that and Syria, to be honest.
We're talking about the indiscriminate killing of civilians by Saudis as just a daily occurrence.
In hunger as a weapon, it is estimated that anywhere from 85,000 to 110,000 children under
the age of five have died of starvation.
This is not a normal war.
This is not blue force, red force, good guys and bad guys.
That's important, and it becomes important because of the way our laws are structured.
mechanisms does the US have to transfer arms to a foreign power? The first of
which is obviously the Arms Export Control Act. Now, it sets out certain
criteria in which the US can give a foreign power arms, one of which is to aid
in legitimate self-defense. Now, even though the government is in exile and
we're actually giving the weapons to the Saudis to run the war, the government in
Exile Act asked for them. Now, I know some people are going to say, it's not the legitimate government,
it's not, you know, none of that matters. The US government says it is, and under the law, that's what matters.
So, as far as that's concerned, there is a criteria that is met for us to transfer arms.
However, we cannot transfer arms unless they are being used for legitimate self-defense.
legitimate self-defense means the response is necessary and proportional.
If it is not necessary and proportional, it is not legitimate self-defense.
In 2017, the American Bar Association said, no, it's not.
It's not even close to legitimate self-defense.
And that has a lot to do with the indiscriminate killings.
Now it should be noted that there was an attempt to change this in 2017.
DOD sent advisors to Saudi Arabia.
Their entire purpose basically was to teach the Saudis how not to kill innocent people.
And apparently they failed because nothing has changed.
So the Arms Export Control Act is out.
That is not a legitimate method, a legal method to transfer arms to a foreign power because
it is in violation of that portion. The next is the Foreign Assistance Act. Now
under that we cannot transfer to any power, any government, any entity that is
consistently violating human rights. If you know nothing about Saudi Arabia, know
this, they're consistent violators of human rights. And you don't have to take
my word for it, in 2016, the State Department put out a report specifically related to the
Foreign Assistance Act about Saudi Arabia and said, no, we can't use this.
They are consistent gross violators of human rights.
So that one's gone.
Then we have Presidential Policy Directive 27, which basically says, we can't transfer
arms if they are going to contribute to violations of international law when it comes to humanitarian
issues or human rights abuses.
Starvation of 100,000 kids, that pretty much kills that dead in the water.
Now that one is causal, and that's an important difference.
Under that directive, the arms themselves have to be related to the act.
Under the Foreign Assistance Act, they don't.
We just can't transfer to anybody that's a human rights abuser, regardless of what
the weapons are used for.
And then CATP, which is the Conventional Arms Transfer Policy, it's got the same rolling
as the presidential policy.
We cannot transfer to bad actors.
That's what it boils down to.
Those are the methods to transfer arms under U.S. law.
We can't under any of them.
We're violating our own laws by doing it.
The President has said that Congress is attempting to undermine his authority as Commander in
Chief, I guess.
terrifying because civics 101 Congress has the authority to make war supplying
arms to one belligerent in a conflict is historically seen as an act of war it's
article 1 section 8 clause 7 of the US Constitution gives the power to make war
to Congress not the president he gets to be commander-in-chief once the war is
declared. That's how that works. So with Congress saying, we don't approve of this,
and him vetoing it, that's pretty unconstitutional. So that's where
we're at. Legally speaking, we can't transfer them arms. Completely,
completely separate from this resolution. Our own laws do not allow the transfer
arms to the Saudis, especially for using Yemen.
So the question then becomes, do we allow a president to just run roughshod over laws?
Do we allow him to dictate law or are we a nation of laws?
I know somebody's going to say, well, you know, Obama sent weapons too.
He did, and it was wrong then as well.
It was.
There was no justification for it.
It was a slightly different scenario, it really was, but not different enough to matter, legally
speaking.
It was illegal then, and it's illegal now, and it needs to stop.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}