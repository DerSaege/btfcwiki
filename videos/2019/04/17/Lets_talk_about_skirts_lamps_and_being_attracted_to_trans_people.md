---
title: Let's talk about skirts, lamps, and being attracted to trans people....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=VJ54ZqI4EDY) |
| Published | 2019/04/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Responding to a question from a heterosexual man who was briefly attracted to a trans woman and wondered if that makes him gay.
- Advocating against using terminology that can harm the transgender community.
- Pointing out the absurdity of questioning one's sexuality based on brief attraction to a trans woman.
- Suggesting that being overly concerned about your sexuality might be a better indication of being gay.
- Emphasizing that being attracted to someone who is trans does not automatically mean you are gay.
- Asserting that being gay is not a negative thing and should not be stigmatized.
- Encouraging individuals to embrace who they are and live their lives without worrying about societal norms or labels.
- Urging people to let go of unnecessary concerns about their sexuality and just be themselves, regardless of societal expectations.
- Reminding everyone that it is 2019 and people's sexual orientation should not matter to those who truly care about them.
- Encouraging individuals to embrace their uniqueness and live authentically without fear of judgment or stereotypes.

### Quotes

- "Stop with this. That is not good terminology because it reinforces the very question you're asking."
- "Just be you, live your life, and let go of all of this."
- "Embrace the weird, okay? Just live your life."

### Oneliner

Beau addresses misconceptions about sexuality, advocating for self-acceptance and rejecting harmful terminology in 2019.

### Audience

All individuals questioning their sexuality or facing societal pressures.

### On-the-ground actions from transcript

- Embrace your uniqueness and live authentically, disregarding societal norms (exemplified).
- Let go of unnecessary concerns about your sexuality and just be yourself (exemplified).

### Whats missing in summary

The nuances and depth of Beau's message on acceptance and self-discovery can best be experienced by watching the full video.

### Tags

#Sexuality #LGBTQ+ #SelfAcceptance #GenderIdentity #SocietalNorms


## Transcript
Well howdy there internet people, it's Bo again.
So I got a question that I feel like I have to answer.
The question goes something to the effect of,
Bo, I'm a heterosexual man.
I was on the street the other day and I saw a woman in a skirt and I turned and checked
out and realized, dun dun dun, she's trans, am I gay?
Now if you're the person that sent this, you're probably saying, well, he might be
talking about my message, but that's not how I said it.
You're right, it's not.
Stop calling them that.
They have enough people in the world who want to hurt them simply for being them.
They don't need society at large adopting terminology that reinforces the idea that
that they have somehow trapped you and changed your sexuality and damaged your masculinity.
You hear about this all the time where a guy whistles at some woman walking down the street
then finds out she's trans and then beats her to death.
Stop with this.
That is not good terminology because it reinforces the very question you're asking.
that question. I'm a heterosexual man and I was briefly attracted to a person I
believed was a biological woman. Am I gay? No. No. Of course not. Of course not.
I'm gonna take this a step further. I think we've all seen photos of inanimate
objects that appeared to be part of a woman, whether it be apples or a lamp that looked
like legs because of the way the lighting was or whatever.
When you saw that photo and you were like, those are nice legs, and then you found out
it was a lamp, did you question whether or not you were really attracted to lamps?
Of course not, because that's not how this works.
That's not how this works.
In fact, to take this yet another step further, I would suggest that being this concerned
about your sexuality is probably a better indication that you are in fact gay than being
briefly attracted to a trans woman.
I have known a lot of guys in my life who have done that, you know, they look, they're
like, hey, she's cute, wait a second, oh well.
At no point were they like, am I gay?
Because they're not, and they're secure in that.
question arises only if you're not. And I'll even take this another step further.
You may even know she's trans and still be attracted to her. That still doesn't necessarily
mean you're gay. Now that topic is probably better left to somebody who is better equipped
to handle the inevitable comment section that would arise from that. But yeah, even that
That doesn't necessarily mean that you're gay.
And then there's the underlying statement in all of this, is that somehow being gay
is bad.
It's not.
It's not.
I mean, it is 2019.
It is 2019.
Look, nobody who matters cares, okay?
nobody who cares matters nobody do you just be you live your life live your
life and let go of all of this I mean maybe maybe you are gay maybe you
are it's not it is not the end of your life it doesn't mean that you're not
masculine. It doesn't mean anything that this question kind of infers. Just be
you, you know, and I know if you are from a very traditional upbringing, you're
like, well that's weird. Who cares? It is 2019. Embrace the weird, okay? Just live
your life. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}