---
title: Let's talk about Ryker Glance and warning signs....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=jnTvWDMu960) |
| Published | 2019/04/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Two-year-old Ryker Glantz survived a horrifying incident where his father shot him in the face with a shotgun after a pistol malfunctioned.
- Beau stresses the importance of recognizing warning signs in domestic abuse situations, as they can escalate suddenly.
- Warning signs include jealousy, possessiveness, unpredictability, bad temper, animal cruelty, verbal abuse, extreme controlling behavior, and more.
- Beau urges victims to seek help from shelters and organizations and stresses the significance of getting out before situations escalate.
- Financial control, limiting access to funds, accusations of infidelity, and harassment at work are also warning signs of domestic abuse.
- Beau encourages everyone to be aware of these signs and act early to prevent potentially tragic outcomes.
- Despite not knowing the specifics of Ryker's situation, Beau advocates for recognizing warning signs and taking action early.
- Beau mentions a GoFundMe for Ryker's mom to support her in being with her son during his recovery.
- Ryker's survival is described as winning "every tough guy competition forever" and serves as a reminder to pay attention to domestic abuse warning signs.
- Beau concludes by encouraging viewers to take stock of the warning signs and to act early to prevent domestic abuse situations from escalating further.

### Quotes

- "The key is to get out before then. There's always a way out."
- "Left unchecked, warning signs always end bad, it always ends bad."
- "Get out early, get out early."
- "You win every tough guy competition forever."
- "It's the perfect opportunity for everybody to kind of take stock of these warning signs."

### Oneliner

Beau stresses recognizing warning signs, urging early action in domestic abuse situations to prevent tragic outcomes.

### Audience

Supporters of domestic abuse victims.

### On-the-ground actions from transcript

- Support Ryker's mom through the GoFundMe to help her be with her son during his recovery (suggested).
- Educate yourself on domestic abuse warning signs and ways to help victims (suggested).

### Whats missing in summary

The emotional impact and urgency conveyed by Beau's storytelling and the importance of community support for victims of domestic abuse. 

### Tags

#DomesticAbuse #WarningSigns #EarlyAction #SupportVictims


## Transcript
Well howdy there internet people, it's Bo again.
So tonight we're going to talk about a young boy named Ryker Glantz.
He's two and according to reports, his mom and dad got into an argument.
In that argument, his dad took a pistol, stuck it to his head and pulled the trigger.
weapon malfunctioned. Mom grabs him, runs outside, sticks him in a car seat. About
the time Dad comes out with a shotgun and fires. It hits him, according to reports,
took off half his face, and he survived. He's currently in critical condition, but
But he has survived.
Reason I'm bringing this up is because in a lot of domestic abuse situations it comes
out of nowhere.
We don't know what happened.
I don't know what happened in this situation.
But a lot of times there are warning signs, but they seem mundane.
They can handle it.
And then all of a sudden it hits a boiling point.
Key is to get out before then.
There's always a way out.
There are shelters.
There are organizations that will help you get in touch with them.
There are ways.
What are the warning signs?
Jealousy, possessiveness, unpredictability, bad temper, obviously, punching walls, that
kind of thing.
animal cruelty, verbal abuse, extreme controlling behavior, and this goes down to the way you
present yourself to the outside world, the way you dress, all of this, antiquated beliefs
about the role of women in the world, victim blaming, it's always your fault, sabotaging
Limiting the victim's ability to work, earn money, get an education, these types of things.
Sabotaging birth control.
Complete disregard for the other's drives, whether they want it or not, it doesn't matter.
Controlling all the finances.
Limiting your access to funds.
Accusations of infidelity and of course harassment at work.
These are the warning signs and left unchecked, they always end bad, it always ends bad.
Now we don't know what happened in this situation, but regardless of how this went down, it's
the perfect opportunity for everybody to kind of take stock of these warning signs, make
sure you know what they are, and when you start to see them get out early, get out early.
ago fund me for the for the mom so she can be with Ryker and Ryker son shotgun
blast to the face and you survived you win every tough guy competition forever
All right, that is amazing anyway, it's just a thought y'all have a good night

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}