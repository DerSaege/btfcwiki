---
title: Let's talk about why military assistance to foreign countries is bad for the drug war....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=n91z9-IGf7w) |
| Published | 2019/04/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains why it's detrimental for the US military to assist Central and South American countries in the War on Drugs.
- Mentions the oppressive nature of some governments and their military use against people.
- Describes how US military training involves learning tactics used by terrorists, leading to unintended consequences.
- Talks about how expensive military training is and how individuals can profit from it in the private sector.
- Illustrates how trained individuals sought after by cartels can compromise counter-narcotics efforts.
- Emphasizes the blurred line between cartels and governments in some countries, leading to corruption and collaboration.
- Points out that cartels operate like paramilitary organizations with intelligence and recruitment strategies.
- States that the dissemination of information to cartels undermines efforts and increases the cost of the War on Drugs.
- Criticizes the inefficacy of current approaches in the drug war, suggesting a shift towards harm reduction and treatment.
- Concludes by reflecting on the failure of the War on Drugs and the need for a different perspective.

### Quotes

- "The plants have won."
- "We've wasted the training, we've wasted all of that money, and it has done no good."
- "The war on drugs is a failure."
- "It's time to maybe look at it from a different perspective."
- "Y'all have a good night."

### Oneliner

Beau explains the pitfalls of US military assistance in the War on Drugs, citing unintended consequences and the inefficacy of current approaches, ultimately calling for a shift towards harm reduction and treatment.

### Audience

Policy advocates, activists

### On-the-ground actions from transcript

- Advocate for harm reduction and treatment approaches in drug policy (suggested)
- Support organizations working towards drug policy reform (implied)
- Educate others on the failures of the War on Drugs and the need for a different approach (implied)

### Whats missing in summary

In watching the full transcript, viewers can gain a deeper understanding of the unintended consequences of US military assistance in the War on Drugs, leading to a call for a more effective and humane approach to drug policy.

### Tags

#WarOnDrugs #USMilitary #CentralAmerica #SouthAmerica #HarmReduction #DrugPolicy


## Transcript
Well, howdy there, Internet people, it's Bo again.
So last night I got a question.
And when I first read it, I was like,
how does anybody not know this?
And then I thought about it.
I'm like, you know, if you were never in that community,
how would you know?
And the question was, why is it bad for the US military
to help these countries and the militaries of these countries
Central and South America in regards to the War on Drugs. Well, the first reason,
and I think the reason most people know, is that a lot of these governments are
very bad and the military is the tool that is used to oppress people. But there
are a couple other reasons that I realized people may not know. In the
In the United States, if you're a counter-terrorist, for example, you are trained in terrorism.
You're a terrorist.
And I don't mean that in a hyperbolic sense of, oh, the US military is a terrorist organization.
I mean that in the sense of you are trained on the theories and strategies and tactics
used.
You can run a terrorist campaign should you choose.
kind of that to kill a lion, you've got to think like a lion type of thing.
Counter-narcotics works the same way.
So what happens is one of these countries says, hey, we want your help, and we send
seventh group down to train them.
And they get down there and they train them.
And then there's this soldier, or this cop, whatever, who is a counter-narcotics expert.
He understands how it all works.
He's got it.
He understands the counter-narcotics side of it, and he understands the narcotics trade.
Then what happens?
I think most people in the U.S. know that military training is very expensive, and those
in the private sector will pay handsomely for it. I know a sergeant who
just left, he was making about 40k a year and now he's making about 206
months and that's legal work. Completely legal, approved everything. Five times as
much. Imagine how much you would make if it was illegal. So let's go back to that
country and that soldier a cop. He has that skill set and who wants it? The
cartels. They want to know what he knows because once they have that information
they can find a way around it. They can find a way to counter it, counter
counter-narcotics. We treat the cartels like a criminal enterprise. They're not.
They're a paramilitary organization in the way they're structured. They have
intelligence and counterintelligence guys. They go out and they find these guys
and they recruit them and down there they have the carrot and the stick. You
can come to work for us and we'll pay you a whole lot more and if you don't
we're gonna kill your family. In a lot of these countries the line between the
cartel and the government doesn't exist. It's hard to tell where one ends and the
next begins. Cartels control the judges, the cops. Why wouldn't he do it? He'd be
stupid not to. So he joins. He starts working with the cartels. Now the cartels
have all of that information and they disseminate it. This is why the war on
drugs keeps getting more and more and more expensive because we have to keep
coming up with new tactics and new technologies to counter the stuff that
we trained that was given away. If you go back to that whistleblower video, means
and methods, that's what's important to protect when you're talking about
secrets. How you do things is what's important, not what you did. So the
The cartels get all of that information.
And we've wasted it.
We've wasted the training, we've wasted all of that money,
and it has done no good.
This is why nothing's changed.
We're 40 or 50 years into the drug war,
and the plants have won.
Then another reason that providing this kind of assistance,
especially the more clandestine kind,
the type that isn't done by the Army,
but is done by intelligence agencies, isn't a good idea because a lot of times, I mean,
I can think of more than one instance where we ran dope for them.
Our assistance is not always good for the people of these countries.
It's good for the geopolitical situation for the U.S.
And then if you ever doubt how misused that training becomes, never forget, MS-13, the
feared MS-13, became MS-13 when a guy we trained took over, was trained by the School of Americanism,
something else now, but that's why they have the attitude that they have and
they have that unit integrity. Seems very military-esque. Why? Because it is.
The war on drugs is a failure. The plants have won. It's time to maybe look at it
from a different perspective and start thinking of harm reduction and treatment rather than
war and incarceration because we got beat by the plants.
Anyway it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}