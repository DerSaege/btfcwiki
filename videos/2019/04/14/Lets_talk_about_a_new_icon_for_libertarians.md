---
title: Let's talk about a new icon for libertarians....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=4HFd7130kOQ) |
| Published | 2019/04/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Questions the essence of libertarianism, describing it as freedom within societal boundaries and complete deregulation.
- Points out that libertarians believe legality is not equivalent to morality and value action over words.
- Contrasts the belief that anyone can achieve greatness through self-improvement in an ideal society with the reality of inherited wealth among today's affluent.
- Suggests Harriet Tubman as an alternative icon of freedom due to her journey from slavery to capability and action, understanding of morality, and fight against oppressive labor practices.
- Questions why Harriet Tubman is not celebrated by libertarians despite embodying values like freedom and empowerment over wealth accumulation.
- Encourages libertarians to shift their focus from idolizing the wealthy to guiding others towards freedom and assisting those in need, akin to Tubman's legacy.

### Quotes

- "It's freedom. As much freedom as is humanly possible within the confines of the society."
- "Action is more important than words."
- "Why on earth do Libertarians idolize the wealthy?"
- "If it's really about freedom, it would certainly seem that Harriet Tubman would be one of the ideals."
- "Seems like that [giving a hand up when needed] would be more in line with the rhetoric from libertarians."

### Oneliner

Beau questions libertarian principles, proposing Harriet Tubman as a better symbol of freedom and urging a shift from wealth idolization to aiding others.

### Audience

Libertarians

### On-the-ground actions from transcript

- Guide people to freedom and assist them when needed (implied)

### Whats missing in summary

The full transcript delves into the dichotomy between libertarian values and the idolization of wealth, prompting reflection on true freedom and empowerment.


## Transcript
Well hi there internet people, it's Bo again.
So tonight we're gonna talk to the libertarians among us.
I know there's a few of you.
Talk to you guys tonight, cause I got some questions.
First, I mean, what is libertarianism?
Nobody knows.
Let's be honest.
It's freedom.
As much freedom as is humanly possible
within the confines of the society. Complete deregulation of just about everything. Why?
Because libertarians know that legality is not morality. The two things aren't the same.
But what do they believe? What's their ethos? In this ideal society, somebody can come from
nothing and achieve great things as long as they become capable, as long as they work
on their own skill set, self-improvement, and then once they've mastered those skills,
they put them into action because action is more important than words.
Pretty much it.
You do all that, you become great.
And then other than that, about the only thing libertarians agree on is that taxation is
theft.
Pretty much it.
So with all of this in mind, why on earth do Libertarians idolize the wealthy?
I mean, let's be honest, today's wealthy, they didn't exactly come from nothing.
Most of that wealth is inherited.
They're not particularly capable, they're just born in a position of privilege.
Action versus words, no, not really.
They don't really know the difference between legality and morality.
If they did, we wouldn't have half the problems we have.
About the only thing you can agree with them on is that taxation is theft.
That's about it.
So I'm going to give you an alternative.
Somebody who I consider to be the ideal American.
Certainly, certainly, pretty much the American patron saint of freedom.
She came from nothing, came from nothing, she was a slave.
But she worked on her skill set, she became very capable, very capable.
And then she put it into action.
Harriet Tubman was definitely not somebody that just sat around and talked.
Certainly understood the difference between legality and morality.
I'm not sure that you can find a clearer example.
As far as taxationist theft goes, well, if you're upset about the feds taking 30% of
your labor, imagine how you would feel about a plantation owner taking all of it.
Pretty libertarian of her.
So my question is, why isn't she an icon of the libertarian movement?
Because she didn't die wealthy?
Is that really what libertarianism is?
Is it really just deregulation in the hopes of, well, then maybe I can make some money?
Or is it really about freedom?
If it's really about freedom, it would certainly seem that Harriet Tubman would be one of the
ideals.
In fact, if you modeled yourself after her, you might be a better libertarian.
If the drive for the accumulation of wealth was replaced with the drive to guide people
to freedom.
Now, of course, you're probably not going to be able to do it in the extreme way that
she did, but even figuratively, even figuratively, or even just giving somebody a hand up when
they need it.
Seems like that would be more in line with the rhetoric from libertarians, but that's
It's not what the idols represent.
Those people they look up to.
I've never understood why.
It's just something to think about.
It's just a thought.
y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}