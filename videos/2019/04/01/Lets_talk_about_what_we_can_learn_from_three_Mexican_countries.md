---
title: Let's talk about what we can learn from three Mexican countries....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=GZ8--13oe9E) |
| Published | 2019/04/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Fox News mistakenly referred to El Salvador, Guatemala, and Honduras as "three Mexican countries," prompting jokes and confusion.
- The decision to cut aid to these countries is concerning and may lead to more refugees, according to experts.
- Beau disagrees with the experts, believing that cutting aid could allow the people to take control of their countries back.
- The aid was cut because the countries didn't do enough to stop refugees from leaving, reminiscent of Cold War tactics.
- This decision implies that the government should prevent people from leaving during difficult times.
- Beau questions whether it's right for any government to have the power to restrict its citizens' movement.
- He brings up the potential dangers of granting such power to politicians, regardless of political affiliation.
- Exit visas, a control measure on citizens leaving, are still present in some countries but are not common.
- Beau warns about the dangers of giving politicians unchecked power over citizens' mobility.
- The transcript ends with a thought-provoking message about the consequences of granting too much power to political figures.

### Quotes

- "There are three Mexicos and Trump can't get any of them to pay for the wall."
- "The power you grant to your favorite politician is going to be there when the politician you hate takes charge."
- "Maybe nobody should have this kind of power."

### Oneliner

Beau questions the implications of cutting aid to countries and the government's role in controlling citizens' movement, urging caution in granting too much power to politicians.

### Audience

Conservative viewers

### On-the-ground actions from transcript

- Question the decisions made by politicians regarding aid and citizens' rights (implied)
- Advocate for transparency and accountability in government actions (implied)

### Whats missing in summary

Importance of questioning governmental decisions and being aware of potential implications of unchecked power.

### Tags

#Government #Aid #PoliticalPower #CitizensRights #MovementControl


## Transcript
Well, howdy there, internet people, it's Bo again.
If you're a conservative, I need you to stick with me for the first minute or so.
There's gonna be some jokes in it you're probably not gonna like, but
there's something really important for you at the end.
Seriously, not an April Fool's thing.
Okay, so we have to talk about the three Mexican countries.
If you don't know, yesterday, Fox News ran a segment, and
in the ticker, it said Trump slashes aid to three Mexican countries, even Fox News viewers
were looking at their screens like, what?
And it's hard to get a Fox News viewer to do that, but it happened.
Of course, they weren't actually talking about three Mexican countries, they were talking
about El Salvador, Guatemala, and Honduras.
The internet did not disappoint.
There were jokes all day, you know, my favorite being there are three Mexicos, Mexico, New
Mexico and certified pre-owned Mexico.
There are three Mexicos and Trump can't get any of them to pay for the wall.
Of course he's worried about Mexico, the country is multiplying.
It's all pretty funny, it really is.
But why he is cutting the aid is not funny at all.
It's pretty unnerving.
I don't want to get into whether or not it's a good idea to cut the aid.
Just about every expert says it's just going to create more refugees.
I kind of disagree, to be honest, with almost every expert.
I think that if the aid money is gone, the puppet strings are gone as well, and these
people can have their countries back and will stop messing them up.
the people will be able to realign their countries.
I know that's a little accelerationist, but that's honestly kind of how I feel.
But why did he cut the aid?
This is where it gets important.
Because they didn't do enough to stop the refugees from leaving.
Exit visas, something that went out more or less with the Cold War, the Iron Curtain.
That's really what we're talking about.
There's some countries that still have these today, but they're not really what we would
consider bastions of freedom.
So another way to say this is that he believes it is government's job to stop you from leaving
if things get bad.
Another way to say that would be he believes that it is his job as head of government to
stop you from leaving if things get bad.
Yet another way to say that would be the guy who wants to build a wall in a militarized
border along the southern part of the United States believes it is his job to stop you
from leaving.
Man, that's kind of unnerving.
And I know right now the Conservatives are like, Anne, USA, USA, America's number one,
why would I ever leave?
Yeah, okay.
And you trust Trump with his power.
I'm sure God Emperor Trump, he would never do you wrong.
But what about when he's out of office?
You trust Pelosi, Schumer, AOC, with that kind of power, with the ability to determine
whether or not you leave.
Maybe there's a reason these things went out of vogue.
Maybe there's a reason exit visas aren't really a thing anymore.
Even in most 10 pot dictatorships, you don't have them anymore.
I mean, let's see, Saudi Arabia still has them, Qatar, Russia, Belarus, those are the
only four I can think of.
I'm sure there's others, but off the top of my head, that's it.
Yeah, and there's Trump.
Let's bring it back.
How would you stop them from leaving?
They're giving up everything they know.
Everything they know, everything they own, they're already giving it up.
There's only one way to do it.
The power you grant to your favorite politician is going to be there when the politician you
hate takes charge.
Maybe nobody should have this kind of power.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}