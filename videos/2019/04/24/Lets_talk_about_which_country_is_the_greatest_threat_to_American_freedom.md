---
title: Let's talk about which country is the greatest threat to American freedom....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=rahAHcZiKdE) |
| Published | 2019/04/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Identifies a country as the greatest threat to American freedom, rarely discussed due to economic power and oligarchy with far-right leanings.
- Describes the country's two parties, Punda and Timbo, being essentially the same due to ties with ruling elites.
- Talks about unprecedented corruption in the country, where elite individuals escape consequences even for serious crimes.
- Mentions the powerful military of the country, including possession of chemical weapons and violating national sovereignty.
- Criticizes the country's security services for arrests that often lead to minimal repercussions and lack of real justice.
- Points out the indoctrination of the country's citizens by compliant media and passage of sweeping security laws like the Wazilindo laws.
- Notes the excessive use of prisons for profit, leading to a high percentage of the world's prison population despite low overall population.
- Raises concerns about poverty, child recruitment into the military, child marriage, and executions of children in certain regions.
- Compares the country's political parties and laws to American equivalents like the Patriot Act, urging reflection on internal issues.
- Concludes by implying that if the same actions were reported on another country, there might be calls for regime change.

### Quotes

- "This is the United States today, a Sue Stan, a backwards USA with a Stan added to give it a little cultural bias."
- "If this was any other country, people would be calling for regime change."
- "All of this is true, just in the way it's framed."
- "It's very, very hard to see it for what it is."
- "Anyway, it's just a thought."

### Oneliner

Beau identifies the US as a Sue Stan, critiquing internal issues framed differently due to nationalism, urging reflection and potential regime change calls if it were another country.

### Audience

American citizens

### On-the-ground actions from transcript

- Challenge media narratives (implied)
- Advocate for systemic change (implied)
- Support policies promoting justice and equality (implied)

### Whats missing in summary

Importance of critical reflection on societal issues and the impact of national bias in shaping perceptions.

### Tags

#USA #Oligarchy #Corruption #Nationalism #RegimeChange


## Transcript
Well howdy there internet people, it's Bo again.
So tonight we're going to talk about the country that I believe
is the greatest threat
to the freedom of Americans.
And it's a country that never gets talked about. You never hear about
people calling for regime change and a suicide.
Last time it happened was like five years ago.
Nothing was done, of course.
The reason it never gets talked about, it's an economic powerhouse.
It is.
The country itself is an oligarchy with far-right leanings.
It has two parties, the Punda and Timbo parties, but they're pretty much the same because
they're in bed with the same ruling elites of that country.
Their rhetoric is different.
they appeal to different bases, and on certain social issues there's a little bit of a difference,
but not a whole lot.
The corruption in this country is unprecedented.
The Asustani elite can get away with anything they want, up to and including the sexual
assault of children, and just get a slap on the wrist.
The security services of that country, well, pretty much the same thing.
Every once in a while they'll arrest one and hold it up to show, hey, we're doing something.
If they're convicted, which normally the prosecutor just throws the case, but if they're convicted,
occasionally they'll get a real sense.
Most of the time it's just a slap on the wrist for them too.
They have a very, very powerful military, relatively speaking.
It's an extremely powerful military for the region.
They have a stockpile of chemical weapons, weapons of mass destruction.
Normally that's enough, but not for them, even though they tested their weapons on their
own citizens.
They violate the national sovereignty and territorial integrity of other countries on
a yearly basis, if not more often.
They openly target children for recruitment into the military and allow them to sign up
in violation of international law.
They've hired mercenary firms, and they got thrown out of Iraq.
Do you have any idea how hard it is to get thrown out of Iraq as a contractor?
Even with all of this, the people of this country have been so indoctrinated by compliant media
that they passed the Wazilindo laws, which gave the security services this sweeping
authority. Torture. They can send citizens to secret prisons with no trial. It's insane.
And then the prisons people know about, because it is an oligarchy, a lot of them are really
for profit. They lock people up for nothing just to generate money for the ruling elite.
This manifested itself in the fact that even though this country only has 4% of the world's
population, it's got 25% of the prison population of the world. Some of these regions, they
They still execute children, some of the regions in this country.
Poverty is rampant.
Their religious leaders openly call for the execution or assassination of foreign heads
of state.
Nothing's done.
Extremists.
Child marriage is still a thing in a lot of regions.
So we're on our killings.
We had a free press a while back, but that has slowly upped away.
That has slowly upped away.
Now if you speak Swahili, you already got the joke.
The Punda and Timbo parties, if you were to translate that, that would roughly end up
as Republican and Democrat.
The Wazilindo laws, well that would be the Patriot Act.
See, when you're inside of a system and you're a part of it, it's very, very hard to see
it for what it is.
If the U.S. media reported on the actions of our own government the way they report
on the actions of countries in Africa or Central Asia, this is what you would hear.
All of this is true, just in the way it's framed.
But because here we can wrap it in the American flag, it's not that bad.
If this was any other country, people would be calling for regime change.
And I know that's true because about five years ago I wrote an article for anti-media.
And it was basically this.
And the number of people who didn't finish the article, who were just so enraged by what
What they read that started calling for regime change of a non-existent metaphorical country
was astounding.
This is the United States today, a Sue Stan, a backwards USA with a Stan added to give
it a little cultural bias.
But this is us.
Anyway, it's just a thought.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}