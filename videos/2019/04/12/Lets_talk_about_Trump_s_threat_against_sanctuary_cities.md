---
title: Let's talk about Trump's threat against sanctuary cities....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8Z3GFgXxF30) |
| Published | 2019/04/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump is considering releasing detained migrants into sanctuary cities, waiting for approval from President Miller.
- Beau urges Trump to stop considering and just do it, challenging his courage.
- He mentions that migrants statistically create less crime than native-born Americans.
- Beau questions the intention behind sorting violent migrants and the harm it may cause American citizens.
- He encourages Trump to bus in and drop off migrants in sanctuary cities.
- Beau expresses support for the idea, believing it will lower the crime rate and get migrants out of cages.
- He humorously points out that cities don't have walls around them, alluding to the lack of segregation.
- Beau concludes with a parting thought for his audience to have a good day.

### Quotes

- "Do it. Bus them in. Drop them off. It's fantastic."
- "Get them out of the cages, man."
- "But you do understand that these cities don't have walls around them, right?"
- "Stop considering it. Do it."
- "Y'all have a good day."

### Oneliner

Beau urges Trump to bus detained migrants into sanctuary cities to lower crime rates and challenges his courage, humorously pointing out the lack of walls around cities.

### Audience

Activists, concerned citizens

### On-the-ground actions from transcript

- Bus detained migrants into sanctuary cities (exemplified)
- Drop them off in sanctuary cities (exemplified)

### Whats missing in summary

The full video provides further context and elaboration on the impact of releasing detained migrants into sanctuary cities.


## Transcript
Well, howdy there Internet people, it's Beau again.
So we got to talk about Trump's threat.
He says that he is considering releasing detained migrants into sanctuary cities.
And by considering, we know that means that he's really just waiting for approval from
President Miller, who is really running the show now.
Stop considering it.
Do it.
What are you waiting for?
I don't think you've got the guts.
See if you're not a bigot, you understand that all you're going to do is lower the crime
rate. Statistically, migrants create less crime than native-born Americans. So unless
you plan on going through and just picking out violent migrants, which you're saying
sorting them is taking forever, unless you're going to do that to intentionally harm American
citizens, all you're going to do is lower the crime rate. Do it. Bus them in. Drop them
off. It's fantastic. I think it's a great idea. Get them out of the cages, man. Do it.
And I know that you're like 4D chess champion and everything, but you do understand that
these cities don't have walls around them, right?
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}