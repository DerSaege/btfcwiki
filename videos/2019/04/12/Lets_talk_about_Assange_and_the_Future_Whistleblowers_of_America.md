---
title: Let's talk about Assange and the Future Whistleblowers of America....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KMKM-6Q-Esc) |
| Published | 2019/04/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Assange is in custody and will be prosecuted, tried, and sentenced in the federal system.
- The focus should be on what future whistleblowers are learning from recent incidents involving Winter, Hammond, Karyaku, Snowden, and Manning.
- Whistleblowers are left with limited options due to lack of protection by the press or anonymous leak methods.
- Going to a foreign power becomes the only option for whistleblowers to disclose information.
- The US press complies with the US intelligence community in protecting means and methods.
- Disclosing means and methods can lead to serious consequences, including loss of life.
- US media's compliance with intelligence services shifts focus from illegal activities to the leak itself.
- The government's response to leaks should prioritize legal and moral behavior to prevent future incidents.
- The battle between disclosure of activities like waterboarding and means and methods obtained from reports will have serious implications in counterintelligence wars.

### Quotes

- "The U.S. government, their big brother in 1984, and like Winston said, they get you. always get you."
- "You can't go to the press. The press cannot protect you."
- "But see, now that they're going to end up going to a foreign power, that reporter, he doesn't have that patriotism."
- "We really need you to keep this secret. If you don't, this person will die."
- "We need to set aside our desire for punishment when it comes to this."

### Oneliner

Beau warns about the dangerous implications of whistleblowing and the limited options available due to lack of protection from the press, leading to potential loss of life by disclosing means and methods.

### Audience

Whistleblowers, journalists, activists

### On-the-ground actions from transcript

- Support and advocate for legal protections for whistleblowers (implied)

### Whats missing in summary

Importance of advocating for legal protections and ethical behavior in response to whistleblowing incidents.

### Tags

#Whistleblowers #PressFreedom #GovernmentAccountability #IntelligenceCommunity #Ethics


## Transcript
Howdy there internet people, it's Beau again.
The very word secrecy is repugnant in a free and open society.
It's a great quote.
Not really true, but it's a great quote.
So Assange is in custody.
What's going to happen?
I'm not talking about his specific case.
We know what's going to happen there.
They're going to prosecute him.
They're going to try him.
they're going to sentence him. That's what's going to go down barring something extremely
bizarre. It's the federal system. They pretty much always get a conviction. It's trial
by ambush. They don't have to really prove intent. The facts are fluid, for lack of a
better word. He'll get convicted. But the thing is, sorry Julian, at the end of the
day, it's not really that important in the grand scheme of things. What is important
is what is being taught to the future whistleblowers of America.
All of these recent incidents, Winter, Hammond, Karyaku, Snowden, Manning, they all send the
same message.
The U.S. government, their big brother in 1984, and like Winston said, they get you.
always get you. So, what's the takeaway? You can't go to the press. The press cannot
protect you. The methods for reporting information like this to Congress, they're lacking. You
can't do an anonymous leak. All of these things end up with you in a cage. So, if you run
across information that you just cannot bear the moral weight caused by keeping
it secret. What do you do? What is your option? There's only one out of those
people who's not in prison. You go to a foreign power. That's the option. That is
the option. It's the only one left on the table because of how we've handled these
cases. And that's pretty dangerous, not for the politicians of course, but for the assets
and agents that are out there in the field. It's pretty dangerous for them. See, the
US press is very compliant with the United States intelligence community. They are. The
Washington Post gets a story. They get documents. The CIA comes to them and says, hey, we know
you've got these documents we understand you're gonna run the story we need you
to obscure means and methods they do it means and methods um in most cases the
what isn't really important how is what the secret is it's typically not that
big of a deal how the secret was obtained or maybe the tactics and methods
that were used that are described in the document, that's what's important.
That's what you really have to secure.
That's what's extremely important because if you don't, people die.
The U.S. media, they're Americans.
They are Americans and the U.S. intelligence community uses that, plays on that patriotism.
We really need you to keep this secret.
If you don't, this person will die.
In the US media, they typically do it.
They typically do it.
Protecting means and methods is something that people have gone to extreme lengths to
do.
That they will throw themselves on grenades, figuratively and literally.
But see, now that they're going to end up going to a foreign power, that reporter, he
doesn't have that patriotism.
He does, but it's just not to the U.S.
So if they go to Russia and the Russian intelligence service comes to them and says, hey, we need
to see those documents, go in the arsenal, and they hand it over, then what?
Those means and methods are out there.
Some asset in Kabul dies.
Some agent in Tashkent dies.
Why?
Because some junior representative wanted to make a headline and call for the execution
of Snowden or whatever.
Crime and punishment, get tough, law and order and all that, except you don't actually care
about that because you don't prosecute the illegal activity that's contained in the leaks,
you just prosecute the leaker.
So those people who are overseas, who are literally living a double life, gathering
intelligence for this country.
Those are going to be the people who die because, well, people were embarrassed by a leak.
We need to set aside our desire for punishment when it comes to this.
Most of the stuff that was leaked, there's very valid reasons for it.
There were crimes, there were immoral activities contained in those leaks.
But because the US press is so compliant with the US intelligence services, that's not
the story.
The story is that the secret got out.
What is more likely to actually cause the death of U.S. servicemen?
Is it the disclosure of waterboarding?
Or is it the means and methods obtained from those reports?
This will probably be a very defining battle in the counterintelligence wars, and the U.S.
is going to lose it.
We've already set this example, everything I'm saying is after the fact, but we may want
to keep this in mind for the next leak and the one after, because there will be more
until the government starts behaving in a legal and moral manner.
Anyway, it's just a thought.
I'll have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}