---
title: Let's talk about books to read....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=uHMcLRjChvw) |
| Published | 2019/04/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the common request for book recommendations, Beau suggests reading ten types of books instead of specific titles.
- Beau explains that most literature is designed to provide wisdom, not just knowledge.
- He recommends starting with reading a religious text that is not your own, to understand other cultures.
- Beau suggests reading about countercultures to gain a different perspective on society.
- He advises reading books that portray the dark aspects of the human condition, making monsters human.
- Beau recommends exploring political ideologies through manifestos to understand manipulation.
- He encourages revisiting a book that deeply impacted you during childhood to reconnect with that wonder.
- Beau suggests reading about other people's heroes, even those you may see as enemies.
- He recommends reading critical books about war to break the romanticized view often portrayed.
- Beau advises reading about old stories and gods to understand the oral traditions and hidden messages.
- He suggests exploring dystopian futures towards the end to tie in themes from other recommended readings.
- Beau proposes reading about characters completely different from oneself to broaden perspectives.
- He concludes by suggesting reading banned books as they often hold valuable content.

### Quotes

- "Band books are the best books."
- "Reading something that is completely out of step with who you are and outside of your comfort zone can help you fill in the gaps."
- "Most literature is designed to give you wisdom, not knowledge."
- "Every year there's a whole bunch of books that get challenged in the United States to get removed from libraries, banned from schools, whatever."
- "The idea of literature is to expose you to things that you wouldn't experience in your life."

### Oneliner

Beau suggests reading ten types of books to gain wisdom and understanding, from religious texts to dystopian futures, challenging readers to broaden their perspectives.

### Audience

Book enthusiasts

### On-the-ground actions from transcript

- Read a religious text that is not your own to understand different cultures (implied)
- Read books about countercultures and dark aspects of human nature to gain different perspectives (implied)
- Revisit a book that impacted you in childhood to rediscover that wonder (implied)
- Read about other people's heroes and critical views on war to challenge romanticized notions (implied)
- Read banned books to discover valuable content (implied)

### Whats missing in summary

Detailed explanations of each book type Beau recommends

### Tags

#BookRecommendations #Literature #Wisdom #Perspective #Reading


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight I'm gonna answer a question
I get asked a lot in my inbox and in comment sections.
Give book recommendations, kind of.
Problem with book recommendations is most times
when you give one, the reader looks to that book
for knowledge, they either try to figure out why
it was recommended to them, or they pick out
details, so it can be discussed. And it becomes an exam. They study it rather than absorb
it. And that's all fine and good, but the problem is that most literature is designed
to give you wisdom, not knowledge. Knowledge is knowing that Frankenstein wasn't the monster.
is knowing that he was. So in that vein, rather than give you ten books to read,
I'm gonna give you ten types of books to read, the first of which is a religious
text that's not your own. If you're Christian, the Bible doesn't count, read
the Quran, read the Vedas. Do this for a couple of reasons. First, it's gonna help
understand other people, other cultures. Second reason is that, well, you may
notice some common messaging, some common themes, and that gulf between cultures
may shrink. Read a book about a counterculture, something you had no
chance to be a part of a group of people that existed outside of society so you
can get that lens of society through their eyes. Anything by Hunter S.
Thompson, The Valley of the Dolls, something like that. Read a book about
the dark and depraved aspects of the human condition, a book that attempts to
make the monster human. Frankenstein, Lolita. You read a book like that and it helps you
understand human monsters when you come across them. Read a book about a political ideology,
a manifesto, Adam Smith, Karl Marx, Emma Goldman, whoever. Understanding the way these
These normally very short little pamphlets impacted the world and trying to figure out
what it was that spoke to so many people can help you understand how people get manipulated.
Think back to when you were a kid, 10 to 15 years old, and a book that really spoke to
to you at that time. Read it again today. Try to tap into that childhood wonder. Try
to figure out why it spoke to you then. See if those reasons still exist or did society
at large beat that idealistic nature out of you. Read a book about other people's heroes.
by other people, I don't mean your neighbor, I mean your enemy.
Read a book that is favorable about somebody you don't like.
And in that same token, read a book that is critical of war or one of your heroes.
Here in the U.S. we have this very romanticized vision of war.
can't do everybody some good to read a book where every hero you come across in war is
bleeding to death, because that's more often than not the reality.
And then the idea behind reading a book that is critical of one of your heroes, well, we
tend to idolize people, and when we do that, we overlook all the horrible things they did,
And it doesn't matter who your hero is, they were flawed.
They did bad things, I promise.
It's a good reminder that people are in fact human.
Read a book about the old people, their stories, about the old gods, pagan stories, native
stories.
When you read it, remember that most of these stories were in oral tradition until somebody
wrote them down.
And try to figure out what they were really trying to say as they sat around that campfire
and told that story.
Almost all of them are metaphors of some kind.
And those thousand year old stories, a lot of the lessons are still pretty valid.
Read a story about a dystopian future.
I would recommend doing this towards the end because they, well, they tend to incorporate
a lot of the themes in the rest of these recommendations.
You see a counterculture most times.
You see a political ideology at work.
You see heroes from both sides.
You see all of this.
And you see how it plays out.
It helps you understand how people fall for cults of personality, how easily manipulated
they are into surrendering freedom, because that is the dystopian future.
And then the last one, well, read a book about a character you can't relate to in any way
shape or form.
that is absolutely nothing like you in a situation that you would never experience.
The idea of literature is to expose you to things that you wouldn't experience in your
life.
Reading something that is completely out of step with who you are and outside of your
comfort zone can help you fill in the gaps a lot of times with what you know and what
you think you know.
And then always remember that every year there's a whole bunch of books that get challenged
in the United States to get removed from libraries, banned from schools, whatever.
lists of them online. Read those. Read those. Band books are the best books.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}