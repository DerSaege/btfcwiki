---
title: Let's talk about why a Senator said nurses were playing cards at work....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=K8U-1Y5WJ6I) |
| Published | 2019/04/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Nurses are like the mafia in how quickly they spread news and defend each other.
- A Washington state senator opposed a bill for nurses' mandatory breaks, claiming they spend time playing cards.
- The senator's top campaign contributor is the Washington State Hospital Association.
- The hospital association posted about unintended consequences of the bill on April 8th.
- The senator proposed an amendment limiting nurses to work 8 hours in a 24-hour period with no exceptions.
- The hospital association claimed no knowledge of this amendment, suggesting possible scripting.
- Beau questions the coincidence and suggests the senator might be parroting hospital association interests.
- American politics are criticized for being influenced by money, where politicians act in favor of their donors.
- The senator's actions raise concerns about legislators being swayed by financial interests rather than representing the people.
- Beau recommends examining how money impacts legislators and their decision-making processes.

### Quotes

- "Nurses are like the mafia."
- "I mean because I know what to do. Anytime a politician says something dumb all you have to do is go look at their campaign contributions and you'll find out why they said it."
- "I have never seen anything like this before."
- "There's so much money in it that it's that easy to find out why your representative, who is supposed to be your representative, is proposing the bills and amendments that they are and why they're voting the way they are."
- "I think we should really take a look at how money is affecting our legislators."

### Oneliner

Nurses unite against a senator's opposition to mandatory breaks, revealing potential scripting by donors in American politics.

### Audience

Voters, Activists, Nurses

### On-the-ground actions from transcript

- Question your representatives about their campaign contributions and how they may influence their decisions (implied)

### What's missing in summary

The full transcript provides a detailed example of how money influences politics and the importance of holding representatives accountable.


## Transcript
Well, howdy there, internet people.
It's Bo again.
So I guess we need to talk about why nurses are just
sitting around playing cards at work.
So if you don't know anybody in the nursing community,
you have no idea what I'm talking about.
If you do, you already know about this,
and you understand that nurses are like the mafia.
If you offend one in Washington, the husband of one in Florida
hears about it 36 seconds later.
And that's what happened today.
A state senator in Washington was speaking in opposition to a bill that would basically
give nurses mandatory breaks.
And she went on about how this would adversely affect rural hospitals, underserved hospitals,
and they didn't really need them anyway because they spent a good portion of the day playing
cards.
Nurses all over the country flew off the handle.
me. I mean because I know what to do. Anytime a politician says something dumb
all you have to do is go look at their campaign contributions and you'll find
out why they said it. Sure enough when you pull up Maureen Walsh's what you
find out is that the Washington State Hospital Association is one of her top
campaign contributors and that in and of itself means nothing.
However, on April 8th, the Washington State Hospital Association posted
something saying the hospitals are now warning the bill could have unintended
consequences for staffing at rural hospitals. It's almost like they gave her
a script. This is a problem with American politics. Now maybe that's a
coincidence, but it gets better. It gets funnier. She went off script and she
She also proposed an amendment that would only allow nurses to work eight hours in a
24-hour period, and there's no exemptions for if they're in the middle of a surgery
or patient safety, anything like that.
So the Washington State Hospital Association released a statement that says, in a surprise
amendment to the nurse staffing bill, the Senate added a provision that would limit
nurses to working only eight hours in a 24-hour period, and it goes on.
But it ends with the Washington State Hospital Association did not support and had no knowledge
of this amendment.
No knowledge of the amendment.
Why would they have knowledge of the amendment?
It's not like they write them for her, right?
It's not like that this senator is literally just parroting what's on their blog.
I've been doing this a long time.
And yeah, this could all be a coincidence.
Really could.
But I have never seen anything like this before.
I mean that's funny.
That is funny.
The chain of events there, that's entertaining to me because I'm not a nurse.
But if you guys look up from your card games long enough, y'all might find that entertaining
too.
But this is the problem with American politics.
There's so much money in it that it's that easy to find out why your representative,
who is supposed to be your representative, is proposing the bills and amendments that
they are and why they're voting the way they are.
It's really that easy.
And I have to say, for $2,000, she's literally just proposing whatever the hospital association
wants.
I mean, granted, she went off script this time.
But other than that, that seems like really good service for really cheap.
Down here in Florida, to get service like that, it costs like $10,000, $15,000, $10,000.
Again, it could be a coincidence, but it's probably not.
my opinion. I think we can all learn something from this. I think that we should really take
a look at how money is affecting our legislators. Anyway, it's just a thought. Y'all have
Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}