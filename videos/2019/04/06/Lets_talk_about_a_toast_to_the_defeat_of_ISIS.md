---
title: Let's talk about a toast to the defeat of ISIS....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=c3fsPqMyqns) |
| Published | 2019/04/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Winding down his Friday night, contemplating a toast.
- Questions who to toast between President Trump, Department of Defense brass, and others.
- Criticizes politicians for making false statements to score political points.
- Suggests toasting the veterans of future wars.
- Mentions American soldiers and Kurdish soldiers killed after ISIS was declared defeated.
- Acknowledges ongoing threats and attacks by terrorist organizations.
- Criticizes the Department of Defense for putting the situation back to 2002 Afghanistan.
- Expresses frustration and disbelief at the current state of affairs.

### Quotes

- "Terrorist organizations do not need land to operate."
- "This isn't over, it's not over."
- "After all of this time, you have basically put us back in the situation we ran in Afghanistan in 2002."

### Oneliner

Beau contemplates a toast while criticizing false political statements, acknowledging ongoing threats from terrorist organizations, and expressing frustration at the current situation.

### Audience

Politically aware individuals.

### On-the-ground actions from transcript

- Support veterans and current soldiers (implied)
- Stay informed about political statements and actions (implied)

### Whats missing in summary

Full context and emotional depth.

### Tags

#USPolitics #Counterterrorism #ISIS #Military #CurrentEvents


## Transcript
Well, howdy there, internet people, it's Bo again.
So, uh, tonight I am winding down my Friday night, but I got a little bit less.
I want to make a toast.
Not sure who I want to make it to yet though.
I don't know if I should toast president Trump for being the visionary that he is
and declaring ISIS and Syria defeated when they're still pretty operational.
I don't know if I should toast him or the brass in the Department of Defense who ignored
their own experts and civilian advisors and chose to measure the potency of an insurgent
organization by how much land they hold, ignoring all insurgency theory.
Maybe I should toast the guy who apparently argued successfully that being forced to defend
and maintain land is not a drain on resources and something that typically prohibits terrorist
organizations from going on the offensive.
Or the guy that had to have written a memo stating that it doesn't matter that in 28
or 29 of the 31 countries in which the ISIS umbrella has operated in, they didn't hold
land and that ISIS is known for being able to strike far beyond their footprint.
I don't know.
I don't really want to toast any of those guys though.
Maybe we should toast the veterans of future wars, the kids of today who will end up fighting
this same enemy because rather than attempting to wage a successful and effective counterterrorism
campaign, the politicians of today have chosen to, well, just score political points by making
statements they know to be false to appeal to an uninformed base that absolutely refuses
to make any attempt to understand what is happening in these countries.
Or maybe we should toast that uninformed base, you know, those guys with the yellow ribbons
on their trucks next to the American flag bumper sticker who cheer USA, USA.
Because without their absolute refusal to inform themselves, we might not have as many
of those pretty flag covered coffins to pretend we care about.
But rather than that, I'd rather toast the American soldiers killed in the car bomb after
the president declared ISIS and Syria defeated.
The seven Kurdish soldiers who got taken out at the checkpoint, the SDF guy who just got
killed in the VEKALBORN IED attack, the Iraqis who are currently afraid to go to church because
they're waiting on an ISIS attack, the four Philippine troopers that got waxed today fighting
Abu Sayyaf, that's the ISIS offshoot in the Philippines.
I'd rather toast them, and all of the KIAs that are going to come after them, because
this isn't over, it's not over.
Terrorist organizations do not need land to operate.
That's stupid, that is stupid.
In fact, some would argue that leaving a small amount of land in their possession would lead
them to waste a lot of resources attempting to defend it.
Might even be where their leadership is.
We don't know where their leadership is anymore.
They've taken the hills.
So congratulations, Department of Defense.
After all of this time, you have basically put us back in the situation we ran in Afghanistan
in 2002.
Congrats.
It took a lot of work to accomplish that.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}