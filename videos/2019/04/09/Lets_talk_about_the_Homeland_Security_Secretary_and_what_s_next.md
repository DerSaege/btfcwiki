---
title: Let's talk about the Homeland Security Secretary and what's next....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=PJylYFS_gjI) |
| Published | 2019/04/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Reports indicate the former Homeland Security Secretary resigned because she refused to continue breaking the law for Trump.
- Trump referred to family separation as a deterrent, which Beau sees as a civil rights violation and cruel and unusual.
- A new Homeland Security Secretary is needed, someone willing to betray American values and take orders from Miller.
- Congress's role is to act as a check against executive power and stop potential dictatorships.
- Supporting Trump is supporting authoritarian rule and going against the Constitution.
- Soldiers don't fight for a flag or a song but for the ideals of the Republic and representative democracy.
- Beau criticizes uninformed patriotic citizens who have fallen for propaganda and sold out their country.
- He points out that working-class conservatives often vote for billionaires who don't truly represent their interests.
- Beau urges those not under Trump's spell to speak up against dangerous rhetoric and propaganda.
- He warns about the constant blame on immigrants and the need to call it out to prevent further deterioration.

### Quotes

- "A vote for Trump is a vote for authoritarian rule."
- "You have farmed off your sovereignty, farmed off your individual responsibility to some politician and you just believe whatever they say."
- "There's never been a democracy that hasn't committed suicide."
- "Soldiers don't go die for a flag or a song. In theory, they go fight for an ideal."
- "It's blindingly apparent, but people still support him."

### Oneliner

Former Homeland Security Secretary resigns over refusing to break the law for Trump, urging for vigilant defense against authoritarian rule and propaganda.

### Audience

Citizens, Activists

### On-the-ground actions from transcript

- Speak up against dangerous rhetoric and propaganda every time you encounter it (implied)
- Call out those supporting authoritarian rule without fail (implied)
- Educate yourself on political matters and history to understand the implications of supporting certain leaders (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the dangers of supporting authoritarian rule, urging citizens to be vigilant and speak out against propaganda to protect democracy.

### Tags

#AuthoritarianRule #Propaganda #Democracy #CallToAction #CivilRights


## Transcript
Well, howdy there, Internet People.
It's Bo again.
Homeland Security Secretary's out.
Reports are basically saying that, well, she didn't want to break the law for
Trump anymore, so she had to go.
Saying that he referred to family separation as a deterrent.
Man, that sure sounds like a civil rights violation to me.
Sure sounds like cruel and unusual.
But whatever.
She had to go.
But they're going to need a new Homeland Security Secretary.
So if you happen to be an immoral goon that has no problem selling out your country betraying
everything that this country stood for, well, you probably got a job at DHS, cabinet-level
position, all you got to do is take orders from Miller, because that's who's going
to be running the show now.
Now he's framing it, Trump is trying to frame this as, well, Congress isn't doing
their job.
They didn't change the laws to make them say what I want them to say so I can do these
illegal things.
First off, Mr. President, that is Congress's job.
Congress's job is to act as a check against executive power.
That's their job.
Their whole job is to stop would-be dictators.
Schoolhouse Rock put out a video on this a long time ago called I'm Just a Bill.
Even with your GPA, you'd probably be able to understand it.
And if it really is Congress's fault, my question would be, why did you have to can
your secretary?
The President of the United States is trying to undermine the very foundations of this
country.
He has been doing it since he took office.
This is extremely apparent, though.
It's blindingly apparent, but people still support him.
support for Trump. A vote for Trump is a vote for authoritarian rule. That's what
it is. It's a vote against the Constitution. It is a spit in the face of
every soldier who has ever died fighting for this country. Because in real life, my
patriotic friends, soldiers don't go die for a flag or a song. In theory, they go
fight for an ideal. And that ideal is the Republic, the representative democracy,
the checks and balances that have been in place since this country was founded, the
very things this president is trying to undermine.
Now in real life, that's not what every soldier's died for.
A lot of them, because of uninformed patriotic citizens who bought propaganda, they wound
of dying just to line somebody's pocket.
Because today's batch of patriotic, uninformed citizens fell for his propaganda, they have
sold out their country for a red hat, a cute slogan, and to own the libs, they're destroying
their country and cheering, because his propaganda scared him, scared him of some dude who just
walked a thousand miles, has nothing, no connections, no contacts, oh he's going to take your job.
If he could take your job with no connections, no contacts, barely speaking the language,
well that says more about you than him.
And that's the thing, that is the thing.
supposed to be a representative of democracy. So why does the working-class
conservative always vote in some billionaire? You think he's really gonna
represent you? He never does. He gets up there and he represents other men of his
ilk. And you? Well, they don't really care about you. Profits go up, your wages stay
the same. That's not Pedro's fault, man. It's your fault. It's your fault for voting
against your own interests. It's your fault for refusing to inform yourself. It's your
fault for pretending that you're a patriot and not understanding the very basic principles
of this country. It's your fault. Now to those who are not under this gentleman's spell,
have not fallen for his cult of personality, we're in an extremely dangerous time.
You have got to start speaking up constantly.
Every time you see it, you have to talk about it because it's going to get really bad.
With Miller now holding the reins, it's going to be constant.
There will be constant rhetoric, constant propaganda blaming immigrants for everything.
You have to call it out.
You're kind of the last line of defense before it gets real bad.
You've got to call it out.
It doesn't matter if it's a relative, a friend since grade school, a co-worker.
You have to call it out every single time without fail.
To those people who are still supporting this goon, there are people who understood history,
who understood politics and the way authoritarian countries develop, who said this was going
to happen.
You didn't believe them.
And here we are.
And here we are.
Because the President of the United States told you differently.
Well, here's the thing.
The President of the United States, this entire time he's been calling these asylum seekers
illegal.
And now, come to find out, just like those stupid libtards said, that is the law.
They aren't illegal.
That's how you claim asylum.
If he lied to you about that, something that was so easily checked, what else do you think
he lied to you about?
The question is, why didn't you check it?
You trust him?
Trust a politician.
That, that bumper sticker mentality, donkey, elephant, red, blue, that is what is destroying
this country because you have farmed off your sovereignty, farmed off your individual responsibility
to some politician and you just believe whatever they say.
There's never been a democracy that hasn't committed suicide and it's because of the
kind of citizens we have today.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}