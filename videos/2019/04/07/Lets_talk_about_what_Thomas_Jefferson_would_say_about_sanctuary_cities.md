---
title: Let's talk about what Thomas Jefferson would say about sanctuary cities....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=lEalJnnkOBo) |
| Published | 2019/04/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Mentioning sanctuary states and the Supremacy Clause.
- Recalling an unofficial war with France in 1798 due to unpaid debts.
- Explaining the Alien Friends and Alien Enemies Act of 1798.
- Referencing Thomas Jefferson's Kentucky resolutions of 1798.
- Explaining Jefferson's views on federal power over naturalization and immigration.
- Citing the 10th Amendment regarding states' powers.
- Asserting that Jefferson supported the idea of sanctuary states.
- Emphasizing the importance of historical context and accuracy.
- Warning against distorting history for convenience.
- Questioning appeals to the Founding Fathers' perspectives.
- Beau's closing remarks on historical authenticity.

### Quotes

- "History is a thing. It exists. You can't just change it and repackage it because you want to."
- "When you make an appeal to the Founding Fathers and their views on things, understand that most of that stuff was written down."
- "Thomas Jefferson, well he didn't even think I should exist."

### Oneliner

Beau explains the historical context behind sanctuary states and challenges distorted interpretations of history.

### Audience

History enthusiasts, policymakers, activists.

### On-the-ground actions from transcript

- Fact-check historical claims and narratives (implied).

### Whats missing in summary

Beau's engaging delivery and historical anecdotes bring clarity to the debate on sanctuary states and historical accuracy.

### Tags

#SanctuaryStates #History #FoundingFathers #ThomasJefferson #DebunkingMisconceptions


## Transcript
Well, howdy there, internet people, it's Bo again.
So we're talking about sanctuary states again, that's a thing, we're going to talk about
that some more.
And now the new packaging is that the founding fathers, well, they didn't build a country
for it to be ripped apart.
that, you know, the Supremacy Clause says that the Fed should have power over this.
Okay, well let's try to figure out what the Founding Fathers really would have thought
about it instead of just making stuff up.
So in 1798, the United States was involved in an unofficial war with France, it wasn't
big deal. It was a lot of naval confrontations. Why? Because we're the
United States and even way back then we didn't pay our bills. During the American
Revolution we incurred some debt with France. Well after the French Revolution
we're like whoa whoa whoa hang on a minute now we incurred these debts with
that other government the one that doesn't exist anymore we don't know you
would die. Understandably, the French were upset by this. We also reopened trade
with England, so they got a little hot. In response to the tension, Congress passed
the Alien Friends and Alien Enemies Act. These acts gave the executive branch
wide discretion in deporting people, throwing them out, basically if the
president thought they were dangerous they could go. So what would the founding
fathers think of that? I mean that was 1798, that wasn't too far afterwards, so
certainly one of them had to have said something, did, Thomas Jefferson, I think
most people have heard of him. He drafted something called the Kentucky
resolutions of 1798. He did this because he understood the US Constitution gives
the federal government power over naturalization, not immigration.
Naturalization is who gets to be a citizen. Immigration, well that's people
moving around. Jefferson said that alien friends are under the jurisdiction and
protection of the laws of this state wherein they are.
Then he cited the 10th Amendment, which basically says that any power that isn't specifically
spelled out in the Constitution and given to the federal government belongs to the states.
So we know that at least Jefferson would be okay with the idea of sanctuary states.
We know this because he said so.
History is a thing.
It exists.
You can't just change it and repackage it because you want to.
When you make an appeal to the Founding Fathers and their views on things, understand that
most of that stuff was written down, and Thomas Jefferson, well he didn't even think I should
exist.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}