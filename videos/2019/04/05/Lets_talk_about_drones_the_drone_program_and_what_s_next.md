---
title: Let's talk about drones, the drone program, and what's next....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Gxv03MXtNhw) |
| Published | 2019/04/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Drones are neutral technology but can be used for good or evil purposes.
- The U.S. drone program is portrayed as surgical strikes targeting command and control.
- Around 90% of those killed in drone strikes were not the intended targets.
- There is a possibility that some of the 90% killed were opposition forces near the target.
- The drone program has hit wedding processions and killed American citizens.
- Insurgency and terrorism thrive on overreactions from the U.S. government.
- For every opposition fighter killed, there is a family member or innocent ready to retaliate.
- The collateral rate in Afghanistan is high despite solid intelligence assets.
- Parents in Pakistan have withdrawn children from school due to fear of drone strikes.
- The secrecy and lack of transparency surrounding the drone program breed insurgency.
- The drone program is currently used as an assassination tool rather than surgical strikes.
- The lack of transparency suggests a potential increase in drone usage and collateral rates.
- Beau advocates for drones to be used similarly to manned aircraft missions, except for surveillance.
- The fear instilled by drone strikes, not freedom or culture, drives hostility towards the U.S.

### Quotes

- "They want to kill us because we've made them fear the sky."
- "The drone program is not being used as surgical strikes. It's being used as an assassination tool."
- "It's secret, it's ineffective, and it's breeding insurgency."
- "For every opposition fighter we take out, there's the family member of somebody we killed, some innocent that we killed, ready to pick up arms."
- "They want to kill us because we've made them fear the sky."

### Oneliner

Beau explains the flaws of the U.S. drone program: secrecy, collateral damage, and breeding insurgency, urging for transparency and responsible use.

### Audience

Policy makers, activists, citizens

### On-the-ground actions from transcript

- Advocate for increased transparency and oversight of the drone program (implied)
- Support initiatives demanding accountability for drone strikes (implied)
- Raise awareness about the consequences of drone strikes on innocent civilians (implied)

### Whats missing in summary

Deeper insights into the ethical implications and consequences of drone warfare, and the need for a reevaluation of current drone usage policies.

### Tags

#Drones #DroneProgram #USForeignPolicy #Transparency #Accountability #Insurgency


## Transcript
Well, howdy there, internet people.
It's Bo again.
So tonight we're going to talk about drones, the drone program, and what we can predict
is going to happen in the future with the drone program because of recent events.
The first thing we need to get out is that the drone is technology.
It's not inherently good or evil.
It is what it is.
A drone can bring you your medicine from Amazon, or it can send a warhead through your bedroom
window while you're asleep. It's technology. Depends on how it's used. Even in a wartime setting,
there are differences in how it can be used. So how is it being used today? The U.S. cast the
drone program as surgical strikes targeting command and control. Synonymous with a sniper's
bullet. Okay, taking out opposition command and control that that's a good
idea. It is. The problem is it's not a sniper's bullet. Snipers operate under
the cliche motto of one shot one kill. That is not how a drone works. We don't
have a solid study of the entire drone program because of the secrecy involved.
The best we have comes from the drone papers, which were obtained by The Intercept.
In that they have one solid data set, and it came from Operation Haymaker in Afghanistan at the end of 2012, beginning
of 2013.  What they found out was about 90% of those people killed in drone strikes were not the target.
target. Does that mean that 90% were innocents? No. It just means they weren't the target.
It is reasonable to assume that some of that 90% were opposition forces that just happened
to be in the orbit of the target at the time of the strike. That's a reasonable assumption.
However, we also know that the drone program has hit wedding processions.
It has killed American citizens.
So we can't assume that it is a high percentage that were actually opposition forces that
were just in the wrong place at the wrong time.
So let's bump the number up to 50%.
50% of those people killed were either the target or allied opposition forces.
Still unacceptable.
Why?
Because insurgency and terrorism, its whole purpose is to get the opposition government,
that's the US, to overreact, to kill innocents.
Because at a one-to-one ratio, for every opposition fighter we take out, there's the family member
of somebody we killed, some innocent that we killed, ready to pick up arms.
That's how insurgency grows.
That is how insurgency grows.
We know that this took place in Afghanistan with a 90% collateral rate, and that's pretty
terrifying because in Afghanistan we have solid intelligence assets.
have solid intelligence networks. The accuracy of the drone program in
Afghanistan should be higher than it is in Somalia, Yemen, Pakistan, all of these
places where we don't have these kind of assets. It is very reasonable to believe
that the collateral rates in these countries and everywhere else the drone
program is active, is even higher, if you can imagine higher than 90 percent.
In Pakistan, we know that parents have taken their kids out of school out of fear of drone
strikes.
I know right now Americans are like, we would never hit a school, you can leave that if
you want to, but even if you do believe it, it doesn't matter.
is just a PR campaign with violence. If the Pakistanis believe we would hit a
school, that's all that matters. That is all that matters. That's all it takes to
breed insurgency, to breed terrorism. It's a hot topic in the U.S. today. Think
about all of the things that have been proposed to stop school shooters. Now
Now imagine if it was a foreign power.
What would the proposals be then?
That's the reality of the drone program as it stands.
It's secret, it's ineffective, and it's breeding insurgency.
That's the reality.
How should it be used?
The same way all of our other aircraft are being used.
If you wouldn't commit a manned aircraft to the mission, with the exception of surveillance
missions, you probably shouldn't commit a drone.
Now Trump has issued an edict even further limiting the amount of information we the
public who are paying for these things can have about how they're being used.
We can only assume from that that the drone program is going to be ramped up.
They're not going to cut off the supply of information if they plan on scaling it back.
So we can assume that in the near future there will be, well, more deaths.
And with less transparency, we can assume that the collateral rates will be even higher.
It's not being used as surgical strikes.
It's being used as an assassination tool and a sloppy one at that.
These people, they don't hate us for our freedom.
They don't want to kill us because of baseball games and hot dogs.
They want to kill us because we've made them fear the sky.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}