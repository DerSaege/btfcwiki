---
title: Let's talk about the guy on the border getting arrested and beat up....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ih1Mate1Uvo) |
| Published | 2019/04/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Militia leader from the border got arrested and beat up while in confinement.
- The leader allegedly led a group that snatched migrants, sometimes posing as border patrols and pointing guns.
- Surprisingly, he was arrested on old weapons charges rather than for his alleged actions.
- Beau questions why the leader didn't follow the process to reinstate his gun rights.
- The incident in jail where he was assaulted was rumored to be due to him not adopting inmate culture.
- Beau criticizes the leader for not assimilating and causing problems.
- He questions why taxpayers should pay for the leader's medical bills after the assault.
- Beau touches on the lack of funding and issues in the prison system.
- Despite making jokes, Beau acknowledges that inmate assaults are no laughing matter.
- He hopes confinement will make the leader better and help him understand freedom.

### Quotes

- "He's just after the benefits."
- "It's never funny when an inmate gets assaulted."
- "All that separates him from freedom is an imaginary line."
- "Confinement can make you better or it can make you better."
- "I hope that he sees that wall and actually starts to begin to understand what freedom is."

### Oneliner

A militia leader gets arrested and assaulted in jail, sparking questions about gun rights, inmate culture, and freedom.

### Audience

Advocates for prison reform

### On-the-ground actions from transcript

- Advocate for better funding and reforms in the prison system (suggested)
- Support organizations working towards improving conditions in jails and detention centers (exemplified)

### Whats missing in summary

The full transcript provides a deeper insight into the repercussions of incarceration and the need for prison reform.

### Tags

#PrisonReform #InmateRights #MilitiaLeader #BorderIssues #Freedom


## Transcript
Well, howdy there, Internet people, it's Bo again.
So tonight we got to talk about that militia leader from down there on the border.
Y'all know the one I'm talking about.
He got arrested.
And then apparently in confinement he got beat up.
Now in case you don't know the one I'm talking about, we're talking about the guy that allegedly
introduced himself as a general.
He was in charge of that little crew down there on the border that was snatching migrants
they were coming across the border, sometimes allegedly misrepresenting themselves as border
patrols, sometimes allegedly pointing guns at people.
We don't know if any of that's true, but the funny part is, that's not what he got
picked up for.
He got picked up on some old weapons charges, which is surprising to me.
I don't understand it, so I got some questions.
You know, I don't understand why he just didn't go through the process.
The feds have an office for felons to send in an application and get their gun rights
reinstated.
Now I understand that that office doesn't get the funding it should, in fact it doesn't
get any, and that going through this process is pretty much impossible.
But it seems like he would be the sort that would, I don't know, just get in line.
Maybe I'm wrong.
And the funny thing is though, from what I understand, according to Inmate.com, the rumor
mill, the reason this incident occurred in jail when he got beat up really stemmed from
him not adopting inmate culture.
See, he didn't assimilate, but you know his sort.
Always causing problems, always causing crime, never wanting to assimilate.
The thing is, if he didn't want to adopt inmate culture, well he never should have come, right?
He should have just followed the law, that's what I think.
But you know his type.
You know what they're really after.
know what they're really about. He's just after the benefits. And there he is now
in the air conditioning, eating three meals a day, watching TV. We're paying
for it all. It's a better life than he lived out there in the desert. What about
those medical bills? For that assault, who's gonna pay for that?
Who's paying for that? Us. Why should we have to pay for that? He's a criminal. Now
we don't know that. He hadn't been to court or anything, but the fed said he was. Isn't
that all it takes? We're not supposed to have judges, right? Just round them up. Don't need
judges for this. I mean, that's what I heard. I can't remember who said that, though. Anyway,
I'm making a lot of jokes here, but the reality is it is never funny when an inmate gets assaulted.
really not. Jails, detention centers, and prisons are violent places. They are. There's
a lot that needs to be addressed in our prison system. Good news, though, is that he's a
real man, so I'm assuming he'll just stay and fix it. The funny thing is, I guess, the
funny thing is that he did get his wall. He got his wall and that wall was going
to have an impact on him. Now I looked at his case ideologically speaking I can't
say I think he deserves any real time for what he was charged with. There's no
victim to it. But that's not the way our society works today in large part
because of people like him who are afraid of what people might do based on
some irrelevant characteristic and therefore people get locked up for a
really long time or they're separated by a wall, a fence, a barrier. Now he's gonna
look at that wall and realize that all that separates him from freedom, from
relative safety from opportunity is an imaginary line that somebody said he couldn't cross.
That's it. I'm willing to bet that every day he dreams of escape. Dreams of getting
out. Confinement can do one of two things to you. It can make you better or it can make
you better. And it is entirely up to the inmate. It is entirely up to how they
react and how they respond to the situation they were put in. It's up to
him. I really hope that it makes him better. I hope that he sees that wall
And he actually starts to begin to understand what freedom is.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}