---
title: Let's talk about getting rid of judges and President Miller....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=obm3P-KoLCU) |
| Published | 2019/04/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President of the United States advocating for abandoning the rule of law and due process.
- President Trump seems to have abdicated his role to Stephen Miller, his puppet master.
- President Miller believes eliminating due process will expedite deportations.
- Mass deportations historically precede the removal of due process and are followed by concentration camps.
- Conservatives seem to be turning a blind eye to these dangerous actions because of team loyalty.
- Due process is vital to prevent false deportations and ensure fair legal proceedings.
- Beau warns that after targeting certain groups, the lack of due process may affect others next.
- The importance of upholding due process as a fundamental right enshrined in the Constitution is emphasized.
- Beau points out cases of American citizens mistakenly targeted for deportation without due process.
- The role of judges in ensuring fair legal proceedings and preventing wrongful deportations is underscored.

### Quotes

- "When they are done with the Mexicans from the four different Mexicos, they will come for you."
- "You are next, and you won't have due process."

### Oneliner

President advocating for abandonment of due process, conservatives silent, warning of dire consequences without fair legal proceedings.

### Audience

Advocates for justice

### On-the-ground actions from transcript

- Advocate for the preservation of due process rights (suggested)
- Educate others on the importance of due process in legal proceedings (exemplified)

### Whats missing in summary

The full transcript provides a detailed analysis of the potential consequences of abandoning due process and the importance of upholding this fundamental right for all individuals.

### Tags

#DueProcess #Immigration #ConstitutionalRights #Deportation #Advocacy


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight, I'm going to start you off with two quotes.
The first quote is, I have to get rid of judges.
The second quote is, and frankly, we should get rid of judges.
I know they're pretty much the same thing, I just want to be very clear.
This isn't something taken out of context, and it wasn't an off the cuff remark.
This is something he has said multiple times.
You can find that second quote on the White House website.
This is the President of the United States advocating for the abandonment of the rule
of law and the end of due process.
It's what it is.
Now, he can't really form a coherent sentence, but it does seem from context that he's only
talking about immigration judges.
That's nice.
I mean, it's a bonus.
He's not talking about suspending due process across the entire country just yet.
But that does appear to be what he's talking about.
The funny part about this is now when Trump is speaking, if you pay attention when he's
got his mouth open really wide, you can actually see Stephen Miller's hand in there running
it.
He's the White House advisor, he's the architect behind the travel ban, the separation of migrant
families, the reduction of rent, all of it, all of it was Stephen Miller.
And it certainly appears that President Trump has now abdicated to him and has willingly
become his puppet.
We are no longer dealing with President Trump, we are dealing with President Miller.
President Miller seems to be under the impression that getting rid of due process will speed
deportations.
That's probably true.
That is probably true.
Historically, mass deportations are preceded by getting rid of due process.
They're also followed by concentration camps.
That's where we're at.
Another step down that road and nobody seems to care.
Those people who should, in theory, care the most, the conservatives, the patriots, the
three percenters, they're ignoring it because it's their team.
Bumper sticker patriotism at its finest.
Now I know there may be some people out there who are saying, well, I mean, they're illegals.
why do they need to see a judge? What are you going to do when they come for you?
That's stupid. I'm not an illegal. I'm an American citizen. Who are you going to tell? There's no
judge. Do you want to trust ICE? You need to ask Lance Corporal Ramis Gomez how that worked out for
him. He was a Marine, served in Afghanistan, got picked up by ICE with his U.S. Passport.
Clearly, a U.S. citizen slated for deportation, or Peter Brown out of Philadelphia, or the
guy from here in Florida, can't remember his name, who was slated to be deported to
Jamaica, although he had only been there one time on a cruise.
Maybe it's not a good idea to just let bureaucrats do what they do, because there's going to
be a lot of false deportations without judges, without legal proceedings.
That's why due process is so important.
That's why it was enshrined in the Constitution.
I understand that, you know, you just like the we the people part so you can put it as
a profile picture, but the rest of the document is pretty important.
When they are done with the Mexicans from the four different Mexicos, they will come
for you.
That's how this works.
That's how it always works.
That's how it always works.
You are next, and you won't have due process.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}