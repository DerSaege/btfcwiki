---
title: Let's talk about the solution to the immigration problem....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=lVuTVrCYrIM) |
| Published | 2019/04/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Describes a friend named Monroe who brought joy and positivity into his life, despite facing challenges.
- Ponders on the immigration problem and questions the perception of immigration as a problem.
- Challenges the notion of immigration being a problem and suggests that it's more about government regulation.
- Criticizes common excuses used to oppose immigration, such as not speaking English or burdening welfare systems.
- Questions the fear of immigrants taking jobs and points out the illogical reasoning behind it.
- Criticizes the mentality of using force to protect jobs and likens it to a gang mentality.
- Calls out the government for deflecting blame onto immigrants rather than addressing internal issues.
- Urges for people to realize that the government may not have their best interests at heart and encourages critical thinking.
- Advocates for a shift in perspective and the need for individuals who have faced hardship to provide insight.
- Concludes by stating that immigration is not the problem but rather the regulations surrounding it, promising to address solutions in a future video.

### Quotes

- "We don't have an immigration problem. We have a government regulation problem dealing with immigration."
- "They're not talking about you. You're not that important."
- "It's crazy to use that as a reason. It doesn't change the morality of it simply because you have some dude in a uniform do it for you."
- "People in D.C. do not care about you. It's an act."
- "I don't have a solution to the immigration problem because it's not a problem."

### Oneliner

Beau challenges the perception of immigration as a problem, criticizing excuses and calling for introspection on government actions.

### Audience

Activists, Policy Makers, Community Members

### On-the-ground actions from transcript

- Challenge misconceptions about immigration (implied)
- Advocate for fair government regulations on immigration (implied)
- Support individuals who have faced hardship (implied)

### Whats missing in summary

The emotional impact and personal anecdotes shared by Beau can be fully experienced in the full transcript. 

### Tags

#Immigration #GovernmentRegulations #CriticalThinking #CommunityAdvocacy #PerspectiveShift


## Transcript
Well, howdy there, internet people, it's Bo again.
I had this friend, I called him Monroe,
that wasn't really his name though.
He reminded me a lot of Monroe Kelly from the movie Congo,
so the black contract, the guide.
He was from Nigeria, and through a twist of fate,
he wound up here.
I had a high-pressure job at the time, a rough job, and it didn't matter, though, it didn't
matter how bad a day I was having.
I called this guy up because he had that stereotypical African happiness, that jovial nature, he
even did that big belly laugh, you know.
Um, I call that guy up and go have a drink with him.
And it didn't matter what was going on that day.
You know, he'd slap you on the legs, your legs work, it's a beautiful day, we're by
the beach, there's beautiful women, we're having a drink, it's a good day.
We had ten minutes of that.
Very good mood.
I love that guy.
Oh man. I get asked a lot what the solution to the immigration problem is. I always think of him.
I always think of him. Because he's a guy that under normal circumstances he would not have been in this country.
It didn't meet the criteria, but a twist of fate, he wound up here.
The solution to the immigration problem. The subtext of that is that immigration is a problem.
What is the solution to an influx of people who are overwhelmingly
hard-working, ethical, good people? What's the solution to that?
I don't know, man, what's the solution to a cure for cancer?
It's its own solution.
We don't have an immigration problem.
We have a government regulation problem dealing with immigration.
There's no immigration problem, but there is a problem with 20-year wait times for an
immigrant visa from Mexico.
That's a problem.
You know, and if you actually go through the excuses, and they are excuses, they're not
reasons that people use to justify their opposition to immigration, they're all ridiculous.
On their face, they're ridiculous.
Well they don't speak English, so?
So what?
Who cares?
Seriously, who cares?
They're not talking about you.
You're not that important.
They're going to get on welfare. They're going to get on welfare. So you have a problem with the way the
government spends your tax dollars.  Sounds like you have a problem with the government, not with them.
They're going to come here and crowd up our schools.
How are schools funded?
Typically through property tax and sales tax, right?
They pay those.
even illegals pay those.
They're going to take our jobs.
So you're willing to use force
to preserve your job.
Why don't you use force now?
Why don't you use force now? Why don't you go and
bust the kneecaps of the other guy lay in tile in your community
so you can get that work.
Well, that sounds crazy.
Of course it is.
Of course it sounds crazy, because it is.
It's crazy to use that as a reason. It doesn't change the morality of it
simply because you have some dude in a uniform do it for you.
It's the same thing.
It's a gang mentality.
They're from a different hood,
different turf,
except you wave a flag instead of a bandana.
idiotic. But it's a defense mechanism. We can't admit that the problem is on our end
because we're a government of the people. And if you admit the problem is on our end,
that means that you have to get involved. It means you have to realize that you've made
bad choices and that we now have a government that, well, as long as they can convince you
that you're better off than those dudes, you'll let them get away with anything, won't you?
People in D.C. do not care about you.
They never have.
It's an act.
And people have fallen for it.
People have fallen for it.
And now it's coming to a boiling point.
See, we've had it so good for so long in this country that everything's a crisis.
We need a lot more Monroe's to kind of put things in perspective.
We need people who have seen what hard times really are because in the U.S., yeah, you've
got some individuals that have seen hard times, you do.
So nationwide, we have no clue what it is.
I don't have a solution to the immigration problem because it's not a problem.
I've got plenty of solutions to the regulation problem, and we'll go over those in another
video.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}