---
title: Let's talk about the Comanche, newcomers, and avocados....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=SYIvoz5HyC0) |
| Published | 2019/04/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Challenges viewers to imagine a Comanche warrior instead of a helicopter.
- Traces the historical connection between dogs and human culture through campfires.
- Emphasizes the importance of understanding American culture as constantly evolving.
- Comments on the older American culture of the Comanche tribe and their adaptation to new technologies.
- Describes how horses transformed the way of life for the Comanche people.
- Points out that newcomers have always influenced and enriched American culture.
- Raises concerns about the current resistance towards newcomers in America, drawing parallels with past immigrant groups.
- Criticizes the prioritization of avocados over people in the context of border debates.

### Quotes

- "I have a feeling that if horses and dogs can change cultures so drastically, so much, that people can too."
- "I have a feeling that if horses and dogs can change cultures so drastically, so much, that people can too."
- "Every group of people has that newcomer, has that thing that they need to accept for their culture to continue, to grow, otherwise it stagnates."
- "The sad part is, as we talk about closing the border, Americans are more concerned about avocados than people."

### Oneliner

Beau challenges viewers to rethink American culture by reflecting on the historical influences of newcomers like the Comanche tribe and urges acceptance and growth for a better future.

### Audience

Americans

### On-the-ground actions from transcript

- Accept newcomers for cultural enrichment (implied)
- Prioritize people over commodities like avocados (implied)

### Whats missing in summary

Beau's passionate delivery and historical anecdotes can provide a deeper understanding of the impact of newcomers on American culture.


## Transcript
Well, howdy there, Internet people, it's Bo again.
So tonight,
tonight, starting off, I want you to get an image of a Comanche in your head,
not the helicopter, a native.
Put you to hold that image tonight.
Historians will tell us that dogs first became part
human culture because of campfires. They saw the light, the warmth, like most
animals. They were curious and wanted to come check it out. Eventually they got
scraps and there you go. They became part of human culture. That newcomer to our
way of life, changed it for the better.
It's important to keep in mind as we discuss as a nation what American culture is.
I don't have a clue.
It's constantly changing.
I do know a little bit about an older American culture though, the Comanche.
Odds are the image you have in your mind is of a warrior.
horseback with a spear or a rifle. It's a fair image. The thing is, they didn't always
have horses. Horses were relatively new. In fact, prior to the horses showing up, which
they got from the Pueblo, they were part of the Shoshone. They broke away because they
They accepted that.
They accepted that new technology, so to speak.
They accepted the newcomer.
Changed their whole way of life.
They were able to follow the buffalo.
Eventually, you had migrants from the tribe they left come to them, it's interesting.
Those changes in culture eventually become the culture and it's hard to separate from
what it was before.
It's just the natural progression of history.
The horse became so important to them that when the U.S. military was trying to weed
them out, they took 1,400 of their horses and they killed 1,000 of them, the horses.
To a lot of people, that was the end.
That was the end of Comanche resistance because the warriors, for the most part, didn't know
how to fight anymore.
That horse had become such an integral part of their culture that they just couldn't do
without it.
I have a feeling that if horses and dogs can change cultures so drastically, so much, that
people can too.
And I would wager to believe that in a hundred years the newcomers that so many people just
don't want today, for whatever reason, they'd be inseparable from what is America.
Just the way the stories of the Irish newcomers, or the Italian, or the French, or the German.
We've all got those stories.
Every group of people has that newcomer, has that thing that they need to accept for their
culture to continue, to grow, otherwise it stagnates.
I would hope that the majority of Americans realize that every newcomer group that has
ever come to this country made it better.
This group isn't going to be any different.
Got to let that go.
The sad part is, as we talk about closing the border, Americans are more concerned about
avocados than people.
Anyway, if it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}