# All videos from April, 2019
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2019-04-30: Let's talk about Cassandra, two videos, the Islamic state, and US Intel.... (<a href="https://youtube.com/watch?v=KDIsogkOc2k">watch</a> || <a href="/videos/2019/04/30/Lets_talk_about_Cassandra_two_videos_the_Islamic_state_and_US_Intel">transcript &amp; editable summary</a>)

Beau outlines the dangers of declaring ISIS defeated, warns of future attacks, and calls for a shift towards human intelligence over technology in counter-terrorism efforts.

</summary>

"Holding land requires resources, and when you take that away, they can go on the offensive."
"US intelligence failure, listening to signal intercepts and satellite photos."
"Technology doesn't provide intent, Baghdadi avoids electronics."
"Declaring ISIS defeated has made them the underdog again."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Cassandra Complex: feeling ignored like Cassandra, gifted with prophecy nobody listens to.
ISIS isn't defeated: holding land isn't a metric for potency, they can go on the offensive.
US intelligence failure: lack of foresight, listening to signal intercepts and satellite photos.
Technology vs. terrorism: spying tech doesn't provide intent, Baghdadi avoids electronics.
Baghdadi's intelligence: US knew he was smart, used him unknowingly to build the Islamic State.
Baghdadi's video: propaganda showing humility or giving orders? Alarming Al Qaeda style attacks expected.
Declaring ISIS defeated: dangerous move, lowers expectations, makes them the underdog.
Foreign fighters' threat: potential attacks in countries listed in video folders.
Need for human intelligence: shift focus from technology to understanding intent.

Actions:

for counter-terrorism experts,
Prioritize human intelligence over technology in counter-terrorism efforts (implied).
Stay vigilant and prepared for potential Al Qaeda style attacks in the future (implied).
</details>
<details>
<summary>
2019-04-29: Let's talk about the world I want to live in.... (<a href="https://youtube.com/watch?v=2pLb_uc0bo8">watch</a> || <a href="/videos/2019/04/29/Lets_talk_about_the_world_I_want_to_live_in">transcript &amp; editable summary</a>)

Beau envisions a harmonious society where community support, education, and innovation thrive, eliminating the need for police and promoting equality of opportunities.

</summary>

"I just want to be able to sit on my porch and drink homemade whiskey while I use my phone to order a pizza that's going to be delivered by a drone that I'm paying for with cryptocurrency..."
"In a world like this, there's no need to escape, so drug abuse of course plummets."
"Kings and queens are things that only exist on chessboards. Empires only exist in video games but free travel exists everywhere..."
"People spend more time trying to master their own life or the world around them rather than each other."
"I know sounds pretty utopian, right? But it's just a thought."

### AI summary (High error rate! Edit errors on video page)

Imagines a world where he can relax on his porch, drinking homemade whiskey, ordering pizza with cryptocurrency, delivered by a drone, funded by renewable energy.
Describes a community where an interracial gay couple introduces him to their refugee child from a natural disaster.
Envisions a society where education is a community asset, funded by bake sales and businesses voluntarily supporting a free hospital.
Portrays a system where citizens handle issues without police, focusing on restitution-based penalties for non-violent crimes and rehabilitation for violent offenses.
Paints a picture of a world without poverty, leading to reduced drug abuse and increased cooperation between communities.
Envisions inter-community trade without profit motivation, faster inventions due to no intellectual property laws, and workers embracing automation for societal advancement.
Dreams of a society where equality of opportunities eliminates bigotry, and people focus on bettering themselves and the world rather than dominating others.

Actions:

for community members,
Support community-run schools through bake sales and donations (exemplified)
Volunteer or donate to free hospitals funded by local businesses (exemplified)
Participate in neighborhood food-sharing initiatives (exemplified)
Advocate for restitution-based penalties for non-violent crimes (suggested)
Encourage rehabilitation programs for violent offenders (suggested)
Foster cooperation between communities through teleconferencing (exemplified)
Support innovation by advocating for relaxed intellectual property laws (suggested)
Embrace automation in industries for societal progress (suggested)
</details>
<details>
<summary>
2019-04-26: Let's talk about why military assistance to foreign countries is bad for the drug war.... (<a href="https://youtube.com/watch?v=n91z9-IGf7w">watch</a> || <a href="/videos/2019/04/26/Lets_talk_about_why_military_assistance_to_foreign_countries_is_bad_for_the_drug_war">transcript &amp; editable summary</a>)

Beau explains the pitfalls of US military assistance in the War on Drugs, citing unintended consequences and the inefficacy of current approaches, ultimately calling for a shift towards harm reduction and treatment.

</summary>

"The plants have won."
"We've wasted the training, we've wasted all of that money, and it has done no good."
"The war on drugs is a failure."
"It's time to maybe look at it from a different perspective."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Explains why it's detrimental for the US military to assist Central and South American countries in the War on Drugs.
Mentions the oppressive nature of some governments and their military use against people.
Describes how US military training involves learning tactics used by terrorists, leading to unintended consequences.
Talks about how expensive military training is and how individuals can profit from it in the private sector.
Illustrates how trained individuals sought after by cartels can compromise counter-narcotics efforts.
Emphasizes the blurred line between cartels and governments in some countries, leading to corruption and collaboration.
Points out that cartels operate like paramilitary organizations with intelligence and recruitment strategies.
States that the dissemination of information to cartels undermines efforts and increases the cost of the War on Drugs.
Criticizes the inefficacy of current approaches in the drug war, suggesting a shift towards harm reduction and treatment.
Concludes by reflecting on the failure of the War on Drugs and the need for a different perspective.

Actions:

for policy advocates, activists,
Advocate for harm reduction and treatment approaches in drug policy (suggested)
Support organizations working towards drug policy reform (implied)
Educate others on the failures of the War on Drugs and the need for a different approach (implied)
</details>
<details>
<summary>
2019-04-25: Let's talk about the guy on the border getting arrested and beat up.... (<a href="https://youtube.com/watch?v=ih1Mate1Uvo">watch</a> || <a href="/videos/2019/04/25/Lets_talk_about_the_guy_on_the_border_getting_arrested_and_beat_up">transcript &amp; editable summary</a>)

A militia leader gets arrested and assaulted in jail, sparking questions about gun rights, inmate culture, and freedom.

</summary>

"He's just after the benefits."
"It's never funny when an inmate gets assaulted."
"All that separates him from freedom is an imaginary line."
"Confinement can make you better or it can make you better."
"I hope that he sees that wall and actually starts to begin to understand what freedom is."

### AI summary (High error rate! Edit errors on video page)

Militia leader from the border got arrested and beat up while in confinement.
The leader allegedly led a group that snatched migrants, sometimes posing as border patrols and pointing guns.
Surprisingly, he was arrested on old weapons charges rather than for his alleged actions.
Beau questions why the leader didn't follow the process to reinstate his gun rights.
The incident in jail where he was assaulted was rumored to be due to him not adopting inmate culture.
Beau criticizes the leader for not assimilating and causing problems.
He questions why taxpayers should pay for the leader's medical bills after the assault.
Beau touches on the lack of funding and issues in the prison system.
Despite making jokes, Beau acknowledges that inmate assaults are no laughing matter.
He hopes confinement will make the leader better and help him understand freedom.

Actions:

for advocates for prison reform,
Advocate for better funding and reforms in the prison system (suggested)
Support organizations working towards improving conditions in jails and detention centers (exemplified)
</details>
<details>
<summary>
2019-04-24: Let's talk about which country is the greatest threat to American freedom.... (<a href="https://youtube.com/watch?v=rahAHcZiKdE">watch</a> || <a href="/videos/2019/04/24/Lets_talk_about_which_country_is_the_greatest_threat_to_American_freedom">transcript &amp; editable summary</a>)

Beau identifies the US as a Sue Stan, critiquing internal issues framed differently due to nationalism, urging reflection and potential regime change calls if it were another country.

</summary>

"This is the United States today, a Sue Stan, a backwards USA with a Stan added to give it a little cultural bias."
"If this was any other country, people would be calling for regime change."
"All of this is true, just in the way it's framed."
"It's very, very hard to see it for what it is."
"Anyway, it's just a thought."

### AI summary (High error rate! Edit errors on video page)

Identifies a country as the greatest threat to American freedom, rarely discussed due to economic power and oligarchy with far-right leanings.
Describes the country's two parties, Punda and Timbo, being essentially the same due to ties with ruling elites.
Talks about unprecedented corruption in the country, where elite individuals escape consequences even for serious crimes.
Mentions the powerful military of the country, including possession of chemical weapons and violating national sovereignty.
Criticizes the country's security services for arrests that often lead to minimal repercussions and lack of real justice.
Points out the indoctrination of the country's citizens by compliant media and passage of sweeping security laws like the Wazilindo laws.
Notes the excessive use of prisons for profit, leading to a high percentage of the world's prison population despite low overall population.
Raises concerns about poverty, child recruitment into the military, child marriage, and executions of children in certain regions.
Compares the country's political parties and laws to American equivalents like the Patriot Act, urging reflection on internal issues.
Concludes by implying that if the same actions were reported on another country, there might be calls for regime change.

Actions:

for american citizens,
Challenge media narratives (implied)
Advocate for systemic change (implied)
Support policies promoting justice and equality (implied)
</details>
<details>
<summary>
2019-04-23: Let's talk about Ryker Glance and warning signs.... (<a href="https://youtube.com/watch?v=jnTvWDMu960">watch</a> || <a href="/videos/2019/04/23/Lets_talk_about_Ryker_Glance_and_warning_signs">transcript &amp; editable summary</a>)

Beau stresses recognizing warning signs, urging early action in domestic abuse situations to prevent tragic outcomes.

</summary>

"The key is to get out before then. There's always a way out."
"Left unchecked, warning signs always end bad, it always ends bad."
"Get out early, get out early."
"You win every tough guy competition forever."
"It's the perfect opportunity for everybody to kind of take stock of these warning signs."

### AI summary (High error rate! Edit errors on video page)

Two-year-old Ryker Glantz survived a horrifying incident where his father shot him in the face with a shotgun after a pistol malfunctioned.
Beau stresses the importance of recognizing warning signs in domestic abuse situations, as they can escalate suddenly.
Warning signs include jealousy, possessiveness, unpredictability, bad temper, animal cruelty, verbal abuse, extreme controlling behavior, and more.
Beau urges victims to seek help from shelters and organizations and stresses the significance of getting out before situations escalate.
Financial control, limiting access to funds, accusations of infidelity, and harassment at work are also warning signs of domestic abuse.
Beau encourages everyone to be aware of these signs and act early to prevent potentially tragic outcomes.
Despite not knowing the specifics of Ryker's situation, Beau advocates for recognizing warning signs and taking action early.
Beau mentions a GoFundMe for Ryker's mom to support her in being with her son during his recovery.
Ryker's survival is described as winning "every tough guy competition forever" and serves as a reminder to pay attention to domestic abuse warning signs.
Beau concludes by encouraging viewers to take stock of the warning signs and to act early to prevent domestic abuse situations from escalating further.

Actions:

for supporters of domestic abuse victims.,
Support Ryker's mom through the GoFundMe to help her be with her son during his recovery (suggested).
Educate yourself on domestic abuse warning signs and ways to help victims (suggested).
</details>
<details>
<summary>
2019-04-22: Let's talk about the problem with young women.... (<a href="https://youtube.com/watch?v=rt7lT1Vo3xg">watch</a> || <a href="/videos/2019/04/22/Lets_talk_about_the_problem_with_young_women">transcript &amp; editable summary</a>)

Kindergarten dress code incidents to high school bathroom confrontations, women face challenges in developing confidence due to societal norms and lack of empowerment.

</summary>

"Staff should be held accountable for any distractions, not the kindergartner."
"Regulations on women's clothing are about control and enforcing traditional gender roles."
"Assertiveness is lacking in young women due to societal conditioning from a young age."
"Women facing challenges in developing confidence and self-reliance from a young age due to societal norms."
"Society's inconsistency in empowering women to stand up for themselves and develop an identity."

### AI summary (High error rate! Edit errors on video page)

Kindergartner forced to change clothes at Hugo Elementary in Minnesota over thin straps on her sundress.
Dress code meant to avoid distraction to boys, but it's kindergarten, are girls still icky?
Staff should be held accountable for any distractions, not the kindergartner.
Incident at Madison High in Houston where a parent was forced to leave for enrolling her child while wearing a Marilyn Monroe t-shirt and headscarf.
School regulations restrict women from expressing their identity or ethnic heritage.
Federal court ruling in North Carolina against making girls wear skirts when boys can wear pants.
Regulations on women's clothing are about control and enforcing traditional gender roles.
Lack of consistency in dress codes and regulations aimed at keeping women in their place.
Incident in an Alaskan high school where a girl transitioning to a boy faced harassment in the bathroom.
Boys attempted to regulate the girl's bathroom use, leading to a physical confrontation where the girl defended herself.
Assertiveness is lacking in young women due to societal conditioning from a young age.
Women are often discouraged from standing up for themselves and developing confidence and identity.
Women facing challenges in developing confidence and self-reliance from a young age due to societal norms.
Society's inconsistency in empowering women to stand up for themselves and develop an identity.
Lack of outcry or outrage over incidents where women are mistreated or silenced.

Actions:

for educators, parents, activists,
Support initiatives promoting confidence and assertiveness in young women (suggested)
Challenge dress code policies that reinforce traditional gender roles (suggested)
Encourage young women to stand up for themselves and assert their identities (implied)
</details>
<details>
<summary>
2019-04-21: Let's talk about the incident in Broward and whether there will be #JusticeForLucca (<a href="https://youtube.com/watch?v=wNgYOmWVShQ">watch</a> || <a href="/videos/2019/04/21/Lets_talk_about_the_incident_in_Broward_and_whether_there_will_be_JusticeForLucca">transcript &amp; editable summary</a>)

Deputies beating kids in Broward County reveal systemic issues beyond just excessive force, urging a focus on training and policy reform over individual justice.

</summary>

"You can maintain control of a suspect without smashing their face into the concrete."
"What it appears to me is that this department has been trained in the right way to do this."
"The officer who pepper sprayed him was attempting to do it the right way."
"It's very clear from the video."
"There's a whole lot we don't know, but that's a pretty important piece that I don't see getting discussed."

### AI summary (High error rate! Edit errors on video page)

Deputies in Broward County beat up some kids, sparking a debate on the use of force.
A key moment in the video is when an officer pepper-sprayed a kid who appeared to be picking something up off the ground.
The focus should not only be on the excessive force used by one officer but also on the actions of the officer who pepper-sprayed the kid.
The officer who pepper-sprayed the kid demonstrated deliberate and trained movements suggesting a proper technique for control without excessive force.
Beau points out that the department may have proper training procedures, but these were not followed in the incident.
The officer who pepper-sprayed the kid appeared calm and in control, contrasting with the excessive force used by another officer.
Despite the questionable actions, it is suggested that the department's policy might justify the use of force as seen in the video.
Beau stresses the importance of scrutinizing the actions of all officers involved in such incidents, not just those directly using excessive force.
There is a need to focus on changing policies rather than solely seeking justice for individual cases to prevent such incidents from happening in the future.
The situation is complex, with many unknown factors, but it is vital to analyze and address the systemic issues contributing to police brutality.

Actions:

for police reform advocates,
Advocate for policy changes within law enforcement departments to ensure proper training and accountability (exemplified)
Support initiatives that address systemic issues leading to police brutality (implied)
</details>
<details>
<summary>
2019-04-21: Let's talk about a guy named Rooster and emergency preparedness.... (<a href="https://youtube.com/watch?v=gzKSVhY-S-I">watch</a> || <a href="/videos/2019/04/21/Lets_talk_about_a_guy_named_Rooster_and_emergency_preparedness">transcript &amp; editable summary</a>)

Beau stresses the importance of emergency preparedness for everyone and provides detailed guidance on assembling an emergency bag with essentials for survival in various situations.

</summary>

"Everybody needs to know something about emergency preparedness."
"Everybody needs one. Everybody. Everybody. I can't stress this enough."
"Emergencies by their very definition you don't know they're coming so get it together anyway."

### AI summary (High error rate! Edit errors on video page)

Introduces Rooster and his involvement in emergency preparedness work with Cajun Navy and Bear Paw Tactical Medical.
Describes Rooster as a responsible individual who always stepped up during emergencies despite his lack of wound wrapping skills.
Emphasizes the importance of emergency preparedness for everyone, regardless of their background or expertise.
Acknowledges the questions he receives about survival situations and announces plans to share videos on basic survival skills.
Stresses the necessity of putting together an emergency bag containing essentials like first aid kits, medications, food, water, fire-starting materials, shelter items, and tools.
Advises against prepacked bug out bags and encourages customization based on individual needs.
Recommends practical items like canned goods, rice, and a can opener for emergency food supplies.
Suggests having a supply of bottled water, purification tablets, or filters for clean drinking water.
Lists items like flashlights, fire-starting tools, shelter materials, and knives as critical for survival.
Urges the importance of having an evacuation plan and backups in place tailored to individual family situations.

Actions:

for community members,
Prepare an emergency bag with essentials like first aid kits, medications, food, water, fire-starting materials, shelter items, and tools (suggested).
Customize your emergency bag based on your family's specific needs and situation (suggested).
Develop an evacuation plan with multiple backup options in case of emergencies (suggested).
</details>
<details>
<summary>
2019-04-20: Let's talk about why a Senator said nurses were playing cards at work.... (<a href="https://youtube.com/watch?v=K8U-1Y5WJ6I">watch</a> || <a href="/videos/2019/04/20/Lets_talk_about_why_a_Senator_said_nurses_were_playing_cards_at_work">transcript &amp; editable summary</a>)

Nurses unite against a senator's opposition to mandatory breaks, revealing potential scripting by donors in American politics.

</summary>

"Nurses are like the mafia."
"I mean because I know what to do. Anytime a politician says something dumb all you have to do is go look at their campaign contributions and you'll find out why they said it."
"I have never seen anything like this before."
"There's so much money in it that it's that easy to find out why your representative, who is supposed to be your representative, is proposing the bills and amendments that they are and why they're voting the way they are."
"I think we should really take a look at how money is affecting our legislators."

### AI summary (High error rate! Edit errors on video page)

Nurses are like the mafia in how quickly they spread news and defend each other.
A Washington state senator opposed a bill for nurses' mandatory breaks, claiming they spend time playing cards.
The senator's top campaign contributor is the Washington State Hospital Association.
The hospital association posted about unintended consequences of the bill on April 8th.
The senator proposed an amendment limiting nurses to work 8 hours in a 24-hour period with no exceptions.
The hospital association claimed no knowledge of this amendment, suggesting possible scripting.
Beau questions the coincidence and suggests the senator might be parroting hospital association interests.
American politics are criticized for being influenced by money, where politicians act in favor of their donors.
The senator's actions raise concerns about legislators being swayed by financial interests rather than representing the people.
Beau recommends examining how money impacts legislators and their decision-making processes.

Actions:

for voters, activists, nurses,
Question your representatives about their campaign contributions and how they may influence their decisions (implied)
</details>
<details>
<summary>
2019-04-20: Let's talk about spirals, the Hopi creation story, and caretakers.... (<a href="https://youtube.com/watch?v=rv7VoVV_BP8">watch</a> || <a href="/videos/2019/04/20/Lets_talk_about_spirals_the_Hopi_creation_story_and_caretakers">transcript &amp; editable summary</a>)

Beau recounts the Hopi creation story and its relevance today, reflecting on the quest for balance and center in society, both past and potentially future.

</summary>

"Every relationship, every society, every civilization is still seeking that."
"Man was seeking center, trying to find balance."
"I think of future people on a hero's journey because their world was destroyed."
"I wonder if the caretaker of tomorrow will be as hospitable."
"Given my views, it is not entirely lost on me that the migration ended in the southwestern United States."

### AI summary (High error rate! Edit errors on video page)

Describes spirals carved into cliffs and stones in the southwestern United States, representing an ancient hero's journey and quest.
Recounts the Hopi creation story heard around a campfire, detailing the emergence of the Hopi people from the earth after their world was destroyed.
Mentions the requirement placed upon the Hopi people by the caretaker to protect the earth and prevent the same destruction caused by greed.
Talks about the quest assigned to the Hopi people by the caretaker to find center and build their society there, marked by spirals in different directions.
Expresses admiration for the story as a creation myth where man has always sought balance and center, a theme relevant even today.
Draws parallels between the ancient Hopi journey and a potential future journey of people emerging from the earth into a new world.

Actions:

for history enthusiasts, culture appreciators,
Share and preserve indigenous creation stories through storytelling events (implied)
</details>
<details>
<summary>
2019-04-18: Let's talk about what Charles Darwin can teach us about the culture wars.... (<a href="https://youtube.com/watch?v=ZcQNOL2tQ4g">watch</a> || <a href="/videos/2019/04/18/Lets_talk_about_what_Charles_Darwin_can_teach_us_about_the_culture_wars">transcript &amp; editable summary</a>)

Charles Darwin's quote on adaptability applies to America's cultural war, where clinging to the past threatens progress and leads to the demise of American ideals.

</summary>

"If you are one of those fighting to return to the good old days, whenever that was, you're killing America."
"American culture will die because we're not changing."
"We're fighting battles that were decided 150 years ago."
"The culture of this country is dying."
"Those who see themselves as the defenders of the American ideal don't even know what it is anymore."

### AI summary (High error rate! Edit errors on video page)

Charles Darwin's quote on adaptability being critical for survival resonates in cultural contexts as well.
The U.S., especially the southern states, is embroiled in a cultural war between regressive and progressive factions.
Arkansas is replacing statues of a Confederate lawyer and a segregationist with desegregationist Daisy Lee Gatson Bates and musician Johnny Cash.
Beau questions the importance of preserving Confederate monuments that represent a short-lived era.
He criticizes how the U.S. clings to outdated hierarchical structures and fails to progress like the rest of the world.
The only remaining Confederate tradition in the South is poor white individuals fighting battles for rich white elites.
Beau laments that culture wars distract from addressing root issues of societal control by the wealthy.
He believes America is stagnant because it fights battles from the past instead of embracing change.
Beau warns that those yearning for a mythical past are undermining the progressive ideals America once stood for.
He expresses concern that the American culture is dying due to a lack of evolution and adaptability.

Actions:

for americans, activists, historians,
Advocate for the removal of Confederate monuments and the recognition of figures promoting progress (exemplified).
Engage in open dialogues to understand the root causes behind cultural divides and work towards unity (implied).
Support initiatives that aim to move away from divisive symbols and embrace a more inclusive cultural narrative (implied).
</details>
<details>
<summary>
2019-04-17: Let's talk about skirts, lamps, and being attracted to trans people.... (<a href="https://youtube.com/watch?v=VJ54ZqI4EDY">watch</a> || <a href="/videos/2019/04/17/Lets_talk_about_skirts_lamps_and_being_attracted_to_trans_people">transcript &amp; editable summary</a>)

Beau addresses misconceptions about sexuality, advocating for self-acceptance and rejecting harmful terminology in 2019.

</summary>

"Stop with this. That is not good terminology because it reinforces the very question you're asking."
"Just be you, live your life, and let go of all of this."
"Embrace the weird, okay? Just live your life."

### AI summary (High error rate! Edit errors on video page)

Responding to a question from a heterosexual man who was briefly attracted to a trans woman and wondered if that makes him gay.
Advocating against using terminology that can harm the transgender community.
Pointing out the absurdity of questioning one's sexuality based on brief attraction to a trans woman.
Suggesting that being overly concerned about your sexuality might be a better indication of being gay.
Emphasizing that being attracted to someone who is trans does not automatically mean you are gay.
Asserting that being gay is not a negative thing and should not be stigmatized.
Encouraging individuals to embrace who they are and live their lives without worrying about societal norms or labels.
Urging people to let go of unnecessary concerns about their sexuality and just be themselves, regardless of societal expectations.
Reminding everyone that it is 2019 and people's sexual orientation should not matter to those who truly care about them.
Encouraging individuals to embrace their uniqueness and live authentically without fear of judgment or stereotypes.

Actions:

for all individuals questioning their sexuality or facing societal pressures.,
Embrace your uniqueness and live authentically, disregarding societal norms (exemplified).
Let go of unnecessary concerns about your sexuality and just be yourself (exemplified).
</details>
<details>
<summary>
2019-04-17: Let's talk about arms transfers to Yemen and Saudi Arabia.... (<a href="https://youtube.com/watch?v=g5xEa-nOdCI">watch</a> || <a href="/videos/2019/04/17/Lets_talk_about_arms_transfers_to_Yemen_and_Saudi_Arabia">transcript &amp; editable summary</a>)

Beau delves into Yemen's crisis, dissecting U.S. arms transfer laws, revealing violations, and questioning presidential authority.

</summary>

"We're violating our own laws by doing it."
"Do we allow a president to just run roughshod over laws?"
"It was illegal then, and it's illegal now, and it needs to stop."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic of Yemen and focuses on the legal framework of U.S. arms transfers.
Explains the importance of understanding the legalities separate from Trump's veto on a resolution.
Points out the attempt by the U.S. government to establish a moral framework within arms export laws.
Describes the dire humanitarian crisis in Yemen, comparing it to one of the worst since World War II.
Talks about the indiscriminate killing of civilians, especially children dying of starvation.
Examines U.S. laws like the Arms Export Control Act, Foreign Assistance Act, Presidential Policy Directive 27, and CATP regarding arms transfers.
States that the U.S. is violating its own laws by transferring arms to Saudi Arabia for use in Yemen.
Addresses the constitutional issue of Congress's authority over making war and supplying arms in conflicts.
Raises questions about allowing a president to bypass laws and stresses the importance of upholding the nation's laws.

Actions:

for policy makers, activists,
Contact your representatives to urge them to uphold laws on arms transfers (implied)
Support organizations advocating for human rights and peace in Yemen (implied)
</details>
<details>
<summary>
2019-04-15: Let's talk about how Trump will be our first open borders President.... (<a href="https://youtube.com/watch?v=iYuFYC720No">watch</a> || <a href="/videos/2019/04/15/Lets_talk_about_how_Trump_will_be_our_first_open_borders_President">transcript &amp; editable summary</a>)

Beau warns of de facto open borders under Trump's presidency, exposing the unintentional support from conservatives, urging action to free caged individuals.

</summary>

"We're on the verge of the first open borders presidency."
"He's advocating a return to Ellis Island-style immigration."
"We have innocent people in cages."
"He's just a man-child throwing a temper tantrum."
"At the end of the day, we're going to get people out of cages and that's what matters."

### AI summary (High error rate! Edit errors on video page)

Warning against de facto open borders under Trump's presidency, despite no one openly advocating for it.
Trump's plan for processing immigrants resembles Ellis Island-style immigration.
Fox News and mayors are unknowingly supporting de facto open borders due to Trump's manipulation.
Trump's history of flip-flopping on issues like gun control and his current stance on immigration.
Urging Democrats to prioritize freeing people from cages over political games.
Suggesting that conservatives are being played by Trump into unintentionally supporting open borders.
Emphasizing the need to maintain the appearance of opposition to de facto open borders.
Speculating whether Trump is deceiving conservatives or simply acting impulsively.
Stating that the ultimate goal is to get people out of cages, regardless of political maneuvers or motivations.

Actions:

for politically engaged citizens,
Advocate for freeing individuals from cages (implied)
Stay informed and aware of political manipulations (implied)
</details>
<details>
<summary>
2019-04-15: Let's talk about 100,000 subscribers and me.... (<a href="https://youtube.com/watch?v=v8H7OdZzNH4">watch</a> || <a href="/videos/2019/04/15/Lets_talk_about_100_000_subscribers_and_me">transcript &amp; editable summary</a>)

Beau shares his journey from downplaying his accent to embracing his Southern roots, addressing misconceptions, and encouraging community engagement based on individual skills and interests.

</summary>

"I don't think that the message should be clouded because somebody did something you think is cool or something that you hate, it shouldn't matter."
"Figure out what your skill set is, I assure you it's needed. Get out there."
"I don't use that word. That word is very loaded. You say that word, people picture teenagers in leather jackets with spiky hair throwing rocks at cops."

### AI summary (High error rate! Edit errors on video page)

Started as a journalist downplaying his accent, which changed after colleagues discovered his Southern roots during a drinking session in 2016.
Made videos showcasing his accent and redneck persona, which garnered more attention and acceptance for his social commentary.
Initially played up his Southern accent but gradually reverted to his real accent, except for satire videos where he exaggerates it for humor.
Clarified his background, confirming he is from the South and has lived in various southern states.
Addressed false claims about being a convicted human trafficker, attributing them to libelous allegations from 2013.
Shared insights into his past as a consultant contractor and emphasized always being a civilian, despite misconceptions about his military involvement.
Responded to common questions about his personal life, activism, and philosophy, including clarifying his approach to conveying ideas over using specific terms.
Encouraged viewers to find ways to help their communities based on their skills and interests, without needing to adopt a militant approach.
Explained the purpose of his videos, discussing the difficulty of staying informed and avoiding scripted content in his delivery.
Mentioned creating a playlist of music he listens to and thanked his audience for their support.

Actions:

for youtube subscribers,
Reach out to various news networks like Greed Media or Pontiac Tribune based on interests (suggested)
Find ways to get involved in community activism using your unique skill set (exemplified)
</details>
<details>
<summary>
2019-04-14: Let's talk about a new icon for libertarians.... (<a href="https://youtube.com/watch?v=4HFd7130kOQ">watch</a> || <a href="/videos/2019/04/14/Lets_talk_about_a_new_icon_for_libertarians">transcript &amp; editable summary</a>)

Beau questions libertarian principles, proposing Harriet Tubman as a better symbol of freedom and urging a shift from wealth idolization to aiding others.

</summary>

"It's freedom. As much freedom as is humanly possible within the confines of the society."
"Action is more important than words."
"Why on earth do Libertarians idolize the wealthy?"
"If it's really about freedom, it would certainly seem that Harriet Tubman would be one of the ideals."
"Seems like that [giving a hand up when needed] would be more in line with the rhetoric from libertarians."

### AI summary (High error rate! Edit errors on video page)

Questions the essence of libertarianism, describing it as freedom within societal boundaries and complete deregulation.
Points out that libertarians believe legality is not equivalent to morality and value action over words.
Contrasts the belief that anyone can achieve greatness through self-improvement in an ideal society with the reality of inherited wealth among today's affluent.
Suggests Harriet Tubman as an alternative icon of freedom due to her journey from slavery to capability and action, understanding of morality, and fight against oppressive labor practices.
Questions why Harriet Tubman is not celebrated by libertarians despite embodying values like freedom and empowerment over wealth accumulation.
Encourages libertarians to shift their focus from idolizing the wealthy to guiding others towards freedom and assisting those in need, akin to Tubman's legacy.

Actions:

for libertarians,
Guide people to freedom and assist them when needed (implied)
</details>
<details>
<summary>
2019-04-12: Let's talk about Trump's threat against sanctuary cities.... (<a href="https://youtube.com/watch?v=8Z3GFgXxF30">watch</a> || <a href="/videos/2019/04/12/Lets_talk_about_Trump_s_threat_against_sanctuary_cities">transcript &amp; editable summary</a>)

Beau urges Trump to bus detained migrants into sanctuary cities to lower crime rates and challenges his courage, humorously pointing out the lack of walls around cities.

</summary>

"Do it. Bus them in. Drop them off. It's fantastic."
"Get them out of the cages, man."
"But you do understand that these cities don't have walls around them, right?"
"Stop considering it. Do it."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Trump is considering releasing detained migrants into sanctuary cities, waiting for approval from President Miller.
Beau urges Trump to stop considering and just do it, challenging his courage.
He mentions that migrants statistically create less crime than native-born Americans.
Beau questions the intention behind sorting violent migrants and the harm it may cause American citizens.
He encourages Trump to bus in and drop off migrants in sanctuary cities.
Beau expresses support for the idea, believing it will lower the crime rate and get migrants out of cages.
He humorously points out that cities don't have walls around them, alluding to the lack of segregation.
Beau concludes with a parting thought for his audience to have a good day.

Actions:

for activists, concerned citizens,
Bus detained migrants into sanctuary cities (exemplified)
Drop them off in sanctuary cities (exemplified)
</details>
<details>
<summary>
2019-04-12: Let's talk about Assange and the Future Whistleblowers of America.... (<a href="https://youtube.com/watch?v=KMKM-6Q-Esc">watch</a> || <a href="/videos/2019/04/12/Lets_talk_about_Assange_and_the_Future_Whistleblowers_of_America">transcript &amp; editable summary</a>)

Beau warns about the dangerous implications of whistleblowing and the limited options available due to lack of protection from the press, leading to potential loss of life by disclosing means and methods.

</summary>

"The U.S. government, their big brother in 1984, and like Winston said, they get you. always get you."
"You can't go to the press. The press cannot protect you."
"But see, now that they're going to end up going to a foreign power, that reporter, he doesn't have that patriotism."
"We really need you to keep this secret. If you don't, this person will die."
"We need to set aside our desire for punishment when it comes to this."

### AI summary (High error rate! Edit errors on video page)

Assange is in custody and will be prosecuted, tried, and sentenced in the federal system.
The focus should be on what future whistleblowers are learning from recent incidents involving Winter, Hammond, Karyaku, Snowden, and Manning.
Whistleblowers are left with limited options due to lack of protection by the press or anonymous leak methods.
Going to a foreign power becomes the only option for whistleblowers to disclose information.
The US press complies with the US intelligence community in protecting means and methods.
Disclosing means and methods can lead to serious consequences, including loss of life.
US media's compliance with intelligence services shifts focus from illegal activities to the leak itself.
The government's response to leaks should prioritize legal and moral behavior to prevent future incidents.
The battle between disclosure of activities like waterboarding and means and methods obtained from reports will have serious implications in counterintelligence wars.

Actions:

for whistleblowers, journalists, activists,
Support and advocate for legal protections for whistleblowers (implied)
</details>
<details>
<summary>
2019-04-11: Let's talk about getting rid of judges and President Miller.... (<a href="https://youtube.com/watch?v=obm3P-KoLCU">watch</a> || <a href="/videos/2019/04/11/Lets_talk_about_getting_rid_of_judges_and_President_Miller">transcript &amp; editable summary</a>)

President advocating for abandonment of due process, conservatives silent, warning of dire consequences without fair legal proceedings.

</summary>

"When they are done with the Mexicans from the four different Mexicos, they will come for you."
"You are next, and you won't have due process."

### AI summary (High error rate! Edit errors on video page)

President of the United States advocating for abandoning the rule of law and due process.
President Trump seems to have abdicated his role to Stephen Miller, his puppet master.
President Miller believes eliminating due process will expedite deportations.
Mass deportations historically precede the removal of due process and are followed by concentration camps.
Conservatives seem to be turning a blind eye to these dangerous actions because of team loyalty.
Due process is vital to prevent false deportations and ensure fair legal proceedings.
Beau warns that after targeting certain groups, the lack of due process may affect others next.
The importance of upholding due process as a fundamental right enshrined in the Constitution is emphasized.
Beau points out cases of American citizens mistakenly targeted for deportation without due process.
The role of judges in ensuring fair legal proceedings and preventing wrongful deportations is underscored.

Actions:

for advocates for justice,
Advocate for the preservation of due process rights (suggested)
Educate others on the importance of due process in legal proceedings (exemplified)
</details>
<details>
<summary>
2019-04-10: Let's talk about the solution to the immigration problem.... (<a href="https://youtube.com/watch?v=lVuTVrCYrIM">watch</a> || <a href="/videos/2019/04/10/Lets_talk_about_the_solution_to_the_immigration_problem">transcript &amp; editable summary</a>)

Beau challenges the perception of immigration as a problem, criticizing excuses and calling for introspection on government actions.

</summary>

"We don't have an immigration problem. We have a government regulation problem dealing with immigration."
"They're not talking about you. You're not that important."
"It's crazy to use that as a reason. It doesn't change the morality of it simply because you have some dude in a uniform do it for you."
"People in D.C. do not care about you. It's an act."
"I don't have a solution to the immigration problem because it's not a problem."

### AI summary (High error rate! Edit errors on video page)

Describes a friend named Monroe who brought joy and positivity into his life, despite facing challenges.
Ponders on the immigration problem and questions the perception of immigration as a problem.
Challenges the notion of immigration being a problem and suggests that it's more about government regulation.
Criticizes common excuses used to oppose immigration, such as not speaking English or burdening welfare systems.
Questions the fear of immigrants taking jobs and points out the illogical reasoning behind it.
Criticizes the mentality of using force to protect jobs and likens it to a gang mentality.
Calls out the government for deflecting blame onto immigrants rather than addressing internal issues.
Urges for people to realize that the government may not have their best interests at heart and encourages critical thinking.
Advocates for a shift in perspective and the need for individuals who have faced hardship to provide insight.
Concludes by stating that immigration is not the problem but rather the regulations surrounding it, promising to address solutions in a future video.

Actions:

for activists, policy makers, community members,
Challenge misconceptions about immigration (implied)
Advocate for fair government regulations on immigration (implied)
Support individuals who have faced hardship (implied)
</details>
<details>
<summary>
2019-04-09: Let's talk about the Homeland Security Secretary and what's next.... (<a href="https://youtube.com/watch?v=PJylYFS_gjI">watch</a> || <a href="/videos/2019/04/09/Lets_talk_about_the_Homeland_Security_Secretary_and_what_s_next">transcript &amp; editable summary</a>)

Former Homeland Security Secretary resigns over refusing to break the law for Trump, urging for vigilant defense against authoritarian rule and propaganda.

</summary>

"A vote for Trump is a vote for authoritarian rule."
"You have farmed off your sovereignty, farmed off your individual responsibility to some politician and you just believe whatever they say."
"There's never been a democracy that hasn't committed suicide."
"Soldiers don't go die for a flag or a song. In theory, they go fight for an ideal."
"It's blindingly apparent, but people still support him."

### AI summary (High error rate! Edit errors on video page)

Reports indicate the former Homeland Security Secretary resigned because she refused to continue breaking the law for Trump.
Trump referred to family separation as a deterrent, which Beau sees as a civil rights violation and cruel and unusual.
A new Homeland Security Secretary is needed, someone willing to betray American values and take orders from Miller.
Congress's role is to act as a check against executive power and stop potential dictatorships.
Supporting Trump is supporting authoritarian rule and going against the Constitution.
Soldiers don't fight for a flag or a song but for the ideals of the Republic and representative democracy.
Beau criticizes uninformed patriotic citizens who have fallen for propaganda and sold out their country.
He points out that working-class conservatives often vote for billionaires who don't truly represent their interests.
Beau urges those not under Trump's spell to speak up against dangerous rhetoric and propaganda.
He warns about the constant blame on immigrants and the need to call it out to prevent further deterioration.

Actions:

for citizens, activists,
Speak up against dangerous rhetoric and propaganda every time you encounter it (implied)
Call out those supporting authoritarian rule without fail (implied)
Educate yourself on political matters and history to understand the implications of supporting certain leaders (implied)
</details>
<details>
<summary>
2019-04-08: Let's talk about books to read.... (<a href="https://youtube.com/watch?v=uHMcLRjChvw">watch</a> || <a href="/videos/2019/04/08/Lets_talk_about_books_to_read">transcript &amp; editable summary</a>)

Beau suggests reading ten types of books to gain wisdom and understanding, from religious texts to dystopian futures, challenging readers to broaden their perspectives.

</summary>

"Band books are the best books."
"Reading something that is completely out of step with who you are and outside of your comfort zone can help you fill in the gaps."
"Most literature is designed to give you wisdom, not knowledge."
"Every year there's a whole bunch of books that get challenged in the United States to get removed from libraries, banned from schools, whatever."
"The idea of literature is to expose you to things that you wouldn't experience in your life."

### AI summary (High error rate! Edit errors on video page)

Addressing the common request for book recommendations, Beau suggests reading ten types of books instead of specific titles.
Beau explains that most literature is designed to provide wisdom, not just knowledge.
He recommends starting with reading a religious text that is not your own, to understand other cultures.
Beau suggests reading about countercultures to gain a different perspective on society.
He advises reading books that portray the dark aspects of the human condition, making monsters human.
Beau recommends exploring political ideologies through manifestos to understand manipulation.
He encourages revisiting a book that deeply impacted you during childhood to reconnect with that wonder.
Beau suggests reading about other people's heroes, even those you may see as enemies.
He recommends reading critical books about war to break the romanticized view often portrayed.
Beau advises reading about old stories and gods to understand the oral traditions and hidden messages.
He suggests exploring dystopian futures towards the end to tie in themes from other recommended readings.
Beau proposes reading about characters completely different from oneself to broaden perspectives.
He concludes by suggesting reading banned books as they often hold valuable content.

Actions:

for book enthusiasts,
Read a religious text that is not your own to understand different cultures (implied)
Read books about countercultures and dark aspects of human nature to gain different perspectives (implied)
Revisit a book that impacted you in childhood to rediscover that wonder (implied)
Read about other people's heroes and critical views on war to challenge romanticized notions (implied)
Read banned books to discover valuable content (implied)
</details>
<details>
<summary>
2019-04-07: Let's talk about what Thomas Jefferson would say about sanctuary cities.... (<a href="https://youtube.com/watch?v=lEalJnnkOBo">watch</a> || <a href="/videos/2019/04/07/Lets_talk_about_what_Thomas_Jefferson_would_say_about_sanctuary_cities">transcript &amp; editable summary</a>)

Beau explains the historical context behind sanctuary states and challenges distorted interpretations of history.

</summary>

"History is a thing. It exists. You can't just change it and repackage it because you want to."
"When you make an appeal to the Founding Fathers and their views on things, understand that most of that stuff was written down."
"Thomas Jefferson, well he didn't even think I should exist."

### AI summary (High error rate! Edit errors on video page)

Mentioning sanctuary states and the Supremacy Clause.
Recalling an unofficial war with France in 1798 due to unpaid debts.
Explaining the Alien Friends and Alien Enemies Act of 1798.
Referencing Thomas Jefferson's Kentucky resolutions of 1798.
Explaining Jefferson's views on federal power over naturalization and immigration.
Citing the 10th Amendment regarding states' powers.
Asserting that Jefferson supported the idea of sanctuary states.
Emphasizing the importance of historical context and accuracy.
Warning against distorting history for convenience.
Questioning appeals to the Founding Fathers' perspectives.
Beau's closing remarks on historical authenticity.

Actions:

for history enthusiasts, policymakers, activists.,
Fact-check historical claims and narratives (implied).
</details>
<details>
<summary>
2019-04-06: Let's talk about a toast to the defeat of ISIS.... (<a href="https://youtube.com/watch?v=c3fsPqMyqns">watch</a> || <a href="/videos/2019/04/06/Lets_talk_about_a_toast_to_the_defeat_of_ISIS">transcript &amp; editable summary</a>)

Beau contemplates a toast while criticizing false political statements, acknowledging ongoing threats from terrorist organizations, and expressing frustration at the current situation.

</summary>

"Terrorist organizations do not need land to operate."
"This isn't over, it's not over."
"After all of this time, you have basically put us back in the situation we ran in Afghanistan in 2002."

### AI summary (High error rate! Edit errors on video page)

Winding down his Friday night, contemplating a toast.
Questions who to toast between President Trump, Department of Defense brass, and others.
Criticizes politicians for making false statements to score political points.
Suggests toasting the veterans of future wars.
Mentions American soldiers and Kurdish soldiers killed after ISIS was declared defeated.
Acknowledges ongoing threats and attacks by terrorist organizations.
Criticizes the Department of Defense for putting the situation back to 2002 Afghanistan.
Expresses frustration and disbelief at the current state of affairs.

Actions:

for politically aware individuals.,
Support veterans and current soldiers (implied)
Stay informed about political statements and actions (implied)
</details>
<details>
<summary>
2019-04-05: Let's talk about drones, the drone program, and what's next.... (<a href="https://youtube.com/watch?v=Gxv03MXtNhw">watch</a> || <a href="/videos/2019/04/05/Lets_talk_about_drones_the_drone_program_and_what_s_next">transcript &amp; editable summary</a>)

Beau explains the flaws of the U.S. drone program: secrecy, collateral damage, and breeding insurgency, urging for transparency and responsible use.

</summary>

"They want to kill us because we've made them fear the sky."
"The drone program is not being used as surgical strikes. It's being used as an assassination tool."
"It's secret, it's ineffective, and it's breeding insurgency."
"For every opposition fighter we take out, there's the family member of somebody we killed, some innocent that we killed, ready to pick up arms."
"They want to kill us because we've made them fear the sky."

### AI summary (High error rate! Edit errors on video page)

Drones are neutral technology but can be used for good or evil purposes.
The U.S. drone program is portrayed as surgical strikes targeting command and control.
Around 90% of those killed in drone strikes were not the intended targets.
There is a possibility that some of the 90% killed were opposition forces near the target.
The drone program has hit wedding processions and killed American citizens.
Insurgency and terrorism thrive on overreactions from the U.S. government.
For every opposition fighter killed, there is a family member or innocent ready to retaliate.
The collateral rate in Afghanistan is high despite solid intelligence assets.
Parents in Pakistan have withdrawn children from school due to fear of drone strikes.
The secrecy and lack of transparency surrounding the drone program breed insurgency.
The drone program is currently used as an assassination tool rather than surgical strikes.
The lack of transparency suggests a potential increase in drone usage and collateral rates.
Beau advocates for drones to be used similarly to manned aircraft missions, except for surveillance.
The fear instilled by drone strikes, not freedom or culture, drives hostility towards the U.S.

Actions:

for policy makers, activists, citizens,
Advocate for increased transparency and oversight of the drone program (implied)
Support initiatives demanding accountability for drone strikes (implied)
Raise awareness about the consequences of drone strikes on innocent civilians (implied)
</details>
<details>
<summary>
2019-04-04: Let's talk about the advice I give my sons.... (<a href="https://youtube.com/watch?v=qxM3txDfMSg">watch</a> || <a href="/videos/2019/04/04/Lets_talk_about_the_advice_I_give_my_sons">transcript &amp; editable summary</a>)

Beau advises on embracing experiences, taking risks, valuing attitude, and shaping the world through actions and choices.

</summary>

"Embrace them all, learn from them all, experience them all."
"It's the risks I didn't take that I regret."
"Your beliefs mean absolutely nothing if you don't act on them."
"Don't let others frame your thought."
"Make sure you consider that when you're making them."

### AI summary (High error rate! Edit errors on video page)

Received questions on advice for sons on living life.
Emphasizes that there's no right way to live life and to ignore critics.
Stresses the importance of embracing both good and bad experiences.
Shares a personal anecdote about a defining canoeing adventure at 17.
Encourages having adventures while young.
Advises not to regret the things done in life but the risks avoided.
Urges to take risks as anything worthwhile involves some risk.
Warns against living a safe, predictable life within the bell curve.
Advocates for being selective with friends and valuing true friendship.
Emphasizes the significance of attitude in facing life's challenges.
Encourages continuous self-improvement even in difficult times.
Provides advice on love, advocating for following one's heart over peers' influence.
Reminds to remain curious and constantly seek knowledge to grow.
Advises developing skill sets for independence and freedom.
Encourages asking the right questions and taking actions based on beliefs.
Urges uplifting all people, avoiding putting others down for personal gain.
Reminds that individual choices shape the world and to be mindful of this impact.
Mentions being prepared for deep tests in life and how life experiences lead up to them.
The advice for his daughter mirrors the advice given for sons.

Actions:

for young adults,
Embrace both good and bad experiences (implied).
Have adventures while young (implied).
Take risks in life (implied).
Be selective with friends and value true friendship (implied).
Keep moving forward in challenging situations (implied).
Follow your heart in love and ignore peer influence (implied).
Remain curious and seek knowledge constantly (implied).
Develop skill sets for independence and freedom (implied).
Ask the right questions and act on beliefs (implied).
Avoid putting others down for personal gain (implied).
</details>
<details>
<summary>
2019-04-04: Let's talk about discussing LGBTQ figures in schools.... (<a href="https://youtube.com/watch?v=VlYjoyyNX4w">watch</a> || <a href="/videos/2019/04/04/Lets_talk_about_discussing_LGBTQ_figures_in_schools">transcript &amp; editable summary</a>)

A parent's outrage over a gay historical figure prompts Beau to challenge the discomfort with discussing LGBTQ+ history, stressing its integral role in shaping our past and present.

</summary>

"History would get really, really, really bare if we didn't talk about gay people."
"It's almost like they're here and you need to get used to it."

### AI summary (High error rate! Edit errors on video page)

A book was read in school, and a parent got outraged because the historical figure was gay.
The outrage stemmed from discomfort with discussing gay people in school.
Beau questions what will happen when children learn about uncomfortable topics like genocide or nuclear war if discussing a historical figure being gay is uncomfortable.
Beau provides examples of significant historical figures who were gay, such as Alexander the Great, Julius Caesar, Michelangelo, Sally Ride, and many others.
He argues that excluding the mention of gay people in history erases significant contributions and diminishes the richness of historical knowledge.
Beau points out that homosexuality has existed for thousands of years and doesn't need to be "normalized" as it is already a normal part of society.
He offers to have a face-to-face talk show to address the issue with those uncomfortable discussing gay historical figures.
Beau underscores the importance of recognizing the presence and contributions of gay people throughout history, urging people to accept it as a part of reality.

Actions:

for teachers, parents, students,
Educate yourself and others on LGBTQ+ history (implied)
Advocate for inclusive and diverse historical education in schools (implied)
</details>
<details>
<summary>
2019-04-03: Let's talk about the Comanche, newcomers, and avocados.... (<a href="https://youtube.com/watch?v=SYIvoz5HyC0">watch</a> || <a href="/videos/2019/04/03/Lets_talk_about_the_Comanche_newcomers_and_avocados">transcript &amp; editable summary</a>)

Beau challenges viewers to rethink American culture by reflecting on the historical influences of newcomers like the Comanche tribe and urges acceptance and growth for a better future.

</summary>

"I have a feeling that if horses and dogs can change cultures so drastically, so much, that people can too."
"I have a feeling that if horses and dogs can change cultures so drastically, so much, that people can too."
"Every group of people has that newcomer, has that thing that they need to accept for their culture to continue, to grow, otherwise it stagnates."
"The sad part is, as we talk about closing the border, Americans are more concerned about avocados than people."

### AI summary (High error rate! Edit errors on video page)

Challenges viewers to imagine a Comanche warrior instead of a helicopter.
Traces the historical connection between dogs and human culture through campfires.
Emphasizes the importance of understanding American culture as constantly evolving.
Comments on the older American culture of the Comanche tribe and their adaptation to new technologies.
Describes how horses transformed the way of life for the Comanche people.
Points out that newcomers have always influenced and enriched American culture.
Raises concerns about the current resistance towards newcomers in America, drawing parallels with past immigrant groups.
Criticizes the prioritization of avocados over people in the context of border debates.

Actions:

for americans,
Accept newcomers for cultural enrichment (implied)
Prioritize people over commodities like avocados (implied)
</details>
<details>
<summary>
2019-04-01: Let's talk about what we can learn from three Mexican countries.... (<a href="https://youtube.com/watch?v=GZ8--13oe9E">watch</a> || <a href="/videos/2019/04/01/Lets_talk_about_what_we_can_learn_from_three_Mexican_countries">transcript &amp; editable summary</a>)

Beau questions the implications of cutting aid to countries and the government's role in controlling citizens' movement, urging caution in granting too much power to politicians.

</summary>

"There are three Mexicos and Trump can't get any of them to pay for the wall."
"The power you grant to your favorite politician is going to be there when the politician you hate takes charge."
"Maybe nobody should have this kind of power."

### AI summary (High error rate! Edit errors on video page)

Fox News mistakenly referred to El Salvador, Guatemala, and Honduras as "three Mexican countries," prompting jokes and confusion.
The decision to cut aid to these countries is concerning and may lead to more refugees, according to experts.
Beau disagrees with the experts, believing that cutting aid could allow the people to take control of their countries back.
The aid was cut because the countries didn't do enough to stop refugees from leaving, reminiscent of Cold War tactics.
This decision implies that the government should prevent people from leaving during difficult times.
Beau questions whether it's right for any government to have the power to restrict its citizens' movement.
He brings up the potential dangers of granting such power to politicians, regardless of political affiliation.
Exit visas, a control measure on citizens leaving, are still present in some countries but are not common.
Beau warns about the dangers of giving politicians unchecked power over citizens' mobility.
The transcript ends with a thought-provoking message about the consequences of granting too much power to political figures.

Actions:

for conservative viewers,
Question the decisions made by politicians regarding aid and citizens' rights (implied)
Advocate for transparency and accountability in government actions (implied)
</details>
