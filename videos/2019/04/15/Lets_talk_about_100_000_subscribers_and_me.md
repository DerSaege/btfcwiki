---
title: Let's talk about 100,000 subscribers and me....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=v8H7OdZzNH4) |
| Published | 2019/04/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Started as a journalist downplaying his accent, which changed after colleagues discovered his Southern roots during a drinking session in 2016.
- Made videos showcasing his accent and redneck persona, which garnered more attention and acceptance for his social commentary.
- Initially played up his Southern accent but gradually reverted to his real accent, except for satire videos where he exaggerates it for humor.
- Clarified his background, confirming he is from the South and has lived in various southern states.
- Addressed false claims about being a convicted human trafficker, attributing them to libelous allegations from 2013.
- Shared insights into his past as a consultant contractor and emphasized always being a civilian, despite misconceptions about his military involvement.
- Responded to common questions about his personal life, activism, and philosophy, including clarifying his approach to conveying ideas over using specific terms.
- Encouraged viewers to find ways to help their communities based on their skills and interests, without needing to adopt a militant approach.
- Explained the purpose of his videos, discussing the difficulty of staying informed and avoiding scripted content in his delivery.
- Mentioned creating a playlist of music he listens to and thanked his audience for their support.

### Quotes

- "I don't think that the message should be clouded because somebody did something you think is cool or something that you hate, it shouldn't matter."
- "Figure out what your skill set is, I assure you it's needed. Get out there."
- "I don't use that word. That word is very loaded. You say that word, people picture teenagers in leather jackets with spiky hair throwing rocks at cops."

### Oneliner

Beau shares his journey from downplaying his accent to embracing his Southern roots, addressing misconceptions, and encouraging community engagement based on individual skills and interests.

### Audience

YouTube subscribers

### On-the-ground actions from transcript

- Reach out to various news networks like Greed Media or Pontiac Tribune based on interests (suggested)
- Find ways to get involved in community activism using your unique skill set (exemplified)

### Whats missing in summary

Details on Beau's specific journalism projects and the impact of community engagement on social change.

### Tags

#SouthernRoots #Journalism #CommunityEngagement #Activism #Misconceptions


## Transcript
Well, howdy there, internet people, it's Beau again.
100,000 subscribers on YouTube.
That is substantial.
Thank you.
Means a lot to me.
So we're gonna take this moment to bring everybody up to speed,
answer the most frequently asked questions,
and then answer two questions, kind of,
that aren't really frequently asked,
But when people do ask, they tend to want some kind of an answer.
I'm going to answer those as best as I can.
OK, so to catch everybody up to speed.
Now, if you're on Facebook watching this,
you probably already know all this, because you've got to see it happen.
You have to watch it develop in real-time.
If you're on YouTube, there may be a little bit of confusion.
You may have gotten a recommended video for a guy who looks just like me,
but doesn't have an accent.
Well, to hear you guys talk about it in the comments section, I still have an accent that's
just downplayed.
Yeah, that's me.
Okay, so I'm a journalist.
Generally speaking, people don't really think you're intelligent if you sound like an extra
from deliverance.
So for years, I downplayed my accent.
Then in 2016, I think, I was drinking with some of my colleagues and the accent slipped
out and they're like, oh my god, you're a redneck.
Yes, yes, thank you, I am a redneck, yeah.
And they encouraged me to make a video kind of showcasing my accent and they thought it'd
be a good gag for my core readers. You know, hey, your international affairs
expert is actually a guy who got put in cuffs when he was a teen because he had
captured an alligator and was gonna let it loose in one of his friend's trailers
as a joke. Yeah, so I made the video and I played up the Southern and the Redneck
as much as humanly possible. I was drinking a beer, had dip in my mouth,
wearing overalls and a trucker cap sitting on a tractor. That is obviously
not how I normally dress but well I didn't have to go buy anything other
than the dip so you can infer from that what you will. As time went on I made
more videos and they're all on my personal page on Facebook and it didn't
really didn't really carry a lot of weight you know it was basically me
making fun of me with a little bit of social commentary thrown in. It didn't take me long
to realize though that the social commentary was much more widely accepted coming from me
with my accent than without it. I could make the same video and the one with the accent is the one
that would get the most attention and people would talk about and therefore in theory would
catch some of the ideas. So I made these videos like this for, I don't know, a year and then put
them on their own Facebook page. It was never supposed to be a secret. There was never supposed
to be a double identity thing going on. In fact the very first video on the on the Facebook page
starts off with Justin said I'd be more effective on my own channel. Justin is my given name by the
the way. I also go by Mick and throughout the videos there are kind of head nods
and little inside jokes to my core readership. In fact in the most recent
one you guys on YouTube will have seen this one where I was on the border
pretending to be on the border get that idiot Justin on the phone is a line in
it when I was talking about Benghazi and I was like you know they couldn't
control their client it happens that is also a little nod to people who who know
me a little bit more about me so in the beginning I really played up the accent
and as time went on I kind of walked it back to my real accent what you hear now
Now that's pretty much how I talk except you're in the satire videos because I do tend to
play it up during those because well it's funny.
Okay so now you're kind of caught up.
Now I know on YouTube you may not have ever seen the Real Heavy Accent because I think
it started, that channel started after I stopped using it.
But it's out there.
Now the immediate follow up questions to this are you know are you really from the south?
Yeah, I'm really from the south.
Grew up mainly on the Tennessee-Kentucky line and then lived in Georgia, Alabama, Florida,
Texas.
I went to a high school that was so small, it was a high school and a middle school.
I lived, well I don't live in town, I live just outside of a town that has a population
of about 4,000 people.
So yeah, really from the south.
Okay, so now one of the questions that pops up, not one of the most frequently asked questions,
but one of the questions that pops up kind of often, am I a convicted human trafficker?
No, of course not, that's stupid.
Convicted alien smuggler.
There is a difference.
I know about the blog, I've read it, it is entertaining.
Most of what is contained in it, like 95% is utterly false.
Fact check it.
Fact check it.
I know there's a theory that I wrote it as kind of a teaching tool because I am known
for going over the top to prove a point.
As much as I would love to claim credit for that, that's not true.
That was put out by somebody in 2013 who was operating at the behest of somebody who was
angry because I wouldn't interview them.
That's where that came from.
The reason I never sent a letter, because it is plainly libelous, the reason I never
sent the letter is because it is a good teaching tool.
It shows exactly how the use of official documents and putting them in a false context can easily
lead the reader to believe something that isn't true, to draw the wrong conclusions.
Like I said, just starting at the top of it, no, that's not the right charge, no, I didn't
take a deal, no, I didn't get out in an abnormal amount of time, I didn't get a short sentence.
All of this stuff is easily fact checkable.
I know there are people that want to know about it... no... no.
It's irrelevant to what I'm doing today, and I know to you guys it's interesting to me
it is something that happened more than 10 years ago.
I'm also not going to put myself in the position of defending myself in the comments section
against any baseless accusation that comes along, so no.
As far as my background before then, there are a lot of questions about that.
I can give a little bit of insight.
Yeah, I was a consultant, contractor, whatever term you want to use.
I don't really care.
I was always a civilian.
This is something that comes up pretty often.
Always a civilian.
That doesn't mean your buddy lied to you though.
Everybody may just have not known who I was.
There are a number of people who are like, you know, hey, well, my friend met you here.
Maybe, maybe he did.
But I wasn't active duty.
Ever.
There are people who are like, you know, my friend says you were in this unit.
No, I wasn't.
I wasn't.
May have been physically present with people who were from that unit.
I was never in that unit.
I was always a civilian.
Always.
Right now there are people from that community going, oh, you're one of those guys.
Now I'm a literal farm boy, not a figurative one.
I was never part of that organization either.
People want details about that and what I did and stuff like that.
Whatever the public record says is what I'm going to say is true, regardless of whether
or not it is.
I think the reason that industry in particular is having so many problems and is looking
around going, you know, where are we going and why are we in this handbasticate is because
people don't know when to shut up.
So I'm not a big fan of tell-all books, as you might imagine.
So anyway, now on to the most frequently asked questions.
First, are you married?
Yes, happily married, extremely married, super married, a bunch of kids married, wonderful
woman. Okay. Is this you? This is always accompanied by a photo, probably. If it's a photo of a
riot or a protest, yeah, that's probably me. That was me in Ferguson, that was me in
Flint, that was me in DC, that was me being waterboarded, yeah. Some of that is my own
personal activism some of that is because I'm a journalist and I go to
these things. As far as photos before that period whatever the public record
says determines whether or not I'm the guy in that photo as far as I'm concerned.
I will comment on one set of photos in particular because they are being
presented way out of context. There's a photo, two photos, of me and a certain
former lieutenant colonel. Yeah, that is me in the photo. However, that is not some
CIA super spook convention. That was him in a book signing and I was handling the
security. I was contracted by the venue. We're not like close friends or anything.
I do believe that he would remember me, but solely because I thought he'd won his senatorial
campaign and I addressed him as senator, which is possibly the most embarrassing moment of
my professional career.
A lot of people have questions about him and what he's like.
No comment.
That is the industry standard.
You don't comment on any of your clients.
If you say something nice, even if you have something nice to say, if you say something
nice about one and then no comment on another, the inference is that the person you gave
no comment on is bad.
So you simply no comment on everybody.
How can I help?
And this is a general question about how can you help the world?
You don't have to go all militant, as you just found out.
That may put you in a tight spot.
Figure out what your skill set is, I assure you it's needed.
Get out there.
The hardest part is getting active.
Once you get active, oh believe me, you're going to have more to do than you can possibly
imagine.
You just have to figure out where you are needed.
would advise watching the series on how to change the world. Now this is a good
one because the first couple of times I got this, I didn't really know how to
respond. It threw me through a loop. Most of the time it was very accusatory. I
chose one that was phrased a little bit nicer. How is what you're doing any
different than performance art? Now when I first got the question I'm like this
This is gonzo journalism, it's not performance art, don't be crazy.
And then I tried to actually spell out the differences and it got really hard, especially
considering I do satire at times.
I don't know, I don't know you got me.
It may be the same thing, it may be the same thing.
I try to convey fact rather than emotion, which was what I think most performance art
tries to convey. But I don't know that that's a significant difference in the grand scheme
of things. I would say that maybe gonzo journalism, especially now that we've moved to video,
is a form of performance art. How can I help with TFC? The fifth column is a huge news
network, huge, multiple websites. Find the one that's right for you. If you're a feminist,
head over to Greed Media. If you want to cut your teeth on independent journalism, go to
Pontiac Tribune. The main fifth column site is being restructured at the moment. In fact,
I think the only thing going up on it is my videos. But it will be back online soon. If
you want to delve into international affairs, head there. Even if you just want to make
memes. Reach out to them via their social media. Surprisingly common question, what
are your dogs named? My dogs are named Baroness and Destro. Yes, after them.
They're the bad guys in GI Joe. My German Shepherd is named Baroness.
Baroness, the character, was an Eastern European woman and well, kind of fits.
Destro was a silver-headed black-bodied guy and my silver-headed black-bodied
husky is named Destro. Why don't you show your whole t-shirt? Looks weird in the
framing? It looks weird in the framing. That's really the only reason. I wish it
could because most of them are from the TFC shop and benefit charity. How do you
stay informed. Staying informed isn't the problem. It's getting informed to begin
with that it's the real issue. To understand what's going on in Syria
today, you need to know what happened in Syria in 1991. So it's a whole lot of
reading of history books before you can get to really understanding current
affairs. Once you have that base of knowledge, it's really easy. So it's just
It's a lot of reading, I definitely think that reading in this case is better than YouTube
videos because you can get more in depth and you can bounce around via hyperlinks and get
to the information you need.
Are you ever going to openly discuss your philosophy?
I like to think I discuss my philosophy every day in these videos.
I don't use that word.
That word is very loaded.
you say that word, people picture teenagers in leather jackets with spiky hair throwing
rocks at cops. That's not what it is. So I avoid that term. And it's become a running
gag among people who know me. We're 200 videos in, and I've actually never said that word
in any of them. And I'm not going to start now. I discuss the ideas. I discuss the ideas.
ideas. Do you script and how many takes does it take? I don't script I get a good
idea of what I'm gonna say and then kind of shoot from the hip. That's why
sometimes my delivery is a bit off. How many takes? I can get it done in one as
long as nothing crazy happens. You know an animal comes in or I get a little
fired up and cuss. You've probably noticed I don't cuss in my videos. I just
think this is the appropriate venue to talk the way I sometimes do. And then
another one, what kind of music do you listen to? I'm actually going to put
together a playlist as soon as I stop filming this. So there you go, you're
caught up and you've got the frequently asked questions answered and then a
couple of bonus questions is answered as best I can. I know that some people are
going to want more information. It is what it is. It was a long time ago. It's not relevant
to what I'm doing today. Ideas should stand and fall on their own. Ideas should stand
and fall on their own. I don't think that the message should be clouded because somebody
did something you think is cool or something that you hate, it shouldn't matter.
The information is there and that's what matters.
That's what I'd like to focus on rather than my personal history.
That's one of the things, one of the reasons I don't talk about it is because normally
once it comes up, that's all people want to know about and it kind of derails any meaningful
conversation. Guys, thank you. You probably don't know what this means to me. I really
appreciate y'all hanging out there with me. Anyway, it's just a thought. Y'all have a
Good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}