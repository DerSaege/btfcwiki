---
title: Let's talk about how Trump will be our first open borders President....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=iYuFYC720No) |
| Published | 2019/04/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Warning against de facto open borders under Trump's presidency, despite no one openly advocating for it.
- Trump's plan for processing immigrants resembles Ellis Island-style immigration.
- Fox News and mayors are unknowingly supporting de facto open borders due to Trump's manipulation.
- Trump's history of flip-flopping on issues like gun control and his current stance on immigration.
- Urging Democrats to prioritize freeing people from cages over political games.
- Suggesting that conservatives are being played by Trump into unintentionally supporting open borders.
- Emphasizing the need to maintain the appearance of opposition to de facto open borders.
- Speculating whether Trump is deceiving conservatives or simply acting impulsively.
- Stating that the ultimate goal is to get people out of cages, regardless of political maneuvers or motivations.

### Quotes

- "We're on the verge of the first open borders presidency."
- "He's advocating a return to Ellis Island-style immigration."
- "We have innocent people in cages."
- "He's just a man-child throwing a temper tantrum."
- "At the end of the day, we're going to get people out of cages and that's what matters."

### Oneliner

Beau warns of de facto open borders under Trump's presidency, exposing the unintentional support from conservatives, urging action to free caged individuals.

### Audience

Politically engaged citizens

### On-the-ground actions from transcript

- Advocate for freeing individuals from cages (implied)
- Stay informed and aware of political manipulations (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of Trump's immigration policies and their potential consequences, urging action to prioritize human rights over politics.

### Tags

#Immigration #Trump #PoliticalManipulation #HumanRights #DeFactoOpenBorders


## Transcript
Well howdy there Internet people, it's Beau again. We've got to talk about open
borders. Well de facto open borders. Don't call it open borders because that
that'll that'll mess up the con. But we're about to have them.
We're about to have de facto open borders. We're on the verge of the first open
borders presidency and it's funny to me because the Democrats are pretty
much already holding their primaries and everybody's already campaigning
And all of them are saying, nobody's saying open borders.
And then there's Trump over there.
Oh yeah, we've got to build a great wall, a big wall, but we're going to bring
these people in, we're going to process them, and then we're going to let them go.
And his followers don't understand that basically he's advocating a return
to Ellis Island-style immigration.
It's open borders with an extra step.
And he's so good at conning conservatives that Fox News is advocating for de-facto open borders.
It's amazing.
The mayors are already on board with it.
They're tweeting, send them here.
And the conservative base that follows Trump that doesn't know how to do math,
they're over the head, he's going to drop off 100,000 illegals.
There's 200 jurisdictions in the United States that are sanctuary.
That's like 500 people each.
It's not even going to be noticed.
I understand the urge among Democrats to resist all that's Trump because he's been at this
long con for so long that we forget that he's really a New York liberal.
Back in the day, he said he was foreign assault weapons banned, and then he had to run Republican
And then he was like, oh, no, no, I'm not.
And they believed him.
And then we got the bump stock ban.
We've got red flag laws going up everywhere.
You have the president of the United States saying, take the guns first, worry about
due process second.
And they don't see it because they bought into his routine.
And now they're advocating for open borders.
We just need to take this.
Take this win.
I mean, we're on the verge of the first open borders presidency and it's being advocated
by the far right because they've fallen so deep into that cult of personality they don't even
understand what they're advocating for anymore. So to the Democrats in Congress that are still
playing politics, stop. Stop. Remember what's at stake here. We have innocent people in cages.
We have children in facilities that where abuse runs rampant, so rampant there's hearings
about it and you're talking about it in Congress, remember, get them out of the cages first.
We can worry about the logistics later.
This isn't going to be a big deal.
We're only talking about a few hundred people per city.
But we have to keep up the appearance that we're against this.
I understand that, because if the conservatives understand that they're getting played and
that Trump has basically just given the progressives everything they could possibly ever want,
then they'll start to pull away from that idea.
But we just need to kind of keep up pretending like, oh, this is a bad idea.
It's political retribution.
Oh, please don't do it.
But at the same time, we don't want to fight too hard because Trump in his 4D chess is
going to bring about de facto open borders in the United States.
And I don't know if he's actually conning conservatives or he's just a man-child throwing
a temper tantrum, but the end result is the same.
If he enacts this, yeah, it's open borders.
Call it whatever you want.
But the end result is open borders.
An even worse case scenario, he tries to make it painful for the sanctuary cities that are
taking them, and he loads them all up on boxcars and sends them in 10,000 at a time.
We can work out the logistics to get them moved to the other jurisdictions, and then
from there they can be anywhere in the country.
And he knows this.
He knows this.
He's just got to keep playing to his base.
Let him keep playing to his base because at the end of the day we're going to get people
out of cages and that's what matters.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}