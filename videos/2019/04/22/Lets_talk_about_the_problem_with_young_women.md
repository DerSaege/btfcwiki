---
title: Let's talk about the problem with young women....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=rt7lT1Vo3xg) |
| Published | 2019/04/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Kindergartner forced to change clothes at Hugo Elementary in Minnesota over thin straps on her sundress.
- Dress code meant to avoid distraction to boys, but it's kindergarten, are girls still icky?
- Staff should be held accountable for any distractions, not the kindergartner.
- Incident at Madison High in Houston where a parent was forced to leave for enrolling her child while wearing a Marilyn Monroe t-shirt and headscarf.
- School regulations restrict women from expressing their identity or ethnic heritage.
- Federal court ruling in North Carolina against making girls wear skirts when boys can wear pants.
- Regulations on women's clothing are about control and enforcing traditional gender roles.
- Lack of consistency in dress codes and regulations aimed at keeping women in their place.
- Incident in an Alaskan high school where a girl transitioning to a boy faced harassment in the bathroom.
- Boys attempted to regulate the girl's bathroom use, leading to a physical confrontation where the girl defended herself.
- Assertiveness is lacking in young women due to societal conditioning from a young age.
- Women are often discouraged from standing up for themselves and developing confidence and identity.
- Women facing challenges in developing confidence and self-reliance from a young age due to societal norms.
- Society's inconsistency in empowering women to stand up for themselves and develop an identity.
- Lack of outcry or outrage over incidents where women are mistreated or silenced.

### Quotes

- "Staff should be held accountable for any distractions, not the kindergartner."
- "Regulations on women's clothing are about control and enforcing traditional gender roles."
- "Assertiveness is lacking in young women due to societal conditioning from a young age."
- "Women facing challenges in developing confidence and self-reliance from a young age due to societal norms."
- "Society's inconsistency in empowering women to stand up for themselves and develop an identity."

### Oneliner

Kindergarten dress code incidents to high school bathroom confrontations, women face challenges in developing confidence due to societal norms and lack of empowerment.

### Audience

Educators, parents, activists

### On-the-ground actions from transcript

- Support initiatives promoting confidence and assertiveness in young women (suggested)
- Challenge dress code policies that reinforce traditional gender roles (suggested)
- Encourage young women to stand up for themselves and assert their identities (implied)

### Whats missing in summary

The full transcript delves deeper into the impact of societal norms on women's confidence and identity development.

### Tags

#DressCode #GenderEquality #Empowerment #SocietalNorms #Confidence #Activism


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're going to talk about a couple of different incidents, but they all share
a common theme, a very common theme, and they all create the same problem down the road.
It's very telling, it's very interesting as we think that we have moved so far as a society.
So a kindergartner in Minnesota at Hugo Elementary was forced to change clothes.
She had to put on a t-shirt because, well, the straps on her sundress were too thin.
That little hussy, a kindergartner.
Now the standard line when it comes to regulating girls' dress at school is always that, well,
distraction to the boys as if the boys having a lack of self-control is somehow their fault and
their responsibility to correct because well you know are you asking for it and boys will be boys
but the thing is this is kindergarten. Kindergarten. I'm fairly certain that girls
are still icky at this point in time. I mean you know it's been a while but I'm pretty sure that's
true. So if it was a distraction to anybody there, what would have been staff?
I think a much better use of time would be finding and identifying those staff
members that might have that kind of issue and getting rid of them rather
than humiliating a kindergartner over the way she's dressed. That's ridiculous.
Then we have another incident in Houston at Madison High.
Well, she couldn't be on campus, and the funny part about it was, it was a parent,
not a student.
She was coming to enroll her child, but she had a Marilyn Monroe t-shirt on that was long
and I guess shorts or a skirt underneath it.
And the shirt itself hung to her fingertips.
But that was just inappropriate.
She also had a headscarf on.
We know we can't have women doing anything that might express any form of identity of
their own, any form of ethnic heritage or anything like that.
Can't have any of that.
She had to leave.
She had to go.
Once again, she was enrolling her student, her child, in the school, which means she
would have been in the administration office.
So once again, not a distraction to the boys, which is always the cop-out, staff.
I guess maybe teachers just can't control themselves.
Maybe we shouldn't have males in schools, only have female teachers, because apparently
there's an issue.
They just can't control themselves.
And then we're not even consistent in this idea, because a federal court in North Carolina
had to rule that no, you can't make girls wear skirts when boys can wear pants, you know.
We're not even consistent in it because it's not about consistency. It's not about any real reason.
It's simply making sure that women stay in their place and understand that they are going to be
regulated right down to the way they dress. And go ahead, that's the United States, go ahead and
and complain about Sharia law.
Well, that skirt is too short.
No, you have to wear a skirt.
But not that one.
And we wonder why young women today have issues with identity,
have issues with self-esteem and confidence.
Well, maybe we should start letting them make decisions for themselves.
that might help in that process. If your entire life you regulated down to what
you wear and how you present yourself to society at large, how can you expect
them to have any identity of their own? Of course maybe that's the point. Then I
waited an entire week to bring up this next one because I was certain there
was going to be an outcry. Just outrage all over the country because there was
when it was in theory. I mean this is a little different but you know during
that whole trans bathroom thing there were men all over the country saying you
know if I ever saw a man go into the girls bathroom I'd beat him to death. Okay
So in a high school in Alaska, a girl transitioning to a boy was using the boy's bathroom.
Well some of the boys didn't like this and actually as it turns out it's actually one
of the boy's step dads or dad, foster father, something like that, that had a real issue
with it and kind of encouraged his son to handle it.
So he gathered a group of boys and went into the girls' restroom.
Not because of any identity that he may have, just because, well, he didn't like it.
Because everybody has to be regulated.
Now there was a girl in the bathroom and according to reports, well, he wouldn't let her leave.
Wow.
That sounds like a crime, actually.
She need him.
She need him in the groin.
And guess who got punished?
She did.
While fathers all over the country were saying that, I mean, this is a capital offense when
And it actually happens because it's not a trans person doing it.
It's just boys being boys.
Well, she can't do that.
She needs to be punished.
We can't let them get out of place, right?
They have to be regulated, controlled.
Our charge, we have to take care of them.
They're the fairer sex property.
And for the record, now my daughter is too young to have had this conversation with yet,
but I can promise you, if some young boy attempts to keep her in a bathroom, she can do a whole
lot more than he can.
But yeah, she ends up getting punished because we are not consistent as a society.
We can't have women standing up for themselves, again, they might develop an identity of some
Some sort.
You cannot complain or even pretend to care about many of the issues that are facing young
women today and remove any chance of them developing confidence and an identity of their
own and self-reliance and all of these things. You can't. And it starts in kindergarten.
From the very beginning, oh, don't wear that. You might provoke a reaction in kindergarten.
From the very beginning, it's always their fault. In that incident in Alaska, the response
was that she should have contacted a school official.
According to reports, he wouldn't let her leave.
How was she supposed to do that?
Assertiveness is something that is lacking in a lot of young women today.
This is why.
You wonder why women have a problem when speaking up for themselves, when men step in and try
to explain things to them they know, or basically just be patronizing.
That's why.
Oh no.
Don't stop someone from falsely imprisoning you, no.
You just suffer through it and wait, and then get someone else to take care of it for you,
little lady. But again, no outcry, no outrage, none. It's almost like it really wasn't about
protecting your dogs. It was about being a bigot. Anyway, it's just a thought. Y'all
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}