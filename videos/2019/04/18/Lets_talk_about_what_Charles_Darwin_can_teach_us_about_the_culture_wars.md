---
title: Let's talk about what Charles Darwin can teach us about the culture wars....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ZcQNOL2tQ4g) |
| Published | 2019/04/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Charles Darwin's quote on adaptability being critical for survival resonates in cultural contexts as well.
- The U.S., especially the southern states, is embroiled in a cultural war between regressive and progressive factions.
- Arkansas is replacing statues of a Confederate lawyer and a segregationist with desegregationist Daisy Lee Gatson Bates and musician Johnny Cash.
- Beau questions the importance of preserving Confederate monuments that represent a short-lived era.
- He criticizes how the U.S. clings to outdated hierarchical structures and fails to progress like the rest of the world.
- The only remaining Confederate tradition in the South is poor white individuals fighting battles for rich white elites.
- Beau laments that culture wars distract from addressing root issues of societal control by the wealthy.
- He believes America is stagnant because it fights battles from the past instead of embracing change.
- Beau warns that those yearning for a mythical past are undermining the progressive ideals America once stood for.
- He expresses concern that the American culture is dying due to a lack of evolution and adaptability.

### Quotes

- "If you are one of those fighting to return to the good old days, whenever that was, you're killing America."
- "American culture will die because we're not changing."
- "We're fighting battles that were decided 150 years ago."
- "The culture of this country is dying."
- "Those who see themselves as the defenders of the American ideal don't even know what it is anymore."

### Oneliner

Charles Darwin's quote on adaptability applies to America's cultural war, where clinging to the past threatens progress and leads to the demise of American ideals.

### Audience

Americans, Activists, Historians

### On-the-ground actions from transcript

- Advocate for the removal of Confederate monuments and the recognition of figures promoting progress (exemplified).
- Engage in open dialogues to understand the root causes behind cultural divides and work towards unity (implied).
- Support initiatives that aim to move away from divisive symbols and embrace a more inclusive cultural narrative (implied).

### Whats missing in summary

A deeper exploration of how societal progress is hindered by the fixation on past conflicts and symbols, leading to a stagnant culture lacking in adaptability and growth.

### Tags

#CulturalWar #Adaptability #ConfederateMonuments #AmericanIdeals #Progressivism


## Transcript
Well howdy there internet people, it's Beau again.
It is not the strongest of the species that survives, nor the most intellectual,
but the one most adaptable to change.
That is Charles Darwin's strongest quote.
I think that he would love that that's his strongest quote.
That quote can teach us
a lot about culture as well.
Cultures are the same way.
If it isn't adaptable,
it dies.
It goes away. It doesn't survive.
In the United States, and certainly in the southern United States, we are in the
middle of a cultural war.
You have those who want to reach back
to the 1950s or the 1850s,
and you have those that want to
take huge strides forward.
And they're fighting.
Arkansas has decided to yank its statues from Capitol Hill.
Each state gets to send up two statues, and then they line the halls up in Capitol Hill.
Arkansas is yanking theirs.
They're pulling theirs out.
One of them is of a lawyer who sided with Confederacy, and the other is a segregationist,
being replaced with Daisy Lee Gatson Bates and Johnny Cash.
was a desegregationist. Johnny Cash was of course a musician. Not exactly the
greatest role model, but I can see why Arkansas chose him. It's great. I mean
Arkansas is entering the 20th century. It's sad that it's now the 21st century,
but whatever. Take the wins where we can get them. Now of course this move is
going to make people angry. A certain subset of society has decided that the preservation
of Confederate monuments, for whatever reason, is important. I'm not sure why. The Confederacy
lasted five years in the 1860s. It doesn't define the South. It really doesn't. Southern
culture existed before the Confederacy and it has existed afterward.
This would be a lot like people a hundred years from now deciding that designing women
defined southern culture, incidentally designing women lasted longer than the Confederacy.
For those that don't know, Overseas Viewers, it was a TV show.
The thing is, the United States as a whole is in the same situation.
We're still clinging to ideas that died a hundred years ago everywhere else in the world.
The idea of this hierarchical structure and keeping each other down, kicking down instead
of punching up at those in power and trying to get them to behave.
We just want to make sure we're better off than those people.
Incidentally, the only Confederate tradition that is still alive in the South is a bunch
of poor white guys going off and fighting the battles of a bunch of rich white guys.
That's about it.
Everything else is gone.
The Confederacy is gone.
that had to do with it is gone. That's the only tradition that remains. It's kind of
sad that that is still around. And that's what it is. You know, a lot of these flash
points in these culture wars, they stem entirely from the wealthy convincing poor people to
fight their battles for them.
We those not in the top 10% of this country or the top 1% or the top.01% are fighting
each other rather than trying to figure out why we're allowing ourselves to be controlled.
That's really what the culture war should be about to me.
But instead, we're fighting over statues.
We're fighting over flags.
We're fighting over things that do not matter.
American culture will die because we're not changing.
We're fighting battles that were decided 150 years ago.
clinging to this ideal of the 1950s that never existed. It was TV shows that people
in my generation watched. That was great. It was never like that. That's not real.
And as we fight these battles, we don't progress. We don't move forward. We don't
embrace new technologies and new ideas. We're stagnant and a culture much like a
species that is stagnant that does not change. It dies. If you are one of those
fighting to return to the good old days, whenever that was, you're killing America.
You're killing the ideas that were present at one time. America had great
ideas. It didn't always live up to them, certainly, but the promise that the
United States kind of gave to its citizens was pretty cool. It was pretty
cool. And as history progressed, more and more people were given that promise
because initially that promise applied to a very few, a very, very few, and it
extended, and it extended, and now we still fight to deny that promise to other
people. We preach about the land of the free and the home of the brave, and we
We fight freedom and we're terrified of everything.
The culture of this country is dying.
The DNA, what makes up America, is gone.
It's gone.
Those who see themselves as the defenders of the American ideal don't even know what
it is anymore.
just chosen a side in the red versus blue battles and they think they're
winning. Nobody's winning. We're all losing. We are all losing because we refuse
to progress. We refuse to think outside of history. We refuse to look forward
instead of back. If that doesn't stop, and stop pretty quickly, give it up.
up. Give it up. Incidentally, Darwin Strong's quote, he never said it. That's the funny
thing about that quote. He never said that, he never wrote that. That quote came about
in the 1960s when somebody paraphrased his work and just tried to sum it up. And because
And that summary of his work was so adaptable and so useful in so many different ways, it
survived and that became his strongest quote.
And I think he would love that, I really do, because it pretty much proves his entire theory.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}