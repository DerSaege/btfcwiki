---
title: Let's talk about a guy named Rooster and emergency preparedness....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=gzKSVhY-S-I) |
| Published | 2019/04/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces Rooster and his involvement in emergency preparedness work with Cajun Navy and Bear Paw Tactical Medical.
- Describes Rooster as a responsible individual who always stepped up during emergencies despite his lack of wound wrapping skills.
- Emphasizes the importance of emergency preparedness for everyone, regardless of their background or expertise.
- Acknowledges the questions he receives about survival situations and announces plans to share videos on basic survival skills.
- Stresses the necessity of putting together an emergency bag containing essentials like first aid kits, medications, food, water, fire-starting materials, shelter items, and tools.
- Advises against prepacked bug out bags and encourages customization based on individual needs.
- Recommends practical items like canned goods, rice, and a can opener for emergency food supplies.
- Suggests having a supply of bottled water, purification tablets, or filters for clean drinking water.
- Lists items like flashlights, fire-starting tools, shelter materials, and knives as critical for survival.
- Urges the importance of having an evacuation plan and backups in place tailored to individual family situations.

### Quotes

- "Everybody needs to know something about emergency preparedness."
- "Everybody needs one. Everybody. Everybody. I can't stress this enough."
- "Emergencies by their very definition you don't know they're coming so get it together anyway."

### Oneliner

Beau stresses the importance of emergency preparedness for everyone and provides detailed guidance on assembling an emergency bag with essentials for survival in various situations.

### Audience
Community members

### On-the-ground actions from transcript

- Prepare an emergency bag with essentials like first aid kits, medications, food, water, fire-starting materials, shelter items, and tools (suggested).
- Customize your emergency bag based on your family's specific needs and situation (suggested).
- Develop an evacuation plan with multiple backup options in case of emergencies (suggested).

### Whats missing in summary

The full transcript provides detailed insights and practical tips on emergency preparedness, which can be best understood by watching the entire video.

### Tags
#EmergencyPreparedness #SurvivalSkills #CommunitySafety #DisasterPreparedness #EvacuationPlanning


## Transcript
Well howdy there internet people, it's Beau again.
So tonight we're going to talk about a guy named Rooster and emergency preparedness.
Now the first time I ran into Rooster, I want to say he was working with the Cajun Navy
after Harvey and Bear Paw Tactical Medical had kind of drafted him and he was doing relief
work there.
Good guy.
of those people that always showed up in the embodiment of rule 303. He had the
means therefore he had the responsibility and when something went
wrong he rolled out. He did relief work during Florence, he was active out on the
res, very very good guy even though according to people at Bear Paw he
couldn't wrap a wound for anything but he was a very very good guy. We lost him
on the 18th. He's gone. I can't think of a more fitting memorial for him than
discussing a little bit about emergency preparedness because everybody needs to
know something about it. Doesn't matter if you're a soccer mom or you're an
X Ranger. You need to stay up to date and you need to have your stuff together.
because there are people who will come to help but the more self-reliant you
are the less stress you're putting on them because they can focus on those
people who could not help themselves so if you have the ability you have the
means you have the responsibility to put it together and take care of yourself
So as more and more people have found out about my background, I'm getting more
and more questions related to survival in various situations. I never want this
channel to turn into a survivalist channel or an insurgency channel or
anything like that. There are channels out there for that.
that's not really what I want to do here. However, I will start putting up some
videos and not putting them into the newsfeed, just uploading them and then
they won't go out to anybody and I'll put them in a playlist that covers some
basic skills and then if you're on Facebook I'll share the playlist every
once in a while and you guys can go through and watch them. But to start off
with, because this isn't specialized stuff, this is something everybody needs,
We're gonna talk about putting together an emergency bag. Everybody needs one.
Everybody. Everybody. I can't stress this enough. When people think about this
stuff, they think about guys who are preparing for doomsday. Hurricanes,
wildfires, earthquakes, blizzards. There's something in your area that can happen
it very very quickly that you're going to need to be prepared for and when I go
through this list it's very simple it is very very simple and most people are
like well I have all of this stuff great where is it and then you see them go
what's in my house well this is in the junk drawer this is in my bathroom this
is great get it all together and put it in one spot because during an emergency
you're not going to have time to run around and pick everything up. The other
question that I've gotten a lot especially after I did the video on
prepacked first aid kits, do you do prepacked bug out bags exist? Not really.
There are companies that make some generic base versions. They're not
they're not great. You'll do better putting one together on your own. Now if
If you just don't have the time, yes, by all means order one.
Having that is better than having nothing.
But you'll be better off putting one together for yourself because it will be tailored to
your needs.
My family, we're talking about an emergency, we're talking about seven people and two dogs
and three of the people are under the age of five.
That's probably different than yours.
That's why the prepacked bug out bags aren't such a great idea because they give you a
false sense of security.
If I pick up one of those, the supplies, they're not going to last a day.
So there's that.
And then there are companies who will literally go out and tailor make your bag for you.
The problem is these services are thousands of dollars and you can put it together yourself
for a few hundred bucks and that's getting into that that's really putting
together a good bag. But the basics what do you need a first aid kit a good one
okay not the blue box you pick up at Walmart that says 153 pieces and 148 of
them are different size Band-Aids. If they can be fixed with a Band-Aid it
probably would have fixed itself on its own. You need a real first-aid kit. I will
link the the video I did on first-aid kits and put that in the comment section.
Then you also need your meds. If you are on meds of any kind that you have to take
regularly, you need a supply of those in this bag because pharmacies aren't going
to be open, you know, after Michael here took weeks before pharmacies really got
back up and running. So you need to have a supply of that on hand. Okay, food. Now
when you think emergency and food, you think MREs, mills ready to eat, used by
the army. Do you need those? No, of course not. One, they're pretty expensive. I mean
And when you break it down per mill, that gets expensive.
Two, they take up a lot of space.
That's not what you need.
Unless you just happen to have a warehouse,
you can store them in.
Now, most people today like to eat fresh and organic
and all that stuff.
It's fantastic.
If you have a specialized diet, that's great.
But unless you need it to survive,
you need to forget about that.
get canned goods, get rice,
get things that last a really long time
that are things you wouldn't normally eat
and that they have a long shelf life
because then you can put them in your pantry
and they'll stay there because they're not really,
you're not gonna be like, ooh, I really wanna eat that.
No, it's just gonna hang out.
That way you have a backup food supply.
And again, you're not looking for the tastiest thing
in the world, you're just looking for something
will keep you alive and it is easy to cook. And because you're doing that you
also need a can opener. Water, unless you know how to distill water and purify
water yourself, you need to get some bottled water. You don't have to go crazy
with it. A couple of cases that are three bucks at Walmart will probably do
you depending on how big your your family size is. Now aside from that you
can also get tablets to purify water you can get filters which are really cool
like the life straw or the sawyer but you need a supply of water because
depending on the type of emergency the water in your house may not be working.
Fire, and fire includes a flashlight.
Now, most emergencies, the wood around you is wet.
Even if you're in a rural area, pulling up the wood that just got rained on by a hurricane,
that's not going to do you much good.
It's going to be really hard to burn.
So you need some charcoal bags, stuff like that, especially for the first couple of days.
As the wood dries out, you can use that because odds are you will need more than a couple
of days worth because it's going to take a while to get the electric back on.
You need to know how to build a fire.
You need lighters.
You need fire strikers, that type of thing.
Waterproof matches would be great.
Then you need shelter.
You need to assume that the structure you're in is going to be damaged.
Tarps, if you don't want to go out and spend a bunch of money on a tent, tarps or those
little Mylar, the silver blankets, they're not great but they work and that's what you
have to focus on.
So you need a little bit of 550 cord, a rope, a thin rope.
And then you need a knife.
And when I say that, people are like, get a K-Bar or whatever.
I will tell you from personal experience, I have a K-Bar, I have a Fairburn.
After Hurricane Michael, what did I use the most?
My Swiss Army knife.
You don't necessarily need a fighting knife or a field knife.
You need something that will be able to cut open bags and stuff like that.
Now, that's the basics.
That's what you have to have to survive.
Beyond that, there's things that can make it easier.
A hatchet, a grill.
Things like this make it easier, but these are the items you have to have to survive.
Gather them all up, throw them in a book bag and put it in your pantry next to the canned
goods and the water.
Beyond that, you need an evacuation plan.
You need to know where you're going to go and then you need a backup for that evacuation
plan. A lot of times when people put one of these together they're like well I'm
gonna go over to my uncle Billy's house and uncle Billy is 40 miles away and the
hurricane hits you both. Well now what are you gonna do? You need to have two
head in two different directions. That's where you need to start at and once you
start you'll think of other things you need or want depending on your
more particular situation. You know, me personally, we have to have dog food on
hand because we have dogs. You know, we needed baby formula, things like this.
And you have to keep this stuff on hand and everything is different for every
family and every skill set. You know, if you graduated Sear and actually went
to the school, you probably don't need as much as the guy who works for Allstate.
state but you still need to have some of your stuff together. So that's that's
your starting point and then from from there we're gonna put up some videos
that will help if you don't know how to build a fire if you don't know how to
rig up a shelter these types of things we will get it all out there but the key
thing here is to remember that emergencies by their very definition you
don't know they're coming so get it together anyway it's just a thought
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}