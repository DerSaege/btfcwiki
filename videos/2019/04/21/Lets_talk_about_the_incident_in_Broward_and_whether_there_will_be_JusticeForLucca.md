---
title: 'Let''s talk about the incident in Broward and whether there will be #JusticeForLucca'
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wNgYOmWVShQ) |
| Published | 2019/04/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Deputies in Broward County beat up some kids, sparking a debate on the use of force.
- A key moment in the video is when an officer pepper-sprayed a kid who appeared to be picking something up off the ground.
- The focus should not only be on the excessive force used by one officer but also on the actions of the officer who pepper-sprayed the kid.
- The officer who pepper-sprayed the kid demonstrated deliberate and trained movements suggesting a proper technique for control without excessive force.
- Beau points out that the department may have proper training procedures, but these were not followed in the incident.
- The officer who pepper-sprayed the kid appeared calm and in control, contrasting with the excessive force used by another officer.
- Despite the questionable actions, it is suggested that the department's policy might justify the use of force as seen in the video.
- Beau stresses the importance of scrutinizing the actions of all officers involved in such incidents, not just those directly using excessive force.
- There is a need to focus on changing policies rather than solely seeking justice for individual cases to prevent such incidents from happening in the future.
- The situation is complex, with many unknown factors, but it is vital to analyze and address the systemic issues contributing to police brutality.

### Quotes

- "You can maintain control of a suspect without smashing their face into the concrete."
- "What it appears to me is that this department has been trained in the right way to do this."
- "The officer who pepper sprayed him was attempting to do it the right way."
- "It's very clear from the video."
- "There's a whole lot we don't know, but that's a pretty important piece that I don't see getting discussed."

### Oneliner

Deputies beating kids in Broward County reveal systemic issues beyond just excessive force, urging a focus on training and policy reform over individual justice.

### Audience

Police reform advocates

### On-the-ground actions from transcript

- Advocate for policy changes within law enforcement departments to ensure proper training and accountability (exemplified)
- Support initiatives that address systemic issues leading to police brutality (implied)

### Whats missing in summary

The full video provides additional context and details that can enhance understanding of the incident and the broader implications for police accountability.

### Tags

#PoliceBrutality #UseOfForce #PoliceTraining #PolicyReform #Accountability


## Transcript
Well, howdy there internet people, it's Bo again.
So we're going to talk about that incident in Broward County.
If you're not familiar with it, deputies beat up some kids.
That's what it boils down to.
We're not going to talk about whether or not the use of force was justified.
Based on the statements that we have, it wasn't.
But we don't have all the statements.
We do not have all of the facts.
So we can't really talk about that.
But there is something extremely interesting that occurred in that video that seems to
be getting missed.
And it's something that citizens in general, and especially police accountability activists,
need to pay attention to.
So what happened was a kid was detained, and another kid, looks like he picked something
up off the ground.
We can't tell because there's a crowd of people.
But when he comes up, an officer is approaching him, looks like he's, you know, basically
saying, what are you doing? Or something like that. We can't hear it, but all of a
sudden that cop pepper sprays him. The kid covers his face, goes, falls back into the
crowd, kind of flailing around. Cop grabs him, throws him to the ground. Now, what
most people are focusing on is the other officer who comes over, smashes the kid's
face into the concrete, punches him in the back of the head because he's trying
to cover his face, and then smashes his face again. That's what they're focusing
And yeah, that's important, that's definitely an important piece of the puzzle, but rather
than watching that and the other officers coming in to screen, pay attention to the
officer who pepper sprayed him.
He walks towards him, places one foot right at his hip, the other foot a little bit back,
bends both of his knees, and his arm is coming up like this.
Anybody who's been arrested knows what's about to happen.
That kid is about to feel that knee in the back, in the small of his back, center of
his back, and that forearm is about to rest right here.
What that tells me is that this department, at least some people in it, are trained in
the proper way to do this.
You can maintain control of a suspect without smashing their face into the concrete.
In fact, that's pretty much wholly unwarranted as far as legal, tactical, and medical liability.
The legal and medical liability there is pretty high and you're not gaining a big tactical
advantage.
It's not warranted.
Now what is warranted and what is within policy are rarely the same thing.
I don't see a high likelihood of people who are calling for justice in this case getting
what they want because more than likely this is going to come back as within policy.
You need to move to change the policy because what it appears to me is that this department
has been trained in the right way to do this and that the deputy that smashed his face
into it just chose not to.
It's important.
Now maybe he's a new deputy, maybe he wasn't trained in it.
There's a whole lot we don't know, but that's a pretty important piece that I don't see
getting discussed.
That officer who pepper sprayed him was calm.
He was abrasive.
He was commanding, but he didn't look like he had let his emotions take control of him.
Look at the other deputy.
People are asking, and this has been a specific question I've gotten, why did he throw him
to the ground to begin with. I know this is going to sound weird, that is the right move.
He pepper sprayed him. He used force on him. He'd be going into custody. That's just standard.
Whether or not it's right, it's what pretty much always happens. As far as throwing him
to the ground, the reason it's the right thing to do is that after the pepper spray, the kid
is blind, he's in a crowd, and he's flailing. He's a risk to the other people in the crowd.
him on the ground is the right thing to do. The way it was done, but I guarantee you that's within
policy. Not the way I would suggest it, but that doesn't look like it was done with malice.
that looks like, well, we got to put him down and here we go. And it certainly
appears that that deputy attempted to take him into custody the right way and
was interrupted by the other officer and the other deputy. It's something that
people need to pay attention to because that tells you that the department has
the training and has the skills that it needs to have and it's just not using
them. Now again, all of this is preliminary. We could find out more. We could find out
that he bent down to pick up a gun or a knife. I doubt it. I seriously doubt it. The most
likely story that I have heard, it appears he was picking up a phone that was dropped.
Again, don't know if it's true. We don't have all of those details yet. What we do
know is that the deputy who pepper sprayed him was attempting to do it the right way.
it's very clear from the video.
Go back, watch it, watch the different angles,
and you'll see that very deliberate movement.
It's exactly what he was gonna do.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}