---
title: Let's talk about the world I want to live in....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2pLb_uc0bo8) |
| Published | 2019/04/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Imagines a world where he can relax on his porch, drinking homemade whiskey, ordering pizza with cryptocurrency, delivered by a drone, funded by renewable energy.
- Describes a community where an interracial gay couple introduces him to their refugee child from a natural disaster.
- Envisions a society where education is a community asset, funded by bake sales and businesses voluntarily supporting a free hospital.
- Portrays a system where citizens handle issues without police, focusing on restitution-based penalties for non-violent crimes and rehabilitation for violent offenses.
- Paints a picture of a world without poverty, leading to reduced drug abuse and increased cooperation between communities.
- Envisions inter-community trade without profit motivation, faster inventions due to no intellectual property laws, and workers embracing automation for societal advancement.
- Dreams of a society where equality of opportunities eliminates bigotry, and people focus on bettering themselves and the world rather than dominating others.

### Quotes

- "I just want to be able to sit on my porch and drink homemade whiskey while I use my phone to order a pizza that's going to be delivered by a drone that I'm paying for with cryptocurrency..."
- "In a world like this, there's no need to escape, so drug abuse of course plummets."
- "Kings and queens are things that only exist on chessboards. Empires only exist in video games but free travel exists everywhere..."
- "People spend more time trying to master their own life or the world around them rather than each other."
- "I know sounds pretty utopian, right? But it's just a thought."

### Oneliner

Beau envisions a harmonious society where community support, education, and innovation thrive, eliminating the need for police and promoting equality of opportunities.

### Audience

Community members

### On-the-ground actions from transcript

- Support community-run schools through bake sales and donations (exemplified)
- Volunteer or donate to free hospitals funded by local businesses (exemplified)
- Participate in neighborhood food-sharing initiatives (exemplified)
- Advocate for restitution-based penalties for non-violent crimes (suggested)
- Encourage rehabilitation programs for violent offenders (suggested)
- Foster cooperation between communities through teleconferencing (exemplified)
- Support innovation by advocating for relaxed intellectual property laws (suggested)
- Embrace automation in industries for societal progress (suggested)

### Whats missing in summary

The full transcript provides a detailed vision of a utopian society where community support, education, innovation, and equality flourish, fostering a harmonious and crime-free environment. 

### Tags

#UtopianSociety #CommunitySupport #Equality #Innovation #CrimeFree #CommunityPolicing


## Transcript
Well, howdy there internet people, it's Bo again.
You know, I talk about what's wrong with the world so much.
People are asking me what kind of world I want to live in.
I never really answered before, not because I don't have an answer, but just because it's
so simple, it sounds silly to say it.
I just want to be able to sit on my porch and drink homemade whiskey while I use my
phone to order a pizza that's going to be delivered by a drone that I'm paying for
with cryptocurrency that was mined using renewable energy.
And I'm ordering this pizza because the newly married interracial gay couple next door is
coming over to introduce me to their recently adopted refugee child.
Now he's a refugee from a natural disaster because we don't have war refugees anymore.
They're bringing dessert.
They bought it at a bake sale benefiting the K through PhD community run school that is
free for all and stays fully funded because people understand that education is a community
asset.
After lunch, a nurse friend of mine is going to come over.
She works up at the free hospital that is voluntarily funded by local businesses who
understand that a healthy community is an economically viable community.
She's picking up some grapes.
She's going to turn them into jelly.
She's going to give me five jars, keep five jars, and give 10 jars to the local food bank.
Every yard grows food and what isn't needed is shared.
There's no big daddy government forcing all of this to happen.
It's just people helping people on the wall in almost every home as an automatic rifle,
But it's only taken down to be trained with or dusted, kind of like a fire extinguisher.
Because there's no poverty, there's not really any crime, therefore there's no cops, that's
handled by citizens.
Matters of justice are handled by an impartial jury.
When it comes to non-violent crimes, it's all restitution-based penalties.
When it comes to violent crimes, it could be rehabilitative or disassociation from the
community, or in extreme cases, execution.
There's no such thing as a victimless crime, that just doesn't exist in my world.
Individual achievement still motivates everybody, but it's not the pursuit of material possessions
that keeps people moving, it's the desire to see their community prosper.
In a world like this, there's no need to escape, so drug abuse of course plummets.
Coordination between communities, well that exists, and mainly done through teleconference
when the experts and whatever the field is that's being discussed sit down and work out a consensus
on what's best for everybody. Inter-community trade occurs much like it does today but without
the profit motivator inventions come to market a whole lot faster because there's no intellectual
property laws. Workers pray for the automation of their industry because their needs are met with or
without their job. And when their position becomes obsolete, well they innovate and drive
society even further into the future. As we embrace that equality of opportunity, bigotry
dies. People spend more time trying to master their own life or the world around them rather
than each other. Kings and queens are things that only exist on chessboards. Empires only
exist in video games but free travel exists everywhere because we as a world
and a society have begun to think much much broader than a border. I know sounds
pretty utopian, right? But it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}