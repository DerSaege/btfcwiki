# All videos from January, 2019
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2019-01-31: Let's talk about how to reach out to cops.... (<a href="https://youtube.com/watch?v=9b3vDdqY1PA">watch</a> || <a href="/videos/2019/01/31/Lets_talk_about_how_to_reach_out_to_cops">transcript &amp; editable summary</a>)

Beau explains the philosophy behind ACAB, suggests focusing on harm reduction and self-interest, and criticizes the failures of the drug war while challenging police perceptions of themselves.

</summary>

"If there's no harm, there's no crime. If there's no victim, there's no crime."
"The drug war and gun control are probably the two best ways to reach out to cops."
"Most of them see themselves as true patriots, true patriots."
"Not waging the drug war is more effective than waging it if you actually believe in harm reduction."
"So give it to them. give it to them."

### AI summary (High error rate! Edit errors on video page)

Explains the philosophy behind the ACAB slogan, which stands for "All cops are bad because they will enforce unjust laws" or because if good cops don't speak out against bad cops, they are complicit.
Acknowledges that slogans like ACAB serve a purpose in energizing like-minded individuals but also points out that they can turn off those who don't agree.
Suggests focusing on harm reduction and approaching the issue from a self-interest perspective to encourage engagement and critical thinking.
Points out that the drug war is a significant factor in the strained relationship between law enforcement and the public, leading to militarization and negative perceptions of police.
Uses statistics to illustrate the failures of the drug war and how countries like Portugal have seen positive outcomes by decriminalizing drugs.
Criticizes the image that many police officers hold of themselves, likening their actions in drug raids to Gestapo tactics and warning about the potential future perception of law enforcement.
Challenges the idea of police officers as patriots by questioning their enforcement of certain laws and suggesting that blind obedience to orders is detrimental.

Actions:

for activists, police reform advocates,
Reach out to police officers to have constructive dialogues about harm reduction and the failures of the drug war (implied).
Advocate for decriminalization and harm reduction policies in your community (implied).
Encourage critical thinking and engagement by approaching issues from a self-interest perspective (implied).
</details>
<details>
<summary>
2019-01-30: Let's talk about those cops in Houston getting shot.... (<a href="https://youtube.com/watch?v=E1oU6E6IKgs">watch</a> || <a href="/videos/2019/01/30/Lets_talk_about_those_cops_in_Houston_getting_shot">transcript &amp; editable summary</a>)

Four cops shot in Houston, but Beau focuses on police response, offering free training if union president resigns for stifling freedom of speech.

</summary>

"If you don't want to be cast as the enemy, don't be the enemy of freedom."
"You representing an armed branch of government do not get to tell people what they can and cannot talk about."
"Mr. Jamali, do you care about your officers enough to resign so they can get the training they need for free?"

### AI summary (High error rate! Edit errors on video page)

Four cops were shot during a box drug raid in Houston.
The reasons for the shooting are likely due to telegraphed movements or a lack of proper threat assessment.
The focus of the talk tonight is on the response to the shooting, not the shooting itself.
The police union president, Joe Jamali, issued a threatening statement in response to those critical of police officers.
Beau shares an interesting statement about training from a group in Texas.
This group is willing to train 15 officers for free if Joe Gimaldi resigns.
Beau stresses the importance of freedom of speech and critiquing the government without fear of reprisal.
He questions whether Joe Jamali cares enough about his officers to resign for their benefit.
The offer of free training for officers is contingent on the resignation of Joe Gimaldi.
Beau concludes with a call for Joe Jamali to resign for the sake of freedom and proper training for officers.

Actions:

for community members, advocates.,
Contact the group from Texas offering free training to see how to support their initiative (suggested).
Advocate for police accountability and freedom of speech in your community (implied).
</details>
<details>
<summary>
2019-01-29: Let's talk about Alexandria Ocasio-Cortez becoming the next Ron Paul.... (<a href="https://youtube.com/watch?v=w1uR6Xy3kks">watch</a> || <a href="/videos/2019/01/29/Lets_talk_about_Alexandria_Ocasio-Cortez_becoming_the_next_Ron_Paul">transcript &amp; editable summary</a>)

Beau contemplates the similarities and differences between Alexandria Ocasio-Cortez and Ron Paul, sparking a debate on establishment politics and the potential for change within the Democratic Party.

</summary>

"What if it's not corporate establishment versus government establishment? What if it's just establishment versus you?"
"With a little bit of refinement and a little bit of time, she'll grow just like everybody does."
"I think that we're looking at a powerhouse in the making."

### AI summary (High error rate! Edit errors on video page)

Made a controversial comparison between Alexandria Ocasio-Cortez and Ron Paul, causing a stir among his libertarian friends.
Discovered that 10-15% of his viewers are overseas, observing the U.S. like the fall of the Roman Empire.
Describes Ron Paul as a libertarian icon known for his strict adherence to the Constitution and opposition to government overreach.
Contrasts the views of Ron Paul and Alexandria Ocasio-Cortez, noting their differing perspectives on corporate greed versus government overreach.
Suggests that while AOC and Ron Paul seem very different, they may both have valid points in critiquing the establishment.
Points out the potential for AOC to grow in influence and impact, similar to how Ron Paul did over time.
Raises the question of whether there will be someone to carry on Ron Paul's legacy and if the Democratic Party establishment will allow it to happen.

Actions:

for political enthusiasts,
Analyze and challenge establishment politics (implied)
</details>
<details>
<summary>
2019-01-28: Let's talk about UNESCO, climate change, and arguing against your own interests.... (<a href="https://youtube.com/watch?v=xKFlY09pCNc">watch</a> || <a href="/videos/2019/01/28/Lets_talk_about_UNESCO_climate_change_and_arguing_against_your_own_interests">transcript &amp; editable summary</a>)

Beau questions the US withdrawal from UNESCO, challenges climate change skeptics, and urges individuals to reconsider their resistance to environmental initiatives.

</summary>

"You're arguing against your own interests."
"The propaganda is that thick."
"Blows my mind."
"It's insane."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Expresses shock and disappointment at the United States' withdrawal from UNESCO, an organization that registers and protects historical and cultural sites.
Questions the reasons behind the decision to withdraw, considering possible justifications like taxation, anti-Semitism, and climate change.
Outlines the significance of UNESCO's focus on climate change due to the impact of rising sea levels on historical sites near waterways.
Criticizes arguments against climate change, pointing out the influence of propaganda funded by oil companies and the importance of sustainable practices like stopping deforestation and using renewable energy.
Challenges individuals to think about why they resist efforts to create a cleaner and more sustainable world, pointing out the contradiction in arguing against their own interests.
Attributes political resistance to environmental initiatives to campaign contributors' financial interests, leading to a detrimental impact on global efforts for conservation and sustainability.

Actions:

for climate activists, sustainability advocates, concerned citizens.,
Join environmental organizations to support conservation efforts (exemplified)
Advocate for sustainable practices in your community (exemplified)
Educate others about the importance of renewable energy and cutting pollution (exemplified)
</details>
<details>
<summary>
2019-01-26: Let's talk about restoring democracy down south.... (<a href="https://youtube.com/watch?v=5L5QrZG2eeM">watch</a> || <a href="/videos/2019/01/26/Lets_talk_about_restoring_democracy_down_south">transcript &amp; editable summary</a>)

Beau stresses the dangers of U.S. involvement in Venezuela, warning of a potential civil war and urging resistance to save lives.

</summary>

"No U.S. involvement. No U.S. involvement in Venezuela."
"This isn't partisan politics. This will save lives. This will save lives."
"If you've got one of those raised fist profile pictures, now's the time."

### AI summary (High error rate! Edit errors on video page)

Talks about the need to restore democracy in the southern regions.
Mentions the long and complicated history of U.S. involvement in South American affairs.
Points out the significance of who the intelligence community brings in to run operations as an indicator of their plans.
Mentions past operations in Nicaragua, El Salvador, and Iran involving heavy hitters who turned a blind eye to atrocities.
Calls out Elliot Abrams, who was involved in covering up mass murders in the past.
Expresses concern over Abrams being tasked with operations in Venezuela, signaling trouble ahead.
Emphasizes that this is not a Trump operation but an intelligence community one.
Warns about the potential for a civil war in Venezuela and the grim outcomes it may bring.
Draws parallels with Syria's situation and questions the organic nature of protests against Assad.
Urges resistance against U.S. involvement in Venezuela to prevent further devastation and loss of lives.

Actions:

for activists, advocates, concerned citizens,
Resist U.S. involvement in Venezuela (suggested)
Spread awareness about the situation in Venezuela and the risks of intervention (implied)
</details>
<details>
<summary>
2019-01-25: Let's talk about Venezuela.... (<a href="https://youtube.com/watch?v=aJkDnb7SLUk">watch</a> || <a href="/videos/2019/01/25/Lets_talk_about_Venezuela">transcript &amp; editable summary</a>)

Beau questions the US motives behind the Venezuela coup, exposing economic interests and criticizing America's hypocrisy in foreign interventions.

</summary>

"Democracy has nothing to do with it."
"U.S. doesn't care about human rights."
"It's normally about money."
"There is no national emergency at the border. That's a lie."
"Maybe this isn't our job."

### AI summary (High error rate! Edit errors on video page)

Explains the US attempt to start a coup in Venezuela for regime change.
Questions the rationale behind preserving democracy by recognizing unelected leaders.
Debunks the idea of rigged elections due to low voter turnout in Venezuela.
Reveals CIA's documented plans for regime change in Venezuela pre-dating the 2018 election.
Challenges the justification of US intervention based on human rights violations.
Points out the inconsistency in US foreign policy regarding human rights concerns.
Suggests that economic interests, specifically oil reserves, are the likely driving force behind the coup.
Warns about the potential refugee crisis and destabilization that could result from a successful coup.
Criticizes the false narrative of a border crisis used by Trump to energize his base.
Questions the validity of collapsing Venezuela's economy to criticize socialism.
Calls for a reflection on America's hypocrisy in meddling in other countries while condemning foreign influence.

Actions:

for activists, policymakers, voters,
Protest US intervention in Venezuela (suggested)
Support organizations aiding refugees (exemplified)
Educate others on US foreign policy inconsistencies (implied)
</details>
<details>
<summary>
2019-01-24: Let's talk about honor killings.... (<a href="https://youtube.com/watch?v=J_PBiU3KIXY">watch</a> || <a href="/videos/2019/01/24/Lets_talk_about_honor_killings">transcript &amp; editable summary</a>)

Beau sheds light on honor killings, linking them to toxic masculinity and violence in modern society.

</summary>

"It's an honor killing."
"This is toxic masculinity."
"We need to call it what it is because this is toxic masculinity and that's the problem."

### AI summary (High error rate! Edit errors on video page)

Explains honor killings where a woman is killed for disgracing a man, often due to infidelity.
Shares the story of Tanya Lynn, a victim of an honor killing in Georgia.
Mentions the Supreme Court of Georgia granting the murderer a retrial over insufficient testimony about the victim's alleged infidelity.
Points out that the court believed more testimony on her infidelity might alter the verdict.
Emphasizes that the fact of the murder and body dumping were not in dispute.
Reveals alarming statistics on women killed by intimate partners in the U.S.
Connects honor killings to toxic masculinity and the view of women as property.
Criticizes the use of sanitized terms like "intimate partner violence" instead of calling it what it is: honor killings.
Condemns the idea that murdering a woman for infidelity is somehow justifiable.
Stresses the link between toxic masculinity and violence, especially in cases like honor killings.

Actions:

for advocates against gender-based violence.,
Advocate for proper terminology in addressing gender-based violence (suggested).
Support organizations working to combat toxic masculinity and violence against women (implied).
</details>
<details>
<summary>
2019-01-24: Let's talk about foreign languages, an army training film, and the MAGA teen.... (<a href="https://youtube.com/watch?v=DBHxYV9HbNg">watch</a> || <a href="/videos/2019/01/24/Lets_talk_about_foreign_languages_an_army_training_film_and_the_MAGA_teen">transcript &amp; editable summary</a>)

Beau underscores the importance of speaking the audience's "language," choosing the right messenger, and being wary of initial perceptions in communication to combat misinformation effectively.

</summary>

"Sometimes it may not be that you're constructing a poor argument. It may be that you're just not the person that can deliver it to the audience."
"The way something is presented is often times more important than the information presented in it."
"Messaging is important and the way things are perceived a lot of times can be more important than the facts."
"If we're fighting fake narratives, fake emergencies, we can't succumb to the same things."
"The reaction wasn't because of the message, but because of the language used when it was first circulated."

### AI summary (High error rate! Edit errors on video page)

Emphasizes the importance of speaking the same "language" as the audience to aid communication and understanding, especially for activists and academics.
Recounts an old army training film where a new lieutenant's attempts to communicate with his troops are hindered by a lack of understanding and respect for rank.
Describes the significance of a first sergeant in the army and how his authority can impact communication and decision-making.
Illustrates the importance of finding the right messenger to effectively deliver a message, even if the message itself is well-constructed.
Mentions the need for considering presentation and perception in communication, citing examples of viral incidents where initial perceptions shifted with more information.
Warns against jumping to conclusions based on initial viral content, pointing out the power of messaging and its ability to shape perception.

Actions:

for communicators, educators, activists,
Find the right messenger for your message (exemplified)
Tailor your communication to your audience's understanding (exemplified)
Be cautious of initial perceptions and viral content (exemplified)
</details>
<details>
<summary>
2019-01-24: Let's talk about Legos, Minecraft, and masculinity.... (<a href="https://youtube.com/watch?v=2hmbhdbkj0U">watch</a> || <a href="/videos/2019/01/24/Lets_talk_about_Legos_Minecraft_and_masculinity">transcript &amp; editable summary</a>)

A father mocks kids building robots, but creativity trumps mockery in masculinity.

</summary>

"Your kid's got a pretty valuable skill there already developed, and you're mocking it."
"You might want to get on Twitter and apologize to your son and all the other kids that got caught in a crossfire."

### AI summary (High error rate! Edit errors on video page)

Describes a man who live-tweeted mocking kids building LEGOs and robots at a competition.
Points out the man's behavior as mocking, with backlash on Twitter.
Shares a personal story of his own son building a temporary castle without plans or diagrams.
Acknowledges LEGO building as nerdy but praises the creativity and skill involved.
Notes the irony of the man mocking kids for their creative accomplishments.
Criticizes the man's actions as unmanly and suggests apologizing to his son and the other kids.

Actions:

for parents, educators, twitter users,
Apologize to your child and other kids on Twitter for mocking their creativity (suggested).
</details>
<details>
<summary>
2019-01-21: Let's talk about Trump, dreamers, and hostages.... (<a href="https://youtube.com/watch?v=SQeaeDzubbI">watch</a> || <a href="/videos/2019/01/21/Lets_talk_about_Trump_dreamers_and_hostages">transcript &amp; editable summary</a>)

President Trump's negotiation tactics with Dreamers and TPS recipients are likened to that of a criminal, lacking a clear path to citizenship and causing disruption.

</summary>

"The President of the United States is acting like a criminal, and he's using the guns of federal law enforcement as his soldiers in the street."
"We need to establish a path to citizenship for these people."
"It's extortion."
"The President knows they're not a detriment, the President knows they're not a threat, but that will not stop him from destroying their lives to get what he wants."
"Not disrupt the lives of federal employees, of people in need, of everybody."

### AI summary (High error rate! Edit errors on video page)

President Trump's negotiation tactics involve offering protection for Dreamers in exchange for $5.7 billion to fund the border wall.
Dreamers, or DACA recipients, are not a threat and are deeply ingrained in American culture.
Trump's offer of three years of protection to Dreamers and TPS recipients seems arbitrary and lacks a path to citizenship.
Trump's approach is compared to that of a thug or terrorist, using hostages to achieve his goals.
The President's actions are seen as criminal and embarrassing, especially when leveraging federal law enforcement.
Congress and the Senate are criticized for not calling out Trump's behavior.
Beau advocates for establishing a clear path to citizenship for Dreamers and TPS recipients.
The political tactics used by Trump are condemned as disruptive and detrimental to society.
Beau points out the flaws in Trump's approach and urges adherence to constitutional procedures.
The transcript concludes with a call for action and a reminder to stay safe.

Actions:

for advocates, activists, voters,
Advocate for establishing a clear path to citizenship for Dreamers and TPS recipients (implied)
Call out political figures who do not condemn detrimental actions (implied)
</details>
<details>
<summary>
2019-01-18: Let's talk about armed teachers.... (<a href="https://youtube.com/watch?v=1o1l2LQGyP8">watch</a> || <a href="/videos/2019/01/18/Lets_talk_about_armed_teachers">transcript &amp; editable summary</a>)

Beau addresses the impracticality and risks of arming teachers, stressing the intense training needed for close-quarters combat and the need for better solutions.

</summary>

"It's a Band-Aid on a bullet wound at best."
"You're talking about close quarters combat in incredibly, incredibly difficult scenarios."
"You alone as an individual, as a teacher, as an educator, you have to become the expert in combat."
"Liability and effectiveness don't go hand in hand. This is America today."
"This isn't going to solve the problem. It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Beau addresses the topic of armed teachers in schools, describing it as a Band-Aid solution at best for a serious issue.
Training teachers to use firearms effectively in active shooter situations is deemed impractical due to lack of resources and intense training requirements.
The dynamics of a teacher being in a live shooting scenario are discussed, focusing on the high stakes and intense pressure to protect students.
Suggestions are made for creating mobile cover using filing cabinets with steel plates and implementing frangible ammo to reduce risks to students.
Beau underlines the psychological impact on teachers and the unrealistic expectations placed on them in such scenarios.
He advises teachers to realistically gauge their ability to handle the responsibilities and risks associated with being armed in a school setting.
The transcript concludes with an emphasis on exploring alternative solutions and setting better examples rather than relying on arming teachers.

Actions:

for teachers, educators,
Build mobile cover using filing cabinets with steel plates (exemplified)
Implement frangible ammo to reduce risks to students (exemplified)
Realistically gauge your ability to handle the responsibilities and risks associated with being armed in a school setting (implied)
</details>
<details>
<summary>
2019-01-16: Let's talk about toxic masculinity and men's movements.... (<a href="https://youtube.com/watch?v=APoQuX5M2P4">watch</a> || <a href="/videos/2019/01/16/Lets_talk_about_toxic_masculinity_and_men_s_movements">transcript &amp; editable summary</a>)

Beau delves into toxic masculinity, the mythopoetic men's movement, and the glorification of violence, challenging perceptions of masculinity and the need for introspection.

</summary>

"Not saying hello can get a woman killed."
"Masculinity is not a team sport."
"Men are more violent. That's fact."

### AI summary (High error rate! Edit errors on video page)

Exploring toxic masculinity and its connection to men's movements.
Men feeling marginalized by feminism and the separation from their boys.
Emotional damage caused by constant accusations of sexism like the Me Too movement.
The mythopoetic men's movement in the 80s and 90s aimed to reclaim slipping masculinity.
Crediting the mythopoetic movement with reintroducing rites of passage and coining the term "toxic masculinity."
Toxic masculinity distinguishing hyper-masculine immature behavior from deep masculine qualities.
Concerns about men becoming hyper-masculine or feminized.
Glorification of violence leading to men wanting to use violence for everything.
Loss of ideals associated with masculinity and the glorification of violence.
Real masculinity involving introspection, knowing oneself, honor, and integrity.

Actions:

for men, activists, allies,
Join organizations promoting healthy masculinity (exemplified)
Educate others on the importance of introspection and knowing oneself (implied)
Challenge toxic masculinity in everyday interactions (exemplified)
</details>
<details>
<summary>
2019-01-16: Let's talk about that Gillette ad.... (<a href="https://youtube.com/watch?v=TinZ63kHgUE">watch</a> || <a href="/videos/2019/01/16/Lets_talk_about_that_Gillette_ad">transcript &amp; editable summary</a>)

Beau tackles misconceptions around masculinity and rites of passage, championing honor, integrity, and resilience in the face of discomfort and societal change.

</summary>

"My 12-year-old boy was more of a man than the people that are upset with this because he didn't identify with the bad guy."
"If you felt attacked by that ad, it says more about you than it does Gillette."
"They were creating warriors, real warriors."
"Our boys need to be tough like that."
"Any worthy goal is going to require you to suffer."

### AI summary (High error rate! Edit errors on video page)

Beau starts by addressing the internet people and delves into discussing razors, sparked by a Gillette ad his son brought up.
His son's reaction to the ad made him proud, as it showcased his understanding of the ad's message against sexual assault, belittling, bullying, and violence.
Beau asserts that the ad wasn't an attack on masculinity but a call for positive traits like honor and integrity.
He questions why some men didn't see themselves as the ones stepping forward in the ad's message.
Beau expresses surprise at feminists' discontent with a corporation leveraging feminism for sales, but he views the ad as promoting real masculinity more than feminism.
Addressing objections about a razor company entering social issues, Beau defends Gillette's role in sparking a necessary societal debate.
He contrasts modern rites of passage like shaving with historic brutal rituals that aimed to foster honor and integrity in men.
Beau draws parallels between ancient rites of passage, like the agoghi, and the values portrayed in the Gillette ad.
He describes a contemporary Amazonian rite where boys endure painful bullet ant bites to prove their manhood, contrasting it with American reactions to discomfort.
Beau concludes by praising his son's maturity in understanding the ad's message, juxtaposing it with the fragile masculinity displayed by those upset with it.

Actions:

for men, feminists, society,
Start a constructive community debate on modern masculinity (suggested)
Teach positive values like honor and integrity to young boys (implied)
Challenge toxic masculinity in personal interactions (implied)
</details>
<details>
<summary>
2019-01-14: Let's talk about foodstamps and gardening.... (<a href="https://youtube.com/watch?v=l4WTeesiFiw">watch</a> || <a href="/videos/2019/01/14/Lets_talk_about_foodstamps_and_gardening">transcript &amp; editable summary</a>)

Beau dispels myths about food stamps, encourages urban gardening using SNAP benefits, and advocates for community-building through growing food.

</summary>

"If you are doing well in this system, you might just want to eat this one."
"Every little bit helps in that regard."
"Once you're locked into that cycle of poverty, it's very hard to get out."
"It's a thought."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Stumbled upon the option to use food stamps for purchasing items like grapevines, fruit trees, and seeds, leading to a positive response from people on social media.
Addresses misconceptions about food stamps, with over 10% of Americans using them, and the average household receiving around $250 a month.
Debunks stereotypes around food stamp users, with about half being working families and the program not solely for "lazy" individuals.
Challenges common complaints about food stamp usage, like the misconception that recipients only buy junk food.
Points out that the prevalence of food stamps is due to a broken system and deflects blame onto marginalized groups rather than those responsible for the system's flaws.
Warns of potential unrest if food stamps were eliminated due to hunger-induced desperation.
Encourages growing food using food stamps, mentioning the ability to purchase items like seeds and fruit trees, especially beneficial for those living in cities with limited space.
Offers creative solutions for urban gardening, such as vertical gardening and utilizing unused spaces like shoe organizers or community centers.
Suggests collaborating with others to set up community gardens or growing food on others' properties with permission.
Emphasizes the long-term benefits of growing food, acknowledging the time investment before reaping the produce and the potential stability it provides for those facing poverty.
Acknowledges the challenges of climate and varying growing conditions, suggesting different options such as container gardening or raised beds based on one's location.
Advocates for using gardening not only to enhance food security and reduce dependence but also to strengthen community ties and potentially improve one's situation.

Actions:

for urban dwellers, snap recipients,
Utilize vertical gardening techniques to maximize space (exemplified).
Collaborate with a community center or church for a communal garden (suggested).
Seek permission to grow food on someone else's property (suggested).
Experiment with container gardening or raised beds based on your location (exemplified).
</details>
<details>
<summary>
2019-01-12: Let's talk about the wall gofundme's failure, math, and Israel's wall.... (<a href="https://youtube.com/watch?v=NdcseSQWdlA">watch</a> || <a href="/videos/2019/01/12/Lets_talk_about_the_wall_gofundme_s_failure_math_and_Israel_s_wall">transcript &amp; editable summary</a>)

Beau breaks down the flawed math and misconceptions behind the wall GoFundMe, revealing Americans' strong opposition to the wall and questioning its feasibility based on Israel's example.

</summary>

"Americans don't want the wall. They know that it's morally wrong, ethically wrong, legally wrong, economically wrong, and mathematically wrong."
"If just a third of the people in the United States wanted the wall, it could have raised more than a hundred million dollars, but it didn't."
"The reason the GoFundMe was an utter failure other than its complete lack of understanding of how the country works is the fact that Americans don't want the wall."
"I think most Americans shudder at the idea of a foreign head of state looking at our president in 20 years going, Mr. President, tear down this wall."
"Their wall is not a line that can be bypassed. Their wall surrounds their opposition."

### AI summary (High error rate! Edit errors on video page)

Explains the situation with the wall GoFundMe being refunded.
Questions the intention behind redirecting donations to a new non-profit.
Criticizes the misunderstanding that donations could dictate government actions.
Breaks down the math behind the raised funds, exposing the minimal percentage in relation to the wall cost.
Asserts that the lack of funding does not indicate widespread support for the wall.
Emphasizes Americans' opposition to the wall on moral, ethical, legal, and economic grounds.
Draws parallels between the proposed US wall and Israel's wall, focusing on differences in design and purpose.
Argues that Israel's wall, often cited as a success, has not effectively stopped terrorism.
Concludes by questioning the feasibility and effectiveness of a similar wall in the US.

Actions:

for american citizens,
Educate others on the inaccuracies surrounding the wall GoFundMe and the implications of supporting such initiatives (implied).
</details>
<details>
<summary>
2019-01-11: Let's talk about teaching techniques.... (<a href="https://youtube.com/watch?v=r5j6-JgegR8">watch</a> || <a href="/videos/2019/01/11/Lets_talk_about_teaching_techniques">transcript &amp; editable summary</a>)

Beau illustrates a story of transformation from anti-gun to AK-47 purchase, discussing effective teaching techniques and bridging ideological divides through information-based, non-confrontational discourse.

</summary>

"Ideas and information stand and fall on their own."
"Those who have seen the most say the least."
"Make it a conversation rather than a debate."
"Never assume anything about anybody."

### AI summary (High error rate! Edit errors on video page)

Illustrates the story of someone going from being anti-gun to purchasing an AK-47 after a synagogue shooting.
Describes a teaching technique used by a sixty-something year old man to effectively teach shooting skills.
Emphasizes the importance of good teaching techniques, allowing time to digest information, and focusing on the information itself.
Reveals that the instructor who taught shooting skills worked for the Israeli intelligence agency, Mossad.
Advocates for using similar techniques to bridge partisan and ideological divides, focusing on information and making it a non-confrontational academic experience.

Actions:

for educators, activists, communicators,
Reach out across partisan lines by engaging in constructive, fact-based dialogues (exemplified)
Avoid assumptions about others and approach interactions with an open mind (exemplified)
</details>
<details>
<summary>
2019-01-09: Let's talk about the thing you missed in Trump's speech.... (<a href="https://youtube.com/watch?v=uebiR0Avstg">watch</a> || <a href="/videos/2019/01/09/Lets_talk_about_the_thing_you_missed_in_Trump_s_speech">transcript &amp; editable summary</a>)

Beau challenges the importance of swearing an oath to defend the Constitution over the country itself, advocating for a society where government becomes unnecessary and individuals actively speak out against corruption and injustice.

</summary>

"When fascism comes to the United States, it'll be wrapped in a flag and carried across."
"Government, even in its best state, is but a necessary evil."
"Getting rid of government is not destroying the Constitution. It's carrying it to its logical conclusion."

### AI summary (High error rate! Edit errors on video page)

Beau introduces a different topic, avoiding the expected, and aims to bring original ideas or concepts to the table through his videos.
He criticizes Trump's speech where he claimed to swear an oath to protect the country, pointing out the importance of swearing to defend the Constitution instead.
Beau stresses that the Constitution's ideas must outlive the existence of the United States itself.
He praises the revolutionary aspects of the Constitution, particularly the idea of a government by the people and its adaptability through amendments.
Beau acknowledges the flawed nature of the Founding Fathers, like Thomas Jefferson, who despite owning slaves, recognized the moral depravity of slavery.
Emphasizes that the Constitution is a living document and expresses his idealistic view of a world where government becomes unnecessary.
Beau quotes Thomas Paine on government being a necessary evil and advocates for moving towards a society where government is no longer needed.
He calls on people to use their voices to speak against injustices and corruption in government, implying a need for activism and vigilance from citizens.

Actions:

for citizens, activists,
Speak out against injustices and corruption in government by using your voice (implied)
Advocate for change through activism and vigilance in upholding democratic values (implied)
</details>
<details>
<summary>
2019-01-08: Let's talk about that planned national emergency.... (<a href="https://youtube.com/watch?v=-tUWLfIiseI">watch</a> || <a href="/videos/2019/01/08/Lets_talk_about_that_planned_national_emergency">transcript &amp; editable summary</a>)

Beau outlines the dangers of paralleling Hitler's actions with Trump's national emergency declaration, urging a choice between supporting the U.S. Constitution or backing authoritarian measures.

</summary>

"Either you support the constitution of the United States or you betray it."
"You're a patriot or a traitor."
"Circumventing Congress, it's a betrayal."
"There is no way to support both anymore."
"The choice is simple, you can support the constitution of the United States or you can support Mango Mussolini."

### AI summary (High error rate! Edit errors on video page)

Border apprehensions along the southern border are at a 40-year low.
The Constitution of the United States outlines a process for funding projects.
Trump's rise to power and his promises of prosperity and military supremacy are discussed.
A comparison is drawn between Hitler's actions and Trump declaring a national emergency.
Fascism is defined by 14 specific elements, including corporativismo and disdain for human rights.
The importance of checks and balances in the U.S. Constitution is emphasized.
Congress serves as a vital checkpoint against tyranny and dictatorship.
The potential consequences of Trump's national emergency declaration are explored.
A choice is presented between supporting the Constitution or backing Trump's actions.
The audience is urged to choose between being a patriot or a traitor in relation to current political events.

Actions:

for americans,
Support the U.S. Constitution by upholding democratic values and principles (implied).
Take a stand against authoritarian actions by advocating for checks and balances within the government (implied).
Choose to be informed and engaged in defending constitutional rights in the face of potential abuses of power (implied).
</details>
<details>
<summary>
2019-01-07: Let's talk about how to change the world  (Part 3).... (<a href="https://youtube.com/watch?v=BVjSHtSVeLY">watch</a> || <a href="/videos/2019/01/07/Lets_talk_about_how_to_change_the_world_Part_3">transcript &amp; editable summary</a>)

Beau explains how increasing income and leveraging networks are key to financial independence and making a difference.

</summary>

"Increasing the amount of money you have coming in is the solution to financial independence."
"Sometimes the skills of the entire network complement each other."
"You want to break away from that system, you don't have to be employed by somebody else."
"A lot of these huge companies that everybody knows were started in somebody's garage."
"This series will continue, but we got to take a break, because there's a whole bunch of stuff in the news we got to talk about."

### AI summary (High error rate! Edit errors on video page)

Part three of how to change the world focuses on money and the resources needed to make a difference.
Individuals of limited means are most aware of the failings of the system but may lack the resources to act.
Often, advice given to those with limited means revolves around cutting out creature comforts to save money.
Saving small amounts like $50 a month may not significantly change one's financial situation in the long run.
Increasing the amount of money coming in is key to financial independence.
Leveraging networks and recommendations can lead to opportunities for additional income.
Building a reputation within your community can open doors for new income streams.
Starting low or no-cost businesses can be a way to increase income over time.
Networks can complement each other, leading to collective success and financial gains.
Windfalls like tax returns can be invested in new income-generating ventures.

Actions:

for community members seeking financial independence.,
Support local businesses and individuals in your community by recommending their services (exemplified).
Invest in someone's venture within your network to help them get started (exemplified).
</details>
<details>
<summary>
2019-01-05: Let's talk about #AOC.... (<a href="https://youtube.com/watch?v=yKPClC6O4Lo">watch</a> || <a href="/videos/2019/01/05/Lets_talk_about_AOC">transcript &amp; editable summary</a>)

Beau asks about The Breakfast Club, appreciates youthful expression, and humorously comments on the portrayal of the First Lady, ending with a light-hearted farewell.

</summary>

"Congratulations to the Republican Party that has spent so much time and energy casting this woman as this evil foreign socialist who comes from money."
"With one video, she has been turned into the most adorable woman in the country."
"Y'all have a nice night."

### AI summary (High error rate! Edit errors on video page)

Beau introduces himself and mentions it is part three.
Beau calls out to his dad and asks about The Breakfast Club movie.
Beau mentions watching a video of a congressperson dancing online.
Beau appreciates how people express themselves when they were younger.
Beau and his dad talk about people evolving as they get older.
Beau jokingly asks about information on the First Lady when she was younger.
Beau congratulates the Republican Party for portraying the First Lady positively in a video.
Beau implies that people are only angry because of the First Lady's appearance and energy.
Beau concludes with a light-hearted farewell message.
The transcript captures a casual and humorous interaction between Beau and his dad.

Actions:

for internet users,
Appreciate people's expressions when they were younger (implied)
Share positive videos to counter negative portrayals (implied)
</details>
<details>
<summary>
2019-01-04: Let's talk about how to change the world (Part 2).... (<a href="https://youtube.com/watch?v=qlHhJ6XCjFw">watch</a> || <a href="/videos/2019/01/04/Lets_talk_about_how_to_change_the_world_Part_2">transcript &amp; editable summary</a>)

Beau explains building community networks for independence, inspired by NATO's Stay Behind Organizations, urging self-evaluation and diverse contributions to form resilient networks.

</summary>

"Anything is possible if you wish hard enough, at least that's what Peter Pan said."
"Everybody has something to offer a network like this."
"Rule 303, if you have the means at hands you have the responsibility."
"Those people look for the helpers, you know."
"Normal people answering questions in the comment section, that's fantastic."

### AI summary (High error rate! Edit errors on video page)

Talks about the importance of building community-based networks to achieve independence.
Mentions NATO's Stay Behind Organizations during the Cold War.
Explains the idea of forming networks before crises to effectively carry out operations.
Describes the diverse roles within these networks, ranging from military to civilian.
Emphasizes the logistics involved in running resistance operations.
Mentions the Gladiou network as an example that went off track but still proved the concept's effectiveness.
Encourages self-evaluation to determine what skills or expertise one can offer to start a network.
Gives examples of how people of different ages can contribute to these networks.
Acknowledges concerns from introverts and suggests ways for them to participate.
Talks about recruiting members for the network and different methods to do so.
Suggests starting with immediate circles and expanding through social media and local groups.
Shares ideas on structuring the organization, including barter systems and communal commitments.
Encourages community service as a way to attract like-minded individuals to the network.

Actions:

for community members,
Start by evaluating what skills or expertise you can offer to begin building a community network (suggested).
Reach out to immediate circles like friends, co-workers, or local groups to recruit members for the network (exemplified).
Engage in community service activities to attract like-minded individuals to join the network (implied).
</details>
<details>
<summary>
2019-01-02: Let's talk about how to change the world.... (<a href="https://youtube.com/watch?v=9TeuLkOiXVc">watch</a> || <a href="/videos/2019/01/02/Lets_talk_about_how_to_change_the_world">transcript &amp; editable summary</a>)

Beau breaks down the cycle of societal control, urging conscious rebellion and community-building for lasting change and independence.

</summary>

"Being independent doesn't actually mean doing everything by yourself. It means having the freedom to choose how it gets done."
"If you make your community strong enough, it doesn't matter who gets elected."
"You're changing the world you live in by building your community."

### AI summary (High error rate! Edit errors on video page)

Beau starts with the introduction to a series on changing the world, comparing it to a previous video about guns, and underlines the importance of the information he is about to share.
The inspiration for this video comes from a genuine comment on Facebook expressing the need for information to help others, leading to a reflection on breaking the cycle of societal control mechanisms.
Drawing parallels to George Orwell's "1984," Beau explains the need to become conscious to rebel against societal constraints and restrictions.
He shares his personal experience of needing help and the importance of building a network to achieve independence and assist others.
Beau acknowledges the challenge of feeling isolated and encourages finding like-minded people to form a supportive network.
Addressing single parents, Beau stresses that everyone has something valuable to offer in building a community network, regardless of their circumstances.
He expresses the idea of force multiplication through community networks and the power of collective strength in creating resilient communities independent of political influences.
Beau rejects the idea of running for office and advocates for bottom-up community building over top-down leadership as a more effective way to bring about positive change.
Recognizing the diversity of situations, Beau hints at discussing tailored approaches for different socio-economic backgrounds in the upcoming videos.
Ending with a reference to Orwell's "1984" and a message of hope in community strength, Beau signs off with a thought-provoking note.

Actions:

for community members,
Build a supportive network by reaching out to like-minded individuals (implied).
Offer your skills and resources to contribute to community building (implied).
Encourage single parents to involve their children in community activities to network effectively (implied).
</details>
<details>
<summary>
2019-01-01: Let's talk about that cop getting killed.... (<a href="https://youtube.com/watch?v=ZMULqDaRCwM">watch</a> || <a href="/videos/2019/01/01/Lets_talk_about_that_cop_getting_killed">transcript &amp; editable summary</a>)

Beau addresses the focus on officers' immigration status over their heroism, challenges the narrative on immigrant crime, criticizes bias in studies, raises issues of race, and urges against using graves for political points.

</summary>

"Stop standing on the graves, the corpses, the flag-covered coffins of people far more honorable than you to make a political point they might not agree with."
"You don't know the names of those cops, more than likely."
"People far more honorable than you."

### AI summary (High error rate! Edit errors on video page)

Mentions the names of cops killed in the last 60 days by gunfire, some heroes like Sergeant Helles and Corporal Singh.
Talks about how headlines focus on an officer's immigration status rather than their heroic actions.
Compares the narrative around immigrant crime to the Molly Tibbets case and the flawed logic of holding entire demographics accountable for one person's actions.
Criticizes the inconsistency in applying laws and concerns about bias in studies from organizations like FAIR and CIS.
Raises the issue of race, pointing out the bias against brown people and the lack of outrage for officers like Officer Smith who was recently killed.
Draws parallels between the lack of attention to certain military incidents based on political narratives and agendas.
Urges people to stop using the graves of honorable individuals for political points.

Actions:

for activists, community members,
Challenge biased narratives (implied)
Support community policing efforts (implied)
</details>
