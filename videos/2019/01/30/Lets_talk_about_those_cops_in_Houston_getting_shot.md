---
title: Let's talk about those cops in Houston getting shot....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=E1oU6E6IKgs) |
| Published | 2019/01/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Four cops were shot during a box drug raid in Houston.
- The reasons for the shooting are likely due to telegraphed movements or a lack of proper threat assessment.
- The focus of the talk tonight is on the response to the shooting, not the shooting itself.
- The police union president, Joe Jamali, issued a threatening statement in response to those critical of police officers.
- Beau shares an interesting statement about training from a group in Texas.
- This group is willing to train 15 officers for free if Joe Gimaldi resigns.
- Beau stresses the importance of freedom of speech and critiquing the government without fear of reprisal.
- He questions whether Joe Jamali cares enough about his officers to resign for their benefit.
- The offer of free training for officers is contingent on the resignation of Joe Gimaldi.
- Beau concludes with a call for Joe Jamali to resign for the sake of freedom and proper training for officers.

### Quotes

- "If you don't want to be cast as the enemy, don't be the enemy of freedom."
- "You representing an armed branch of government do not get to tell people what they can and cannot talk about."
- "Mr. Jamali, do you care about your officers enough to resign so they can get the training they need for free?"

### Oneliner

Four cops shot in Houston, but Beau focuses on police response, offering free training if union president resigns for stifling freedom of speech.

### Audience

Community members, advocates.

### On-the-ground actions from transcript

- Contact the group from Texas offering free training to see how to support their initiative (suggested).
- Advocate for police accountability and freedom of speech in your community (implied).

### Whats missing in summary

The emotional impact and detailed analysis can be best experienced by watching the full transcript. 

### Tags

#HoustonShooting #PoliceAccountability #FreedomOfSpeech #CommunityAction #TrainingInitiative


## Transcript
Well, howdy there, internet people, it's Beau again.
So tonight, we're gonna talk about that shooting out there in Houston.
Four cops got shot.
Looks, during a box drug raid,
looks like the reason they got shot is gonna be the reason it always is.
Either they telegraphed their movements or
they didn't perform a proper threat assessment, or they did and
they ignored it, whatever.
As much as we talk about those things, we're not gonna talk about it tonight
cuz something else happened.
After the shooting, the police union president, a guy named Joe Jamali, came out and he made
a statement, and I'm going to read that statement, so I want to make sure I get it right.
If you're the ones that are out there spreading the rhetoric that police officers are the
enemy, just know we've all got your number now.
We're going to be keeping track of all of y'all, and we're going to make sure that we
We hold you accountable every time you stir a pot on our police officers."
Wow.
That is something else.
A very interesting statement.
But back to the training aspect of it.
There happens to be a group out in Texas that I know.
Most of them live there.
Now, to get them to come out and train your department for three days, you'll run 30 grand.
However, I talked to them tonight, and they'll come out and train 15 officers of your choosing for free,
but they have a condition, and that condition is that Joe Gimaldi resign.
See, people who are actually out there trying to protect their community,
they understand that freedom is the bedrock of safety.
that the freedom of speech and the ability to criticize your government or stir the pot
against your government is pretty important. If you don't want to be cast as the enemy,
don't be the enemy of freedom. You representing an armed branch of government do not get to tell
people what they can and cannot talk about. We have a whole part of the constitution about this.
So, there you go. Now the question is, Mr. Jamali, do you care about your officers enough
to resign so they can get the training they need for free? Or not? See, I think I've got
your number. Anyway, it's just a thought. Y'all have a good night. Hey, dinner's ready!

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}