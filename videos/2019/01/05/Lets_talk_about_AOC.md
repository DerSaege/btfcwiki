---
title: 'Let''s talk about #AOC....'
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=yKPClC6O4Lo) |
| Published | 2019/01/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces himself and mentions it is part three.
- Beau calls out to his dad and asks about The Breakfast Club movie.
- Beau mentions watching a video of a congressperson dancing online.
- Beau appreciates how people express themselves when they were younger.
- Beau and his dad talk about people evolving as they get older.
- Beau jokingly asks about information on the First Lady when she was younger.
- Beau congratulates the Republican Party for portraying the First Lady positively in a video.
- Beau implies that people are only angry because of the First Lady's appearance and energy.
- Beau concludes with a light-hearted farewell message.
- The transcript captures a casual and humorous interaction between Beau and his dad.

### Quotes

- "Congratulations to the Republican Party that has spent so much time and energy casting this woman as this evil foreign socialist who comes from money."
- "With one video, she has been turned into the most adorable woman in the country."
- "Y'all have a nice night."

### Oneliner

Beau asks about The Breakfast Club, appreciates youthful expression, and humorously comments on the portrayal of the First Lady, ending with a light-hearted farewell.

### Audience

Internet users

### On-the-ground actions from transcript

- Appreciate people's expressions when they were younger (implied)
- Share positive videos to counter negative portrayals (implied)

### Whats missing in summary

The full transcript provides a light-hearted and humorous take on current events and family interactions.


## Transcript
Well howdy there internet people, it's Bo again.
Today is part three.
Hey dad, you busy?
A little bit, why, what's up?
What's The Breakfast Club?
It's a movie from the 80s, how do you not know this?
How did your mother not make you watch this?
I've seen the video.
The video, the video online
of the congressperson dancing.
Yeah, no, no, no, no, I've already seen the video.
It's just, I like how people express themselves
when they were younger.
As opposed to when they're older and all stuffy, yeah.
Well, people change and as they get older, they evolve.
I like to think I was pretty cool when I was younger.
Do you have anything on the First Lady when she was younger?
Nice try. Get out.
Congratulations
to the Republican Party that has spent so much time and energy casting this
woman as this evil foreign socialist who comes from money.
With one video, she has been turned into the most adorable woman in the country.
Good job on that.
And I would like to point out, let's be honest, everybody knows the only reason you guys are angry is because
she looks the way she does and she still has enough energy to dance because she's not dying of dehydration.
Anyway, just a thought, y'all have a nice night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}