---
title: Let's talk about teaching techniques....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=r5j6-JgegR8) |
| Published | 2019/01/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Illustrates the story of someone going from being anti-gun to purchasing an AK-47 after a synagogue shooting.
- Describes a teaching technique used by a sixty-something year old man to effectively teach shooting skills.
- Emphasizes the importance of good teaching techniques, allowing time to digest information, and focusing on the information itself.
- Reveals that the instructor who taught shooting skills worked for the Israeli intelligence agency, Mossad.
- Advocates for using similar techniques to bridge partisan and ideological divides, focusing on information and making it a non-confrontational academic experience.

### Quotes

- "Ideas and information stand and fall on their own."
- "Those who have seen the most say the least."
- "Make it a conversation rather than a debate."
- "Never assume anything about anybody."

### Oneliner

Beau illustrates a story of transformation from anti-gun to AK-47 purchase, discussing effective teaching techniques and bridging ideological divides through information-based, non-confrontational discourse.

### Audience

Educators, activists, communicators

### On-the-ground actions from transcript

- Reach out across partisan lines by engaging in constructive, fact-based dialogues (exemplified)
- Avoid assumptions about others and approach interactions with an open mind (exemplified)

### Whats missing in summary

The impact of effective teaching techniques and information-based communication in fostering understanding and bridging divides.

### Tags

#Teaching #Communication #CommunityPolicing #BridgeDivides #InformationExchange


## Transcript
Well, howdy there, internet people, it's Beau again.
So tonight we're gonna talk about how to share information.
And I've got a story that we can use to illustrate that.
It can be used to illustrate a lot of things,
but we're gonna use it for this.
And it's cool because you guys know how it started.
Some of you probably know that after that synagogue shooting,
I had a bunch of people who were anti-gun ask me about purchasing a firearm and how
to do it.
And I made a very long video explaining the realities of owning a firearm, that even though
it's self-defense, you are purchasing to kill someone.
I have been checking in on these people.
And one young lady, she took my advice to the letter.
She went to her synagogue and she found the Never Again guy.
And that never again guy, he had an arsenal, and he took her out and let her shoot.
And she found weapons that she liked, ones that were right for her.
She bought a.45 caliber pistol and an AK.
I know that's funny.
Going from being anti-gun to purchasing an AK is humorous in and of itself.
But at the same time, for those that don't know, despite the weapon's reputation, it
is a good rifle.
especially true for females who are of a certain stature. Everything can be
operated with the right hand. You don't have to cross in front of yourself to do
anything. So if you're larger up top, it's a good rifle for you. So from there, she
goes shooting and she goes shooting with a couple of army guys, a Marine, and an
She said she didn't learn much, didn't get much out of it, didn't learn much about shooting anyway, learned a whole lot
about them and the stuff that they had done, but not much about shooting.
She said she learned the most from a sixty-something year old man that she went shooting.
And she thought it was funny because he had never done anything like that.
anything like that. He worked at some institute. So the first day he takes her out and they
don't even put bullets in the gun. They take them apart, they learn how they work, he explains
how they work, explains how the bullets work, explains the physics of it, explains how to
get a good sight picture, explains what the difference between cover and concealment is.
All of that stuff, no bullets in the gun.
Second day, she learns different stances, learns how to change magazines in different
positions, learns how to walk and pull the trigger, learns everything, but no bullets
in the gun.
Third day, they finally start shooting, and they spend six hours at the range, and if
you don't shoot, that's a while.
That's a while at the range, but by the end of it, she said she can successfully put that
magazine into a 3x5 card. That's impressive. That is impressive.
So why did he succeed where others have failed?
He used good teaching techniques.
He used good teaching techniques, plain and simple.
He let her crawl before she walked, before she eventually sprinted.
That was one thing and he gave each phase of that information time to digest.
The other thing was that he made it about the information. Ideas and
information stand and fall on their own. Working at an institute, you certainly
learn this. Doesn't matter who's presenting the information, it's either
true or it's not. He made it about the information. He made it an academic
experience rather than a confrontational one. And then the most important thing I
I think he did is that he didn't make it about him.
I mean, how could he, right?
He just worked at an institute.
And here's the twist to that story.
If you raised your eyebrows when I said institute,
yep, that one.
So, and I did confirm this, and the funny part about this
is that she gets to find this out at the same time y'all do,
watching this video.
The institute is slang. That is slang.
That is like saying you work for the company
if you are working for the CIA.
The Institute is the Masad, it is Israeli intelligence.
The guy who she is certain has never really done anything like that, has done and seen
more than everybody else she went shooting with, all added up.
But he didn't bring it up, he didn't make it about him, because ideas and information
stand and fall on their own.
And he presented it in an academic manner, and it goes back to that old saying, those
who have seen the most say the least.
And so there's a good overview of a nice teaching technique.
How does that apply to what prompted this video and the questions of how to reach out
across partisan lines, across ideological lines?
Use the same techniques.
If you want to talk about immigration, talk about immigration.
Talk about Trump, talk about the statistics, talk about the laws that are currently on
the books, talk about the laws that should be on the books, talk about the laws that
we are violating ourselves, talk about this stuff, and then let it digest.
Then come back and talk about what do you think the effects of this are?
If you were on the other side of this proposed wall, how would you respond to each one of
these developments and then go in and then you can close out and explain you
know a wall would in fact you know drive more people into the hands of
traffickers a wall would not really do anything because of the statistics you
learned last week make it a conversation rather than a debate you know provide
information in a non confrontational manner and then obviously the other
thing to take away from this is never never assume anything about anybody
because you could you could definitely be wrong anyway it's just a thought
y'all y'all y'all have a good night

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}