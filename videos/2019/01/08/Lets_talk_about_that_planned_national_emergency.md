---
title: Let's talk about that planned national emergency....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-tUWLfIiseI) |
| Published | 2019/01/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Border apprehensions along the southern border are at a 40-year low.
- The Constitution of the United States outlines a process for funding projects.
- Trump's rise to power and his promises of prosperity and military supremacy are discussed.
- A comparison is drawn between Hitler's actions and Trump declaring a national emergency.
- Fascism is defined by 14 specific elements, including corporativismo and disdain for human rights.
- The importance of checks and balances in the U.S. Constitution is emphasized.
- Congress serves as a vital checkpoint against tyranny and dictatorship.
- The potential consequences of Trump's national emergency declaration are explored.
- A choice is presented between supporting the Constitution or backing Trump's actions.
- The audience is urged to choose between being a patriot or a traitor in relation to current political events.

### Quotes

- "Either you support the constitution of the United States or you betray it."
- "You're a patriot or a traitor."
- "Circumventing Congress, it's a betrayal."
- "There is no way to support both anymore."
- "The choice is simple, you can support the constitution of the United States or you can support Mango Mussolini."

### Oneliner

Beau outlines the dangers of paralleling Hitler's actions with Trump's national emergency declaration, urging a choice between supporting the U.S. Constitution or backing authoritarian measures.

### Audience

Americans

### On-the-ground actions from transcript

- Support the U.S. Constitution by upholding democratic values and principles (implied).
- Take a stand against authoritarian actions by advocating for checks and balances within the government (implied).
- Choose to be informed and engaged in defending constitutional rights in the face of potential abuses of power (implied).

### Whats missing in summary

The full transcript provides detailed historical context and a thorough analysis of the current political climate that can deepen understanding beyond this summary. It also offers insight into the importance of civic engagement in safeguarding democracy.

### Tags

#Constitution #ChecksAndBalances #Fascism #Authoritarianism #PoliticalAnalysis


## Transcript
Well, howdy there, internet people.
It's Bo again.
So tonight we are going to talk about that planned national
emergency over the border wall and how that's going to play
out, if it does.
But before we get into that, I want to, we have to establish
two things that are really important to this
conversation, so we need to get them out there up front.
First, border apprehensions, which are the metric that
Border Patrol uses to determine the flow along the southern border are at a 40-year low.
Forty-year low, and have been for a number of years.
The other thing we need to establish is the Constitution of the United States lays out
a process for funding projects.
It's Article 1, Section 9, Clause 7.
Okay, so now that those things are out there, we need to recap a little bit of history.
So he ran with promises of bringing back prosperity and making the country great again.
He talked about supremacy of our military and how he's going to build it up.
He said that we weren't taken seriously on the international stage anymore and that he
fix that. And he got elected. Got elected. The international community in the
beginning, they laughed at him. He was a laughingstock because he's ridiculous.
Then it didn't take him long to realize that, well, his unpredictable nature made
him somebody they had to watch. And it didn't take him long to realize that the
The country wasn't set up the way he wanted, he didn't have the power he needed to make
all the changes that he wanted and to fulfill the promises that he had made.
So when the Reichstag burned, he declared a national emergency.
Yeah, I'm not talking about Trump, I'm talking about Hitler.
I know it's hard to tell the difference sometimes.
You know, people said that comparison has played out.
It's not.
There's a reason almost every expert on fascism makes the comparison.
It's because Trump's a fascist.
Fascism doesn't mean, oh, he says mean things and he has violent rhetoric.
That's not what fascism is.
Fascism has 14 pretty specific elements, the first of which is what Mussolini and his crew
called corporativismo.
That's the blending of government and economic power.
The second is an overriding disdain for human rights.
Water boredom, go after their families.
There's a war with the media and they want to control the media.
Fake news, an obsession with crime and punishment.
We're talking about the guy who took out a full page ad demanding the execution of
people who later turned out to be innocent.
overriding theme of nationalism. The guy said he was a nationalist. Advocacy for
a supreme military. Space force. We're gonna build the greatest, the greatest,
the biggest. A paranoid obsession with national security. Sound familiar?
Scapegoating about groups, sexism, religious propaganda that, you know,
scapegoats the minority religions in the country, the suppression of labor, a
disdain for intellectuals and the arts.
I think he's trying to sue Saturday Night Live right now.
Uh, cronyism, believe in epitism would probably fall under that, uh,
corruption, and fraudulent elections.
That's fascism.
Even if you're a Trump supporter, you have to admit.
Yeah, that's pretty accurate.
The Constitution of the United States does not allow for the President of the United
States to stomp his feet, throw a temper tantrum, and declare a national emergency because Congress
didn't do what he wanted.
That's the whole point of Congress.
It's a check.
Checks and balances.
Come on, third grade civics here, that's their whole job.
That is their whole job, is to make sure that we don't descend into tyranny, don't descend
into a dictatorship.
They're supposed to put the brakes on stupid projects.
That's their job.
Article 1 Section 9 Clause 7 lays out how projects are funded.
This little national emergency theater, I think it may play out, I think it may play
out if Trump, contrary to his character, sticks to his word.
It probably will because nobody in the Democratic party is stupid enough to vote for a compromise
that includes a wall.
Be the end of their careers in politics.
The American people don't want it.
So if he follows through with his threat, we'll see it.
And when we see it, you have a choice.
And it's the same choice that Germans faced after they backed Hitler in the beginning,
when they realized what was going on.
They didn't want to face it.
They didn't want to admit they'd been duped.
They'd been tricked.
So they continue to play along.
You can do the same thing.
The choice is simple, you can support the constitution of the United States or you can
support Mango Mussolini.
There is no way to support both anymore.
Either you support the constitution of the United States or you betray it.
You're a patriot or a traitor.
It's your choice.
Doesn't matter if you support what he wants to do.
Circumventing Congress, it's a betrayal.
of a trail of the very ideas this country was founded on.
When that time comes, I hope that you choose to be an August Landmesser, or Sophie Scholl,
rather than a good German.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}