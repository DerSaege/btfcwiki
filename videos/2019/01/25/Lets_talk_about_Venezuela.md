---
title: Let's talk about Venezuela....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=aJkDnb7SLUk) |
| Published | 2019/01/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Explains the US attempt to start a coup in Venezuela for regime change.
- Questions the rationale behind preserving democracy by recognizing unelected leaders.
- Debunks the idea of rigged elections due to low voter turnout in Venezuela.
- Reveals CIA's documented plans for regime change in Venezuela pre-dating the 2018 election.
- Challenges the justification of US intervention based on human rights violations.
- Points out the inconsistency in US foreign policy regarding human rights concerns.
- Suggests that economic interests, specifically oil reserves, are the likely driving force behind the coup.
- Warns about the potential refugee crisis and destabilization that could result from a successful coup.
- Criticizes the false narrative of a border crisis used by Trump to energize his base.
- Questions the validity of collapsing Venezuela's economy to criticize socialism.
- Calls for a reflection on America's hypocrisy in meddling in other countries while condemning foreign influence.

### Quotes

- "Democracy has nothing to do with it."
- "U.S. doesn't care about human rights."
- "It's normally about money."
- "There is no national emergency at the border. That's a lie."
- "Maybe this isn't our job."

### Oneliner

Beau questions the US motives behind the Venezuela coup, exposing economic interests and criticizing America's hypocrisy in foreign interventions.

### Audience

Activists, policymakers, voters

### On-the-ground actions from transcript

- Protest US intervention in Venezuela (suggested)
- Support organizations aiding refugees (exemplified)
- Educate others on US foreign policy inconsistencies (implied)

### Whats missing in summary

The full transcript provides a comprehensive analysis of the underlying reasons for US involvement in Venezuela and calls for reflection on American foreign policy.

### Tags

#Venezuela #USForeignPolicy #Coup #HumanRights #Hypocrisy


## Transcript
Well, howdy there, internet people, it's Bo again.
So I guess we need to talk about Venezuela.
Okay.
So what's happening?
We're trying to start a coup.
That that's what's happening.
We're trying to Institute regime change.
And there are a bunch of talking points about why we should do this.
And I think we need to talk about them.
Okay.
So the first is we have to preserve democracy in Venezuela by recognizing
people that weren't elected as the legitimate government that that doesn't
make a whole lot of sense and then when you bring that up they're like well it
was a rigged election was it though was it really rigged because it doesn't seem
like it see the opposition party called for a boycott of the election voter
turnout was really low it appears that their followers did what they asked they
boycotted the election, therefore they lost. That's kind of how that works. Now
as far as it being rigged, one of the signs of a rigged election is high voter
turnout, not low. See when you stuff the ballot boxes, it appears more people
voted than did, so if you have a record low voter turnout, odds are it wasn't
rigged, odds are is that the opposition party boycotted the election, but that
really doesn't matter because that election was in 2018 and the plans for
The regime change in Venezuela predate that.
In July of 2017, the CIA director is on record talking about the attempts to trigger a transition,
I think was the word he used.
So we can get rid of that right off the bat.
Democracy has nothing to do with it.
The other talking point is we need to go down there because of human rights violations.
Their police are shooting people in the street and that actually does look like that is happening
to some degree, to more than is acceptable, certainly.
But does that really warrant a U.S. response?
Because if it does, I'm wondering when we're going to stop propping up Duarte in the Philippines.
We're going to stop backing Saudi Arabia.
U.S. doesn't care about human rights.
It never has.
That is not something that influences U.S. foreign policy.
That's borne out by the fact that if it's a dictator that is in our pocket, they can
do whatever they want.
The only time human rights gets brought up as far as foreign policy is when it's time
to convince you that it's time to go invade some country or trigger regime change.
So that's not it either, not the real reason.
You know what could be the real reason?
Exxon and Chevron getting thrown out?
That might have something to do with it.
It's normally about money.
without money, and it doesn't matter who gets hurt in the pursuit of that money.
And Venezuela is a very, very, very large supplier of oil.
They have huge reserves, huge known reserves.
Now what happens if this little coup is successful?
Same thing that always happens when one of our little coups is successful.
A bunch of refugees are created.
they going to go? That's right. Some of them will settle along the way, but a lot
of them will end up at our southern border, which is funny because right now
we're at a 40-year low for border apprehensions, which is the metric used to
determine illegal border crossings. 40-year low. There is no national
emergency at the border. That's a lie. Trump is lying. Border Patrol's own
numbers will tell you that. He's making it up. He's using it as a scare tactic to
energize his base. Period. But there may actually be a crisis if this coup goes
off, especially if it turns violent. And it might. Maduro does have a significant
amount of support, even with everything that's going on. Now, I'm sure somebody is
going to want to debate the merits of socialism versus capitalism, and that is
an argument to have. But, prior to getting into it, I would ask yourself, A, is the
fact that a nation operates under a socialist system called enough to wage
an economic war against it and collapse their economy. To quote, make their
economy scream. And if it is, when the economy does scream and things turn bad,
does that really reflect on socialism? The other question that you'd have to
ask is a free market really what's in competition with Venezuela's socialism
right now or is it the type of crony capitalism that gets Saudi Arabia to
flood the market with oil and therefore destabilize the one real
export that Venezuela has. Anyway, it just might be better for the U.S. to sit
this one out. Maybe this isn't our job. It certainly showcases America's
hypocrisy while we sit here at home and whine about possible foreign influence in our elections.
And we openly try to trigger a coup in another country.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}