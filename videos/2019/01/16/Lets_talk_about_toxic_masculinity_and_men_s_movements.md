---
title: Let's talk about toxic masculinity and men's movements....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=APoQuX5M2P4) |
| Published | 2019/01/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring toxic masculinity and its connection to men's movements.
- Men feeling marginalized by feminism and the separation from their boys.
- Emotional damage caused by constant accusations of sexism like the Me Too movement.
- The mythopoetic men's movement in the 80s and 90s aimed to reclaim slipping masculinity.
- Crediting the mythopoetic movement with reintroducing rites of passage and coining the term "toxic masculinity."
- Toxic masculinity distinguishing hyper-masculine immature behavior from deep masculine qualities.
- Concerns about men becoming hyper-masculine or feminized.
- Glorification of violence leading to men wanting to use violence for everything.
- Loss of ideals associated with masculinity and the glorification of violence.
- Real masculinity involving introspection, knowing oneself, honor, and integrity.

### Quotes

- "Not saying hello can get a woman killed."
- "Masculinity is not a team sport."
- "Men are more violent. That's fact."

### Oneliner

Beau delves into toxic masculinity, the mythopoetic men's movement, and the glorification of violence, challenging perceptions of masculinity and the need for introspection.

### Audience

Men, activists, allies

### On-the-ground actions from transcript

- Join organizations promoting healthy masculinity (exemplified)
- Educate others on the importance of introspection and knowing oneself (implied)
- Challenge toxic masculinity in everyday interactions (exemplified)

### Whats missing in summary

The full transcript provides a comprehensive exploration of toxic masculinity, the impact of men's movements, and the glorification of violence that shapes perceptions of masculinity.

### Tags

#ToxicMasculinity #Men #GenderRoles #Violence #Activism


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight, we're going to talk about toxic masculinity
a little bit.
But before we get into that, I want
to talk about some men's movements.
We have to talk about toxic masculinity,
because I mentioned in that last video,
in the comments section, especially on YouTube,
went crazy.
Apparently, that is an offensive term.
I didn't know that.
But we need to talk about some men's movements first,
because there's an overlap there
that I think people need to understand
and it's really important.
So if you're a member of a men's movement,
what I want you to do,
or if you were offended by that commercial,
I want you to just nod your head
as I'm listing off these ideas if you agree with them
and keep track of how many of them you agreed with, okay?
The first is that feminism
has kind of turned down the volume on men.
We're just not, we're not as important anymore and we're getting blamed for everything, okay?
And that the separation of men from their boys, you know, a bunch of men, or boys being
raised by women, has led to them being feminized and emasculated, alright, that just too much
exposure to femininity in general has led to men not being men anymore.
And the other one is that things like the Me Too movement, just constant accusations
of sexism, has kind of caused emotional damage to men to the point where we don't even know
what we can say anymore without getting in trouble.
Those are four main ideas.
I'm willing to bet that a lot of those that were upset
agreed with them.
If you did, and you were alive in the 80s or 90s,
you might have belonged to something called the mythopoetic
men's movement.
Now these were men that felt masculinity was slipping away.
And they needed to come up with a way to get it back.
So, they're credited with a lot of things, you know.
They tried to bring back some rites of passage
for young boys, but for the most part,
they just wound up hanging out in the woods,
beating their chest and telling stories to each other,
which is cool, and a little strange, but cool.
The other thing they're credited with
is coming up with the term toxic masculinity.
Yeah, that didn't come from feminism, guys.
Came from the first MRAs.
That's where the term originated.
Toxic is not an adjective.
It's a modifier.
It was a way for them to distinguish between
what they referred to as
hyper-masculine immature behavior
and the deep masculine,
the stuff that mattered.
It didn't come from feminism.
The problem is that today, that hyper-masculinity has gone so far, is that people don't even
realize, a lot of men don't even realize, that it's bad.
They don't get it.
So the term is now offensive, because people don't know what it means, you know, anyway.
So these guys, they were afraid that if left unchecked, one of two things would happen.
Men would either become hyper-masculine, toxic masculine, or they would become feminized
and we would lose that half, we would lose masculinity, interesting.
And they believe that that toxic masculinity was the extremes.
The extremes, you know, the need to dominate everything.
And see people right now are going, well, no, masculinity means assertiveness and aggressiveness
with cause, with a reason.
That reason's been lost.
Now, at this point, we're talking about what I think, not their views, but it certainly
seems like men have wanted to hold on to the violence and to that dominating spirit, but
they lost all the ideals that went with it.
They wanted to hang on to it because it's glorified.
It had to be glorified.
The system had to glorify it, otherwise stupid men wouldn't run off to go to war.
Nobody wants to go die.
But if it's glorified, then you've got a reason to do it.
And that glorification of violence has led to men wanting to use violence for everything.
And the loss of the ideals is perfectly encapsulated in one of the comments, well actually the
same comment over and over again from a bunch of different people, but basically the comment
reads something similar to, well why is it my responsibility to act?
Because you have the means at hand.
Rule 303, go watch the video.
Because you have the means at hand, you have the responsibility to act.
And if you were masculine, real masculinity, you'd know that.
That ideal would still be ingrained in you.
it's not there. It's not there because rather than men becoming hyper-masculine
or feminized, both happened. Both happened. All of these men want to cry and want
somebody else to take care of them while they pretend that they're this
aggressive, dominant, self-reliant person. Why is it your responsibility to act?
Because you're there. That's why. And the funniest part about the comment section,
I meant to write down his name, but he had a Qbert for his profile picture on
YouTube. He said the best part about this is that if these men were really
Masculine the ones that were upset that they wouldn't care and it's true. That's true
Masculinity is not a team sport
That's one of the things that the mythopoetics have right requires a lot of introspection
Requires knowing yourself
And then the other comment the other question was well
How is how are these things honor and integrity? How is this different from, you know, the female?
Characteristics can't females be this yes, absolutely
Absolutely, they can embody all of those central tenants
But they tend to exercise it in a different way
Men are more aggressive and
and right now there's so many, well, you know,
women can be assertive.
Assertive is not aggressive.
Men are more violent.
That's, that's fact.
Statistically proven, it's fact.
Men are more violent.
Um, and that,
that violence is the reason that toxic masculinity
has to be put in check.
There was a, somebody who said,
well, he was just walking after her to say hello,
reference that commercial when it was talking about the cat calling. Put
yourself in a woman's point of view. You're walking along, some guy calls out
to you, you don't want to respond, and you keep walking and he starts following
you. What's going through your head right now? That need to dominate physically,
That extreme, that toxic masculinity, that's what's going through your head.
Because you're worried that this person following you might act on it.
Not saying hello can get a woman killed.
So no, it's not harmless, it isn't harmless behavior.
And yes, you do have the responsibility to act.
Anyway, it's just a thought or two.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}