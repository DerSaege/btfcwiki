---
title: Let's talk about that Gillette ad....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=TinZ63kHgUE) |
| Published | 2019/01/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau starts by addressing the internet people and delves into discussing razors, sparked by a Gillette ad his son brought up.
- His son's reaction to the ad made him proud, as it showcased his understanding of the ad's message against sexual assault, belittling, bullying, and violence.
- Beau asserts that the ad wasn't an attack on masculinity but a call for positive traits like honor and integrity.
- He questions why some men didn't see themselves as the ones stepping forward in the ad's message.
- Beau expresses surprise at feminists' discontent with a corporation leveraging feminism for sales, but he views the ad as promoting real masculinity more than feminism.
- Addressing objections about a razor company entering social issues, Beau defends Gillette's role in sparking a necessary societal debate.
- He contrasts modern rites of passage like shaving with historic brutal rituals that aimed to foster honor and integrity in men.
- Beau draws parallels between ancient rites of passage, like the agoghi, and the values portrayed in the Gillette ad.
- He describes a contemporary Amazonian rite where boys endure painful bullet ant bites to prove their manhood, contrasting it with American reactions to discomfort.
- Beau concludes by praising his son's maturity in understanding the ad's message, juxtaposing it with the fragile masculinity displayed by those upset with it.

### Quotes

- "My 12-year-old boy was more of a man than the people that are upset with this because he didn't identify with the bad guy."
- "If you felt attacked by that ad, it says more about you than it does Gillette."
- "They were creating warriors, real warriors."
- "Our boys need to be tough like that."
- "Any worthy goal is going to require you to suffer."

### Oneliner

Beau tackles misconceptions around masculinity and rites of passage, championing honor, integrity, and resilience in the face of discomfort and societal change.

### Audience

Men, feminists, society

### On-the-ground actions from transcript

- Start a constructive community debate on modern masculinity (suggested)
- Teach positive values like honor and integrity to young boys (implied)
- Challenge toxic masculinity in personal interactions (implied)

### Whats missing in summary

The full transcript provides deeper insights into societal expectations of masculinity and the necessity for positive values in rites of passage.

### Tags

#Masculinity #RitesOfPassage #GilletteAd #AmazonianCulture #HonorAndIntegrity


## Transcript
Well, howdy there, internet people.
It's Bo again.
So I guess today we're going to talk about razors.
Now, I am obviously not a Gillette customer.
In fact, the only reason I know about the ad is because my
son came home and asked me about it.
He couldn't understand why men were upset with it.
And man, that made me proud.
OK.
So here's the thing.
The message is in that ad.
What are they?
Don't sexually assault.
Don't belittle.
Don't bully.
Violence isn't always the answer.
These are concepts that men don't have a problem with.
This was not an attack on masculinity.
My 12-year-old boy understood that because he didn't identify with the bad guys in the
video. If you felt attacked by that ad, it says more about you than it does Gillette.
My question is, why didn't you see yourself as the guy stepping forward? Then there's
a lot of feminists who were upset with it, which surprised me, but they felt that a multi-billion
dollar corporation should not leverage feminism for sales. That's true, that's true, but
But when I look at that ad, I don't see feminism.
I see masculinity, real masculinity, that's what I see.
The idea of masculinity, it has a lot of components to it.
Honor and integrity are central.
Integrity could be practically defined as doing the right thing even when nobody's
looking.
no matter who's looking. That's what I saw in the video. I saw it praising world masculinity
and mocking toxic masculinity. That's what I saw. I didn't see feminism.
Then there were a lot of people who were upset because a razor company shouldn't insert themselves
into social issues like this. Oh no razor company is the perfect company to insert themselves into
to this conversation, we don't have rights of passage in the United States from boy
to man.
We don't.
Not on a nationwide level.
There's some ethnic groups that still carry on their cultures.
But nationwide, we don't have that.
But what suffices for it?
Shaving.
They're the perfect company to do this.
And on the topic of rites of passage, I saw people bringing that up.
You know, the soft masculinity, you know, the old rites of passage from the olden days,
they were brutal and violent.
They were incredibly brutal, incredibly violent.
But was it violence for violence sake?
Was it violence to subjugate the women?
No.
The agoghi was one that I saw brought up.
And yeah, brutal, violent.
Those are the right terms for that insane, insane amount of violence, why?
Because they were creating warriors, real warriors.
They were trying to instill honor and integrity.
They were creating men who would one day cling to the motto of with your shield or upon it.
You're either going to come back with your shield held high because you won or you're
going to come back laying on it because you're dead.
There's no surrender.
There's no loss.
There's no retreat.
winter you die. Why? Who were they protecting? Their community, right? They
were standing up for those around them? Sounds a lot like that ad. There's a
culture in the Amazon and they still do this today. The boys will go out and
collect bullet ants and if you don't know a bullet ant will tear you up. It
It will tear you up.
I'm not sure if it's a bite or a sting, to be honest, but it doesn't matter.
It's incredibly painful.
And these boys, becoming men, take these ants and they're placed in woven gloves and they
put their hands in them.
They sit there and they endure that pain.
And that's their rite of passage.
That's how they become a man in their community.
And American men look at this and say, see, our boys need to be tough like that.
The lesson being taught is that any worthy goal is going to require you to suffer.
It's going to require you to endure.
And I think that is perfect commentary on American masculinity.
In the Amazon, you've got these young boys becoming men who endure some of the most painful
bites known to man for an extended period of time.
And here in the U.S., men can't sit through a two-minute video without losing their minds
and going to Twitter to cry about it.
My 12-year-old boy was more of a man than the people that are upset with this because
he didn't identify with the bad guy. If you feel attacked by this, it says a whole lot
about you. A whole lot. Anyways, just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}