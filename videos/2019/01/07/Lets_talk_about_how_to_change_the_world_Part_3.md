---
title: Let's talk about how to change the world  (Part 3)....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=BVjSHtSVeLY) |
| Published | 2019/01/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Part three of how to change the world focuses on money and the resources needed to make a difference.
- Individuals of limited means are most aware of the failings of the system but may lack the resources to act.
- Often, advice given to those with limited means revolves around cutting out creature comforts to save money.
- Saving small amounts like $50 a month may not significantly change one's financial situation in the long run.
- Increasing the amount of money coming in is key to financial independence.
- Leveraging networks and recommendations can lead to opportunities for additional income.
- Building a reputation within your community can open doors for new income streams.
- Starting low or no-cost businesses can be a way to increase income over time.
- Networks can complement each other, leading to collective success and financial gains.
- Windfalls like tax returns can be invested in new income-generating ventures.

### Quotes

- "Increasing the amount of money you have coming in is the solution to financial independence."
- "Sometimes the skills of the entire network complement each other."
- "You want to break away from that system, you don't have to be employed by somebody else."
- "A lot of these huge companies that everybody knows were started in somebody's garage."
- "This series will continue, but we got to take a break, because there's a whole bunch of stuff in the news we got to talk about."

### Oneliner

Beau explains how increasing income and leveraging networks are key to financial independence and making a difference.

### Audience

Community members seeking financial independence.

### On-the-ground actions from transcript

- Support local businesses and individuals in your community by recommending their services (exemplified).
- Invest in someone's venture within your network to help them get started (exemplified).

### Whats missing in summary

The importance of leveraging community networks and recommendations for financial growth.

### Tags

#FinancialIndependence #CommunitySupport #IncomeGeneration #Networking #SmallBusinesses


## Transcript
Well, howdy there, internet people.
It's Beau again.
And today really is part three of how to change the world.
And today we're going to talk about money.
I know.
Not the topic most people want to talk about.
But if you're at war with the system, you have to act like it.
And wars cost money.
It's not for bullets and bombs, but you need resources to do
just about anything.
And who are the people that are most aware
of the failings of the system?
Those of limited means.
Those that don't have the resources to do much.
And you're kind of trained to be like that.
You really are.
In school, learn this.
You can get hired.
You'll be employable.
Working for somebody else may not be the best answer,
especially if you're looking for independence.
Now, I know a lot of people, you're in a situation
where you have to work for somebody else.
And those people of limited means,
what is the advice we give them?
Most times, oh, well, cut out creature comforts.
You know, if you didn't go to Starbucks,
you could save that $5.
So let's say you're on minimum wage.
You're working a job and you're getting minimum wage,
and you manage to sack away $50 a month,
at the end of the year you got $600.
Now that's assuming
that you didn't get a flat tire and a dead battery in the same month.
$600.
That $600 is not going to change your station in life.
Now I'm definitely not saying
don't save.
You definitely need to
for when that inevitable bad thing does happen.
That's not the solution.
The solution is increasing the amount of money you have coming in.
Sounds easy, right?
That's easy to say.
What happens when somebody outside of one of your networks needs something that you
know how to do and somebody inside your network hears about it?
Who do they recommend?
You.
Sometimes that person could be somebody who could end up being brought into the network
if they're of a like mind.
Most times they won't be and that can help, especially that recommendation because that
recommendation carries a lot of weight.
People that are in these networks, they're active in their community and if it's about
somebody else who's active in their community, well, they're not recommending a company.
recommending a person. Think back to when I brought up Bear Paw, that first aid
kit company. It was the fact that these people were out there helping, doing
hurricane relief, doing search and rescue, helping train people, helping their
community that made them so intriguing.
You know, yeah, their kits are a little bit cheaper.
And yeah, they're good kits.
But that wasn't really the selling point.
And I know that that recommendation mattered because I got a message from the guy that
runs it.
They ran out of supplies.
Y'all cleaned them out.
It works.
know if it's if it's a recommendation that isn't just some nameless company
it's a person and this person is a good person that's who people go with and
those recommendations can turn into little micro businesses little home
businesses that can help you and you know they can help you increase the
amount of money you have coming in and that's the goal that's what you've got
to do to become financially independent you know a lot of the other stuff we've
been talking about it's breaking away from the system but nobody wants to talk
about this aspect of it and you know even if you don't have a skill that is
marketable you know you're not a handyman or whatever there are a whole
bunch of low startup or no startup businesses that can be done with all
almost nothing.
There are entire websites dedicated to just listing the different types of businesses
that can be started for like under $500.
That is a way to increase the amount of money you have coming in.
And it helps in the long run.
And never forget that sometimes the skills of the entire network complement each other.
twist of fate I know a group of firefighters who everybody in the station happened to know something
about repairing houses. One was an electrician, one was a plumber, one was a carpenter, one was a
painter. So of course these guys are there together all day anyway. So they started remodeling and
flipping houses and they make a lot of money doing it. In some years they make more money doing that
than they do as a firefighter. And never forget about windfalls, you know, your tax return.
You get that wad of cash in your hand. Yeah, you can go get a creature of comfort with
it, you know, get a new PlayStation, whatever. I'm not going to fault you for that. Everybody
deserves to have some nice things in their life. I don't look down on people that do
do that. At the same time, you could take that money and put it into one of those low
startup or no startup businesses. And that could be the beginning of having more money
coming in. And let's say you just don't have the time to do that. You can always invest
in somebody else in the network. Let's say you've got a guy that wants to start a landscaping
company, but he needs a trailer. You can get him that trailer. Say, okay, well I'll get you the
trailer, but 10%. Let's say that first year, that 10% is $100 a month. It's already double
you would make, sacking away the $5 from Starbucks.
If you want to talk about independence, this is part of it. You want to break away from that system,
them, you don't have to be employed by somebody else.
Now a lot of times people are in a situation where they've
got to be for a while, but a lot of these huge companies
that everybody knows were started in somebody's garage.
In fact, the fifth column, that was started with $100
and an old laptop.
And now it's, I don't know how many websites they own.
Anyway, it's just a thought.
This series will continue, but we got to take a break,
because there's a whole bunch of stuff in the news
we got to talk about.
So y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}