---
title: Let's talk about how to reach out to cops....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=9b3vDdqY1PA) |
| Published | 2019/01/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the philosophy behind the ACAB slogan, which stands for "All cops are bad because they will enforce unjust laws" or because if good cops don't speak out against bad cops, they are complicit.
- Acknowledges that slogans like ACAB serve a purpose in energizing like-minded individuals but also points out that they can turn off those who don't agree.
- Suggests focusing on harm reduction and approaching the issue from a self-interest perspective to encourage engagement and critical thinking.
- Points out that the drug war is a significant factor in the strained relationship between law enforcement and the public, leading to militarization and negative perceptions of police.
- Uses statistics to illustrate the failures of the drug war and how countries like Portugal have seen positive outcomes by decriminalizing drugs.
- Criticizes the image that many police officers hold of themselves, likening their actions in drug raids to Gestapo tactics and warning about the potential future perception of law enforcement.
- Challenges the idea of police officers as patriots by questioning their enforcement of certain laws and suggesting that blind obedience to orders is detrimental.

### Quotes

- "If there's no harm, there's no crime. If there's no victim, there's no crime."
- "The drug war and gun control are probably the two best ways to reach out to cops."
- "Most of them see themselves as true patriots, true patriots."
- "Not waging the drug war is more effective than waging it if you actually believe in harm reduction."
- "So give it to them. give it to them."

### Oneliner

Beau explains the philosophy behind ACAB, suggests focusing on harm reduction and self-interest, and criticizes the failures of the drug war while challenging police perceptions of themselves.

### Audience

Activists, Police Reform Advocates

### On-the-ground actions from transcript

- Reach out to police officers to have constructive dialogues about harm reduction and the failures of the drug war (implied).
- Advocate for decriminalization and harm reduction policies in your community (implied).
- Encourage critical thinking and engagement by approaching issues from a self-interest perspective (implied).

### Whats missing in summary

Exploration of the potential impact of changing police perceptions and approaches on community-police relations.

### Tags

#PoliceReform #ACAB #HarmReduction #DrugWar #CommunityPolicing


## Transcript
Well, howdy there, internet people.
It's Bo again.
So tonight, we're going to talk about talking about cops.
In that last video, there were a whole lot of comments
in the comments section.
And they weren't all nice about cops.
And that's fine.
That's fine.
They're slogans.
For those that don't know, ACAB, that's
All cops are people whose parents weren't married.
And that can also be signified as 1-3-1-2, but it's a slogan.
Now, the philosophy behind that slogan
is that all cops are bad because either A,
of their own free will, chose a profession in which they
agree to enforce unjust laws.
Therefore, all cops are bad because they will enforce unjust laws.
All right.
The other reasoning behind it is that if, yeah, maybe there are some good cops out there,
but if they don't say anything about the bad cops, they're complicit.
Therefore, they're bad.
We don't have many coming forward.
We know there's a bunch of bad cops.
Therefore, they're all bad.
Philosophically, ideologically, it's very hard
to make an argument against those two trains of thought.
I mean, they're pretty solid.
But just because they're solid doesn't
mean they're the arguments you should be using.
Now, I'm not saying don't use slogans.
Slogans serve a purpose.
They energize the base.
They do.
Those people who are already in tune with that idea when
they see that, they know they see an ally, you know.
So it makes sense to throw them up every once in a while.
However, those slogans work both ways.
MAGA.
Now, if you're a racist or somebody who doesn't realize
that almost everybody around you is a racist, you're like,
Yeah, build the wall, build the wall.
All right.
If you're not, what was your reaction?
Yeah, that slogan energizes the base.
But what was your reaction if you're not part of that?
Automatically on the defensive.
Automatically.
I don't have to listen to anything this idiot says, right?
And maybe that's a sound theory.
But that works with all slogans.
So when somebody sees that, they automatically
discount everything you say.
Now, I know a lot of you are not actually reaching out to cops
because on an ideological level, they're all bad.
But there's the way things should be and the way they are
and the way they are the cops are around.
So maybe we should focus on harm reduction, at least
a little bit.
And when you're doing that, those slogans
don't come in handy at all.
They really don't.
They present many problems because they turn the listener
off.
They're not going to listen to you.
Most people need a little self-interest
to really get interested in something,
to really kind of start to question anything they hold dear.
They need some kind of self-interest.
So give it to them.
give it to them.
The drug war is without a doubt the focal point of our problems with law enforcement
in this country.
The drug war has led to police militarization.
It has led to a lot of things that go very, very badly that have changed cops from peace
officers into what a majority of people see in the United States as the enemy.
Right now somebody's like, the majority don't see that.
We'll get to the stats in a minute.
But let's just start off with, so the drug war, most cops killed in the line of duty
are killed because of the drug war.
That's fact.
objective fact. So, officer, how do you feel about the fact that your friends are dying
so some politician can make money in his private prison? All of a sudden there's that self-interest.
They may feel they're being duped. Now, the fact is the drug war is an utter failure.
utter failure on every level. In the last month, 9.4% of the population of the US has
used illegal drugs. It's one out of ten people right off the bat. See you as the enemy. See
a cop as the enemy. Because you are. You are. 40% of high school seniors have used drugs.
It's half the population almost.
52% of Americans have.
The majority of Americans see you as the enemy because, well, you're out to arrest them.
And most of these people aren't doing anything wrong.
They're not hurting anybody.
So you've got death, you've got assault, you have all this risk in your job because of
this drug war.
But you have to do it right, you're protecting people.
Well, what happens in countries that decriminalize?
Liberalize completely.
Portugal is a perfect example because they decriminalized
everything, not just pot, everything, heroin, whatever.
They get anything under a 10-day supply.
What happened in that country?
That started in 2001.
Prevalence of drug use, it's down.
Continuing drug use, down.
Death from drug use, down.
HIV infection among intravenous drug users,
down.
Literally everything.
Everything is down.
So not just our officers dying for some politician
to make money in his private prison or his urinalysis
testing company. They're dying over something that's in vain. Not waging the drug war is
more effective than waging it if you actually believe in harm reduction. It's crazy. It's
crazy. Now the other thing you talk about is the image because cops in general love
their image. They love it because they're mostly in an echo chamber. Most cops hang
out with other cops. Why? Because half the population views them as the enemy. So they
hang out with other cops and they see themselves as the thin blue line, the protectors. And
that's what people, you know, that's what they believe other people see. They don't
realize that a majority of the population sees their drug raids as very Gestapo-esque.
don't realize that at some point the drug war will end and as cool as they
think they look right now they're gonna look like those idiots busting up
barrels of whiskey. That image may be more important than anything. And then
maybe another one is something that strikes to the heart of cop culture. Most
of them see themselves as true patriots, true patriots, and you see it all the
time when you see that thin blue line sticker next to the 3% logo, well I
won't enforce an unjust drug gun law, really, really. You won't confiscate
guns when the time comes? How many guns have you taken off people? Is there a gun law out
of the hundreds on the bucks that you won't enforce? Looks like you sold your principles
for that paycheck. And because you're caught in that echo chamber, that's how it happens.
how people end up just following orders.
The drug war and gun control are probably the two best ways to reach out to cops and
explain that what they are now isn't what they think they are, isn't what they pride
themselves on being.
I'm lucky, I live out in an area where most cops around here still go by the spirit of
the law, not the letter of it.
So I don't deal with a lot of this stuff in my everyday life.
Cops around here, cops in this area in general, know when to kind of, well, ask the Carter
boys, you know, and just kind of let it go. If there's no harm, there's no crime. If
there's no victim, there's no crime. And we still get that down here. There's a
lot of places that that doesn't exist. So if you're trying to reach out, maybe the
FTP and ACAB not the way to go. Now if you're just looking to energize the
bass, more power to you.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}