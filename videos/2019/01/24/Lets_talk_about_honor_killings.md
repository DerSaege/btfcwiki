---
title: Let's talk about honor killings....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=J_PBiU3KIXY) |
| Published | 2019/01/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains honor killings where a woman is killed for disgracing a man, often due to infidelity.
- Shares the story of Tanya Lynn, a victim of an honor killing in Georgia.
- Mentions the Supreme Court of Georgia granting the murderer a retrial over insufficient testimony about the victim's alleged infidelity.
- Points out that the court believed more testimony on her infidelity might alter the verdict.
- Emphasizes that the fact of the murder and body dumping were not in dispute.
- Reveals alarming statistics on women killed by intimate partners in the U.S.
- Connects honor killings to toxic masculinity and the view of women as property.
- Criticizes the use of sanitized terms like "intimate partner violence" instead of calling it what it is: honor killings.
- Condemns the idea that murdering a woman for infidelity is somehow justifiable.
- Stresses the link between toxic masculinity and violence, especially in cases like honor killings.

### Quotes

- "It's an honor killing."
- "This is toxic masculinity."
- "We need to call it what it is because this is toxic masculinity and that's the problem."

### Oneliner

Beau sheds light on honor killings, linking them to toxic masculinity and violence in modern society.

### Audience

Advocates against gender-based violence.

### On-the-ground actions from transcript

- Advocate for proper terminology in addressing gender-based violence (suggested).
- Support organizations working to combat toxic masculinity and violence against women (implied).

### Whats missing in summary

Full context and emotional depth.

### Tags

#HonorKillings #ToxicMasculinity #GenderViolence #Awareness #CommunitySafety


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're gonna talk about honor killings.
You don't know what one of these is.
It's when a woman does something
that somehow disgraces the man, typically infidelity.
So he takes a club, beats her to death,
and then throws her body in a well.
And right now, everybody's thinking of Afghanistan
or some other place that ends in a stand.
That story is about Tanya Lynn.
Happened in Georgia.
And the horrifying thing about that case is that the Supreme Court of Georgia gave him a retrial,
gave the murderer a retrial.
on the grounds that the trial judge did not allow enough testimony about her alleged infidelity.
Now the murderer himself talked about it in his testimony,
but other people weren't allowed to come in and talk about it. What does that say? What does that
really say? It says that the Supreme Court of Georgia believed that that might alter the verdict.
Make no mistake, the fact that he killed her was not in dispute.
The fact that he dumped her body was not in dispute.
Just that it might have been okay, might have been less of a crime because at some point in
In the past, she had slept with someone else.
If that's not an honor killing, I don't know what is.
We don't call it that though.
We call it intimate partner violence, I think is the current term.
And you may not think that this is a big problem in the US, but it is.
of all women who are killed in the U.S. are killed by a current or former
intimate partner. Those numbers, they're in the thousands. I think 1600 to 2400
were the was the range each year. That's pretty significant. And then the stats
for murder-suicide. Don't remember them exactly but I want to say it's 80 to 90
percent. Honor killings. And it all stems from the same idea that the property
disobeyed the owner. That's what it is. That is what it is. That's where it comes
from.
This is toxic masculinity.
A woman cheats, engages in infidelity, has an affair.
The response is not to leave, it's not divorce, it's not counseling, it's murder.
And state supreme courts think that, well, maybe that would alter the outcome of the
jury.
That information, more of that information.
We're not talking about somebody who flies into a jealous rage catching their wife in
bed with another man.
We're talking about something that happens afterwards.
It's an honor killing.
Thinking that maybe using a sanitized terminology for this isn't the right thing to do.
We need to call it what it is because this is toxic masculinity and that's the problem.
That's the problem.
that that fragility, that insecurity in yourself, is violent. That's why this is
such an issue. That's why this hyper masculinity, this false masculinity, is
such a worry in modern society because it breeds violence. It breeds murder.
Anyway, that's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}