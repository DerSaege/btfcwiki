---
title: Let's talk about Legos, Minecraft, and masculinity....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2hmbhdbkj0U) |
| Published | 2019/01/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Describes a man who live-tweeted mocking kids building LEGOs and robots at a competition.
- Points out the man's behavior as mocking, with backlash on Twitter.
- Shares a personal story of his own son building a temporary castle without plans or diagrams.
- Acknowledges LEGO building as nerdy but praises the creativity and skill involved.
- Notes the irony of the man mocking kids for their creative accomplishments.
- Criticizes the man's actions as unmanly and suggests apologizing to his son and the other kids.

### Quotes

- "Your kid's got a pretty valuable skill there already developed, and you're mocking it."
- "You might want to get on Twitter and apologize to your son and all the other kids that got caught in a crossfire."

### Oneliner

A father mocks kids building robots, but creativity trumps mockery in masculinity.

### Audience

Parents, educators, Twitter users

### On-the-ground actions from transcript

- Apologize to your child and other kids on Twitter for mocking their creativity (suggested).

### Whats missing in summary

The full transcript provides a deeper understanding of the importance of valuing creativity in children and the impact of mocking such skills on their development and self-esteem.

### Tags

#Parenting #Creativity #Masculinity #Mockery #Apology


## Transcript
Well, howdy there internet people it's Bo again, so today is gonna be a little bit different
The internet is mad at this guy
he was at a Lego building competition or something like that with his son and
He got on Twitter and started live tweeting
and
basically, you know, he's like, you know some kids play football and I'm stuck here watching this and
just constantly tweet after tweet mocked him and
And, uh, yeah, I mean, he definitely got some, uh, backlash on Twitter, but, uh, I get it.
I understand.
I mean, LEGOs, that's kind of nerdy, you know, especially get it, because my son's
just a couple years older than him.
This castle behind us, you know, that's just something for his little brothers to play
in.
And he, he, he built that.
It's temporary.
It's going to be torn down.
But he built it to code anyway, no plans, no diagrams.
Made all the measurements himself, laid out the walls, everything.
I did the cutting and I lifted the hardy board because cement fiber is heavy.
But he did that.
He did that.
I imagine that if this guy's son had done that, he would have been proud.
That's manly.
That is masculine.
something out of nothing just yeah sheer will bringing something into existence that is cool
you know where my son got interested in building stuff i wish i could say legos that would really
drive the point home but it wasn't it was a minecraft i'll uh let you guys argue over what is
perceived as nerdier at the end of the day dude your kid built a robot a working
robot. That is awesome. You should be proud. But in the interest of looking manly to your
followers you got on Twitter and made fun of him. And all those other kids that were
there too. All those other kids. These kids are building robots. I'm pretty sure they
know how a computer works. They're probably going to see that. And I know what you're
saying it's just a joke. It's not. It wasn't a joke when you were probably doing it to
the kids in your class 20 or 30 years ago, and it's certainly not a joke now when you're
a grown man still picking on kids. Dude, what is wrong with you? At the end of the day,
your kid's got a pretty valuable skill there already developed, and you're mocking it.
And you're mocking it. You're mocking the creative aspects of masculinity. That's probably
bad idea just saying you might want to get on Twitter and apologize to your son
and all the other kids that got caught in a crossfire because you wanted to look
tough and manly picking on kids. Anyway it's just a thought y'all uh y'all have a
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}