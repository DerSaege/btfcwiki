---
title: Let's talk about foreign languages, an army training film, and the MAGA teen....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=DBHxYV9HbNg) |
| Published | 2019/01/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Emphasizes the importance of speaking the same "language" as the audience to aid communication and understanding, especially for activists and academics.
- Recounts an old army training film where a new lieutenant's attempts to communicate with his troops are hindered by a lack of understanding and respect for rank.
- Describes the significance of a first sergeant in the army and how his authority can impact communication and decision-making.
- Illustrates the importance of finding the right messenger to effectively deliver a message, even if the message itself is well-constructed.
- Mentions the need for considering presentation and perception in communication, citing examples of viral incidents where initial perceptions shifted with more information.
- Warns against jumping to conclusions based on initial viral content, pointing out the power of messaging and its ability to shape perception.

### Quotes

- "Sometimes it may not be that you're constructing a poor argument. It may be that you're just not the person that can deliver it to the audience."
- "The way something is presented is often times more important than the information presented in it."
- "Messaging is important and the way things are perceived a lot of times can be more important than the facts."
- "If we're fighting fake narratives, fake emergencies, we can't succumb to the same things."
- "The reaction wasn't because of the message, but because of the language used when it was first circulated."

### Oneliner

Beau underscores the importance of speaking the audience's "language," choosing the right messenger, and being wary of initial perceptions in communication to combat misinformation effectively.

### Audience

Communicators, Educators, Activists

### On-the-ground actions from transcript

- Find the right messenger for your message (exemplified)
- Tailor your communication to your audience's understanding (exemplified)
- Be cautious of initial perceptions and viral content (exemplified)

### Whats missing in summary

The full transcript provides a detailed narrative on effective communication strategies, historical military dynamics, and the impact of presentation on perception, offering valuable insights for navigating messaging in various contexts.

### Tags

#Communication #Messaging #Perception #AudienceEngagement #CombatMisinformation


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we're gonna talk about foreign languages
and an old army training film
and what we can learn from it.
I speak a few languages
and when I hear somebody struggling in English,
I will switch to their native language if I know it.
Maybe I speak that language better than they speak English
and it can aid in communication.
This is something that I think activists, and especially
academics, really need to pay attention to.
Because at times, I see it a lot,
where people refuse to speak the language of those
they're talking to.
It doesn't matter how well your argument is constructed,
if the person you're talking to doesn't understand
the basic terminology you're using, it's not gonna go anywhere. Sometimes, to get a
point across, it might be better to speak their language, so to speak, even if that
means using terminology that in your circles is no longer used, or maybe slightly reinforcing
what may seem to be reinforcing a negative concept to weed out a much more negative concept.
Now years ago, like 15 years ago, I saw this army training film and it was ancient then.
It was set right after Vietnam and I think that's when it was made.
I'm sure the first copies of this were on reel to reel.
But it's a film designed to help a new lieutenant talk to his troops and kind of get the lay
of the land.
Now what you need to know before I start talking about this is that a second lieutenant walking
into the army, the very first day he reports for duty, he outranks men that had been in
for 15 years and seen combat.
So there's a gap, there's a knowledge gap.
a good first sergeant will train his officers without them ever knowing it, but that takes time.
This is that first day and that first day in walks that lieutenant. He's holding nothing
but his orders and a bible because he is the all-american boy and as he's walking into the
building some of his troops see that. That's important in a minute.
He goes inside, and he winds up talking to the captain, and the captain's giving him
a list of stuff he needs to do, and kind of offhandedly says to somebody else that the
first sergeant is not real receptive of a new grenade launcher the Army's getting.
The Army's getting the M203, and he still likes the M79.
lieutenant he's like well you know I was trained on the on the 203 I'll go talk
to the first sergeant and explain you know the specs on it to them and the
captain being a apparently funny man kind of looks at him was like yeah go do
that you know now what you need to know if you know nothing about the military
and I don't want to over overstate the importance of the first sergeant but
But he's God.
He is God within that company.
So the idea of any second lieutenant explaining anything to a first sergeant is funny to start
with.
And they played this up.
The first scene is the second lieutenant walks in, and there's the first sergeant sitting
at his desk, squirreling his coffee, does a good job of showing his ribbons, and he's
got a rack of them.
And on top of it, he's got a CIB with a wreath and two stars.
In English, that means this man has been around.
He has seen some things, okay?
The implication was that he was in Vietnam and Korea.
And there's a second lieutenant.
He's like, you know, see, here are the specs on it.
It's objectively a better weapon, and it's going to make us more effective in combat.
effective in combat, Hassan. You know a lot about combat. First sergeant sets his
coffee down, walks out of his own office. Conversation is over. Nothing is
accomplished. Second scene. They reset it. Lieutenant walks in. Hey, Top, you know,
I'm new to the company. You were in Vietnam, right? Yes. So we're getting the
M203. How would you have used that during Vietnam? How would you have used that, you
know, with the benefits and the drawbacks of it? How would you have used that because
it's not in the manual? Well, it is useful to have that soldier holding his rifle and
the grenade launcher at the same time, and he starts to explain how he'd use it, and
at the same time he's convincing himself unintentionally that maybe it's not such a horrible device.
You know, statistics and specifications, they can get to facts.
Sometimes a little bit of fiction can get to the truth, something we might want to keep
mind. After that, he starts working through the list. Second lieutenant, and
his first task on it is that he's got to give a briefing to his platoon on VD.
That's STDs. So scene one, he walks in and he starts giving the briefing and
And everybody's just kind of rolling their eyes. What does this guy know about it?
He came walking in with a Bible and here he is first first day on the job telling us
Not to do what we as soldiers of the time like to go do
Complete failure no communication
Second scene he goes back to that first sergeant. Hey
Who's been to the medic the most for VD
The first sergeant laughs, he's like, well, that'd probably be Corporal Clapp.
And so they call in Corporal Clapp and they give him the training materials, and he goes
and gives it.
And everybody's laughing because he has stories.
He has a way to relate it.
He speaks the same language, and most importantly, he's the right messenger.
Sometimes it may not be that you're constructing a poor argument.
It may be that you're just not the person that can deliver it to the audience.
I had a lot of people ask me why I didn't say anything about toxic femininity.
Number one, I can barely say the word.
I sound like Trump trying to say anonymous.
Number two, I'm not the right messenger.
Why on earth would anybody listen to me on that subject?
Now that film was 40 years ago, it was probably when it was made, maybe 50.
But it still holds a lot of truth.
The way something is presented is often times more important than the information presented
in it.
Now I know that's not good, that's not the way things should be, but it's the way things
are.
Think about the recent incident with the kid in the Make America Great hack in the native.
way that was presented initially, it looks one way. The way it was presented as
more and more viewpoints came out, it changed. It altered. So you got to be
leery about jumping on any viral video right out of the gate because there's
always more to it. Now the more you look at it, the the Catholic schoolboys are
are not the heroes of that story by any means.
But it may be possible that the little smirking kid,
despite that look on his face, he may not
have been the villain either.
It's one of those things we're probably never going to know.
Because it looks, when you watch multiple viewpoints,
it looks like there's a whole lot of stuff that happened
there where one group thought it was somebody else that said something and that while the
whole thing was blown out of proportion, the reaction wasn't because of the message,
because of the language used when it was first circulated. And it created this flurry of
memes and most of them are of the same vein. They are propaganda more than
anything. I loved watching people I knew that I watched share memes glorifying
the police response at Standing Rock. All of a sudden sharing memes talking about
You know, how could they dishonor this native?
Yeah, where were you when when they were getting hit with tear gas?
Messaging is important and the way things are perceived a lot of times can be more important than the facts
As far as the reaction that it generates
And it's just something that we need to be conscious of, especially if we are in a battle
against fake narratives, fake emergencies.
If we're fighting that, we can't succumb to the same things.
Anyways, just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}