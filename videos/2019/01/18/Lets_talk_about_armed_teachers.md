---
title: Let's talk about armed teachers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=1o1l2LQGyP8) |
| Published | 2019/01/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses the topic of armed teachers in schools, describing it as a Band-Aid solution at best for a serious issue.
- Training teachers to use firearms effectively in active shooter situations is deemed impractical due to lack of resources and intense training requirements.
- The dynamics of a teacher being in a live shooting scenario are discussed, focusing on the high stakes and intense pressure to protect students.
- Suggestions are made for creating mobile cover using filing cabinets with steel plates and implementing frangible ammo to reduce risks to students.
- Beau underlines the psychological impact on teachers and the unrealistic expectations placed on them in such scenarios.
- He advises teachers to realistically gauge their ability to handle the responsibilities and risks associated with being armed in a school setting.
- The transcript concludes with an emphasis on exploring alternative solutions and setting better examples rather than relying on arming teachers.

### Quotes

- "It's a Band-Aid on a bullet wound at best."
- "You're talking about close quarters combat in incredibly, incredibly difficult scenarios."
- "You alone as an individual, as a teacher, as an educator, you have to become the expert in combat."
- "Liability and effectiveness don't go hand in hand. This is America today."
- "This isn't going to solve the problem. It's just a thought."

### Oneliner

Beau addresses the impracticality and risks of arming teachers, stressing the intense training needed for close-quarters combat and the need for better solutions.

### Audience

Teachers, educators

### On-the-ground actions from transcript

- Build mobile cover using filing cabinets with steel plates (exemplified)
- Implement frangible ammo to reduce risks to students (exemplified)
- Realistically gauge your ability to handle the responsibilities and risks associated with being armed in a school setting (implied)

### Whats missing in summary

The full transcript provides a detailed exploration of the challenges and risks associated with arming teachers, offering practical insights and considerations for educators facing such situations.

### Tags

#ArmedTeachers #SchoolSafety #TeacherTraining #CombatSkills #RiskAssessment


## Transcript
Well, howdy there, Internet people. It's Beau again.
So, today we're going to talk about something I don't want to talk about.
Something I've been trying to avoid.
I've been asked about it a couple times.
The difference is I was asked by a teacher now.
So we're going to talk about armed teachers. Armed teachers in schools.
Is it a solution? No. No.
It's a Band-Aid on a bullet wound at best.
Will it help, maybe, if it's done right?
Is it going to be done right?
Absolutely not.
You guys have trouble getting money for pencils, paper,
textbooks from this century, things you actually
need to do your job.
I'm very hard pressed to believe that the school board
going to give you an ammo budget and to do what you're talking about you got to
be better trained than the average cop a lot better trained and that's not to say
well no it is to say cops don't get good training but it you can't train to that
level you you got to train the level of an HRT a hostage rescue team because
you're gonna be in a more intense situation than the cops are when they
arrive. What happens? Somebody starts shooting. Cops get notified. Takes them
time to drive there. Eventually they make entry. During that lag, things
stabilize. Kids find cover. School what can get locked down gets locked down.
Kids evacuate. Maybe the shooter runs out of ammo or pops himself. Things
happen. You don't get that luxury. As a teacher, you don't get that. When the
shooting starts, you're there. So when you step out in that hallway, it's not
empty. There's kids running around, panicked. You can't miss, period. You
cannot miss because if you do, you're hitting another student. You can't miss.
So, you've got to get good, real good at putting the bullets where you want them.
And then once you get to that point, you've got to figure out how to do that while you're
moving, while kids are running in front of you.
You have to change your entire mindset.
When you're walking through the hallway, you're not a teacher anymore, you're a warrior.
not worried about the students thinking about them and your class, you're wondering if
that nook over there is going to make good cover. You have to change everything. You
have to get very good. You have to train and you have to do it all in secret. You can't
tell anybody. The second a student finds out, you're done. It's over. Any effectiveness
you had is now gone. Contrary to popular belief, the idea of an armed teacher isn't going
to be a deterrent. There's already cops in schools. There's already armed people at schools.
It's not a deterrent. Students find out you got a gun and there is a school shooting,
just number one on the kill list. So no. No. It's not practical. It is not practical.
The amount of ammo it's going to take to get you where you need to be to engage in
a close-quarters battle in a crowded hallway, it is insane and that's expensive.
It is expensive.
You want to make it workable, okay?
Can't leave your class.
You can't leave your class.
School shooting starts, you lock down your class, you save those 30 lives, everybody
else is on their own.
reality, because you can't go engage him. You got to wait for him to come to you.
If you're in one of those schools that's built like a prison, you know, with the
wings that can get locked down, maybe you can lock that down successfully and
protect those students. Maybe.
The problem is, even in that scenario, schools are meant to be inviting.
Your classroom is inviting.
Schools are inviting.
There's a lot of open space.
You know what there's not a lot of? Cover.
There's not a lot of things to hide behind.
You can't engage them by standing there in the open.
You'll die.
You will die.
They have the initiative.
has the initiative. You can't just stand in the hallway so you got to make cover.
You have to make cover and you got to make something that can sit in your
classroom and the students not know what it is because the second they know it's
over.
filing cabinet. You're going to take a filing cabinet and you're going to put wheels on it.
By the time you're done with the wheels, it needs to be just high enough for you to peek over.
You're going to fill that filing cabinet, you're going to put armor, well, no,
putting plates in the back of it. That's going to run a grand.
I'm assuming, as a teacher, that's not in your budget, although apparently now
for your pay we're also expecting you to. Call up your local metal manufacturer
your local metal fabricator or welder. Can't tell them what you're really
doing though because you can't tell anybody. He may have a kid who he tells
so you can't tell them. Tell them you're gonna build a range so you need steel
plates and you're going to give them the measurements from the inside of that filing cabinet.
You need steel plates that you can wring a 308 off of, write it down.
308, it's about the most powerful thing you can realistically expect to run into in this
kind of situation.
So you're going to get those plates cut and then you're going to stick them in the back
of that filing cabinet.
You can't do that.
You're going to get textbooks, phone books, and fill it.
Standing vertically, not sideways, just laying in there,
standing up vertically.
You're going to fill it.
So you've got mobile cover.
Put that in your classroom.
And if you're in one of those schools that you can
successfully lock down that hallway, you're going to put
one near the entry to that hallway, whether it be another
classroom or a closet, whatever.
So you have something that will stop a bullet that you
can be behind.
Otherwise, this is all a moot point.
Most schools are concrete.
You're going to want to put frangible ammo in your weapon.
It's a special type of ammo that when it hits the wall, it
breaks up, lowers the risk of ricochet and hitting one of
those kids that's panicked in the hallway.
You need to watch that video, Buying a Gun for Political Violence.
Watch that, become familiar with the concepts in that, and do a realistic evaluation of
whether or not you are really going to be able to put the brains of one of your students
on the ground because that's what it's going to take.
Good news is that psychology is working in your favor for a change.
When you're talking about cops, people that have to make entry, they have to set aside
their instinct for self-preservation.
You don't have to do that.
You're already at risk, so you have to fight.
You're more likely to get the fight side of that fight or flight response.
So that'll help.
Then the other thing you've got to figure out is how you're going to avoid getting
killed when the cops finally do make entry.
They don't know what's going on, they just see somebody with a gun.
Can't tell them.
nobody gets to know. Nobody. The second one student finds out, all of them know.
And then if a school shooting happens in your school, it's going to start in your
classroom and you're going to be the first target. Because you're a threat.
nobody can know. So, nah, nah, it's not a solution. It is not a solution. For this to
be a solution, the amount of training and money that it would take is ridiculous.
You'd have to engage in psychological warfare against the students as well.
Having the principal announce fake messages about, oh it's range day
teachers and make it seem like everybody's got a gun. Not really a place
I'd want to send my kid to be honest. Now I'm not saying don't do it. I'm not.
I'm not. In fact I feel more comfortable with the idea of you doing it than
people who think it's a good idea because at least you understand that you
You are so far from the level of training you need to do this that maybe you'll take
it seriously.
You're talking about close quarters combat in incredibly, incredibly difficult scenarios.
hallway. Ask any vet. Ask anybody who's ever been in a gun fight. Where do you
not want to be? Somewhere where you can only move towards the target or away
from it. You don't want to be there because there's nothing. When you
and lean up against the lockers, there's no cover.
There's nothing to hide behind.
Maybe, maybe it'll help.
It's not a solution, but maybe it is that bandaid
on a bullet wound until someone comes up.
But what you have to do is you have to weigh
the risk of a school shooting versus the likelihood of you losing control of that weapon in that classroom.
The likelihood of a student seeing it because none of them ever can. Ever.
If it works, it's because you made it work.
It's going to be up to you.
You alone as an individual, as a teacher, as an educator, you have to become the expert
in combat because the school board's policy is going to be horrible.
I can tell you that now.
They're going to get some police advisor who doesn't know what they're doing, probably
from the local sheriff's office.
Maybe he's from their SRT, from the sheriff's response team.
Maybe he's part of that.
But even then, their training is on a static scenario.
Their training isn't 10 seconds after the shooting.
Their training is after it's stabilized.
They're not expecting kids running in the hallways.
They're going to be more concerned about liability than effectiveness.
When you're talking about combat, liability is just part of it.
Liability and effectiveness don't go hand in hand.
This is America today.
need mobile cover and need to need to learn how to engage in close quarters
combat. It's fantastic. This is the world we're in.
Anyway,
we probably need to look at other solutions and other concepts and maybe
set better examples than this.
This isn't going to solve the problem.
It's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}