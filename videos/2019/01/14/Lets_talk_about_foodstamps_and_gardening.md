---
title: Let's talk about foodstamps and gardening....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=l4WTeesiFiw) |
| Published | 2019/01/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Stumbled upon the option to use food stamps for purchasing items like grapevines, fruit trees, and seeds, leading to a positive response from people on social media.
- Addresses misconceptions about food stamps, with over 10% of Americans using them, and the average household receiving around $250 a month.
- Debunks stereotypes around food stamp users, with about half being working families and the program not solely for "lazy" individuals.
- Challenges common complaints about food stamp usage, like the misconception that recipients only buy junk food.
- Points out that the prevalence of food stamps is due to a broken system and deflects blame onto marginalized groups rather than those responsible for the system's flaws.
- Warns of potential unrest if food stamps were eliminated due to hunger-induced desperation.
- Encourages growing food using food stamps, mentioning the ability to purchase items like seeds and fruit trees, especially beneficial for those living in cities with limited space.
- Offers creative solutions for urban gardening, such as vertical gardening and utilizing unused spaces like shoe organizers or community centers.
- Suggests collaborating with others to set up community gardens or growing food on others' properties with permission.
- Emphasizes the long-term benefits of growing food, acknowledging the time investment before reaping the produce and the potential stability it provides for those facing poverty.
- Acknowledges the challenges of climate and varying growing conditions, suggesting different options such as container gardening or raised beds based on one's location.
- Advocates for using gardening not only to enhance food security and reduce dependence but also to strengthen community ties and potentially improve one's situation.

### Quotes

- "If you are doing well in this system, you might just want to eat this one."
- "Every little bit helps in that regard."
- "Once you're locked into that cycle of poverty, it's very hard to get out."
- "It's a thought."
- "Y'all have a good night."

### Oneliner

Beau dispels myths about food stamps, encourages urban gardening using SNAP benefits, and advocates for community-building through growing food.

### Audience

Urban dwellers, SNAP recipients

### On-the-ground actions from transcript

- Utilize vertical gardening techniques to maximize space (exemplified).
- Collaborate with a community center or church for a communal garden (suggested).
- Seek permission to grow food on someone else's property (suggested).
- Experiment with container gardening or raised beds based on your location (exemplified).

### Whats missing in summary

The full transcript provides detailed insights into navigating food stamp usage, dispelling myths, and offering practical solutions for urban gardening and community-building through growing food. Viewing the full transcript offers a comprehensive understanding of these topics. 

### Tags

#FoodStamps #UrbanGardening #CommunityBuilding #Myths #Poverty


## Transcript
Well, howdy there, internet people, it's Bo again.
So I was in the garden section the other day.
I was looking to get in some more grapevines,
and I happened to look on the side of the package,
and it said that this product could be bought
with a snap, food stamps, and I thought that was great.
I mean, that's cool.
And then I did a little bit of research,
found out that that applies to fruit trees,
berry bushes, seeds, anything that's gonna grow food. It's fantastic. I took a photo of the
of the sticker and I put it on Facebook in the comment section filled up with people talking
about how to do it or talking about that they probably couldn't do it because they live in a
city or their climate or whatever. But it isn't a solution for everybody but it's a solution for
some, so I want to talk about it. But before we talk about that we have to
talk about food stamps. Period. Because it's a hot-button political issue and
therefore it's been ruined with talking points. There's a lot of misconceptions.
So here we go. Here are the facts. More than 10% of Americans are on food stamps.
That is a substantial number. That's a lot of people. You're talking I think it
was 44 million that is a lot of people. On average the average household gets
about $250 a month in benefits. That's not a whole lot for food for a month.
About half are working families. It's not the stereotypical image you have in your
head. If you don't, the odds are you know somebody on food stamps. Odds are one of
your friends is on snap. So it's not what you think it is. Okay, so let's just go
through the typical complaints real quick. You know, I was at the grocery store
and the person in front of me had a full cart and they paid for it with an EBT
card you know I don't even eat that well yeah I'm sure their cart was full those
benefits get paid out once a month so what you're looking at is a month's
worth of food you go there every week don't you that's the difference that's
why well it was all junk food probably it's probably all cheap food frozen
pizzas stuff like that yeah it's because it's cheap it's because it's cheap it
stretches that out they've got to last a month on it it's also not very healthy
which is kind of why I think the growing your own foods probably a benefit in a
lot of ways well they're just lazy bombs there are tens of thousands of active
duty military on food stamps. The amount of work you're willing to do has
absolutely nothing to do with whether or not you end up on food stamps. That
argument dies right there. It has to do with income. Period. Full stop. Well, it's
a sign of the breakup of the American family. All these single mothers on food
stamps. Not as much as you think. If you have, say you got two parents, they both
work full-time at a job making a little bit more than minimum wage and they have
two kids, they'll qualify. They will qualify. The prevalence of food stamps
has to do with the fact that the system is broken. And the people who broke it
have done a very good job of training Americans to kick down. They always have
you looking down. It's the immigrants fault. It's the refugees fault. It's those
people on food stamps. Anybody doing worse than you, it's their fault. It's not
the people who actually broke the system. It's not the people making the decisions
up in DC. It's not the people enabling multi-billion dollar corporations to pay
their employees next to nothing because they know those employees will be okay
because they'll be subsidized with your tax dollars. It's not them. It's not them.
It's the family working full-time on minimum wage. It's their fault. And let's
just go ahead and take it to its conclusion. You want to get rid of food
stamps? Okay. You're gonna get a system change real quick. People are not
known for being calm when they're hungry. So if you are doing well in this system,
you might just want to eat this one. There are a lot of other government
wastes that you may want to focus on before this. This government shutdown
goes on much longer and food stamps really do dry up, you'll see it. Anyway, so
there you go. That is dispelling some of the general myths of this. So now let's
talk about the fact that you can actually buy seeds and fruit trees and
bushes and vines and all of this stuff that grow food with snap cards. It's
fantastic. It's fantastic. So the major concern from that comment section was
people who live in cities. They're like, I can't do it here. I don't have any land.
I don't have any space. Okay so some solutions, one of which I can't believe I
didn't think of myself because I do it. Vertical gardening. Now I've took a
pallet and flipped it on its end and I've closed off the bottom so it makes
a little trough, fill it with soil and that's my herb garden. You can do it with anything,
well not anything but a lot of things. It works, takes up very very little space and
you can get a lot of variety out of it because you have different rows. You also don't have
to weed it as much because it's in the air. One I didn't think of, which I thought was
really cool, was those little organizers for shoes that hang on the back of a closet, they
little pouches in them. People use those, they fill them with soil, they poke a hole in the bottom
for drainage, and they grow in those. They can hang those on whatever on the sunny side of the house.
I thought that was cool. Some of the other people I talked to that grow in cities, they had some
some more unconventional means of doing it. I'm not recommending this, but it's worth mentioning
because I mean that is problem-solving right there. He just grows whatever,
wherever on any unused land that he finds. Plants in an isolated area, grows it.
Again, I'm 100% certain that is illegal, but I mean that's cool. Okay. Another
option is to, you know, use a church community center or something like that
and set up a community garden so everybody pitches in.
Again, it also builds that network, builds that community,
which is sorely lacking in this country.
Another option would be to grow it
on somebody else's property who knows you're doing it.
And I think you'd be surprised.
A lot of people, this is how farming was done
for a very, very long time, a lot of people
would be more than happy to let you put in
a couple of apple trees or a blueberry bush
whatever on their property. You get the produce off of it because you're increasing the value of
their land and it's providing a stable location for you. A lot of times people on food stamps,
people of lower means, they end up moving. They stay in the same city, but they end up moving around.
This will still give you access to this because trees and bushes, as mentioned in the comment
section do take years to produce, and this would give you access to them no matter where
you live.
And that was one of the other things, it takes years to produce, some of the things that
I was talking about.
Yeah, it does, it does.
The reality is, once you're locked into that cycle of poverty, it's very hard to get out.
In a couple of years, when apples start coming, you're probably going to need them still.
Every little bit helps in that regard.
Now a lot of people were talking about doing veggies.
The entry to that is a whole lot less.
You get seed packets for a dollar.
They don't come back year after year in most cases, but it's something that can be done.
There are questions about climate.
That's another one.
It's something I forget about.
I live in the South.
Right now I'm in USDA growing zone 8 in English.
That means I throw seeds on the ground and spit on them and something grows.
It's very easy to grow here.
If you live in North Dakota, this is probably not something that's going to work for you.
So those are some of the different options.
own. You can always grow in containers, in pots, or raised beds, anything like that.
And again, it's something that you can use to help build your community and strengthen your
community. And it increases your food security, hopefully cuts down your dependence, and
maybe can contribute to getting you out. It's a thought. So, anyway, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}