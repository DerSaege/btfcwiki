---
title: Let's talk about the wall gofundme's failure, math, and Israel's wall....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NdcseSQWdlA) |
| Published | 2019/01/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the situation with the wall GoFundMe being refunded.
- Questions the intention behind redirecting donations to a new non-profit.
- Criticizes the misunderstanding that donations could dictate government actions.
- Breaks down the math behind the raised funds, exposing the minimal percentage in relation to the wall cost.
- Asserts that the lack of funding does not indicate widespread support for the wall.
- Emphasizes Americans' opposition to the wall on moral, ethical, legal, and economic grounds.
- Draws parallels between the proposed US wall and Israel's wall, focusing on differences in design and purpose.
- Argues that Israel's wall, often cited as a success, has not effectively stopped terrorism.
- Concludes by questioning the feasibility and effectiveness of a similar wall in the US.

### Quotes

- "Americans don't want the wall. They know that it's morally wrong, ethically wrong, legally wrong, economically wrong, and mathematically wrong."
- "If just a third of the people in the United States wanted the wall, it could have raised more than a hundred million dollars, but it didn't."
- "The reason the GoFundMe was an utter failure other than its complete lack of understanding of how the country works is the fact that Americans don't want the wall."
- "I think most Americans shudder at the idea of a foreign head of state looking at our president in 20 years going, Mr. President, tear down this wall."
- "Their wall is not a line that can be bypassed. Their wall surrounds their opposition."

### Oneliner

Beau breaks down the flawed math and misconceptions behind the wall GoFundMe, revealing Americans' strong opposition to the wall and questioning its feasibility based on Israel's example.

### Audience

American citizens

### On-the-ground actions from transcript

- Educate others on the inaccuracies surrounding the wall GoFundMe and the implications of supporting such initiatives (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the financial and ethical implications of the wall GoFundMe, shedding light on the public sentiment and debunking misconceptions surrounding border walls.

### Tags

#WallGoFundMe #BorderWall #PublicSentiment #EthicalConcerns #Misconceptions


## Transcript
Well howdy there internet people, it's Bo again.
So today we are going to talk about the wall GoFundMe.
Why?
Because the money is being refunded, go figure.
The guy that is in charge of the GoFundMe says that he would like people to redirect
their donations to his new non-profit.
Not sure how I feel about that.
The reason the money is being refunded is because they realized that they can't give
the money to the government and make the government do what they want.
That's not how our country works.
I would be leery of any non-profit set up with that intention that does not understand
that.
It's terrifying to me that people thought that that was how it worked.
More importantly, and what is more terrifying to me, is watching people do the math on this.
So we're going to do the math.
are saying they raised a fifth of the gold. No. No you didn't. No you didn't. A fifth
of a billion dollars is 200 million dollars. You raised 20. That's a tenth. You raised
a tenth of a fifth. So you raised a fiftieth of a billion dollars. You raised 2%. And that's
That's cool, except that the billion dollars is a fifth of what Trump's asking for.
So you raised one 250th.
And what he's asking for is the down payment.
Estimates for the completed wall range from 20 to 70 billion dollars.
The one that I liked, like when I looked at the methodology, the one that I think is accurate
is about 35 billion dollars.
You need to take that 250 and multiply it by 7.
So you have raised 1,750th of what is required to build the wall.
That's nothing.
This does not prove that people want the wall, in fact it proves the opposite.
If just a third, one third of the people in the United States wanted the wall and were
willing to give a dollar, one dollar, it would have raised more than a hundred
million dollars but it didn't. It did not. I think the easiest way to put in
perspective how much money was actually raised is to not look at the cost of
building the wall but rather just the maintenance. The wall is going to run
about a hundred fifty million dollars a year in maintenance. So if you ran this
GoFundMe every other month and you got 20 million dollars every other month, well, you'd
be able to pay to maintain it.
The reason the GoFundMe was an utter failure other than its complete lack of understanding
of how the country works is the fact that Americans don't want the wall.
Americans don't want the wall.
They know that it's morally wrong.
They know it's ethically wrong.
They know it's legally wrong.
They know it's economically wrong, and they know that you're mathematically wrong.
I think most Americans shudder at the idea of a foreign head of state looking at our
president in 20 years going, Mr. President, tear down this wall.
We don't want to join the ranks of countries that engage in apartheid.
It's not what we want, most people, anyway.
And I've had people say, you know, other countries have walls that have worked.
Name one.
And the one that people always point to is Israel.
Israel has a wall and it works.
I'm not going to get into the moral arguments surrounding the Israeli-Palestinian conflict.
What I am going to do is talk about the effectiveness of that wall and the differences between the
US wall and the proposed US wall and their wall.
Their wall is not a line that can be bypassed.
Their wall surrounds their opposition.
They also control the waters off the coast.
They also control the air.
And it wasn't designed to stop immigration.
It was designed to stop terrorism.
And they do a whole bunch of things that would be illegal and unconstitutional in the United
States to attempt to make that wall work.
And it doesn't, you point to it as a success, but it does not work.
If it did work, it wouldn't be in the news all the time.
What did it do?
Changed the method of delivery.
That's what it did.
That's it.
Instead of some PLF guy walking in and dropping some SimTech somewhere, now they fire a rocket.
That's it.
The IDF, the Israeli Defense Forces, they fired tunnels all the time.
The wall didn't work there either, guys.
And that's with completely subverting everything that we hold dear in the United States as
far as unreasonable search and seizure, the separation, not using the military at home.
We'd have to give all of that up and it still wouldn't work.
I actually love the example.
Anyway, so it's just a thought.
Uh, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}