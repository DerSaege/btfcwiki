---
title: Let's talk about that cop getting killed....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ZMULqDaRCwM) |
| Published | 2019/01/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Mentions the names of cops killed in the last 60 days by gunfire, some heroes like Sergeant Helles and Corporal Singh.
- Talks about how headlines focus on an officer's immigration status rather than their heroic actions.
- Compares the narrative around immigrant crime to the Molly Tibbets case and the flawed logic of holding entire demographics accountable for one person's actions.
- Criticizes the inconsistency in applying laws and concerns about bias in studies from organizations like FAIR and CIS.
- Raises the issue of race, pointing out the bias against brown people and the lack of outrage for officers like Officer Smith who was recently killed.
- Draws parallels between the lack of attention to certain military incidents based on political narratives and agendas.
- Urges people to stop using the graves of honorable individuals for political points.

### Quotes

- "Stop standing on the graves, the corpses, the flag-covered coffins of people far more honorable than you to make a political point they might not agree with."
- "You don't know the names of those cops, more than likely."
- "People far more honorable than you."

### Oneliner

Beau addresses the focus on officers' immigration status over their heroism, challenges the narrative on immigrant crime, criticizes bias in studies, raises issues of race, and urges against using graves for political points.

### Audience

Activists, community members

### On-the-ground actions from transcript

- Challenge biased narratives (implied)
- Support community policing efforts (implied)

### Whats missing in summary

The emotional impact of using fallen officers for political gain.

### Tags

#PoliceBrutality #Immigration #RaceRelations #PoliticalNarratives #CommunityPolicing


## Transcript
Howdy there, internet people.
It's Bo again.
Sergeant Helles, Officer Jimenez, Deputy Marshall White, Officer
Edgar Flores, Officer Michael Smith.
Not the cops you thought I was going to talk about, is it?
I didn't want to talk about any of this, to be honest.
I was told I was avoiding the subject because it didn't fit my narrative.
No, it fits my narrative just fine.
It makes me angry.
That's the reason I didn't want to talk about it.
You don't know the names of those cops, more than likely.
All cops killed in the last 60 days or so by gunfire.
Some of them real heroes.
Hellas showed up at the scene of an active shooting.
And unlike Parkland, he found his courage and he entered.
He paid for it with his life.
And then as he showed up at a hospital, a domestic disturbance going on, a guy killed
his ex-fiance.
He entered, paid for it with his life.
So why don't you know their names?
But you know, Corporal Sings.
Because nobody can stand on their graves, their corpses, to make a political point.
That's why.
That's why you don't know who they are.
you know him and that's reflected in the headlines.
Illegal Immigrant Kills Police Officer I mean come on we knew he was an illegal before
they were even certain of his name and they put it out there real quick seems like somebody
in that department's got some political ambitions wants to insert themselves in the
national narrative and that is one way to say it you could say illegal immigrant kills
police officer, another way to say it is that an immigrant laid down his life confronting
a gang member.
Corporal Singh was from Fiji.
Whole thing reminds me a lot of that Molly Tibbets thing.
There's that photo of that girl plastered everywhere.
Look at this pretty girl killed by an illegal.
You know what her family's done since then?
They took in an immigrant because they know you don't hold an entire demographic accountable
for the actions of one person.
They're not racists.
They know you don't hold an entire group of people accountable for the actions of one
person, which is what, that's the entire basis of build the wall.
We got to be safe.
We want to be safe and we're going to use that.
Okay, well then, are we going to build a wall around all the cops because all the unarmed people killed?
No, no, don't want to do that because that doesn't fit your narrative, right?
Well, Bo, you know, seeing he came here legally, that other guy was an illegal.
Yeah, you don't get to use that argument anymore.
The second you applauded a bunch of men, women, and kids getting tear gassed
while trying to seek asylum, because under our laws, they should be inside the United
States being treated as U.S. nationals until they're hearing.
You don't care about the law.
You don't.
You don't even know what it is.
I mean, I see that all the time in the comments section, it's hilarious.
And then, you know, it's not about race.
And the same people that say that share studies from FAIR or CIS, or Numbers USA.
FAIR was founded by John Tanton.
And today's political climate, it's messed up.
I don't know if I'm dealing with a white nationalist, a nativist, or somebody who's
just looking for something that fits their narrative when I see this.
Let me tell you something about John Tanton.
John Tanton, the guy who founded FAIR and was instrumental in the other two organizations,
he said that his concern was maintaining a white European majority and a clear one.
He said the white people need to breed more, otherwise we'll be outpaced by Hispanics.
I am certain that the organizations that he's involved with, I'm sure that their studies
are completely accurate, positive.
There's no way in the world that those places would be biased.
You look, most of their studies have been debunked.
And it is about race.
It's about brown people.
I mean, let's be honest.
Let's really be honest with ourselves.
If you saw Corporal Singh, in plain clothes, walking down the street, you'd tell him to
go back where he came from.
You're not outraged about a cop getting killed, you didn't know the names of the other ones,
and just so you know, Smith, he's been killed since Singh got killed, but you don't know
who he is, because you can't stand on his corpse.
Reminds me a lot of Benghazi too.
What happened in Benghazi?
The reality of it.
Some warriors couldn't control their client.
It happens, believe me.
They got caught in an insurmountable situation.
They couldn't get air support and they got left twisting in the wind.
That's what went down.
When you're a part of that community, you understand it can happen.
You don't like it, but you understand it.
When it happens to somebody else in that community, it hurts.
But the reality is, we're not going to risk war with Libya over a few guys.
It's numbers at that point.
Not saying it's right.
I'm saying that's the reality of it.
People got mad about that, understandably, and they talked about it for years under Obama.
But when a very, very similar situation happened in Western Africa, nobody cared.
Green Berets get caught, insurmountable, couldn't get US air.
Some French guys showed up to help.
Even when the ball got dropped notifying the families, nobody cared because it didn't
fit the narrative.
corpses, well they don't matter as much because I can't stand on them to make my
political point. Yeah, I didn't want to talk about this. Here's a New Year's
resolution for you. Stop standing on the graves, the corpses, the flag-covered
coffins of people far more honorable than you to make a political point they might
not agree with. Anyway, it's just a thought. Y'all have a good night. Happy New Year.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}