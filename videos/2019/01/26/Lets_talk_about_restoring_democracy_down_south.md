---
title: Let's talk about restoring democracy down south....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=5L5QrZG2eeM) |
| Published | 2019/01/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the need to restore democracy in the southern regions.
- Mentions the long and complicated history of U.S. involvement in South American affairs.
- Points out the significance of who the intelligence community brings in to run operations as an indicator of their plans.
- Mentions past operations in Nicaragua, El Salvador, and Iran involving heavy hitters who turned a blind eye to atrocities.
- Calls out Elliot Abrams, who was involved in covering up mass murders in the past.
- Expresses concern over Abrams being tasked with operations in Venezuela, signaling trouble ahead.
- Emphasizes that this is not a Trump operation but an intelligence community one.
- Warns about the potential for a civil war in Venezuela and the grim outcomes it may bring.
- Draws parallels with Syria's situation and questions the organic nature of protests against Assad.
- Urges resistance against U.S. involvement in Venezuela to prevent further devastation and loss of lives.

### Quotes

- "No U.S. involvement. No U.S. involvement in Venezuela."
- "This isn't partisan politics. This will save lives. This will save lives."
- "If you've got one of those raised fist profile pictures, now's the time."

### Oneliner

Beau stresses the dangers of U.S. involvement in Venezuela, warning of a potential civil war and urging resistance to save lives.

### Audience

Activists, Advocates, Concerned Citizens

### On-the-ground actions from transcript

- Resist U.S. involvement in Venezuela (suggested)
- Spread awareness about the situation in Venezuela and the risks of intervention (implied)

### Whats missing in summary

Deeper analysis on the implications of foreign intervention in Venezuela and the importance of grassroots resistance.

### Tags

#Democracy #USIntervention #Venezuela #Resistance #CivilWar


## Transcript
Well howdy there internet people, it's Beau again.
So we gotta talk about restoring democracy down south because that's apparently a thing.
That's what we need to talk about.
We've got a long history of it.
Now if you're watching this on Facebook, I made a video about Venezuela last night but
for some reason it wouldn't upload to Facebook, it's on YouTube, I'll put the links in the
comment section. The history of the U.S. engaging in this type of behavior south
of our border is very long and it's very short and one of the things you can
really kind of use to pinpoint exactly what the intelligence community believes
is going to happen is who they bring in, who they task to run operations. That's a
good indicator of what they're planning. The operations in Nicaragua and El
Salvador, you know, back in Iran, Contra days, right from the beginning they
brought in heavy hitters. They brought in, they brought in men that were fully
aware that they were going to have to look the other way when things got nasty.
And by nasty, to clarify the euphemism, mass murder.
They knew they were going to have to look the other way when they came in, and you know,
they did.
In fact, one of the guys that did it, his name was Elliot Abrams.
He was actually, I want to say he pled guilty to a couple of counts of withholding information
Congress about a mass murder. He was accused of covering them up, covering a
bunch of stuff up, but I want to say he was he pled guilty to two counts I think.
One directly related to to a mass murder and he categorized it as opposition
propaganda and it being blown out of proportion. When the investigation was
completed it was discovered that there were 500 people who were systematically
murdered by forces that were backed by the US. Yeah, I mean so you've got to
have those kind of guys in charge and that telegraphs what the
intelligence community is up to. Kind of sends a message real quick this is
what you need to be prepared for because these are the guys we're bringing in. So
Who are they bringing in to run the operations in Venezuela?
Elliott Abrams, the same guy.
The same guy.
That's who got tasked.
And I know some people are probably thinking, well, you know, maybe he's
just a friend of Donald Trump.
No, no, he actually adamantly and openly opposed him during the campaign.
I want to say his name actually got floated for a deputy cabinet position.
And Trump said, no, because of his opposition.
But all of a sudden, it's cool to have them in because this is not a Trump thing.
This is not a Trump operation.
This is an intelligence community operation.
This was in the works long before the contested election in Venezuela and long before Trump was elected.
You're bringing in somebody like that.
It's because you know it's going to get bad.
It is going to get nasty.
And you need somebody who is proving themselves in that regard.
So that's where we're at.
And that is real bad news for the people of Venezuela because they're
in a no-win situation now.
If Maduro stays in power and holds on, it is very likely that the intelligence
community funds forces and triggers a civil war. If the right-wing groups, the opposition
groups come in, it's very likely that Maduro's forces are going to fight back. And it's the
civilian. In civil wars, it is the civilian who dies. This whole thing is shaping up to
look a whole lot like Syria. And once we found out about intelligence community involvement
In Syria, we had to go back and we had to start thinking, were these protests against
Assad organic?
Was it really the people that wanted this fight?
Or were they funded from outside sources?
Were they inundated with propaganda?
And of course, we found out they were.
They were inundated with propaganda.
those protests and the initial spark for that civil war was not organic.
Now we have to wonder the same thing about Venezuela.
All those crazy claims that Maduro has made about the US intelligence trying to kill
him and all of this stuff, now we have to wonder if they're true.
Now we have to wonder if they're true because they have brought in somebody that very clearly
states their goals.
I'd be very nervous if I was in Venezuela right now.
I would be very nervous.
If this goes loud, if shooting starts, it's going to get real bad, real quick.
There are a lot of people on the Democratic side of the aisle, for the first two years
this administration, you guys have been, it's time to resist. Now's the time. This isn't partisan
politics. This will save lives. This will save lives. This is something you need to resist.
No U.S. involvement. No U.S. involvement in Venezuela. We need to send that message and make
it very clear. We can't do this. We can't do this to yet another country and rip another country apart
and caused tens of thousands of deaths, over natural resources.
There are people who oppose Maduro in Venezuela.
There are.
There are people who oppose Trump in the United States.
Does that give another nation the right to come in and start a war?
Anyway, it's just a thought.
If you've got one of those raised fist profile pictures, now's the time.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}