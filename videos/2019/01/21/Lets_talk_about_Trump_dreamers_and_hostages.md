---
title: Let's talk about Trump, dreamers, and hostages....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=SQeaeDzubbI) |
| Published | 2019/01/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Trump's negotiation tactics involve offering protection for Dreamers in exchange for $5.7 billion to fund the border wall.
- Dreamers, or DACA recipients, are not a threat and are deeply ingrained in American culture.
- Trump's offer of three years of protection to Dreamers and TPS recipients seems arbitrary and lacks a path to citizenship.
- Trump's approach is compared to that of a thug or terrorist, using hostages to achieve his goals.
- The President's actions are seen as criminal and embarrassing, especially when leveraging federal law enforcement.
- Congress and the Senate are criticized for not calling out Trump's behavior.
- Beau advocates for establishing a clear path to citizenship for Dreamers and TPS recipients.
- The political tactics used by Trump are condemned as disruptive and detrimental to society.
- Beau points out the flaws in Trump's approach and urges adherence to constitutional procedures.
- The transcript concludes with a call for action and a reminder to stay safe.

### Quotes

- "The President of the United States is acting like a criminal, and he's using the guns of federal law enforcement as his soldiers in the street."
- "We need to establish a path to citizenship for these people."
- "It's extortion."
- "The President knows they're not a detriment, the President knows they're not a threat, but that will not stop him from destroying their lives to get what he wants."
- "Not disrupt the lives of federal employees, of people in need, of everybody."

### Oneliner

President Trump's negotiation tactics with Dreamers and TPS recipients are likened to that of a criminal, lacking a clear path to citizenship and causing disruption.

### Audience

Advocates, activists, voters

### On-the-ground actions from transcript

- Advocate for establishing a clear path to citizenship for Dreamers and TPS recipients (implied)
- Call out political figures who do not condemn detrimental actions (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of President Trump's negotiation tactics and their impact on Dreamers and TPS recipients. Viewing the entire transcript offers a comprehensive understanding of the speaker's perspective and call to action.

### Tags

#Immigration #Dreamers #TPS #Citizenship #Advocacy #PoliticalTactics


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're going to talk about President Trump's olive ranch,
his negotiation tactics, the art of the deal.
He's extended an offer to get his $5.7 billion and like every hostage
taker before him, he starts by offering to release the children to get what he wants.
that $5.7 billion down payment for his wall.
In exchange for that, he'll offer three years of protection
for the dreamers, kids, immigrants who came here as kids.
Now, I don't think we should be talking about
whether or not we should take the deal.
I think we should talk about the fact
that even a two-bit bank robber knows that releasing the kids is an act of good faith.
Very presidential.
The act of a statesman.
The act of a thug.
The act of a man using the guns and cages of ice to hold people hostage to get $5.7
billion dollars to build a monument to himself.
See the thing is, Dreamers themselves, that term is confusing because a lot of people
think it has to do with the Dream Act.
Well, the Dream Act never passed, that's not a thing.
Dreamers has become slang for DACA recipient and I know a number of them, I know quite
a few of them.
these people, they've got to get a background check. They have to prove that they are either
in school, gainfully employed, or in the US military, and they have to have been brought
to the United States as kids. Most of them are more American in heart and mind than the
I'd say half of the ones I know don't even speak the language of their home country,
because they came here when they were two or three.
The interesting thing about this is the most anti-immigrant president in modern memory
is willing to offer them three years of protection.
Why?
Because he knows they're not a threat.
knows these people are not a detriment to the United States, so he's willing to give
them blanket immunity for three years.
Why three?
Why not seven?
Why not ten?
Why can't we just establish a path to citizenship for them, which is what the original Dream
Act included?
Why are we holding these people hostage?
The president knows they're not a detriment, the president knows they're not a threat,
but that will not stop him from destroying their lives to get what he wants.
Just like every other terrorist that has ever taken hostages.
So the other group of people that are being offered protection are people that are in
TPS.
That's Temporary Protected Status.
If you don't know what that is, that's basically there's a natural disaster somewhere.
The US agrees to take people from that country.
They come here temporarily.
Some of them decide they want to stay.
So they hang out and they keep filing for this.
people again, they've been here for years, there's no problem, they're gainfully employed,
they're doing everything they're supposed to.
Now the other thing people need to understand is that they're legal, they're legal, they're
following the law, they're doing everything right.
But somebody, President Trump by the way, cut six countries from the program and gave
those people a date and said you need to be out of the country by this date if
not we're gonna round you up and put you in a camp not his words but that's
what's gonna happen so his olive branch is to undo what he did it's the mob pay
Give me protection money by $5.7 billion and maybe your windows don't get broke.
It's extortion.
The President of the United States is acting like a criminal, and he's using the guns
of federal law enforcement as his soldiers in the street.
It's an embarrassment.
What's more embarrassing is that the people in Congress and in the Senate aren't calling
him out for it.
This is thuggery.
It's embarrassing.
They're kids.
They came here as kids.
I don't think they should be punished for the sins of their father.
We need to establish a path to citizenship for these people.
And I know there's some people saying, well, they didn't follow the procedure.
Last I heard, you did not get a public works project by shutting down the government and
threatening to harm kids.
You want to talk about procedures?
Okay.
The procedure is you submit the legislation for your stupid wall and watch you get shot
down.
That's the procedure.
That's what the Constitution says you're supposed to do.
Not shut down the country.
Not disrupt the lives of federal employees, of people in need, of everybody.
Because you want a monument to yourself, Mr. President.
Anyway, it's just a thought.
Y'all have a good night.
Be careful out there.
I'm John for HostageTakers.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}