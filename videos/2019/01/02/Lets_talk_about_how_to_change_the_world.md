---
title: Let's talk about how to change the world....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=9TeuLkOiXVc) |
| Published | 2019/01/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau starts with the introduction to a series on changing the world, comparing it to a previous video about guns, and underlines the importance of the information he is about to share.
- The inspiration for this video comes from a genuine comment on Facebook expressing the need for information to help others, leading to a reflection on breaking the cycle of societal control mechanisms.
- Drawing parallels to George Orwell's "1984," Beau explains the need to become conscious to rebel against societal constraints and restrictions.
- He shares his personal experience of needing help and the importance of building a network to achieve independence and assist others.
- Beau acknowledges the challenge of feeling isolated and encourages finding like-minded people to form a supportive network.
- Addressing single parents, Beau stresses that everyone has something valuable to offer in building a community network, regardless of their circumstances.
- He expresses the idea of force multiplication through community networks and the power of collective strength in creating resilient communities independent of political influences.
- Beau rejects the idea of running for office and advocates for bottom-up community building over top-down leadership as a more effective way to bring about positive change.
- Recognizing the diversity of situations, Beau hints at discussing tailored approaches for different socio-economic backgrounds in the upcoming videos.
- Ending with a reference to Orwell's "1984" and a message of hope in community strength, Beau signs off with a thought-provoking note.

### Quotes

- "Being independent doesn't actually mean doing everything by yourself. It means having the freedom to choose how it gets done."
- "If you make your community strong enough, it doesn't matter who gets elected."
- "You're changing the world you live in by building your community."

### Oneliner

Beau breaks down the cycle of societal control, urging conscious rebellion and community-building for lasting change and independence.

### Audience

Community Members

### On-the-ground actions from transcript

- Build a supportive network by reaching out to like-minded individuals (implied).
- Offer your skills and resources to contribute to community building (implied).
- Encourage single parents to involve their children in community activities to network effectively (implied).

### Whats missing in summary

The full transcript provides a detailed roadmap on breaking free from societal constraints, fostering community connections, and advocating for grassroots empowerment.

### Tags

#CommunityBuilding #Independence #ConsciousRebellion #StrengthInUnity #BottomUpLeadership


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today is going to be part one of how to change the world.
This video is going to be a lot like that gun video, the
first part in that series.
Kind of boring, but you need the information in this to
understand the rest of it.
This was prompted by a comment on Facebook.
It's a long comment.
I'm not going to read the whole thing.
But it was very, very genuine.
And the end of it said that other people might need the
same information. I agree. It starts off with downtrodden reached, light bulb went
off. This is what I've always felt, believed, tried to live by. I'm not alone.
Now what? Right? We've been there. In 1984, George Orwell wrote that they will
never rebel until they become conscious and they can't become conscious until
after they rebel. Something along those lines. I might have messed that quote up.
But, the point is that it's a cycle and it's reinforced.
The society we live in today in the Western world is very similar to 1984's fictional
world.
It's not as brutal, it's not as drab, but there are certainly control mechanisms in
place that keep you thinking within the box.
And you can't think outside of that box until you become conscious.
then you can rebel.
The hard part's over.
You've become conscious.
You broke that cycle.
Now you have to rebel.
Now the good news is you don't need a rifle.
The message goes on, the comment goes on, I'm still poor, isolated, feel like I'm
living in a hostile environment, and I'm overwhelmed.
I'm still dependent on government.
If I knew how to raise my kids safely and be independent, I would, but I need help to
need help anymore and I want to help other people. That is a noble goal. You're
always going to need help. I'm a pretty independent guy and a lot of you may
know that after Hurricane Michael I was down in Panama City cutting down trees,
clearing debris, delivering food and medicine, whatever was needed. Meanwhile
back here yeah I cleared all the trees except one was right near the house and
it was a big tree about five feet off the ground it split into two trunks and
when the hurricane came through it twisted it one of the trunks leaned one
direction the other one stood more or less straight up couldn't cut it down
the whole thing at once because I didn't know because of how the weight was
distributed. I didn't know where it was going to fall and there weren't a lot of
places for it to fall that it wouldn't damage something. If I cut
off one trunk at a time, if I cut off the right trunk, it looked like it was going to
go into the dining room, the left trunk, and pretty sure my wife wouldn't have
appreciated that. If I cut off the left trunk, it looked like the right trunk
would have fallen, well, right behind that camera through the wall in the shop and I
wasn't real thrilled about that prospect. So I rattled my network, called this guy
Jason. Jason was in the Air Force and he was part of Red Horse. If you don't know
what that is, it's like their version of the Seabees. If you don't know what that
is, these guys, these are people that you could be standing on the side of a
mountain in the middle of the woods and say, hey I need to land a C-130 here.
And you walk away and a day and a half later you come back and it's an airfield.
How did they do it? I don't know it's magic. So he comes over and he looks at
the tree, he shakes it, walks out to his truck, grabs a strap, comes in, straps it
up, cuts the tree, both pieces boom-boom land in this 10-foot clearing right on
top of each other. Don't hit anything. How'd he do it? Magic. I don't know. I needed
that help though because I never would have been able to do that. I needed
somebody with that skill set. I needed that network. And if you want to be
independent, you're gonna need that network. Being independent doesn't
actually mean doing everything by yourself. It means having the freedom to
choose how it gets done. You are going to need that network and you're gonna have
to build it. And I know you said you felt isolated. Those people are around. You
just got to find them. And we're gonna talk about how to build that network in
the next video. I'm gonna go out on a limb here. You didn't say but you said I
use the pronoun I and you talk about your kids. I'm gonna assume you're a
single parent. I've dealt with this before. When you're talking about
building a network, single parents are normally at a loss. I had a single mom
that wanted to to become more self-reliant but she's a single mom you
know she wakes up in the morning get your kid on the bus goes to work comes
home just before he gets off the bus spends the rest of the time with him
because babysitters are really expensive then on the weekend she spends time with
her kid takes him to the zoo the park or whatever she felt there was no way for
her to contribute to a network like this because she didn't have anything to offer, because
she couldn't break away from her kid because babysitters are really expensive.
Didn't take long for her to realize that babysitters are really expensive and that
it would be really easy for her to bring other people in the network, bring their kids with
her and her kid when she went to the park or went to the zoo.
Everybody
has something to offer a network like this.
Everybody.
But you need to do a real honest self-evaluation
of your skills and what you can offer.
And you're going to need to know that before you start trying to build the network.
It's going to be pretty critical.
message goes on, we need to be force multiplied before we can become force
multipliers. Yeah, because that's what that network is. That network is force
multiply. And as that network grows, because when you build your network those
people are going to have friends and then eventually they'll be part of that
network. It'll grow to the point where it's no longer just a network, it's a
community. And as you build that community, you're building your community.
You're making your community stronger. If you make your community strong enough,
it doesn't matter who gets elected. Doesn't matter who's sitting in the
Oval Office. Doesn't matter if you hate Trump or Obama, whoever it is, they're in
power. You're gonna be fine because your community can take care of itself.
You're changing the world you live in.
You're changing the world by building your community.
People in the comments section joke about me running for office.
The reason I'll never run for office, well, one of them is that politicians have skeletons
in their closet.
I've got a mass grave the UN should be investigating to be honest.
The other reason is I'm just a guy.
I'm just as corruptible as anybody else that has ever been given power.
We've lived under rulers in this country for a really long time.
Not sure that's really where we need to go.
Not sure top-down leadership is the answer.
Think maybe community building is the answer.
You know what's best for your community and your area.
You know it more than anybody in your state capitol, a representative that shows
up to do a PR photo op once a year and certainly more than anybody that's
sitting in the Oval Office. So anyway we're gonna talk about this. I've tried
to figure out how to build a template to build these networks. The problem is
everybody's situation is very very different. If you are middle class in the
suburbs you doing it's gonna be a whole lot different than somebody that is
economically disadvantaged in a city or somebody in a rural community where a
lot of this it already exists you call it a good old boys network that networks
critical that network is critical so we're going to talk about that in the
next video. Another quote from 1984 is that if there is any hope, it must lie
in the proles. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}