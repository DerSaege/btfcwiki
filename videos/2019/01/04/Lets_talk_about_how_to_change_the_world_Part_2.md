---
title: Let's talk about how to change the world (Part 2)....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=qlHhJ6XCjFw) |
| Published | 2019/01/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the importance of building community-based networks to achieve independence.
- Mentions NATO's Stay Behind Organizations during the Cold War.
- Explains the idea of forming networks before crises to effectively carry out operations.
- Describes the diverse roles within these networks, ranging from military to civilian.
- Emphasizes the logistics involved in running resistance operations.
- Mentions the Gladiou network as an example that went off track but still proved the concept's effectiveness.
- Encourages self-evaluation to determine what skills or expertise one can offer to start a network.
- Gives examples of how people of different ages can contribute to these networks.
- Acknowledges concerns from introverts and suggests ways for them to participate.
- Talks about recruiting members for the network and different methods to do so.
- Suggests starting with immediate circles and expanding through social media and local groups.
- Shares ideas on structuring the organization, including barter systems and communal commitments.
- Encourages community service as a way to attract like-minded individuals to the network.

### Quotes

- "Anything is possible if you wish hard enough, at least that's what Peter Pan said."
- "Everybody has something to offer a network like this."
- "Rule 303, if you have the means at hands you have the responsibility."
- "Those people look for the helpers, you know."
- "Normal people answering questions in the comment section, that's fantastic."

### Oneliner

Beau explains building community networks for independence, inspired by NATO's Stay Behind Organizations, urging self-evaluation and diverse contributions to form resilient networks.

### Audience

Community members

### On-the-ground actions from transcript

- Start by evaluating what skills or expertise you can offer to begin building a community network (suggested).
- Reach out to immediate circles like friends, co-workers, or local groups to recruit members for the network (exemplified).
- Engage in community service activities to attract like-minded individuals to join the network (implied).

### Whats missing in summary

The full transcript provides a detailed guide on forming community-based networks for independence, recruiting members, and structuring organizations effectively.

### Tags

#CommunityNetworks #Independence #BuildingConnections #Resilience #Recruitment


## Transcript
Well, howdy there, internet people, it's Beau again.
So today is part two of how to change the world by building community-based networks
that will make you more independent.
If you haven't watched that first video, go back and watch it because you kind of need
that to understand what we're talking about.
Otherwise, this is going to sound really weird, especially in the beginning.
A lot of the comments on Facebook were concerned about the limitations of these types of networks
The reason I first became interested in them and started really thinking about them was
because of something called Stay Behind Organizations.
During the Cold War, NATO created these little networks all over Europe and their job, as
the name suggests, was to stay behind in the event of a Soviet invasion.
What they did was they put people at railroad yards, hospitals, police stations, city halls,
all kinds of different places.
And they figured they needed to do it beforehand because they learned during World War II that
if you waited until after the invasion, the Foreign Intelligence Service would be looking
for new people in these places.
So they had to get in place beforehand.
A lot of these guys were military.
Some of them.
Some of them were intelligence.
But a whole lot of them were civilians.
And I want you to think about what NATO believed they could accomplish.
They thought they could basically ferment a revolution from inside the country and they
could run resistance operations.
That entails a lot.
That entails a whole lot.
You're talking about treating the wounded, gathering intelligence, running arms, maybe
engaging in military operations, sabotage, all kinds of stuff.
of which carries an amazing amount of logistics behind it if you actually want to accomplish
it. NATO was convinced that these little networks could do it and they're set up the same way.
It's a group of people come together for a cause, they accomplish it, and then they go
on about their lives. Now if you know anything about stay behind organizations, you're probably
going you got to talk about gladiobow yeah okay so there was one of these
called gladiou that went off the rails they got way out of hand even without an
invasion they carry out assassinations all kinds of stuff not the best example
but that was one of like 50 of these things and the thing is though if you
take away the horror of this situation, shows you exactly how effective they are
and what they can accomplish. The logistics behind some of the stuff that
they carried out was amazing. Again, I'm not saying that what they did was good,
I'm just saying that it proved the theory. So are there limitations? Sure, but
they're determined by the people in the network. Anything is possible if you wish
hard enough, at least that's what Peter Pan said. So how do you go about starting
one of these things for purposes other than, you know, resisting an invasion?
Well, you have to start off with a real self-evaluation. What do you have to
offer and what do the people in your immediate circle have to offer that you
can kind of use to begin? Because that's how it has to start. It has to start with
you offering to do stuff and then in the hopes of it being reciprocated and when
you're doing that you got to be real honest with yourself okay yeah you you
built a birdhouse but are you really a carpenter you know that type of thing
and then you have to get over your own hang-ups and we see a lot of that in the
comments, and we're going to talk about the three main ones.
Everybody has something to offer a network like this.
Everyone, every single person.
There was a comment on Facebook from somebody that had a
medical issue.
And the overriding theme of it was something that comes from
older people a lot.
And it's basically, at this point in my life, I'm broke.
I am broken.
I can't go out and do anything.
I pretty much stay at home.
OK, but you've got decades of life experience.
You have decades of experience at something.
And that expertise is valuable.
Sometimes it's not actually going out and swinging a
hammer.
It's providing the advice to make that easier.
Don't sell yourself short.
Most cultures actually look upon older people as wise.
Because, well, you are.
And then there was a comment from a younger guy.
I want to say he was 17, 16, 17, something like that.
He's like, man, I want to do this.
But can I do it at my age?
Absolutely.
Absolutely you can.
In fact, the story in the last video about the guy with the
chainsaw coming out, I met him as a teenager.
We worked at a fast food joint together.
We just happened to, you know, we went on about our lives
and traveled similar paths and we kept running
into each other and as fate would have it,
you know, here we are 20 years later.
And he lives, I don't know, 45 minutes away.
And so you can definitely start it now.
You can definitely start it now.
And the thing is, even though you don't have any skills
offer. It may seem that way. I'm a kid. I don't have anything to offer. Oh yeah you do.
Another guy in my network is named Keith. And somebody else I met as a teenager. This
spring his 17 year old boy is going to come out and help me put a fence around the property.
He doesn't know how to put up a fence, but he will by the time we're done. See at your
age you have a lot of energy and that's very helpful to people my age and it's
very helpful to you because if you were to start this now and were to use social
media to help link up with other people in your community that need something
you can learn so much you can get a free education you can gather this wide array
of skills just by saying yeah I'll help you do that. You know he's gonna come out
here and have no idea how to do this and by the time we're done because it is a
it's it's an ordeal he's gonna know how to do it he's gonna have that skill
mastered. So even beyond the fact that he's kind of you know putting his chips
into this little network of ours he's getting something out of it right away.
Knowledge, he's learning and that's that's
That's extremely empowering, especially when you're talking about becoming more independent
So yes, you can do this when you're dealing with people that you're meeting on social media
I don't have to tell you but I'm going to make sure you check them out. Be safe
The other group of people that have a concern is introverts, you know, this is easy if you're a social butterfly
not so much if you're an introvert. Yeah, but if you're an introvert, you probably
have a job that allows you to be alone most times, which means you probably have
skills that other people don't have. I'm gonna guess, I mean, you use social media,
okay, so you can network that way, at least in the beginning, build a digital
community very similar and maybe you can provide computer services or whatever it is that you
do via that method so you don't have to actually meet these people and then as time goes on and
you interact with them online you may actually end up meeting them. Just an idea to get it out
there everybody though everybody has something to offer a network like this that there are no
exceptions to that. Everybody. So how do you recruit people to join your network?
Well obviously you start off with your immediate circle. That's where it
begins and that could be your friends, your co-workers, people from school. Maybe
you're in a club that focuses on some hobby and you can expand it beyond that.
that. You can look on social media. You know, there's a lot of local-based
Facebook groups and you could look in those. Then, I mean, another good place to
find people that would be interested in this is activist communities. Those
people look for the helpers, you know. Those people that are out there already
doing it, getting nothing in return most times. Those people are definitely going
be interested in something like this, and if you just can't, for whatever reason, apply
any of that. You can look into something called time banks. Now, people on Facebook are already
familiar with this because that link showed up over and over and over again. Time banks
do have some limitations. They're mainly in larger cities, and it doesn't always translate
into actual network building, but it gets you out there, and eventually you can find
the people to build those networks. It's kind of like I said, it's kind of like a
Craigslist for this type of thing. The other thing you have to decide is the
structure. How are you going to structure your organization? I know a group
of Army guys and what they do is they've got poker chips with their names on them
and when you know when somebody comes and does a favor for you, you give them a
poker chip and later on they've got that poker chip with your name on it.
And that's kind of how their system works.
There's a storm coming and branches falling.
So there's no formal agreement, there's no money changing hands or anything like that,
but there's a marker and that's one way to do it is to engage in some kind of barter
system that's there. The other way to do it is set it up kind of on a more social
level where everybody's involved and everybody just makes the commitment to
help if they can. Rule 303, if you have the means at hands you have the
responsibility, that type of thing. And then you can go further than that, you
can turn it into monthly meetings, you can engage in community service in your
community that helps get the word out about your network and kind of shows
that you're a group of people worthy of being around. There was a group
set up, I think it was set up by a guy named Rick. I get asked to
participate in a lot of activist groups and I normally don't for a whole bunch
of reasons but you know I help as I can but I don't ever join a group. One of
these groups, the first two guys I met from it, they really impressed me so I
was like well I'll go check them out and I was like okay so where do I go? How do
I find out? You know, how do I meet up with you guys? And they're like well on
you know Thursday we're feeding the homeless here, on Saturday we're gonna be
doing this, on Sunday this, next Tuesday this, and it just it was a list. Rather
than just saying, oh well we have a monthly meeting on the 27th. What that told me was
that these people were active. And it wasn't, hey come to this monthly meeting and we're
going to talk. It's like, hey show up and help. Yeah, that's, I like to that. And when
you're creating something like this, those are the kind of people you want to attract.
You want to attract people that are less about talking, more about actually getting something
done and that's one way to demonstrate that that's who you are and that's how
you're going to attract them. Normally people that are busy, that are
active, they're going to be more likely to come out if it's something they're
certain isn't going to be a waste of their time. So if there's already a goal
that they they can get behind, like feeding the homeless, they're going to be
more likely to come than going to have coffee. So those are the basics of how to
set one up. Now if you have any questions ask and the cool thing is in the comment
section there's other people answering the questions. That's fantastic.
This little series is going to continue because not just can these little
organizations and these little networks help you with the everyday things. It
It can be translated into things much larger than that.
These things, yeah, there is that barter system in play, that network of favors that can exist,
but it can also help you become more financially independent by kind of backing each other
up.
We're going to talk about that in the next video because there is, most of the people
who are interested in this type of thing are of limited means because they see the need
for it and we're gonna try to talk about some ways to maybe help increase your
means and I'm not talking about don't go to Starbucks and save the five dollars
and talk about things that might actually help. So anyway it's just a
fart. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}