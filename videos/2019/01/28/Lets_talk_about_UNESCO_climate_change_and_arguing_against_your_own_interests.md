---
title: Let's talk about UNESCO, climate change, and arguing against your own interests....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xKFlY09pCNc) |
| Published | 2019/01/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Expresses shock and disappointment at the United States' withdrawal from UNESCO, an organization that registers and protects historical and cultural sites.
- Questions the reasons behind the decision to withdraw, considering possible justifications like taxation, anti-Semitism, and climate change.
- Outlines the significance of UNESCO's focus on climate change due to the impact of rising sea levels on historical sites near waterways.
- Criticizes arguments against climate change, pointing out the influence of propaganda funded by oil companies and the importance of sustainable practices like stopping deforestation and using renewable energy.
- Challenges individuals to think about why they resist efforts to create a cleaner and more sustainable world, pointing out the contradiction in arguing against their own interests.
- Attributes political resistance to environmental initiatives to campaign contributors' financial interests, leading to a detrimental impact on global efforts for conservation and sustainability.

### Quotes

- "You're arguing against your own interests."
- "The propaganda is that thick."
- "Blows my mind."
- "It's insane."
- "Y'all have a good night."

### Oneliner

Beau questions the US withdrawal from UNESCO, challenges climate change skeptics, and urges individuals to reconsider their resistance to environmental initiatives.

### Audience

Climate activists, sustainability advocates, concerned citizens.

### On-the-ground actions from transcript

- Join environmental organizations to support conservation efforts (exemplified)
- Advocate for sustainable practices in your community (exemplified)
- Educate others about the importance of renewable energy and cutting pollution (exemplified)

### Whats missing in summary

The full transcript provides deeper insights into the impact of political decisions on global conservation efforts and the importance of personal responsibility in supporting environmental sustainability.

### Tags

#ClimateChange #Conservation #Sustainability #UNESCO #PoliticalDecisions


## Transcript
Well, howdy there, internet people, it's Bo again.
So as some of you may have picked up,
I'm a little bit of a history buff, and I like to travel.
You can imagine my horror when I found out the United States
had pulled out of UNESCO.
I was not completely shocked, but I was a little surprised.
If you're not familiar with this organization,
Its main job is to register and work to protect sites of significance, historical, cultural,
whatever.
In the U.S., some UNESCO protected sites are like the Cahokia Mounds, the Statue of Liberty,
Jefferson's House, you know, there's quite a few in the U.S., and we pulled out of it.
And even given the fascist tendency to have a disdain for arts and culture, that's one
of the characteristics of it, there had to be another reason Trump pulled us out.
It couldn't just be that.
So I was at a loss, though.
I was at a loss.
I couldn't figure it out.
I had to start asking around, and I got some funny answers.
Most Trump supporters were like, I don't know.
But a few gave me some answers and then we're going to talk about them.
One was that taxation is theft.
And that is a cool hashtag if you're not familiar with it.
That's a cool hashtag and that may be your argument, but it's certainly not Trump's.
Trump has no problem taxing you, none.
So that's not it.
Then one was that UNESCO is anti-Semitic.
Because it's part of the UN, it's anti-Semitic.
I didn't know what to say to that.
There are nine sites in Israel.
And I guess there was some tension because there are three sites in Palestine as well.
whatever and to show you the amount of sense that this made the the next
statement was that the UN is ran by Jews so and that's why we had to pull out I
don't understand that either especially those anti-semitic Jews I got nothing
then somebody said it got to it that they got the right answer
UNESCO talks about climate change a lot.
And I'm like, that's it, that's it, that's why he pulled us out.
That's what it has to do with.
Um, so I went to a Trump, uh, pro-Trump Facebook group that I used to kind of
find out what, uh, what Trump supporters are thinking and in it, the first one
was, why is this organization even talking about climate change?
They're supposed to be history nerds.
Okay.
Well, as a history nerd, allow me to tell you why they're talking about it.
Prior to say, uh, I don't know what, 70 years ago, there was no air travel.
How'd everything get moved around?
By boat.
So most sites of historical and cultural significance are by waterways.
What is a major concern of climate change? Rising sea levels. That's why. No great mystery.
That's why they're concerned about it. Along the Mediterranean there are dozens of sites that are protected,
two of which are safe from rising sea level. That's why.
Then we get into the argument of climate change isn't real.
I'm a humanities guy. I am a humanities guy. Science is not really my strong suit,
but climate change is something I've been reading a lot about. Now when I say
this next part, I'm not joking. If you actually have a study that doesn't fit
this pattern, please post it in the comment sections. I'd like to
read it. I try to read both sides of this debate. The problem is anytime I look
into a study that is against climate change saying that it isn't a thing. I find out it's
produced by a think tank that is funded by an oil company. Makes it really hard to take
it seriously. It seems like propaganda. So, but even those, most of those, they're not
saying that climate change isn't happening. They're saying that it's not man's fault
that it is. Now I don't know enough about it to sit here and say for certain that
it's man's fault. I don't. So we're gonna go the other way with it. We're gonna say
man has nothing to do with it at all. It's just it's one of those things. But
What are the main suggestions to mitigate it? Stopping deforestation, okay,
moving towards renewable energy, and cutting down on pollution. Where's the
problem being more sustainable? Where's the problem with this? Seriously, why is
this an issue? Even without climate change, without doomsday
scenarios, why is it an issue?
You, we should be doing this anyway.
You know, a bunch of eggheads want us to do it and we're saying no,
because give me a reason.
Deforestation is a problem.
And if you're in the logging industry, allow me to point out that if
deforestation goes too far, you don't have a job looking into ways to, uh,
Make sure that there are always trees. Keeps you in a job. Sustainability is an
obvious thing. We need to work towards that. Cutting down on pollution. You like
clean air, you like clean water, right? These aren't problems. These shouldn't be.
And then renewable energy. Do you not like the idea of not having a light bill?
Well, you know, solar shingles take off, that can happen.
When you realize you're arguing against your own interests, then you need to wonder why.
The propaganda is that thick.
The propaganda is that thick.
And it's normally two people, two kinds of people.
It's either somebody that is very low information and doesn't really look into things for themselves.
And you can see that in the memes.
I'm not a science guy, but when you see the anti-climate change memes, I mean even I'm
looking at it and I'm like, man, I didn't know how that works at all.
And then it's rural people.
It's people from my neck of the woods.
And it drives me crazy because we do it all the time.
Go tell a Republican farmer that he can't amend his soil.
He's going to look at you like you're nuts.
That's sustainability.
You've got a crop that takes a bunch of nitrogen out of the soil, you're going to plant clover,
right?
Find some way to put that nitrogen back in.
We're just talking about that on a bigger level.
You work on your car in your garage and you want to test it, see if it works.
You crank it up and leave it running with the door closed.
The garage door closed.
No, of course not.
That's stupid.
The garage is the earth and its atmosphere.
Cutting down on pollution is a good thing.
You know this, but you're arguing against it because of partisan politics.
You're arguing against your own interests.
That should worry you.
And the real reason that the politicians are arguing against it is because their campaign
Contributors are going to be the ones that have to change their style of business
So and rather than there being a public outcry for them to do so
There's been talk of taxing the carbon tax and stuff like this
To get them to lower their emissions to get them to do things. They should be doing anyway
So they're paying these politicians to
to slow it down so they don't have to do it so they can keep raking in that money.
You're arguing against your own interests
over
and arguing about whether or not something is happening
when it doesn't matter. Even if it's not happening
all of the suggestions we should be doing anyway.
So I really want you to think
about why you're dead set against making the world a cleaner and better place.
Lowering your own light bill.
Making sure that you have a job. Blows my mind.
And because of this, and because the propaganda is so thick they have you guys arguing against your own interests,
We had to pull out of an organization designed to protect sites of historical and cultural significance.
It's insane.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}