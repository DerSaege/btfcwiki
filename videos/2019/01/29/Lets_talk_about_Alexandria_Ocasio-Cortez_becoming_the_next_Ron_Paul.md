---
title: Let's talk about Alexandria Ocasio-Cortez becoming the next Ron Paul....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=w1uR6Xy3kks) |
| Published | 2019/01/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Made a controversial comparison between Alexandria Ocasio-Cortez and Ron Paul, causing a stir among his libertarian friends.
- Discovered that 10-15% of his viewers are overseas, observing the U.S. like the fall of the Roman Empire.
- Describes Ron Paul as a libertarian icon known for his strict adherence to the Constitution and opposition to government overreach.
- Contrasts the views of Ron Paul and Alexandria Ocasio-Cortez, noting their differing perspectives on corporate greed versus government overreach.
- Suggests that while AOC and Ron Paul seem very different, they may both have valid points in critiquing the establishment.
- Points out the potential for AOC to grow in influence and impact, similar to how Ron Paul did over time.
- Raises the question of whether there will be someone to carry on Ron Paul's legacy and if the Democratic Party establishment will allow it to happen.

### Quotes

- "What if it's not corporate establishment versus government establishment? What if it's just establishment versus you?"
- "With a little bit of refinement and a little bit of time, she'll grow just like everybody does."
- "I think that we're looking at a powerhouse in the making."

### Oneliner

Beau contemplates the similarities and differences between Alexandria Ocasio-Cortez and Ron Paul, sparking a debate on establishment politics and the potential for change within the Democratic Party.

### Audience

Political enthusiasts

### On-the-ground actions from transcript

- Analyze and challenge establishment politics (implied)

### Whats missing in summary

Deeper insights into the evolving political landscape and the potential impact of emerging leaders like Alexandria Ocasio-Cortez. 

### Tags

#Politics #RonPaul #AlexandriaOcasioCortez #Establishment #Change #DemocraticParty


## Transcript
Well, howdy there internet people, it's Bo again.
So I said something over the weekend that had my libertarian friends ready to pinochet
me right out of a helicopter.
I compared, well I said that I could see Alexandria Ocasio-Cortez given time and a little bit
of refinement of her beliefs as being the next Ron Paul, and man they got mad.
The other thing that I found out over the weekend was that I looked at the analytics
for the first time and I found that 10-15% of you guys don't live in the U.S. You live
overseas and I guess you're watching this like a live feed of the fall of the Roman
Empire.
So I guess I need to explain who Ron Paul is.
Ron Paul was a doctor, became a congressman from Texas, and he is a philosophical giant
in the libertarian community, which is conservative in the U.S.
He was known as Dr. No, because anything that wasn't expressly authorized by the Constitution
he'd vote no on.
A lot of the legislation that has become a problem today, like the Patriot Act, he voted
against.
He had a very different view of a lot of things.
felt that government overreach and government regulation and government
just pushing down was what was keeping the little guy down. Now you go to AOC
and you see a female young commie from New York and just on appearances they
look very different. That's how she is seen among conservative circles. So
comparing the two, especially in a favorable light, definitely brought a
reaction. Her belief is that corporate greed, and greed in general, is what is
keeping the little guy down. And so she is attracting many of the people that
would have flocked to Ron Paul a generation ago, because these are
millennials that have navigated this destroyed economy for 10 years and they
see that greed as an issue and I know people are gonna say no the economy
isn't destroyed it's not really that bad I don't judge the economy by how well
rich people are doing the strongest indicator of the state of the economy to
me was that you had federal employees who were very well paid who have a very
secure job, missing one paycheck and ending up at a food bank.
That is a stronger indication of the economy to me than whether or not some multi-millionaire
made an extra 10 grand.
She seems to understand that a corporation like Walmart that makes around 14 billion
dollars a year could take half of that profit and give each one of their 1.4
million employees an extra five grand a year and given the average sales
associate makes around 19,000 that's a 25 percent pay increase that is
substantial to anybody some especially somebody that close to the poverty line
So while you are looking at it through the lens of Left vs. Right, yes, they are very
different.
They are very different in that one sees the corporate establishment as a problem, one
sees the government establishment as a problem.
But what if they are both right, both are correct in their assertion?
What if it isn't like how it is portrayed in the media?
What if it isn't corporate establishment vs. government establishment?
What if it's more like Weekend at Bernie's?
What if over here you've got the corporate establishment working the head of that body
and over here you've got the government establishment working the arms and that body is what used
to be a representative of democracy and today we have an oligarchy like the study out of
Princeton and Northwestern suggests.
What if it's not corporate establishment versus government establishment?
What if it's just establishment versus you?
But we're so trained to kick down and to fight each other that we don't see it.
With a little bit of refinement and a little bit of time, she'll grow just like everybody
does.
Ron Paul didn't have all his ducks in a row when he first started.
I think that we're looking at a powerhouse in the making.
It can definitely happen.
The question is, is there going to be anybody to fill Ron Paul's shoes at the same time?
And the question is, whether or not the Democratic Party, the establishment within that is going
to let it happen.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}