---
title: Let's talk about the thing you missed in Trump's speech....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=uebiR0Avstg) |
| Published | 2019/01/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces a different topic, avoiding the expected, and aims to bring original ideas or concepts to the table through his videos.
- He criticizes Trump's speech where he claimed to swear an oath to protect the country, pointing out the importance of swearing to defend the Constitution instead.
- Beau stresses that the Constitution's ideas must outlive the existence of the United States itself.
- He praises the revolutionary aspects of the Constitution, particularly the idea of a government by the people and its adaptability through amendments.
- Beau acknowledges the flawed nature of the Founding Fathers, like Thomas Jefferson, who despite owning slaves, recognized the moral depravity of slavery.
- Emphasizes that the Constitution is a living document and expresses his idealistic view of a world where government becomes unnecessary.
- Beau quotes Thomas Paine on government being a necessary evil and advocates for moving towards a society where government is no longer needed.
- He calls on people to use their voices to speak against injustices and corruption in government, implying a need for activism and vigilance from citizens.

### Quotes

- "When fascism comes to the United States, it'll be wrapped in a flag and carried across."
- "Government, even in its best state, is but a necessary evil."
- "Getting rid of government is not destroying the Constitution. It's carrying it to its logical conclusion."

### Oneliner

Beau challenges the importance of swearing an oath to defend the Constitution over the country itself, advocating for a society where government becomes unnecessary and individuals actively speak out against corruption and injustice.

### Audience

Citizens, Activists

### On-the-ground actions from transcript

- Speak out against injustices and corruption in government by using your voice (implied)
- Advocate for change through activism and vigilance in upholding democratic values (implied)

### Whats missing in summary

The full transcript provides a deeper understanding of the importance of upholding democratic values and the evolving nature of governance through citizen activism.

### Tags

#Democracy #Constitution #Activism #Government #Freedom


## Transcript
Well, howdy, internet people, it's Beau again.
So tonight, I'm not gonna talk about anything
you're expecting me to talk about.
When I make these videos, I try to bring an original idea
to the table, to the conversation.
At the very least, I try to bring a concept or idea
to people that may not have heard it otherwise.
If I can't do that, I don't make a video.
So I'm not gonna talk about the fact
that I called Trump a fascist in the morning,
And by evening, he was giving a speech using the same tactics that Hitler used.
Although that's funny, not going to fact check him, debunk what he said.
Other people would do that now.
I want to talk about some, a little bit more subtle, something in his speech that
I think most people missed and may not understand the importance of the end of
it, he said, I swore an oath to protect our country and that is what I'll always
do. So help me God." That's great. That is a propagandist dream statement right
there. Nationalism, patriotism, eternal vigilance, a religious duty, the desire
to defend your nation. Man, big brother looking out for you. It's great. You are
honor-bound to do this. It's fantastic. Except he never took an oath to defend
country. Now, he took an oath to preserve, protect, and defend the Constitution of
the United States. The very document he's been trying to undermine, the very
document he's currently trying to do an end run around, and it's important. And I
know right now there's people saying, well, you're kind of splitting hairs here,
you know, country, constitution, it's pretty much the same thing. It's not. When people
come up with these oaths, they agonize over every word. The reason you swear an oath to
the Constitution rather than the country is that any two-bit thug can get elected. Any
two-bit thug can seize power. Do you still defend it? No. The reason you swear an oath
to the Constitution is because the Constitution and the ideas in it are more important than
the country.
One day the United States will cease to be.
Like all empires, it will fall.
But the ideas in the Constitution must live on.
We take it for granted today.
We really do.
We take it for granted.
It's hard to really grasp how revolutionary, pun kind of intended, that document is.
On two levels.
The first is the idea of a government of the people.
That was, I mean, just groundbreaking.
It was laughable.
It was laughable.
The idea that it would even work.
Self-governance, give me a break.
But it's still here.
And, you know, there's probably people right now going,
oh, you know, there was some pretty messed up stuff
in the Constitution, right?
And, yeah, there was, there was, and the fact that institutions
like slavery are gone, but the Constitution remains is the
other groundbreaking thing about it.
It included the machinery for change.
You know, we look at the Founding Fathers.
They were not perfect men by any means.
You know, Thomas Jefferson, perfect example.
Brilliant man, brilliant man, unquestionable.
Also a slaver and a rapist.
Slaver for certain, rapist like 99% certain.
They weren't mythical people.
They were flawed.
And yeah, he didn't even free his own slaves.
But do you know what he said about slavery?
He said it was a moral depravity and a hideous
blocked that would one day have to be undone.
And they included the machinery for change.
They included that in the Constitution.
It's a living document.
It is a living document.
You know, there are people who know me, who know my laughable ideal of what the world
should be like one day.
Like, I don't even understand how you can even care about the Constitution.
In the world you want, it'd be kind of irrelevant.
And I guess that's one way to look at it.
I guess that is one way to look at it.
Another way to look at it is that it's fulfilling it.
It's fulfilling it.
You know, it's said that when fascism comes to the United States, it'll be wrapped in
a flag and carried across.
Certainly seems to be true.
I believe that when freedom comes to the world, it'll originate here.
It'll have that Made in USA stamp on it.
Because we have the machinery for change.
And I'm not talking about spreading democracy with bombs, I'm talking about real freedom.
Real freedom.
You know, the idea of no government, of people behaving ethically without having a government
gun pointed at them, it's laughable.
People in competition with each other, peaceful competition, but with an eye towards the better
men of humanity as a whole, it's laughable and that's fine.
I don't think it will happen in my lifetime, I'm okay with that, but I think that should
be the goal.
The goal should be an end to government.
goal should be for man to advance, for humanity to advance to a point where it's unnecessary.
And yeah, that may seem like a slap in the face to this document that I've been talking
about, but it's really not.
The philosophical granddaddy of the American Revolution was a man named Thomas Paine.
Thomas Paine wrote pamphlets.
He's another brilliant man with some problems.
One of my favorite quotes from him was that government, even in its best state, is but
a necessary evil.
No.
Getting rid of government is not destroying the Constitution.
It's carrying it to its logical conclusion.
Today, we the people can ensure tranquility, provide for the common defense, promote the
general welfare, and secure the blessings of liberty everywhere.
Be a good day.
But today, we don't live in that world.
We live in a different world.
If ever the time shall come when vain and aspiring men possess the highest seats in
government, our country will stand in need of its experienced patriots.
That was Samuel Adams.
It's a very relevant quote for today.
It's a battle cry for today, because it is today.
Today is the time you've got to start using your voice, often, to speak out against the
injustices that you see and speak out against the corruption in government that you see
and the undermining of that promise, of that document that possess that machinery for change.
We've got to do it.
We can do it with videos and Facebook posts now.
day our kids are gonna have to do with bullets and lamp posts. Anyway, it's just
a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}