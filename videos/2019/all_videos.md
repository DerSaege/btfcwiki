# All videos from 2019
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14

Click on a video summary for some quotes, a point by point summary, and some possible actions for the topic.
[&lt; 2018](/videos/2018/all_videos.md) - 2019 - [2020 &gt;](/videos/2020/all_videos.md)

## December
<details>
<summary>
2019-12-31: We've talked about everything: A recap moving into 2020.... (<a href="https://youtube.com/watch?v=mEryako3A-U">watch</a> || <a href="/videos/2019/12/31/We_ve_talked_about_everything_A_recap_moving_into_2020">transcript &amp; editable summary</a>)

Beau challenges blind patriotism, addresses gun violence, and advocates for community empowerment.

</summary>

"Guns don't kill people. Governments do."
"We are a violent country. We always have been."
"If you make your community strong enough, it doesn't matter who gets elected."

### AI summary (High error rate! Edit errors on video page)

The president's decision to cut aid to Central American countries out of fear.
Concern over the wall working both ways to confine Americans.
Criticism of the younger generation's lack of exposure to accurate history.
Pointing out the propaganda in history textbooks and the internet's raw information.
Criticizing blind patriotism that supports harmful actions.
Addressing the tactic of using fear to win elections and gain power.
Challenging the unfounded fears and stereotypes around asylum seekers.
Criticizing the dehumanization and mistreatment of migrants at the border.
Addressing the issue of civilian gun violence and the role of governments.
Arguing against the effectiveness of an assault weapons ban.
Exploring the glorification of violence and its impact on society.
Advocating for understanding the historical trauma of minority communities.
Pointing out the importance of arming minority groups for protection.
Addressing the fear and division around the changing demographics of America.
Criticizing the lack of accountability and excessive use of force by law enforcement.
Drawing parallels between legal actions and moral implications in history.

Actions:

for community members, activists,
Contact your representative to address foreign policy impacting migration (implied).
Advocate for understanding historical trauma in minority communities (implied).
Strengthen your community to be self-sufficient regardless of political leadership (implied).
</details>
<details>
<summary>
2019-12-31: Let's talk about Ice T, hobbies, and power.... (<a href="https://youtube.com/watch?v=l9IzxwTAFC8">watch</a> || <a href="/videos/2019/12/31/Lets_talk_about_Ice_T_hobbies_and_power">transcript &amp; editable summary</a>)



</summary>

"Politics isn't a hobby. Politics is power."
"We stop letting politics be a hobby. We start letting it be a calling."
"Stick to what you're truly passionate about and work towards bettering that particular topic."

### AI summary (High error rate! Edit errors on video page)

Beau starts by discussing ICE-T's tweet about a Napoleon quote and a flaming queue image, which led to controversy.
Politics is described as a hobby that some are deeply steeped in, mentioning a far-right theory related to the Trump administration.
Politics should not just be a hobby but a powerful force that requires action to convert beliefs into tangible change.
Beau encourages moving from politics as a hobby to a calling, suggesting taking tangible actions once a month to make a real-world impact.
He stresses the importance of channeling passion into action to see significant changes in society, especially in the year 2020.

Actions:

for activists, community members,
Organize a tangible action once a month to affect real change (suggested)
Channel passion into making a difference in a chosen topic (implied)
</details>
<details>
<summary>
2019-12-29: Let's talk about the global Green New Deal and 100% clean energy.... (<a href="https://youtube.com/watch?v=5eQQX3LBEhE">watch</a> || <a href="/videos/2019/12/29/Lets_talk_about_the_global_Green_New_Deal_and_100_clean_energy">transcript &amp; editable summary</a>)

Beau introduces the monumental global Green New Deal, suggesting immediate action to combat climate change and pollution, despite human resistance to change.

</summary>

"It's a mortgage on the planet."
"We are addicted to fossil fuels."
"We are resistant to change."
"We're going to have to do this at some point. Why not do it now?"
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Introduces the concept of the global Green New Deal as a massive infrastructure project to achieve 100% clean energy by 2050.
Emphasizes that 95% of the technology needed for this project is already available commercially.
Mentions that the remaining theoretical technology pertains to long-distance and ocean travel.
Reveals the $73 trillion price tag spread across 143 countries, with potential to create 28.6 million more jobs than those lost.
Asserts that the project will pay for itself within seven years, even if it takes longer.
Notes the feasibility of the project both technologically and economically, with potential for completion by 2030 if resistance to change is overcome.
Addresses human resistance to change as a significant factor in delaying the implementation of such projects.
Raises the issue of inevitable opposition studies funded by oil companies that may slow down progress.
Urges for immediate action due to the pressing need to combat climate change and pollution.
Ends with a thought-provoking question on why not start the transition away from fossil fuels now.

Actions:

for climate activists, policymakers, environmental advocates.,
Advocate for and support policies that prioritize transitioning to clean energy sources (implied).
Raise awareness about the urgency of addressing climate change and pollution in communities (implied).
Support initiatives that create job opportunities in the clean energy sector (implied).
</details>
<details>
<summary>
2019-12-29: Let's talk about Aleksandr Solzhenitsyn and themes.... (<a href="https://youtube.com/watch?v=1yVS-FLHnc4">watch</a> || <a href="/videos/2019/12/29/Lets_talk_about_Aleksandr_Solzhenitsyn_and_themes">transcript &amp; editable summary</a>)

Beau delves into the universal themes of propaganda, ideology, and power in Solzhenitsyn's "The Gulag Archipelago," urging readers to grasp the insights applicable to all governments and ideologies.

</summary>

"Without evildoers, there would have been no archipelago."
"No government is best, no ideology is best, that they can all be corrupted, and that they can all create evildoers who believe they are doing good."
"There's a lot in it that you'll have to research the history of along the way. But it's worth it."

### AI summary (High error rate! Edit errors on video page)

Alexander Solzhenitsyn published The Gulag Archipelago in 1973, a significant work detailing the brutal Soviet prison system.
Solzhenitsyn wrote the book in sections, not carrying all of it at once due to security concerns.
The book is often seen as a criticism of state communism under Stalin but Beau believes it goes beyond that.
Beau points out the universal nature of the book's themes on propaganda, legality, and morality in all governments and ideologies.
The work examines how collective ideologies can justify actions that individuals wouldn't justify on their own.
Solzhenitsyn understood the dangers of absolute power and unjust hierarchies in governments.
Beau encourages reading the book for its insights into how governments operate and create evildoers who believe they are doing good.
Despite being a Russian book with historical references, Beau recommends it as a valuable read for understanding government dynamics.
Beau notes the parallel between the number of incarcerated individuals in the US today and those in the gulags.
The book's message and subtext make it a valuable read beyond its historical context.

Actions:

for readers, history enthusiasts, book lovers,
Read "The Gulag Archipelago" to understand governmental dynamics (suggested)
Research the historical context of the book's themes (suggested)
Share the insights from the book with others (implied)
</details>
<details>
<summary>
2019-12-28: Let's talk about what it means to be a Republican.... (<a href="https://youtube.com/watch?v=5bUwzgIt4yw">watch</a> || <a href="/videos/2019/12/28/Lets_talk_about_what_it_means_to_be_a_Republican">transcript &amp; editable summary</a>)

Exploring the historical Republican values through Eisenhower's presidency, contrasting past policies with the present party stance.

</summary>

"It's also worth noting that he used his farewell address to warn the United States about something called the Military Industrial Complex."
"But it meant that you hate all of his policies."
"At this time, Republicans still pretended, at least, to care about those people who voted for them."
"That's the reality of it."
"He was a great Republican president in history. Just remember, it's history."

### AI summary (High error rate! Edit errors on video page)

Explains the historical context of the Republican party and what it used to mean.
Mentions the transition from Roosevelt to Eisenhower, showcasing the values and policies of these presidents.
Talks about how Eisenhower continued and expanded on the New Deal policies initiated by Truman.
Describes Eisenhower's actions in office, including strengthening Social Security, increasing the minimum wage, and initiating major public works projects like the Interstate.
Emphasizes Eisenhower's understanding and empathy towards those in need, reflected in policies like the Department of Health, Education, and Welfare.
Notes Eisenhower's support for civil rights, mentioning his involvement in desegregation efforts like the Little Rock Nine.
Acknowledges Eisenhower's faults, such as authorizing CIA activities against communism.
Points out Eisenhower's warning about the Military Industrial Complex in his farewell address.
Reminds the audience of the contrast between Eisenhower's policies and the current stance of the Republican party.
Encourages reflecting on history and understanding the evolution of political ideologies.

Actions:

for history enthusiasts, political analysts.,
Study and understand historical political ideologies (implied).
Analyze and compare past and present policies of political parties (implied).
</details>
<details>
<summary>
2019-12-27: Let's talk about the state and finding the money for it.... (<a href="https://youtube.com/watch?v=veVGLwT5vxw">watch</a> || <a href="/videos/2019/12/27/Lets_talk_about_the_state_and_finding_the_money_for_it">transcript &amp; editable summary</a>)

Beau addresses the funding double standard, linking societal well-being to supporting the marginalized; failing to fund projects for the disadvantaged is akin to killing the state.

</summary>

"When you hear that, just assume it's something that rich people don't want to do."
"Those social safety nets that keep the elderly, the disabled, and just the general poor, keep them alive. Well, those people, they are the state."
"Failing to find the money for it is literally killing the state."

### AI summary (High error rate! Edit errors on video page)

Addresses the question of finding money for projects benefiting lower socioeconomic classes in the U.S.
Points out the double standard in funding when it comes to projects that benefit wealthy classes vs. the general populace.
Mentions Chuck DeVore's statement on progressive policies in California.
Talks about the state's role in serving the people and the dangers of blending corporate interests with government.
Explains the term fascism in the context of a state serving corporate interests.
Notes the lack of funding for education, especially if opposed by wealthier classes.
Gives an example of funding for a project benefiting the wealthy, the Slavery Abolition Act in the UK.
Mentions the extensive loan taken out for reparations to slave owners, paid off in 2015.
Emphasizes that policies benefiting the marginalized actually benefit society as a whole.
Concludes by suggesting that failing to fund projects for the disadvantaged is equivalent to killing the state.

Actions:

for advocates for social justice,
Support and advocate for policies that benefit the lower socioeconomic classes (suggested)
Educate others about the importance of funding social safety nets and infrastructure projects (suggested)
</details>
<details>
<summary>
2019-12-24: Let's talk about how Barbie can change the world.... (<a href="https://youtube.com/watch?v=7KnV-0L6_Ew">watch</a> || <a href="/videos/2019/12/24/Lets_talk_about_how_Barbie_can_change_the_world">transcript &amp; editable summary</a>)

Beau explains how Barbie's evolution towards diversity can positively impact young minds and perceptions on race relations, showcasing the power of representation in shaping attitudes.

</summary>

"Barbie is woke now."
"We win."
"That is going to alter her mindset about race relations in general."
"Even if it is an act, if the results are real, does it matter that it was an act?"
"When brands do this, they are probably doing it for all the wrong reasons. But the results will end up being right."

### AI summary (High error rate! Edit errors on video page)

Explains how Barbie has evolved to represent diverse body types, skin tones, and career choices.
Recounts a heartwarming encounter of a little girl searching for a marine biologist Barbie for her sister.
Emphasizes the positive impact of diverse representation in dolls on shaping young minds.
Points out the significance of a white girl playing with a black Barbie in altering perceptions on race relations.
Expresses optimism that despite current bigotry, positive change will prevail through such representations.

Actions:

for parents, educators, activists,
Buy diverse dolls for children, exemplified
Encourage diverse career aspirations in children, exemplified
</details>
<details>
<summary>
2019-12-24: Let's talk about airplanes, helmets, studies, and statistics.... (<a href="https://youtube.com/watch?v=f615wgvz90M">watch</a> || <a href="/videos/2019/12/24/Lets_talk_about_airplanes_helmets_studies_and_statistics">transcript &amp; editable summary</a>)

Beau explains the flaws in studies and statistics using historical and modern examples, stressing the importance of a complete dataset for accurate interpretations.

</summary>

"When you're missing half of your data set, you can't extrapolate."
"Statistics and studies are useful. They can give you a clear picture of some things."
"If you only look at little bits and pieces or you choose to interpret it however you want, you don't really pay attention to the methodology used and how it's collected."

### AI summary (High error rate! Edit errors on video page)

Illustrates flaws in studies and statistics using historical and modern examples.
Navy commissioned study during World War II on aircraft damage.
Mathematician Wald recommended armor where there were no holes on planes.
Example of new helmet during World War I saving lives but doctors misinterpreted data.
Flaw: analyzing data assuming complete dataset when it's not.
Mention of the misconception about demographic X committing the most crime.
Statistics can be misinterpreted, like the Bureau of Justice Statistics example.
Emphasizes the importance of having a complete dataset for accurate interpretations.
Mentions climate models inaccuracies due to incomplete datasets.
Scientists' estimations improve as they gather more data.

Actions:

for data analysts, researchers, students.,
Verify data sources before making conclusions (implied).
Ensure data completeness for accurate analysis (implied).
</details>
<details>
<summary>
2019-12-23: Let's talk about why being "Anti-Trump" is wrong and solutions.... (<a href="https://youtube.com/watch?v=jwNHtA3hsUs">watch</a> || <a href="/videos/2019/12/23/Lets_talk_about_why_being_Anti-Trump_is_wrong_and_solutions">transcript &amp; editable summary</a>)

Beau urges for a shift from being against to being for, advocating for radical solutions addressing poverty, basic necessities, and education to combat polarization and stagnation.

</summary>

"Being against something doesn't create solutions, just creates resistance."
"We need to move forward with the more radical ideas, those that will create solutions."
"The best defense is a good offense."
"We need to be for ending poverty."
"Teaching people to question. Narratives."

### AI summary (High error rate! Edit errors on video page)

Cable news is opinionated, telling viewers what to be against instead of presenting just news.
Opposition should be based on moral obligation, not just rhetoric.
Being against something creates resistance, not solutions.
The nation is polarized because people focus on being against things rather than for them.
Root causes of many issues are a lack of access to basic necessities and education.
Lack of education leads to fear and opposition to things people don't understand.
Democrats positioning themselves as anti-Trump is not sufficient to bring about real change.
Solutions need to come from individuals, not just political leaders.
Being anti-something is defensive, but being for something is proactive and necessary for progress.
It's time to push for radical ideas that address poverty, basic necessities, and education to create real solutions and avoid repeating the same arguments in the future.

Actions:

for activists, community leaders,
Advocate for education reform to encourage critical thinking and questioning of narratives (implied).
Support initiatives that address poverty and provide basic necessities in communities (implied).
Shift focus from being against something to being for solutions in local activism and organizing (implied).
</details>
<details>
<summary>
2019-12-23: Let's talk about a threat to American democracy.... (<a href="https://youtube.com/watch?v=BBhBNMsOqkc">watch</a> || <a href="/videos/2019/12/23/Lets_talk_about_a_threat_to_American_democracy">transcript &amp; editable summary</a>)

The Trump campaign alleges political action committees are taking in money insincerely, raising concerns about the lack of accountability and potential impact on American democracy.

</summary>

"Trump supporters getting conned? No, no, no, no. Trump supporters are not known for being gullible."
"That's a massive demographic to be tricked that easily."
"There's probably not a meaningful difference, not to the person who made the contribution."

### AI summary (High error rate! Edit errors on video page)

Trump campaign alleging political action committees are taking in money insincerely.
Trump supporters not easily manipulated by silly slogans or memes.
Allegation of almost $50 million flowing to these groups, mostly from contributions less than $200.
Surprising that a massive demographic could be tricked easily.
Public surprise at the lack of outcry to reform campaign finance despite $50 million involved in politics.
$50 million enough to sway a candidate's vote or even buy one.
Lack of accountability in methods used to influence votes.
Concern that money given to Trump's campaign or political action committees may not serve contributors' best interests.
Administration not interested in making America great or representing the electorate.
No meaningful difference between giving money directly to Trump's campaign or political action committees.

Actions:

for american voters,
Verify the authenticity of donation requests before contributing (implied).
Advocate for campaign finance reform and increased transparency (implied).
</details>
<details>
<summary>
2019-12-20: Let's talk about a misunderstood Constitutional premise and a coin.... (<a href="https://youtube.com/watch?v=m3RGizuRbK8">watch</a> || <a href="/videos/2019/12/20/Lets_talk_about_a_misunderstood_Constitutional_premise_and_a_coin">transcript &amp; editable summary</a>)

Beau explains the concept of separation of church and state and how it applies to various contemporary issues, stressing the secular foundation of the United States.

</summary>

"The United States was designed to be a secular nation."
"It was supposed to be secular. Does that mean that our founders weren't Christians? No."
"When you are working for the government as an agent of government, you have no religious freedom."
"The wall is up. Separation of church and state."
"At the end of the day, I think it'll probably go back to what's in the Civil Rights Act."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of separation of church and state, not explicitly stated in the Constitution but implied.
Describes how the United States was designed to be a secular nation where no law favored or opposed any religion.
Mentions a treaty signed in 1797 with Tripoli stating that the U.S. government is not founded on the Christian religion.
Emphasizes that while some founders were Christian, atheist, or deist, the goal was to avoid a theocracy.
Addresses contemporary issues like prayer in schools, displaying religious monuments, and marriage laws in the context of separation of church and state.

Actions:

for americans, policymakers,
Understand and advocate for the separation of church and state (implied)
Ensure that public accommodations are available to everybody, regardless of religious beliefs (implied)
Educate others on the historical context and intent behind the secular foundation of the United States (implied)
</details>
<details>
<summary>
2019-12-19: Let's talk with Kathrin about the UK and women's issues.... (<a href="https://youtube.com/watch?v=-h3WjmwGVqM">watch</a> || <a href="/videos/2019/12/19/Lets_talk_with_Kathrin_about_the_UK_and_women_s_issues">transcript &amp; editable summary</a>)

Beau introduces Catherine, covers the UK election results, Brexit's impact on Ireland, body positivity's flaws, patriarchy's effects on men, and the ties between capitalism and patriarchy.

</summary>

"So many ways in which patriarchy harms men just as much well not just as much but definitely harms men as well."
"Capitalism is inherently based off of the oppression of women."
"I don't think that we can get rid of men dominating women without also getting rid of men, like humans dominating nature and dominating animals."

### AI summary (High error rate! Edit errors on video page)

Introduction of Catherine and her work as an intersectional anti-capitalist YouTuber covering social and political issues from an intersectional anti-capitalist perspective.
Overview of the UK election results where the Conservative Party secured a significant majority while Labour faced a major defeat, with reasons including media bias and their stance on Brexit.
Impact of the election results on Scotland and Northern Ireland, with the SNP gaining seats and potential calls for a second referendum for Scotland to leave England.
Speculation on the impact of Brexit on the border between North and South Ireland, considering varying statements made by Boris Johnson regarding a hard border.
Delving into Catherine's video on body positivity, critiquing how mainstream representations still reinforce normative beauty standards and capitalist consumption.
Explanation of patriarchy as a system where men hold power, discussing how masculinity and femininity traits are valued unequally in society.
Exploration of how patriarchy harms men, leading to issues like suppressed emotions, mental health problems, and harmful gender expectations.
Connection between capitalism and patriarchy, detailing how capitalism historically oppressed women and continues to subjugate them for profit.
Vision of a post-patriarchal society where all hierarchies are dismantled, allowing individuals to live without gendered norms and restrictions.

Actions:

for intersectional activists,
Advocate for gender equality and challenge societal norms through education and awareness (implied).
Support organizations promoting body positivity and representation for all body types (implied).
Engage in community dialogues about patriarchy and its impact on society (implied).
</details>
<details>
<summary>
2019-12-19: Let's talk about what to expect next in the impeachment.... (<a href="https://youtube.com/watch?v=HrNG9cSCsBU">watch</a> || <a href="/videos/2019/12/19/Lets_talk_about_what_to_expect_next_in_the_impeachment">transcript &amp; editable summary</a>)

Beau Gyan criticizes the theatrics of impeachment, questions understanding of due process, and draws parallels to history and current political waiting games.

</summary>

"It is utter nonsense."
"Today I watched as Republican representatives compared the impeachment to Pearl Harbor and the crucifixion of Christ."
"These are representatives on the House floor saying this, as if they don't actually understand how the process works."
"The moral of this story is that right now, both Democrats and Republicans, they're waiting for that morning."
"The Senate has to figure out what the American people will accept before they cast their vote."

### AI summary (High error rate! Edit errors on video page)

Criticizes the theatrics surrounding the impeachment process, labeling it as utter nonsense.
Expresses concern over Republican representatives comparing impeachment to Pearl Harbor and the crucifixion of Christ.
Points out the flawed argument of the lack of due process, especially when made by elected representatives.
Explains the impeachment process, clarifying that the trial takes place in the Senate, not the House.
Shares a historical anecdote about women gaining political power in Argonia in the late 1800s through a prank.
Draws parallels between the current political situation and the waiting game for new evidence to sway opinions on impeachment.
Emphasizes the importance of the Senate considering what the American people will accept before voting on impeachment.

Actions:

for politically engaged citizens,
Understand the impeachment process and how it differs between the House and the Senate (implied)
Stay informed about political proceedings and hold elected representatives accountable for their statements (implied)
Advocate for transparency and accountability in political processes (implied)
</details>
<details>
<summary>
2019-12-18: Let's talk about that TV show and independent journalism.... (<a href="https://youtube.com/watch?v=VsRR4Bwx1lE">watch</a> || <a href="/videos/2019/12/18/Lets_talk_about_that_TV_show_and_independent_journalism">transcript &amp; editable summary</a>)

Beau stresses the importance of independent journalists disrupting narratives and the need for projects like the reality TV show he was approached for, urging people to follow independent voices.

</summary>

"It's something this country desperately needs."
"I've seen that clip a hundred times and I laugh every time because that is independent journalism in a nutshell right there."

### AI summary (High error rate! Edit errors on video page)

Shared a post on social media and received many questions about it.
Approached by a production company in LA to create a reality TV show about independent journalists.
Flew to LA and covered events there and in Vegas with a diverse crew.
Mentioned crew members like Carrie Wedler, Ford, and Derek Brose.
Discussed the possibility of the reality TV show happening in the future.
Stressed the importance of independent journalists disrupting corporate news narratives.
Urged for the need of such projects in the current polarized environment.
Noted that many people do not follow independent journalists, leading to a lack of nuance in debates.
Shared details about his nickname, accent flexibility, and beard trimming for TV.
Encouraged learning different accents for better interaction with people from various regions.
Expressed comfort in being himself on his channel rather than using a non-regional accent.
Recommended watching a specific clip involving Derek getting shot for a glimpse into independent journalism.

Actions:

for journalism enthusiasts, media consumers,
Support independent journalism by following and sharing content (suggested)
Encourage nuance in debates by exposing oneself to various independent journalistic viewpoints (implied)
</details>
<details>
<summary>
2019-12-18: Let's talk about Trump's love letter.... (<a href="https://youtube.com/watch?v=aZl2UDMV-wM">watch</a> || <a href="/videos/2019/12/18/Lets_talk_about_Trump_s_love_letter">transcript &amp; editable summary</a>)

Beau Gyan dissects Trump's letter to Pelosi and speculates on its intended audience, warning Senate Republicans of potential fallout post-Trump.

</summary>

"It's not enough to make a Republican cast their vote for a Democrat, but they certainly might vote for somebody else in a primary who wants a Senator who was tricked."
"Neither one of those sound like very appealing legacies."
"And I think he may be a little worried about that."
"Doesn't take much to get through it."
"So he can remain unscathed while the republicans in the senate suffer the wrath after he's gone."

### AI summary (High error rate! Edit errors on video page)

Analyzes Donald Trump's letter to Nancy Pelosi, questioning the motive behind it.
Considers various possible reasons for Trump's actions, ruling out typical motivations like preserving his legacy or energizing his base.
Suggests that the letter may be intended to rally Senate Republicans, especially in light of public opinion showing a split on Trump's impeachment.
Points out that Senate Republicans may be staying supportive due to Trump's campaign efforts and warns of potential repercussions when more information about Trump's actions surfaces post-presidency.
Raises ethical concerns about Trump's spending on golf and personal profit from taxpayers.
Speculates about how Senate Republicans defending Trump till the end may impact their reputations in the future.
Notes that staying with Trump might affect the re-election prospects of Senate Republicans.
Surmises that there might be internal divisions within the Republican party, despite outward displays of unity.

Actions:

for political analysts, concerned citizens,
Reach out to Senate Republicans to express concerns about their continued support for Trump (implied)
Stay informed about political developments and hold elected officials accountable (implied)
</details>
<details>
<summary>
2019-12-17: Let's talk about the electoral college, power, and Jules Verne.... (<a href="https://youtube.com/watch?v=0Yqf-EHlTus">watch</a> || <a href="/videos/2019/12/17/Lets_talk_about_the_electoral_college_power_and_Jules_Verne">transcript &amp; editable summary</a>)

Beau delves into the Electoral College, advocating for higher standards and participation, suggesting a tougher process for presidential candidates.

</summary>

"It's not about education, it's about participation."
"You want to sit in the most powerful chair in the world? Prove yourself out on the campaign trail."
"We've had enough clowns stumble into that office and they've caused a lot of damage along the way."
"Let's set the standards higher not lower."
"I'm not certain of how to fix this problem but the one thing I am sure about is that the answer is not making it easier to sit in the most powerful office in the world."

### AI summary (High error rate! Edit errors on video page)

Talks about the Electoral College and voting, prompted by his son's question about it.
Shares a joke about John Quincy Adams approving an expedition to the center of the Earth.
Explains his son's concerns about how to prevent unsuitable candidates from reaching the Oval Office.
Mentions the issue of candidates campaigning only in major cities if the Electoral College is eliminated.
Expresses disappointment at his son's elitist view associating better education with people in major cities.
Comments on the importance of participation over geography in elections.
Acknowledges flaws in the current system where votes matter more in swing states.
Suggests that candidates currently win elections by dividing the country and playing one side against the other.
Proposes a harder process for candidates to prove themselves and unite people before becoming president.
Advocates for introducing ranked choice voting to address concerns about voting for third-party candidates.
Emphasizes the need to set higher standards for presidential candidates to make America better.

Actions:

for voters, political activists,
Advocate for ranked choice voting to provide more options for voters (suggested).
Participate in campaigns or initiatives that aim to reform the electoral process (implied).
</details>
<details>
<summary>
2019-12-17: Let's talk about homelessness in the wake of the Supreme Court decision.... (<a href="https://youtube.com/watch?v=Rb-MW0LmbEA">watch</a> || <a href="/videos/2019/12/17/Lets_talk_about_homelessness_in_the_wake_of_the_Supreme_Court_decision">transcript &amp; editable summary</a>)

Supreme Court non-ruling decriminalizes homelessness, forcing companies to become humanitarians and invest in shelters and affordable housing nationwide.

</summary>

"In essence, it decriminalized homelessness."
"They're going to have to chip into the communities they're profiting from."
"We have the technology to solve this problem, we just need the will."
"If you're an advocate for housing for all, if you are an advocate for people who are in poverty, if you're an advocate for the homeless, now is your time."
"You can't criminalize this without an alternative."

### AI summary (High error rate! Edit errors on video page)

Supreme Court non-ruling on Boise versus Martin decriminalized homelessness.
Companies pushing to criminalize homelessness driven by desire to keep homeless away from storefronts.
Companies now have to become humanitarians and contribute to communities they profit from.
Need for homeless shelters and affordable housing will increase.
Ruling currently applies on West Coast but expected to spread across the US.
Predicted consequences: pushing homeless into areas covered by ruling, blaming politicians for increase in homelessness.
Companies previously funding criminalization of homelessness may now support shelters and affordable housing.
Technology exists to solve homelessness issue, lack of will is the obstacle.
Advocates for housing, poverty, and homelessness urged to step up and take action.
Courts now require alternatives before criminalizing homelessness.

Actions:

for advocates, community members,
Advocate for housing for all, poverty, and homeless (exemplified)
Push companies to contribute to communities (implied)
Support organizations providing shelters and affordable housing (implied)
</details>
<details>
<summary>
2019-12-17: Let's talk about DOD's PR disaster.... (<a href="https://youtube.com/watch?v=_xzcgzFraF0">watch</a> || <a href="/videos/2019/12/17/Lets_talk_about_DOD_s_PR_disaster">transcript &amp; editable summary</a>)

Department of Defense posted a glowing biography of a war criminal, sparking confusion and concern, revealing a management blunder, not a moral lapse.

</summary>

"They didn't explain what was going on. It was shocking, to say the least."
"When we see social media blunders they may not always be what they appear..."
"The mistake was likely due to a management error, not a moral one."

### AI summary (High error rate! Edit errors on video page)

Department of Defense posted a glowing biography of a literal war criminal without explanation.
The post was shocking and lacked context, causing confusion and concern.
The mistake was likely due to a management error, not a moral one.
The narrative may have been intended to be posted in parts throughout a commemoration.
The post has been deleted, but there may have been plans for more posts.
The incident serves as a reminder that social media blunders may not always be what they seem.
The importance of proper context and clarity in social media communications.

Actions:

for social media managers,
Contact Department of Defense to express concerns about the post (suggested)
</details>
<details>
<summary>
2019-12-16: Let's talk about an email warning about me.... (<a href="https://youtube.com/watch?v=GWhQPNqqenM">watch</a> || <a href="/videos/2019/12/16/Lets_talk_about_an_email_warning_about_me">transcript &amp; editable summary</a>)

Beau warns about dangerous stereotypes surrounding domestic violence and the importance of not overlooking signs due to preconceived notions.

</summary>

"There is no uniform for a domestic abuser."
"Domestic violence is truly intersectional."
"We really need to remember there is no uniform for a domestic abuser."

### AI summary (High error rate! Edit errors on video page)

Received an email warning a shelter about his appearance before delivering charity supplies.
Stereotype: resembling the image of a person you wouldn't want to buzz in - big beard, tattoos.
Stereotypes can be dangerous as they may lead to missing signs of domestic violence.
Domestic violence affects everyone regardless of race, ethnicity, gender, or socioeconomic class.
Dangerous to perceive domestic violence as only happening in certain areas, leading to overlooking signs.
Shelter previously printed shirts to dispel the stereotype associated with Beau's appearance.
Domestic abusers don't have a specific uniform.
Urges to be mindful of stereotypes and not overlook signs of domestic violence.
Domestic violence is intersectional and can happen anywhere.
Importance of dispelling harmful stereotypes surrounding domestic violence.

Actions:

for community members, advocates,
Support and volunteer at domestic violence shelters (implied)
Educate others on the intersectionality of domestic violence (implied)
Challenge and dispel harmful stereotypes about domestic violence (implied)
</details>
<details>
<summary>
2019-12-16: Let's talk about Virginia, principles, logic, and consistency.... (<a href="https://youtube.com/watch?v=5CH4RbTbLdw">watch</a> || <a href="/videos/2019/12/16/Lets_talk_about_Virginia_principles_logic_and_consistency">transcript &amp; editable summary</a>)

Beau in Virginia challenges the lack of principles in political thought, supporting Second Amendment sanctuaries and urging consistency in beliefs.

</summary>

"If there's not a victim, there's not a crime."
"The mere possession of something doesn't necessarily create a victim."
"Either what you believe to be right and true is right and true or you don't have any principles."
"You guys want to take this stand, more power to you, seriously."
"But I would ask if they're your principles, or you're just being told what to do."

### AI summary (High error rate! Edit errors on video page)

American political thought lacks principles, logic, and consistency.
Virginia is proposing sweeping legislation, but local jurisdictions are resisting by creating Second Amendment sanctuaries.
Beau's principles dictate that for something to be a crime, there must be a victim.
He supports the local jurisdictions' decision to not comply with the proposed laws.
Beau warns against deploying the National Guard to force compliance, as it could escalate and result in real victims.
He mentions the importance of supporting other forms of sanctuary jurisdictions based on consistent principles.
Beau argues that the Constitution reserves certain powers to the states and people, not the federal government.
He stresses the need for logical consistency in principles, rather than being swayed by political propaganda.
Beau encourages individuals to take a stand based on their true principles, even if it may lead to conflict.
He questions whether individuals are truly acting on their principles or simply following directives.

Actions:

for virginia residents, activists,
Support and join Second Amendment sanctuaries (exemplified)
Advocate for consistent principles in political decision-making (exemplified)
Engage in local governance to uphold community values (exemplified)
</details>
<details>
<summary>
2019-12-15: Let's talk about two questions and civil rights.... (<a href="https://youtube.com/watch?v=28caQlP6WQI">watch</a> || <a href="/videos/2019/12/15/Lets_talk_about_two_questions_and_civil_rights">transcript &amp; editable summary</a>)

Exploring why white people who care often have Irish ancestry, examining Civil Rights Movement parallels, and stressing the need to end systemic issues before focusing on reparations.

</summary>

"The problem is in the US, it's not over."
"We can't say, 'Pull yourself up by your bootstraps' until those institutional issues are gone."
"The primary thing is we've got to make it stop first."

### AI summary (High error rate! Edit errors on video page)

Exploring the question of why white people who seem to care or understand are often Irish or of Irish ancestry.
Providing a timeline of the Civil Rights Movement in the 1960s, detailing protests, police response, bombings, and government infiltration.
Drawing parallels between the Black Panthers in the U.S. and the Irish Republican Army in Europe during the same timeline.
Noting the camaraderie between Irish and black communities in the American South due to shared experiences of oppression and intermarriage.
Mentioning that Irish-Americans in the South tend to have strong family bonds and pride in their heritage.
Questioning when personal responsibility should kick in, considering ongoing systemic issues like disproportionate sentencing and redlining.
Stating that until institutional and systemic issues are eradicated, expecting individuals to solely overcome challenges is unreasonable.
Emphasizing the need to first stop systemic issues before taking steps like reparations to make amends.
Criticizing proposed reparations figures as initially insulting but acknowledging recent talks of higher figures being more appropriate.
Stressing the importance of awareness that the fight against systemic issues is ongoing and subtle, requiring continued efforts to overcome.

Actions:

for activists, community members,
Advocate for ending systemic issues (implied)
Support reparations initiatives (implied)
Stay informed and engaged in ongoing fights against systemic racism (implied)
</details>
<details>
<summary>
2019-12-15: Let's talk about solving the problem of homeless vets.... (<a href="https://youtube.com/watch?v=OiJaAbW_ZQk">watch</a> || <a href="/videos/2019/12/15/Lets_talk_about_solving_the_problem_of_homeless_vets">transcript &amp; editable summary</a>)

Beau addresses homelessness among veterans, proposing a solution that involves utilizing existing facilities, honoring treaty obligations, and prioritizing genuine care over political biases.

</summary>

"All it takes is for those people who say they care about homeless vets to love homeless vets more than they hate brown people."
"The solution is there."
"The facilities are already there."
"What are we waiting for?"
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Addresses the issue of homeless veterans and the common response of prioritizing their needs over other social safety net concerns.
Mentions the need for facilities across states to house homeless vets, providing them with necessities like hygiene, food, and shelter.
Points out that there are already facilities capable of housing homeless vets, with room for more people.
Suggests honoring treaty obligations to free up space in facilities currently holding asylum seekers and migrants.
Proposes putting individuals with criminal records or deemed dangerous in county jails, while utilizing current incarcerated individuals for assistance.
Challenges viewers to take action by contacting their senators instead of just discussing the issue online.
Emphasizes the importance of prioritizing the well-being of homeless vets over political agendas.
Advocates for a comprehensive solution to homelessness rather than temporary fixes.
Urges for support in reintegrating homeless individuals back into society through assistance with housing costs.
Calls for a shift in focus towards loving homeless vets more than harboring negative sentiments towards certain groups.

Actions:

for advocates, activists, supporters,
Call your senator to advocate for better facilities to support homeless veterans (suggested)
Support efforts to reintegrate homeless individuals back into society by assisting with housing costs (implied)
</details>
<details>
<summary>
2019-12-14: Let's talk with Greta Thunberg.... (<a href="https://youtube.com/watch?v=-NBbsDmxNjQ">watch</a> || <a href="/videos/2019/12/14/Lets_talk_with_Greta_Thunberg">transcript &amp; editable summary</a>)

Beau confronts Greta Thunberg on her advocacy, mocking her tech skills, but ends up praising her resilience and urging continued activism and learning.

</summary>

"I could have swore they said she's come by."
"You're in this problem right now because a lot of us, that's what we did. We didn't do anything."
"You keep watching documentaries. You be better than us."

### AI summary (High error rate! Edit errors on video page)

Welcomes Greta Thunberg as a special guest, prepared for a confrontational interview due to her controversial figure.
Questions Greta's lack of life experience to advocate for climate change and mocks her inability to work basic technology.
Mistakes a Halloween decoration for Greta and continues the playful banter about generational differences in technology use.
Applauds Greta for facing criticism from powerful figures like Putin and Trump, encouraging her not to chill out but to keep advocating and learning.
Urges Greta to watch documentaries and be better than older generations who didn't take action on climate change.
Responds with silence to Beau's initial mocking about technology, prompting him to realize his mistake.
Listens as Beau acknowledges the unjust bullying she faces online and praises her for standing strong against powerful critics.

Actions:

for climate activists, youth advocates.,
Watch documentaries on climate change and take action to be better advocates (implied).
</details>
<details>
<summary>
2019-12-14: Let's talk about Biden's qualifications.... (<a href="https://youtube.com/watch?v=iFNr_LR164k">watch</a> || <a href="/videos/2019/12/14/Lets_talk_about_Biden_s_qualifications">transcript &amp; editable summary</a>)

Addressing the false claims around Hunter Biden's qualifications, Beau exposes the double standards and political narratives at play.

</summary>

"He has the qualifications. They're right there."
"At the end of the day, Trump in some ways has been a benefit because he's really laid bare the out-and-out lies and corruption in our government."
"Maybe this type of corruption is just accepted here and the idea is that yeah, George Bush was trying to curry favor with Biden and you know, here it's okay, but over there, well, we have something different."

### AI summary (High error rate! Edit errors on video page)

Addressing Hunter Biden's qualifications and the repeated claims of his lack of qualifications for board positions.
Mentioning George Bush's appointment of Hunter Biden to a board of directors and confirmation by the Senate.
Noting Hunter Biden's past role on the Amtrak board of directors and his qualifications, including his education and work experience.
Pointing out the false narrative perpetuated by some Republicans regarding Hunter Biden's qualifications.
Exploring the idea of different standards being applied to Ukraine compared to the US government.
Suggesting that the repeated falsehoods about Hunter Biden are believed by a base that does not fact-check.
Contemplating whether there is a double standard in scrutinizing appointments based on political affiliations.
Acknowledging the potential benefit of President Trump's administration in exposing lies and corruption in government.

Actions:

for fact-checkers, political observers,
Fact-check repeated claims and correct misinformation (implied)
</details>
<details>
<summary>
2019-12-11: Let's talk with Emerican Johnson of Non-Compete.... (<a href="https://youtube.com/watch?v=FObbjvpVVKA">watch</a> || <a href="/videos/2019/12/11/Lets_talk_with_Emerican_Johnson_of_Non-Compete">transcript &amp; editable summary</a>)



</summary>

"The real solution to all of our problems is organizing together and building solidarity with the working class."
"Unions have become a shadow of what they used to be. I think they're starting to come back, fortunately."
"Let's use them, let's fight together, and let's change the world."

### AI summary (High error rate! Edit errors on video page)

American Johnson introduces himself as an anarcho-communist running the YouTube channel Noncompete, engaging with various leftist ideologies critical of capitalism.
The channel, despite discussing complex topics like means of production, uses puppets and Legos to attract a younger audience, prompting concerns regarding COPPA guidelines.
COPPA, aimed at protecting children's privacy online, fined YouTube $127 million for data harvesting from children, threatening content creators with penalties for not collecting data.
American Johnson criticizes the FTC's COPPA enforcement, arguing that it contradicts the First Amendment by restricting free speech based on potential appeal to children.
Concerns arise about potential FTC lawsuits affecting creators globally, like American Johnson's partner Luna, a Vietnamese citizen creating adult-focused content.
American Johnson advocates for alternative social media platforms like Mastodon and PeerTube, promoting decentralized, community-owned networks as replacements for capitalist platforms.
Encouraging viewers to join and create channels on these alternative platforms, American Johnson stresses the importance of supporting and building solidarity within the working class.
In Vietnam, a culture of skepticism towards hierarchy and authority fosters local problem-solving and community-focused governance.
American Johnson's call to action revolves around organizing, building solidarity, and fostering class consciousness as key solutions to societal challenges.
He invites viewers to subscribe to his channel for updates on transitioning to PeerTube and participating in the YouTube walkout on the 10th, advocating for collective action and utilizing available tools to enact change.

Actions:

for creators and activists,
Join Mastodon or PeerTube to support decentralized platforms (suggested)
Start a channel on existing instances like PeerTube (suggested)
Participate in the YouTube walkout on the 10th (implied)
</details>
<details>
<summary>
2019-12-11: Let's talk about shelters, sales, smells, and internet people.... (<a href="https://youtube.com/watch?v=elHndoJW2aI">watch</a> || <a href="/videos/2019/12/11/Lets_talk_about_shelters_sales_smells_and_internet_people">transcript &amp; editable summary</a>)

Beau Gadd organized a livestream to provide Christmas presents for teens in a shelter, showcasing the power of collective action and urging companies to support domestic violence shelters.

</summary>

"Y'all did this in about an hour."
"Seems like a win-win-win-win."
"Shows the value of individual action in pursuit of a collective goal."

### AI summary (High error rate! Edit errors on video page)

Beau organized a livestream on YouTube where donations were earmarked for Christmas presents for teens in a shelter.
Five bags were prepared, each containing a tablet, a Netflix card, a tablet case, earbuds, and a board game.
The tablets and Netflix cards were included to provide the teens with their own space, especially if they have younger siblings monopolizing the TV.
The goal was quickly accomplished through collective effort; people donated money and time to make it happen.
Additional items like cleaning supplies were also provided to the shelter, which are often overlooked but necessary for PTSD sufferers.
Beau questioned why companies don't donate products like diapers and soap to domestic violence shelters to avoid triggering bad memories.
He suggested that companies could benefit from such donations through tax deductions, advertising, and building brand loyalty.
Beau emphasized the importance of individual actions contributing to collective goals, showcasing the power of community effort.
He expressed gratitude towards the internet community for their swift action in addressing the initial problem and going beyond it.
Beau reflected on the world we live in, where companies could do more to support shelters, but also praised the internet community for their quick and effective response.

Actions:

for community members, donors.,
Donate products like diapers and soap to domestic violence shelters for survivors (implied).
</details>
<details>
<summary>
2019-12-10: Let's talk about reports, reactions, and responses.... (<a href="https://youtube.com/watch?v=c1ZFPh4LRyc">watch</a> || <a href="/videos/2019/12/10/Lets_talk_about_reports_reactions_and_responses">transcript &amp; editable summary</a>)

People are rightfully angry after being lied to, but learning from past mistakes and supporting the truth can prevent unnecessary military actions and protect American soldiers.

</summary>

"Be mad, be angry, that's fine, but learn from it."
"Supporting the truth will protect American soldiers more than any politician in DC ever could."
"Iran needs a political response, not a military one."
"This is a moment where the American people can learn from the graves of thousands of American troops."
"The U.S. runs the greatest war machine the world has ever known."

### AI summary (High error rate! Edit errors on video page)

People are rightfully angry after being lied to for 18 years by multiple administrations, leading to immeasurable costs in money and lives.
Rather than formulating an educated response, the U.S. reacted emotionally to the events in the Middle East in 2001, resulting in unnecessary invasions.
The U.S. government is currently pushing the idea that the Taliban wants to make a deal, but experts suggest otherwise.
The Taliban are strategic and waiting out the U.S., with any deal likely not being honored, leading to unnecessary deaths.
Beau points out that learning from past mistakes is critical, such as abandoning the Kurds against expert advice.
There are concerns about a potential march to war with Iran, despite experts warning against it.
Supporting the truth is more beneficial to American soldiers than blind patriotism, and curbing unnecessary military actions is vital.
Beau stresses the need for American citizens to hold their government accountable and make informed decisions about military involvement.

Actions:

for american citizens,
Hold the government accountable by demanding transparency and truthfulness (exemplified)
Educate yourself and others on foreign policy decisions and their consequences (suggested)
Advocate for political responses over military actions in international conflicts (exemplified)
</details>
<details>
<summary>
2019-12-09: Let's talk about Miss Universe, waste, and wins.... (<a href="https://youtube.com/watch?v=KJpwfxdXUq4">watch</a> || <a href="/videos/2019/12/09/Lets_talk_about_Miss_Universe_waste_and_wins">transcript &amp; editable summary</a>)

Beau describes a surprising and uplifting encounter at a remote gas station, showcasing progressive views on acceptance and equality, marking it as a win towards a better world.

</summary>

"I can't believe in this day and age, there are still places where people just can't be themselves."
"I'm assuming you're not picturing us. If you're not picturing us and you are picturing them, you might want What?"
"And sometimes we just got to take the wins where we can get them."

### AI summary (High error rate! Edit errors on video page)

Describes his visit to a remote gas station in the country.
Talks about the unique setup of the gas station with a barbecue stand, picnic tables, and a little diner.
Advises on how to navigate interactions at such busy places, comparing it to being a new inmate in prison.
Shares an unexpected and positive interaction he witnessed between three guys at the gas station.
Recounts their open-mindedness towards discussing LGBTQ+ issues, particularly around Miss Universe competition.
Expresses surprise and admiration for the guys' progressive views on acceptance and equality.
Questions society's ability to let people be themselves in this day and age.
Appreciates the guys' inclusive commentary as a win and a step in the right direction for the world.
Acknowledges the importance of recognizing and celebrating small victories for progress.
Concludes with a reflective message on valuing individuals beyond surface judgments and stereotypes.

Actions:

for all individuals,
Celebrate small victories for progress (implied)
Engage in open-minded and inclusive conversations (implied)
</details>
<details>
<summary>
2019-12-08: Let's talk about the smartest Republican Senators.... (<a href="https://youtube.com/watch?v=9aFvSldIoRk">watch</a> || <a href="/videos/2019/12/08/Lets_talk_about_the_smartest_Republican_Senators">transcript &amp; editable summary</a>)

Exploring the political implications of Republican senators potentially voting to impeach Trump, with a focus on voter turnout, party loyalty, and the long-term consequences for the GOP.

</summary>

"I think that they understand what happened in 2016."
"If you want to save the party, you've got to get rid of Trump."
"Removing Trump is actually the safest play for Republicans."
"It's gonna be easy to defeat somebody in a primary whose whole purpose in running is that you honored your oath."
"Long-term, the worst possible thing in the world for the Republican party is a Trump second term."

### AI summary (High error rate! Edit errors on video page)

Exploring the political implications of Republican senators considering voting to impeach and convict Trump.
Speculating on the potential consequences for these senators in terms of primary challenges and general elections.
Pointing out the impact of a single-issue voter base whose goal is to remove Trump from office.
Drawing parallels to the 2016 election and the potential effects on voter turnout based on the impeachment vote.
Arguing that removing Trump from the equation may actually benefit Republicans in the long run.
Warning about the dangers of a second term for Trump and the possible repercussions for the Republican Party.
Advocating for putting aside loyalty to the party in order to save it from potential long-term damage.

Actions:

for politically engaged citizens,
Mobilize voters to understand the long-term implications of supporting candidates who prioritize country over party (implied).
Engage in political discourse within your community to raise awareness about the potential impact of removing Trump from office (implied).
</details>
<details>
<summary>
2019-12-08: Let's talk about Rudy Giuliani's report (<a href="https://youtube.com/watch?v=Jz-IYX0GHWU">watch</a> || <a href="/videos/2019/12/08/Lets_talk_about_Rudy_Giuliani_s_report">transcript &amp; editable summary</a>)

Beau delves into Giuliani's report, advocates for accountability across political lines, and questions the timing of evidence presentation during trials.

</summary>

"But it does not absolve the administration of any of their alleged crimes, even if they were right. It's not how this works."
"Either what we believe is good and true is good and true, or we don't have any order. We don't have any laws."
"If Biden abused his office for personal gain, he should go down. And if you believe that, you have to believe the same is true for Trump."

### AI summary (High error rate! Edit errors on video page)

Beau sets the stage to talk about Giuliani's report and shares a hypothetical story about a cop convinced of someone's wrongdoing.
The cop, obsessively watching the person's house, discovers stolen property inside.
Both the cop and the person with stolen property get arrested after separate trials, illustrating accountability.
Beau expresses curiosity about Giuliani's report, wanting to see the evidence against the Bidens.
He mentions that if Biden abused his office, he should face consequences, but the same accountability should apply to Trump.
Beau stresses the importance of holding leaders accountable for any abuse of power, regardless of political affiliation.
He questions the timing of bringing up evidence during Biden's trial instead of the President's impeachment.
Beau argues that evidence should not be used to muddy the waters but to ensure accountability.
He concludes by urging for accountability for both Biden and Trump if wrongdoing is proven.

Actions:

for citizens, voters, activists,
Hold leaders accountable for abuses of power (implied)
Advocate for equal accountability across political affiliations (implied)
</details>
<details>
<summary>
2019-12-07: Let's talk with Howie Hawkins about the Green Party.... (<a href="https://youtube.com/watch?v=CxlGzHwE1dU">watch</a> || <a href="/videos/2019/12/07/Lets_talk_with_Howie_Hawkins_about_the_Green_Party">transcript &amp; editable summary</a>)

Howie Hawkins advocates for eco-socialist solutions, a Green New Deal by 2030, and grassroots organizing to build a major Green Party base, challenging the Democratic establishment and calling for electoral reform.

</summary>

"If we're gonna have a left and alternative in this country, we gotta have our own independent voice and our own independent power."
"There's no such thing as an unorganized socialist. You gotta get in an organization."
"Strength in numbers. When we're working together, we get our ideas tested."

### AI summary (High error rate! Edit errors on video page)

Introduction of Howie Hawkins, a Green Party candidate with a background in social movements and environmental activism.
Howie Hawkins shares his history of being a teamster, his involvement in forming the Green Party, and his previous statewide campaigns.
Emphasis on building the Green Party as a major party and advocating for eco-socialist solutions to pressing issues like climate change, inequality, and nuclear disarmament.
Howie Hawkins details his Green New Deal proposal, contrasting it with the Democrats' version and stressing the urgency to address climate change by 2030.
Importance of third-party representation in elections to push progressive agendas and hold major parties accountable.
Howie Hawkins explains the dangers of the current nuclear arms race, advocating for disarmament and referencing past treaties.
Addressing job concerns in the transition to a green economy, ensuring a just transition and providing millions of jobs.
Expansion on the Economic Bill of Rights proposed by the Green Party, including job guarantees, income above poverty, affordable housing, universal rent control, and healthcare for all.
Advocacy for open borders and ending mass surveillance, supporting whistleblowers like Edward Snowden.
The need for grassroots organizing to bridge divides among working-class individuals and build a mass party for working people.
Focus on electoral reform, advocating for a national popular vote with ranked-choice voting to eliminate the Electoral College.
Challenges faced by progressive Democrats and the potential for a Green Party to attract disenchanted voters, particularly non-voters.

Actions:

for progressive activists, third-party supporters,
Contact the Howie Hawkins campaign via howiehawkins.us to volunteer, donate, and help with ballot access efforts (suggested).
Host house parties for grassroots fundraising and awareness about Howie Hawkins' campaign (suggested).
Engage in electioneering canvassing activities to reach out to potential supporters and collect information for future engagement (suggested).
</details>
<details>
<summary>
2019-12-06: Let's talk about white vans and legends.... (<a href="https://youtube.com/watch?v=Ck2iZM3stFg">watch</a> || <a href="/videos/2019/12/06/Lets_talk_about_white_vans_and_legends">transcript &amp; editable summary</a>)

Be more aware of the lessons behind viral stories, as even if the story is fake, the lesson is real and valuable.

</summary>

"Most of these stories, yeah they're not true, but the lesson is, the lesson's real and most of them are a positive benefit to society."
"Somewhere not just did we lose the ability to tell facts from fiction, we lost the ability to learn from fiction, and that's sad."
"So when you see one of these stories, one of these viral stories on Facebook or whatever, don't really think about the story. Think about the lesson."

### AI summary (High error rate! Edit errors on video page)

Addresses the urban legend about white vans in the United States where people with white vans abduct others for organs.
Mentions that the story originated in Baltimore and caused panic, prompting the Baltimore PD to issue a statement debunking it.
Emphasizes that the core of such stories is often about increasing situational awareness and being cautious.
Points out the importance of being aware of your surroundings, especially when vulnerable like when putting a child in a car seat.
Gives an example of a firefighter spreading a false meme about smoke detectors to encourage people to replace batteries for safety.
Expresses that while many of these viral stories are untrue, they often carry real lessons that can benefit society.
Criticizes the loss of the ability to distinguish facts from fiction and the missed opportunities to learn from fictional stories.
Encourages looking beyond the story itself to find the lesson embedded within most viral tales.
Concludes by urging viewers to focus on the lessons rather than the sensationalism of viral stories on social media.

Actions:

for internet users,
Verify information before spreading it on social media (implied)
Be cautious and aware of your surroundings, especially in vulnerable situations like parking lots (implied)
</details>
<details>
<summary>
2019-12-06: Let's talk about UPS trucks.... (<a href="https://youtube.com/watch?v=o-DYbOGMVBI">watch</a> || <a href="/videos/2019/12/06/Lets_talk_about_UPS_trucks">transcript &amp; editable summary</a>)

Miami UPS truck pursuit raises questions about law enforcement tactics and the importance of preserving human life over property.

</summary>

"Speed, surprise, and violence of action."
"The primary objective is the preservation of human life."
"Planning, logistics, and training failed here massively."

### AI summary (High error rate! Edit errors on video page)

UPS truck incident prompts Beau to address unusual events in Miami involving law enforcement.
Miami cops have a reputation, but not for tactical incompetence.
Pursuit of UPS truck ends tragically with suspects, driver, and bystander killed.
UPS trucks are equipped with sensors for tracking and monitoring.
Tactical approach to the situation raises questions about law enforcement actions.
Primary objective in such situations should be preserving human lives.
Law enforcement's use of bystander vehicles as cover raises ethical concerns.
Ineffective and inaccurate fire directed at the vehicle during the incident.
Critique on the failure of planning, logistics, and training in the UPS truck pursuit.
Beau concludes with reflections on the need for better preparedness and training in law enforcement.

Actions:

for law enforcement officials,
Reassess training and tactics in law enforcement (implied)
</details>
<details>
<summary>
2019-12-05: Let's talk about truth and consequences, Trump edition.... (<a href="https://youtube.com/watch?v=ob5Hhbn4oVs">watch</a> || <a href="/videos/2019/12/05/Lets_talk_about_truth_and_consequences_Trump_edition">transcript &amp; editable summary</a>)

President Trump considers sending 14,000 troops to the Middle East, prioritizing election strategy over the well-being of troops and allies.

</summary>

"He cares about power. He doesn't care about the troops."
"The cost of believing that is going to be paid by them."
"When you don't see them at the gas station for a while, you're going to know why."

### AI summary (High error rate! Edit errors on video page)

President Trump is considering sending an additional 14,000 troops to the Middle East to prepare for conflict with Iran.
The decision stems from the need to energize his supporters for the election season.
The consequences of discounting foreign policy experts and abandoning allies, like the Kurds, are coming to light.
Iran, similar to other countries in the region, has a Kurdish population historically friendly to the U.S.
The president's focus appears to be on power and re-election rather than the well-being of troops.
The move towards conflict seems driven by political motives rather than genuine concern.
Trusting a businessman over generals may have severe consequences for American troops.
The cost of prioritizing political gain over alliances and military expertise may result in significant losses.

Actions:

for voters, activists, military families,
Reach out to elected officials to voice opposition to escalating conflicts (implied)
</details>
<details>
<summary>
2019-12-05: Let's talk about tomorrow, December 6th.... (<a href="https://youtube.com/watch?v=c9XbsR7x7CY">watch</a> || <a href="/videos/2019/12/05/Lets_talk_about_tomorrow_December_6th">transcript &amp; editable summary</a>)

December 6th should be a national holiday, marking the end of a dark American institution, but its significance is not appropriately commemorated.

</summary>

"December 6th should be a national holiday, marking the end of a dark American institution."
"The Emancipation Proclamation did not end the institution; it only freed slaves in conflict zones."
"The day marks the closing of a dark chapter in American history, yet it is not appropriately commemorated."
"People who defended slavery until the end are remembered for their stance."
"Current events may lead to individuals being similarly remembered for supporting indefensible causes."

### AI summary (High error rate! Edit errors on video page)

December 6th should be a national holiday, marking the end of a dark American institution.
The Emancipation Proclamation did not end the institution; it only freed slaves in conflict zones.
Lincoln's strategic decision as commander in chief aimed to destabilize the South.
The 13th Amendment was necessary to prevent the resurgence of the institution after the war.
Lincoln pushed for the 13th Amendment, which was ratified on December 6th, 1865.
Some states were slow to ratify the amendment, with Kentucky and Mississippi waiting until 1976 and 1995, respectively.
The day marks the closing of a dark chapter in American history, yet it is not appropriately commemorated.
It took another hundred years for further steps towards equality after the 13th Amendment.
People who defended slavery until the end are remembered for their stance.
Current events may lead to individuals being similarly remembered for supporting indefensible causes.

Actions:

for history enthusiasts, activists,
Advocate for December 6th to be recognized as a national holiday (suggested)
Educate others about the importance of the 13th Amendment and its significance in American history (suggested)
</details>
<details>
<summary>
2019-12-04: Let's talk about Fred Hampton's legacy after 50 years.... (<a href="https://youtube.com/watch?v=3aK7hY_67Ok">watch</a> || <a href="/videos/2019/12/04/Lets_talk_about_Fred_Hampton_s_legacy_after_50_years">transcript &amp; editable summary</a>)

50 years ago, an incident in Chicago altered history, revealing systemic resistance to change but also the unstoppable power of ideas.

</summary>

"Nobody can tell you this is what happened that day."
"When an idea's time has come, nothing will stop it."

### AI summary (High error rate! Edit errors on video page)

Recalls an incident from 50 years ago at 2337 West Monroe Street in Chicago that altered American history.
Mentions an up-and-coming activist named Fred Hampton who could have created significant social change.
Expresses a belief that the incident was a deliberate effort to destabilize and remove black leadership in the United States.
Encourages people to look into the event themselves and analyze all the evidence.
Suggests two possible conclusions: isolated bad actions or a targeted effort against black leadership.
Believes the system lashed out against those seeking change, showing systemic resistance to change.
Despite the tragic event, notes that it led to a massive outcry and political changes.
Points out the election impact following the incident, leading to Chicago's first black mayor.
Draws a connection from the past events to the election of President Obama.
Emphasizes that when the time is right, no system can stop an idea from manifesting.

Actions:

for activists, historians, social change advocates,
Research and learn about historical events like the one mentioned (suggested)
Get involved in local politics and elections to create change (implied)
</details>
<details>
<summary>
2019-12-03: Let's talk about logical fallacies.... (<a href="https://youtube.com/watch?v=M2WKnNkrCdI">watch</a> || <a href="/videos/2019/12/03/Lets_talk_about_logical_fallacies">transcript &amp; editable summary</a>)

Beau gives a crash course on logical fallacies through real-life stories, urging for better aircraft maintenance and critical thinking in debates.

</summary>

"We really need to pay our aircraft maintenance crews more."
"Just because one of these fallacies was used does not mean the argument is wrong."
"The middle ground is not always where you want to end up."
"The world is a bad place sometimes."
"It's just a thought y'all have a good Good night."

### AI summary (High error rate! Edit errors on video page)

Tells a true story about a trip where several planes had issues which leads to the point of needing to pay aircraft maintenance crews more.
Mentions misleading vividness, where a detailed story doesn't actually support the claim being made.
Explains logical fallacies including appealed hypocrisy, straw man, genetic fallacy, appeal to belief, appeal to tradition, appeal to authority, ad hominem, argument from ignorance, cherry-picking data, slippery slope, Nirvana Fallacy, moving the goalpost, false dilemma, fallacy of composition, divine fallacy, argument to moderation, and argument from fallacy.
Provides examples for each logical fallacy and explains how they can lead to flawed arguments.
Emphasizes the importance of understanding logical fallacies especially in the context of debates and arguments.
Ends by suggesting that the information on logical fallacies will be valuable in the coming year.

Actions:

for debaters and critical thinkers.,
Learn about logical fallacies and how to identify them in arguments (suggested).
Educate others on common logical fallacies and how they can weaken arguments (suggested).
</details>
<details>
<summary>
2019-12-02: Let's talk about interviewing Presidential candidates.... (<a href="https://youtube.com/watch?v=bQqZBzBksVA">watch</a> || <a href="/videos/2019/12/02/Lets_talk_about_interviewing_Presidential_candidates">transcript &amp; editable summary</a>)

Beau plans to interview presidential candidates to understand their support, preferring a conversational approach to reveal candidates' true selves, ultimately questioning the concentration of power in political office.

</summary>

"You should not be trusted with that much power."
"It's almost as if nobody should be trusted with that much power."
"No big mystery there."
"The office has too much."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Planning to interview presidential candidates to understand their base of support.
Interested in learning who the candidates are and why they have support.
Prefers a conversational approach over hardball questions to get to the truth.
Believes that keeping candidates talking will eventually reveal their true selves.
Values offhand comments over prepared sound bites for genuine insights.
Acknowledges valid reasons to disqualify every candidate from holding office.
Questions the concentration of power in the hands of political officeholders.
Leaves viewers with a thought-provoking reflection on trust and power.

Actions:

for viewers,
Reach out to neighbors to understand their support for political candidates (implied)
Engage in meaningful, conversational dialogues to uncover truths (implied)
Question the concentration of power in political office and its implications (implied)
</details>
<details>
<summary>
2019-12-02: Let's talk about genius, robots, futures, and impeachment.... (<a href="https://youtube.com/watch?v=Eb2krF7Ydho">watch</a> || <a href="/videos/2019/12/02/Lets_talk_about_genius_robots_futures_and_impeachment">transcript &amp; editable summary</a>)

Beau talks about Isaac Asimov's genius, his accurate predictions, the importance of holding leaders accountable, and makes a chilling prediction for the future.

</summary>

"He saw all this coming."
"If we don't start holding those in the White House accountable 50 years from now, we won't be talking about impeachment because we won't have a presidency."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic of discussing a genius.
Talks about Isaac Asimov, a genius known for science fiction.
Mentions that Asimov made predictions in 1964 about life in 2014, some of which have come true.
Talks about Asimov's interest in history and politics, particularly Watergate.
Criticizes the idea of excusing criminal actions based on historical precedents.
Calls for holding leaders accountable.
Mentions World AIDS Day and Asimov's death due to AIDS on April 6, 1992.
Makes a prediction about the future if leaders are not held accountable.

Actions:

for citizens, voters, activists.,
Hold leaders accountable (implied).
Advocate for transparency and accountability in government (suggested).
</details>
<details>
<summary>
2019-12-01: Let's talk with Marianne Williamson.... (<a href="https://youtube.com/watch?v=OXlXGl7_BmQ">watch</a> || <a href="/videos/2019/12/01/Lets_talk_with_Marianne_Williamson">transcript &amp; editable summary</a>)

Marianne Williamson advocates for reparations, peace-building efforts, and a nonviolent political revolution to address historical wrongs and corporate influence on government policies.

</summary>

"Purify your own heart."
"What reparations do is they carry an inherent mea culpa, an inherent acknowledgement of the historical wrong that was done."
"We need a nonviolent political revolution in this country."

### AI summary (High error rate! Edit errors on video page)

Introduces Marianne Williamson, known for being a self-help guru and running for president.
Marianne talks about her background, career, and involvement during the AIDS epidemic.
Describes the despair and love experienced during the AIDS crisis.
Marianne founded organizations to provide support services to people living with HIV.
Shares the story of Project Angel Food, delivering meals to homebound people with AIDS.
Connects her work during the AIDS crisis to her current political campaign.
Advocates for harnessing goodness and decency for political purposes.
Talks about the need for a Department of Peace to focus on peace-building efforts.
Proposes a Department of Children and Youth to address vulnerabilities and challenges faced by American children.
Advocates for reparations as a way to address historical wrongs and economic disparities.
Calls for purifying hearts and promoting forgiveness and respect for differing opinions.
Addresses the influence of corporate interests on government policies and the need for a nonviolent political revolution.

Actions:

for activists, advocates, voters,
Support organizations providing non-medical services to those living with HIV (exemplified).
Advocate for the establishment of a Department of Peace and a Department of Children and Youth (suggested).
Join movements advocating for reparations to address economic disparities and historical wrongs (implied).
Educate oneself and others on the impact of corporate influence on government policies (exemplified).
</details>
<details>
<summary>
2019-12-01: Let's talk about opening a can and the future.... (<a href="https://youtube.com/watch?v=kpgsAJjMiu0">watch</a> || <a href="/videos/2019/12/01/Lets_talk_about_opening_a_can_and_the_future">transcript &amp; editable summary</a>)

Beau challenges the notion that young people can't be trusted to vote based on their ability to use a can opener, advocating for a shift in perspective towards youth resilience and capability.

</summary>

"Can't trust young people to vote."
"Don't listen to those young people who did not give up even though they didn't understand something."
"Y'all should lay off the kids because at the end of the day whether or not they open that can doesn't matter they'll figure it out."
"Most people today have an automatic can opener and even then most of the brands you don't need it anymore."
"Let's listen to the people who will laugh and record a video because they don't understand what they did."

### AI summary (High error rate! Edit errors on video page)

Beau introduces a video of a young man struggling to use a can opener during Thanksgiving dinner, sparking a tutorial on can opener usage.
The accompanying message suggests young people can't be trusted to vote because they struggle with basic tasks like using a can opener.
Beau showcases various pop-top cans, indicating the prevalence of can designs that don't require a traditional can opener.
He demonstrates how to use a can opener, expressing disbelief at the notion that young people can't figure it out.
Beau criticizes the message that undermines young voters, pointing out the hypocrisy of older generations who mock youth for not knowing certain skills.
Despite the ridicule, the young man in the video perseveres, in contrast to the elders who filmed and laughed at him.
Beau questions why there's a tendency to blame young people for their supposed incompetence rather than acknowledging their strengths and resilience.
He challenges the idea that older generations are inherently wiser and more capable of making sound decisions for the future.
Beau suggests that young people, despite facing ridicule, will figure things out on their own while being undermined by those who are supposed to guide them.
The message conveyed is to not underestimate young people's abilities based on isolated incidents of struggle or ignorance.

Actions:

for young voters,
Support and encourage young voters (implied)
Challenge stereotypes and biases against young people (implied)
</details>

## November
<details>
<summary>
2019-11-30: Let's talk about that new bill in Ohio.... (<a href="https://youtube.com/watch?v=NO5VVA0ElB8">watch</a> || <a href="/videos/2019/11/30/Lets_talk_about_that_new_bill_in_Ohio">transcript &amp; editable summary</a>)

Legislators in Ohio propose a controversial bill allowing doctors to remove ectopic pregnancies, facing criticism for ignorance and arrogance, urging public action to oppose such dangerous legislation.

</summary>

"It's a combination of arrogance and ignorance, otherwise this bill never would have existed."
"You cannot let this stand."
"This is everything that is wrong with America's leadership."
"They know nothing of the subject matter and incidentally, this is the second time it's come up."
"They were willing to put you at risk of execution without even reading the bill."

### AI summary (High error rate! Edit errors on video page)

Legislators in Ohio are proposing a bill that suggests doctors should remove ectopic pregnancies from the fallopian tubes and implant them in the uterus.
The bill implies that doctors who don't comply can be arrested for a crime called aggravated abortion murder, punishable by death.
Beau criticizes the bill as legislating medicine without a license, labeling it as aggravated arrogance murder due to ignorance and arrogance.
Despite Ohio facing hunger issues affecting 500,000 kids, legislators are focusing on controversial bills like this one.
The bill has garnered support from 21 sponsors in the House of Representatives in Ohio.
Beau questions the sponsors' understanding of the bill's implications, especially since it carries the death penalty.
He calls for action against the sponsors, urging people to campaign against them regardless of political affiliations.
Beau expresses disbelief at the lack of knowledge displayed by the sponsors regarding the bill's subject matter.
He condemns the level of arrogance and ignorance displayed by the legislators willing to pass such a bill.
Beau stresses the importance of removing such individuals from positions of power to enact positive change in the state.

Actions:

for ohio residents, activists,
Campaign against the sponsors of the bill in Ohio (suggested)
Advocate for informed and compassionate legislation (implied)
</details>
<details>
<summary>
2019-11-29: Let's talk about Trump's speech and a 200-year-old concept.... (<a href="https://youtube.com/watch?v=_Nhlu4IAlIc">watch</a> || <a href="/videos/2019/11/29/Lets_talk_about_Trump_s_speech_and_a_200-year-old_concept">transcript &amp; editable summary</a>)

Beau explains the importance of civilian control over the military, cautioning against blurring the line and potential war crimes.

</summary>

"The reason it exists is twofold."
"He's a civilian, he's not a general, period, full stop."
"When the head of state starts to be seen as the general, the next thing that happens is war crimes."
"Keeping Syria's oil, that's a war crime."
"It's illegal under the Geneva Conventions and the U.S. War Crimes Act."

### AI summary (High error rate! Edit errors on video page)

Reverse format for this historical event.
Hanging out with veterans and ex-contractors.
Watching Trump's speech to troops in Afghanistan.
Trump falsely claiming they kept oil in Syria.
Beau not fact-checking, focusing on civilian control.
Emphasizing civilian leadership over the military.
President as a civilian, not a rank like general.
Concept of civilian control of the military.
President's power for strategic decisions towards peace.
Importance of preventing military dictatorship.
Historic taboo on presidents saluting troops.
Reagan starting the tradition of saluting troops.
Concerns about blurring the line between civilian and military leadership.
Keeping Syria's oil as a potential war crime.
Importance of Pentagon's integrity in addressing such issues.

Actions:

for citizens, policymakers, military personnel.,
Ensure awareness and understanding of the concept of civilian control over the military (suggested).
Advocate for integrity within the Pentagon to uphold international and U.S. laws regarding war crimes (exemplified).
</details>
<details>
<summary>
2019-11-28: Let's talk about a message to parents about bonding.... (<a href="https://youtube.com/watch?v=4ir46_N23ls">watch</a> || <a href="/videos/2019/11/28/Lets_talk_about_a_message_to_parents_about_bonding">transcript &amp; editable summary</a>)

A reflection on parenting, violence, and preparing for the future in a changing world.

</summary>

"I don't want to bond over violence if it's really bonding."
"Cooperation, creation, building, that's what they're going to have to struggle to learn."
"The next war for humanity's survival is not going to be fought by soldiers or sailors and pilots."

### AI summary (High error rate! Edit errors on video page)

A single mother's message and a sports player taking his boys to the range sparked the topic.
Criticism arose regarding the reasons for taking the boys, things said, and teaching techniques.
Despite the criticism, bonding experiences between parents and children were brought up.
Beau shared his childhood experience of learning about military tactics and small arms from his father.
He chose not to teach his son about violence but instead sent him elsewhere for unarmed defense lessons.
Beau values memories of childhood involving cooperation, creation, and building over destruction.
He emphasized the importance of teaching children cooperation and creation instead of violence.
Beau discussed the quote about preparing for the next war rather than focusing on the last war.
He stressed the need for future generations to study fields like mathematics, philosophy, and engineering.
Beau emphasized that the next war for humanity's survival will require engineers, architects, and philosophers.

Actions:

for parents, educators, community leaders,
Teach children cooperation and creation through hands-on activities (exemplified)
Encourage children to study fields like mathematics, philosophy, engineering (exemplified)
</details>
<details>
<summary>
2019-11-26: We talked about emergency preparedness.... (<a href="https://youtube.com/watch?v=SCtLFnnyHEE">watch</a> || <a href="/videos/2019/11/26/We_talked_about_emergency_preparedness">transcript &amp; editable summary</a>)

Be prepared for disasters, be self-reliant, teach survival skills, and change how you see the world - Beau covers essentials from emergency readiness to mindset shifts.

</summary>

"We're not the sons of the pioneers and the daughters of Rosie the Riveter anymore."
"It's up to you. If not you, who? Who's gonna solve the problems if it's not you?"
"Just take care of this stuff and you're going to be able to help people."
"When people start to learn survival skills, they start to look at the world in a very, very different way."
"Supplies are pretty much everywhere. You just have to know where to look and how to think about them."

### AI summary (High error rate! Edit errors on video page)

Urges everyone to be prepared for natural disasters due to delayed government response.
Criticizes the inefficiency of federal agencies in disaster response.
Encourages families to create emergency kits with essentials like water, food, and first aid supplies.
Emphasizes the importance of being self-reliant and having a plan for evacuation.
Advocates for teaching survival skills to children in a fun and engaging way.
Shares examples of people overlooking readily available supplies during emergencies.
Stresses the value of improvisation and critical thinking learned through survival skills.
Encourages seeing the world differently by recognizing possibilities and the importance of change.
Concludes by underlining the significance of learning survival skills for the youth.

Actions:

for families, communities,
Prepare an emergency kit with essentials like water, food, first aid supplies (suggested)
Teach children survival skills in a fun and engaging way (suggested)
Encourage critical thinking and improvisation skills through survival training (suggested)
Practice looking at components as improvisational tools (implied)
</details>
<details>
<summary>
2019-11-26: Let's talk about a story you can tell on Thanksgiving.... (<a href="https://youtube.com/watch?v=igktJc1ufM4">watch</a> || <a href="/videos/2019/11/26/Lets_talk_about_a_story_you_can_tell_on_Thanksgiving">transcript &amp; editable summary</a>)

Beau narrates the inspiring tale of Nikolai Vavilov and Leningrad scientists, embodying sacrifice and dedication for the greater good, tying the story seamlessly to the essence of Thanksgiving.

</summary>

"When you really think about it, people that dedicated to helping others, they literally just waste away right next to food."
"The idea of Thanksgiving, being thankful for food, I think it fits."

### AI summary (High error rate! Edit errors on video page)

Tells a story fit for Thanksgiving with themes of poverty, sacrifice, duty, political conflict, and environmental consciousness.
Narrates the tale of Nikolai Vavilov, a man from a rural town dedicated to ending famine by gathering seeds worldwide.
Vavilov's pursuit led to the creation of the first seed bank in the Soviet Union.
Despite his contributions, Vavilov was denounced by a rival with Stalin's favor and sent to the gulags, where he died of starvation.
During World War II, scientists in Leningrad protected the seed bank through a siege, sacrificing their lives by not consuming the stored food.
The dedication of these scientists ensured the preservation of vital crop samples for future generations.
The seeds from this bank have influenced the food on our tables, connecting us to this impactful story of sacrifice and dedication.
The narrative showcases individuals who prioritize the greater good over their own survival, symbolizing the essence of Thanksgiving.
Beau leaves listeners with a reflection on the significance of being thankful for food and the enduring legacy of those who work to address global challenges.

Actions:

for history enthusiasts, environmentalists, storytellers,
Preserve local plant varieties and seeds to protect biodiversity (implied)
Support seed banks or initiatives that focus on crop preservation (implied)
</details>
<details>
<summary>
2019-11-25: Let's talk about what talking robots can tell us about ourselves.... (<a href="https://youtube.com/watch?v=jo13NLjIEZM">watch</a> || <a href="/videos/2019/11/25/Lets_talk_about_what_talking_robots_can_tell_us_about_ourselves">transcript &amp; editable summary</a>)

Beau from Carnegie Mellon shows how a robot's words affect gameplay, urging us to understand the power of our own words, especially towards children and online.

</summary>

"Words have a lot of power."
"You have a choice in what you put out into the world."
"Your words will have an effect on those people that read them."
"Even if you think it's just a harmless comment..."
"The reality is that's a real person."

### AI summary (High error rate! Edit errors on video page)

Talks about a study from Carnegie Mellon University where people played a game against a robot named Pepper.
Some players were encouraged by the robot while others were insulted.
Players who were encouraged performed better, while those who were insulted played worse.
Even though the players knew it was a robot and programmed to act that way, it affected their emotions and gameplay.
AI researchers find this interaction between robots and humans extremely interesting.
Beau raises the point that if robots can impact adults like this, the power of words on children must be considered.
He mentions the influence of internet comments and how they can affect individuals more than they realize.
Beau urges viewers to understand the impact their comments can have on others, as they are real people reading them.
He stresses the importance of being mindful of the words we put out into the world.
Beau concludes by reminding viewers that their words, even seemingly harmless ones, can have a significant effect on others.

Actions:

for online users,
Choose words carefully (implied)
Be mindful of comments' impact (implied)
</details>
<details>
<summary>
2019-11-25: Let's talk about the Secretary of the Navy, tweets, and consequences.... (<a href="https://youtube.com/watch?v=NfwTPc_mKnw">watch</a> || <a href="/videos/2019/11/25/Lets_talk_about_the_Secretary_of_the_Navy_tweets_and_consequences">transcript &amp; editable summary</a>)

Secretary of Navy resigns, President interferes in military proceedings, posing with a corpse deemed acceptable - damaging moral compass and counterinsurgency efforts.

</summary>

"One guy has done more damage to this country's moral compass than any other president in history."
"The standard has been set. This isn't a big deal."
"You better fight harder."
"Changed the way you looked at things."
"We lowered the bar."

### AI summary (High error rate! Edit errors on video page)

Secretary of Navy's resignation due to discrepancy between public statements and private actions.
Administration prioritizes truthfulness and transparency, sarcastically mentioning their stellar reputation.
Secretary of Navy's refusal to obey an order believed to violate his oath to support and defend the Constitution.
Admirals' concern about good order and discipline after the President interfered in military proceedings.
President's tweet expressing dissatisfaction with Navy Seal Eddie Gallagher's trial outcome.
Gallagher was acquitted on major charges but found to have posed with an enemy prisoner's corpse.
Beau questions the seriousness of posing with a corpse, explaining its impact on propaganda for the opposition.
Beau stresses the repercussions of such actions on local populace and counterinsurgency efforts.
Concerns raised about the President's orders being delivered via tweet to a wide audience.
Beau criticizes the lowering of moral standards and its implications on military operations.
Beau underlines the psychological impact of such actions on public perception and counterinsurgency success.
The lasting effects of setting a standard where posing with a corpse is considered acceptable.
Beau points out the significant damage done to the country's moral compass by such actions.

Actions:

for military personnel, policymakers, activists,
Challenge and question actions that compromise moral standards (exemplified).
Advocate for transparent and ethical leadership in the military (exemplified).
Educate others on the psychological impact of actions on public perception (implied).
</details>
<details>
<summary>
2019-11-25: Let's talk about a new vehicle for societal change on YouTube.... (<a href="https://youtube.com/watch?v=kkxPjpYFgLM">watch</a> || <a href="/videos/2019/11/25/Lets_talk_about_a_new_vehicle_for_societal_change_on_YouTube">transcript &amp; editable summary</a>)

The FTC's enforcement of a law on childhood privacy affects kids' content on YouTube, urging major creators to utilize their influence for societal change.

</summary>

"They can influence change, but they're probably not going to."
"If you have that appearance or that voice that would appeal to children, you want to get involved, this is something you can do."
"The driver's door just opened. You can really get somewhere with this."
"You can literally raise the children that don't have the influence from the parents they need."
"I think you can create a better world with a camera and a YouTube channel."

### AI summary (High error rate! Edit errors on video page)

The FTC is enforcing a 1998 law on childhood privacy, affecting kids' content on YouTube.
Creators of kids' content may see a drop in income due to the removal of targeted ads.
Kid-directed content involves high production costs with visual effects, sound effects, travel, location shooting, and toy unboxing.
Major kid content creators have the potential to influence change by educating their viewers about laws.
There is a sense of learned helplessness among people when it comes to government actions.
Despite having millions of subscribers, major creators may not utilize their influence for change.
Starting a kids' content YouTube channel with low overhead costs could be profitable as YouTube is profit-driven.
Creating content like story time teaching children with morals can be financially viable with minimal overhead.
Beau suggests creating content that is more educational like Mr. Rogers instead of traditional cartoons.
Using a YouTube channel for societal change is emphasized by Beau, encouraging those interested to get involved.

Actions:

for content creators,
Start a kids' content YouTube channel with low overhead costs to adapt to the changes in kid-directed content (suggested)
Create educational content like story time teaching children with morals to make an impact (implied)
Use your appearance and voice to appeal to children and get involved in creating content for societal change (implied)
</details>
<details>
<summary>
2019-11-24: Let's talk about a Thanksgiving message from a Native leader.... (<a href="https://youtube.com/watch?v=xrGl0lOFzzY">watch</a> || <a href="/videos/2019/11/24/Lets_talk_about_a_Thanksgiving_message_from_a_Native_leader">transcript &amp; editable summary</a>)

Beau expresses gratitude, encourages kindness and generosity, and longs for freedom, reflecting on Thanksgiving and indigenous experiences in America.

</summary>

"If you see someone hurting and in need of a kind word or two, be that person who steps forward and lends a hand."
"Thank you for listening to whomever is voicing my words."
"There isn't a minute in any day that passes without me hoping that this will be the day I will be granted freedom."
"I long for the day when I can smell clean, fresh air, witness the clouds as their movement hides the sun."
"Thank you for continuing to support and believe in me."

### AI summary (High error rate! Edit errors on video page)

Reading a heartfelt message reflecting on Thanksgiving and the indigenous experience in America.
Expressing gratitude for those fighting for environmental protection and indigenous rights.
Encouraging generosity towards those in need and speaking out against injustice.
Longing for freedom and a return to nature.
Thanking supporters and hoping for a day of liberation.
Acknowledging the importance of remembering ancestors' sacrifices.
Describing a vision of freedom, fresh air, and connection to nature.
Emphasizing the need to support indigenous communities.
Urging listeners to be kind, generous, and brave in the face of injustice.
Ending with a message of gratitude and hope for freedom.

Actions:

for supporters of indigenous rights,
Support indigenous communities by donating resources or volunteering to help (implied)
Speak up against injustice and confront it bravely in your community (implied)
Extend kindness and generosity to those in need by offering help or resources (implied)
</details>
<details>
<summary>
2019-11-23: Let's talk about the three most exciting sounds in the world.... (<a href="https://youtube.com/watch?v=SwkdHcm19Iw">watch</a> || <a href="/videos/2019/11/23/Lets_talk_about_the_three_most_exciting_sounds_in_the_world">transcript &amp; editable summary</a>)

Beau talks about the importance of exploring and experiencing one's community to broaden the mind and combat societal detachment.

</summary>

"Those sounds signal that adventure is coming, travel is coming, we're going to see something new."
"It might be time to visit your own community and experience it rather than just see it."

### AI summary (High error rate! Edit errors on video page)

Beau talks about the exciting sounds that signal adventure like the whistle of a train, an anchor chain, and an airplane engine.
He mentions how in the internet age, people can see everything online, losing the desire for adventure and contact with strangers in strange places.
Americans travel abroad and leave their state very little, missing out on experiencing their own communities.
Beau encourages exploring one's community, even if it's close by, to see something worth experiencing.
He points out that travel broadens the mind not just by moving from one place to another but by being present in a different community.
Beau suggests that being part of a community and experiencing it can have a significant impact, especially in today's detached society.
Taking kids to places like fairs or zoos can be mundane for adults but a fascinating experience for children as they interact with new things.
Amid the division portrayed in the news, Beau believes it's vital to realize that people aren't as divided and hostile towards each other as it may seem.
He hints at the importance of reconnecting with one's community, appreciating the simple sounds and experiences around us.

Actions:

for community members,
Visit a local landmark or special place in your community (implied)
Take your kids to new places like fairs or zoos to let them interact with different things (implied)
</details>
<details>
<summary>
2019-11-23: Let's talk about the impeachment's mid-season plot twist.... (<a href="https://youtube.com/watch?v=5J5Prg2bV98">watch</a> || <a href="/videos/2019/11/23/Lets_talk_about_the_impeachment_s_mid-season_plot_twist">transcript &amp; editable summary</a>)

Beau gives an overview of the impeachment episode, pointing out twists and suggesting granting immunity to a key Ukrainian figure for testimony amid corruption concerns.

</summary>

"I didn't have this on my impeachment bingo card."
"I gotta know how this season ends."
"I'd rather have this Ukrainian sitting in the Oval Office than the President."
"It's not just a symptom of the Republican Party, it's a symptom of us not valuing substance, not valuing leadership."
"I'm hooked on this show."

### AI summary (High error rate! Edit errors on video page)

Excited to share about the impeachment episode and the unexpected plot twists.
Overview of the alleged attempts by the President to extort Ukraine into investigating Hunter Biden.
Mention of Ukraine as a strategic partner for the United States.
Description of Nunez as the President's biggest defender during the impeachment inquiry.
Details about Ukrainians tied to Rudy Giuliani and Fraud Guarantee getting arrested for campaign finance issues.
Reference to evidence implicating Nunez from the beginning and the House Democratic Coalition filing an ethics complaint.
Mention of emails linking Giuliani to the White House and Secretary of State Pompeo in efforts to smear the ambassador in Ukraine.
Suggestion to grant immunity to a Ukrainian involved and have him testify publicly.
Criticism of corruption within the administration and lack of trust in impeachment proceedings.
Reflection on valuing substance and leadership in politics.

Actions:

for politically engaged individuals,
Grant full immunity to the Ukrainian figure implicated in the events for public testimony (suggested).
Keep a close eye on the individual involved (suggested).
</details>
<details>
<summary>
2019-11-23: Let's talk about a statistic out of California and blue-collar skills.... (<a href="https://youtube.com/watch?v=aZXIDiiZQ6U">watch</a> || <a href="/videos/2019/11/23/Lets_talk_about_a_statistic_out_of_California_and_blue-collar_skills">transcript &amp; editable summary</a>)

Beau reveals the significant trend of homemade firearms in California, underscoring the need to address root causes rather than focus on bans.

</summary>

"Now we do because the information is out."
"It tells us a lot and it tells us that this is something that has to be taken into consideration anytime legislation is proposed."
"So this shows us again that we have to address root causes."
"People in the United States have way too much of a desire to own a firearm."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Talks about statistics from California regarding homemade firearms.
Mentions the significance of the data but notes that it's often overlooked.
Raises the issue of individuals building firearms at home to bypass bans.
Mentions the ease of manufacturing firearms at home, referencing the ATF report.
Points out that 30% of firearms in California are homemade, indicating a significant trend.
Notes that homemade firearms are used to circumvent California's restrictions.
Emphasizes that manufacturing firearms at home is easier than dealing with neighboring states' gun laws.
States that a variety of firearms, from pistols to AKs, are being produced at home.
Explains the process of making firearms at home, focusing on manufacturing the receiver.
Stresses the importance of addressing root causes rather than focusing on banning specific parts.

Actions:

for legislators, gun control advocates,
Advocate for comprehensive legislation addressing root causes (suggested)
Raise awareness about the implications of homemade firearms (implied)
</details>
<details>
<summary>
2019-11-22: Let's talk about what China can teach us about the Ukraine narrative.... (<a href="https://youtube.com/watch?v=6ookSSqyC1c">watch</a> || <a href="/videos/2019/11/22/Lets_talk_about_what_China_can_teach_us_about_the_Ukraine_narrative">transcript &amp; editable summary</a>)

Beau explains the debunked Ukraine election interference theory by comparing it to a historic fake story, showcasing how misinformation spreads without verification, impacting public trust.

</summary>

"Ideas travel faster than bullets."
"How could they be this dumb?"
"It doesn't even make sense, but it got repeated just like this story."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of Ukraine interfering in the 2016 election.
Draws a parallel to a fabricated story about tearing down the Great Wall of China in 1899.
Mentions a made-up person, Frank C. Lewis, and how journalists created a false narrative.
Talks about how the fake Chinese story spread and led to the manufacturing of evidence.
Debunks the conspiracy theory about Ukraine's interference in the election.
Points out that CrowdStrike, a Ukrainian company, was falsely accused, though it's American.
Criticizes the spread of misinformation by right-wing news outlets.
Compares the repetition of false stories to how misinformation spreads.
Raises concerns about the impact of repeated lies on public perception and trust.

Actions:

for information seekers,
Verify information before sharing (implied).
</details>
<details>
<summary>
2019-11-22: Let's talk about silver, gold, Vikings, Kings, history, and tradition.... (<a href="https://youtube.com/watch?v=ANOEbHqv2aw">watch</a> || <a href="/videos/2019/11/22/Lets_talk_about_silver_gold_Vikings_Kings_history_and_tradition">transcript &amp; editable summary</a>)

Beau reveals a treasure trove of Viking coins challenging historical narratives and traditions, urging reflection on the value of history over precious metals.

</summary>

"Two guys stumbled across a viking stash of coins and jewelry."
"The histories at the time, well they're a little murky."
"This is upending history and tradition."
"Silver and gold, even today, survivalists will tell you to keep some around because it's always valuable."
"It's just a thought, y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Two individuals stumbled upon a Viking treasure stash of coins and jewelry in 2015, with coins dating back 1100 years.
The stash was worth roughly 3.8 million dollars in the US.
Instead of alerting authorities, they decided to keep and eventually sell the treasure on the open market.
Only 31 out of the 300 coins have been recovered, some of which have significant historical value.
Some coins reveal an alliance between Alfred the Great of Wessex and Caelwulf II of Mercia, challenging traditional historical narratives.
The coins suggest a different story where Caelwulf II was actively allied with Alfred the Great, contrary to the usual portrayal of him as a puppet of the Vikings.
The disappearance of Caelwulf II from historical records around 879 raises suspicions about how Alfred the Great gained control of Mercia.
Beau draws a parallel to the series "The Last Kingdom" on Netflix for those unfamiliar with the historical context.
The discovery of these coins has the potential to rewrite history and challenge established traditions.
Beau questions the intrinsic value of silver and gold, likening it to the value placed on the history and traditions of a nation.

Actions:

for history enthusiasts,
Research local archaeology groups to support preservation efforts (suggested)
Visit historical sites or museums to learn more about ancient civilizations (implied)
</details>
<details>
<summary>
2019-11-21: Let's talk about COPPA (Not Legal Advice).... (<a href="https://youtube.com/watch?v=OUid1YxgwsU">watch</a> || <a href="/videos/2019/11/21/Lets_talk_about_COPPA_Not_Legal_Advice">transcript &amp; editable summary</a>)

Beau explains COPPA's impact on YouTube creators, urging action to address the negative consequences for wholesome kids' content and creators.

</summary>

"It's dead. There's no comment section. They won't get a notification. The monetization is severely limited."
"What they're doing is they're going to limit their child's entertainment and educational opportunities, what they can see for viewing."
"We don't need to succumb to learned helplessness. We don't just have to roll over and take this."

### AI summary (High error rate! Edit errors on video page)

Explains COPPA, a law impacting YouTube creators due to child privacy protection.
Mentions emotional reactions from creators, some not fully understanding the law's implications.
Notes the potential impact on wholesome kids' content on YouTube.
Describes the practical implications of COPPA, including marking videos for kids and potential fines for non-compliance.
Outlines factors determining kids' content, clarifying misunderstandings around the list provided by YouTube.
Raises concerns about the subjective nature of compliance with COPPA.
Emphasizes the severe consequences for creators if they mislabel their content.
Suggests actions to address the issues with COPPA, including contacting the FTC and legislators to push for changes.
Urges support for kid content creators through Patreon or merchandise purchases.
Advocates for getting rid of COPPA altogether, citing its outdated nature and negative impact on content creation.
Encourages viewers to take action by contacting representatives and engaging with the issue.

Actions:

for content creators, youtube users,
Contact the FTC and leave comments to advocate for changing COPPA (implied).
Reach out to representatives and senators to explain the outdated nature of the law and push for revisions (implied).
Support kid content creators by signing up for their Patreon or buying their merchandise (implied).
Tweet the president to raise awareness and potentially influence the FTC's decisions (implied).
</details>
<details>
<summary>
2019-11-20: Let's talk about impeachment, today, and tomorrow.... (<a href="https://youtube.com/watch?v=GRzfXOkHbk4">watch</a> || <a href="/videos/2019/11/20/Lets_talk_about_impeachment_today_and_tomorrow">transcript &amp; editable summary</a>)

The impeachment trial's historical significance transcends partisan politics as the Senate's decision shapes the balance of power between branches.

</summary>

"His initial testimony conflicts with what was given by pretty much all of the other witnesses."
"It's not about the immediate political fallout. It's about the long-term fallout."
"The Senate will go down in history one way or the other."
"Allowing foreign nationals to influence our election, it's going to be mighty hard to complain about foreign treaties."
"There's a whole bunch riding on this and it goes far, far beyond partisan politics."

### AI summary (High error rate! Edit errors on video page)

The impeachment trial is coming up, with a key testimony from Sondland tomorrow.
Even witnesses called by Republicans provided damaging evidence against the president.
Regardless of Sondland's testimony tomorrow, the evidence against the president is already strong.
Asking for a statement from a foreign national for a campaign is a crime, regardless of quid pro quo.
Historians will focus on the evidence presented in the impeachment trial.
The Senate's decision on impeachment will have long-term historical significance.
The Senate could choose to convict and remove the president or dismiss the trial altogether.
Republicans should be mindful of the precedent set for future presidents in terms of executive power.
Allowing foreign influence in elections sets a dangerous precedent for future administrations.
The Senate's decision will shape its historical legacy.

Actions:

for political observers, voters, historians,
Contact senators to urge them to prioritize the long-term implications of their impeachment trial decision (suggested).
Stay informed about the impeachment trial proceedings and outcomes (implied).
</details>
<details>
<summary>
2019-11-20: Let's talk about a hero you've probably never heard of.... (<a href="https://youtube.com/watch?v=jP4KFyqua3w">watch</a> || <a href="/videos/2019/11/20/Lets_talk_about_a_hero_you_ve_probably_never_heard_of">transcript &amp; editable summary</a>)

Beau introduces Martha Gellhorn, an exceptional correspondent who defied norms, inspiring societal change.

</summary>

"They are wrong in all of the right ways, and it moves our society forward."
"It's changing because of people like her."
"A lot of things in society change because people just do it anyway."

### AI summary (High error rate! Edit errors on video page)

Introduces one of his heroes, Martha Gellhorn, as one of the most interesting and prolific correspondents of the 20th century.
Martha Gellhorn covered significant events like the Spanish Civil War, the rise of a German dictator, D-Day, camps during WWII, Vietnam, Israeli-Arab conflicts, wars in Central America, and almost went to Bosnia.
Shares a fascinating story of Martha Gellhorn sneaking ashore on D-Day by impersonating a stretcher bearer.
Mentions Martha Gellhorn's detention and escape after being arrested for going ashore without authorization on D-Day.
Talks about Martha Gellhorn's marriage to Ernest Hemingway and how she challenged societal norms in combat correspondence.
Recognizes Martha Gellhorn as someone who changed the rules and broke societal barriers by being wrong in the right ways.
Acknowledges that many individuals who challenge societal norms often do not receive the recognition they deserve.

Actions:

for journalists, historians, activists,
Read about Martha Gellhorn's extraordinary life and contributions (suggested)
</details>
<details>
<summary>
2019-11-20: Let's talk about SEALs and deals.... (<a href="https://youtube.com/watch?v=HGvlgfBUK0Q">watch</a> || <a href="/videos/2019/11/20/Lets_talk_about_SEALs_and_deals">transcript &amp; editable summary</a>)

Chief Gallagher's potential removal from the SEALs could deepen divides between the Navy, Defense Department, and the President, showcasing a unique political development.

</summary>

"The fallout of this action will further drive a wedge between the Department of Defense, specifically the Department of the Navy, and the President."
"SEALs are supposed to be a cut above everybody, in every respect."

### AI summary (High error rate! Edit errors on video page)

Chief Gallagher, a SEAL accused of misconduct, was cleared by the President after being demoted.
Admiral Greene, along with other top Navy officials, plan to remove Gallagher from the SEALs.
The media reports suggest Gallagher may lose his trident badge symbolizing SEAL qualification.
The potential expulsion of Gallagher is within the Navy's power and may spark a political firestorm.
SEALs are held to high standards, with past instances of even DUIs leading to expulsion.
Gallagher, if removed, will likely take on different duties, preventing a repeat of the alleged misconduct.
The fallout from Gallagher's case could deepen the divide between the Department of Defense and the President.
This situation could lead to further isolation of the White House, a unique development in American history.
Beau anticipates a strong reaction, possibly in the form of a tweet storm, if Gallagher is indeed expelled.

Actions:

for military personnel, political analysts,
Watch for updates and developments in Chief Gallagher's case (implied)
Stay informed about the impact of military decisions on political dynamics (implied)
</details>
<details>
<summary>
2019-11-19: Let's talk about straws and studies.... (<a href="https://youtube.com/watch?v=TTkSFu1OQOI">watch</a> || <a href="/videos/2019/11/19/Lets_talk_about_straws_and_studies">transcript &amp; editable summary</a>)

Beau addresses misconceptions on emissions, advocates for plant-based alternatives, and stresses the power of individual action in driving environmental change.

</summary>

"We can't be defeated by a straw."
"Your voice matters more than you think."
"It requires individual action."

### AI summary (High error rate! Edit errors on video page)

Introduced the topic of straws and studies, addressing the controversy surrounding giving up plastic straws.
Pointed out that most people misinterpret information and discussed the impact of 100 companies producing 71% of emissions.
Raised concerns about how the U.S. might be defeated by a simple plastic straw due to lack of suitable alternatives.
Proposed using plant-based, biodegradable straws as a viable solution, mentioning their existence and cost.
Urged for individual responsibility and action in tackling environmental issues, contrasting popular misconceptions about emissions.
Advocated for a cultural shift away from profit-driven motives and emphasized the power of individual action over waiting for governmental intervention.
Stressed the importance of sending letters to major corporations like McDonald's to drive change and mentioned the effectiveness of boycotting.
Encouraged viewers to believe in the impact of their voices and emphasized the significance of individual actions in inciting change.

Actions:

for environmental activists, conscious consumers,
Contact major corporations like McDonald's to advocate for sustainable alternatives (suggested)
Boycott companies to drive change towards sustainability (exemplified)
</details>
<details>
<summary>
2019-11-19: Let's talk about learning from sliced bread and Gibraltar.... (<a href="https://youtube.com/watch?v=EBCSjUkywBM">watch</a> || <a href="/videos/2019/11/19/Lets_talk_about_learning_from_sliced_bread_and_Gibraltar">transcript &amp; editable summary</a>)

Beau examines the sacrifices of the greatest generation and criticizes the modern reluctance to make similar sacrifices in the face of global threats.

</summary>

"Yeah, these things aren't as dramatic as storming a machine gun nest on D-Day, but it's those little things that could truly alter the course of history."
"Today, people don't want to give up single-use straws. That's too much to ask when confronted with a global threat."
"These were people who lived through the Depression. They knew what hard times were."
"We have a global threat and it's a real one. Doesn't matter what the study from the oil company tells you."
"It's going to take both the dramatic and the mundane to solve it anyway."

### AI summary (High error rate! Edit errors on video page)

Examines the lessons to learn from the sacrifices made by the greatest generation during WWII.
Describes the lesser-known but significant actions taken by individuals facing a global threat.
Recounts the story of six individuals who volunteered to stay in secret chambers in Gibraltar to report back if it fell to the opposition.
Reveals the commitment of these individuals who were willing to be sealed inside the chambers with years worth of supplies.
Mentions Operation Tracer, a compartmentalized plan involving multiple teams willing to stay sealed in rooms for years.
Contrasts the minor inconveniences faced in the U.S. during WWII, like the temporary ban on sliced bread, to contribute to the war effort.
Draws parallels between the sacrifices made by the greatest generation and the lack of willingness in today's society to make similar sacrifices in the face of a global threat.
Criticizes the reluctance of current generations to give up conveniences like single-use straws when compared to the sacrifices of those before them.
Emphasizes the importance of both dramatic and mundane actions in addressing present-day global threats.
Calls for a shift in mindset towards tackling real global issues with the same dedication and sacrifice as seen in the past.

Actions:

for activists, history enthusiasts,
Form a local community group dedicated to environmental conservation and taking small, impactful actions. (implied)
Educate others about the historical sacrifices made during times of crisis to inspire collective action. (implied)
</details>
<details>
<summary>
2019-11-18: Let's talk about the wall and Constitutional obligations.... (<a href="https://youtube.com/watch?v=hmFGSiAWmRE">watch</a> || <a href="/videos/2019/11/18/Lets_talk_about_the_wall_and_Constitutional_obligations">transcript &amp; editable summary</a>)

Beau on lack of border wall progress, potential crimes, and constitutional duty to remove the president amidst turning base.

</summary>

"There was already a wall there. Where's the new wall?"
"It is now the Senate's constitutional obligation to remove the president."
"His base is shrinking rapidly because he keeps insulting them."
"All evidence shows that he violated the law more than likely."
"It's more than branding and that's what Trump is good at, branding."

### AI summary (High error rate! Edit errors on video page)

Down on the Mexican border, searching for Trump's wall, Beau gets lost and may have ended up in Mexico.
Beau questions the lack of progress on the border wall, noting that not a single new mile has been built in three years.
He criticizes the government for disrupting processes and seizing land without fair compensation under the Declaration and Taking Act.
Beau points out the failure of Mexico to pay for the wall, despite promises.
He addresses public opinion on the Ukraine call, suggesting that regardless of intentions, a crime may have been committed.
Beau argues that the Constitution mandates the Senate to remove the president if he committed crimes, including bribery and extortion.
The lack of progress on the wall, military decisions in Syria, and trade wars are factors causing even his base to turn against the president.
Beau questions Trump's ability to handle the job as president, mentioning his focus on branding and economic issues.
Despite some successes in avoiding recession, Beau believes all evidence points to Trump violating the law and suggests it's the Senate's obligation to remove him.

Actions:

for voters, concerned citizens,
Contact your senators to express your views on potential violations of the law by the president (suggested).
Join or support organizations advocating for government accountability (exemplified).
Organize community dialogues on constitutional obligations and holding leaders accountable (implied).
</details>
<details>
<summary>
2019-11-18: Let's talk about party loyalty and what happened in Louisiana.... (<a href="https://youtube.com/watch?v=Q524bP2gMQg">watch</a> || <a href="/videos/2019/11/18/Lets_talk_about_party_loyalty_and_what_happened_in_Louisiana">transcript &amp; editable summary</a>)

Republicans lost in Louisiana because loyal Republicans stuck to their values, showing that voting means becoming morally responsible for the candidate you support.

</summary>

"You don't co-sign evil out of tradition."
"Voting when you vote for someone, you become morally responsible."
"You become complicit in everything they do."
"If you vote for Trump, past, present, and future, Trump is you."
"You own that. You co-signed it."

### AI summary (High error rate! Edit errors on video page)

Republicans lost in Louisiana, a deep red state, because loyal Republicans stuck to their values.
If your party doesn't offer a candidate worthy of your vote, you can choose to abstain from voting.
Voting is not just about party loyalty; it's about becoming morally responsible for the actions of the candidate you support.
By voting for a candidate, you become complicit in everything they do, past, present, and future.
Voting for a candidate means you are endorsing and supporting all their actions.
Beau questions whether individuals who support Trump through their vote truly agree with actions like putting kids in cages or weakening environmental protections.
Voting for a candidate means taking ownership of their actions and decisions.
Beau urges people to think critically about the actions of the candidates they support rather than blindly following party loyalty.
Supporting a candidate through voting means endorsing everything they stand for and everything they do.
You can choose not to vote if the candidate presented by your party conflicts with your values and principles.

Actions:

for voters,
Re-evaluate your loyalty to a political party based on the actions and values of the candidates they present (exemplified)
</details>
<details>
<summary>
2019-11-16: Let's talk about today in Trump world.... (<a href="https://youtube.com/watch?v=5sdbnGAbliA">watch</a> || <a href="/videos/2019/11/16/Lets_talk_about_today_in_Trump_world">transcript &amp; editable summary</a>)

Beau recaps notable events from Trump world, warning Republicans of their legacy's impending tarnish for blindly supporting Trump's actions.

</summary>

"You're throwing away your legacies to protect somebody who is not going to protect you."
"Certainly painting Trump as a very self-absorbed person who does not care about the United States."
"Those in Congress, in the Senate, that are defending him will forever be remembered, for the rest of their very short political careers, as idiots who got conned by a real estate mogul."

### AI summary (High error rate! Edit errors on video page)

Recapping the noteworthy events from Trump world, including a probe into Giuliani's potential personal benefit from a Ukrainian energy company.
Giuliani questioned about whether Trump might betray him in the impeachment hearing and his cryptic reference to having "very, very good insurance."
President Trump's intimidating tweet about witness Yovanovitch, adding to his list of impeachable offenses.
Witness David Holmes testified about a call where Trump specifically inquired about the Biden investigation, eliminating plausible deniability.
Roger Stone convicted on all counts for obstructing the Russia Trump investigation, signaling potential future criminal prosecutions post-administration.
Republicans are warned about their defense of Trump, as future administrations will uncover the truth, tarnishing their legacies.
Criticizing Trump's self-absorbed nature and lack of concern for the country's well-being.
The episode ends with a foreboding message about the consequences for those blindly supporting Trump.

Actions:

for political observers, republicans,
Hold elected officials accountable for their support of actions that harm the country (implied).
Prepare for potential criminal prosecutions post-administration by staying informed and engaged (implied).
</details>
<details>
<summary>
2019-11-16: Let's talk about storing open source code in the Arctic.... (<a href="https://youtube.com/watch?v=OyiW1GRyKM8">watch</a> || <a href="/videos/2019/11/16/Lets_talk_about_storing_open_source_code_in_the_Arctic">transcript &amp; editable summary</a>)

Microsoft stores open source code in Arctic vault, questioning human arrogance and proposing basic knowledge preservation for humanity's survival.

</summary>

"Perhaps, given humanity's seeming unwillingness to work together to avoid a fall, maybe we should focus on building vaults all over the place."
"Basic information about medicine, food preparation, agriculture, that's what humanity needs to survive."
"World governments and large companies foresee a world reset within 750 years because that's how long that film lasts."

### AI summary (High error rate! Edit errors on video page)

Microsoft is storing the world's open source code in a vault in the Arctic, similar to the crypt of civilization built in the 40s at Oglethorpe University in Georgia.
The crypt at Oglethorpe University contains microfilm and was created due to the lack of information about ancient civilizations after their fall.
The Arctic World Archive near the global seed vault in Norway houses the open source code on a microfilm-like substance.
Time capsules aim to send information to the future, acknowledging that future civilizations may not have certain knowledge.
There's a mix of useful information like open source code and encyclopedic knowledge, but also trivial items like the secret sauce recipe for McDonald's in the Vatican archives.
Beau questions the arrogance behind presuming to know what future civilizations will need, suggesting a focus on preserving basic human knowledge instead.
He proposes multiple vaults containing vital information on medicine, food preparation, and agriculture as a more practical approach to ensuring humanity's survival.
The reality is that governments, large companies, and institutions like Microsoft and McDonald's foresee a world reset within 750 years, the lifespan of the microfilm.
Beau concludes by suggesting the idea of preparing for such a collapse by disseminating fundamental human knowledge.

Actions:

for concerned citizens, futurists,
Build community-led vaults with basic human information for survival (suggested)
Preserve knowledge on medicine, food preparation, and agriculture in accessible formats (suggested)
</details>
<details>
<summary>
2019-11-14: We talked about masculinity.... (<a href="https://youtube.com/watch?v=PflmaMpuGEc">watch</a> || <a href="/videos/2019/11/14/We_talked_about_masculinity">transcript &amp; editable summary</a>)

Beau talks about the fluidity of masculinity, the concept of Shibumi, and the importance of defining masculinity for oneself while challenging toxic masculinity and advocating for women's autonomy.

</summary>

"Masculinity is something you're going to define for yourself."
"The fact that you don't find her attractive doesn't matter. What you think doesn't matter."
"All women have to be two things and that's it. Who and what they want."

### AI summary (High error rate! Edit errors on video page)

Talks about the ridiculousness of guides designed to help young American males find their masculinity.
Shares a personal story about a Japanese concept called Shibumi explained by an old Japanese man.
Describes masculinity as subtle, understated, in control of nature, and effortless perfection.
Asserts that masculinity cannot be quantified or charted, and each person's masculinity is unique.
Emphasizes masculinity as a journey of introspection and self-improvement, not a checklist or conforming to societal norms.
Criticizes toxic masculinity and its origins in glorified violence and immature behavior.
Challenges the idea of toxic masculinity glorification by some men.
Counters arguments against feminism and patriarchy, pointing out the need for equal representation and the existence of patriarchal systems.
Advocates for respecting women's autonomy and choices, regardless of personal opinions or insecurities.

Actions:

for men, feminists, advocates,
Challenge toxic masculinity by uplifting others and focusing on self-improvement (implied).
Advocate for equal representation of women in positions of power (implied).
Respect women's autonomy and choices, regardless of personal opinions (implied).
</details>
<details>
<summary>
2019-11-14: Let's talk about thoughts and prayers.... (<a href="https://youtube.com/watch?v=wOfpAfIppk4">watch</a> || <a href="/videos/2019/11/14/Lets_talk_about_thoughts_and_prayers">transcript &amp; editable summary</a>)

Beau dives into the societal glorification of violence and the urgent need for non-violent solutions to address the underlying issues leading to tragic events.

</summary>

"We have a systemic problem."
"Our high school students are talking like rangers after the first time they were in combat."

### AI summary (High error rate! Edit errors on video page)

Discussed the reactions after seeing something trending on Twitter.
Mentions the common responses to tragic events: better security, arming teachers, and gun control.
Critiques thoughts and prayers as not being a solution.
Explores the impracticality of extreme security measures at schools.
Raises the issue of the underlying problem being kids wanting to harm others.
Points out the cycle of violence being perpetuated by societal norms.
Calls for comments on solutions that do not involve additional violence.
Urges for a cultural shift away from glorifying violence.
Expresses the need for a systemic change rather than relying on thoughts and prayers.
Shares the perspective of a student feeling unprepared despite drills in schools.

Actions:

for community members, activists,
Share and create solutions that do not involve additional violence (suggested).
Advocate for a cultural shift away from glorifying violence (implied).
</details>
<details>
<summary>
2019-11-14: Let's talk about the happiest prisoner.... (<a href="https://youtube.com/watch?v=6Dl5kYKBGxk">watch</a> || <a href="/videos/2019/11/14/Lets_talk_about_the_happiest_prisoner">transcript &amp; editable summary</a>)

Beau shares the story of Joe, the happiest prisoner on death row in the 1930s, urging action on Rodney Reed's case in Texas within a week.

</summary>

"His name was Joe Aradie, known as the happiest prisoner on death row."
"Took me too long to see a toy train."

### AI summary (High error rate! Edit errors on video page)

Beau shares a personal anecdote about his son playing with a toy train, leading into a story he wanted to talk about.
He introduces the story of Joe, known as the happiest prisoner on death row, in the 1930s.
Joe, with an IQ of 45, faced a challenging life and was picked on before being arrested for vagrancy in Cheyenne, Wyoming.
Sheriff Carroll questions Joe about crimes in Pueblo, and Joe falsely confesses to be with another man named Frank.
Despite inconsistencies in Joe's confession and lack of identification by the surviving sister, he is convicted and sentenced to death.
Joe's attorney tries to help with appeals, but Joe is executed without a pardon.
Joe's last moments before execution involve playing with a toy train, showing innocence and ignorance of his impending death.
Beau mentions Rodney Reed's case in Texas and urges viewers to research and possibly take action within a week.
Beau concludes with a reflective note, leaving the audience with something to ponder.

Actions:

for advocates for justice,
Research Rodney Reed's case and contact the governor within a week (implied)
Advocate for justice by raising awareness about cases like Rodney Reed's (implied)
</details>
<details>
<summary>
2019-11-13: Let's talk about impeachment, being uncomfortable, and saving lives.... (<a href="https://youtube.com/watch?v=fcmXcPYvSnk">watch</a> || <a href="/videos/2019/11/13/Lets_talk_about_impeachment_being_uncomfortable_and_saving_lives">transcript &amp; editable summary</a>)

GOP senators refuse to research, spreading misinformation and harming marginalized groups, while medical professionals support gender-affirming treatments for children.

</summary>

"I don't care if Tommy plays with Barbie, if that means that Tommy gets to live."
"Imagine if it was about who you are, not just your state and their education."
"Let's get that statistic a little higher, 41%. Those are rookie numbers."

### AI summary (High error rate! Edit errors on video page)

GOP senators are divided on watching the impeachment hearing due to their base not researching.
The GOP operates by latching onto ideas and repeating them, creating a mythical enemy.
Georgia and Kentucky are introducing bills to stop gender-affirming surgeries in children based on misinformation.
Social transitioning for children does not involve surgery but may include puberty blockers in their teens.
The idea of children undergoing surgery for gender affirmation is false.
Some politicians oppose gender-affirming treatments supported by medical associations.
Misinformation leads to scapegoating marginalized groups like children and their parents.
Over 41% of the LGBTQ+ community attempts suicide due to lack of acceptance.
Ignorance and refusal to understand lead to discomfort and immorality judgments.
Medical professionals support gender-affirming treatments, urging the Republican Party to research and understand.

Actions:

for republicans, medical professionals,
Contact GOP senators urging them to research and understand the importance of gender-affirming treatments (suggested).
Support and advocate for gender-affirming treatments for children by spreading accurate information and fighting misinformation (exemplified).
</details>
<details>
<summary>
2019-11-12: Let's talk about Native schools, today's schools, and what we all need to do.... (<a href="https://youtube.com/watch?v=P4uUEOENwp0">watch</a> || <a href="/videos/2019/11/12/Lets_talk_about_Native_schools_today_s_schools_and_what_we_all_need_to_do">transcript &amp; editable summary</a>)

Beau brings attention to the brutal history of Native American schools, the modern-day impact of assimilation in education, and the importance of fostering curiosity to combat uniformity in learning systems.

</summary>

"The schools that were imposed on the natives, they were something else."
"The schools, they kill that individuality, they really do, it's the way they're designed."
"We have to create that curiosity so that they can take that base knowledge that they're getting to the next level."
"You try to impart that lesson. I guarantee you, you're never going to oppose a raise for teachers again."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Native American Heritage Month, discussing the brutal history of native schools imposed on Indigenous peoples, designed to crush their spirit and eradicate their culture.
Schools historically aimed to assimilate Indigenous children, forbidding them from speaking their native tongue, using their real names, or practicing their culture.
The impact of these schools on Indigenous communities has been long-lasting, with repurposed facilities sometimes revealing unmarked graves of children.
Modern-day schools, while not as brutal, still prioritize assimilation and uniformity, stifling curiosity and individuality.
Teachers are often overworked and focused on meeting standardized test requirements, leading to students learning what to think rather than how to think.
As parents and community members, it's vital to supplement the education provided by the state and encourage curiosity and critical thinking in children.
Beau shares an anecdote about encouraging his child's curiosity by exploring topics together, utilizing resources like YouTube to foster learning opportunities.
The education system prioritizes uniformity over individuality, hindering the development of critical thinking skills in students.
Beau stresses the importance of fostering curiosity in children to ensure they can apply their knowledge effectively and think for themselves.
While acknowledging the challenges teachers face, Beau encourages a shift towards promoting critical thinking skills and individuality in education systems.

Actions:

for parents, community members,
Foster curiosity in children through supplementary learning opportunities (exemplified)
Encourage critical thinking skills and individuality in education systems (implied)
Utilize resources like YouTube to support children's interests and curiosity (exemplified)
</details>
<details>
<summary>
2019-11-12: Let's talk about DACA, Stephen Miller, and the future.... (<a href="https://youtube.com/watch?v=bj3dKW_FtFA">watch</a> || <a href="/videos/2019/11/12/Lets_talk_about_DACA_Stephen_Miller_and_the_future">transcript &amp; editable summary</a>)

700,000 Americans are at risk, led by a senior advisor exposed as a racist fascist, while the nation turns a blind eye to illegal actions.

</summary>

"700,000 Americans, known as dreamers, are at risk of having their lives destroyed."
"The country is being led by a senior White House advisor with disturbing emails that expose his true nature."
"We're letting it happen right now."

### AI summary (High error rate! Edit errors on video page)

700,000 Americans, known as dreamers, are at risk of having their lives destroyed due to policies recommended by Stephen Miller.
Steve Miller's emails reveal him as a racist and fascist, yet the President and Republicans continue to support him.
The country is being led by a senior White House advisor with disturbing emails that expose his true nature.
The president solicited a thing of value from a foreign national for his campaign, which is illegal, but his supporters turn a blind eye.
While harmless individuals are at risk, the country is focused on destroying lives for political gain.
The nation's leadership is lacking, and it will require ideologues to step forward to rebuild and heal the country.
There is a need to undo the damage caused by current leadership and work towards healing the nation.

Actions:

for americans,
Protest in support of the 700,000 individuals at risk (implied)
Step forward as ideologues to rebuild and heal the country (implied)
</details>
<details>
<summary>
2019-11-11: Let's talk about Veterans Day (Part 3).... (<a href="https://youtube.com/watch?v=xSgJnZddJaE">watch</a> || <a href="/videos/2019/11/11/Lets_talk_about_Veterans_Day_Part_3">transcript &amp; editable summary</a>)

Beau outlines numerous instances of U.S. military interventions, urging to stop turning veterans into combat veterans and reflecting on the country's history of conflict.

</summary>

"The best thing we can do for Veterans Day is to stop turning veterans into combat veterans."
"This isn't even all of it."
"Maybe go back through this list and try to figure out how many years the U.S. was at peace out of its entire existence."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Outlines various instances where veterans were turned into combat veterans throughout history.
Mentions interventions and wars from early 1900s to present, including conflicts in Nicaragua, Haiti, Dominican Republic, Russian Civil War, World Wars, Korean War, Vietnam War, Bay of Pigs, and more.
Talks about the U.S. involvement in different civil wars and conflicts globally, showcasing a pattern of intervention.
Criticizes the continuous cycle of turning veterans into combat veterans and the impact it has had.
Points out the importance of recognizing and reflecting on the history of U.S. military interventions.
Suggests that the best way to honor Veterans Day is to stop turning veterans into combat veterans.

Actions:

for history enthusiasts, anti-war advocates,
Research and understand the history of U.S. military interventions (suggested)
Advocate for policies that prioritize peace over war (implied)
</details>
<details>
<summary>
2019-11-11: Let's talk about Veterans Day (Part 2).... (<a href="https://youtube.com/watch?v=c_fiqEuwXuE">watch</a> || <a href="/videos/2019/11/11/Lets_talk_about_Veterans_Day_Part_2">transcript &amp; editable summary</a>)

Beau covers America's lesser-known conflicts, exposing a history of constant war and injustice.

</summary>

"We're always at war. We just don't always talk about them."
"Looking back at it through clearer eyes, a lot of this wasn't just."

### AI summary (High error rate! Edit errors on video page)

Veterans Day special covering America's conflicts, not just major ones in history books.
Apache Wars lasted from 1852 to 1924, consisted of constant skirmishes.
US military deployed 5,000 men to deal with Geronimo and his 30 followers.
Bleeding Kansas (1854-186) determined if Kansas was to be a slave state.
Puget Sound War (1855-1856) part of the Akimlu War, notable for a chief's execution.
First Fiji expedition in 1855 due to a civil war incident involving an American businessman.
Rogue River Wars (1855-1856) in Oregon marked another land grab.
Various conflicts like Third Seminole War, Yakima War, Second Opium War, Utah War, and Navajo Wars involved land grabbing.
Second Fiji expedition in 1859 rumored to involve two Americans being eaten by Natives.
Dakota War, Colorado War, Shimonoseki campaign, Snake War, Powder River War, and Red Cloud's War were all land grabs.
The Great Sioux War (1876), Buffalo Hunters War, Pierce War, Bannock War, Cheyenne War, Sheep Eaters War, and White River War were all about seizing land.
Pine Ridge Campaign (1890-1891) led to the Wounded Knee massacre.
Spanish-American War (1898) triggered by the USS Maine incident.
Philippine-American War (1899-1902) followed the Spanish-American War.
Boxer Rebellion (1899-1902) and Crazy Snake Rebellion (1909) occurred due to various reasons such as stolen meat.
Border War (1910-1916) involving Pancho Villa, and the armed uprising of independence of color in Cuba (1912).

Actions:

for history enthusiasts, activists,
Research and learn more about the lesser-known conflicts in American history (suggested)
Advocate for educational reforms to include a comprehensive history curriculum (suggested)
</details>
<details>
<summary>
2019-11-11: Let's talk about Veterans Day (Part 1).... (<a href="https://youtube.com/watch?v=JYvL5cUj5ds">watch</a> || <a href="/videos/2019/11/11/Lets_talk_about_Veterans_Day_Part_1">transcript &amp; editable summary</a>)

Beau introduces a Veterans Day special to shed light on America's lesser-known conflicts throughout history, reflecting on the vast array of wars often excluded from mainstream narratives.

</summary>

"History often overlooks minor conflicts."
"Veterans Day is probably a good day to talk about all of these."
"We don't like why the war started. We don't like the outcome."
"Some of these weren't major, but some of them were huge."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Introducing a Veterans Day special to talk about America's conflicts, including lesser-known ones.
Listing major conflicts such as the Revolutionary War, War of 1812, Mexican-American War, and more.
Noting that history often overlooks minor conflicts like the Cherokee American War, Shays Rebellion, and others.
Explaining each conflict briefly and providing insights, such as tax revolts and internal wars.
Mentioning wars like the Barbary Wars, Creek War, and the Seminole Wars that shaped American history.
Describing events like the Texas Indian Wars involving land grabs and ethnic cleansing over 55 years.
Sharing lesser-known conflicts like the Aegean Sea operations, Winnebago War, and Sumatran expeditions.
Recounting sad events like the Black Hawk War and conflicts like the Pork and Beans War over borders.
Touching on unique expeditions like the Ivory Coast expedition and discussing motivations behind conflicts.
Concluding by reflecting on the vast array of conflicts often excluded from mainstream historical narratives.

Actions:

for history enthusiasts, veterans,
Research and learn about the lesser-known conflicts mentioned by Beau (implied)
Take time to commemorate and acknowledge the sacrifices made in all conflicts, major and minor (implied)
</details>
<details>
<summary>
2019-11-11: Let's talk about Senator Graham's impeachment claim.... (<a href="https://youtube.com/watch?v=jJ5kEXWC96s">watch</a> || <a href="/videos/2019/11/11/Lets_talk_about_Senator_Graham_s_impeachment_claim">transcript &amp; editable summary</a>)

Senator Graham's attempt to bully the House into revealing the whistleblower's identity undermines the Constitution and raises concerns about Senate integrity.

</summary>

"You're trying to bully… Kind of sounds like what the president's getting impeached for, doesn't it?"
"Whistleblowers and people who like to remain anonymous, do so because they don't want to be smeared by an attorney or some politician who has put his party over his country."
"It certainly appears that Senator Graham is willing to undermine the Constitution and the rule of law to protect somebody he knows is guilty."
"Failing to uphold your oath and your duty and attempting to undermine the Constitution through some rhetoric that we all see through, that's going to hurt your re-election chances a whole lot more than simply saying, yeah, Trump broke the law."

### AI summary (High error rate! Edit errors on video page)

Senator Graham threatened that impeachment is dead on arrival if the whistleblower is not named, attempting to bully the House into revealing the whistleblower's identity.
The Senator's demand is baseless, as the House has sole power over impeachment and the Senate's duty is to conduct a trial, not dismiss impeachment without due process.
Beau criticizes Senator Graham's attempt to bully and manipulate the process by demanding the whistleblower's identity, likening it to the behavior that led to the President's impeachment.
He questions the Senator's logic by sarcastically suggesting that Crime Stoppers and anonymous tip lines should be banned if anonymity invalidates due process.
The importance of whistleblowers remaining anonymous is emphasized, as they fear being attacked by individuals who prioritize party loyalty over the country's well-being.
Senator Graham's actions are seen as undermining the Constitution and the rule of law to protect a guilty party, raising concerns about the Senate's trustworthiness and integrity.
Beau accuses the GOP of engaging in corrupt behavior by attempting to overlook wrongdoing in favor of reelection prospects, rather than upholding their oath and duty to uphold the Constitution.
He argues that failing to uphold their oath and attempting to undermine the Constitution will have more significant negative consequences for reelection than acknowledging any illegal actions committed by the President.

Actions:

for politically engaged citizens,
Contact your representatives to express support for protecting whistleblowers and upholding due process (exemplified)
</details>
<details>
<summary>
2019-11-09: Let's talk about why people in history fled the US.... (<a href="https://youtube.com/watch?v=NR0tN1_GaCA">watch</a> || <a href="/videos/2019/11/09/Lets_talk_about_why_people_in_history_fled_the_US">transcript &amp; editable summary</a>)

Beau challenges the romanticized American narrative by exposing historical refugee movements and criticizing the treatment of refugees in the US, urging Canada to reconsider asylum agreements.

</summary>

"We don't like to talk about American history, not the bad parts anyway."
"The U.S. is violating international law as far as the treatment of refugees."
"It's not safe. The amount of abuse that goes on in those detention centers is just unimaginable."
"As we talk about American history, it should be noted that Donald Trump gets to join the ranks of the Civil War and the draft as the few reasons people would flee this country."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Critiques the avoidance of discussing the negative parts of American history, particularly during National American History and Founders Month.
Points out the fallacy of the idea that people in the United States do not flee, citing historical instances like during the Civil War and Vietnam draft.
Describes a period when tens of thousands of people fled to Canada, challenging the narrative of Americans as problem-solvers.
Explains the loophole in the agreement between Canada and the US that allowed refugees to seek asylum in Canada.
Mentions upcoming court proceedings in Toronto led by Amnesty International Canada to challenge the US asylum system.
Argues that the US is violating international law in its treatment of refugees, including family separation and unwarranted detention.
Urges the Canadian government to recognize the flaws in the US asylum system and scrap the agreement due to the abuse in US detention centers.
Draws parallels between Donald Trump's policies and historical reasons for people fleeing the country.

Actions:

for advocates for refugee rights,
Support organizations like Amnesty International Canada in their efforts to challenge the US asylum system (suggested).
Advocate for refugee rights and fair treatment of asylum seekers in your community (implied).
</details>
<details>
<summary>
2019-11-08: Let's talk about NBC helping build a tunnel under the wall.... (<a href="https://youtube.com/watch?v=2YoOoMc4T7w">watch</a> || <a href="/videos/2019/11/08/Lets_talk_about_NBC_helping_build_a_tunnel_under_the_wall">transcript &amp; editable summary</a>)

Two men recruit an engineering student to build a tunnel to freedom, facing challenges and risks while aiding refugees on both sides of the border.

</summary>

"It's amazing how your views on these things are just based on which side of the wall you're on and what decade you're at."
"When Kennedy saw the footage, he cried."
"Defeated that wall."
"This is a good story to tell."
"If you want more information on it, you can find it under Tunnel 29."

### AI summary (High error rate! Edit errors on video page)

Two men approach a 22-year-old engineering student, seeking help to build a tunnel to the other side of the border.
The student, a refugee himself, agrees and they locate a factory to conceal the entrance.
As they dig towards their destination, realizing the need for more help, they recruit additional engineering students.
NBC News funds the tunnel construction after the group faces challenges and realizes the need for better tools.
A burst pipe floods the tunnel, causing a setback, but they manage to fix it without getting caught.
Discovering another tunnel nearby, they offer their assistance and help complete it successfully.
Despite the first tunnel's failure and arrest of others, the engineering student and his friends press on with their mission.
They redirect their tunnel to a different building due to increased border patrols, aiming to help refugees escape safely.
Using coded signals in bars, they coordinate the rescue operation, facing risks and uncertainty on both sides.
Refugees emerge from the tunnel, including a woman, a baby, and many others seeking freedom in West Berlin.

Actions:

for activists for refugee rights,
Support refugee organizations in your community by volunteering or donating (implied)
Advocate for humane immigration policies and support refugees seeking safety (implied)
</details>
<details>
<summary>
2019-11-07: We talked about how to change the world.... (<a href="https://youtube.com/watch?v=2deSJ61eg7o">watch</a> || <a href="/videos/2019/11/07/We_talked_about_how_to_change_the_world">transcript &amp; editable summary</a>)

Beau shares insights on building community networks, stressing the importance of conscious rebellion, mutual support, and self-sufficiency for societal change.

</summary>

"Being independent doesn't actually mean doing everything by yourself, it means having the freedom to choose how it gets done."
"Everybody has something to offer a network like this, everybody."
"If you make your community strong enough, it doesn't matter who gets elected, it doesn't matter who's sitting in the Oval Office."
"Not sure top-down leadership is the answer. Think maybe community building is the answer."
"You know what's best for your community and your area."

### AI summary (High error rate! Edit errors on video page)

Compares today's Western society to George Orwell's 1984, where control mechanisms limit thinking outside the box.
Emphasizes the need to become conscious before rebelling against societal norms.
Shares a personal story of needing help from a network, showcasing the importance of community.
Encourages building a network by evaluating personal skills and offering assistance.
Challenges stereotypes about age and expertise, urging everyone to contribute to a community network.
Suggests using social media to connect and build a digital community for introverts.
Stresses the value of expertise and the importance of not underestimating one's skills.
Recommends recruiting people from immediate circles, social media, and activist communities for a network.
Shares examples of structured networks using poker chips for favors or a commitment to help each other.
Advocates for increasing financial resources by starting low-cost businesses and investing within the network.
Mentions the concept of Stay Behind Organizations during the Cold War as an example of effective community networks.
Proposes that building a strong community can lead to self-sufficiency regardless of political leadership.
Rejects top-down leadership in favor of community building to empower local areas.

Actions:

for community builders, activists.,
Connect with immediate circles, social media, and activist communities to recruit members for a network (suggested).
Start a low-cost business or invest in someone else within the network to increase financial resources (exemplified).
Build a digital community for introverts using social media (implied).
Offer help based on personal skills and expertise to contribute to a community network (suggested).
Participate in community service activities to showcase the network's positive impact (implied).
</details>
<details>
<summary>
2019-11-07: Let's talk about what we can learn from Wounded Knee.... (<a href="https://youtube.com/watch?v=-M9QajRgSHI">watch</a> || <a href="/videos/2019/11/07/Lets_talk_about_what_we_can_learn_from_Wounded_Knee">transcript &amp; editable summary</a>)

General Miles' telegram reveals unfulfilled treaty obligations leading to the Wounded Knee massacre, showcasing American history's mistreatment of Native Americans and serving as a lesson for modern foreign policy.

</summary>

"It's Native American Heritage Month. But their heritage is our heritage."
"Ignorance, arrogance, supremacy, fear of the other. It's what caused it."
"We could learn a lot from our Native American heritage."

### AI summary (High error rate! Edit errors on video page)

General Miles sent a telegram to D.C. stating that the "difficult Indian problem" could not be solved without fulfilling treaty obligations.
Native Americans had signed away valuable portions of reservations to white people without receiving adequate support as promised.
Leading up to the Wounded Knee massacre, the US government continued to seize Native land and ignore treaty agreements.
The Ghost Dance movement, based on a vision of a native Jesus, scared settlers, leading to conflict with authorities.
Sitting Bull was targeted for arrest, leading to a violent confrontation where he was killed.
Natives fleeing reprisals were surrounded by the Seventh Cavalry, resulting in a massacre at Wounded Knee.
The Army awarded 20 Medals of Honor for the massacre, indicating a cover-up to justify the excessive force used.
The Wounded Knee massacre exemplifies ignorance, arrogance, supremacy, and fear of the other in US history and foreign policy.
Beau suggests reflecting on Native American heritage and how it relates to modern-day foreign policy and exploitation.
Native American history serves as a vital lesson on the consequences of colonialism, militarism, and exploitation.

Actions:

for history enthusiasts, activists,
Study Native American history and share the truth with others (suggested)
Support organizations advocating for Native American rights and reparations (exemplified)
</details>
<details>
<summary>
2019-11-07: Let's talk about Washington, Jefferson, and Henry.... (<a href="https://youtube.com/watch?v=lxNOs-J8STg">watch</a> || <a href="/videos/2019/11/07/Lets_talk_about_Washington_Jefferson_and_Henry">transcript &amp; editable summary</a>)

November is National American History and Founders' Month; Beau sheds light on the dark truths behind the popular myths surrounding George Washington, Thomas Jefferson, and Patrick Henry, urging a reevaluation of history and recognition of Native American Heritage Month.

</summary>

"Take some of the mythology out of it."
"Those probably aren't the stories you know about the Founding Fathers."
"You might be surprised."
"There's a lot of mythology."
"Just a thought, y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

November is National American History and Founders' Month proclaimed by Donald J. Trump, which initially sparked mixed emotions but Beau is warming up to the idea.
Beau stresses the importance of understanding American history and the founding fathers to debunk the mythology surrounding them.
George Washington, despite his military leadership, had a dubious past including surrendering due to not knowing French and allegedly committing war crimes.
George Washington's treatment of slaves included brutalities and separating families by selling them to traders from the West Indies.
Thomas Jefferson engaged in a relationship with one of his slaves, likely starting when she was 14 and he was in his 40s, bearing children he raised as slaves.
Patrick Henry, known for "give me liberty or give me death," imprisoned his wife in a cellar for four years, leading to her believed suicide due to postpartum depression.
The popular stories about the founding fathers, like George Washington's wooden teeth, are mostly myths; his teeth were reportedly from slaves.
Beau points out the significant historical inaccuracies and the need to also recognize Native American Heritage Month.

Actions:

for history enthusiasts, truth seekers,
Research and learn about the real stories of the founding fathers (suggested)
Educate others about the dark truths of American history (implied)
</details>
<details>
<summary>
2019-11-06: Let's talk about the future, futility, and freedom.... (<a href="https://youtube.com/watch?v=yjMEC8KTqLc">watch</a> || <a href="/videos/2019/11/06/Lets_talk_about_the_future_futility_and_freedom">transcript &amp; editable summary</a>)

Beau dives into conservatism, freedom, and the pushback against progress, envisioning a future of increasing freedom and equality despite resistance from some.

</summary>

"The future is more freedom for more people."
"That wild and free future cannot be stopped."
"The future is a future where women, racial minorities, gender and sexual minorities are free and equal."

### AI summary (High error rate! Edit errors on video page)

Talks about the essence of conservatism, freedom, and futility, reflecting on the scary future and the theme of the universe being wild and free.
Describes how the future should bring more freedom for more people, encouraging innovation and letting go of old ideas to become better.
Compares the progress of freedom to waves on a beach, with each wave reaching a bit further ashore.
Mentions how some people find the advancing freedom scary as it challenges their beliefs, leading them to reach back to preserve dated ideas.
Shares the story of Americana City, where people moved to Brazil to preserve their culture but ultimately assimilated into the new society.
Examines the Make America Great Again voter demographic, born in 1966 or before, seeking clarity and purpose in a changing world.
Points out how the older generation's upbringing during segregation influences their views, even if they don't overtly hold those ideas anymore.
Talks about the fear of the unknown and reluctance to change among certain groups, contrasting them with the younger, more progressive minds joining the electorate.
Emphasizes the inevitable progression towards a future where all individuals are free and equal, predicting the fading of old ideas and institutions.
Concludes by acknowledging the unstoppable nature of progress towards a more inclusive and equitable society, despite resistance from some individuals.

Actions:

for progressive individuals,
Connect with younger individuals to encourage progressive thinking and inclusivity (implied)
Challenge outdated ideas within communities and advocate for equality (implied)
</details>
<details>
<summary>
2019-11-05: Let's talk about the 5th of November.... (<a href="https://youtube.com/watch?v=9F2PzR6o0_g">watch</a> || <a href="/videos/2019/11/05/Lets_talk_about_the_5th_of_November">transcript &amp; editable summary</a>)

Beau dives into the history of the 5th of November, the Gunpowder Plot, and how symbols like Guy Fawkes have evolved into broader symbols of resistance, resonating for unknown reasons.

</summary>

"Probably not."
"But at some point, Guy Fawkes and that mask became more than history, became something else, became a symbol, a symbol of resistance."
"These slogans, these symbols, these ideas, they resonate for whatever reason."
"It seems funny that the reality is, for Americans, Guy Fawkes Day is not about Guy Fawkes."
"It's about a comic character and somebody uniting the faceless masses against an oppressive government."

### AI summary (High error rate! Edit errors on video page)

Explains the history of the 5th of November and the Gunpowder Plot.
Queen Elizabeth I's conflicts with the Catholic Church and James I's actions.
Details the plot by Guy Fawkes and Robert Catesby to blow up Parliament.
Mentions the failed attempt due to an anonymous letter and the subsequent capture of the plotters.
Talks about how the modern image of Guy Fawkes comes from the comic book and movie "V for Vendetta."
Examines how symbols like Guy Fawkes have transformed into broader symbols of resistance.
Raises the point about symbols throughout history that resonate without full understanding.
Beau muses on the evolution of historical symbols and their meanings in modern times.

Actions:

for history enthusiasts,
Research and learn more about historical events like the Gunpowder Plot and their significance (suggested).
Engage in dialogues about the transformation of historical symbols into modern-day representations (exemplified).
</details>
<details>
<summary>
2019-11-05: Let's talk about science, art, lasers, and waste.... (<a href="https://youtube.com/watch?v=_5ZVqFDXKoU">watch</a> || <a href="/videos/2019/11/05/Lets_talk_about_science_art_lasers_and_waste">transcript &amp; editable summary</a>)

Beau dives into nuclear waste management, warning messages, and a physicist's laser innovation, showcasing human ingenuity and the need for resource allocation in solving global challenges.

</summary>

"This place is a message and part of a system of messages."
"The danger is unleashed only if you substantially disturb this place physically."
"Understand human ingenuity is amazing and we can come up with some pretty brilliant stuff given the resources."
"An idea like this is out there for every problem that faces humanity."
"Sometimes we might should research just to research."

### AI summary (High error rate! Edit errors on video page)

Exploring the potential of nuclear power and its waste management.
Long-term storage of nuclear waste being a major issue.
The challenge of conveying a warning message about nuclear waste for 10,000 years.
Various creative ideas proposed to deter future generations from accessing the waste site.
The message to future generations about the danger of the nuclear waste.
A physicist's innovative idea to use lasers to alter nuclear waste drastically.
The potential impact of laser technology on reducing nuclear waste's danger.
The importance of human ingenuity in solving global problems.
The necessity of allocating resources towards creative solutions rather than destruction.
The role of funding and resources in supporting groundbreaking ideas.

Actions:

for innovators, policymakers, environmentalists.,
Support innovative solutions for nuclear waste management (suggested).
Advocate for proper funding for creative problem-solving initiatives (implied).
</details>
<details>
<summary>
2019-11-05: Let's talk about Canada's water.... (<a href="https://youtube.com/watch?v=2DbS-41Rgt4">watch</a> || <a href="/videos/2019/11/05/Lets_talk_about_Canada_s_water">transcript &amp; editable summary</a>)

Canadian water crisis reveals widespread lead contamination worse than Flint, urging citizens to hold officials accountable and prioritize fixing infrastructure for clean water.

</summary>

"There is no safe level of lead. It's all bad."
"If a government cannot provide the most basic of services, water. If a jurisdiction can't provide that, if a government can't provide that, what good is it?"
"I personally rather pay to have all of the pipes in Flint fixed rather than send some more bombs overseas."

### AI summary (High error rate! Edit errors on video page)

Canadian journalism students and others conducted a massive investigation revealing a national water crisis worse than Flint, with hundreds of thousands unknowingly drinking contaminated water with high lead levels.
Lead levels in several Canadian cities were consistently higher than Flint's, with 33% of tested water sources exceeding the Canadian guideline of 5 parts per billion.
There is no safe level of lead, as even a single glass of highly contaminated water can lead to hospitalization, especially harmful to children by lowering IQ, causing focus problems, and damaging the brain and kidneys.
Schools in Canada are not regularly tested for lead, and even if high levels are found, there is no obligation to inform anyone.
Approximately half a million service lines in Canada are lead, affecting a significant number of people due to lead pipes corroding and contaminating the water.
Aging infrastructure in both Canada and the United States is a key issue, necessitating costly repairs to prevent further lead contamination.
Some Canadian jurisdictions like Quebec are taking steps to address the crisis, with plans to replace both public and private lead pipes.
Beau questions the priorities of developed countries like Canada and the U.S., urging citizens to care about how government funds are allocated to prevent such crises.
Beau advocates for prioritizing fixing infrastructure issues like lead pipes over military spending, questioning the government's ability to provide basic services like clean water.

Actions:

for canadians, americans,
Hold local officials accountable for ensuring clean water (implied)
Advocate for regular testing of schools for lead contamination (implied)
Support initiatives to replace lead pipes in affected areas (implied)
</details>
<details>
<summary>
2019-11-04: Let's talk about ethically using resources.... (<a href="https://youtube.com/watch?v=OUu0VVLHYjI">watch</a> || <a href="/videos/2019/11/04/Lets_talk_about_ethically_using_resources">transcript &amp; editable summary</a>)

Beau advises using the opposition's resources strategically when behind enemy lines in altering society, focusing on benefiting people over establishments.

</summary>

"When you're talking about altering society, we're behind enemy lines."
"There are times when using the opposition's resources is your only option."
"You have no ethical obligation to correct that mistake for them."
"You just have to make sure that the benefit to the people outweighs the benefit to the establishment."
"As long as the money isn't affecting the content that you're putting out, as long as it's not going to change your speaker list, as long as it's not going to alter what they're going to talk about, you know..."

### AI summary (High error rate! Edit errors on video page)

Received a question from a person attending a university heavily funded by a nearby corporation.
The person opposes the corporation but was given the chance to organize speaking engagements funded by them.
Advises that as long as the funding doesn't affect the content or speakers, taking the money is acceptable.
Mentions how the Department of Defense teaches soldiers to use resources left behind by the opposition.
Argues that using the corporation's resources, even if it seems hypocritical, can be necessary in certain situations.
States that altering society is like being behind enemy lines, and using the opposition's resources can be strategic.
Emphasizes that taking the corporation's money for speakers who will speak out against them is not ethically wrong.
Points out that large corporations fund events to generate goodwill and that drawing attention to their practices can be a win.
Notes that in a society dominated by big corporations, it's challenging to completely avoid their influence.
Concludes with the idea that ethical consumption is rare, but the focus should be on ensuring benefits to people outweigh those to the establishment.

Actions:

for university organizers,
Organize speaking engagements funded by corporations (suggested)
Draw attention to questionable business practices of large companies (implied)
</details>
<details>
<summary>
2019-11-04: Let's talk about a camping story from the 1950s and what it can teach us.... (<a href="https://youtube.com/watch?v=4dbRGaPph-w">watch</a> || <a href="/videos/2019/11/04/Lets_talk_about_a_camping_story_from_the_1950s_and_what_it_can_teach_us">transcript &amp; editable summary</a>)

In 1950s camping experiment, shared struggles united warring boy scout groups, showing how common challenges can dissolve divisions and foster unity.

</summary>

"When the struggle comes, we will unite."
"Those petty divisions disappear. Skin tone, religion, borders, none of it matters because we'd all be united."
"We have the struggles. We just need somebody to leave the rope, set things in motion, get us to unite."
"The insults stopped. The grudges, well, they're gone. They became friends."
"It's been repeated pretty often."

### AI summary (High error rate! Edit errors on video page)

In 1954, two groups of 11 boy scouts who had never met before went camping at Robbers Cove State Park in Oklahoma.
The boys quickly formed bonds and a social order, but things took a competitive turn when they found out about another group nearby.
The competition escalated with sporting events, team challenges, and the winning team was promised medals and pocket knives.
Name-calling, stealing flags, and animosity grew between the two groups, known as the Rattlers and the Eagles.
The losing team stole the winning team's medals and pocket knives after the tournament, leading to continued grudges and anger.
When the camp lost water, all the boys worked together to fix the pipes and even collaborated to pull a stuck pickup truck using a rope from a tug of war.
The act of pulling together to solve a common problem led to the insults and grudges disappearing, transforming enemies into friends.
Psychologists intentionally set up this experiment with similar groups to observe how shared struggles could unite people.
A similar study in Beirut in 1963 with mixed Muslim and Christian teams did not go as planned, showing that shared struggles might not always overcome deep divisions.
Beau suggests that when faced with a common struggle like a stuck truck or lack of water, people can unite despite their differences, transcending barriers like religion, skin tone, and borders.

Actions:

for community members,
Organize community events that involve collaboration and teamwork (implied)
Encourage diverse groups to work together on common goals or projects (implied)
Support initiatives that address shared struggles like poverty, hunger, or climate change (implied)
</details>
<details>
<summary>
2019-11-03: Let's talk with Re-Education about journeys and lefties.... (<a href="https://youtube.com/watch?v=D-pHZIEvfpc">watch</a> || <a href="/videos/2019/11/03/Lets_talk_with_Re-Education_about_journeys_and_lefties">transcript &amp; editable summary</a>)

Aaron's journey from a conservative with racist influences to an anarcho-communist advocate, criticizing misrepresentation and advocating for direct action.

</summary>

"Everybody is different, and they're all going to come to their positions in a different way."
"It's about working people against the system that is corrupted and pushing down on all of us."
"News crews have entire groups of people that deal with editing, they deal with sound, they deal with all of these sorts of things."
"A lot of the military back coups done by the United States or done by any other major superpower always involves a certain amount of people on the ground that are actually angry and that actually have real concerns."
"Putting physical bodies in physical spaces is the only way that we're going to actually see any sort of real reform, any sort of real change in the world."

### AI summary (High error rate! Edit errors on video page)

Aaron talks about his journey from being a conservative-leaning person to eventually becoming an anarcho-communist.
He shares how his upbringing in a community with racist influences led him to adopt harmful beliefs and associate with neo-Nazis.
Aaron recounts incidents of violence and vandalism he witnessed and participated in during his past.
He describes his transition to rejecting his previous beliefs and embracing a more inclusive and empathetic worldview.
Aaron credits skepticism and exposure to alternative arguments for helping him change his perspective.
He explains his views on communism, socialism, and anarcho-communism, and distinguishes between them.
Aaron criticizes the misrepresentation of socialism and communism in the United States.
He provides insights into the differences between communism, socialism, social democracy, and democratic socialism.
Aaron shares his support for worker-owned co-ops as a practical solution for societal issues under capitalism.
He expresses the challenges of discussing complex global issues like protests in China due to propaganda and lack of in-depth knowledge.

Actions:

for activists, advocates,
Join protests and demonstrations to physically show support for social change (implied).
Support worker-owned co-ops as a practical solution for societal issues under capitalism (exemplified).
</details>
<details>
<summary>
2019-11-03: Let's talk about solutions to complex problems and slogans.... (<a href="https://youtube.com/watch?v=fDGBH42GibY">watch</a> || <a href="/videos/2019/11/03/Lets_talk_about_solutions_to_complex_problems_and_slogans">transcript &amp; editable summary</a>)

Beau addresses the preference for simple solutions over complex issues, calling for critical thinking and deeper examination of root causes.

</summary>

"Soliciting a thing of value from a foreign national is illegal, period, full stop."
"The only reason they'd do that is if it had value."
"The average American is not dumb."
"We'd rather cheerlead than think."
"If you understand history, you can understand the future."

### AI summary (High error rate! Edit errors on video page)

Americans prefer simple solutions for complex problems, but real issues require thought, planning, and multiple tactics.
Trump exploited perceived ignorance by proposing a wall as a simplistic solution to a complex issue.
The wall's failure resulted in wasted money, disrupted lives, and damaged America's image.
Pelosi's fear stems from underestimating Americans' ability to understand complex issues, especially regarding campaign finance laws.
Soliciting anything of value from a foreign national for a campaign is illegal, regardless of the specifics.
Americans are capable of understanding complex issues but often choose to defer to authority rather than think critically.
Beau calls for a deeper examination of history and policies to address root causes instead of relying on simple solutions.

Actions:

for american citizens,
Challenge yourself and others to think critically about complex issues (implied)
Educate yourself on history and policies to understand root causes (implied)
</details>
<details>
<summary>
2019-11-03: Let's talk about pronouns and grammar.... (<a href="https://youtube.com/watch?v=uOmj5Bva0YA">watch</a> || <a href="/videos/2019/11/03/Lets_talk_about_pronouns_and_grammar">transcript &amp; editable summary</a>)

Beau dives into the history of the English language and questions the necessity of rewriting it for one person's preference, advocating for the historically used gender-neutral pronoun "they."

</summary>

"We wouldn't really be able to read it today unless we were trained to do so."
"For the overwhelming majority of the history of the English
language, we had a gender neutral pronoun, a singular gender neutral pronoun, they."
"It's not actually rewriting the English langauge to suit one person's personal preference."
"That's how it happened. So they, them, theirs, that's actually the norm."
"I guess we can rewrite the entire langauge because of one person's preference."

### AI summary (High error rate! Edit errors on video page)

Explains the history of the English language, starting with Old English in 450 to 1100 when Germanic people mixed with locals.
Describes the transition to Middle English from 1100 to 1500, influenced by French brought over by William the Conqueror.
Talks about early modern English from 1500 to 1800, with shorter vowels and more foreign words due to colonization.
Mentions late modern English from 1800 to now, with a wider vocabulary from the industrial and technological revolutions.
Details the development of prescriptive grammar in the 1700s, which dictated proper speech through books like Rob Lough's on English grammar.
Explains how Sir Charles Coutts' expansion on Lough's work included using "he" and "him" as generic pronouns, later ratified by Parliament in 1850.
Points out that historically, the English language had a singular gender-neutral pronoun, "they," for the majority of its history.
Criticizes the idea of rewriting the entire English language for one person's preferences, as it historically happened through influential figures like Coutts.
Ends by questioning the necessity of changing the entire English language for personal preferences.

Actions:

for english speakers,
Advocate for the use of gender-neutral pronouns like "they" in everyday speech (exemplified)
</details>
<details>
<summary>
2019-11-02: Let's talk about another question I wish wasn't being asked.... (<a href="https://youtube.com/watch?v=7S5dUoJXcmY">watch</a> || <a href="/videos/2019/11/02/Lets_talk_about_another_question_I_wish_wasn_t_being_asked">transcript &amp; editable summary</a>)

Beau addresses the rise in questions about arming oneself due to escalating anti-trans sentiment and provides critical advice on firearm ownership.

</summary>

"If you're dumb enough to pull a gun, you better be smart enough to pull the trigger."
"You're talking about purchasing a firearm for the express purpose of killing someone."
"If you're willing to put in the effort to learn how to do that, It's a good idea."
"When you learn how to use one to win, there's a new respect that comes along with it for the tool."
"I think we'd see less violence if people understood how to employ it better."

### AI summary (High error rate! Edit errors on video page)

Addressing the rise in questions about arming oneself due to increasing anti-trans sentiment, especially since the last presidential election.
Expressing feeling unsafe and contemplating buying a gun despite not being a "gun person" previously.
Mentioning the ease of purchasing a gun in their state and considering taking a safety class to handle it responsibly.
Responding to the question of whether LGBT individuals should arm themselves, stating it's not more likely to escalate violence against them.
Advising taking multiple safety courses until feeling extremely comfortable with handling a firearm.
Emphasizing the seriousness of owning a gun for the purpose of self-defense and the responsibility that comes with it.
Sharing advice on being certain of the intent before ever drawing a weapon in a potentially confrontational situation.
Noting the importance of understanding the tool and learning how to employ it effectively in combat situations.
Encouraging individuals to only purchase a firearm if they are willing to put in the effort to learn how to use it properly.
Mentioning the lack of understanding among many gun owners on how to use firearms beyond paper target shooting, stressing the need for training to reduce potential violence.

Actions:

for lgbt community,
Take multiple safety courses to handle firearms responsibly (suggested).
Put in the effort to learn how to use a firearm effectively in combat situations (suggested).
Understand the responsibility and seriousness that come with owning a gun for self-defense (implied).
</details>
<details>
<summary>
2019-11-02: Let's talk about Trump, Obama, gun control, and Florida man.... (<a href="https://youtube.com/watch?v=wbSOB-tFdXc">watch</a> || <a href="/videos/2019/11/02/Lets_talk_about_Trump_Obama_gun_control_and_Florida_man">transcript &amp; editable summary</a>)

Debunks the myth that Trump is a Second Amendment defender, contrasting his actions with Obama's and revealing surprising truths about gun control.

</summary>

"President Obama expanded gun rights. He wasn't a gun grabber."
"Trump, on the other hand, well, take the guns first. Worry about due process second."

### AI summary (High error rate! Edit errors on video page)

Debunks the widely held belief among older Republicans that Donald Trump is a defender of the Second Amendment.
Compares Trump's gun control actions to Obama's, revealing surprising facts.
Trump supported banning assault weapons, longer waiting periods, banning suppressors, universal background checks, and increased federal firearm prosecutions.
Trump set a record for federal firearm prosecutions, surpassing Obama and even Bush.
Obama received an F rating from the Brady campaign, indicating his stance on gun control.
Trump continued and expanded upon Obama's actions, releasing military surplus weapons and allowing guns in national parks.
Contrasts the perception of Obama as a gun grabber with his actual actions in expanding gun rights.
Criticizes Trump's stance on guns, quoting his willingness to "take the guns first, worry about due process second."
Draws parallels between Trump's approach to guns and his views on impeachment.
Concludes by challenging the perception of Trump as a defender of gun rights.

Actions:

for second amendment supporters,
Share this information with fellow Republicans to challenge misconceptions about Trump's stance on the Second Amendment. (implied)
</details>
<details>
<summary>
2019-11-01: Let's talk about NASA's new space travel plans.... (<a href="https://youtube.com/watch?v=DRgFRink3YE">watch</a> || <a href="/videos/2019/11/01/Lets_talk_about_NASA_s_new_space_travel_plans">transcript &amp; editable summary</a>)

NASA plans space exploration while a billion go hungry; Beau questions food distribution priorities.

</summary>

"We're on the cusp of a new adventure in humanity, conquering the stars."
"We produce 150% of what it would take to feed everybody."
"NASA can plan to put a crew of four on the moon for two weeks and plan to launch an interstellar probe, the rest of us, we can figure out how to move some food."

### AI summary (High error rate! Edit errors on video page)

NASA plans to put a crew of four on the moon for two weeks and launch an intentional interstellar space probe.
Despite producing 150% of the food needed to feed everyone, a billion people go hungry nightly due to food distribution issues.
Beau challenges the notion that there are logistical barriers to distributing food when transportation systems are already in place.
He questions the lack of incentive to distribute food efficiently, pointing out the benefits of stability and reduced defense spending.
Beau advocates for prioritizing basic needs like food over space exploration, suggesting it's a moral and monetary imperative.
He stresses the importance of addressing hunger beyond America's borders and treating all people equally.

Actions:

for global citizens,
Coordinate transportation networks to distribute excess food efficiently (implied)
Advocate for stable governments through food aid to reduce conflict and defense spending (implied)
</details>

## October
<details>
<summary>
2019-10-31: Let's talk about staying centered with Carey Wedler.... (<a href="https://youtube.com/watch?v=3ydf0cohiNU">watch</a> || <a href="/videos/2019/10/31/Lets_talk_about_staying_centered_with_Carey_Wedler">transcript &amp; editable summary</a>)

Carrie Wedler advocates for agorism over violent revolution, prioritizing emotional healing, mindfulness, and curiosity to navigate turbulent times and advocate for positive change.

</summary>

"My solution is not violence in the streets and taking down the government with guns as much as I support gun rights."
"I think this is a really sweet one, and I think it's so symbolic of new beginnings."
"Just start to become curious about what comes up for you."
"I'm banned from Twitter, but my ha ha ha, it's so funny that I'm banned from Twitter. It's hilarious."

### AI summary (High error rate! Edit errors on video page)

Started activism due to disillusionment with politics after supporting Obama in 2008 and realizing the continuation of wars.
Believes in evolution over revolution, advocating for agorism and peaceful protest against systemic injustices.
Trauma from her parents' divorce shaped her emotional responses, leading her to pursue healing and introspection.
Recommends yoga and meditation for maintaining calm amidst chaotic news cycles.
Emphasizes the importance of addressing deeper emotional wounds to prevent reactive responses.
Advocates for skepticism towards all media sources, both mainstream and independent, promoting critical thinking.
Plans to continue making videos challenging political authority and advocating for emotional exploration.
Suggests planting trees as a simple yet impactful solution to mitigate carbon dioxide and contribute positively to the environment.
Encourages curiosity and mindfulness towards emotional reactions as a pathway to healing and self-awareness.

Actions:

for activists, community members,
Plant trees or support organizations that focus on tree planting to mitigate carbon dioxide (implied)
Incorporate yoga and meditation practices into daily routines for emotional well-being (implied)
Cultivate curiosity and mindfulness towards emotional responses as a pathway to healing (implied)
</details>
<details>
<summary>
2019-10-31: Let's talk about Twitter banning political ads.... (<a href="https://youtube.com/watch?v=rplRofOFFKQ">watch</a> || <a href="/videos/2019/10/31/Lets_talk_about_Twitter_banning_political_ads">transcript &amp; editable summary</a>)

Twitter's ban on political ads shifts focus from money to ideas, empowering users to control the spread of information and potentially reducing the influence of campaign contributions.

</summary>

"Twitter has decided to ban political ads across its platform. That's cool."
"Ideas should stand and fall on their own."
"They can't use repetition as a weapon."
"I think it is going to help return a little bit of power to the average person if they use it."
"If those campaign contributions can't buy the platforms that swing elections, they're not that important."

### AI summary (High error rate! Edit errors on video page)

Twitter decided to ban political ads on its platform, signaling a shift towards valuing ideas over money as speech.
The move is seen as a step in the right direction, contrasting with Facebook's approach of exempting political ads from fact-checking.
Twitter aims for ideas to gain reach organically through user interaction rather than through paid promotion.
Paid advertising can make false information seem true through repetition, shaping beliefs regardless of facts.
Beau warns of potential loopholes where individuals with platforms may be influenced by politicians or supporters offering money to push certain agendas.
While some may argue this move limits free speech, Beau believes it actually empowers individuals to control the spread of ideas based on their interactions.
Supporting this change could potentially reduce the influence of campaign contributions and make politicians more accountable to the people.
Beau suggests following candidates and commentators on Twitter to ensure their ideas appear in your feed organically.
By promoting the worth of ideas over financial backing, social media platforms could diminish the impact of campaign contributions on elections.

Actions:

for social media users,
Follow candidates and commentators on Twitter to support their organic reach (implied).
</details>
<details>
<summary>
2019-10-30: Let's talk about a dark and scary night and life after Trump.... (<a href="https://youtube.com/watch?v=e34LFUNheYY">watch</a> || <a href="/videos/2019/10/30/Lets_talk_about_a_dark_and_scary_night_and_life_after_Trump">transcript &amp; editable summary</a>)

Dark and stormy nights make it easier for predators to harm, but after Trump, the community must build a better society by guiding and pushing the next leaders.

</summary>

"Dark and stormy nights is when it happens, even today."
"We're waiting for our dark and stormy night to do it."
"It's not just resistance, it's not destroying the ideas that are harmful, it's building the ideas that are helpful."
"It's up to us."

### AI summary (High error rate! Edit errors on video page)

Dark and stormy nights are when real monsters, meaning other men, roam the earth to harm people.
The conditions during dark and stormy nights, like rain, limited visibility, and dulled senses, make it easier for predators to attack.
Groups of indoctrinated men are ingrained with the idea of never quitting and always accomplishing the mission.
After Trump leaves office, the real work begins for the community to build the society they want.
The next president won't have to do much to be better than Trump, so it's up to the community to push, guide, and lead them.
It's not just about resistance; it's about building helpful ideas and creating power structures for a better society.

Actions:

for community members,
Guide, push, and lead the next president to build a better society (implied)
Create local power structures to influence change (implied)
</details>
<details>
<summary>
2019-10-30: Let's talk about Republicans supporting the troops..... (<a href="https://youtube.com/watch?v=ZNmsGvWBdfU">watch</a> || <a href="/videos/2019/10/30/Lets_talk_about_Republicans_supporting_the_troops">transcript &amp; editable summary</a>)

Beau questions GOP's support for troops, criticizes their actions towards veterans, and urges viewers to look beyond political talking points.

</summary>

"Maybe they don't really support the troops."
"They care about appearances."
"You should stop listening to the talking points and actually look at what they do."

### AI summary (High error rate! Edit errors on video page)

Questioning the GOP's supposed support for troops.
Contrasting the treatment of Tongo-Tongo ambush and Benghazi incident.
Republicans walking out of a bipartisan bill for women veterans due to amendments.
Creation of whistleblower protection office in the VA not protecting whistleblowers.
Accusing Republicans of caring more about appearances than veterans.
Criticizing Trump's actions regarding veterans and the VA.
Criticizing Republicans for siding with Trump over Lieutenant Colonel testimony.
Suggesting that recent actions indicate Republicans don't truly support the troops.
Pointing out the GOP's stance on gun rights and restrictions under Trump compared to Obama.
Encouraging viewers to look beyond political talking points and observe actions.

Actions:

for veterans, political activists,
Contact organizations supporting veterans' rights (implied)
Join advocacy groups for whistleblowers in the VA (implied)
Organize community events to raise awareness about mistreatment of veterans (implied)
</details>
<details>
<summary>
2019-10-28: We talked about impeachment.... (<a href="https://youtube.com/watch?v=_VJoSOPUQVc">watch</a> || <a href="/videos/2019/10/28/We_talked_about_impeachment">transcript &amp; editable summary</a>)

Beau explains the constitutional validity of the impeachment process, dismantles misleading analogies, and warns against unchecked presidential power.

</summary>

"The impeachment process is exactly nothing like a lynching."
"They have to attack the foundations of the country."
"It’s not a coup. It’s the way the process is laid out."
"This argument that is being made is the legal argument to turn Trump into a dictator."
"Trump becomes Tsar, and all men are slaves to the Tsar."

### AI summary (High error rate! Edit errors on video page)

Explains that the impeachment process is not unconstitutional and follows the rules outlined in the Constitution.
Clarifies the difference between impeachment proceedings and a criminal prosecution.
Addresses the comparison made by President Trump and Senator Lindsay Graham to a lynching, explaining the inaccuracy of the analogy.
Points out that the impeachment proceedings are not a criminal prosecution, so due process applies at a later stage.
Describes the closed-door nature of the impeachment proceedings to prevent witnesses from coordinating stories.
Criticizes Republican lawmakers for storming the impeachment inquiry and obstructing the process.
Raises concerns about the legal argument that the President is immune from all criminal liability, potentially leading to unchecked power and abuse.

Actions:

for activists, constitutional scholars, concerned citizens,
Contact elected officials to express support for upholding constitutional processes (suggested)
Educate others on the differences between impeachment proceedings and criminal prosecutions (exemplified)
Organize community forums to raise awareness about the implications of unchecked presidential power (implied)
</details>
<details>
<summary>
2019-10-28: Let's talk about bagging the Big Baghdadi Wolf.... (<a href="https://youtube.com/watch?v=3NhSJ4SdA4A">watch</a> || <a href="/videos/2019/10/28/Lets_talk_about_bagging_the_Big_Baghdadi_Wolf">transcript &amp; editable summary</a>)

The big bad wolf may be captured, but disrupting command structures without clarity on succession can have dangerous consequences, as history shows.

</summary>

"It's the end, right? They're defeated. Maybe not."
"If they take out all of them, if they get rid of all of the politically savvy people, you have an incredibly dangerous organization."
"You didn't defeat us."

### AI summary (High error rate! Edit errors on video page)

The big bad wolf has been captured, but the organization may not be entirely defeated.
Disrupting command structures can be risky if the next leaders are unknown.
Historical examples, like the Irish Rebellion of 1916, show the consequences of eliminating leadership without a clear plan for succession.
Two key figures, Devalera and Collins, exemplify the different types of leaders within organizations.
The strategy of eliminating only one faction within an organization to render it combat ineffective but still intact.
The goal is to force politically savvy individuals towards peace negotiations due to incompetent military leadership.
The U.S. strategy may involve targeting key figures to destabilize organizations.
The operation against the captured individual was expedited due to the president's actions.
Success in neutralizing competent commanders will lead to minimal activities from the group.
Failure to eliminate key figures may result in infighting or the rise of a more dangerous organization.

Actions:

for strategists, policymakers, analysts.,
Analyze the leadership structures within organizations to understand potential outcomes (implied).
Monitor for signs of infighting or power struggles within groups (implied).
Stay informed about international events to understand the implications of strategic decisions (implied).
</details>
<details>
<summary>
2019-10-28: Let's talk about a baseball chant, a hashtag, Chicago, and Trump.... (<a href="https://youtube.com/watch?v=lVfLKfPMsdU">watch</a> || <a href="/videos/2019/10/28/Lets_talk_about_a_baseball_chant_a_hashtag_Chicago_and_Trump">transcript &amp; editable summary</a>)

President's approval should be soaring, facing backlash from veterans, baseball stadium booing, and Chicago demonstration, urging for inflammatory rhetoric to understand dangers of political climate and prevent descent into dictatorship.

</summary>

"Be patriotic. This is un-American. Don't do this. Just sit there, be a good German. It'll all work out. No, we know how that works out. We're not doing it again."
"You're sending us there."
"I'm not going to be a person that just follows orders."

### AI summary (High error rate! Edit errors on video page)

President's approval should be soaring due to recent wins but it's not; facing backlash from veterans, baseball stadium booing, and Chicago demonstration.
Media is trying to pacify Americans, labeling dissent as un-American and unpatriotic.
Beau argues that inflammatory rhetoric is necessary to understand the dangers of the current political climate.
Warning against the potential for a dictatorship if civil liberties are disregarded.
Beau criticizes conservatives for only now opposing dangerous rhetoric when it's aimed at them.
The time for civil debate ended when Trump's lawyers argued he was above the law in court.
Beau believes it's time for people to understand the reality of the current political situation and not shy away from inflammatory debate.
Criticism towards those who followed Trump blindly, selling out the country for a red hat and slogan.
Emphasizes the importance of using strong rhetoric to prevent a descent into dictatorship.
Beau expresses readiness to stand against authoritarianism, even if it means going beyond rhetoric.

Actions:

for concerned citizens,
Challenge dangerous rhetoric and stand against authoritarianism (exemplified)
Engage in open, honest, and potentially inflammatory debate to address the current political situation (exemplified)
</details>
<details>
<summary>
2019-10-28: Let's talk about #TeamTrees.... (<a href="https://youtube.com/watch?v=f5xaL23roXg">watch</a> || <a href="/videos/2019/10/28/Lets_talk_about_TeamTrees">transcript &amp; editable summary</a>)

Mr. Beast collaborates with Arbor Day Foundation to plant 20 million trees by December 2022, setting a goal of raising $20 million, already reaching $5 million in two days.

</summary>

"Your donation to them [Arbor Day Foundation] would be tax deductible in most cases."
"Definitely kind of jumping on team trees here."
"It's actually going to create some meaningful impact."

### AI summary (High error rate! Edit errors on video page)

Mr. Beast, a 21-year-old YouTuber known for silly stunts, is involved in a unique project to raise $20 million for planting trees.
Partnered with the Arbor Day Foundation, the goal is to plant 20 million trees across the country by December 2022.
The foundation, with a 50-year history, will ensure that each dollar donated equals one tree planted.
The project aims to plant native species, promoting environmental impact.
Within just two days, $5 million has already been raised for the cause.
Beau encourages viewers to donate to the Arbor Day Foundation, mentioning that donations may be tax-deductible.
Beyond donating, Beau suggests planting native fruit trees in personal yards to enhance food security, reduce grocery bills, and foster community spirit.
Beau acknowledges the project's success in engaging a wide audience, with various creative videos showcasing the tree-planting efforts.
Unlike typical online challenges, this initiative is praised for its potential significant impact on the environment.
Beau urges viewers to support the cause by contributing a dollar or two.

Actions:

for environmental enthusiasts, tree advocates,
Donate a dollar or two to the Arbor Day Foundation to support tree-planting efforts (suggested).
Plant native fruit trees in your yard to enhance food security, reduce grocery bills, and build community spirit (implied).
</details>
<details>
<summary>
2019-10-26: Let's talk about a little hope and solutions.... (<a href="https://youtube.com/watch?v=uhzuYC5M6yo">watch</a> || <a href="/videos/2019/10/26/Lets_talk_about_a_little_hope_and_solutions">transcript &amp; editable summary</a>)

Beau addresses the prevalence of negativity, challenges the reliance on future leaders, and calls for individual action and unity to bring about real change.

</summary>

"We have everything we need. It's us."
"It's us, the individuals down here on the bottom, the commoners that are going to solve it."

### AI summary (High error rate! Edit errors on video page)

Addressing solutions and how to reach them.
Noting the prevalence of negative content online.
Emphasizing the importance of identifying problems.
Acknowledging the lack of action despite watching videos.
Stating the impact of even a few individuals taking action.
Describing the constant presence of negativity in daily life.
Pointing out issues like environmental degradation and corruption.
Challenging the belief that the next leader will solve everything.
Calling for leadership over rulership.
Asserting that each person has the capacity to lead and solve problems.
Expressing gratitude for Patreon support.
Planning to expand the podcast with diverse guest speakers.
Emphasizing the need for collaboration and unity for real change.

Actions:

for community members,
Reach out to diverse individuals to join in discussing and seeking solutions (suggested)
Collaborate with people from various backgrounds to address today's issues (implied)
Come together with fellow community members to drive real change (implied)
</details>
<details>
<summary>
2019-10-25: Let's talk about the US deploying more troops to Syria.... (<a href="https://youtube.com/watch?v=LCGFQcvE3eQ">watch</a> || <a href="/videos/2019/10/25/Lets_talk_about_the_US_deploying_more_troops_to_Syria">transcript &amp; editable summary</a>)

The US is sending more troops and armor to Syria, contradicting claims of withdrawal, revealing ulterior motives, and risking complicity in ethnic cleansing.

</summary>

"Sending armor to Syria contradicts the idea of a withdrawal."
"More troops are being sent than withdrawn. It's about money and creating a U.S. zone."
"The President is complicit in ethnic cleansing by allowing Turkish military forces in."
"It's not anti-imperialist or anti-colonialist, just packaged differently for supporters."
"We sold out powerful Middle Eastern actors for questionable foreign policy decisions."

### AI summary (High error rate! Edit errors on video page)

The US is sending armor, including tanks, to Syria to protect a gas plant, contradicting the idea of a withdrawal.
Sending tanks without ground troops makes them vulnerable to anti-tank missiles in Syria.
Ground troops, maintenance crews, supply guys, infantry, surveillance units, drones, and jets are needed for this operation.
Despite claims of de-escalation, more troops are being sent to Syria than withdrawn.
The true motive behind these actions is money and creating a U.S. zone in northeastern Syria.
The President suggested Kurds move to oil regions, complicit in ethnic cleansing by allowing Turkish military forces in.
The actions taken do not represent anti-imperialism or anti-colonialism but serve other interests.
The US is prioritizing protecting gas plants despite becoming a net energy exporter soon.
Beau criticizes the administration for selling out powerful Middle Eastern actors for questionable foreign policy decisions.

Actions:

for activists, policymakers.,
Contact policymakers to express opposition to military actions in Syria (suggested).
Support organizations advocating for the protection of vulnerable groups in conflict zones (exemplified).
Join local movements opposing questionable foreign policy decisions (implied).
</details>
<details>
<summary>
2019-10-25: Let's talk about Katie Hill.... (<a href="https://youtube.com/watch?v=vzs8ggPww94">watch</a> || <a href="/videos/2019/10/25/Lets_talk_about_Katie_Hill">transcript &amp; editable summary</a>)

Beau points out the double standard faced by women in politics through the scandal surrounding Congressperson Hill's photos, urging a shift in priorities towards more critical issues.

</summary>

"Women are still held to this false idea, this standard that they need to remain pure and that they need to behave in a certain way."
"None of this is any of our business."
"This country has its priorities mixed up."
"It's just a thought."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Congressperson Hill's scandalous photos surfaced, sparking a national debate.
The photos show adults engaged in consensual activity and Hill using a substance that was once illegal.
Hill has a tattoo that may have been an iron cross, a symbol associated with various groups.
She is the first bisexual representative from California and these photos confirm her admission.
There's ethical concern about her dating an employee, although not illegal.
Beau questions why these photos are circulating; suggests it's due to Hill being a pretty woman.
He challenges the double standard women face regarding purity and behavior.
Beau points out the lack of harm in Hill's actions and questions the public interest.
He criticizes the misplaced priorities of focusing on these photos rather than more critical issues.
Beau contrasts the attention on Hill's photos with the President's lawyers arguing for his right to kill people.

Actions:

for political activists,
Support organizations advocating for gender equality and against slut-shaming (implied)
Engage in critical discourse on gender bias and double standards in politics (implied)
</details>
<details>
<summary>
2019-10-25: Let's talk about AOC vs DOD.... (<a href="https://youtube.com/watch?v=GKDbGc3g9KA">watch</a> || <a href="/videos/2019/10/25/Lets_talk_about_AOC_vs_DOD">transcript &amp; editable summary</a>)

AOC and the Department of Defense present contrasting reports on climate change, with experts warning of imminent threats and the urgent need for action.

</summary>

"AOC is right. The environmentalists are right and the Department of Defense is co-signing that."
"This is a real threat. The people who are paid to decide what threats are, they say it is."
"It's your choice. You can listen to the Department of Defense, NASA, the Defense Intelligence Agency, environmental scientists."

### AI summary (High error rate! Edit errors on video page)

AOC and the Department of Defense released dueling reports on climate change.
AOC's report predicts rising sea levels displacing 600 million people, infectious diseases spreading, decreased food security, extreme weather incidents, and stress on the power grid.
The Department of Defense report, with input from NASA and the Defense Intelligence Agency, describes climate change as a huge threat to be dealt with immediately.
The U.S. Army is considering a culture change to reduce their carbon footprint and create fresh water from humidity for warfighters.
Beau fabricated AOC's report to draw attention to the seriousness of climate change and the need for action.
The Department of Defense, NASA, and environmental scientists all warn about the real threat of climate change.
Beau contrasts listening to experts versus dismissing climate change as fake news promoted by oil companies.

Actions:

for climate activists, policymakers,
Contact local policymakers to advocate for climate action (implied)
Join environmental organizations working towards climate solutions (implied)
Support initiatives that reduce carbon footprint in your community (implied)
</details>
<details>
<summary>
2019-10-24: Let's talk about the President's legal argument against all criminal liability.... (<a href="https://youtube.com/watch?v=Ce3f_2dV7sA">watch</a> || <a href="/videos/2019/10/24/Lets_talk_about_the_President_s_legal_argument_against_all_criminal_liability">transcript &amp; editable summary</a>)

President's legal team argued for total immunity, implying Trump could commit crimes without consequences, potentially paving the way for unchecked dictatorial powers.

</summary>

"He is total immunity from all forms of criminal liability."
"This argument that is being made is the legal argument to turn Trump into a dictator with absolute power."
"It's terrifying because that's not how it's being framed."
"Trump becomes Tsar, and all men are slaves to the Tsar."
"Meanwhile he's attempting to gain the power to do whatever he wants with no legal ramifications."

### AI summary (High error rate! Edit errors on video page)

President's legal team argued for total immunity from criminal liability.
Even judge found the argument bizarre and proposed a scenario of the president shooting someone on Fifth Avenue.
The legal argument implies the president could commit murder without facing any consequences.
Lack of criminal liability could allow the president to shut down voting stations or commit voter fraud with impunity.
Impeachment by Congress may not be a safeguard if the president has absolute power.
The argument aims to grant Trump dictatorial powers with no checks or balances.
Trump could become a dictator with unchecked authority if the argument is accepted.
Judges deciding on the president's immunity from criminal liability are essentially determining the fate of the country.
Granting total immunity could lead to the abuse of power and Trump acting as a Tsar.
This legal argument threatens to give Trump unrestricted power and evade legal consequences.

Actions:

for american citizens,
Contact elected representatives to express concerns about the implications of granting total immunity to the president (implied).
</details>
<details>
<summary>
2019-10-24: Let's talk about secret impeachment hearings and cop shows.... (<a href="https://youtube.com/watch?v=8yRxM-Z1tpo">watch</a> || <a href="/videos/2019/10/24/Lets_talk_about_secret_impeachment_hearings_and_cop_shows">transcript &amp; editable summary</a>)

Beau explains the rationale behind secret impeachment hearings and why attacking the process is the Republican's only defense strategy.

</summary>

"The hearings are in secret so the other witnesses don't know what's been said already. It's a way to keep them honest."
"They're trying to suppress evidence because they can't defend the president's actions. It's really that simple."

### AI summary (High error rate! Edit errors on video page)

Explains the reason behind holding impeachment hearings in secret compared to past cases like Clinton and Nixon where facts were already established.
Compares the secretive hearings to a cop show where witnesses are separated to prevent collaboration in testimonies.
Mentions that leaking testimonies is akin to a cop revealing information to suspects to test their honesty.
Notes that the Republicans attack the process because they cannot defend the President's actions.
Draws a parallel between defense strategies in criminal cases and the Republicans trying to suppress evidence.
Concludes by stating that the testimonies in the current impeachment process are critical compared to past cases where events were clear.

Actions:

for political enthusiasts, voters, activists,
Contact your representatives to advocate for transparent and fair impeachment proceedings (suggested)
Stay informed about the impeachment process and share accurate information with others (implied)
</details>
<details>
<summary>
2019-10-23: Let's talk about the foreign policy we should have and the one we do.... (<a href="https://youtube.com/watch?v=id54nN0Lh90">watch</a> || <a href="/videos/2019/10/23/Lets_talk_about_the_foreign_policy_we_should_have_and_the_one_we_do">transcript &amp; editable summary</a>)

Beau delves into the essence of foreign policy, advocating for promoting life and self-determination while critiquing the dominance-focused current approach.

</summary>

"Life, liberty, pursuit of happiness, and self-determination."
"Ideas stand and fall on their own."
"You have to continue to evolve your position based on the reality."
"A whole bunch of people are going to lose their life, liberty, pursuit of happiness and self-determination."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Beau introduces the topic of foreign policy and questions the foreign policy the country should have.
He refers to the Declaration of Independence, focusing on life, liberty, pursuit of happiness, and self-determination as fundamental principles.
Beau points out that promoting happiness through enforcement is impractical in foreign policy.
He distinguishes between freedom and liberty, noting that not everyone values liberty over safety.
Beau advocates for promoting life and self-determination in universal foreign policy.
He contrasts the ideal foreign policy focused on promoting life and self-determination with the current dominance-oriented foreign policy.
Beau uses the example of Syria to illustrate how foreign policy stances should evolve based on changing situations while prioritizing life and self-determination.
He criticizes actions that go against the principles of life, liberty, pursuit of happiness, and self-determination in foreign policy decisions.
Beau stresses the importance of evolving positions based on reality and new information in foreign policy advocacy.
He concludes by reminding viewers to adapt their opinions as situations evolve and to prioritize the well-being and rights of individuals in foreign policy decisions.

Actions:

for policy advocates, activists,
Advocate for foreign policies that prioritize life and self-determination (advocated)
Evolve positions based on changing realities and new information in foreign policy advocacy (advocated)
</details>
<details>
<summary>
2019-10-23: Let's talk about the President's tweet and Senator Graham.... (<a href="https://youtube.com/watch?v=xg_LnDyhKmE">watch</a> || <a href="/videos/2019/10/23/Lets_talk_about_the_President_s_tweet_and_Senator_Graham">transcript &amp; editable summary</a>)

Beau clarifies the misuse of the term "lynching" in political discourse and sheds light on the historical and constitutional inaccuracies surrounding it.

</summary>

"The impeachment process is exactly nothing like a lynching."
"The closest thing to a lynching that President Trump has been involved in is when he took out a full-page ad advocating for the execution of people who turned out to be innocent."
"Over 3,400 recorded lynchings of Black Americans occurred."

### AI summary (High error rate! Edit errors on video page)

President mentioned Emmett Till, leading to the term "lynching" surfacing in political discourse.
Joe Biden used the term 20 years ago, but it was still wrong.
Republicans claimed impeachment proceedings were like a lynching, which is false.
Senator Lindsey Graham echoed this sentiment, showing ignorance of history and the Constitution.
Graham failed to understand that due process in impeachment occurs during the Senate trial.
The impeachment inquiry is not extrajudicial; it follows a constitutional process.
Wealthy individuals can't halt investigations, unlike what some may be accustomed to.
President Trump's involvement in an actual lynching-like scenario was advocating for the execution of innocent people.
Lynchings are not ancient history, with the most recent recorded case in 2011.
Over 3,400 recorded lynchings of Black Americans occurred, dispelling the idea that it's ancient history or insignificant in magnitude.

Actions:

for politically aware individuals,
Educate others on the historical significance and brutality of lynchings (implied)
</details>
<details>
<summary>
2019-10-23: Let's talk about storming the impeachment inquiry.... (<a href="https://youtube.com/watch?v=eWH0_TE_JaE">watch</a> || <a href="/videos/2019/10/23/Lets_talk_about_storming_the_impeachment_inquiry">transcript &amp; editable summary</a>)

Republican lawmakers disrupt impeachment inquiry, prioritizing party over Constitution and justice.

</summary>

"They care more about their party than they do the Constitution."
"They care more about their party than you."

### AI summary (High error rate! Edit errors on video page)

Republican lawmakers stormed the impeachment inquiry to disrupt the process.
McConnell instructed them to attack the process instead of defending the President's actions.
Closed-door proceedings, like the one in question, are common and not unusual.
Those excluded from the hearing are either on the wrong committees or lack security clearance knowledge.
The double standard exists as normal individuals engaging in similar actions face severe consequences compared to lawmakers.
The President's lawyer argued for immunity from prosecution, suggesting a disregard for the law.
The focus on attacking the process signifies a prioritization of party over upholding the Constitution and justice.

Actions:

for politically engaged individuals,
Challenge political double standards and demand accountability for actions (implied)
Stay informed and advocate for transparency in political processes (implied)
</details>
<details>
<summary>
2019-10-22: Let's talk about goats, confusion, oil, and novels.... (<a href="https://youtube.com/watch?v=pKCurn1bO94">watch</a> || <a href="/videos/2019/10/22/Lets_talk_about_goats_confusion_oil_and_novels">transcript &amp; editable summary</a>)

US foreign policy in Syria under Trump: predictable chaos or calculated moves?

</summary>

"It is completely predictable if you accept the one assessment all of US intelligence agreed upon from the very beginning."
"Either he's a complete idiot or he's a useful one."
"We're living in a Tom Clancy novel and the President of the United States is compromised."

### AI summary (High error rate! Edit errors on video page)

US troops may stay in Syria to protect oil fields, contradicting arguments of supporters.
Troops leaving Syria are just moving to Western Iraq, waiting to go back.
Trump is perceived as anti-interventionist but allows US troops to protect natural resources in Syria.
Despite being labeled a peace lover, Trump threatens war with Iran.
Critics find Trump's foreign policy unpredictable, but Beau argues it is completely predictable.
Betraying allies and inability to recruit indigenous forces overseas have negative effects.
US actions benefit non-state actors and strengthen certain groups in Syria.
US decisions have made it harder to gain trust and support from other countries.
Russia can use US actions as propaganda against other nations.
Turkey's meeting with Russia shows a shift in power dynamics in the Middle East.
Beau questions whether Trump is incompetent or a useful tool in shaping foreign policy.

Actions:

for foreign policy analysts,
Question US foreign policy decisions (implied)
Stay informed on geopolitical shifts and implications (implied)
</details>
<details>
<summary>
2019-10-21: We talked about Trump's betrayal of the Kurds.... (<a href="https://youtube.com/watch?v=P3Eg-BvRUJg">watch</a> || <a href="/videos/2019/10/21/We_talked_about_Trump_s_betrayal_of_the_Kurds">transcript &amp; editable summary</a>)

The President's decision to allow Turkey's invasion of Syria and abandon Kurdish allies reveals the harsh realities of war and foreign policy.

</summary>

"War is just a continuation of politics by other means, right? It has always been like this."
"The Department of Defense has become the world's largest private military contractor."
"The only thing that's happening over there right now is waste."
"The American people. I got a feeling we're going to have to look at what we've done."
"We are not going to use and abuse indigenous forces like this and to just hang them out to dry when it's politically expedient."

### AI summary (High error rate! Edit errors on video page)

The President of the United States allowed Turkey to invade Syria, abandoning Kurdish allies who have fought alongside American soldiers for 15 years.
Turkish-backed militants committed atrocities against the Kurds, driving them to seek support from Russia.
The Kurds are a significant ally in the Middle East, not a country but an ethnic group who have been betrayed and sold out openly.
The U.S. military moved a small number of troops, enabling Turkey's invasion of Syria, leading to Russian and Syrian troops filling the power vacuum left by U.S. forces.
ISIS could potentially grow stronger due to recent events, posing a threat that might lead to U.S. military intervention in the future.
The term "special forces" refers specifically to Green Berets, who train indigenous forces and stand out as instructors in hostile environments.
The Department of Defense is seen as a private military contractor, with the U.S. military viewed as serving Saudi Arabia's interests in the Middle East.
War is depicted as a profitable and vicious enterprise that exposes soldiers to being bought and sold for money and power.
The current situation in the Middle East is characterized by waste, with slogans like "End the war" and "Anti-imperialism" failing to match the reality of the conflict.
The speaker calls for a reevaluation of how indigenous forces are used and discarded for political convenience.

Actions:

for american citizens,
Support organizations advocating for the protection and rights of indigenous forces (implied)
Advocate for a more transparent and accountable foreign policy that prioritizes ethical considerations (implied)
</details>
<details>
<summary>
2019-10-21: Let's talk about what we can learn from Lego bricks.... (<a href="https://youtube.com/watch?v=_R33lcUUs4g">watch</a> || <a href="/videos/2019/10/21/Lets_talk_about_what_we_can_learn_from_Lego_bricks">transcript &amp; editable summary</a>)

Beau at Legoland compares society to Lego sculptures, urging individuals to think for themselves and build a more inclusive community.

</summary>

"It might be time for bricks in the local community to start connecting and start building stuff on their own."
"The power is in the brick."
"Most bricks have an innate desire to create, rather than destroy."

### AI summary (High error rate! Edit errors on video page)

Took a break and visited Legoland with his kids, describing it as an amusement park with Lego-themed massive sculptures like elephants and dragons.
Compares Legoland sculptures to society and history, where the focus is often on individuals rather than the collective effort of every "brick."
Talks about how society often directs individuals on what to build, leaving out those who don't conform, and suggests it's time for "bricks" to start thinking for themselves.
Points out that the power lies in individuals connecting and building together, creating more colorful and inclusive images if they build for themselves.
Encourages local communities to connect, build their own images, and create something different and useful for all instead of just conforming to existing structures.

Actions:

for local community members,
Connect with local community members to start building projects together (implied)
Encourage creativity and inclusivity in community building efforts (implied)
</details>
<details>
<summary>
2019-10-21: Let's talk about steel and Emmett Till.... (<a href="https://youtube.com/watch?v=x5Iiw39Jbbc">watch</a> || <a href="/videos/2019/10/21/Lets_talk_about_steel_and_Emmett_Till">transcript &amp; editable summary</a>)

Beau delves into the story of Emmett Till, from his tragic murder to his enduring legacy represented by a new bulletproof steel sign.

</summary>

"His legacy was bulletproof, his mom was still unwavering, unflinching, just like that new sign."
"It doesn't matter if somebody shoots the sign or spray paints it or takes it. It's there. It's carved in stone and blood in American history."

### AI summary (High error rate! Edit errors on video page)

Introduction to discussing Emmett Till and his legacy with a focus on a new sign made of steel.
Emmett Till, a 14-year-old boy, was murdered in 1955 for allegedly whistling at a white woman in Mississippi.
The woman whose accusation led to Emmett Till's murder later admitted she fabricated parts of the story, and the accused men were acquitted.
Emmett Till's mother displayed incredible strength by having an open casket funeral for her son, revealing the brutal reality of his murder.
Photos of Emmett Till's body were published and became a catalyst for the civil rights movement.
Rosa Parks credited Emmett Till with strengthening her resolve in the fight for civil rights.
The acquittal of Emmett Till's murderers marked a turning point where the Black community realized they couldn't rely on the courts for justice.
Emmett Till's legacy continues to inspire, symbolized by a new bulletproof sign made of steel in his honor.
The significance of the steel sign lies in its resilience, mirroring the unwavering strength of Emmett Till's mother and the enduring impact of Emmett Till's story on American history.

Actions:

for history enthusiasts, social justice advocates,
Visit historical sites related to civil rights movements (implied)
Support organizations working towards racial justice (implied)
</details>
<details>
<summary>
2019-10-18: Let's talk about generals, business, and a coup.... (<a href="https://youtube.com/watch?v=_cd9317BgKM">watch</a> || <a href="/videos/2019/10/18/Lets_talk_about_generals_business_and_a_coup">transcript &amp; editable summary</a>)

General Butler's refusal to lead a coup against FDR prevented a potential fascist dictatorship in the U.S., showcasing the immense impact of individual choices on history.

</summary>

"One guy refused to take command. One guy refused. One person. Literally changed the entire course of human history."
"Assuming that the allegations are true, the world owes General Butler the entire world."

### AI summary (High error rate! Edit errors on video page)

Beau dives into the story of General Butler, who was a highly respected Marine with extensive experience in various wars.
FDR's progressive reforms didn't sit well with the wealthy business class, leading them to plan a coup to install a fascist government.
Wealthy and powerful figures like J.P. Morgan and Prescott Bush were implicated in the coup plan.
McGuire acted as an intermediary, gradually escalating the plan to have Butler lead a march on Washington and seize control.
Butler, being a staunch anti-capitalist, was the wrong choice for the coup leaders.
Butler exposed the plan by going straight to Congress, but newspapers dismissed it as a hoax initially.
Even though the truth came out later, no one was prosecuted for the coup attempt.
The coup attempt serves as a reminder of the dangers of political power plays and the importance of individuals' choices.
If Butler had agreed to lead the coup, the course of history could have been drastically altered.
The story underscores the significance of one person's decision in shaping the world's future.

Actions:

for history enthusiasts, advocates for democracy.,
Support and defend democratic institutions (exemplified).
Stay vigilant against threats to democracy (exemplified).
</details>
<details>
<summary>
2019-10-17: Let's talk about the constitutionality of impeachment.... (<a href="https://youtube.com/watch?v=H3tk7EMOmpY">watch</a> || <a href="/videos/2019/10/17/Lets_talk_about_the_constitutionality_of_impeachment">transcript &amp; editable summary</a>)

Beau explains the constitutionality of impeachment, debunking claims of unconstitutionality and clarifying the process with a focus on indictment and limited punishment.

</summary>

"The House has sole power over impeachment proceedings."
"There is nothing unconstitutional occurring."
"It's not a coup. It's the way the process is laid out."

### AI summary (High error rate! Edit errors on video page)

Explains the constitutionality of impeachment amid claims of unconstitutionality.
Emphasizes the impeachment process as the indictment phase.
Quotes Article 3 Section 2, which stipulates that impeachment trials are not criminal prosecutions.
Clarifies the limited punishment the Senate can impose: removal from office and disqualification from holding future office.
Points out that the President's right to pardon does not apply in cases of impeachment.
Mentions the requirements for the Senate during impeachment trials: under oath, Chief Justice presides, and a two-thirds majority is needed to convict.
Stresses that the President's right to confront accusers applies in a criminal trial, not impeachment.
Asserts that there is nothing unconstitutional about the current impeachment proceedings.
Addresses claims of denial of due process by explaining its inapplicability at this stage.
Concludes by debunking notions of a coup and affirming the legitimacy of the impeachment process.

Actions:

for citizens, activists, voters,
Stay informed on the impeachment proceedings and the constitutional aspects involved (suggested).
Educate others on the impeachment process and the constitutional guidelines (suggested).
</details>
<details>
<summary>
2019-10-17: Let's talk about black community defense groups.... (<a href="https://youtube.com/watch?v=T3A6hqI-Oxg">watch</a> || <a href="/videos/2019/10/17/Lets_talk_about_black_community_defense_groups">transcript &amp; editable summary</a>)

Beau advocates for establishing community defense organizations with a focus on community development, stressing the importance of winning the PR battle to avoid negative portrayal and scrutiny.

</summary>

"Creating that redundant power, creating that localized power structure is really important."
"The PR battle is what's gonna be important here."
"The first battle is a PR battle."
"And I don't think that they will become a target as long as they win the PR war up front."
"The US has changed, it's gotten better, but it hadn't changed that much."

### AI summary (High error rate! Edit errors on video page)

There is a call to action in minority communities post the Jefferson shooting, where a woman was shot by a cop in her own home.
The call to action is to establish a community defense force, reminiscent of the original days of the Black Panther Party.
Beau advocates for localized community defense organizations for all communities, not just particular racial or ethnic groups.
He believes such organizations can avoid the pitfalls the Black Panthers faced and should focus on community development beyond just defense.
Beau suggests avoiding a militant structure like the Black Panthers had to prevent negative media portrayal and law enforcement scrutiny.
Winning the PR battle is critical for such organizations to avoid being labeled negatively by the media or law enforcement.
Beau acknowledges the concerns and challenges but believes that with proper messaging and community support, these organizations can succeed.
He encourages the establishment of organizations that create redundant and localized power structures to benefit communities.
Beau stresses the importance of steering away from a paramilitary image to maintain a positive perception and prevent becoming a target.
The focus should be on community development activities like microloans, mentoring, childcare, and skill development within these organizations.

Actions:

for community organizers,
Establish localized community defense organizations with a focus on community development (implied)
Ensure proper messaging to win the PR battle and prevent negative portrayal (implied)
Seek support from organizers within the community for setting up these organizations (exemplified)
</details>
<details>
<summary>
2019-10-16: Let's talk about fallout from the President's decision.... (<a href="https://youtube.com/watch?v=dDRSXmd9LAQ">watch</a> || <a href="/videos/2019/10/16/Lets_talk_about_fallout_from_the_President_s_decision">transcript &amp; editable summary</a>)

Eight days after the president's decision led to chaos in Syria, US forces moved for Turkey to invade, potentially resulting in catastrophic consequences if not contained immediately.

</summary>

"This will go down as the worst foreign policy decision in American history."
"It is spitting on the grave of every U.S. soldier who died in the last 15 years over there."
"All because the president would not listen to people that knew more than him."

### AI summary (High error rate! Edit errors on video page)

Eight days since the president's decision, fallout is evident.
US forces moved for Turkey to invade Syria, leading to atrocities by Turkish-backed militants.
Kurds turned to Russia for support, with Russian and Syrian troops filling the power vacuum.
ISIS staged jailbreaks, potentially growing stronger.
US forces nearly hit by artillery from a NATO ally.
Air Force scrambling to move 50 nuclear weapons from Incirlik Air Force Base.
White House discussing nuclear weapons before removal is alarming.
Turkish perspective sees potential superpower status by overtaking US base.
B61 nuclear weapon can yield up to 340 kilotons of power.
Beau warns of catastrophic impact if such weapons are used, referencing Hiroshima.

Actions:

for policy advocates, global citizens,
Contact policymakers to urge immediate containment of the situation (suggested)
Support organizations aiding those affected by the fallout in Syria (exemplified)
</details>
<details>
<summary>
2019-10-15: Let's talk about growing and changing as a person.... (<a href="https://youtube.com/watch?v=nrzNTJaq1rw">watch</a> || <a href="/videos/2019/10/15/Lets_talk_about_growing_and_changing_as_a_person">transcript &amp; editable summary</a>)

Beau advises on moving forward without explaining past beliefs, embracing growth, and not letting the past define you.

</summary>

"You cannot allow your past to define you."
"People change. If you don't look back on the person you were [...] and just cringe at how stupid you were, you're not growing as a person."
"Everybody believed something problematic at one point in time. And everybody still does."
"Growing as a person is a lot like those PT requirements."
"Just continue growing and showing that through actions and people will understand."

### AI summary (High error rate! Edit errors on video page)

Received a message about past racist beliefs and desire to make a positive impact.
Encourages moving forward without explaining past beliefs to unchanged friends.
Acknowledges past problematic beliefs and ongoing growth for everyone.
Emphasizes exposure to new perspectives as catalyst for change.
Shares personal experience of evolving views on accessibility.
Urges continuous learning and evolving perspectives as a natural process.
Compares personal growth to physical training - setting new goals and getting stronger mentally.
Assures that growth is ongoing and that past mistakes do not define a person.
Mentions example of ex-Nazi YouTuber who changed for the better.
Stresses the importance of reflecting on past beliefs and embracing personal growth.

Actions:

for individuals seeking guidance on personal growth and overcoming past beliefs.,
Continuously expose yourself to new perspectives and information (implied).
Embrace personal growth by setting new goals and evolving perspectives (implied).
Show through actions that you have evolved from past beliefs (implied).
</details>
<details>
<summary>
2019-10-15: Let's talk about John Bolton's closet.... (<a href="https://youtube.com/watch?v=FHy9JYILFvg">watch</a> || <a href="/videos/2019/10/15/Lets_talk_about_John_Bolton_s_closet">transcript &amp; editable summary</a>)

Beau reveals John Bolton's shady past and questions his sudden ethical objection to Trump administration's actions, cautioning against turning him into a hero.

</summary>

"They're true believers. Bolton is a true believer."
"True believers are the most dangerous people on the planet."
"Even in his most noble act, deceit was at its core."

### AI summary (High error rate! Edit errors on video page)

Surprised by John Bolton's ethical objection to the Trump administration's actions.
Bolton referred to Trump administration's actions as a "drug deal."
Bolton previously covered up the Iran Contra affair as assistant attorney general in the 80s.
Claimed happiest moment was getting the U.S. out of the International Criminal Court to protect U.S. war criminals.
Accused of derailing a conference on stopping weapons of mass destruction and later using that as a pretext for invasion.
Allegations of blackmailing the chief of the Organization for the Prohibition of Chemical Weapons.
Describes Bolton as a true believer who rationalizes his actions to protect American interests.
Three options presented: Bolton grew a conscience, Trump's actions were too shady even for Bolton, or the actions truly jeopardized national security.
Beau criticizes Bolton's role in American foreign policy and warns against turning him into a hero even if he brings down Trump.
Concludes by pointing out Bolton's deceit in not coming forward about the transgressions he knew of.

Actions:

for viewers, activists, critics,
Hold officials accountable for their actions (implied)
</details>
<details>
<summary>
2019-10-14: Let's talk about Trump's use of the military.... (<a href="https://youtube.com/watch?v=whTgLSevdc8">watch</a> || <a href="/videos/2019/10/14/Lets_talk_about_Trump_s_use_of_the_military">transcript &amp; editable summary</a>)

Beau criticizes the commodification of US military forces, exposing how they are being viewed as mercenaries for hire rather than defenders of national security.

</summary>

"Deploying US military forces overseas should not be elective. It should be the only alternative."
"The might of the U.S. military, well, it's on the market. It can be bought."
"War as a racket always has been. It is possibly the oldest, easily the most profitable, surely the most vicious."
"US servicemen have been bought and sold for a very, very long time. It's just normally not done this cheaply."
"The Department defense has become the world's largest private military contractor."

### AI summary (High error rate! Edit errors on video page)

Sitting with a recently separated veteran who was upset after watching the President of the United States brag about accepting payment to deploy US military forces overseas.
Criticizing the notion that recent actions were about bringing troops home, pointing out that it was more about anti-imperialism and enabling other countries' ambitions.
Expressing concern that the US military is now seen as a mercenary force for Saudi Arabia due to financial agreements.
Questioning the morality of deploying US military forces overseas as an elective option that can be bought, rather than a duty required for national security and freedom.
Mentioning the uncertainty surrounding Trump's statements and the potential impact on how the US military is perceived in the Middle East.
Indicating that US soldiers are now viewed as mercenaries being bought and sold, serving as bait for conflicts in the Middle East.
Noting the historical perspective that war has always been intertwined with money and power, with soldiers essentially being mercenaries.
Quoting General Butler, a highly decorated Marine, who referred to himself as a "gangster for capitalism" and acknowledged serving as muscle for big business interests.
Describing how the Department of Defense has effectively become a private military contractor, sending troops overseas based on financial incentives rather than national interests.
Suggesting that individuals considering joining the military should be aware that private firms may offer better pay and more competent leadership.

Actions:

for veterans and anti-war advocates.,
Contact organizations supporting veterans' rights and well-being (implied).
Educate yourself on the impacts of militarization in foreign policy (generated).
Advocate for policies that prioritize national security over financial incentives (implied).
</details>
<details>
<summary>
2019-10-14: Let's talk about Kennedy's unrealized legacy and dreams.... (<a href="https://youtube.com/watch?v=A1qOu5rEIeQ">watch</a> || <a href="/videos/2019/10/14/Lets_talk_about_Kennedy_s_unrealized_legacy_and_dreams">transcript &amp; editable summary</a>)

President Kennedy's legacy, from the Peace Corps to community networks, inspires a vision of people helping people beyond borders and government involvement.

</summary>

"Ask not what society can do for you, but ask what you can do for society."
"Just people helping people."
"Choose to do it. Not because it's easy, but because it's hard."

### AI summary (High error rate! Edit errors on video page)

President Kennedy is often mythologized as the most significant president in American history, associated with Camelot and the Golden Age.
Kennedy's famous quote "Ask not what your country can do for you, ask what you can do for your country" has a great impact.
Kennedy's vision aimed at using education programs to empower people to stand on their own.
The Green Berets, an organization that flourished under Kennedy, could have been utilized differently according to his original vision.
In 1960, Kennedy introduced the Peace Corps, envisioning Americans traveling globally to do good.
The Peace Corps, originally intended for humanitarian purposes, eventually became a tool for US foreign policy.
Beau advocates for a shift towards asking what you can do for society rather than focusing solely on what society can do for you.
He stresses the importance of community networks at the local level to build communities without government intervention.
Beau dreams of individuals helping each other across borders and without government interference, envisioning a world where people help people.
He encourages action towards building community networks and helping others without borders or government involvement.

Actions:

for global citizens,
Build community networks at the local level to empower communities (suggested)
Help others without borders or government intervention (implied)
</details>
<details>
<summary>
2019-10-13: Let's talk about the Ben Shapiro vs Beto feud.... (<a href="https://youtube.com/watch?v=H9h6uwxvuR4">watch</a> || <a href="/videos/2019/10/13/Lets_talk_about_the_Ben_Shapiro_vs_Beto_feud">transcript &amp; editable summary</a>)

Responding to sensationalist fear-mongering, Beau disapproves of violence as a solution, criticizing irresponsible rhetoric and advocating for responsible action.

</summary>

"Schools are the problem, pick up a gun."
"You should be embarrassed."
"It's not the solution. It's not even a problem."
"That violence is always the answer. It's not."
"If you keep trying to use that one tool, you're going to destroy your home."

### AI summary (High error rate! Edit errors on video page)

Responding to a trending campaign promise, Beau disapproves of the sensationalist rhetoric used by Ben Shapiro to incite fear.
Ben Shapiro creates a false dilemma by suggesting that exposing children to certain academic content will make them emulate it.
The slippery slope argument leads to an extreme scenario where armed men are at the door to take Shapiro's children, a situation Beau finds absurd.
Beau criticizes Shapiro's lack of responsible coverage and suggests using constitutional mechanisms to challenge laws instead of inciting fear.
Beau questions the masculinity-driven tough guy rhetoric that Shapiro employs, pointing out the real issue with using violence as a solution.
Beau admonishes Shapiro for irresponsibly advocating for picking up a gun as a solution to perceived problems with schools, calling it embarrassing and dangerous.
Violence is not always the answer according to Beau, who likens it to a tool that must be used responsibly to avoid destructive consequences.

Actions:

for internet users, viewers,
Challenge unconstitutional laws using constitutional mechanisms (implied)
</details>
<details>
<summary>
2019-10-12: Let's talk about President Trump, truth, and fiction.... (<a href="https://youtube.com/watch?v=Ow1fNC-oSMI">watch</a> || <a href="/videos/2019/10/12/Lets_talk_about_President_Trump_truth_and_fiction">transcript &amp; editable summary</a>)

Beau outlines the President's actions, suggesting they benefit Russia more than the United States, prompting consideration and investigation by journalists.

</summary>

"Every one of these moves benefits Russia."
"Maybe something to investigate."
"Y'all are the journalists."

### AI summary (High error rate! Edit errors on video page)

Beau expresses concern about being followed and introduces himself as Deep Goat.
He outlines the President's actions since taking office, focusing on various decisions and their impact.
The President withdrew from international treaties, diverted military resources to monitor migrants, and undermined different communities.
Beau mentions how the President's actions drove away valuable allies like the Kurds and disrupted international relationships.
The President's decisions include pulling out of the Iran deal, withholding security funds from Ukraine, and engaging in a trade war with China.
Beau implies that these actions have ultimately benefited Russia rather than the United States.
He does not explicitly label the President as a Russian asset but suggests that it's something worth investigating.

Actions:

for journalists,
Investigate the President's actions and their potential impacts on national interests (suggested)
</details>
<details>
<summary>
2019-10-11: Let's talk about China, parallels, technology, and traps.... (<a href="https://youtube.com/watch?v=04Edv148XiE">watch</a> || <a href="/videos/2019/10/11/Lets_talk_about_China_parallels_technology_and_traps">transcript &amp; editable summary</a>)

Beau introduces the issue of learning about cultures only after tragedies and delves into the parallels between China and the U.S. through the lens of the Uyghurs' plight, illustrating rapid cultural erasure through mass surveillance and internment camps.

</summary>

"We only learn about it when something bad happens to them."
"Beijing was worried about extremification."
"They're erasing the culture. They're erasing the ethnicity."
"Mass surveillance started. Eight years later there's camps."
"So that's a little brief primer on who they are."

### AI summary (High error rate! Edit errors on video page)

Beau introduces a comment about learning about cultures only when something bad happens to them, specifically mentioning at-risk cultures.
The parallel between China and the United States is discussed, focusing on technology becoming a trap through the example of the Uyghurs.
The Uyghurs, residing in Xinjiang, China, have a rich history and were part of the Republic of East Turkestan before becoming part of China in 1949.
The Uyghurs practice Sufism, a mystical component of Islam, which fundamentalists may not approve of due to its newer traditions and rituals.
Beijing's uncomfortable relationship with the Uyghurs intensified after some Uyghurs were found in a training camp in Afghanistan, leading to riots, bombings, and a security clampdown in the 1990s and early 2000s.
Beijing justified their actions as benevolent civilizing efforts, but they actually subjugated and relocated the Uyghurs, bringing in millions of Han Chinese for settlement.
Mass surveillance on the Uyghurs began in 2016 with technologies like smartphones, DNA scanning, facial recognition, and more, under the guise of preventing extremism.
A million adults are currently in camps termed "vocational centers" by China, facing erasure of their culture and ethnicity through forced assimilation.
Uyghur children are being taught Chinese culture instead of their own, further erasing their identity.
Beau draws parallels between the Uyghur experience and the native experience in the United States, reflecting on the rapid progression from technological advancements to mass internment camps.

Actions:

for activists, humanitarians,
Contact human rights organizations to support the Uyghur cause (suggested)
Join protests or awareness campaigns advocating for the rights of the Uyghur community (exemplified)
Support initiatives providing aid to Uyghur refugees or families affected by the crisis (implied)
</details>
<details>
<summary>
2019-10-10: Let's talk about an interview with a Special Forces officer and one word.... (<a href="https://youtube.com/watch?v=oNv0fjY4qjQ">watch</a> || <a href="/videos/2019/10/10/Lets_talk_about_an_interview_with_a_Special_Forces_officer_and_one_word">transcript &amp; editable summary</a>)

A Special Forces officer's interview sheds light on the realities and emotions behind military actions, urging the American people to confront their government's decisions and treatment of indigenous forces.

</summary>

"To free the oppressed."
"We did it because we have allowed our government to get out of control."
"They are not robots. They have emotions."
"Their actions, if they happened, are completely excusable."
"This is probably the time to address it."

### AI summary (High error rate! Edit errors on video page)

Special Forces officer's interview on Fox News raised eyebrows.
Special Forces are green berets, not Seals or rangers.
They teach, weaponize education, and train indigenous forces.
Special Forces motto: "To free the oppressed."
Their image in popular culture stems from misconceptions.
Special Forces are intelligent and not like robots.
Officer on Syrian Turkish border expressed displeasure with senior command and commander-in-chief's decision.
Officer used the word "atrocities" to describe events happening worldwide.
American people need to be ready for what may be revealed.
Officer criticized senior command and commander-in-chief's decisions in the interview.
Elite unit members may disregard orders if witnessing atrocities.
Special Forces are not robots; they have emotions and connections with local forces.
Actions of Special Forces should not be a reflection on them but on the Oval Office.
The American people need to address the use and abuse of indigenous forces.
A moment of truth is approaching for the American people to confront their actions worldwide.

Actions:

for american citizens,
Spread awareness about the true nature of Special Forces and their emotions (suggested)
Advocate for the fair treatment of indigenous forces (suggested)
Start a discourse on government accountability and responsible decision-making (suggested)
</details>
<details>
<summary>
2019-10-10: Let's talk about a UBI study.... (<a href="https://youtube.com/watch?v=EH83zaWikI4">watch</a> || <a href="/videos/2019/10/10/Lets_talk_about_a_UBI_study">transcript &amp; editable summary</a>)

Beau talks UBI, referencing a Stockton study on $500 monthly payments to 125 individuals, hinting at the transformative potential of large-scale implementation and importance of public opinion, particularly from younger generations like Lily.

</summary>

"Being in poverty is a lack of cash, not a lack of character."
"They're the ones that are going to implement it, or not. They're the ones that will live with it."
"Revolutionary ideas are coming out; this is an old idea. But implementing it on a scale that Yang is talking about, that's a pretty big undertaking."
"People have said it before, being in poverty is a lack of cash, not a lack of character."
"Matters what Lily thinks."

### AI summary (High error rate! Edit errors on video page)

Wishes a happy birthday to a young viewer before discussing UBI and Andrew Yang.
Mentions an 18-month study in Stockton, California, where 125 people received $500 a month, a boost for Yang's $1000 plan.
Researchers from the University of Tennessee and University of Pennsylvania label the study as anecdotal, focusing more on storytelling than gathering hard data due to the limited sample size.
Study aims to measure impact on mental and physical health, with results pending release.
Breakdown of recipients: 43% working, 2% unemployed, 8% retired, 20% disabled, and 10% stay-at-home caregivers.
Spending breakdown: 40% on food, 24% at stores like Walmart, Dollar General, 11% on utilities, 9% on auto repairs and gas, and 16% on medical expenses and insurance.
Beau acknowledges the limitations of the sample size but appreciates the storytelling aspect supporting Yang's plan.
Emphasizes the importance of younger generations like Lily in implementing such ideas that could revolutionize society.
Acknowledges concerns about people's spending habits and stereotypes related to poverty.
Concludes by hinting at the transformative potential of large-scale UBI implementation and the significance of public opinion, particularly from younger individuals like Lily.

Actions:

for policy advocates, ubi supporters,
Advocate for UBI implementation in your community (implied)
Support research on UBI impacts on mental and physical health (implied)
Challenge stereotypes about poverty and spending habits (implied)
</details>
<details>
<summary>
2019-10-09: Let's talk about unmatched wisdom, slogans, and waste.... (<a href="https://youtube.com/watch?v=U3CaDZRHo4k">watch</a> || <a href="/videos/2019/10/09/Lets_talk_about_unmatched_wisdom_slogans_and_waste">transcript &amp; editable summary</a>)

Toy soldiers line up, slogans overshadow lethal consequences, waste prevails - a bad move here means mass graves there.

</summary>

"Ended US imperialism because when Turkey and the US got together and decided how best to carve up a chunk of a third country, that was anti-imperialism."
"What's a bad move over here is a mass grave over there."
"The only thing that's happening over there right now is waste."

### AI summary (High error rate! Edit errors on video page)

Toy soldiers lined up, game pieces on the board, watching the live play-out from a safe distance.
Two groups of people ready to face off, at the mercy of their superiors, playing "Masters of the Universe."
Turkish troops preparing to cross the border, Kurds needing more troops as theirs are occupied guarding suspected IS fighters.
Guards from facilities likely to join the front, increasing the risk of jailbreaks.
President's claim of defeating a side contradicted; potential for U.S. intervention.
Slogans and campaign points overshadowing the disastrous consequences of decisions.
Expansion of war and fresh combatants, not the end of imperialism.
Decision's immediate fallout overlooked, resulting in lethal consequences overseas.
Bad moves in one place lead to mass graves in another.
The reality of waste amidst slogans and talking points.

Actions:

for concerned citizens,
Contact organizations providing aid to affected regions (implied)
Support initiatives offering assistance to refugees and displaced individuals (implied)
</details>
<details>
<summary>
2019-10-09: Let's talk about the Kurds as Americans see them.... (<a href="https://youtube.com/watch?v=EhuYF5vgX68">watch</a> || <a href="/videos/2019/10/09/Lets_talk_about_the_Kurds_as_Americans_see_them">transcript &amp; editable summary</a>)

Beau provides historical context on the Kurds, criticizes shallow discourse, and calls for meaningful support beyond memes.

</summary>

"I think we can muster a little more support than just sharing a meme on Facebook."
"There's 30 million people out there without a country."
"We'd rather string them along in hopes of it, rather than actually do something that could promote peace."
"I think this time we think beyond memes and realize there's 30 million people out there without a country."
"I don't think that arguing with other commentators does a lot of good."

### AI summary (High error rate! Edit errors on video page)

Received a screenshot of a tweet referencing his bias in favor of the Kurds due to wearing a Kurdish scarf in a video about them.
Explains that he tries to remain objective but acknowledges his bias.
Disagrees with starting debates with other commentators, believing they reinforce existing ideas rather than lead to productive discourse.
Expresses understanding and frustration over a tweet questioning the sudden interest in the Kurds by those who previously showed no concern for them.
Provides historical context on the Kurds, dating back to the 7th century, their struggles with colonial powers, and the lack of recognition of their independence.
Points out the unique aspect of Kurdish identity, where nationality comes before religion, unlike in many other Middle Eastern countries.
Addresses common questions about the Kurds, including their religious diversity and presence across multiple countries like Syria, Turkey, Iraq, Iran, and Armenia.
Talks about the geopolitical significance of the Kurds, their desire for independence, and how they are viewed by Western powers.
Criticizes the notion that Turkey is strategically vital to the US, questioning the importance of air bases and troop numbers in NATO.
Emphasizes the true strategic value lies in assets on the ground, like human intelligence and local support, which the Kurds provide.
Expresses concern over the US's treatment of the Kurds and how it may push them towards other superpowers like Russia.
Condemns the lack of action in granting the Kurds a homeland and promoting peace in the region.
Advocates for recognizing the agency of Kurdish people in determining their future rather than oppressing them through other nations.
Criticizes the American public's shallow knowledge of the Kurds, reduced to memes about their governance and attractive women warriors.
Calls for more substantial support for the Kurds beyond social media sharing, considering the significant population without a homeland.

Actions:

for advocates and activists,
Educate yourself on the history and struggles of the Kurdish people (suggested).
Support organizations advocating for Kurdish rights and independence (implied).
Raise awareness about the plight of the Kurds in your community (implied).
</details>
<details>
<summary>
2019-10-08: Let's talk about a lockdown with no locks.... (<a href="https://youtube.com/watch?v=Acxto-W5of4">watch</a> || <a href="/videos/2019/10/08/Lets_talk_about_a_lockdown_with_no_locks">transcript &amp; editable summary</a>)

Beau explains psychological effects, praises proactive students during a lockdown without locks, and challenges misconceptions about safety protocols.

</summary>

"The purpose of a lockdown is to get as many people as possible somewhere secure to maintain their safety."
"You made the right decision with the information you had at hand."
"A lockdown is a guideline. It's there to keep you safe, okay?"
"You get somewhere safe."
"It's not there to order you to sit in a room that's unlocked."

### AI summary (High error rate! Edit errors on video page)

Introduces psychological effects: Milgram experiments and bystander effect.
Milgram experiments show people tend to follow authority figures.
Bystander effect means people are less likely to act in a group.
California incident at Cal State, Long Beach involved a lockdown without locks.
Some students barricaded doors during the lockdown.
Others did nothing and waited in unlocked rooms.
Beau praises proactive students who took action.
He criticizes those who did nothing during the lockdown.
Beau challenges law enforcement's approach to lockdowns.
Urges schools to install locks immediately for safety.
Warns that the school without locks is now a target.
Emphasizes that lockdowns are guidelines for safety, not suicide pacts.

Actions:

for students, school staff, community members,
Install locks immediately in facilities without adequate security (implied)
Take proactive measures during emergencies (implied)
</details>
<details>
<summary>
2019-10-08: Let's talk about Trump's next move if he's smart.... (<a href="https://youtube.com/watch?v=M4KoqH9-1QY">watch</a> || <a href="/videos/2019/10/08/Lets_talk_about_Trump_s_next_move_if_he_s_smart">transcript &amp; editable summary</a>)

Beau criticizes Trump's handling of diplomatic situations, suggests releasing phone call audio, and warns of a negative legacy if action isn't taken.

</summary>

"Release the audio of those phone calls."
"He's scared of everything."
"You didn't vote for the guy who's going to hide in the White House."
"He made the U.S. military look the way it does now."
"The base is going to want to see him out there taking these hoaxsters and scamsters to task."

### AI summary (High error rate! Edit errors on video page)

Criticizes Trump's administration and advisors for giving bad advice.
Talks about the chain of events starting in Turkey and Syria, where Trump ordered troops out of the way, leading to a betrayal of allies who fought alongside American soldiers.
Points out that Trump's base defended him, but it made Trump look worse than Obama in handling diplomatic situations.
Mentions the obstruction of a State Department employee from testifying in the impeachment inquiry.
Suggests that Trump release the audio of phone calls and testify before the House to set the record straight.
Warns that Trump's legacy will be worse than Obama's if he doesn't take action.

Actions:

for political activists,
Release the audio of phone calls and testify before the House (suggested)
Hold hoaxsters and scamsters accountable (implied)
</details>
<details>
<summary>
2019-10-07: Let's talk about the UK, apologies, the Kurds, and friends.... (<a href="https://youtube.com/watch?v=qyG910MsU4s">watch</a> || <a href="/videos/2019/10/07/Lets_talk_about_the_UK_apologies_the_Kurds_and_friends">transcript &amp; editable summary</a>)

An American diplomat's wife avoids accountability using diplomatic immunity while the US betrays its Kurdish allies in Syria, revealing a deeper theme of establishment versus the people.

</summary>

"It's always portrayed as left versus right, as your demographic versus another equal or lesser maybe just above demographic. At the end of the day, it's not what it is."
"It's establishment versus you. They will always write you off to protect themselves. It's always what happens."
"George Carlin said it best. It's a big club and you ain't in it."

### AI summary (High error rate! Edit errors on video page)

An American diplomat's wife, protected by diplomatic immunity, caused the death of a 19-year-old in the UK by driving the wrong way.
The family of the victim is left with no closure as she used immunity to return to the US without facing consequences.
The Kurds, a vital ally of the US in the Middle East, are being betrayed as the US greenlights Turkey's incursion into Syria.
Despite the Kurds' loyalty and support to the US, they are at risk during this invasion with no country of their own.
The US is actively supporting the invasion, unlike past betrayals that were more discreet.
Beau connects these seemingly unrelated events, revealing a deeper theme of establishment versus the people.
He criticizes the establishment for always protecting themselves at the expense of the people.

Actions:

for global citizens,
Contact local representatives to advocate for justice for victims of diplomatic immunity (implied).
Support organizations aiding Kurds and raise awareness about their plight (implied).
</details>
<details>
<summary>
2019-10-06: Let's talk about G.I. Joe, an old quote, and Trump.... (<a href="https://youtube.com/watch?v=DdkoyU1Zdsg">watch</a> || <a href="/videos/2019/10/06/Lets_talk_about_G_I_Joe_an_old_quote_and_Trump">transcript &amp; editable summary</a>)

Beau points out dangerous parallels between historical power dynamics and current events, warning against the implications of targeting individuals before crimes exist.

</summary>

"Show me the person and I'll show you the crime."
"Doing it any other way is extremely dangerous."
"Find a person that we want to hang, and then we go find the crime to hang on them."
"Show me the man, I'll show you the crime."
"He is accused of doing some pretty horrible things using that thought process."

### AI summary (High error rate! Edit errors on video page)

Takes a day off to play with kids, recalls G.I. Joe show from the 80s and its plot dynamics.
Draws parallels between G.I. Joe's preemptive actions against Cobra and real-life situations.
Mentions Lavrenty Beria, a powerful figure in the Soviet Union, known for his infamous quote.
Criticizes President involving himself in corruption cases without identifiable crimes.
Questions the process of finding a person to hang and then looking for a crime to pin on them.
Talks about the dangerous thought process of "Show me the man, I'll show you the crime."
References Beria's role as chief of the NKVD secret police and his history of heinous actions.

Actions:

for those concerned about justice.,
Investigate and stay informed about cases where individuals are targeted without identifiable crimes (implied).
Advocate for due process and fair investigations in political matters (implied).
</details>
<details>
<summary>
2019-10-04: Let's talk about the Biden-Ukraine timeline and journalism 101.... (<a href="https://youtube.com/watch?v=Q4XvIM046po">watch</a> || <a href="/videos/2019/10/04/Lets_talk_about_the_Biden-Ukraine_timeline_and_journalism_101">transcript &amp; editable summary</a>)

Beau breaks down the Biden-Ukraine timeline, debunks misconceptions, and stresses the need for concrete evidence in journalism.

</summary>

"If you are attempting to pass something off as objective and you have a bias, you should disclose it to your readers or your viewers, that is Journalism 101."
"You need evidence, physical evidence, call logs, emails, something."
"Unless there's physical evidence that you can present, there's no story here."
"You need evidence to change this. And it might be out there."
"But you need evidence, not any window."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic of the Biden-Ukraine event and the importance of journalism in the narrative.
Talks about disclosure in journalism and the basic principles of Journalism 101.
Explains the relevance of laying out a timeline in investigative journalism.
Provides a detailed timeline of events related to the Biden-Ukraine issue, starting from 2012.
Addresses the misconception surrounding the investigation into Biden's son and the energy company.
Mentions the concerns about corruption in Ukraine and the role of various Western powers in addressing it.
Describes Vice President Biden's involvement in dealing with corruption issues in Ukraine.
Points out the bipartisan effort within the United States to address corruption in Ukraine.
Talks about the change in prosecutors in Ukraine in 2016 and the reopening of the investigation.
Emphasizes the importance of evidence in journalism and refutes claims without substantial proof.
Concludes by stressing the need for physical evidence to support any narrative changes.

Actions:

for journalism students, truth-seekers,
Fact-check information shared on social media platforms (implied)
Support investigative journalism by subscribing to reputable sources (implied)
</details>
<details>
<summary>
2019-10-04: Let's talk about climate change cuisine.... (<a href="https://youtube.com/watch?v=1QkrOiTWpeY">watch</a> || <a href="/videos/2019/10/04/Lets_talk_about_climate_change_cuisine">transcript &amp; editable summary</a>)

AOC faces absurd suggestion at town hall, revealing Republican reactions, fake news, and deeper racial implications of climate change fears, while Beau advocates for hope and rational solutions.

</summary>

"There's always hope."
"Their solution is eat the babies."
"If you look at the climate issue and you think the solution is population and not consumption? Well, one, you're evil. Two, you're racist."
"Fake news has hit the president yet again."
"y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

AOC held a town hall where unfiltered questions were asked, a departure from the norm of politicians listening only to those who pay them.
During the town hall, a woman suggested eating babies to save the climate, sparking wild reactions from Republicans.
The woman was later revealed to be a La Roche PAC operative, not a genuine proposal.
Republicans caring about human life after birth due to this incident was noted.
President Trump joined in calling AOC names and spreading fake news, without verifying facts.
AOC responded with composure and hope, focusing on the goal of reaching net zero emissions.
Beau criticizes those who panic about climate change to the point of advocating depopulation as a solution.
He points out the flawed logic of prioritizing population control over consumption reduction, calling it evil and racist.
Beau recommends a video by YouTuber Mexi for further insight into the racial undertones of this issue.
Despite the prevalence of fake news, Beau ends on a note of hope and encourages further reflection on the topic.

Actions:

for climate change advocates,
Watch the video by YouTuber Mexi to gain further insights into racial undertones related to climate change (suggested).
</details>
<details>
<summary>
2019-10-03: Let's talk about recessions and headlines.... (<a href="https://youtube.com/watch?v=vimH_tZWCHg">watch</a> || <a href="/videos/2019/10/03/Lets_talk_about_recessions_and_headlines">transcript &amp; editable summary</a>)

Beau contradicts claims of no recession, pointing to historical indicators and political motivations to delay economic downturn for electoral reasons, implying Trump's policies hastened the impending recession.

</summary>

"They don't want you to think about how they might have tanked the economy."
"A recession is natural in some ways, a lot of times."
"It's really that simple."
"I am not a trained economist."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Participated in a telethon for the Trevor Project.
Addressed questions about an upcoming recession.
Contradicts headlines claiming no recession is imminent.
Cites headlines from 2007-2008 before the last recession.
Manufacturing report indicates lowest levels since the previous recession.
Suggests the current administration aims to delay recession until after the election.
Points out the natural occurrence of recessions after periods of growth.
Expresses belief that Trump's economic policies contributed to the impending recession.
Speculates that the government aims to maintain consumer confidence to delay recession.
Acknowledges not being a trained economist and presents his views as thoughts.

Actions:

for voters, concerned citizens.,
Stay informed about economic indicators and historical patterns (implied).
Support policies that prioritize long-term economic stability over short-term gains (implied).
</details>
<details>
<summary>
2019-10-03: Let's talk about Trump on the South Lawn.... (<a href="https://youtube.com/watch?v=LZ0YVdiV_Ks">watch</a> || <a href="/videos/2019/10/03/Lets_talk_about_Trump_on_the_South_Lawn">transcript &amp; editable summary</a>)

Beau breaks down Trump's impeachment support, his response, and the irony of fake news in politics.

</summary>

"45%, those are rookie numbers, we need to pump those numbers up."
"I guess Trump saw that and was like, you know what, 45%, those are rookie numbers, we need to pump those numbers up."
"The man that told actual journalists that they were fake news is going to be brought down by faking it."
"And they say poetic justice is dead. It's not. It's alive and well."
"You can't write stuff like this."

### AI summary (High error rate! Edit errors on video page)

Shares the chain of events in the morning following a poll on impeachment support.
Trump's response to the poll results by soliciting interference in the election live on TV.
Beau expresses his belief that Trump's actions are impeachable.
Mentions Trump being a victim of the fake news media.
Compares the Biden theory to a fabricated storyline about JFK's assassination involving Jackie Kennedy.
Criticizes right-wing conspiracy sites for spreading misinformation.
Talks about Giuliani showing the fabricated story to the president, who acts on it.
Points out the irony of Trump being brought down by fake news after attacking journalists for the same.
Beau humorously comments on the situation, suggesting everyone could use a good laugh.

Actions:

for political observers,
Fact-check and verify information before sharing (implied)
</details>
<details>
<summary>
2019-10-03: Let's talk about Botham and justice.... (<a href="https://youtube.com/watch?v=-N2MKhD1GVQ">watch</a> || <a href="/videos/2019/10/03/Lets_talk_about_Botham_and_justice">transcript &amp; editable summary</a>)

Beau addresses disappointment in falling short of perceived justice, societal conditioning to accept subpar outcomes, racial sentencing disparities, and the challenging forgiveness displayed by victims' families.

</summary>

"Disappointed. Disappointed."
"We're conditioned now to accept something that is less than okay, and just be like, well it's not right, but it's better than I was expecting."
"Sentencing disparity is a very, very real thing."
"I yield to them."
"They are better people than I am."

### AI summary (High error rate! Edit errors on video page)

Addressing the aftermath of a certain event and the feelings surrounding it, particularly disappointment.
Expressing disappointment at falling short of perceived real justice by a slim margin.
Noting society's tendency to accept less-than-ideal outcomes as long as they exceed expectations.
Sharing personal views on prison being rehabilitative, disapproving of mandatory minimums and non-violent crime incarcerations.
Acknowledging a leaning towards retribution over justice for violent crimes, especially of a certain magnitude.
Acknowledging racial sentencing disparities and the role of income inequality in access to quality legal representation.
Speculating that being a police officer may have influenced the leniency of the sentence.
Mentioning potential challenges for the convicted individual in a prison environment not welcoming to law enforcement.
Contemplating whether the outcome constitutes justice and deferring to the victim's family for that judgment.
Acknowledging the family's disappointment and anger while also appreciating their forgiveness and ability to embody compassion.
Expressing difficulty in comprehending the level of forgiveness displayed by Botham's brother towards the convicted individual.

Actions:

for justice seekers,
Stand against sentencing disparities by supporting legal aid organizations that work towards fair representation for all (implied).
Advocate for prison reform that focuses on rehabilitation rather than punishment (implied).
Show empathy and support for victims' families in their pursuit of justice and healing (implied).
</details>
<details>
<summary>
2019-10-02: Let's talk about reactions to the Guyger verdict.... (<a href="https://youtube.com/watch?v=PicasjT5mSE">watch</a> || <a href="/videos/2019/10/02/Lets_talk_about_reactions_to_the_Guyger_verdict">transcript &amp; editable summary</a>)

Beau reacts to the Dallas verdict, criticizing leniency advocates in a case where a woman fatally shot an unarmed person in his own home, stressing the responsibility that comes with owning firearms.

</summary>

"A lack of situational awareness does not provide grounds for lethal force."
"If she was confused and that disoriented, she shouldn't have been armed."
"What she says doesn't matter."
"Rights come with responsibilities."
"Y'all have me second-guessing my stance on gun control."

### AI summary (High error rate! Edit errors on video page)

Reacting to the verdict of a trial in Dallas where a woman entered the wrong apartment and fatally shot an unarmed person.
Expresses surprise at the satisfactory verdict regarding the incident.
Observes a divide among gun owners in their reactions to the case, particularly regarding gun control.
Criticizes those who advocate for leniency in this case, stating they are not responsible enough to own firearms.
Emphasizes the indisputable facts of the case: the woman entered someone else's home and killed an unarmed person.
Argues that confusion or lack of situational awareness does not justify the use of lethal force.
Points out the disregard for safety protocols and responsibilities in handling a firearm.
Challenges justifications for leniency based on the perpetrator's background.
Questions what the reaction might have been if the demographics were reversed in the case.
Concludes that rights, such as owning a weapon, come with responsibilities.

Actions:

for gun owners,
Reassess your stance on gun control and gun ownership (exemplified).
Challenge biases and prejudices, especially in cases of racial implications (exemplified).
</details>
<details>
<summary>
2019-10-01: Let's talk about engaging the youth and survival.... (<a href="https://youtube.com/watch?v=5pBXMov6lNU">watch</a> || <a href="/videos/2019/10/01/Lets_talk_about_engaging_the_youth_and_survival">transcript &amp; editable summary</a>)

Teach survival skills to youth by making it engaging, fun, and tailored to their interests, promoting critical thinking and problem-solving while spending quality time together as a family.

</summary>

"Teach them the skills. Make it fun."
"If you can survive a zombie apocalypse, you can certainly survive a hurricane."
"Just make it fun and for God's sakes do not try to toughen up your kid."

### AI summary (High error rate! Edit errors on video page)

Techniques to teach youth survival skills can be applied to any topic, not just survival.
Outward Bound is a good option for training if you have the budget.
Instructors who teach survival skills well go beyond practical skills to include critical thinking and problem-solving.
Many instructors make a lot of money by training corporate executives.
Beau encourages individuals to take on the role of the instructor themselves.
Learning survival skills teaches critical thinking, problem-solving, improvisation, and self-reliance.
Beau advises against trying to toughen up kids physically; mental toughness develops naturally.
He recommends reading the U.S. Army Survival Manual as a starting point.
Beau suggests making learning fun by not fighting popular culture and incorporating the youth's interests.
Teaching survival skills through activities like escape rooms, chemistry sets, fishing, and camping can be engaging.
Spending quality time with family while teaching these skills is a valuable aspect of the process.

Actions:

for parents, educators, mentors,
Read and familiarize yourself with the U.S. Army Survival Manual (suggested).
Incorporate survival skill activities into family time like camping, fishing, or learning chemistry sets (exemplified).
Organize hip-pocket classes and activities for hands-on learning (exemplified).
Use escape rooms or paper and pencil puzzles to develop problem-solving skills (exemplified).
</details>

## September
<details>
<summary>
2019-09-30: Let's talk about Biden, deplatforming Rudy, and Scooby-Doo.... (<a href="https://youtube.com/watch?v=uQH93_BfwK4">watch</a> || <a href="/videos/2019/09/30/Lets_talk_about_Biden_deplatforming_Rudy_and_Scooby-Doo">transcript &amp; editable summary</a>)

Beau explains how Scooby-Doo logic can reveal the true motives behind the administration's investigations, urging transparency for election integrity over political gains.

</summary>

"In the world of alternative facts, we're going to say it's all true."
"Every time he does, he reveals more."
"Your candidacy isn't as important as maintaining the integrity of the election process."

### AI summary (High error rate! Edit errors on video page)

Explains how Scooby-Doo logic can be applied to the administration's investigations into Joe Biden and his son.
Mentions Biden's aides asking news networks not to platform Rudy Giuliani.
Questions the approach of denying allegations and suggests embracing the world of alternative facts.
Talks about Hunter Biden's activities in Ukraine and speculates on unethical behavior without evidence of illegality.
Describes a scenario where Biden allegedly pressured the Ukrainian government to protect his son.
Criticizes the current administration for setting up investigations based on false information.
Points out inaccuracies regarding CrowdStrike's nationality.
Comments on the back channel investigation undermining U.S. intelligence.
Quotes Rudy Giuliani discussing the true motive behind the investigations.
Stresses the importance of maintaining election integrity over political candidacy.

Actions:

for politically engaged citizens,
Trust the American people to analyze evidence and prioritize election integrity (implied).
</details>
<details>
<summary>
2019-09-28: Let's talk about hearsay, credibility, and whistleblowers.... (<a href="https://youtube.com/watch?v=UM6YgRNxYnE">watch</a> || <a href="/videos/2019/09/28/Lets_talk_about_hearsay_credibility_and_whistleblowers">transcript &amp; editable summary</a>)

Republican senators attempting to discredit whistleblowers through hearsay will likely backfire given the significance of the information and the integrity of those involved.

</summary>

"His actions undermine the integrity of the system used to protect national security."
"The intelligence community that is so good at keeping secrets and keeping their mouths shut, those are the people blowing the whistle."

### AI summary (High error rate! Edit errors on video page)

Republican senators are trying to undermine the credibility of the whistleblower report by calling it hearsay.
The whistleblowers had access to highly classified information and are trusted individuals.
The senators, many of whom are lawyers, know that the whistleblower report is not hearsay.
Hearsay typically refers to statements intended to prove something, not entire reports or narratives.
The focus should be on the content of the report, not on disputing intent.
The Republican senators are treating this as a political scandal rather than a serious breach of security.
Overclassifying documents, as seen in this case, can be an attempt to obstruct justice.
The whistleblowers, who understand the significance of the information, are key players in this scandal.
The actions of the President undermine the integrity of the system designed to protect national security.
The intelligence community, experts at keeping secrets, are the ones raising the alarm on this issue.

Actions:

for senators, public,
Contact Senators to demand accountability for undermining the credibility of whistleblowers (implied)
Support transparency and accountability in government actions (implied)
</details>
<details>
<summary>
2019-09-27: Let's talk about young women and beauty standards.... (<a href="https://youtube.com/watch?v=8zaNWEVlzGM">watch</a> || <a href="/videos/2019/09/27/Lets_talk_about_young_women_and_beauty_standards">transcript &amp; editable summary</a>)

Beau addresses a disturbing school incident, criticizes toxic beauty standards, and advocates for recognizing intrinsic worth beyond superficial judgments.

</summary>

"Never let anybody tell you your worth."
"If you're not pretty, you're nothing. You're trophy. Trust me, it's not the guy you want."
"Never, never let a man determine your worth, because most men don't know value."

### AI summary (High error rate! Edit errors on video page)

Shifts focus from the White House to a disturbing incident at a school involving a 12-year-old girl.
Describes how the girl was allegedly assaulted on the playground, with racial slurs and degrading comments.
Mentions the school's strict regulations on marriage and homosexuality in contrast to its failure in regulating bullying.
Questions the toxic beauty standards imposed, particularly in the United States, and their impact on self-worth.
Encourages not to conform to unrealistic beauty standards and to recognize one's true worth beyond physical appearance.
Emphasizes that those who judge based on looks reveal more about their values than the person judged.
Criticizes the school's role in fostering intolerance and producing intolerant children.
Condemns the devaluation of individuals based on exploitable traits, evident from the incident.
Advocates for individuals not to let others determine their worth and to recognize intrinsic value beyond superficial standards.

Actions:

for students, parents, educators,
Contact the school administration to address bullying and intolerance (suggested)
Educate students on diversity and inclusion through workshops or programs (suggested)
</details>
<details>
<summary>
2019-09-26: Let's talk about whistleblowers and treason.... (<a href="https://youtube.com/watch?v=AuVTVhY0KEY">watch</a> || <a href="/videos/2019/09/26/Lets_talk_about_whistleblowers_and_treason">transcript &amp; editable summary</a>)

Beau clarifies misconceptions about treason and national security, stressing the administration's role in damaging intelligence efforts.

</summary>

"Words have definition. And treason doesn't mean did something the president doesn't like."
"The administration damaged national security. The administration aided spies, not the whistleblower."

### AI summary (High error rate! Edit errors on video page)

Beau addresses the LA Times report on the President's private breakfast where he questioned who provided the whistleblower with information, likening them to a spy.
He clarifies that those who shared the information were simply doing their jobs and providing oversight to Congress.
Beau explains that treason has a specific legal definition, and providing a whistleblower complaint is not treason.
He points out that damaging national security was not done by those who shared the information, but rather by the administration itself.
The over-classification of information can weaken the significance of true national security interests.
Beau asserts that the administration's actions have harmed national security and intelligence efforts.

Actions:

for concerned citizens, activists,
Contact your representatives to demand accountability and transparency in government actions (implied)
Educate others on the importance of protecting whistleblowers and upholding oversight committees (implied)
Join advocacy groups working to strengthen national security measures and protect intelligence gathering capabilities (implied)
</details>
<details>
<summary>
2019-09-25: Let's talk about the primaries, impeachments, and power.... (<a href="https://youtube.com/watch?v=KjF8SqP9veg">watch</a> || <a href="/videos/2019/09/25/Lets_talk_about_the_primaries_impeachments_and_power">transcript &amp; editable summary</a>)

Beau challenges the accepted corruption in politics, urging a shift towards local solutions and a rejection of deceitful politicians prioritizing power over truth.

</summary>

"Your solution is not in D.C., it's local."
"The fact that we don't realize that is why we're in trouble."
"That is corruption on a wide level."
"If that statement is true… none of them are fit to hold office."
"Because they tell us they're liars and cheats."

### AI summary (High error rate! Edit errors on video page)

Talks about the first Republican primary debates between Weld and Walsh, which most people missed.
Accuses the Republican party of rigging the election in favor of Donald Trump by shutting down primaries and not promoting debates.
Points out the party's support for Trump amid impeachment proceedings, predicting the Senate will never convict him.
Criticizes the lack of accountability and integrity in politics, focusing on the pursuit of power over truth and justice.
Emphasizes that the solution lies with local communities and individuals who genuinely believe in their values.
Challenges the notion that corruption in politics is a bug in the system rather than inherent to it.
Expresses concern over Americans being misled by corrupt politicians who prioritize self-interest over serving the people.
Condemns the Senate's potential refusal to hold the President accountable, questioning the fitness of all Republican senators for office.
Urges people to recognize the widespread deceit and corruption in politics rather than accepting it as normal.
Calls for a shift in mindset to acknowledge the truth about politicians' motives and representation of the public.

Actions:

for american voters,
Reach out to local communities and individuals who prioritize truth and justice (suggested)
Challenge corrupt practices within your local political sphere (exemplified)
Reject the normalization of deceit and corruption in politics (implied)
</details>
<details>
<summary>
2019-09-24: Let's talk about republics and democracies.... (<a href="https://youtube.com/watch?v=qYScSvpqZXU">watch</a> || <a href="/videos/2019/09/24/Lets_talk_about_republics_and_democracies">transcript &amp; editable summary</a>)

Beau explains the nuances between a republic and a democracy, stressing that democracy's survival hinges on an educated citizenry and active participation.

</summary>

"The United States is a democracy that happens to be a republic."
"Rights were endowed by the Creator or are self-evident. They existed beforehand."
"Democracy of any kind is going to be in danger as long as voters have the Facebook feeds they do."

### AI summary (High error rate! Edit errors on video page)

Explains the difference between a republic and a democracy, addressing common misconceptions and why people draw that distinction.
The United States is a democracy that happens to be a republic, not a direct democracy.
Describes a republic as a representative democracy, different from a direct democracy.
Talks about the misconception that a republic protects against mob rule, but points out that rights are not granted by the government.
Emphasizes that rights existed before the government and were protected by the Bill of Rights due to historical government attacks.
Warns against the dangerous idea that rights are always guaranteed, as amendments can be repealed.
Challenges the notion that a republic is better than a direct democracy due to requiring a larger mob.
Stresses the importance of an educated and informed citizenry for democracy to function properly.
Rejects the idea that common folk are too dumb to make decisions, advocating for individual autonomy in decision-making.
Encourages active participation to safeguard democracy in the face of modern challenges like misinformation on social media.

Actions:

for citizens, voters, educated individuals,
Educate yourself and others on the differences between a republic and a democracy (implied).
Actively participate in the democratic process by staying informed and engaged in political decisions (implied).
</details>
<details>
<summary>
2019-09-24: Let's talk about family values and climate change.... (<a href="https://youtube.com/watch?v=lQr1SqB0jEo">watch</a> || <a href="/videos/2019/09/24/Lets_talk_about_family_values_and_climate_change">transcript &amp; editable summary</a>)

Beau criticizes adults for failing to guide youth and encourages parents to prioritize education over political allegiance to empower the next generation.

</summary>

"You aren't working to protect your kids. You're leaving it to them to figure out."
"Your kid will betray your ideology in her name. Because they're smarter. Because they're more willing to learn."
"If you want teens out of the political discussion, you've got to step up."

### AI summary (High error rate! Edit errors on video page)

Recounts an experience in a cave with his family and a Spanish-speaking family.
Observes young kids charging ahead during a guided cave tour.
Criticizes adults who dismiss teenagers' political opinions.
Points out the lack of guidance offered to youth about education.
Expresses disagreement with the conclusions of the Parkland teens but respects their effort.
Compares Parkland teens to adults making jokes about guns on social media.
Emphasizes that youth speak up because adults won't.
Draws a parallel between cave instinct and adults not leading in society.
Calls out parents who neglect educating themselves and their kids about climate change.
Condemns parents who prioritize politics over their children's future.
Encourages parents to prioritize educating their children over political allegiance.
Talks about the impact of Greta Thunberg's speech on children's awareness.
Urges parents to prioritize their children over political parties for a better future.

Actions:

for parents, adults,
Educate yourself on climate change and other critical issues affecting your children's future (suggested)
Prioritize educating your children over political allegiance (suggested)
Encourage open-mindedness and learning in your children (suggested)
</details>
<details>
<summary>
2019-09-24: Let's talk about Greta.... (<a href="https://youtube.com/watch?v=TI-6x7GLwN8">watch</a> || <a href="/videos/2019/09/24/Lets_talk_about_Greta">transcript &amp; editable summary</a>)

Beau points out the hypocrisy and lack of decency in cyberbullying a teenager leading the fight against climate change.

</summary>

"She's better than you. So you try to attack her character."
"Greta, you're judged in this life by your enemies more than your friends."

### AI summary (High error rate! Edit errors on video page)

Woke up to news about Greta, a teenager leading the charge against climate change.
Conservative and Republican pundits faced backlash for cyberbullying Greta.
Expresses surprise at the backlash received by the pundits.
Points out that bullying a teenager isn't the worst thing they've done.
Mentions the lack of decency standards being held for the pundits.
Reads comments on Twitter attacking Greta's character.
Criticizes attacking Greta's looks and making fun of her hair.
Comments on the ridiculousness of the attacks compared to discussing climate change mitigation ideas.
Notes the hypocrisy of those accusing Greta of emotional child abuse.
Points out the absurdity of attacking a teenager personally instead of discussing ideas.
Acknowledges Greta's efforts in trying to save the world.
Notes the irony of calling Greta a fear monger while promoting fear themselves.
Encourages Greta to keep going despite the criticism.

Actions:

for social media users,
Show support for teenagers like Greta who are advocating for climate change action (implied)
</details>
<details>
<summary>
2019-09-23: Let's talk about a teen questioning spreading democracy.... (<a href="https://youtube.com/watch?v=1nhsQAYa-E4">watch</a> || <a href="/videos/2019/09/23/Lets_talk_about_a_teen_questioning_spreading_democracy">transcript &amp; editable summary</a>)

A 15-year-old's profound query on democracy prompts Beau to reveal contradictions in US actions, urging critical thinking and questioning narratives with gaps.

</summary>

"Democracy is advanced citizenship, man. It's hard, you gotta want it."
"Democracy doesn't matter. If it did, we wouldn't be doing that."
"Most of them have holes you can walk through."
"You really should. You are thinking at a level far beyond what is expected of you."
"Democracy in retreat."

### AI summary (High error rate! Edit errors on video page)

A 15-year-old asks why the US government wants countries to be democratic, questioning if democracy can be forced.
Beau praises the question as advanced thinking for a teenager, stating that democracy requires active participation and desire.
He mentions that the US has a powerful war machine and has been spreading democracy globally.
Beau questions if the US truly spreads democracy or if it's just a narrative.
Referring to a study by Rich Whitney, Beau reveals that the US provides military aid to many dictatorships, contradicting the promotion of democracy.
He explains that intelligence operations prioritize American interests over idealistic values like democracy.
Beau points out that stability is paramount for the US, sometimes conflicting with the establishment of democracies.
Democracy is described as being in retreat, as per the 2019 Freedom House report.
Beau encourages critical thinking and questioning narratives with holes.

Actions:

for youth, critical thinkers,
Question narratives and seek the truth (implied)
Advocate for transparency in government actions (implied)
Engage in critical thinking and analyze information critically (implied)
</details>
<details>
<summary>
2019-09-23: Let's talk about Trump's move to protect historic sites.... (<a href="https://youtube.com/watch?v=Nh6bexLk9gU">watch</a> || <a href="/videos/2019/09/23/Lets_talk_about_Trump_s_move_to_protect_historic_sites">transcript &amp; editable summary</a>)

Beau criticizes the president's proposal to protect religious freedom, calling it a con and pointing out his history of undermining similar initiatives.

</summary>

"It's a con."
"There's nothing new in this. There's nothing visionary in this."
"He realizes he's losing. He realizes that people are turning against him, realizing that he's a con."
"If he actually cared about any of this stuff, he could have acted on it any time within the last three years."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Addresses the president's proposal to protect religious freedom by safeguarding religious sites, including those of historical significance.
Supports the idea of protecting religious and cultural sites, expanding beyond just religious places.
Mentions the existing organization UNESCO, which already works on this concept.
Criticizes the president for pulling out of UNESCO and undermining global efforts to protect cultural and religious sites.
Points out the importance of protecting religious freedom for individuals, as outlined in the Universal Declaration on Human Rights.
Criticizes the president for undermining human rights through relationships with dictators.
Calls out the president for failing to uphold treaty obligations regarding the treatment of refugees.
Labels the president's proposal as a con, stating that it is nothing new and that he has undermined similar initiatives throughout his administration.
Acknowledges the importance of protecting historical and cultural sites but criticizes the president for only now showing interest due to public backlash.
Concludes by suggesting that the president's recent actions are merely attempts to deceive the public in light of his declining popularity.

Actions:

for policy analysts,
Support UNESCO's efforts to protect cultural and religious sites (exemplified)
Advocate for upholding treaty obligations regarding the treatment of refugees (exemplified)
</details>
<details>
<summary>
2019-09-22: Let's talk about opinions on Trudeau and Yang.... (<a href="https://youtube.com/watch?v=4XwMiPZVrpA">watch</a> || <a href="/videos/2019/09/22/Lets_talk_about_opinions_on_Trudeau_and_Yang">transcript &amp; editable summary</a>)

Beau addresses controversies around Trudeau's blackface and Yang's stereotypes, urging to prioritize the voices of those directly impacted over irrelevant opinions in societal discourse.

</summary>

"Blackface is bad, okay? Southern American blackface is evil. It is evil."
"If Jimmy insults Suzie, you don't ask Billy if the apology was sufficient."
"Sometimes it's better to get the opinion of people that are actually involved."
"Not everything requires your personal response."
"Your personal response may drown out the opinions of those people that need to be heard more."

### AI summary (High error rate! Edit errors on video page)

Addresses questions about Trudeau and Yang, both involving a common component.
Mentions the controversy surrounding Trudeau's blackface incident and the apology.
Describes Southern American blackface as evil due to its malicious nature and historical context.
Comments on the relevance of his opinion regarding Trudeau's apology, suggesting the focus should be on those directly impacted.
Shares responses from people of color regarding Trudeau, including differing opinions on his actions and apology.
Emphasizes the importance of understanding varying perspectives within impacted communities.
Responds to questions about Yang and stereotypes, particularly regarding Asians excelling in math and becoming doctors.
Advocates for seeking the opinions of those directly involved in a situation rather than assuming all opinions carry equal weight.
Critiques the societal pressure to have an opinion on everything in the age of social media.
Encourages considering whose voices need to be amplified and heard more in various situations.

Actions:

for social media users,
Reach out to and amplify the voices of those directly impacted by controversies rather than focusing on irrelevant opinions (implied).
Engage in meaningful dialogues with communities affected by stereotypes to understand their perspectives (implied).
</details>
<details>
<summary>
2019-09-22: Let's talk about one of my favorite weeks.... (<a href="https://youtube.com/watch?v=r1w9eF3Egxo">watch</a> || <a href="/videos/2019/09/22/Lets_talk_about_one_of_my_favorite_weeks">transcript &amp; editable summary</a>)

Beau introduces Banned Books Week, encourages reading banned books to uncover truths and controversial ideas independently, and offers an alternative book club through an Amazon influencer store.

</summary>

"Fiction gets to truth sometimes a whole lot faster than fact does."
"Truth isn't told, it's realized on your own."
"Go find a book that somebody doesn't want you to read."
"It's just you in the book."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Introduces Banned Books Week, celebrating books like "Of Mice and Men," "Catcher in the Rye," "To Kill a Mockingbird," and others that have been banned or challenged in libraries and schools.
Encourages people to pick up one of the banned books to learn about the norms and controversies of the time they were written.
Points out that fiction can sometimes reveal truth faster than facts and expose readers to controversial ideas in a non-debate setting.
Expresses the importance of individuals realizing truth on their own while reading, without outside influence or defense mechanisms.
Explains his alternative to a book club by setting up an Amazon influencer store where viewers can access book lists without being priced out, supporting emergency preparedness gear as well.
Invites viewers to participate by recommending books for the store, promoting engagement and collective reading.
Urges viewers to participate in Banned Books Week by reading a book that someone doesn't want them to read, encouraging exploration of diverse perspectives.

Actions:

for book lovers, advocates of free speech,
Visit Beau's Amazon influencer store to access book lists and support emergency preparedness gear (suggested)
Recommend books in the comments for Beau's store, promoting collective reading (suggested)
</details>
<details>
<summary>
2019-09-21: Let's talk about Upton Sinclair and your next Bacon purchase.... (<a href="https://youtube.com/watch?v=fmsdS9f0M4A">watch</a> || <a href="/videos/2019/09/21/Lets_talk_about_Upton_Sinclair_and_your_next_Bacon_purchase">transcript &amp; editable summary</a>)

Beau warns of the dangerous implications of allowing slaughterhouses to self-police, prioritizing profits over consumer safety.

</summary>

"It's going to make it more profitable for them to staff the safety inspectors than it is for the government to do it."
"In my ideal world, yeah companies like this would police themselves, but in my ideal world they'd police themselves and we know that isn't what's going to happen here."
"It's something that we might want to keep in mind."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic of Upton Sinclair, an American author known for writing almost 100 books and winning the Pulitzer Prize for fiction.
Upton Sinclair's books, often journalism disguised as fiction, had a significant impact in the United States.
"The Jungle," one of Sinclair's famous works, written in 1906 about the meatpacking industry, led to the passing of the Pure Food and Drug Act and the Meat Inspection Act.
Upton Sinclair is credited with being a driving force behind better policing in slaughterhouses due to his impactful book.
The USDA is removing prohibitions on line speed in slaughterhouses, allowing them to self-police, which can have detrimental effects on food safety.
The industry, not just Trump, has been lobbying for removing regulations on line speed for profitability reasons.
Despite past failures in pilot programs, the plan to let slaughterhouses develop their own standards is still moving forward.
Companies prioritizing profits over safety may lead to a rise in foodborne illnesses from pork.
Beau questions the effectiveness of companies self-policing and raises concerns about potential consequences.

Actions:

for food safety advocates,
Contact local representatives to advocate for stricter regulations on slaughterhouses (implied)
Join or support organizations that focus on food safety and regulation (implied)
</details>
<details>
<summary>
2019-09-20: Let's talk about the Climate Strike, hope, Trudeau, and stepping stones.... (<a href="https://youtube.com/watch?v=9QmxW4YUWSc">watch</a> || <a href="/videos/2019/09/20/Lets_talk_about_the_Climate_Strike_hope_Trudeau_and_stepping_stones">transcript &amp; editable summary</a>)

Trudeau's scandal and the global climate strike inspire hope by uniting millions across borders for positive change, signaling a shift towards global unity and activism.

</summary>

"Man, that's cool. Gives me hope."
"They're thinking about the planet as a whole."
"Millions of people tomorrow will be acting together because they started thinking broader than a border."
"We might one day see that world I want."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Trudeau's scandal sparked hope in unexpected ways.
Beau's son questioned the appropriateness of blackface in 2001, leading to a hopeful realization.
Young American teens discussing Canadian politics signifies a positive change.
The global climate strike, with millions participating, is seen as the largest protest in history.
Companies granting employees paid time off for the climate strike indicates a shift towards social responsibility.
Grassroots movements unite millions globally for a common cause, transcending borders.
Technology allows today's youth to connect and mobilize internationally, diminishing nationalism.
The upcoming generation is less likely to be divided by nationalism and xenophobia.
Individuals are coming together across borders to urge governments to act on climate change.
Millions are thinking beyond borders and considering the planet as a whole, paving the way for a borderless mindset.

Actions:

for global citizens,
Join or support grassroots movements for climate action (implied)
Connect with individuals internationally to advocate for global change (exemplified)
</details>
<details>
<summary>
2019-09-19: Let's talk about a message to the Republican Party.... (<a href="https://youtube.com/watch?v=_zRjUCY3KRc">watch</a> || <a href="/videos/2019/09/19/Lets_talk_about_a_message_to_the_Republican_Party">transcript &amp; editable summary</a>)

Beau delivers a message to the Republican Party, expressing disappointment in their stances on economics, military involvement, gun control, and values, signaling a growing dissatisfaction among individuals like him.

</summary>

"I certainly don't think that come next fall I should see a gold star in front of his parents' house because you guys want to increase your stock portfolios."
"And the other 60 to 70%, I can answer that in one sentence, I don't care about somebody's skin tone, country of origin, religion, gender, orientation, or the language they speak."
"The reality is now, there's a whole bunch of guys, look like me, sound like me, real sick of you."
"But I believe they believe it. That's more than I can say for you."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Sends a message to the Republican Party as a blue-collar, rural, swing state resident.
Expresses disappointment in the economic platform and stance on free trade and trickle-down economics.
Questions the military involvement with Saudi Arabia and the arms sales to them.
Criticizes the Republican Party for not upholding Second Amendment rights while enacting more gun control measures under the current administration.
Calls out the GOP for canceling primaries, supporting the current administration's actions, and diverting funds from critical projects.
Emphasizes that he doesn't base his opinions on skin tone, origin, religion, gender, orientation, or spoken language.
States that he is not a bigot and focuses on judging individuals based on character rather than demographics.
Points out a growing dissatisfaction among individuals like him with the Republican Party.
Acknowledges differences in beliefs with progressive figures like AOC and the squad but respects their authenticity.
Concludes with a note of concern for the Republican Party's loss of support from individuals like him.

Actions:

for blue-collar voters,
Reach out and connect with disillusioned individuals in your community who share similar concerns with political parties (implied).
Engage in open dialogues with individuals holding diverse viewpoints to understand their perspectives and concerns (implied).
</details>
<details>
<summary>
2019-09-18: Let's talk about the Trump cheerleaders.... (<a href="https://youtube.com/watch?v=l0K0U3F5IGY">watch</a> || <a href="/videos/2019/09/18/Lets_talk_about_the_Trump_cheerleaders">transcript &amp; editable summary</a>)

Beau explains the controversy around Trump-supporting cheerleaders, advocating for meaningful political discourse in schools within the bounds of free speech.

</summary>

"Students do not shed their rights at schoolhouse gate, period."
"Given the fact that they were in uniform, yeah, that's probably fitting."
"I think that is something that needs to change and in the meantime we need to defend what little ability students have to discuss politics at school."
"We have to defend it as much as we can within the bounds of a civilized society."
"Whether or not I agree with it shouldn't matter."

### AI summary (High error rate! Edit errors on video page)

Responds to a message about Trump cheerleaders being punished for displaying a banner supporting Trump 2020 before a game at North Stanley High School in North Carolina.
North Stanley High School has a policy against political signs, but the issue is whether the banner was disruptive to the school, not the political nature of the sign.
Points out that students have rights at school, and the test is whether their actions are disruptive.
Explains that the cheerleaders, as representatives of a government entity receiving government money, can't display political messages while in uniform.
Clarifies that the punishment for the cheerleaders was probation, meaning they were simply told not to do it again and faced no significant consequences.
Emphasizes that allowing students to have meaningful political discourse in schools is vital to avoid the rise of candidates like Trump.
Encourages open, non-confrontational discourse to understand why the cheerleaders support Trump.
Advocates for defending free speech within the bounds of a civilized society, even if one disagrees with the message being conveyed.

Actions:

for students, educators, community members,
Contact the cheerleaders for non-confrontational political discourse (suggested)
</details>
<details>
<summary>
2019-09-18: Let's talk about the First Amendment.... (<a href="https://youtube.com/watch?v=eJmh0qql0FA">watch</a> || <a href="/videos/2019/09/18/Lets_talk_about_the_First_Amendment">transcript &amp; editable summary</a>)

Beau breaks down the misconception around free speech, exposing the far right's desire for a platform over true freedom of expression, critiquing their attempts to dictate speech through government intervention.

</summary>

"The far right doesn't want speech. They don't want free speech. They want a platform."
"You don't need the government's gun. You better let this guy talk."
"A good idea will find a platform. Doesn't need one handed to him."
"You can be deplatformed and if the idea is solid, another platformer will arise where you can create your own."
"That's the right and y'all are utter failures at it."

### AI summary (High error rate! Edit errors on video page)

People were mad about squelching free speech rights, exemplified by criminalizing pipeline protests in red states and Trump's suggestion to strip citizenship for flag burning.
Republicans support making it illegal to burn the flag, prohibiting offensive statements against police or military, and banning face coverings.
Republicans show less support for extending free speech courtesies to the LGBTQ community.
Republicans favor stopping construction of new mosques and arresting hecklers rather than disciplining them.
Republicans believe the press has too much freedom and express a negative view towards journalists.
Beau dismisses claims of a free speech crisis on campuses, noting that professors being fired for political speech are mostly liberals, not conservatives.
Beau explains that universities provide platforms for dissenting viewpoints, and students protesting speeches is protected by the First Amendment.
The far right seeks a platform, not free speech, and universities are more accepting of dissenting viewpoints.
Free speech means the right to speak, not entitlement to a platform or equal time for all ideas.
Beau criticizes the far right for wanting to use the government to dictate speech and actions, contrasting it with the principles of free speech and dissent.

Actions:

for activists, free speech advocates,
Stand up for true free speech by advocating for platforms for diverse voices (implied).
Educate others on the distinction between a platform and free speech (implied).
Support institutions that uphold principles of free speech and dissent (implied).
</details>
<details>
<summary>
2019-09-17: Let's talk about what Judd Gregg said Trump was.... (<a href="https://youtube.com/watch?v=xrW2wLqOxJk">watch</a> || <a href="/videos/2019/09/17/Lets_talk_about_what_Judd_Gregg_said_Trump_was">transcript &amp; editable summary</a>)

Beau expresses support for Trump, refutes claims of socialism in Trump's policies, and questions the article's assertions about the President's core policies, ending with a thought for the audience to ponder.

</summary>

"He is not helping to make America great."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Beau expresses his longstanding support for President Trump and his surprise at an article labeling Trump as socialist.
He questions the characterization of Trump's policies as socialist and sees them more as a blend of corporate and government interests.
Beau becomes defensive of Trump, dismissing the idea that his policies are socialist and getting upset at the article's claims.
He reacts strongly to the suggestion that Trump lacks core policies, defending Trump's nationalist stance.
Beau ends by leaving it as a thought for his audience, questioning the claims made in the article.

Actions:

for trump supporters,
Question media narratives and do independent research to form your own opinions (implied).
</details>
<details>
<summary>
2019-09-17: Let's talk about President Trump being wishy-washy.... (<a href="https://youtube.com/watch?v=8iOi5yCrg4Q">watch</a> || <a href="/videos/2019/09/17/Lets_talk_about_President_Trump_being_wishy-washy">transcript &amp; editable summary</a>)

Trump's wishy-washy stance stems from his own ignorance, while complex geopolitics challenge his diplomatic footing, leaving no easy way out.

</summary>

"What the American public doesn't know is what makes them the American public."
"Geopolitics in the region is a little bit more complicated than building a golf course."
"There's no reason American lives should be risked and wasted because they didn't fulfill their duties."
"The American public needs running the country and the largest war machine on the planet."
"It may just be a reminder that the American public needs running the country."

### AI summary (High error rate! Edit errors on video page)

Trump is wishy-washy due to his own making and lack of intelligence reports.
Multiple past incidents were ignored due to not appearing on Fox News.
The situation involving Iran and Saudi Arabia is complex and requires a nuanced understanding.
Scrapping previous deals with Iran puts Trump in a difficult diplomatic position.
Talking points blaming Iran for supplying weaponry are weak due to US support of Saudis.
Geopolitics in the region is intricate, contrasting with Trump's usual dealings.
Blaming Saudis for failing to protect critical infrastructure could be Trump's way out.
Lack of sharp advisors on Trump's staff poses a challenge.
Uncertainty looms over future actions and viable options for the administration.
Running a country differs greatly from selling a brand, hinting at the responsibilities at hand.

Actions:

for political analysts,
Contact political analysts to provide insights and strategies (suggested)
Join organizations advocating for informed decision-making in geopolitics (implied)
</details>
<details>
<summary>
2019-09-16: Let's talk about being edgy and offending people.... (<a href="https://youtube.com/watch?v=SF7m9xf4To8">watch</a> || <a href="/videos/2019/09/16/Lets_talk_about_being_edgy_and_offending_people">transcript &amp; editable summary</a>)

Beau believes in thoughtful speech over shock value, stresses common courtesy, and plans community workshops.

</summary>

"Being edgy is not a substitute for having a personality."
"It's almost always better to be nice."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Expresses no desire to change speech despite Democratic candidates' coarsening.
Believes in reaching people with thought, not shock value.
Views being edgy as a poor substitute for personality.
Shares an incident on Twitter where he misunderstood a tweet and sought clarification.
Emphasizes the importance of common courtesy and avoiding intentional offense.
Stresses the value of reaching out for understanding.
Mentions starting a podcast with reruns of older videos to introduce himself to the community.
Talks about potential sponsored videos, stating they must fit the channel's theme.
Mentions plans for workshops in the South to build community networks and possibly going on the road next summer.

Actions:

for content creators,
Set up workshops to help build community networks (planned)
Attend workshops to learn from others (planned)
</details>
<details>
<summary>
2019-09-15: Let's talk about Smokey Bear and fear.... (<a href="https://youtube.com/watch?v=HlOydP2DASE">watch</a> || <a href="/videos/2019/09/15/Lets_talk_about_Smokey_Bear_and_fear">transcript &amp; editable summary</a>)

Beau celebrates Smokey the Bear's birthday, encounters different animals with his son, and shares insights on overcoming fear through education to reduce conflict globally.

</summary>

"Most fear can be killed with education."
"That kind of fear is a gift, use it wisely."
"If everybody did that, there'd be less fear. There'd be less fear in the world."

### AI summary (High error rate! Edit errors on video page)

Took his kids to the Tallahassee Museum to celebrate Smokey the Bear's 75th birthday.
Smokey the Bear is a fire prevention mascot born during World War II when firefighters joined the military.
The museum is outdoors with animal enclosures, including wolves, a Florida Panther, and a bear.
Beau's son wanted to play with the bear despite knowing they are dangerous animals.
Encountered a person dressed as Smokey the Bear, and Beau's son was scared due to unfamiliarity.
Beau talks about different kinds of fear: rational fear, irrational fear, and subconscious fear.
Education can help overcome fear by learning and understanding the subject.
Beau encourages researching things we fear to eliminate that fear and reduce conflict in the world.

Actions:

for parents, educators, individuals,
Research something you fear to understand and overcome it (implied).
</details>
<details>
<summary>
2019-09-14: Let's talk about getting an education from Roo.... (<a href="https://youtube.com/watch?v=TD-kywHwzLQ">watch</a> || <a href="/videos/2019/09/14/Lets_talk_about_getting_an_education_from_Roo">transcript &amp; editable summary</a>)

An educational chatbot fills the void left by parents and schools in providing vital information and support to teens, reflecting broader societal failures in communication and education.

</summary>

"Teens don't make the wisest decisions even when they have all the information."
"Teens are turning to the internet, turning to a chat bot to get advice that they should very, that they should feel comfortable getting from their parents."
"This is not a topic that you want teens making decisions without all the information."

### AI summary (High error rate! Edit errors on video page)

Introduces an educational chatbot named Roo that provides information many parents are uncomfortable discussing.
Only 24 states in the U.S. require comprehensive sexual education, with just 13 of those states mandating medically accurate content.
Roo is capable of answering a wide range of questions, from puberty to relationship advice, and has had over a million interactions in nine months.
Acknowledges that many teens turn to Roo because they lack someone in their lives to talk to about sensitive topics.
Emphasizes the importance of open communication between parents and children to prevent misinformation and poor decision-making.
Criticizes the irony of individuals opposing funding for organizations like Roo while their own children benefit from its services.
Views the reliance on Roo for critical information as a reflection of societal failures in providing adequate education and support for teens.
Stresses the significance of education in empowering young people to make informed choices and navigate complex issues.

Actions:

for parents, educators, policymakers,
Support and advocate for comprehensive sexual education in all states (implied)
Encourage open communication with children on sensitive topics (implied)
Promote resources like educational chatbots for teens lacking support systems (implied)
</details>
<details>
<summary>
2019-09-13: Let's talk about your obligation to humanity.... (<a href="https://youtube.com/watch?v=TuSUycij6jU">watch</a> || <a href="/videos/2019/09/13/Lets_talk_about_your_obligation_to_humanity">transcript &amp; editable summary</a>)

Beau challenges nationalism, advocating for helping beyond borders and finding common ground with those in need.

</summary>

"Your obligation to humanity does not end at the border."
"We have to start thinking broader than a border."
"Those colors. So much of what you believe is a person and how you identify yourself, all hinges on what colors are flying on that flagpole outside of the hospital you were born in."
"Me and that guy from the Bahamas, oh, we sit down and have a drink. We can relate on a meaningful level because we're kind of the same."
"Your obligation to humanity does not end at the border."

### AI summary (High error rate! Edit errors on video page)

Responds to comments about helping the Bahamas, questioning why we can't help our own people.
Believes in helping those in need regardless of skin tone, religion, or cultural differences.
Challenges the idea of "othering" people based on nationality or location.
Compares his relatability to a person from the Bahamas over politicians on Capitol Hill.
Encourages thinking beyond borders and nationalism.
Compares nationalism to gang mentality, questioning the difference.
Emphasizes the commonalities between people born in different places rather than their differences.
Stresses the importance of humanity and helping beyond borders.

Actions:

for global citizens,
Connect with individuals from different countries to understand their experiences (implied)
Challenge nationalist rhetoric and stereotypes in your community (implied)
Volunteer or donate to organizations supporting international aid efforts (implied)
</details>
<details>
<summary>
2019-09-12: Let's talk about the Bahamas and talking points.... (<a href="https://youtube.com/watch?v=U-trVlTIB14">watch</a> || <a href="/videos/2019/09/12/Lets_talk_about_the_Bahamas_and_talking_points">transcript &amp; editable summary</a>)

Beau details the devastation in the Bahamas post-Cat 5 hurricane, criticizing the Trump administration for refusing aid and urging people to pressure representatives for help.

</summary>

"This is a humanitarian disaster of biblical proportions."
"We are abandoning our responsibilities as human beings."
"We are leaving people to die."
"If you want to help, you need to put pressure on your representatives."
"It's that simple."

### AI summary (High error rate! Edit errors on video page)

The Bahamas were hit by a Cat 5 hurricane, with winds up to 220 miles per hour and a storm surge of 20 feet, causing widespread destruction.
Estimates indicate 70,000 people are homeless and displaced, a significant portion of the Bahamas' 400,000 population.
Thousands are missing, and the human toll is still unknown.
Despite the massive scale of the disaster, the Trump administration refuses to grant temporary protected status to the victims.
Beau draws parallels to the administration's treatment of refugees at the southern border, where they are also denied refugee status.
He criticizes the lack of empathy and humanitarian response, accusing the administration of fostering a climate that denies help to those in need.
Beau points out that the administration could use parole to admit those affected without full documentation, given the circumstances.
He expresses dismay at the abandonment of responsibilities towards fellow humans and the disregard for basic human decency.
Beau condemns the administration's actions as un-American, unprecedented, and disgraceful.
He calls for people to pressure their representatives to take action and provide assistance to the Bahamas.
Beau urges against merely spending money at major resorts but rather directs people to seek ways to help through bahamas.com/relief and by contacting their representatives.

Actions:

for global citizens,
Pressure representatives to provide assistance (suggested)
Support relief efforts through bahamas.com/relief (suggested)
</details>
<details>
<summary>
2019-09-12: Let's talk about superstitions, Jurassic Park, dreaming, and rhinos.... (<a href="https://youtube.com/watch?v=pauyvIE8tQc">watch</a> || <a href="/videos/2019/09/12/Lets_talk_about_superstitions_Jurassic_Park_dreaming_and_rhinos">transcript &amp; editable summary</a>)

Beau shares the hopeful story of reviving the Northern White Rhino and suggests that with the same mentality, humanity can solve various global issues alongside saving species.

</summary>

"It's amazing. It's amazing and it gives me hope for a lot of the other problems we have in this world."
"We produce more food than we need. We just got to figure out how to get it where it needs to be."
"We're an advanced species. If we can believe in superstitions like this, and this kind of technology, if we can do both of those at the same time, we can certainly work on world hunger, disease, climate change, homelessness, all of this stuff, and save the rhinos."

### AI summary (High error rate! Edit errors on video page)

The Northern White Rhino is basically extinct, with only two left - a mother and daughter.
Scientists harvested eggs from the rhinos and created embryos from frozen samples of male northern white rhinos.
Two embryos have been created and frozen in liquid nitrogen to be transported and inserted into a southern white rhino surrogate mom to bring back the species.
The reason behind the extinction of these rhinos is the demand for their horns, believed to have magical healing powers due to superstition.
Despite this ancient superstition, efforts are being made to revive the species, showcasing the strange yet hopeful nature of humans.
Beau draws parallels between this conservation effort and solving other global issues like world hunger, disease, and homelessness.
He believes that with the same mentality applied to food distribution, these problems can also be solved.
Beau advocates for addressing multiple issues simultaneously, suggesting that as an advanced species, humans can work on conservation and other challenges concurrently.
He expresses hope and confidence in humanity's ability to tackle various issues if there is a collective desire to do so.
Beau encourages maintaining dreams and optimism in the face of challenges.

Actions:

for conservation enthusiasts,
Support conservation efforts (exemplified)
Advocate for addressing global issues simultaneously (implied)
</details>
<details>
<summary>
2019-09-12: Let's talk about North Carolina.... (<a href="https://youtube.com/watch?v=YeW5fTcUR68">watch</a> || <a href="/videos/2019/09/12/Lets_talk_about_North_Carolina">transcript &amp; editable summary</a>)

House Republicans in North Carolina attempt a budget veto override during a 9/11 memorial, exposing a pattern of subverting democracy and sparking the decline of the party.

</summary>

"They went out of their way to subvert the democratic process."
"You're watching the death of a majority."
"These representatives are more dangerous than any terrorist."

### AI summary (High error rate! Edit errors on video page)

Yesterday marked an anniversary nobody celebrates but acknowledges, reflecting on changes and perspectives shaped by political beliefs.
House Republicans in North Carolina attempted a veto override of the budget while Democrats were at a 9/11 memorial.
Republicans in North Carolina have a history of subverting democracy to maintain power.
Despite past instances of gerrymandering being struck down, Republicans continue to manipulate districts.
The passing of the override in North Carolina is seen as an attack on democracy.
Deb Butler's passionate attempt to stop the override made headlines.
The passing of the budget override signals the decline of the Republican Party in North Carolina.
Beau predicts the Republican Party's decline due to fairer districts in the future.
Beau compares the actions of representatives in North Carolina to those who perpetrated the 9/11 attacks, stressing the danger posed by political figures.
Beau ends by remarking on the power politicians hold in shaping the country's foundation.

Actions:

for north carolina residents,
Support fair districting in North Carolina (implied)
Stay informed and engaged in local politics (implied)
</details>
<details>
<summary>
2019-09-11: Let's talk about ending higher education and Tennessee Senator.... (<a href="https://youtube.com/watch?v=okB22EuoKgA">watch</a> || <a href="/videos/2019/09/11/Lets_talk_about_ending_higher_education_and_Tennessee_Senator">transcript &amp; editable summary</a>)

Tennessee Senator calls for abolishing higher education, believing it breeds liberalism, while overlooking policy reflections and opting for uneducated workforce to bolster GOP control.

</summary>

"The stupid stuff that our kids are being taught is absolutely ridiculous."
"Rather than take that information and say, hey, are our policies outdated? Have we lost touch? Have we not kept up with the growth of new information in the world? No, we need to ban schools."
"They want to create that permanent underclass, that uneducated worker class that can't think for themselves, the proles, the good workers for Big Brother and their corporate donors."

### AI summary (High error rate! Edit errors on video page)

Tennessee Senator Kerry Roberts expressed the belief that higher education institutions should be abolished because they are "liberal breeding grounds" that teach ridiculous things contradicting American values.
Roberts criticized a woman with higher education who spoke out against white supremacy and oppression, believing such views go against the country's values.
Instead of reflecting on whether Republican policies are outdated, Roberts proposed banning schools and eliminating institutions of higher education.
Beau mentions that individuals with higher education, or those who read more, are less likely to support Republican policies.
Conservative figure George Will referred to the GOP as "the dumb party," indicating a disconnect with young people.
Roberts' approach of promoting uneducated workers who can't think for themselves is seen as a strategy to maintain power and control over a permanent underclass.
This view suggests a desire to create a workforce easily manipulated by corporate interests.
The proposal to abolish higher education institutions serves the agenda of perpetuating a class of individuals reliant on authority figures for direction.
Beau implies that the GOP's salvation, as perceived by Roberts, lies in limiting education to maintain control over the population.
The focus appears to be on securing a compliant workforce rather than encouraging critical thinking and informed decision-making.

Actions:

for advocates for education,
Advocate for accessible and inclusive education (implied)
Support policies and initiatives that prioritize education and critical thinking (implied)
</details>
<details>
<summary>
2019-09-09: Let's talk about Trump and the First Step Act.... (<a href="https://youtube.com/watch?v=48Tzh6G9kNI">watch</a> || <a href="/videos/2019/09/09/Lets_talk_about_Trump_and_the_First_Step_Act">transcript &amp; editable summary</a>)

Trump signed a small first step in criminal justice reform, but underfunded and undermined it, while failing to address root causes and perpetuating harmful systems.

</summary>

"A bandaid on a bullet wound."
"You want a participation trophy."
"You've done nothing other than increase militarization of police."

### AI summary (High error rate! Edit errors on video page)

Acknowledges Trump's signing of the First Step Act but questions the follow-up actions.
Trump felt personally attacked by the news and lashed out on Twitter, making himself a national embarrassment.
The First Step Act was described as a small first step in criminal justice reform.
Despite its importance to those directly impacted, Beau likens it to a bandaid on a bullet wound in the grand scheme.
Provides cost details of the First Step Act and contrasts it with the allocated budget amount.
Points out the massive scope of the incarceration problem in America, with the highest rate globally.
Trump's legislation is criticized for not addressing the root causes of the issue.
Beau accuses Trump of increasing police militarization, expanding incarceration centers, and benefiting lobbyists profiting from human suffering.
Mocks Trump for wanting recognition for minimal efforts in criminal justice reform.
Concludes by calling out Trump's actions as insufficient and damaging, despite signing a bill.

Actions:

for reform advocates,
Advocate for comprehensive criminal justice reform (implied)
Support organizations working towards true systemic change (implied)
Hold elected officials accountable for meaningful reform (implied)
</details>
<details>
<summary>
2019-09-09: Let's talk about George Orwell.... (<a href="https://youtube.com/watch?v=QAPEBdzwhQg">watch</a> || <a href="/videos/2019/09/09/Lets_talk_about_George_Orwell">transcript &amp; editable summary</a>)

Beau introduces George Orwell, dissects his quotes through a satirical lens, and reveals Orwell's true beliefs against totalitarianism and for democratic socialism.

</summary>

"In a time of deceit, telling the truth is a revolutionary act."
"Journalism is printing what someone else does not want printed."
"If you want a picture of the future, picture a boot stomping on a human face forever."
"Orwell was a unique person. His writing, a lot of it was satire, it was parody."
"Every line of serious work that I have written since 1936 has been written directly or indirectly against totalitarianism and for democratic socialism."

### AI summary (High error rate! Edit errors on video page)

Beau introduces the topic of George Orwell in relation to Banned Books Week.
He explains how people often misinterpret Orwell's quotes by not understanding his use of satire and parody in his writings.
Orwell believed that all issues are political issues and warned against ignoring political matters until they directly impact individuals.
Beau talks about war propaganda and how it is typically perpetuated by those who are not directly involved in the fighting.
Orwell's focus on truth over fact and his use of fiction to convey truths are discussed.
Beau delves into the concept that telling the truth in a time of deceit is a revolutionary act.
The importance of journalism in printing what others do not want printed is emphasized.
Beau draws parallels between Orwell's writing and journalism, particularly in "Homage to Catalonia."
He contrasts Orwell's famous quote about a boot stomping on a human face forever with the true intention behind it.
Beau reveals Orwell's stance against totalitarianism and for democratic socialism post-1936.

Actions:

for readers, bookworms, orwell enthusiasts,
Share Orwell's true beliefs against totalitarianism and for democratic socialism on social media to spread awareness (suggested).
Research more about George Orwell and his works to understand his messages better (suggested).
</details>
<details>
<summary>
2019-09-08: Let's talk about the lack of primaries on the campaign trail.... (<a href="https://youtube.com/watch?v=-9g5VkROBys">watch</a> || <a href="/videos/2019/09/08/Lets_talk_about_the_lack_of_primaries_on_the_campaign_trail">transcript &amp; editable summary</a>)

The GOP is canceling primaries out of fear of challenging Trump internally, risking revealing his weak support and policies, causing dissent within the party.

</summary>

"They're scared of the dissension within their own party."
"It's going to tear down the entire GOP establishment."
"If the election goes the way I think it might, it's going to be fantastic."
"Your opinion doesn't matter. It's what happens when authoritarianism infects a party."
"It's going to be a dumpster fire of just epic proportions."

### AI summary (High error rate! Edit errors on video page)

The GOP is canceling primaries across the country to prevent challenges to Trump, showcasing their fear of internal dissension.
Rules were bent in South Carolina to cancel the primary, possibly leading to legal issues.
The GOP establishment worries that if Trump doesn't win by a large margin in the primaries, it will reveal the weakness of his support and policies.
Recent shifts in support are evident, with once fervent Trump supporters now concealing their loyalty.
Trump's ineffective leadership and controversial actions have led to declining support even within his base.
The GOP establishment's strategy is to prevent challenges to Trump in the primaries and rely on party loyalty during the election.
Similar tactics failed for Democrats in the past, and Beau predicts a similar outcome for Republicans if they continue to support Trump.
The GOP establishment's decision to protect Trump is causing more dissent within the party, with many Republicans fearing electoral losses.
Beau suggests Democrats stay quiet and let the GOP make mistakes that could lead to the downfall of the establishment.
Republicans who have distanced themselves from Trump may face challenges in influencing party decisions due to authoritarian tendencies within the GOP.

Actions:

for voters, political activists,
Support candidates who challenge the status quo within the GOP (exemplified)
Stay informed about GOP decisions and internal dynamics (suggested)
Participate in local politics to influence party directions (implied)
</details>
<details>
<summary>
2019-09-08: Let's talk about how Trump makes the best deals.... (<a href="https://youtube.com/watch?v=MuIQ4OlIuGo">watch</a> || <a href="/videos/2019/09/08/Lets_talk_about_how_Trump_makes_the_best_deals">transcript &amp; editable summary</a>)

Beau expresses lack of surprise at the president's international incompetence, questions negotiations with the Taliban, and accuses the administration of selling out allies for political gain.

</summary>

"What kind of people would kill so many in order to seemingly strengthen their position? Well for one, the Taliban."
"Everybody knows this and everybody knows that we're selling out our allies."
"The only thing we're doing right now is determining the body count."
"I did for the first month or two, but at this point, every time they do something, I'm like, Now that wasn't as bad as I thought it was going to be."
"It is going to happen."

### AI summary (High error rate! Edit errors on video page)

Expresses lack of surprise at the president's incompetence and hypocrisy on the international stage.
Criticizes the choice of a real estate lawyer for Mideast peace negotiations.
Calls out the president's decision to cancel peace negotiations with the Taliban.
Questions the moral difference between the Taliban's actions and the president's deportation policies.
Points out the futility of negotiating with the Taliban when the U.S. has announced its withdrawal.
Accuses the administration of selling out allies in Afghanistan for domestic political leverage.
Criticizes the president's negotiations with the Taliban while innocent people are dying.
Compares President Trump's actions to his past criticism of negotiating with the Taliban.
Condemns the administration's actions and questions if anyone is genuinely shocked by them.

Actions:

for critically engaged citizens,
Protest against decisions that prioritize political gain over human lives (implied)
Advocate for transparent and ethical international negotiations (implied)
</details>
<details>
<summary>
2019-09-07: Let's talk about my son and Andrew Yang.... (<a href="https://youtube.com/watch?v=TLIbgU3L7wE">watch</a> || <a href="/videos/2019/09/07/Lets_talk_about_my_son_and_Andrew_Yang">transcript &amp; editable summary</a>)

Beau dives into Universal Basic Income (UBI), discussing its historical roots, potential impact, criticisms, and the need for open debate on its viability in a capitalist framework.

</summary>

"UBI could work. So you're letting your own ideological biases interfere with you making videos about something that could work and change the world."
"It's not new, it's not radical."
"So when you hear criticisms of this, just remember that not all of them are ideological. You need to consider the source."
"This isn't socialism. This is firmly rooted in capitalism."
"It's worth talking about."

### AI summary (High error rate! Edit errors on video page)

Beau's son prompted him to talk about candidate Y's proposal X and Universal Basic Income (UBI).
UBI is not a new concept; it dates back to the 1600s, proposed by Thomas Paine.
UBI involves giving $1000 a month to all US citizens over 18, eliminating most social safety nets.
Studies show positive outcomes of UBI, but criticism is often influenced by the establishment's interests.
Andrew Yang advocates for UBI due to the threat of automation eliminating jobs.
UBI could be funded by replacing social safety nets with a Value Added Tax (VAT).
The potential impact of UBI on different sectors, like housing, is discussed.
Beau acknowledges reservations about politicians using UBI to manipulate citizens.
UBI is not socialism but rooted in capitalism and requires capitalism to function.
Beau concludes that while he has reservations, UBI is a workable plan that deserves consideration.

Actions:

for citizens, policymakers, voters,
Engage in open, constructive dialogues about UBI's implications and feasibility (implied)
Support candidates who advocate for innovative social policies like UBI (implied)
</details>
<details>
<summary>
2019-09-06: Let's talk about the history of the Second.,... (<a href="https://youtube.com/watch?v=K_Aq8ZIbDQQ">watch</a> || <a href="/videos/2019/09/06/Lets_talk_about_the_history_of_the_Second">transcript &amp; editable summary</a>)

Beau dives into the history and intention behind the Second Amendment to clarify its purpose and significance in protecting weapons of war and maintaining parity between individuals and soldiers against government tyranny.

</summary>

"The Second Amendment was designed to give parity of arms between the average person and a soldier."
"The Second Amendment was specifically designed to protect weapons of war."
"Guns don't kill people. Governments do."

### AI summary (High error rate! Edit errors on video page)

Exploring the history and meaning of the Second Amendment to clear confusion.
George Mason's role in the writing of the Second Amendment and its ties to the Virginia Declaration of Rights.
Differences between the Virginia Declaration and the Second Amendment, particularly regarding standing armies.
The intention behind the Second Amendment was to give parity of arms between the average person and a soldier.
The Second Amendment was specifically designed to protect weapons of war.
The Supreme Court's ruling in US v. Miller (1939) regarding the relationship of weapons to militia efficiency.
The purpose of the Second Amendment was to allow locals to defend against federal or foreign troops.
The Founding Fathers' foresight in including the Second Amendment as a hedge against government tyranny.
The belief that guns don't kill people; governments do.

Actions:

for history buffs, second amendment advocates,
Study the historical context and debates surrounding the Second Amendment (suggested)
Advocate for responsible gun ownership and understanding of the Second Amendment's original intent (exemplified)
</details>
<details>
<summary>
2019-09-06: Let's talk about a school house in Michigan that rocks.... (<a href="https://youtube.com/watch?v=j8zm3pasNlA">watch</a> || <a href="/videos/2019/09/06/Lets_talk_about_a_school_house_in_Michigan_that_rocks">transcript &amp; editable summary</a>)

Beau explains a Michigan school's innovative design to enhance safety, criticizing "magic pill" solutions for gun violence while advocating for holistic approaches.

</summary>

"That school will save lives."
"The magnitude of the problem we are facing in this country is a lot bigger than I think most people realize."
"There's no magic pill."
"We need to make mental health care available early."
"We are going to have to go down a long road to fix this problem."

### AI summary (High error rate! Edit errors on video page)

Discussed a school in Michigan designed to mitigate the effects of an attack with a $48 million cost.
Shared frustrations about the difficulties of defending schools due to their design flaws.
Acknowledged the distressing nature of a teacher seeking advice on being armed at school.
Praised the innovative design of the Michigan school for addressing security concerns effectively.
Emphasized the importance of solutions like the Michigan school while addressing root causes like mental health and misconceptions about masculinity.
Stressed the ineffectiveness of a "magic pill" solution like banning guns in a society with a vast number of firearms.
Illustrated the impracticality of removing guns from society by using numerical examples.
Urged for multiple solutions like the Michigan school design to save lives in the face of a complex societal problem.
Called for early mental health care availability and addressing misconceptions about masculinity as part of the solution.
Criticized politicians for promising quick fixes akin to a "magic pill" for the deep-rooted issue of gun violence.

Actions:

for community members, policymakers.,
Advocate for early mental health care availability and challenge misconceptions about masculinity (implied).
Support the implementation of innovative security designs in schools (implied).
</details>
<details>
<summary>
2019-09-06: Let's talk about Thoreau and the President.... (<a href="https://youtube.com/watch?v=n5AvTeF-aj8">watch</a> || <a href="/videos/2019/09/06/Lets_talk_about_Thoreau_and_the_President">transcript &amp; editable summary</a>)

Beau talks about Thoreau's deliberate living, influence on civil disobedience, and standing against unjust governments.

</summary>

"Under a government which imposes any unjustly, the true place for a just man is prison."
"Civil disobedience is, you can download it for free on Google."
"When confronted with a government that is behaving immorally, he said that a man cannot without disgrace associate with it."

### AI summary (High error rate! Edit errors on video page)

Beau introduces the topic of Henry David Thoreau leaving Walden Pond and rejoining society 172 years ago.
Thoreau was Harvard-educated but rejected traditional careers, choosing to live deliberately and self-reliantly.
Thoreau's connection with Ralph Waldo Emerson, a transcendentalist, influenced his beliefs in rejecting materialism and valuing spiritual over material.
Thoreau lived at Walden Pond in a cabin, working one day a week and focusing on self-discovery.
Thoreau believed that technology and social interactions were often used to fill inner voids.
Thoreau appreciated nature, saw man as part of nature, and emphasized living deliberately before death.
He wrote an essay "Civil Disobedience" criticizing President James K. Polk's administration and advocating for peaceful resistance.
"Civil Disobedience" influenced figures like Gandhi, Martin Luther King Jr., and Sophie Scholl.
Thoreau refused to pay taxes to a government supporting unjust practices like slave patrols, leading to his imprisonment.
He believed that under an unjust government, a just man's place is in prison.

Actions:

for history enthusiasts, activists,
Download and read "Civil Disobedience" for free on Google (suggested).
</details>
<details>
<summary>
2019-09-04: Let's talk about what we can learn from zombies.... (<a href="https://youtube.com/watch?v=o-plIVJo0nw">watch</a> || <a href="/videos/2019/09/04/Lets_talk_about_what_we_can_learn_from_zombies">transcript &amp; editable summary</a>)

Beau compares surviving natural disasters to zombie movies, urging a shift in perspective to see possibilities amidst challenges and empowering youth to reshape their world.

</summary>

"The rules are the same."
"You see what things could be, if you looked at them in the right way."
"People get in that mindset of, I have to get water."
"The impossible is possible."
"They can use the tools around them to make something else."

### AI summary (High error rate! Edit errors on video page)

Compares zombie movies to surviving natural disasters and life in general, noting that the rules are the same.
Suggests framing natural disaster preparedness for kids as a zombie apocalypse to make it less scary.
Explains that learning survival skills changes how people view the world and what's truly valuable in life.
Outlines key rules for surviving a natural disaster by drawing parallels to a zombie outbreak.
Emphasizes the importance of staying with a group, finding safety, and maintaining communication in a crisis.
Points out that most people in disaster situations are good but cautions about individuals looking to exploit vulnerabilities.
Shares a real-life example where people overlooked needed supplies during a crisis due to societal norms.
Stresses the importance of improvisation and seeing things differently to navigate challenging situations effectively.
Encourages teaching youth survival skills to empower them to change their circumstances and use resources creatively.
Concludes by underscoring the potential for individuals to reshape their world by realizing the possible amidst the seemingly impossible.

Actions:

for parents, educators, disaster preparedness advocates,
Teach children survival skills through fun activities and games (implied)
Organize community workshops on disaster preparedness and improvisation techniques (implied)
Encourage youth to think creatively and problem-solve in challenging scenarios (implied)
</details>
<details>
<summary>
2019-09-04: Let's talk about an appeal to the gun crowd.... (<a href="https://youtube.com/watch?v=FyngQ4xNHFA">watch</a> || <a href="/videos/2019/09/04/Lets_talk_about_an_appeal_to_the_gun_crowd">transcript &amp; editable summary</a>)

Beau outlines practical steps for gun owners to derail gun legislation, focusing on responsible ownership and ending the drug war.

</summary>

"Secure your firearms."
"Stop behaving like children."
"Just because you can do something doesn't mean that you should."
"The gun crowd is its own biggest enemy."
"They have no idea what it's for."

### AI summary (High error rate! Edit errors on video page)

Points out how owners can impede the push for gun legislation by taking individual actions.
Stresses the importance of securing firearms to prevent access by unauthorized individuals, especially kids.
Recommends against open carry and advocates for concealed carry permits for tactical advantages.
Talks about ending the drug war to reduce gun violence and eliminate the pretext for gun control.
Encourages responsible gun ownership, including securing weapons, ensuring background checks for private sales, and using good judgment.
Suggests supporting the intent behind the Second Amendment by selling or trading guns with known individuals.
Criticizes the gun culture that focuses on looking tough rather than responsible ownership.
Urges gun owners to prioritize being responsible over just following poorly written gun laws.
Emphasizes the negative impact of glorifying violence and the importance of discouraging such behavior.
Concludes by criticizing the gun crowd for prioritizing image over safety and responsible ownership.

Actions:

for gun owners,
Secure your firearms to prevent unauthorized access (implied)
Support ending the drug war to reduce gun violence (implied)
Obtain a concealed carry permit for tactical advantages (exemplified)
Prioritize responsible gun ownership over just following laws (implied)
</details>
<details>
<summary>
2019-09-04: Let's talk about Nixon, Trump, and the media.... (<a href="https://youtube.com/watch?v=EKhYeII14XM">watch</a> || <a href="/videos/2019/09/04/Lets_talk_about_Nixon_Trump_and_the_media">transcript &amp; editable summary</a>)

President Trump demands complete subservience from the media, leaving even former allies like Fox News facing consequences for not taking a stand earlier against his actions.

</summary>

"Yeah, you do."
"You standing up to Trump now means nothing."
"When the president branded other journalists as fake news, that's when you should have said something."
"Now you want to reclaim some of that objectivity. Doesn't matter, nobody's buying it."
"Without Trump, what have they got?"

### AI summary (High error rate! Edit errors on video page)

President Trump's strained relationship with the media began early in his campaign, attacking them consistently.
Trump demands total subservience, compliance, and complicity from news outlets, similar to a dictator.
Fox News, previously spared from Trump's attacks, is now facing his wrath for not fully supporting his agenda.
Some Fox News anchors are standing up to Trump, pointing out that they don't work for him.
Beau criticizes Fox News for justifying and promoting the administration's heinous acts to the American people.
Trump's targeting of CNN and now Fox News shows his disregard for any dissenting voices in the media.
Beau points out that Fox News is facing consequences for not taking a stand against Trump earlier when he attacked other journalists and threw children in cages.
The attempt by some personalities at Fox News to reclaim objectivity now that they are under attack is seen as insincere by Beau.
Beau finds it entertaining that Trump's actions are leaving former allies like Fox News on the sidelines.
Comparisons are drawn between Trump and Nixon in terms of their controversial relationships with the media and desire for revenge upon leaving office.

Actions:

for media consumers,
Stand up to demands for subservience from any source, whether political or otherwise (implied)
Take a stand against attacks on journalistic ethics and freedom of the press (implied)
</details>
<details>
<summary>
2019-09-03: Let's talk about recession indicators and a ray of sunshine.... (<a href="https://youtube.com/watch?v=pxTPtnDholA">watch</a> || <a href="/videos/2019/09/03/Lets_talk_about_recession_indicators_and_a_ray_of_sunshine">transcript &amp; editable summary</a>)

Beau warns of recession indicators like dropping consumer confidence and slowing GDP, offering insights on how normal people can navigate economic downturns.

</summary>

"Toys get cheaper, and by toys I don't mean toys for children."
"If you've been planning on bootstrapping and starting your own company, now may not be the brightest time to start it, but it may be the best time to start getting the equipment."
"It's one of those things where if we continue fighting something that is probably inevitable, we may end up hurting ourselves."

### AI summary (High error rate! Edit errors on video page)

Indicates recession indicators like the bond curve inversion, gold and silver price increase, and copper price decrease signaling hard economic times.
Mentions that consumer confidence has dropped significantly, with the University of Michigan's data from August contradicting old data from the Bureau of Economic Analysis in July.
Points out slowing growth in GDP, lowest manufacturing growth levels in a decade, and significant drop in freight shipments.
Private domestic investments have fallen by 5%, indicating a significant economic shift.
S&P 500 estimates of earnings growth are constantly dropping, a strong signal of an impending recession.
Normal people may face layoffs but could benefit from cheaper goods and opportunities to start businesses or invest in property during a recession.

Actions:

for economic observers,
Buy equipment for planned businesses or investments (exemplified)
Look for deals on property during a recession (exemplified)
</details>
<details>
<summary>
2019-09-02: Lets talk about nurses, cops, and appearances.... (<a href="https://youtube.com/watch?v=7eVhgGbzzTY">watch</a> || <a href="/videos/2019/09/02/Lets_talk_about_nurses_cops_and_appearances">transcript &amp; editable summary</a>)

Former Olympic skier turned nurse intervenes to protect patient rights, leading to officer's firing, settlement, and controversial return to work as civilian corrections assistant.

</summary>

"Nurses tend to see the best in people."
"She stood in the way of an officer abusing his authority."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Former Olympic skier turned nurse arrested for obstruction of justice at University of Utah Hospital in 2017.
Nurse intervened when police officer demanded blood draw from unconscious patient without consent.
Nurse explained patient rights and was later released without charges.
Arresting officer fired and lieutenant demoted after viral body cam footage of arrest.
Nurse settled for $500,000, part of which went towards making body camera footage public.
Officer sued for $1.5 million, claiming negativity from people who hate cops.
Nurse Wubbles listed as one of the top physicians of 2017 for standing against officer abuse of authority.
Officer started working as a civilian corrections assistant on August 9th.
Opinions vary on officer's actions and whether he should be working in a facility without patient advocates.
Beau leaves it open for viewers to share their thoughts on the situation.

Actions:

for advocates for patient rights.,
Support organizations advocating for patient rights (implied)
Advocate for transparency in law enforcement through body camera footage (implied)
</details>
<details>
<summary>
2019-09-01: Let's talk about the Great Fire of London and what it foreshadows.... (<a href="https://youtube.com/watch?v=AGJJs_nTow8">watch</a> || <a href="/videos/2019/09/01/Lets_talk_about_the_Great_Fire_of_London_and_what_it_foreshadows">transcript &amp; editable summary</a>)

Beau delves into the Great Fire of London, drawing striking parallels to today's climate crisis and the repercussions of profit-driven denial.

</summary>

"It's funny. Americans are the way we are."
"Take your arguments against it, your go-to arguments. Type it into Google, followed by the word debunked, and just read."
"People denying it when anybody who looks into it can see that it's very evident."
"Just like this."
"We don't want to waste money on something."

### AI summary (High error rate! Edit errors on video page)

Talks about the Great Fire of London in 1666, starting with a bake house near Pudding Lane.
Explains that a spark fell into fuel, setting off a massive fire due to the kindling-like surroundings.
Around 60 to 100,000 people were left homeless, with a third of London destroyed.
Despite claims of only six to eight recorded deaths, the true toll remains unknown.
Describes the lack of infrastructure and planning that exacerbated the disaster.
Mentions the warnings and knowledge of potential fire spread, but profit interests hindered preventative measures.
Points out the parallels between this historical event and today's climate crisis.
Challenges those who deny climate change to research and acknowledge the overwhelming evidence against their arguments.
Emphasizes the disproportionate impact of such crises on the powerless and poor.
Raises the issue of prioritizing profits over necessary investments in infrastructure and planning.

Actions:

for climate change activists,
Research the overwhelming evidence supporting climate change and share it with skeptics (suggested)
Advocate for prioritizing infrastructure and planning for climate resilience in your community (implied)
</details>

## August
<details>
<summary>
2019-08-31: Let's talk about the history of immigration laws in the US.... (<a href="https://youtube.com/watch?v=cKwYR_v5sLo">watch</a> || <a href="/videos/2019/08/31/Lets_talk_about_the_history_of_immigration_laws_in_the_US">transcript &amp; editable summary</a>)

Beau explains the history of US immigration laws to showcase their racist origins, calling for reform to eliminate caps and address systemic racism.

</summary>

"The law is bad. It's always been bad."
"All of them are racist. They are racist in origin."
"Any real immigration reform is going to have to get rid of these caps..."
"Because it was enacted because of racism."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains the history of major US immigration laws to address the question of why wanting people to follow laws can be seen as racist.
Points out that immigration laws prior to 1776 didn't apply since colonizers were not immigrants but rather replacing existing systems.
Mentions that from 1787 to about 1880, there were no federal immigration laws due to debates about federal government's power over immigration.
Notes the Chinese Exclusion Act of 1882 as the first law prohibiting entry based on nationality.
Talks about the Immigration Act of 1917, which expanded exclusion to Asian countries and included other restrictions like barring sex workers and mentally defective individuals.
Describes the Immigration Act of 1924, which introduced numerical caps, particularly affecting people from Asia.
Mentions the 1965 Immigration and Nationality Act, which extended caps to all countries and marked the first limitations on immigration from south of the border.
Suggests that illegal immigration as a concept emerged when caps were introduced, making it difficult for certain groups to enter legally.
States that immigration laws have always been racist in origin, leading to the need for amnesty in the 1980s.
Concludes that real immigration reform must eliminate these caps to address the racist elements embedded in the laws.

Actions:

for policy advocates, activists, educators,
Advocate for immigration reform by supporting the removal of numerical caps and addressing the racist elements in immigration laws (suggested).
Educate others on the racist origins of US immigration laws and the need for reform (exemplified).
</details>
<details>
<summary>
2019-08-30: Let's talk about why we celebrate Labor Day.... (<a href="https://youtube.com/watch?v=4aYjWs4I7Co">watch</a> || <a href="/videos/2019/08/30/Lets_talk_about_why_we_celebrate_Labor_Day">transcript &amp; editable summary</a>)

Labor Day carries dual meanings of celebrating labor and suppressing labor movements, rooted in historical events like the Haymarket riot and violent strike suppression.

</summary>

"This is a day to celebrate lefty ideas."
"Labor Day is historically linked to both celebrating labor and suppressing labor movements."

### AI summary (High error rate! Edit errors on video page)

Labor Day marks the official end of hot dog season and holds different meanings for different cultures.
Americans associate Labor Day with the fashion rule of not wearing white after.
Labor Day originated as a celebration of labor, union activity, and socialist ideals by blue-collar workers.
The first Labor Day parade was on September 5th, 1852 in New York organized by the Central Labor Union.
In the 1880s, organized labor intensified with events like the Haymarket riot, influencing the emergence of May Day.
In 1894, the Pullman Sleeping Car Company layoffs triggered a strike due to wage cuts without rent or price reductions in the company town.
President Grover Cleveland, appearing pro-labor, enacted Labor Day legislation before sending federal troops to crush the strike violently.
Labor Day was designed as a distraction from the strike's violent suppression.
Labor Day is historically linked to both celebrating labor and suppressing labor movements.
Beau warns of the dangers during Labor Day weekend, urging caution and awareness of its risks.

Actions:

for history enthusiasts,
Celebrate labor and socialist ideals within your community (implied)
Educate others on the origins of Labor Day and its historical significance (implied)
Support local labor movements and initiatives (implied)
</details>
<details>
<summary>
2019-08-30: Let's talk about Martin County High School again.... (<a href="https://youtube.com/watch?v=LfrL7y63YiU">watch</a> || <a href="/videos/2019/08/30/Lets_talk_about_Martin_County_High_School_again">transcript &amp; editable summary</a>)

Students at Martin County High School face bullying daily, seeking support and tolerance from an administration that fails to take action.

</summary>

"We just want tolerance because we tolerate everyone around us and we deserve that too."
"Students are asking for your help, too."
"You don't need help, y'all need backup, that's it, and you've got it."

### AI summary (High error rate! Edit errors on video page)

Students at Martin County High School in Kentucky wore pro-LGBTQ shirts to support a fellow student and were made to change by the administration.
The administration expressed concern about bullying, but the students had already decided to wear the shirts again as a form of solidarity.
Despite facing bullying every day, the administration fails to take action to address the issue.
A hit list circulated at the school, causing students to stay home, only to find the administrator and police officer joking about it upon their return.
The lack of seriousness and support from the administration is appalling to Beau, especially considering the safety and security of the students.
Beau questions the administration's ability to provide a safe environment for the students and offers the assistance of friends from Fort Campbell or Wright-Patt to step in.
The superintendent claimed a miscommunication had been resolved, but students were left in the dark without any communication from them.
Students are labeled as attention-seeking by faculty when they are actually seeking support and tolerance.
The students, despite facing cynicism and opposition, express their desire for acceptance and tolerance within the school.
Beau encourages the students to continue fighting for what they believe is right and assures them of his support and that of others watching.

Actions:

for students, lgbtq community, supporters,
Support the students at Martin County High School by wearing pride flags or showing solidarity in any way possible (implied)
Encourage tolerance and acceptance within your own community and schools (implied)
</details>
<details>
<summary>
2019-08-29: Let's talk about LGBTQ students at a Kentucky High School.... (<a href="https://youtube.com/watch?v=IGvyk3LxiHQ">watch</a> || <a href="/videos/2019/08/29/Lets_talk_about_LGBTQ_students_at_a_Kentucky_High_School">transcript &amp; editable summary</a>)

High school students forced to change for wearing LGBTQ clothing face silence instead of support, revealing a double standard in Kentucky's school system.

</summary>

"Just do whatever the mean man says. Don't make yourself a target. Hide who you are."
"Maybe it's been resolved. I don't know. But there's no quotes from the teens saying that it was. None."
"They won't have any of those mean and hurtful words on them, and they will be completely in line with Tinker versus Des Moines."
"My advice to the superintendent and the school administrator is to leave these kids alone."

### AI summary (High error rate! Edit errors on video page)

High school students at Martin County High School in Kentucky wore pro-LGBTQ clothing and were forced to change by the school administrator.
The school claimed they were worried about the students being bullied, but allowed students to wear Confederate flags.
The students felt like they were silenced while the school didn't address the bullies.
The administrator told the students that their clothing wasn't appropriate and that school wasn't the place to talk about sexual orientation.
Despite the superintendent claiming the issue was resolved as miscommunication, a student fears further disciplinary action.
Beau questions the lack of confirmation from the teens that the issue was truly resolved and urges them to speak up to prevent it from being ignored.
Teens have the right to free speech and expression according to the Supreme Court, as stated in Tinker versus Des Moines.
Beau encourages the superintendent and school administrator to leave the students alone.

Actions:

for students, educators, activists,
Contact the students to offer support and ensure their voices are heard (implied).
Advocate for inclusive policies and support systems within schools (implied).
Stand up against discrimination and bullying in educational environments (implied).
</details>
<details>
<summary>
2019-08-29: Let's talk about Hurricane Dorian vs Florida Man.... (<a href="https://youtube.com/watch?v=g-WqaIoTXN4">watch</a> || <a href="/videos/2019/08/29/Lets_talk_about_Hurricane_Dorian_vs_Florida_Man">transcript &amp; editable summary</a>)

Beau warns Floridians about Hurricane Dorian's potential impact, urges necessary precautions, and questions FEMA funding diversion amid immigration policies.

</summary>

"Get your old people out."
"Don't do anything stupid during the storm."
"Be prepared to be on your own for a little while."
"We know that 90 miles of ocean doesn't deter the drive for freedom."
"Because of our experiences here in Florida, we know that wall isn't going to work."

### AI summary (High error rate! Edit errors on video page)

Addressing Floridians about Hurricane Dorian approaching the state, specifically central Florida.
Warning about the potential impact of the hurricane, citing past surprises like Hurricane Michael.
Urging people to take necessary precautions and evacuate if needed, especially for the elderly.
Advising on essentials to have on hand: phone batteries, fuel for generators, food, water, first aid kit, and medications.
Cautioning against risky behavior during the storm, calling out Gainesville residents.
Noting the diversion of FEMA funds to immigration efforts, potentially affecting hurricane response.
Mentioning the historical delays in federal funding post-hurricane, especially in the panhandle.
Acknowledging Florida's preparedness due to previous experiences but warning of potential self-reliance needed.
Mentioning the state of emergency declaration and the impending presence of the Florida National Guard.
Connecting the lack of FEMA funding to immigration policies and questioning the effectiveness of a border wall.

Actions:

for floridians,
Evacuate if necessary and help others, especially the elderly, to do so (implied).
Stock up on essentials like phone batteries, fuel, food, water, first aid kit, and medications (implied).
Stay safe during the storm and avoid risky behavior (implied).
Be prepared to be self-reliant for a few days post-hurricane (implied).
Support local communities and organizations in hurricane preparation and response (implied).
</details>
<details>
<summary>
2019-08-28: Let's talk about how we talk about climate change.... (<a href="https://youtube.com/watch?v=OrwIfQgxuao">watch</a> || <a href="/videos/2019/08/28/Lets_talk_about_how_we_talk_about_climate_change">transcript &amp; editable summary</a>)

Beau stresses the urgency of framing climate change as an infrastructure project and seizing the economic stimulus potential during a recession to drive clean energy initiatives forward.

</summary>

"We are running out of time to sit there and act as if all ideas are equal when it comes to the debate over climate change."
"It's an infrastructure project to bring about clean energy. That's what it is."
"When the recession hits, this is your economic stimulus package."
"The urgency is not about a doomsday scenario in 2030 but about avoiding very, very bad outcomes."
"We know what needs to be done; we just need to be ready for it."

### AI summary (High error rate! Edit errors on video page)

Climate change discourse is hindered by how it's framed and discussed, with media simplifying complex issues into sound bites.
The 12-year timeline to combat climate change was derived from the need to reach net zero CO2 emissions by 2050, requiring emissions to decrease significantly by 2030.
The urgency is not about a doomsday scenario in 2030 but about avoiding very negative outcomes by 2050.
Climate change action is essentially an infrastructure project aimed at transitioning to clean energy, similar to past massive infrastructure projects like indoor plumbing or the internet.
Failing to address climate change could lead to catastrophic consequences like irreversible melting of ice caps and extreme environmental disruptions.
Trump's economic policies could inadvertently provide an ideal backdrop for implementing climate change initiatives as an economic stimulus or jobs program during a potential recession.
The focus should be on utilizing the economic downturn as an opportune moment to push forward climate change actions, akin to the New Deal era.
The urgency of addressing climate change cannot be understated, requiring a shift towards decisive actions rather than debating the validity of climate science.

Actions:

for climate activists, policymakers, concerned citizens,
Prepare for and support climate-friendly policies during economic downturns (implied)
Advocate for viewing climate change action as an infrastructure project (implied)
Educate communities on the importance of transitioning to clean energy (implied)
</details>
<details>
<summary>
2019-08-28: Let's talk about a joke from the President.... (<a href="https://youtube.com/watch?v=sE336w15T_M">watch</a> || <a href="/videos/2019/08/28/Lets_talk_about_a_joke_from_the_President">transcript &amp; editable summary</a>)

Beau delves into the dangerous implications of the president's "joke" and its undermining of the rule of law, warning of unchecked power and potential repercussions for those in his administration.

</summary>

"Don't worry, I'll pardon you."
"It's not a joke, it's an inducement to commit a crime, it's an overt act in furtherance of a criminal conspiracy."
"Man, that kind of power is intoxicating, and it will be used and abused."
"When it's over, you're the one that doesn't have a chair."
"Anyway, it's just a thought."

### AI summary (High error rate! Edit errors on video page)

Analyzing a joke made by the president and the gravity behind it.
Exploring the context in which the president's statement was made.
Examining the president's history of questionable actions and why his statement cannot be taken lightly.
Comparing the president's statement to inducements to commit crimes in other scenarios.
Addressing the danger of the president undermining the rule of law and placing his administration above it.
Expressing concerns about the impact on federal agencies and the dangerous implications of unchecked power.
Warning individuals in the administration about the potential consequences when the administration ends.
Emphasizing the lack of protection for those not in the limelight within the administration.
Concluding with a reflection on the dangerous nature of the situation and its implications.

Actions:

for policy advocates,
Contact policymakers to demand accountability for actions that undermine the rule of law (implied)
Stay informed on political developments and hold leaders accountable (implied)
</details>
<details>
<summary>
2019-08-27: Let's talk about the supporting the troops, the Hmong, and Iraq.... (<a href="https://youtube.com/watch?v=WjwASgtKRMk">watch</a> || <a href="/videos/2019/08/27/Lets_talk_about_the_supporting_the_troops_the_Hmong_and_Iraq">transcript &amp; editable summary</a>)

In modern warfare, supporting troops means prioritizing local communities, not slogans, while history repeats the betrayal of allies risking their lives.

</summary>

"The reality is, in modern war, your support doesn't matter."
"They are being left to die. That is what is going to happen to them."
"We learned nothing from Vietnam. We are making the same mistakes again."

### AI summary (High error rate! Edit errors on video page)

Exploring the concept of supporting the troops in modern times, beyond bumper stickers and slogans.
Emphasizing that supporting troops primarily means the local populace in areas of operations.
Providing a historical example of the Hmong people's betrayal after assisting the U.S. during the Vietnam War.
Criticizing the abandonment of Iraqi nationals who helped the U.S., facing visa backlogs and imminent danger.
Pointing out the plight of interpreters waiting for visas, with a drastic decline in approvals.
Expressing concern for individuals risking their lives and being left behind due to changing political tides.
Raising the issue of child soldiers and questioning the widespread nature of this practice.
Drawing parallels between past mistakes, like Vietnam, and the current abandonment of allies.

Actions:

for advocates for human rights,
Advocate for expedited visa processing for Iraqi nationals and interpreters (implied)
Support organizations aiding at-risk allies and interpreters (exemplified)
</details>
<details>
<summary>
2019-08-26: Let's talk about Episode 1: Education (<a href="https://youtube.com/watch?v=s-DdK7BqWa8">watch</a> || <a href="/videos/2019/08/26/Lets_talk_about_Episode_1_Education">transcript &amp; editable summary</a>)

Beau dives into the interconnected issues of underfunded schools, the value of education, and the stereotypes that discourage learning.

</summary>

"No teacher should have to appeal to the public for tools they need when they should be provided."
"Education is not its own reward, we focus on graduation, not the value of learning itself."
"No education is wasted, never stop learning, always stay curious."

### AI summary (High error rate! Edit errors on video page)

Teachers asking for help to clear their lists for supplies.
The President's idea of nuking a hurricane won't work due to heat energy.
The root cause of underfunded schools lies with those in power, not the uneducated.
Policy makers prioritize creating worker bees over critical thinkers.
Society values job skills over a comprehensive education.
Increasing funding for schools may improve outcomes, but it's more about valuing education.
Education is an ongoing, daily benefit, not just about graduation.
The impact of a great teacher surpasses that of a great school.
Education should focus on instilling a love for learning, not just obtaining a diploma.
Stereotypes discourage men and women from valuing education.
Real tough guys, like military leaders, prioritize education.
Education enriches lives and should be valued by all.

Actions:

for educators, policymakers, community members.,
Advocate for comprehensive education that includes arts, music, and humanities (implied).
Support educational causes like Pencils of Promise that build schools in underserved communities (implied).
Challenge stereotypes and encourage a love for learning in both men and women (implied).
</details>
<details>
<summary>
2019-08-25: Let's talk about trade, taxes, emergencies, and power.... (<a href="https://youtube.com/watch?v=OddtcH2rhoY">watch</a> || <a href="/videos/2019/08/25/Lets_talk_about_trade_taxes_emergencies_and_power">transcript &amp; editable summary</a>)

Beau warns about unchecked presidential power leading to potential dictatorship and advocates for limiting executive authority to safeguard democracy and prevent economic devastation.

</summary>

"President regrets not raising tariffs higher, implying he wishes to tax Americans more."
"He can destroy the economy with a pen stroke. He shouldn't, but he could."
"We're not living in a representative democracy because we subverted it."

### AI summary (High error rate! Edit errors on video page)

President regrets not raising tariffs higher, implying he wishes to tax Americans more.
Americans misunderstand tariffs; President taxes American importers on Chinese goods, passing the cost to consumers.
President has authority, under the International Emergency Economic Powers Act, to end trade with China.
This authority grants the presidency immense power, designed for catastrophic scenarios like nuclear war.
Beau believes the presidency has too much power, risking a dictatorship where a pen stroke alters lives.
Advocates for limiting presidential power to uphold democracy and prevent potential abuse by future leaders.
Urges rescinding executive powers to prevent authoritarian behavior in the future.
Warns that without checks on executive power, a leader like Trump could severely impact millions without resistance.
Acknowledges the president's authority to devastate the economy with a single decision.
Calls for action to address the excessive powers vested in the presidency for safeguarding democracy.

Actions:

for american citizens,
Advocate for legislative measures to limit presidential powers (implied)
Educate others on the implications of unchecked executive authority (implied)
</details>
<details>
<summary>
2019-08-25: Let's talk about Cherokee people and the House.... (<a href="https://youtube.com/watch?v=5bdWiqybsUg">watch</a> || <a href="/videos/2019/08/25/Lets_talk_about_Cherokee_people_and_the_House">transcript &amp; editable summary</a>)

The Cherokee Nation seeks to appoint a delegate to the House of Representatives, reflecting on historical treaties and the potential for Native representation in government.

</summary>

"The federal government has a horrible track record of honoring treaties with native nations."
"The idea of Native interest being represented on the house floor is going to be great."

### AI summary (High error rate! Edit errors on video page)

The Cherokee Nation wants to appoint a delegate to the House of Representatives as allowed by a treaty signed in 1835.
This treaty ultimately led to the Trail of Tears, a genocidal event that forcibly removed indigenous people from their land.
The federal government's relationship with indigenous cultures is framed as two sovereign nations interacting, but honoring treaties has been an issue.
Despite having voting rights, gerrymandering has significantly limited the voting power of the Cherokee Nation, consisting of 350,000 to 375,000 people.
The potential delegate from the Cherokee Nation, likely Kimberly Teehee, may not have voting powers but can represent Native nations' issues in the House.
Concerns exist that only the Cherokee Nation has this provision in their treaty, leading to worries about exclusive representation.
Kimberly Teehee is viewed as a front runner for the position and serves as a diplomat between the Cherokee Nation and the U.S. government.
While the process may take time, having Native interests represented in the House could significantly impact Native communities positively.
Beau believes that the representation of Native interests in the House will make C-SPAN more engaging and is worth the support.

Actions:

for advocates for native rights,
Support Native representation efforts within the government (suggested)
Stay informed about the developments in Native rights and representation (implied)
</details>
<details>
<summary>
2019-08-24: Let's talk about the Republicans trying to save the country.... (<a href="https://youtube.com/watch?v=HMlbWZkhYU4">watch</a> || <a href="/videos/2019/08/24/Lets_talk_about_the_Republicans_trying_to_save_the_country">transcript &amp; editable summary</a>)

Beau urges Joe Walsh to take action in primarying President Donald Trump to potentially save the Republican Party and secure his place in history.

</summary>

"If you're serious, do it."
"If you're gonna do it, do it. Save the country and save your party."
"Your only option is to primary the president because a lot of Republicans have seen what he is now."
"I don't particularly like you, to be honest, but as far as getting that man out of office, we're on the same page."
"Maybe you can save your party."

### AI summary (High error rate! Edit errors on video page)

Talks about a former Republican congressman turned conservative radio talk show host, Joe Walsh, who wants to primary President Donald Trump.
Urges Walsh to stop talking and actually take action if he is serious about challenging Trump.
Describes Walsh as the Republican Party's potential savior due to changing demographics and his ability to appeal to moderates.
Points out Walsh's social liberalism, clear communication skills, and lack of fascist tendencies as reasons he could make a difference.
Emphasizes the importance of Walsh's independent platform in reaching voters without GOP support.
Warns that the Republican Party is on life support and primarying Trump may be the only way to maintain it.
Suggests that many Republicans may not support Trump in the next election, potentially leading them to stay home instead of voting for him.
Acknowledges personal reservations about Walsh but supports the idea of removing Trump from office.
Concludes by stating that ousting Trump from the ticket could secure Walsh's place in history and potentially save the Republican Party.

Actions:

for republicans, political activists,
Primary President Donald Trump (implied)
Mobilize support for a potential challenge within the Republican Party (implied)
</details>
<details>
<summary>
2019-08-23: Let's talk about thanking Pia Klemp.... (<a href="https://youtube.com/watch?v=zf-xXnirnCM">watch</a> || <a href="/videos/2019/08/23/Lets_talk_about_thanking_Pia_Klemp">transcript &amp; editable summary</a>)

Beau introduces "Thank a Criminal Day," shares Pia Klemp's powerful stance on equality, and appreciates her actions.

</summary>

"We do not need authorities deciding about who is a hero and who is illegal."
"Thank you, Pia Klemp."

### AI summary (High error rate! Edit errors on video page)

Beau introduces the concept of "Thank a Criminal Day" in his video.
He shares a quote from Pia Klemp, a German ship captain who has saved thousands of migrants in the Mediterranean Sea.
Pia Klemp is facing accusations in Italy for assisting illegal immigration and working with human traffickers.
Italy is not the only government opposed to Pia's actions, with Paris offering her a medal which she turned down.
Pia's response to the medal offer is scathing, criticizing the authorities for their treatment of migrants and asylum seekers.
She rejects the notion of authorities deciding who is a hero and who is illegal, asserting that everyone is equal.
Beau expresses gratitude towards Pia Klemp for her actions and her powerful statement.
He commends Pia's sentiment and describes it as a wonderful statement.
Beau concludes the video by wishing his viewers a good night.

Actions:

for advocates for equality,
Support organizations aiding migrants and asylum seekers (suggested)
Advocate for fair treatment of migrants and asylum seekers (implied)
</details>
<details>
<summary>
2019-08-23: Let's talk about Trump's trade war, automation, and board games.... (<a href="https://youtube.com/watch?v=Dz-QFGhjhco">watch</a> || <a href="/videos/2019/08/23/Lets_talk_about_Trump_s_trade_war_automation_and_board_games">transcript &amp; editable summary</a>)

Beau criticizes the economic decisions affecting American companies, warns of increased inequality, and calls for a fair society where everyone can participate equally.

</summary>

"Bringing production facilities back won't bring back jobs as automation has replaced manual labor."
"Need for a new system not built on human exploitation."
"People get so far ahead that they start mocking those in poverty."
"Upheaval, the pitchforks come out."
"Build a society that is more equitable, that is more fair, a society in which everybody can stay in the game."

### AI summary (High error rate! Edit errors on video page)

Chinese tariffs on another $75 billion imposed.
President orders American companies to find alternative to China.
Blending corporate and government interests.
President's exact words: American companies must start looking for alternative to China, including bringing companies home and producing in the US.
Unlikely for companies to return production to the US; more likely to move to Vietnam or similar countries.
Bringing production facilities back won't bring back jobs as automation has replaced manual labor.
Automation is cheaper and more reliable than human labor.
U.S. losing in trade war against China.
Move increases chance of recession and income inequality.
Need for a new system not built on human exploitation.
Trump's scapegoating in blaming Jay Powell for economic issues.
Loss of largest trading partner (China) without valid reason.
Beau points out the need for a more equitable society where everyone can "stay in the game."
Contrasts Monopoly game dynamics with real life inequality and exploitation.
Calls for a fair society where everyone starts off equal.

Actions:

for economic activists, social justice advocates,
Advocate for fair trade policies (implied)
Support movements for income equality (implied)
Engage in community organizing for equitable systems (implied)
</details>
<details>
<summary>
2019-08-22: Let's talk about #ThankACriminalDay and #FreeRoss.... (<a href="https://youtube.com/watch?v=tvg2C2yZN-w">watch</a> || <a href="/videos/2019/08/22/Lets_talk_about_ThankACriminalDay_and_FreeRoss">transcript &amp; editable summary</a>)

Beau questions the criminal justice system's treatment of non-violent offenders and advocates for Ross Ulbricht's clemency, challenging the narrative around criminals on Criminal Day.

</summary>

"If there is no victim, there is no crime."
"Life in the federal system means life."
"That guy paid the price. The problem is, the price we're asking him to pay is too high."
"Criminals have changed the world and they will continue to."
"He won't get out. That's a little much for a website."

### AI summary (High error rate! Edit errors on video page)

Celebrates August 22nd, Criminal Day, created by Brian to honor criminals who have made a positive impact.
Questions the notion that soldiers protect freedom, arguing that criminals actually obtain it for us.
Mentions heroes like Martin Luther King Jr., whose actions were considered criminal at the time.
Talks about Ross Ulbricht, the creator of Silk Road, an online platform for drug sales, and his controversial case.
Points out that Ross Ulbricht received two life sentences plus forty years for a non-violent offense.
Raises the question of whether prison is about rehabilitation or punishment in Ross's case.
Describes Ross Ulbricht as a nerdy Eagle Scout who believed in the freedom for individuals to make their own choices.
Emphasizes that Ross is not a danger to society and does not deserve his harsh sentence.
Encourages viewers to visit freeross.org to learn more about Ross's case and support the petition for his clemency.
Argues that if there is no victim, there is no crime, and advocates for a reevaluation of the justice system.

Actions:

for justice reform advocates,
Visit freeross.org to learn more and support the petition for Ross Ulbricht's clemency (suggested).
Share the video with the hashtags #ThankCriminalDay and #FreeRoss (suggested).
</details>
<details>
<summary>
2019-08-21: Let's talk about whistling while drowning and the 2020 election.... (<a href="https://youtube.com/watch?v=ApBJra1taIA">watch</a> || <a href="/videos/2019/08/21/Lets_talk_about_whistling_while_drowning_and_the_2020_election">transcript &amp; editable summary</a>)

Beau reveals parallels between rescuing drowning victims and the administration's tactics, warning of divisive dog whistles to energize a shrinking base in the upcoming election.

</summary>

"He has to energize his base and he thinks his base is bigger than it is."
"His base is not going to carry him to victory, but that's who he wants to energize."
"We just need to be ready for them."
"Either you are completely incompetent or your dog whistling to a racist base."
"Is he stupid or is he a racist? That's it."

### AI summary (High error rate! Edit errors on video page)

Recalls taking a wilderness first responder course with realistic training scenarios.
Volunteers to rescue a drowning victim during training, facing unexpected challenges.
Draws a parallel between rescuing drowning victims and the current state of the administration.
Describes how panic affects drowning victims and likens it to the president's situation.
Analyzes the president's strategy to energize his base for the upcoming election.
Points out that negative voter turnout, not overwhelming support, led to the president's victory in the past.
Identifies the president's base as bigoted old white men and explains his focus on energizing them.
Criticizes the president's proposal to end birthright citizenship through executive order.
Explains the historical significance of the 14th Amendment and its importance to certain demographics.
Condemns the president's rhetoric around the 14th Amendment as a huge dog whistle to his base.
Questions the need to indefinitely detain migrant children and points out the profit-driven motives behind it.
Challenges the illogical reasoning behind holding children longer in detention facilities.
Dispels common misconceptions about birthright citizenship and who actually engages in birth tourism.
Warns of future attacks on marginalized groups by the president to rally his base and maintain power.
Urges readiness to identify and address the dog whistles used by the president to appeal to his base.

Actions:

for voters, activists, allies,
Recognize and challenge dog whistles in political rhetoric (suggested)
Stay vigilant against divisive tactics targeting marginalized groups (implied)
</details>
<details>
<summary>
2019-08-21: Let's talk about Narcan and Epi-pens.... (<a href="https://youtube.com/watch?v=xT89xscuVt0">watch</a> || <a href="/videos/2019/08/21/Lets_talk_about_Narcan_and_Epi-pens">transcript &amp; editable summary</a>)

Beau addresses misconceptions about Narcan and EpiPens, debunking myths and revealing societal attitudes towards addiction and pharmaceutical pricing.

</summary>

"Well, addicts should die."
"It encourages the devaluation of human life."
"Can't do anything about that. Need to kick down. That lowly addict."
"No sane person is angry about that."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Addresses a question about a meme comparing the cost of Narcan to EpiPens.
Clarifies that Narcan is not free; someone pays for it, and there are ways to cover the cost.
Explains the process where EMS administers Narcan and bills for it, just like any other life-saving drug.
Questions whether people want a scenario where insurance is checked before administering life-saving treatment.
Mentions grants, insurance companies, treatment centers, and foundations that cover the cost of Narcan.
Talks about free training available for Narcan administration on International Overdose Awareness Day.
Debunks the argument that Narcan enables addiction, as it actually inhibits effects and can lead to recovery.
Addresses the stigma surrounding substance abuse and how it influences perceptions of who should receive life-saving drugs.
Comments on society's anger towards pharmaceutical companies for high prices, like $600 EpiPens with a manufacturing cost of $2.
Conveys the message that societal anger often gets misdirected towards vulnerable groups like addicts.

Actions:

for advocates, community members,
Contact local EMS or substance abuse treatment centers to inquire about Narcan training opportunities (suggested).
Support foundations that provide funding for Narcan distribution (exemplified).
Advocate for pharmaceutical pricing transparency and fair drug pricing (implied).
</details>
<details>
<summary>
2019-08-20: Let's talk about PP and life after Title X.... (<a href="https://youtube.com/watch?v=dcGT848vJDY">watch</a> || <a href="/videos/2019/08/20/Lets_talk_about_PP_and_life_after_Title_X">transcript &amp; editable summary</a>)

Beau breaks down misconceptions around Planned Parenthood's funding and services, urging support to maintain vital healthcare access beyond abortion.

</summary>

"Well golly gee whiz, it seems like they do things other than abortion."
"Why abort when adoption is an option?"
"Congratulations, you played yourself."

### AI summary (High error rate! Edit errors on video page)

Explains the situation with Title X funding and Planned Parenthood, pointing out misconceptions and facts.
Describes how Planned Parenthood withdrew from Title X programs due to a rule prohibiting abortion referrals in exchange for funding.
Clarifies that the $60 million given up by Planned Parenthood was not used for abortions but for services like STD screenings and birth control under Title X.
Counters the idea of Planned Parenthood being defunded by putting the $60 million loss into perspective compared to their overall revenue of $1.67 billion.
Reveals that Planned Parenthood received $563 million in tax dollars last year, the majority of which came from Medicaid and was not used for abortions.
Notes that Planned Parenthood will need to increase fundraising by 10% to make up for the loss of $60 million.
Urges individuals to donate or purchase merchandise to support Planned Parenthood's health centers.
Emphasizes the various healthcare services provided by Planned Parenthood beyond abortion, including breast exams and birth control.
Points out the irony that reducing access to birth control can lead to more unplanned pregnancies and, consequently, more abortions.
Explains the importance of private donations in keeping Planned Parenthood health centers operational and serving underserved communities.

Actions:

for advocates, donors, supporters,
Support Planned Parenthood by donating directly or purchasing merchandise (suggested).
Increase donation to Planned Parenthood by 10% to help maintain their health centers (suggested).
Educate others on the misconceptions surrounding Planned Parenthood's funding and services (implied).
</details>
<details>
<summary>
2019-08-20: Let's talk about  nonsense and getting lost in Wonderland.... (<a href="https://youtube.com/watch?v=RA3H6CaGd1E">watch</a> || <a href="/videos/2019/08/20/Lets_talk_about_nonsense_and_getting_lost_in_Wonderland">transcript &amp; editable summary</a>)

Beau narrates the bizarre events in Wonderland under the rule of the erratic Jack of Diamonds, showcasing economic turmoil, communication chaos, and unfounded fears of invasion.

</summary>

"Must be a tradition here in Wonderland, I'm not really sure."
"He tends to blame foreigners for the loss of jobs rather than automation."
"Currently in the palace, Paranoia is running deep."
"A strange place here. It is a strange place."
"If I had a world of my own everything would be nonsense."

### AI summary (High error rate! Edit errors on video page)

Beau finds himself in Wonderland after falling down a hole with his dog, recounting the bizarre events unfolding.
The Jack of Diamonds has taken power through multiple marriages, mirroring the Queen of Hearts' style of ruling by beheading officials.
The leader has initiated a trade war with the Land of Oz, causing economic distress and material shortages in Wonderland.
Communication is chaotic, with messages delivered by borrowed birds from Snow White, leading to confusion and paranoia.
The Jack of Diamonds blames foreigners for job losses, inciting fear and deflecting from the real issues like automation.
In an attempt to help the poor, royal funds were given to the rich, who hoarded the money instead of aiding others.
Paranoia and witch hunts are rampant, prompting extreme security measures like contacting Elsa to build an ice wall.
Destruction of windmills and plans for clean coal reveal misguided environmental policies and uninformed decisions.
Attempts to purchase grain acres and suppress riots show a disconnect from reality and a reliance on authoritarian tactics.
The presence of a fearful lion immigrant illustrates misplaced fears of invasion by unarmed individuals seeking safety.

Actions:

for observers, wonderland residents,
Organize community meetings to address economic concerns and propose solutions (suggested)
Support local unions and workers affected by tariffs through donations or advocacy efforts (exemplified)
Educate fellow citizens on the importance of fact-checking and critical thinking in the face of misinformation (implied)
</details>
<details>
<summary>
2019-08-19: Let's talk about the future of Afghanistan.... (<a href="https://youtube.com/watch?v=RFU6jw9iyCU">watch</a> || <a href="/videos/2019/08/19/Lets_talk_about_the_future_of_Afghanistan">transcript &amp; editable summary</a>)

The U.S. is leaving Afghanistan, leaving behind a mess and facing the reality of Taliban resurgence, with no easy solutions in sight.

</summary>

"We are leaving, and we are leaving behind a mess."
"The time to figure that out is over. It's going to happen."
"Giving jets to the Afghan government is risky."
"Helicopters could help change the course of the war."
"This region of the world, this is where empires go to die."

### AI summary (High error rate! Edit errors on video page)

Four main groups operating in Afghanistan: U.S., Afghan government, Taliban, and I.S.
Taliban trying to reestablish itself as U.S. leaves, leading to dynamics changing.
U.S. selling out the Afghan government by talking directly to the Taliban without them.
Negotiators have no leverage with Taliban because they know the U.S. is leaving.
Taliban likely to reassert control over Afghanistan; U.S. probably needs to give them supplies to follow preferred route.
Staying in Afghanistan prolongs violence and doesn't help stabilize the country.
Violence will continue after the U.S. leaves, and Taliban might reassert control.
U.S. mindset of military solutions won't clean up the mess in Afghanistan.
Insurgents have historically won despite facing technologically advanced military forces.
Giving jets to the Afghan government is risky as Taliban or IS might end up controlling them.
Helicopters could be valuable aid as neither Taliban nor IS have significant air capabilities.
Need to learn from the mistakes in Afghanistan and plan military operations with the aftermath in mind.

Actions:

for policy makers, activists,
Provide humanitarian aid to Afghan civilians (implied)
Advocate for diplomatic solutions to stabilize Afghanistan (implied)
</details>
<details>
<summary>
2019-08-19: Let's talk about Trump, Shell, and Representative Democracy.... (<a href="https://youtube.com/watch?v=bkFyFxKpIhk">watch</a> || <a href="/videos/2019/08/19/Lets_talk_about_Trump_Shell_and_Representative_Democracy">transcript &amp; editable summary</a>)

Beau criticizes the Shell Trump photo-op, exposes unfair treatment of workers, and calls for more working-class representation in government for true democracy.

</summary>

"You're not actually going to be doing any work you don't need your safety gear except for that shirt so it looks like you're working class because you're a prop nothing more."
"What we really need is a lot more bartenders, carpenters, plumbers, people like that in the House of Representatives, people that actually represent us."
"We need a little bit more working class unity."

### AI summary (High error rate! Edit errors on video page)

Critiques the blending of corporate and state interests in the Shell Trump photo-op.
Explains how Shell contractors faced unfair choices during the event.
Mentions leaked memos revealing instructions for workers to appear as props.
Describes how corporations repay politicians by using workers as props.
Points out the false image created by such propaganda.
Analyzes how politicians manipulate public perception through divisive tactics.
Questions the dominance of certain professions in running the country.
Calls for more representation of working-class individuals in positions of power.
Advocates for unity among the working class for better governance.
Suggests the need for genuine representative democracy.

Actions:

for working-class americans,
Advocate for more working-class representation in government (implied)
Foster unity and communication among the working class (implied)
</details>
<details>
<summary>
2019-08-19: Let's talk about Apple, Trump, Trade Wars, and Farmers.... (<a href="https://youtube.com/watch?v=ufjjjqwJnQs">watch</a> || <a href="/videos/2019/08/19/Lets_talk_about_Apple_Trump_Trade_Wars_and_Farmers">transcript &amp; editable summary</a>)

Tim Cook exposes Trump's trade war, revealing the administration's favoritism towards the wealthy over American farmers.

</summary>

"He doesn't care about you, he never did, and he never will."
"It was just stuff he said to get people to follow him, that's it, that's all it was."
"That's President Trump's administration."

### AI summary (High error rate! Edit errors on video page)

Tim Cook exposed President Trump's trade war during a dinner meeting at the president's golf club.
Apple pays tariffs on Chinese components, making it hard to compete with companies not involved in the trade war like Samsung.
President Trump found Cook's argument compelling and is reconsidering the trade war.
Beau questions why American farmers didn't receive similar consideration during the trade war.
The Secretary of agriculture dismissed farmers' concerns as whining.
President Trump's administration favors the wealthy, with policies benefiting those with high net worth.
The administration's policies contradict the rhetoric aimed at common people.
Beau criticizes Trump for prioritizing personal gain over the well-being of Americans.
Trump's presidency is characterized by self-interest and exploitation of taxpayers.
Beau concludes by asserting that Trump never cared about the American people.

Actions:

for american voters,
Support policies that prioritize the well-being of all Americans, not just the wealthy (implied).
</details>
<details>
<summary>
2019-08-16: Let's talk about a wholesome story for 2019 in Arkansas.... (<a href="https://youtube.com/watch?v=RFaEIabn4qA">watch</a> || <a href="/videos/2019/08/16/Lets_talk_about_a_wholesome_story_for_2019_in_Arkansas">transcript &amp; editable summary</a>)

Four African-American boys fundraising for football faced racial bias and a gun in a white neighborhood in 2019, leading to charges against the aggressor with ties to law enforcement.

</summary>

"Four teenage boys had a revolver shoved in their face because they were fundraising for football in a white neighborhood."
"Man, that is some Dred Scott stuff right there."
"How could this happen? But we have to say it every day."
"Because well, I mean, they don't have any real rights."
"Anyway, it's just a fault, y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Four boys, ages 15 and 16 in Arkansas, finished football practice and started fundraising in August 2019.
The boys, African-American, were selling discount cards door-to-door when a woman confronted them with a gun, mistaking them for suspicious individuals.
The woman called the cops, describing the boys as suspicious because they were African-American in a white neighborhood.
Despite one of the cops recognizing the boys as school resource officer, the woman lectured them on "horseplay" instead of apologizing after the misunderstanding.
Beau draws parallels to the infamous Dred Scott case, pointing out the persistent racist attitudes that lead to incidents like this.
The woman, Jerry Kelly, 46, was charged with four counts of aggravated assault, false imprisonment, and endangering the welfare of a minor.
Kelly's connection to law enforcement through her previous employment and her husband's position raised concerns about preferential treatment in the case.
The incident underscores the ongoing racial prejudices and dangers faced by African-American youth, even in 2019.
Beau questions the progress made in society when incidents like this continue to occur.
The boys, engaging in a wholesome activity, faced a traumatic experience due to racial bias and ignorance.

Actions:

for community members,
Support community initiatives promoting racial equality and understanding (suggested)
Advocate for fair treatment and justice for victims of racial discrimination (implied)
Stand against racial prejudices and biases in your community (implied)
</details>
<details>
<summary>
2019-08-15: Let's talk about Trump telling the truth about the economy.... (<a href="https://youtube.com/watch?v=eHJlCZjLttc">watch</a> || <a href="/videos/2019/08/15/Lets_talk_about_Trump_telling_the_truth_about_the_economy">transcript &amp; editable summary</a>)

Beau exposes the president's dismissal of experts as "fake news" and warns against blind loyalty, urging scrutiny based on actions, not words.

</summary>

"There is no giant scheme and group of people out to get the President of the United States. He's not that important."
"The trust fund baby billionaire is not your friend."
"He's admitted that he used to get politicians money. So he, as a businessman, cut them out."
"The policies that are going to benefit them [billionaires] are going to hurt you."
"It's just a thought, y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Watching the president, Beau notes his claims of a strong economy, contrary to global recession predictions.
The president dismisses economists as "fake news" and untrustworthy for not supporting his narrative.
Beau points out the president's pattern of discrediting various experts and professionals as fake news.
He criticizes blind loyalty to the president despite his track record of unfulfilled promises and benefiting the wealthy.
Beau warns against the president's manipulation and urges people to re-evaluate their support based on his actions.

Actions:

for voters, citizens,
Re-evaluate support based on actions, not promises (implied)
Analyze the president's track record and promises kept (implied)
Question blind loyalty and scrutinize policies affecting the populace (implied)
</details>
<details>
<summary>
2019-08-14: Let's talk about economics and the #TrumpRecession.... (<a href="https://youtube.com/watch?v=B-Sh6jtGuUg">watch</a> || <a href="/videos/2019/08/14/Lets_talk_about_economics_and_the_TrumpRecession">transcript &amp; editable summary</a>)

Most Americans don't understand the looming recession triggered by tariffs; prepare financially and increase demand to mitigate its effects.

</summary>

"Get prepared. Don't make any purchases."
"The tariffs opposed on the other side limited the goods produced. That's what triggered this."
"Burn through any reserves you have. Spend it."
"We need to do what we can to mitigate the destruction that his administration has caused."
"Don't cheer this on as much as you want to, as much as I want to. We've got to work to get out of it."

### AI summary (High error rate! Edit errors on video page)

Most Americans don't understand the economy, and a recession is looming.
The yield curve inverted, signaling a recession.
A recession is a decline in trade and industry activity, where GDP falls for two consecutive quarters.
The former chairman of the Fed, Greenspan, warned about yields falling below zero on bonds.
This recession is unique as it was triggered by tariffs, not the typical unemployment route.
People on Twitter are already calling this the "Trump recession."
Recommendations for lower economic classes include preparing by not making big purchases and saving money.
Those with money to burn are encouraged to increase demand for goods and services within the US.
History shows that tariffs triggered a recession leading into the Great Depression.
Mitigating the effects of the recession caused by the administration is vital to prevent widespread harm.

Actions:

for americans,
Prepare financially by avoiding big purchases and saving money (suggested)
Increase demand for goods and services within the US by spending reserves (suggested)
Work to mitigate the effects caused by the administration's policies (implied)
</details>
<details>
<summary>
2019-08-14: Let's talk about Trump, Obama, Alejandra, friends, and enemies.... (<a href="https://youtube.com/watch?v=nd7YhsJS5MI">watch</a> || <a href="/videos/2019/08/14/Lets_talk_about_Trump_Obama_Alejandra_friends_and_enemies">transcript &amp; editable summary</a>)

Beau criticizes the administration's immigration focus, deportation tactics, and treatment of deported military spouses while urging working-class solidarity and exposing harmful policies.

</summary>

"It's about framing it so you kick down. You blame those with less power than you for the mistakes or the deliberate attacks on you by those at the top."
"They find somebody to scare you. Somebody for you to kick at. Most times isn't real."
"This administration is not the friend of anybody in the working class."

### AI summary (High error rate! Edit errors on video page)

Criticizes the administration for focusing on rewriting the immigration laws instead of addressing more pressing issues.
Points out the media's role in perpetuating misleading narratives about legal immigration.
Contrasts Trump's deportation numbers with those of the Obama administration.
Notes the ineffectiveness of Trump's efforts due to his fixation on stereotypes of undocumented immigrants.
Explains how the Obama administration utilized the surveillance state for deportations.
Tells the story of Alejandra Juarez, who self-deported after facing legal challenges despite being in the US for 20 years.
Criticizes the administration's treatment of deported military spouses and its impact on combat veterans.
Addresses the demonization of immigrants receiving benefits compared to corporate welfare.
Calls out the administration for causing harm through policies like the trade war.

Actions:

for working-class americans,
Support deported military spouses' communities (exemplified)
Advocate for fair treatment of immigrants and combat stereotypes (suggested)
Educate others on the impact of harmful policies on working-class communities (implied)
</details>
<details>
<summary>
2019-08-13: Let's talk about the Endangered Species Act, Chickens, and Painters.... (<a href="https://youtube.com/watch?v=4Qb0wLYv0jo">watch</a> || <a href="/videos/2019/08/13/Lets_talk_about_the_Endangered_Species_Act_Chickens_and_Painters">transcript &amp; editable summary</a>)

The Endangered Species Act changes put a price on species, urging everyone to become conservationists to prevent irreversible environmental damage.

</summary>

"Every species will have a dollar sign attached to it."
"We have to become environmentalists or conservationists, otherwise we're all just house painters putting it over time on August 8th, 1945 in Nagasaki."
"We're not trying to stop huge issues from occurring in the future. We're trying to mitigate, trying to slow it down, because it's going to happen."

### AI summary (High error rate! Edit errors on video page)

The Endangered Species Act is being changed, causing concern among conservationists worldwide.
Oil and coal lobbyists, David Bernhardt and Andrew Willer, along with Wilbur Ross, are involved in the changes.
The current administration's key environmental positions are held by individuals with backgrounds in oil, coal, and mining.
Under the new changes, economic impact will now be a significant factor in court decisions regarding the protection of species.
Every species will essentially have a price tag attached to it under the new considerations.
Conservationists are alarmed as over a million species face extinction, now subject to economic evaluations for protection.
Multinational corporations lack loyalty to any particular country and prioritize wealth extraction over environmental protection.
The impact of these corporations on the environment will directly affect those residing in the United States and the Western world.
Beau urges his audience to not just be activists but also become conservationists to combat the environmental challenges we face.
He stresses the urgency of placing individuals in power who can make significant environmental policy changes before it's too late.

Actions:

for environmental advocates,
Advocate for strong environmental protections through local initiatives and community action (implied)
Support and volunteer for conservation organizations working to protect endangered species and habitats (implied)
</details>
<details>
<summary>
2019-08-12: Let's talk about an old atlas, colonization, and the future.... (<a href="https://youtube.com/watch?v=Nab7UB7bbGA">watch</a> || <a href="/videos/2019/08/12/Lets_talk_about_an_old_atlas_colonization_and_the_future">transcript &amp; editable summary</a>)

Beau delves into the nuances of colonization, propaganda, and Western supremacy, urging a critical reevaluation of historical narratives for a more equitable future.

</summary>

"The victor will never be asked if he's told the truth."
"Colonization never ended, it never stopped, it just changed a little bit."
"It's still about wealth extraction."
"The people of Africa, they don't want our troops. They want our business."
"We have to start seeing through it or our reluctance to look at the facts is going to cost a lot of lives."

### AI summary (High error rate! Edit errors on video page)

Talks about the Italian occupation of Ethiopia in 1935-1936 and how it was not a functioning colony.
Mentions the conflict during the Italian occupation of Ethiopia.
Questions why an old Collier's Atlas from 1940 marks Italian East Africa as if giving the fascists and Italians a win.
Points out the propaganda and biased narratives in historical accounts and atlases.
Differentiates between two types of propaganda: nationalist and the narrative of Western supremacy.
Emphasizes the importance of challenging the image of the uncivilized African perpetuated by colonial powers.
States that colonization never truly ended, just evolved into wealth extraction through different means.
Criticizes the creation of Africa Command by the Department of Defense (DOD) in 2007 and its mission centered around military intervention.
Argues that African nations desire business partnerships rather than exploitative military presence.
Warns about the consequences of not seeking the truth from current powers and victors, especially in relation to Africa.

Actions:

for history enthusiasts, activists,
Challenge biased narratives in history books and educational materials (implied)
Support African nations in building their economies through fair business practices (implied)
Advocate for transparency and accountability from current powers and victors (implied)
</details>
<details>
<summary>
2019-08-12: Let's talk about a teachable life-saving moment in Tennessee.... (<a href="https://youtube.com/watch?v=vgIziXFYK_c">watch</a> || <a href="/videos/2019/08/12/Lets_talk_about_a_teachable_life-saving_moment_in_Tennessee">transcript &amp; editable summary</a>)

Addressing a Tennessee incident involving stolen firearms, Beau stresses learning from warning signs to prevent harm and advocates for better gun safety measures in schools.

</summary>

"You have to err on the side of caution."
"If it can be defeated with this, the weapon's not secured."
"They can make the decision whether or not to send their kid to school."
"Keep the ammo on you."
"Real gun safes. Keep the ammo on you."

### AI summary (High error rate! Edit errors on video page)

Addressing an incident in Tennessee involving two 18-year-olds breaking into a school and stealing an AR-15 and vests, stressing the need to learn from it.
Emphasizing the importance of acknowledging warning signs and making changes before harm occurs.
Criticizing the downplaying of the incident by officials and the lack of notification to parents about the danger students were in.
Advocating for proper gun safes and keeping ammunition on school resource officers to prevent unauthorized access to weapons.
Challenging misconceptions about firearms, particularly around the use of AR-15s in school shootings.
Arguing for practical solutions like real gun safes, better security measures, and more training with firearms to improve safety in schools.

Actions:

for parents, educators, community members,
Keep ammunition on school resource officers for immediate access (implied)
Implement real gun safes for secure storage of weapons (implied)
Educate on firearm safety and training for effective use (implied)
</details>
<details>
<summary>
2019-08-11: Let's talk about socially conditioned responses to domestic violence.... (<a href="https://youtube.com/watch?v=JvI41qfDYXg">watch</a> || <a href="/videos/2019/08/11/Lets_talk_about_socially_conditioned_responses_to_domestic_violence">transcript &amp; editable summary</a>)

Addressing harmful social conditioning around violence and protection, Beau advocates for non-violent solutions and prioritizing the safety of individuals in abusive situations, urging them to leave and seek assistance.

</summary>

"You've been socially conditioned to use violence to solve problems."
"Get her out. That's the goal."
"Nothing. You owe him nothing. Get out."
"Everything that society has said and conditioned people to believe is the wrong thing."
"Just go."

### AI summary (High error rate! Edit errors on video page)

Urgently addressing a personal message that supersedes any news.
Exploring social conditioning and the problematic association of protection with violence, especially for men in America.
Sharing a story of a husband with substance abuse issues who beat up his wife, leading to questions of retaliation upon his release from jail.
Challenging the concept of using violence as a solution and advocating for non-violent ways to ensure protection.
Encouraging getting the wife out of the abusive situation as the primary goal.
Addressing societal conditioning that perpetuates harmful norms, such as women feeling obligated to fix their partners or stay in abusive relationships.
Emphasizing the importance of leaving an abusive situation, despite potential stigmas or economic considerations.
Providing information about shelters and organizations that can help individuals in abusive situations.
Asserting that once violence occurs, any obligation or debt to the abuser is immediately nullified.
Advocating for seeking assistance and leaving the abusive environment for safety.

Actions:

for men in america,
Find and utilize organizations and shelters that can provide assistance in leaving an abusive situation (suggested).
Prioritize getting individuals in abusive situations out to ensure their safety (implied).
Challenge societal norms and conditioning by advocating for non-violent solutions to protection (implied).
</details>
<details>
<summary>
2019-08-10: Let's talk about St. Louis, Ethiopia, Tunisia, Italy, and Open Arms (<a href="https://youtube.com/watch?v=SF_F-d7p-a4">watch</a> || <a href="/videos/2019/08/10/Lets_talk_about_St_Louis_Ethiopia_Tunisia_Italy_and_Open_Arms">transcript &amp; editable summary</a>)

Beau explains the significance of historical context in understanding current events, reflecting on the rejection of refugees reminiscent of past tragedies and proposing a compassionate solution to address the crisis.

</summary>

"Everything that happens changes what happens tomorrow."
"The scars that we're creating today, they're gonna have backlash too."
"You know, if Trump was smart, he could stop these attacks overnight."
"That relationship between the people of the area that is now Tunisia and the area that is now Italy has been around since Italy was known as Rome."
"It's estimated that about a quarter of them got caught and sent to death camps where they died."

### AI summary (High error rate! Edit errors on video page)

Explains the importance of understanding historical context to comprehend current events.
Recounts the tragic story of the St. Louis ship during WWII carrying Jewish refugees rejected by multiple countries.
Draws parallels to a modern-day situation involving the ship "Open Arms" with refugees off the Italian coast.
Notes the refusal of the Italian government and other European countries to allow the refugees ashore.
Links the desire of refugees to reach Italy to historical ties, such as Ethiopia's past as an Italian colony.
Mentions the long-standing relationship between Tunisia and Italy, despite Tunisia not being an official Italian colony.
Comments on the impact of extreme nationalism leading to the current plight of refugees.
Suggests a solution by proposing a strategy to address attacks by welcoming refugees, contrasting it with the current stance.
Warns of the lasting effects and backlash of current actions on children and citizens.
Concludes with a reflection on the repercussions of present actions influencing the future.

Actions:

for global citizens, activists,
Welcome refugees in your community (implied)
</details>
<details>
<summary>
2019-08-09: Let's talk about this week.... (<a href="https://youtube.com/watch?v=qlwmvqp0E-I">watch</a> || <a href="/videos/2019/08/09/Lets_talk_about_this_week">transcript &amp; editable summary</a>)

Beau recaps recent events, warns of escalating violence against minority groups, and urges vigilance in the face of deteriorating political and economic conditions.

</summary>

"If you were out there to own the libs, yeah, you are now forever identified as a white nationalist."
"Imagine being so in love with an idea of a symbol that you start to hate what it's supposed to represent."
"People are dying. Children are having their skulls cracked."
"As things start to deteriorate in the economy, you're going to see them start to become disillusioned and it's going to make them more violent."
"Winning comes at a cost, and it's gonna be the minority groups that's who's gonna be hit the most."

### AI summary (High error rate! Edit errors on video page)

Recaps recent events, including Pentagon surveillance balloons in Wisconsin for total information awareness.
Mentions the Walmart incident where thankfully no one got hurt.
Talks about ice raids resulting in hundreds of people disappearing, leaving kids at daycare and coming home to empty houses.
Shares a story of a man deported to Iraq where he eventually died, despite never having been there and not speaking Arabic.
Describes a disturbing incident of a child's skull being literally cracked over not removing a hat during the National Anthem.
Comments on an immigration activist caught with weapons standing by a Trump mural on his truck.
Addresses the evolution of a gesture from a joke to a white nationalist symbol.
Criticizes blind loyalty to symbols over their true meanings and the dangerous consequences.
Warns about the increasing violence and threat faced by minority groups in the current political climate.
Urges everyone, especially non-white and non-straight individuals, to be vigilant and cautious in the deteriorating situation.

Actions:

for activists, minorities, allies,
Stay vigilant and aware of your surroundings (implied)
Be cautious and careful in the current political climate (implied)
Keep your eyes open for signs of escalating violence (implied)
</details>
<details>
<summary>
2019-08-08: Let's talk about the American Farmer, Iowa, and China.... (<a href="https://youtube.com/watch?v=TskYfNBE8go">watch</a> || <a href="/videos/2019/08/08/Lets_talk_about_the_American_Farmer_Iowa_and_China">transcript &amp; editable summary</a>)

American farmers hit hard by China's halt in agricultural purchases, facing long-lasting consequences; trade war effects extend beyond economy to impact jobs and businesses.

</summary>

"They bought $19.5 billion worth. 2018, 9.2, a loss of $10 billion that was going to the American farmer."
"It's going to have a long-lasting effect because food isn't like an iPad, you know?"
"The problem with that is most farmers don't really want subsidies. They don't."
"They know what their pocketbook looks like, and I think that's going to have a bigger impact than people are saying."
"If it goes on much longer, they're looking at store closings and job eliminations."

### AI summary (High error rate! Edit errors on video page)

Explains the impact of the trade war on American farmers, particularly with China no longer buying agricultural products.
China bought $19.5 billion worth of goods from the U.S. in 2017, dropping to $9.2 billion in 2018, and now it's zero.
Points out that food is a necessity, unlike luxury items like iPads, meaning the effects will be felt more intensely.
Mentions deals between Brazil and China for soybeans, which used to make up 60% of U.S. soybean exports.
Talks about the long-lasting effects of the trade war even after it ends, as American farmers will need to compete to regain lost business.
Notes that subsidies for farmers might not be a preferred solution, as farmers want to rely on business rather than government aid.
Predicts a significant impact on Trump in the 2020 elections due to economic repercussions felt by states like Iowa, known for soybean production.
States that the trade war has resulted in $27 billion in extra costs for American taxpayers until June.
Mentions potential store closings and job losses in companies like Joanne Fabrics and the American Textile Company due to increased prices and reduced demand.

Actions:

for american citizens, farmers, policymakers,
Contact local representatives to advocate for fair trade policies that support American farmers (implied)
Support local farmers' markets and businesses to help offset the impacts of trade wars (implied)
</details>
<details>
<summary>
2019-08-08: Let's talk about reputations and Chicago.... (<a href="https://youtube.com/watch?v=VGX49N4I22w">watch</a> || <a href="/videos/2019/08/08/Lets_talk_about_reputations_and_Chicago">transcript &amp; editable summary</a>)

Beau challenges misconceptions about Chicago's crime reputation and warns against believing manipulated statistics and narratives.

</summary>

"Chicago is not Mogadishu."
"Reputations are sometimes unearned."
"Be very, very careful when you're looking at statistics and you're looking at narratives that are supposedly based on statistics."

### AI summary (High error rate! Edit errors on video page)

Shares a story about misconceptions based on reputation, using his friend's experience in Chicago as an example.
Recalls a personal experience from high school where a fight led to him gaining a reputation that wasn't entirely deserved.
Examines murder rates in various cities and states, comparing them to challenge the reputation of Chicago as a crime-ridden city.
Points out that Chicago doesn't have one of the highest murder rates when compared to other cities and states.
Mentions the role of propaganda in shaping false narratives about crime rates in Chicago, related to a former president and certain news networks.
Concludes by urging caution when interpreting statistics and narratives, especially when they are manipulated to create fear.

Actions:

for community members, skeptics,
Challenge false narratives about communities (implied)
Verify statistics and information before forming opinions (implied)
</details>
<details>
<summary>
2019-08-07: Let's talk about money in politics and San Antonio.... (<a href="https://youtube.com/watch?v=d6feSYhzvYk">watch</a> || <a href="/videos/2019/08/07/Lets_talk_about_money_in_politics_and_San_Antonio">transcript &amp; editable summary</a>)

Representative Castro's tweet on Trump donors sparks debate on transparency in political funding, with Beau advocating for open disclosure of contributions to ensure accountability and support for good causes.

</summary>

"If you believe in what you're donating to, you don't care if it becomes public, especially if they're doing good."
"The only reason you'd be worried about it becoming public is if you know it's not doing good."
"Let's see who's funding, who's paying who off."
"We all are finding it. We all are doing good."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Representative Castro from Texas tweeted out a list of people from San Antonio who gave the maximum donation to President Trump's re-election campaign, causing anger.
People are angry about the public release of donor information, questioning why supporting a cause should be a secret.
Donations to political campaigns are driven by belief in the cause or creating political favor, but why hide support if it's for a good cause?
Kevin McCarthy criticized those funding Trump's campaign for supporting divisive rhetoric and dehumanizing others.
Beau suggests that all candidates should disclose who funds them to ensure transparency.
Beau lists various organizations he supports through donations, advocating for the public disclosure of contributions to ensure accountability and transparency.
He encourages the public release of donation information to reveal who is funding political campaigns.
Beau believes that if donations are made in good faith to support beneficial causes, there should be no shame in making them public.
Transparency in political donations is vital to ensure that the funds are used for good purposes.
Beau expresses support for creating a Twitter account to disclose funding sources, promoting accountability in political campaigns.

Actions:

for political activists,
Support organizations like Pencils of Promise, Habitat for Humanity Louisiana, Planned Parenthood, ACLU, among others (exemplified).
Create a Twitter account to disclose political campaign funding sources (suggested).
</details>
<details>
<summary>
2019-08-07: Let's talk about a bizarre working theory.... (<a href="https://youtube.com/watch?v=yZkFtTQjF5A">watch</a> || <a href="/videos/2019/08/07/Lets_talk_about_a_bizarre_working_theory">transcript &amp; editable summary</a>)

Analyzing demographic info to prevent violent acts, mass shooters more like serial killers, urge to respond not react.

</summary>

"If somebody tells you they're going to do it, believe them."
"We need to respond rather than react."
"Mass shooters are more closely related to serial killers than they are terrorists."

### AI summary (High error rate! Edit errors on video page)

Analyzing demographic information to identify potential perpetrators before they commit violent acts.
Majority of mass shooters are male, white, and display concerning behaviors.
Early childhood trauma, inciting incidents, and a planning phase precede violent acts.
Characteristics of mass shooters include abusive behavior, mental health concerns, and leaked plans.
Mass shooters often have personal grievances blended with other grievances.
They have a desire for control and notoriety, similar to serial killers.
Mass shooters may be more closely related to serial killers than terrorists.
Need to respond, not react, to prevent future violent incidents.
Current strategies focusing on political violence may not be effective.
Urges a shift in perspective and approach to addressing mass shootings.

Actions:

for policy makers, law enforcement, psychologists,
Contact local authorities or FBI to report suspicious behavior (implied)
Advocate for standardized reporting methods for potential threats (implied)
</details>
<details>
<summary>
2019-08-06: Let's talk about log cabins in Vietnam, tough guys, and solutions.... (<a href="https://youtube.com/watch?v=cHV7N-2y58Q">watch</a> || <a href="/videos/2019/08/06/Lets_talk_about_log_cabins_in_Vietnam_tough_guys_and_solutions">transcript &amp; editable summary</a>)

Beau introduces Dan Glenn's story of building a cabin in his mind as a coping mechanism during captivity, stressing the importance of effective coping strategies in preventing tragedies like mass shootings through education and understanding.

</summary>

"He built a cabin. Not physically, he built it in his mind."
"The toughest people in the U.S. military are taught coping skills at one of the toughest schools in the military."
"Good ideas normally don't require force, they require just education, just a thought."

### AI summary (High error rate! Edit errors on video page)

Introduces the story of Dan Glenn, a Navy pilot shot down in Vietnam in 1966 and held in prison camps until 1973.
Glenn built a cabin in his mind while in captivity, using ashes and rice water to sketch it out, a coping mechanism that saved lives and preserved sanity.
Upon returning to the United States, Glenn physically built the cabin in Oklahoma.
The story of Glenn's coping mechanism is taught at SEER, a survival school for the US military's tough guys.
Beau explains coping skills, categorizing them into emotion-based (like imagination), problem-based (working harder, learning from mistakes), and others.
Coping strategies aim to maintain a positive outlook, prevent overreactions, and accept situations objectively.
Having a positive social support network is vital in coping effectively.
Recognizing successes, learning from mistakes, developing self-control, and maintaining oneself are part of good coping strategies.
Beau points out a pattern of early childhood trauma, lack of coping skills, and a triggering incident leading to mass shootings.
The root solution to preventing mass shootings lies in developing effective coping strategies, addressing the motive rather than the means (such as limiting access to weapons).
Beau stresses that education and understanding are key to implementing this solution effectively.

Actions:

for individuals, educators, mental health advocates,
Learn about different coping strategies and mental health practices (suggested)
Educate others on the importance of coping skills and positive social support networks (implied)
</details>
<details>
<summary>
2019-08-06: Let's talk about it happening here.... (<a href="https://youtube.com/watch?v=NovuyaBMkBc">watch</a> || <a href="/videos/2019/08/06/Lets_talk_about_it_happening_here">transcript &amp; editable summary</a>)

Beau presents a stark reality-check on the potential for violence in the country, dissecting various gun control proposals and advocating for comprehensive solutions to prevent tragedies.

</summary>

"They're forgetting it because they're angry, they're frustrated because they don't know what to do, they're afraid."
"We're chaos driven. We are a violent country. We always have been."
"It can happen here."
"Sometimes the deterrent is what stops violence."
"We don't want violence."

### AI summary (High error rate! Edit errors on video page)

Addressing the urgency of the situation, Beau foregoes his usual style of civil discourse to present facts regarding the potential for violent events in the country.
In light of recent tragedies, people are forgetting the possibility of such events occurring in their own communities as they grapple with feelings of frustration, helplessness, and fear.
Beau points out a unique turn where individuals who previously spoke out against fascism are now advocating for disarmament.
Drawing attention to the El Paso shooter's motives and the strategic thinking of those planning violent acts, Beau underscores the need to analyze proposed gun control measures critically.
Beau dissects various gun control proposals, shedding light on how they may disproportionately affect economically disadvantaged and minority communities.
He challenges the effectiveness and potential unintended consequences of suggested measures like training requirements, insurance mandates, and ammunition rationing.
Beau also touches on the legislative side, discussing the feasibility and implications of banning body armor and high-capacity magazines.
Addressing mental health checks and the nuances of implementing such measures, Beau stresses the importance of a comprehensive and effective approach to preventing violence.
Beau advocates for solutions focusing on domestic violence indicators, responsible gun storage practices, coping skills education, and improved reporting methods for identifying individuals at risk of violence.
Lastly, Beau underscores the significance of maintaining a balance between gun rights and public safety, stressing the role of a well-armed civilian population as a deterrent against potential government overreach.

Actions:

for advocates for comprehensive gun control measures,
Advocate for expanded laws addressing domestic violence indicators to prevent firearm access (suggested)
Promote responsible gun storage practices and education on coping mechanisms (suggested)
Support improved reporting methods for identifying individuals at risk of violence (suggested)
</details>
<details>
<summary>
2019-08-05: Let's talk about how we can stop the shootings.... (<a href="https://youtube.com/watch?v=Ht5ldqQdioM">watch</a> || <a href="/videos/2019/08/05/Lets_talk_about_how_we_can_stop_the_shootings">transcript &amp; editable summary</a>)

Beau addresses the cultural roots of violence, advocating for social reform, coping skills education, responsible gun ownership, and de-glorifying shooters to stem the tide.

</summary>

"Early childhood trauma. This is bullying, parental suicide, child abuse, exposure to violence at an early age."
"There's an identifiable moment where the decision was made. That was the point of no return."
"We need to let kids be kids, but that's going to require pretty wide-ranging social reform."
"Secure your firearms and don't tell your favorite nephew the code."
"Don't name them. Make them a non-person."

### AI summary (High error rate! Edit errors on video page)

Addressing solutions to help stop the plague of violence sweeping the nation.
Noting the cultural roots of the issue rather than blaming specific factors like firearms or pharmaceuticals.
Sharing findings from a study by the Violence Project funded by the National Institute of Justice.
Identifying four common factors among mass shooters that are almost universally present.
Emphasizing early childhood trauma and its long-lasting impacts.
Explaining the importance of identifying crisis points to intervene before shootings occur.
Describing how shooters often study other shooters, contributing to a socially transmitted disease.
Pointing out the prevalence of obtaining firearms from family members in mass shooting incidents.
Advocating for social reform to address underlying issues like coping skills and trauma.
Urging responsible gun ownership and securing firearms to prevent access by potential shooters.

Actions:

for community members,
Secure your firearms to prevent unauthorized access (implied)
Educate yourself and others on identifying crisis points in individuals in crisis situations (implied)
Advocate for social reform to address early childhood trauma and improve coping skills education (implied)
</details>
<details>
<summary>
2019-08-04: Let's talk about backpacks, rhetoric, and paths.... (<a href="https://youtube.com/watch?v=SxmMBfVFvX8">watch</a> || <a href="/videos/2019/08/04/Lets_talk_about_backpacks_rhetoric_and_paths">transcript &amp; editable summary</a>)

Beau questions the President's ability to sleep at night, holding him accountable for the consequences of his divisive rhetoric and urging a change to save lives.

</summary>

"How do you go to sleep at night knowing that your words killed people and they did?"
"Dead bodies in Walmart built the wall. That's your legacy."
"You can change your rhetoric and you can save lives."
"I am a small-time YouTuber and I am more conscientious about what I say, than the President of the United States."
"How does he sleep?"

### AI summary (High error rate! Edit errors on video page)

Recent days have been serendipitous for him, with news stories allowing him to make wider points.
A native woman asked about bulletproof backpacks out of fear for her child being mistaken for a Latino due to the current climate in America.
Beau advised against bulletproof backpacks, suggesting using a laptop bag with a steel plate instead.
Beau receives questions related to his background but is selective in answering due to the dual-use nature of the information.
He questions how the President can sleep at night knowing his rhetoric has led to shootings and deaths.
Beau points out the President's role in dividing rather than uniting the country.
He urges the President to change his rhetoric to save lives and prevent further violence.
Beau expresses disappointment that as a small-time YouTuber, he is more conscientious about his words than the President.
He calls on the media to press the President on how he can sleep at night given the consequences of his rhetoric.
Beau stresses the need for the President to adjust his rhetoric and work towards unifying the country, as the divide is widening and leading to deaths.

Actions:

for media, concerned citizens,
Press the President to change his rhetoric and work towards unifying the country (implied)
</details>
<details>
<summary>
2019-08-03: Let's talk about the tolerant left.... (<a href="https://youtube.com/watch?v=yQFY8iosTno">watch</a> || <a href="/videos/2019/08/03/Lets_talk_about_the_tolerant_left">transcript &amp; editable summary</a>)

Beau explains the differences between left and right-wing armed groups, touches on historical challenges, and stresses the importance of peaceful approaches over violence.

</summary>

"Ideas travel faster than bullets."
"We need to keep this peaceful as much as humanly possible."
"The idea of sparking that kind of conflict, it's never good."

### AI summary (High error rate! Edit errors on video page)

Explains the lack of liberal armed groups, attributing it to most liberals being anti-gun.
Contrasts right-wing militias, characterized by conventional tactics and structure, with leftists who don't style themselves as militias.
Mentions armed leftist groups like the John Brown Gun Club and Redneck Revolt that don't have traditional militia setups.
Talks about the historical challenges faced by armed leftists in the US, with FBI scrutiny in the past.
Describes the security consciousness of armed leftist groups, mentioning a unique vetting process involving friends.
Notes the shift in security culture and how some groups now ask more probing questions, leading to hesitation from potential members.
Points out that armed leftists train in guerrilla-style operations rather than conventional tactics.
States that firearms are seen as a tool by the left, unlike a status symbol on the right.
Emphasizes the importance of keeping things peaceful and discourages violence in any form.
Encourages responsible gun ownership for defense or recreational purposes but warns against offensive uses.

Actions:

for activists, gun owners,
Join or support organizations like the John Brown Gun Club or Redneck Revolt (exemplified)
</details>
<details>
<summary>
2019-08-02: Let's talk about Tulsi Gabbard and Foreign Policy.... (<a href="https://youtube.com/watch?v=gUxQY0xHEFY">watch</a> || <a href="/videos/2019/08/02/Lets_talk_about_Tulsi_Gabbard_and_Foreign_Policy">transcript &amp; editable summary</a>)

Candidates' backgrounds shape their positions, evident in Tulsi Gabbard's nuanced stance on Syria, challenging conventional narratives.

</summary>

"Candidates' backgrounds influence their positions."
"Gabbard's stance acknowledges the US's role in triggering the conflict in Syria and the bloodshed."
"Understanding candidates' backgrounds and past actions is key to predicting their future decisions in office."

### AI summary (High error rate! Edit errors on video page)

Candidates' backgrounds influence their positions, as seen with a candidate facing criticism over her Syria stance.
Trump's policy decisions were not surprising, reflecting his approach to running companies for short-term gains and cashing out.
Tulsi Gabbard's military background explains her nuanced stance on Syria, contrasting the dehumanization in infantry units with the empathy in medical units.
Gabbard's attention to international affairs and understanding of the prologue in Syria sets her apart.
The US involvement in Syria stemmed from a goal of destabilizing and ousting Assad, not supporting rebels or fighting extremists.
Beau suggests a personal opinion that the US involvement in Syria may have been linked to a pipeline.
Gabbard's stance acknowledges the US's role in triggering the conflict in Syria and the bloodshed.
Beau cautions against viewing Assad solely through the US narrative, noting the nuances and authoritarian context of the Middle East.
Gabbard's opposition to intervention should not be confused with support for Assad; she aims to oppose intervention while acknowledging Assad's actions.
Understanding candidates' backgrounds and past actions is key to predicting their future decisions in office.

Actions:

for voters, political analysts,
Research candidates' backgrounds to understand their positions better (implied).
</details>
<details>
<summary>
2019-08-01: Let's talk about puppets on parade and the important part of this election.... (<a href="https://youtube.com/watch?v=4Y1RmIMF_b4">watch</a> || <a href="/videos/2019/08/01/Lets_talk_about_puppets_on_parade_and_the_important_part_of_this_election">transcript &amp; editable summary</a>)

Beau stresses the necessity of moving back towards representative democracy, holding officials accountable, and avoiding traits of dictatorships, urging caution in prosecuting Trump and advocating for international accountability mechanisms.

</summary>

"We need to move back towards representative democracy and away from totalitarian regimes."
"Bad judgment cost almost half a million lives; precludes you from higher office."
"Locking up the opposing political party is dangerous."
"Reestablish ties with the International Criminal Court to hold President Trump accountable."
"Turn away from fascism by using existing mechanisms for accountability."

### AI summary (High error rate! Edit errors on video page)

Analyzing the recent debates, it wasn't surprising as most candidates' positions were anticipated based on their campaign contributors and past actions.
Some candidates like Yang, Williamson, and Tulsi stand outside the political norm, but the majority were predictable.
Beau stresses the importance of the upcoming election not just to remove Trump from office but also to steer away from totalitarian regimes and fascism.
Holding elected officials accountable and veering clear of dictator-like traits are imperative for a functioning representative democracy.
Beau is surprised by Joe Biden owning his bad judgment regarding the Iraq war vote but believes that such significant errors should disqualify politicians from higher office.
He criticizes the calls to prosecute President Trump, citing the unlikelihood of a Republican Senate convicting him post-impeachment.
Beau acknowledges the obligation of the House to impeach Trump but questions the feasibility of prosecuting him once out of office due to potential pressures and the risk of setting a dangerous precedent.
Concerned about political party persecution, Beau references the dangers of locking up the opposing political party, even if he disagrees with their policies.
He suggests reestablishing ties with the International Criminal Court to hold President Trump accountable for his actions on the border, advocating for a legal process outside the U.S. to avoid traits of dictatorships.
Beau calls for a step away from fascism and towards justice through existing legal mechanisms like the ICC.

Actions:

for voters, activists, political analysts,
Contact elected officials to demand accountability and transparency (implied)
Support initiatives to uphold democratic values and prevent authoritarian tendencies (suggested)
Advocate for international accountability mechanisms like the International Criminal Court (suggested)
</details>
<details>
<summary>
2019-08-01: Let's talk about Grizzlies, Natives, and Liz Cheney.... (<a href="https://youtube.com/watch?v=sd9GhWFfIs8">watch</a> || <a href="/videos/2019/08/01/Lets_talk_about_Grizzlies_Natives_and_Liz_Cheney">transcript &amp; editable summary</a>)

Beau questions the Western way of life narrative, defends native practices, and sheds light on the sacred significance of grizzlies to indigenous communities.

</summary>

"That powerful creature is God."
"Your buddies not being able to go on high dollar trophy hunts is not destroying your way of life."
"The American way of life is not actually extracting and ripping every single thing you can out of the earth and giving nothing back."
"We could learn a lot from the natives."
"In the immortal words of your father, Miss Cheney, go have a good day."

### AI summary (High error rate! Edit errors on video page)

Beau usually avoids talking about native issues but made an exception due to a recent win regarding grizzly bear protection.
A coalition of native groups and conservationists worked hard to protect grizzlies in the Greater Yellowstone area.
Liz Cheney criticized the protection of grizzlies, referring to it as a threat to the Western way of life.
Beau questions the concept of "Western way of life" and implies it may refer more to European colonizers than true Americans.
Beau contrasts the lack of criticism towards Trump's jokes about genocide with the protection of natives, showing a selective approach.
Grizzlies hold deep significance for native people, representing a sacred connection akin to God.
Native practices involving grizzlies, like the bear dance, hold immense historical and spiritual importance.
The use of grizzly claws in necklaces by natives is sacred and not a mere fashion statement.
Despite the spiritual importance of grizzlies to natives, they had to resort to environmental law to protect them successfully.
Beau criticizes the insensitive and dismissive attitude towards native issues and practices in the legal system.

Actions:

for advocates for indigenous rights,
Respect and support indigenous-led conservation efforts (exemplified)
Educate yourself on the cultural significance of animals like grizzlies to native communities (suggested)
Advocate for fair treatment and representation of native issues in legal and political spheres (implied)
</details>

## July
<details>
<summary>
2019-07-31: Let's talk about some moms in Chicago.... (<a href="https://youtube.com/watch?v=UQMzDd9z-pI">watch</a> || <a href="/videos/2019/07/31/Lets_talk_about_some_moms_in_Chicago">transcript &amp; editable summary</a>)

Beau talks about the tragic shooting of two moms in Chicago, urging community action and support for M.A.S.K.'s GoFundMe to gather information and combat violence.

</summary>

"Community building from the ground up. Don't wait for somebody else to make it better, you do it."
"They were moms. They lived in that community. They were just trying to make it better."
"Trying to end violence through love and more opportunities."
"Every dollar is going to help, because it loosens lips."
"Now that organization has put together GoFundMe to get a reward together for information."

### AI summary (High error rate! Edit errors on video page)

Emphasizes community networking and building as a common, everyday practice for most people.
Shares the story of two moms tragically gunned down at a bus stop in Chicago.
Criticizes the media for misrepresenting the victims as anti-gun activists when they were actually community-building moms.
Recognizes the organization the moms belonged to, M.A.S.K. (Moms/Men Against Senseless Killing), for their efforts in ending violence through love and increased opportunities.
Encourages support for the GoFundMe set up by M.A.S.K. to gather information about the shooting.
Expresses a sense of personal loss for the victims despite not knowing them personally.
Calls for community action and not relying on law enforcement to address such issues effectively.

Actions:

for community members,
Support the GoFundMe set up by M.A.S.K. to gather information about the shooting (suggested).
</details>
<details>
<summary>
2019-07-30: Let's talk about the goals of safety nets like SNAP.... (<a href="https://youtube.com/watch?v=owjQzkB9RCk">watch</a> || <a href="/videos/2019/07/30/Lets_talk_about_the_goals_of_safety_nets_like_SNAP">transcript &amp; editable summary</a>)

Beau challenges the purpose of SNAP, suggesting eliminating means tests to truly help people escape poverty and build a stronger country.

</summary>

"If the goal is to build a stronger country, expand SNAP, don't close it."
"SNAP is not just a safety net program. It's also an economic stimulus program."
"It's about controlling you."
"Expand the program. Don't shut it down."
"Make it more accessible."

### AI summary (High error rate! Edit errors on video page)

Most states ignore the asset requirement for SNAP, leading millionaires to receive food stamps.
The federal government sets the asset requirement for SNAP at $2,250, including assets like cars.
Americans are advised to save three to six months of income, but 40% can't handle a $400 emergency.
A person on SNAP who saves up to $2,250 loses assistance, making it challenging to escape poverty.
It takes 20 years under the current system to lift oneself out of poverty without setbacks.
The goal of assistance programs like SNAP should be questioned: Is it to keep people above poverty or help them escape it?
Beau suggests eliminating all means tests for SNAP to truly assist those in need.
SNAP serves not only as a safety net but also as an economic stimulus during weak economic periods.
Removing means tests and allowing easier access to SNAP could help people move above the poverty line.
The issue with SNAP isn't about budget but about control and perpetuating income inequality.

Actions:

for advocates, policymakers,
Expand SNAP to make it more accessible (suggested)
Challenge the purpose of assistance programs and advocate for change (implied)
</details>
<details>
<summary>
2019-07-29: Let's talk about snapping up cost of living differences..... (<a href="https://youtube.com/watch?v=iYpwnRdsdn8">watch</a> || <a href="/videos/2019/07/29/Lets_talk_about_snapping_up_cost_of_living_differences">transcript &amp; editable summary</a>)

Beau explains loopholes in SNAP income requirements, criticizes Trump's manipulation, and stresses the real issue of Americans living in poverty.

</summary>

"This isn't a loophole. This is Donald Trump doing what Donald Trump does."
"The issue is we have 40 million Americans living at the poverty line."
"It's $102 million for the president of the United States to go golfing before we start cutting into programs that might affect children being fed."
"He wants to be able to say, look at the number of people I got off food stamps. Look how great the economy is."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Explains the SNAP program, which provides food assistance to 40 million Americans, with about half being children.
Details the income requirements to qualify for the program, including gross and net pay thresholds.
Points out the discrepancy between the national poverty line and the cost of living across different states.
Mentions the existence of loopholes in the SNAP program due to these discrepancies.
Contrasts the basic allowance for housing in the military with the income limits for SNAP recipients.
Addresses concerns about fraud within the SNAP program, providing statistics on the actual amount of fraud.
Criticizes the focus on cutting programs like SNAP while ignoring extravagant spending in other areas.
Accuses Donald Trump of manipulating SNAP numbers for political gain.
Emphasizes that the real issue is the high number of Americans living at the poverty line.

Actions:

for advocates, policy influencers,
Advocate for accurate representation of poverty levels and support programs assisting those in need (implied).
Stay informed about political actions affecting social welfare programs (implied).
</details>
<details>
<summary>
2019-07-29: Let's talk about Earth Overshoot Day and what comes next.... (<a href="https://youtube.com/watch?v=5y1BWaZdf1U">watch</a> || <a href="/videos/2019/07/29/Lets_talk_about_Earth_Overshoot_Day_and_what_comes_next">transcript &amp; editable summary</a>)

Today is Earth Overshoot Day, consumption choices today determine whether we embrace sustainability or face authoritarian socialism tomorrow.

</summary>

"You can become green, or you can become red."
"If we don't do something about consumption, we're going to get socialism."
"It's your choice."
"We're going to get the one with guns and boots."
"Means of production, distribution, and exchange. That's critical to socialism."

### AI summary (High error rate! Edit errors on video page)

Today is Earth Overshoot Day, meaning all resources that will be regenerated by the Earth by the end of the year are already used up.
Overconsumption and mitigating climate change often lead to accusations of socialism.
Socialism is not just when the government does stuff; it's about community control of production, distribution, and exchange.
Mitigating climate change is not socialism; in fact, not addressing climate change is a more likely path to socialism in the United States.
Doomsday scenarios of flooding and disasters will lead people from affected areas to move to other states with better infrastructure, mainly red states.
The influx of people fleeing climate disasters will require significant central planning and government involvement to allocate resources.
History shows that in emergencies like nuclear threats, government control of production and distribution has been necessary.
To avoid a future with authoritarian socialism due to climate crises, becoming conservationists and changing consumption habits is imperative.
Failure to address consumption and unsustainable living will lead to an emergency situation where authoritarian socialism may become a reality.
The choice is between embracing sustainable practices or facing a future of authoritarian socialism in emergency conditions.

Actions:

for environmental advocates, policymakers,
Advocate for sustainable consumption practices within your community (implied)
Support policies that mitigate climate change and prioritize environmental conservation (implied)
</details>
<details>
<summary>
2019-07-27: Let's talk about a certain profession that can't have an opinion.... (<a href="https://youtube.com/watch?v=AGnbhHGfmkY">watch</a> || <a href="/videos/2019/07/27/Lets_talk_about_a_certain_profession_that_can_t_have_an_opinion">transcript &amp; editable summary</a>)

Beau tackles societal biases around professional relationships and advocates for valuing individuals beyond their professions.

</summary>

"It's okay to be friends with a professional killer. It's not okay to be friends with a professional lover, I guess."
"That's how violence happens."
"Just shut up and play ball, right?"

### AI summary (High error rate! Edit errors on video page)

Acknowledges viewers using videos for homeschooling.
Reads an email criticizing his association with a woman in a particular industry.
Shares the story of a woman active in street actions who he knew as an immigrant rights advocate.
Emphasizes the importance of the woman's advocacy work over her past industry.
Criticizes how people are treated differently once they voice opinions and show themselves as individuals.
Notes the issue of objectification in different industries, like sports.
Talks about being friends with military contractors and the lack of scrutiny compared to associations with other professionals.
Raises societal concerns about valuing relationships with certain professions over others.

Actions:

for activists, advocates, supporters,
Contact immigrant rights advocacy organizations to support their work (suggested)
Join local community actions supporting marginalized groups (exemplified)
Challenge objectification in industries by amplifying voices and stories of individuals (implied)
</details>
<details>
<summary>
2019-07-26: Let's talk about what Curious George can teach the US.... (<a href="https://youtube.com/watch?v=GNlai8E2vE4">watch</a> || <a href="/videos/2019/07/26/Lets_talk_about_what_Curious_George_can_teach_the_US">transcript &amp; editable summary</a>)

Beau explains the symbolism of Curious George, uncovers the true refugee story behind it, and advocates for welcoming refugees to better the country.

</summary>

"They're going to better the country if you let them."
"They're going to re-instill that spirit of being able to accomplish the impossible."
"All of those things that we pretend are still American traits."
"It's a great story."
"That's my thought."

### AI summary (High error rate! Edit errors on video page)

Explains the symbolism of Curious George on his hat and how it relates to his videos.
Recounts his history with Curious George as a mascot for a group he was part of.
Talks about his middle son's love for monkeys and how it reignited his interest in Curious George.
Acknowledges problematic themes in Curious George stories, particularly regarding a white man taking a monkey from Africa.
Discovers the true story behind Curious George, where George was actually Fifi, a monkey from South America, and his human parents were Jewish refugees escaping the Nazis in France.
Expresses admiration for how the story of Curious George/Fifi mirrors the resilience and resourcefulness of refugees.
Draws parallels between the refugee experience and the potential positive contributions refugees can make to society.
Expresses gratitude for learning the true story of Curious George/Fifi and its impact on his perspective.

Actions:

for viewers, refugee advocates,
Welcome and support refugees in your community by offering assistance and resources (implied).
Advocate for policies that facilitate the integration of refugees into society (implied).
Educate others about the valuable contributions refugees can make to society (implied).
</details>
<details>
<summary>
2019-07-26: Let's talk about how 5 minutes of your time can save 500,000 lives.... (<a href="https://youtube.com/watch?v=IpOSJR-_ElM">watch</a> || <a href="/videos/2019/07/26/Lets_talk_about_how_5_minutes_of_your_time_can_save_500_000_lives">transcript &amp; editable summary</a>)

Beau urges action to support the Connegates Amendment in the NDAA, preventing unauthorized attacks on Iran and potentially saving half a million lives.

</summary>

"A few minutes of your time can potentially save half a million lives or more."
"If Trump has to go back to Congress before launching a strike, that's going to force him to come up with a good reason."
"This is your time."
"We need to keep this amendment in there."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Urges viewers for a call to action regarding the Connegates Amendment in the NDAA, aiming to prevent unauthorized attacks on Iran.
Two versions of the Defense Department budget exist, with the House version containing the amendment but not the Senate version.
The amendment requires President Trump to seek authorization from Congress before attacking Iran, limiting executive power.
Viewers are encouraged to contact members of the Armed Services Committees, especially in the Senate, to support the amendment.
Emphasizes the importance of maintaining the amendment to prevent potential devastating consequences akin to the Iraq war.
Appeals to resistors, Republicans, anti-war advocates, and veterans to advocate for the amendment's retention.
Stresses the potential loss of lives, referencing the toll of half a million in the Iraq conflict.
Acknowledges Trump's restraint in military force but expresses concerns about his changing positions and escalating tensions.
Calls upon viewers to take action to ensure Trump is held accountable before launching any strikes on Iran.
Urges everyone to dedicate a few minutes to potentially save hundreds of thousands of lives.

Actions:

for advocates, activists, citizens,
Contact members of the Armed Services Committees, especially in the Senate, to support the Connegates Amendment (suggested).
Email, write, tweet to push for the retention of the amendment (suggested).
</details>
<details>
<summary>
2019-07-25: Let's talk about getting too woke.... (<a href="https://youtube.com/watch?v=infL_y3DxhU">watch</a> || <a href="/videos/2019/07/25/Lets_talk_about_getting_too_woke">transcript &amp; editable summary</a>)

Beau explains "getting woke," warning against despair when perceiving the establishment as unbeatable, urging continuous fight for a better world.

</summary>

"There's nothing wrong with getting woke."
"It's okay to get woke, but never get so woke that you can't dream anyway."
"We don't have to beat them today or tomorrow. We just have to keep fighting."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of "getting woke" when people see the establishment for what it truly is and start understanding the finer details of control systems, money, and politics.
Mentions how veterans often experience disillusionment after multiple tours, realizing they are guarding poppy fields for the pharmaceutical industry's profit.
Notes that the issue arises when this awareness leads to despair, especially when individuals perceive the opposition as overwhelmingly powerful.
Describes a friend who feels exhausted and stagnant in their efforts, unable to see any progress or victory.
Observes that the black community frequently faces this phenomenon, understanding the corrupt system but then feeling so woke that they lose hope and fall into depression.
Emphasizes that while recognizing the system's corruption and power is not problematic, feeling like victory is unattainable is where the real issue lies.
Encourages the idea of continuous fight and not succumbing to despair, reminding that change may take a long time but it's vital to hold onto the hope for a better world.

Actions:

for activists, advocates, community members,
Keep fighting for a better world (implied)
Maintain hope and vision for change (implied)
</details>
<details>
<summary>
2019-07-24: Let's talk about Penthouses and Taco Bells.... (<a href="https://youtube.com/watch?v=R4z9FPD8ZiM">watch</a> || <a href="/videos/2019/07/24/Lets_talk_about_Penthouses_and_Taco_Bells">transcript &amp; editable summary</a>)

Beau shares insights from attending AnarchoVegas, stressing the importance of networking and community-building based on skill sets and responsibilities.

</summary>

"I learned a whole lot, probably more than I contributed while I was there."
"If you have the means, you have the responsibility."
"Everybody who shows up to an event about building a community network, that's your network."
"Rather than it being a lecture, it's a conversation."
"You need to be attending these things, you need to be going because you can learn a lot."

### AI summary (High error rate! Edit errors on video page)

Beau missed a few days but assures everyone he is fine after attending a conference in Vegas.
He attended AnarchoVegas, a big event for him, and had a unique experience.
Beau interacted with a Taco Bell employee who recognized him from YouTube, sparking a meaningful connection.
The conference included a fundraiser for the Free Ross campaign, aiming to raise funds for Ross Ulbricht's legal defense.
The event mainly consisted of entrepreneurs striving to use their businesses for social change, alongside street activists.
Beau reflected on feeling out of place but acknowledged the value of attending such conferences to learn and network.
He realized the importance of following his own advice on getting involved based on skill set, leading him to plan community-building events in the deep south.
Beau emphasized the significance of informal gatherings for networking and community-building, aiming to start the initiative in the deep south before expanding elsewhere.

Actions:

for activists, entrepreneurs, community builders,
Start organizing informal gatherings in your community to build networks and initiate social change (suggested).
Attend conferences or events related to your skills and interests to network and learn from others (suggested).
Participate in community-building initiatives to widen your network and influence society positively (suggested).
</details>
<details>
<summary>
2019-07-24: Let's talk about Hermitage and Andrew Jackson.... (<a href="https://youtube.com/watch?v=LXC6Fbdy4DM">watch</a> || <a href="/videos/2019/07/24/Lets_talk_about_Hermitage_and_Andrew_Jackson">transcript &amp; editable summary</a>)

Beau analyzes historical echoes, warns against echo chambers in ICE and Border Patrol, and hails community action in Hermitage against ICE, foreseeing repercussions for those enforcing questionable policies.

</summary>

"The view inside an echo chamber, the view inside of an agency like that, it's not the view of the public and the talking points on TV."
"Hermitage, an impossibly diverse group of people, ages, ethnicities, languages, everything, banded together and broke the law publicly, no masks, broke the law to get that father and son away from ICE."
"That's the level of opposition that exists."

### AI summary (High error rate! Edit errors on video page)

Beau provides historical context by talking about Andrew Jackson's victory at the Battle of New Orleans, which propelled him to the national scene.
Jackson's victory happened after the War of 1812 had technically ended with the Treaty of Ghent signed, but news hadn't reached him yet.
Jackson's success at the Battle of New Orleans, though attributed to a British battle plan failure, made him a hero and later helped him become President.
Beau recalls visiting Jackson's plantation, Hermitage, as a child, where the sanitized version of plantation life was presented without addressing slavery or cruelty.
He mentions growing up in an echo chamber in Tennessee with Confederate flags prevalent, hindering meaningful discourse.
Beau draws parallels between the echo chamber of his upbringing and the potential echo chamber within ICE and Border Patrol regarding immigration issues.
Public opinion has shifted on immigration, but the battles may continue despite the war being deemed over.
Beau warns those working for ICE and Border Patrol about potential future repercussions and job prospects, contrasting their situation with that of political figures.
He mentions a diverse group in Hermitage who publicly broke the law to rescue a father and son from ICE, showcasing opposition to current policies.
Beau sees this incident as a turning point, indicating the strong level of opposition that could impact the future employment prospects of those in Border Patrol and ICE.

Actions:

for community members, activists,
Band together with neighbors to oppose unjust policies and take direct action to protect vulnerable community members (exemplified)
</details>
<details>
<summary>
2019-07-17: Let's talk about the America I remember.... (<a href="https://youtube.com/watch?v=GZm505MCvyk">watch</a> || <a href="/videos/2019/07/17/Lets_talk_about_the_America_I_remember">transcript &amp; editable summary</a>)

Exploring generational perceptions of America through history education: sanitized textbooks versus raw, unfiltered truths, questioning the future based on embracing true history or idealized versions.

</summary>

"They didn't learn history, not the way we did. They didn't get it from a thick government-approved textbook, a sanitized version."
"We can actually have the American dream because they got the truth. We know where we're going wrong."
"The future of this country rests in those kids and what they decide to do, those kids, they're not all kids, be younger generations."
"America will stand or fall based on the actions of these generations that a lot of us are making fun of."
"They're not going to sell out their country for a red hat and a stupid slogan."

### AI summary (High error rate! Edit errors on video page)

Exploring the generational gap in perceptions of America, with older generations remembering a sanitized version learned from textbooks and younger generations exposed to raw, unfiltered truths from the internet.
Older generations learned history through government-approved textbooks, while younger generations have freer access to information and primary sources.
The shock of discovering America's darker history, like mistreatment of natives and slaves, challenges the traditional narrative of America as the "good guy."
The younger generation still values the ideals of America, such as equality and freedom, but questions whether the future will embrace the raw, unfiltered truth or a sanitized version of history.
Beau contrasts the desire for America to be an EMT (Emergency Medical Technician) globally instead of a police force, helping people achieve freedom genuinely.
Acknowledging the transformative impact of the internet on how history is learned and understood, Beau recognizes the power held by younger generations in shaping America's future.

Actions:

for educators, activists, students,
Advocate for inclusive and accurate history education in schools (implied)
Support initiatives that provide access to primary sources and unfiltered information about historical events (implied)
Encourage critical thinking and questioning of traditional narratives about America's history (implied)
</details>
<details>
<summary>
2019-07-17: Let's talk about Asuistan again.... (<a href="https://youtube.com/watch?v=DX9hYvpMBR8">watch</a> || <a href="/videos/2019/07/17/Lets_talk_about_Asuistan_again">transcript &amp; editable summary</a>)

Beau addresses overlooked issues within the U.S., revealing alarming parallels with dictatorships and urging a critical reevaluation of media coverage.

</summary>

"Assuistan is not a real country."
"We need to look at it again."
"In large part because of mass media."
"It's just a thought, y'all."
"The targeting of child soldiers, refusing international inspectors, trying to disrupt investigations into our own war crimes..."

### AI summary (High error rate! Edit errors on video page)

Addressing a suicide and discussing overlooked issues within the country.
The regime in the country is denying international inspectors entrance and visas to ICC staff.
Threatening judges to dissuade investigations into war crimes in a recent conflict.
Military recruitment numbers have dropped due to lack of faith in the regime, leading to targeting children as young as 16 for recruitment.
Children involved in hostilities must be 18 or older according to international law.
Legislation in some provinces prevents military recruiters from entering campuses, prompting a digital campaign to target children.
Clashes between protesters, rebels, and security services across the country due to security sweeps targeting a social minority group.
Horrendous conditions and widespread sexual abuse of the targeted group leading religious institutions to shelter them.
Centrist party within the government asking the regime's leader, Don Joe Trump, to step down, which is unlikely.
Beau uses "Assuistan" to demonstrate how media coverage changes when events in the U.S. are portrayed as happening in another nation.
The U.S. faces issues like targeting child soldiers, refusing international inspectors, disrupting war crimes investigations, and running concentration camps.

Actions:

for policy makers, activists, citizens,
Contact religious institutions for information on how to support and help shelter the targeted group (implied).
</details>
<details>
<summary>
2019-07-16: Let's talk about citizens and persons.... (<a href="https://youtube.com/watch?v=e8w23X9AL74">watch</a> || <a href="/videos/2019/07/16/Lets_talk_about_citizens_and_persons">transcript &amp; editable summary</a>)

Beau engages internet individuals on constitutional rights, revealing misconceptions and contradictions, urging critical reflection on the Constitution's importance.

</summary>

"You can't do both. So what rights are guaranteed to persons? All the ones that matter."
"So when you get to this point and you've explained all of this to them, you can then ask, you know, why do you hate the Constitution?"
"Every constitutional protection that matters as far as getting locked up for no good reason, being detained without any kind of due process, being denied life, liberty, property, all of these things, they all apply to persons, not citizens."

### AI summary (High error rate! Edit errors on video page)

Beau engaged with internet individuals who believe undocumented workers have no constitutional rights.
Beau found it enlightening to interact with these individuals who had misconceptions about constitutional rights.
He explained that the Constitution guarantees rights to both citizens and persons.
Beau's goal was to make these individuals realize that undocumented workers have rights protected by the Constitution.
Beau pointed out that supporting the Constitution and supporting certain policies may be contradictory.
He listed the rights guaranteed to persons under the Constitution, including the right to assemble peaceably and protection against unreasonable searches and seizures.
Beau emphasized the importance of trials and equal protection under the law for all persons.
He mentioned historical examples to illustrate how constitutional rights apply even in cases of terrorism.
Beau raised the question of why some individuals seem to disregard the Constitution when discussing certain policies.
He encouraged engaging with those who misinterpret or ignore constitutional rights, aiming to provoke critical thinking and questioning.

Actions:

for online activists,
Challenge misconceptions about constitutional rights (suggested)
Encourage critical engagement with individuals who misunderstand constitutional protections (suggested)
Spark debates on the significance of constitutional rights in policy debates (suggested)
</details>
<details>
<summary>
2019-07-15: Let's talk about what went wrong in the Fullerton incident.... (<a href="https://youtube.com/watch?v=U3lCH_vLgos">watch</a> || <a href="/videos/2019/07/15/Lets_talk_about_what_went_wrong_in_the_Fullerton_incident">transcript &amp; editable summary</a>)

Beau explains the Fullerton incident, defending the cop's actions within normal standards, and suggests potential policy debates.

</summary>

"There's not. nothing that this cop did that any reasonable person, even extremely experienced, would do differently."
"This wasn't a kid at a playground where one could reasonably expect it to be a toy."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Beau is asked to dissect the Fullerton incident and explain what the cop did wrong.
He expresses strong criticism towards police departments and their actions.
Beau mentions his history of criticism towards the Fullerton PD.
The cop in the Fullerton incident observed a woman with a firearm.
Despite the firearm turning out to be fake, the officer had to make a split-second decision.
The officer engaged with the woman without using excessive force.
He called for medics before securing the scene or clearing the car, which is outside of standards.
The officer then proceeded to render aid to the woman.
Beau points out that the officer's actions were all within normal standards and best practices.
He suggests that there could be a debate around implementing a "don't fire until fired upon" policy.

Actions:

for policing critics,
Debate and advocate for policies like "don't fire until fired upon" (implied)
</details>
<details>
<summary>
2019-07-15: Let's talk about solutions to the conditions.... (<a href="https://youtube.com/watch?v=1Q0IJoOzm_4">watch</a> || <a href="/videos/2019/07/15/Lets_talk_about_solutions_to_the_conditions">transcript &amp; editable summary</a>)

Beau suggests housing asylum seekers in a plantation house, offering education and opportunities, criticizing government treatment, and advocating for decentralized solutions, believing individuals can do better than ICE.

</summary>

"Modern problems require modern solutions."
"Turning a problem into an asset, fantastic."
"I think we can do it better than ICE if we let the average individual do it."

### AI summary (High error rate! Edit errors on video page)

Proposes a solution to current issues, including hygiene, overcrowding, and food problems, and the situation at concentration camps.
Shares a dream of buying an old plantation house and his plan to house asylum-seeking mothers and their infants.
Calculates the costs and potential profit by housing these individuals compared to what camps are paid.
Plans to convert the unoccupied sixth bedroom into a school to teach English.
Mentions a friend who wants to take at-risk teens to his farm to prepare them for the Marines.
Suggests that farmers could provide housing for these teens and potentially offer them jobs.
Advocates for decentralization as a modern solution and criticizes the government for treating people inhumanely.
Finds poetic justice in using a home built on slavery to help others achieve freedom.
Believes that individuals can handle the situation better than government agencies like ICE.

Actions:

for advocates and activists,
Provide housing, education, and opportunities for asylum seekers and at-risk teens (implied)
Advocate for decentralized solutions to address humanitarian issues (implied)
</details>
<details>
<summary>
2019-07-14: Let's talk about the most important Presidential campaign promise.... (<a href="https://youtube.com/watch?v=Y7yv5_xSmHU">watch</a> || <a href="/videos/2019/07/14/Lets_talk_about_the_most_important_Presidential_campaign_promise">transcript &amp; editable summary</a>)

Beau insists on the importance of upholding the rule of law, particularly in the treatment of immigrants, urging all presidential candidates to endorse this fundamental principle.

</summary>

"If it's a good idea, it's a good idea."
"We cannot allow a tradition of just following orders to take root in this country."
"That's law. That's the rule of law."
"If we're going to have a nation, it's got to have the rule of law, right?"
"This is an important promise and it needs to be kept by whoever occupies the Oval Office next."

### AI summary (High error rate! Edit errors on video page)

Beau supports ideas over politicians, believing good ideas stand on their own merits regardless of who presents them.
He heard a powerful idea recently that he believes every presidential candidate should endorse, regarding the restoration of the rule of law in the treatment of immigrants.
Beau criticizes the abuse, physical and sexual, of immigrants and the lack of medical care provided to them, calling for accountability and justice.
He specifically mentions President Elizabeth Warren and her commitment to addressing crimes against immigrants on her first day in office.
Beau expresses disappointment that such a promise is even necessary from a presidential candidate, indicating a concerning lack of commitment to upholding the rule of law.
He references a clip where a former ICE director argues with AOC about asylum seekers, showcasing differing interpretations of the law.
Beau advocates for upholding the rule of law, citing specific legal provisions that protect asylum seekers and criticizing any attempts to undermine these laws.
He stresses the importance of maintaining the rule of law for the functioning of a nation and calls for accountability from those in power.

Actions:

for voters, immigration advocates,
Support organizations advocating for immigrant rights (implied)
Stay informed about immigration laws and rights (implied)
Advocate for accountability in the treatment of immigrants (implied)
</details>
<details>
<summary>
2019-07-13: Let's talk about the stories we're writing.... (<a href="https://youtube.com/watch?v=rtkwPBx-qcE">watch</a> || <a href="/videos/2019/07/13/Lets_talk_about_the_stories_we_re_writing">transcript &amp; editable summary</a>)

A country is shaped by its stories, with current narratives reflecting a potential dark path towards acceptance of oppressive actions.

</summary>

"A country, a lot like a person, is just stories."
"That's the character of the nation."
"The stories that make up the United States' character are being written right now."
"We're headed down a pretty dark road."
"What's happening there is part of America's history now."

### AI summary (High error rate! Edit errors on video page)

A country is a collection of stories, shaping its character and reputation.
The stories written today define the nation's character.
Recent internet debates suggest a fear of speaking out against a fascist government.
People are starting to accept oppressive actions instead of questioning them.
Internet exchanges are becoming part of America's story, potentially showcased in future museums.
The focus on political pundits and debates overshadows experts' warnings about the dark path ahead.
The Smithsonian aims to preserve drawings by children from border camps as part of America's history.
The framing of these events in history will shape future perspectives on the country's actions.

Actions:

for americans,
Preserve and share stories and narratives that challenge oppressive actions in society (implied).
Support initiatives like the Smithsonian's efforts to document and frame critical events in history (exemplified).
</details>
<details>
<summary>
2019-07-12: Let's talk about money and expansion.... (<a href="https://youtube.com/watch?v=O_igsJyR1D4">watch</a> || <a href="/videos/2019/07/12/Lets_talk_about_money_and_expansion">transcript &amp; editable summary</a>)

Beau introduces a new video format, explains Patreon launch, and shares plans to expand into other media forms, seeking support for future projects and community initiatives.

</summary>

"Beyond that, I'd like to branch out into other types of media, the podcast everybody's asking for, I'd like to write a book too."
"It's tentative because we have no idea how this is going to play out."
"The rest is up to you guys."

### AI summary (High error rate! Edit errors on video page)

Introducing a unique video format unlike any done before.
Explaining the reasons for not asking viewers to like, subscribe, or support Patreon in every video.
Announcing the launch of a Patreon account due to viewer requests.
Describing Patreon as a platform for sponsorships starting at a dollar per month.
Detailing the perks of sponsoring, including access to a Discord server, voting on future projects, and early video releases.
Ensuring that no content will be kept behind a paywall.
Mentioning plans to use the Patreon funds to free up time from battling algorithms on YouTube and Facebook.
Expressing a desire to expand into other media forms like podcasts and writing a book.
Stating the intention to use the funds to pay people who might assist in these new ventures.
Sharing a vision of potentially taking the show on the road to actively help build community networks and address social issues.

Actions:

for viewers,
Support Beau's Patreon account to help him free up time from battling algorithms on social media and to assist in expanding into other media forms (suggested).
Engage with the community by participating in live streams and supporting efforts to address social issues (implied).
</details>
<details>
<summary>
2019-07-11: Let's talk about the government's relationship with the environment.... (<a href="https://youtube.com/watch?v=wMpHPykNENI">watch</a> || <a href="/videos/2019/07/11/Lets_talk_about_the_government_s_relationship_with_the_environment">transcript &amp; editable summary</a>)

French intelligence bombed the Rainbow Warrior to silence environmental advocacy; Today, U.S. environmental regulations are rolled back while climate change information is suppressed for corporate interests.

</summary>

"Government's not here to protect you."
"We don't colonize anymore with flags and guns, we do it with corporate logos."
"But at the end of the day, it's happening."
"The environment as a whole doesn't have any money but the oil and coal industry do."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Recalls the bombing of the Rainbow Warrior in 1985 by French intelligence due to its mission to draw attention to a French nuclear test, resulting in a death and French government conviction.
Compares the French government's suppression of environmental advocacy to current actions by the U.S. government.
Notes the U.S. President's contradictory claim of being a land protector while overseeing significant rollbacks of environmental regulations.
Points out the administration's extensive list of environmental regulation rollbacks, including opening the U.S. coastline to offshore drilling and scrapping safety regulations.
Mentions the removal of references to climate change by federal agencies, drawing parallels to the French bombing incident to prevent public discourse on climate change.
Emphasizes that while debates exist on the origins of climate change, its occurrence is undeniable and poses a national security threat.
Argues that governments prioritize control and power over protecting citizens, using malice to suppress climate change information.
Compares historical military power motives for actions like the boat bombing to present-day colonization through corporate influence in environmental matters.
Points out the presence of former industry lobbyists during the President's press conference, indicating a politicization of environmental issues driven by industry interests.
Concludes with a reflection on the financial influence of industries like oil and coal in environmental policymaking.

Actions:

for environmental activists,
Organize local environmental protection events (implied)
Support organizations advocating for environmental conservation (implied)
</details>
<details>
<summary>
2019-07-11: Let's talk about social media's impacts on political campaigns.... (<a href="https://youtube.com/watch?v=1zZ5ETq2Vj4">watch</a> || <a href="/videos/2019/07/11/Lets_talk_about_social_media_s_impacts_on_political_campaigns">transcript &amp; editable summary</a>)

Social media is dismantling political elitism by exposing inauthenticity and enabling non-establishment candidates like AOC, paving the way for a new era in politics.

</summary>

"We're going to see more Justin Armash, AOC, candidates like this that are on the edges of the establishment."
"She appears to be gearing up to run the exact opposite style of campaign, where she's hoping to appeal to the better instincts and the better nature of humanity."
"It's a bold strategy. She seems, by tweeting that without any kind of explanation, she seems to be hoping and targeting the more educated Americans."
"It is authentic. That is who she appears to be."
"We're going to see them for the establishment. We're going to see them for those who are really there just to enrich themselves with the money they take from us."

### AI summary (High error rate! Edit errors on video page)

Social media may end up dismantling the political elite in the country by exposing inauthenticity and enabling non-establishment candidates to succeed.
In the past, candidates could easily stage authenticity for the public, but social media now reveals the staged nature of these events.
The rise of social media is already impacting politics, with the emergence of non-establishment candidates who win elections.
People like AOC are examples of candidates on the fringes of the establishment gaining traction through social media.
Social media allows for greater scrutiny of politicians' actions, making it harder for them to spin narratives like before.
Marianne Williamson's campaign is described as anti-Trump, appealing to compassion and love rather than fear and bigotry.
Williamson's strategy contrasts sharply with Trump's, focusing on appealing to the better instincts of humanity.
Social media enables candidates like Williamson to target more educated Americans and run authentic campaigns.
Candidates who are not part of the traditional power structures are finding a voice through social media platforms.
The future of politics might involve more non-establishment figures being able to challenge the professional political class.

Actions:

for political activists, social media users,
Support non-establishment candidates in elections (implied)
Engage in social media platforms to amplify authentic voices and challenge political elitism (implied)
</details>
<details>
<summary>
2019-07-10: Let's talk about helping homeless vets.... (<a href="https://youtube.com/watch?v=K7gFW0nfF_I">watch</a> || <a href="/videos/2019/07/10/Lets_talk_about_helping_homeless_vets">transcript &amp; editable summary</a>)

Beau challenges the false dilemma of helping migrants or homeless vets, proposing existing solutions for veteran homelessness while critiquing misplaced priorities.

</summary>

"We can do more than one thing at a time."
"The money's always been there for this to be done."
"You want an excuse to not help other people, and they're using homeless vets to do that."
"If you want a solution, there it is. Turn these camps into facilities to house homeless vets. Done."
"The question is why do you want to lock up toddlers instead of helping homeless vets?"

### AI summary (High error rate! Edit errors on video page)

United States is resilient and populated by resourceful people.
Americans can multitask and handle multiple challenges simultaneously.
Challenges like fighting wars and sending a man to the moon were tackled simultaneously in the past.
The issue at hand is homelessness among veterans.
On average, 40,056 homeless veterans spend their nights on the streets.
To address this problem, facilities offering food, beds, showers, hygiene items, psychological support, education, and recreation are needed.
In 2018, there were over 180 facilities with more than enough beds to house 40,520 people.
Beau proposes moving criminal aliens to county jails and releasing non-criminal migrants for court dates, in line with laws and international norms.
Retraining guards in existing facilities for job reentry programs could solve the problem.
Beau believes the money needed for this solution has always been available.
Many people use homeless vets as an excuse to avoid helping others.
Combat veterans, according to Beau, are compassionate and willing to help others in need.
Beau questions the deportation of veterans and the lack of care for homeless vets.
He suggests repurposing existing camps into facilities for homeless vets as a viable solution.
Beau challenges the idea that helping migrants is prioritized over homeless vets, pointing out the available resources for veterans.

Actions:

for advocates for homeless veterans,
Repurpose existing camps into facilities for homeless veterans (suggested)
</details>
<details>
<summary>
2019-07-09: Let's talk about being able to leave anytime you want.... (<a href="https://youtube.com/watch?v=uHfFBmM7MwY">watch</a> || <a href="/videos/2019/07/09/Lets_talk_about_being_able_to_leave_anytime_you_want">transcript &amp; editable summary</a>)

Sharing stories to put views in perspective, asylum seekers facing dehumanization, a call to learn from mistakes.

</summary>

"We like to remain blissfully ignorant of the situation of the world, and we do this as a defense mechanism."
"They're people. And because we do this, we can say cute little things like, oh well, they just should have followed the law."
"If we learn from our mistakes, the next time you hear a story about somebody collaborating with one of the worst regimes of the 20th century, you won't think it's about you."

### AI summary (High error rate! Edit errors on video page)

Sharing stories from others' lives can help put your views and life into perspective.
A family goes into hiding on July 9th out of fear of being rounded up after their asylum request is denied in the United States.
The inevitable end of the family's story is being given up, rounded up, and separated in camps.
Drawing a parallel to Anne Frank, who went into hiding on July 9th and later died in a camp.
The comparison between people who turn in asylum seekers and those who remained silent during historical atrocities.
Americans tend to remain ignorant of global situations to avoid facing culpability for actions done in their name.
Refugees come to the U.S. due to interventions in their countries, seeking asylum, facing dehumanization and propaganda.
The reality of conditions in camps, including audio tapes of children crying for their parents.
The argument that refugees staying despite horrific conditions at home shows asylum is necessary.
Calling for learning from mistakes and not repeating history by dehumanizing asylum seekers.

Actions:

for advocates for human rights.,
Contact local refugee support organizations to offer assistance and support (suggested).
Educate yourself on asylum processes and advocate for humane treatment of refugees (implied).
Volunteer at or donate to organizations working with asylum seekers (suggested).
</details>
<details>
<summary>
2019-07-09: Let's talk about adventure, waterboarding, and licking ice cream.... (<a href="https://youtube.com/watch?v=8oWLQsfSgrY">watch</a> || <a href="/videos/2019/07/09/Lets_talk_about_adventure_waterboarding_and_licking_ice_cream">transcript &amp; editable summary</a>)

Beau believes embracing adventures in activism can shape history and encourages individuals to choose their level of involvement to create meaningful change.

</summary>

"We're Coming into a period in American history that's going to require a lot of adventures."
"You can change the world, or not."
"It could be anything from donating to lawyers, to calling your senator, to marching, to organizing a protest, to anything, leafletting, all kinds of stuff."
"If you let other people do that for you, you're going to end up over your head."
"Take that adventure in service of something greater than yourself."

### AI summary (High error rate! Edit errors on video page)

Recalls a past demonstration where he was waterboarded by another person as part of an activist event following the US torture report release.
Describes how a young man volunteered to be waterboarded during the demonstration despite Beau's skepticism.
Talks about the importance of adventures in shaping history, especially during times of social upheaval.
Mentions the need for individuals to choose their own level of involvement in activism.
Encourages the youth to embrace adventure in service of a greater cause rather than engaging in superficial acts like licking ice cream.
Emphasizes that impactful change often comes from individuals taking risks and going on adventures.
Urges people to decide what they are willing to do in terms of activism and not let others dictate their level of involvement.

Actions:

for youth activists,
Reach out to the youth and encourage them to take meaningful actions (implied)
Decide your own level of involvement in activism (implied)
Embrace adventures in service of a greater cause (implied)
</details>
<details>
<summary>
2019-07-08: Let's talk about partisanship and Epstein.... (<a href="https://youtube.com/watch?v=hMIZt6DGRyY">watch</a> || <a href="/videos/2019/07/08/Lets_talk_about_partisanship_and_Epstein">transcript &amp; editable summary</a>)

Beau delves into the dangers of partisanship, urging individuals to prioritize morals over political allegiance and speak out against injustice.

</summary>

"I don't fault the second person in a gang rape. If it's wrong, it's wrong."
"If you ever reach the point where you can up play or down play child abuse of that type for political ends, that should be a red flag."
"We might want to remember that these types of things occur because we silently consent to it."
"These things happen because we allow it."
"You have lost yourself in support of an elephant or a donkey."

### AI summary (High error rate! Edit errors on video page)

Exploring the dangers of partisanship and blind loyalty to a political party.
Mentioning recent headlines showcasing partisanship in the media.
Speculating on the potential involvement of influential figures in criminal activities.
Addressing the issue of overlooking wrongdoing based on party affiliation.
Drawing parallels between excuses made based on past actions of different political figures.
Criticizing the hypocrisy in condemning certain actions while supporting similar ones.
Expressing disbelief at the lack of outrage over serious allegations in certain situations.
Warning about the dangerous consequences of prioritizing political allegiance over morality.
Urging people to speak out against injustice and not remain silent.
Concluding with a call for reflection on complicity in enabling harmful actions.

Actions:

for active citizens,
Speak out against injustice and wrongdoing, regardless of political affiliation (implied)
Raise your voice against harmful actions and policies in your community (implied)
Refuse to overlook immoral behavior based on political party loyalty (implied)
</details>
<details>
<summary>
2019-07-08: Let's talk about Marianne Williamson, Pelosi, AOC, and the future.... (<a href="https://youtube.com/watch?v=MpIwE7KwQYU">watch</a> || <a href="/videos/2019/07/08/Lets_talk_about_Marianne_Williamson_Pelosi_AOC_and_the_future">transcript &amp; editable summary</a>)

Beau examines societal norms, challenges conventional thinking, and calls for accountability amidst generational shifts in politics and values.

</summary>

"Humanity needs a mental shower."
"We need to wash off the prejudices of the 20th century."
"You can get with the program or you can go home."
"The question is why are you there still?"
"They're not going to sit on their hands."

### AI summary (High error rate! Edit errors on video page)

Examines the lessons from three women and their impact on the country's direction.
Advocates for shedding 20th-century prejudices and embracing new perspectives.
Criticizes the dismissal of Marianne Williamson as a viable candidate due to unconventional views.
Challenges the hypocrisy of ridiculing Williamson's beliefs while accepting traditional forms of prayer from other candidates.
Questions the notion of presidential behavior, contrasting showering with love versus bombing countries.
Disagrees with Williamson on the importance of unraveling national secrets to understand sickness.
Comments on the marketability of values like peace, love, and hope in elections.
Addresses the pushback against younger Congress members like AOC by established figures like Nancy Pelosi.
Considers the inevitability of more Millennials entering Congress and the impact of a growing younger voting population.
Criticizes the lack of action on pressing issues such as border camps, incarceration rates, income inequality, drug war, and healthcare.
Urges incumbents to question their own presence in Congress as younger generations demand change.
Emphasizes the rising influence of a generation seeking tangible progress and holding leaders accountable.

Actions:

for voters, activists, politicians,
Join or support political campaigns that advocate for progressive change (exemplified).
Get involved in local community organizing to address pressing social issues (exemplified).
Engage in political education and encourage participation in elections, especially among younger generations (exemplified).
</details>
<details>
<summary>
2019-07-07: Let's talk about coming from a good family.... (<a href="https://youtube.com/watch?v=5pwslfaBVjs">watch</a> || <a href="/videos/2019/07/07/Lets_talk_about_coming_from_a_good_family">transcript &amp; editable summary</a>)

Beau blends journalism styles to uncover truth, challenges victim-blaming narratives in a sexual assault case, reminding us to believe victims even without video evidence.

</summary>

"Everything is alleged until there's a conviction."
"In most cases, the accused does not text a video."
"Without that video, never would have gone anywhere."

### AI summary (High error rate! Edit errors on video page)

Introduction to gonzo journalism blending facts, statistics, fictions, opinions, stories, and historical tidbits to uncover truth.
Emphasizes that everything is alleged until there's a conviction, video evidence doesn't change this fact.
Content warning issued about discussing a sexual assault case in New Jersey involving a 16-year-old girl.
The judge in the case, Judge James Troiano, didn't want it to leave family court for adult criminal court.
Details of the alleged assault at a pajama party where the girl got drunk and raped.
Beau questions the typical victim-blaming response that the girl might face if not for the evidence.
The prosecution believes the accused texted a video of the act with a condemning caption.
Despite the evidence, the judge hesitates to label it as rape due to the accused's background and achievements.
Class distinctions and the privilege of the accused are brought into focus as factors influencing the case.
Beau stresses that without the video evidence, the case might not have been pursued.
Reminds the audience that most cases don't have a video, urging reflection on believing victims without such evidence.

Actions:

for journalists, advocates, allies,
Support survivors of sexual assault (implied)
Believe victims even without video evidence (implied)
Advocate for fair treatment in the justice system (implied)
</details>
<details>
<summary>
2019-07-06: Let's talk about Earthquakes, Hurricanes, and helping yourself.... (<a href="https://youtube.com/watch?v=dkJCy2r0Uaw">watch</a> || <a href="/videos/2019/07/06/Lets_talk_about_Earthquakes_Hurricanes_and_helping_yourself">transcript &amp; editable summary</a>)

Be prepared for emergencies with water, food, shelter, tools, and communication; your readiness can help others in your community survive.

</summary>

"Help doesn't come immediately. It takes time."
"You don't have to eat well, you just have to eat."
"This is all you need to stay OK until help arrives."
"Your children are going to cause you more stress than the actual natural disaster."
"Just take care of this stuff and you're going to be able to help people."

### AI summary (High error rate! Edit errors on video page)

Urges viewers to prepare for emergencies, despite it not being the main focus of his channel.
Emphasizes the importance of self-preparedness due to delays in external help during natural disasters.
Advocates for two gallons of water per person per day and the necessity of water for various activities during a disaster.
Recommends storing water in gallon jugs or five-gallon containers.
Stresses the significance of having enough canned goods to sustain daily calorie intake during a disaster.
Advises having multiple methods for starting a fire and ensuring one has shelter materials like tarps or mylar tents.
Urges viewers to assemble a first aid kit and stock up on prescription medications.
Encourages the inclusion of a knife or multi-tool in the emergency supplies.
Suggests maintaining hygiene products in the emergency kit for comfort.
Recommends having a battery-operated radio for communication during emergencies.

Actions:

for community members,
Stock up on two gallons of water per person per day (suggested).
Purchase canned goods to ensure a daily intake of calories (suggested).
Assemble a first aid kit with necessary medications (suggested).
Include a knife or multi-tool in your emergency supplies (suggested).
Buy hygiene products for comfort during emergencies (suggested).
</details>
<details>
<summary>
2019-07-05: Let's talk about what we can learn from a black Little Mermaid.... (<a href="https://youtube.com/watch?v=VlSfah57lbQ">watch</a> || <a href="/videos/2019/07/05/Lets_talk_about_what_we_can_learn_from_a_black_Little_Mermaid">transcript &amp; editable summary</a>)

People upset over a Black Little Mermaid fail to see the positive theme of breaking down racial divisions present in the original story, missing an opportunity to challenge harmful narratives.

</summary>

"We could probably learn a lot from A Black Little Mermaid."
"The one positive theme in it is giving up these illusions that these things make us different, that these things matter."

### AI summary (High error rate! Edit errors on video page)

Disney is facing backlash for deciding to make the Little Mermaid black, with people upset because the story dates back to 1837.
The original story from 1837 involves a mermaid saving a prince, losing her voice and identity, and ultimately dissolving into sea foam after failing to win his heart.
The themes in the original story include giving up one's voice and identity to fit in and find love, perpetuating harmful ideas about women's worth and the importance of conforming.
Beau criticizes those who oppose a Black Little Mermaid for not recognizing the positive theme of letting go of racial divisions present in the original story.
He suggests that embracing a Black Little Mermaid could teach valuable lessons about breaking down racial barriers and challenging traditional narratives.

Actions:

for storytellers, activists, educators,
Challenge traditional narratives and stereotypes (implied)
Promote diversity and representation in storytelling (implied)
</details>
<details>
<summary>
2019-07-04: Hillbilly tries to 'Merica with gas and fireworks, blows up instead.... (<a href="https://youtube.com/watch?v=_VwfYeEhbvo">watch</a> || <a href="/videos/2019/07/04/Hillbilly_tries_to_Merica_with_gas_and_fireworks_blows_up_instead">transcript &amp; editable summary</a>)

Beau questions the celebration of America on the 4th of July in light of government treatment of individuals in detention, lack of investigation into sexual abuse complaints, and the pursuit of happiness being overlooked.

</summary>

"What are you celebrating? Freedom? No. No, don't think so."
"I didn't see anything about, you know, lines in the sand prohibiting that."
"Y'all have a happy Fourth of July. It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Beau plans to light a giant box of fireworks soaked in gasoline on the 4th of July.
He questions the celebration of America while mentioning the government's treatment of people in concentration camps.
Beau criticizes the lack of basic necessities like soap, toothpaste, and food for those in detention.
He points out the indifference towards sexual abuse complaints in certain locations.
Questioning the celebration of freedom when such injustices occur.
Beau mentions the guarantee of the pursuit of happiness in the document but questions its application.
Ending with a thought-provoking message about the 4th of July celebrations.

Actions:

for activists, americans,
Question the celebration of national holidays (implied)
Advocate for the rights and well-being of individuals in detention (implied)
Raise awareness about overlooked injustices (implied)
</details>
<details>
<summary>
2019-07-03: Let's talk about why they can't fix their country.... (<a href="https://youtube.com/watch?v=mhlAEqf39Sg">watch</a> || <a href="/videos/2019/07/03/Lets_talk_about_why_they_can_t_fix_their_country">transcript &amp; editable summary</a>)

Beau explains how US interventions have fueled instability in the Northern Triangle, urging accountability and a shift in foreign policy to stop perpetuating harm in Central and South America.

</summary>

"We don't need to put better locks on our houses of the United States because other people are breaking in, we just need to stop setting other people's houses on fire."
"It isn't that they can't fix their country. They did it multiple times. It got overthrown because you can't fix your country."
"We can't constantly allow our government to do this and then cry when people try to get away from the situations that we created."

### AI summary (High error rate! Edit errors on video page)

Explains the history of the Northern Triangle countries (El Salvador, Honduras, Guatemala) and their current situations.
Details how the US intervention, support, and influence have significantly contributed to the turmoil in these countries.
Mentions the role of organizations like MS-13 and Barrio-18, originally from the US but transplanted to El Salvador, in causing violence and instability.
Describes the impact of US interventions in Honduras, particularly in controlling the economy and military.
Talks about the United Fruit Company's control over Guatemala and the subsequent interventions that disrupted the country's attempts at self-governance.
Criticizes US actions, including supporting coups and overthrowing democratically elected governments, leading to prolonged civil unrest and human rights abuses in these countries.
Calls for accountability for the US's role in destabilizing Central and South America.
Emphasizes the need for the US to stop causing harm in other countries rather than fortifying itself against the consequences.

Actions:

for policy makers, activists, advocates,
Hold policymakers accountable for past and present US interventions (implied)
Advocate for a shift in US foreign policy towards non-intervention and support for self-governance in affected countries (implied)
Support organizations working towards justice and stability in Central and South America (implied)
</details>
<details>
<summary>
2019-07-03: Let's talk about prosthetic legs, George Soros, and Charles Koch.... (<a href="https://youtube.com/watch?v=h3rzIOeLBNU">watch</a> || <a href="/videos/2019/07/03/Lets_talk_about_prosthetic_legs_George_Soros_and_Charles_Koch">transcript &amp; editable summary</a>)

Beau experiences a heartwarming moment at the grocery store with his son and sheds light on the collaboration between Soros and Koch to establish a think tank aiming to end forever wars, urging grassroots movements to seize the moment for change.

</summary>

"You are so cool."
"This might be the golden moment for the anti-war movement to pop its head back up."
"That's force multiplication, guys."
"For once the big boys are going to be on your side."
"There's going to be a whole lot less prosthetics. I'm okay with that."

### AI summary (High error rate! Edit errors on video page)

Beau experiences a moment at the grocery store with his four-year-old son where he anticipates his son saying something about a woman with a camouflage prosthetic leg, but instead, the son admires her, calling her cool.
George Soros and Charles Koch, considered America's bond villains by Beau, are teaming up to start a think tank called the Quincy Institute for Responsible Statecraft with the goal of ending forever wars like those in Afghanistan and Iraq.
The Institute aims to embody John Quincy Adams' vision of American foreign policy, focusing on diplomacy over military intervention.
Beau encourages grassroots anti-war movements to seize this moment and work alongside special interest groups supported by Soros and Koch.
He suggests that incorporating personal stories of those affected by wars, like veterans who have served multiple tours, into the anti-war movement could be more impactful than solely focusing on economic and policy aspects.
Beau acknowledges potential skepticism about billionaires profiting from peace efforts but believes in the potential reduction of prosthetic needs due to fewer wars.

Actions:

for peace advocates, activists,
Keep an eye on the Quincy Institute for Responsible Statecraft's efforts (suggested)
</details>
<details>
<summary>
2019-07-02: Let's talk about coffee, women, and honor.... (<a href="https://youtube.com/watch?v=NizYw2IbML4">watch</a> || <a href="/videos/2019/07/02/Lets_talk_about_coffee_women_and_honor">transcript &amp; editable summary</a>)

Beau challenges toxic masculinity and advocates for labeling dishonorable actions to drive change, urging simplicity in communication.

</summary>

"Men determine their morality based on the opinions of women."
"Trump supporters are not a well-educated demographic."
"It is time to use toxic masculinity as an advantage."
"If Border Patrol starts to be seen as dishonorable, well, you'll see changes in a bunch of different ways."
"They can't spin the morality of this. This is dishonorable."

### AI summary (High error rate! Edit errors on video page)

Expresses love for coffee and keeps multiple flavors on hand at all times.
Talks about Border Patrol feeling threatened by AOC, a young representative in Congress.
Mentions jokes circulating about why Border Patrol felt threatened by AOC.
Suggests that men in the country get their moral compass from their mothers, impacting their behavior.
Emphasizes the importance of speaking a relatable and simple language to reach a broader audience.
Points out the lack of education among Trump supporters and the need for simplicity in communication.
Encourages using the word "dishonorable" to question immoral actions effectively.
Advocates for challenging toxic masculinity and questioning the masculinity of individuals engaging in dishonorable actions.
Urges women to lead the charge in addressing toxic masculinity and labeling actions as dishonorable.
Stresses the need to paint Border Patrol's actions as dishonorable to drive change.

Actions:

for activists, advocates, women,
Challenge toxic masculinity by calling out dishonorable actions (exemplified)
Use the word "dishonorable" to question immoral behavior effectively (exemplified)
Lead the charge in addressing toxic masculinity and labeling actions as dishonorable (exemplified)
</details>
<details>
<summary>
2019-07-01: Let's talk about toilets, borders, and AOC.... (<a href="https://youtube.com/watch?v=QH71ldfiNMU">watch</a> || <a href="/videos/2019/07/01/Lets_talk_about_toilets_borders_and_AOC">transcript &amp; editable summary</a>)

Beau responds to denial of fascism, recounts border guards' dehumanizing actions, and warns of escalating violence, urging action to prevent dark times ahead.

</summary>

"Guns don't kill people. Governments do and we're on our way."
"We are headed to some very very dark times."
"If you can sit there and laugh at a mother being told to drink out of a toilet, you'll probably be okay with putting a bullet in the back of her head."

### AI summary (High error rate! Edit errors on video page)

Responds to comments denying fascism and genocide.
Border guards joked about a mother drinking out of a toilet.
A Facebook group of guards planned a GoFundMe for assaulting AOC.
The guards knew about upcoming inspections but were unfazed.
Acts of dehumanization and abuse are rampant in detention centers.
Deplorable conditions and inhumane treatment are overlooked.
Laughter at dehumanization makes it easier to escalate violence.
Points out the trend towards escalating violence and dehumanization.
Urges action to prevent further descent into darkness.
Calls for accountability and action to stop the worsening situation.

Actions:

for activists, advocates,
Call for accountability and action to stop the worsening situation (suggested).
Start calling, marching, or doing whatever you're comfortable with to prevent further descent into darkness (suggested).
</details>
<details>
<summary>
2019-07-01: Let's talk about 14 characteristics, 10 stages, and where you are.... (<a href="https://youtube.com/watch?v=83mtXbwPNkc">watch</a> || <a href="/videos/2019/07/01/Lets_talk_about_14_characteristics_10_stages_and_where_you_are">transcript &amp; editable summary</a>)

Beau lists 14 characteristics of fascism, outlines 10 stages, and warns about the signs of genocide happening currently.

</summary>

"There's never been a fascist regime in history that did not go through these ten stages."
"You have all of these lists that have been around a long, long time, and we're matching them up almost like we're following them like a blueprint."
"There are people that need to understand this. They need to understand where we're at."

### AI summary (High error rate! Edit errors on video page)

Lists 14 characteristics of a certain kind of government with examples like powerful displays of nationalism, disdain for human rights, identifying enemies and scapegoats.
Mentions supremacy of the military and rampant sexism as characteristics.
Talks about an obsession with national security, blending of religion and government, and corporate power protection or blending.
Mentions labor suppression and disdain for intellectuals in the arts.
Addresses obsession with crime and punishment, police brutality, cronyism, and corruption.
Talks about fraudulent elections and the 10 stages of fascism, from classification to extermination.
Raises awareness about the signs of genocide and ethnic cleansing happening currently.
Urges viewers to pay attention to the unfolding situation and make a choice to correct it.

Actions:

for activists, community members,
Contact local representatives to demand accountability for human rights violations (implied).
Support organizations working to protect human rights and fight against discrimination (implied).
Educate others about the signs of fascism and genocide happening currently (implied).
</details>

## June
<details>
<summary>
2019-06-30: Let's talk about what we can learn from AOC getting caught in a PR stunt.... (<a href="https://youtube.com/watch?v=coZzVfYiNf8">watch</a> || <a href="/videos/2019/06/30/Lets_talk_about_what_we_can_learn_from_AOC_getting_caught_in_a_PR_stunt">transcript &amp; editable summary</a>)

Conservative news sites spread a false story about AOC visiting a detention center, revealing the dangers of manufactured opinions and the importance of personal fact-checking.

</summary>

"This is how opinions are manufactured."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Conservative news sites are spreading a misleading story about AOC visiting an empty parking lot and pretending it's a detention center.
The photos in question were actually taken a year before AOC became a representative, during a large protest at a detention center.
No children were visible at the fence line because they were not allowed that close to the edge of the detention center.
The story originated from InfoWars and was not fact-checked by reputable outlets.
Beau urges people to fact-check information themselves, as news sites may push false narratives to fit their agenda.

Actions:

for media consumers,
Fact-check stories spread by news sites (suggested)
Be vigilant about misinformation and false narratives (implied)
</details>
<details>
<summary>
2019-06-28: Lets talk about my response to Carey's #ThisIsVirtue challenge.... (<a href="https://youtube.com/watch?v=1Yf8P5APf5g">watch</a> || <a href="/videos/2019/06/28/Lets_talk_about_my_response_to_Carey_s_ThisIsVirtue_challenge">transcript &amp; editable summary</a>)

Beau challenges viewers to recite the "New Colossus" sonnet and participate in the "This Is Virtue challenge" to remind people of American virtues, encouraging them to take action and use their voices against injustice.

</summary>

"Give me your tired, your poor, your huddled masses yearning to breathe free."
"What are you going to tell them? Are you going to tell them that you used your voice?"
"The idea is to remind people what American virtues are, what they're supposed to be."

### AI summary (High error rate! Edit errors on video page)

Recites "New Colossus" sonnet, focusing on America's promise of welcoming immigrants.
Raises questions about what people are doing to uphold American virtues and values.
Challenges people to participate in the "This Is Virtue challenge" by reciting the sonnet and sharing it on social media with the hashtag #thisisvirtue.
Encourages individuals to use their voices and be proactive in standing against policies that harm vulnerable populations.
Points out the importance of not getting lost in trivial arguments when significant issues are at stake.
Calls for action to remind people of American virtues and the country's promised values.
Urges viewers to think about how they will answer future generations when asked about their actions during this era.

Actions:

for social media users,
Recite and share the "New Colossus" sonnet on social media with the hashtag #thisisvirtue (suggested).
</details>
<details>
<summary>
2019-06-28: Let's talk about the lawsuit brought by federal workers.... (<a href="https://youtube.com/watch?v=Zg7W3rmR2bo">watch</a> || <a href="/videos/2019/06/28/Lets_talk_about_the_lawsuit_brought_by_federal_workers">transcript &amp; editable summary</a>)

Federal asylum officers challenge Trump's dictatorial control over the "remain in Mexico policy," condemning it as fundamentally immoral.

</summary>

"The fact that they oppose Trump is not news. What is news is who brought the lawsuit."
"It most certainly is."
"It's intentionally barring asylum officers from doing their job."
"It's about stopping people and then slowing down the rate at which we process asylums claims so they die."
"People who are used to making life and death decisions. People who are totally comfortable sending people back to squalor. They're saying that this is immoral."

### AI summary (High error rate! Edit errors on video page)

Lawsuit brought by federal asylum officers, not politicians like former Secretary of Homeland Security and James Clapper.
Federal asylum officers listen to harrowing stories daily and make life-and-death decisions on asylum claims.
Lawsuit is against the "remain in Mexico policy" or migrant protection protocols, considered a violation of international and domestic law.
Migrant protection protocols were dictated by Trump and cause widespread violations of rights.
Asylum officers are barred from asking asylum seekers if they fear for their safety in Mexico under the current system.
Policy intentionally slows down asylum claims processing to the point where people suffer and die.
Beau criticizes Trump's actions as dictatorial, bypassing constitutional checks and controlling concentration camps.
Federal asylum officers challenge Trump's actions, despite their usual role in making tough decisions.
They condemn the policy as immoral and fundamentally against the moral values of the nation.

Actions:

for advocates for human rights,
Support organizations advocating for asylum seekers' rights (implied)
Raise awareness about the violations caused by the "remain in Mexico policy" (implied)
Advocate for policy changes to protect asylum seekers (implied)
</details>
<details>
<summary>
2019-06-27: Let's talk about the positive virtue signaling.... (<a href="https://youtube.com/watch?v=3rO_-oCBCNA">watch</a> || <a href="/videos/2019/06/27/Lets_talk_about_the_positive_virtue_signaling">transcript &amp; editable summary</a>)

Beau talks about the positive impact of virtue signaling, the failure of compassionate campaigns, and the need for morality-focused campaigns to address critical issues like abuse and inhumane treatment.

</summary>

"We need more virtue signaling, a lot of it."
"I believe that the average American does not actually want kids to be sexually assaulted."
"We need campaigns that focus on the morality of what's actually happening."
"We're funding rape rooms."
"We need to show America what a virtue is."

### AI summary (High error rate! Edit errors on video page)

Virtue signaling has positive aspects and is a significant part of human communication.
The ice bucket challenge, despite criticism of being empty, raised over a hundred million dollars.
Compassionate campaigns supporting asylum seekers are failing due to how they're framed, not their message.
Campaigns framed in opposition to Trump may not produce the best results.
Beau is concerned about stopping the abuse of kids in detention facilities and preventing drownings.
Loyalty to political party often outweighs loyalty to principles, allowing wrong actions to be discounted.
Beau calls for campaigns reminding people of America's virtues and what they should stand for.
He finds it hard to believe that half the population tolerates sexual assault on kids and inhumane treatment of children in detention.
Beau believes most Americans do not want kids to be abused but may justify it due to loyalty to Trump.
Campaigns should focus on the morality of the situation and not political arguments.

Actions:

for advocates for social change,
Develop and support campaigns reminding people of America's virtues (implied)
Share suggestions for campaigns focused on addressing critical issues like abuse and inhumane treatment (implied)
</details>
<details>
<summary>
2019-06-27: Let's talk about the people who would risk their children to immigrate.... (<a href="https://youtube.com/watch?v=OMrHm4GFvpg">watch</a> || <a href="/videos/2019/06/27/Lets_talk_about_the_people_who_would_risk_their_children_to_immigrate">transcript &amp; editable summary</a>)

Beau dives into history to explain the courage of immigrants and challenges individuals to rethink their views on immigration and refugees.

</summary>

"People are still taking that risk today."
"The spiritual sons and daughters of the people who built this country are the people dying down there on the border."
"No self-evaluation will ever be as valuable or as accurate as an enemy's evaluation of you."
"Who if you have the courage."
"You'd be welcoming these people as new additions to the American tapestry, not crying and saying that their children deserve to drown."

### AI summary (High error rate! Edit errors on video page)

Beau was asked a question that led him to research and provide an insightful answer about the kind of people who immigrate.
He shares a first-person account from the 1700s detailing the harsh conditions immigrants faced, similar to present-day situations.
Immigrants arriving before the mid-1800s faced challenges like being purchased off ships and enduring sickness while waiting to come ashore.
Families often had to give their sick children to strangers to save their lives during immigration.
The conditions for immigrants improved slightly in the 1840s but were still challenging.
Beau points out that throughout history, people came to the U.S. as refugees fleeing various crises.
He questions individuals who suggest fixing their corrupt home countries instead of seeking better opportunities elsewhere.
Beau criticizes those who blame immigrants for the country's issues while failing to address systemic problems like lack of basic necessities and democracy concerns.
He challenges the notion of leaving the country if one has issues with it, citing the true embodiment of the American spirit in those enduring hardships at the border.
Beau underscores the courage and spirit of immigrants and urges embracing them as additions to the American tapestry rather than condemning them.

Actions:

for advocates, activists, citizens,
Welcome and support immigrants and refugees as new additions to your community (implied)
Challenge misconceptions about immigrants and refugees in your circles (implied)
Advocate for humane treatment and support for those seeking refuge (implied)
</details>
<details>
<summary>
2019-06-26: Let's talk about people drowning on the border and stronger locks.... (<a href="https://youtube.com/watch?v=GhWZqhAPZXU">watch</a> || <a href="/videos/2019/06/26/Lets_talk_about_people_drowning_on_the_border_and_stronger_locks">transcript &amp; editable summary</a>)

Beau addresses the misrepresentation of drowned individuals as "illegal immigrants invading our house" and reveals the harsh reality of their situation, condemning US policies and acknowledging America's role in the tragic events.

</summary>

"We set their house on fire and we're taking pot shots at them as they leave, and then when they die, well, it happens."
"If you support these policies that are going on down at the border, it's this moral equivalent, is if you took that little girl, put your boot on her head, and held her head under that water until the bubble stopped, you killed her."
"We killed them. We killed that little girl. We killed her father."
"That's America today."
"We live here, we won the geographic lottery, and we can look at these things that happen in other countries and act like, well, it doesn't have anything to do with us."

### AI summary (High error rate! Edit errors on video page)

Addresses the portrayal of drowned individuals as "illegal immigrants invading our house."
Explains that these people were fleeing from a country, El Salvador, with a corrupt government supported by the US.
Describes El Salvador as being overrun by a gang that originated in the United States.
Mentions issues in El Salvador leading to the ban on a police force as part of a peace agreement.
Talks about how the gang, with US-trained leadership, became a transnational threat known as MS-13.
Criticizes policies at the border and compares supporting them to committing a heinous act.
Points out the role of the US in the situation and the consequences faced by the fleeing individuals.
Emphasizes the luck and geographic advantage enjoyed by people in the US.
Condemns the actions that led to the deaths of individuals including a little girl and her family.
Concludes by reflecting on the impact of these events.

Actions:

for policy advocates,
Support organizations aiding asylum seekers (suggested)
Advocate for policy changes regarding immigration and border control (implied)
</details>
<details>
<summary>
2019-06-25: Let's talk about the evils of virtue signaling.... (<a href="https://youtube.com/watch?v=pCTllwHd5RQ">watch</a> || <a href="/videos/2019/06/25/Lets_talk_about_the_evils_of_virtue_signaling">transcript &amp; editable summary</a>)

Virtue signaling, both effective and divisive, influences societal norms and political rhetoric, shaping perceptions and policies.

</summary>

"Virtue signaling is extremely effective. It causes societal change."
"Calling out virtue signaling can also be a form of virtue signaling."
"The normalization of inhumane practices, such as in ICE's statement on immigration policy, can be seen as a form of virtue within certain groups."

### AI summary (High error rate! Edit errors on video page)

Virtue signaling is the expression of a positive social behavior to your in-group, not necessarily an action rooted in compassion.
Virtue signaling can be effective in causing societal change, even though it may come across as empty or hollow.
Examples of virtue signaling include the creation of the "warrior cop" and gestures of loyalty to certain dictators in history.
Calling out virtue signaling can also be a form of virtue signaling, conveying superiority and rationality within a group.
Experts have expressed concern over how Trump's rhetoric normalized dehumanization and spread through political parties and government agencies.
The normalization of inhumane practices, such as in ICE's statement on immigration policy, can be seen as a form of virtue within certain groups.
Conflicting views on immigration policies and human trafficking often overlook the root causes and focus solely on containment rather than solutions.
Society needs positive virtue signaling to steer towards a more humane society by addressing underlying issues and promoting compassion.

Actions:

for activists, advocates, observers,
Challenge dehumanizing narratives within your community (exemplified)
Advocate for addressing root causes of issues (implied)
Promote compassionate solutions in policy and discourse (exemplified)
</details>
<details>
<summary>
2019-06-24: Let's talk about the 300 kids.... (<a href="https://youtube.com/watch?v=aJ_5SO1VR_w">watch</a> || <a href="/videos/2019/06/24/Lets_talk_about_the_300_kids">transcript &amp; editable summary</a>)

300 children removed from overcrowded Border Patrol facility to another, facing neglect and inhumane conditions, a crisis overlooked on American soil.

</summary>

"Do you want to wait until we have a bunch of deaths?"
"This is a travesty. This is ridiculous."
"They're livestock. They're a product."
"This is not even surprising anymore."
"They have no idea where their family is."

### AI summary (High error rate! Edit errors on video page)

300 children were removed from a Border Patrol facility in Clint, Texas, designed for 100 people but housed 350.
Visitors described the facility as having a stench.
The children lacked adequate food, hygiene items like soap and toothpaste, and hadn't showered in almost a month.
Their clothing had bodily fluids ranging from breast milk to urine.
Many children don't know the whereabouts of their families.
Flu outbreaks were present, and there was a lack of beds.
Border Patrol had younger children taking care of toddlers.
The children have not been rescued; they were moved to another facility, which Beau calls a "Trump concentration camp."
Deaths in concentration camps were often due to disease and lack of food caused by poor hygiene.
Beau questions when people will call their senators and address the crisis.
Children are being treated like livestock, not as human beings.
The situation is so dire that it's no longer shocking, which Beau finds tragic.
The media's portrayal of the children being "removed" is misleading; they remain in the custody of the same agency that neglected them.
These children are seen as numbers on a board, not as individuals facing a humanitarian crisis.

Actions:

for advocates for human rights,
Contact your senator to demand immediate action to address the inhumane conditions faced by children in Border Patrol facilities (suggested)
Advocate for policy changes to ensure proper care and treatment of detained children (implied)
Organize or participate in protests to raise awareness about the crisis and pressure authorities for change (implied)
</details>
<details>
<summary>
2019-06-24: Let's talk about a ship getting attacked.... (<a href="https://youtube.com/watch?v=4FRpZZPcQfs">watch</a> || <a href="/videos/2019/06/24/Lets_talk_about_a_ship_getting_attacked">transcript &amp; editable summary</a>)

Beau reveals the truth behind the Vietnam War, a conflict built on deception and lies, leading to immense loss of life and a stark reminder of the consequences of manufactured narratives.

</summary>

"The worst war in American history was fought over a lie."
"All of those graves were filled over a lie."
"The Vietnam War was based on a lie."

### AI summary (High error rate! Edit errors on video page)

Tonight, I'm sharing links on a topic most Americans might not know or believe.
The U.S. Senate repealed the Gulf of Tonkin Resolution on June 24, 1970.
This resolution authorized the Vietnam War based on a false incident.
The Gulf of Tonkin incident was used to justify the war, but the truth was different.
Operation De Soto and Opland 34A were running simultaneously in Vietnam.
The North Vietnamese attack on the Maddox was a response to a previous US action.
Fabricated evidence was used to justify the Vietnam War.
Many lives were lost based on this lie.
The war monument stands as a reminder of the cost of this deception.
Similar pretexts for war have been sought since then, like tying Iran to al-Qaeda post-9/11.

Actions:

for history buffs, truth-seekers, activists,
Share this history lesson with others (suggested)
Educate yourself and others on the truths behind past conflicts (implied)
</details>
<details>
<summary>
2019-06-23: Let's talk about what Radio London can teach us about today.... (<a href="https://youtube.com/watch?v=-mMCQRiuB9g">watch</a> || <a href="/videos/2019/06/23/Lets_talk_about_what_Radio_London_can_teach_us_about_today">transcript &amp; editable summary</a>)

Allies' WWII broadcasts inspire messages of hope amidst current immigrant community support and LAPD's refusal to cooperate, reminding that history rhymes with present challenges.

</summary>

"History doesn't repeat. It rhymes."
"The front line is everywhere."
"No, it's not the same as Nazi Germany."
"We are in occupied territory."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Allies in World War II broadcasted messages to the resistance in occupied territories using various frequencies and codes to provide information and hope.
A reenactment of one of those broadcasts is presented, including personal messages, music, and messages of hope.
Messages of hope address the targeting of immigrant families by the federal administration, providing support and information to communities.
Various mayors, including San Francisco, L.A., Atlanta, Denver, Baltimore, and D.C., stand in solidarity with immigrant communities and against inhumane practices.
LAPD released a statement refusing to cooperate with ICE raids, distancing themselves from such actions.
The president's threats and ultimatums are compared to past historical events, with a reminder that history rhymes but does not repeat itself.

Actions:

for community members,
Stand in solidarity with immigrant communities (implied)
Provide support and information to immigrant families (implied)
Refuse to cooperate with inhumane practices (implied)
</details>
<details>
<summary>
2019-06-22: Let's talk about a golden moment with Iran.... (<a href="https://youtube.com/watch?v=alstUioeuG0">watch</a> || <a href="/videos/2019/06/22/Lets_talk_about_a_golden_moment_with_Iran">transcript &amp; editable summary</a>)

Understanding Iran's history is key to making informed decisions for a peaceful future, necessitating the strengthening of its democratic side while keeping conflict at bay.

</summary>

"War isn't good for anybody."
"This cluster that was caused by the president pulling out of the deal may actually be a blessing in disguise."
"We're at this golden moment where we can actually take another step forward."
"In order to say that though, we've got to keep the hawks at bay."
"Strengthening the democratic side of the government there needs to be the primary mission."

### AI summary (High error rate! Edit errors on video page)

Iran's history dating back to the late 40s and early 50s is vital to understanding current events.
In 1953, the CIA orchestrated the overthrow of Iran's elected government in Operation Ajax.
The Shah consolidated power with the help of the brutal intelligence agency, SAVAK.
SAVAK operated with vague laws, no legal representation, and severe consequences like imprisonment or execution.
Resentment grew among the populace, particularly targeting dissidents like Azerbaijanis and Kurds.
Economic prosperity from oil sales in the early 70s did not meet expectations, leading to dissatisfaction.
Rising Islamic nationalism in the Middle East fueled revolutionary sentiments.
The 1979 revolution caught the intelligence community off guard, ushering in the current government.
Iranians view their government as a necessary defense against the West, providing relative prosperity and regional power.
Strengthening the democratic side of the Iranian government is key to improving relations and avoiding conflict.

Actions:

for policymakers, activists, concerned citizens,
Strengthen the democratic side of the Iranian government by supporting diplomatic efforts and engaging with moderate voices (suggested).
Advocate for peaceful resolutions and diplomacy over aggression in dealing with Iran (implied).
</details>
<details>
<summary>
2019-06-21: Let's talk about semantics and camps.... (<a href="https://youtube.com/watch?v=l6pYSp4OjNE">watch</a> || <a href="/videos/2019/06/21/Lets_talk_about_semantics_and_camps">transcript &amp; editable summary</a>)

Beau unpacks the comparison between current detention facilities and concentration camps, urging differentiation and reflecting on potential future repercussions.

</summary>

"Sleep deprivation, temperature extremes, solitary without calls. These are all forms of torture."
"I will defend ICE on one thing. I do understand why they're not quick to provide soap to the detainees."
"You can trick them and tell them they're going to take a shower, right? It's America."

### AI summary (High error rate! Edit errors on video page)

Addressing the controversy around certain terms used for current detention facilities.
Comparing the conditions inside detention facilities to forms of torture.
Mentioning sleep deprivation, temperature extremes, solitary confinement, and more as torture methods.
Noting instances of nooses in cells, sex abuse complaints, and attempts to destroy records.
Bringing up the government lawyer's argument about not needing to provide basic necessities like beds, toothbrushes, toothpaste, and soap.
Referring to experts like George Takei and Andrea Pitzer who identify the facilities as concentration camps.
Explaining that concentration camps don't necessarily mean death camps but are a part of a network.
Suggesting differentiating between German concentration camps and current ones by adding "Trump" to the name.
Defending ICE's lack of providing soap due to limited supply and potential misuse by employees.
Expressing concern about the dehumanizing treatment and potential future consequences.
Ending with a reference to the manipulation and deception faced by detainees.

Actions:

for activists, advocates, individuals,
Contact organizations supporting detainees (implied)
Advocate for humane treatment of detainees (implied)
Educate others on the conditions in detention facilities (implied)
</details>
<details>
<summary>
2019-06-20: Let's talk about the bigger question in the Bella Thorne scandal.... (<a href="https://youtube.com/watch?v=nisumVzhGSs">watch</a> || <a href="/videos/2019/06/20/Lets_talk_about_the_bigger_question_in_the_Bella_Thorne_scandal">transcript &amp; editable summary</a>)

Beau addresses the unfair stigma surrounding leaked intimate photos, advocating for a shift in societal attitudes towards privacy and normal behavior.

</summary>

"She's a victim of a crime."
"This is extremely common behavior."
"You're talking about consenting adults exchanging images of themselves."
"She didn't do anything wrong, and she certainly didn't do anything wrong simply because of the way she was dressed."
"Don't turn this into a scandal."

### AI summary (High error rate! Edit errors on video page)

Bella Thorne's intimate photos were hacked and threatened to be released by a blackmailer.
Thorne took control by posting the photos herself on Twitter.
Whoopi Goldberg criticized Thorne, implying celebrities should expect hacking if they take such photos.
Beau questions the victim-blaming mentality behind Goldberg's statement.
He points out the unfair societal expectations placed on celebrities.
The scandal around Thorne's photos stems from unrealistic expectations of public figures.
Beau mentions the commonality of taking such photos, especially among young people.
He criticizes the stigmatization of normal behavior when it comes to celebrities.
There's a focus on the pressure for public figures to appear perfect all the time.
Beau suggests removing the scandalization of leaked photos to combat blackmail and violations.

Actions:

for social media users,
Challenge societal norms around privacy and body autonomy (implied)
Advocate for the destigmatization of common behaviors (implied)
Support individuals affected by privacy violations (implied)
</details>
<details>
<summary>
2019-06-20: Let's talk about Iran, lies, and confusion.... (<a href="https://youtube.com/watch?v=ASaT7RHA_Ow">watch</a> || <a href="/videos/2019/06/20/Lets_talk_about_Iran_lies_and_confusion">transcript &amp; editable summary</a>)

Beau predicts war, questions credibility, rejects war for political gain, and calls for valuing lives over politics.

</summary>

"You're not going to be able to fool anybody this time."
"That accusation is a confession."
"Get on those instead of standing on the graves of American troops and innocent Iranians."

### AI summary (High error rate! Edit errors on video page)

Predicts a storm coming and addresses the urgency of discussing Iran.
Mentions the inevitability of war when discussed in a previous video.
Questions the credibility of claims linking Iran to sponsoring Al-Qaeda.
Criticizes the lack of believability in the narrative presented to Congress.
Raises doubts about the U.S. Navy's claim of a drone being shot down in international waters.
Reminds of past incidents like the U.S. shooting down Iran Air 655 without consequences.
Rejects the idea of going to war over the recent drone incident.
Calls out politicians, including Donald Trump, for using Iran as a tool for political gain.
Challenges the idea of going to war for political motives at the expense of American soldiers and innocent Iranians.
Ends with a thought-provoking message about not sacrificing lives for political gains.

Actions:

for concerned citizens,
Stand against war for political gain (implied)
</details>
<details>
<summary>
2019-06-19: Let's talk about mass removals and freedom.... (<a href="https://youtube.com/watch?v=uCt60ViLh7M">watch</a> || <a href="/videos/2019/06/19/Lets_talk_about_mass_removals_and_freedom">transcript &amp; editable summary</a>)

The Statue of Liberty, removals, and the call for amnesty – a poignant reminder of the humanity behind immigration policies.

</summary>

"You're going to see they're real people."
"Amnesty, amnesty, amnesty. the law. Amnesty."
"Let's help the tempest-tossed, the huddled masses."

### AI summary (High error rate! Edit errors on video page)

Statue of Liberty arrived in New York in 1885, without the famous inscription.
The President plans to target undocumented people for removal, including those who committed no other crime than crossing a line.
ICE is hesitant about this approach due to bad optics and potential public backlash.
Removals are typically done at job sites to avoid public scrutiny and dehumanization.
The impact of seeing families torn apart during removals will be significant.
The punishment for undocumented immigrants seems harsh compared to other violations like watching a movie without a ticket.
Children of undocumented parents face dire situations if their families are targeted for removal.
Cheering on mass removals contradicts the fear of government overreach.
Amnesty is suggested as a humane solution to prevent the destruction of millions of lives.
Prioritizing removals over going after real criminals like human traffickers is questioned.

Actions:

for advocates, activists, citizens,
Advocate for humane immigration policies (implied)
Support organizations working to protect undocumented individuals (implied)
Educate others on the realities of immigration enforcement (implied)
</details>
<details>
<summary>
2019-06-18: Let's talk about my friend who was at the shooting today.... (<a href="https://youtube.com/watch?v=gxEg2-bWM9M">watch</a> || <a href="/videos/2019/06/18/Lets_talk_about_my_friend_who_was_at_the_shooting_today">transcript &amp; editable summary</a>)

Beau challenges the myth of the "good guy with a gun" and addresses the cultural issues around gun ownership, advocating for a shift in mindset away from toxic masculinity.

</summary>

"Guns don't kill people. People kill people."
"The gun doesn't make the man."
"It's a bandaid on a bullet wound."

### AI summary (High error rate! Edit errors on video page)

Beau shares a personal experience of a friend involved in a shooting incident at a gun-free zone.
The friend secured his weapon in the car, heard gunfire, and moved to a safer location instead of engaging the threat.
Beau challenges the myth of the "good guy with a gun," stating that untrained individuals are more likely to die in combat than successfully intervene.
He mentions the prevailing myth of guns not killing people, but rather people killing people, and how it leads to a dangerous mindset.
Beau refers to the conditioning required for individuals to kill and how only 4% of people are naturally wired for it.
He talks about the flawed belief that arming more people will stop mass shootings and dismantles the idea of a "good guy with a gun."
Beau criticizes the cultural glorification of guns and stresses the need to address the root cultural issues rather than rely on legislation.
He mentions the case of the Parkland cop and the different standards applied due to training and duty to intervene.
Beau concludes by stressing that the concept of a "good guy with a gun" is part of the problem, not the solution, and signifies toxic masculinity.

Actions:

for advocates for cultural change,
Educate others on the dangers of the "good guy with a gun" myth (suggested)
Challenge the glorification of guns in culture (implied)
</details>
<details>
<summary>
2019-06-17: Let's talk about flags, promises, and logos.... (<a href="https://youtube.com/watch?v=vB8I9uQTTx0">watch</a> || <a href="/videos/2019/06/17/Lets_talk_about_flags_promises_and_logos">transcript &amp; editable summary</a>)

Beau questions the true symbolism of the American flag amidst debates over flag burning, urging patriots to actively uphold promises of equality and justice to give meaning to national symbols.

</summary>

"If you don't act on those promises, this means nothing."
"The person burning this flag, they didn't disgrace it. You did when you failed to keep the promises."
"It's just a thought. Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Questions the symbolism of flag burning as a constitutional amendment banning it gains attention.
Points out the irony of claiming freedom in a nation with high incarceration rates and fear-driven policies.
Emphasizes that the founding fathers spoke more of ideas like freedom and liberty than nationalism.
Challenges if society truly upholds promises of equality, liberty, and justice for all.
Questions the real meaning of symbols like the American flag and who truly understands its promises.
Criticizes those who aim to criminalize flag burning while ignoring the essence of freedom and equality.
Urges patriots to actively work towards equal rights and justice for all to truly embody American values.
States that symbols like the flag lose meaning if promises of equality and liberty are not upheld.
Condemns those in power who prioritize protecting a flag over fulfilling the country's foundational ideals.
Expresses concern over the dwindling efforts to ensure equality for all in society.

Actions:

for patriots, activists, americans,
Work towards equal rights and justice for all in your community (implied)
Actively uphold the promises of equality and liberty in your daily actions (implied)
</details>
<details>
<summary>
2019-06-16: Let's talk about Phoenix PD and Barbies.... (<a href="https://youtube.com/watch?v=rN1wEwOHkAA">watch</a> || <a href="/videos/2019/06/16/Lets_talk_about_Phoenix_PD_and_Barbies">transcript &amp; editable summary</a>)

Phoenix PD's credibility is questioned as Beau exposes discrepancies, lack of training, and accountability in a troubling incident involving excessive force.

</summary>

"You threaten to kill a mother in front of her kids. Yeah. Nobody cares."
"If you drag this out it's just going to get worse."
"Your credibility is already shot. You need to make your decision on what you're going to do."
"You're going to get somebody killed."
"You have the same propensity for violence as an addict looking for a fix."

### AI summary (High error rate! Edit errors on video page)

Phoenix PD released a fact sheet light on facts, attempting to justify their actions with no use of force mentioned, despite contradicting evidence in original reports.
The credibility of the Phoenix PD is questioned, given the discrepancies in their statements.
Even if the facts were true, the situation worsens due to the presence of children during the incident.
Beau questions the justification for excessive force and violence by the Phoenix PD, even in hypothetical scenarios like shoplifting.
The core issues lie in the lack of proper training and tactics displayed by the officers involved.
Failure to follow standard procedures like time, distance, and cover, and conflicting commands led to a chaotic and dangerous situation.
The officers' actions instilled fear and panic unnecessarily, leading to a traumatic experience for the individuals involved.
Beau criticizes the lack of de-escalation attempts and the absence of body cameras, which are vital for accountability and learning from such incidents.
The safety of civilians, especially children, was compromised by the actions of the Phoenix PD.
Beau calls for accountability within the department, suggesting that all officers on scene should be removed due to negligence and lack of accurate reporting.

Actions:

for community members, activists,
Demand accountability from Phoenix PD (suggested)
Advocate for policy changes regarding the use of force (exemplified)
Support civilians standing against police misconduct (implied)
</details>
<details>
<summary>
2019-06-15: Let's talk about the right and left in America.... (<a href="https://youtube.com/watch?v=ufzwfNWOmZ4">watch</a> || <a href="/videos/2019/06/15/Lets_talk_about_the_right_and_left_in_America">transcript &amp; editable summary</a>)

Beau clarifies the misusage of "left" and "right," distinguishing Democrats as liberals, not leftists, and advocates for defending radical ideas to prevent society from moving further right.

</summary>

"Without the radical left, well, we're all just right-wing."
"History has a well-known leftist bias."

### AI summary (High error rate! Edit errors on video page)

Points out the incorrect usage of "left" and "right" in the US and explains common terms.
Differentiates between Democrats as liberals and not leftists, more center-right.
Mentions the lack of a viable left-wing party in the United States.
Provides an example of a Facebook post and tweet supporting amnesty for undocumented individuals.
Acknowledges potential backlash for his statement on amnesty.
References Ronald Reagan's stance on amnesty, contrasting it with President Trump's wall policy.
Criticizes the right for not having a hard line like the left, attributing it to the lack of theocratic elements.
Encourages not to shy away from radical ideas and to defend them.
Suggests that without the radical left, society tends to move further right.
Emphasizes the importance of advocating for a fair and just society to move towards the left globally.

Actions:

for activists, progressives, liberals,
Defend radical ideas when you hear them (advocated)
Advocate for a fair and just society (advocated)
</details>
<details>
<summary>
2019-06-14: Let's talk about a midnight revival.... (<a href="https://youtube.com/watch?v=YBQhFPXaO2o">watch</a> || <a href="/videos/2019/06/14/Lets_talk_about_a_midnight_revival">transcript &amp; editable summary</a>)

Beau addresses a sermon calling for mass murder against the LGBTQ community, urging individuals to reject complicity in hate and choose resistance against oppressive ideologies.

</summary>

"He's carried it, he struggled with it for so long for too long the sacrifice makes a stone of the heart and that is what happened when people talked about him he's giving himself over."
"I thought of another congregation a long time ago, back in the 30s. It was in Hamburg, another firebrand comes to town. Preaching the same hate ends the same way, advocating for the government to take care of the undesirables, take care of those who just don't fit."
"We are fast approaching a time in this country where we have a choice. We can become brown shirts. We can become white roses."

### AI summary (High error rate! Edit errors on video page)

Beau sets the scene for a midnight revival outside, prompted by a congregation member's distress over a sermon.
The congregation member, labeled a heretic, brought to Beau's attention a sermon filled with hate speech and calls for mass murder against the LGBTQ community.
The sermon in question was delivered by a former detective from Knox County, Tennessee.
The detective in the sermon uses anecdotes of drug abuse and rape to demonize the LGBTQ community, sparking Beau's reflection on the potential to vilify any community with such anecdotes.
Beau condemns the sermon's message as rooted in hatred and points out the speaker's long-standing struggle with carrying that hate.
He criticizes the congregation's silent complicity in listening to calls for mass murder and likens it to historical instances of silent consent to atrocities.
Beau invokes the image of August Landmesser, a lone dissenter in a crowd, as a symbol of defiance against hateful ideologies.
He warns of the country's choice between becoming brown shirts or white roses, referencing historical symbols of complicity versus resistance.
Beau ends with a thought-provoking message about the need for individuals to stand up against hate and choose the path of resistance.

Actions:

for congregation members, activists,
Stand against hate speech and discriminatory ideologies within your community (exemplified)
Speak out against calls for violence and mass murder targeting marginalized communities (exemplified)
</details>
<details>
<summary>
2019-06-12: Let's talk about dog whistles, murder rates, and country music.... (<a href="https://youtube.com/watch?v=vbUbEnm-mh4">watch</a> || <a href="/videos/2019/06/12/Lets_talk_about_dog_whistles_murder_rates_and_country_music">transcript &amp; editable summary</a>)

Beau explains dog whistles, urges dismantling hidden meanings to combat harmful ideologies, and advocates focusing solely on exposing the core messages behind them.

</summary>

"You continually hammer on that one statement and get them to address that one thing."
"You destroy those pillars, those things that hold up the rest of the ideology, those things that are so commonly believed that everybody can recognize them."
"Engage those dog whistles every chance you get and you focus on that dog whistle."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of dog whistles, which are statements with hidden meanings that give the speaker plausible deniability.
Dog whistles are used to convey messages to a specific group while appearing innocent to others.
Beau admits to using dog whistles in his videos, not necessarily racist ones, but to communicate with certain groups.
Talks about how to identify and respond to dog whistles by focusing solely on the hidden meaning and not letting the speaker divert the attention.
Mentions examples like the statement "if no government is best, then maybe no government is best" as a dog whistle for those who believe in a stateless society.
Emphasizes the importance of dismantling these hidden messages to expose the true intentions behind them.
Compares the use of dog whistles to racist jokes and suggests challenging the speaker to explain the hidden meaning behind their statements.
Provides examples like statistics on crime rates among different groups to illustrate how dog whistles work.
Encourages engaging with dog whistles to prevent others from being influenced by harmful ideologies.
Stresses the need to focus on the core message of dog whistles to dismantle the underlying beliefs.
Beau delves into the nuances of crime statistics and challenges stereotypes associated with criminal behavior.
Talks about the prevalence of dog whistles in culture, including music, and how they perpetuate harmful stereotypes.
Urges readers to confront dog whistles and dismantle the pillars of harmful ideologies.
Beau concludes by encouraging active engagement with dog whistles to combat harmful beliefs in society.

Actions:

for online activists, community organizers,
Challenge dog whistles by focusing solely on dismantling the hidden messages (suggested).
Engage actively with dog whistles to prevent harmful ideologies from spreading (suggested).
Address core messages behind dog whistles to expose harmful beliefs in society (suggested).
</details>
<details>
<summary>
2019-06-12: Let's talk about a meeting at a burger joint, the scars of history, and today.... (<a href="https://youtube.com/watch?v=naKy0o23uZU">watch</a> || <a href="/videos/2019/06/12/Lets_talk_about_a_meeting_at_a_burger_joint_the_scars_of_history_and_today">transcript &amp; editable summary</a>)

Beau witnessed a poignant encounter with a recently released elderly Black man, leading to reflections on learned behaviors and societal injustices still prevalent today.

</summary>

"Learned behavior, something he learned as a kid, fell back on it immediately, 50 years later."
"Sat there and watched the scars of history burst open in front of me."
"Hope I never hear it again."

### AI summary (High error rate! Edit errors on video page)

Witnessed an encounter at a fast-food joint involving an elderly Black man just released from prison, triggering reflections on societal dynamics and learned behaviors.
Noticed the man's defensive tone, reminiscent of interactions with racists in the 60s, which saddened him.
Offered the man a ride to his halfway house, assuming he was on his way to meet his probation officer.
Realized the man had already been to the halfway house and was waiting for his probation officer.
Learned the man had been incarcerated since the early 70s and was now in his 70s.
Felt heartbroken realizing his own actions had triggered the defensive tone in the man due to past experiences.
Reflected on societal norms and learned behaviors that persist despite the passage of time.
Expresses anger at the continued mistreatment in ICE facilities, including sexual abuse complaints and inhumane conditions for children.
Contemplates the long-lasting impact of current mistreatment on future generations' learned behaviors.
Acknowledges the scars of history and expresses frustration at the perpetuation of such injustices.
Questions the defense mechanisms and learned responses being instilled in today's youth that may resurface in the future.
Considers the lasting effects of historical trauma and ongoing mistreatment.
Expresses hope to never hear the defensive tone from the elderly man again.
Ends with a thoughtful message wishing everyone a good night.

Actions:

for community members, activists.,
Support organizations working to improve conditions in ICE facilities (implied).
Educate others on the systemic issues leading to mistreatment in detention facilities (implied).
Advocate for humane treatment of all individuals in detention (implied).
</details>
<details>
<summary>
2019-06-10: Let's talk about interstate Gospel, Eve, and a really important bag.... (<a href="https://youtube.com/watch?v=OnsPQw23QsY">watch</a> || <a href="/videos/2019/06/10/Lets_talk_about_interstate_Gospel_Eve_and_a_really_important_bag">transcript &amp; editable summary</a>)

Beau Garg questions the effectiveness of pro-life spending, suggesting research on artificial wombs as a solution, while pointing out the reality of children in foster care.

</summary>

"Maybe, if it really is about the preservation of life, that money should go to research on these artificial wombs."
"I think for a lot of people this isn't about the preservation of life. I think it's about controlling other people."
"Tens of thousands will age out of the system this year."

### AI summary (High error rate! Edit errors on video page)

Driving to Pensacola on I-10, encountering billboards with scripture, including one targeting abortion.
Billboards in the South costing a few hundred bucks in rural areas and thousands in major markets.
Hundreds of billboards all over the South, with millions spent on lobbying and informational literature for the pro-life cause.
Questioning the effectiveness of the money spent on billboards and propaganda.
Mentioning the artificial uterus, artificial womb technology successfully maturing a lamb at 24 weeks.
Research on artificial wombs could potentially end the abortion debate.
Suggesting a shift in funding towards research on artificial wombs if the goal is truly about life preservation.
Noting the significant number of children in foster care and the thousands aging out of the system annually.

Actions:

for pro-choice advocates,
Support research on artificial wombs to potentially address the abortion debate (implied).
</details>
<details>
<summary>
2019-06-10: Let's talk about Pulse nightclub, Florida man, and Florida's laws.... (<a href="https://youtube.com/watch?v=iXN8eypHbVQ">watch</a> || <a href="/videos/2019/06/10/Lets_talk_about_Pulse_nightclub_Florida_man_and_Florida_s_laws">transcript &amp; editable summary</a>)

Beau warns pastors planning to visit Pulse nightclub in Florida, criticizing their morally and legally wrong actions and urging them to call it off before someone gets hurt.

</summary>

"Y'all need to take this to another state."
"What you're doing is wrong. It's wrong morally. It's wrong on every level."
"And in Florida, oh, it's legally wrong."
"You're going to travel across the country so you can have your little bigoted conference and mock the deaths of people who were murdered? Yeah, that's what Jesus would do."

### AI summary (High error rate! Edit errors on video page)

Raises concern about pastors planning to go to the Pulse nightclub in Florida on the anniversary of the attack.
Expresses dismay at the pastors' history of celebrating deaths and saying gay people don't deserve to live.
Warns that the pastors' actions could be seen as a threat of bodily harm and instigating violence.
Explains the concept of "Florida man" and how the state's unique environment influences behavior.
Quotes the law allowing deadly force in certain circumstances to prevent harm or forcible felony.
Advises the pastors to reconsider their plans as their actions could legally lead to harm in Florida.
Urges them to call off their trip and conduct their activities in a more appropriate setting.
Condemns the pastors' behavior as morally, ethically, and legally wrong, potentially leading to forcible felonies.
Criticizes the pastors for mocking the deaths of those murdered at Pulse nightclub.
Ends with a strong statement against the pastors and their actions, questioning their alignment with Christian values.

Actions:

for community members, activists.,
Contact local authorities to raise awareness about the pastors' threatening behavior (implied).
Support LGBTQ+ organizations in Florida to ensure a safe environment during sensitive anniversaries (implied).
</details>
<details>
<summary>
2019-06-09: Let's talk about the censorship of hate speech (Part 2).... (<a href="https://youtube.com/watch?v=UTEb5cok6Bw">watch</a> || <a href="/videos/2019/06/09/Lets_talk_about_the_censorship_of_hate_speech_Part_2">transcript &amp; editable summary</a>)

Beau questions the concentration of power in regulating information and suggests decentralization to prevent echo chambers and control over content.

</summary>

"Should anybody have this much power?"
"Maybe the solution is less power in more hands rather than a whole lot of power in one set of hands."
"Instead, much like this, we attempt to influence the regulation rather than stop it."
"We're reaching a point with technology where we could very easily head into a black mirror type of situation."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Part two discussing censorship of hate speech, with three sides in the comments: two with a plan and one with a slogan.
The slogan side advocates for "free speech for all, all the time," which sounds good but raises questions about implementation.
There's a side advocating for each website determining its own content, leading to the current situation.
Another side argues for government regulation of the internet, likening it to a public utility, but Beau questions giving such power to those already holding the monopoly on violence.
Doubts if the First Amendment can truly protect in case of government censorship, pointing out precedents from other countries.
Raises concerns about government control over information, including the potential for censoring critical content and promoting propaganda.
Questions whether anyone should have such immense power over information, suggesting a need for less concentrated power.
Proposes exploring alternatives to centralized platforms like YouTube, advocating for less power in more hands.
Notes the disparity in outrage over censorship by platforms compared to other powerful entities regulating everyday lives.
Suggests that decentralization could make spreading harmful ideas harder by disrupting echo chambers and targeting specific audiences rather than broad social media users.

Actions:

for internet users,
Advocate for decentralized platforms to prevent echo chambers (suggested)
Question concentration of power in regulating information (implied)
</details>
<details>
<summary>
2019-06-08: Let's talk about the censorship of hate speech.... (<a href="https://youtube.com/watch?v=cLEZrTmkixY">watch</a> || <a href="/videos/2019/06/08/Lets_talk_about_the_censorship_of_hate_speech">transcript &amp; editable summary</a>)

Beau supports YouTube's censorship of hate speech, encourages building alternative platforms, defends valuable journalism, and acknowledges mistakes in the process.

</summary>

"You want to be a bigot? You got a right to be a bigot. The second you advocate for violence, discrimination, or some kind of negative thing to be visited upon whoever it is you're bigoted against, well then that's a problem."
"Freedom of speech protects you from the government, not from a private company and not from public outcry."
"Sunlight is a really good disinfectant. It makes it easier to debunk."
"The market has spoken. We don't want you."
"There are people that don't have access to your stuff. Video evidence of those speeches provides intent."

### AI summary (High error rate! Edit errors on video page)

Expresses support for YouTube's efforts to clean up its platform by censoring hate speech.
Defines hate speech as advocating for violence, discrimination, or negative actions against a group.
Emphasizes that freedom of speech protects from the government, not private companies.
Encourages individuals to build their own platforms if they disagree with censorship policies.
Acknowledges mistakes made by YouTube in banning channels, including a history professor and an organization called News to Share.
Defends the value of journalism like that done by Ford Fisher, documenting extremist activities and protests.
Argues that documenting extremist activities can help identify individuals inciting violence versus those exploiting the situation for financial gain.
Points out the importance of video evidence in intelligence work to determine intent and identify perpetrators.
Stresses the value of News to Share's journalism, even though mistakes were made in its censorship.
Mentions Ben Shapiro's support for Ford Fisher and the importance of Fisher's work in journalism.

Actions:

for content creators,
Contact Ford Fisher to offer support and solidarity (implied)
Support independent journalism that exposes extremist activities (implied)
</details>
<details>
<summary>
2019-06-06: Let's talk about the facts behind making things quieter.... (<a href="https://youtube.com/watch?v=COj1gl_fdsI">watch</a> || <a href="/videos/2019/06/06/Lets_talk_about_the_facts_behind_making_things_quieter">transcript &amp; editable summary</a>)

Beau explains the science behind suppressors, debunking myths and discussing their legitimate uses among law-abiding gun owners, while addressing the potential effectiveness of a ban.

</summary>

"It doesn't make anything silent."
"Decibels are a logarithmic system."
"They reduce recoil by about a third."
"If you're just letting off one shot, that's going to sound like anything other than a gunshot."
"They're very, very law-abiding people."

### AI summary (High error rate! Edit errors on video page)

Receives questions about making something quieter from both Second Amendment advocates and gun control advocates.
Explains the difference between suppressors and silencers, debunking myths.
Describes how suppressors work by reducing the decibels of a gunshot through a tube with baffles.
Notes that suppressors reduce recoil by about a third and help mask the location of a shooter by changing the sound profile.
Mentions that suppressors also reduce muzzle flash and are primarily used for military purposes to avoid raising alarms.
Addresses the illegal use of suppressors, stating that statistically, they are not significant in crimes.
Emphasizes that suppressors have legitimate uses, particularly for hearing protection, and are popular among law-abiding gun owners.
Explains the limitations of homemade suppressors compared to commercial ones.
Concludes that a ban on suppressors could be effective but acknowledges that it targets law-abiding citizens who are unlikely to use them for criminal activities.
Touches on the debate regarding the necessity of suppressors under the Second Amendment.

Actions:

for gun owners,
Contact local officials to advocate for sensible gun regulations (implied).
Educate others on the legitimate uses of suppressors for hearing protection (implied).
Support organizations promoting responsible gun ownership (implied).
</details>
<details>
<summary>
2019-06-05: Let's talk about how it can't happen here, but it can happen in Alabama.... (<a href="https://youtube.com/watch?v=NJo9vcwlI80">watch</a> || <a href="/videos/2019/06/05/Lets_talk_about_how_it_can_t_happen_here_but_it_can_happen_in_Alabama">transcript &amp; editable summary</a>)

Contrasts belief in immunity to atrocities with the disturbing advocacy for mass murder by a government official, calling for his resignation and criticizing the lack of action in Alabama.

</summary>

"The only way to change it would be to kill the problem out."
"They want your resignation. That's what they want."
"Your prospective soldiers, they think you're a threat."
"They see you as a fascist thug."
"The fact that there are no wills churning already to remove this person says everything you need to know about the great state of Alabama."

### AI summary (High error rate! Edit errors on video page)

Contrasts the belief that certain atrocities can't happen in the United States with historical evidence suggesting otherwise.
Reads a disturbing social media post by the mayor of Carbon Hill, Alabama, advocating for killing minorities.
Comments on the mayor's apology after being exposed, stating that it doesn't excuse his actions.
Calls for the mayor's resignation rather than accepting his apology.
Raises concerns about the mayor's influence over law enforcement and treatment of minority groups.
Mentions that he learned about the issue not through the news but from a far-right constitutionalist.
Criticizes the lack of action from the governor and others to remove the mayor from office.
Condemns the mayor's advocacy for mass murder and lack of due process.
Urges for the removal of the mayor to send a message that such behavior is unacceptable.
Expresses disappointment in the state of Alabama for allowing this situation to persist.

Actions:

for alabama residents,
Call for the mayor of Carbon Hill, Alabama, Mark Chambers, to resign (suggested)
Pressure the governor and other officials to take action to remove Mayor Chambers from office (exemplified)
</details>
<details>
<summary>
2019-06-04: Let's talk about tariffs, driving, Scooby Doo, and climate change.... (<a href="https://youtube.com/watch?v=Zg3HbiXeFjc">watch</a> || <a href="/videos/2019/06/04/Lets_talk_about_tariffs_driving_Scooby_Doo_and_climate_change">transcript &amp; editable summary</a>)

Beau breaks down the impacts of tariffs, the urgency of climate change, and the vital role of the younger generation in taking action for our planet's future.

</summary>

"We're talking about a border crisis now. Worrying about refugees now. Wait until there's a billion of them."
"Those meddling kids, when they finally get that villain, and they pull the mask off of them just like Scooby-Doo, they're gonna find out it's some old dude trying to make some money."
"It threatens the premature extinction of Earth originating intelligent life."

### AI summary (High error rate! Edit errors on video page)

Tariffs are beginning to have real impacts, leading to price increases at major retailers like Costco, Walmart, Dollar General, Family Dollar, and Dollar Tree.
Despite warnings from experts about the consequences of tariffs, many Americans allowed politicians to interpret the facts for them.
The timeline for climate change is compared to driving a car, where we must act before passing the point of no return.
By 2050, we are projected to reach a critical point in climate change, leading to the collapse of society.
Climate change is described as an existential national security threat and a risk to Earth's intelligent life.
A mere two-degree increase in temperature could displace a billion people globally.
The younger generation is emphasized as the key fighters against climate change, needing to take significant action.
Planting trees and other small measures can buy time but won't solve the climate crisis.
A World War II-style mobilization is recommended to address climate change effectively.
Individuals are urged to vote with their dollars and hold both multinational corporations and governments accountable for their environmental impact.

Actions:

for climate activists, young generation,
Mobilize for climate action like in World War II (implied)
Vote with your dollar to support environmentally responsible companies (implied)
Hold governments accountable for their impact on the environment (implied)
</details>
<details>
<summary>
2019-06-03: Let's talk about bridges, roads, and parallel power structures.... (<a href="https://youtube.com/watch?v=fBbd0Gj11SU">watch</a> || <a href="/videos/2019/06/03/Lets_talk_about_bridges_roads_and_parallel_power_structures">transcript &amp; editable summary</a>)

Beau introduces parallel power structures as a path to real freedom, using the example of building a road through community effort to showcase individual responsibility and empowerment.

</summary>

"Freedom comes with responsibility."
"You want that kind of freedom, you have to take that kind of responsibility."
"There's nothing that the government can do that you can't."
"Parallel power structures are the road to a future that has real freedom."
"It's probably time to start building bridges so you can build your road."

### AI summary (High error rate! Edit errors on video page)

Introduces the concept of parallel power structures in relation to freedom and responsibility.
Emphasizes the need for individuals to take responsibility for their freedom.
Suggests that limiting the power of the state can be achieved by creating parallel power structures.
Gives an example of building a road as a parallel power structure to showcase real freedom.
Explains how individuals can come together to build infrastructure without relying on the government.
Shares a historical example of people building a 300-mile road in a single day through community effort.
Points out that parallel power structures can lead to government change or improvements in responsiveness.
Mentions how social programs like free lunch programs may have been inspired by grassroots initiatives like the Black Panthers.
Encourages using social media and community engagement to create change and build parallel power structures.

Actions:

for community organizers, activists,
Gather volunteers to work on community projects (exemplified)
Utilize social media to mobilize support for grassroots initiatives (exemplified)
</details>
<details>
<summary>
2019-06-01: Let's talk about power and potato chips.... (<a href="https://youtube.com/watch?v=vS4RQfkDl_M">watch</a> || <a href="/videos/2019/06/01/Lets_talk_about_power_and_potato_chips">transcript &amp; editable summary</a>)

Beau questions the state's authority, urging individuals to follow conscience over blind obedience and exposing hypocrisy in societal norms.

</summary>

"If something is beyond the power of your own conscience, it should be beyond the power of the state."
"You don't have the authority to give something you don't have as individuals."
"Infringing on the freedom of others is something that has to be taken very seriously."
"Maybe it is time for this country to start following its conscience instead of simply following orders."
"You have a right to be a bigot I guess. Whatever. But does that mean that you have the power?"

### AI summary (High error rate! Edit errors on video page)

Questions the source of power and authority that the state derives from the governed.
Raises concerns about delegating authority to the state that individuals do not possess.
Draws parallels between giving authority to the state and giving something one does not have personally.
Challenges the concept of majority rule justifying violations of others' rights.
Advocates for taking infringement on others' freedom seriously and tying it to one's conscience.
Suggests that if individuals lack the authority to do something on their own, they cannot delegate it to the government.
Differentiates between a justice system based on conscience and a legal system.
Criticizes the hypocrisy in immigration debates and the treatment of asylum seekers.
Questions the morality of preventing others from seeking a better life based on arbitrary distinctions.
Encourages following one's conscience over blindly obeying authority figures.

Actions:

for citizens, activists, voters,
Follow your conscience in decision-making (implied)
Challenge unjust authority and norms (implied)
Advocate for policies based on morality and conscience (implied)
</details>

## May
<details>
<summary>
2019-05-31: Let's talk about the effects of the tariff on Mexico.... (<a href="https://youtube.com/watch?v=fjR5mi532e0">watch</a> || <a href="/videos/2019/05/31/Lets_talk_about_the_effects_of_the_tariff_on_Mexico">transcript &amp; editable summary</a>)

Trump's tariffs on Mexico will raise costs for American consumers, potentially benefiting farmers but also incentivizing illegal border crossings, undermining asylum claims, and revealing contradictions in border policies.

</summary>

"You actually end up paying for this."
"Congratulations President, you have just increased demand and incentive to cross the border illegally."
"Walls work both ways y'all need to remember that."

### AI summary (High error rate! Edit errors on video page)

Trump's tariffs on Mexico, 5% on everything, will increase the cost of fruits and veggies from Mexico, ultimately paid by the American consumer.
Adrian Wiley, a Florida man, criticized Trump's tariffs in a unique way, shedding light on the impact on consumers.
Despite the humor, the tariffs will lead restaurants and grocers to seek alternative sources for produce.
The tariffs may inadvertently benefit American farmers, creating a competitive edge and increasing demand for their products.
This increased demand will require more labor, potentially incentivizing illegal border crossings.
Trump's belief that Mexico should prevent its citizens from leaving undermines the concept of asylum and individual freedom.
Trump's push for a border wall contradicts his stance on freedom of movement, as walls work both ways.

Actions:

for american consumers and those concerned about border policies.,
Support local farmers by purchasing their produce (suggested)
Advocate for fair trade policies that benefit consumers and farmers (implied)
</details>
<details>
<summary>
2019-05-30: Let's talk about Hulk, China, rare metals, and Trump keeping his promises.... (<a href="https://youtube.com/watch?v=cx6XjvMYrWE">watch</a> || <a href="/videos/2019/05/30/Lets_talk_about_Hulk_China_rare_metals_and_Trump_keeping_his_promises">transcript &amp; editable summary</a>)

China issues a warning to the US amidst trade tensions, signaling potential economic impacts and environmental concerns, with long-lasting repercussions.

</summary>

"China has issued a catchphrase that has led to shooting wars throughout history."
"The decisions that Trump is making are going to have long, long lasting and far reaching impacts in the economy, and they're not good ones."
"We're all gonna end up paying for this."

### AI summary (High error rate! Edit errors on video page)

China has a history of issuing a catchphrase, "don't say we didn't warn you," that has led to shooting wars.
The catchphrase has been recently issued by China to the United States, but it's likely related to the trade war rather than escalating to an actual war.
China is considering cutting off or reducing exports of rare earth minerals in response to Trump's trade war.
Rare earth minerals are vital for high-tech goods and China controls about 85% of the market.
The trade deficit is not inherently bad, but Trump is using tariffs as a solution, which ultimately affects American consumers.
Trump's focus on keeping campaign promises may lead to ineffective solutions that impact the economy negatively.
The trade war won't bring back American manufacturing jobs but might shift them to other countries like Vietnam.
The decision to move jobs abroad is driven by economic factors and the cost benefits of operating in countries with less stringent environmental regulations.
Mining rare earth minerals has severe environmental impacts, as seen in China where they are currently mined.
Trump's decisions regarding the trade war will have lasting negative impacts on the economy and future presidents will likely face the consequences.

Actions:

for global citizens, policymakers,
Research the environmental impacts of mining rare earth minerals (suggested)
Advocate for environmentally responsible practices in mining (exemplified)
</details>
<details>
<summary>
2019-05-29: Let's talk about a third of migrants lying about their kids.... (<a href="https://youtube.com/watch?v=oYhj2GMahHo">watch</a> || <a href="/videos/2019/05/29/Lets_talk_about_a_third_of_migrants_lying_about_their_kids">transcript &amp; editable summary</a>)

Beau debunks the false narrative that a third of migrants lied about their children, exposing flawed statistics and calling out the harmful impact of sharing misinformation online.

</summary>

"If you wouldn't lie to stop a kid from getting raped, you're a pretty horrible person."
"This is made up and 145,000 people shared one version of this stupid story."

### AI summary (High error rate! Edit errors on video page)

Beau addresses a headline claiming that a third of migrants lied about their family status with their children, expressing concern and calling for an investigation.
He points out that people are sharing the headline without critically examining it.
Beau clarifies that the 30% mentioned in the headline was rounded up to a third, but the actual percentage was 30%.
The sample used in the study was not representative of all migrants, but specifically those suspected by ICE of lying about family relationships.
ICE's DNA testing method to verify family relationships does not account for step parents, legal guardians, or other complex family situations.
He criticizes ICE for being wrong 70% of the time in cases where they suspected migrants of lying about family relationships.
Beau questions the ethics of sharing a false narrative that demonizes migrants and perpetuates harmful stereotypes.
He underscores the dangers faced by unaccompanied minors who end up in the custody of Health and Human Services, where reports of sexual abuse are prevalent.
Beau concludes by debunking the manufactured story and criticizing the 145,000 people who shared it without verifying its accuracy.

Actions:

for online activists,
Fact-check and verify information before sharing online (implied)
Advocate for the protection of unaccompanied minors and migrant families (implied)
Combat misinformation by educating others on how to critically analyze news stories (implied)
</details>
<details>
<summary>
2019-05-28: Let's talk about Tank Man and somebody.... (<a href="https://youtube.com/watch?v=BF3NU0bgDk8">watch</a> || <a href="/videos/2019/05/28/Lets_talk_about_Tank_Man_and_somebody">transcript &amp; editable summary</a>)

Beau describes a scenario resembling Tiananmen Square, urging viewers to recognize their potential as change-makers in the face of adversity.

</summary>

"If there's ever a metaphor for the power of the state versus the individual, it's that image."
"A million people with popular support were all waiting for somebody. You are somebody."

### AI summary (High error rate! Edit errors on video page)

Describes a scenario where graduates are worried about job prospects due to a changing economy and negative media portrayal.
Mentions attacks on freedom of press, assembly, and speech, with intellectuals being demonized.
Portrays a protest that grows exponentially and faces logistical problems like food and hygiene issues.
Describes government response with tanks, armored personnel carriers, and a quarter million troops.
Points out the role of surrounding suburbs in nonviolently blocking the government from reaching the protesters.
Notes infighting among movement leadership and missed opportunities for reform.
Describes a government crackdown on protesters with tanks and rifles, resulting in casualties.
Talks about the iconic Tank Man image from Tiananmen Square, symbolizing the power struggle between the state and the individual.
Emphasizes the courage and defiance displayed by Tank Man in standing up to the tanks.
Concludes by urging viewers to recognize their own potential as change-makers in the face of adversity.

Actions:

for young activists,
Support movements for freedom and democracy (exemplified)
Stand up against government oppression (exemplified)
Be prepared to take courageous action in the face of tyranny (exemplified)
</details>
<details>
<summary>
2019-05-27: Let's talk about film cameras, social media, and political debate.... (<a href="https://youtube.com/watch?v=cVQ8xAZt2pg">watch</a> || <a href="/videos/2019/05/27/Lets_talk_about_film_cameras_social_media_and_political_debate">transcript &amp; editable summary</a>)

Beau draws parallels between old film cameras and social media filters, criticizes groupthink in political discourse, challenges rigid labels in the abortion debate, and calls for nuanced opinions beyond social media slogans.

</summary>

"It's overexposed so there's no independent thought, there's no ideas being developed in the dark room."
"Pro-life and pro-choice, right? Means nothing. Those terms mean nothing."
"We want a 30-second sound bite on the nightly news and a tweet."
"If we can't even accomplish that, you can give up putting it into practice."
"It might be time to form opinions in private before introducing them to social media."

### AI summary (High error rate! Edit errors on video page)

Draws parallels between old film cameras and social media filters, likening overexposure in both to the loss of depth and nuance.
Criticizes how social media shapes political discourse, particularly among young people who are constantly under peer scrutiny for online behavior.
Points out how groupthink and lack of independent thought harm debates and limit the development of ideas.
Gives an example of a law in Alabama that, despite its origins in bigotry, actually makes it easier for same-sex couples to marry.
Challenges the rigid labels and group mentalities in the abortion debate, illustrating how they stifle nuanced understanding and critical thinking.
Analyzes a controversial image on social media related to abortion, showcasing extreme reactions from both sides.
Raises awareness about the importance of exploring gray areas in debates and forming well-thought-out opinions beyond simplistic slogans and hashtags.

Actions:

for social media users,
Form opinions in private before sharing on social media (implied)
Challenge groupthink by engaging in nuanced debates (implied)
Encourage independent thought and depth in online discourse (implied)
</details>
<details>
<summary>
2019-05-26: Let's talk about the dos and don'ts of Memorial Day.... (<a href="https://youtube.com/watch?v=i987Ke9CzPE">watch</a> || <a href="/videos/2019/05/26/Lets_talk_about_the_dos_and_don_ts_of_Memorial_Day">transcript &amp; editable summary</a>)

A guide on navigating Memorial Day sensitively, from recognizing diverse veteran perspectives to honoring the fallen and advocating against unjust wars.

</summary>

"Memorial Day is a day to remember the fallen. It's a day to remember those lost. It's not a happy day. It is not a happy day."
"Just leave it at that. I'm sorry for your loss."
"Make sure that those who are active duty today get to celebrate Veterans Day and not get remembered on Memorial Day."
"You've got a voice, use it."
"We stop losing people, we don't need to remember."

### AI summary (High error rate! Edit errors on video page)

A veteran asked Beau to provide a do's and don'ts of Memorial Day, but Beau initially felt he wasn't the right person for it.
Veterans already familiar with Memorial Day's significance, but the general public may not be aware due to lack of discourse by politicians.
Beau acknowledges the diversity of perspectives among veterans, contrary to a singular portrayal.
Armed Forces Day, Veterans Day, and Memorial Day serve different purposes: active duty appreciation, all who served recognition, and honoring the fallen, respectively.
Memorial Day is a solemn occasion to commemorate those who have died in service.
Beau advises against saying "Happy Memorial Day" as it may not be appropriate given the solemnity of the occasion.
Younger veterans from recent conflicts may still be dealing with emotional wounds, so sensitivity is key.
Beau stresses the importance of not asking certain questions to veterans, particularly about combat experiences or killing others.
Gold Star families, who have lost a family member in service, should be acknowledged with a simple "I'm sorry for your loss" without trying to relate their loss to a cause.
Beau suggests allowing veterans to enjoy Memorial Day without feeling the need to relate to their experiences.
Some veterans believe kneeling during the National Anthem on Memorial Day is a way to honor those unjustly killed by law enforcement.
Beau encourages advocating against unjust wars and ensuring that active-duty personnel get celebrated on Veterans Day instead of being remembered on Memorial Day.

Actions:

for community members,
Acknowledge Gold Star families with a simple "I'm sorry for your loss." (implied)
Advocate against unjust wars and ensure active duty personnel are celebrated on Veterans Day. (implied)
</details>
<details>
<summary>
2019-05-25: Let's talk about baking a cake.... (<a href="https://youtube.com/watch?v=FoP7q6zrS-Q">watch</a> || <a href="/videos/2019/05/25/Lets_talk_about_baking_a_cake">transcript &amp; editable summary</a>)

Beau humorously navigates a disagreement about baking a birthday cake, turning absurd with comparisons to cake murder and Hitler.

</summary>

"Well, okay, so what you're saying then is that all the cakes that made it that had less than perfect ingredients. We should just round all of them up and throw them away too. Cake murderer."
"Hitler didn't like cakes either."
"That's not even a thing, and that's not even a cake! It will be if you take responsibility for your actions."
"You obviously didn't love it. Actually I did love it. It was strawberry cheesecake and I don't want any more cake."
"You can shove this icing where the sun doesn't shine."

### AI summary (High error rate! Edit errors on video page)

Beau introduces the scenario of being married to someone who behaves like people in the comment section, illustrating a pro-choice female married to a pro-life male dynamic.
The humor and absurdity escalate as the couple's disagreement unfolds over baking a cake for the wife's birthday.
The husband pushes for baking the cake, disregarding his wife's concerns about allergies and her lack of desire for cake.
The wife tries to end the cake debate multiple times, but the husband insists on baking it, leading to a comical argument.
The husband's persistence leads to extreme statements comparing the wife to a cake murderer and Hitler for not wanting to bake the cake.
The disagreement culminates in a ridiculous exchange about icing and the wife leaving the situation.

Actions:

for couples,
Bake a cake for a loved one's birthday (exemplified)
Share cakes with neighbors (implied)
</details>
<details>
<summary>
2019-05-24: Let's talk about how the world will always get you what you want.... (<a href="https://youtube.com/watch?v=jxc-7SE8Ev8">watch</a> || <a href="/videos/2019/05/24/Lets_talk_about_how_the_world_will_always_get_you_what_you_want">transcript &amp; editable summary</a>)

Beau explains how prohibition fuels demand for restricted items like firearms, showing why it fails without victims.

</summary>

"Prohibition doesn't work with guns. It doesn't work with drugs. It's not going to work with abortion."
"Focusing on prohibiting items like this is always a losing proposition."
"When you take something that is, in essence, a victimless crime, possessing this, there's no victim."

### AI summary (High error rate! Edit errors on video page)

Explains the rarity and unique features of a Glock 18C, a fully automatic firearm with a rapid cyclic rate of 1200 rounds per minute.
Describes the strict regulations that essentially made the Glock 18C non-existent for civilians in the United States.
Mentions the illegal conversion kits that can turn a semi-automatic Glock into a fully automatic one, which are now being tracked by the ATF.
Points out that prohibition often creates demand for restricted items, as seen with the Glock 18C conversion kits.
Argues against prohibition of certain items like firearms, drugs, and abortion, stating that it only increases their desirability without a clear victim.

Actions:

for policy makers, activists,
Monitor and advocate for sensible gun control policies (implied)
Support community programs that address root causes of violence (implied)
</details>
<details>
<summary>
2019-05-23: Let's talk about how men are from memes and women are from very long posts.... (<a href="https://youtube.com/watch?v=bSuCtoAZDcw">watch</a> || <a href="/videos/2019/05/23/Lets_talk_about_how_men_are_from_memes_and_women_are_from_very_long_posts">transcript &amp; editable summary</a>)

Beau sheds light on the influence of social media posts on real-life perceptions and warns against the limitations of sharing memes, advocating for meaningful communication over superficial interactions.

</summary>

"Your social media posts are seen by people in your real life, and it shapes their opinion of you."
"Stop using memes. It limits your thought."
"She saw something. She understood something that you didn't get."
"Stop letting other people define your thoughts like that because this is what happens."
"It's an important lesson to learn."

### AI summary (High error rate! Edit errors on video page)

Speaking in generalities, discussing the impact of social media posts on real-life perceptions.
Sharing personal experiences related to the Kavanaugh hearings and the abortion debate.
Emphasizing the importance of being mindful of the messages conveyed through social media comments.
Explaining how a seemingly harmless meme on abortion led to a relationship ending.
Pointing out that sharing memes can limit one's thought process and influence others' opinions.
Advising against using memes and encouraging open, direct discussions instead.
Warning against the implications of sharing content without fully understanding its message.
Stressing the significance of being aware of how one's online actions shape others' perceptions in real life.
Encouraging deeper understanding and communication rather than relying on superficial social media posts.
Concluding with a reminder to be thoughtful about the messages shared online.

Actions:

for social media users,
Have open, direct discussions instead of relying on memes (suggested)
Be mindful of the messages shared online and their potential impact on real-life perceptions (implied)
</details>
<details>
<summary>
2019-05-22: Let's talk about the Russian plan, undertows, and national security.... (<a href="https://youtube.com/watch?v=4x9cflMxahQ">watch</a> || <a href="/videos/2019/05/22/Lets_talk_about_the_Russian_plan_undertows_and_national_security">transcript &amp; editable summary</a>)

Beau explains a Russian plan to trigger an insurgency in the US using black Americans reveals systemic racial issues needing urgent attention, urging action against racism.

</summary>

"You want to make America great? You got to stop racism."
"No self-appraisal will ever be as valuable as your opposition's appraisal."
"Addressing racial disparities in this country is no longer just the moral thing to do. It's a national security issue."
"Countering racism is now a national security issue."
"They identified them two years ago."

### AI summary (High error rate! Edit errors on video page)

Beau starts by sharing a personal story about taking his kid to the beach and facing an undertow, using it as an analogy to explain a complex situation.
The Russian documents aren't a government plan but were created by a group of civilian contractors who pitched it to their handler and then to the government.
The plan originated from a group of intelligence professionals contracted by the Internet Research Agency (IRA) or possibly Wagner.
The plan is to trigger an insurgency within the United States using black Americans, causing racial discord.
The strategy targets a specific demographic - black Americans who make up about 12% of the population and have high incarceration rates and economic disparities.
Beau explains the importance of choosing a group with a high incarceration rate for radicalization.
By alienating and persecuting black Americans further, it becomes easier to radicalize them.
Beau stresses that the question should be why intelligence professionals chose this demographic, pointing out systemic racial issues that need addressing.
Addressing racial disparities is not just a moral issue but a national security concern as targeted groups are easier to radicalize.
Beau concludes by stating that countering racism is now a national security issue and urges for action against it.

Actions:

for policy makers, activists,
Address systemic racial issues in your community (implied)
Advocate for policies that counter racism and support marginalized communities (implied)
</details>
<details>
<summary>
2019-05-21: Let's talk about pardoning war criminals.... (<a href="https://youtube.com/watch?v=ppRdK82vjfI">watch</a> || <a href="/videos/2019/05/21/Lets_talk_about_pardoning_war_criminals">transcript &amp; editable summary</a>)

Beau addresses specific cases of war criminals, expressing doubts about pardons and advocating for professional conduct, stressing Memorial Day's significance for the fallen.

</summary>

"Memorial Day is not Veterans Day. Memorial Day is for those who were killed."
"If you want to behave like that, there's a whole bunch of places, whole bunch of locations, whole bunch of firms that you can go to and you can work like that and you'll end up at the end of the machete just like everybody else."
"As a contractor, you are supposed to be a professional, not open fire on crowds, ever, period, full stop, end of story."

### AI summary (High error rate! Edit errors on video page)

Addressing the issue of pardoning war criminals after returning from a break on Facebook.
Responding to messages about opinions on specific cases involving contractors.
Describing a situation where an Army lieutenant interrogated a suspected bomb maker and ended up killing him in a physical altercation.
Mentioning the ambiguity surrounding evidence in the case and the lieutenant's intention.
Pointing out that incidents like these, although not right, do happen.
Explaining that the lieutenant had already served his time in prison before being pardoned.
Expressing understanding towards the lieutenant's emotional reaction leading to the death.
Moving on to discussing potential pardons for a Navy chief accused of heinous acts in villages.
Emphasizing the significance of SEALs turning in the accused Navy chief as a grave indicator.
Expressing disbelief in the Navy chief's deservingness of a pardon due to his actions aiding the insurgency.
Mentioning another case involving an Army Major killing a suspected bomb maker, casting doubt on the need for a pardon.
Touching on the case of Marines urinating on opposition KIAs, condemning their actions but suggesting that they have already faced consequences.
Acknowledging the impact of the Marines' actions in aiding insurgency propaganda.
Stating that criminal justice consequences may not match the burden of living with the outcomes of their actions.
Concluding with strong disapproval of a contractor behaving unprofessionally by opening fire on crowds.
Arguing that the contractor should have faced justice under Iraqi jurisdiction rather than being brought back to the US.
Asserting that such behavior is unacceptable in a professional setting.

Actions:

for war critics and advocates,
Contact Iraqi authorities to address the contractor's actions under their jurisdiction (suggested)
Educate on the distinctions between Memorial Day and Veterans Day to honor the fallen appropriately (exemplified)
Share Beau's message to raise awareness about the implications of pardoning war criminals (implied)
</details>
<details>
<summary>
2019-05-20: Let's talk about rights, kicking down, and punching up.... (<a href="https://youtube.com/watch?v=Zx0w0k0xurU">watch</a> || <a href="/videos/2019/05/20/Lets_talk_about_rights_kicking_down_and_punching_up">transcript &amp; editable summary</a>)

Beau reacts to a message questioning his support for abortion, urging people to care about the rights of others beyond themselves and warning against the manipulation of division for power.

</summary>

"Stop caring about things that impact you directly and only those things."
"You vote each other's rights away."
"Stop kicking down. Only punch up."

### AI summary (High error rate! Edit errors on video page)

Reacts to a message questioning his support for abortion due to having six kids.
Expresses surprise and introspection at the hypocrisy pointed out by the message.
Lists various causes he supports, despite not personally embodying all of them.
Urges people to care about the rights of others beyond those that directly impact them.
Warns against falling into the trap of division created by propaganda and those in power.
Criticizes the manipulation of people's rights in the name of democracy.
Acknowledges living by far-right ideals in his personal life but opposes forcing them on others.
Advocates for freedom by not using force to restrict others based on personal beliefs.
Condemns the idea of government having ultimate control over individuals' lives as oppressive.
Calls for a shift in mindset to stop fighting amongst each other and focus on real freedom.

Actions:

for activists, advocates, citizens,
Challenge yourself to care about causes that may not directly affect you (exemplified)
Advocate for the rights of marginalized groups in your community (exemplified)
Refrain from imposing personal beliefs on others through force (exemplified)
</details>
<details>
<summary>
2019-05-19: Let's talk about how I like my coffee, a new skill, and the lessons of history.... (<a href="https://youtube.com/watch?v=t8RNlkIkvCM">watch</a> || <a href="/videos/2019/05/19/Lets_talk_about_how_I_like_my_coffee_a_new_skill_and_the_lessons_of_history">transcript &amp; editable summary</a>)

Beau talks about his coffee preferences, learning about conducting abortions, and how a new law in Alabama might actually increase access and affordability while undermining government authority.

</summary>

"I like my coffee like I like my markets."
"It's almost like prohibition doesn't work."
"Never give an order that won't be followed."

### AI summary (High error rate! Edit errors on video page)

Shares how he likes his coffee, drawing parallels with his preference for markets.
Talks about living on the Alabama line and learning how to conduct abortions out of curiosity.
Mentions the misconception around back alley abortions and how things have changed.
Points out that 97% of abortions occur before 20 weeks, mostly done with a pill.
Explains the multi-use nature of the abortion pill and its availability.
Notes the advancement in technology making abortion procedures safer.
Mentions the availability of tools for the procedure at hardware stores.
Talks about the affordability of ultrasound machines and how they enhance safety.
Suggests that the new law in Alabama might increase access to abortions and make them cheaper.
Points out the lack of a paper trail or protesters when abortions are performed at home.
Mentions how this law could undermine government authority and resemble the failure of prohibition.
Concludes by stating that leadership should not give orders that won't be followed.

Actions:

for community members,
Contact local healthcare providers for information on abortion services (suggested)
Research affordable and safe abortion options in your area (implied)
</details>
<details>
<summary>
2019-05-17: Let's talk about choice, life, and statistics.... (<a href="https://youtube.com/watch?v=jgGpFYEMiw0">watch</a> || <a href="/videos/2019/05/17/Lets_talk_about_choice_life_and_statistics">transcript &amp; editable summary</a>)

Beau debunks myths, presents statistics, and challenges the notion of bodily autonomy in the abortion debate, urging society to prioritize ethics over morals or religion.

</summary>

"97%, remember that number."
"Consent for sex is not consent to carry a child."
"Poverty is a lack of cash. It's not a lack of character."
"Bodily autonomy, it is not your body."
"When does life exist and the rights of that new life trump the rights of the woman?"

### AI summary (High error rate! Edit errors on video page)

Addresses choice, life, and statistics related to abortion, debunking false information dominating headlines.
Mentions the religious issue surrounding abortion and references a passage in Numbers from the Bible.
Talks about the misconception around heartbeats in embryos and the size of embryos at six weeks.
Emphasizes the statistic that 97% of abortions occur before 20 weeks.
Argues against viewing abortion as a consequence, stating it's more of a punishment due to legislative restrictions.
Challenges the idea that consent for sex equals consent to carry a child.
Counters the belief that women use abortion as birth control, presenting statistics on abortion frequency.
Addresses the misconception that tax dollars fund abortions, clarifying the role of the Hyde Amendment.
Links abortion rates to poverty and criticizes the lack of focus on real societal issues.
Centers the abortion debate on bodily autonomy and challenges the notion of men's rights over women's bodies.

Actions:

for advocates for reproductive rights,
Advocate for comprehensive sex education in states with limited education on the topic (implied).
Support organizations that provide resources for women facing unplanned pregnancies (implied).
Challenge misconceptions and stigma surrounding abortion through education and open dialogues (implied).
</details>
<details>
<summary>
2019-05-16: Let's talk about what car accidents, child support, and bodily autonomy.... (<a href="https://youtube.com/watch?v=jDbn0sZN0co">watch</a> || <a href="/videos/2019/05/16/Lets_talk_about_what_car_accidents_child_support_and_bodily_autonomy">transcript &amp; editable summary</a>)

Beau draws parallels between bodily autonomy and obligations, stressing the simplicity of respecting individual choices.

</summary>

"It's her body. It's really that simple."
"Guys this is what it sounds like when you say, well if I don't have a say in whether or not she keeps the baby, then I shouldn't have to pay child support."

### AI summary (High error rate! Edit errors on video page)

Driving and texting led to a four car pile up, resulting in an old man on life support.
Beau suggested to the judge that the old man be taken off life support, but the family disagreed.
Beau believes he is not obligated to pay for the medical bills in this situation.
He draws a parallel to the argument of not wanting to pay child support if one doesn't have a say in the decision to keep the baby.
Beau points out the lack of understanding of bodily autonomy in the country.
He stresses that bodily autonomy is fundamental and simple—it's her body.
Beau leaves with the message that it's just a thought and wishes everyone a good night.

Actions:

for advocates for bodily autonomy,
Respect and support bodily autonomy (implied)
</details>
<details>
<summary>
2019-05-15: Let's talk about what picking a color can teach us about gender.... (<a href="https://youtube.com/watch?v=ijooHoBoey8">watch</a> || <a href="/videos/2019/05/15/Lets_talk_about_what_picking_a_color_can_teach_us_about_gender">transcript &amp; editable summary</a>)

Beau challenges gender norms, questions societal constructs, and advocates for acceptance and understanding of diverse gender identities.

</summary>

"Gender, that's what's between your ears."
"At the end of the day, sex, well that's between your legs."
"It's just a thought."
"Throughout history, what is viewed as acceptable within the societal norms for a gender has changed."
"Gender fluid people, people that want to cross over, maybe even to the point of identifying as a different gender."

### AI summary (High error rate! Edit errors on video page)

Painting yard toys and questioning gender norms.
Challenging societal constructs of pink for girls and blue for boys.
Introduces the concept of gender neutrality and non-binary identities.
Exploring historical gender norms like colors and clothing.
Examining the fluidity of gender norms and societal expectations.
Acknowledging the existence of gender fluid individuals.
Addressing the misconception of only two genders in the U.S.
Stating that accommodating gender identity isn't a national issue.
Criticizing the stigmatization and control of gender identities.
Comparing the treatment of gender identity to mental illness and lack of accommodations.
Questioning the inconsistency in accommodating mental illnesses.
Criticizing attempts to legislate and control people's sex lives.
Distinguishing between sex and gender, asserting it's not others' business.

Actions:

for all individuals,
Embrace gender diversity by educating yourself and others (implied).
Support and advocate for gender-neutral spaces and inclusive policies (implied).
Challenge gender stereotypes in daily interactions and choices (implied).
</details>
<details>
<summary>
2019-05-15: Let's talk about Iran.... (<a href="https://youtube.com/watch?v=QqXzZKtCFGc">watch</a> || <a href="/videos/2019/05/15/Lets_talk_about_Iran">transcript &amp; editable summary</a>)

A warning against blindly supporting a potentially disastrous and manufactured war driven by political agendas rather than national interest.

</summary>

"They're not fighting for freedom. They're fighting so Trump can be more reelectable."
"Before you say that you support the troops, you have to support the truth."
"This is a manufactured war."

### AI summary (High error rate! Edit errors on video page)

A young guy, just back from boot camp, is excited about the prospect of war with Iran, despite Trump's mixed signals.
The young guy missed out on Iraq and Afghanistan and is eager to go to war.
He's underestimating the potential troop numbers needed for a conflict with Iran.
The conflict was sparked by alleged sabotaged shipping boats in the Gulf of Oman, blamed on Iran without concrete evidence.
Senator Rubio and Tom Cotton are pushing for aggressive action against Iran.
There's a discrepancy between the threat level reported by Operation Inherent Resolve and CENTCOM regarding Iran.
Acting Secretary of Defense Shanahan discussed a plan to send 100,000 to 120,000 troops to Iran with Trump.
Beau warns that a war with Iran won't be like Iraq and could lead to disastrous consequences.
Iran's military is well-prepared for insurgency and technologically advanced, posing a significant challenge for any military action.
Beau points out that the real motivation behind a potential conflict with Iran may be to boost Trump's reelection chances, not national security.
He stresses that going into Iran half-heartedly will only lead to more conflict and loss of life without solving the root issues.
Beau questions the motives behind supporting a war that benefits Saudi interests over American lives.
He calls for supporting the truth rather than blindly backing a manufactured war.

Actions:

for concerned citizens, anti-war activists.,
Contact elected officials to express opposition to military action against Iran (implied).
Support organizations advocating for diplomatic solutions and peace (implied).
</details>
<details>
<summary>
2019-05-14: Let's talk about churchgoers, witches, and fear.... (<a href="https://youtube.com/watch?v=cV9YO7E_vgo">watch</a> || <a href="/videos/2019/05/14/Lets_talk_about_churchgoers_witches_and_fear">transcript &amp; editable summary</a>)

Beau talks about how fear is used to control and divide people, urging reflection on who benefits from keeping individuals afraid and divided in society.

</summary>

"They whisper in your ear and tell you what to be afraid of."
"Panic struck, terror."
"Nobody whispered that spell into your ear."
"Keep us kicking down or punching left and right rather than punching up."
"Turn them into cowards."

### AI summary (High error rate! Edit errors on video page)

Tells an anecdote about a civics teacher whispering words to divide students into groups based on seating, leading to unfair outcomes.
The teacher labeled himself as a "witch" in the seating chart, symbolizing the government's role in instilling fear and control.
Talks about fear and false accusations, particularly related to sexual assault and terrorism.
Mentions a dark video concept addressing the Alabama law, not shared due to its nature.
Expresses how fear is manipulated by those in power to divide and control people.
Compares fear of terrorists, school shooters, illegal immigrants, and other issues with statistical likelihoods.
Points out the manipulation of fear in elections and society to maintain control and division.
Urges reflection on who benefits from keeping individuals afraid and divided in society.

Actions:

for citizens, activists,
Challenge fear-mongering narratives (implied)
Support mental health initiatives to combat fear and anxiety (implied)
Advocate for policies based on facts, not fear (implied)
</details>
<details>
<summary>
2019-05-12: Let's talk about why rapists are going to be moving to Alabama.... (<a href="https://youtube.com/watch?v=ViDLhi-QvfM">watch</a> || <a href="/videos/2019/05/12/Lets_talk_about_why_rapists_are_going_to_be_moving_to_Alabama">transcript &amp; editable summary</a>)

Beau questions Alabama's flawed legislation that could lead to innocent victims of rape being imprisoned based on false accusations.

</summary>

"A careful rapist who doesn't leave physical evidence always walk and their victim will go to prison because Dickie Drake's friend, his ex-wife, said something."
"So he gets to rape her and then put her in prison?"
"You're saying the criminal justice system is broke, but you're just making the assumption that it's only broke one way."

### AI summary (High error rate! Edit errors on video page)

Alabama's legislature is compared to the Taliban due to a bill introduced by Dicky Drake to address false accusations of sex crimes.
Filing a false report of a sex crime is already a crime in Alabama, but the penalties may be increased to 10 years in prison for the accuser if the accusation is proven false.
The bill could lead to victims of rape being imprisoned if their accusations are proven false, even though studies show that false accusations of rape are in the single digits.
Beau questions the flawed criminal justice system in Alabama and suggests that putting rape victims on trial is not the solution.
The bill may create an environment where rapists flock to Alabama, as there is a risk of innocent victims being imprisoned based on false accusations.
Beau expresses disbelief at the flawed logic behind the bill and its potential consequences on rape victims and the justice system in Alabama.

Actions:

for legislators, advocates, activists,
Advocate for comprehensive justice system reform in Alabama (implied)
Support organizations working to protect victims of sexual assault and improve reporting mechanisms (implied)
</details>
<details>
<summary>
2019-05-11: Let's talk about Ben Shapiro, getting destroyed, and something important.... (<a href="https://youtube.com/watch?v=2YmDv2GsEvA">watch</a> || <a href="/videos/2019/05/11/Lets_talk_about_Ben_Shapiro_getting_destroyed_and_something_important">transcript &amp; editable summary</a>)

Beau addresses a divisive interview incident with Ben Shapiro, stressing the need to move beyond political labels for constructive dialogues to foster real progress.

</summary>

"just say that you're on the left."
"It's all about that now, that bumper sticker politics, right, left, red and blue."
"If you actually start talking to the right-wing voters, you're going to find out that they have a lot of the same problems you have."
"It's going to be up to you."
"Anyway, it's just a thought y'all have a good night"

### AI summary (High error rate! Edit errors on video page)

Beau addresses the incident involving Ben Shapiro and Andrew Neal, focusing on a key moment during their interview.
Ben Shapiro uses terms like "radical left" to create division right from the start of the interview.
Neil points out that new ideas in the US are emerging from what is considered the left, particularly the Democrats.
Shapiro talks about the conservative intelligentsia having debates but doesn't present any new ideas himself.
When asked about abortion, Shapiro reacts defensively and accuses Neal of bias.
Shapiro dismisses Neil as being part of the "other team" and questions his motives.
Neil's main point is that pundits like Shapiro are worsening the quality of debate in American media by immediately labeling others as the enemy.
Beau notes that this divisive tactic is common on both the American right and left but more prevalent on the right.
Beau clarifies the difference between liberal, left, and Democrat, stressing that they are not interchangeable terms.
Beau challenges the notion of a true leftist party in the US and points out the lack of socialist ideologies in mainstream politics.
The core takeaway is the tendency to shut down opposing views based on political labels rather than engaging in meaningful debate.
Beau criticizes the conservative movement for lacking new ideas and focusing on maintaining the status quo.
Beau encourages bridging the gap between left and right by engaging in constructive dialogues to address shared problems.
The importance of individuals taking the initiative to drive positive change in the political landscape is emphasized.
Beau concludes by stressing the need for genuine communication and understanding between differing political ideologies for progress to be achieved.

Actions:

for political activists,
Start engaging in meaningful dialogues with individuals from different political ideologies (implied)
Foster understanding and communication by reaching out to right-wing voters to identify shared concerns (implied)
</details>
<details>
<summary>
2019-05-09: Let's talk about workshops and conferences.... (<a href="https://youtube.com/watch?v=Pj4RAw_HX1g">watch</a> || <a href="/videos/2019/05/09/Lets_talk_about_workshops_and_conferences">transcript &amp; editable summary</a>)

Beau invites his community to an inclusive and purpose-driven conference focusing on community action and networking, offering discounts and VIP upgrades for attendees who share on social media.

</summary>

"All of a sudden, I'm out of excuses not to do it, and I guess I'm going to Vegas, baby."
"If you watch the videos about the world I want or how freedom will come to America, and you're sitting there going, yeah, yeah, you need to go."
"Here you go. Y'all have a good night. Hope to see you there."

### AI summary (High error rate! Edit errors on video page)

Workshops and conferences are common among like-minded individuals to exchange ideas and tactics.
Workshops typically piggyback on activist events, making it challenging for outsiders to know about them.
Conferences are widely advertised but can be cost-prohibitive, lasting a week and held in remote locations.
Beau has been hesitant to attend conferences due to high costs and exclusivity, feeling they cater to a niche group.
A new conference in a major U.S. city with diverse speakers, affordable tickets, and a clear purpose of community action has caught Beau's interest.
The upcoming conference focuses on practical actions for community building and force multiplication.
Beau will be speaking at the conference on July 21st, encouraging those who resonate with his videos to attend.
The conference will feature speakers like G. Edward Griffin, independent journalists, and activists discussing various topics.
Attendees can use the promo code BOWE for a discount on tickets and a chance to upgrade to VIP by sharing the event on social media.
Beau invites his audience to join him at the conference and hopes it will be an eye-opening experience for many.

Actions:

for community members, activists,
Attend the community-focused conference in a major U.S. city (Suggested)
Use promo code BOWE for a ticket discount and a chance to upgrade to VIP by sharing on social media (Implied)
</details>
<details>
<summary>
2019-05-08: Let's talk about crying babies, Trump, and the poverty level.... (<a href="https://youtube.com/watch?v=c_VIOZWhR6o">watch</a> || <a href="/videos/2019/05/08/Lets_talk_about_crying_babies_Trump_and_the_poverty_level">transcript &amp; editable summary</a>)

Beau shares a personal story, criticizes the manipulation of poverty statistics, and advocates for real solutions to address financial struggles faced by Americans.

</summary>

"Babies don't stop crying when they're hungry. What are they going to do?"
"These benefit programs, entitlement programs, as people sometimes like to call them, that's just crime insurance at this point because if you get rid of them, you're going to see an uptick in crime."
"Maybe it would be better if we didn't cook the books and we actually took some steps to try to fix the fact that 80% of Americans are living paycheck to paycheck."

### AI summary (High error rate! Edit errors on video page)

Recounts a story from when he was 20 about a friend's ex showing up with four kids, only one of whom the friend knew, and leaving them with no food or supplies.
Stayed with the kids while his friend went to get baby formula, learning that a hungry baby won't stop crying no matter how much you try to soothe them.
Criticizes the Trump administration for redefining poverty without actually improving people's situations through the Chained CPI method.
Explains how Chained CPI manipulates statistics to make it seem like people are lifted out of poverty when in reality they lose benefits and struggle.
Points out that with the current economy, even a slight increase in income can disqualify individuals from receiving necessary benefits.
Shares statistics on the financial struggles of Americans, such as the inability to find good-paying jobs, pay credit card balances, or afford healthcare.
Notes that while the economy may seem great for some, the majority of Americans are living paycheck to paycheck and cannot handle emergencies.
Expresses the importance of not just focusing on the top earners but addressing the needs of the bottom percentile who significantly impact society.
Suggests that cutting benefit programs leads to an increase in crime as people resort to illegal activities to survive.
Advocates for real solutions to address the financial hardships faced by a large portion of the population instead of manipulating data for political gain.

Actions:

for concerned citizens,
Advocate for policies that genuinely address poverty and financial hardships (implied)
</details>
<details>
<summary>
2019-05-07: Let's talk about the next 11-year-old girl.... (<a href="https://youtube.com/watch?v=eLpe5mMvVSI">watch</a> || <a href="/videos/2019/05/07/Lets_talk_about_the_next_11-year-old_girl">transcript &amp; editable summary</a>)

An 11-year-old girl in Ohio, pregnant after allegedly being raped, faces the consequences of legislation removing her choice, while societal focus shifts from critical issues like testing rape kits.

</summary>

"That's the society we live in, in part because the legislatures of this country are more interested in regulating stuff like this than they are making sure that rape kits get tested."
"It's just a thought, y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

An 11-year-old girl in Ohio, allegedly raped by a 26-year-old, is pregnant after the state removed her choice through legislation.
The focus on regulating issues like abortion rather than testing rape kits contributes to societal problems.
The 11-year-old girl is at a critical developmental stage, facing emotional turmoil, peer pressure, and self-consciousness.
She may struggle with eating disorders, substance abuse, and self-harm during this period.
Laws in Ohio may prevent the perpetrator from getting parental rights if convicted of rape or sexual battery.
Poorly written laws aiming to restrict abortion may lead to unsafe practices and disproportionately affect economically disadvantaged individuals.
Beau challenges the notion that a six-week-old embryo is equivalent to a living child by presenting a stark moral dilemma.
He underscores the complex shades of gray in the world that an 11-year-old is beginning to grasp, contrasting with black-and-white legislation.
Beau questions the heavy burden placed on an 11-year-old in such circumstances.

Actions:

for legislators, activists, advocates,
Advocate for comprehensive sex education in schools to empower young individuals (suggested)
Support organizations providing resources for survivors of sexual violence (exemplified)
</details>
<details>
<summary>
2019-05-06: Let's talk about what outrage on Fox News can accomplish.... (<a href="https://youtube.com/watch?v=3CMfrhAScfY">watch</a> || <a href="/videos/2019/05/06/Lets_talk_about_what_outrage_on_Fox_News_can_accomplish">transcript &amp; editable summary</a>)

Beau addresses how outrage fuels terrorism, criticizing outlets like Fox News for inadvertently aiding radicalization by stoking fear and bigotry.

</summary>

"Being outraged is not good when you are talking about countering terrorism."
"I don't want them to win."
"The reason I talk about these things in a very clinical manner so as not to provoke outrage."
"Fox News and outlets of their ilk have done more to help that small percentage of radicalized Middle Easterners than their own propaganda networks have."
"The Islamic State, Al-Qaeda, these guys, they should have been cutting Fox News a check the entire time because they were their greatest recruitment tool."

### AI summary (High error rate! Edit errors on video page)

Addressing outrage and a comment on YouTube about discussing unpleasant happenings.
Explaining terrorism as a PR campaign with violence to provoke outrage.
Describing how outrage leads to justifying and condoning extreme actions like drone strikes and torture.
Linking overreactions to an increase in terrorism and insurgency.
Connecting foreign fighters in Iraq to the abuse of detainees, Guantanamo Bay, and Abu Ghraib.
Noting that outlets like Fox News contribute to radicalizing Middle Easterners through outrage.
Criticizing Fox News for using fear, bigotry, and ignorance to stoke outrage and ratings.
Emphasizing that being outraged plays into the hands of terrorist organizations.
Suggesting that Fox News inadvertently aids in the recruitment of terrorists.
Advocating for discussing sensitive topics in a calm and clinical manner to avoid perpetuating outrage.

Actions:

for media consumers,
Fact-check news sources and avoid outlets that sensationalize and provoke outrage (implied).
Support media outlets that report calmly and objectively on sensitive issues (implied).
</details>
<details>
<summary>
2019-05-05: Let's talk about American kids learning Arabic numerals.... (<a href="https://youtube.com/watch?v=0uGvZCg0nN4">watch</a> || <a href="/videos/2019/05/05/Lets_talk_about_American_kids_learning_Arabic_numerals">transcript &amp; editable summary</a>)

Beau questions the resistance to teaching Arabic numerals, advocating for a society that values knowledge and continuous learning.

</summary>

"The fact that we have become a nation where knowledge is looked down upon, where gaining knowledge of any kind is looked down upon, it's a bad sign."
"Education is never wasted. Knowledge is never wasted."
"So anytime this topic comes up, the answer should pretty much always be yes."
"It is how we end up with a society where 60 to 85 percent of people answer a poll they know nothing about."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Criticizes the US government for planning to teach American kids Arabic numerals.
Mentions polls on Facebook showing 60% to 85% against learning Arabic numerals.
Expresses agreement with the majority, questioning the need for Arabic numerals.
Believes that the push for teaching Arabic numerals is part of a plot and mentions "Shakira Law."
Points out that Arabic numerals are the numbers commonly used today.
Condemns basing opinions on misinformation and lack of context.
States that knowledge should not be looked down upon and advocates for continuous learning.
Argues that disregarding knowledge simply because of its Arab origin is ridiculous.
Emphasizes that education and knowledge are never wasted.
Encourages Americans to be open to learning, especially when it comes to new topics or ideas.

Actions:

for americans,
Educate yourself about the origins and significance of Arabic numerals (implied)
Challenge misconceptions and misinformation about learning new things (implied)
Embrace a culture of continuous learning and curiosity (implied)
</details>
<details>
<summary>
2019-05-04: Let's talk about subtle ways to change the behavior of others.... (<a href="https://youtube.com/watch?v=UUtgVyiEwVE">watch</a> || <a href="/videos/2019/05/04/Lets_talk_about_subtle_ways_to_change_the_behavior_of_others">transcript &amp; editable summary</a>)

Beau shares strategies to address bigotry, focusing on changing actions rather than beliefs, leveraging influence wisely, and recognizing the power individuals hold in shifting societal norms.

</summary>

"If bigots had trouble getting dates, it [bigotry] would disappear."
"Women do not understand the power they wield in this particular battle."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Recounts an encounter with a friend in South Florida who expressed bigoted and racist views, causing a moment of realization about institutional racism.
Addresses questions received about dealing with bigots in one's life, focusing on changing their actions rather than beliefs.
Advises not to waste time trying to change older bigots, as influencing younger generations has a longer-lasting impact.
Suggests exposing a racist boss to potential consequences like losing business due to public backlash.
Warns about dealing with a partner who exhibits bigotry as a defense mechanism, urging careful consideration before addressing the issue.
Encourages women to recognize the power they hold in combatting bigotry by making it socially unacceptable, thus changing behavior over time.
Advocates for challenging racist jokes by pretending not to understand them, forcing individuals to confront the racism behind the humor and potentially leading to a reduction in such behavior.

Actions:

for individuals combating bigotry,
Leave a newspaper article exposing racist behavior for a boss to see, potentially impacting their actions (suggested).
Challenge racist jokes by pretending not to understand them, forcing individuals to confront their racism (implied).
</details>
<details>
<summary>
2019-05-03: Let's talk about teens, combat vets, and suicide.... (<a href="https://youtube.com/watch?v=j74Y57R15Qs">watch</a> || <a href="/videos/2019/05/03/Lets_talk_about_teens_combat_vets_and_suicide">transcript &amp; editable summary</a>)

May's Mental Health Awareness Month: Beau shares shocking stats on teen suicide, questioning the lack of national focus and urging to value youth.

</summary>

"As Americans, we want to know, well, what's the reason this is happening? It's not a reason."
"Life is never without the means to dismiss itself."
"We have a society that is creating a suicide epidemic among teens."
"There's not going to be an easy fix to this one, guys."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

May's Mental Health Awareness Month.
Beau addresses the heavy topic of suicide.
Beau shares shocking statistics on suicide attempts by young girls.
Suicide among teens is a significant issue.
The rise in successful suicides is alarming.
Beau provides the National Suicide Hotline number.
Suicide is a prevalent issue among teens.
The lack of national focus on suicide is questioned.
Beau criticizes the lack of attention to suicide compared to school shootings.
The impact of popular media like "13 Reasons Why" on suicide is discussed.
Suicide is often a result of multiple factors, not just one reason.
Beau talks about the struggles teens face in today's society.
The importance of valuing and supporting youth is emphasized.

Actions:

for parents, educators, policymakers,
Call the National Suicide Hotline at 1-800-273-8255 or text 741-741 for immediate help (exemplified)
Take interest in the well-being of teens contemplating suicide; it can make a life-saving difference (exemplified)
</details>
<details>
<summary>
2019-05-03: Let's talk about how freedom is coming to America.... (<a href="https://youtube.com/watch?v=NowVx9S1Lgs">watch</a> || <a href="/videos/2019/05/03/Lets_talk_about_how_freedom_is_coming_to_America">transcript &amp; editable summary</a>)

A call to action for freedom through societal change, solidarity, and standing up for what's right, regardless of direct impact.

</summary>

"It's coming from the people who are standing in the streets facing off against MRAPs and tear gas over yet another unjust killing."
"It's coming from anybody who stands up for what's right, even though it doesn't directly impact them."
"Good ideas don't really require force."
"It's coming from those people within the existing power structures who are attempting to dismantle them."
"First we take DC, then we'll worry about Beijing."

### AI summary (High error rate! Edit errors on video page)

Describes a world with a higher level of freedom.
Mentions various instances where this freedom is emerging.
Talks about societal change and resistance against injustice.
Points out different acts of solidarity and unity.
Emphasizes on the importance of standing up for what's right, even if it doesn't directly affect individuals.
Encourages the building of parallel power structures.
Raises the question of individual participation in the ongoing fight for change.
Addresses the global impact of positive ideas spreading.
Concludes by wishing everyone a good night.

Actions:

for activists, community members,
Stand up against injustice by joining protests and demonstrations (exemplified).
Engage in acts of solidarity and unity with marginalized communities (suggested).
Contribute to building parallel power structures within communities (exemplified).
Educate oneself and others about ongoing social issues (implied).
</details>
<details>
<summary>
2019-05-01: Let's talk about the candidate who can beat Trump.... (<a href="https://youtube.com/watch?v=xfGxX9EXHsg">watch</a> || <a href="/videos/2019/05/01/Lets_talk_about_the_candidate_who_can_beat_Trump">transcript &amp; editable summary</a>)

The Democratic establishment should focus on policies, not stealing votes from Trump, and represent the people by supporting a platform that appeals to those who stayed home.

</summary>

"Don't co-sign evil because you don't like the other candidate."
"The moral thing to do is stay home."
"Represent the people of this country. In other words, do your job."

### AI summary (High error rate! Edit errors on video page)

Questions the Democratic establishment's focus on which candidate can beat Trump rather than policies
Criticizes the idea of running a candidate to appeal to Trump's voters, moving policies to the right
Points out that supporting a candidate who appeals to bigots is unacceptable
Encourages people who don't support Trump's policies to stay home rather than vote for them
Urges the Democratic establishment to listen to younger progressives who prioritize moral values
Notes that younger demographics tend to be more progressive, and running a pro-war candidate may lead them to stay home
Observes that Trump didn't receive a majority of votes in any age demographic until 50 years old or older
Warns against trying to steal votes from Trump by appealing to his supporters
Emphasizes the importance of supporting a candidate with a platform that appeals to eligible voters who stayed home
Calls on the Democratic establishment to represent the people and do their job

Actions:

for voters, democratic establishment,
Support a candidate with a platform that appeals to eligible voters who stayed home (suggested)
Encourage the Democratic establishment to prioritize policies over stealing votes from Trump (implied)
Represent the people by supporting candidates that truly resonate with the electorate (suggested)
</details>
<details>
<summary>
2019-05-01: Let's talk about #850Strong and Northwest Florida still waiting on congress.... (<a href="https://youtube.com/watch?v=EZ3Q4LnlOiY">watch</a> || <a href="/videos/2019/05/01/Lets_talk_about_850Strong_and_Northwest_Florida_still_waiting_on_congress">transcript &amp; editable summary</a>)

Hurricane Michael relief efforts face significant delays, especially in aiding Puerto Rico and brown Spanish-speaking communities, showcasing political negligence and a call for accountability in resource allocation.

</summary>

"And now, here we are more than 200 days later and we got nothing."
"You guys might want to get your act together."
"Y'all might be making a mistake here."
"There's a whole bunch of brown people that speak Spanish in Florida, Tambien."
"You guys might want to get your act together."

### AI summary (High error rate! Edit errors on video page)

Hurricane Michael relief efforts revisited due to lack of Congressional action after more than 200 days.
Republicans trying to shift blame onto Democrats for the lack of progress in assisting Northwest Florida.
Despite the hurricane occurring before a change in control, no significant aid has been provided.
Previous hurricanes like Katrina and Andrew received Congressional action within days to weeks.
Republicans are hesitant to allocate funds to Puerto Rico, seen as part of the U.S., leading to accusations of bigotry.
Florida contributes more to the federal government than it receives but struggles to get assistance.
The delay in aid extends to other affected regions like California wildfires and Florence victims.
Criticism towards Republicans for withholding aid from Spanish-speaking brown communities.
Reminder to voters to take note of the neglect during the next election, especially in states like Florida.
Call for accountability and efficient allocation of resources in providing necessary assistance.

Actions:

for voters, community members,
Vote in elections to hold accountable those responsible for delayed aid (implied).
</details>

## April
<details>
<summary>
2019-04-30: Let's talk about Cassandra, two videos, the Islamic state, and US Intel.... (<a href="https://youtube.com/watch?v=KDIsogkOc2k">watch</a> || <a href="/videos/2019/04/30/Lets_talk_about_Cassandra_two_videos_the_Islamic_state_and_US_Intel">transcript &amp; editable summary</a>)

Beau outlines the dangers of declaring ISIS defeated, warns of future attacks, and calls for a shift towards human intelligence over technology in counter-terrorism efforts.

</summary>

"Holding land requires resources, and when you take that away, they can go on the offensive."
"US intelligence failure, listening to signal intercepts and satellite photos."
"Technology doesn't provide intent, Baghdadi avoids electronics."
"Declaring ISIS defeated has made them the underdog again."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Cassandra Complex: feeling ignored like Cassandra, gifted with prophecy nobody listens to.
ISIS isn't defeated: holding land isn't a metric for potency, they can go on the offensive.
US intelligence failure: lack of foresight, listening to signal intercepts and satellite photos.
Technology vs. terrorism: spying tech doesn't provide intent, Baghdadi avoids electronics.
Baghdadi's intelligence: US knew he was smart, used him unknowingly to build the Islamic State.
Baghdadi's video: propaganda showing humility or giving orders? Alarming Al Qaeda style attacks expected.
Declaring ISIS defeated: dangerous move, lowers expectations, makes them the underdog.
Foreign fighters' threat: potential attacks in countries listed in video folders.
Need for human intelligence: shift focus from technology to understanding intent.

Actions:

for counter-terrorism experts,
Prioritize human intelligence over technology in counter-terrorism efforts (implied).
Stay vigilant and prepared for potential Al Qaeda style attacks in the future (implied).
</details>
<details>
<summary>
2019-04-29: Let's talk about the world I want to live in.... (<a href="https://youtube.com/watch?v=2pLb_uc0bo8">watch</a> || <a href="/videos/2019/04/29/Lets_talk_about_the_world_I_want_to_live_in">transcript &amp; editable summary</a>)

Beau envisions a harmonious society where community support, education, and innovation thrive, eliminating the need for police and promoting equality of opportunities.

</summary>

"I just want to be able to sit on my porch and drink homemade whiskey while I use my phone to order a pizza that's going to be delivered by a drone that I'm paying for with cryptocurrency..."
"In a world like this, there's no need to escape, so drug abuse of course plummets."
"Kings and queens are things that only exist on chessboards. Empires only exist in video games but free travel exists everywhere..."
"People spend more time trying to master their own life or the world around them rather than each other."
"I know sounds pretty utopian, right? But it's just a thought."

### AI summary (High error rate! Edit errors on video page)

Imagines a world where he can relax on his porch, drinking homemade whiskey, ordering pizza with cryptocurrency, delivered by a drone, funded by renewable energy.
Describes a community where an interracial gay couple introduces him to their refugee child from a natural disaster.
Envisions a society where education is a community asset, funded by bake sales and businesses voluntarily supporting a free hospital.
Portrays a system where citizens handle issues without police, focusing on restitution-based penalties for non-violent crimes and rehabilitation for violent offenses.
Paints a picture of a world without poverty, leading to reduced drug abuse and increased cooperation between communities.
Envisions inter-community trade without profit motivation, faster inventions due to no intellectual property laws, and workers embracing automation for societal advancement.
Dreams of a society where equality of opportunities eliminates bigotry, and people focus on bettering themselves and the world rather than dominating others.

Actions:

for community members,
Support community-run schools through bake sales and donations (exemplified)
Volunteer or donate to free hospitals funded by local businesses (exemplified)
Participate in neighborhood food-sharing initiatives (exemplified)
Advocate for restitution-based penalties for non-violent crimes (suggested)
Encourage rehabilitation programs for violent offenders (suggested)
Foster cooperation between communities through teleconferencing (exemplified)
Support innovation by advocating for relaxed intellectual property laws (suggested)
Embrace automation in industries for societal progress (suggested)
</details>
<details>
<summary>
2019-04-26: Let's talk about why military assistance to foreign countries is bad for the drug war.... (<a href="https://youtube.com/watch?v=n91z9-IGf7w">watch</a> || <a href="/videos/2019/04/26/Lets_talk_about_why_military_assistance_to_foreign_countries_is_bad_for_the_drug_war">transcript &amp; editable summary</a>)

Beau explains the pitfalls of US military assistance in the War on Drugs, citing unintended consequences and the inefficacy of current approaches, ultimately calling for a shift towards harm reduction and treatment.

</summary>

"The plants have won."
"We've wasted the training, we've wasted all of that money, and it has done no good."
"The war on drugs is a failure."
"It's time to maybe look at it from a different perspective."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Explains why it's detrimental for the US military to assist Central and South American countries in the War on Drugs.
Mentions the oppressive nature of some governments and their military use against people.
Describes how US military training involves learning tactics used by terrorists, leading to unintended consequences.
Talks about how expensive military training is and how individuals can profit from it in the private sector.
Illustrates how trained individuals sought after by cartels can compromise counter-narcotics efforts.
Emphasizes the blurred line between cartels and governments in some countries, leading to corruption and collaboration.
Points out that cartels operate like paramilitary organizations with intelligence and recruitment strategies.
States that the dissemination of information to cartels undermines efforts and increases the cost of the War on Drugs.
Criticizes the inefficacy of current approaches in the drug war, suggesting a shift towards harm reduction and treatment.
Concludes by reflecting on the failure of the War on Drugs and the need for a different perspective.

Actions:

for policy advocates, activists,
Advocate for harm reduction and treatment approaches in drug policy (suggested)
Support organizations working towards drug policy reform (implied)
Educate others on the failures of the War on Drugs and the need for a different approach (implied)
</details>
<details>
<summary>
2019-04-25: Let's talk about the guy on the border getting arrested and beat up.... (<a href="https://youtube.com/watch?v=ih1Mate1Uvo">watch</a> || <a href="/videos/2019/04/25/Lets_talk_about_the_guy_on_the_border_getting_arrested_and_beat_up">transcript &amp; editable summary</a>)

A militia leader gets arrested and assaulted in jail, sparking questions about gun rights, inmate culture, and freedom.

</summary>

"He's just after the benefits."
"It's never funny when an inmate gets assaulted."
"All that separates him from freedom is an imaginary line."
"Confinement can make you better or it can make you better."
"I hope that he sees that wall and actually starts to begin to understand what freedom is."

### AI summary (High error rate! Edit errors on video page)

Militia leader from the border got arrested and beat up while in confinement.
The leader allegedly led a group that snatched migrants, sometimes posing as border patrols and pointing guns.
Surprisingly, he was arrested on old weapons charges rather than for his alleged actions.
Beau questions why the leader didn't follow the process to reinstate his gun rights.
The incident in jail where he was assaulted was rumored to be due to him not adopting inmate culture.
Beau criticizes the leader for not assimilating and causing problems.
He questions why taxpayers should pay for the leader's medical bills after the assault.
Beau touches on the lack of funding and issues in the prison system.
Despite making jokes, Beau acknowledges that inmate assaults are no laughing matter.
He hopes confinement will make the leader better and help him understand freedom.

Actions:

for advocates for prison reform,
Advocate for better funding and reforms in the prison system (suggested)
Support organizations working towards improving conditions in jails and detention centers (exemplified)
</details>
<details>
<summary>
2019-04-24: Let's talk about which country is the greatest threat to American freedom.... (<a href="https://youtube.com/watch?v=rahAHcZiKdE">watch</a> || <a href="/videos/2019/04/24/Lets_talk_about_which_country_is_the_greatest_threat_to_American_freedom">transcript &amp; editable summary</a>)

Beau identifies the US as a Sue Stan, critiquing internal issues framed differently due to nationalism, urging reflection and potential regime change calls if it were another country.

</summary>

"This is the United States today, a Sue Stan, a backwards USA with a Stan added to give it a little cultural bias."
"If this was any other country, people would be calling for regime change."
"All of this is true, just in the way it's framed."
"It's very, very hard to see it for what it is."
"Anyway, it's just a thought."

### AI summary (High error rate! Edit errors on video page)

Identifies a country as the greatest threat to American freedom, rarely discussed due to economic power and oligarchy with far-right leanings.
Describes the country's two parties, Punda and Timbo, being essentially the same due to ties with ruling elites.
Talks about unprecedented corruption in the country, where elite individuals escape consequences even for serious crimes.
Mentions the powerful military of the country, including possession of chemical weapons and violating national sovereignty.
Criticizes the country's security services for arrests that often lead to minimal repercussions and lack of real justice.
Points out the indoctrination of the country's citizens by compliant media and passage of sweeping security laws like the Wazilindo laws.
Notes the excessive use of prisons for profit, leading to a high percentage of the world's prison population despite low overall population.
Raises concerns about poverty, child recruitment into the military, child marriage, and executions of children in certain regions.
Compares the country's political parties and laws to American equivalents like the Patriot Act, urging reflection on internal issues.
Concludes by implying that if the same actions were reported on another country, there might be calls for regime change.

Actions:

for american citizens,
Challenge media narratives (implied)
Advocate for systemic change (implied)
Support policies promoting justice and equality (implied)
</details>
<details>
<summary>
2019-04-23: Let's talk about Ryker Glance and warning signs.... (<a href="https://youtube.com/watch?v=jnTvWDMu960">watch</a> || <a href="/videos/2019/04/23/Lets_talk_about_Ryker_Glance_and_warning_signs">transcript &amp; editable summary</a>)

Beau stresses recognizing warning signs, urging early action in domestic abuse situations to prevent tragic outcomes.

</summary>

"The key is to get out before then. There's always a way out."
"Left unchecked, warning signs always end bad, it always ends bad."
"Get out early, get out early."
"You win every tough guy competition forever."
"It's the perfect opportunity for everybody to kind of take stock of these warning signs."

### AI summary (High error rate! Edit errors on video page)

Two-year-old Ryker Glantz survived a horrifying incident where his father shot him in the face with a shotgun after a pistol malfunctioned.
Beau stresses the importance of recognizing warning signs in domestic abuse situations, as they can escalate suddenly.
Warning signs include jealousy, possessiveness, unpredictability, bad temper, animal cruelty, verbal abuse, extreme controlling behavior, and more.
Beau urges victims to seek help from shelters and organizations and stresses the significance of getting out before situations escalate.
Financial control, limiting access to funds, accusations of infidelity, and harassment at work are also warning signs of domestic abuse.
Beau encourages everyone to be aware of these signs and act early to prevent potentially tragic outcomes.
Despite not knowing the specifics of Ryker's situation, Beau advocates for recognizing warning signs and taking action early.
Beau mentions a GoFundMe for Ryker's mom to support her in being with her son during his recovery.
Ryker's survival is described as winning "every tough guy competition forever" and serves as a reminder to pay attention to domestic abuse warning signs.
Beau concludes by encouraging viewers to take stock of the warning signs and to act early to prevent domestic abuse situations from escalating further.

Actions:

for supporters of domestic abuse victims.,
Support Ryker's mom through the GoFundMe to help her be with her son during his recovery (suggested).
Educate yourself on domestic abuse warning signs and ways to help victims (suggested).
</details>
<details>
<summary>
2019-04-22: Let's talk about the problem with young women.... (<a href="https://youtube.com/watch?v=rt7lT1Vo3xg">watch</a> || <a href="/videos/2019/04/22/Lets_talk_about_the_problem_with_young_women">transcript &amp; editable summary</a>)

Kindergarten dress code incidents to high school bathroom confrontations, women face challenges in developing confidence due to societal norms and lack of empowerment.

</summary>

"Staff should be held accountable for any distractions, not the kindergartner."
"Regulations on women's clothing are about control and enforcing traditional gender roles."
"Assertiveness is lacking in young women due to societal conditioning from a young age."
"Women facing challenges in developing confidence and self-reliance from a young age due to societal norms."
"Society's inconsistency in empowering women to stand up for themselves and develop an identity."

### AI summary (High error rate! Edit errors on video page)

Kindergartner forced to change clothes at Hugo Elementary in Minnesota over thin straps on her sundress.
Dress code meant to avoid distraction to boys, but it's kindergarten, are girls still icky?
Staff should be held accountable for any distractions, not the kindergartner.
Incident at Madison High in Houston where a parent was forced to leave for enrolling her child while wearing a Marilyn Monroe t-shirt and headscarf.
School regulations restrict women from expressing their identity or ethnic heritage.
Federal court ruling in North Carolina against making girls wear skirts when boys can wear pants.
Regulations on women's clothing are about control and enforcing traditional gender roles.
Lack of consistency in dress codes and regulations aimed at keeping women in their place.
Incident in an Alaskan high school where a girl transitioning to a boy faced harassment in the bathroom.
Boys attempted to regulate the girl's bathroom use, leading to a physical confrontation where the girl defended herself.
Assertiveness is lacking in young women due to societal conditioning from a young age.
Women are often discouraged from standing up for themselves and developing confidence and identity.
Women facing challenges in developing confidence and self-reliance from a young age due to societal norms.
Society's inconsistency in empowering women to stand up for themselves and develop an identity.
Lack of outcry or outrage over incidents where women are mistreated or silenced.

Actions:

for educators, parents, activists,
Support initiatives promoting confidence and assertiveness in young women (suggested)
Challenge dress code policies that reinforce traditional gender roles (suggested)
Encourage young women to stand up for themselves and assert their identities (implied)
</details>
<details>
<summary>
2019-04-21: Let's talk about the incident in Broward and whether there will be #JusticeForLucca (<a href="https://youtube.com/watch?v=wNgYOmWVShQ">watch</a> || <a href="/videos/2019/04/21/Lets_talk_about_the_incident_in_Broward_and_whether_there_will_be_JusticeForLucca">transcript &amp; editable summary</a>)

Deputies beating kids in Broward County reveal systemic issues beyond just excessive force, urging a focus on training and policy reform over individual justice.

</summary>

"You can maintain control of a suspect without smashing their face into the concrete."
"What it appears to me is that this department has been trained in the right way to do this."
"The officer who pepper sprayed him was attempting to do it the right way."
"It's very clear from the video."
"There's a whole lot we don't know, but that's a pretty important piece that I don't see getting discussed."

### AI summary (High error rate! Edit errors on video page)

Deputies in Broward County beat up some kids, sparking a debate on the use of force.
A key moment in the video is when an officer pepper-sprayed a kid who appeared to be picking something up off the ground.
The focus should not only be on the excessive force used by one officer but also on the actions of the officer who pepper-sprayed the kid.
The officer who pepper-sprayed the kid demonstrated deliberate and trained movements suggesting a proper technique for control without excessive force.
Beau points out that the department may have proper training procedures, but these were not followed in the incident.
The officer who pepper-sprayed the kid appeared calm and in control, contrasting with the excessive force used by another officer.
Despite the questionable actions, it is suggested that the department's policy might justify the use of force as seen in the video.
Beau stresses the importance of scrutinizing the actions of all officers involved in such incidents, not just those directly using excessive force.
There is a need to focus on changing policies rather than solely seeking justice for individual cases to prevent such incidents from happening in the future.
The situation is complex, with many unknown factors, but it is vital to analyze and address the systemic issues contributing to police brutality.

Actions:

for police reform advocates,
Advocate for policy changes within law enforcement departments to ensure proper training and accountability (exemplified)
Support initiatives that address systemic issues leading to police brutality (implied)
</details>
<details>
<summary>
2019-04-21: Let's talk about a guy named Rooster and emergency preparedness.... (<a href="https://youtube.com/watch?v=gzKSVhY-S-I">watch</a> || <a href="/videos/2019/04/21/Lets_talk_about_a_guy_named_Rooster_and_emergency_preparedness">transcript &amp; editable summary</a>)

Beau stresses the importance of emergency preparedness for everyone and provides detailed guidance on assembling an emergency bag with essentials for survival in various situations.

</summary>

"Everybody needs to know something about emergency preparedness."
"Everybody needs one. Everybody. Everybody. I can't stress this enough."
"Emergencies by their very definition you don't know they're coming so get it together anyway."

### AI summary (High error rate! Edit errors on video page)

Introduces Rooster and his involvement in emergency preparedness work with Cajun Navy and Bear Paw Tactical Medical.
Describes Rooster as a responsible individual who always stepped up during emergencies despite his lack of wound wrapping skills.
Emphasizes the importance of emergency preparedness for everyone, regardless of their background or expertise.
Acknowledges the questions he receives about survival situations and announces plans to share videos on basic survival skills.
Stresses the necessity of putting together an emergency bag containing essentials like first aid kits, medications, food, water, fire-starting materials, shelter items, and tools.
Advises against prepacked bug out bags and encourages customization based on individual needs.
Recommends practical items like canned goods, rice, and a can opener for emergency food supplies.
Suggests having a supply of bottled water, purification tablets, or filters for clean drinking water.
Lists items like flashlights, fire-starting tools, shelter materials, and knives as critical for survival.
Urges the importance of having an evacuation plan and backups in place tailored to individual family situations.

Actions:

for community members,
Prepare an emergency bag with essentials like first aid kits, medications, food, water, fire-starting materials, shelter items, and tools (suggested).
Customize your emergency bag based on your family's specific needs and situation (suggested).
Develop an evacuation plan with multiple backup options in case of emergencies (suggested).
</details>
<details>
<summary>
2019-04-20: Let's talk about why a Senator said nurses were playing cards at work.... (<a href="https://youtube.com/watch?v=K8U-1Y5WJ6I">watch</a> || <a href="/videos/2019/04/20/Lets_talk_about_why_a_Senator_said_nurses_were_playing_cards_at_work">transcript &amp; editable summary</a>)

Nurses unite against a senator's opposition to mandatory breaks, revealing potential scripting by donors in American politics.

</summary>

"Nurses are like the mafia."
"I mean because I know what to do. Anytime a politician says something dumb all you have to do is go look at their campaign contributions and you'll find out why they said it."
"I have never seen anything like this before."
"There's so much money in it that it's that easy to find out why your representative, who is supposed to be your representative, is proposing the bills and amendments that they are and why they're voting the way they are."
"I think we should really take a look at how money is affecting our legislators."

### AI summary (High error rate! Edit errors on video page)

Nurses are like the mafia in how quickly they spread news and defend each other.
A Washington state senator opposed a bill for nurses' mandatory breaks, claiming they spend time playing cards.
The senator's top campaign contributor is the Washington State Hospital Association.
The hospital association posted about unintended consequences of the bill on April 8th.
The senator proposed an amendment limiting nurses to work 8 hours in a 24-hour period with no exceptions.
The hospital association claimed no knowledge of this amendment, suggesting possible scripting.
Beau questions the coincidence and suggests the senator might be parroting hospital association interests.
American politics are criticized for being influenced by money, where politicians act in favor of their donors.
The senator's actions raise concerns about legislators being swayed by financial interests rather than representing the people.
Beau recommends examining how money impacts legislators and their decision-making processes.

Actions:

for voters, activists, nurses,
Question your representatives about their campaign contributions and how they may influence their decisions (implied)
</details>
<details>
<summary>
2019-04-20: Let's talk about spirals, the Hopi creation story, and caretakers.... (<a href="https://youtube.com/watch?v=rv7VoVV_BP8">watch</a> || <a href="/videos/2019/04/20/Lets_talk_about_spirals_the_Hopi_creation_story_and_caretakers">transcript &amp; editable summary</a>)

Beau recounts the Hopi creation story and its relevance today, reflecting on the quest for balance and center in society, both past and potentially future.

</summary>

"Every relationship, every society, every civilization is still seeking that."
"Man was seeking center, trying to find balance."
"I think of future people on a hero's journey because their world was destroyed."
"I wonder if the caretaker of tomorrow will be as hospitable."
"Given my views, it is not entirely lost on me that the migration ended in the southwestern United States."

### AI summary (High error rate! Edit errors on video page)

Describes spirals carved into cliffs and stones in the southwestern United States, representing an ancient hero's journey and quest.
Recounts the Hopi creation story heard around a campfire, detailing the emergence of the Hopi people from the earth after their world was destroyed.
Mentions the requirement placed upon the Hopi people by the caretaker to protect the earth and prevent the same destruction caused by greed.
Talks about the quest assigned to the Hopi people by the caretaker to find center and build their society there, marked by spirals in different directions.
Expresses admiration for the story as a creation myth where man has always sought balance and center, a theme relevant even today.
Draws parallels between the ancient Hopi journey and a potential future journey of people emerging from the earth into a new world.

Actions:

for history enthusiasts, culture appreciators,
Share and preserve indigenous creation stories through storytelling events (implied)
</details>
<details>
<summary>
2019-04-18: Let's talk about what Charles Darwin can teach us about the culture wars.... (<a href="https://youtube.com/watch?v=ZcQNOL2tQ4g">watch</a> || <a href="/videos/2019/04/18/Lets_talk_about_what_Charles_Darwin_can_teach_us_about_the_culture_wars">transcript &amp; editable summary</a>)

Charles Darwin's quote on adaptability applies to America's cultural war, where clinging to the past threatens progress and leads to the demise of American ideals.

</summary>

"If you are one of those fighting to return to the good old days, whenever that was, you're killing America."
"American culture will die because we're not changing."
"We're fighting battles that were decided 150 years ago."
"The culture of this country is dying."
"Those who see themselves as the defenders of the American ideal don't even know what it is anymore."

### AI summary (High error rate! Edit errors on video page)

Charles Darwin's quote on adaptability being critical for survival resonates in cultural contexts as well.
The U.S., especially the southern states, is embroiled in a cultural war between regressive and progressive factions.
Arkansas is replacing statues of a Confederate lawyer and a segregationist with desegregationist Daisy Lee Gatson Bates and musician Johnny Cash.
Beau questions the importance of preserving Confederate monuments that represent a short-lived era.
He criticizes how the U.S. clings to outdated hierarchical structures and fails to progress like the rest of the world.
The only remaining Confederate tradition in the South is poor white individuals fighting battles for rich white elites.
Beau laments that culture wars distract from addressing root issues of societal control by the wealthy.
He believes America is stagnant because it fights battles from the past instead of embracing change.
Beau warns that those yearning for a mythical past are undermining the progressive ideals America once stood for.
He expresses concern that the American culture is dying due to a lack of evolution and adaptability.

Actions:

for americans, activists, historians,
Advocate for the removal of Confederate monuments and the recognition of figures promoting progress (exemplified).
Engage in open dialogues to understand the root causes behind cultural divides and work towards unity (implied).
Support initiatives that aim to move away from divisive symbols and embrace a more inclusive cultural narrative (implied).
</details>
<details>
<summary>
2019-04-17: Let's talk about skirts, lamps, and being attracted to trans people.... (<a href="https://youtube.com/watch?v=VJ54ZqI4EDY">watch</a> || <a href="/videos/2019/04/17/Lets_talk_about_skirts_lamps_and_being_attracted_to_trans_people">transcript &amp; editable summary</a>)

Beau addresses misconceptions about sexuality, advocating for self-acceptance and rejecting harmful terminology in 2019.

</summary>

"Stop with this. That is not good terminology because it reinforces the very question you're asking."
"Just be you, live your life, and let go of all of this."
"Embrace the weird, okay? Just live your life."

### AI summary (High error rate! Edit errors on video page)

Responding to a question from a heterosexual man who was briefly attracted to a trans woman and wondered if that makes him gay.
Advocating against using terminology that can harm the transgender community.
Pointing out the absurdity of questioning one's sexuality based on brief attraction to a trans woman.
Suggesting that being overly concerned about your sexuality might be a better indication of being gay.
Emphasizing that being attracted to someone who is trans does not automatically mean you are gay.
Asserting that being gay is not a negative thing and should not be stigmatized.
Encouraging individuals to embrace who they are and live their lives without worrying about societal norms or labels.
Urging people to let go of unnecessary concerns about their sexuality and just be themselves, regardless of societal expectations.
Reminding everyone that it is 2019 and people's sexual orientation should not matter to those who truly care about them.
Encouraging individuals to embrace their uniqueness and live authentically without fear of judgment or stereotypes.

Actions:

for all individuals questioning their sexuality or facing societal pressures.,
Embrace your uniqueness and live authentically, disregarding societal norms (exemplified).
Let go of unnecessary concerns about your sexuality and just be yourself (exemplified).
</details>
<details>
<summary>
2019-04-17: Let's talk about arms transfers to Yemen and Saudi Arabia.... (<a href="https://youtube.com/watch?v=g5xEa-nOdCI">watch</a> || <a href="/videos/2019/04/17/Lets_talk_about_arms_transfers_to_Yemen_and_Saudi_Arabia">transcript &amp; editable summary</a>)

Beau delves into Yemen's crisis, dissecting U.S. arms transfer laws, revealing violations, and questioning presidential authority.

</summary>

"We're violating our own laws by doing it."
"Do we allow a president to just run roughshod over laws?"
"It was illegal then, and it's illegal now, and it needs to stop."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic of Yemen and focuses on the legal framework of U.S. arms transfers.
Explains the importance of understanding the legalities separate from Trump's veto on a resolution.
Points out the attempt by the U.S. government to establish a moral framework within arms export laws.
Describes the dire humanitarian crisis in Yemen, comparing it to one of the worst since World War II.
Talks about the indiscriminate killing of civilians, especially children dying of starvation.
Examines U.S. laws like the Arms Export Control Act, Foreign Assistance Act, Presidential Policy Directive 27, and CATP regarding arms transfers.
States that the U.S. is violating its own laws by transferring arms to Saudi Arabia for use in Yemen.
Addresses the constitutional issue of Congress's authority over making war and supplying arms in conflicts.
Raises questions about allowing a president to bypass laws and stresses the importance of upholding the nation's laws.

Actions:

for policy makers, activists,
Contact your representatives to urge them to uphold laws on arms transfers (implied)
Support organizations advocating for human rights and peace in Yemen (implied)
</details>
<details>
<summary>
2019-04-15: Let's talk about how Trump will be our first open borders President.... (<a href="https://youtube.com/watch?v=iYuFYC720No">watch</a> || <a href="/videos/2019/04/15/Lets_talk_about_how_Trump_will_be_our_first_open_borders_President">transcript &amp; editable summary</a>)

Beau warns of de facto open borders under Trump's presidency, exposing the unintentional support from conservatives, urging action to free caged individuals.

</summary>

"We're on the verge of the first open borders presidency."
"He's advocating a return to Ellis Island-style immigration."
"We have innocent people in cages."
"He's just a man-child throwing a temper tantrum."
"At the end of the day, we're going to get people out of cages and that's what matters."

### AI summary (High error rate! Edit errors on video page)

Warning against de facto open borders under Trump's presidency, despite no one openly advocating for it.
Trump's plan for processing immigrants resembles Ellis Island-style immigration.
Fox News and mayors are unknowingly supporting de facto open borders due to Trump's manipulation.
Trump's history of flip-flopping on issues like gun control and his current stance on immigration.
Urging Democrats to prioritize freeing people from cages over political games.
Suggesting that conservatives are being played by Trump into unintentionally supporting open borders.
Emphasizing the need to maintain the appearance of opposition to de facto open borders.
Speculating whether Trump is deceiving conservatives or simply acting impulsively.
Stating that the ultimate goal is to get people out of cages, regardless of political maneuvers or motivations.

Actions:

for politically engaged citizens,
Advocate for freeing individuals from cages (implied)
Stay informed and aware of political manipulations (implied)
</details>
<details>
<summary>
2019-04-15: Let's talk about 100,000 subscribers and me.... (<a href="https://youtube.com/watch?v=v8H7OdZzNH4">watch</a> || <a href="/videos/2019/04/15/Lets_talk_about_100_000_subscribers_and_me">transcript &amp; editable summary</a>)

Beau shares his journey from downplaying his accent to embracing his Southern roots, addressing misconceptions, and encouraging community engagement based on individual skills and interests.

</summary>

"I don't think that the message should be clouded because somebody did something you think is cool or something that you hate, it shouldn't matter."
"Figure out what your skill set is, I assure you it's needed. Get out there."
"I don't use that word. That word is very loaded. You say that word, people picture teenagers in leather jackets with spiky hair throwing rocks at cops."

### AI summary (High error rate! Edit errors on video page)

Started as a journalist downplaying his accent, which changed after colleagues discovered his Southern roots during a drinking session in 2016.
Made videos showcasing his accent and redneck persona, which garnered more attention and acceptance for his social commentary.
Initially played up his Southern accent but gradually reverted to his real accent, except for satire videos where he exaggerates it for humor.
Clarified his background, confirming he is from the South and has lived in various southern states.
Addressed false claims about being a convicted human trafficker, attributing them to libelous allegations from 2013.
Shared insights into his past as a consultant contractor and emphasized always being a civilian, despite misconceptions about his military involvement.
Responded to common questions about his personal life, activism, and philosophy, including clarifying his approach to conveying ideas over using specific terms.
Encouraged viewers to find ways to help their communities based on their skills and interests, without needing to adopt a militant approach.
Explained the purpose of his videos, discussing the difficulty of staying informed and avoiding scripted content in his delivery.
Mentioned creating a playlist of music he listens to and thanked his audience for their support.

Actions:

for youtube subscribers,
Reach out to various news networks like Greed Media or Pontiac Tribune based on interests (suggested)
Find ways to get involved in community activism using your unique skill set (exemplified)
</details>
<details>
<summary>
2019-04-14: Let's talk about a new icon for libertarians.... (<a href="https://youtube.com/watch?v=4HFd7130kOQ">watch</a> || <a href="/videos/2019/04/14/Lets_talk_about_a_new_icon_for_libertarians">transcript &amp; editable summary</a>)

Beau questions libertarian principles, proposing Harriet Tubman as a better symbol of freedom and urging a shift from wealth idolization to aiding others.

</summary>

"It's freedom. As much freedom as is humanly possible within the confines of the society."
"Action is more important than words."
"Why on earth do Libertarians idolize the wealthy?"
"If it's really about freedom, it would certainly seem that Harriet Tubman would be one of the ideals."
"Seems like that [giving a hand up when needed] would be more in line with the rhetoric from libertarians."

### AI summary (High error rate! Edit errors on video page)

Questions the essence of libertarianism, describing it as freedom within societal boundaries and complete deregulation.
Points out that libertarians believe legality is not equivalent to morality and value action over words.
Contrasts the belief that anyone can achieve greatness through self-improvement in an ideal society with the reality of inherited wealth among today's affluent.
Suggests Harriet Tubman as an alternative icon of freedom due to her journey from slavery to capability and action, understanding of morality, and fight against oppressive labor practices.
Questions why Harriet Tubman is not celebrated by libertarians despite embodying values like freedom and empowerment over wealth accumulation.
Encourages libertarians to shift their focus from idolizing the wealthy to guiding others towards freedom and assisting those in need, akin to Tubman's legacy.

Actions:

for libertarians,
Guide people to freedom and assist them when needed (implied)
</details>
<details>
<summary>
2019-04-12: Let's talk about Trump's threat against sanctuary cities.... (<a href="https://youtube.com/watch?v=8Z3GFgXxF30">watch</a> || <a href="/videos/2019/04/12/Lets_talk_about_Trump_s_threat_against_sanctuary_cities">transcript &amp; editable summary</a>)

Beau urges Trump to bus detained migrants into sanctuary cities to lower crime rates and challenges his courage, humorously pointing out the lack of walls around cities.

</summary>

"Do it. Bus them in. Drop them off. It's fantastic."
"Get them out of the cages, man."
"But you do understand that these cities don't have walls around them, right?"
"Stop considering it. Do it."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Trump is considering releasing detained migrants into sanctuary cities, waiting for approval from President Miller.
Beau urges Trump to stop considering and just do it, challenging his courage.
He mentions that migrants statistically create less crime than native-born Americans.
Beau questions the intention behind sorting violent migrants and the harm it may cause American citizens.
He encourages Trump to bus in and drop off migrants in sanctuary cities.
Beau expresses support for the idea, believing it will lower the crime rate and get migrants out of cages.
He humorously points out that cities don't have walls around them, alluding to the lack of segregation.
Beau concludes with a parting thought for his audience to have a good day.

Actions:

for activists, concerned citizens,
Bus detained migrants into sanctuary cities (exemplified)
Drop them off in sanctuary cities (exemplified)
</details>
<details>
<summary>
2019-04-12: Let's talk about Assange and the Future Whistleblowers of America.... (<a href="https://youtube.com/watch?v=KMKM-6Q-Esc">watch</a> || <a href="/videos/2019/04/12/Lets_talk_about_Assange_and_the_Future_Whistleblowers_of_America">transcript &amp; editable summary</a>)

Beau warns about the dangerous implications of whistleblowing and the limited options available due to lack of protection from the press, leading to potential loss of life by disclosing means and methods.

</summary>

"The U.S. government, their big brother in 1984, and like Winston said, they get you. always get you."
"You can't go to the press. The press cannot protect you."
"But see, now that they're going to end up going to a foreign power, that reporter, he doesn't have that patriotism."
"We really need you to keep this secret. If you don't, this person will die."
"We need to set aside our desire for punishment when it comes to this."

### AI summary (High error rate! Edit errors on video page)

Assange is in custody and will be prosecuted, tried, and sentenced in the federal system.
The focus should be on what future whistleblowers are learning from recent incidents involving Winter, Hammond, Karyaku, Snowden, and Manning.
Whistleblowers are left with limited options due to lack of protection by the press or anonymous leak methods.
Going to a foreign power becomes the only option for whistleblowers to disclose information.
The US press complies with the US intelligence community in protecting means and methods.
Disclosing means and methods can lead to serious consequences, including loss of life.
US media's compliance with intelligence services shifts focus from illegal activities to the leak itself.
The government's response to leaks should prioritize legal and moral behavior to prevent future incidents.
The battle between disclosure of activities like waterboarding and means and methods obtained from reports will have serious implications in counterintelligence wars.

Actions:

for whistleblowers, journalists, activists,
Support and advocate for legal protections for whistleblowers (implied)
</details>
<details>
<summary>
2019-04-11: Let's talk about getting rid of judges and President Miller.... (<a href="https://youtube.com/watch?v=obm3P-KoLCU">watch</a> || <a href="/videos/2019/04/11/Lets_talk_about_getting_rid_of_judges_and_President_Miller">transcript &amp; editable summary</a>)

President advocating for abandonment of due process, conservatives silent, warning of dire consequences without fair legal proceedings.

</summary>

"When they are done with the Mexicans from the four different Mexicos, they will come for you."
"You are next, and you won't have due process."

### AI summary (High error rate! Edit errors on video page)

President of the United States advocating for abandoning the rule of law and due process.
President Trump seems to have abdicated his role to Stephen Miller, his puppet master.
President Miller believes eliminating due process will expedite deportations.
Mass deportations historically precede the removal of due process and are followed by concentration camps.
Conservatives seem to be turning a blind eye to these dangerous actions because of team loyalty.
Due process is vital to prevent false deportations and ensure fair legal proceedings.
Beau warns that after targeting certain groups, the lack of due process may affect others next.
The importance of upholding due process as a fundamental right enshrined in the Constitution is emphasized.
Beau points out cases of American citizens mistakenly targeted for deportation without due process.
The role of judges in ensuring fair legal proceedings and preventing wrongful deportations is underscored.

Actions:

for advocates for justice,
Advocate for the preservation of due process rights (suggested)
Educate others on the importance of due process in legal proceedings (exemplified)
</details>
<details>
<summary>
2019-04-10: Let's talk about the solution to the immigration problem.... (<a href="https://youtube.com/watch?v=lVuTVrCYrIM">watch</a> || <a href="/videos/2019/04/10/Lets_talk_about_the_solution_to_the_immigration_problem">transcript &amp; editable summary</a>)

Beau challenges the perception of immigration as a problem, criticizing excuses and calling for introspection on government actions.

</summary>

"We don't have an immigration problem. We have a government regulation problem dealing with immigration."
"They're not talking about you. You're not that important."
"It's crazy to use that as a reason. It doesn't change the morality of it simply because you have some dude in a uniform do it for you."
"People in D.C. do not care about you. It's an act."
"I don't have a solution to the immigration problem because it's not a problem."

### AI summary (High error rate! Edit errors on video page)

Describes a friend named Monroe who brought joy and positivity into his life, despite facing challenges.
Ponders on the immigration problem and questions the perception of immigration as a problem.
Challenges the notion of immigration being a problem and suggests that it's more about government regulation.
Criticizes common excuses used to oppose immigration, such as not speaking English or burdening welfare systems.
Questions the fear of immigrants taking jobs and points out the illogical reasoning behind it.
Criticizes the mentality of using force to protect jobs and likens it to a gang mentality.
Calls out the government for deflecting blame onto immigrants rather than addressing internal issues.
Urges for people to realize that the government may not have their best interests at heart and encourages critical thinking.
Advocates for a shift in perspective and the need for individuals who have faced hardship to provide insight.
Concludes by stating that immigration is not the problem but rather the regulations surrounding it, promising to address solutions in a future video.

Actions:

for activists, policy makers, community members,
Challenge misconceptions about immigration (implied)
Advocate for fair government regulations on immigration (implied)
Support individuals who have faced hardship (implied)
</details>
<details>
<summary>
2019-04-09: Let's talk about the Homeland Security Secretary and what's next.... (<a href="https://youtube.com/watch?v=PJylYFS_gjI">watch</a> || <a href="/videos/2019/04/09/Lets_talk_about_the_Homeland_Security_Secretary_and_what_s_next">transcript &amp; editable summary</a>)

Former Homeland Security Secretary resigns over refusing to break the law for Trump, urging for vigilant defense against authoritarian rule and propaganda.

</summary>

"A vote for Trump is a vote for authoritarian rule."
"You have farmed off your sovereignty, farmed off your individual responsibility to some politician and you just believe whatever they say."
"There's never been a democracy that hasn't committed suicide."
"Soldiers don't go die for a flag or a song. In theory, they go fight for an ideal."
"It's blindingly apparent, but people still support him."

### AI summary (High error rate! Edit errors on video page)

Reports indicate the former Homeland Security Secretary resigned because she refused to continue breaking the law for Trump.
Trump referred to family separation as a deterrent, which Beau sees as a civil rights violation and cruel and unusual.
A new Homeland Security Secretary is needed, someone willing to betray American values and take orders from Miller.
Congress's role is to act as a check against executive power and stop potential dictatorships.
Supporting Trump is supporting authoritarian rule and going against the Constitution.
Soldiers don't fight for a flag or a song but for the ideals of the Republic and representative democracy.
Beau criticizes uninformed patriotic citizens who have fallen for propaganda and sold out their country.
He points out that working-class conservatives often vote for billionaires who don't truly represent their interests.
Beau urges those not under Trump's spell to speak up against dangerous rhetoric and propaganda.
He warns about the constant blame on immigrants and the need to call it out to prevent further deterioration.

Actions:

for citizens, activists,
Speak up against dangerous rhetoric and propaganda every time you encounter it (implied)
Call out those supporting authoritarian rule without fail (implied)
Educate yourself on political matters and history to understand the implications of supporting certain leaders (implied)
</details>
<details>
<summary>
2019-04-08: Let's talk about books to read.... (<a href="https://youtube.com/watch?v=uHMcLRjChvw">watch</a> || <a href="/videos/2019/04/08/Lets_talk_about_books_to_read">transcript &amp; editable summary</a>)

Beau suggests reading ten types of books to gain wisdom and understanding, from religious texts to dystopian futures, challenging readers to broaden their perspectives.

</summary>

"Band books are the best books."
"Reading something that is completely out of step with who you are and outside of your comfort zone can help you fill in the gaps."
"Most literature is designed to give you wisdom, not knowledge."
"Every year there's a whole bunch of books that get challenged in the United States to get removed from libraries, banned from schools, whatever."
"The idea of literature is to expose you to things that you wouldn't experience in your life."

### AI summary (High error rate! Edit errors on video page)

Addressing the common request for book recommendations, Beau suggests reading ten types of books instead of specific titles.
Beau explains that most literature is designed to provide wisdom, not just knowledge.
He recommends starting with reading a religious text that is not your own, to understand other cultures.
Beau suggests reading about countercultures to gain a different perspective on society.
He advises reading books that portray the dark aspects of the human condition, making monsters human.
Beau recommends exploring political ideologies through manifestos to understand manipulation.
He encourages revisiting a book that deeply impacted you during childhood to reconnect with that wonder.
Beau suggests reading about other people's heroes, even those you may see as enemies.
He recommends reading critical books about war to break the romanticized view often portrayed.
Beau advises reading about old stories and gods to understand the oral traditions and hidden messages.
He suggests exploring dystopian futures towards the end to tie in themes from other recommended readings.
Beau proposes reading about characters completely different from oneself to broaden perspectives.
He concludes by suggesting reading banned books as they often hold valuable content.

Actions:

for book enthusiasts,
Read a religious text that is not your own to understand different cultures (implied)
Read books about countercultures and dark aspects of human nature to gain different perspectives (implied)
Revisit a book that impacted you in childhood to rediscover that wonder (implied)
Read about other people's heroes and critical views on war to challenge romanticized notions (implied)
Read banned books to discover valuable content (implied)
</details>
<details>
<summary>
2019-04-07: Let's talk about what Thomas Jefferson would say about sanctuary cities.... (<a href="https://youtube.com/watch?v=lEalJnnkOBo">watch</a> || <a href="/videos/2019/04/07/Lets_talk_about_what_Thomas_Jefferson_would_say_about_sanctuary_cities">transcript &amp; editable summary</a>)

Beau explains the historical context behind sanctuary states and challenges distorted interpretations of history.

</summary>

"History is a thing. It exists. You can't just change it and repackage it because you want to."
"When you make an appeal to the Founding Fathers and their views on things, understand that most of that stuff was written down."
"Thomas Jefferson, well he didn't even think I should exist."

### AI summary (High error rate! Edit errors on video page)

Mentioning sanctuary states and the Supremacy Clause.
Recalling an unofficial war with France in 1798 due to unpaid debts.
Explaining the Alien Friends and Alien Enemies Act of 1798.
Referencing Thomas Jefferson's Kentucky resolutions of 1798.
Explaining Jefferson's views on federal power over naturalization and immigration.
Citing the 10th Amendment regarding states' powers.
Asserting that Jefferson supported the idea of sanctuary states.
Emphasizing the importance of historical context and accuracy.
Warning against distorting history for convenience.
Questioning appeals to the Founding Fathers' perspectives.
Beau's closing remarks on historical authenticity.

Actions:

for history enthusiasts, policymakers, activists.,
Fact-check historical claims and narratives (implied).
</details>
<details>
<summary>
2019-04-06: Let's talk about a toast to the defeat of ISIS.... (<a href="https://youtube.com/watch?v=c3fsPqMyqns">watch</a> || <a href="/videos/2019/04/06/Lets_talk_about_a_toast_to_the_defeat_of_ISIS">transcript &amp; editable summary</a>)

Beau contemplates a toast while criticizing false political statements, acknowledging ongoing threats from terrorist organizations, and expressing frustration at the current situation.

</summary>

"Terrorist organizations do not need land to operate."
"This isn't over, it's not over."
"After all of this time, you have basically put us back in the situation we ran in Afghanistan in 2002."

### AI summary (High error rate! Edit errors on video page)

Winding down his Friday night, contemplating a toast.
Questions who to toast between President Trump, Department of Defense brass, and others.
Criticizes politicians for making false statements to score political points.
Suggests toasting the veterans of future wars.
Mentions American soldiers and Kurdish soldiers killed after ISIS was declared defeated.
Acknowledges ongoing threats and attacks by terrorist organizations.
Criticizes the Department of Defense for putting the situation back to 2002 Afghanistan.
Expresses frustration and disbelief at the current state of affairs.

Actions:

for politically aware individuals.,
Support veterans and current soldiers (implied)
Stay informed about political statements and actions (implied)
</details>
<details>
<summary>
2019-04-05: Let's talk about drones, the drone program, and what's next.... (<a href="https://youtube.com/watch?v=Gxv03MXtNhw">watch</a> || <a href="/videos/2019/04/05/Lets_talk_about_drones_the_drone_program_and_what_s_next">transcript &amp; editable summary</a>)

Beau explains the flaws of the U.S. drone program: secrecy, collateral damage, and breeding insurgency, urging for transparency and responsible use.

</summary>

"They want to kill us because we've made them fear the sky."
"The drone program is not being used as surgical strikes. It's being used as an assassination tool."
"It's secret, it's ineffective, and it's breeding insurgency."
"For every opposition fighter we take out, there's the family member of somebody we killed, some innocent that we killed, ready to pick up arms."
"They want to kill us because we've made them fear the sky."

### AI summary (High error rate! Edit errors on video page)

Drones are neutral technology but can be used for good or evil purposes.
The U.S. drone program is portrayed as surgical strikes targeting command and control.
Around 90% of those killed in drone strikes were not the intended targets.
There is a possibility that some of the 90% killed were opposition forces near the target.
The drone program has hit wedding processions and killed American citizens.
Insurgency and terrorism thrive on overreactions from the U.S. government.
For every opposition fighter killed, there is a family member or innocent ready to retaliate.
The collateral rate in Afghanistan is high despite solid intelligence assets.
Parents in Pakistan have withdrawn children from school due to fear of drone strikes.
The secrecy and lack of transparency surrounding the drone program breed insurgency.
The drone program is currently used as an assassination tool rather than surgical strikes.
The lack of transparency suggests a potential increase in drone usage and collateral rates.
Beau advocates for drones to be used similarly to manned aircraft missions, except for surveillance.
The fear instilled by drone strikes, not freedom or culture, drives hostility towards the U.S.

Actions:

for policy makers, activists, citizens,
Advocate for increased transparency and oversight of the drone program (implied)
Support initiatives demanding accountability for drone strikes (implied)
Raise awareness about the consequences of drone strikes on innocent civilians (implied)
</details>
<details>
<summary>
2019-04-04: Let's talk about the advice I give my sons.... (<a href="https://youtube.com/watch?v=qxM3txDfMSg">watch</a> || <a href="/videos/2019/04/04/Lets_talk_about_the_advice_I_give_my_sons">transcript &amp; editable summary</a>)

Beau advises on embracing experiences, taking risks, valuing attitude, and shaping the world through actions and choices.

</summary>

"Embrace them all, learn from them all, experience them all."
"It's the risks I didn't take that I regret."
"Your beliefs mean absolutely nothing if you don't act on them."
"Don't let others frame your thought."
"Make sure you consider that when you're making them."

### AI summary (High error rate! Edit errors on video page)

Received questions on advice for sons on living life.
Emphasizes that there's no right way to live life and to ignore critics.
Stresses the importance of embracing both good and bad experiences.
Shares a personal anecdote about a defining canoeing adventure at 17.
Encourages having adventures while young.
Advises not to regret the things done in life but the risks avoided.
Urges to take risks as anything worthwhile involves some risk.
Warns against living a safe, predictable life within the bell curve.
Advocates for being selective with friends and valuing true friendship.
Emphasizes the significance of attitude in facing life's challenges.
Encourages continuous self-improvement even in difficult times.
Provides advice on love, advocating for following one's heart over peers' influence.
Reminds to remain curious and constantly seek knowledge to grow.
Advises developing skill sets for independence and freedom.
Encourages asking the right questions and taking actions based on beliefs.
Urges uplifting all people, avoiding putting others down for personal gain.
Reminds that individual choices shape the world and to be mindful of this impact.
Mentions being prepared for deep tests in life and how life experiences lead up to them.
The advice for his daughter mirrors the advice given for sons.

Actions:

for young adults,
Embrace both good and bad experiences (implied).
Have adventures while young (implied).
Take risks in life (implied).
Be selective with friends and value true friendship (implied).
Keep moving forward in challenging situations (implied).
Follow your heart in love and ignore peer influence (implied).
Remain curious and seek knowledge constantly (implied).
Develop skill sets for independence and freedom (implied).
Ask the right questions and act on beliefs (implied).
Avoid putting others down for personal gain (implied).
</details>
<details>
<summary>
2019-04-04: Let's talk about discussing LGBTQ figures in schools.... (<a href="https://youtube.com/watch?v=VlYjoyyNX4w">watch</a> || <a href="/videos/2019/04/04/Lets_talk_about_discussing_LGBTQ_figures_in_schools">transcript &amp; editable summary</a>)

A parent's outrage over a gay historical figure prompts Beau to challenge the discomfort with discussing LGBTQ+ history, stressing its integral role in shaping our past and present.

</summary>

"History would get really, really, really bare if we didn't talk about gay people."
"It's almost like they're here and you need to get used to it."

### AI summary (High error rate! Edit errors on video page)

A book was read in school, and a parent got outraged because the historical figure was gay.
The outrage stemmed from discomfort with discussing gay people in school.
Beau questions what will happen when children learn about uncomfortable topics like genocide or nuclear war if discussing a historical figure being gay is uncomfortable.
Beau provides examples of significant historical figures who were gay, such as Alexander the Great, Julius Caesar, Michelangelo, Sally Ride, and many others.
He argues that excluding the mention of gay people in history erases significant contributions and diminishes the richness of historical knowledge.
Beau points out that homosexuality has existed for thousands of years and doesn't need to be "normalized" as it is already a normal part of society.
He offers to have a face-to-face talk show to address the issue with those uncomfortable discussing gay historical figures.
Beau underscores the importance of recognizing the presence and contributions of gay people throughout history, urging people to accept it as a part of reality.

Actions:

for teachers, parents, students,
Educate yourself and others on LGBTQ+ history (implied)
Advocate for inclusive and diverse historical education in schools (implied)
</details>
<details>
<summary>
2019-04-03: Let's talk about the Comanche, newcomers, and avocados.... (<a href="https://youtube.com/watch?v=SYIvoz5HyC0">watch</a> || <a href="/videos/2019/04/03/Lets_talk_about_the_Comanche_newcomers_and_avocados">transcript &amp; editable summary</a>)

Beau challenges viewers to rethink American culture by reflecting on the historical influences of newcomers like the Comanche tribe and urges acceptance and growth for a better future.

</summary>

"I have a feeling that if horses and dogs can change cultures so drastically, so much, that people can too."
"I have a feeling that if horses and dogs can change cultures so drastically, so much, that people can too."
"Every group of people has that newcomer, has that thing that they need to accept for their culture to continue, to grow, otherwise it stagnates."
"The sad part is, as we talk about closing the border, Americans are more concerned about avocados than people."

### AI summary (High error rate! Edit errors on video page)

Challenges viewers to imagine a Comanche warrior instead of a helicopter.
Traces the historical connection between dogs and human culture through campfires.
Emphasizes the importance of understanding American culture as constantly evolving.
Comments on the older American culture of the Comanche tribe and their adaptation to new technologies.
Describes how horses transformed the way of life for the Comanche people.
Points out that newcomers have always influenced and enriched American culture.
Raises concerns about the current resistance towards newcomers in America, drawing parallels with past immigrant groups.
Criticizes the prioritization of avocados over people in the context of border debates.

Actions:

for americans,
Accept newcomers for cultural enrichment (implied)
Prioritize people over commodities like avocados (implied)
</details>
<details>
<summary>
2019-04-01: Let's talk about what we can learn from three Mexican countries.... (<a href="https://youtube.com/watch?v=GZ8--13oe9E">watch</a> || <a href="/videos/2019/04/01/Lets_talk_about_what_we_can_learn_from_three_Mexican_countries">transcript &amp; editable summary</a>)

Beau questions the implications of cutting aid to countries and the government's role in controlling citizens' movement, urging caution in granting too much power to politicians.

</summary>

"There are three Mexicos and Trump can't get any of them to pay for the wall."
"The power you grant to your favorite politician is going to be there when the politician you hate takes charge."
"Maybe nobody should have this kind of power."

### AI summary (High error rate! Edit errors on video page)

Fox News mistakenly referred to El Salvador, Guatemala, and Honduras as "three Mexican countries," prompting jokes and confusion.
The decision to cut aid to these countries is concerning and may lead to more refugees, according to experts.
Beau disagrees with the experts, believing that cutting aid could allow the people to take control of their countries back.
The aid was cut because the countries didn't do enough to stop refugees from leaving, reminiscent of Cold War tactics.
This decision implies that the government should prevent people from leaving during difficult times.
Beau questions whether it's right for any government to have the power to restrict its citizens' movement.
He brings up the potential dangers of granting such power to politicians, regardless of political affiliation.
Exit visas, a control measure on citizens leaving, are still present in some countries but are not common.
Beau warns about the dangers of giving politicians unchecked power over citizens' mobility.
The transcript ends with a thought-provoking message about the consequences of granting too much power to political figures.

Actions:

for conservative viewers,
Question the decisions made by politicians regarding aid and citizens' rights (implied)
Advocate for transparency and accountability in government actions (implied)
</details>

## March
<details>
<summary>
2019-03-30: Let's talk about the land of the free and the home of the brave.... (<a href="https://youtube.com/watch?v=jz6EcnjDxu8">watch</a> || <a href="/videos/2019/03/30/Lets_talk_about_the_land_of_the_free_and_the_home_of_the_brave">transcript &amp; editable summary</a>)

Secretary of Homeland Security seeks to deport unaccompanied minors, revealing deeper fears and contradictions in the "land of the free and home of the brave."

</summary>

"We want to maintain the idea that we are still the land of the free and the home of the brave."
"Your ignorance of American foreign policy and the impacts it has doesn't mean it's not really our problem, it just means you're too ignorant to know it."
"Land of the free, home of the brave."
"Fear of white people who are afraid to lose their place in this country."
"We cannot continue to call for the U.S. military to protect us from toddlers."

### AI summary (High error rate! Edit errors on video page)

Secretary of Homeland Security requested authority to deport unaccompanied minors crossing the border, transitioning from baby prisons to baby deportations.
Five-month-olds have been in legal proceedings, requiring crayons for them to draw since they can't write a statement.
Administration policies are making the journey more dangerous, like a real-life Hunger Games.
Reuniting deported children with their families poses challenges as their families might be the reason they fled.
There's a moral dilemma in sending children back to dangerous situations or taking more drastic measures.
Calls for militarization of the border to protect against children reveal underlying fears of losing dominance.
Fear of the browning of America drives some to extreme measures, like calling for military intervention against children.
The pervasive fear of losing dominance leads to irrational calls for military protection from perceived threats.
The irony of calling for military intervention against children in the "land of the free and home of the brave."
Beau shares a powerful anecdote about a Palestinian woman facing off against an IDF trooper, questioning if a similar scene will unfold on American soil.
Deporting children may prevent them from being sexually abused in custody, but it doesn't address the root problem.
Beau urges America to confront its contradictions in claiming to be the land of the free while seeking military protection from toddlers.

Actions:

for advocates for migrant rights,
Advocate for humane treatment and fair legal proceedings for unaccompanied minors (implied)
Educate others on the impacts of American foreign policy on migration (implied)
Challenge calls for militarization of borders and protection against children (implied)
</details>
<details>
<summary>
2019-03-29: Let's talk about Cardi B.... (<a href="https://youtube.com/watch?v=dcXfi5amUyE">watch</a> || <a href="/videos/2019/03/29/Lets_talk_about_Cardi_B">transcript &amp; editable summary</a>)

Beau questions victims, doubts credibility, and warns against harmful comparisons in the Cardi B controversy.

</summary>

"I guess she kind of admitted that when she was younger, didn't have a lot of options or whatever, she enticed men to come back with her because they were going to pay her for sex, and then she drugged them and robbed them."
"Not adding up to me at all."
"You're telling me that she had to drug you? You? You gotta be kidding me."
"They're property. They're not people."
"Two things aren't the same."

### AI summary (High error rate! Edit errors on video page)

Addresses the controversy surrounding Cardi B and her past actions.
Questions the victims of Cardi B's actions, implying they are partly to blame.
Expresses disbelief that Cardi B needed to drug anyone, given her appearance.
Raises doubts about the credibility of the victims and suggests they may have a history of blacking out.
Criticizes those who compare Cardi B's actions to Bill Cosby's, arguing that it dehumanizes women.
Warns against damaging relationships by making such comparisons.
Urges people to stop equating drugging and robbing with drugging and raping.
Ends with a message to have a good day.

Actions:

for onlookers, individuals, allies,
Confront harmful comparisons in your social circles (suggested)
Challenge victim-blaming narratives in discussions (implied)
</details>
<details>
<summary>
2019-03-28: Let's talk about prohibition.... (<a href="https://youtube.com/watch?v=xQTVRzcnIIU">watch</a> || <a href="/videos/2019/03/28/Lets_talk_about_prohibition">transcript &amp; editable summary</a>)

Beau explains how prohibition fails due to demand, advocating for education, culture change, and treatment to address societal issues like gun violence and drug use.

</summary>

"Prohibition doesn't work."
"To address issues like alcohol or abortion, focus on education, changing culture, and treatment."
"The drug war is ineffective; there is a market for drugs that education and treatment can address."
"Focus on treating the root causes of crime like poverty and lack of options rather than banning firearms."
"The illegal firearms market thrives due to supply and demand, showing prohibition's ineffectiveness."

### AI summary (High error rate! Edit errors on video page)

Prohibition failed because people wanted alcohol, creating a market for it despite laws.
To address issues like alcohol or abortion, focus on education, changing culture, and treatment.
The drug war is ineffective; there is a market for drugs that education and treatment can address.
Manufacturing firearms is easier than commonly thought, with designs like the Luddy being made at home.
Countries like the UK have found fully automatic weapons made at home despite stricter gun laws.
Addressing gun violence requires education on firearm use, mental health treatment, and changing toxic masculinity.
Toxic masculinity is a key factor in gun violence and needs to be addressed through cultural change.
Focus on treating the root causes of crime like poverty and lack of options rather than banning firearms.
Prohibition of firearms will not eliminate the market but drive it underground, leading to illegal firearms trade.
The illegal firearms market thrives due to supply and demand, showing prohibition's ineffectiveness.

Actions:

for advocates for social change,
Educate others on the ineffectiveness of prohibition through real-life examples (exemplified)
Support mental health initiatives to address underlying issues of gun violence (implied)
Challenge toxic masculinity by promoting healthier views of masculinity in your community (suggested)
</details>
<details>
<summary>
2019-03-26: Let's talk about whether civilian firearms can prevent tyranny.... (<a href="https://youtube.com/watch?v=dK2fH7JNRhM">watch</a> || <a href="/videos/2019/03/26/Lets_talk_about_whether_civilian_firearms_can_prevent_tyranny">transcript &amp; editable summary</a>)

Beau addresses the limitations and realities of armed revolutions, underscoring the need for societal change and the potential consequences of violence.

</summary>

"You don't fight a tank. You don't fight a drone. You go find the tank driver's family or the drone operator's family."
"Advocating for armed revolution doesn't make you tough. It's an indictment of it."
"It's too hard to teach that violence is just easier, and it is."
"At some point in the future, we may need another armed revolution. But if they do, it's because we failed them."

### AI summary (High error rate! Edit errors on video page)

Addressing the idea that Jews having guns during the Holocaust could have prevented it.
Explaining the importance of having the right skills and knowledge along with guns.
Mentioning that guns in themselves do not prevent tyranny; it's about how they are used.
Stating that untrained individuals are ineffective in combat and are likely to perish.
Pointing out the discrepancy between people's romanticized views of insurgency and the harsh reality.
Describing the strategy of insurgency to make the government overreact and clamp down on civil liberties.
Emphasizing that armed revolution is a last resort and a reflection of societal failure.
Concluding with a reminder of the gravity and horror of armed revolutions.

Actions:

for society members,
Educate others on the complex realities and consequences of armed revolutions (suggested).
Advocate for peaceful societal change through education and awareness (implied).
</details>
<details>
<summary>
2019-03-25: Let's talk about examples of masculinity.... (<a href="https://youtube.com/watch?v=UHZ9VYJARL4">watch</a> || <a href="/videos/2019/03/25/Lets_talk_about_examples_of_masculinity">transcript &amp; editable summary</a>)

Young men question masculinity without a checklist, Beau shares diverse role models, defining masculinity as a journey of self-improvement and personal definition.

</summary>

"Masculinity isn't a trait, it's a journey."
"Real masculinity likes a challenge."
"Masculinity is something you're going to define yourself."

### AI summary (High error rate! Edit errors on video page)

Young men are questioning masculinity and its definition in the absence of a checklist or skill set.
Masculinity is individualized and not determined by others.
Beau shares three masculine role models: Teddy Roosevelt, Sheriff Andy Taylor, and John F. Kennedy, each showcasing different aspects of masculinity.
Teddy Roosevelt exemplified American masculinity through courage, individualism, and advocacy for the downtrodden.
Sheriff Andy Taylor humanized conflict resolution and was active in his community without carrying a gun.
John F. Kennedy demonstrated principles, uplifted the oppressed, and had a vision for positive change.
Beau defines masculinity as not using aggression to subjugate others but to uplift and improve oneself.
Real masculinity embraces challenges and focuses on personal growth rather than dominating others.
Masculinity is a journey of self-improvement and defining what matters to oneself.
It ultimately boils down to an individual's definition, irrespective of others' opinions.

Actions:

for men, individuals exploring masculinity,
Define masculinity for yourself (exemplified)
Focus on self-improvement and uplifting others (implied)
Embrace challenges and strive for personal growth (implied)
</details>
<details>
<summary>
2019-03-24: Let's talk about the assault weapons ban.... (<a href="https://youtube.com/watch?v=sIIK--yFdoA">watch</a> || <a href="/videos/2019/03/24/Lets_talk_about_the_assault_weapons_ban">transcript &amp; editable summary</a>)

Beau questions the effectiveness of an assault weapons ban, pointing out the deeper issues surrounding gun violence and individual responsibility.

</summary>

"It's not going to do any good because in the firearms vocabulary, there's no such thing as an assault weapon."
"Glorify violence for everything, every little infraction, become something worthy of death."
"It's a tool with one purpose, to kill. That's what a firearm is for."

### AI summary (High error rate! Edit errors on video page)

Lists multiple school shootings under the assault weapons ban to illustrate the stakes.
Points out the ineffectiveness of the assault weapons ban, citing that it didn't work previously.
Argues that there's no such thing as an "assault weapon" in firearms vocabulary, labeling it a political term.
Mentions that popular rifles like the AR-15 will still be present even with an assault weapons ban.
Raises concerns that banning certain weapons could lead to the use of more powerful firearms like the .30-06.
Notes that the U.S. has more guns than people, making it a unique case in terms of gun ownership.
Shifts the focus from firearms to the individuals using them, discussing the importance of secure weapon storage.
Criticizes the glorification of violence and the lack of emphasis on conflict resolution and coping skills.
Shares a personal story of learning to shoot at a young age, stressing the importance of understanding firearms as tools for a specific purpose.
Concludes with a thought on the consequences of actions and presentations in society.

Actions:

for gun policy advocates,
Secure your firearms to prevent unauthorized access (implied)
Teach conflict resolution and coping skills alongside firearm safety (implied)
</details>
<details>
<summary>
2019-03-23: Let's talk about the fingerprints of history.... (<a href="https://youtube.com/watch?v=89hi45P4Z6k">watch</a> || <a href="/videos/2019/03/23/Lets_talk_about_the_fingerprints_of_history">transcript &amp; editable summary</a>)

Beau examines how history books are shaped by the fingerprints of history and how future generations will judge us based on our actions and inactions today.

</summary>

"They're going to wonder why we didn't act for the environment."
"We spend a lot of time being entertained by stuff that is mindless."
"We know that a lot of the stuff going on around us is wrong. But we're not doing much to solve it."
"Our fingerprints will be there for them to look at and the society we leave them."
"Society today is often busy but accomplishing little, confusing motion with progress."

### AI summary (High error rate! Edit errors on video page)

History books are created from the fingerprints of history, like letters, photographs, and government records.
Historians shape history books to appeal to readers, often judging the past by today's standards.
People of the future will judge us based on the standards of tomorrow, wondering why we didn't act on issues like the environment or political systems.
Digital records today will shape how future generations view us, similar to how Thomas Jefferson is viewed for his actions and inactions.
Society today is often busy but accomplishing little, confusing motion with progress.
Our actions today will leave behind fingerprints for future generations to judge us by.

Actions:

for history enthusiasts, future generations,
Document your actions for future generations to learn from (implied)
Actively work towards solving societal issues (implied)
</details>
<details>
<summary>
2019-03-22: Let's talk about the training incident in Indiana..... (<a href="https://youtube.com/watch?v=eeAvhhppe6I">watch</a> || <a href="/videos/2019/03/22/Lets_talk_about_the_training_incident_in_Indiana">transcript &amp; editable summary</a>)

Beau criticizes the mock execution of teachers during an active shooter drill in Indiana, questioning the effectiveness and ethics of such training and advocating for a more proactive approach to school safety.

</summary>

"I wonder where the school shooters get the idea from us, from us, because violence is easy."
"Just kill them, two in the chest, one in the head. Blood makes the grass grow green."
"If we keep this posture, we can't be surprised when there are more small caskets because of an event at a school."

### AI summary (High error rate! Edit errors on video page)

Explains an incident in Indiana where teachers were mock executed during an active shooter drill.
Clarifies that the incident was not part of the ALICE training program and expresses doubt that it was authorized by them.
Criticizes the lack of effective training programs for active shooter situations and the mentality needed for such training.
Describes how teachers were taken into a room, lined up against a wall, and shot in the back with a high-powered airsoft gun as part of the drill.
Questions the effectiveness of using fear and pain in training teachers to respond to active shooter situations.
Raises concerns about whether teachers were aware they could fight back during the drill.
Criticizes the reactive nature of the training and suggests a more proactive approach involving psychologists to identify warning signs.
Condemns the use of violence as a solution and points out the potential impact on students' mental health and well-being.
Urges for a reevaluation of current training methods for teachers in dealing with school shootings.

Actions:

for teachers, educators, school administrators,
Advocate for comprehensive and trauma-informed training programs for teachers (implied)
Support initiatives that focus on proactive measures such as identifying warning signs of potential violence (implied)
</details>
<details>
<summary>
2019-03-22: Let's talk about a critique of the reparations video.... (<a href="https://youtube.com/watch?v=lmJY61rbsqI">watch</a> || <a href="/videos/2019/03/22/Lets_talk_about_a_critique_of_the_reparations_video">transcript &amp; editable summary</a>)

Beau engages in a thoughtful response to a reparations video, proposing a refundable tax credit over tax exemption as a more feasible and beneficial approach to reparations, fostering unexpected allies through tax credits.

</summary>

"A Nazi wants to talk, let's talk."
"Rather than tax exemption, what about a tax credit, a refundable tax credit more importantly?"
"Taxation is theft, man."
"You will find people that would march against you in the street supporting this just based on the idea that it's cutting down on government extortion."
"This is something that if with the right amount of organizing could be done pretty quickly."

### AI summary (High error rate! Edit errors on video page)

Beau was watching YouTube videos while feeling under the weather and stumbled upon a video critiquing reparations, prompting him to respond.
The video he watched critiqued the reparations proposal, focusing on the $100 billion amount mentioned, which Beau believes is not sufficient given the true value of slavery.
Beau appreciates the video's approach of sparking a constructive conversation and wants to contribute to the discourse.
He suggests a tweak to the proposed tax exemption for reparations, advocating for a refundable tax credit instead to benefit those at the bottom economically.
Beau sees tax credits as more politically feasible than direct dollar reparations funds, as it could garner support from unexpected allies who oppose taxes.
Implementing tax credits for reparations is seen as more achievable and effective compared to other reparations plans, making it a practical and timely solution.
Beau values the importance of turning criticisms into productive dialogues, which is the essence of his channel.
He concludes by encouraging further reflection and engagement on the topic of reparations.

Actions:

for tax reform advocates,
Advocate for implementing refundable tax credits for reparations (suggested)
Organize grassroots movements around the idea of tax credits for reparations (implied)
</details>
<details>
<summary>
2019-03-19: Let's talk about why people believe conspiracy theories (and debunk one).... (<a href="https://youtube.com/watch?v=86d-fgSK_oo">watch</a> || <a href="/videos/2019/03/19/Lets_talk_about_why_people_believe_conspiracy_theories_and_debunk_one">transcript &amp; editable summary</a>)

Beau debunks a conspiracy theory surrounding the New Zealand shooting, illustrating why people grasp onto such narratives for comfort and control in a chaotic world.

</summary>

"If you don't believe that, the alternative is that somebody really did walk in and murder 50 people."
"There is no solace believing that the CIA controls the world. It's not really true."
"The world is chaotic. Bad things happen. Murders happen. That's what it's about."
"The world's a scary place, and without somebody pulling the strings, it's downright terrifying."
"It's either they don't want to admit that they've elected this guy, or they need something to believe in."

### AI summary (High error rate! Edit errors on video page)

Expresses his interest in conspiracy theories as great thought exercises.
Refutes the conspiracy theory about the New Zealand shooting being staged by the CIA.
Points out the flaws in the theory, such as the CIA's unlikely involvement and the lack of evidence.
Explains the physics of gunshot wounds, debunking misconceptions about bullet impact.
Mentions the lack of blood in the conspiracy theory and questions its validity.
Conducts a demonstration to debunk claims about bullet impact on a windshield.
Debunks the theory using evidence from the video, like the presence of a magazine on the floor.
Criticizes the theory for its lack of credibility and reliance on false evidence.
Explains why people may believe in conspiracy theories for comfort and a sense of control.
Links the belief in the conspiracy theory to political motivations, particularly among Donald Trump supporters.

Actions:

for skeptics, truth-seekers,
Fact-check conspiracy theories (implied)
Challenge misinformation online (implied)
</details>
<details>
<summary>
2019-03-18: Let's talk about pirates and emperors.... (<a href="https://youtube.com/watch?v=JonuekkDeGM">watch</a> || <a href="/videos/2019/03/18/Lets_talk_about_pirates_and_emperors">transcript &amp; editable summary</a>)

Beau addresses the blurred lines between legality and morality, drawing parallels between different actions and urging a critical examination of power dynamics.

</summary>

"The technological advance doesn't somehow make it more moral. Just makes it legal."
"Everything the Taliban did, well that was legal under their laws."
"There's no difference in real life. It's the same thing."
"Size doesn't matter. At least not in this case."
"It's power."

### AI summary (High error rate! Edit errors on video page)

Addresses questions about terrorism and non-state actors.
Talks about a terrorist action committed by the government.
Emphasizes the importance of definitions in understanding propaganda.
References St. Augustine's story about Alexander the Great and a pirate to make a point about morality and legality.
Compares street gangs collecting protection money to taxation.
Draws parallels between a suicide bomber and a drone operator in terms of morality.
Argues that legality does not equate to morality.
Mentions historical instances where atrocities were legal under respective laws.
Stresses the role of power in determining what is legal and not morality or social consensus.
Encourages watching a related video to further understand the topic.

Actions:

for ethical thinkers, justice advocates.,
Watch the suggested video to deepen understanding (suggested).
</details>
<details>
<summary>
2019-03-17: Let's talk about terrorism and propaganda.... (<a href="https://youtube.com/watch?v=CMRuMaRUq8Q">watch</a> || <a href="/videos/2019/03/17/Lets_talk_about_terrorism_and_propaganda">transcript &amp; editable summary</a>)

Beau explains terrorism as a strategy to provoke overreactions, urges keeping terrorist groups unrelatable to combat radicalization and violence.

</summary>

"Hang them from lamp posts. Nobody cares, they're fascists."
"You've got to keep that wall up. It's the most important thing you, as an average citizen, can do to combat terrorism."
"It's extremely important that you do this. Because if this group becomes relatable to the average person, people start to radicalize."
"The founding fathers were terrorists, textbook."
"Point out the flaws in the ideology and make sure they stay unrelatable."

### AI summary (High error rate! Edit errors on video page)

Defines terrorism as a strategy to provoke an overreaction from the establishment.
Explains how terrorists aim to make themselves relatable to gain support.
Talks about fascists being hard to offend or make relatable.
Analyzes a manifesto's brilliance in making the author relatable through nods to different groups.
Mentions the ideological journey from left to right and doubts its authenticity.
Points out the existence of a libertarian to fascist pipeline.
Urges libertarians to address and clean out radical elements within their group.
Emphasizes the importance of not getting defensive when discussing these issues.
Stresses the need to keep terrorist groups marginalized and unrelatable.
Connects the importance of keeping terrorists unrelatable to preventing radicalization and violence.
Mentions the founding fathers as historical examples of terrorists.
Suggests that pointing out flaws in terrorist ideologies and keeping them unrelatable is critical in combating terrorism.

Actions:

for average citizens, libertarians.,
Address and clean out radical elements within libertarian groups (implied).
Keep terrorist groups marginalized and unrelatable to prevent radicalization and violence (implied).
Point out flaws in terrorist ideologies to combat terrorism (implied).
</details>
<details>
<summary>
2019-03-16: Let's talk about the dark side of freedom of speech.... (<a href="https://youtube.com/watch?v=d5pJOE-nzFU">watch</a> || <a href="/videos/2019/03/16/Lets_talk_about_the_dark_side_of_freedom_of_speech">transcript &amp; editable summary</a>)

Beau stresses the importance of speaking out against dangerous rhetoric to prevent ethnic cleansing in the United States, warning that history's rhymes are already echoing.

</summary>

"It can happen here."
"You have an obligation to speak over them."
"Genocide. That's how it ends."
"History doesn't repeat, but it rhymes."
"The only thing that's going to change it is you."

### AI summary (High error rate! Edit errors on video page)

Mentioned ethnic cleansing in a recent video, sparking reactions in the comment section.
People are talking about ethnic cleansing but using more subtle rhetoric.
Individuals have the right to express bigoted views, but it's your obligation to speak out against it.
There is a dangerous belief that atrocities only happen in countries with brown people.
Gives an example from the 1990s where mass rape and murder occurred in a well-developed country.
Warns that ethnic cleansing can happen in the United States and has already shown its face through mass shootings.
Talks about the dangerous propaganda tactics used by certain groups to manipulate well-meaning individuals.
Stresses the importance of waking up to the reality of the situation to prevent further atrocities.
Ethnic cleansing doesn't always mean mass murder but can involve making certain groups uncomfortable or forcing them to leave.
Emphasizes that history doesn't repeat but rhymes, indicating current dangerous trends.

Actions:

for activists, advocates, community members,
Speak out against bigotry and dangerous rhetoric (implied)
Educate others about the dangers of subtle forms of ethnic cleansing (implied)
Advocate for inclusive and diverse communities (implied)
Support organizations working to prevent hate crimes and discrimination (implied)
</details>
<details>
<summary>
2019-03-15: Let's talk about preserving a culture.... (<a href="https://youtube.com/watch?v=JCmSbWPKWrI">watch</a> || <a href="/videos/2019/03/15/Lets_talk_about_preserving_a_culture">transcript &amp; editable summary</a>)

Beau questions the concept of white culture, dismantles the link between culture and skin tone, and advocates for preserving meaningful cultures beyond physical characteristics.

</summary>

"Culture is not a thing."
"Culture and skin tone are not linked."
"Even once that happens, I'd be willing to bet that those people in the Northeastern United States have a very different culture than those people in Central Africa."
"The blending of races is an inevitability."
"If the culture becomes solely about skin tone, why should it remain?"

### AI summary (High error rate! Edit errors on video page)

Exploring the concept of white culture and its perceived destruction.
Questions the existence of a unified white culture compared to specific ethnic cultures.
Shares a personal story about a Creek man who identifies as white due to ancestry.
Emphasizes that culture and skin tone are not inherently linked.
Talks about his family's Danish ancestry and how culture transcends physical appearance.
Predicts a future where racial blending leads to a homogenized appearance.
Argues that linking culture solely to skin tone ensures its destruction.
Advocates for preserving meaningful cultures beyond just physical characteristics.

Actions:

for cultural enthusiasts, anti-racism advocates,
Challenge stereotypes and misconceptions about culture (suggested)
Encourage cultural exchange and understanding in communities (implied)
</details>
<details>
<summary>
2019-03-14: Let's talk about giving everyone free education.... (<a href="https://youtube.com/watch?v=rHuXHJPxeZs">watch</a> || <a href="/videos/2019/03/14/Lets_talk_about_giving_everyone_free_education">transcript &amp; editable summary</a>)

Beau criticizes the current education system's focus on credentialing over true education, urging individuals to take action in revolutionizing education themselves.

</summary>

"Education's easy. Right now, you've got access to the entire base of humanity's knowledge."
"It's not about education, it's about status, it's about class, it's about a way of dividing us normal folk from our betters."
"The government could have done this a long time ago, this training, the CBTs and this technology has been around for a very long time."
"If the government isn't gonna do it, we just can't have it then I guess you do it."
"You want a revolution? Start one."

### AI summary (High error rate! Edit errors on video page)

Critiques the current education system for prioritizing credentialing over actual education.
Mentions a bribery scandal where rich individuals paid to get their kids into elite schools, questioning the purpose of education in that scenario.
Talks about the university system and its lack of overhaul despite advancements in technology.
Mentions computer-based training (CBTs) in the military and how it simplifies the learning process.
Points out that the government has no interest in breaking down class barriers or providing free education.
Encourages individuals or groups to set up alternative education platforms in states with lax regulations.
Suggests creating online certifications and vocational training programs funded through ads.
Emphasizes the need for individuals to take action in revolutionizing education since the government won't prioritize it.

Actions:

for education reformists,
Set up alternative education platforms in states with lax regulations on educational institutions (suggested)
Create online certifications and vocational training programs funded through ads (suggested)
</details>
<details>
<summary>
2019-03-13: Let's talk about how I learned to love MAGA hats.... (<a href="https://youtube.com/watch?v=2kqqTHs397Y">watch</a> || <a href="/videos/2019/03/13/Lets_talk_about_how_I_learned_to_love_MAGA_hats">transcript &amp; editable summary</a>)

Beau talks about his changed view on MAGA hats and shares eye-opening experiences of a Trump supporter facing microaggressions and negative treatment in a Trump-supporting area, suggesting using the hat as a teaching tool for understanding societal dynamics.

</summary>

"Love it. It is a fantastic teaching tool."
"By the way, those harassment free zones you're looking for, those are safe spaces."
"I'm like, okay, do tell."
"Those stares, those sneers, those are the micro aggressions."
"There is a whole lot to unpack there."

### AI summary (High error rate! Edit errors on video page)

Talks about his changed view on MAGA hats after a revealing interaction.
Mentions his intention to buy a MAGA hat for the video but couldn't find one.
Raises the topic of an app to help MAGA supporters find harassment-free places.
Shares a story of a Trump supporter facing negative reactions and treatment in a Trump-supporting area.
Describes microaggressions faced by the Trump supporter, including stares and sneers.
Recounts instances of verbal harassment directed at the Trump supporter for wearing a MAGA hat.
Shares an incident where the manager of a bar asked the Trump supporter to speak more friendly about Trump.
Mentions a situation in a store where the Trump supporter was served but told not to wear the hat again.
Suggests that these negative experiences have made the Trump supporter hesitant to wear the hat.
Draws parallels between the treatment of the Trump supporter and other marginalized groups.
Encourages using the MAGA hat as a teaching tool to understand societal dynamics.
Points out that the younger generation might learn empathy through experiencing such treatment.
Mentions safe spaces as places free from harassment.
Concludes with a thought for the viewers to ponder.

Actions:

for community members,
Have open and empathetic dialogues with individuals facing discrimination (implied).
Encourage introspection and empathy in younger generations towards marginalized groups (implied).
Create safe spaces free from harassment in communities (implied).
</details>
<details>
<summary>
2019-03-12: Let's talk about the dangers of being moderate.... (<a href="https://youtube.com/watch?v=CGeKdAWnnxc">watch</a> || <a href="/videos/2019/03/12/Lets_talk_about_the_dangers_of_being_moderate">transcript &amp; editable summary</a>)

Moderates moving to the right enable extreme positions, endangering society by compromising on fundamental morals and ethics.

</summary>

"You're talking about murdering people. The moderate position of today? Well, can we just kill some of them? Trying to find that middle ground. It's dangerous."
"Moderates in this country need to get their act together."
"Are we a nation of ethnic cleansers? Are we a nation that enables child sexual abuse? Is that what we are? Is that moderate?"
"Call me crazy. Maybe I'm an extremist."
"What's the next compromise?"

### AI summary (High error rate! Edit errors on video page)

Moderates are dangerous and could be the downfall of the country.
The Democratic Party is not truly leftist but slightly right, particularly evident in their foreign policy.
Moderates trying to find compromise can enable extreme positions.
Moderates are moving dangerously to the right, tolerating positions unthinkable in the past.
Democrats are catering to far-right administrations and moving right to chase moderate votes.
The moderate position has shifted significantly over time.
Moderates are swayed by finding a middle ground between extreme positions, moving the country further right.
Child sexual abuse in immigration camps is a result of the moderate position.
Far-right extremists are taking advantage of moderates not researching or fact-checking.
Moderates need to re-evaluate their morals and beliefs to prevent further dangerous compromises.

Actions:

for moderates, activists, voters,
Re-evaluate your morals and beliefs on society's ethics (implied).
Research and fact-check claims instead of blindly seeking a middle ground (implied).
</details>
<details>
<summary>
2019-03-12: Let's talk about that new liberal plan.... (<a href="https://youtube.com/watch?v=qUZiVPYTxYY">watch</a> || <a href="/videos/2019/03/12/Lets_talk_about_that_new_liberal_plan">transcript &amp; editable summary</a>)

Beau critically examines a new liberal plan, challenges historical myths, and contrasts the values of the greatest generation with the modern American conservative ideology.

</summary>

"Living with a myth."
"Nothing like the American conservative today."
"Fearless is what made the greatest generation great."

### AI summary (High error rate! Edit errors on video page)

Analyzing a new liberal plan with various components, including cutting military pay by 15% and creating employment opportunities for conservation work.
The plan includes funding for renewable energy, railway projects, and paying artists for artwork to decorate public buildings.
There is a focus on employee rights, strengthening unions, and the establishment of entitlement programs for different groups.
A significant aspect of the plan is the proposal for a living wage for all American workers.
Beau criticizes the economic impact of the plan and instead focuses on its historical context and implications.
He challenges the myth that all World War II veterans were conservatives and dissects the reality of the greatest generation's values.
The transcript delves into issues of bigotry, refugee support, and the caring nature of the greatest generation during hard times.
Beau contrasts the values of the greatest generation with the modern American conservative ideology, pointing out discrepancies in their beliefs and actions.
He argues that American history is complex, with flawed leaders and mistakes, challenging idealized notions of historical figures.
The transcript concludes by reiterating the fearlessness and compassion of the greatest generation and contrasting it with the attitudes of the modern American conservative.

Actions:

for american citizens,
Challenge historical myths about political ideologies (implied)
Advocate for employee rights and a living wage (implied)
Support conservation efforts and renewable energy initiatives (implied)
</details>
<details>
<summary>
2019-03-10: Let's talk about questions from teens and moral injury.... (<a href="https://youtube.com/watch?v=2_OX5v9z5MA">watch</a> || <a href="/videos/2019/03/10/Lets_talk_about_questions_from_teens_and_moral_injury">transcript &amp; editable summary</a>)

Teens face anger and apathy but can combat moral injury by starting small actions, finding their role in activism, and holding onto motivations.

</summary>

"Get in the fight for 28 days. That's all it takes."
"Don't go all militant right away. Figure out what you're good at."
"The most dangerous people in the world are true believers."
"Hold onto your motivation, it's going to be the reason you make it through 28 days."
"True believers are all ideologues."

### AI summary (High error rate! Edit errors on video page)

Teens are angry and apathetic but know they should get involved in societal issues.
They face a sense of hopelessness and futility, making it difficult to take action.
Beau warns against looking up to him as a role-model.
Beau introduces the concept of moral injury, often associated with war zones.
Moral injury stems from failing to prevent or witnessing transgressions of morals.
It can result from witnessing everyday injustices like racism or environmental degradation.
Similar to PTSD, moral injury can lead to suicide, demoralization, and self-harm.
Ways to cope include belief in a just world view and building self-esteem through small daily actions.
Beau advises starting with small actions to combat issues causing moral injury, like sitting with a kid wearing a hijab.
He stresses the importance of developing skills gradually in activism and finding where you are most useful.
Effective activists share a common motivation that breaks through demoralization and self-handicapping behavior.
Motivations of activists can stem from various sources like monetary addiction, religion, ideology, or ego.
Beau encourages holding onto motivations as a reason to persist in activism.
True believers who are ideologues are the most dangerous but also the most committed in the fight for justice.

Actions:

for teens and young activists,
Start small actions to combat issues causing moral injury (suggested)
Sit with a kid that faces discrimination at school (suggested)
Use social media to raise awareness about societal issues (suggested)
Find where you are most useful in activism (suggested)
Hold onto your motivations as a reason to persist in activism (suggested)
</details>
<details>
<summary>
2019-03-09: Let's talk about the feelings of HHS and questions about the staff.... (<a href="https://youtube.com/watch?v=CK2wZLdNIIg">watch</a> || <a href="/videos/2019/03/09/Lets_talk_about_the_feelings_of_HHS_and_questions_about_the_staff">transcript &amp; editable summary</a>)

Congress investigates migrant children abuse, Health and Human Services prioritizes feelings over accountability, Beau calls for action and accountability.

</summary>

"It doesn't matter if your feelings are hurt."
"Which one of your staff should replace the person responsible for this decision?"
"Probably don't deserve to be in the position you're in."
"We had a job to do. So do you."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Congress investigates sexual abuse reports from migrant children in U.S. custody.
A congressperson misspoke about responsibility for abuse, hurting Health and Human Services' feelings.
Health and Human Services clarifies it was not all sexual abuse but also misconduct against children.
Beau stresses that regardless of contractors, the responsibility lies with Health and Human Services.
Health and Human Services demands an apology for the congressperson's misspeaking.
Beau questions which staff member waived background checks, recommended shelters, and oversaw children's welfare.
He questions who placed children at the mercy of others and made the decision to hinder investigations.
Beau compares the situation to journalists' work despite criticism, pointing out the duty to act.
He concludes by suggesting that staff hindering investigations should be replaced.
Beau delivers a critical message on accountability and prioritizing the safety of children in care.

Actions:

for congressional oversight committees,
Replace staff hindering investigations (suggested)
Prioritize child safety over personal feelings (exemplified)
Demand accountability from Health and Human Services (implied)
</details>
<details>
<summary>
2019-03-09: Let's talk about being armed and black.... (<a href="https://youtube.com/watch?v=zL_IX8yX_JU">watch</a> || <a href="/videos/2019/03/09/Lets_talk_about_being_armed_and_black">transcript &amp; editable summary</a>)

Beau recounts a tense encounter at a shooting range, offering advice for black gun clubs on engaging with law enforcement and challenging stereotypes to survive potentially lethal encounters.

</summary>

"What if I had been black? You wouldn't be hearing this story right now."
"If they tell you to put your hands on the roof of the car the second you get pulled over, do it."
"They'll kill you. They will kill you."

### AI summary (High error rate! Edit errors on video page)

Recounts a tense encounter at a shooting range with a rookie cop checking guns.
Describes how the situation was de-escalated by a sergeant using humor.
Raises the question of how the situation might have played out differently if he were black.
Offers advice for black gun clubs on engaging with law enforcement and setting rules of engagement.
Emphasizes the importance of complying with law enforcement to avoid escalation.
Advises against carrying certain types of firearms to avoid reinforcing stereotypes.
Suggests diversifying attire to challenge stereotypes about armed black individuals.
Recommends setting an example of compliant behavior during encounters with law enforcement.
Addresses questions about standardizing equipment in community defense scenarios.
Explains the historical significance and popularity of the 1911 pistol among white gun enthusiasts.
Urges the inclusion of a first aid kit in the car for emergencies.
Points out the heightened danger and need for caution for black individuals exercising their gun rights.

Actions:

for black gun club members,
Organize an event with top law enforcement officials to establish rules of engagement (suggested)
Comply with law enforcement instructions during encounters (implied)
Include a diverse range of attire during events to challenge stereotypes (exemplified)
</details>
<details>
<summary>
2019-03-08: Let's talk mistakes, human nature, unicorns, and what we're leaving our children.... (<a href="https://youtube.com/watch?v=iFj6f7ooW-w">watch</a> || <a href="/videos/2019/03/08/Lets_talk_mistakes_human_nature_unicorns_and_what_we_re_leaving_our_children">transcript &amp; editable summary</a>)

Society's resistance to change, corrupt political systems, and the need for individuals to pave the way towards a better future, as Beau advocates for a stateless society based on cooperation rather than competition.

</summary>

"We embrace a system that encourages the worst instincts humanity has to offer."
"People always say, think globally and act globally."
"Simply, we plant the seeds today for trees that we will never sit under."

### AI summary (High error rate! Edit errors on video page)

Society takes a long time to learn from mistakes and tends to double down without questioning.
The war on drugs has resulted in wasted lives, resources, and money, with plants ultimately winning.
Society embraces a system that encourages greed and exploitation.
Political leaders often participate in corruption, rising through ranks to Washington D.C.
Rare "unicorns" who genuinely care about people get corrupted in the political system.
Beau rejects the current political system and believes in people living together without borders or government.
He plants seeds for a future society he won't see, aiming for a stateless society doing no harm.
Beau criticizes societal conditioning that discourages thinking globally and acting locally.
Youth today are either angry and rebellious or apathetic due to the bleak future laid out for them.
Technology offers immense potential to solve global issues, but society clings to outdated systems.

Actions:

for activists, reformers, individuals,
Plant seeds for a future society based on cooperation and doing no harm (suggested)
Encourage critical thinking and acting locally to bring about change (suggested)
Advocate for a society without borders or government, focused on people living together (implied)
</details>
<details>
<summary>
2019-03-07: Let's talk about Omar, pro-Israeli lobbying, and nuance.... (<a href="https://youtube.com/watch?v=L7UdEb8V8bM">watch</a> || <a href="/videos/2019/03/07/Lets_talk_about_Omar_pro-Israeli_lobbying_and_nuance">transcript &amp; editable summary</a>)

Be accurate with your words when discussing Israel's policies and influence to avoid anti-Semitism accusations and acknowledge American Jews' diverse perspectives.

</summary>

"You have that obligation."
"Jews did not bomb Gaza. Israel did."
"American Jews who speak out against Israel. Wow."

### AI summary (High error rate! Edit errors on video page)

Talks about Omar's tweet, Israeli lobbying, campaign contributions, and anti-Semitism.
Mentions the importance of using OpenSecrets.org to track campaign contributions and lobbying money.
Points out that Omar's tweet about APEC donating to congresspeople was factually inaccurate.
Clarifies that AIPAC doesn't give campaign contributions but lobbies with a significant amount of money.
Emphasizes the influence of pro-Israeli groups in Congress through campaign contributions.
Questions why Democrats didn't support Omar, suggesting their ties to pro-Israeli groups.
Explains how Israeli lobbying groups strategically spread their money around to influence both parties.
Stresses the importance of discussing Israel's policies without coming off as anti-Semitic.
Advises being precise with terminology when discussing Israel and its policies.
Mentions the challenges faced by American Jews who speak out against Israel.

Actions:

for advocates and activists.,
Contact your politicians to demand transparency on campaign contributions and lobbying influences (implied).
Support American Jews who speak out against Israeli policies by amplifying their voices and stories (implied).
</details>
<details>
<summary>
2019-03-06: Let's talk about my mullet and Hoover, Alabama.... (<a href="https://youtube.com/watch?v=Ulf3hTtoNOw">watch</a> || <a href="/videos/2019/03/06/Lets_talk_about_my_mullet_and_Hoover_Alabama">transcript &amp; editable summary</a>)

Beau addresses the rise in racism triggered by societal shifts, pointing out Hoover, Alabama's history of racial issues and urging young people to challenge outdated community ideologies.

</summary>

"It's a fad, triggered because we had a black president and a bunch of white people felt like they were losing their place in the world."
"This uptick in racism is a fad, triggered because we had a black president and a bunch of white people felt like they were losing their place in the world."
"Society, they know that their elders in their community aren't always right. These kids know that. And they should have been able to discern what's right and wrong."
"It's the community's fault, but it's gonna be these kids that pay."

### AI summary (High error rate! Edit errors on video page)

Recalls a photo from his past where he was skipping school with older kids in Stewart County, Tennessee.
Mentions witnessing young men and women in Hoover, Alabama fondly discussing ethnic cleansing on the internet.
Draws attention to a series of racist incidents in Hoover, Alabama, including a girl using racial slurs and a teacher's inappropriate behavior.
Describes a case in Hoover where the police mistakenly shot the wrong person, noting a history of racial issues in the area.
Points out that Hoover, Alabama is named after a figure associated with white nationalist movements and a synagogue bombing suspect.
Attributes the recent rise in racism to a reaction against having a black president and white people feeling threatened.
Compares the current surge in racism to a passing trend, like a mullet haircut, suggesting it will eventually fade.
Expresses concern for the young individuals in Hoover who may face consequences for their community's racist views, instilled by older generations.
Notes the danger of nationalism fostering unfounded pride and hatred towards others.
Encourages young people to be critical of outdated ideologies perpetuated by elders in their community.

Actions:

for community members, youth,
Address outdated ideologies in your community by initiating open dialogues and challenging discriminatory beliefs (implied).
Educate yourself and others on the harmful impacts of nationalism and racism to foster a more inclusive society (implied).
</details>
<details>
<summary>
2019-03-05: Let's talk about the unlisted penalty behind every law.... (<a href="https://youtube.com/watch?v=ws3w2EqpioE">watch</a> || <a href="/videos/2019/03/05/Lets_talk_about_the_unlisted_penalty_behind_every_law">transcript &amp; editable summary</a>)

Beau explains how even seemingly trivial laws can escalate to violence, raising questions about the justification of laws that could lead to death.

</summary>

"Every law, no matter how trivial, is backed up by penalty of death."
"At the end of the road, whatever law that is being suggested, they will kill somebody over."
"Arrest is a violent act."

### AI summary (High error rate! Edit errors on video page)

Laws, no matter how trivial, are backed by the threat of death.
Death is rarely listed as a penalty for not complying with the law.
Using jaywalking as an example, Beau illustrates the escalation of consequences for not obeying the law.
Non-violent actions can lead to violent arrests by law enforcement.
The use of force continuum can ultimately lead to death.
Beau questions the justification of laws that could result in someone's death.
He points out that many deaths involve seemingly trivial offenses.
The power of government violence is a real and concerning issue.
The potential for fatal outcomes exists for any law enforcement interaction.
Beau expresses reluctance to support laws that could lead to lethal consequences.

Actions:

for policy advocates,
Question the justification of laws that could lead to lethal outcomes (suggested)
Advocate for police reform and de-escalation training (implied)
</details>
<details>
<summary>
2019-03-04: Let's talk about reparations.... (<a href="https://youtube.com/watch?v=v5tsHSrG-_4">watch</a> || <a href="/videos/2019/03/04/Lets_talk_about_reparations">transcript &amp; editable summary</a>)

Reparations aim to address generational wealth disparities rooted in slavery, proposing a $100 billion trust for low-interest loans to empower individuals economically.

</summary>

"Everybody in the United States has personally benefited from the institution of slavery."
"Reparations are not about seeking justice for past wrongs but about remedying the lasting impacts of historical injustices."
"It's not a handout by any means. It's a loan. It's a loan and it's going to solve a problem."

### AI summary (High error rate! Edit errors on video page)

Reparations are a topic that impacts hundreds of millions of people and must be discussed in general terms.
The wealth and foundation of the United States were built on slavery, benefiting all individuals currently residing in the country.
Generational wealth, passed down through families, plays a significant role in economic opportunities and success.
Reparations are not about taxing specific groups but should come from the general government fund.
Direct financial disbursements as reparations may not address the root issues of generational wealth disparity.
One proposed solution involves creating a trust with $100 billion for low-interest loans to help individuals start businesses or access education.
This approach aims to empower individuals to control their economic destiny and address the ongoing effects of slavery.
Reparations are not about seeking justice for past wrongs but about remedying the lasting impacts of historical injustices.
Delayed justice for slavery can never fully be achieved, but reparations can help alleviate some of its effects.
Reparations, in the form of loans, aim to empower individuals and address economic disparities.

Actions:

for americans,
Create and support trusts for low-interest loans to empower marginalized communities (suggested)
Advocate for reparations initiatives that address economic disparities (implied)
</details>
<details>
<summary>
2019-03-03: Let's talk about "not racist" people and Cherokee Princesses.... (<a href="https://youtube.com/watch?v=inKPdS7l1PQ">watch</a> || <a href="/videos/2019/03/03/Lets_talk_about_not_racist_people_and_Cherokee_Princesses">transcript &amp; editable summary</a>)

Beau explains the importance of actively opposing racism and changing behavior to combat societal prejudices, offering tactics for confronting and challenging hidden racism.

</summary>

"To change society you have to change the way people act."
"It's time for those people who are not racist to start being anti-racist."
"You don't change their heart, you don't change their mind, you don't change the way they think, but you change the way they act."
"It's time for those people who are not racist to actively oppose racism."
"Most effective thing I've ever seen is when the racist dad becomes the grandpa to a mixed kid."

### AI summary (High error rate! Edit errors on video page)

Sharing a message received about being banned for using racist slurs on social media.
Explaining his stance on banning racist slurs on his platform.
Emphasizing the need to change societal behavior to combat racism.
Encouraging people to move from not being racist to actively being anti-racist.
Debunking the myth of having a Cherokee princess in one's family tree.
Providing a historical tidbit about the mistranslation of Cherokee princesses.
Suggesting a tactic to confront hidden racism through jokes by pretending not to understand them.
Stating that confronting hidden racism can change behavior, even if not beliefs.
Advocating for changing the behavior of racist family members by not tolerating their behavior.
Mentioning a powerful example of changing a racist individual's mindset through becoming a grandparent to a mixed-race child.

Actions:

for social activists, anti-racism advocates,
Challenge hidden racism by pretending not to understand jokes that perpetuate harmful stereotypes (implied)
Change the behavior of racist family members by not tolerating their racist comments or jokes (implied)
Actively oppose racism by standing up against discriminatory behavior in social circles (implied)
</details>
<details>
<summary>
2019-03-02: Let's talk about drafting women and men's rights activists.... (<a href="https://youtube.com/watch?v=NHO8eBkeykc">watch</a> || <a href="/videos/2019/03/02/Lets_talk_about_drafting_women_and_men_s_rights_activists">transcript &amp; editable summary</a>)

Men's rights activists challenge the draft's discrimination but risk harming innocents in their strategy, while advocating for equality in freedom.

</summary>

"You seek equality in freedom."
"You've taken hostages in an attempt to get what you want. You're willing to hurt the innocent, those that aren't complicit in it, to get what you want."
"If you want to take a stand, take a stand. Don't take hostages."

### AI summary (High error rate! Edit errors on video page)

Men's rights activists are viewed negatively by society and the activist community for reasons like not wanting to pay child support or using their children as leverage.
An MRA group in Texas filed a lawsuit claiming that the draft was discriminatory and won in federal court.
The Supreme Court used the argument of women not being allowed in combat as a reason to disallow them from the draft, a ruling from the 80s.
MRAs believe that if women were subject to the draft, they'd be more vocal in ending it, leading to its sooner end.
Beau argues that the draft is immoral, using government violence to force people to fight is wrong, and not a modern-age responsibility.
Beau points out the inconsistency of opposing the draft while advocating to draft women to fight, exposing them to government violence.
He stresses the importance of ideological consistency in building a movement and seeking equality in freedom, not oppression.
Beau criticizes the strategy employed by MRAs as akin to seeking equality in oppression, contrasting it with seeking freedom.
Despite the controversy around the draft, a military commission is likely to advise the Department of Defense to end the Selective Service.
Beau concludes by condemning the approach of taking hostages to achieve goals, urging for a stand without resorting to harming the innocent.

Actions:

for activists, advocates, critics,
Advocate for ideological consistency within movements (implied)
Stand against harmful tactics and advocate for equality without resorting to violence or harm (implied)
</details>
<details>
<summary>
2019-03-01: Let's talk about what we can learn from Momo (<a href="https://youtube.com/watch?v=S7L6wCzscDg">watch</a> || <a href="/videos/2019/03/01/Lets_talk_about_what_we_can_learn_from_Momo">transcript &amp; editable summary</a>)

Momo meme panic reveals lack of parental involvement, urging focus on real teen struggles over fictional scares.

</summary>

"We don't really spend enough time with our kids."
"We sell hours of our life every day to some faceless entity at the expense of our families."
"Maybe Momo is a good thing. Maybe like most good stories, it has a moral."
"Become a little bit more involved if you can."
"So maybe Momo is a good thing. Maybe like most good stories, it has a moral."

### AI summary (High error rate! Edit errors on video page)

Momo, a meme causing panic, is actually a joke known to kids for a year and a half.
Children understand it's a joke and find parents' reactions hilarious.
The fear around Momo reveals a lack of parental involvement in children's lives.
Parents often use technology as a babysitter, unaware of what's on their kids' phones.
Modern society values work over family time, leading to disconnection.
Beau suggests that Momo's scare might have helped people focus more on their kids.
He points out statistics on teen struggles like suicide attempts, drug use, and pregnancies.
These real-life issues are much scarier than the Momo meme.
Beau implies that the moral of the Momo story is to be more involved with our children.
He encourages viewers to prioritize spending time with their kids.

Actions:

for parents, caregivers, families,
Spend quality time with your kids regularly (suggested)
Monitor your children's online activities (suggested)
</details>

## February
<details>
<summary>
2019-02-27: Let's talk about fake hate crimes.... (<a href="https://youtube.com/watch?v=Bs4GfJYGnBA">watch</a> || <a href="/videos/2019/02/27/Lets_talk_about_fake_hate_crimes">transcript &amp; editable summary</a>)

Beau questions the intense outrage over fake hate crimes, criticizing partisan divides and advocating for ideological consistency.

</summary>

"If you only care about this guy and what he did, this instance it's blindingly racist."
"One of the biggest problems in this country is that people are more concerned about red versus blue than they are their own beliefs."
"That's what they do, you know, they tell you why you need to be mad."
"I think people call the cops too much anyway, and I don't even watch his show."
"Can't imagine what the difference is. This isn't the 1960s."

### AI summary (High error rate! Edit errors on video page)

Addresses the topic of fake hate crimes, which was repeatedly requested by viewers.
Initially found the subject uninteresting until looking into it due to right-wing rage pages.
Questions the intensity of outrage over the fake hate crime incident compared to other similar cases.
Criticizes the focus on partisan divides rather than individual beliefs.
Condemns the blinding racism in only caring about this specific incident.
Expresses disinterest in discussing topics that cannot be used to make a wider point.
Points out the inconsistency and divisiveness caused by outrage-driven news coverage.
Suggests that people prioritize ideological consistency over political affiliations.
Raises concerns about innocent people possibly being arrested due to false complaints.
Comments on the lack of utility in focusing solely on outrage without broader implications.

Actions:

for viewers,
Challenge partisan divides by prioritizing individual beliefs and ideological consistency (implied).
</details>
<details>
<summary>
2019-02-26: Let's talk about terrorism, how it works, and how to fight it.... (<a href="https://youtube.com/watch?v=L_EQcqzJvz4">watch</a> || <a href="/videos/2019/02/26/Lets_talk_about_terrorism_how_it_works_and_how_to_fight_it">transcript &amp; editable summary</a>)

Beau explains terrorism as the unlawful use of violence to achieve goals through influence beyond the immediate area, suggesting engaging with terrorists to prevent attacks and addressing grievances.

</summary>

"Terrorism is not a slur. It's a strategy."
"That's how terrorism works, start to finish, and it is so predictable it occurs on playgrounds."
"None of the ways we tried after 9-11."
"It's going to happen. So we can address it now or address it later. It's that simple."
"Violence isn't widespread yet. It's isolated within a subsection of this group."

### AI summary (High error rate! Edit errors on video page)

Defines terrorism as the unlawful use of violence or the threat of violence to influence people beyond the immediate area of attack to achieve a political, religious, ideological, or monetary goal.
Notes that terrorism is carried out by non-state actors, distinguishing it from acts committed by governments.
Mentions state-directed terrorism, where weak states use terrorist groups to achieve their objectives.
Provides a real-life playground analogy to illustrate the dynamics of terrorism, insurgency, and regime change.
Suggests that in-person assassinations of competent terrorist leadership are the most effective military strategy.
Criticizes ineffective methods like drone strikes and invasions, which often lead to more militants and insurgency.
Recommends addressing terrorists' grievances to prevent attacks and foster resolution before resorting to violence.
Emphasizes the importance of understanding and engaging with terrorists to prevent future attacks.

Actions:

for community members, policymakers, activists,
Talk about grievances with potential terrorists (suggested)
Engage with individuals prone to radicalization (suggested)
Address potential terrorists before resorting to violence (exemplified)
</details>
<details>
<summary>
2019-02-25: Let's talk about SEALs, INCELs, and how they have something in common.... (<a href="https://youtube.com/watch?v=lLZfKs5N8jQ">watch</a> || <a href="/videos/2019/02/25/Lets_talk_about_SEALs_INCELs_and_how_they_have_something_in_common">transcript &amp; editable summary</a>)

Beau delves into the controversial figure of Dick Marcinko, incel community insights, and the importance of psychological resilience in facing challenges and improving society.

</summary>

"He looked for people that had suffered, that had that psychological fortitude."
"Killing people, killing yourself, it's not the answer."
"You're not going to make it better for the people after you. You're going to make it worse."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic of Dick Marcinko, the founder of SEAL Team 6, and addresses his controversial reputation.
Questions the conviction of Marcinko, suggesting it may have been a result of personal vendettas.
Comments on Marcinko's arrogance based on personal interactions but acknowledges his military accolades.
Explores Marcinko's unique approach to building SEAL Team 6 by selecting individuals who had to struggle, showcasing psychological fortitude.
Draws parallels between Marcinko's team-building strategy and insights into the incel community.
Observes intelligence, self-awareness, misogyny, and racism within the incel groups.
Identifies three types of incels: physically unattractive, psychological or cognitive issues, and those with negative personality traits.
Notes that violent rhetoric and suicidal talk predominantly stem from the third group of incels.
Encourages incels to potentially use their experiences and psychological resilience for positive purposes.
Urges incels to avoid resorting to violence or isolation, suggesting that improving society benefits not only them but future generations.

Actions:

for incels, community builders,
Reach out to support groups or mental health professionals for assistance (suggested)
Engage in positive community activities to channel experiences and resilience positively (implied)
Advocate for societal change to prevent isolation and violence (implied)
</details>
<details>
<summary>
2019-02-25: Let's talk about Brexit, Ireland, and a hard border.... (<a href="https://youtube.com/watch?v=br9YiOB0Ddo">watch</a> || <a href="/videos/2019/02/25/Lets_talk_about_Brexit_Ireland_and_a_hard_border">transcript &amp; editable summary</a>)

Beau explains the implications of Brexit on the Irish border, warning about potential violence and advising complete non-involvement for safety.

</summary>

"I mean they make Bernie Sanders look like Trump."
"Stay out of whatever area develops as a hotbed, just stay out of it."
"This is just throwing a match on the powder keg."

### AI summary (High error rate! Edit errors on video page)

Explains the division of the island of Ireland between North and South, controlled by the UK and Republic of Ireland respectively.
Describes the significance of Brexit in relation to the open border between Northern Ireland and the Republic of Ireland.
Mentions the US's stance on backing a post-Brexit trade deal that includes an open border on the Irish island.
Raises concerns about potential violence if a hard border is implemented, specifically mentioning Republican violence.
Differentiates Irish Republicans from American Republicans, noting the political ideologies of each.
Advises individuals living near potential conflict areas to stay completely out of any involvement to ensure safety.
Talks about the increased accessibility of explosives due to online materials, making it easier for individuals to manufacture them.
Mentions the historical practice of giving warnings before bomb attacks and the potential for modern technology to aid in issuing alerts.
Warns against getting involved in areas where violence might erupt and advises staying away from such hotbeds.
Comments on the changing dynamic regarding targeting the royal family and British establishment in potential attacks.

Actions:

for irish residents, brexit observers,
Stay completely out of potential conflict areas (implied)
Remove any nationalist symbols from visible locations (implied)
Keep any political affiliations to yourself (implied)
Pay attention to warnings through modern technology (implied)
</details>
<details>
<summary>
2019-02-24: Let's talk about a question from a teacher and the next generation of activists.... (<a href="https://youtube.com/watch?v=4lSzdINAfSU">watch</a> || <a href="/videos/2019/02/24/Lets_talk_about_a_question_from_a_teacher_and_the_next_generation_of_activists">transcript &amp; editable summary</a>)

A teacher seeks guidance on a student's defiance, sparking Beau's insights on diverse self-advocacy tactics and the role of activism in teaching students their rights.

</summary>

"How do we create the next generation of people that will look at an injustice and say, no, not today."
"If you don't assert your rights, you don't have any."
"It wasn't until this message that I realized we don't have to. People like Lakeland PD are going to do it for us."
"There's always going to be somebody that's going to say whatever you did was wrong. The question is, was it effective?"
"Just let them know that they can. Just let them know that it is okay to advocate for yourself, to stand up for your rights."

### AI summary (High error rate! Edit errors on video page)

A teacher asked about a student refusing to leave with the principal, leading to police involvement.
Questions arise about compliance, respect, and self-advocacy for students.
Beau questions the training and support available to the student in advocating for their rights.
Beau believes in diverse tactics for self-advocacy and questions the concept of a "right way."
He expresses concerns about the complex nature of this teachable moment and its potential impact.
Beau underscores the importance of teaching students about their rights and self-advocacy.
He acknowledges the role of the Lakeland PD in sparking defiance and activism in students.
Beau mentions the national attention drawn to the incident in Lakeland, Florida, and its impact on students' awareness of rights.
Respectful activism may still provoke criticism, but effectiveness is key.
Beau encourages individuals to determine their level of involvement in activism and self-advocacy.

Actions:

for teachers, students, activists,
Teach students about their rights and how to self-advocate (exemplified)
Encourage diverse tactics for self-advocacy (exemplified)
Spark awareness and activism among students through real-life examples (exemplified)
</details>
<details>
<summary>
2019-02-23: Let's talk about reading more than the headline and Lakeland, Florida.... (<a href="https://youtube.com/watch?v=CjNgYXH2UoA">watch</a> || <a href="/videos/2019/02/23/Lets_talk_about_reading_more_than_the_headline_and_Lakeland_Florida">transcript &amp; editable summary</a>)

Beau dissects the situation in Lakeland, Florida where a child faces punishment for not standing during the pledge, exposing the attempt to spin headlines and uphold constitutional rights.

</summary>

"You're punishing him for exercising his constitutionally protected rights."
"If you do that, that flag is worthless. It means nothing."
"You are making that flag worthless."
"It's not worth a pledge."
"It isn't."

### AI summary (High error rate! Edit errors on video page)

Beau addresses the situation in Lakeland, Florida where a child who did not stand for the pledge is facing punishment despite headlines stating otherwise.
The child's actions are protected by the West Virginia State Board of Education versus Barnett Supreme Court case law, allowing students to choose not to stand for the pledge.
A substitute teacher attempted to shame or coerce the child into standing for the pledge, leading to interference with the educational process.
The school administrator intervened and told the child to leave the class, which goes against the Tinker versus Des Moines ruling that students cannot be punished for peaceful protests that don't disrupt education.
Law enforcement got involved, with a police officer backing up the school's decision to remove the child from the class.
The child was arrested for disrupting the school and resisting arrest without violence, but questions arise about the validity of these charges.
Beau planned activism involving playing the National Anthem on loop at the school board meeting but was overruled by local activists.
The current plan of activism involves targeting campaign contributors and businesses associated with government officials for boycotts.
Despite headlines suggesting the child may not be prosecuted, the situation is far from resolved, and punishing the child for exercising his rights undermines the value of the flag.
Beau concludes by noting the importance of upholding constitutionally protected rights, regardless of how the child is punished.

Actions:

for activists, advocates, parents,
Contact local activists to support their efforts in addressing the situation in Lakeland, Florida (implied).
Participate in boycotts of businesses associated with government officials who are involved in the case (implied).
Stay informed about legal developments in the case and support the child's legal counsel (implied).
</details>
<details>
<summary>
2019-02-23: Let's talk about left wing media bias and two exercises in satire.... (<a href="https://youtube.com/watch?v=8V1RnieO7mw">watch</a> || <a href="/videos/2019/02/23/Lets_talk_about_left_wing_media_bias_and_two_exercises_in_satire">transcript &amp; editable summary</a>)

AOC Press tweets cause outrage, revealing political manipulation, media bias, and the overlooked accomplishments of deserving Congress members.

</summary>

"The right-wing rage machine in this country knows their audience is dumb."
"People are susceptible to confirmation bias."
"The media's left-wing bias is evident."
"A Congresswoman with an impressive resume remains largely ignored."
"The media neglects stories about her involvement in controversies surrounding Flint and Standing Rock."

### AI summary (High error rate! Edit errors on video page)

AOC Press tweets were perceived as real, causing outrage, but it's actually a parody account.
The reaction to the tweets reveals how the right-wing uses gullibility for political gain.
People's susceptibility to confirmation bias fuels polarization.
AOC receives disproportionate attention compared to other deserving Congress members.
Beau praises a Congresswoman with an impressive resume who remains largely ignored.
The media's left-wing bias is evident in its coverage or lack thereof.
The overlooked Congresswoman has a notable background in science and international relations.
Despite significant achievements, the media fails to give her the attention she deserves.
The media neglects stories about her involvement in controversies surrounding Flint and Standing Rock.
Beau criticizes the media's focus on sensationalism over substantive contributions.

Actions:

for media consumers,
Support and amplify deserving Congress members who are overlooked in media coverage (suggested).
Challenge and fact-check sensationalist news to combat misinformation (suggested).
</details>
<details>
<summary>
2019-02-22: Let's talk about roses and one of my heroes.... (<a href="https://youtube.com/watch?v=iboTt4la7RI">watch</a> || <a href="/videos/2019/02/22/Lets_talk_about_roses_and_one_of_my_heroes">transcript &amp; editable summary</a>)

Beau introduces Sophie Scholl from the White Rose Society, showcasing her impact through passive resistance against the Nazi regime and the importance of individual action for righteous causes.

</summary>

"How can we expect righteousness to prevail when there is hardly anybody willing to give himself up individually to a righteous cause?"
"If through us, thousands of people are awakened and stirred to action."
"Even in a sea of brown shirts there can be white roses."
"Don't be as good at your job as you could be. Slow down at work."
"Somebody after all had to make a start. What we wrote and said is also believed by others. They just don't dare express themselves as we did."

### AI summary (High error rate! Edit errors on video page)

Beau introduces a historical figure who had a significant impact during WWII.
The figure, Sophie Scholl, was part of the White Rose Society, which advocated passive resistance against the Nazi regime.
Sophie, along with her brother Hans, was part of an anti-Hitler family but took a different path towards resistance.
The White Rose Society wrote leaflets framed in biblical and philosophical arguments to reach a wider audience.
Despite not engaging in violent resistance like blowing up bridges or taking out trains, Sophie believed in the power of passive resistance.
Sophie's execution at 21 years old left a lasting impact, showcasing bravery and standing up for righteous causes.
Her message emphasized the importance of meeting people where they are in life's journey and the difference between legality and morality.
Sophie's actions taught that everyone can contribute to resistance efforts in their unique ways.
She believed in the power of passive resistance, even in small acts like slowing down work to obstruct the Nazi regime.
Sophie's defense during her trial reflected her belief that someone had to start expressing dissent, inspiring others to do the same.

Actions:

for history enthusiasts, activists,
Learn more about historical figures like Sophie Scholl and the White Rose Society (suggested)
Educate others on the difference between legality and morality (implied)
Engage in passive resistance in everyday actions to stand up against injustices (implied)
</details>
<details>
<summary>
2019-02-21: Let's talk about transgender soldiers.... (<a href="https://youtube.com/watch?v=99Jw8ci4Uvk">watch</a> || <a href="/videos/2019/02/21/Lets_talk_about_transgender_soldiers">transcript &amp; editable summary</a>)

Beau breaks down the insignificant impact of transgender individuals on military readiness and stresses the importance of their service for societal acceptance and diversity.

</summary>

"It is upsetting that soldiers who have fought are being discarded in this way by the government."
"This small percentage of soldiers has become a political pawn."
"At the end of the day, this ban, it has nothing to do with readiness, nothing."
"The military has always been on the cutting edge of social acceptance."
"I served with a black guy. They're all right."

### AI summary (High error rate! Edit errors on video page)

Responds to a question about transgender people in the military impacting readiness and the importance of their service.
Approximately 1000 to 15,000 transgender individuals serve in the military, a small percentage.
Military readiness is based on worldwide deployability and unit cohesion.
RAND Corporation's study found no breakdown in unit cohesion in countries allowing transgender individuals to serve.
Concerns about worldwide deployability due to hormone treatments requiring refrigeration, similar to insulin dependence.
Loss of time for a transgender person in labor years is minimal, not a significant readiness issue.
Beau compares the minimal impact of transgender individuals on readiness to pregnancy sidelining soldiers for nine months.
Acknowledges a legitimate readiness concern for a small percentage of transgender individuals requiring long-term hormone treatment.
Beau argues that turning away willing recruits impacts readiness more than allowing transgender individuals to serve.
Draws parallels to historical military acceptance movements like desegregation and acceptance of gay individuals.
Points out the military's role in driving social acceptance and the importance of diverse representation in the armed forces.
Condemns the ban on transgender individuals serving in the military as politically motivated, not related to readiness.
Expresses disappointment at the mistreatment and politicization of soldiers who have served.
Mentions the California National Guard's decision to allow transgender individuals to serve despite DOD policy, expressing concerns about potential issues.

Actions:

for advocates for inclusive military policies.,
Support efforts to allow transgender individuals to serve openly in the military (exemplified).
Advocate for policies that prioritize diversity and inclusion in the armed forces (suggested).
</details>
<details>
<summary>
2019-02-21: Let's talk about a hypothetical question, military readiness, and the wall.... (<a href="https://youtube.com/watch?v=h7WkKRmj-1c">watch</a> || <a href="/videos/2019/02/21/Lets_talk_about_a_hypothetical_question_military_readiness_and_the_wall">transcript &amp; editable summary</a>)

Beau poses a hypothetical scenario about targeting critical military facilities, revealing they are actually sacrifices for a partial border wall, jeopardizing military readiness.

</summary>

"They're crippling our air because if these aircraft aren't maintained, and they don't fly. It's that simple."
"What we're doing is we're going to give up our ability to defend against an actual invasion to build part of a wall to stop an invasion he made up."
"Two hundred thirty-four miles is not building the wall. It's not. It's a start."

### AI summary (High error rate! Edit errors on video page)

Poses a hypothetical question about a list of facilities being targeted by a foreign power and their potential goals and strategies.
Mentions critical facilities like maintenance facilities for the F-35, dry dock facilities at Pearl Harbor, and training facilities for special operations.
Suggests that targeting these facilities is an attempt to degrade readiness before a war.
Reveals that the list of facilities in question are actually Department of Defense programs being sacrificed for border wall construction.
Criticizes the idea of sacrificing vital military programs for a partial border wall, which falls short of fulfilling promises to rebuild the military.
Expresses concern about the negative impact on military readiness and logistics due to diverting funding from vital programs.

Actions:

for policy makers, activists,
Contact policymakers to advocate for prioritizing military readiness over border wall construction (suggested).
Join advocacy groups working to protect vital Department of Defense programs (exemplified).
</details>
<details>
<summary>
2019-02-20: Let's talk about those new immigration numbers.... (<a href="https://youtube.com/watch?v=eUrR0eFsWvI">watch</a> || <a href="/videos/2019/02/20/Lets_talk_about_those_new_immigration_numbers">transcript &amp; editable summary</a>)

Beau debunks the border crisis narrative, reveals data on undocumented population decline, and underscores the lengthy wait times for legal immigration as real issues to address.

</summary>

"There is no crisis at the border. It doesn't exist."
"That's why people don't just get in line and come here legally."
"If we're going to pretend that there is some crisis, maybe we should start with the stuff we have control over, which is the wait times."

### AI summary (High error rate! Edit errors on video page)

Distinguishes between the Center for Migration Studies and the Center for Immigration Studies, cautioning against using the latter as a source.
Presents a new study from the Center for Migration Studies with seven years of hard data on undocumented workers entering the United States.
Notes that most undocumented workers enter the U.S. via visa overstays rather than border crossings.
Breaks down the annual numbers: $320,000 from visa overstays and $190,000 from border crossings, questioning President Trump's claims of an invasion based on secret intelligence.
Debunks the crisis at the border narrative, pointing out that the undocumented population in the U.S. fell by 1.1 million from 2010 to 2017, with more leaving than arriving.
Provides insights into the lengthy wait times for legal immigration, using the example of a Mexican individual with ties to the U.S. needing to apply for a visa back in 1997.
Suggests that addressing wait times for legal immigration could be a practical starting point if there are concerns about illegal immigration.
Encourages focusing on areas within our control, like wait times, if there is a perceived crisis at the border.

Actions:

for citizens concerned about immigration policies,
Advocate for reducing wait times for legal immigration processes (suggested)
Share information on visa application processing times to raise awareness (implied)
</details>
<details>
<summary>
2019-02-20: Let's talk about astronauts, Portuguese art, and becoming well read without reading.... (<a href="https://youtube.com/watch?v=qGwlNQ-EViw">watch</a> || <a href="/videos/2019/02/20/Lets_talk_about_astronauts_Portuguese_art_and_becoming_well_read_without_reading">transcript &amp; editable summary</a>)

Beau shares a story about meeting an astronaut and advocates for using LibriVox to become well-read despite a busy schedule, stressing the importance of reading banned books.

</summary>

"Just because I'm sure somebody will ask, no, this isn't a commercial for LibriVox, they did not pay me, they don't even know I'm talking about it right now."
"There's always a way to accomplish your goal."
"Banned books are the best books."

### AI summary (High error rate! Edit errors on video page)

Beau starts by sharing a story about a guy who meets an astronaut on a plane and tries to strike up a conversation with him.
The astronaut's interest in Portuguese art despite being an astronaut is explained by his desire to learn and expand his knowledge.
Beau addresses a common question he receives about how to become well-read without having the time to read.
He recommends an app called LibriVox, which offers thousands of audio books for free, allowing people to listen while cooking or commuting.
Beau underscores the importance of reading for education, understanding, and gaining insights from different perspectives.
He mentions that banned books are often the best books and recommends exploring them for a richer reading experience.
Beau clarifies that his endorsement of LibriVox is not sponsored and encourages people to find ways to achieve their goals.

Actions:

for book lovers, busy individuals,
Download the LibriVox app and listen to audiobooks during daily activities (suggested)
Make time for reading by incorporating it into cooking or commuting routines (implied)
</details>
<details>
<summary>
2019-02-19: Let's talk about vaccines, jaywalking, and abortion (<a href="https://youtube.com/watch?v=Wu4s1GkWIgk">watch</a> || <a href="/videos/2019/02/19/Lets_talk_about_vaccines_jaywalking_and_abortion">transcript &amp; editable summary</a>)

Beau challenges viewers to watch his controversial take on vaccines, exploring their history, risks, rewards, and the ethical debate around bodily autonomy in healthcare decisions.

</summary>

"Don't. Watch the whole thing."
"Risk versus reward, that's what it's all about."
"Every law is backed up by penalty of death."
"Either we have bodily autonomy or we do not."
"Either we're that servant's eight-year-old boy, or we do maintain control over our own destiny."

### AI summary (High error rate! Edit errors on video page)

Urges viewers to watch the whole video even if they disagree with the content.
Traces back the history of vaccines to 1902 when smallpox vaccination was made mandatory in France.
Describes how the first vaccine came about through Jenner's experimentation with cowpox and smallpox.
Acknowledges different perspectives on vaccine history, including ethical concerns and conspiracy theories.
Mentions the practice of inoculation in Africa, India, and China before Jenner's discovery.
Emphasizes the concept of risk versus reward in vaccination.
Clarifies that adverse reactions to vaccines don't always mean death.
Encourages anti-vaxxers to research and make informed decisions.
Notes that parents not following post-vaccination instructions can complicate adverse reactions.
Explains how vaccines work, addressing concerns about unvaccinated children posing a risk.
Compares principles of bodily autonomy in vaccination and abortion.
Asserts the importance of individual freedom in making healthcare choices.
Raises ethical concerns about mandatory vaccines despite advocating for vaccination.
Draws parallels between government control in vaccine mandates and abortion regulations.

Actions:

for health-conscious individuals,
Research vaccines thoroughly before making decisions (suggested)
Follow post-vaccination instructions from healthcare providers (exemplified)
</details>
<details>
<summary>
2019-02-18: Let's talk about getting arrested for not saying the pledge.... (<a href="https://youtube.com/watch?v=4-Y0vE97JMc">watch</a> || <a href="/videos/2019/02/18/Lets_talk_about_getting_arrested_for_not_saying_the_pledge">transcript &amp; editable summary</a>)

A student in Florida arrested for refusing to stand for the Pledge of Allegiance sparks debate on constitutional rights and coercive behavior in schools.

</summary>

"Indoctrinating kids creates nationalists, not patriots."
"If a contract is coerced or forced, it's meaningless."
"His rights were violated by a government employee."

### AI summary (High error rate! Edit errors on video page)

A sixth-grade student in Polk County, Florida was arrested for refusing to stand for the Pledge of Allegiance at Lawton Childs Middle Academy.
The student was charged with disrupting a school function and resisting arrest without violence.
The substitute teacher at the school questioned the student's refusal to stand, leading to a confrontation about the perceived racism of the flag and national anthem.
Despite settled law that students do not lose their constitutional rights at school, the student faced charges for disrupting the school function.
Beau argues that the disruption was caused by the teacher's attempt to coerce the student into pledging allegiance, not by the student's peaceful protest.
The student was defending his constitutionally protected rights and should not have been arrested for doing so.
Beau criticizes the teacher's actions as coercive and questions why the student faced consequences while the teacher was let go by the school board.
Beau calls for the immediate dropping of charges against the student and stresses the importance of defending his rights against government overreach.

Actions:

for students, teachers, activists,
Defend the student's rights by advocating for the immediate dropping of charges (suggested).
Support initiatives that protect students' freedom of expression in educational settings (implied).
</details>
<details>
<summary>
2019-02-17: Reporting from the Mexican border.... (<a href="https://youtube.com/watch?v=oJpmdtW7N0Q">watch</a> || <a href="/videos/2019/02/17/Reporting_from_the_Mexican_border">transcript &amp; editable summary</a>)

Reporting from the Mexican border on Trump's national emergency declaration due to asylum seekers; frustration and disbelief at the situation.

</summary>

"He declared a national emergency over people crossing the border to legally claim asylum."
"I am so done with this network."

### AI summary (High error rate! Edit errors on video page)

Reporting live from the Mexican border for the fifth column.
President Trump declared a national emergency due to an invasion.
No signs of any opposition forces at the border.
Pentagon has not specified which country is invading.
Refugees seen fleeing in terror.
Assumption made that opposition forces must be close by.
New information reveals it was a family of six, not an invasion.
National emergency declared over people crossing the border to claim asylum legally.
Beau questions who sent him there and expresses frustration with the situation.
Beau is done with the network.

Actions:

for media consumers, activists.,
Contact network to express frustration (implied).
</details>
<details>
<summary>
2019-02-17: Let's talk about that shooting in Houston again.... (<a href="https://youtube.com/watch?v=-NJVLls-II4">watch</a> || <a href="/videos/2019/02/17/Lets_talk_about_that_shooting_in_Houston_again">transcript &amp; editable summary</a>)

Police union threats, fabricated warrants, and demonization of victims reveal systemic issues demanding an unbiased investigation.

</summary>

"Don't paint us all with a broad brush, isolated incident, a few bad apples, same song and dance we always hear."
"And please stop demonizing the victim."
"You need to bring somebody in that doesn't have loyalty, that doesn't want to cover for their buddy, that isn't part of that thin blue line."
"In court that [evidence] would be suppressed."
"What makes you think he didn't have pot and coke."

### AI summary (High error rate! Edit errors on video page)

Police union president in Houston made veiled threats after shooting incident.
Warrant used for the raid was entirely fabricated.
Confidential informants who were supposed to buy heroin from the house never did.
One officer had heroin in his car.
Houston PD targeted an independent journalist in 2016, leading to trumped-up charges.
Journalists cultivated sources within the Houston Police Department after the incident.
Chief of police claims isolated incident with a few bad apples, demonizes the victim.
Private face tells officers to tear apart cases, suggesting it's not an isolated incident.
Beau suggests bringing in the FBI for a thorough investigation.
Raises questions about the source of heroin, integrity of officers, and demonization of the victim.
Points out discrepancies in the evidence used for the raid.
Calls for an end to demonizing the victim and a thorough, unbiased investigation.
Questions the credibility of officers involved in the raid.
Criticizes the use of evidence that wouldn't hold up in court to justify the raid.
Raises suspicions about other possible illegal substances in the officer's possession.

Actions:

for journalists, activists, community members,
Contact local journalists or media outlets to raise awareness of the fabricated warrant and biased practices within the Houston Police Department (suggested).
Reach out to community organizations advocating for police accountability and transparency to support unbiased investigations (implied).
</details>
<details>
<summary>
2019-02-16: Let's talk about national emergencies.... (<a href="https://youtube.com/watch?v=Coh3j4EH-DY">watch</a> || <a href="/videos/2019/02/16/Lets_talk_about_national_emergencies">transcript &amp; editable summary</a>)

Beau warns of the dangerous abuse of power and erosion of democracy through the misuse of national emergencies, urging Americans to take action before it's too late.

</summary>

"This is how every democracy, every Republican history committed suicide."
"No one person should have this much power. This is dictatorial rule."
"The average American needs to start leading themselves."
"Pandora's box is being opened and it's extremely dangerous."
"If the Senate and House of Representatives, the Supreme Court, do not put an end to this immediately. That's it. It's over."

### AI summary (High error rate! Edit errors on video page)

Expresses frustration over the abuse of national emergencies for political gain.
Describes attempts to create a video addressing the issue, including initial anger and satire.
Criticizes the current national emergency declaration as dangerous and unnecessary.
Points out that border apprehensions are at a 41-year low, indicating no crisis.
Emphasizes the dangerous precedent set by one person having too much power.
Explains the limitations of a national emergency declaration in creating legislation.
Raises concerns about potential misuse of power, such as seizing firearms or factories.
Warns about the erosion of democracy if checks and balances are not upheld.
Urges Americans to be more vigilant and proactive in protecting their rights.
Calls for action from groups traditionally focused on constitutional rights.

Actions:

for american citizens,
Contact your Senators and Representatives to urge them to uphold checks and balances in government (implied)
Join or support organizations focused on defending constitutional rights and democracy (implied)
Attend marches or protests advocating for governmental accountability and transparency (implied)
</details>
<details>
<summary>
2019-02-14: Let's talk about the problem with sanctuary cities.... (<a href="https://youtube.com/watch?v=I3Nkjkoxock">watch</a> || <a href="/videos/2019/02/14/Lets_talk_about_the_problem_with_sanctuary_cities">transcript &amp; editable summary</a>)

The problem with sanctuary cities is the necessity they exemplify in protecting undocumented individuals from deportation when seeking help after being victims of violence, countering misconceptions and advocating for federal responsibility over immigration enforcement.

</summary>

"Because if the cops, local cops, have to run everybody that they won't run into through ICE's database, I mean let's just set aside the whole fact of local law enforcement walking around like the Gestapo asking for papers."
"They're not real people. They can't go to the cops."
"Statistically proven, illegal immigrants commit less violent crime than native-born."
"No local department should be enforcing laws outside of this jurisdiction."
"There's no reason for your local deputy or your local cop to walk around like the Gestapo."

### AI summary (High error rate! Edit errors on video page)

A man in Florida was lured into a trailer, tied up, beaten, robbed, and threatened by individuals who ultimately let him go after warning him not to go to the police because he was undocumented.
The attackers were white, which may surprise some listeners.
Sanctuary cities exist to protect undocumented individuals from being deported when seeking help from law enforcement after being victims of violent crimes.
Some local police departments resist cooperating with ICE due to concerns about inaccuracies in ICE's database and potential liability issues.
An example is shared of a man wrongfully detained by ICE for three weeks due to a database error.
Even a veteran carrying military ID and a passport was detained by ICE, demonstrating the flaws in the system.
The argument against sanctuary cities fostering non-assimilation is countered by the fact that undocumented workers statistically commit less violent crime than native-born individuals.
Private prisons may be pushing for a focus on immigration enforcement as a new source of profit once marijuana legalization reduces the need for housing non-violent drug offenders.
Beau argues against local law enforcement acting like immigration enforcers, stating that immigration is a federal issue and should remain as such.

Actions:

for advocates for immigrant rights,
Advocate for the protection of undocumented individuals and support sanctuary city policies (implied)
Educate your community on the importance of sanctuary cities in protecting victims of violent crimes regardless of immigration status (implied)
Support initiatives that push for federal responsibility over immigration enforcement (implied)
</details>
<details>
<summary>
2019-02-14: Let's talk about the most dangerous gun in the world.... (<a href="https://youtube.com/watch?v=9c8Zpm4HXMI">watch</a> || <a href="/videos/2019/02/14/Lets_talk_about_the_most_dangerous_gun_in_the_world">transcript &amp; editable summary</a>)

Beau stresses the inherent danger of firearms and the critical importance of gun safety measures to prevent accidents.

</summary>

"Safe and firearm do not go together."
"All firearms will kill."
"There is no safe firearm, but there are a lot of dangerous ones."

### AI summary (High error rate! Edit errors on video page)

Beau answers a question about getting a safe rifle for his son, sharing a personal story about firearm safety.
A friend accidentally shoots a metal plate hanging on the wall with a .22 pistol, illustrating the danger of firearms.
Beau stresses that all firearms are designed to kill and that safety must be a top priority.
He mentions starting with an air rifle before introducing a .22 due to its misconception of being safe.
Despite being tiny, a .22 bullet is dangerous, moving at high speeds and causing significant harm.
Even experienced individuals can make mistakes with firearms, as seen in the story Beau shares.
Beau talks about accidental discharges and how they can happen to anyone who handles guns.
Emphasizes the importance of gun safety measures like keeping the gun pointed in a safe direction and finger off the trigger.
Beau shares a humorous incident where a mistake with a gun led to beer being thrown but thankfully no serious harm.
Concludes by stating that there are no truly safe firearms, only varying levels of danger.

Actions:

for parents, gun owners,
Teach firearm safety to children and beginners, starting with air rifles (implied)
Emphasize the importance of gun safety measures like keeping the gun pointed in a safe direction and finger off the trigger (implied)
</details>
<details>
<summary>
2019-02-13: Let's talk about what a concept of Japanese art can teach us about masculinity.... (<a href="https://youtube.com/watch?v=2Px9F0p2TCE">watch</a> || <a href="/videos/2019/02/13/Lets_talk_about_what_a_concept_of_Japanese_art_can_teach_us_about_masculinity">transcript &amp; editable summary</a>)

Beau shares a Japanese concept, Shibumi, to redefine masculinity as effortless perfection, separate from conforming to charts or molds.

</summary>

"Maybe masculinity can't be quantified the way people try."
"Effortless perfection. Now that is a hard idea to get your head around."
"When you see somebody trying to be masculine, you see somebody trying to be a tough guy, happens you immediately know they're not."

### AI summary (High error rate! Edit errors on video page)

Critiques resources aimed at helping young American males find masculinity on the internet as ridiculous.
Recalls a time working in a call center with an old Japanese man during the overnight shift.
Describes drinking sake with the old Japanese man who talked about a Japanese concept called Shibumi.
Explains Shibumi as subtle, understated, part of nature but separate from it, in control of nature, and representing perfection without effort.
Suggests that masculinity cannot be quantified or charted and should involve maintaining control over natural characteristics effortlessly.
Emphasizes the importance of effortless perfection in masculinity and the need to accept oneself rather than mimicking role models.

Actions:

for young american males,
Embrace your natural characteristics and accept yourself as you are (implied).
Refrain from trying to force yourself to conform to societal molds of masculinity (implied).
</details>
<details>
<summary>
2019-02-12: Let's talk about why I don't like Facebook memes.... (<a href="https://youtube.com/watch?v=GIZ14Szfe10">watch</a> || <a href="/videos/2019/02/12/Lets_talk_about_why_I_don_t_like_Facebook_memes">transcript &amp; editable summary</a>)

Beau conducts a meme demonstration, revealing misattributed quotes and advocating for meaningful discourse, contrasting Trump and AOC's stances on gun control.

</summary>

"This is why I don't like memes."
"Boils things down to talking points, eliminates discussion. It's a slogan."
"It completely destroys any meaningful conversation."
"Donald Trump is not pro-Second Amendment."
"Don't believe everything you read or see on the internet, George Washington."

### AI summary (High error rate! Edit errors on video page)

Expresses dislike for Facebook memes and conducts a demonstration to illustrate his point.
Shares a meme featuring AOC with a controversial quote about guns.
Reads through comments on the shared meme, showcasing derogatory and misinformed remarks.
Clarifies that the quote attributed to AOC actually belongs to Donald Trump.
Criticizes memes for oversimplifying complex issues and hindering meaningful discourse.
Advocates for engaging in thoughtful and informed conversations.
Contrasts Trump and AOC's stances on the Second Amendment and due process.
Points out Trump's actions regarding firearms regulations.
Mentions AOC's position statement on gun violence and the Second Amendment.
Concludes with a quote from George Washington about not believing everything on the internet.

Actions:

for social media users,
Fact-check memes before sharing (suggested)
Engage in informed and meaningful dialogues about political issues (implied)
</details>
<details>
<summary>
2019-02-10: Let's talk about how Trump could actually drain the swamp.... (<a href="https://youtube.com/watch?v=7a-f3kptCQ4">watch</a> || <a href="/videos/2019/02/10/Lets_talk_about_how_Trump_could_actually_drain_the_swamp">transcript &amp; editable summary</a>)

Beau questions Trump's commitment to draining the swamp, contrasting AOC's actions with his inaction and lack of engagement.

</summary>

"He doesn't care about draining the swamp. He was just cutting out the middleman."
"But you know she's down there actually bringing it up and talking about it and instead of reaching out to her he's up there swimming with alligators."

### AI summary (High error rate! Edit errors on video page)

Beau questions President Trump's campaign promise to "drain the swamp of campaign finances" and mentions reaching out to him.
He refers to AOC taking Congress to task over campaign finance issues and suggests that the President could work across the aisle for real reform.
Beau criticizes Trump's true intentions, pointing out that as a businessman, he may not actually care about draining the swamp but was cutting out the middleman.
He contrasts AOC's efforts in addressing campaign finance with Trump's lack of action and engagement.
Beau signs off with a casual goodbye, wishing the viewers a good night.

Actions:

for political observers, reform advocates,
Reach out to elected officials for campaign finance reform (implied)
Stay informed and engaged in political issues (implied)
</details>
<details>
<summary>
2019-02-09: Let's talk about 1916, 1984, thought, and rebellion.... (<a href="https://youtube.com/watch?v=4i9gbymKMjw">watch</a> || <a href="/videos/2019/02/09/Lets_talk_about_1916_1984_thought_and_rebellion">transcript &amp; editable summary</a>)

In 1916, Irish rebellion sparked by seizing Dublin's General Post Office led to a legacy of independent thought, urging individuals to challenge oppressive systems with new ideas.

</summary>

"Real rebellion starts with thought, independent thought, free thought, looking around and analyzing things for yourself."
"For the first time, ideas travel faster than bullets."
"Create real change. Put a new idea in somebody's head."
"That's how you fight a system."
"Anyway, it's just a thought, y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

In 1916, 1200 men seized the General Post Office in Dublin, igniting the Irish Movement for Independence against British rule.
The Irish rebellion, though militarily a failure, sparked a legacy of independent thought and resistance.
The act of rebellion often begins with independent thought and questioning the status quo.
Winston's first act of rebellion was not writing a diary but realizing the falsehood of the system around him.
Rebellion starts with independent thought, analyzing situations, and questioning norms.
Real change in society can be brought about by spreading ideas rather than resorting to violence.
Ideas have the power to create change without the need for force or coercion.
Sparking independent thought and defending ideas can lead to significant transformations in society.
Encourages individuals to think for themselves, spark meaningful dialogues, defend their ideas, and create genuine change.
The key to fighting oppressive systems lies in challenging them through independent thought and spreading new ideas.

Actions:

for activists, free thinkers, change-makers,
Spark independent thought by encouraging others to question norms and think critically (implied).
Defend your ideas in dialogues and spark meaningful exchanges to create real change (implied).
Engage in open, honest dialogues without relying on memes or slogans to foster independent thought (implied).
</details>
<details>
<summary>
2019-02-08: Let's talk about what happens "if you don't get an education".... (<a href="https://youtube.com/watch?v=aCvYfWCcMPc">watch</a> || <a href="/videos/2019/02/08/Lets_talk_about_what_happens_if_you_don_t_get_an_education">transcript &amp; editable summary</a>)

Beau questions the link between education and labor devaluation, calls for a more representative democracy that prioritizes common ground over privilege.

</summary>

"If you're working a blue collar, no collar job, and got your name on your shirt that you've done something wrong."
"Education doesn't mean education anymore."
"More welders, less lobbyists, more scientists, less CEOs, more teachers and less lawyers."
"Let's create a system where you actually have something in common with the person governing your life."
"They're taking care of their own and they're leaving us out in the cold."

### AI summary (High error rate! Edit errors on video page)

Education often tied to devaluing labor in the United States, reinforcing class ideas.
Misconception that blue-collar jobs equate to failure in achieving the American dream.
Dangerous concept when education is confused with credentialing, favoring those with money.
Public education system not what it used to be; emphasis on credentialing over true education.
Those with money tend to receive the best credentialing, leading to power imbalances in a democracy.
Education system in the country may need a revolution due to current flaws.
Educators' decisions influenced by distant policymakers who may not understand teaching.
Calls for a more diverse representation in governance, including more waitresses and fewer millionaires.
Emphasizes the importance of creating a representative democracy where the governed have common ground with their representatives.
Critique on how current system prioritizes the wealthy and powerful over the general population.

Actions:

for educators, policymakers, activists.,
Advocate for educational reforms that prioritize true learning over credentialing (implied).
Support and amplify voices calling for more diverse representation in governance (implied).
Engage in local politics to ensure representation that mirrors community diversity (implied).
</details>
<details>
<summary>
2019-02-07: Let's talk about Cub Scouts taking a knee, nationalism, and patriotism.... (<a href="https://youtube.com/watch?v=ixedV66SfMY">watch</a> || <a href="/videos/2019/02/07/Lets_talk_about_Cub_Scouts_taking_a_knee_nationalism_and_patriotism">transcript &amp; editable summary</a>)

A ten-year-old Cub Scout's act challenges racism, embodying true patriotism and independent thought, symbolizing hope for dismantling divisive symbols.

</summary>

"Patriotism is correcting your country when it's wrong."
"Scouts are doing just fine."
"This country can build that monument. And these kids are gonna tear it down."

### AI summary (High error rate! Edit errors on video page)

A ten-year-old Cub Scout took a knee during the Pledge of Allegiance at a city council meeting, sparking varied reactions.
Some people struggle to fit their standard responses to this unique situation.
The attempt to blend racism and nationalism has led to young people associating the American flag with racism.
The rejection of racism by the youth will result in them rejecting the flag as well.
The young Scout's actions are rooted in independence, self-reliance, and free thought taught by scouting.
Patriotism involves correcting your country when it's wrong, not blind obedience.
Beau expresses gratitude towards the young Scout for embodying true patriotism at a young age and standing against racism.
Beau shares his Scout background and praises the Scout for his independent and thoughtful act.
The Scout's action symbolizes independent thought, self-reliance, and preparedness for future decisions.
Beau sees hope in the younger generation dismantling racist symbols and standing up for what's right.

Actions:

for youth advocates,
Support youth empowerment in community groups (implied)
Advocate for inclusive and anti-racist education in scouting organizations (implied)
Encourage independent thought and standing up against discrimination in youth (implied)
</details>
<details>
<summary>
2019-02-06: Let's talk about domestic violence and why women don't "just leave".... (<a href="https://youtube.com/watch?v=3YH26YcE6hc">watch</a> || <a href="/videos/2019/02/06/Lets_talk_about_domestic_violence_and_why_women_don_t_just_leave">transcript &amp; editable summary</a>)

Beau explains the misconceptions around domestic violence shelters and the critical support they provide for survivors leaving abusive relationships.

</summary>

"One of the comments was, I don't even understand why these things exist, why can't women just leave?"
"It affects everybody, every class, but it's not as simple as just leaving."
"Women are 500 times more likely to be killed if they're in one of these relationships when they're just leaving."
"Any obstacle that's standing in the way, they get rid of it."
"It's something that can be done."

### AI summary (High error rate! Edit errors on video page)

Addresses misconceptions about domestic violence shelters and the challenges faced by survivors.
Domestic violence affects everyone regardless of gender, race, or class.
Personal connections drive Beau's involvement in the fight against domestic violence.
Shares personal stories of friends who were victims of domestic violence.
Women are 500 times more likely to be killed when leaving abusive relationships.
Domestic violence shelters play a critical role in reducing risks and providing support.
Financial control by abusers complicates leaving abusive relationships.
Challenges include finding a safe place to go, caring for pets, and potential job changes.
Domestic violence extends to the workplace, with a high percentage of women being killed by intimate partners.
Beau supports Shelter House of Northwest Florida for their innovative solutions and stigma-fighting initiatives.

Actions:

for supporters of domestic violence survivors.,
Donate money or old cell phones to organizations supporting domestic violence survivors (suggested).
Contact Shelter House of Northwest Florida to inquire about ways to help (suggested).
Support initiatives like providing kits for rape victims in hospitals (suggested).
</details>
<details>
<summary>
2019-02-05: Let's talk about the real state of the union.... (<a href="https://youtube.com/watch?v=eegrx5LlUBo">watch</a> || <a href="/videos/2019/02/05/Lets_talk_about_the_real_state_of_the_union">transcript &amp; editable summary</a>)

The United States faces political division, fragile masculinity, systemic racism, and rising hate crimes while individuals quietly rebel and build new systems for change.

</summary>

"We don't defeat tyranny and oppression by complying with it while we fight it."
"We defeat it by ignoring it while we quietly create our own systems to replace it."
"The state of those people who care? Well, they're in a state of defiance."

### AI summary (High error rate! Edit errors on video page)

The United States is as politically divided today as during desegregation.
Washington, D.C. is not in a good state.
Trump's wall dominates headlines despite not being a top priority for most Americans.
Families are still being torn apart for crossing a border.
Government and large corporations preach conservation while harming the environment.
$3.4 billion is spent on lobbying and corruption.
News focuses on manipulating emotions rather than informing.
The drug war continues, imprisoning innocent individuals.
Fragile masculinity is evident in men's reactions to a razor ad.
Teachers are forced to prepare for combat.
Systemic racism persists in society.
Youth are mocked and discredited.
Hate crimes are increasing, and Nazis are present in the streets.
Americans are fighting for safety and freedom for others, even at personal risk.
Eight companies produce more pollution than the entire U.S. population.
Communities are growing stronger, decreasing the power of DC politicians.
Independent journalists strive to separate fact from fiction.
Masculine men openly criticize toxic masculinity.
Teachers are implementing diverse methods to combat violence in schools.
People of all races are uniting against systemic racism.
Youth are at the forefront of societal battles.

Actions:

for americans,
Joining together with community members to combat hate crimes and systemic racism (exemplified)
Supporting independent journalists striving for truth (exemplified)
Implementing diverse methods to combat violence in schools (exemplified)
</details>
<details>
<summary>
2019-02-04: Let's talk about Patty Hearst, the homeless, and pitchforks.... (<a href="https://youtube.com/watch?v=mk7T5yS7_jI">watch</a> || <a href="/videos/2019/02/04/Lets_talk_about_Patty_Hearst_the_homeless_and_pitchforks">transcript &amp; editable summary</a>)

45 years after Patty Hearst's kidnapping, Beau questions the morality of extreme wealth through a Jeff Bezos example, proposing an end to homelessness in the US for a fraction of his wealth.

</summary>

"One guy can end homelessness and give them $200 a week. It's crazy. It's insane."
"When does it become moral to act? The system as it is, is turning the working class into the working poor and millionaires into billionaires."
"The entitled nature of the wealthy in the United States now."
"We still know where the pitchforks are. And if things don't change, those pitchforks are coming."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Patty Hearst was kidnapped 45 years ago today, and her family gave up $2 million in negotiations with the SLA.
The $2 million given up by the Hearst family was to be turned into food and distributed to the needy, not handed over to the kidnappers.
Patty Hearst's grandfather, adjusted for inflation, was worth about $30 billion when he died.
Beau questions the morality of extreme wealth in society, using Jeff Bezos as an example.
Bezos earned $78.5 billion last year, but most of it is not liquid cash.
Beau imagines what Bezos could do if he decided to end homelessness in the U.S., estimating it to cost $5.7 billion.
Beau suggests the idea of taxing the ultra-wealthy at a higher rate or holding them socially responsible.
He points out the extreme income inequality and questions when it becomes a moral imperative to act against it.
Beau criticizes the entitlement and lack of empathy shown by the wealthy in the United States.
He warns that if things don't change, people may resort to drastic actions to address inequality.

Actions:

for social justice advocates,
Advocate for policies that address income inequality and homelessness (implied)
Support initiatives that tax the ultra-wealthy at a higher rate (implied)
Raise awareness about the moral implications of extreme wealth accumulation (implied)
</details>
<details>
<summary>
2019-02-03: Let's talk about Trump's gift to Putin.... (<a href="https://youtube.com/watch?v=WP_BGUp-ScQ">watch</a> || <a href="/videos/2019/02/03/Lets_talk_about_Trump_s_gift_to_Putin">transcript &amp; editable summary</a>)

Trump's action towards Putin was a gift, removing the only obstacle for Russia to develop weapon systems that threaten European allies and jeopardizing European security.

</summary>

"Trump's action towards Putin was a gift, not punishment or leverage."
"The move jeopardizes European security and could lead to an arms race."
"Removing the treaty allows Russia to openly develop weapon systems that threaten European allies."
"Russia, on the other hand, needed these missiles as NATO expanded closer to its borders."
"Trump's decision signals a disregard for treaties and sets a dangerous precedent."

### AI summary (High error rate! Edit errors on video page)

Trump's action towards Putin was a gift, not punishment or leverage.
The treaty negotiated in 1987 banned land-based short and intermediate-range missiles.
The treaty heavily favored the United States over the Soviet Union.
The U.S. did not need land-based missiles due to geographical factors.
Russia, on the other hand, needed these missiles as NATO expanded closer to its borders.
Removing the treaty allows Russia to openly develop such missiles.
Obama's administration understood the limitations in dealing with Putin's Russia.
Trump's action has removed the only obstacle for Russia to develop weapon systems that threaten European allies.
The move jeopardizes European security and could lead to an arms race.
Trump's decision signals a disregard for treaties and sets a dangerous precedent.

Actions:

for politically engaged citizens,
Contact political representatives to express concerns about the potential repercussions of abandoning treaties (implied)
Stay informed about international treaties and their implications on global security (implied)
</details>
<details>
<summary>
2019-02-02: Let's talk about teaching the Bible in school.... (<a href="https://youtube.com/watch?v=lft9g73Smbw">watch</a> || <a href="/videos/2019/02/02/Lets_talk_about_teaching_the_Bible_in_school">transcript &amp; editable summary</a>)

President Trump's proposal to put the Bible back in schools sparks a reflection on religious tolerance, philosophy, and the decline of Christianity due to hypocrisy.

</summary>

"Teaching philosophy is education; enforcing moral code is indoctrination."
"The downfall of Christianity in the US is due to blatant hypocrisy."
"The ultimate irony is that today's Church of Satan embodies most of the ideals of Christianity better than most Christian churches."
"The goal of education should not be to teach you what to think. It should be to teach you how to think."

### AI summary (High error rate! Edit errors on video page)

President Trump wants to put the Bible back in school.
Christian students at school didn't like the idea much.
The modern church is indicted for not fostering religious tolerance.
Beau believes in teaching religious texts in schools if all are taught equally.
Teaching philosophy is education; enforcing moral code is indoctrination.
Philosophy, including religious philosophy, helps people think critically.
Beau points out similarities in teachings across different religions.
The downfall of Christianity in the US is due to hypocrisy, especially in politics.
Christianity is on the decline due to blatant hypocrisy.
Beau doubts the political expedience of disregarding Jesus' teachings.
Public schools turning into religious institutions is unlikely to happen.
Beau mentions the Church of Satan demanding their texts to be taught in schools.
Teaching philosophy, not just religious philosophy, can benefit the American people.
Education should focus on teaching how to think, not what to think.

Actions:

for educators, religious leaders,
Advocate for equal representation of all religious texts in school curriculum (implied)
Support teaching philosophy, including religious philosophy, in educational institutions (implied)
</details>

## January
<details>
<summary>
2019-01-31: Let's talk about how to reach out to cops.... (<a href="https://youtube.com/watch?v=9b3vDdqY1PA">watch</a> || <a href="/videos/2019/01/31/Lets_talk_about_how_to_reach_out_to_cops">transcript &amp; editable summary</a>)

Beau explains the philosophy behind ACAB, suggests focusing on harm reduction and self-interest, and criticizes the failures of the drug war while challenging police perceptions of themselves.

</summary>

"If there's no harm, there's no crime. If there's no victim, there's no crime."
"The drug war and gun control are probably the two best ways to reach out to cops."
"Most of them see themselves as true patriots, true patriots."
"Not waging the drug war is more effective than waging it if you actually believe in harm reduction."
"So give it to them. give it to them."

### AI summary (High error rate! Edit errors on video page)

Explains the philosophy behind the ACAB slogan, which stands for "All cops are bad because they will enforce unjust laws" or because if good cops don't speak out against bad cops, they are complicit.
Acknowledges that slogans like ACAB serve a purpose in energizing like-minded individuals but also points out that they can turn off those who don't agree.
Suggests focusing on harm reduction and approaching the issue from a self-interest perspective to encourage engagement and critical thinking.
Points out that the drug war is a significant factor in the strained relationship between law enforcement and the public, leading to militarization and negative perceptions of police.
Uses statistics to illustrate the failures of the drug war and how countries like Portugal have seen positive outcomes by decriminalizing drugs.
Criticizes the image that many police officers hold of themselves, likening their actions in drug raids to Gestapo tactics and warning about the potential future perception of law enforcement.
Challenges the idea of police officers as patriots by questioning their enforcement of certain laws and suggesting that blind obedience to orders is detrimental.

Actions:

for activists, police reform advocates,
Reach out to police officers to have constructive dialogues about harm reduction and the failures of the drug war (implied).
Advocate for decriminalization and harm reduction policies in your community (implied).
Encourage critical thinking and engagement by approaching issues from a self-interest perspective (implied).
</details>
<details>
<summary>
2019-01-30: Let's talk about those cops in Houston getting shot.... (<a href="https://youtube.com/watch?v=E1oU6E6IKgs">watch</a> || <a href="/videos/2019/01/30/Lets_talk_about_those_cops_in_Houston_getting_shot">transcript &amp; editable summary</a>)

Four cops shot in Houston, but Beau focuses on police response, offering free training if union president resigns for stifling freedom of speech.

</summary>

"If you don't want to be cast as the enemy, don't be the enemy of freedom."
"You representing an armed branch of government do not get to tell people what they can and cannot talk about."
"Mr. Jamali, do you care about your officers enough to resign so they can get the training they need for free?"

### AI summary (High error rate! Edit errors on video page)

Four cops were shot during a box drug raid in Houston.
The reasons for the shooting are likely due to telegraphed movements or a lack of proper threat assessment.
The focus of the talk tonight is on the response to the shooting, not the shooting itself.
The police union president, Joe Jamali, issued a threatening statement in response to those critical of police officers.
Beau shares an interesting statement about training from a group in Texas.
This group is willing to train 15 officers for free if Joe Gimaldi resigns.
Beau stresses the importance of freedom of speech and critiquing the government without fear of reprisal.
He questions whether Joe Jamali cares enough about his officers to resign for their benefit.
The offer of free training for officers is contingent on the resignation of Joe Gimaldi.
Beau concludes with a call for Joe Jamali to resign for the sake of freedom and proper training for officers.

Actions:

for community members, advocates.,
Contact the group from Texas offering free training to see how to support their initiative (suggested).
Advocate for police accountability and freedom of speech in your community (implied).
</details>
<details>
<summary>
2019-01-29: Let's talk about Alexandria Ocasio-Cortez becoming the next Ron Paul.... (<a href="https://youtube.com/watch?v=w1uR6Xy3kks">watch</a> || <a href="/videos/2019/01/29/Lets_talk_about_Alexandria_Ocasio-Cortez_becoming_the_next_Ron_Paul">transcript &amp; editable summary</a>)

Beau contemplates the similarities and differences between Alexandria Ocasio-Cortez and Ron Paul, sparking a debate on establishment politics and the potential for change within the Democratic Party.

</summary>

"What if it's not corporate establishment versus government establishment? What if it's just establishment versus you?"
"With a little bit of refinement and a little bit of time, she'll grow just like everybody does."
"I think that we're looking at a powerhouse in the making."

### AI summary (High error rate! Edit errors on video page)

Made a controversial comparison between Alexandria Ocasio-Cortez and Ron Paul, causing a stir among his libertarian friends.
Discovered that 10-15% of his viewers are overseas, observing the U.S. like the fall of the Roman Empire.
Describes Ron Paul as a libertarian icon known for his strict adherence to the Constitution and opposition to government overreach.
Contrasts the views of Ron Paul and Alexandria Ocasio-Cortez, noting their differing perspectives on corporate greed versus government overreach.
Suggests that while AOC and Ron Paul seem very different, they may both have valid points in critiquing the establishment.
Points out the potential for AOC to grow in influence and impact, similar to how Ron Paul did over time.
Raises the question of whether there will be someone to carry on Ron Paul's legacy and if the Democratic Party establishment will allow it to happen.

Actions:

for political enthusiasts,
Analyze and challenge establishment politics (implied)
</details>
<details>
<summary>
2019-01-28: Let's talk about UNESCO, climate change, and arguing against your own interests.... (<a href="https://youtube.com/watch?v=xKFlY09pCNc">watch</a> || <a href="/videos/2019/01/28/Lets_talk_about_UNESCO_climate_change_and_arguing_against_your_own_interests">transcript &amp; editable summary</a>)

Beau questions the US withdrawal from UNESCO, challenges climate change skeptics, and urges individuals to reconsider their resistance to environmental initiatives.

</summary>

"You're arguing against your own interests."
"The propaganda is that thick."
"Blows my mind."
"It's insane."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Expresses shock and disappointment at the United States' withdrawal from UNESCO, an organization that registers and protects historical and cultural sites.
Questions the reasons behind the decision to withdraw, considering possible justifications like taxation, anti-Semitism, and climate change.
Outlines the significance of UNESCO's focus on climate change due to the impact of rising sea levels on historical sites near waterways.
Criticizes arguments against climate change, pointing out the influence of propaganda funded by oil companies and the importance of sustainable practices like stopping deforestation and using renewable energy.
Challenges individuals to think about why they resist efforts to create a cleaner and more sustainable world, pointing out the contradiction in arguing against their own interests.
Attributes political resistance to environmental initiatives to campaign contributors' financial interests, leading to a detrimental impact on global efforts for conservation and sustainability.

Actions:

for climate activists, sustainability advocates, concerned citizens.,
Join environmental organizations to support conservation efforts (exemplified)
Advocate for sustainable practices in your community (exemplified)
Educate others about the importance of renewable energy and cutting pollution (exemplified)
</details>
<details>
<summary>
2019-01-26: Let's talk about restoring democracy down south.... (<a href="https://youtube.com/watch?v=5L5QrZG2eeM">watch</a> || <a href="/videos/2019/01/26/Lets_talk_about_restoring_democracy_down_south">transcript &amp; editable summary</a>)

Beau stresses the dangers of U.S. involvement in Venezuela, warning of a potential civil war and urging resistance to save lives.

</summary>

"No U.S. involvement. No U.S. involvement in Venezuela."
"This isn't partisan politics. This will save lives. This will save lives."
"If you've got one of those raised fist profile pictures, now's the time."

### AI summary (High error rate! Edit errors on video page)

Talks about the need to restore democracy in the southern regions.
Mentions the long and complicated history of U.S. involvement in South American affairs.
Points out the significance of who the intelligence community brings in to run operations as an indicator of their plans.
Mentions past operations in Nicaragua, El Salvador, and Iran involving heavy hitters who turned a blind eye to atrocities.
Calls out Elliot Abrams, who was involved in covering up mass murders in the past.
Expresses concern over Abrams being tasked with operations in Venezuela, signaling trouble ahead.
Emphasizes that this is not a Trump operation but an intelligence community one.
Warns about the potential for a civil war in Venezuela and the grim outcomes it may bring.
Draws parallels with Syria's situation and questions the organic nature of protests against Assad.
Urges resistance against U.S. involvement in Venezuela to prevent further devastation and loss of lives.

Actions:

for activists, advocates, concerned citizens,
Resist U.S. involvement in Venezuela (suggested)
Spread awareness about the situation in Venezuela and the risks of intervention (implied)
</details>
<details>
<summary>
2019-01-25: Let's talk about Venezuela.... (<a href="https://youtube.com/watch?v=aJkDnb7SLUk">watch</a> || <a href="/videos/2019/01/25/Lets_talk_about_Venezuela">transcript &amp; editable summary</a>)

Beau questions the US motives behind the Venezuela coup, exposing economic interests and criticizing America's hypocrisy in foreign interventions.

</summary>

"Democracy has nothing to do with it."
"U.S. doesn't care about human rights."
"It's normally about money."
"There is no national emergency at the border. That's a lie."
"Maybe this isn't our job."

### AI summary (High error rate! Edit errors on video page)

Explains the US attempt to start a coup in Venezuela for regime change.
Questions the rationale behind preserving democracy by recognizing unelected leaders.
Debunks the idea of rigged elections due to low voter turnout in Venezuela.
Reveals CIA's documented plans for regime change in Venezuela pre-dating the 2018 election.
Challenges the justification of US intervention based on human rights violations.
Points out the inconsistency in US foreign policy regarding human rights concerns.
Suggests that economic interests, specifically oil reserves, are the likely driving force behind the coup.
Warns about the potential refugee crisis and destabilization that could result from a successful coup.
Criticizes the false narrative of a border crisis used by Trump to energize his base.
Questions the validity of collapsing Venezuela's economy to criticize socialism.
Calls for a reflection on America's hypocrisy in meddling in other countries while condemning foreign influence.

Actions:

for activists, policymakers, voters,
Protest US intervention in Venezuela (suggested)
Support organizations aiding refugees (exemplified)
Educate others on US foreign policy inconsistencies (implied)
</details>
<details>
<summary>
2019-01-24: Let's talk about honor killings.... (<a href="https://youtube.com/watch?v=J_PBiU3KIXY">watch</a> || <a href="/videos/2019/01/24/Lets_talk_about_honor_killings">transcript &amp; editable summary</a>)

Beau sheds light on honor killings, linking them to toxic masculinity and violence in modern society.

</summary>

"It's an honor killing."
"This is toxic masculinity."
"We need to call it what it is because this is toxic masculinity and that's the problem."

### AI summary (High error rate! Edit errors on video page)

Explains honor killings where a woman is killed for disgracing a man, often due to infidelity.
Shares the story of Tanya Lynn, a victim of an honor killing in Georgia.
Mentions the Supreme Court of Georgia granting the murderer a retrial over insufficient testimony about the victim's alleged infidelity.
Points out that the court believed more testimony on her infidelity might alter the verdict.
Emphasizes that the fact of the murder and body dumping were not in dispute.
Reveals alarming statistics on women killed by intimate partners in the U.S.
Connects honor killings to toxic masculinity and the view of women as property.
Criticizes the use of sanitized terms like "intimate partner violence" instead of calling it what it is: honor killings.
Condemns the idea that murdering a woman for infidelity is somehow justifiable.
Stresses the link between toxic masculinity and violence, especially in cases like honor killings.

Actions:

for advocates against gender-based violence.,
Advocate for proper terminology in addressing gender-based violence (suggested).
Support organizations working to combat toxic masculinity and violence against women (implied).
</details>
<details>
<summary>
2019-01-24: Let's talk about foreign languages, an army training film, and the MAGA teen.... (<a href="https://youtube.com/watch?v=DBHxYV9HbNg">watch</a> || <a href="/videos/2019/01/24/Lets_talk_about_foreign_languages_an_army_training_film_and_the_MAGA_teen">transcript &amp; editable summary</a>)

Beau underscores the importance of speaking the audience's "language," choosing the right messenger, and being wary of initial perceptions in communication to combat misinformation effectively.

</summary>

"Sometimes it may not be that you're constructing a poor argument. It may be that you're just not the person that can deliver it to the audience."
"The way something is presented is often times more important than the information presented in it."
"Messaging is important and the way things are perceived a lot of times can be more important than the facts."
"If we're fighting fake narratives, fake emergencies, we can't succumb to the same things."
"The reaction wasn't because of the message, but because of the language used when it was first circulated."

### AI summary (High error rate! Edit errors on video page)

Emphasizes the importance of speaking the same "language" as the audience to aid communication and understanding, especially for activists and academics.
Recounts an old army training film where a new lieutenant's attempts to communicate with his troops are hindered by a lack of understanding and respect for rank.
Describes the significance of a first sergeant in the army and how his authority can impact communication and decision-making.
Illustrates the importance of finding the right messenger to effectively deliver a message, even if the message itself is well-constructed.
Mentions the need for considering presentation and perception in communication, citing examples of viral incidents where initial perceptions shifted with more information.
Warns against jumping to conclusions based on initial viral content, pointing out the power of messaging and its ability to shape perception.

Actions:

for communicators, educators, activists,
Find the right messenger for your message (exemplified)
Tailor your communication to your audience's understanding (exemplified)
Be cautious of initial perceptions and viral content (exemplified)
</details>
<details>
<summary>
2019-01-24: Let's talk about Legos, Minecraft, and masculinity.... (<a href="https://youtube.com/watch?v=2hmbhdbkj0U">watch</a> || <a href="/videos/2019/01/24/Lets_talk_about_Legos_Minecraft_and_masculinity">transcript &amp; editable summary</a>)

A father mocks kids building robots, but creativity trumps mockery in masculinity.

</summary>

"Your kid's got a pretty valuable skill there already developed, and you're mocking it."
"You might want to get on Twitter and apologize to your son and all the other kids that got caught in a crossfire."

### AI summary (High error rate! Edit errors on video page)

Describes a man who live-tweeted mocking kids building LEGOs and robots at a competition.
Points out the man's behavior as mocking, with backlash on Twitter.
Shares a personal story of his own son building a temporary castle without plans or diagrams.
Acknowledges LEGO building as nerdy but praises the creativity and skill involved.
Notes the irony of the man mocking kids for their creative accomplishments.
Criticizes the man's actions as unmanly and suggests apologizing to his son and the other kids.

Actions:

for parents, educators, twitter users,
Apologize to your child and other kids on Twitter for mocking their creativity (suggested).
</details>
<details>
<summary>
2019-01-21: Let's talk about Trump, dreamers, and hostages.... (<a href="https://youtube.com/watch?v=SQeaeDzubbI">watch</a> || <a href="/videos/2019/01/21/Lets_talk_about_Trump_dreamers_and_hostages">transcript &amp; editable summary</a>)

President Trump's negotiation tactics with Dreamers and TPS recipients are likened to that of a criminal, lacking a clear path to citizenship and causing disruption.

</summary>

"The President of the United States is acting like a criminal, and he's using the guns of federal law enforcement as his soldiers in the street."
"We need to establish a path to citizenship for these people."
"It's extortion."
"The President knows they're not a detriment, the President knows they're not a threat, but that will not stop him from destroying their lives to get what he wants."
"Not disrupt the lives of federal employees, of people in need, of everybody."

### AI summary (High error rate! Edit errors on video page)

President Trump's negotiation tactics involve offering protection for Dreamers in exchange for $5.7 billion to fund the border wall.
Dreamers, or DACA recipients, are not a threat and are deeply ingrained in American culture.
Trump's offer of three years of protection to Dreamers and TPS recipients seems arbitrary and lacks a path to citizenship.
Trump's approach is compared to that of a thug or terrorist, using hostages to achieve his goals.
The President's actions are seen as criminal and embarrassing, especially when leveraging federal law enforcement.
Congress and the Senate are criticized for not calling out Trump's behavior.
Beau advocates for establishing a clear path to citizenship for Dreamers and TPS recipients.
The political tactics used by Trump are condemned as disruptive and detrimental to society.
Beau points out the flaws in Trump's approach and urges adherence to constitutional procedures.
The transcript concludes with a call for action and a reminder to stay safe.

Actions:

for advocates, activists, voters,
Advocate for establishing a clear path to citizenship for Dreamers and TPS recipients (implied)
Call out political figures who do not condemn detrimental actions (implied)
</details>
<details>
<summary>
2019-01-18: Let's talk about armed teachers.... (<a href="https://youtube.com/watch?v=1o1l2LQGyP8">watch</a> || <a href="/videos/2019/01/18/Lets_talk_about_armed_teachers">transcript &amp; editable summary</a>)

Beau addresses the impracticality and risks of arming teachers, stressing the intense training needed for close-quarters combat and the need for better solutions.

</summary>

"It's a Band-Aid on a bullet wound at best."
"You're talking about close quarters combat in incredibly, incredibly difficult scenarios."
"You alone as an individual, as a teacher, as an educator, you have to become the expert in combat."
"Liability and effectiveness don't go hand in hand. This is America today."
"This isn't going to solve the problem. It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Beau addresses the topic of armed teachers in schools, describing it as a Band-Aid solution at best for a serious issue.
Training teachers to use firearms effectively in active shooter situations is deemed impractical due to lack of resources and intense training requirements.
The dynamics of a teacher being in a live shooting scenario are discussed, focusing on the high stakes and intense pressure to protect students.
Suggestions are made for creating mobile cover using filing cabinets with steel plates and implementing frangible ammo to reduce risks to students.
Beau underlines the psychological impact on teachers and the unrealistic expectations placed on them in such scenarios.
He advises teachers to realistically gauge their ability to handle the responsibilities and risks associated with being armed in a school setting.
The transcript concludes with an emphasis on exploring alternative solutions and setting better examples rather than relying on arming teachers.

Actions:

for teachers, educators,
Build mobile cover using filing cabinets with steel plates (exemplified)
Implement frangible ammo to reduce risks to students (exemplified)
Realistically gauge your ability to handle the responsibilities and risks associated with being armed in a school setting (implied)
</details>
<details>
<summary>
2019-01-16: Let's talk about toxic masculinity and men's movements.... (<a href="https://youtube.com/watch?v=APoQuX5M2P4">watch</a> || <a href="/videos/2019/01/16/Lets_talk_about_toxic_masculinity_and_men_s_movements">transcript &amp; editable summary</a>)

Beau delves into toxic masculinity, the mythopoetic men's movement, and the glorification of violence, challenging perceptions of masculinity and the need for introspection.

</summary>

"Not saying hello can get a woman killed."
"Masculinity is not a team sport."
"Men are more violent. That's fact."

### AI summary (High error rate! Edit errors on video page)

Exploring toxic masculinity and its connection to men's movements.
Men feeling marginalized by feminism and the separation from their boys.
Emotional damage caused by constant accusations of sexism like the Me Too movement.
The mythopoetic men's movement in the 80s and 90s aimed to reclaim slipping masculinity.
Crediting the mythopoetic movement with reintroducing rites of passage and coining the term "toxic masculinity."
Toxic masculinity distinguishing hyper-masculine immature behavior from deep masculine qualities.
Concerns about men becoming hyper-masculine or feminized.
Glorification of violence leading to men wanting to use violence for everything.
Loss of ideals associated with masculinity and the glorification of violence.
Real masculinity involving introspection, knowing oneself, honor, and integrity.

Actions:

for men, activists, allies,
Join organizations promoting healthy masculinity (exemplified)
Educate others on the importance of introspection and knowing oneself (implied)
Challenge toxic masculinity in everyday interactions (exemplified)
</details>
<details>
<summary>
2019-01-16: Let's talk about that Gillette ad.... (<a href="https://youtube.com/watch?v=TinZ63kHgUE">watch</a> || <a href="/videos/2019/01/16/Lets_talk_about_that_Gillette_ad">transcript &amp; editable summary</a>)

Beau tackles misconceptions around masculinity and rites of passage, championing honor, integrity, and resilience in the face of discomfort and societal change.

</summary>

"My 12-year-old boy was more of a man than the people that are upset with this because he didn't identify with the bad guy."
"If you felt attacked by that ad, it says more about you than it does Gillette."
"They were creating warriors, real warriors."
"Our boys need to be tough like that."
"Any worthy goal is going to require you to suffer."

### AI summary (High error rate! Edit errors on video page)

Beau starts by addressing the internet people and delves into discussing razors, sparked by a Gillette ad his son brought up.
His son's reaction to the ad made him proud, as it showcased his understanding of the ad's message against sexual assault, belittling, bullying, and violence.
Beau asserts that the ad wasn't an attack on masculinity but a call for positive traits like honor and integrity.
He questions why some men didn't see themselves as the ones stepping forward in the ad's message.
Beau expresses surprise at feminists' discontent with a corporation leveraging feminism for sales, but he views the ad as promoting real masculinity more than feminism.
Addressing objections about a razor company entering social issues, Beau defends Gillette's role in sparking a necessary societal debate.
He contrasts modern rites of passage like shaving with historic brutal rituals that aimed to foster honor and integrity in men.
Beau draws parallels between ancient rites of passage, like the agoghi, and the values portrayed in the Gillette ad.
He describes a contemporary Amazonian rite where boys endure painful bullet ant bites to prove their manhood, contrasting it with American reactions to discomfort.
Beau concludes by praising his son's maturity in understanding the ad's message, juxtaposing it with the fragile masculinity displayed by those upset with it.

Actions:

for men, feminists, society,
Start a constructive community debate on modern masculinity (suggested)
Teach positive values like honor and integrity to young boys (implied)
Challenge toxic masculinity in personal interactions (implied)
</details>
<details>
<summary>
2019-01-14: Let's talk about foodstamps and gardening.... (<a href="https://youtube.com/watch?v=l4WTeesiFiw">watch</a> || <a href="/videos/2019/01/14/Lets_talk_about_foodstamps_and_gardening">transcript &amp; editable summary</a>)

Beau dispels myths about food stamps, encourages urban gardening using SNAP benefits, and advocates for community-building through growing food.

</summary>

"If you are doing well in this system, you might just want to eat this one."
"Every little bit helps in that regard."
"Once you're locked into that cycle of poverty, it's very hard to get out."
"It's a thought."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Stumbled upon the option to use food stamps for purchasing items like grapevines, fruit trees, and seeds, leading to a positive response from people on social media.
Addresses misconceptions about food stamps, with over 10% of Americans using them, and the average household receiving around $250 a month.
Debunks stereotypes around food stamp users, with about half being working families and the program not solely for "lazy" individuals.
Challenges common complaints about food stamp usage, like the misconception that recipients only buy junk food.
Points out that the prevalence of food stamps is due to a broken system and deflects blame onto marginalized groups rather than those responsible for the system's flaws.
Warns of potential unrest if food stamps were eliminated due to hunger-induced desperation.
Encourages growing food using food stamps, mentioning the ability to purchase items like seeds and fruit trees, especially beneficial for those living in cities with limited space.
Offers creative solutions for urban gardening, such as vertical gardening and utilizing unused spaces like shoe organizers or community centers.
Suggests collaborating with others to set up community gardens or growing food on others' properties with permission.
Emphasizes the long-term benefits of growing food, acknowledging the time investment before reaping the produce and the potential stability it provides for those facing poverty.
Acknowledges the challenges of climate and varying growing conditions, suggesting different options such as container gardening or raised beds based on one's location.
Advocates for using gardening not only to enhance food security and reduce dependence but also to strengthen community ties and potentially improve one's situation.

Actions:

for urban dwellers, snap recipients,
Utilize vertical gardening techniques to maximize space (exemplified).
Collaborate with a community center or church for a communal garden (suggested).
Seek permission to grow food on someone else's property (suggested).
Experiment with container gardening or raised beds based on your location (exemplified).
</details>
<details>
<summary>
2019-01-12: Let's talk about the wall gofundme's failure, math, and Israel's wall.... (<a href="https://youtube.com/watch?v=NdcseSQWdlA">watch</a> || <a href="/videos/2019/01/12/Lets_talk_about_the_wall_gofundme_s_failure_math_and_Israel_s_wall">transcript &amp; editable summary</a>)

Beau breaks down the flawed math and misconceptions behind the wall GoFundMe, revealing Americans' strong opposition to the wall and questioning its feasibility based on Israel's example.

</summary>

"Americans don't want the wall. They know that it's morally wrong, ethically wrong, legally wrong, economically wrong, and mathematically wrong."
"If just a third of the people in the United States wanted the wall, it could have raised more than a hundred million dollars, but it didn't."
"The reason the GoFundMe was an utter failure other than its complete lack of understanding of how the country works is the fact that Americans don't want the wall."
"I think most Americans shudder at the idea of a foreign head of state looking at our president in 20 years going, Mr. President, tear down this wall."
"Their wall is not a line that can be bypassed. Their wall surrounds their opposition."

### AI summary (High error rate! Edit errors on video page)

Explains the situation with the wall GoFundMe being refunded.
Questions the intention behind redirecting donations to a new non-profit.
Criticizes the misunderstanding that donations could dictate government actions.
Breaks down the math behind the raised funds, exposing the minimal percentage in relation to the wall cost.
Asserts that the lack of funding does not indicate widespread support for the wall.
Emphasizes Americans' opposition to the wall on moral, ethical, legal, and economic grounds.
Draws parallels between the proposed US wall and Israel's wall, focusing on differences in design and purpose.
Argues that Israel's wall, often cited as a success, has not effectively stopped terrorism.
Concludes by questioning the feasibility and effectiveness of a similar wall in the US.

Actions:

for american citizens,
Educate others on the inaccuracies surrounding the wall GoFundMe and the implications of supporting such initiatives (implied).
</details>
<details>
<summary>
2019-01-11: Let's talk about teaching techniques.... (<a href="https://youtube.com/watch?v=r5j6-JgegR8">watch</a> || <a href="/videos/2019/01/11/Lets_talk_about_teaching_techniques">transcript &amp; editable summary</a>)

Beau illustrates a story of transformation from anti-gun to AK-47 purchase, discussing effective teaching techniques and bridging ideological divides through information-based, non-confrontational discourse.

</summary>

"Ideas and information stand and fall on their own."
"Those who have seen the most say the least."
"Make it a conversation rather than a debate."
"Never assume anything about anybody."

### AI summary (High error rate! Edit errors on video page)

Illustrates the story of someone going from being anti-gun to purchasing an AK-47 after a synagogue shooting.
Describes a teaching technique used by a sixty-something year old man to effectively teach shooting skills.
Emphasizes the importance of good teaching techniques, allowing time to digest information, and focusing on the information itself.
Reveals that the instructor who taught shooting skills worked for the Israeli intelligence agency, Mossad.
Advocates for using similar techniques to bridge partisan and ideological divides, focusing on information and making it a non-confrontational academic experience.

Actions:

for educators, activists, communicators,
Reach out across partisan lines by engaging in constructive, fact-based dialogues (exemplified)
Avoid assumptions about others and approach interactions with an open mind (exemplified)
</details>
<details>
<summary>
2019-01-09: Let's talk about the thing you missed in Trump's speech.... (<a href="https://youtube.com/watch?v=uebiR0Avstg">watch</a> || <a href="/videos/2019/01/09/Lets_talk_about_the_thing_you_missed_in_Trump_s_speech">transcript &amp; editable summary</a>)

Beau challenges the importance of swearing an oath to defend the Constitution over the country itself, advocating for a society where government becomes unnecessary and individuals actively speak out against corruption and injustice.

</summary>

"When fascism comes to the United States, it'll be wrapped in a flag and carried across."
"Government, even in its best state, is but a necessary evil."
"Getting rid of government is not destroying the Constitution. It's carrying it to its logical conclusion."

### AI summary (High error rate! Edit errors on video page)

Beau introduces a different topic, avoiding the expected, and aims to bring original ideas or concepts to the table through his videos.
He criticizes Trump's speech where he claimed to swear an oath to protect the country, pointing out the importance of swearing to defend the Constitution instead.
Beau stresses that the Constitution's ideas must outlive the existence of the United States itself.
He praises the revolutionary aspects of the Constitution, particularly the idea of a government by the people and its adaptability through amendments.
Beau acknowledges the flawed nature of the Founding Fathers, like Thomas Jefferson, who despite owning slaves, recognized the moral depravity of slavery.
Emphasizes that the Constitution is a living document and expresses his idealistic view of a world where government becomes unnecessary.
Beau quotes Thomas Paine on government being a necessary evil and advocates for moving towards a society where government is no longer needed.
He calls on people to use their voices to speak against injustices and corruption in government, implying a need for activism and vigilance from citizens.

Actions:

for citizens, activists,
Speak out against injustices and corruption in government by using your voice (implied)
Advocate for change through activism and vigilance in upholding democratic values (implied)
</details>
<details>
<summary>
2019-01-08: Let's talk about that planned national emergency.... (<a href="https://youtube.com/watch?v=-tUWLfIiseI">watch</a> || <a href="/videos/2019/01/08/Lets_talk_about_that_planned_national_emergency">transcript &amp; editable summary</a>)

Beau outlines the dangers of paralleling Hitler's actions with Trump's national emergency declaration, urging a choice between supporting the U.S. Constitution or backing authoritarian measures.

</summary>

"Either you support the constitution of the United States or you betray it."
"You're a patriot or a traitor."
"Circumventing Congress, it's a betrayal."
"There is no way to support both anymore."
"The choice is simple, you can support the constitution of the United States or you can support Mango Mussolini."

### AI summary (High error rate! Edit errors on video page)

Border apprehensions along the southern border are at a 40-year low.
The Constitution of the United States outlines a process for funding projects.
Trump's rise to power and his promises of prosperity and military supremacy are discussed.
A comparison is drawn between Hitler's actions and Trump declaring a national emergency.
Fascism is defined by 14 specific elements, including corporativismo and disdain for human rights.
The importance of checks and balances in the U.S. Constitution is emphasized.
Congress serves as a vital checkpoint against tyranny and dictatorship.
The potential consequences of Trump's national emergency declaration are explored.
A choice is presented between supporting the Constitution or backing Trump's actions.
The audience is urged to choose between being a patriot or a traitor in relation to current political events.

Actions:

for americans,
Support the U.S. Constitution by upholding democratic values and principles (implied).
Take a stand against authoritarian actions by advocating for checks and balances within the government (implied).
Choose to be informed and engaged in defending constitutional rights in the face of potential abuses of power (implied).
</details>
<details>
<summary>
2019-01-07: Let's talk about how to change the world  (Part 3).... (<a href="https://youtube.com/watch?v=BVjSHtSVeLY">watch</a> || <a href="/videos/2019/01/07/Lets_talk_about_how_to_change_the_world_Part_3">transcript &amp; editable summary</a>)

Beau explains how increasing income and leveraging networks are key to financial independence and making a difference.

</summary>

"Increasing the amount of money you have coming in is the solution to financial independence."
"Sometimes the skills of the entire network complement each other."
"You want to break away from that system, you don't have to be employed by somebody else."
"A lot of these huge companies that everybody knows were started in somebody's garage."
"This series will continue, but we got to take a break, because there's a whole bunch of stuff in the news we got to talk about."

### AI summary (High error rate! Edit errors on video page)

Part three of how to change the world focuses on money and the resources needed to make a difference.
Individuals of limited means are most aware of the failings of the system but may lack the resources to act.
Often, advice given to those with limited means revolves around cutting out creature comforts to save money.
Saving small amounts like $50 a month may not significantly change one's financial situation in the long run.
Increasing the amount of money coming in is key to financial independence.
Leveraging networks and recommendations can lead to opportunities for additional income.
Building a reputation within your community can open doors for new income streams.
Starting low or no-cost businesses can be a way to increase income over time.
Networks can complement each other, leading to collective success and financial gains.
Windfalls like tax returns can be invested in new income-generating ventures.

Actions:

for community members seeking financial independence.,
Support local businesses and individuals in your community by recommending their services (exemplified).
Invest in someone's venture within your network to help them get started (exemplified).
</details>
<details>
<summary>
2019-01-05: Let's talk about #AOC.... (<a href="https://youtube.com/watch?v=yKPClC6O4Lo">watch</a> || <a href="/videos/2019/01/05/Lets_talk_about_AOC">transcript &amp; editable summary</a>)

Beau asks about The Breakfast Club, appreciates youthful expression, and humorously comments on the portrayal of the First Lady, ending with a light-hearted farewell.

</summary>

"Congratulations to the Republican Party that has spent so much time and energy casting this woman as this evil foreign socialist who comes from money."
"With one video, she has been turned into the most adorable woman in the country."
"Y'all have a nice night."

### AI summary (High error rate! Edit errors on video page)

Beau introduces himself and mentions it is part three.
Beau calls out to his dad and asks about The Breakfast Club movie.
Beau mentions watching a video of a congressperson dancing online.
Beau appreciates how people express themselves when they were younger.
Beau and his dad talk about people evolving as they get older.
Beau jokingly asks about information on the First Lady when she was younger.
Beau congratulates the Republican Party for portraying the First Lady positively in a video.
Beau implies that people are only angry because of the First Lady's appearance and energy.
Beau concludes with a light-hearted farewell message.
The transcript captures a casual and humorous interaction between Beau and his dad.

Actions:

for internet users,
Appreciate people's expressions when they were younger (implied)
Share positive videos to counter negative portrayals (implied)
</details>
<details>
<summary>
2019-01-04: Let's talk about how to change the world (Part 2).... (<a href="https://youtube.com/watch?v=qlHhJ6XCjFw">watch</a> || <a href="/videos/2019/01/04/Lets_talk_about_how_to_change_the_world_Part_2">transcript &amp; editable summary</a>)

Beau explains building community networks for independence, inspired by NATO's Stay Behind Organizations, urging self-evaluation and diverse contributions to form resilient networks.

</summary>

"Anything is possible if you wish hard enough, at least that's what Peter Pan said."
"Everybody has something to offer a network like this."
"Rule 303, if you have the means at hands you have the responsibility."
"Those people look for the helpers, you know."
"Normal people answering questions in the comment section, that's fantastic."

### AI summary (High error rate! Edit errors on video page)

Talks about the importance of building community-based networks to achieve independence.
Mentions NATO's Stay Behind Organizations during the Cold War.
Explains the idea of forming networks before crises to effectively carry out operations.
Describes the diverse roles within these networks, ranging from military to civilian.
Emphasizes the logistics involved in running resistance operations.
Mentions the Gladiou network as an example that went off track but still proved the concept's effectiveness.
Encourages self-evaluation to determine what skills or expertise one can offer to start a network.
Gives examples of how people of different ages can contribute to these networks.
Acknowledges concerns from introverts and suggests ways for them to participate.
Talks about recruiting members for the network and different methods to do so.
Suggests starting with immediate circles and expanding through social media and local groups.
Shares ideas on structuring the organization, including barter systems and communal commitments.
Encourages community service as a way to attract like-minded individuals to the network.

Actions:

for community members,
Start by evaluating what skills or expertise you can offer to begin building a community network (suggested).
Reach out to immediate circles like friends, co-workers, or local groups to recruit members for the network (exemplified).
Engage in community service activities to attract like-minded individuals to join the network (implied).
</details>
<details>
<summary>
2019-01-02: Let's talk about how to change the world.... (<a href="https://youtube.com/watch?v=9TeuLkOiXVc">watch</a> || <a href="/videos/2019/01/02/Lets_talk_about_how_to_change_the_world">transcript &amp; editable summary</a>)

Beau breaks down the cycle of societal control, urging conscious rebellion and community-building for lasting change and independence.

</summary>

"Being independent doesn't actually mean doing everything by yourself. It means having the freedom to choose how it gets done."
"If you make your community strong enough, it doesn't matter who gets elected."
"You're changing the world you live in by building your community."

### AI summary (High error rate! Edit errors on video page)

Beau starts with the introduction to a series on changing the world, comparing it to a previous video about guns, and underlines the importance of the information he is about to share.
The inspiration for this video comes from a genuine comment on Facebook expressing the need for information to help others, leading to a reflection on breaking the cycle of societal control mechanisms.
Drawing parallels to George Orwell's "1984," Beau explains the need to become conscious to rebel against societal constraints and restrictions.
He shares his personal experience of needing help and the importance of building a network to achieve independence and assist others.
Beau acknowledges the challenge of feeling isolated and encourages finding like-minded people to form a supportive network.
Addressing single parents, Beau stresses that everyone has something valuable to offer in building a community network, regardless of their circumstances.
He expresses the idea of force multiplication through community networks and the power of collective strength in creating resilient communities independent of political influences.
Beau rejects the idea of running for office and advocates for bottom-up community building over top-down leadership as a more effective way to bring about positive change.
Recognizing the diversity of situations, Beau hints at discussing tailored approaches for different socio-economic backgrounds in the upcoming videos.
Ending with a reference to Orwell's "1984" and a message of hope in community strength, Beau signs off with a thought-provoking note.

Actions:

for community members,
Build a supportive network by reaching out to like-minded individuals (implied).
Offer your skills and resources to contribute to community building (implied).
Encourage single parents to involve their children in community activities to network effectively (implied).
</details>
<details>
<summary>
2019-01-01: Let's talk about that cop getting killed.... (<a href="https://youtube.com/watch?v=ZMULqDaRCwM">watch</a> || <a href="/videos/2019/01/01/Lets_talk_about_that_cop_getting_killed">transcript &amp; editable summary</a>)

Beau addresses the focus on officers' immigration status over their heroism, challenges the narrative on immigrant crime, criticizes bias in studies, raises issues of race, and urges against using graves for political points.

</summary>

"Stop standing on the graves, the corpses, the flag-covered coffins of people far more honorable than you to make a political point they might not agree with."
"You don't know the names of those cops, more than likely."
"People far more honorable than you."

### AI summary (High error rate! Edit errors on video page)

Mentions the names of cops killed in the last 60 days by gunfire, some heroes like Sergeant Helles and Corporal Singh.
Talks about how headlines focus on an officer's immigration status rather than their heroic actions.
Compares the narrative around immigrant crime to the Molly Tibbets case and the flawed logic of holding entire demographics accountable for one person's actions.
Criticizes the inconsistency in applying laws and concerns about bias in studies from organizations like FAIR and CIS.
Raises the issue of race, pointing out the bias against brown people and the lack of outrage for officers like Officer Smith who was recently killed.
Draws parallels between the lack of attention to certain military incidents based on political narratives and agendas.
Urges people to stop using the graves of honorable individuals for political points.

Actions:

for activists, community members,
Challenge biased narratives (implied)
Support community policing efforts (implied)
</details>
