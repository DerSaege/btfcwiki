---
title: Let's talk about the America I remember....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=GZm505MCvyk) |
| Published | 2019/07/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the generational gap in perceptions of America, with older generations remembering a sanitized version learned from textbooks and younger generations exposed to raw, unfiltered truths from the internet.
- Older generations learned history through government-approved textbooks, while younger generations have freer access to information and primary sources.
- The shock of discovering America's darker history, like mistreatment of natives and slaves, challenges the traditional narrative of America as the "good guy."
- The younger generation still values the ideals of America, such as equality and freedom, but questions whether the future will embrace the raw, unfiltered truth or a sanitized version of history.
- Beau contrasts the desire for America to be an EMT (Emergency Medical Technician) globally instead of a police force, helping people achieve freedom genuinely.
- Acknowledging the transformative impact of the internet on how history is learned and understood, Beau recognizes the power held by younger generations in shaping America's future.

### Quotes

- "They didn't learn history, not the way we did. They didn't get it from a thick government-approved textbook, a sanitized version."
- "We can actually have the American dream because they got the truth. We know where we're going wrong."
- "The future of this country rests in those kids and what they decide to do, those kids, they're not all kids, be younger generations."
- "America will stand or fall based on the actions of these generations that a lot of us are making fun of."
- "They're not going to sell out their country for a red hat and a stupid slogan."

### Oneliner

Exploring generational perceptions of America through history education: sanitized textbooks versus raw, unfiltered truths, questioning the future based on embracing true history or idealized versions.

### Audience

Educators, Activists, Students

### On-the-ground actions from transcript

- Advocate for inclusive and accurate history education in schools (implied)
- Support initiatives that provide access to primary sources and unfiltered information about historical events (implied)
- Encourage critical thinking and questioning of traditional narratives about America's history (implied)

### Whats missing in summary

The full transcript provides a deep dive into the impact of generational differences in understanding America's history and its implications for the future.

### Tags

#GenerationalGap #HistoryEducation #AmericanDream #RawTruth #FutureLeaders


## Transcript
Well, howdy there, internet people, it's Bo again.
Tonight we're going to talk about the America I remember.
It's a phrase we're hearing a lot from people my age and older.
This isn't the America I remember.
I don't recognize America anymore.
You're not hearing it from people younger than me.
I think I figured out why.
People my age and older, we still
We've got a little bit of that Cold War propaganda, you know, leader of the free world versus
the evil empire, but we're good guys.
We are the good guys.
We learned our history in schools, learned about how great we were in those textbooks,
those big, thick textbooks.
They didn't get that.
They didn't get their propaganda, what they grew up with, finding out that they had been
lied to about Iraq, watching their friends die.
And they didn't learn history, not the way we did.
They didn't get it from a thick government-approved textbook, a sanitized version.
They got it from the internet, raw.
That's why when we say, you know, I don't recognize America,
I can't believe we're taking kids from families.
And they show up, really?
Because here's a picture of us doing it to natives.
Here's a picture of us doing it to slaves.
No, this is as American as apple pie, and it's a shock.
It is a shock.
We wouldn't have concentration camps.
I can't believe this.
Here are the Japanese internment camps.
Here's what we did to the natives.
Yeah.
They didn't grow up with the presumption that the United
States was the good guy.
That's the difference.
government-issued textbook wasn't as important.
They had freer access to information.
They could get to the raw, unfiltered truth.
But see, the interesting part about it is they still want
the promise.
They still want the promise.
They want the huddled masses yearning to breathe free.
They want all men created equal.
They still want this.
And what we have to decide, and what's going to be decided over the next few years,
is are we going to have, in the future, are we going to have the America they know,
the raw, unfiltered, true history?
Or are we going to have that sanitized version?
Are we going to have that ideal?
Because we can have it.
We can be the good guys.
Because we've got a crop that isn't falling for
make America great again.
When they hear that, they're like, when?
When exactly are you talking about right now?
It's real hard to give them an answer.
But we can do it.
We can become the world's EMT instead of the world's police
We can actually help people achieve freedom, rather than saying we're spreading democracy,
but really just keeping them under the thumbs of our large companies.
We can actually have the American dream because they got the truth.
We know where we're going wrong.
You don't hear them say, go back to where you came from.
Not in any large numbers.
You really don't.
The funny thing was that when Trump got elected, you know you have to get to age 50 or older
before he gets a majority of the votes, when you break it up by age, they got to be 50
older before he gets a majority, but below that he didn't have one because they didn't
fall for it.
The older crowd, my age and older, not all, but a lot, looked down on these kids, oh man,
You got that all messed up.
They are the ones that are going to make America great for the first time.
They understand that beyond America's borders do not live a lesser people because they've
interacted with them.
That instant communication, the internet changed everything and we're still coming to terms
with it.
The biggest impact it may have is the fact that Americans are no longer subject to learning
their history through a government-approved textbook that is sanitized, that has all of
the bad stuff taken out.
Just enough left in to make you think that it's truly objective, because that's what
we got.
That's what we got, and that's not something unique to the United States.
One of my most prized possessions is a collection of history books from all over the world.
It's amazing.
The variations on events.
Every country does it.
Every government does it, but they can't do it anymore because those primary sources are
out there. And they're taking advantage of them. They know more about what happened in
this country than most of the people who were alive when it happened. Because they're processing
that information. We make fun of them because they always have their phone in their hand.
y'all keep your phone in your hand, it's doing you a lot of good.
The future of this country rests in those kids and what they decide to do, those kids,
they're not all kids, be younger generations.
What they decide to do over the next few years determines everything.
a scale that I'm not sure that even they get. I don't think they understand. But
America will stand or fall based on the actions of these generations that a lot
of us are making fun of. The good news is it looks like they've got it together.
They're not going to sell out their country for a red hat and a stupid slogan.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}