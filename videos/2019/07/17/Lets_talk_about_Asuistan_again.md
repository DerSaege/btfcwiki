---
title: Let's talk about Asuistan again....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=DX9hYvpMBR8) |
| Published | 2019/07/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing a suicide and discussing overlooked issues within the country.
- The regime in the country is denying international inspectors entrance and visas to ICC staff.
- Threatening judges to dissuade investigations into war crimes in a recent conflict.
- Military recruitment numbers have dropped due to lack of faith in the regime, leading to targeting children as young as 16 for recruitment.
- Children involved in hostilities must be 18 or older according to international law.
- Legislation in some provinces prevents military recruiters from entering campuses, prompting a digital campaign to target children.
- Clashes between protesters, rebels, and security services across the country due to security sweeps targeting a social minority group.
- Horrendous conditions and widespread sexual abuse of the targeted group leading religious institutions to shelter them.
- Centrist party within the government asking the regime's leader, Don Joe Trump, to step down, which is unlikely.
- Beau uses "Assuistan" to demonstrate how media coverage changes when events in the U.S. are portrayed as happening in another nation.
- The U.S. faces issues like targeting child soldiers, refusing international inspectors, disrupting war crimes investigations, and running concentration camps.

### Quotes

- "Assuistan is not a real country."
- "We need to look at it again."
- "In large part because of mass media."
- "It's just a thought, y'all."
- "The targeting of child soldiers, refusing international inspectors, trying to disrupt investigations into our own war crimes..."

### Oneliner

Beau addresses overlooked issues within the U.S., revealing alarming parallels with dictatorships and urging a critical reevaluation of media coverage.

### Audience

Policy makers, activists, citizens

### On-the-ground actions from transcript

- Contact religious institutions for information on how to support and help shelter the targeted group (implied).

### Whats missing in summary

The full transcript provides detailed insights into overlooked issues within the U.S. and the importance of critical media analysis.

### Tags

#US #Government #Media #WarCrimes #ChildSoldiers


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we got to talk about a suicide again.
With everything going on over the world, we've kind of forgotten about this country and we
need to look at it again.
Over the last month, the regime has denied international inspectors entrance to the country.
They refuse to allow ICC, International Criminal Court, staff, visas that allow them to enter
of the country.
They have also threatened judges with the ICC in an attempt to dissuade them from investigating
or prosecuting the nation's war crimes in a recent conflict.
Because of a lack of faith in the regime, in the government of Afghanistan, military
recruitment numbers have fallen.
The response from the regime is to begin targeting children for recruitment.
Children as young as 16.
International law dictates that those involved in hostilities must be 18 years or older.
Provinces within a suicide have enacted legislation that prevents military recruiters from entering
campuses.
So the military has designed a digital campaign to target children.
Over the weekend, protesters and rebels clashed with security services all over the country.
The clashes stem from the security services sweeps targeting a social minority group.
The conditions in which those of that group are held once captured are so horrendous and
the sexual abuse is so widespread that religious institutions within the nation have begun
sheltering those being targeted.
The regime's leader, Don Joe Trump, has been asked to step down by the centrist party within
his own government. It is unlikely that he will do so and he will attempt to maintain
the reigns of power in this nation for as long as possible. In case you haven't figured
that out, Assuistan is not a real country. Assuistan is a way I use to demonstrate how
the media would cover events in the United States if they occurred in any other nation,
particularly those that end in a stand.
All of this stuff is happening in the United States.
This is what's going on.
We need to remember, as we continually say, it can't happen here, that we believe that
in large part because of mass media, and it's just demonstrated, the media doesn't really
do a good job of accurately portraying how similar our government is to the governments
we refer to as tempeh dictatorships.
This is all the U.S.
The targeting of child soldiers, refusing international inspectors, trying to disrupt
investigations into our own war crimes...concentration camps...anyway, it's just a thought, y'all
Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}