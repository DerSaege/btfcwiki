---
title: Let's talk about getting too woke....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=infL_y3DxhU) |
| Published | 2019/07/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of "getting woke" when people see the establishment for what it truly is and start understanding the finer details of control systems, money, and politics.
- Mentions how veterans often experience disillusionment after multiple tours, realizing they are guarding poppy fields for the pharmaceutical industry's profit.
- Notes that the issue arises when this awareness leads to despair, especially when individuals perceive the opposition as overwhelmingly powerful.
- Describes a friend who feels exhausted and stagnant in their efforts, unable to see any progress or victory.
- Observes that the black community frequently faces this phenomenon, understanding the corrupt system but then feeling so woke that they lose hope and fall into depression.
- Emphasizes that while recognizing the system's corruption and power is not problematic, feeling like victory is unattainable is where the real issue lies.
- Encourages the idea of continuous fight and not succumbing to despair, reminding that change may take a long time but it's vital to hold onto the hope for a better world.

### Quotes

- "There's nothing wrong with getting woke."
- "It's okay to get woke, but never get so woke that you can't dream anyway."
- "We don't have to beat them today or tomorrow. We just have to keep fighting."

### Oneliner

Beau explains "getting woke," warning against despair when perceiving the establishment as unbeatable, urging continuous fight for a better world.

### Audience

Activists, Advocates, Community Members

### On-the-ground actions from transcript

- Keep fighting for a better world (implied)
- Maintain hope and vision for change (implied)

### Whats missing in summary

The full transcript provides a deeper insight into the emotional journey of individuals as they transition from awareness to despair, underlining the importance of perseverance and hope in striving for a better future.

### Tags

#Awareness #Despair #Hope #Change #Activism


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight I want to talk about getting woke.
It's a phenomenon that happens when
people begin to see the establishment for what it is
and they start picking up on the finer details of all the different control
systems and
the money and politics and the way things work
and sometimes it goes a little further than that.
I've seen it happen a lot with vets.
They go over, and after their third tour, they're disillusioned, and they're standing
there like, you know, we're guarding poppy fields so the pharmaceutical industry can
make money.
There's nothing wrong with that.
There's nothing wrong with that at all.
The problem comes in when that wokeness turns to despair, because you realize how powerful
that opposition is.
Same thing can happen if you fall down rabbit holes on YouTube watching videos, and you
understand how much money is in politics and how far the establishment has gone, it makes
it seem like the opposition is just unbeatable, you can't see victory.
I have a friend who's the same type of thing that I do, but hit that point where it doesn't
feel like the returns are there anymore.
Been doing it for so long and just not getting anywhere.
Can't see victory.
Same thing, same thing.
And you see it happen a lot in the black community.
they live it. They see the system for what it is on a daily basis. And when they start
learning about it, man, they got it. They understand how it works. But then that line
comes and they cross it. And they're so woke, they don't believe they can win. And it turns
to despair. It turns to depression. There's nothing wrong with getting woke. There's nothing
wrong with being able to identify how corrupt the system is. There's nothing wrong with
realizing that the establishment really is that powerful. It's not the problem. The problem
comes in when, because of that, and you understand the opposition you're facing, that you feel
like you can't win. You can't envision one day things being different. You can't
envision a better world because you're so woke. You see how hard it's going to be
and how long that fight is going to be. We don't have to beat them today or tomorrow.
We just have to keep fighting. And that's the hard part. That idea that this is
gonna take a long, long time. But you can't succumb to the depression. You can't give
up on the idea that one day we can have a better world. It's okay to get woke, but never
get so woke that you can't dream anyway it's just a thought y'all have a good
night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}