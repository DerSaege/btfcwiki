---
title: Let's talk about snapping up cost of living differences.....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=iYpwnRdsdn8) |
| Published | 2019/07/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the SNAP program, which provides food assistance to 40 million Americans, with about half being children.
- Details the income requirements to qualify for the program, including gross and net pay thresholds.
- Points out the discrepancy between the national poverty line and the cost of living across different states.
- Mentions the existence of loopholes in the SNAP program due to these discrepancies.
- Contrasts the basic allowance for housing in the military with the income limits for SNAP recipients.
- Addresses concerns about fraud within the SNAP program, providing statistics on the actual amount of fraud.
- Criticizes the focus on cutting programs like SNAP while ignoring extravagant spending in other areas.
- Accuses Donald Trump of manipulating SNAP numbers for political gain.
- Emphasizes that the real issue is the high number of Americans living at the poverty line.

### Quotes

- "This isn't a loophole. This is Donald Trump doing what Donald Trump does."
- "The issue is we have 40 million Americans living at the poverty line."
- "It's $102 million for the president of the United States to go golfing before we start cutting into programs that might affect children being fed."
- "He wants to be able to say, look at the number of people I got off food stamps. Look how great the economy is."
- "Y'all have a good night."

### Oneliner

Beau explains loopholes in SNAP income requirements, criticizes Trump's manipulation, and stresses the real issue of Americans living in poverty.

### Audience

Advocates, policy influencers

### On-the-ground actions from transcript

- Advocate for accurate representation of poverty levels and support programs assisting those in need (implied).
- Stay informed about political actions affecting social welfare programs (implied).

### Whats missing in summary

Deeper insights into the impact of policy decisions on vulnerable populations.

### Tags

#SNAP #FoodAssistance #Poverty #GovernmentPolicy #SocialWelfare


## Transcript
Well, howdy there, internet people, it's Bo again.
So I guess we're going to talk about food stamp loopholes,
because apparently that's a thing.
Before we get into this, we'll go ahead and hit the big numbers
and explain the SNAP program.
SNAP program provides food assistance
to 40 million Americans, a lot.
About half of that 40 million are children, 44%.
It costs $68 billion a year.
Sounds like a lot of money.
But it is to feed 40 million people.
So what does it take to get on this program?
OK, so your gross pay has to be at 130%
of the national poverty line or below.
The fact that it is a national poverty line
becomes important when we get to this loophole.
So if you are a single mom with two kids,
you can make about $27,000 a year gross.
Your take-home pay, your net, has
to be 100% of the poverty line or below,
which means that single mom with two kids
can bring home about $1,700 a month.
depending on where you live in the United States you're looking at that
number $1,700 a month for a mom and two kids going man that would be really tight
but maybe it could be done if these circumstances were in their favor or
you're looking at it like I don't even know if you can get an apartment here for
because cost of living varies across the United States. That's the loophole. That's
what he wants to close. Some states have adjusted these limits because the
national poverty line does not reflect the cost of living where they are.
That's the loophole. The federal government is extremely aware that there
cost-of-living differences. This is not a loophole, it's a function of design. The
feds have their own system for dealing with cost-of-living adjustments for
their people. All you vets get ready to explain questions about BAH. So the
military has a thing called basic allowance for housing. Now if you are an
E1, and guys I understand an E1 is probably not living off base, but if
If you're in E1, you'll get about $660 a month extra if you're stationed at Fort Rucker,
Alabama.
If you're working up at the Pentagon and you're living in D.C., you will get $1,850 a month
extra.
The feds are very well aware of this issue.
It's not a loophole.
Somebody right now, why are you bringing the military up?
in 2013 there were 23,000 active duty families on SNAP. That's why. So you can
save that their lazy bums routine for somebody else. Okay, so the next thing
that comes up is always, well what about the fraud? Fine, we'll talk about the
fraud too. Let's say one out of ten people on the program are committing
fraud. That's 6.8 billion dollars. You know, I'll do you one better. One out of five. Okay, so 13.7
billion dollars. That's almost as much as we give in welfare to oil and gas companies.
I don't care. I do not care. Now the actual number of money wasted on fraud within SNAP is 590
million dollars. That's pretty low. It's actually one of the least fraud-ridden
federal programs in existence. 592 million. If you were to break that down
per person per month by the number of people in the program to show what it
cost to absorb that fraud? It's $1.23 on $126 per client subsidy. Doesn't really
seem like a lot, does it? Keep in mind, we've paid $102 million for the
president of the United States to go golfing before we start cutting into
programs that might affect children being fed, maybe he should stay off the
golf course just a little bit.
This isn't a loophole.
This is Donald Trump doing what Donald Trump does.
He's cooking the books, cooking the books before the election.
He wants to be able to say, look at the number of people I got off food stamps.
Look how great the economy is.
Simply disqualifying them doesn't actually
change the fact that they're living at the poverty line.
The issue is not the number of people on food stamps
and the cost to the taxpayers.
That's not really the issue.
The issue is we have 40 million Americans
living at the poverty line.
That's the issue.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}