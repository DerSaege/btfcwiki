---
title: Let's talk about the stories we're writing....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=rtkwPBx-qcE) |
| Published | 2019/07/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A country is a collection of stories, shaping its character and reputation.
- The stories written today define the nation's character.
- Recent internet debates suggest a fear of speaking out against a fascist government.
- People are starting to accept oppressive actions instead of questioning them.
- Internet exchanges are becoming part of America's story, potentially showcased in future museums.
- The focus on political pundits and debates overshadows experts' warnings about the dark path ahead.
- The Smithsonian aims to preserve drawings by children from border camps as part of America's history.
- The framing of these events in history will shape future perspectives on the country's actions.

### Quotes

- "A country, a lot like a person, is just stories."
- "That's the character of the nation."
- "The stories that make up the United States' character are being written right now."
- "We're headed down a pretty dark road."
- "What's happening there is part of America's history now."

### Oneliner

A country is shaped by its stories, with current narratives reflecting a potential dark path towards acceptance of oppressive actions.

### Audience

Americans

### On-the-ground actions from transcript

- Preserve and share stories and narratives that challenge oppressive actions in society (implied).
- Support initiatives like the Smithsonian's efforts to document and frame critical events in history (exemplified).

### Whats missing in summary

The full transcript provides deeper insights into the importance of current narratives in shaping a nation's character and history.


## Transcript
Well, howdy there, Internet people, it's Bo again.
You know, a country, a lot like a person, is just stories.
They're just the sum of their collected stories.
That's the character of the nation.
If I was to say which country is most likely to surrender
without a fight, a whole bunch of people right now
are thinking of the French.
and they're all Americans.
Because nowhere else in the world
does France have that reputation,
just with Americans.
Because in our stories,
even though it isn't really deserved,
that's the reputation that came through.
And that shapes the character.
These stories
are being written constantly. They're being written today.
The stories that make up
the United States' character
being written right now. I had a conversation a couple of weeks ago and since then I've
seen it repeatedly in internet talking points basically when people are arguing. He told
me that I need to be careful about what I said and that the fact that I say it is proof
that I don't really believe the country is headed towards fascism because if I believed
that I wouldn't say it, because if we are headed there, well, the government will find
a way to arrest me."
Wow, okay.
Then he went on to do the whole, and think of your kids, okay, yeah, I mean, I suppose
that's true, I suppose that is true, if it gets out of hand, that could happen, I'm not
sure that suggesting people imagine themselves being ripped away from their
families unjustly is going to have the effect you think it's going to have in
this particular case because I mean that's kind of why a lot of people are
getting motivated. But it's a little unnerving because people are already
starting to accept the actions of a fascist government they're already
moving towards well you better not say that or they'll get you rather than the
government is wrong for behaving in that way it's unnerving and that is the story
that is being written right now that's the story that's coming out these little
conversations. They become part of America's collection of stories. I'm
certain that in the future museums will have printouts of internet conversations
that highlight the tone of the country. I'm sure that'll be a thing. You know,
they have letters. If you go to museums today you'll see collections of letters
and those are used to frame the context of things and to help write those
stories so that we can explain later on what was happening in the country while
major events were occurring. You know, we focus today on the political talking
points of the pundits and the politicians and we watch those debates.
We don't watch the debates of the historians or the experts in political violence or the
experts in political science.
We don't watch those debates because there aren't any.
There's a pretty broad consensus that we're headed down a pretty dark road.
And that's the story we're writing.
I think it's probably worth mentioning that right now the Smithsonian is trying to get
their hands on drawings made by children in camps down on the border.
That's already become part of the story.
What's happening there is part of America's history now.
The question is, when it gets framed, how is it going to be framed?
Is it going to be framed as an out of control administration and the people spoke up and
squashed it or is there an out-of-control administration that was
allowed to proceed because people were afraid anyway it's just a thought y'all
have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}