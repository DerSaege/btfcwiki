---
title: Let's talk about adventure, waterboarding, and licking ice cream....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8oWLQsfSgrY) |
| Published | 2019/07/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recalls a past demonstration where he was waterboarded by another person as part of an activist event following the US torture report release.
- Describes how a young man volunteered to be waterboarded during the demonstration despite Beau's skepticism.
- Talks about the importance of adventures in shaping history, especially during times of social upheaval.
- Mentions the need for individuals to choose their own level of involvement in activism.
- Encourages the youth to embrace adventure in service of a greater cause rather than engaging in superficial acts like licking ice cream.
- Emphasizes that impactful change often comes from individuals taking risks and going on adventures.
- Urges people to decide what they are willing to do in terms of activism and not let others dictate their level of involvement.

### Quotes

- "We're Coming into a period in American history that's going to require a lot of adventures."
- "You can change the world, or not."
- "It could be anything from donating to lawyers, to calling your senator, to marching, to organizing a protest, to anything, leafletting, all kinds of stuff."
- "If you let other people do that for you, you're going to end up over your head."
- "Take that adventure in service of something greater than yourself."

### Oneliner

Beau believes embracing adventures in activism can shape history and encourages individuals to choose their level of involvement to create meaningful change.

### Audience

Youth activists

### On-the-ground actions from transcript

- Reach out to the youth and encourage them to take meaningful actions (implied)
- Decide your own level of involvement in activism (implied)
- Embrace adventures in service of a greater cause (implied)

### Whats missing in summary

The full transcript provides a deep reflection on the significance of adventures in activism and the importance of individual involvement in creating impactful change.


## Transcript
well howdy there internet people it's Bo again tonight we're gonna talk about
adventure something I've been thinking about a lot lately because of a few
conversations and Facebook sent me one of those you know X number of years ago
today this happened and it was a photo of me about to get water bored was part
of a demonstration right after the torture report came out about the US
torture program and I had well let's just call him another penitent former
foot soldier of the Empire there to waterboard me and he understood the
process and then a mountain of a man named Cody to hold me down because we
really didn't have the right equipment to do this and sitting up while being
waterboard it can be well just bad that can be really bad and basically the plan
is they're just we're gonna show people what it's really like and he is going to
beat the fire out of me and they're gonna waterboard me we're standing there
talking about this and this kid not a kid he's 20 21 I want to be waterboard
it. You ask the right group of guys, dude, we'll waterboard you, you know, whatever floats
your boat, man. And I'll be honest, I didn't expect him to go through with it. But he did.
He did. And, you know, so we did a real clinical version with him and then we moved on to mine
and then the cops broke it up but the point is that guy he chose that moment
to have an adventure to do something crazy and he chose to do it in service
of something greater than himself and yeah he was an activist and he
understood the morality of what was happening and he understood it wasn't
right, but at that moment I think it was about the adventure. I think it was about doing
something that was outside of the norm, doing something that, well, I mean, years later
and he can tell that story. There are not a lot of people who have been waterboarded.
There are even fewer who have been waterboarded on the state house steps voluntarily. We're
Coming into a period in American history that's going to require a lot of adventures, I think
we're going to see a lot of social upheaval, the likes of which we haven't seen since the
1960s.
This period, the next few years, it's going to set the tone for decades to come.
It's going to define the United States because we're at a crossroads.
The adventures we choose to take and the adventures we elect not to take will define it.
And while I'm thinking about all of this, somebody licks ice cream.
And it's the same thing.
It's that same drive, man, humanity, not just men.
We have a desire to push the envelope, to have an adventure, to do something crazy and
because of the internet and everything being available on demand at any time, you can see
anything you want.
It's really hard to do something out of the norm.
You got to do something weird, like lick ice cream.
And yeah, it gets you that story, gets you that notoriety for however long a viral video
lasts.
There are people in the comments section all the time that ask, you know, what can I do?
I don't know.
I don't know you.
I believe in a diversity of tactics, but I don't know you.
I don't know your skill set.
I don't know what you're capable of.
I don't know what you're willing to do.
It could be anything from donating to lawyers, to calling your senator, to marching, to organizing
a protest, to anything, leafletting, all kinds of stuff.
It's like the resistance in the 1940s.
You have to determine your own level of involvement.
Nobody can do that for you.
If you let other people do that for you, you're going to end up over your head.
You're going to end up in over your head.
You've got to decide what you're willing to do.
I hope that somebody decides and somebody has the skill set to reach out to the youth,
to reach out to those young people.
Because when they decide to do something stupid, and I cannot fault them for doing something
stupid. I'm the king of doing something stupid. I hope that somebody's there to
say, yeah, I embrace it, but do something stupid. Take that adventure in service of
something greater than yourself. So many times the world was changed by somebody
doing something stupid, by taking an adventure, by going on an adventure that
that just maybe didn't seem like it needed to be taken.
We're going to have a lot of adventures over the next few years that have to be done if
we want to set the right tone for the decades after.
It's going to be important.
I just hope that a lot of the youth chooses to be the guy that got waterboarded, rather
than licking ice cream or dancing beside a car.
You can change the world, or not.
Anyway, it's just a thought.
have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}