---
title: Let's talk about being able to leave anytime you want....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=uHfFBmM7MwY) |
| Published | 2019/07/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Sharing stories from others' lives can help put your views and life into perspective.
- A family goes into hiding on July 9th out of fear of being rounded up after their asylum request is denied in the United States.
- The inevitable end of the family's story is being given up, rounded up, and separated in camps.
- Drawing a parallel to Anne Frank, who went into hiding on July 9th and later died in a camp.
- The comparison between people who turn in asylum seekers and those who remained silent during historical atrocities.
- Americans tend to remain ignorant of global situations to avoid facing culpability for actions done in their name.
- Refugees come to the U.S. due to interventions in their countries, seeking asylum, facing dehumanization and propaganda.
- The reality of conditions in camps, including audio tapes of children crying for their parents.
- The argument that refugees staying despite horrific conditions at home shows asylum is necessary.
- Calling for learning from mistakes and not repeating history by dehumanizing asylum seekers.

### Quotes

- "We like to remain blissfully ignorant of the situation of the world, and we do this as a defense mechanism."
- "They're people. And because we do this, we can say cute little things like, oh well, they just should have followed the law."
- "If we learn from our mistakes, the next time you hear a story about somebody collaborating with one of the worst regimes of the 20th century, you won't think it's about you."

### Oneliner

Sharing stories to put views in perspective, asylum seekers facing dehumanization, a call to learn from mistakes.

### Audience

Advocates for human rights.

### On-the-ground actions from transcript

- Contact local refugee support organizations to offer assistance and support (suggested).
- Educate yourself on asylum processes and advocate for humane treatment of refugees (implied).
- Volunteer at or donate to organizations working with asylum seekers (suggested).

### Whats missing in summary

The emotional impact and depth of understanding gained from Beau's storytelling and historical parallels.

### Tags

#Refugees #AsylumSeekers #HumanRights #Dehumanization #LearningFromMistakes


## Transcript
Well, howdy there, internet people, it's Bo again.
I just got off the phone.
And sometimes, a story from somebody else's life
can help put your views into focus,
can help put your life into perspective,
can help you examine things that are going on around you.
I mean, that's why most literature exists is
to help expose you to other ideas
and bring about different things, different emotions,
different thought processes. Today, July 9th, a family went into hiding. They went into
hiding. They were worried about getting rounded up. They had applied for asylum in the United
States but it was denied. So they went into hiding. Now the truth is we all know how this
story ends. Somebody more concerned with legality than morality is going to give them up. It's
what's going to happen. That's how the story ends. It ends with, and they get rounded up
shipped off to camps, separated, the family, ripped apart.
And in one of those camps, Anne Frank, she dies of typhus.
I'm willing to bet that a lot of people thought I was talking about something that happened
in the United States this July 9th.
July 9th is the day that Anne Frank and her family went into the attic.
Now, if when I was saying that a family went into hiding and, you know, starting that story,
if you were sitting there going, I can't believe he didn't turn them in, that's why people
make the comparison they do.
That's why.
Because you're cut from the same cloth as the people that turned her in.
You know, Americans, we like to remain blissfully ignorant of the situation of the world, and
we do this as a defense mechanism because we don't want to accept culpability and responsibility
for the things that are done in our name, things that are done by this government.
These people are coming here because of our interventions in their countries.
They get up here, they claim asylum, and just like back then, we're saying, oh, they're
We're not real asylum seekers.
The difference is this time we don't have the Atlantic separating us.
They can make it here and we have to look at them.
So rather than look at them, we're going to put them in camps and propagandize them.
Use all kind of rhetoric to dehumanize them.
What we do, they're people.
And because we do this, we can say cute little things like, oh well, they just should have
followed the law.
They can leave any time they want.
That's kind of true, kind of.
They can, in a way, but they don't just get to walk out the front gates.
They got to go back.
Now the Office of the Inspector General released that report describing the conditions in these
places.
audio tapes of children being mocked as they cry for their parents.
If people are in those conditions, separated from their kids, whom may be getting abused
and they know this and they still don't go back, I don't think that says what you think
it does.
It kind of shows that what they're facing at home is worse.
It kind of shows that their life is in danger, which is kind of the reason asylum exists.
That statement doesn't prove what you think it does.
As we sit here and debate and ramble on about this topic, it's still going on.
Right now it's happening.
You know, we can be a country that has kids in school, read a story, and talk about how
horrible it was, and how we made a mistake by not giving them asylum.
And then we do the same thing.
We continue to do it while those kids are reading that book, as we decry how horrible
it was.
We engage in the same activity, or we can learn from our mistakes.
If we learn from our mistakes, and to those people who support this, if you learn from
your mistakes, the next time you hear a story about somebody collaborating with one of the
worst regimes of the 20th century, you won't think it's about you.
Anyway, it's just a thought.
have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}