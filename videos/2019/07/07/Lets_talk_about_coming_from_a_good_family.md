---
title: Let's talk about coming from a good family....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=5pwslfaBVjs) |
| Published | 2019/07/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduction to gonzo journalism blending facts, statistics, fictions, opinions, stories, and historical tidbits to uncover truth.
- Emphasizes that everything is alleged until there's a conviction, video evidence doesn't change this fact.
- Content warning issued about discussing a sexual assault case in New Jersey involving a 16-year-old girl.
- The judge in the case, Judge James Troiano, didn't want it to leave family court for adult criminal court.
- Details of the alleged assault at a pajama party where the girl got drunk and raped.
- Beau questions the typical victim-blaming response that the girl might face if not for the evidence.
- The prosecution believes the accused texted a video of the act with a condemning caption.
- Despite the evidence, the judge hesitates to label it as rape due to the accused's background and achievements.
- Class distinctions and the privilege of the accused are brought into focus as factors influencing the case.
- Beau stresses that without the video evidence, the case might not have been pursued.
- Reminds the audience that most cases don't have a video, urging reflection on believing victims without such evidence.

### Quotes

- "Everything is alleged until there's a conviction."
- "In most cases, the accused does not text a video."
- "Without that video, never would have gone anywhere."

### Oneliner

Beau blends journalism styles to uncover truth, challenges victim-blaming narratives in a sexual assault case, reminding us to believe victims even without video evidence.

### Audience

Journalists, advocates, allies

### On-the-ground actions from transcript

- Support survivors of sexual assault (implied)
- Believe victims even without video evidence (implied)
- Advocate for fair treatment in the justice system (implied)

### Whats missing in summary

The emotional impact and depth of victim-blaming and privilege in legal proceedings.

### Tags

#Journalism #SexualAssault #VictimBlaming #BelieveSurvivors #JusticeSystem


## Transcript
Well, howdy there, internet people, it's Bo again.
Before we get started tonight,
I want to remind everybody that what I do
is gonzo journalism on this channel.
And that's a blend of facts and statistics and fictions
and opinions and stories and historical tidbits,
all blended together in hopes of getting to truth,
rather than just isolated facts,
because facts and truth aren't always the same thing.
In that regard, I want to also point out
that at heart, I'm still a journalist.
So in my world, everything is alleged
until there's a conviction.
Doesn't matter if there's a video of the crime
until there's a conviction, it's alleged.
That's how it works.
And what we're going to be discussing tonight
proceeding. So of course, everything is alleged. There's been no conviction. In
fact, there haven't even been formal criminal charges filed as of yet,
although I believe they're forthcoming. Okay, so now that we have that all out of
the way, I also want to say content warning. We're going to be discussing a
sexual assault. Not in explicit detail, but in enough detail to maybe bother
some people. So the case comes out in New Jersey and the only reason we know
about this case is because the prosecution got this ruling and took it
to the appeals court and the appeals court agreed with the prosecution
therefore we get to hear about this otherwise we never would have known this
happened. The judge that issued the ruling is Judge James Troiano, T-R-O-I-A-N-O, and
he didn't want to let this case leave family court and go to adult criminal court. Now
what the prosecution contends happened is that a 16-year-old girl went to a party. It
It was a pajama party, she was dressed accordingly.
She drank, got drunk, and got raped.
I want to stop here, pause here for a second.
Under normal circumstances, this would have been the end of it.
Never would have gotten into court, probably never would have even been reported.
Because even at 16, she knows what she's going to be asked.
She knows what the public court of opinion is going to do to her.
And where are we at?
Oh a party, huh?
You drinking?
Underage.
Illegally.
What kind of party was it?
A jama party?
What were you wearing?
Really?
Don't suppose anybody heard you struggle or anything, right?
Because this is really starting to sound like a regret case to me.
rape case. Tell me it's not true. Based on just this, that's what she would have
been subjected to. This case is a little different though because the prosecution
believes that the accused texted his friends a video of the act with the
caption, when your first time is rape.
Now, nobody outside of the original recipients or the criminal justice
community up there has seen the video, but the descriptions of it make it
sound as though she's almost unconscious.
So, when your first time is rape, that's pretty condemning evidence in anybody's opinion.
Anybody's opinion except for this judge.
Because this judge likes to reserve the term rape to incidents or for incidents that, well,
they have more than one assailant or they were, you know, beating them or something
like that.
Even though 90% of rapes are single assailant incidents, and in 80% of cases the assailant
is known to the victim.
So even with that incredibly condemning piece of evidence, the wills of justice just don't
have enough oil to spin, I guess.
We find out in the ruling why, because of the quote devastating effect that it might
have on this young man? On the accused?
Wow, that's a crazy thing to be concerned with until you read the rest of it because
he comes from a good family. Now that's code. What that means is he comes from money. He
comes from the establishment. He comes from that ruling class. That class of people that
look out for each other.
Judge goes on to talk about how he's an Eagle Scout and does well in school because that
is extremely relevant. Extremely relevant. He's a candidate for college, not just any
college, but probably good college. These are really things that he said. You know,
we like to pretend that class issues in this country are a thing of the past. They're not.
They're not. And any time a situation like this comes up, we are reminded that if someone
wants to get away with rape all they have to do is well target somebody of a
lower class then it's real easy nobody's gonna believe them after all you're an
Eagle Scout guess you know how to tie ropes the key thing I want people to
to take away from this incident is that in most cases, the accused does not text a video.
Most cases, there's not a video.
Next time you hear of a situation like this, think back to this.
Next time you hear that story of the girl who got drunk and says something happened
that nobody believes her, think back to this.
Because without that video, never would have gone anywhere.
Even with the video, the prosecution had to go get an appeal.
Something we all need to keep in mind.
Now again, I want to reiterate, this is all alleged, there's no conviction.
These are the facts as we know it of the case.
Anyway, it's just a thought.
You guys have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}