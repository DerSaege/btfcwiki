---
title: Let's talk about what we can learn from a black Little Mermaid....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=VlSfah57lbQ) |
| Published | 2019/07/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Disney is facing backlash for deciding to make the Little Mermaid black, with people upset because the story dates back to 1837.
- The original story from 1837 involves a mermaid saving a prince, losing her voice and identity, and ultimately dissolving into sea foam after failing to win his heart.
- The themes in the original story include giving up one's voice and identity to fit in and find love, perpetuating harmful ideas about women's worth and the importance of conforming.
- Beau criticizes those who oppose a Black Little Mermaid for not recognizing the positive theme of letting go of racial divisions present in the original story.
- He suggests that embracing a Black Little Mermaid could teach valuable lessons about breaking down racial barriers and challenging traditional narratives.

### Quotes

- "We could probably learn a lot from A Black Little Mermaid."
- "The one positive theme in it is giving up these illusions that these things make us different, that these things matter."

### Oneliner

People upset over a Black Little Mermaid fail to see the positive theme of breaking down racial divisions present in the original story, missing an opportunity to challenge harmful narratives.

### Audience

Storytellers, activists, educators

### On-the-ground actions from transcript

- Challenge traditional narratives and stereotypes (implied)
- Promote diversity and representation in storytelling (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the original Little Mermaid story and the implications of Disney's decision to introduce a Black Little Mermaid, offering insights into themes of identity, conformity, and racial divisions that can be explored further.

### Tags

#Representation #Diversity #ChallengingNarratives #RacialJustice #Empowerment


## Transcript
Well, howdy there, internet people, it's Bo again.
Fun fact, I've got a Little Mermaid tattoo.
Incidentally, it's all done in black.
Man, people are mad.
People are freaking out.
So Disney's decided to make the Little Mermaid black.
And people are upset by this because,
I mean, this is an old story, dates back to 1837.
And we can't go changing it.
mess with tradition if we do that.
Let's recap that story from 1837 real quick.
So when a mermaid turns 15, she gets to go up to the surface.
When our little mermaid does that, of course, she runs into a prince.
She saves him.
He never sees her though, has no idea a mermaid saved him, but she falls in love with him.
She goes down and tells the sea witch the story and the sea witch is like, fine, I got
you.
Don't worry about it.
But I'm going to take your voice, oh and your tongue, and when you walk it's going to feel
like you're getting stabbed and your feet are going to bleed constantly.
But don't worry, all you have to do is win his heart.
Of course if you don't, you're going to die of a broken heart and dissolve into sebum.
And he marries a princess, not her.
So she's sitting there on the beach waiting to die, and her sisters show up.
bald because they just traded their hair to the sea witch to get a knife.
They give this knife to the little mermaid and all she has to do is go and kill the prince.
Then everything goes back to normal except for him being dead.
But she can't do that.
She's a good person so she dives off of the ship, dies, and dissolves into sea foam.
But luckily, she becomes a spirit of sorts and basically all she has to do is do good
deeds for the next 300 years and she'll get to go to heaven.
Because mermaids don't have a soul, I forgot that part.
And that is why mermaids help sailors.
Crazy.
That's not the story most people know, is it?
Yeah, you can't hearken back to the tradition of 1837
when the actual tradition you're talking about started in 1989.
There have been a lot of changes to this story over the years.
Skin tone would be the least of them.
Would be the least of them.
That doesn't really seem like a really big deal.
I would also point out that as we have made these changes
throughout history, the themes that we've
managed to save in this story are that, well, ladies, if you want a man, you better give
up your voice, and if you don't get a man, well, you're nothing. You dissolve. And that
in order to fit in and go to heaven, we have to give up your identity. Interesting. It's
funny because the theme is give up that identity. Give up that identity of your
race. Mermaids. I'm guessing that the people that are mad about this didn't
really learn much from this story. The one positive theme in it is giving up
the these illusions that these things make us different that these things
matter. That's the one positive aspect of the story. I mean deep down this is a
messed up story but that's the one good piece. Letting go of the racial
separations but we can't do that and in fact we're gonna call back to the
tradition of this story to say we can't do that. That's pretty messed up. That's
pretty messed up. We could probably learn a lot from A Black Little Mermaid. Anyway
It's just a thought y'all y'all have a good night

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}