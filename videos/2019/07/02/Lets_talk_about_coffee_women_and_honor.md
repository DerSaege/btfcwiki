---
title: Let's talk about coffee, women, and honor....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NizYw2IbML4) |
| Published | 2019/07/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Expresses love for coffee and keeps multiple flavors on hand at all times.
- Talks about Border Patrol feeling threatened by AOC, a young representative in Congress.
- Mentions jokes circulating about why Border Patrol felt threatened by AOC.
- Suggests that men in the country get their moral compass from their mothers, impacting their behavior.
- Emphasizes the importance of speaking a relatable and simple language to reach a broader audience.
- Points out the lack of education among Trump supporters and the need for simplicity in communication.
- Encourages using the word "dishonorable" to question immoral actions effectively.
- Advocates for challenging toxic masculinity and questioning the masculinity of individuals engaging in dishonorable actions.
- Urges women to lead the charge in addressing toxic masculinity and labeling actions as dishonorable.
- Stresses the need to paint Border Patrol's actions as dishonorable to drive change.

### Quotes

- "Men determine their morality based on the opinions of women."
- "Trump supporters are not a well-educated demographic."
- "It is time to use toxic masculinity as an advantage."
- "If Border Patrol starts to be seen as dishonorable, well, you'll see changes in a bunch of different ways."
- "They can't spin the morality of this. This is dishonorable."

### Oneliner

Beau challenges toxic masculinity and advocates for labeling dishonorable actions to drive change, urging simplicity in communication.

### Audience

Activists, Advocates, Women

### On-the-ground actions from transcript

- Challenge toxic masculinity by calling out dishonorable actions (exemplified)
- Use the word "dishonorable" to question immoral behavior effectively (exemplified)
- Lead the charge in addressing toxic masculinity and labeling actions as dishonorable (exemplified)

### Whats missing in summary

The full transcript provides a deeper understanding of how toxic masculinity impacts societal behavior and the importance of addressing dishonorable actions effectively.

### Tags

#ToxicMasculinity #DishonorableActions #SocietalChange #Communication #Activism


## Transcript
Well, howdy there, internet people, it's Bo again.
So I love coffee.
Love it.
Bunch of different flavors.
Keep multiple flavors on hand at all time.
There's that old saying, I like my women
like I like my coffee.
I do, bold and strong enough to stand on its own.
Um, border patrol said they felt threatened by AOC.
Overseas viewers, y'all keep asking,
What's an AOC?
AOC is a young representative in our Congress.
And she's about 5'5", 120 pounds, soaking wet, 54 kilos.
She is not a physically intimidating woman.
But they said they felt threatened by her.
Now, of course, there's a bunch of jokes going around
about why they felt threatened by her.
One is that we all know that, you know,
American law enforcement fears for their life anytime they're around a brown person.
And then the other is that they don't have her child held hostage to gain compliance.
They're actually not really great jokes.
I have a different theory as to why they felt threatened by her.
Men in this country, they generally get their moral compass from their mother.
That's who corrects their behavior.
I think that may play into it.
Men determine their morality based on the opinions of women.
It's a thing, especially men who engage in the profession of arms.
Their morality gets lost along the way, so they look to those around them.
This is important, you know.
We've talked about speaking the language of the person you're talking to a lot.
And people asked in that one video why I used Britt's List instead of the older one that
he maybe plagiarized.
The older one is full of philosophical jargon.
It's less relatable.
people that I want to understand it, it's not you guys, it's the people you're going
to share it to, they may not grasp it.
What we have to understand and we have to keep in mind, and this isn't an elitist thing,
this is reality, Trump supporters are not a well-educated demographic.
That's a fact.
And you can see this in how they respond to criticisms.
They get called deplorable.
So first they Google it and find out what it means.
And then they focus on one piece of it
and kind of embrace it.
That's why you have so many people walking around
saying I'm an infidel.
Now, you go to church, you're not an infidel.
You just don't know what that word means.
We need to remember that when we are
having these discussions, we need to keep it simple.
And ladies, y'all are on the front line on this one.
Because you have the ability to question their morals.
I want you to start using the word dishonorable, something
that can't be confused, can't be picked up and embraced.
These actions are dishonorable.
No honorable man would do these things.
play into it these guys tend to they tend to cast themselves as men's men you
know the sons of pioneers and all of that I mean we know that isn't true
based on their susceptibility to fear but that's what they like to believe
they are. This is dishonorable. Another word to use, it's un-American and you'll
of course get some pushback on that. These are Americans. Now the migrants,
the immigrants, they embody the American spirit. You don't. Well they're trying to
protect the country. I don't need protection from toddlers. I don't need
protection from teen mothers.
You have to question their masculinity.
It is time to use toxic masculinity as an advantage.
These things that they're engaging in, they fit no definition of masculinity, not real
masculinity, but they fit very well into that toxic masculinity.
That desire for control, we need to use it and it will be effective, but ladies, it's
going to be you that has to do it.
It's going to be so much more powerful coming from you.
If Border Patrol starts to be seen as dishonorable, well, you'll see changes in a bunch of different
ways.
It's an image we need to work to cast at every opportunity.
actions are not honorable. They're not. Telling a mother that she needs to drink
out of a toilet is not honorable and I know there's already the excuses going
around about that. If the bottom worked the top works that's how plumbing works
unless the faucets broke. The fixture itself. Master plumber. Well you know
they're joint. Yeah they are joint and one side of it can break. This is and the
The top was broke.
They're going to do anything they can to excuse the behavior and debate the little trivial
facts.
They're going to try to spin those.
They can't spin the morality of this.
This is dishonorable.
She needs to be hammered home at every opportunity.
Anyway it's just a thought.
y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}