---
title: Let's talk about 14 characteristics, 10 stages, and where you are....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=83mtXbwPNkc) |
| Published | 2019/07/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Lists 14 characteristics of a certain kind of government with examples like powerful displays of nationalism, disdain for human rights, identifying enemies and scapegoats.
- Mentions supremacy of the military and rampant sexism as characteristics.
- Talks about an obsession with national security, blending of religion and government, and corporate power protection or blending.
- Mentions labor suppression and disdain for intellectuals in the arts.
- Addresses obsession with crime and punishment, police brutality, cronyism, and corruption.
- Talks about fraudulent elections and the 10 stages of fascism, from classification to extermination.
- Raises awareness about the signs of genocide and ethnic cleansing happening currently.
- Urges viewers to pay attention to the unfolding situation and make a choice to correct it.

### Quotes

- "There's never been a fascist regime in history that did not go through these ten stages."
- "You have all of these lists that have been around a long, long time, and we're matching them up almost like we're following them like a blueprint."
- "There are people that need to understand this. They need to understand where we're at."

### Oneliner

Beau lists 14 characteristics of fascism, outlines 10 stages, and warns about the signs of genocide happening currently.

### Audience

Activists, Community Members

### On-the-ground actions from transcript

- Contact local representatives to demand accountability for human rights violations (implied).
- Support organizations working to protect human rights and fight against discrimination (implied).
- Educate others about the signs of fascism and genocide happening currently (implied).

### Whats missing in summary

The full transcript provides a detailed breakdown of the characteristics of fascism and the stages leading to genocide, urging viewers to pay attention and take action against these dangerous trends.

### Tags

#Fascism #Genocide #HumanRights #Activism #Awareness


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're going to talk about 14 characteristics
in 10 stages.
If you know where this is going to stick around,
you're going to find out something
you don't know at the end.
So I'm going to list 14 characteristics
of a certain kind of government, give you
examples of the ones that aren't plainly obvious,
and then we're going to talk about something else.
So those 14 characteristics, first of which is powerful and continuing displays of nationalism.
Picture flags on everything, very patriotic slogans, Trump hugging the flag.
The next is a disdain for human rights.
That should be plainly obvious, support of torture, black sites, concentration camps,
that sort of thing.
Identifying enemies and scapegoats.
and them, those Muslims, those illegals, we got to know who the bad people are, right?
The others.
Supremacy of the military.
In these regimes, even if there are widespread problems at home, the military receives a
ridiculous portion of the budget, like here.
Rampant sexism.
That one should be obvious, grab her by the, she's a good piece of, yeah, that should
be pretty obvious.
The other thing that's interesting is that in this category also is included discrimination
against gays and opposition to abortion.
Seems a little on the nose, doesn't it?
Just so you know, this was written in 2003.
It wasn't tailor-made for Trump.
just seems like it is. Control of the mass media is really important to these
kinds of regimes. Maybe suggesting that there's a state-run news outlet to
counter all the fake news. There's an obsession with national security and
this is more about governing through fear. We've got to tell you who to be
afraid of so we can tell you we're going to protect you from them and you'll give
us your rights in order to do that. That's what it's about. The blending of
religion and government. Now this is really just innately American. There's
always been religious rhetoric in government since very early on. Very
early on. The one gripe I have with this list is it says that corporate power is
protected, whereas most other experts on this subject would say that corporate
power and government are blended, not just protected. And this would be, you
know, the privatization of government functions like prisons, detainment
facilities, contracting along the border, these kinds of things. And an obsession
with the corporate well-being of entities within the country.
Labor suppression, now that one's a little more obscure, but let's just say in the U.S.
there are two agencies that deal with unions. One regulates the unions and makes sure that they're
They're not doing anything wrong.
The other regulates the companies that have unions.
One of these had their budget slashed, the one that regulates the companies.
The other had their budget increased, the one that regulates the unions.
A disdain for intellectuals in the arts.
I love uneducated voters.
An obsession with crime and punishment.
I'll pardon those people that do stuff wrong,
because you want to punish those you feel
that are doing something illegal.
And it extends to creating a situation
where the police can get away with anything.
Bang their head, you don't have to treat them too nice.
Cronyism and corruption, I'm fairly certain
that nepotism would fall under that,
or using government funds to go to your own hotels.
And then the last one is fraudulent elections.
see the most recent Supreme Court case most of the time.
This is accomplished through Judicial Review and gerrymandering.
These are the 14 characteristics of fascism.
You are here.
To those that are saying, well, that doesn't sound so bad.
There's never been a fascist regime in history that did not go through these ten
stages, the first of which is classification. That's where you divide them up, us and them,
good guys and bad guys. The next is symbolization where we define them by other terms, you know,
not people but illegals. Maybe come up with other terms to use them for and we create
a mascot for a boogeyman to scare everybody with, like MS-13.
Next is discrimination, and that is where we start to deny their rights.
Now, historically, this is done through legislation.
In the US, we're just saying, oh, they don't have them because they're illegal, but they
do.
Constitutionally, that precedent has been set for a very, very, very long time.
The next is dehumanization and you know that's where they become animals and disease carriers
which is why we've got to check them at the border right because they're all just dirty
brown people.
It's really on the nose isn't it?
The next stage is organization and that's where the militias start to pop up and you
You see a lot of replacements in government positions, shaking things up, and a lot of
times it's a decentralized network that pulls this stuff together.
Then there's polarization, and that's where you have hate groups start to emerge and become
very vocal, and they mainstream these ideas.
Don't worry though, there's nice people on both sides.
Then we get to preparation.
And this is more mental preparation of you, where they start using terms like purifying
the nation, mass removal.
Well they won't assimilate.
They're not really part of us, are they?
We've got to do something about them.
And then we get to persecution, and that's concentration camps.
That's when they'll show up.
You are here.
The next step, the next stage is extermination.
The stage after that is denial.
Yeah, that's where we're at, we're really close.
And right now there's people saying, you know, we're not going to commit genocide, that's
ridiculous.
Maybe.
But genocide, like the term ethnic cleansing, is misunderstood in the United States.
doesn't just mean mass killings. It's not what it means. Ethnic cleansing and genocide
are not interchangeable terms. Ethnic cleansing could be renaming street signs. If at the
end of World War II when we had control of Berlin we renamed all the roads after US generals,
that would be ethnic cleansing. There's the common usage and then there's the legal term.
Under the legal term of genocide, there are five components, five things that qualify.
One is the forcible transfer of children to another group.
That one's already happening.
Not just through adoption, but in a truly American twist, we're taking them and putting
them into facilities where, because of the cronyism, our friends are making money.
That one's already happening.
The next one is to prevent births from that target group, because we've got to get rid
of those anchor babies, right?
Already happening.
Deliberately inflicting poor conditions on that group in hopes of destroying them in
part, such as, well, you know, they don't need soap, toothbrushes, or beds, food.
Already happening.
Doing things to intentionally cause them physical and mental harm, child separation, the conditions
in these camps, it's already happening.
In fact, the only one that isn't happening under the legal definition of genocide is
mass killings, that's it.
You are here.
It's a little sobering, but it's something that I think we really need to pay attention
to.
You have all of these lists that have been around a long, long time, and we're matching
them up almost like we're following them like a blueprint, and nobody wants to dare call
it what it is.
I don't normally, at the end of my videos, I don't normally say, hey, share this.
this one. There are people that need to understand this. They need to understand where we're
at. Because very soon they're going to have to make a choice. Are they going to double
down on their mistake? Or are they going to attempt to correct it? Are they going to speak
out. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}