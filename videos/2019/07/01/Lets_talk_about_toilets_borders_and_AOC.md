---
title: Let's talk about toilets, borders, and AOC....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=QH71ldfiNMU) |
| Published | 2019/07/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Responds to comments denying fascism and genocide.
- Border guards joked about a mother drinking out of a toilet.
- A Facebook group of guards planned a GoFundMe for assaulting AOC.
- The guards knew about upcoming inspections but were unfazed.
- Acts of dehumanization and abuse are rampant in detention centers.
- Deplorable conditions and inhumane treatment are overlooked.
- Laughter at dehumanization makes it easier to escalate violence.
- Points out the trend towards escalating violence and dehumanization.
- Urges action to prevent further descent into darkness.
- Calls for accountability and action to stop the worsening situation.

### Quotes

- "Guns don't kill people. Governments do and we're on our way."
- "We are headed to some very very dark times."
- "If you can sit there and laugh at a mother being told to drink out of a toilet, you'll probably be okay with putting a bullet in the back of her head."

### Oneliner

Beau responds to denial of fascism, recounts border guards' dehumanizing actions, and warns of escalating violence, urging action to prevent dark times ahead.

### Audience

Activists, Advocates

### On-the-ground actions from transcript

- Call for accountability and action to stop the worsening situation (suggested).
- Start calling, marching, or doing whatever you're comfortable with to prevent further descent into darkness (suggested).

### Whats missing in summary

The full transcript provides a detailed account of the dehumanizing treatment in detention centers and the urgency of taking action to prevent further atrocities.

### Tags

#Fascism #Dehumanization #DetentionCenters #Accountability #Activism


## Transcript
Well, howdy there, internet people, it's Beau again.
I was busy answering the comments in my last video on YouTube,
all the ones saying that, no, we're not really going down
this road to fascism.
We're not headed towards genocide.
This would never happen here.
They're not concentration camps.
We're not dehumanizing people.
I was busy replying to all of that when news broke
that border guards were laughing about telling a mother
drink out of a toilet.
The part that we really need to remember is that that Facebook group made up of these
guys was exposed and they were fully aware that these inspections were coming.
So much so that they were talking about doing a GoFundMe in case somebody had the courage
throw a burrito at AOC because you know that's not suddenly racist at all, they
were aware they were going to be inspected by congresspeople and this
wasn't concealed. They thought this was okay and when it was brought up to a
superior, well sometimes they act out. Act out. They're federal law enforcement,
They're not toddlers. Those are your prisoners. Act out. These are armed people
operating under the authority of the United States. This didn't happen in
Iraq. It didn't happen in Afghanistan. Didn't happen at some black site. This
This happened on US soil, US law enforcement.
The statistics are very, very clear.
Sexual abuse is rampant.
The conditions are deplorable.
It's inhumane.
But they're laughing about it because that dehumanization has already started.
It's already started.
They're not real people, they've been drinking out of toilets like a dog.
And that's part of it, that is part of this trend, it's part of the stages.
This is what makes it really easy when that order comes down.
We need to evacuate these people, and by evacuate I mean into a ditch.
If you can sit there and laugh at a mother being told to drink out of a toilet, you'll
probably be okay with putting a bullet in the back of her head. We are headed to
some very very dark times. What people need to remember is that civilian gun
violence is nothing. It is nothing in the grand scheme of things. If you look at
the statistics it's a blip. Guns don't kill people. Governments do and we're on
our way. You've got to start calling. You've got to start marching. You've got
to start doing whatever it is you feel comfortable doing. This has got to stop.
Otherwise, it goes to a really bad place.
This is what wasn't concealed. They thought this was okay. And the
supervisor wrote it off as acting out.
Your federal law enforcement, you don't get to act out.
These are people you're supposed to be caring for.
Asylum seeking is legal.
It is legal.
They're engaging in a lawful activity and you've got them in a cage telling them to
drink out of a toilet because they're the other. They're not real people, they're
illegals. You can keep saying it won't happen here and then you're gonna have
to face the fact that a lot of it already is happening. A lot of the things
and this is discussed in that last video, there are five things that are
consider genocide. Four of them are already happening. The only one that isn't is mass
killing. And we're making it really easy to do that. Make America great. Anyway, it's
just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}