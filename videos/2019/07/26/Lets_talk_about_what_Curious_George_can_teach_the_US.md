---
title: Let's talk about what Curious George can teach the US....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=GNlai8E2vE4) |
| Published | 2019/07/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the symbolism of Curious George on his hat and how it relates to his videos.
- Recounts his history with Curious George as a mascot for a group he was part of.
- Talks about his middle son's love for monkeys and how it reignited his interest in Curious George.
- Acknowledges problematic themes in Curious George stories, particularly regarding a white man taking a monkey from Africa.
- Discovers the true story behind Curious George, where George was actually Fifi, a monkey from South America, and his human parents were Jewish refugees escaping the Nazis in France.
- Expresses admiration for how the story of Curious George/Fifi mirrors the resilience and resourcefulness of refugees.
- Draws parallels between the refugee experience and the potential positive contributions refugees can make to society.
- Expresses gratitude for learning the true story of Curious George/Fifi and its impact on his perspective.

### Quotes

- "They're going to better the country if you let them."
- "They're going to re-instill that spirit of being able to accomplish the impossible."
- "All of those things that we pretend are still American traits."
- "It's a great story."
- "That's my thought."

### Oneliner

Beau explains the symbolism of Curious George, uncovers the true refugee story behind it, and advocates for welcoming refugees to better the country.

### Audience

Viewers, Refugee Advocates

### On-the-ground actions from transcript

- Welcome and support refugees in your community by offering assistance and resources (implied).
- Advocate for policies that facilitate the integration of refugees into society (implied).
- Educate others about the valuable contributions refugees can make to society (implied).

### Whats missing in summary

The emotional impact and personal reflection Beau experiences upon learning the true story of Curious George/Fifi.

### Tags

#CuriousGeorge #Refugees #Resilience #Symbolism #Advocacy


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we're going to talk about Curious George.
People ask me about it all the time,
why I have this image on my hat.
I think it's because I use so much symbolism in my videos
in a whole bunch of different ways
that they assume there is a deeper meaning to it.
There wasn't, to be honest.
What seems like a lifetime ago, I
was part of a group of guys that were normally somewhere trying to figure
something out. Nobody knew where they were. People were looking for them and
they probably had no business being there. Curious George seemed like a really
fitting mascot. That was only a few years and Curious George kind of left my life
for a while. Then my middle son, before he was even old enough to say curious,
Well, he just loved monkey, so all that stuff came back out.
Was at that point that I read the stories for the first time since I was a kid, and
I couldn't help but notice certain themes that are, well, let's just say what we would
call today problematic.
idea of a white man dressed the way that he is, taking a monkey from Africa, putting him
on a boat, shackling him, bringing him to the United States, putting him in a cage,
and eventually bringing him home, was a theme I couldn't miss.
At the same time, I love when something good comes from something bad, and today, Curious
George is a science teacher, teaches kids about STEM, teaches them to build something
from nothing. Love it. So I was willing to overlook the problematic themes that I saw
in those stories. And to be honest, I don't know if that was an intentional thing and
after what I have recently found out, it probably wasn't. Because when he all sent me an email
telling the story of Curious George and I love him even more today. So Curious
George was a real monkey named Fifi in South America, not Africa. His human
parents were Margaret and Hans. Well, they go to France on their honeymoon and
they're there in May of 1940. If you're not a history buff, the Nazis just showed
up. It's about to get real bad. Margaret and Hans are Jews. They can't get out. The
trains are gone. They don't have bicycles to buy, so they build them something from
nothing. They use spare parts and cobbled together bicycles. And they become
refugees, they head out in the basket of one of those bicycles, all of these
beautiful pastel images that they had made of a monkey. Eventually they get out,
they get to Lisbon and they get out. They hop on a boat, they go back to Rio,
I think, and then they make it to the United States where that refugee adopts
and assumed name, George. That's where Fifi became George. George goes on to
better the country for the next 70 years more than. Teaches kids to build
something from nothing. Something that refugees are really really good at. It's
great story. It's a great story. It is very fitting for what I talk about a lot of times.
But I didn't know that, to be honest. I can't thank you enough for sending me that email.
That's amazing. But I would imagine that a lot of the refugees from south of the border
that are trying to get here now, they're going to be just like George. They're going to better
the country if you let them. They're going to re-instill that spirit of being
able to accomplish the impossible, that curiosity about the world around them.
All of those things that we pretend are still American traits. Anyway, it's just a
That's my thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}