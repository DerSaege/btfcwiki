---
title: Let's talk about how 5 minutes of your time can save 500,000 lives....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=IpOSJR-_ElM) |
| Published | 2019/07/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Urges viewers for a call to action regarding the Connegates Amendment in the NDAA, aiming to prevent unauthorized attacks on Iran.
- Two versions of the Defense Department budget exist, with the House version containing the amendment but not the Senate version.
- The amendment requires President Trump to seek authorization from Congress before attacking Iran, limiting executive power.
- Viewers are encouraged to contact members of the Armed Services Committees, especially in the Senate, to support the amendment.
- Emphasizes the importance of maintaining the amendment to prevent potential devastating consequences akin to the Iraq war.
- Appeals to resistors, Republicans, anti-war advocates, and veterans to advocate for the amendment's retention.
- Stresses the potential loss of lives, referencing the toll of half a million in the Iraq conflict.
- Acknowledges Trump's restraint in military force but expresses concerns about his changing positions and escalating tensions.
- Calls upon viewers to take action to ensure Trump is held accountable before launching any strikes on Iran.
- Urges everyone to dedicate a few minutes to potentially save hundreds of thousands of lives.

### Quotes

- "A few minutes of your time can potentially save half a million lives or more."
- "If Trump has to go back to Congress before launching a strike, that's going to force him to come up with a good reason."
- "This is your time."
- "We need to keep this amendment in there."
- "Y'all have a good night."

### Oneliner

Beau urges action to support the Connegates Amendment in the NDAA, preventing unauthorized attacks on Iran and potentially saving half a million lives.

### Audience

Advocates, activists, citizens

### On-the-ground actions from transcript

- Contact members of the Armed Services Committees, especially in the Senate, to support the Connegates Amendment (suggested).
- Email, write, tweet to push for the retention of the amendment (suggested).

### Whats missing in summary

The urgency and importance of preventing unauthorized attacks on Iran and the potential devastating consequences similar to past conflicts.

### Tags

#CallToAction #ConnegatesAmendment #NDAA #PreventWar #Advocate


## Transcript
Well, howdy there, internet people, it's Bo again.
Tonight's going to be a little bit different.
There's going to be a call to action in this video, something
you don't normally find in mine, but this is important.
Headline's not a joke.
A few minutes of your time can potentially save half a
million lives or more.
This is what went down, the NDAA, the Defense
Department budget.
It has, the House version has an amendment called the Connegates Amendment.
The Senate version does not have a similar provision.
They voted on it and it had a majority of the votes, but not enough to pass.
So the two versions are different, it has to go into conference.
The Senate Armed Services Committee and the House Armed Services Committee will decide
whether or not it stays in there.
We need to make sure it does.
What the amendment does is it forces President Trump to go back to Congress to get authorization
to attack Iran.
I know, if you're a student of the Constitution, you're going, well, he's kind of supposed
to do that anyway, right?
He is, but over the years, so much power has been ceded to the executive branch that if
If he can tie it in a way to make it seem like previous authorizations for use of force
apply, he may not need to.
He can make that decision unilaterally without Congress.
Now this amendment will prohibit him from doing that.
So we need to get on the phone.
We need to email, write, tweet, whatever, to those people on the Armed Services Committees,
paying particular attention to the Senate.
Add a majority of the votes in both Houses, but not enough to pass in the Senate.
If they want to reflect a representative democracy, it should stay in.
This amendment is important.
Okay, if you are a resister, this is your time to resist.
If you are a Republican, Connagate's amendment.
That Gates, yeah, your guy.
This is widely supported by Republicans as well.
It's a good amendment.
It needs to stay in there if you're anti-war, it should be obvious.
You want this in there if you're a veteran.
We don't need another 22 a day.
A war with Iran would be very similar in intensity to the war with Iraq, maybe even more fierce.
And half a million people, that was the toll.
That's how many people lost their lives in that conflict.
If Trump has to go back to Congress before launching a strike, that's going to force
him to come up with a good reason.
Now I will give it to Trump.
He has done a good job and shown a proper amount of restraint as far as using military
force, but he is still ratcheting up tensions.
And his positions do change frequently.
So it's not a position I feel comfortable that he's going to continue to hold.
He may decide tomorrow that it's time to attack Iran.
We need to keep this amendment in there.
Connaghate's amendment.
This is your time.
minutes of your time can save 500,000 people's lives.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}