---
title: Let's talk about solutions to the conditions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=1Q0IJoOzm_4) |
| Published | 2019/07/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Proposes a solution to current issues, including hygiene, overcrowding, and food problems, and the situation at concentration camps.
- Shares a dream of buying an old plantation house and his plan to house asylum-seeking mothers and their infants.
- Calculates the costs and potential profit by housing these individuals compared to what camps are paid.
- Plans to convert the unoccupied sixth bedroom into a school to teach English.
- Mentions a friend who wants to take at-risk teens to his farm to prepare them for the Marines.
- Suggests that farmers could provide housing for these teens and potentially offer them jobs.
- Advocates for decentralization as a modern solution and criticizes the government for treating people inhumanely.
- Finds poetic justice in using a home built on slavery to help others achieve freedom.
- Believes that individuals can handle the situation better than government agencies like ICE.

### Quotes

- "Modern problems require modern solutions."
- "Turning a problem into an asset, fantastic."
- "I think we can do it better than ICE if we let the average individual do it."

### Oneliner

Beau suggests housing asylum seekers in a plantation house, offering education and opportunities, criticizing government treatment, and advocating for decentralized solutions, believing individuals can do better than ICE.

### Audience

Advocates and activists

### On-the-ground actions from transcript

- Provide housing, education, and opportunities for asylum seekers and at-risk teens (implied)
- Advocate for decentralized solutions to address humanitarian issues (implied)

### Whats missing in summary

Details on the feasibility and scalability of implementing Beau's proposed solutions.

### Tags

#ModernSolutions #Immigration #HumanitarianIssues #Decentralization #Activism


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're going to talk about solutions.
Modern problems require modern solutions.
I think I've come up with one.
Did the math, and I'm pretty sure it'll work out.
It will solve a problem we're facing,
and it'll even help the economy, help the average person.
It's great, it's fantastic.
It will solve the hygiene issues,
the overcrowding issues, the food issues, all of the issues
that are going on, including the idea of people being in cages
that are happening at these concentration camps.
But before we talk about that, I want
to tell you about my dream home.
It's not too far from here.
It's an old plantation house.
It needs some cosmetic work, but it's a good structure.
Six bedrooms, massive kitchen, massive dining room, study,
whole nine yards.
And it sits on a chunk of property.
And to those in the city, a chunk of property out here
does not mean an acre.
That means like you sitting on your porch
and you can't see your property line.
Problem is it's $600,000.
Now I know in a lot of major cities that's not a lot.
here, oh that's a lot of money. So here's my plan. I will take five asylum-seeking
mothers and their infants. That's five bedrooms occupied. At the current rate
that we're paying these camps that apparently can't afford to get the
necessities, they're paying them $775 a day per person, per night. So five
mothers, five kids, that's ten people, that's $7,750 a day times $232,500 a month. I am fairly
certain that I can provide food, health insurance, hygiene, everything they need for like 32 grand
leaving $200,000 a month in profit. Yeah, when you start doing the numbers they're raking in the money
on human suffering aren't they? So in three months that home is paid for. That sixth
bedroom, the unoccupied one, we're going to turn that into a school, you know, no
perfecto, we'll teach them English. And then guess what? That other big
complaint, well they won't assimilate. Well that goes away won't it? It's really
hard to assimilate when you're in a cage, but maybe if you're around people, it'd be
easier, might be easier.
I mentioned this to a friend of mine, he's an XDI, he was a boot camp instructor with
the Marines.
He said he'd take 20 at-risk teens, at-risk meaning possible gang members, up to his farm
in South Carolina, said if they were 16 when they got there,
by the time they were 18, they'd be
ready to go into the Marines.
Think of that.
Turning a problem into an asset, fantastic.
I know some farmers have bunk houses, like old school.
And I'm pretty sure they'd be willing to let them come in,
take them in.
And then once they're processed, look at that, they got a job.
Because it's not like there's a lot of Americans hanging out
those bunk houses. Modern solution right there, decentralization. Of course the
government will never allow this because if they do and we actually start talking
to them we're gonna realize that they're people, not animals, not livestock. I think
it's a good idea. I really do. And then there is, in my situation of course, I do
find some form of poetic justice in the idea that a home that was built on the
back of slavery could be used to help people achieve freedom. That would be
nice too. Anyway, I think we can do it better than ice if we let the average
individual do it. And then when somebody asks, well how many immigrants have you
taken in? We can all say as many as you'll give me. Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}