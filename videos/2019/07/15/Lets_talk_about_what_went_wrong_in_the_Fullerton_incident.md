---
title: Let's talk about what went wrong in the Fullerton incident....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=U3lCH_vLgos) |
| Published | 2019/07/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau is asked to dissect the Fullerton incident and explain what the cop did wrong.
- He expresses strong criticism towards police departments and their actions.
- Beau mentions his history of criticism towards the Fullerton PD.
- The cop in the Fullerton incident observed a woman with a firearm.
- Despite the firearm turning out to be fake, the officer had to make a split-second decision.
- The officer engaged with the woman without using excessive force.
- He called for medics before securing the scene or clearing the car, which is outside of standards.
- The officer then proceeded to render aid to the woman.
- Beau points out that the officer's actions were all within normal standards and best practices.
- He suggests that there could be a debate around implementing a "don't fire until fired upon" policy.

### Quotes

- "There's not. nothing that this cop did that any reasonable person, even extremely experienced, would do differently."
- "This wasn't a kid at a playground where one could reasonably expect it to be a toy."
- "It's just a thought."

### Oneliner

Beau explains the Fullerton incident, defending the cop's actions within normal standards, and suggests potential policy debates.

### Audience

Policing critics

### On-the-ground actions from transcript

- Debate and advocate for policies like "don't fire until fired upon" (implied)

### Whats missing in summary

Detailed breakdown of the specific actions taken by the officer and the context of the situation.

### Tags

#PoliceCriticism #UseOfForce #PolicyDebate #CommunityPolicing #PolicingStandards


## Transcript
Well howdy there internet people, it's Bo again.
I intended on doing something funny this morning,
but I started trying to get through some of my messages
and there's a whole lot of people asking me
to dissect the Fullerton incident.
The message that caught my attention is Bo,
what happened in Fullerton.
If anybody can tell us what this cop did wrong, it's you.
Okay, if you are new to my videos,
I'm a very strong critic of police departments
and tend to rip apart their actions
when they use bad tactics.
OK, so other than that, the other thing you need to know
is that I hate Fullerton PD.
We have a long history of criticism.
What did this cop do wrong?
Nothing, nothing.
What can be expected in a situation like this,
the use of time, distance, and cover?
He tried.
That's why he went around the back of the vehicle like that.
When he gets there, what does he observe?
woman with a firearm. You and I know after the fact that it's not real. There
are people who can tell the difference at distance. There's not a lot of them
and there are very very very few that could do it under stress. So from there
he's got to make a decision what to do. This isn't a kid playing in a park with
a toy gun. This is somebody that got stopped and got out of the vehicle with
firearm on a freeway. There's other people at risk. He engages, doesn't use an excessive
amount of force. It's not like he dumped 45 rounds into this woman. From there, he backs
out, calls the medics before securing the scene, before even clearing the car. That's
outside of standards, okay, but it's bed care, not delayed. Then he goes to render aid. There's
another person that shows up. He kind of takes him at his word that he's a cop. From the
video it does not appear that they know each other. There's a question as to why he was
just standing over her afterward asking her where she's at. While he's doing that he's
putting on her gloves, his gloves. Now he may have also been patting her down, which
is also normal. That's standard. And people asked why she was cuffed. He doesn't know
at the time how severely she's wounded. We know after the fact. He doesn't know at the
time. For all he knows, when he goes to roll her over, she's going to have a second weapon.
All of that's normal. That delay where it just looks like he's standing there looking
at her. When his hands come back up, you can see the gloves on. And he may have been patting
her down in that time too, which is also normal. From there, they pull out the medical kit
and begin rendering aid. The medical kit they used, I would be surprised if that was a standard
kit, but it's better than what I imagined they're issued. It may be standard, but it
well-packed, they did everything they could, they applied the tourniquet and the chest seal,
I mean there's nothing, there's nothing tactically that this officer did wrong in any way.
When he's at the back of the vehicle you can argue about, well maybe he should have tried to
talk her down, maybe, yeah maybe, but that's not expected. That's not something that is,
is it's not a reasonable request, necessarily, given everything that's going on.
Now this can open the debate to whether or not there should be a don't fire until fired
upon policy, but I would point out, while that's a topic in a debate to be had, it's
not policy, and it's not standard, you know, that's not a national standard.
And I don't know that there's anything that he did that could have changed the outcome
of this.
From everything that I see, everything that's been presented so far, the only times he stepped
outside of what the norm is, and what standards are, and what best practices are, it's bed
care, not delayed it.
Yeah, so there you go.
Now, you can take this and run with it any way you want.
There are a lot of discussions that can be had from this incident, but none of them have
to do with bad police tactics.
If I'm hot washing this, the only critique I have is that this guy needs to get to the
range and run some drills because some of the weapons handling is not great.
But it's nothing that caused an unjust shoot.
This wasn't a kid at a playground where one could reasonably expect it to be a toy.
This is somebody who got out of the vehicle holding it and then pointed it at the officer.
That was a shooting stance.
There you go.
I hope that answers the questions.
There's not.
nothing that this cop did that any reasonable person, even extremely experienced, would
do differently.
You might have had somebody try to talk her down, but it's not going to be many.
It's not going to be many.
Anyway, so there you go.
It's just a thought.
y'all have a good night or day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}