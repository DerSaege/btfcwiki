---
title: Let's talk about Earthquakes, Hurricanes, and helping yourself....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=dkJCy2r0Uaw) |
| Published | 2019/07/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Urges viewers to prepare for emergencies, despite it not being the main focus of his channel.
- Emphasizes the importance of self-preparedness due to delays in external help during natural disasters.
- Advocates for two gallons of water per person per day and the necessity of water for various activities during a disaster.
- Recommends storing water in gallon jugs or five-gallon containers.
- Stresses the significance of having enough canned goods to sustain daily calorie intake during a disaster.
- Advises having multiple methods for starting a fire and ensuring one has shelter materials like tarps or mylar tents.
- Urges viewers to assemble a first aid kit and stock up on prescription medications.
- Encourages the inclusion of a knife or multi-tool in the emergency supplies.
- Suggests maintaining hygiene products in the emergency kit for comfort.
- Recommends having a battery-operated radio for communication during emergencies.

### Quotes

- "Help doesn't come immediately. It takes time."
- "You don't have to eat well, you just have to eat."
- "This is all you need to stay OK until help arrives."
- "Your children are going to cause you more stress than the actual natural disaster."
- "Just take care of this stuff and you're going to be able to help people."

### Oneliner

Be prepared for emergencies with water, food, shelter, tools, and communication; your readiness can help others in your community survive.

### Audience

Community members

### On-the-ground actions from transcript

- Stock up on two gallons of water per person per day (suggested).
- Purchase canned goods to ensure a daily intake of calories (suggested).
- Assemble a first aid kit with necessary medications (suggested).
- Include a knife or multi-tool in your emergency supplies (suggested).
- Buy hygiene products for comfort during emergencies (suggested).

### Whats missing in summary

Detailed instructions on developing a comprehensive emergency plan can be best understood by watching the full video. 

### Tags

#EmergencyPreparedness #CommunityPreparedness #DisasterReadiness #SurvivalSkills #CommunityAid


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're gonna talk about a subject
I've been putting off because I get asked this a lot,
but it's not really what most people
on this channel signed up for.
People that know I have a survival background,
they've asked me to do emergency preparedness videos.
Because of the hurricanes, the earthquakes,
it's probably the right time to do a basic one.
One for everybody, and this is stuff
everybody, everybody needs to have. As you probably know and have seen with your
own eyes from the last few major natural disasters, help doesn't come immediately.
It takes time. How long it takes depends on your state and how good your state
agencies are and your local agencies are because the federal agencies are pretty
much worthless now that that's excluding DOD's efforts everybody else they'll get
here eventually but you need to have yourself covered for a few days
depending on the type of disaster because they're they're not gonna be
here right away so that's what we're gonna talk about today and this is for
everybody this isn't for survivalists this isn't for preppers this is for
everybody
Okay, so the first thing water most important thing
I
Would say two gallons of water per person per day
However long you expect to be
Without help and I know people are saying no well, you don't really need that much
You can survive on a lot less you can you can survive on a lot less than that. However
However, you're going to want to cook.
You may be able to flush your toilet if you dump water into it.
You may need to clean a wound.
You may want to wash your hands.
You're going to need water.
Get gallon jugs, or if you have a collagen thing at your house, get some extra five-gallon
things, keep it on hand.
People think that, you know, I've got a case of Dasani at the house, that's enough.
It's not.
It's not even close to enough.
through that so fast. Make sure you have water. You can go through and get water
purification tablets or life straws and that's fine and it'll work if you're out
in the country. If you're in the city that is not going to filter out all the
chemicals that are going to be in that water that you're able to find. So you're
not going to want to go that route. You want to you want to have fresh water of
your own. Food, canned goods, just get canned goods. You're not looking to eat
organic and healthy and fresh, at this you want the calories, that's it, and I would
recommend a couple thousand calories per day, per person.
You don't have to eat well, you just have to eat.
If you have the money, yeah, get the survival food, get some MREs, whatever, but at the
end of the day you're just trying to stay well fed enough to not deteriorate until help
arrives.
that's it. Fire, and by fire this also includes flashlights. You obviously need flashlights,
electricity is going to be out. But fire, Bic lighters, a Zippo, fire strikers, whatever.
And you want to have multiple ones. One of the big rules of survival is if you have one
you have none. Because trust me, the second you are down to one of a particular item,
when it's gonna break or you're gonna lose it. So you want to have multiple
methods of doing anything. Shelter. Now this is for people who are sheltering in
place. Why do you need shelter? Because something could fall through your roof
or the structure may become inhabitable. So you want tarps, you want mylar tents
or blankets. After Hurricane Michael I saw people using the vinyl tablecloths
From the Dollar Tree. I'm talking about something that was cheap and I mean it wasn't ideal, but it kept the rain off of
them
I mean, I thought that was pretty genius
So again, and this is just for a few days
I mean if you have the money yet get a tent whatever
But for most people you don't want to spend a lot of money on this because most people don't think it's ever gonna
happen to them  First aid kit you want one
I did a whole video on first aid kits from little baby ones all the way up to full-blown
trauma kits.
I'll put it in the comments.
And then you also want your meds.
You want your prescription meds.
If you're on meds that you have to have, you need to have some extras.
A knife.
And this is where I have to stop and give advice to people who are of that mindset,
who are survival-minded people.
You've probably given this exact advice to people before.
They didn't take it.
And you wonder why.
And most times I've found out it's because of the knife.
What happens is because we are people who are outdoorsy, we camp, we fish, we, you know,
all of this stuff and we're outdoors, we're working with our hands, we use our knife a
lot.
So when we recommend a knife, what do we recommend?
A really good one, right?
They hop online, they go to Amazon and they go to order an Emerson or a Spyderco and then
all of a sudden they're a whole lot less interested in this because they're about to drop $300
on a knife that they're going to stick in a box in the bottom of their closet.
You don't need that.
You just need a knife.
I would suggest much more valuable than a really good filled knife would be a multi-tool
like a Leatherman or something like that,
and you can even get a cheap one.
It's got a knife on it, but it also has pliers
and a can opener, you know you're gonna need that.
It has all of those other things, a Swiss Army knife.
This is another one of those items
you want more than one of, though.
Because you're gonna use it a lot,
it runs a high risk of getting lost.
Hygiene, not necessary for survival for a week,
but it's gonna make you feel better.
So hit up Dollar Tree, go to the travel section,
deodorant, toothbrush, toothpaste, soap, hand sanitizer,
all of that stuff, throw it in a bag,
put it with the other stuff.
All of this stuff stays together.
That's one of the things.
People say, I have all this stuff in my house.
That's great, but after the hurricane,
or the earthquake, or whatever,
are you gonna be able to find it?
Keep it all together.
The last thing is a battery-operated,
AM, FM, Weather Would Be Great radio.
The internet's going to be down.
You're not going to get reliable news
except through the radio.
When relief does come, you're going
to find out about the pickup points
to get supplies via the radio or from somebody else who
has a radio.
Go ahead and get your own.
Make sure you have plenty of batteries for all of this stuff.
Now, special considerations, because that's it.
This is all you need to stay OK until help arrives.
But special considerations, this is a lesson
learned from Hurricane Michael.
It hit here at a four.
Because we were already set with all of this stuff,
and our home wasn't really damaged,
I was able to go down to Panama City
and help with relief efforts.
And people were like, man, that's great.
Good for you, I'm glad you did that.
Yeah, I was down there cutting people out of houses
with a chainsaw, but my wife was up here
doing the hard stuff because we had no electricity
and we have a bunch of small children.
If you have kids, oh, please, go to whatever
Cheap little store is around you, the 99-cent store, the Dollar Tree, Walmart, whatever,
and go through and get activities for them.
Let them build a bird house, Play-Doh, coloring books, crayons, anything like that that's
going to keep them occupied that doesn't require electricity.
If you don't do that, your children are going to cause you more stress than the actual
natural disaster.
You have to find a way to keep them occupied.
They don't suddenly become adults because there was a natural disaster.
They don't really get what happens.
And then obviously if you have pets, you need food for them, their meds if they need any
water for them as well.
That's it.
There's not a lot to this.
And if you have this stuff on hand, you're going to be in so much a better shape than
anybody else.
This is all you need.
be okay. Now as you start getting into it you're gonna realize well I also
want some rope or I also want this or you know this this factories nearby so I
want to make sure that I have a rubber suit. There's all kinds of stuff that
people think of and that's good because you're developing a plan. In addition to
all of this stuff anybody that's going to use it anybody that's part of the
plan needs to know what the plan is. You need to know where you're going to
meet up in a secondary location, in case you can't meet up at the first one, then you need
to have the evac location, the place you're going to go to if you can't shelter in place,
and then a second one.
And everybody needs to know about this, if you're discussing it with people that don't
like to talk about this stuff, talk about it like you're preparing for a zombie apocalypse
and they will remember it, it's also less scary if you're dealing with younger kids,
not toddlers or anything that would be terrified of zombies, but kids that are a little bit
older than that but not really not really there to the point where they're
actually gonna really grasp this but they'll remember what they're gonna do
to fight off the zombies it's also less scary that's it now to those that wanted
the advanced stuff I'll do those videos I will and I will space them out because
again this isn't what people really signed up here for but this is something
everybody needs if you have this stuff you're able to help other people because
you're already taken care of. And in a situation like this, it's just your community. This is
putting on, this is being on an airplane and putting your mask on before you put the person
next to you, before you help them put their mask on. Just take care of this stuff and you're going
to be able to help people and you're going to be in a much, much better situation. And then we will
We'll go into how to bug out and get out of the area.
And we'll do a whole other video on that.
Anyway, you guys stay safe.
It's just a thought.
and everybody have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}