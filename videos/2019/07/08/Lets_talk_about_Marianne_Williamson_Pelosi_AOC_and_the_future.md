---
title: Let's talk about Marianne Williamson, Pelosi, AOC, and the future....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=MpIwE7KwQYU) |
| Published | 2019/07/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Examines the lessons from three women and their impact on the country's direction.
- Advocates for shedding 20th-century prejudices and embracing new perspectives.
- Criticizes the dismissal of Marianne Williamson as a viable candidate due to unconventional views.
- Challenges the hypocrisy of ridiculing Williamson's beliefs while accepting traditional forms of prayer from other candidates.
- Questions the notion of presidential behavior, contrasting showering with love versus bombing countries.
- Disagrees with Williamson on the importance of unraveling national secrets to understand sickness.
- Comments on the marketability of values like peace, love, and hope in elections.
- Addresses the pushback against younger Congress members like AOC by established figures like Nancy Pelosi.
- Considers the inevitability of more Millennials entering Congress and the impact of a growing younger voting population.
- Criticizes the lack of action on pressing issues such as border camps, incarceration rates, income inequality, drug war, and healthcare.
- Urges incumbents to question their own presence in Congress as younger generations demand change.
- Emphasizes the rising influence of a generation seeking tangible progress and holding leaders accountable.

### Quotes

- "Humanity needs a mental shower."
- "We need to wash off the prejudices of the 20th century."
- "You can get with the program or you can go home."
- "The question is why are you there still?"
- "They're not going to sit on their hands."

### Oneliner

Beau examines societal norms, challenges conventional thinking, and calls for accountability amidst generational shifts in politics and values.

### Audience

Voters, Activists, Politicians

### On-the-ground actions from transcript

- Join or support political campaigns that advocate for progressive change (exemplified).
- Get involved in local community organizing to address pressing social issues (exemplified).
- Engage in political education and encourage participation in elections, especially among younger generations (exemplified).

### Whats missing in summary

The full transcript provides a comprehensive analysis of societal norms, political stagnation, generational shifts, and the call for accountability and progressive change.

### Tags

#Politics #GenerationalShift #SocialChange #ProgressiveValues #Accountability


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're gonna talk about three women
and what they can teach us about the country,
where it is, and where it's headed.
Humanity needs a mental shower.
We need to wash off the prejudices of the 20th century.
And I like that.
That's Marianne Williams.
That's the crazy hippie crystal lady that everybody's making fun of.
Like that a lot.
Because of some of the things that she's said over the years and the tweets that she's made,
she's not really a viable candidate, I admit that, but why isn't she a viable candidate?
The tweets that are being mocked, you know, there's the one that's being mocked by the
right and it just it cracks me up because I don't think it I don't think
they realize what it reveals about themselves. She made a tweet during the
oil spill that we need to visualize angels plugging the hole with filling it
up and that's crazy that's crazy talk. Meanwhile I'm certain that multiple
candidates. So they were praying for an outcome. So we expect them to pray, we
just don't expect them to believe it will work. Is that what it is? It's messed
up. You know and they're really mocking her for the whole thing about showering.
She said she wanted to shower the president of Syria with love.
That's not presidential.
We need somebody who's going to walk out there and say, no, we're going to shower the country
with bombs and kill a bunch of kids.
That's presidential.
She said it was important to know who killed JFK because a country like a person is as
How sick is it's secrets?
Oh, Mary Ann, on that one, I actually am going to differ with you.
I don't think you need to know the secrets to know the country's sick.
Peace, love, hope, these things.
They can't win an election. They're not marketable.
Because the voting populace has been trained
to vote for people who are going to
fix what they fear
play on those fears. Anything that isn't based on that is so outside of the norm, it gets ridiculed.
So that's where we're at. But where are we headed? Nancy Pelosi is attempting to take the younger
Congress people up to task, pushing back against AOC, all of this stuff. And that's fine. That's
fine. The question being raised is, you know, should we send more Millennials to
Congress? Yeah, I mean we're kind of gonna have to. That's a mathematical
certainty on a long enough timeline that is going to occur. And the idea is that
AOC shouldn't be there. That's the question. Why is she even there? I don't
know. That's a question most people are asking though. Not the younger people. And
keep in mind that the younger people, every election cycle, there's more of
them that can vote. The question they're asking has to do with something else. See, even since
I was a kid, they had TVs in classrooms and every morning they'd play a little bit of
the news. And these kids that are now adults and they're getting to vote, they got to hear
Congress people, people in Congress, talk about these problems. And they're the same
problems they're facing today, and they get to hear the same congress people that have
been there all this time, Ms. Pelosi, and nothing was done.
We have concentration camps down on the border.
We have more people incarcerated today in the United States than were in Stalin's gulags.
Income inequality is growing.
The drug war is raging, which is only destabilizing these countries in Central and South America.
It's like you guys don't even understand supply and demand.
Healthcare is a joke, and nothing's being done.
Yeah maybe this new crop is a little more aggressive.
Maybe you should have been.
The question that I think most people are asking is not why should AOC be there.
The question is why are you there still?
You can stand there on Capitol Hill and yell, get off my lawn to this younger crowd, but
understand they're coming and in greater numbers.
And yeah, there's only a few of them this time.
Wait till the next election.
More and more.
So you can get with the program or you can go home.
generation that is coming up now, they're not going to sit on their hands. I think
they've shown that. They actually want the hope and change that was promised to
though. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}