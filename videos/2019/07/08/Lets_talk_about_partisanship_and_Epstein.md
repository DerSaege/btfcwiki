---
title: Let's talk about partisanship and Epstein....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=hMIZt6DGRyY) |
| Published | 2019/07/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the dangers of partisanship and blind loyalty to a political party.
- Mentioning recent headlines showcasing partisanship in the media.
- Speculating on the potential involvement of influential figures in criminal activities.
- Addressing the issue of overlooking wrongdoing based on party affiliation.
- Drawing parallels between excuses made based on past actions of different political figures.
- Criticizing the hypocrisy in condemning certain actions while supporting similar ones.
- Expressing disbelief at the lack of outrage over serious allegations in certain situations.
- Warning about the dangerous consequences of prioritizing political allegiance over morality.
- Urging people to speak out against injustice and not remain silent.
- Concluding with a call for reflection on complicity in enabling harmful actions.

### Quotes

- "I don't fault the second person in a gang rape. If it's wrong, it's wrong."
- "If you ever reach the point where you can up play or down play child abuse of that type for political ends, that should be a red flag."
- "We might want to remember that these types of things occur because we silently consent to it."
- "These things happen because we allow it."
- "You have lost yourself in support of an elephant or a donkey."

### Oneliner

Beau delves into the dangers of partisanship, urging individuals to prioritize morals over political allegiance and speak out against injustice.

### Audience

Active citizens

### On-the-ground actions from transcript

- Speak out against injustice and wrongdoing, regardless of political affiliation (implied)
- Raise your voice against harmful actions and policies in your community (implied)
- Refuse to overlook immoral behavior based on political party loyalty (implied)

### What's missing in summary

The emotional impact and tone of Beau's message, along with the urgency for individuals to take a stand against harmful actions enabled by partisanship.

### Tags

#Partisanship #PoliticalAllegiance #Injustice #Morality #SpeakOut


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight, we're going to talk about partisanship because, man,
it is on full display lately.
Partisanship, when you have developed such a loyalty
to a party that you forget to be loyal to yourself
and your principles.
And it's, I mean, if you just look at the last few days
you look at the headlines, if you go to a liberal leaning website right now, you
will undoubtedly find a headline that says, rich guy with connections to Trump
arrested for heinous things. If you go to a conservative website, you will
undoubtedly find an article that says, rich guy with connections to Clinton
arrested for horrible things. Both of those are true. This guy was connected to
lot of people. A lot of people. Did President Clinton or President Trump
engage in these activities with him? I don't know. I imagine we'll find out, to
be honest, to find out that either or both engaged in it would not surprise me.
me. There's a level of depravity that comes with that much money with having
to push the limits, having that kind of money in power. At the same time we also
need to remember in reference to that last video all of these people come from
good families. So whether or not it occurred doesn't necessarily mean we'll
find out and even if we find out it doesn't necessarily mean they'll be punished.
And that, that fact rests in large part on people's partisanship and their willingness
to overlook the things that their party did while condemning the exact same activity from
the other side.
You hear it a lot on just about any topic.
When something comes up and somebody gets into a corner during a discussion, they will
inevitably say, but Obama did it, but Bush did it, but Clinton did it.
And that's what people say.
What I hear is, I don't fault the second person in a gang rape.
If it's wrong, it's wrong.
fact that somebody else did it first doesn't make it better. If the policy is
wrong it is wrong. And you know you've got a whole bunch of people who are very
very outraged over the allegations towards this this this guy and but they
support the camps they support the Trump concentration camps I find it very hard
to believe that outrage there are physical facilities where that type of
of activity is going on, thousands of reports, thousands of complaints, but doesn't care.
One party at the moment does not care.
If you ever reach the point where you can up play or down play child abuse of that type
political ends that should be a red flag. You have gone way too far down that
rabbit hole. You have lost yourself in support of an elephant or a donkey. We
might want to remember that these types of things occur because we silently
consent to it. We condone it. We don't raise our voice. These things
happen because we allow it. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}