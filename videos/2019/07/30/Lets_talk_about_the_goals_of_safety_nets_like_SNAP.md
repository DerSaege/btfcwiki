---
title: Let's talk about the goals of safety nets like SNAP....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=owjQzkB9RCk) |
| Published | 2019/07/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Most states ignore the asset requirement for SNAP, leading millionaires to receive food stamps.
- The federal government sets the asset requirement for SNAP at $2,250, including assets like cars.
- Americans are advised to save three to six months of income, but 40% can't handle a $400 emergency.
- A person on SNAP who saves up to $2,250 loses assistance, making it challenging to escape poverty.
- It takes 20 years under the current system to lift oneself out of poverty without setbacks.
- The goal of assistance programs like SNAP should be questioned: Is it to keep people above poverty or help them escape it?
- Beau suggests eliminating all means tests for SNAP to truly assist those in need.
- SNAP serves not only as a safety net but also as an economic stimulus during weak economic periods.
- Removing means tests and allowing easier access to SNAP could help people move above the poverty line.
- The issue with SNAP isn't about budget but about control and perpetuating income inequality.

### Quotes

- "If the goal is to build a stronger country, expand SNAP, don't close it."
- "SNAP is not just a safety net program. It's also an economic stimulus program."
- "It's about controlling you."
- "Expand the program. Don't shut it down."
- "Make it more accessible."

### Oneliner

Beau challenges the purpose of SNAP, suggesting eliminating means tests to truly help people escape poverty and build a stronger country.

### Audience

Advocates, policymakers

### On-the-ground actions from transcript

- Expand SNAP to make it more accessible (suggested)
- Challenge the purpose of assistance programs and advocate for change (implied)

### Whats missing in summary

The full transcript dives deeper into the impact of means tests and income inequality on SNAP recipients and society as a whole.

### Tags

#SNAP #Poverty #MeansTests #IncomeInequality #FinancialAssistance


## Transcript
Well howdy there internet people, it's Bo again.
So we're going to talk about SNAP again, food stamps.
After that last video where I talked about the cost of living, people were like, well
there's another loophole that needs to be closed.
You know, most states ignore the asset requirement.
Millionaires got food stamps.
Yeah okay, so we're going to talk about that.
There's a reason that most states ignore this requirement, and it is most states.
I want to say like 30 or more, just ignore it.
The asset requirement as set out by the federal government is $2,250.
If you have $2,250 in assets, well, you don't get food assistance, $2,250.
cases that includes your car if your car is worth more than $4,500, something like that.
So here's a question.
What are you supposed to have set away for emergencies and stuff like that?
If you ask your banker, what is he going to tell you?
What is she going to tell you?
Three to six months of your yearly income.
You're supposed to have set away.
That's what you need to be financially independent.
Most Americans don't have that.
In fact, 40% of Americans cannot handle a $400 emergency.
So what happens to our person on SNAP?
So they're getting assistance.
They start to get ahead.
They put away some money.
They hit that $2,250 mark, and then they lose the assistance.
What happens next?
eat through that savings. A guy from MIT wrote a book and basically it takes 20 years under
the current system with nothing going wrong to get out of poverty. 20 years. A generation.
United States is a pretty powerful country. I think we can do better than that. You know,
When you talk about these millionaires, this millionaire that went and did this, yeah,
he had assets.
He had a retirement account, a home, things like that, but he had no income.
Here's the thing, a retirement account and a home, those are things that everybody's
supposed to have.
That's not something that's just supposed to be the purview of the wealthy, but they've
got you thinking it is.
got you thinking that you should have to work until the day you die, that you need to pay
them to rent.
You shouldn't have any assets.
What is the purpose of these programs?
That's the actual question we need to be asking here.
What is the purpose?
Why do we have them?
Is it to keep people just above the poverty line?
Because if we apply all of these means tests, that's what happens.
they can never get out, if that's the purpose, then we just need to get rid of them.
We need to get rid of them and let nature take its course because what's going to happen
is when people fall into poverty and you have 40 million Americans on snap, 40 million Americans,
oh that's a movement.
We want to talk about the pitchforks coming.
show up real quick. Being hungry has a way of doing that to people. So if the
goal is just to keep people in poverty, let's just get rid of it. But if the goal
is to actually help people, if the goal is to build a stronger country, well then
maybe we should get rid of the means tests altogether. All of them. No, that's
That's crazy.
Not really.
The average payout for SNAP per person is $126.
There's a built-in means test to SNAP.
If it is worth it to you for $126 a month to go through the trouble, to fill out the
paperwork, to go through this process, you probably need it.
If you're willing to deal with the social stigma that
comes from using that card for $126 a month,
you probably need it.
There's a built-in means test, so just get rid of it.
But then people who aren't really needy,
well, they might use it.
You're right.
Worst case scenario, somebody in the middle class
eats a little better.
Who cares?
This is such a tiny chunk of the federal budget.
It's amazing that it's even a topic.
It's not amazing because there's a reason it keeps coming up.
If the goal is to build a stronger country, expand SNAP, don't close it.
See here's the thing, and I don't know that people are aware of this, SNAP is not just
a safety net program.
It's also an economic stimulus program.
If it's a weak economy, which is when and where most SNAP gets spent, for every dollar
that's spent, it expands the economy by $1.70.
It's not a drain on the economy, it's a benefit to it.
So let's just get rid of the means test and allow people to access it and watch those
people on the bottom start to move up, get rid of the barriers, allow them to
get that 126 bucks until they're making 30 grand a year instead of 12. Let's get
them out of being just above the poverty line. That should be the goal. But we
We can't do that because, well, there's another factor.
This isn't about the budget.
The amount of money we're talking about here is nothing.
It's nothing.
In the grand scheme of things, it's nothing.
It's about controlling you.
It's about the people at the top making sure that you know that if you fall down here,
you're never getting out.
If you're in the middle class, oh, you better keep working.
You better thank me for that job I'm giving you.
You better not get in a situation that requires any help, because then you're never getting
out.
It also gives you a carrot.
That's the stick.
It has a carrot, too.
Look, you're better than these people, and it gives these politicians somebody to point
to.
of your problems. Those people at the bottom, those people with exactly zero
lobbying power, they're the reason you're having a bad time. When you say that and
you say it out loud, you realize exactly how stupid that is. The decisions that
affect you are not made in Section 8 housing. They're made on Capitol Hill.
kicking down, punch up. It's not these people that are going to keep you from
your dreams, but instead what those at the top do with their pundits in
the media is sell you on the idea that if you're a good boy and you do what
you're supposed to, one day you're going to own the boot that's on somebody else's
neck. No, you won't. You won't. Every chart shows that income inequality is growing.
The people at the bottom, they can't take any more from them.
Who are they taking it from?
Me and you.
They're taking it from the middle.
You allow this to go on, you don't break free of this, you keep blaming the people that
have no power for the problems in your life, you're going to be one of those with no power.
It's just a matter of time.
look at the charts, it's not a question of if, it's a question of when. Because
you're not going to float to the top. It's not going to happen. Everybody in
this country is a temporarily embarrassed millionaire. It's not true.
We can express some solidarity with those people who are in a bad way and we
We can help them or we can become one of them.
Remember what the purpose of these programs is supposed to be.
It's not to keep people poor.
It's not to give you somebody to look down on.
It's supposed to be to help people.
And let's say you don't care about that.
I'm going to give you a selfish reason to change this program and make it more accessible
rather than less accessible.
As it stands, you're not getting off.
You can't get off the program once you're on it.
If you play by the rules, you'll never
be able to have enough in savings
to actually get out of poverty, which
means you're going to pay for them the rest of your life.
If you're in the middle and this program isn't changed
to make it more accessible, not less accessible,
you'll pay for it the rest of your life.
So there's your selfish reason for the altruistic people out there.
Expand the program.
Don't shut it down.
Make it more accessible.
If it's worth it to you to go through that headache for $126 a month, you need the money.
That's the means test.
Anyway, it's just a thought.
y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}