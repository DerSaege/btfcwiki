---
title: Hillbilly tries to 'Merica with gas and fireworks, blows up instead....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_VwfYeEhbvo) |
| Published | 2019/07/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau plans to light a giant box of fireworks soaked in gasoline on the 4th of July.
- He questions the celebration of America while mentioning the government's treatment of people in concentration camps.
- Beau criticizes the lack of basic necessities like soap, toothpaste, and food for those in detention.
- He points out the indifference towards sexual abuse complaints in certain locations.
- Questioning the celebration of freedom when such injustices occur.
- Beau mentions the guarantee of the pursuit of happiness in the document but questions its application.
- Ending with a thought-provoking message about the 4th of July celebrations.

### Quotes

- "What are you celebrating? Freedom? No. No, don't think so."
- "I didn't see anything about, you know, lines in the sand prohibiting that."
- "Y'all have a happy Fourth of July. It's just a thought."

### Oneliner

Beau questions the celebration of America on the 4th of July in light of government treatment of individuals in detention, lack of investigation into sexual abuse complaints, and the pursuit of happiness being overlooked.

### Audience

Activists, Americans

### On-the-ground actions from transcript

- Question the celebration of national holidays (implied)
- Advocate for the rights and well-being of individuals in detention (implied)
- Raise awareness about overlooked injustices (implied)

### Whats missing in summary

The full transcript provides a deeper reflection on the contradictions between celebrating freedom and happiness on the 4th of July while neglecting the basic rights and well-being of certain individuals.

### Tags

#4thofJuly #Freedom #Injustice #HumanRights #America #Activism


## Transcript
Well, howdy there, internet people, it's Bo again,
and it is the 4th of July.
So, I got a giant box of fireworks,
and what we're gonna do is we're gonna light it all
on fire at once, I soaked it in gasoline,
we're gonna see what happens, it's gonna be great.
Um, and we're doing this because it's the 4th of July,
and we got that important document that got released today,
Celebratin' America.
All men are created equal,
unless you're in a concentration camp,
Where the government is arguing that they don't have to provide you with soap, or toothpaste, or even sleep, adequate
food, bathing, none of that.  Because I guess those people, they're not real people.
Or maybe you're in one of those locations that there's thousands of sexual abuse complaints that get
filed, but they're not investigated because nobody cares what happens to kids if they're brown, right?
What are you celebrating? Freedom? No.  No, don't think so.
Another phrase that's in that document is the guarantee to the pursuit of happiness.
I didn't see anything about, you know, lines in the sand prohibiting that.
All men, endowed by their creator.
Anyway, y'all have a happy Fourth of July.
It's just a thought.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}