---
title: Let's talk about money and expansion....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=O_igsJyR1D4) |
| Published | 2019/07/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introducing a unique video format unlike any done before.
- Explaining the reasons for not asking viewers to like, subscribe, or support Patreon in every video.
- Announcing the launch of a Patreon account due to viewer requests.
- Describing Patreon as a platform for sponsorships starting at a dollar per month.
- Detailing the perks of sponsoring, including access to a Discord server, voting on future projects, and early video releases.
- Ensuring that no content will be kept behind a paywall.
- Mentioning plans to use the Patreon funds to free up time from battling algorithms on YouTube and Facebook.
- Expressing a desire to expand into other media forms like podcasts and writing a book.
- Stating the intention to use the funds to pay people who might assist in these new ventures.
- Sharing a vision of potentially taking the show on the road to actively help build community networks and address social issues.

### Quotes

- "Beyond that, I'd like to branch out into other types of media, the podcast everybody's asking for, I'd like to write a book too."
- "It's tentative because we have no idea how this is going to play out."
- "The rest is up to you guys."

### Oneliner

Beau introduces a new video format, explains Patreon launch, and shares plans to expand into other media forms, seeking support for future projects and community initiatives.

### Audience

Viewers

### On-the-ground actions from transcript

- Support Beau's Patreon account to help him free up time from battling algorithms on social media and to assist in expanding into other media forms (suggested).
- Engage with the community by participating in live streams and supporting efforts to address social issues (implied).

### Whats missing in summary

More details on specific ways viewers can contribute to community-building initiatives and support Beau's expansion into other media forms.

### Tags

#ContentCreator #Patreon #CommunityBuilding #Support #SocialIssues


## Transcript
Well, howdy there internet people it's Bo again, so tonight's gonna be unlike any video. I've done before
As you probably noticed I don't do the whole click like subscribe
Ring the bell speech in every one of my videos mainly because I find it annoying when I'm watching somebody else's video
So I don't do it in mine
Same thing applies for support my patreon
Mainly because until about 15 minutes ago. I didn't have one you guys have asked for it. So now it exists
If you're not familiar with Patreon, it is a platform that allows you to basically sponsor me.
It charges you once a month. Our plans start at a dollar and go up from there.
In exchange for that, you get little perks, get access to a Discord server, it's a chat room,
the ability to vote on future projects. You'll get to see some selective videos early.
nothing will stay behind the paywall that was something I was pretty adamant
about participate in live streams stuff like that at the higher levels you'll
get merchandise shipped to you and this would be like a t-shirt that isn't
available in the teespring store or something along those lines even at the
lower levels you can probably expect something every once in a while so
where's the money gonna go well initially it's just gonna go to free me
up from the algorithm, so I don't have to worry about fighting YouTube and Facebook
at the same time just to get the video seen.
Beyond that, I'd like to branch out into other types of media, the podcast everybody's
asking for, I'd like to write a book too, actually, and a few other things.
Some of those things are going to require help, and this money will pay those people.
And then eventually, if we get enough sponsors or supporters, I'd like to take this show
on the road.
And I don't just mean going around speaking, I mean actually getting out there and doing
the force multiplication that we talk about, helping people build those community networks
and addressing some of the issues that we talk about in these videos.
Now that's the plan.
It's tentative because we have no idea how this is going to play out.
subject to the whims of fate but that's that's the plan it's well it's just a
the thought. The rest is up to you guys. Anyway, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}