---
title: Let's talk about why they can't fix their country....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=mhlAEqf39Sg) |
| Published | 2019/07/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the history of the Northern Triangle countries (El Salvador, Honduras, Guatemala) and their current situations.
- Details how the US intervention, support, and influence have significantly contributed to the turmoil in these countries.
- Mentions the role of organizations like MS-13 and Barrio-18, originally from the US but transplanted to El Salvador, in causing violence and instability.
- Describes the impact of US interventions in Honduras, particularly in controlling the economy and military.
- Talks about the United Fruit Company's control over Guatemala and the subsequent interventions that disrupted the country's attempts at self-governance.
- Criticizes US actions, including supporting coups and overthrowing democratically elected governments, leading to prolonged civil unrest and human rights abuses in these countries.
- Calls for accountability for the US's role in destabilizing Central and South America.
- Emphasizes the need for the US to stop causing harm in other countries rather than fortifying itself against the consequences.

### Quotes

- "We don't need to put better locks on our houses of the United States because other people are breaking in, we just need to stop setting other people's houses on fire."
- "It isn't that they can't fix their country. They did it multiple times. It got overthrown because you can't fix your country."
- "We can't constantly allow our government to do this and then cry when people try to get away from the situations that we created."

### Oneliner

Beau explains how US interventions have fueled instability in the Northern Triangle, urging accountability and a shift in foreign policy to stop perpetuating harm in Central and South America.

### Audience

Policy Makers, Activists, Advocates

### On-the-ground actions from transcript

- Hold policymakers accountable for past and present US interventions (implied)
- Advocate for a shift in US foreign policy towards non-intervention and support for self-governance in affected countries (implied)
- Support organizations working towards justice and stability in Central and South America (implied)

### Whats missing in summary

The full transcript provides a detailed historical context and analysis of US interventions in Central and South America, shedding light on the root causes of migration and instability in the region.

### Tags

#USIntervention #CentralAmerica #Migration #Accountability #ForeignPolicy


## Transcript
Well, howdy there, internet people, it's Beau again.
I did that video on MS-13 and the situation in El Salvador,
explained why people were fleeing.
And of course, there were comments saying,
but that's just one country.
You're trying to cast that as if that's everything.
I apologize.
That was an oversight on my part.
I should have seen that coming.
So challenge accepted.
Most of the migrants are coming from the Sunni triangle.
Not the Sunni triangle.
The Northern Triangle, my bad, and that is made up of El Salvador, Honduras, and Guatemala.
I'm going to do a real quick recap of El Salvador.
The 1980s, there were a series of junta's, US supported, triggered a 12-year civil war.
US trained troops during this period engaged in massacres, war crimes, all kinds of stuff.
Horrible, horrible things.
At the end of the war, there was a peace treaty.
The peace treaty stipulated that because the police engaged in a lot of this, they had
to be disbanded.
The military was not very strong, it had been reduced.
We chose this exact moment to send a gang back to El Salvador, and by back I mean there.
This gang is MS-13.
MS-13 was not founded in El Salvador, it was founded in the United States.
We transplanted it there.
Once they were there, they had no police opposition, so of course they got a pretty good footprint.
Real quick, got a good hold on the country.
Even more so, once they ran into a guy named Ernesto Deris, who we trained, he took his
training and turned MS-13 from the penny ante street gang that it was, into the transnational
threat that it is today.
His nickname was Satan.
All of that's discussed in detail in the other video, so let's move on.
talk about Honduras and we're going to go all the way back to show exactly how
long we've been doing this. In 1914, U.S. banana companies owned a million acres.
By 1920, they owned all the good soil. Peasants had no choice but to work for them
because they couldn't get good soil of their own. During this period, there were
some gripes from the government, so we engaged in a couple of direct political
or military interventions down there.
By the 1960s, the military was all that mattered.
It was the only real political institution in the country.
By the 80s, under Reagan, it was USS Honduras.
It was a staging area, that's it.
We controlled that country in every way, from the economy to the government to the military.
And we militarized the entire country to deal with the Sandinistas in Nicaragua, yet another
country we screwed up.
This militarization of course led to tyranny.
There were political assassinations, mass disappearances, illegal detentions, so on
and so forth.
And this goes on until 2006 when they finally get a reformer.
They finally get a guy in there who's going to fix it.
They're going to fix their country.
He lasts three years until a US-supported coup ousted him.
Since then, the heavy hand of repression is back.
Two thirds of the people live in poverty, they have to deal with gangs like MMS-13 from
neighboring El Salvador, which as we just discussed is our creation, as well as Barrio-18,
which basically just modeled themselves after MS-13 using the same training and tactics
and everything else.
Gender-based violence is the second leading cause of death for women of reproductive age
in that country. Abortion is illegal, so when sexual violence does occur, as it often does,
the women have no choice but to carry that pregnancy to term. However, there's no real
health care, so they're likely to die. This is why you see a bunch of pregnant women making
this trip. So that's Honduras in a nutshell. Now let's go to Guatemala. I'm going to speed
through it a little bit faster because it's not like it's the exact same
story. In 1900 the United Fruit Company runs the country. I mean for real the
government is just a rubber stamp for them. They control the ports, they
control everything. By 1920 because of this the Guatemalan people decide to
fix their country. They overthrow the government that is so compliant. What
does the US do? They send troops and they basically tell them you're gonna be just
like the last guys and if not we're going to depose you.
By 1931 a real thug has taken over.
There's a series of authoritarian thugs along the way but in 1931 a really bad guy came
in and he authorized the landowners who were supplying the banana plantations to punish
their employees any way they saw fit.
This included rape and execution, land of the free, home of the brave.
Now because of this brutality in 1944 there was an overthrow of this guy and there was
a little panel that ran it for a few years but in 1950 they finally got free elections.
They fixed their country, they did it, woo hoo, right?
And everybody's excited because democracy came.
No!
Four years later, in 1954, the CIA overthrew the democratically elected government in Operation
PB Success.
They ousted them.
This triggered 40 years of civil war.
40 years.
Along the way, the Mayan genocide occurred.
US trained troops engaged in all kinds of atrocities.
Now this all officially ended in 1996, but it really didn't end, it just kind of tapered
off, started tapering off at that point.
Today they're in the exact same situation Honduras is.
They're plagued by gangs, they have massive poverty, no healthcare, constant corruption,
police brutality, government repression.
of these things that we did. We did all of this. And this, although we're just focusing
on the Northern Triangle, this is an example of pretty much all of Central America and
most of South America. This is what happened under the Monroe Doctrine. We don't need
to put better locks on our houses, on our house of the United States because other people
are breaking in, we just need to stop setting other people's houses on fire.
That's what we need to do.
It isn't that they can't fix their country.
They did it multiple times.
It got overthrown because you can't fix your country.
We can't constantly allow our government to do this and then cry when people try to
get away from the situations that we created.
So yes, it is our fault, yes, it is our responsibility, no, no, it isn't just El Salvador, this is
everywhere down there anyway it's just a thought y'all have a y'all have a good
day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}