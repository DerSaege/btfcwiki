---
title: Let's talk about prosthetic legs, George Soros, and Charles Koch....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=h3rzIOeLBNU) |
| Published | 2019/07/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau experiences a moment at the grocery store with his four-year-old son where he anticipates his son saying something about a woman with a camouflage prosthetic leg, but instead, the son admires her, calling her cool.
- George Soros and Charles Koch, considered America's bond villains by Beau, are teaming up to start a think tank called the Quincy Institute for Responsible Statecraft with the goal of ending forever wars like those in Afghanistan and Iraq.
- The Institute aims to embody John Quincy Adams' vision of American foreign policy, focusing on diplomacy over military intervention.
- Beau encourages grassroots anti-war movements to seize this moment and work alongside special interest groups supported by Soros and Koch.
- He suggests that incorporating personal stories of those affected by wars, like veterans who have served multiple tours, into the anti-war movement could be more impactful than solely focusing on economic and policy aspects.
- Beau acknowledges potential skepticism about billionaires profiting from peace efforts but believes in the potential reduction of prosthetic needs due to fewer wars.

### Quotes

- "You are so cool."
- "This might be the golden moment for the anti-war movement to pop its head back up."
- "That's force multiplication, guys."
- "For once the big boys are going to be on your side."
- "There's going to be a whole lot less prosthetics. I'm okay with that."

### Oneliner

Beau experiences a heartwarming moment at the grocery store with his son and sheds light on the collaboration between Soros and Koch to establish a think tank aiming to end forever wars, urging grassroots movements to seize the moment for change.

### Audience

Peace advocates, activists

### On-the-ground actions from transcript

- Keep an eye on the Quincy Institute for Responsible Statecraft's efforts (suggested)

### What's missing in summary

The full transcript provides a detailed insight into Beau's reflections on anti-war movements, the collaboration between Soros and Koch, and the potential impact of personal narratives in advocating for peace.

### Tags

#AntiWar #GrassrootsActivism #Collaboration #Diplomacy #Veterans


## Transcript
Well, howdy there, internet people, it's Bo again.
He's at the grocery store today.
Got my four-year-old with me.
And pull into the aisle to check out.
He's in the cart.
And I have that moment that most parents have at some point
where you go, oh, no, don't say anything, just be quiet.
Just be quiet.
Because the woman in front of us has a prosthetic.
She's wearing shorts.
And the top of the prosthetic is camouflage.
It's ACU.
She's obviously ex-military.
And I see him looking at it, and I'm like, oh, no.
What's he going to say?
And he looks at her, and he's like, you are so cool.
The reason I'm saying this is because I found out
the other day that America's two bond villains are up to something.
There are two men in American politics that if they wanted to be bond villains, they could.
George Soros and Charles Koch.
If you're not familiar with these guys, George Soros is a liberal financier.
He's a billionaire.
He's pumped more than $30 billion into something called the Open Societies Foundation.
Charles Koch is the right-wing equivalent.
He's worth about $50 billion.
He also has a foundation.
He's a libertarian in the American sense of that word, deregulation, small government,
that kind of thing.
They've teamed up to start a new think tank.
And right now, conspiracy theorists all over the world, their ears have perked.
The goal is to stop forever wars, like Afghanistan and Iraq, where that woman's leg probably
is.
That's the goal.
The think tank will open in September.
It's named the Quincy Institute for Responsible Statecraft.
The goal is to try to embody John Quincy Adams and his vision of American foreign policy,
which is we don't go abroad searching for monsters to fight.
Diplomacy becomes the preferred method of foreign affairs.
I'm certain that somebody out there is going, well, billionaires found a way to make money
off peace, good, good.
This might be the golden moment for the anti-war movement to pop its head back up because some
of y'all have been missing for a while.
For once, grassroots anti-war movements are going to be able to benefit from special interest
groups.
This institute will certainly hire policy experts, lobbyists, essayists, people who
You can give talking points, PR people, people who are going to be in Congress's face.
They're going to focus on the economic side of it, I'm almost certain.
Given these two guys, that's what they're going to talk about.
Wouldn't it be great and a whole lot more effective if they also had some faces?
There are people in these comments sections who have mentioned doing five or six tours.
probably got some stories about the forever wars, would make the defense
industry's job a whole lot harder if there was a personal aspect not just an
economic and policy one. I'm not saying you got to go to work for them, I'm not
saying you got to take their money, I'm certainly not saying you got to let
be co-opted but you may want to check in on this Institute and see what they're
doing every once in a while because if you coincide your marches with their
efforts that is force multiplication guys I mean that's like calling an
artillery because for once the big boys are going to be on your side for
whatever reason I'm not even going to speculate as to motive but at the end of
the day even if they did find some way to profit off of it and they probably
did there's going to be a whole lot less prosthetics I'm okay with that I'm okay
with it anyway it's just a thought y'all have a good
night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}