---
title: Let's talk about the most important Presidential campaign promise....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Y7yv5_xSmHU) |
| Published | 2019/07/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau supports ideas over politicians, believing good ideas stand on their own merits regardless of who presents them.
- He heard a powerful idea recently that he believes every presidential candidate should endorse, regarding the restoration of the rule of law in the treatment of immigrants.
- Beau criticizes the abuse, physical and sexual, of immigrants and the lack of medical care provided to them, calling for accountability and justice.
- He specifically mentions President Elizabeth Warren and her commitment to addressing crimes against immigrants on her first day in office.
- Beau expresses disappointment that such a promise is even necessary from a presidential candidate, indicating a concerning lack of commitment to upholding the rule of law.
- He references a clip where a former ICE director argues with AOC about asylum seekers, showcasing differing interpretations of the law.
- Beau advocates for upholding the rule of law, citing specific legal provisions that protect asylum seekers and criticizing any attempts to undermine these laws.
- He stresses the importance of maintaining the rule of law for the functioning of a nation and calls for accountability from those in power.

### Quotes

- "If it's a good idea, it's a good idea."
- "We cannot allow a tradition of just following orders to take root in this country."
- "That's law. That's the rule of law."
- "If we're going to have a nation, it's got to have the rule of law, right?"
- "This is an important promise and it needs to be kept by whoever occupies the Oval Office next."

### Oneliner

Beau insists on the importance of upholding the rule of law, particularly in the treatment of immigrants, urging all presidential candidates to endorse this fundamental principle.

### Audience

Voters, Immigration Advocates

### On-the-ground actions from transcript

- Support organizations advocating for immigrant rights (implied)
- Stay informed about immigration laws and rights (implied)
- Advocate for accountability in the treatment of immigrants (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of Beau's views on the rule of law and its significance in addressing issues related to immigration.

### Tags

#RuleOfLaw #Immigration #PresidentialCandidates #Accountability #Advocacy


## Transcript
Well, howdy there, internet people.
It's Beau again.
I don't support politicians.
I don't endorse them.
I don't support politicians.
I support ideas.
Ideas stand or fall on their own.
It doesn't matter who says them.
If it's a good idea, it's a good idea.
Sometimes there's an idea that is so good that if every
candidate doesn't endorse them, you kind of have to
wonder about. And I heard one of those ideas in the last couple of days. It's
one of those ideas that if every candidate for the presidency doesn't
take on to their platform, well you have to wonder about their commitment to the
rule of law, which is something that really has to be restored. To anyone out
there, who's working in this system.
Understand, you abuse immigrants, you
physically abuse immigrants, you sexually
abuse immigrants, you fail to get them
medical care when they need, you break
the law of the United States of America
and Donald Trump may be willing to look
the other way, but President Elizabeth
Warren will not. On my first day, I will
empower a commission in the Department
of Justice to investigate crimes
committed by the United States against
against immigrants.
The fact that a presidential candidate has to make a promise like this is pretty sad.
The fact that they haven't all already made it is pretty bad too.
This is an important promise that has to be kept.
We cannot allow a tradition of just following orders to take root in this country.
There are a lot of people in this system that don't even know what the laws are.
There's a clip floating around Facebook right now of a former ICE director arguing with
AOC about asylum seekers being illegal.
And she's like, you know, claiming asylum is legal and he's like, but crossing the
border without authorization isn't and he cites the code she says but they're
not charged and he goes off on a tangent talking about he's saving lives she's
right article 6 section 2 of the US Constitution gives international treaty
the weight of law article 31 of the 1967 protocols on the treatment of refugees
asylum seekers cannot be punished for entering without authorization it's kind
like an advanced pardon.
This is normally illegal, but not under these circumstances.
She's right.
He didn't school her.
She was correct.
That's law.
That's the rule of law.
A president having a whim to change that means nothing.
If we're going to have a nation, it's got to have the rule of law, right?
that what all of these people have been saying this entire time if you want to
do something do it legally that applies to the government too the Constitution
is a permission slip to the government this is what you're allowed to do they
are going well beyond that permission slip this is an important promise and
it needs to be kept by whoever occupies the Oval Office next. Anyway, it's just a
thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}