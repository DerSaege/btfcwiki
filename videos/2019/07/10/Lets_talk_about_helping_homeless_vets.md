---
title: Let's talk about helping homeless vets....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=K7gFW0nfF_I) |
| Published | 2019/07/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- United States is resilient and populated by resourceful people.
- Americans can multitask and handle multiple challenges simultaneously.
- Challenges like fighting wars and sending a man to the moon were tackled simultaneously in the past.
- The issue at hand is homelessness among veterans.
- On average, 40,056 homeless veterans spend their nights on the streets.
- To address this problem, facilities offering food, beds, showers, hygiene items, psychological support, education, and recreation are needed.
- In 2018, there were over 180 facilities with more than enough beds to house 40,520 people.
- Beau proposes moving criminal aliens to county jails and releasing non-criminal migrants for court dates, in line with laws and international norms.
- Retraining guards in existing facilities for job reentry programs could solve the problem.
- Beau believes the money needed for this solution has always been available.
- Many people use homeless vets as an excuse to avoid helping others.
- Combat veterans, according to Beau, are compassionate and willing to help others in need.
- Beau questions the deportation of veterans and the lack of care for homeless vets.
- He suggests repurposing existing camps into facilities for homeless vets as a viable solution.
- Beau challenges the idea that helping migrants is prioritized over homeless vets, pointing out the available resources for veterans.

### Quotes

- "We can do more than one thing at a time."
- "The money's always been there for this to be done."
- "You want an excuse to not help other people, and they're using homeless vets to do that."
- "If you want a solution, there it is. Turn these camps into facilities to house homeless vets. Done."
- "The question is why do you want to lock up toddlers instead of helping homeless vets?"

### Oneliner

Beau challenges the false dilemma of helping migrants or homeless vets, proposing existing solutions for veteran homelessness while critiquing misplaced priorities.

### Audience

Advocates for homeless veterans

### On-the-ground actions from transcript

- Repurpose existing camps into facilities for homeless veterans (suggested)

### Whats missing in summary

The full transcript provides a passionate call to action for repurposing existing resources to address veteran homelessness effectively.

### Tags

#VeteranHomelessness #Priorities #Solutions #CommunitySupport


## Transcript
Well, howdy there, internet people, it's Beau again.
The United States is a resilient country,
populated by a very resourceful people.
We can do more than one thing at a time.
We can.
We fought the Germans at the same time
we were fighting the Japanese.
We were fighting the Vietnam War
and working to put a man on the moon.
We fought in Iraq and Afghanistan at the same time.
We can do more than one thing at a time.
We are multitasking people.
So why does it always get presented as an either-or?
You'd rather help migrants than our own homeless vets.
I think we can do both.
But to do that, we've got to know how big the problem is.
On the average night, 40,056 is the number
homeless vets on the street in an average night. 40,056 please remember
that number. What do you need to solve that problem? Well in every state and in
some states you need more than one but you're gonna need a facility. Need to
offer food, beds, showers, hygiene stuff, maybe a psychologist on staff, educational
programs, recreational programs. That's what you need to solve the problem. If only a network
of facilities like that already existed, more than 180 of them, with enough beds to house
40,520 people in 2018.
More than enough.
So here's my plan.
We take the actual criminal aliens, those that have committed a crime in their home
country and we put them in county jails where they belong.
And those that haven't, well we let them out and let them come to their court date as we
We should, you know, buy our own laws, international laws, the Constitution.
We do that.
And then we have 180 facilities plus with more than enough beds for every homeless vet
in the country, and they're already geared to house them.
Retrain the guards to do job reentry programs.
Problem solved.
See, the thing is, the money's always been there.
It's always been there for this to be done.
But the truth is, most people don't actually care about homeless vets.
They want an excuse to not help other people, and they're using homeless vets to do that.
And man, I got to tell you, I know a whole lot of combat vets.
I have never heard one say, don't help that person, ever.
I know some that want without meals so they could give that MRE to somebody that needed
it more.
I think it's pretty insulting to constantly use them as the reason to not help somebody.
It doesn't really fit with their character from what I've seen.
And I really find it funny when you say, well, we've got to help our homeless vets, meanwhile
we are deporting vets.
You think they've got a home waiting for them in that next country?
Truth is, you don't actually care about homeless vets.
You want an excuse.
But if you want a solution, there it is.
turn these camps into facilities to house homeless vets. Done. Problem solved. The money's
there, or would you rather spend it locking up toddlers, teens, people that have done
nothing wrong except work for a better life, people who have done nothing wrong except
for wanting the life that you would probably say was guaranteed by the actions of those
vets.
So the question isn't why do people want to help migrants more than homeless vets.
The question is why do you want to lock up toddlers instead of helping homeless vets?
The facilities exist.
They're already there.
There's more than enough vets and if they do have the conditions that everybody seems
to think they do and they're great and everything's hunky dory and there's no problems, they're
already designed.
you're already ready. Just process it. Make it happen. But you're not going to do that.
Because as much as you talk about your compassion, well you're really governed by fear. Fear
of the other. Fear of Spanish speaking toddlers. Anyway, it's just a thought. Y'all have a
Good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}