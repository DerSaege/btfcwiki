---
title: Let's talk about Hermitage and Andrew Jackson....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=LXC6Fbdy4DM) |
| Published | 2019/07/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau provides historical context by talking about Andrew Jackson's victory at the Battle of New Orleans, which propelled him to the national scene.
- Jackson's victory happened after the War of 1812 had technically ended with the Treaty of Ghent signed, but news hadn't reached him yet.
- Jackson's success at the Battle of New Orleans, though attributed to a British battle plan failure, made him a hero and later helped him become President.
- Beau recalls visiting Jackson's plantation, Hermitage, as a child, where the sanitized version of plantation life was presented without addressing slavery or cruelty.
- He mentions growing up in an echo chamber in Tennessee with Confederate flags prevalent, hindering meaningful discourse.
- Beau draws parallels between the echo chamber of his upbringing and the potential echo chamber within ICE and Border Patrol regarding immigration issues.
- Public opinion has shifted on immigration, but the battles may continue despite the war being deemed over.
- Beau warns those working for ICE and Border Patrol about potential future repercussions and job prospects, contrasting their situation with that of political figures.
- He mentions a diverse group in Hermitage who publicly broke the law to rescue a father and son from ICE, showcasing opposition to current policies.
- Beau sees this incident as a turning point, indicating the strong level of opposition that could impact the future employment prospects of those in Border Patrol and ICE.

### Quotes

- "The view inside an echo chamber, the view inside of an agency like that, it's not the view of the public and the talking points on TV."
- "Hermitage, an impossibly diverse group of people, ages, ethnicities, languages, everything, banded together and broke the law publicly, no masks, broke the law to get that father and son away from ICE."
- "That's the level of opposition that exists."

### Oneliner

Beau analyzes historical echoes, warns against echo chambers in ICE and Border Patrol, and hails community action in Hermitage against ICE, foreseeing repercussions for those enforcing questionable policies.

### Audience

Community members, activists

### On-the-ground actions from transcript

- Band together with neighbors to oppose unjust policies and take direct action to protect vulnerable community members (exemplified)

### Whats missing in summary

The full transcript provides a detailed examination of historical echoes, warning against echo chambers in enforcing agencies, and celebrating community action against unjust policies. Viewing the full transcript offers a deeper understanding of these interconnected themes.

### Tags

#AndrewJackson #Hermitage #CommunityAction #Immigration #HistoricalEchoes #ICE #BorderPatrol


## Transcript
Well, howdy there Internet people, it's Bo again.
So I want to talk about what happened in Hermitage, but before we get into that, I want to talk
about Andrew Jackson because there's a cool little historical connection in my mind, beyond
the obvious.
So Andrew Jackson got propelled to the national scene after his victory at the Battle of New
Orleans.
during the War of 1812, but not really, because the war was over.
The Treaty of Ghent had already been signed, just news hadn't gotten to him yet.
But the war, hostilities, had ended.
The Brits were trying to take New Orleans, Jackson defeated them.
In reality it was more a bad British battle plan than Jackson's brilliance.
But it didn't matter, plays, songs, movies, even later, were made about it.
Made him a hero.
That's what enabled him to attain the presidency later, was a battle that was fought after
the war was over.
Sometimes wars are won or lost, and the fighting goes on.
Jackson also owned a plantation, Hermitage.
Same area.
That's where it gets its name.
And it was a plantation in the truest sense of the term with everything that that entails.
I've mentioned I grew up on the Tennessee-Kentucky line.
We used to take field trips to Hermitage.
Now, back then, when you were there,
you didn't learn about slavery, not really.
You didn't learn about the cruelty or anything like that.
You were given a very sanitized version.
You didn't really learn much about Jackson.
You learned about the benefits of plantation life.
And that happened because back then, Tennessee existed in an echo chamber.
There were Confederate flags everywhere.
The South was going to rise again or whatever.
Now I would imagine, after seeing the character of the people that are in that community today,
the tours are a little bit different.
I imagine they go into things in a little better detail and actually have some meaningful
discussions about the cruelty, but it didn't happen at that time, didn't happen at that
time because it was an echo chamber.
There weren't any dissenting voices.
I have to feel that those people working for ICE or Border Patrol, they're in the same
kind of echo chamber, and they don't realize that the war on immigrants, it's over.
It's over.
public opinion has shifted and that was illustrated by people in hermitage. The
war is over. The battles are going to still keep going on. The fighting will
continue but as far as this becoming an ongoing national policy, it's
going to happen. The thing is, Jackson became an icon because he won the battle
after the war was over. If you go on fighting after the war is over and you
lose, well it's a little different. See, when this administration is over, the
The president, he's going to go back to his gold-plated toilets.
He's set.
You're working for border patrol or ICE now.
You don't have that.
I've already heard people who contract with the government talking about how they don't
feel that they can hire anybody who worked specifically at Clint was the conversation.
Some of them because of the obvious PR issues, and some of them because they honestly feel
like if you work there and you participate, you can't make a moral decision.
If you're working for the feds, you're working in one of these agencies, you're looking
forward to your retirement and probably to that cush contracting job when you're out.
Those jobs may be gone.
jobs may be gone. And this conversation means nothing. It's not going to change that. Maybe
public opinion sways back and people forgive it or forget it. But maybe not. It's not going
to change anything for you. But I'm hoping that maybe it will change things for other
people. They get asked to do something questionable and they need to remember
that the view inside an echo chamber, the view inside of an agency like that,
it's not the view of the public and the talking points on TV. If you're listening
to those who support you, it's not the view of the public either, Hermitage, an impossibly
diverse group of people, ages, ethnicities, languages, everything, banded together and
broke the law publicly, no masks, broke the law to get that father and son away from ICE.
turning point. A lot of people pointed to religious organizations sheltering those
that ICE was looking for as a turning point. Yeah, but you can count on
religious organizations to do stuff like that most times. These are random people.
These are neighbors. No fear. I'm certain that many of them understood they could
been arrested for that, and they did it anyway.
That's the level of opposition that exists.
It does not bode well for the future employment prospects of those in Border Patrol and ICE.
Even those who don't take a moral stance on the issue, just from a PR perspective, the
sent you down a pretty bad road. But he'll be fine. Don't worry. Anyway, it's just a
thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}