---
title: Let's talk about Penthouses and Taco Bells....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=R4z9FPD8ZiM) |
| Published | 2019/07/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Beau missed a few days but assures everyone he is fine after attending a conference in Vegas.
- He attended AnarchoVegas, a big event for him, and had a unique experience.
- Beau interacted with a Taco Bell employee who recognized him from YouTube, sparking a meaningful connection.
- The conference included a fundraiser for the Free Ross campaign, aiming to raise funds for Ross Ulbricht's legal defense.
- The event mainly consisted of entrepreneurs striving to use their businesses for social change, alongside street activists.
- Beau reflected on feeling out of place but acknowledged the value of attending such conferences to learn and network.
- He realized the importance of following his own advice on getting involved based on skill set, leading him to plan community-building events in the deep south.
- Beau emphasized the significance of informal gatherings for networking and community-building, aiming to start the initiative in the deep south before expanding elsewhere.

### Quotes

- "I learned a whole lot, probably more than I contributed while I was there."
- "If you have the means, you have the responsibility."
- "Everybody who shows up to an event about building a community network, that's your network."
- "Rather than it being a lecture, it's a conversation."
- "You need to be attending these things, you need to be going because you can learn a lot."

### Oneliner

Beau shares insights from attending AnarchoVegas, stressing the importance of networking and community-building based on skill sets and responsibilities.

### Audience

Activists, Entrepreneurs, Community Builders

### On-the-ground actions from transcript

- Start organizing informal gatherings in your community to build networks and initiate social change (suggested).
- Attend conferences or events related to your skills and interests to network and learn from others (suggested).
- Participate in community-building initiatives to widen your network and influence society positively (suggested).

### Whats missing in summary

The full transcript provides a comprehensive look into Beau's experience at AnarchoVegas and the lessons learned about networking, community-building, and taking responsibility for social change initiatives.

### Tags

#Networking #CommunityBuilding #Responsibility #Activism #Entrepreneurship


## Transcript
Well, howdy there, internet people, it's Beau again.
So I've been in Vegas, that's where I've been.
Sorry, I've missed the last few days.
Thank you for the messages of concern, I'm fine.
I went out to Vegas to speak
at a conference called AnarchoVegas.
It's a unique experience for me
because I normally don't do big events like that.
I don't go to these conferences.
This is the first time I've ever done one.
As we're driving out there in a Taco Bell,
and as I'm placing my order, the guy, he looks up,
and he's like, no way, YouTube.
Yeah, and I sit there, and I talk to him for a minute.
And we're in this small town, and he's
talking about the things that we talk about on this channel.
And I can tell that he may not have a lot of people around him that he can talk to about
it.
And this little incident feeds into everything else that happens on this trip for me.
The first night of the conference is a fundraiser for the Free Ross campaign.
If you're not familiar with this, it's definitely something worth looking into, the hashtag
Free Ross.
He got a pretty raw deal, and you have to look a little bit deeper into what actually
happened rather than just the press releases on this.
But he got a pretty raw deal, and we were raising money for his legal defense.
But this fundraiser occurred in the penthouse of a 40-story building in Vegas.
Most of the people around us at this event, there's probably 150 of them.
They are people who attempt to use the market to affect social change.
They're entrepreneurs in some way.
But they're entrepreneurs with some form of conscience.
And they're hoping that their product or their company can help advance the cause of freedom.
That's the main person at this thing.
And then there's a handful of street activists there.
And of course, we clump together like we're on a lifeboat because we are way out of our
element.
After the fundraiser, I'm standing outside with this guy who I've known for a few years
and didn't ask his name to see if I could use his name in this, so we're going to call
him Johnny.
Johnny and I are standing outside on the patio and we're looking out over the city.
And without saying anything, we're both thinking the same thing and we both know we're thinking
the same thing somewhere down there 400 and something feet below us there's something
that we need to be mixed up in and instead we're up here
and it's not a slight against the people at this conference at all it's a difference in tactics
And as the conference went on, I watched.
And to be honest, I shouldn't have
been speaking at this thing.
I should have been in the crowd, because I learned a lot.
The fact that I am not a big conference type of person
doesn't mean these things are bad.
If you have that entrepreneurial streak,
if you are looking to develop a for-purpose company,
You want to develop an app to advance the cause of freedom.
You want to get into cryptocurrency.
You want to use technology in some way to fight for social change.
You need to be at these events because the people around you, the people that will be
sitting beside you, they've done it.
And I learned a whole lot, probably more than I contributed while I was there.
But it led me to realize that I was violating my own rules, you know, my own advice when
people ask me, you know, I don't know where to get involved, I don't know where to start.
I always tell them, you know, look at your skill set.
What are you good at?
where you need to be. I'm not a big conference guy. I'm somebody that's good
at building networks, building community networks, small informal gatherings.
That's what I'm good at. Rule 303. If you have the means, you have the
responsibility. I have the means to travel to locations and help do this. So
we're looking at starting to do it. The first locations we're looking at are
to be in the deep south.
And the reason we want to do this is because when we talk about building these community
networks, you look in the comments, the most common response is, I don't know anybody else
that would want to be in that network.
If we show up to a city, everybody who shows up to an event about building a community
network, that's your network.
That's where you can start.
And if we do it in an informal manner and get everybody together, it can start right
there.
And rather than it being a lecture, it's a conversation.
And I think that that will help speed things along and then hopefully there won't be a
guy at Taco Bell that doesn't have somebody else in that same mindset.
So we're going to start looking at doing this, we're going to start in the deep south.
And as we get all the details and logistics hammered out, we're going to spread out.
That's the current plan.
And this all came from attending this large conference.
But again, if you are part of that community, and you want to widen your network, and you
want to get involved and try to influence society through the market you
need to be attending these things you need to be going because you can learn a
lot I sure did anyway we'll be back to our regularly scheduled programming
here probably tomorrow. I guess it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}