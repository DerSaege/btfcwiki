---
title: Let's talk about citizens and persons....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=e8w23X9AL74) |
| Published | 2019/07/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau engaged with internet individuals who believe undocumented workers have no constitutional rights.
- Beau found it enlightening to interact with these individuals who had misconceptions about constitutional rights.
- He explained that the Constitution guarantees rights to both citizens and persons.
- Beau's goal was to make these individuals realize that undocumented workers have rights protected by the Constitution.
- Beau pointed out that supporting the Constitution and supporting certain policies may be contradictory.
- He listed the rights guaranteed to persons under the Constitution, including the right to assemble peaceably and protection against unreasonable searches and seizures.
- Beau emphasized the importance of trials and equal protection under the law for all persons.
- He mentioned historical examples to illustrate how constitutional rights apply even in cases of terrorism.
- Beau raised the question of why some individuals seem to disregard the Constitution when discussing certain policies.
- He encouraged engaging with those who misinterpret or ignore constitutional rights, aiming to provoke critical thinking and questioning.

### Quotes

- "You can't do both. So what rights are guaranteed to persons? All the ones that matter."
- "So when you get to this point and you've explained all of this to them, you can then ask, you know, why do you hate the Constitution?"
- "Every constitutional protection that matters as far as getting locked up for no good reason, being detained without any kind of due process, being denied life, liberty, property, all of these things, they all apply to persons, not citizens."

### Oneliner

Beau engages internet individuals on constitutional rights, revealing misconceptions and contradictions, urging critical reflection on the Constitution's importance.

### Audience

Online Activists

### On-the-ground actions from transcript

- Challenge misconceptions about constitutional rights (suggested)
- Encourage critical engagement with individuals who misunderstand constitutional protections (suggested)
- Spark debates on the significance of constitutional rights in policy debates (suggested)

### Whats missing in summary

The full transcript provides a detailed breakdown of constitutional rights and the importance of engaging with those who misunderstand or misrepresent them.

### Tags

#ConstitutionalRights #CriticalThinking #Engagement #PolicyDebates #InternetActivism


## Transcript
Well, howdy there internet people, it's Bo again.
So tonight, for the first time, I
engaged with a certain kind of creature
that you find on the internet.
I never engaged with them before because if they
don't think they're people, why would I even
bother trying to explain anything else to them?
Um, and it was fantastic.
Now I'm really glad I did it and of course we're talking about the person on the internet
who says that undocumented workers don't have constitutional rights and the reason
I engaged with them tonight was because in his comment he said that the constitution
only applied to citizens and then it clicked these guys have never read the
Constitution they sit there and talk about it but they've never read it most
of them have we the people as their profile picture but they've never read
it because despite all of their failings our founders were really bright guys
they addressed this topic from the very beginning if you actually read the
Constitution, which you will find out, is that some rights are guaranteed to
citizens and some rights are guaranteed to persons. That's why I'm like, you know,
if he doesn't think they're people, because that's the only way they could
not have constitutional rights. Some are reserved to persons, some are
reserve to citizens, that's how it works, and it was fantastic, because as I
explained this, I got to watch his whole worldview just crumble in front of him.
It was great because once this is understood, they have a choice, they can
either support the constitution that they pretend they've read or support the
president. You can't do both. So what rights are guaranteed to persons? All the ones that matter
as far as what we're talking about dealing with immigration and dealing with the camps. They have
the right to peaceably assemble. The first amendment right to peaceably assemble. They have the fourth
Amendment right to be secure in their persons, houses, papers, and effects.
They have a Fifth Amendment right to not be denied life, liberty, or property without
due process.
They have a Sixth Amendment right to trials, which becomes important later to illustrate
some anecdotal stuff because the facts are great, but sometimes you need something they
can relate to.
And then the equal protection under the law by the 14th Amendment.
And that one's great too, because the 14th Amendment's really long.
And it uses citizens and persons in the same amendment, so it shows that it doesn't actually
mean the same thing.
There's tons of case law on this going back more than a hundred years, but it becomes
very very clear that you have to make that choice whether or not you're going
to support the Constitution or support the president's policies because the
president's policies are in direct defiance of the Constitution. Now on to
that anecdotal evidence. The war on terror the early years had all those
black sites, right? We're finding out about one all the time. You know, the ones
where they had the enhanced interrogation. Where were they at? What
countries? Literally all over the world. Except one country, the U.S. Why? Because
the second they set foot on U.S. soil, they had constitutional protections
because they're people, they are persons.
That's why the one US military base that is best known for this, well it's in Cuba, it's
not one in the United States.
And even that became an issue, you know, well we have this base, but it's leased.
Article 3 of the lease says that Cuba retains sovereignty over it.
That's why it can be done there.
I personally don't agree with that, but that's the legal ruling on it.
So when you get to this point and you've explained all of this to them, you can then ask, you
know, why do you hate the Constitution?
And watch them get mad.
You can ask them to pick one.
And I would suggest starting this conversation with that question, have you ever read the
Constitution?
Because they're going to lie, they're going to say, of course I have, I have a pocket
Constitution right here.
That's great, but you never read it.
Or you didn't read it in enough detail to understand what it says.
Because this is extremely clear.
There are persons and there are citizens.
Every constitutional protection that matters as far as getting locked up for no good reason,
being detained without any kind of due process, being denied life, liberty, property, all
of these things, they all apply to persons, not citizens.
So anyway, I would highly suggest engaging them.
I don't know that it will stick, but he certainly now has something he's questioning because
most people in the United States, they've been indoctrinated for so long, the Constitution
might as well be the Bible. And when you can prove without a doubt that something
is in direct defiance of it, it tends to resonate, especially among this crowd,
even though, as we have found out, most of them have never read it. Anyway, it's
Just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}