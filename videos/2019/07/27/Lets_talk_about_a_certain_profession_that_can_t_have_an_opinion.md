---
title: Let's talk about a certain profession that can't have an opinion....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=AGnbhHGfmkY) |
| Published | 2019/07/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Acknowledges viewers using videos for homeschooling.
- Reads an email criticizing his association with a woman in a particular industry.
- Shares the story of a woman active in street actions who he knew as an immigrant rights advocate.
- Emphasizes the importance of the woman's advocacy work over her past industry.
- Criticizes how people are treated differently once they voice opinions and show themselves as individuals.
- Notes the issue of objectification in different industries, like sports.
- Talks about being friends with military contractors and the lack of scrutiny compared to associations with other professionals.
- Raises societal concerns about valuing relationships with certain professions over others.

### Quotes

- "It's okay to be friends with a professional killer. It's not okay to be friends with a professional lover, I guess."
- "That's how violence happens."
- "Just shut up and play ball, right?"

### Oneliner

Beau tackles societal biases around professional relationships and advocates for valuing individuals beyond their professions.

### Audience

Activists, Advocates, Supporters

### On-the-ground actions from transcript

- Contact immigrant rights advocacy organizations to support their work (suggested)
- Join local community actions supporting marginalized groups (exemplified)
- Challenge objectification in industries by amplifying voices and stories of individuals (implied)

### Whats missing in summary

The full transcript provides a deeper exploration of societal biases and the impact of objectification in different professions, urging viewers to re-evaluate their perspectives on relationships with individuals based on their professions.

### Tags

#SocietalBiases #Objectification #Advocacy #CommunityPolicing #Activism


## Transcript
Well, howdy there, internet people, it's Bo again.
Some of you have mentioned that you use these videos homeschooling.
If you do, screen this one first.
Okay, so I've been trying to catch up on messages and emails.
Going through the emails tonight, and one came in while I was checking them.
We're gonna talk about it because it's something that has come up in the past, and it irritates
me. So here's the email in as much of it as I can read. The wholesome Bo of the
fifth column spends his nights tweeting a blanking lady of the evening, not his
term. She knows you so well she knows what kind of stories you'd like to read.
Bet you've a list of things he's imagined us doing. Why are you even
talking to this person. Not his term. Okay, so if you spend any amount of time
advocating for freedom you will run into people from this industry or related
fields. They're pretty politically active and something always seems to happen
happening, it's like clockwork.
There was a young woman who was in a similar industry.
She was at every street action that happened north of DC in
the northeastern United States.
She'd show up.
And she was the type that would mask up if necessary.
Did it for years.
Then one day, she picked up a megaphone and she spoke within 10 minutes.
Do you know what she used to do?
No and I don't care.
Just like the woman referenced in this email, I didn't know what she did.
I don't care.
That's not how I know her.
I know her as an immigrant rights advocate.
why she knows what kind of stories I would like and that's why she was
tweeting me about the Marines that got busted smuggling. That's probably more
important than her past, the fact that active duty members are violating the
commander-in-chief's prime policy en masse. They busted more than a dozen of
them, and those are just the ones they caught. That's probably more important
than what this woman does for a living. But that's the way it always works. As
soon as they find their voice, as soon as they start to speak out, that's when it
comes up. It's okay for them to be around, but when they start to talk and have an
opinion and show themselves as a person, that's when it becomes an issue. You see
this to a lesser degree in other industries. You see it in sports. Just shut
up and play ball, right? They've become an item of entertainment rather than a
person. That's pretty dangerous for people in their field when you become
objectified to the point that you're not a person. That's how violence happens to
I've seen it over and over again.
I know maybe a dozen people in this or similar industries have been asked about my relationships
with them more than a hundred times, easy.
It is no secret that I am friends with hundreds of current or former military contractors.
No matter how you church that job up, you're ready to kill for money.
That's the job.
I have never been asked in an accusatory tone or a hushed whisper about my relationships
with them. That's the state of the United States. It's okay to be friends with a professional
killer. It's not okay to be friends with a professional lover, I guess. Something we
might want to think about as a society anyway it's just a thought y'all have a good night

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}