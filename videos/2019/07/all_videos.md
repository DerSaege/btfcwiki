# All videos from July, 2019
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2019-07-31: Let's talk about some moms in Chicago.... (<a href="https://youtube.com/watch?v=UQMzDd9z-pI">watch</a> || <a href="/videos/2019/07/31/Lets_talk_about_some_moms_in_Chicago">transcript &amp; editable summary</a>)

Beau talks about the tragic shooting of two moms in Chicago, urging community action and support for M.A.S.K.'s GoFundMe to gather information and combat violence.

</summary>

"Community building from the ground up. Don't wait for somebody else to make it better, you do it."
"They were moms. They lived in that community. They were just trying to make it better."
"Trying to end violence through love and more opportunities."
"Every dollar is going to help, because it loosens lips."
"Now that organization has put together GoFundMe to get a reward together for information."

### AI summary (High error rate! Edit errors on video page)

Emphasizes community networking and building as a common, everyday practice for most people.
Shares the story of two moms tragically gunned down at a bus stop in Chicago.
Criticizes the media for misrepresenting the victims as anti-gun activists when they were actually community-building moms.
Recognizes the organization the moms belonged to, M.A.S.K. (Moms/Men Against Senseless Killing), for their efforts in ending violence through love and increased opportunities.
Encourages support for the GoFundMe set up by M.A.S.K. to gather information about the shooting.
Expresses a sense of personal loss for the victims despite not knowing them personally.
Calls for community action and not relying on law enforcement to address such issues effectively.

Actions:

for community members,
Support the GoFundMe set up by M.A.S.K. to gather information about the shooting (suggested).
</details>
<details>
<summary>
2019-07-30: Let's talk about the goals of safety nets like SNAP.... (<a href="https://youtube.com/watch?v=owjQzkB9RCk">watch</a> || <a href="/videos/2019/07/30/Lets_talk_about_the_goals_of_safety_nets_like_SNAP">transcript &amp; editable summary</a>)

Beau challenges the purpose of SNAP, suggesting eliminating means tests to truly help people escape poverty and build a stronger country.

</summary>

"If the goal is to build a stronger country, expand SNAP, don't close it."
"SNAP is not just a safety net program. It's also an economic stimulus program."
"It's about controlling you."
"Expand the program. Don't shut it down."
"Make it more accessible."

### AI summary (High error rate! Edit errors on video page)

Most states ignore the asset requirement for SNAP, leading millionaires to receive food stamps.
The federal government sets the asset requirement for SNAP at $2,250, including assets like cars.
Americans are advised to save three to six months of income, but 40% can't handle a $400 emergency.
A person on SNAP who saves up to $2,250 loses assistance, making it challenging to escape poverty.
It takes 20 years under the current system to lift oneself out of poverty without setbacks.
The goal of assistance programs like SNAP should be questioned: Is it to keep people above poverty or help them escape it?
Beau suggests eliminating all means tests for SNAP to truly assist those in need.
SNAP serves not only as a safety net but also as an economic stimulus during weak economic periods.
Removing means tests and allowing easier access to SNAP could help people move above the poverty line.
The issue with SNAP isn't about budget but about control and perpetuating income inequality.

Actions:

for advocates, policymakers,
Expand SNAP to make it more accessible (suggested)
Challenge the purpose of assistance programs and advocate for change (implied)
</details>
<details>
<summary>
2019-07-29: Let's talk about snapping up cost of living differences..... (<a href="https://youtube.com/watch?v=iYpwnRdsdn8">watch</a> || <a href="/videos/2019/07/29/Lets_talk_about_snapping_up_cost_of_living_differences">transcript &amp; editable summary</a>)

Beau explains loopholes in SNAP income requirements, criticizes Trump's manipulation, and stresses the real issue of Americans living in poverty.

</summary>

"This isn't a loophole. This is Donald Trump doing what Donald Trump does."
"The issue is we have 40 million Americans living at the poverty line."
"It's $102 million for the president of the United States to go golfing before we start cutting into programs that might affect children being fed."
"He wants to be able to say, look at the number of people I got off food stamps. Look how great the economy is."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Explains the SNAP program, which provides food assistance to 40 million Americans, with about half being children.
Details the income requirements to qualify for the program, including gross and net pay thresholds.
Points out the discrepancy between the national poverty line and the cost of living across different states.
Mentions the existence of loopholes in the SNAP program due to these discrepancies.
Contrasts the basic allowance for housing in the military with the income limits for SNAP recipients.
Addresses concerns about fraud within the SNAP program, providing statistics on the actual amount of fraud.
Criticizes the focus on cutting programs like SNAP while ignoring extravagant spending in other areas.
Accuses Donald Trump of manipulating SNAP numbers for political gain.
Emphasizes that the real issue is the high number of Americans living at the poverty line.

Actions:

for advocates, policy influencers,
Advocate for accurate representation of poverty levels and support programs assisting those in need (implied).
Stay informed about political actions affecting social welfare programs (implied).
</details>
<details>
<summary>
2019-07-29: Let's talk about Earth Overshoot Day and what comes next.... (<a href="https://youtube.com/watch?v=5y1BWaZdf1U">watch</a> || <a href="/videos/2019/07/29/Lets_talk_about_Earth_Overshoot_Day_and_what_comes_next">transcript &amp; editable summary</a>)

Today is Earth Overshoot Day, consumption choices today determine whether we embrace sustainability or face authoritarian socialism tomorrow.

</summary>

"You can become green, or you can become red."
"If we don't do something about consumption, we're going to get socialism."
"It's your choice."
"We're going to get the one with guns and boots."
"Means of production, distribution, and exchange. That's critical to socialism."

### AI summary (High error rate! Edit errors on video page)

Today is Earth Overshoot Day, meaning all resources that will be regenerated by the Earth by the end of the year are already used up.
Overconsumption and mitigating climate change often lead to accusations of socialism.
Socialism is not just when the government does stuff; it's about community control of production, distribution, and exchange.
Mitigating climate change is not socialism; in fact, not addressing climate change is a more likely path to socialism in the United States.
Doomsday scenarios of flooding and disasters will lead people from affected areas to move to other states with better infrastructure, mainly red states.
The influx of people fleeing climate disasters will require significant central planning and government involvement to allocate resources.
History shows that in emergencies like nuclear threats, government control of production and distribution has been necessary.
To avoid a future with authoritarian socialism due to climate crises, becoming conservationists and changing consumption habits is imperative.
Failure to address consumption and unsustainable living will lead to an emergency situation where authoritarian socialism may become a reality.
The choice is between embracing sustainable practices or facing a future of authoritarian socialism in emergency conditions.

Actions:

for environmental advocates, policymakers,
Advocate for sustainable consumption practices within your community (implied)
Support policies that mitigate climate change and prioritize environmental conservation (implied)
</details>
<details>
<summary>
2019-07-27: Let's talk about a certain profession that can't have an opinion.... (<a href="https://youtube.com/watch?v=AGnbhHGfmkY">watch</a> || <a href="/videos/2019/07/27/Lets_talk_about_a_certain_profession_that_can_t_have_an_opinion">transcript &amp; editable summary</a>)

Beau tackles societal biases around professional relationships and advocates for valuing individuals beyond their professions.

</summary>

"It's okay to be friends with a professional killer. It's not okay to be friends with a professional lover, I guess."
"That's how violence happens."
"Just shut up and play ball, right?"

### AI summary (High error rate! Edit errors on video page)

Acknowledges viewers using videos for homeschooling.
Reads an email criticizing his association with a woman in a particular industry.
Shares the story of a woman active in street actions who he knew as an immigrant rights advocate.
Emphasizes the importance of the woman's advocacy work over her past industry.
Criticizes how people are treated differently once they voice opinions and show themselves as individuals.
Notes the issue of objectification in different industries, like sports.
Talks about being friends with military contractors and the lack of scrutiny compared to associations with other professionals.
Raises societal concerns about valuing relationships with certain professions over others.

Actions:

for activists, advocates, supporters,
Contact immigrant rights advocacy organizations to support their work (suggested)
Join local community actions supporting marginalized groups (exemplified)
Challenge objectification in industries by amplifying voices and stories of individuals (implied)
</details>
<details>
<summary>
2019-07-26: Let's talk about what Curious George can teach the US.... (<a href="https://youtube.com/watch?v=GNlai8E2vE4">watch</a> || <a href="/videos/2019/07/26/Lets_talk_about_what_Curious_George_can_teach_the_US">transcript &amp; editable summary</a>)

Beau explains the symbolism of Curious George, uncovers the true refugee story behind it, and advocates for welcoming refugees to better the country.

</summary>

"They're going to better the country if you let them."
"They're going to re-instill that spirit of being able to accomplish the impossible."
"All of those things that we pretend are still American traits."
"It's a great story."
"That's my thought."

### AI summary (High error rate! Edit errors on video page)

Explains the symbolism of Curious George on his hat and how it relates to his videos.
Recounts his history with Curious George as a mascot for a group he was part of.
Talks about his middle son's love for monkeys and how it reignited his interest in Curious George.
Acknowledges problematic themes in Curious George stories, particularly regarding a white man taking a monkey from Africa.
Discovers the true story behind Curious George, where George was actually Fifi, a monkey from South America, and his human parents were Jewish refugees escaping the Nazis in France.
Expresses admiration for how the story of Curious George/Fifi mirrors the resilience and resourcefulness of refugees.
Draws parallels between the refugee experience and the potential positive contributions refugees can make to society.
Expresses gratitude for learning the true story of Curious George/Fifi and its impact on his perspective.

Actions:

for viewers, refugee advocates,
Welcome and support refugees in your community by offering assistance and resources (implied).
Advocate for policies that facilitate the integration of refugees into society (implied).
Educate others about the valuable contributions refugees can make to society (implied).
</details>
<details>
<summary>
2019-07-26: Let's talk about how 5 minutes of your time can save 500,000 lives.... (<a href="https://youtube.com/watch?v=IpOSJR-_ElM">watch</a> || <a href="/videos/2019/07/26/Lets_talk_about_how_5_minutes_of_your_time_can_save_500_000_lives">transcript &amp; editable summary</a>)

Beau urges action to support the Connegates Amendment in the NDAA, preventing unauthorized attacks on Iran and potentially saving half a million lives.

</summary>

"A few minutes of your time can potentially save half a million lives or more."
"If Trump has to go back to Congress before launching a strike, that's going to force him to come up with a good reason."
"This is your time."
"We need to keep this amendment in there."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Urges viewers for a call to action regarding the Connegates Amendment in the NDAA, aiming to prevent unauthorized attacks on Iran.
Two versions of the Defense Department budget exist, with the House version containing the amendment but not the Senate version.
The amendment requires President Trump to seek authorization from Congress before attacking Iran, limiting executive power.
Viewers are encouraged to contact members of the Armed Services Committees, especially in the Senate, to support the amendment.
Emphasizes the importance of maintaining the amendment to prevent potential devastating consequences akin to the Iraq war.
Appeals to resistors, Republicans, anti-war advocates, and veterans to advocate for the amendment's retention.
Stresses the potential loss of lives, referencing the toll of half a million in the Iraq conflict.
Acknowledges Trump's restraint in military force but expresses concerns about his changing positions and escalating tensions.
Calls upon viewers to take action to ensure Trump is held accountable before launching any strikes on Iran.
Urges everyone to dedicate a few minutes to potentially save hundreds of thousands of lives.

Actions:

for advocates, activists, citizens,
Contact members of the Armed Services Committees, especially in the Senate, to support the Connegates Amendment (suggested).
Email, write, tweet to push for the retention of the amendment (suggested).
</details>
<details>
<summary>
2019-07-25: Let's talk about getting too woke.... (<a href="https://youtube.com/watch?v=infL_y3DxhU">watch</a> || <a href="/videos/2019/07/25/Lets_talk_about_getting_too_woke">transcript &amp; editable summary</a>)

Beau explains "getting woke," warning against despair when perceiving the establishment as unbeatable, urging continuous fight for a better world.

</summary>

"There's nothing wrong with getting woke."
"It's okay to get woke, but never get so woke that you can't dream anyway."
"We don't have to beat them today or tomorrow. We just have to keep fighting."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of "getting woke" when people see the establishment for what it truly is and start understanding the finer details of control systems, money, and politics.
Mentions how veterans often experience disillusionment after multiple tours, realizing they are guarding poppy fields for the pharmaceutical industry's profit.
Notes that the issue arises when this awareness leads to despair, especially when individuals perceive the opposition as overwhelmingly powerful.
Describes a friend who feels exhausted and stagnant in their efforts, unable to see any progress or victory.
Observes that the black community frequently faces this phenomenon, understanding the corrupt system but then feeling so woke that they lose hope and fall into depression.
Emphasizes that while recognizing the system's corruption and power is not problematic, feeling like victory is unattainable is where the real issue lies.
Encourages the idea of continuous fight and not succumbing to despair, reminding that change may take a long time but it's vital to hold onto the hope for a better world.

Actions:

for activists, advocates, community members,
Keep fighting for a better world (implied)
Maintain hope and vision for change (implied)
</details>
<details>
<summary>
2019-07-24: Let's talk about Penthouses and Taco Bells.... (<a href="https://youtube.com/watch?v=R4z9FPD8ZiM">watch</a> || <a href="/videos/2019/07/24/Lets_talk_about_Penthouses_and_Taco_Bells">transcript &amp; editable summary</a>)

Beau shares insights from attending AnarchoVegas, stressing the importance of networking and community-building based on skill sets and responsibilities.

</summary>

"I learned a whole lot, probably more than I contributed while I was there."
"If you have the means, you have the responsibility."
"Everybody who shows up to an event about building a community network, that's your network."
"Rather than it being a lecture, it's a conversation."
"You need to be attending these things, you need to be going because you can learn a lot."

### AI summary (High error rate! Edit errors on video page)

Beau missed a few days but assures everyone he is fine after attending a conference in Vegas.
He attended AnarchoVegas, a big event for him, and had a unique experience.
Beau interacted with a Taco Bell employee who recognized him from YouTube, sparking a meaningful connection.
The conference included a fundraiser for the Free Ross campaign, aiming to raise funds for Ross Ulbricht's legal defense.
The event mainly consisted of entrepreneurs striving to use their businesses for social change, alongside street activists.
Beau reflected on feeling out of place but acknowledged the value of attending such conferences to learn and network.
He realized the importance of following his own advice on getting involved based on skill set, leading him to plan community-building events in the deep south.
Beau emphasized the significance of informal gatherings for networking and community-building, aiming to start the initiative in the deep south before expanding elsewhere.

Actions:

for activists, entrepreneurs, community builders,
Start organizing informal gatherings in your community to build networks and initiate social change (suggested).
Attend conferences or events related to your skills and interests to network and learn from others (suggested).
Participate in community-building initiatives to widen your network and influence society positively (suggested).
</details>
<details>
<summary>
2019-07-24: Let's talk about Hermitage and Andrew Jackson.... (<a href="https://youtube.com/watch?v=LXC6Fbdy4DM">watch</a> || <a href="/videos/2019/07/24/Lets_talk_about_Hermitage_and_Andrew_Jackson">transcript &amp; editable summary</a>)

Beau analyzes historical echoes, warns against echo chambers in ICE and Border Patrol, and hails community action in Hermitage against ICE, foreseeing repercussions for those enforcing questionable policies.

</summary>

"The view inside an echo chamber, the view inside of an agency like that, it's not the view of the public and the talking points on TV."
"Hermitage, an impossibly diverse group of people, ages, ethnicities, languages, everything, banded together and broke the law publicly, no masks, broke the law to get that father and son away from ICE."
"That's the level of opposition that exists."

### AI summary (High error rate! Edit errors on video page)

Beau provides historical context by talking about Andrew Jackson's victory at the Battle of New Orleans, which propelled him to the national scene.
Jackson's victory happened after the War of 1812 had technically ended with the Treaty of Ghent signed, but news hadn't reached him yet.
Jackson's success at the Battle of New Orleans, though attributed to a British battle plan failure, made him a hero and later helped him become President.
Beau recalls visiting Jackson's plantation, Hermitage, as a child, where the sanitized version of plantation life was presented without addressing slavery or cruelty.
He mentions growing up in an echo chamber in Tennessee with Confederate flags prevalent, hindering meaningful discourse.
Beau draws parallels between the echo chamber of his upbringing and the potential echo chamber within ICE and Border Patrol regarding immigration issues.
Public opinion has shifted on immigration, but the battles may continue despite the war being deemed over.
Beau warns those working for ICE and Border Patrol about potential future repercussions and job prospects, contrasting their situation with that of political figures.
He mentions a diverse group in Hermitage who publicly broke the law to rescue a father and son from ICE, showcasing opposition to current policies.
Beau sees this incident as a turning point, indicating the strong level of opposition that could impact the future employment prospects of those in Border Patrol and ICE.

Actions:

for community members, activists,
Band together with neighbors to oppose unjust policies and take direct action to protect vulnerable community members (exemplified)
</details>
<details>
<summary>
2019-07-17: Let's talk about the America I remember.... (<a href="https://youtube.com/watch?v=GZm505MCvyk">watch</a> || <a href="/videos/2019/07/17/Lets_talk_about_the_America_I_remember">transcript &amp; editable summary</a>)

Exploring generational perceptions of America through history education: sanitized textbooks versus raw, unfiltered truths, questioning the future based on embracing true history or idealized versions.

</summary>

"They didn't learn history, not the way we did. They didn't get it from a thick government-approved textbook, a sanitized version."
"We can actually have the American dream because they got the truth. We know where we're going wrong."
"The future of this country rests in those kids and what they decide to do, those kids, they're not all kids, be younger generations."
"America will stand or fall based on the actions of these generations that a lot of us are making fun of."
"They're not going to sell out their country for a red hat and a stupid slogan."

### AI summary (High error rate! Edit errors on video page)

Exploring the generational gap in perceptions of America, with older generations remembering a sanitized version learned from textbooks and younger generations exposed to raw, unfiltered truths from the internet.
Older generations learned history through government-approved textbooks, while younger generations have freer access to information and primary sources.
The shock of discovering America's darker history, like mistreatment of natives and slaves, challenges the traditional narrative of America as the "good guy."
The younger generation still values the ideals of America, such as equality and freedom, but questions whether the future will embrace the raw, unfiltered truth or a sanitized version of history.
Beau contrasts the desire for America to be an EMT (Emergency Medical Technician) globally instead of a police force, helping people achieve freedom genuinely.
Acknowledging the transformative impact of the internet on how history is learned and understood, Beau recognizes the power held by younger generations in shaping America's future.

Actions:

for educators, activists, students,
Advocate for inclusive and accurate history education in schools (implied)
Support initiatives that provide access to primary sources and unfiltered information about historical events (implied)
Encourage critical thinking and questioning of traditional narratives about America's history (implied)
</details>
<details>
<summary>
2019-07-17: Let's talk about Asuistan again.... (<a href="https://youtube.com/watch?v=DX9hYvpMBR8">watch</a> || <a href="/videos/2019/07/17/Lets_talk_about_Asuistan_again">transcript &amp; editable summary</a>)

Beau addresses overlooked issues within the U.S., revealing alarming parallels with dictatorships and urging a critical reevaluation of media coverage.

</summary>

"Assuistan is not a real country."
"We need to look at it again."
"In large part because of mass media."
"It's just a thought, y'all."
"The targeting of child soldiers, refusing international inspectors, trying to disrupt investigations into our own war crimes..."

### AI summary (High error rate! Edit errors on video page)

Addressing a suicide and discussing overlooked issues within the country.
The regime in the country is denying international inspectors entrance and visas to ICC staff.
Threatening judges to dissuade investigations into war crimes in a recent conflict.
Military recruitment numbers have dropped due to lack of faith in the regime, leading to targeting children as young as 16 for recruitment.
Children involved in hostilities must be 18 or older according to international law.
Legislation in some provinces prevents military recruiters from entering campuses, prompting a digital campaign to target children.
Clashes between protesters, rebels, and security services across the country due to security sweeps targeting a social minority group.
Horrendous conditions and widespread sexual abuse of the targeted group leading religious institutions to shelter them.
Centrist party within the government asking the regime's leader, Don Joe Trump, to step down, which is unlikely.
Beau uses "Assuistan" to demonstrate how media coverage changes when events in the U.S. are portrayed as happening in another nation.
The U.S. faces issues like targeting child soldiers, refusing international inspectors, disrupting war crimes investigations, and running concentration camps.

Actions:

for policy makers, activists, citizens,
Contact religious institutions for information on how to support and help shelter the targeted group (implied).
</details>
<details>
<summary>
2019-07-16: Let's talk about citizens and persons.... (<a href="https://youtube.com/watch?v=e8w23X9AL74">watch</a> || <a href="/videos/2019/07/16/Lets_talk_about_citizens_and_persons">transcript &amp; editable summary</a>)

Beau engages internet individuals on constitutional rights, revealing misconceptions and contradictions, urging critical reflection on the Constitution's importance.

</summary>

"You can't do both. So what rights are guaranteed to persons? All the ones that matter."
"So when you get to this point and you've explained all of this to them, you can then ask, you know, why do you hate the Constitution?"
"Every constitutional protection that matters as far as getting locked up for no good reason, being detained without any kind of due process, being denied life, liberty, property, all of these things, they all apply to persons, not citizens."

### AI summary (High error rate! Edit errors on video page)

Beau engaged with internet individuals who believe undocumented workers have no constitutional rights.
Beau found it enlightening to interact with these individuals who had misconceptions about constitutional rights.
He explained that the Constitution guarantees rights to both citizens and persons.
Beau's goal was to make these individuals realize that undocumented workers have rights protected by the Constitution.
Beau pointed out that supporting the Constitution and supporting certain policies may be contradictory.
He listed the rights guaranteed to persons under the Constitution, including the right to assemble peaceably and protection against unreasonable searches and seizures.
Beau emphasized the importance of trials and equal protection under the law for all persons.
He mentioned historical examples to illustrate how constitutional rights apply even in cases of terrorism.
Beau raised the question of why some individuals seem to disregard the Constitution when discussing certain policies.
He encouraged engaging with those who misinterpret or ignore constitutional rights, aiming to provoke critical thinking and questioning.

Actions:

for online activists,
Challenge misconceptions about constitutional rights (suggested)
Encourage critical engagement with individuals who misunderstand constitutional protections (suggested)
Spark debates on the significance of constitutional rights in policy debates (suggested)
</details>
<details>
<summary>
2019-07-15: Let's talk about what went wrong in the Fullerton incident.... (<a href="https://youtube.com/watch?v=U3lCH_vLgos">watch</a> || <a href="/videos/2019/07/15/Lets_talk_about_what_went_wrong_in_the_Fullerton_incident">transcript &amp; editable summary</a>)

Beau explains the Fullerton incident, defending the cop's actions within normal standards, and suggests potential policy debates.

</summary>

"There's not. nothing that this cop did that any reasonable person, even extremely experienced, would do differently."
"This wasn't a kid at a playground where one could reasonably expect it to be a toy."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Beau is asked to dissect the Fullerton incident and explain what the cop did wrong.
He expresses strong criticism towards police departments and their actions.
Beau mentions his history of criticism towards the Fullerton PD.
The cop in the Fullerton incident observed a woman with a firearm.
Despite the firearm turning out to be fake, the officer had to make a split-second decision.
The officer engaged with the woman without using excessive force.
He called for medics before securing the scene or clearing the car, which is outside of standards.
The officer then proceeded to render aid to the woman.
Beau points out that the officer's actions were all within normal standards and best practices.
He suggests that there could be a debate around implementing a "don't fire until fired upon" policy.

Actions:

for policing critics,
Debate and advocate for policies like "don't fire until fired upon" (implied)
</details>
<details>
<summary>
2019-07-15: Let's talk about solutions to the conditions.... (<a href="https://youtube.com/watch?v=1Q0IJoOzm_4">watch</a> || <a href="/videos/2019/07/15/Lets_talk_about_solutions_to_the_conditions">transcript &amp; editable summary</a>)

Beau suggests housing asylum seekers in a plantation house, offering education and opportunities, criticizing government treatment, and advocating for decentralized solutions, believing individuals can do better than ICE.

</summary>

"Modern problems require modern solutions."
"Turning a problem into an asset, fantastic."
"I think we can do it better than ICE if we let the average individual do it."

### AI summary (High error rate! Edit errors on video page)

Proposes a solution to current issues, including hygiene, overcrowding, and food problems, and the situation at concentration camps.
Shares a dream of buying an old plantation house and his plan to house asylum-seeking mothers and their infants.
Calculates the costs and potential profit by housing these individuals compared to what camps are paid.
Plans to convert the unoccupied sixth bedroom into a school to teach English.
Mentions a friend who wants to take at-risk teens to his farm to prepare them for the Marines.
Suggests that farmers could provide housing for these teens and potentially offer them jobs.
Advocates for decentralization as a modern solution and criticizes the government for treating people inhumanely.
Finds poetic justice in using a home built on slavery to help others achieve freedom.
Believes that individuals can handle the situation better than government agencies like ICE.

Actions:

for advocates and activists,
Provide housing, education, and opportunities for asylum seekers and at-risk teens (implied)
Advocate for decentralized solutions to address humanitarian issues (implied)
</details>
<details>
<summary>
2019-07-14: Let's talk about the most important Presidential campaign promise.... (<a href="https://youtube.com/watch?v=Y7yv5_xSmHU">watch</a> || <a href="/videos/2019/07/14/Lets_talk_about_the_most_important_Presidential_campaign_promise">transcript &amp; editable summary</a>)

Beau insists on the importance of upholding the rule of law, particularly in the treatment of immigrants, urging all presidential candidates to endorse this fundamental principle.

</summary>

"If it's a good idea, it's a good idea."
"We cannot allow a tradition of just following orders to take root in this country."
"That's law. That's the rule of law."
"If we're going to have a nation, it's got to have the rule of law, right?"
"This is an important promise and it needs to be kept by whoever occupies the Oval Office next."

### AI summary (High error rate! Edit errors on video page)

Beau supports ideas over politicians, believing good ideas stand on their own merits regardless of who presents them.
He heard a powerful idea recently that he believes every presidential candidate should endorse, regarding the restoration of the rule of law in the treatment of immigrants.
Beau criticizes the abuse, physical and sexual, of immigrants and the lack of medical care provided to them, calling for accountability and justice.
He specifically mentions President Elizabeth Warren and her commitment to addressing crimes against immigrants on her first day in office.
Beau expresses disappointment that such a promise is even necessary from a presidential candidate, indicating a concerning lack of commitment to upholding the rule of law.
He references a clip where a former ICE director argues with AOC about asylum seekers, showcasing differing interpretations of the law.
Beau advocates for upholding the rule of law, citing specific legal provisions that protect asylum seekers and criticizing any attempts to undermine these laws.
He stresses the importance of maintaining the rule of law for the functioning of a nation and calls for accountability from those in power.

Actions:

for voters, immigration advocates,
Support organizations advocating for immigrant rights (implied)
Stay informed about immigration laws and rights (implied)
Advocate for accountability in the treatment of immigrants (implied)
</details>
<details>
<summary>
2019-07-13: Let's talk about the stories we're writing.... (<a href="https://youtube.com/watch?v=rtkwPBx-qcE">watch</a> || <a href="/videos/2019/07/13/Lets_talk_about_the_stories_we_re_writing">transcript &amp; editable summary</a>)

A country is shaped by its stories, with current narratives reflecting a potential dark path towards acceptance of oppressive actions.

</summary>

"A country, a lot like a person, is just stories."
"That's the character of the nation."
"The stories that make up the United States' character are being written right now."
"We're headed down a pretty dark road."
"What's happening there is part of America's history now."

### AI summary (High error rate! Edit errors on video page)

A country is a collection of stories, shaping its character and reputation.
The stories written today define the nation's character.
Recent internet debates suggest a fear of speaking out against a fascist government.
People are starting to accept oppressive actions instead of questioning them.
Internet exchanges are becoming part of America's story, potentially showcased in future museums.
The focus on political pundits and debates overshadows experts' warnings about the dark path ahead.
The Smithsonian aims to preserve drawings by children from border camps as part of America's history.
The framing of these events in history will shape future perspectives on the country's actions.

Actions:

for americans,
Preserve and share stories and narratives that challenge oppressive actions in society (implied).
Support initiatives like the Smithsonian's efforts to document and frame critical events in history (exemplified).
</details>
<details>
<summary>
2019-07-12: Let's talk about money and expansion.... (<a href="https://youtube.com/watch?v=O_igsJyR1D4">watch</a> || <a href="/videos/2019/07/12/Lets_talk_about_money_and_expansion">transcript &amp; editable summary</a>)

Beau introduces a new video format, explains Patreon launch, and shares plans to expand into other media forms, seeking support for future projects and community initiatives.

</summary>

"Beyond that, I'd like to branch out into other types of media, the podcast everybody's asking for, I'd like to write a book too."
"It's tentative because we have no idea how this is going to play out."
"The rest is up to you guys."

### AI summary (High error rate! Edit errors on video page)

Introducing a unique video format unlike any done before.
Explaining the reasons for not asking viewers to like, subscribe, or support Patreon in every video.
Announcing the launch of a Patreon account due to viewer requests.
Describing Patreon as a platform for sponsorships starting at a dollar per month.
Detailing the perks of sponsoring, including access to a Discord server, voting on future projects, and early video releases.
Ensuring that no content will be kept behind a paywall.
Mentioning plans to use the Patreon funds to free up time from battling algorithms on YouTube and Facebook.
Expressing a desire to expand into other media forms like podcasts and writing a book.
Stating the intention to use the funds to pay people who might assist in these new ventures.
Sharing a vision of potentially taking the show on the road to actively help build community networks and address social issues.

Actions:

for viewers,
Support Beau's Patreon account to help him free up time from battling algorithms on social media and to assist in expanding into other media forms (suggested).
Engage with the community by participating in live streams and supporting efforts to address social issues (implied).
</details>
<details>
<summary>
2019-07-11: Let's talk about the government's relationship with the environment.... (<a href="https://youtube.com/watch?v=wMpHPykNENI">watch</a> || <a href="/videos/2019/07/11/Lets_talk_about_the_government_s_relationship_with_the_environment">transcript &amp; editable summary</a>)

French intelligence bombed the Rainbow Warrior to silence environmental advocacy; Today, U.S. environmental regulations are rolled back while climate change information is suppressed for corporate interests.

</summary>

"Government's not here to protect you."
"We don't colonize anymore with flags and guns, we do it with corporate logos."
"But at the end of the day, it's happening."
"The environment as a whole doesn't have any money but the oil and coal industry do."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Recalls the bombing of the Rainbow Warrior in 1985 by French intelligence due to its mission to draw attention to a French nuclear test, resulting in a death and French government conviction.
Compares the French government's suppression of environmental advocacy to current actions by the U.S. government.
Notes the U.S. President's contradictory claim of being a land protector while overseeing significant rollbacks of environmental regulations.
Points out the administration's extensive list of environmental regulation rollbacks, including opening the U.S. coastline to offshore drilling and scrapping safety regulations.
Mentions the removal of references to climate change by federal agencies, drawing parallels to the French bombing incident to prevent public discourse on climate change.
Emphasizes that while debates exist on the origins of climate change, its occurrence is undeniable and poses a national security threat.
Argues that governments prioritize control and power over protecting citizens, using malice to suppress climate change information.
Compares historical military power motives for actions like the boat bombing to present-day colonization through corporate influence in environmental matters.
Points out the presence of former industry lobbyists during the President's press conference, indicating a politicization of environmental issues driven by industry interests.
Concludes with a reflection on the financial influence of industries like oil and coal in environmental policymaking.

Actions:

for environmental activists,
Organize local environmental protection events (implied)
Support organizations advocating for environmental conservation (implied)
</details>
<details>
<summary>
2019-07-11: Let's talk about social media's impacts on political campaigns.... (<a href="https://youtube.com/watch?v=1zZ5ETq2Vj4">watch</a> || <a href="/videos/2019/07/11/Lets_talk_about_social_media_s_impacts_on_political_campaigns">transcript &amp; editable summary</a>)

Social media is dismantling political elitism by exposing inauthenticity and enabling non-establishment candidates like AOC, paving the way for a new era in politics.

</summary>

"We're going to see more Justin Armash, AOC, candidates like this that are on the edges of the establishment."
"She appears to be gearing up to run the exact opposite style of campaign, where she's hoping to appeal to the better instincts and the better nature of humanity."
"It's a bold strategy. She seems, by tweeting that without any kind of explanation, she seems to be hoping and targeting the more educated Americans."
"It is authentic. That is who she appears to be."
"We're going to see them for the establishment. We're going to see them for those who are really there just to enrich themselves with the money they take from us."

### AI summary (High error rate! Edit errors on video page)

Social media may end up dismantling the political elite in the country by exposing inauthenticity and enabling non-establishment candidates to succeed.
In the past, candidates could easily stage authenticity for the public, but social media now reveals the staged nature of these events.
The rise of social media is already impacting politics, with the emergence of non-establishment candidates who win elections.
People like AOC are examples of candidates on the fringes of the establishment gaining traction through social media.
Social media allows for greater scrutiny of politicians' actions, making it harder for them to spin narratives like before.
Marianne Williamson's campaign is described as anti-Trump, appealing to compassion and love rather than fear and bigotry.
Williamson's strategy contrasts sharply with Trump's, focusing on appealing to the better instincts of humanity.
Social media enables candidates like Williamson to target more educated Americans and run authentic campaigns.
Candidates who are not part of the traditional power structures are finding a voice through social media platforms.
The future of politics might involve more non-establishment figures being able to challenge the professional political class.

Actions:

for political activists, social media users,
Support non-establishment candidates in elections (implied)
Engage in social media platforms to amplify authentic voices and challenge political elitism (implied)
</details>
<details>
<summary>
2019-07-10: Let's talk about helping homeless vets.... (<a href="https://youtube.com/watch?v=K7gFW0nfF_I">watch</a> || <a href="/videos/2019/07/10/Lets_talk_about_helping_homeless_vets">transcript &amp; editable summary</a>)

Beau challenges the false dilemma of helping migrants or homeless vets, proposing existing solutions for veteran homelessness while critiquing misplaced priorities.

</summary>

"We can do more than one thing at a time."
"The money's always been there for this to be done."
"You want an excuse to not help other people, and they're using homeless vets to do that."
"If you want a solution, there it is. Turn these camps into facilities to house homeless vets. Done."
"The question is why do you want to lock up toddlers instead of helping homeless vets?"

### AI summary (High error rate! Edit errors on video page)

United States is resilient and populated by resourceful people.
Americans can multitask and handle multiple challenges simultaneously.
Challenges like fighting wars and sending a man to the moon were tackled simultaneously in the past.
The issue at hand is homelessness among veterans.
On average, 40,056 homeless veterans spend their nights on the streets.
To address this problem, facilities offering food, beds, showers, hygiene items, psychological support, education, and recreation are needed.
In 2018, there were over 180 facilities with more than enough beds to house 40,520 people.
Beau proposes moving criminal aliens to county jails and releasing non-criminal migrants for court dates, in line with laws and international norms.
Retraining guards in existing facilities for job reentry programs could solve the problem.
Beau believes the money needed for this solution has always been available.
Many people use homeless vets as an excuse to avoid helping others.
Combat veterans, according to Beau, are compassionate and willing to help others in need.
Beau questions the deportation of veterans and the lack of care for homeless vets.
He suggests repurposing existing camps into facilities for homeless vets as a viable solution.
Beau challenges the idea that helping migrants is prioritized over homeless vets, pointing out the available resources for veterans.

Actions:

for advocates for homeless veterans,
Repurpose existing camps into facilities for homeless veterans (suggested)
</details>
<details>
<summary>
2019-07-09: Let's talk about being able to leave anytime you want.... (<a href="https://youtube.com/watch?v=uHfFBmM7MwY">watch</a> || <a href="/videos/2019/07/09/Lets_talk_about_being_able_to_leave_anytime_you_want">transcript &amp; editable summary</a>)

Sharing stories to put views in perspective, asylum seekers facing dehumanization, a call to learn from mistakes.

</summary>

"We like to remain blissfully ignorant of the situation of the world, and we do this as a defense mechanism."
"They're people. And because we do this, we can say cute little things like, oh well, they just should have followed the law."
"If we learn from our mistakes, the next time you hear a story about somebody collaborating with one of the worst regimes of the 20th century, you won't think it's about you."

### AI summary (High error rate! Edit errors on video page)

Sharing stories from others' lives can help put your views and life into perspective.
A family goes into hiding on July 9th out of fear of being rounded up after their asylum request is denied in the United States.
The inevitable end of the family's story is being given up, rounded up, and separated in camps.
Drawing a parallel to Anne Frank, who went into hiding on July 9th and later died in a camp.
The comparison between people who turn in asylum seekers and those who remained silent during historical atrocities.
Americans tend to remain ignorant of global situations to avoid facing culpability for actions done in their name.
Refugees come to the U.S. due to interventions in their countries, seeking asylum, facing dehumanization and propaganda.
The reality of conditions in camps, including audio tapes of children crying for their parents.
The argument that refugees staying despite horrific conditions at home shows asylum is necessary.
Calling for learning from mistakes and not repeating history by dehumanizing asylum seekers.

Actions:

for advocates for human rights.,
Contact local refugee support organizations to offer assistance and support (suggested).
Educate yourself on asylum processes and advocate for humane treatment of refugees (implied).
Volunteer at or donate to organizations working with asylum seekers (suggested).
</details>
<details>
<summary>
2019-07-09: Let's talk about adventure, waterboarding, and licking ice cream.... (<a href="https://youtube.com/watch?v=8oWLQsfSgrY">watch</a> || <a href="/videos/2019/07/09/Lets_talk_about_adventure_waterboarding_and_licking_ice_cream">transcript &amp; editable summary</a>)

Beau believes embracing adventures in activism can shape history and encourages individuals to choose their level of involvement to create meaningful change.

</summary>

"We're Coming into a period in American history that's going to require a lot of adventures."
"You can change the world, or not."
"It could be anything from donating to lawyers, to calling your senator, to marching, to organizing a protest, to anything, leafletting, all kinds of stuff."
"If you let other people do that for you, you're going to end up over your head."
"Take that adventure in service of something greater than yourself."

### AI summary (High error rate! Edit errors on video page)

Recalls a past demonstration where he was waterboarded by another person as part of an activist event following the US torture report release.
Describes how a young man volunteered to be waterboarded during the demonstration despite Beau's skepticism.
Talks about the importance of adventures in shaping history, especially during times of social upheaval.
Mentions the need for individuals to choose their own level of involvement in activism.
Encourages the youth to embrace adventure in service of a greater cause rather than engaging in superficial acts like licking ice cream.
Emphasizes that impactful change often comes from individuals taking risks and going on adventures.
Urges people to decide what they are willing to do in terms of activism and not let others dictate their level of involvement.

Actions:

for youth activists,
Reach out to the youth and encourage them to take meaningful actions (implied)
Decide your own level of involvement in activism (implied)
Embrace adventures in service of a greater cause (implied)
</details>
<details>
<summary>
2019-07-08: Let's talk about partisanship and Epstein.... (<a href="https://youtube.com/watch?v=hMIZt6DGRyY">watch</a> || <a href="/videos/2019/07/08/Lets_talk_about_partisanship_and_Epstein">transcript &amp; editable summary</a>)

Beau delves into the dangers of partisanship, urging individuals to prioritize morals over political allegiance and speak out against injustice.

</summary>

"I don't fault the second person in a gang rape. If it's wrong, it's wrong."
"If you ever reach the point where you can up play or down play child abuse of that type for political ends, that should be a red flag."
"We might want to remember that these types of things occur because we silently consent to it."
"These things happen because we allow it."
"You have lost yourself in support of an elephant or a donkey."

### AI summary (High error rate! Edit errors on video page)

Exploring the dangers of partisanship and blind loyalty to a political party.
Mentioning recent headlines showcasing partisanship in the media.
Speculating on the potential involvement of influential figures in criminal activities.
Addressing the issue of overlooking wrongdoing based on party affiliation.
Drawing parallels between excuses made based on past actions of different political figures.
Criticizing the hypocrisy in condemning certain actions while supporting similar ones.
Expressing disbelief at the lack of outrage over serious allegations in certain situations.
Warning about the dangerous consequences of prioritizing political allegiance over morality.
Urging people to speak out against injustice and not remain silent.
Concluding with a call for reflection on complicity in enabling harmful actions.

Actions:

for active citizens,
Speak out against injustice and wrongdoing, regardless of political affiliation (implied)
Raise your voice against harmful actions and policies in your community (implied)
Refuse to overlook immoral behavior based on political party loyalty (implied)
</details>
<details>
<summary>
2019-07-08: Let's talk about Marianne Williamson, Pelosi, AOC, and the future.... (<a href="https://youtube.com/watch?v=MpIwE7KwQYU">watch</a> || <a href="/videos/2019/07/08/Lets_talk_about_Marianne_Williamson_Pelosi_AOC_and_the_future">transcript &amp; editable summary</a>)

Beau examines societal norms, challenges conventional thinking, and calls for accountability amidst generational shifts in politics and values.

</summary>

"Humanity needs a mental shower."
"We need to wash off the prejudices of the 20th century."
"You can get with the program or you can go home."
"The question is why are you there still?"
"They're not going to sit on their hands."

### AI summary (High error rate! Edit errors on video page)

Examines the lessons from three women and their impact on the country's direction.
Advocates for shedding 20th-century prejudices and embracing new perspectives.
Criticizes the dismissal of Marianne Williamson as a viable candidate due to unconventional views.
Challenges the hypocrisy of ridiculing Williamson's beliefs while accepting traditional forms of prayer from other candidates.
Questions the notion of presidential behavior, contrasting showering with love versus bombing countries.
Disagrees with Williamson on the importance of unraveling national secrets to understand sickness.
Comments on the marketability of values like peace, love, and hope in elections.
Addresses the pushback against younger Congress members like AOC by established figures like Nancy Pelosi.
Considers the inevitability of more Millennials entering Congress and the impact of a growing younger voting population.
Criticizes the lack of action on pressing issues such as border camps, incarceration rates, income inequality, drug war, and healthcare.
Urges incumbents to question their own presence in Congress as younger generations demand change.
Emphasizes the rising influence of a generation seeking tangible progress and holding leaders accountable.

Actions:

for voters, activists, politicians,
Join or support political campaigns that advocate for progressive change (exemplified).
Get involved in local community organizing to address pressing social issues (exemplified).
Engage in political education and encourage participation in elections, especially among younger generations (exemplified).
</details>
<details>
<summary>
2019-07-07: Let's talk about coming from a good family.... (<a href="https://youtube.com/watch?v=5pwslfaBVjs">watch</a> || <a href="/videos/2019/07/07/Lets_talk_about_coming_from_a_good_family">transcript &amp; editable summary</a>)

Beau blends journalism styles to uncover truth, challenges victim-blaming narratives in a sexual assault case, reminding us to believe victims even without video evidence.

</summary>

"Everything is alleged until there's a conviction."
"In most cases, the accused does not text a video."
"Without that video, never would have gone anywhere."

### AI summary (High error rate! Edit errors on video page)

Introduction to gonzo journalism blending facts, statistics, fictions, opinions, stories, and historical tidbits to uncover truth.
Emphasizes that everything is alleged until there's a conviction, video evidence doesn't change this fact.
Content warning issued about discussing a sexual assault case in New Jersey involving a 16-year-old girl.
The judge in the case, Judge James Troiano, didn't want it to leave family court for adult criminal court.
Details of the alleged assault at a pajama party where the girl got drunk and raped.
Beau questions the typical victim-blaming response that the girl might face if not for the evidence.
The prosecution believes the accused texted a video of the act with a condemning caption.
Despite the evidence, the judge hesitates to label it as rape due to the accused's background and achievements.
Class distinctions and the privilege of the accused are brought into focus as factors influencing the case.
Beau stresses that without the video evidence, the case might not have been pursued.
Reminds the audience that most cases don't have a video, urging reflection on believing victims without such evidence.

Actions:

for journalists, advocates, allies,
Support survivors of sexual assault (implied)
Believe victims even without video evidence (implied)
Advocate for fair treatment in the justice system (implied)
</details>
<details>
<summary>
2019-07-06: Let's talk about Earthquakes, Hurricanes, and helping yourself.... (<a href="https://youtube.com/watch?v=dkJCy2r0Uaw">watch</a> || <a href="/videos/2019/07/06/Lets_talk_about_Earthquakes_Hurricanes_and_helping_yourself">transcript &amp; editable summary</a>)

Be prepared for emergencies with water, food, shelter, tools, and communication; your readiness can help others in your community survive.

</summary>

"Help doesn't come immediately. It takes time."
"You don't have to eat well, you just have to eat."
"This is all you need to stay OK until help arrives."
"Your children are going to cause you more stress than the actual natural disaster."
"Just take care of this stuff and you're going to be able to help people."

### AI summary (High error rate! Edit errors on video page)

Urges viewers to prepare for emergencies, despite it not being the main focus of his channel.
Emphasizes the importance of self-preparedness due to delays in external help during natural disasters.
Advocates for two gallons of water per person per day and the necessity of water for various activities during a disaster.
Recommends storing water in gallon jugs or five-gallon containers.
Stresses the significance of having enough canned goods to sustain daily calorie intake during a disaster.
Advises having multiple methods for starting a fire and ensuring one has shelter materials like tarps or mylar tents.
Urges viewers to assemble a first aid kit and stock up on prescription medications.
Encourages the inclusion of a knife or multi-tool in the emergency supplies.
Suggests maintaining hygiene products in the emergency kit for comfort.
Recommends having a battery-operated radio for communication during emergencies.

Actions:

for community members,
Stock up on two gallons of water per person per day (suggested).
Purchase canned goods to ensure a daily intake of calories (suggested).
Assemble a first aid kit with necessary medications (suggested).
Include a knife or multi-tool in your emergency supplies (suggested).
Buy hygiene products for comfort during emergencies (suggested).
</details>
<details>
<summary>
2019-07-05: Let's talk about what we can learn from a black Little Mermaid.... (<a href="https://youtube.com/watch?v=VlSfah57lbQ">watch</a> || <a href="/videos/2019/07/05/Lets_talk_about_what_we_can_learn_from_a_black_Little_Mermaid">transcript &amp; editable summary</a>)

People upset over a Black Little Mermaid fail to see the positive theme of breaking down racial divisions present in the original story, missing an opportunity to challenge harmful narratives.

</summary>

"We could probably learn a lot from A Black Little Mermaid."
"The one positive theme in it is giving up these illusions that these things make us different, that these things matter."

### AI summary (High error rate! Edit errors on video page)

Disney is facing backlash for deciding to make the Little Mermaid black, with people upset because the story dates back to 1837.
The original story from 1837 involves a mermaid saving a prince, losing her voice and identity, and ultimately dissolving into sea foam after failing to win his heart.
The themes in the original story include giving up one's voice and identity to fit in and find love, perpetuating harmful ideas about women's worth and the importance of conforming.
Beau criticizes those who oppose a Black Little Mermaid for not recognizing the positive theme of letting go of racial divisions present in the original story.
He suggests that embracing a Black Little Mermaid could teach valuable lessons about breaking down racial barriers and challenging traditional narratives.

Actions:

for storytellers, activists, educators,
Challenge traditional narratives and stereotypes (implied)
Promote diversity and representation in storytelling (implied)
</details>
<details>
<summary>
2019-07-04: Hillbilly tries to 'Merica with gas and fireworks, blows up instead.... (<a href="https://youtube.com/watch?v=_VwfYeEhbvo">watch</a> || <a href="/videos/2019/07/04/Hillbilly_tries_to_Merica_with_gas_and_fireworks_blows_up_instead">transcript &amp; editable summary</a>)

Beau questions the celebration of America on the 4th of July in light of government treatment of individuals in detention, lack of investigation into sexual abuse complaints, and the pursuit of happiness being overlooked.

</summary>

"What are you celebrating? Freedom? No. No, don't think so."
"I didn't see anything about, you know, lines in the sand prohibiting that."
"Y'all have a happy Fourth of July. It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Beau plans to light a giant box of fireworks soaked in gasoline on the 4th of July.
He questions the celebration of America while mentioning the government's treatment of people in concentration camps.
Beau criticizes the lack of basic necessities like soap, toothpaste, and food for those in detention.
He points out the indifference towards sexual abuse complaints in certain locations.
Questioning the celebration of freedom when such injustices occur.
Beau mentions the guarantee of the pursuit of happiness in the document but questions its application.
Ending with a thought-provoking message about the 4th of July celebrations.

Actions:

for activists, americans,
Question the celebration of national holidays (implied)
Advocate for the rights and well-being of individuals in detention (implied)
Raise awareness about overlooked injustices (implied)
</details>
<details>
<summary>
2019-07-03: Let's talk about why they can't fix their country.... (<a href="https://youtube.com/watch?v=mhlAEqf39Sg">watch</a> || <a href="/videos/2019/07/03/Lets_talk_about_why_they_can_t_fix_their_country">transcript &amp; editable summary</a>)

Beau explains how US interventions have fueled instability in the Northern Triangle, urging accountability and a shift in foreign policy to stop perpetuating harm in Central and South America.

</summary>

"We don't need to put better locks on our houses of the United States because other people are breaking in, we just need to stop setting other people's houses on fire."
"It isn't that they can't fix their country. They did it multiple times. It got overthrown because you can't fix your country."
"We can't constantly allow our government to do this and then cry when people try to get away from the situations that we created."

### AI summary (High error rate! Edit errors on video page)

Explains the history of the Northern Triangle countries (El Salvador, Honduras, Guatemala) and their current situations.
Details how the US intervention, support, and influence have significantly contributed to the turmoil in these countries.
Mentions the role of organizations like MS-13 and Barrio-18, originally from the US but transplanted to El Salvador, in causing violence and instability.
Describes the impact of US interventions in Honduras, particularly in controlling the economy and military.
Talks about the United Fruit Company's control over Guatemala and the subsequent interventions that disrupted the country's attempts at self-governance.
Criticizes US actions, including supporting coups and overthrowing democratically elected governments, leading to prolonged civil unrest and human rights abuses in these countries.
Calls for accountability for the US's role in destabilizing Central and South America.
Emphasizes the need for the US to stop causing harm in other countries rather than fortifying itself against the consequences.

Actions:

for policy makers, activists, advocates,
Hold policymakers accountable for past and present US interventions (implied)
Advocate for a shift in US foreign policy towards non-intervention and support for self-governance in affected countries (implied)
Support organizations working towards justice and stability in Central and South America (implied)
</details>
<details>
<summary>
2019-07-03: Let's talk about prosthetic legs, George Soros, and Charles Koch.... (<a href="https://youtube.com/watch?v=h3rzIOeLBNU">watch</a> || <a href="/videos/2019/07/03/Lets_talk_about_prosthetic_legs_George_Soros_and_Charles_Koch">transcript &amp; editable summary</a>)

Beau experiences a heartwarming moment at the grocery store with his son and sheds light on the collaboration between Soros and Koch to establish a think tank aiming to end forever wars, urging grassroots movements to seize the moment for change.

</summary>

"You are so cool."
"This might be the golden moment for the anti-war movement to pop its head back up."
"That's force multiplication, guys."
"For once the big boys are going to be on your side."
"There's going to be a whole lot less prosthetics. I'm okay with that."

### AI summary (High error rate! Edit errors on video page)

Beau experiences a moment at the grocery store with his four-year-old son where he anticipates his son saying something about a woman with a camouflage prosthetic leg, but instead, the son admires her, calling her cool.
George Soros and Charles Koch, considered America's bond villains by Beau, are teaming up to start a think tank called the Quincy Institute for Responsible Statecraft with the goal of ending forever wars like those in Afghanistan and Iraq.
The Institute aims to embody John Quincy Adams' vision of American foreign policy, focusing on diplomacy over military intervention.
Beau encourages grassroots anti-war movements to seize this moment and work alongside special interest groups supported by Soros and Koch.
He suggests that incorporating personal stories of those affected by wars, like veterans who have served multiple tours, into the anti-war movement could be more impactful than solely focusing on economic and policy aspects.
Beau acknowledges potential skepticism about billionaires profiting from peace efforts but believes in the potential reduction of prosthetic needs due to fewer wars.

Actions:

for peace advocates, activists,
Keep an eye on the Quincy Institute for Responsible Statecraft's efforts (suggested)
</details>
<details>
<summary>
2019-07-02: Let's talk about coffee, women, and honor.... (<a href="https://youtube.com/watch?v=NizYw2IbML4">watch</a> || <a href="/videos/2019/07/02/Lets_talk_about_coffee_women_and_honor">transcript &amp; editable summary</a>)

Beau challenges toxic masculinity and advocates for labeling dishonorable actions to drive change, urging simplicity in communication.

</summary>

"Men determine their morality based on the opinions of women."
"Trump supporters are not a well-educated demographic."
"It is time to use toxic masculinity as an advantage."
"If Border Patrol starts to be seen as dishonorable, well, you'll see changes in a bunch of different ways."
"They can't spin the morality of this. This is dishonorable."

### AI summary (High error rate! Edit errors on video page)

Expresses love for coffee and keeps multiple flavors on hand at all times.
Talks about Border Patrol feeling threatened by AOC, a young representative in Congress.
Mentions jokes circulating about why Border Patrol felt threatened by AOC.
Suggests that men in the country get their moral compass from their mothers, impacting their behavior.
Emphasizes the importance of speaking a relatable and simple language to reach a broader audience.
Points out the lack of education among Trump supporters and the need for simplicity in communication.
Encourages using the word "dishonorable" to question immoral actions effectively.
Advocates for challenging toxic masculinity and questioning the masculinity of individuals engaging in dishonorable actions.
Urges women to lead the charge in addressing toxic masculinity and labeling actions as dishonorable.
Stresses the need to paint Border Patrol's actions as dishonorable to drive change.

Actions:

for activists, advocates, women,
Challenge toxic masculinity by calling out dishonorable actions (exemplified)
Use the word "dishonorable" to question immoral behavior effectively (exemplified)
Lead the charge in addressing toxic masculinity and labeling actions as dishonorable (exemplified)
</details>
<details>
<summary>
2019-07-01: Let's talk about toilets, borders, and AOC.... (<a href="https://youtube.com/watch?v=QH71ldfiNMU">watch</a> || <a href="/videos/2019/07/01/Lets_talk_about_toilets_borders_and_AOC">transcript &amp; editable summary</a>)

Beau responds to denial of fascism, recounts border guards' dehumanizing actions, and warns of escalating violence, urging action to prevent dark times ahead.

</summary>

"Guns don't kill people. Governments do and we're on our way."
"We are headed to some very very dark times."
"If you can sit there and laugh at a mother being told to drink out of a toilet, you'll probably be okay with putting a bullet in the back of her head."

### AI summary (High error rate! Edit errors on video page)

Responds to comments denying fascism and genocide.
Border guards joked about a mother drinking out of a toilet.
A Facebook group of guards planned a GoFundMe for assaulting AOC.
The guards knew about upcoming inspections but were unfazed.
Acts of dehumanization and abuse are rampant in detention centers.
Deplorable conditions and inhumane treatment are overlooked.
Laughter at dehumanization makes it easier to escalate violence.
Points out the trend towards escalating violence and dehumanization.
Urges action to prevent further descent into darkness.
Calls for accountability and action to stop the worsening situation.

Actions:

for activists, advocates,
Call for accountability and action to stop the worsening situation (suggested).
Start calling, marching, or doing whatever you're comfortable with to prevent further descent into darkness (suggested).
</details>
<details>
<summary>
2019-07-01: Let's talk about 14 characteristics, 10 stages, and where you are.... (<a href="https://youtube.com/watch?v=83mtXbwPNkc">watch</a> || <a href="/videos/2019/07/01/Lets_talk_about_14_characteristics_10_stages_and_where_you_are">transcript &amp; editable summary</a>)

Beau lists 14 characteristics of fascism, outlines 10 stages, and warns about the signs of genocide happening currently.

</summary>

"There's never been a fascist regime in history that did not go through these ten stages."
"You have all of these lists that have been around a long, long time, and we're matching them up almost like we're following them like a blueprint."
"There are people that need to understand this. They need to understand where we're at."

### AI summary (High error rate! Edit errors on video page)

Lists 14 characteristics of a certain kind of government with examples like powerful displays of nationalism, disdain for human rights, identifying enemies and scapegoats.
Mentions supremacy of the military and rampant sexism as characteristics.
Talks about an obsession with national security, blending of religion and government, and corporate power protection or blending.
Mentions labor suppression and disdain for intellectuals in the arts.
Addresses obsession with crime and punishment, police brutality, cronyism, and corruption.
Talks about fraudulent elections and the 10 stages of fascism, from classification to extermination.
Raises awareness about the signs of genocide and ethnic cleansing happening currently.
Urges viewers to pay attention to the unfolding situation and make a choice to correct it.

Actions:

for activists, community members,
Contact local representatives to demand accountability for human rights violations (implied).
Support organizations working to protect human rights and fight against discrimination (implied).
Educate others about the signs of fascism and genocide happening currently (implied).
</details>
