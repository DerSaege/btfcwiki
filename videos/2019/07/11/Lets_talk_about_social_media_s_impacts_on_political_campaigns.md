---
title: Let's talk about social media's impacts on political campaigns....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=1zZ5ETq2Vj4) |
| Published | 2019/07/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Social media may end up dismantling the political elite in the country by exposing inauthenticity and enabling non-establishment candidates to succeed.
- In the past, candidates could easily stage authenticity for the public, but social media now reveals the staged nature of these events.
- The rise of social media is already impacting politics, with the emergence of non-establishment candidates who win elections.
- People like AOC are examples of candidates on the fringes of the establishment gaining traction through social media.
- Social media allows for greater scrutiny of politicians' actions, making it harder for them to spin narratives like before.
- Marianne Williamson's campaign is described as anti-Trump, appealing to compassion and love rather than fear and bigotry.
- Williamson's strategy contrasts sharply with Trump's, focusing on appealing to the better instincts of humanity.
- Social media enables candidates like Williamson to target more educated Americans and run authentic campaigns.
- Candidates who are not part of the traditional power structures are finding a voice through social media platforms.
- The future of politics might involve more non-establishment figures being able to challenge the professional political class.


### Quotes

- "We're going to see more Justin Armash, AOC, candidates like this that are on the edges of the establishment."
- "She appears to be gearing up to run the exact opposite style of campaign, where she's hoping to appeal to the better instincts and the better nature of humanity."
- "It's a bold strategy. She seems, by tweeting that without any kind of explanation, she seems to be hoping and targeting the more educated Americans."
- "It is authentic. That is who she appears to be."
- "We're going to see them for the establishment. We're going to see them for those who are really there just to enrich themselves with the money they take from us."


### Oneliner

Social media is dismantling political elitism by exposing inauthenticity and enabling non-establishment candidates like AOC, paving the way for a new era in politics.


### Audience

Political Activists, Social Media Users


### On-the-ground actions from transcript

- Support non-establishment candidates in elections (implied)
- Engage in social media platforms to amplify authentic voices and challenge political elitism (implied)


### Whats missing in summary

The full transcript provides a detailed analysis of how social media is reshaping politics and enabling non-establishment candidates to challenge the traditional political class with authenticity and transparency.


### Tags

#SocialMedia #Politics #NonEstablishmentCandidates #Authenticity #PoliticalElite


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're going to talk about how social media may end up killing
the political class in this country.
You know, we have that establishment class where their dad was a congressman
and the whole family is involved in politics, whether it be lobbying
or holding a seat themselves.
And social media is quickly enabling an end to that.
Now, whether or not we take advantage of this,
that's going to be up to us.
But seeing the old days, not too long ago,
if a candidate wanted to show their blue collar roots,
well, he'd throw on a denim shirt,
go to a sports bar, and drink a Bud Light.
Then he'd head over to one of his friends' estates,
grab a hammer and act like he's putting up a pole barn.
These would be shown in three second clips
on the nightly news.
And it would work, because it appeared authentic,
because that was all the information we had.
With the advent and the coming of age of social media,
well, now these images are on there.
And we're sitting there like, is there still
shrink wrap on that hammer?
And it betrays the inauthenticity of these events,
of these staged events like this.
It's going to make it harder and harder for that class of people
to cast the image that there are peers, because they're not.
and the social media accounts are going to show it.
It's already having an impact.
We're starting to see the first social media candidates that win.
And we're seeing these people who are not part of the establishment at the time they arrive.
They may become part of the establishment later.
But when they show up, they're not.
you've got like the one Republican who's just like I'm out I'm done with this
party I can't I can't I can't deal with it because this is a person who truly
believes in what he's saying right or wrong he believes it and he gets up
there and he realizes that they don't that it's a show and at the same time
the Democrats just had to have a come to Pelosi meeting over Twitter and Pelosi
basically telling them, you know, don't take, don't take pot shots at our,
our, our Senate Democrats.
Um, and this came about because they voted in favor of that ICE funding bill
that, uh, didn't include any safeguards for the kids.
And I guess somebody said something about the, it being the child abuse caucus.
And that hurt their feelings, you know, if the jackboot fits, slip it on a goose step.
You all chose politics over principle?
It is what it is.
It's the social media age.
We have access to your voting records now instantaneously.
You're not going to be able to spin this the way you used to.
And the reason I started thinking about this is because this morning I woke up to a text
and it said the candidate retweeted you.
And I'm thinking, Oh God, what did Trump say?
I don't know if y'all are aware of this in indie news circles, Trump was the
candidate quotation marks, because we didn't think he was a viable candidate.
We were wrong.
It happens.
Okay.
But it wasn't him.
It was Marianne Williams.
She retweeted the video that I made in which I referred to her as the crazy,
hippie crystal lady, and I said she wasn't a viable candidate.
Now before we get into this, I have to say I'm not talking about her policies because
I haven't looked into all of them yet.
Strictly talking about the campaign, she's going to run an anti-Trump campaign, and I
don't mean that in the sense of she's just going to bash Trump at every opportunity.
I mean that in the sense of she's going to run the antithesis of the campaign he ran.
He ran a campaign that played on fear, that appealed to bias and bigotry, that's what
he did, and he targeted the uneducated, his work, not mine.
He wanted those people that were too uneducated to understand that a tariff is going to impact
them until it's too late and they're standing at the checkout line, but by then they've
already bought into him. So they can't change their mind, otherwise they'll have to admit
they were wrong.
She appears to be gearing up to run the exact opposite style of campaign, where she's hoping
to appeal to the better instincts and the better nature of humanity. You know, let's
be the world's EMT instead of being the world's policeman. Appealing to compassion and love
crazy things like that. It's a bold strategy. She seems, by tweeting that without any kind of
explanation, she seems to be hoping and targeting the more educated Americans. Those that can look
at that video and understand the subtext is she's not a viable candidate because the system won't
let her be one but it'd be a whole lot cooler if it did but there was no
explanation of that it was just... here it is. It's a bold strategy considering the
last guy won running the exact opposite style of campaign but it's authentic it
It is authentic.
That is who she appears to be.
Now, I'm sure there's somebody out there
that's gonna be like, well, it's just her brand.
Maybe.
Maybe it is.
I don't know, I don't know her.
It certainly could be.
This whole thing could just be her brand and her front.
But she's been at it a long time.
And at some point, if you're gonna live a brand that long,
well, it's just you.
So, this is kind of what we have to look forward to.
We're going to see more Justin Armash, AOC, candidates like this that are on the edges
of the establishment.
You know, you're probably not going to see a bunch of poor people running yet, but social
media is enabling those people that aren't within the true halls of power to
get a voice and to get their message out there. And it's going to be interesting
to see how this plays out because it may truly kill the professional political
class because we're going to see them for what they are. We're going to see
them for the establishment. We're going to see them for those who are really
there just to enrich themselves with the money they take from us. Anyway it's just
of thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}