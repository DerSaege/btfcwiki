---
title: Let's talk about the government's relationship with the environment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wMpHPykNENI) |
| Published | 2019/07/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recalls the bombing of the Rainbow Warrior in 1985 by French intelligence due to its mission to draw attention to a French nuclear test, resulting in a death and French government conviction.
- Compares the French government's suppression of environmental advocacy to current actions by the U.S. government.
- Notes the U.S. President's contradictory claim of being a land protector while overseeing significant rollbacks of environmental regulations.
- Points out the administration's extensive list of environmental regulation rollbacks, including opening the U.S. coastline to offshore drilling and scrapping safety regulations.
- Mentions the removal of references to climate change by federal agencies, drawing parallels to the French bombing incident to prevent public discourse on climate change.
- Emphasizes that while debates exist on the origins of climate change, its occurrence is undeniable and poses a national security threat.
- Argues that governments prioritize control and power over protecting citizens, using malice to suppress climate change information.
- Compares historical military power motives for actions like the boat bombing to present-day colonization through corporate influence in environmental matters.
- Points out the presence of former industry lobbyists during the President's press conference, indicating a politicization of environmental issues driven by industry interests.
- Concludes with a reflection on the financial influence of industries like oil and coal in environmental policymaking.

### Quotes

- "Government's not here to protect you."
- "We don't colonize anymore with flags and guns, we do it with corporate logos."
- "But at the end of the day, it's happening."
- "The environment as a whole doesn't have any money but the oil and coal industry do."
- "Y'all have a good night."

### Oneliner

French intelligence bombed the Rainbow Warrior to silence environmental advocacy; Today, U.S. environmental regulations are rolled back while climate change information is suppressed for corporate interests.

### Audience

Environmental activists

### On-the-ground actions from transcript

- Organize local environmental protection events (implied)
- Support organizations advocating for environmental conservation (implied)

### Whats missing in summary

Detailed examples and historical context of environmental harm and government actions.

### Tags

#Environment #Government #ClimateChange #Regulations #IndustryInfluence


## Transcript
Well howdy there internet people, it's Bo again.
So tonight we're gonna talk about
government's relationship with the environment.
And I don't just mean the U.S. government,
although it's gonna be part of the example.
I mean all governments in general.
The environment's always a secondary concern.
Very rarely, the primary.
On July 10th, 1985, there was a boat down in New Zealand.
It was called the Rainbow Warrior.
It was down there to draw attention to a French nuclear test.
French government didn't like this very much.
They didn't want people talking about it.
So French intelligence blew it up, killed somebody in the process.
That's not a conspiracy theory.
They got caught, pled guilty, went to prison, it happened.
In a less dramatic way, something very similar is happening in the United States right now.
Recently the President gave a press conference of sorts, in which he proclaimed himself a
land protector.
Conservationists all over the world just stared at their TV in utter confusion.
This administration has rolled back so many environmental regulations I was going to list
them, but we don't have that kind of time.
It's insane.
It's an immense list.
But Cliff notes, he's overseen the largest rollback of federal land protections in history.
He tossed out the idea of opening the entire coastline of the United States up to offshore
drilling, while simultaneously getting rid of the safety regulations that help prevent
spells.
He killed the moratorium on leasing federal lands to the coal industry, squashed regulations
about the disposal of coal ash, rolled back so many clean water regulations I don't even
where to begin. So, yeah, it's double-speak. Politicians saying one thing and doing another.
That's kind of normal. It's kind of normal. But at the same time this is going on, federal
agencies are currently scrubbing all references to climate change. Because, well, can't have
you talking about it. Kind of like bombing that boat. The test is going to happen. Climate
change is going to happen. We just don't want people talking about it. So, yeah, it's
normal politics as usual. But this is done with malice. There's no debate about whether
or not climate change is a thing. There's some debate, although not vigorous debate
over whether or not it is man-made or man-assisted or whether or not it's natural. You can debate
that if you'd like. But at the end of the day, it's happening. The Pentagon is drawing
up plans to deal with it. It's a national security threat. But you're going to be denied
the information you need to deal with it, because contrary to the very popular fiction,
government's not here to protect you.
It's not a protectorment, it's a government.
It's there to govern you, to maintain control and maintain power, and that's what it's about.
Back in 1985, it was about military power.
So that's why they bombed the boat, but today it's different.
We don't colonize anymore with flags and guns, we do it with corporate logos.
We engage in cultural colonization as we rip whatever we can out of the ground.
It's worth noting that while he gave this press conference, he was flanked by a former
oil lobbyist
and a former lobbyist for the coal industry
it's something we need to keep in mind as the environment gets politicized
because the environment as a whole
doesn't have any money
but the oil and coal industry do
anyway it's just a thought
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}