---
title: Let's talk about how it can't happen here, but it can happen in Alabama....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NJo9vcwlI80) |
| Published | 2019/06/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Contrasts the belief that certain atrocities can't happen in the United States with historical evidence suggesting otherwise.
- Reads a disturbing social media post by the mayor of Carbon Hill, Alabama, advocating for killing minorities.
- Comments on the mayor's apology after being exposed, stating that it doesn't excuse his actions.
- Calls for the mayor's resignation rather than accepting his apology.
- Raises concerns about the mayor's influence over law enforcement and treatment of minority groups.
- Mentions that he learned about the issue not through the news but from a far-right constitutionalist.
- Criticizes the lack of action from the governor and others to remove the mayor from office.
- Condemns the mayor's advocacy for mass murder and lack of due process.
- Urges for the removal of the mayor to send a message that such behavior is unacceptable.
- Expresses disappointment in the state of Alabama for allowing this situation to persist.

### Quotes

- "The only way to change it would be to kill the problem out."
- "They want your resignation. That's what they want."
- "Your prospective soldiers, they think you're a threat."
- "They see you as a fascist thug."
- "The fact that there are no wills churning already to remove this person says everything you need to know about the great state of Alabama."

### Oneliner

Contrasts belief in immunity to atrocities with the disturbing advocacy for mass murder by a government official, calling for his resignation and criticizing the lack of action in Alabama.

### Audience

Alabama residents

### On-the-ground actions from transcript

- Call for the mayor of Carbon Hill, Alabama, Mark Chambers, to resign (suggested)
- Pressure the governor and other officials to take action to remove Mayor Chambers from office (exemplified)

### Whats missing in summary

The full transcript provides a detailed analysis of the disturbing advocacy for mass murder by a government official and the urgent need for accountability and action to address such behavior effectively.

### Tags

#GovernmentAccountability #Advocacy #Resignation #Alabama #MassMurder


## Transcript
Well, howdy there, Internet people, it's Bo again.
So, you know,
when we look around and we look back,
we think it can't happen here.
When we look at what's going on in other countries,
it can't happen here.
When we look at World War II, it can't happen here.
We feel that the United States is somehow immune
to all of the horrible things
have gone on in the world when all historical evidence points to the contrary but it's this
widely held belief that now we're more civilized, now we're better. So I want to read you a
social media post real quick and I'll explain why I'm reading them here in a minute. The original
post written in all capital letters because, of course, is we live in a society where homosexuals
lecturers on morals, transvestites lecturers on human biology, baby killers lecturers on
human rights, and socialists lecturers on economics. The response, by giving the minority
more rights than the majority, I hate to think of the country my grandkids will live in unless
somehow we change, and I think that will take a revolution. The original poster.
The only way to change it would be to kill the problem out. I know it's bad to say,
but without killing them out, there's no way to fix it.
Just some stupid bigots on Facebook, right? That's all it is.
The problem is, this is the mayor of Carbon Hill, Alabama, Mark Chambers.
Kill him out.
That's how you fix it.
It's a good final solution there, I guess.
It's insane.
Anyway, once he was found out about this, he did issue an apology.
Well, first he said that it wasn't his.
Then he said, well, it was his,
but I thought it was private.
That doesn't make it better.
And then he said that, well, it's not really calling for
because that's only if there's a revolution.
And then he releases this.
I would like to make a public apology to my community.
I and I alone am responsible for the comment that was made.
It is not a reflection of the Carbon Hill City Council or any city personnel or citizens.
Although I believe my comment was taken out of context and was not targeting the LGBTQ
community, I know that it was wrong to say anyone should be killed.
I am truly sorry that I have embarrassed our city.
I love this city and while in office I have done everything in my power to make this a
a better place for our families.
There are not enough words for me to express how much I regret posting that comment.
I hope very much our citizens and anyone that was hurt by this comment can accept my apology.
No.
Nobody wants your apology.
They want your resignation.
That's what they want.
A government employee, government official, elected official, openly, publicly, advocating
for the mass murder of people.
Can't happen here, right?
Now as far as you're out that, well, it's only in a revolution.
That doesn't make it better.
Generally, even in revolutions, mass murder is frowned upon, especially when you're targeting
people based on the criteria you chose.
This is ridiculous.
The fact that this person is in office, it shouldn't surprise anybody who's familiar
with Alabama-stan, but it's what it is.
This person has power.
person has influence over law enforcement in that area. I wonder how
they treat minority groups. I wonder how they treat the LGBTQ community. You know,
that environment is fostered from the top and that's the top. That's the top.
The funny part about this is that I found out about this not through the news, but because
a three percenter from this area called a three percenter in Dothan.
Now for those that don't know, three percenters are far right constitutionalists.
When you hear the media talk about the patriot movement, that's these guys.
So one from this area called One in Dothan, who wound up talking to me, and the guy from
Dothan was like, yeah, man, this guy, he hates gay people.
I mean, like literally hates them, and he doesn't know what to do because from where
he's sitting, this is a government employee advocating for people to be murdered.
This is obviously somebody that would abuse their power.
So the idea, Mark Chambers from Carbon Hill, that you would somehow lead a revolution.
Your prospective soldiers, they think you're a threat.
They see you as not a patriot because you're not.
I'm assuming you took an oath to support and defend the Constitution of the United States.
I don't see a whole lot of due process in this.
They don't see you as a patriot.
They see you as a fascist thug.
Yeah, you have embarrassed your city.
And yes, it is a reflection on the city council.
It is a reflection on everybody that isn't calling for your resignation.
It can't happen here, right?
The only way to make sure that that's true is if you send the message that it can't
happen here.
And the only way to do that right now is to remove this clown from office.
The fact that the governor has not put pressure on it, the fact that there are no wills churning
already to remove this person says everything you need to know about the great state of
Alabama, openly, mass murder, kill the problem out, anyway, can't happen here right, it's
It's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}