---
title: Let's talk about power and potato chips....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=vS4RQfkDl_M) |
| Published | 2019/06/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Questions the source of power and authority that the state derives from the governed.
- Raises concerns about delegating authority to the state that individuals do not possess.
- Draws parallels between giving authority to the state and giving something one does not have personally.
- Challenges the concept of majority rule justifying violations of others' rights.
- Advocates for taking infringement on others' freedom seriously and tying it to one's conscience.
- Suggests that if individuals lack the authority to do something on their own, they cannot delegate it to the government.
- Differentiates between a justice system based on conscience and a legal system.
- Criticizes the hypocrisy in immigration debates and the treatment of asylum seekers.
- Questions the morality of preventing others from seeking a better life based on arbitrary distinctions.
- Encourages following one's conscience over blindly obeying authority figures.

### Quotes

- "If something is beyond the power of your own conscience, it should be beyond the power of the state."
- "You don't have the authority to give something you don't have as individuals."
- "Infringing on the freedom of others is something that has to be taken very seriously."
- "Maybe it is time for this country to start following its conscience instead of simply following orders."
- "You have a right to be a bigot I guess. Whatever. But does that mean that you have the power?"

### Oneliner

Beau questions the state's authority, urging individuals to follow conscience over blind obedience and exposing hypocrisy in societal norms.

### Audience

Citizens, Activists, Voters

### On-the-ground actions from transcript

- Follow your conscience in decision-making (implied)
- Challenge unjust authority and norms (implied)
- Advocate for policies based on morality and conscience (implied)

### Whats missing in summary

Beau's thought-provoking analysis and call for introspection on power dynamics and moral responsibility.

### Tags

#Authority #Conscience #Justice #Immigration #Hypocrisy


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're going to talk about power, where it comes from.
Where does the state get its power?
Ideally, from you, the consent of the governed, right?
That's the theory.
Something always bothered me about this, though.
How do we delegate an authority to the state that we don't have?
How do we give something that we don't have as individuals?
Try it with anything else.
You've got two bags of potato chips in front of you, a bag of Doritos and a bag of Lay's.
You cannot give a bag of Ruffles.
You don't have it.
You don't have that authority.
You don't have that power.
But for some reason when it comes to the state, we seem to think that we can do it.
And somebody out there right now is going, majority rules.
Yeah, that's true in a democracy, majority rules.
However, you can't violate the rights of others simply because the majority of people want
to do it. It's not how it works. If that was true, if just because the majority of
a group wants something, the rights of others don't matter, well then every
gang, sexual assault, and history was justified. It's not how it works. You
can't do that.
Infringing on the freedom of others is something that has to be taken very
seriously. I'm a firm believer in the idea that if something is beyond the
power of your own conscience, it should be beyond the power of the state. If you
don't feel you have the authority to do it on your own, you can't delegate that
authority to the government. If we were to apply this principle, we would have a
justice system rather than a legal system. Willing to bet most people
watching this do not feel that they have the authority, acting on their own, to
kicking the door on somebody else's home because they're smoking a plant. That's
silly. They're not harming anyone, right? But I do believe your conscience would
allow you to act on behalf of an actual victim, of somebody who is harmed by
somebody else. Makes sense. And I'm bringing this up because I read all of
the conversations on the shares on Facebook and everything on the
immigration thing. And it just blew my mind. You know first it always starts
with well we just want them to come here legally. Asylum seeking is illegal. Well
not like that though. Look at some point you guys are just gonna have to admit
you don't want them here. You're just gonna have to admit that. Fine. You have
a right to be a bigot I guess. Whatever. But does that mean that you have the
power. You have that authority to stop them from coming here. I'm willing to bet that
your conscience would not allow you to take the kids from somebody and throw them in a
cage simply because they moved from Alabama to Tennessee. But because it's some other
line and these people are others, they're the other, somebody else, they're not like
us, it seems okay, especially if somebody in a costume wearing a badge doesn't.
All of a sudden it's alright.
Doesn't make any sense.
And then it blew my mind to watch people talking about it in regards to a job, protecting
a job for Americans.
Man, would you go kidnap the person working at the local 7-Eleven so you could have their
job?
Of course not.
That's insane.
That is insane.
But for some reason, we feel we have the ability to delegate that authority to others.
It would violate your conscience to do it on your own, but because the state somehow
says it's okay, well then it's alright, it doesn't make sense.
We never had the authority to do that anyway, we couldn't delegate it to them.
There are people out there who will follow orders, no matter what, under the guise of
authority.
Maybe it is time for this country to start following its conscience instead of simply
following orders.
Anyway, it's just a thought.
y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}