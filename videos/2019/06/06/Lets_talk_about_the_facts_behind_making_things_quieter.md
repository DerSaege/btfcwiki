---
title: Let's talk about the facts behind making things quieter....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=COj1gl_fdsI) |
| Published | 2019/06/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Receives questions about making something quieter from both Second Amendment advocates and gun control advocates.
- Explains the difference between suppressors and silencers, debunking myths.
- Describes how suppressors work by reducing the decibels of a gunshot through a tube with baffles.
- Notes that suppressors reduce recoil by about a third and help mask the location of a shooter by changing the sound profile.
- Mentions that suppressors also reduce muzzle flash and are primarily used for military purposes to avoid raising alarms.
- Addresses the illegal use of suppressors, stating that statistically, they are not significant in crimes.
- Emphasizes that suppressors have legitimate uses, particularly for hearing protection, and are popular among law-abiding gun owners.
- Explains the limitations of homemade suppressors compared to commercial ones.
- Concludes that a ban on suppressors could be effective but acknowledges that it targets law-abiding citizens who are unlikely to use them for criminal activities.
- Touches on the debate regarding the necessity of suppressors under the Second Amendment.

### Quotes

- "It doesn't make anything silent."
- "Decibels are a logarithmic system."
- "They reduce recoil by about a third."
- "If you're just letting off one shot, that's going to sound like anything other than a gunshot."
- "They're very, very law-abiding people."

### Oneliner

Beau explains the science behind suppressors, debunking myths and discussing their legitimate uses among law-abiding gun owners, while addressing the potential effectiveness of a ban.

### Audience

Gun Owners

### On-the-ground actions from transcript

- Contact local officials to advocate for sensible gun regulations (implied).
- Educate others on the legitimate uses of suppressors for hearing protection (implied).
- Support organizations promoting responsible gun ownership (implied).

### Whats missing in summary

In-depth analysis of the debate around suppressors in relation to gun control and the Second Amendment.

### Tags

#GunControl #SecondAmendment #Suppressors #DebunkingMyths #CommunityPolicing


## Transcript
Well, howdy there, internet people, it's Bo again.
So over the last 36, 48 hours, my inbox has been filling up with questions about how to
make something quieter.
They're coming from both Second Amendment advocates and gun control advocates, both,
I think, hoping I'm going to tell the other person they're wrong.
So I guess today we're going to go through all the questions and we are going to talk
suppressers, silencers. We're going to approach it the same way we did the gun
control conversation. I'm going to give you the facts and you can debate it in
the comments section. My opinion on this really doesn't matter much. Okay so the
first and most common question, what exactly is a silencer? A really bad name
for a suppressor. Doesn't make anything silent. There's a lot of myths involving
suppressors and we're going to go through them right now. You have to understand the
science behind how they work and what they do to understand the debate. So we're going
to start there. The average gunshot, it can range in decibels from 150 to 200 plus or
minus a little bit. The suppressors today reduce that anywhere from 10 to 45 depending
on what type of ammo is being used, what type of weapon it's on, the caliber of the weapon
and the type of suppressor.
All of these things can change how much it's being reduced.
The average is about 30.
Now when you're just looking at the numbers that doesn't seem like a huge reduction and
in one way it's not, but it doesn't tell the whole story unless you understand that decibels
are a logarithmic system meaning 200 decibels isn't double 100 decibels it's not twice as loud
it's more like a thousand times as loud so a 30 decibel reduction is pretty substantial. However
even once you put the suppressor on the weapon and fire it you're going to get decibels in the
range of 115 to 150. Hearing loss can occur at 85. 115, 150, this is like sitting in your
car with the stereo all the way up. It's not silent. It's really not. That's definitely
a misnomer. So that's the sound sign. Now how do they work? It's a tube and it's baffling.
Suppressor joke. Inside the tube there are baffles and what these things do is they slow
down the gas that's leaving it. What's pushing the bullet out of the barrel slows it down
and since it slows it down, it reduces the amount of bang that comes out the end.
It's simple theory, putting it into practice is a little bit harder.
What do they do other than make the gunshot quieter?
They reduce recoil by about a third.
Now if you're a sniper, they're really important because there are two sounds that come out
of a gunshot.
One is the bang.
The other is a sonic crack that's coming off of the bullet itself as it travels through
the air.
If you can reduce the bang and leave the crack, as that bullet travels, that noise is bouncing
off of everything around it.
It makes it very hard to pinpoint the location of the shooter.
It really does a good job of masking your location.
This is why in a lot of theories there's confusion over the number of shooters because they're
going off of what they heard and it sounded like the shot came from over here and another
one came from over here.
It's probably the same bullet and they're experiencing the sound at two different times
So, it seems like two bullets.
They reduce the flash and in some cases completely eliminate it, the muzzle flash that comes
out of the end of the barrel.
The most important thing they do as far as militarily speaking, it's not the reduction
in sound.
It changes the sound.
It doesn't sound like a gunshot anymore and if you're out somewhere where there's ambient
noise in an urban area or even here with the grasshoppers and crickets, that noise if you're
just letting off one shot, that's going to sound like anything other than a gunshot.
Your mind will attribute it to something else.
So it doesn't get reported, so it doesn't raise an alarm.
That's why they're really important for the military.
How are they used illegally?
or not, not in any statistically significant way.
You can buy them legally in the US.
The process, the last person that I know that got one,
it took them about eight months to get the tax stamp
and the paperwork through the ATF and all of that.
The ATF knows you have it, and there's a lot of them.
There's a lot of them.
And the statistics as far as how often they're used in crime,
it's normally about 40 a year.
Those aren't even all violent crimes.
But even if you took all of them,
you're looking at 0.003% of silencers are used in a crime.
It doesn't happen very often.
Yes, advanced and sophisticated organized crime, they use them, smugglers use them,
high-end illicit gun dealers use them, but those people tend to only kill each other.
It's not something that the general populace has to worry about.
I know all of this is very fresh in everybody's mind because of the shooting, but prior to
this, when was the last time you heard about one being used? They don't get used
very often. The most common illegal use that I'm aware of is hunting out of
season because just like the sniper, masking that location makes it hard for
the game warden to find them. That's the most common illegal use that
I'm aware of. Why are they used legally? Why do people, literally a million
people have one. If you combine hearing protection and a suppressor, you get below that 85 decibels.
They're used typically by people who shoot a lot, and I mean a lot, and this is an extremely
law-abiding crowd. These are the guys that have the Molon Labe sticker, you know, come
and take it on their truck right next to the thin blue line sticker because they still
just have no idea who's going to come and take it. These are very, very law-abiding
people. That's who uses them. You don't find them in the normal criminal world very
often. Let's see the next question was actually this this cracked me up. You
changed my opinion of the assault weapons ban by explaining how easily they
were manufactured at home. I looked it up and found a tutorial of a guy who made
an AK out of a shovel. Are suppressors easily manufactured at home? Kind of. You
You can make them at home, you can.
You're not going to achieve the same quality and you're not going to get the same decibel
reduction that you get in a commercial one.
It's much harder to make a properly functioning suppressor than it is a firearm.
There are improvised ways to do them, there's little cheap tricks.
Most of them aren't good for more than a few shots and the ones that are good for more
than that, the people that know how to make them, they're not telling anybody.
That's still something that's pretty closely held in that community.
They don't let that secret out.
And then the important question, I guess, would a ban be effective?
Yeah, but the reason it would be effective is probably the strongest argument anybody's
going to be able to come up with against the ban.
These are very, very law-abiding people.
So if they were banned and they were told they had to turn them in, they would.
They would.
And they're registered, the ATF knows they have them.
It's not like the bump stock band, where it was just a piece of plastic or whatever and
nobody knew they had it so they didn't have to turn it in.
The suppressors are registered and I mean, the people that own them will turn them in.
So in a way, yeah, it would be effective but you're taking them from people who are extremely
law-abiding that they're not going to use them in a mass shooting or in crime in general,
going by the statistics.
My understanding is that a lot of that 40 per year are people who did stuff with the
suppressor that they shouldn't have under the regulations.
They didn't store it properly or they didn't, you know, stuff like that.
So that's the general, a general overview of them.
Now the other question, is it necessary under the intent of the Second Amendment?
Depends on how you look at the Second Amendment.
If you look at it the way I do, the purpose of it is to be able to strike back against
the government.
Not really.
It's not necessary.
Could it be helpful in some situations, maybe.
And there are a whole lot of people who don't look at the Second Amendment that way.
So that debate is definitely going to rage.
Now the one thing that I do want to point out is that I find it hilarious that Donald
Trump has become more of an advocate for gun control than Obama ever was.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}