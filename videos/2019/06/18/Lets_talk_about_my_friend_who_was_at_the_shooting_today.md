---
title: Let's talk about my friend who was at the shooting today....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=gxEg2-bWM9M) |
| Published | 2019/06/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau shares a personal experience of a friend involved in a shooting incident at a gun-free zone.
- The friend secured his weapon in the car, heard gunfire, and moved to a safer location instead of engaging the threat.
- Beau challenges the myth of the "good guy with a gun," stating that untrained individuals are more likely to die in combat than successfully intervene.
- He mentions the prevailing myth of guns not killing people, but rather people killing people, and how it leads to a dangerous mindset.
- Beau refers to the conditioning required for individuals to kill and how only 4% of people are naturally wired for it.
- He talks about the flawed belief that arming more people will stop mass shootings and dismantles the idea of a "good guy with a gun."
- Beau criticizes the cultural glorification of guns and stresses the need to address the root cultural issues rather than rely on legislation.
- He mentions the case of the Parkland cop and the different standards applied due to training and duty to intervene.
- Beau concludes by stressing that the concept of a "good guy with a gun" is part of the problem, not the solution, and signifies toxic masculinity.

### Quotes

- "Guns don't kill people. People kill people."
- "The gun doesn't make the man."
- "It's a bandaid on a bullet wound."

### Oneliner

Beau challenges the myth of the "good guy with a gun" and addresses the cultural issues around gun ownership, advocating for a shift in mindset away from toxic masculinity.

### Audience

Advocates for cultural change

### On-the-ground actions from transcript

- Educate others on the dangers of the "good guy with a gun" myth (suggested)
- Challenge the glorification of guns in culture (implied)

### Whats missing in summary

Full understanding of the dangers of perpetuating the myth of the "good guy with a gun" and the need for cultural change.

### Tags

#GunViolence #ToxicMasculinity #CulturalChange #GunMyths


## Transcript
Well, howdy there, internet people, it's Bo again.
I had a friend at the shooting today.
Pulled up to work.
It's a gun-free zone, so he took his weapon out,
secured it in his car, got out of his car,
locked his car gunfire ring out.
Looked like from the photo 50 yards away.
Um, so every good guy with a gun out there
knows what happened next.
He unlocked his car, grabbed that weapon, and moved to engage the threat, right?
No, of course not.
Because this isn't the comments section, this is real life.
He went to the federal building where there's armed security and they locked it down.
Everybody has this image that they're a superhero or a supervillain and they know
how they're going to respond.
You don't, you don't.
people would do what he did. 96% of people would do what he did. And before we get into
this so I don't forget, I think he made the right call. I think he made the right call.
He's not ex-military, not an ex-cop, not trained and conditioned to kill. Untrained people
do one thing really well in combat, die in a very loud and grotesque manner. He did the
right thing. That's the right call for him to make. There's this prevailing myth
of the good guy with a gun and it's just utterly false. It's utterly false and the
gun crowd knows it's utterly false too. They have to know it because it's
That's their slogan.
Guns don't kill people.
People kill people.
You'll say that to a gun control advocate, but for some reason when you go to the store
and buy a Mark 23, all of a sudden you're an operator.
It's not how it works.
The gun doesn't make the man.
And that concept is what's fueling a lot of these shootings.
that violence is the answer.
Violence is always the answer.
And that you've got to be capable of this.
Most people aren't, the overwhelming majority
of people aren't, 4%.
If you want to look into that topic,
there's a series of books by Lieutenant Colonel Grossman.
On Killing is the one I recommend.
And I know somebody brought him up in the comments section.
That's just serendipity, completely random.
that we're talking about him today.
I know you wanted to discuss him training cops now,
and believe me, there's gonna be a video on that.
But 4% of people are miswired to be capable of killing
with no issues.
They don't suffer the psychological consequences
of killing, and they don't have to overcome
psychological barriers to do it, 4% of people.
Everybody else needs to be conditioned to do that.
Everybody else.
What makes a grass grow green?
Somebody just screamed back at their screen, blood, blood,
blood, I hope you weren't at work, because if you were, now
you look like a psycho.
But you're conditioned to do that, right?
It's part of it.
The profession of arms as a whole has methods of conditioning people to be capable of killing.
That's where it begins, right there at Benning.
That's where it starts.
Later on, those pop-up metal targets, right?
Does that really prepare you for combat?
No, of course not, because that's not what they're teaching you.
They're conditioning you to kill.
Target, shoot it.
They're conditioning you.
what it is because the military understands only four percent of people are naturally
wired that way.
Now I know somebody is going to bring up that FBI survey that is often pointed to by people
who subscribe to the good guy with a gun theory that you know, well you know out of 50 mass
shootings in 2016 and 2017, a good guy stopped him eight times, that's true, that
is true. However, it's a critically flawed survey and we'll talk about that, but eight
people, eight shooters were stopped. Four times the people that did it were
unarmed. Only two times did they exchange gunfire with the person and end
the threat. Two is what percent of 50? Even in a study that flawed, that didn't
take into account how many people were armed and whether or not people chose
not to act or anything like that, the number is still 4%. It's a
constant and has been since this topic really started being looked at in World
two. So with all of this out there the idea good guy with a gun get rid of gun
free zones therefore more people are armed we'll stop it. It's just false it's
not how it works it's not gonna work and besides I know there's a correlation
between mass shootings and gun free zones but is it causation? Most places
where large amounts of people gather which is kind of a requirement for a
mass shooting are gun-free zones. Now evidence to the contrary, to kind of
dispel this correlation, since Columbine every school has somebody with body
armor and a gun there, has the rate of school shooting slowed? No, of course not.
Not everybody in the world is part of that four percent, but every mass shooter is they don't care
that's not it's not even going to factor into their equation most of them don't plan on walking out anyway
so obviously this goes to the whole army of teachers thing I did a video on it I'll link it
It's a bandaid on a bullet wound.
This is not a problem that's going to be solved with legislation.
It's a cultural thing.
We've got to get rid of the idea that the gun makes the man.
That's step one.
It's a tool and it needs to be remembered that it's simply a tool.
It doesn't make you something special because you own a gun.
Now, as far as the psychological aspects, a teacher doesn't have to rush headlong
into gunfire.
A teacher is under attack, fight, flight, or freeze, they'll choose one, if they choose
fight and they happen to be armed, it will make them more effective in the fight.
Is this going to be a grand determining factor in mass shootings at schools?
No, of course not.
And that's assuming it's all done right.
And as discussed in that video, it won't be.
It won't be.
So how does this relate to the Parkland cop?
Why am I holding them to two different standards?
Because one person was part of the profession of arms.
One person was trained.
One person, in my opinion, had a duty to intervene to save those kids.
I know the Supreme Court disagrees with me on that, and that's fine.
They've been wrong before.
good guy with a gun, it's not a real thing. It's not a real, it's not a theory
that you can base domestic policy on. And in fact, to me, the concept that all of a
sudden, if somebody has a gun, they're more of a man, it's part of the problem, not
part of the solution. Because to that 4%, those with that mindset, that weapon, that
That gun is a tool, they're the weapon, they can do it with a pocket knife or a rock
if they have to, or unarmed as in that study.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}