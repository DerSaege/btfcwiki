---
title: Let's talk about mass removals and freedom....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=uCt60ViLh7M) |
| Published | 2019/06/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Statue of Liberty arrived in New York in 1885, without the famous inscription.
- The President plans to target undocumented people for removal, including those who committed no other crime than crossing a line.
- ICE is hesitant about this approach due to bad optics and potential public backlash.
- Removals are typically done at job sites to avoid public scrutiny and dehumanization.
- The impact of seeing families torn apart during removals will be significant.
- The punishment for undocumented immigrants seems harsh compared to other violations like watching a movie without a ticket.
- Children of undocumented parents face dire situations if their families are targeted for removal.
- Cheering on mass removals contradicts the fear of government overreach.
- Amnesty is suggested as a humane solution to prevent the destruction of millions of lives.
- Prioritizing removals over going after real criminals like human traffickers is questioned.

### Quotes

- "You're going to see they're real people."
- "Amnesty, amnesty, amnesty. the law. Amnesty."
- "Let's help the tempest-tossed, the huddled masses."

### Oneliner

The Statue of Liberty, removals, and the call for amnesty – a poignant reminder of the humanity behind immigration policies.

### Audience

Advocates, Activists, Citizens

### On-the-ground actions from transcript
- Advocate for humane immigration policies (implied)
- Support organizations working to protect undocumented individuals (implied)
- Educate others on the realities of immigration enforcement (implied)

### Whats missing in summary

The full transcript conveys a powerful message on the need for empathy and compassion in immigration policies.

### Tags

#Immigration #Humanity #Amnesty #Government #Advocacy


## Transcript
Well, howdy there, internet people, it's Bo again.
Give me your tired, your poor, your huddled masses
yearning to breathe free, the wretched refuse
of your teeming shore.
Send these, the homeless, the tempest-tossed to me.
I lift my lamp beside the golden door.
Today, in 1885, the Statue of Liberty arrived in New York.
This inscription wasn't on it at the time, and it was added later.
But the Statue of Liberty arrived 134 years ago, and here we are, 134 years later.
Man, that made a lot of difference.
The president has said that there are going to be millions of removal, so at least two
million and one of undocumented people.
To get to those numbers, you have to be going after people that committed no other crime
other than crossing a line without a permission slip.
And you're going to have to go to people's homes to do it.
ICE doesn't want to do this because they're smart.
Their PR people have already explained to them, this is really bad.
Optics.
because we're going to see cell phone footage.
The American people are going to get to see what they've been
cheering on or turning a blind eye to.
They're going to see children crying as men in masks,
carrying guns, rip their family apart, literally, and drag
someone off to never be seen again, or removal.
It's a pretty clinical term for that, isn't it?
That's why they always prefer to do it at job sites, because
You don't have that.
You don't get to see it.
And when it's done at a job site, it's really easy to cast them as some plague that's upon
us because you don't really see their family.
They're not real people.
But now we're going to, and you're going to see their homes and their family life as it
is destroyed because they didn't get a permission slip.
Would you suggest this same punishment for people who went and do a movie without a ticket?
Or built something on their property without a building permit?
No?
Because you think of them as lesser.
You think of them as others.
These kids, if both parents are undocumented, but they were born here, they're going to
the boards of the state. If they were brought here when they were younger, they'll end up
in one of those camps where there are thousands of sexual abuse complaints. Man, real American
hero going door to door and snatching people. And it's funny because ISIS already said they
I can't really get millions.
They don't have the logistics for that.
They're gonna need to practice first
before they can set a goal that high.
And it cracks me up on one level
because the people that are cheering this on,
the one thing they have always feared
is the government actually developing the skill set
to go door to door and get something.
But you're gonna cheer it on when they practice
Because, well, it's not you, it's those others.
Those illegals, they're not real people, right?
You're going to see they're real people.
You're going to see it.
It's going to be on Facebook, and Twitter, and YouTube.
I hope you're proud of yourselves.
Not just those people that did it, but those people that
condoned it.
Well, they should have just followed the law. If that's really the only argument you can come up with,
if you can't come up with a moral argument to rip someone's family apart, completely destroy them, and their family, and
their friends, simply because they didn't get papers, well if that's the only argument is that it's illegal,
then we need to change the law. Amnesty, amnesty, amnesty.
the law. Amnesty. I'll say it, nobody else will. Amnesty. You've been here three years,
you haven't been convicted of a crime, you pass a background check, here you go. Happy
birthday. Welcome to America. Let's do it. It's better than destroying the lives of millions.
This is word, not mine, because they didn't get a permission slip.
It's unreal.
It is unreal.
The only other thing I want to point out is that apparently they're going to pull people
from HSI, Homeland Security Investigations, to go work removals.
HSI, they go after human traffickers, real human traffickers.
And now, for political reasons, they're going to be going after some guy who spends his
day swinging a hammer on a job site.
We've got our priorities mixed up.
We've got our morality mixed up.
If the only argument you have is that they're illegal or something that stems from them
being illegal, well, you know they don't pay taxes, they do, but that's beside the point.
If that's your argument, you know what would stop that?
Making them legal.
Let's just do it.
Let's help the tempest-tossed, the huddled masses.
yearning to breathe free. All those things that give you chills when they
play that song. What this country is supposed to embody when you look at that
flag. The idea of freedom. But apparently you don't even have the freedom to just
be here. The freest country in the world as long as you have government
permission. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}