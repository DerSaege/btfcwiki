---
title: Let's talk about the bigger question in the Bella Thorne scandal....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=nisumVzhGSs) |
| Published | 2019/06/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Bella Thorne's intimate photos were hacked and threatened to be released by a blackmailer.
- Thorne took control by posting the photos herself on Twitter.
- Whoopi Goldberg criticized Thorne, implying celebrities should expect hacking if they take such photos.
- Beau questions the victim-blaming mentality behind Goldberg's statement.
- He points out the unfair societal expectations placed on celebrities.
- The scandal around Thorne's photos stems from unrealistic expectations of public figures.
- Beau mentions the commonality of taking such photos, especially among young people.
- He criticizes the stigmatization of normal behavior when it comes to celebrities.
- There's a focus on the pressure for public figures to appear perfect all the time.
- Beau suggests removing the scandalization of leaked photos to combat blackmail and violations.

### Quotes

- "She's a victim of a crime."
- "This is extremely common behavior."
- "You're talking about consenting adults exchanging images of themselves."
- "She didn't do anything wrong, and she certainly didn't do anything wrong simply because of the way she was dressed."
- "Don't turn this into a scandal."

### Oneliner

Beau addresses the unfair stigma surrounding leaked intimate photos, advocating for a shift in societal attitudes towards privacy and normal behavior.

### Audience

Social media users

### On-the-ground actions from transcript

- Challenge societal norms around privacy and body autonomy (implied)
- Advocate for the destigmatization of common behaviors (implied)
- Support individuals affected by privacy violations (implied)

### Whats missing in summary

The full transcript dives deeper into societal expectations, privacy rights, and the need to combat victim-blaming mentalities.

### Tags

#Privacy #VictimBlaming #SocietalExpectations #Celebrities #Intimacy


## Transcript
Well howdy there internet people, it's Bo again.
So tonight we're going to talk about Bella Thorne, Whoopi Goldberg, I know that seems
weird for this channel, but there's a bigger question that I think is being missed in this
little scandal.
Now if you're not familiar with what happened, Bella Thorne apparently took some intimate
photos and they were stored on the cloud, she was hacked, the hacker threatened the
blackmailer with them.
So like a boss, she just put them on Twitter herself.
Then Whoopi Goldberg basically made the statement that if you're a celebrity in 2019, you should
know better than this.
You know, you're gonna get hacked and this is gonna happen if you put these kind of images
out there. You shouldn't take these types of photos. That's a troubling statement, and
I know right now somebody's saying, no, that's just, I mean, that's the way it is, the way
the world right now. Let me try to put this in another way. She's a victim of a crime.
Now if the hacker had taken photos of her at a birthday party, would this be said? No.
So when you actually dissect this statement, well, because of what she was wearing, she
was asking for it.
That's a pretty troubling statement, and that's what the main debate is over, but there is
a much bigger, much bigger question.
Before we get into it, I want to say that Ms. Thorne has every right to feel violated.
Her privacy was violated, she's a victim of a crime.
And nothing I'm about to say is meant to suggest she shouldn't feel violated.
But I don't feel that she should feel shame.
What's the motivation behind blackmail?
You're going to do what I say, I'm going to create a scandal, right?
Okay, now I've made a point not to look at the photos, but I had a journalist friend
who was a woman because somehow that felt less invasive look at them and just kind of
tell me what they were.
And they are the same photos that almost everybody under the age of 30 has taken.
Why is this a scandal?
This is extremely common behavior.
This is something that almost every young person does.
Yes, even your princess.
This is really common, but it's a scandal.
Millions of people did this today.
It's a scandal because we have unrealistic expectations of people who are in the public
eye.
We have unrealistic expectations of celebrities.
Their actions, when they do something that is completely normal, simply because they're
in the limelight, oh well it's a scandal, it's a little ridiculous.
I don't feel that simply because somebody is in films, or a musician, that they need
to lead a life without incident.
a little ridiculous and normal behavior gets stigmatized to to insane degrees at
times. I watched a TED talk by Jenna Hayes and if you're not familiar with her
she's an adult actress, was an adult actress and after she left that
industry, a major bank closed her account because they didn't want to be associated
with it.
In this day and age, when it's everywhere, when it is something that is extremely common
and used by almost everybody, there's still that stigma.
And that's the same thing that happened here.
Miss Thorne did something that was extremely common, and yet she's suffering some stigma
because of it.
She should have known better, whatever.
Those unrealistic expectations extend into beauty as well.
We expect those in the public eye to be perfect at all times.
And I'm not sure if this played into it in her case, but I had a friend who had her intimate
photos leaked by an ex.
And she felt violated and betrayed, as she should, but her main concern and the thing
that really bothered her was in one of the photos she had a love handle.
It's very telling about our society that something like that is the concern.
Because we have to have this Instagram image of perfect, right?
Everything has to be like that.
We have to have a fake life because it's all broadcast.
Everything's on social media.
So everybody becomes in the public eye and we compare ourselves to those perfect people
in Hollywood.
And maybe that's part of the reason why we enjoy these scandals so much.
Well they made a mistake too, it's okay.
There's nothing, there's no mistake here.
You didn't do anything wrong.
So how can we counter this?
You have to remove the reasoning behind blackmail,
and that it's going to cause a scandal.
When you do that, you remove the power,
you remove the motivation, the monetary motivation,
and then these hacks really don't happen as much.
So rather than turn each time a celebrity's nudes get late
leaked into some major event, instead, don't.
It's the human body.
You've probably taken nude photos.
No big deal.
You're talking about consenting adults exchanging images
of themselves.
This is not a big deal.
And if we can remove that scandal and treat these leaks the same way we would
if suddenly somebody said well you know Bella Thorne went to Walmart, the
motivation is gone and then those violations don't happen. Don't turn this
into a scandal. It's not. She didn't do anything wrong, and she certainly didn't do anything
wrong simply because of the way she was dressed. Anyway, it's just a thought. Y'all have a
good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}