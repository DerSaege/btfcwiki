---
title: Let's talk about Iran, lies, and confusion....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ASaT7RHA_Ow) |
| Published | 2019/06/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Predicts a storm coming and addresses the urgency of discussing Iran.
- Mentions the inevitability of war when discussed in a previous video.
- Questions the credibility of claims linking Iran to sponsoring Al-Qaeda.
- Criticizes the lack of believability in the narrative presented to Congress.
- Raises doubts about the U.S. Navy's claim of a drone being shot down in international waters.
- Reminds of past incidents like the U.S. shooting down Iran Air 655 without consequences.
- Rejects the idea of going to war over the recent drone incident.
- Calls out politicians, including Donald Trump, for using Iran as a tool for political gain.
- Challenges the idea of going to war for political motives at the expense of American soldiers and innocent Iranians.
- Ends with a thought-provoking message about not sacrificing lives for political gains.

### Quotes

- "You're not going to be able to fool anybody this time."
- "That accusation is a confession."
- "Get on those instead of standing on the graves of American troops and innocent Iranians."

### Oneliner

Beau predicts war, questions credibility, rejects war for political gain, and calls for valuing lives over politics.

### Audience

Concerned citizens

### On-the-ground actions from transcript

- Stand against war for political gain (implied)

### Whats missing in summary

Insights on the potential consequences of prioritizing political gains over human lives.

### Tags

#Iran #War #PoliticalGain #HumanRights #Credibility


## Transcript
Well, howdy there, internet people, it's Beau again.
There's a storm coming, so if it gets noisy, just bear with me.
We're going to talk about Iran real quick today, seems important.
Remember in that last video, I said when they push for war.
Well, we're here.
It wasn't a matter of if, it was when.
I think it's going to be harder to sell this war, because people
who are familiar with the region are not going to remain silent
complicit this time, I don't think. In that vein, I would like to start, and we will start
with what was told to Congress. Apparently, the regional Shia power is now sponsoring
a Sunni group. Al-Qaeda is getting its support from Iran, not from other Sunni nations like
Saudi Arabia and all the places they always have historically. They're getting it from
Iran, even though al-Qaeda is on the verge of embracing a Takfiri style of ideology,
which will lead them to kill Shias.
The Shias are now sponsoring it.
I understand you guys like to fancy yourselves as intelligence professionals who lie for
a living, but could you at least make the lies believable?
My 13-year-old could come up with a better story than that.
I understand that you're banking on the fact that Americans do not understand the difference
between the various types and schools of thought in Islam.
But we're going to tell them this time.
Let's talk about that drone.
Navy is saying it was shot down over the Straits of Hormuz
in international waters.
That's possible.
It's possible.
It is completely plausible that that is accurate.
There's some debate, but it's plausible.
The Straits of Hormuz are a confusing place.
It is completely within the realm of possibility that a military would mistake the location
of an aircraft or shoot one down that they shouldn't.
That's completely possible.
In fact, it's happened before when the U.S. blew Iran Air 655 out of the sky, killing
290 people, including 66 kids, and never even apologized for it.
That didn't start a war.
But now you want us to send people into harm's way because they blew Wally's flying cousin
out of the sky?
No.
Absolutely not.
It's ridiculous.
We know what this is about.
We understand what this is about.
You're not going to be able to fool anybody this time.
I'm going to read a tweet.
22nd of October 2012 from Donald J. Trump.
Don't let Obama play the Iran card in order to start a war, in order to get elected.
Be careful, Republicans.
Yeah, you don't get to do it either.
You don't get to do it either.
That's what this is about.
That accusation is a confession.
That's what this is about.
You want to get re-elected?
Why don't you stand on the value of your policies?
They're winning, right?
Get on those instead of standing on the graves of American troops and innocent Iranians.
You want to put the country on a wartime footing because you're going to get a bump in the
polls and you don't care if American soldiers die to get it.
Anyway it's just a thought.
and I'll have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}