---
title: Let's talk about a meeting at a burger joint, the scars of history, and today....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=naKy0o23uZU) |
| Published | 2019/06/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Witnessed an encounter at a fast-food joint involving an elderly Black man just released from prison, triggering reflections on societal dynamics and learned behaviors.
- Noticed the man's defensive tone, reminiscent of interactions with racists in the 60s, which saddened him.
- Offered the man a ride to his halfway house, assuming he was on his way to meet his probation officer.
- Realized the man had already been to the halfway house and was waiting for his probation officer.
- Learned the man had been incarcerated since the early 70s and was now in his 70s.
- Felt heartbroken realizing his own actions had triggered the defensive tone in the man due to past experiences.
- Reflected on societal norms and learned behaviors that persist despite the passage of time.
- Expresses anger at the continued mistreatment in ICE facilities, including sexual abuse complaints and inhumane conditions for children.
- Contemplates the long-lasting impact of current mistreatment on future generations' learned behaviors.
- Acknowledges the scars of history and expresses frustration at the perpetuation of such injustices.
- Questions the defense mechanisms and learned responses being instilled in today's youth that may resurface in the future.
- Considers the lasting effects of historical trauma and ongoing mistreatment.
- Expresses hope to never hear the defensive tone from the elderly man again.
- Ends with a thoughtful message wishing everyone a good night.

### Quotes

- "Learned behavior, something he learned as a kid, fell back on it immediately, 50 years later."
- "Sat there and watched the scars of history burst open in front of me."
- "Hope I never hear it again."

### Oneliner

Beau witnessed a poignant encounter with a recently released elderly Black man, leading to reflections on learned behaviors and societal injustices still prevalent today.

### Audience

Community members, activists.

### On-the-ground actions from transcript

- Support organizations working to improve conditions in ICE facilities (implied).
- Educate others on the systemic issues leading to mistreatment in detention facilities (implied).
- Advocate for humane treatment of all individuals in detention (implied).

### Whats missing in summary

The full transcript delves deeper into the lasting impact of historical trauma and the urgent need to address systemic injustices to prevent perpetuating harmful learned behaviors in society.

### Tags

#Injustice #SystemicIssues #LearnedBehaviors #ICEFacilities #Activism


## Transcript
Well, howdy there internet people, it's Bo again.
So today, well, tomorrow, or yesterday, by the time y'all watch this, I was at the fast
food joint and the person that was taking my order, she turned to her manager and she's
like, hey, you know that guy over there, he's been here a long time, he's acting really
weird and he keeps asking what time it is.
And I look over my shoulder, back at him.
It's an old black guy, old.
And he's in greens, he's in institutional greens.
He just got out of prison, like recently.
And I tell the manager, I'm like, I'm going to talk to him.
And I walk over there.
I'm like, hey, what time are you doing OK?
And he uses a tone of voice I'd never
heard before in real life and he's like I need to leave I'm not trying to cause
any trouble boss and it's that total voice you hear in these movies where you
know the black guy runs into the racist back in the 60s and he that was the tone
of voice you used. You know, casting that, you know, I'm one of the dumb good ones.
I sat down with him and I'm like, nah man, I just recognize the clothes and you're headed
to the halfway house. You need a ride. And basically when you get out, a lot of times
you'll take a bus and then take a cab to the halfway house. That's the way it works.
and a lot of people will take a bus part of the way and then have their friend
pick them up and drive them the rest of the way. However if you do not make it to
that halfway house on time, oh wow, you're in a world of trouble. That's what I
thought was going on. Now as it turns out he's already been and he's there
waiting for his first meeting with his probation officer or whatever and I
sit there and I talk to him I find out he he went in in the early 70s and he
just got out he's 70 something years old and as we talk I noticed that that tone
the voice is gone. And it just broke my heart. It confirmed that something about me had elicited
that response in him. It brought it about. And you know, yeah, to us it's 2019. He's
been gone for 50 years. 50 years ago in this area, somebody looks like me comes up to you,
guess it's better to play it safe. Learned behavior, something he learned as a kid, fell
back on it immediately, 50 years later. It upset me. Those people that know me know I
I don't get upset.
It's very hard to surprise me or shock me or whatever this did.
The more I thought about it, the angrier I got for a whole bunch of different reasons.
But the most contributing factor to it is that it still happened differently, in a different
but it's still happening today. The same type of thing that caused that behavior
in him. They gave him that defense mechanism. You know, they did that
inspection of those ICE facilities. Rotten food, nooses in cells, throwing
people in solitary for no good reason, still have not found the time to
investigate the thousands of sexual abuse complaints. Now, from the kids, we're
taking away soccer and school. Yeah, treating them like they're not people.
That's what we're doing. And I got to wonder what the learned response is, what
What the learned behavior is, what defense mechanisms are they having that they're developing
right now that 50 years from now they're going to fall back on in the blink of an eye.
Sat there and watched the scars of history burst open in front of me.
But we're still doing it.
tone of voice. It's a cliche in a movie. Hope I never hear it again. Anyway, it's
just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}