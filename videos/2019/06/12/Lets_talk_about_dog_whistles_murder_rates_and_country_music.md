---
title: Let's talk about dog whistles, murder rates, and country music....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=vbUbEnm-mh4) |
| Published | 2019/06/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of dog whistles, which are statements with hidden meanings that give the speaker plausible deniability.
- Dog whistles are used to convey messages to a specific group while appearing innocent to others.
- Beau admits to using dog whistles in his videos, not necessarily racist ones, but to communicate with certain groups.
- Talks about how to identify and respond to dog whistles by focusing solely on the hidden meaning and not letting the speaker divert the attention.
- Mentions examples like the statement "if no government is best, then maybe no government is best" as a dog whistle for those who believe in a stateless society.
- Emphasizes the importance of dismantling these hidden messages to expose the true intentions behind them.
- Compares the use of dog whistles to racist jokes and suggests challenging the speaker to explain the hidden meaning behind their statements.
- Provides examples like statistics on crime rates among different groups to illustrate how dog whistles work.
- Encourages engaging with dog whistles to prevent others from being influenced by harmful ideologies.
- Stresses the need to focus on the core message of dog whistles to dismantle the underlying beliefs.
- Beau delves into the nuances of crime statistics and challenges stereotypes associated with criminal behavior.
- Talks about the prevalence of dog whistles in culture, including music, and how they perpetuate harmful stereotypes.
- Urges readers to confront dog whistles and dismantle the pillars of harmful ideologies.
- Beau concludes by encouraging active engagement with dog whistles to combat harmful beliefs in society.

### Quotes

- "You continually hammer on that one statement and get them to address that one thing."
- "You destroy those pillars, those things that hold up the rest of the ideology, those things that are so commonly believed that everybody can recognize them."
- "Engage those dog whistles every chance you get and you focus on that dog whistle."

### Oneliner

Beau explains dog whistles, urges dismantling hidden meanings to combat harmful ideologies, and advocates focusing solely on exposing the core messages behind them.

### Audience

Online activists, community organizers

### On-the-ground actions from transcript

- Challenge dog whistles by focusing solely on dismantling the hidden messages (suggested).
- Engage actively with dog whistles to prevent harmful ideologies from spreading (suggested).
- Address core messages behind dog whistles to expose harmful beliefs in society (suggested).

### Whats missing in summary

Beau's detailed breakdown of dog whistles and strategies for combating harmful ideologies through active engagement.

### Tags

#DogWhistles #CombatIdeologies #OnlineActivism #CommunityOrganizing #ChallengingBeliefs


## Transcript
Well, howdy there, internet people.
It's Bo again.
So tonight, we're going to talk about dog whistles.
We're running into them a lot all over the place, certainly
on the internet.
They're even in government.
And government seems powerless to stop it, any kind.
I mean, if no government is best, then, well, maybe no
government is best.
Anyway, dog whistles are statements that give the
speaker plausible deniability.
They can say, oh, no, no, no.
That's not what I meant.
That is not what I meant.
I didn't mean that.
You're taking that the wrong way.
And then they start to gaslight you.
Look at you.
You're seeing racist everywhere.
That's not what I was saying.
It's crazy.
But even though they can deny it to a certain subset of people,
well, they heard it.
And they know what it means.
Because it's a core belief of that ideology.
something that really, really matters to that group.
I use dog whistles all the time in my videos, constantly.
I don't use racist dog whistles, but I use dog whistles
to other groups.
I used one already in this video.
Now, when the average person hears,
if no government is best, then no government is best,
what they hear is, well, there's no perfect government, right?
To a certain subset of people, if no government
best, well then no government is best. It's a dog whistle to people who believe
in a stateless society because it's better not to have a government. That's
how dog whistles work. Odds are you didn't even hear that, but it was there
and there were people in these comment sections, I'm certain, that heard it.
That's how they work. So first you have to be able to identify them and some of
them are very very obvious and we're going to use some of those as examples
but you treat them the same way you treat a racist joke I don't get it explain
it to me and force them to drag it out and explain it you focus on nothing
else in that comment when you see one or you hear one you don't address anything
else you zero in on that dog whistle I don't know what is a globalist because
Because then they have only a few options.
They can pretend they don't know, which nobody on the internet admits they don't know something,
or they can come up with an alternate meaning, or they can own it.
Those are the options.
But either way, it doesn't matter because you don't let it go.
You continually hammer on that one statement and get them to address that one thing.
A really common one is, well you know black people are responsible for a disproportionate
amount of murders in the U.S.
Okay so why is that important?
Well I just said that they're responsible.
So their skin tone makes them kill?
Is that what you're saying?
You just force it, drag it out.
I mean that doesn't really make sense, I mean unless you're actually saying that black people
We're subhuman.
I mean, that's certainly not what you're saying, right?
And by this point, they can't gaslight you anymore.
They can't pretend that's not what they're talking about
because they've already hit that.
They've already gone too far.
You can expose them.
And then you can expose the propaganda
because most of these dog whistles are like that.
There's no nuance.
They're very simple pillars, though.
There's something that everybody in that community recognizes, and if you can destroy it, well
then not the person you're talking to, but everybody else in the comments section sees
it.
Everybody who heard that dog whistle, who might be starting to believe that, can start
to identify for the bunk that it is.
You know by that same data set, people with brown eyes are responsible for a disproportionate
amount of murder.
Should we base policy on that?
that a reason to develop a prejudice? It's a spurious correlation. Obviously, skin tone
or eye color doesn't relate to whether or not you're more likely to be a murderer. That's
stupid. That's like saying your astrological sign determines whether or not you're going
to be a killer. So you drag that out. And then you go further with it. And you point
that you know I mean if you're going to use statistics you at least need a
complete data set and this is a complete data set it came from the Department of
Justice yeah but what's it what's what are the statistics of cleared murder
cases not all murder cases so it actually stand to reason that the people
that are better at crime are the people that aren't on it because well it only
only focuses on a very small group, focuses on those that were cleared, those cases.
And get ready to not sleep at night.
I know that because of CSI and shows like that, everybody thinks that the clearance
rate for murders is really high.
In reality, it's like 50 to 60 percent, 40 to 50 percent of murders go unsolved.
But it's even worse than that, really, because that only includes murders where an investigation
started, if the person just became a missing person, it's not figured in at all.
Let me ask you this, going by stereotypes, who is most likely to be able to make a body
disappear?
What do they look like?
Where do they live?
What do they sound like?
Me, right?
I mean, let's be honest, hillbillies, rednecks, I mean, that is what it is.
Those are the people that are most likely to be able to make a body just disappear.
And you know, when you bring this up, what you get is, well, you know, you're saying
all of this, but it's in their culture, it's in their music.
That's the one that always follows it up.
Really.
You know, I learned a thing or two from Charlie, don't you know?
You better stay away from Copperhead Road.
Steve Earle, now granted, he is outlaw country, this is not mainstream.
That song is about booby-trapping pot-builds to kill DEA agents, that's really what it's
about, so you can't really count him though, because he's not really mainstream, but what
about the lawman said he found some tracks, he went up there looking and he never come
back.
That's Montgomery Gentry, pretty mainstream, not just is it talking about murdering somebody,
it's talking about killing a cop and disposing of the body so it's not found.
criminality there it is it's throughout country music with you in that tarp
because Earl had to die mainstream now I know country music disowned them
because they spoke out against a Republican but it was there just take
him to the knee or take him to the swamp knock or take that rascal out the swamp
knock him to their knees and tie him to a stump let the rattlers and the bugs
and the alligators do the rest, it does not get more mainstream than Charlie Daniels.
Murder and disposing of the body.
I wonder where that Louisiana sheriff went to. Jerry Reed, murder of a cop and
disposing of the body. It's a pretty common theme actually, but I guess it's
just in our culture, right? And I know right now there's a whole lot of rednecks
and hillbillies out there going, well, you know, in almost all those cases, that was
really justified.
I mean, they really, I mean, these people had it coming.
That's literally the point.
So dog whistles, in general, can be defeated.
You just have to know what they are.
And then you ignore everything else they say and you focus on that.
Because you're not trying to convince that person.
gets their mind changed in an Internet debate. But the people reading it do. The other people
do. You might be able to reach one of them. You might be able to save one of them from
going down some racist road. Engage those dog whiffles every chance you get and you
forget about everything else in that comment. It doesn't matter what they call you, ignore
it. Just focus on that dog whistle. You destroy those pillars, those things that hold up the
rest of the ideology, those things that are so commonly believed that everybody
can recognize them. You destroy those and that ideology comes falling down. Anyway
It's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}