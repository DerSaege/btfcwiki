---
title: Let's talk about the 300 kids....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=aJ_5SO1VR_w) |
| Published | 2019/06/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- 300 children were removed from a Border Patrol facility in Clint, Texas, designed for 100 people but housed 350.
- Visitors described the facility as having a stench.
- The children lacked adequate food, hygiene items like soap and toothpaste, and hadn't showered in almost a month.
- Their clothing had bodily fluids ranging from breast milk to urine.
- Many children don't know the whereabouts of their families.
- Flu outbreaks were present, and there was a lack of beds.
- Border Patrol had younger children taking care of toddlers.
- The children have not been rescued; they were moved to another facility, which Beau calls a "Trump concentration camp."
- Deaths in concentration camps were often due to disease and lack of food caused by poor hygiene.
- Beau questions when people will call their senators and address the crisis.
- Children are being treated like livestock, not as human beings.
- The situation is so dire that it's no longer shocking, which Beau finds tragic.
- The media's portrayal of the children being "removed" is misleading; they remain in the custody of the same agency that neglected them.
- These children are seen as numbers on a board, not as individuals facing a humanitarian crisis.

### Quotes

- "Do you want to wait until we have a bunch of deaths?"
- "This is a travesty. This is ridiculous."
- "They're livestock. They're a product."
- "This is not even surprising anymore."
- "They have no idea where their family is."

### Oneliner

300 children removed from overcrowded Border Patrol facility to another, facing neglect and inhumane conditions, a crisis overlooked on American soil.

### Audience

Advocates for human rights

### On-the-ground actions from transcript

- Contact your senator to demand immediate action to address the inhumane conditions faced by children in Border Patrol facilities (suggested)
- Advocate for policy changes to ensure proper care and treatment of detained children (implied)
- Organize or participate in protests to raise awareness about the crisis and pressure authorities for change (implied)

### Whats missing in summary

The full transcript provides a detailed and emotional account of the appalling conditions faced by children in Border Patrol facilities, urging immediate action and shedding light on the dehumanizing treatment they endure.

### Tags

#BorderPatrol #HumanRights #Children #InhumaneConditions #Crisis


## Transcript
Well, howdy there, internet people, it's Bo again.
So we've got to talk about the 300.
The 300 children that were just removed from a Border Patrol facility in Clint, Texas that
was designed to house 100 people had 350 in it.
The one word that everybody who visited used to describe it was stench.
The children present had inadequate food, no access to hygiene items like soap and toothpaste.
Many had not taken a shower in almost a month.
Their clothing had bodily fluids on it ranging from breast milk to urine.
They don't know where their family is.
There's flu outbreaks, they don't have beds, Border Patrol has apparently entrusted seven
and eight-year-olds with taking care of two-year-olds.
And we wonder why we have so many in custody deaths.
The media, the headlines are saying that they were removed, making it sound like it's over
like they've been rescued from American law enforcement. They weren't removed.
They're still in the custody of the same agency that allowed this, that permitted
this, that encouraged this to happen to them. They just moved them to a different
facility. Another tent city, another Trump concentration camp. Yes, that's what they
are. Many of the people who died in the concentration camps that everybody
thinks of when you say that term, they died of disease and a lack of food.
Disease brought on from a lack of hygiene.
How long do you want to wait until you call your senator?
Do you want to wait until we have a bunch of deaths?
We have concentration camps operating on American soil.
What's worse is that this isn't even a shock anymore.
It's not.
These children are packed in their livestock because that's what they are at this point.
They're not people.
They're livestock.
They're a product.
So they can be shown as numbers on a board to show what, how huge the crisis is, a crisis
of his own making.
Can you imagine what would happen if a daycare facility was over capacity by three and a
half times?
The kids didn't have enough to eat and they were covered in bodily fluids and hadn't
showered in a month.
Do you think just following orders would prevent those day care providers from going to prison?
Of course not, you shouldn't hear either.
This is a travesty.
This is ridiculous.
And the sad part is, it's not even surprising anymore.
And the media, they were removed.
They weren't.
The same people that did this to them still have power over life and death for them.
They have no idea where their family is.
I don't know what it's going to take to make people realize the road we're on.
But if this doesn't do it, nothing will.
Anyway, it's just a thought.
y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}