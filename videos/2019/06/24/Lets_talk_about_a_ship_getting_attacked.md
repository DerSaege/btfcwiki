---
title: Let's talk about a ship getting attacked....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=4FRpZZPcQfs) |
| Published | 2019/06/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Tonight, I'm sharing links on a topic most Americans might not know or believe.
- The U.S. Senate repealed the Gulf of Tonkin Resolution on June 24, 1970.
- This resolution authorized the Vietnam War based on a false incident.
- The Gulf of Tonkin incident was used to justify the war, but the truth was different.
- Operation De Soto and Opland 34A were running simultaneously in Vietnam.
- The North Vietnamese attack on the Maddox was a response to a previous US action.
- Fabricated evidence was used to justify the Vietnam War.
- Many lives were lost based on this lie.
- The war monument stands as a reminder of the cost of this deception.
- Similar pretexts for war have been sought since then, like tying Iran to al-Qaeda post-9/11.

### Quotes

- "The worst war in American history was fought over a lie."
- "All of those graves were filled over a lie."
- "The Vietnam War was based on a lie."

### Oneliner

Beau reveals the truth behind the Vietnam War, a conflict built on deception and lies, leading to immense loss of life and a stark reminder of the consequences of manufactured narratives.

### Audience

History buffs, truth-seekers, activists

### On-the-ground actions from transcript

- Share this history lesson with others (suggested)
- Educate yourself and others on the truths behind past conflicts (implied)
  
### Whats missing in summary

Delve deeper into the deceptive narratives of past conflicts and the importance of seeking the truth to avoid similar tragedies in the future.

### Tags

#VietnamWar #Deception #Truth #HistoryLessons #Activism


## Transcript
Well, howdy there internet people, it's Beau again.
So tonight's going to be one of the few nights
you're going to find links in the description.
Um, the topic tonight is something I don't think most
Americans know, and I don't think most Americans are going
to want to believe, so I'm going to provide links
when I normally don't.
Um, and the links will not be to wild websites,
they will be to the U.S. Naval Institute, places like that.
Places that would actually have a vested interest in saying what I'm about to say is a lie,
but they're not going to.
They're going to tell you it's true.
So in 1970, on June 24th, that's today, the Senate rescinded, repealed the Gulf of Tonkin
Resolution.
The Gulf of Tonkin Resolution was the authorization for the Vietnam War, basically, didn't do
any good for them to repeal it, but it was a nice symbolic gesture.
Okay, so this resolution was enacted because of the Gulf of Tonkin incident.
Most people, most Americans understand the North Vietnamese attacked one of our destroyers.
We had no choice but to respond.
Okay, so this is what really happened.
Operation De Soto was running. I think in the beginning it was minesweepers, but in
early 1963, U.S. destroyers started running these
routes, and it was shipborne spying. They were gathering
intelligence on the North Vietnamese from these ships, running up and down
the coast.
At the same time,
The DOD and the CIA were running something called Opland 34A, it was based out of Da Nang,
it was a bunch of SEALs, Marine Corps guys and some civilians who were training South
Vietnamese troops in unconventional warfare, putting them on ships or little boats that
DOD had given them, sending them north to North Vietnam, having them insert and do commando
style raids.
The insertions didn't go very well.
The guys kept getting killed or captured or turned.
So they started just keeping it right near the shore.
These two things were occurring at the same time
in the same area.
Although it is not in any of the documents that have been
declassified so far, DOD certainly should have
understood that if there's a US destroyer present and then
there's an attack, the North Vietnamese are going to link the two, especially if it happens
more than once.
On August 2nd, the Maddox was attacked.
The day before, Opland 34A blew up a radio transmitter.
It was a response to that attack.
That's why the North Vietnamese attacked.
That alone wasn't justification for war, though.
They needed another one, and they got it on August 4th.
And this was a crazy, crazy incident.
The Maddox expended hundreds of rounds during this battle against Fish.
They were shooting at sonar echoes.
There never was a second attack, didn't exist, and the administration knew it.
The NSA actually helped them fabricate evidence to support the idea that there was an attack,
though there wasn't.
That was the justification for the Vietnam War, something that never happened.
When senior members of an administration attempt to justify a war that just doesn't seem right
and doesn't seem to make sense, pay attention.
This is one that is completely verifiable that they knew they were being less than truthful
about.
It killed tens of thousands of Americans and hundreds of thousands if not millions of Vietnamese.
This is all declassified.
It's not a secret anymore.
The Vietnam War was based on a lie.
All of those graves were filled over a lie.
That monument, that wall, is there because of a lie and because, well, Americans chose
to support the troops rather than support the truth.
Next time you watch Platoon, Full Metal Jacket, Apocalypse Now, whatever, remember that none
None of it had to have happened.
And we run across this all the time now as administrations search for pretexts for war.
That was the important thing about tying Iran to al-Qaeda.
The authorization for the use of military force that came out after 9-11 authorized
attacks against any state
that was helping al-qaeda
so if they could tie them to that
then they wouldn't need any more
approval
they could just roll with it
that's why it was so important
that's why they did it
it's just
it's something that we need to keep in mind
it is something that we definitely need to be aware of
because it hasn't stopped.
Since Vietnam, this has happened again and again.
Now, we don't have the declassified documents
to prove it yet.
But just like in this case, they'll show up.
And it'll be so long that those that were affected by it,
those who lost their friends, well, they're too old.
nobody cares about them anymore. It's ancient history. The worst war in American history
was fought over a lie. It was made up. And it's admitted now that they manufactured it.
As we move through the next year or so, I think that's going to be really important
remember. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}