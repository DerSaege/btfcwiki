---
title: Let's talk about the censorship of hate speech....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=cLEZrTmkixY) |
| Published | 2019/06/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Expresses support for YouTube's efforts to clean up its platform by censoring hate speech.
- Defines hate speech as advocating for violence, discrimination, or negative actions against a group.
- Emphasizes that freedom of speech protects from the government, not private companies.
- Encourages individuals to build their own platforms if they disagree with censorship policies.
- Acknowledges mistakes made by YouTube in banning channels, including a history professor and an organization called News to Share.
- Defends the value of journalism like that done by Ford Fisher, documenting extremist activities and protests.
- Argues that documenting extremist activities can help identify individuals inciting violence versus those exploiting the situation for financial gain.
- Points out the importance of video evidence in intelligence work to determine intent and identify perpetrators.
- Stresses the value of News to Share's journalism, even though mistakes were made in its censorship.
- Mentions Ben Shapiro's support for Ford Fisher and the importance of Fisher's work in journalism.

### Quotes

- "You want to be a bigot? You got a right to be a bigot. The second you advocate for violence, discrimination, or some kind of negative thing to be visited upon whoever it is you're bigoted against, well then that's a problem."
- "Freedom of speech protects you from the government, not from a private company and not from public outcry."
- "Sunlight is a really good disinfectant. It makes it easier to debunk."
- "The market has spoken. We don't want you."
- "There are people that don't have access to your stuff. Video evidence of those speeches provides intent."

### Oneliner

Beau supports YouTube's censorship of hate speech, encourages building alternative platforms, defends valuable journalism, and acknowledges mistakes in the process.

### Audience

Content creators

### On-the-ground actions from transcript

- Contact Ford Fisher to offer support and solidarity (implied)
- Support independent journalism that exposes extremist activities (implied)

### Whats missing in summary

The full transcript provides a nuanced perspective on censorship, the importance of independent journalism, and the need to balance free speech with responsible platform management.

### Tags

#Censorship #HateSpeech #Journalism #FreedomOfSpeech #YouTube


## Transcript
Well, howdy there, internet people, it's Bo again.
So I guess we gotta talk about the censorship of hate speech.
OK, so to start off with, I'm gonna give you a disclosure notice.
I'm an avid, rabid anti-racist, so I'm gonna be a little biased here.
Generally, in broad terms, I support what YouTube did.
I understand why they're doing it.
They're trying to clean up their platform.
I get that.
I especially understand it because you can go to the comments section of videos on my channel
and you'll see this is a breath of fresh air, you know, in comparison to the cesspool that YouTube
has become. I can see them wanting to make a change. So the question is, when does something
become hate speech? When does it become dangerous? When should it be censored? That's the question.
question. To me the line is simple. You want to be a bigot? You got a right to be a bigot.
You do. The second you advocate for violence, discrimination, or some kind of negative thing
to be visited upon whoever it is you're bigoted against, well then that's a problem. That's
the line. You know, you can think whatever you want. You can talk about whatever you
want but the second you start wanting to encourage others to hurt people well
that changes things that changes things and I don't care how veiled it is when
you say well we need a leader leaderless resistance historians know what that
means that's assassinations and church bombings yeah you need to get off the
platform. So I don't have time to be in the comment section tonight. I'm going to
go ahead and get ahead of some of the arguments that are going to pop
up. Will I have a First Amendment right? You do. You do. That protects you from
the government, not from a private company and not from public outcry. This
is censorship and I have freedom of speech. You do have freedom of speech but
Nobody has to platform you.
I'm an editor.
I have my own platforms.
I don't allow racist stuff on mine.
I can't hold YouTube to a higher standard.
Go build your own platform.
Well, they took out things that weren't racist.
They took out stuff about conspiracy
theories and all of this stuff.
Yeah, that's a gray area to me.
But it's not my platform.
In my opinion, that stuff should be out there.
Sunlight is a really good disinfectant.
It makes it easier to debunk.
And I also like it because it encourages people
to become interested in history.
Yeah, they start off in a bad place a lot of times.
But then they find out some truth along the way.
And then you just have to get rid of the junk they picked up.
Um, a lot of these guys are ardent capitalists
that are complaining about this move.
Gentlemen, the market has spoken.
The market has spoken.
It's that simple.
We don't want you.
Well it could happen to you, it could at any time.
It's their platform, they have that ability.
It's theirs, they own it.
Their responsibility is to the shareholders, that's it.
That's why maybe large corporations really aren't such a great thing in a lot of ways.
But in this case, I don't really have a whole lot of problem
with what they did.
But they made mistakes.
They banned a history professor.
How can you?
Yeah, I know.
They did.
They made mistakes.
They banned thousands of channels.
They hit thousands of channels in this thing.
They made mistakes.
If we take all of the ones that we know about, all the
mistakes that we know about, multiply it by 10, what do you got, 200? YouTube was more
accurate with their deletions and demonetization than police are with their gunfire. I mean,
I am certain that they will roll out a patch, they will fix this, there are bugs. But let
me tell you about one of the channels they did take out. They took out an organization,
demonetized an organization called News to Share.
Go over there.
It is hours upon hours of Nazis speaking, Klansmen speaking, alt-right rallies.
That's like 60% of the content on that thing.
It's ran by a guy named Ford Fisher.
He's the front man.
We're drinking buddies.
I'm an avid anti-racist.
Yeah, they made a mistake.
They did, and they'll fix it, I'm sure.
Um, there are different kinds of journalism.
What you're watching right now is a gonzo journalism.
Um, Ford, he's a documentarian.
He documents extremist activity
and protests in the United States.
That's what he does.
Yeah, there's gonna be a whole lot of Nazis
alt-right guys on his channel. I know some people are saying, well he's
platforming them. You know, he's doing this. He's getting their ideas out there.
I understand that. I do. I really do. And you'll definitely come to that conclusion
if you go to his comment section. There's a lot of racism in it. So much so that I
don't go there anymore and I'm a Patreon supporter of them. I understand that.
However, to those who would say he's platforming them and that he's helping them, the question
I have is how many violent Nazis or Klansmen have you helped put behind bars?
I guarantee you Ford Fisher's number is higher.
You remember when that goon drove his car into the crowd?
who's footage was instrumental in figuring that out. Guy opening fire on the crowd? Yeah.
And I know that a lot of the big complaint I get from a lot of the anti-fascists that
I know is that, well, you know, we have our own people who do, you know, who go to these
things and photograph and videotape it, and we don't have to put it out there for public
consumption, you know, and we can dox them and yeah, I get it. You can do all your intelligence work from your from your
stuff.
However, there are people that don't have access to your stuff.
And those speeches,
yeah, still photographs. They can provide identification.
Video evidence of those speeches provides intent.
That's what
intelligence work is really about.
You got to know who the guys are who are actually trying to
incite violence and the guys who are just there to to rip off the racists
because that's a large portion of them a lot of these guys don't believe their
own stuff they're just there to make money and his videos can really help
determine who's who and so yeah they made mistakes and I'm sure they will
fix it and and for the news to share is a just a brilliant shining example of
but it's the one time you're gonna find me and Ben Shapiro
on the same side.
Because I guess he's taken up for him too.
There are a lot of outlets that have used their material.
I mean, seriously, only one of the outlets
that I've written for and I have written for a lot
has not used their stuff.
It's extremely valuable journalism.
Now, to forward, because I'm sure you're gonna watch this,
two things.
One, come pick up your emergency car kit.
Two, quit your belly aching.
I read about you in Rolling Stone today.
You can't buy publicity like this.
Anyway guys, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}