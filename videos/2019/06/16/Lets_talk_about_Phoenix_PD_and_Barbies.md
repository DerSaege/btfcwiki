---
title: Let's talk about Phoenix PD and Barbies....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=rN1wEwOHkAA) |
| Published | 2019/06/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Phoenix PD released a fact sheet light on facts, attempting to justify their actions with no use of force mentioned, despite contradicting evidence in original reports.
- The credibility of the Phoenix PD is questioned, given the discrepancies in their statements.
- Even if the facts were true, the situation worsens due to the presence of children during the incident.
- Beau questions the justification for excessive force and violence by the Phoenix PD, even in hypothetical scenarios like shoplifting.
- The core issues lie in the lack of proper training and tactics displayed by the officers involved.
- Failure to follow standard procedures like time, distance, and cover, and conflicting commands led to a chaotic and dangerous situation.
- The officers' actions instilled fear and panic unnecessarily, leading to a traumatic experience for the individuals involved.
- Beau criticizes the lack of de-escalation attempts and the absence of body cameras, which are vital for accountability and learning from such incidents.
- The safety of civilians, especially children, was compromised by the actions of the Phoenix PD.
- Beau calls for accountability within the department, suggesting that all officers on scene should be removed due to negligence and lack of accurate reporting.

### Quotes

- "You threaten to kill a mother in front of her kids. Yeah. Nobody cares."
- "If you drag this out it's just going to get worse."
- "Your credibility is already shot. You need to make your decision on what you're going to do."
- "You're going to get somebody killed."
- "You have the same propensity for violence as an addict looking for a fix."

### Oneliner

Phoenix PD's credibility is questioned as Beau exposes discrepancies, lack of training, and accountability in a troubling incident involving excessive force.

### Audience

Community members, activists

### On-the-ground actions from transcript

- Demand accountability from Phoenix PD (suggested)
- Advocate for policy changes regarding the use of force (exemplified)
- Support civilians standing against police misconduct (implied)

### Whats missing in summary

Beau's emotional delivery and detailed breakdown of the incident can best be experienced in the full transcript.

### Tags

#PoliceAccountability #ExcessiveForce #CommunityAction #Training #BodyCameras


## Transcript
Well, howdy there, internet people.
It's Bo again.
So we're going to talk about Phoenix.
We're going to hit the things that most people are talking
about in a matter of seconds.
And then we're going to get to the meat of this issue.
Because there's a lot of things that people are not
talking about that they need to be talking about.
OK, so Phoenix PD released their fact sheet, which was
really light on facts.
if you were to just look at their fact sheet, there was no force used.
More importantly, why would we believe this?
After what we saw, and what was apparently in the original reports, why would we believe
anything that comes out of that department?
The credibility of that department is ruined.
It is gone.
But let's just say, for the sake of argument, that it's all true.
It makes things worse, not better.
Okay so first you have to address the fact that they made that first stop.
At that point they should have known, because the first question, who's in the car?
So they knew there were kids present when all of this started.
That makes all of their other actions that much more reprehensible.
Now, let's say, they did, they shoplifted some stuff, let's say that happened, even
though there's no charges, let's just say that happened, I'm gonna pop a cap in you
over this, is it worth it, does that make sense, congratulations Phoenix PD, you have
the same propensity for violence as an addict looking for a fix. Good job. And you're acting
as though that might justify it. It doesn't. It makes it worse.
Okay now to the meat of the issues. Homework. Right now, below there's a video, let's talk
about a cop asking me for training, watch it and then watch the videos of this incident
again.
Try to count how many of the points that I talked about that create bad shoots, that
create excessive force, that happened in the video.
It's a textbook, okay so you've watched it, alright welcome back.
Time, distance and cover was not used and it should have been, especially with the knowledge
the kids were present, made it that much more important. They didn't. They start
skiing and hutched it right up to the car. There was one guy who used cover. At that
point, if you become in fear for your life of a crying woman holding kids, well
that's on you because you put yourself in that situation. This isn't new stuff.
This has been around for more than a decade. One person gives commands. Not
Not there though, not in Phoenix.
Put your hands up, get out of the car.
Which one gets her shot?
Which one are you going to kill her over and say, well she wasn't following our commands?
You give conflicting commands and people die as far as I'm concerned.
If that happens, that should be murder.
This has been known long enough that every department should be acting on this.
Auditory exclusion.
I didn't think I needed to cover the fact that threatening to kill someone and bust
a cap in them would be a traumatic experience and could trigger auditory exclusion.
At that point, you can't say you're not complying.
Once you've done this, once you have put them in that state of mind, auditory exclusion
is a possibility.
So from that point forward, it's on you.
on you. And from the video, even though the officer is saying you're not complying, it
certainly appears that he was. Became afraid of their own shadows because they put themselves
in a situation they didn't need to be in. They have bought into the propaganda that
apparently a shoplifter is going to kill them. And you can hear it. Anybody who has ever
been in one of those situations, when they're listening to those videos, they hear it.
They hear panic, the inability to self help.
You can hear it in the voices.
If you are that afraid of a situation like this, and you cannot control a situation like
this, you're in the wrong job.
You're going to get yourself or an innocent killed.
Not a bad guy.
It's not going to happen.
It's going to be you, an innocent, your partner, it's not going to be the person that needs
to go.
Something that I think is definitely being missed is the ideologically motivated thing.
One of the people standing there, somebody filming, you could hear them.
When it started to get really crazy, you hear them, hey, hey, hey.
How long until that becomes pop, pop, pop?
When you behave like this, you turn the populace against you.
Your actions become an officer safety issue.
You're going to get somebody killed.
You're going to get somebody killed.
When you have civilians that are fighting not to intervene, even though they're surrounded
by officers, you're out of line.
That's what we call an indicator.
We write the report, right?
According to what's coming out, there was no use of force.
Seems like there was in the videos.
None of this was in the reports because we write the report.
There was no de-escalation attempted, any of the videos from any of those angles, none.
That's not actually covered because it's the modern era and I didn't think that was necessary.
And then there's no body cameras used.
Now all this is tactical and people are saying no, body cameras are a policy issue.
It's also tactical.
It is also a tactical issue because when a situation like this arises, you can go back
and you can look at all of the things that went wrong and in this case, a lot of things
went wrong.
You can hot wash it, after action it, you can stop it from happening again, unless of
course, you know, you don't comply with the regulations.
Didn't comply.
What happens to civilians when they don't comply?
I don't think the people that are demanding these officers be fired are asking too much.
to policy. Use of force. If that, I'm not familiar with Phoenix's use of force
policy. I'm not familiar with it. If this is within it, you need a change of
policy. You need a change of policy. Period. Period. The no body cameras thing
obviously. Because that allows them to just, well we write the report. There were
omissions, apparently. I haven't been able to see them, but according to everybody, there
were things that were left out, and it certainly seems that way. Again, we write the report,
right? But now your credibility is gone. And then there's one other thing that is very
important that I think is being missed, those kids. When they got handed over. Now,
personally, I believe the people that took those kids, I think you're heroes,
and I don't use that word lightly. I think you may have saved lives in more
ways than one. At the same time, it certainly appears from the conversation
on the film that they didn't know each other woman was so terrified that she
handed her kids over to somebody a stranger or somebody she barely knew
didn't know what apartment she was in. I've seen that before. Congratulations
Phoenix PD you're on you're on par with death squads. Now if something had
happen to those kids, who's to blame? Phoenix PD. And then there's the big
issue. The brass is acting like they found out about this via the videos. If
this is my department, everybody on scene, every officer on scene is gone because
Because that means nobody put paper on these guys.
Nobody put paper on the perpetrators.
Nobody filed a report.
Nobody put accurate information in the report.
Now, to me, they're gone.
That's what should happen.
If it was my department, that's what I would do.
I'm not sure how you can get around that,
because now every defense attorney in the city
is going to try to get their names.
And then from that point forward, no report.
they write is gonna be worth anything. Isn't it true that you omitted facts in
this report? So how can we trust this one?
The situation was horrible and the fact that the department is trying to
justify it over the fact that they maybe were shoplifting or maybe had a
suspended license. Nobody cares. You threaten to kill a mother in front of her kids.
Yeah. Nobody cares. And this is in a department that has systemic issues. The
longer this drags out, the worse it's going to be. The department needs to make
it's decisions and let the chips fall where they may. If you drag this out it's
just going to get worse. Your credibility is already shot. You need to make your
decision on what you're going to do. And it really should include cutting these
people would check. A big one. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}