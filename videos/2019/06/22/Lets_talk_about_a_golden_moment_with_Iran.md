---
title: Let's talk about a golden moment with Iran....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=alstUioeuG0) |
| Published | 2019/06/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Iran's history dating back to the late 40s and early 50s is vital to understanding current events.
- In 1953, the CIA orchestrated the overthrow of Iran's elected government in Operation Ajax.
- The Shah consolidated power with the help of the brutal intelligence agency, SAVAK.
- SAVAK operated with vague laws, no legal representation, and severe consequences like imprisonment or execution.
- Resentment grew among the populace, particularly targeting dissidents like Azerbaijanis and Kurds.
- Economic prosperity from oil sales in the early 70s did not meet expectations, leading to dissatisfaction.
- Rising Islamic nationalism in the Middle East fueled revolutionary sentiments.
- The 1979 revolution caught the intelligence community off guard, ushering in the current government.
- Iranians view their government as a necessary defense against the West, providing relative prosperity and regional power.
- Strengthening the democratic side of the Iranian government is key to improving relations and avoiding conflict.

### Quotes

- "War isn't good for anybody."
- "This cluster that was caused by the president pulling out of the deal may actually be a blessing in disguise."
- "We're at this golden moment where we can actually take another step forward."
- "In order to say that though, we've got to keep the hawks at bay."
- "Strengthening the democratic side of the government there needs to be the primary mission."

### Oneliner

Understanding Iran's history is key to making informed decisions for a peaceful future, necessitating the strengthening of its democratic side while keeping conflict at bay.

### Audience

Policymakers, Activists, Concerned Citizens

### On-the-ground actions from transcript

- Strengthen the democratic side of the Iranian government by supporting diplomatic efforts and engaging with moderate voices (suggested).
- Advocate for peaceful resolutions and diplomacy over aggression in dealing with Iran (implied).

### Whats missing in summary

The full transcript provides an in-depth analysis of Iran's history and its implications on current political decisions, offering valuable insights into the importance of diplomacy and peacekeeping efforts.

### Tags

#Iran #History #USRelations #Diplomacy #Peacebuilding


## Transcript
Well howdy there internet people, it's Beau again.
So tonight, we're gonna talk about
what everybody needs to know about Iran.
And specifically what the president needs to know
to make good decisions from this point forward.
For the very few people who will get the reference
in Walks Weirdo.
Okay, to understand what's going on,
like most things you have to understand
a little bit of history.
This isn't ancient people.
This is an ancient people.
Don't confuse that with backwards though.
They're not, they didn't survive this long by being stupid.
We don't have to go back thousands of years though.
We only have to go back to the late 40s, early 50s
to really get a firm grasp of what's going on.
At that point in time,
the country was a constitutional monarchy.
You had the elected government and you had the shah, the king.
Now, the government looked at the Anglo-Iranian oil company
and was like, hey, you guys, y'all control a whole lot
of our national reserves.
We want to conduct an audit.
And the oil company that later became a part of BP
was like, no.
And the government was like, nationalized.
And the British were like, invasion.
But cooler heads prevailed, and they didn't invade.
Instead, they decided to go the covert route.
If you're in the UK, Operation Boot.
If you're in the United States, it's Operation Ajax,
if you want to look into this.
This was the CIA's first dabble in what
would become its calling card, overthrowing governments.
So what they did, long story short,
They hired a bunch of gangsters from Tehran, the capital, and then they bussed in tough
guys from all over the country and they staged protests.
The first time they tried this, it failed.
Government forces overpowered them.
The second time they tried it, it worked and they got rid of that pesky elected government.
They consolidated power under the Shah.
That happened in 1953.
From there, now you have the Shah, pretty much undisputed.
He had control of SAVAK, S-A-V-A-K.
SAVAK is like if you took the FBI, crossed it with the CIA to include their black sites,
and added a dash of the Spanish Inquisition.
Now SAVAK had wide power, wide ranging authority.
And the laws in the country were very vague, very, very vague.
So much so that if they were in the U.S. and they went before a judge, they would be rolled
void for vagueness under our doctrines.
So what would happen, a SAVAK agent would snatch somebody that for whatever reason had
displeased the government, or maybe not.
And they would interrogate them, torture them.
They would gather a bunch of evidence on them.
They put it into a dossier.
Then Sivak is no longer the interrogator, they're the judge.
No legal representation, no witnesses for the defense, because there is no defense.
To wait entirely on what's inside that dossier.
Then they pass judgment.
Sometimes you go to prison, sometimes they take you out back and shoot you.
Obviously, this caused some resentment among the populace.
And it furthered and fostered a revolutionary feeling, especially when a lot of the people
they were targeting were already dissidents to begin with.
They did this a lot to the Azerbaijanis, the Kurds, and it fostered a lot of resentment.
Then there was the money.
They just started pouring into the country because of oil sales in the early 70s.
And there were a lot of expectations as to what was going to happen, and then they didn't.
Now all over the Middle East you have a rising sense of Islamic nationalism, okay?
And this was brought on by a whole lot of things that were going on at the time that
I don't want to get into because that's just going to spark a whole bunch of debates, but
there was a lot of Western interference in the Middle East at the time, a lot.
And it fostered this camaraderie in this sense of Islam being the defense against the West.
A lot of politically motivated people used the Koran the same way a lot of American politicians
use the Bible, something to rally behind.
And it radicalized people the same way it does in the U.S.
And what you're going to find out is that Iran and the U.S. are very, very similar.
It's kind of like we're so much alike.
why we can't get along but anyway so this rise in Islamic nationalism coupled
with the economic issues added into an already radicalized dissident populace
led to the 1979 revolution and this is one that caught everybody with their
pants pants down the entire intelligence community was just like
what was kind of like the Berlin Wall nobody knew what was gonna happen till
bricks started hitting them in the head and so that brought us to essentially the government
we have in Iran today, in Iran today and here's the interesting thing.
The people there, what they see, it's a lot like the US, yeah it's not the greatest government
but it's the best one we have.
To them, they've kept the West at bay.
This form of government has kept the West at bay.
It has made them relatively prosperous.
They're a regional superpower.
To the U.S., they're like, ah, and we downplay them.
They're pretty technologically advanced, realize they just took down a drone.
They're not a dumb people.
That's something that seems like a common belief in the U.S. that it's just patently
false.
There's a reason they have survived this long.
But here's the interesting thing.
We're back in 1953 again.
Something that a lot of Americans don't know is that they have an elected government.
It's just like it was before the Shaw, kind of.
They have an elected government and they have the theocratic side.
Now if you want to bring this country out of isolation, bring it into the world, so
to speak, what do you have to do?
You have to strengthen the democratic side, not weaken it the way we did the first time.
That's what the nuclear deal was supposed to do.
It wasn't a great deal.
For those that are huge supporters of it, not so much.
But long term it was strategically smart because it bolstered the moderates, the liberals in
the country so to speak, those that want to engage.
Then the president comes in and tears up the deal.
What message does that send?
You can't trust the West.
So who does that strengthen?
Who's that strengthening?
The theocratic side.
They get bolstered by that.
They get wind in their sails.
But then, for whatever reason, and I don't know why, and all I can say is I'm glad, Trump
didn't attack.
Smartest thing he's going to do in office.
I'm almost certain of it.
So now, the people there, they're kind of, well, okay, so we can't trust them, but they're
They're not going to attack us.
Now is the time to move back in and strengthen that democratic side.
To show that even if there's a dispute, it can be resolved peacefully and that we can
move in and we can advance.
We can bring them out.
The reason I care about this is not because I really care about Iranian interests or American
interests over there.
I care about the people involved.
For once, the interest of the American people, the interest of the Iranian people, the interest
of the Iranian government, and the interest of the American government, they all line
up.
War isn't good for anybody.
It's not.
And we're at this golden moment where we can actually take another step forward.
Because this cluster that was caused by the president pulling out of the deal may actually
be a blessing in disguise.
We can move forward from it and show, yes, we pulled out, whatever, but no matter what,
we're not actually looking for war.
In order to say that though, we've got to keep the hawks at bay.
We have to make sure that we don't attack.
We have no justification for it.
We need to strengthen the democratic side of the government there.
That needs to be the primary mission.
I understand the intelligence community doesn't like that because to them, ever since 1979,
they've been upset.
Imagine, put yourself in that institution's shoes.
This was your first big win, and it blew up in your face.
You have to redeem yourself.
That's why there's been this obsession.
That's why the CIA has constantly been at Iran's door.
They're not a huge threat to us.
Are they going to invade?
Of course not.
It's pride.
It's ego within that agency.
The goal of the intelligence community is not to direct policy, it's to provide information.
The information that was in this video.
Anyway, it's just a thought y'all have a good night

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}