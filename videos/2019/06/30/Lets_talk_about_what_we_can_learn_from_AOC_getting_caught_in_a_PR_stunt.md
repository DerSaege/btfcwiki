---
title: Let's talk about what we can learn from AOC getting caught in a PR stunt....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=coZzVfYiNf8) |
| Published | 2019/06/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Conservative news sites are spreading a misleading story about AOC visiting an empty parking lot and pretending it's a detention center.
- The photos in question were actually taken a year before AOC became a representative, during a large protest at a detention center.
- No children were visible at the fence line because they were not allowed that close to the edge of the detention center.
- The story originated from InfoWars and was not fact-checked by reputable outlets.
- Beau urges people to fact-check information themselves, as news sites may push false narratives to fit their agenda.

### Quotes

- "This is how opinions are manufactured."
- "Y'all have a good night."

### Oneliner

Conservative news sites spread a false story about AOC visiting a detention center, revealing the dangers of manufactured opinions and the importance of personal fact-checking.

### Audience

Media Consumers

### On-the-ground actions from transcript

- Fact-check stories spread by news sites (suggested)
- Be vigilant about misinformation and false narratives (implied)

### Whats missing in summary

The tone and delivery of Beau's message, as well as the call to action for personal responsibility in verifying information.

### Tags

#ElectionCycle #Misinformation #FactChecking #ManufacturedOpinions #MediaLiteracy


## Transcript
Well, howdy there, internet people, it's Bo again.
So we gotta talk about the next election cycle
because we can already see what it's gonna be like.
If you go to any conservative news site right now,
you're gonna find out all about the just misleading
out-and-out fraud that's taking place
because they all run in that story about AOC.
She got caught going up to an empty parking lot
at a fence and pretending like it's a detention center
crying out her eyeballs and I mean they're all running that story it's all
over the place she is busted it's even on RT how embarrassing getting caught on
international news I mean that's that's upsetting and the reason I'm saying that
you need to go look at it and that's what the next election cycle is gonna
be like is because that is not accurate that's the story on a whole bunch of
But the reality is the newly uncovered photos came off of the photographer's Twitter.
And they're a year old from before she was even a representative.
She was at a very large protest at a detention center.
And yes, no, there are no children visible at the fence line.
Because even back then, they didn't let them that close to the edge of the concentration camp.
And the photographer, if you want to check out what he has to say about it, it's on Twitter.
You go to at I underscore P underscore A underscore one.
And he basically explains what was going on.
It was a very large protest.
The one year anniversary just happened.
So he put some other photos that he hadn't released yet up on Twitter.
photos that he hadn't released yet up on Twitter, these were some of them.
They did not uncover these photos unless you find pulling them, you consider pulling them
off of Twitter, uncovering them.
It's also important to note that all of these reputable outlets, these outlets that want
you to believe them and want you to take them seriously, didn't fact check info wars.
Alex Jones.
That's where this story came from, Infowars, and all of these outlets that ran this story,
they did in fact check it.
This is the guy that's constantly in court.
I guess AOC does the DMT.
If you haven't seen that video, it's on YouTube, totally worth looking into Alex Jones' DMT,
look it up.
But this should prepare you for the next election because this is what you're going to have
You're going to have a bunch of news sites running articles that they probably know are
false because they fit the agenda.
This is how opinions are manufactured.
So they're not going to fact check anything, so you're going to have to do it.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}