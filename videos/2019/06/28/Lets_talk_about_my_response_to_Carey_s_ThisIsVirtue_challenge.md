---
title: 'Lets talk about my response to Carey''s #ThisIsVirtue challenge....'
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=1Yf8P5APf5g) |
| Published | 2019/06/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recites "New Colossus" sonnet, focusing on America's promise of welcoming immigrants.
- Raises questions about what people are doing to uphold American virtues and values.
- Challenges people to participate in the "This Is Virtue challenge" by reciting the sonnet and sharing it on social media with the hashtag #thisisvirtue.
- Encourages individuals to use their voices and be proactive in standing against policies that harm vulnerable populations.
- Points out the importance of not getting lost in trivial arguments when significant issues are at stake.
- Calls for action to remind people of American virtues and the country's promised values.
- Urges viewers to think about how they will answer future generations when asked about their actions during this era.

### Quotes

- "Give me your tired, your poor, your huddled masses yearning to breathe free."
- "What are you going to tell them? Are you going to tell them that you used your voice?"
- "The idea is to remind people what American virtues are, what they're supposed to be."

### Oneliner

Beau challenges viewers to recite the "New Colossus" sonnet and participate in the "This Is Virtue challenge" to remind people of American virtues, encouraging them to take action and use their voices against injustice.

### Audience

Social media users

### On-the-ground actions from transcript

- Recite and share the "New Colossus" sonnet on social media with the hashtag #thisisvirtue (suggested).

### Whats missing in summary

The emotional impact of participating in the challenge and the power of collective action to uphold American values.

### Tags

#ThisIsVirtue #AmericanValues #Immigration #SocialMedia #Activism


## Transcript
Well, howdy there, internet people, it's Bo again.
Hashtag, this is virtue.
Not like the brazen giant of Greek fame,
with conquering limbs, a stride from land to land,
here at our sunset sea wash skate shall stand a mighty woman
with a torch, whose flame is the imprisoned lightning,
and her name, mother of exiles, from her beacon hand
worldwide welcome. Her mild eyes command the air bridge harbor that Twin Cities
frame. Keep ancient lands and storied pomp cry she with silent lips. Give me
your tired, your poor, your huddled masses yearning to breathe free. The wretched
refuse of your teeming shores. Send these, the homeless, the tempest-tossed, to me."
I lift my lamp beside the golden door. This sonnet and the statue that it's on,
well, they're really for America's ghost writers. They're for the people that did
all of the work and got none of the credit. They're not really mentioned in
the history books, but my question is when that next batch of history books is
written 20 years or 40 years from now, and your kids or grandkids ask you what
you did in this era, what are you going to tell them? Are you going to tell them that you
used your voice? You spoke out against policies that were not even fully enacted
yet are leading to toddlers face down in rivers or covered in their own bodily
fluids in camps? Families getting ripped apart? Gestapo-like raids? Sexual abuse in
those camps? Are you gonna be able to tell them that? Or are you gonna tell them well
I would have but I was too busy arguing over the finer points in the nuances of
of the definition of the word concentration camp.
So this is the This Is Virtue challenge.
You take this sonnet, New Colossus, and you recite it, put it on your social media.
If you can and you're a creative person, do something creative with it.
The idea is to remind people what American virtues are, what they're supposed to be,
what they were promised that they were.
So you put it on your social media, you hashtag it, thisisvirtue.
I personally think you should do it, but it's just a thought.
You guys, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}