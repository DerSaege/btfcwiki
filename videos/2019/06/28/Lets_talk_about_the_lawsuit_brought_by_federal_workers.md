---
title: Let's talk about the lawsuit brought by federal workers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Zg7W3rmR2bo) |
| Published | 2019/06/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Lawsuit brought by federal asylum officers, not politicians like former Secretary of Homeland Security and James Clapper.
- Federal asylum officers listen to harrowing stories daily and make life-and-death decisions on asylum claims.
- Lawsuit is against the "remain in Mexico policy" or migrant protection protocols, considered a violation of international and domestic law.
- Migrant protection protocols were dictated by Trump and cause widespread violations of rights.
- Asylum officers are barred from asking asylum seekers if they fear for their safety in Mexico under the current system.
- Policy intentionally slows down asylum claims processing to the point where people suffer and die.
- Beau criticizes Trump's actions as dictatorial, bypassing constitutional checks and controlling concentration camps.
- Federal asylum officers challenge Trump's actions, despite their usual role in making tough decisions.
- They condemn the policy as immoral and fundamentally against the moral values of the nation.

### Quotes

- "The fact that they oppose Trump is not news. What is news is who brought the lawsuit."
- "It most certainly is."
- "It's intentionally barring asylum officers from doing their job."
- "It's about stopping people and then slowing down the rate at which we process asylums claims so they die."
- "People who are used to making life and death decisions. People who are totally comfortable sending people back to squalor. They're saying that this is immoral."

### Oneliner

Federal asylum officers challenge Trump's dictatorial control over the "remain in Mexico policy," condemning it as fundamentally immoral.

### Audience

Advocates for human rights

### On-the-ground actions from transcript

- Support organizations advocating for asylum seekers' rights (implied)
- Raise awareness about the violations caused by the "remain in Mexico policy" (implied)
- Advocate for policy changes to protect asylum seekers (implied)

### Whats missing in summary

The emotional impact of federal asylum officers challenging a policy they deem fundamentally immoral.

### Tags

#Immigration #AsylumSeekers #HumanRights #Trump #Policy


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we're going to talk about a lawsuit.
The media seems to be focusing on the involvement
of the former Secretary of Homeland Security
and James Clapper.
They've kind of co-signed this lawsuit.
That's not important.
Those people are politicians.
They were in Obama's administration.
The fact that they oppose Trump is not news.
What is news is who brought the lawsuit.
It wasn't brought by them.
It was brought by the union
represents federal asylum officers. Now to understand why that's important, you
have to understand what federal asylum officers do. On a daily basis, they
listen to the most horrible stories of human suffering imaginable. They sit
there and they listen to it and then they make a decision. If you're talking
to a federal asylum officer, you are in a bad way. However, being in a bad way
alone does not necessarily qualify you for asylum. So they listen to these
horrible situations that people are in and they reach up and they click the
button that says deny and they send them right back to that situation. These are
not tender-hearted people. These are not bleeding heart liberals. These are people
who on a daily basis make decisions of life and death for people based on
criteria. They're bringing the lawsuit over the remain in Mexico policy. The
The formal name of that is the migrant protection protocols and if there has ever been an example
of new speak in the United States, it is that.
The migrant protection protocols were brought into existence by Trump.
He spoke them into existence, he dictated them and they became the way we operate.
The asylum officers are saying that it is a violation, it's causing widespread violation
of international and domestic law.
course it is. Anybody that has watched this channel for any length of time has
seen me go through this. I'm going to go through it again real quick. Article 6
section 2 of the US Constitution says that international treaties are law. The
1967 protocols for the treatment of refugees lay out how we are supposed to
treat these people. We are violating international law, US law, and the US
Constitution based on the whims of President Trump speaking.
They also say that it is fundamentally contrary to the moral fabric of our nation.
I would agree.
It most certainly is.
It most certainly is.
There's a State Department report that says asylum seekers are likely to suffer the same
persecution they face at home in Mexico.
killing people. If this policy was not in existence, that little girl and her dad
would still be alive today. Okay, the one thing that came out during this that I
think is important, I'm gonna say it twice to make sure people catch the
importance of it, the current system as it is operating right now does not allow
asylum officers to ask if people fear for their safety in Mexico. However, if it
is brought up by the asylum seeker it can be considered. Asylum officers cannot
ask if people fear for their safety in Mexico. However, if it is brought up it
can be considered. If the asylum seeker brings it up it can be considered. That
seems like really important information and it also shows exactly how messed up
this policy is. It's intentionally barring asylum officers from doing their
job, which is to determine who is really at risk and who isn't. That's their job.
And the president is stopping them from doing it because this isn't about
protecting migrants. It never was. It's about stopping people and then slowing
down the rate at which we process asylums claims so they die. That's what
it is. Now, at the end of the day, what's really important here is that we have a
man in the Oval Office who dictated law, there's a word for that, dictator, who is
in control of concentration camps. That's where we're at in the United States
States today. He bypassed all constitutional checks to do this. He didn't get congressional
authority. He certainly didn't find a way to undermine an international treaty. He just
spoke. He dictated law. He's a dictator. We let him get away with it. Now these federal
Asylum officers are taking him to task for it.
People who are used to making life and death decisions.
People who are totally comfortable sending people back to squalor.
They're saying that this is immoral.
They're saying it is fundamentally contrary to the moral fabric of our nation.
It is.
Ah, anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}