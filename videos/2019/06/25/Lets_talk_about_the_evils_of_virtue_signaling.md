---
title: Let's talk about the evils of virtue signaling....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=pCTllwHd5RQ) |
| Published | 2019/06/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Virtue signaling is the expression of a positive social behavior to your in-group, not necessarily an action rooted in compassion.
- Virtue signaling can be effective in causing societal change, even though it may come across as empty or hollow.
- Examples of virtue signaling include the creation of the "warrior cop" and gestures of loyalty to certain dictators in history.
- Calling out virtue signaling can also be a form of virtue signaling, conveying superiority and rationality within a group.
- Experts have expressed concern over how Trump's rhetoric normalized dehumanization and spread through political parties and government agencies.
- The normalization of inhumane practices, such as in ICE's statement on immigration policy, can be seen as a form of virtue within certain groups.
- Conflicting views on immigration policies and human trafficking often overlook the root causes and focus solely on containment rather than solutions.
- Society needs positive virtue signaling to steer towards a more humane society by addressing underlying issues and promoting compassion.

### Quotes

- "Virtue signaling is extremely effective. It causes societal change."
- "Calling out virtue signaling can also be a form of virtue signaling."
- "The normalization of inhumane practices, such as in ICE's statement on immigration policy, can be seen as a form of virtue within certain groups."

### Oneliner

Virtue signaling, both effective and divisive, influences societal norms and political rhetoric, shaping perceptions and policies.

### Audience

Activists, Advocates, Observers

### On-the-ground actions from transcript

- Challenge dehumanizing narratives within your community (exemplified)
- Advocate for addressing root causes of issues (implied)
- Promote compassionate solutions in policy and discourse (exemplified)
  
### Whats missing in summary

The full transcript delves into the negative impacts of virtue signaling, calling for a shift towards positive virtue signaling to create a more humane society.

### Tags

#VirtueSignaling #SocietalChange #HumanRights #ImmigrationPolicy #CommunityAdvocacy


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we're going to talk about virtue signaling, what
it is.
Is it a thing?
Yeah, of course it's a thing.
A huge part of human communication is signaling.
It's a massive part of it.
There are a bunch of different kinds.
Virtue signaling is the expression
of a positive social behavior to your in-group.
That's it.
That's what it means.
It's not that, oh, this person is
saying something compassionate.
The other idea that goes along with it as it gets used as an insult
is that it's empty, that it doesn't mean anything, it doesn't do anything.
It's just a hollow statement.
Sometimes that's true, but it's not.
Virtue signaling is extremely effective.
It causes societal change.
societal change. Some examples of virtue signaling. The warrior cop was in large
part created because of virtue signaling. I want to go home at the end of my shift.
We write the report. The image that those messages they got conveyed they
They were edgy at first, and then they got normalized.
They were virtue signals to that end group, and it took hold, wasn't a good thing.
In the 1930s and 1940s, you walk into a room and click your heels and give a salute to
a certain dictator, that was a virtue signal.
Doesn't necessarily mean that you're doing something that is compassionate, it just means
that the people around you believe it to be a positive social behavior, that's it, doesn't
actually have to be virtuous.
My favorite example of virtue signaling is when somebody points out in the comment section,
hey that's virtue signaling, because that's a virtue signal.
telling those around you in your in-group that I, the person who is calling out this
virtue signaling, I am above being taken in by emotional arguments. I am rational, I am
pragmatic, I am cynical, I am dead inside. That's what it conveys because within that
in group, well that's a virtue, it's a positive social behavior.
This is why experts on fascism, experts on political violence were very worried about
Trump's rhetoric because they watched it, they watched him signal and they watched it
resonate with the bigots and then spread and normalize throughout the Republican party.
That's what happened.
And now it has spread into government agencies.
That dehumanization of the other, it's normal now.
It's a virtue.
We can see this in ICE's statement.
They're upset about the president's chaotic immigration policy.
Not because children are covered in their own bodily fluids and don't have enough food
and are dying in custody, but because it's inefficient.
That should be a worry.
That should be a worry.
Because that is virtuous within that end group now.
Next time you're having a discussion, just throw it out there.
You know what's going to happen when you do it, but throw it out there.
Well we could let them out, do it the way we used to.
No.
Because within that end group, within the pro-police state end group, the anti-immigration
group, it's virtuous to keep them in, that's no longer an option, we can't let them out.
That's extremely dangerous, because the conditions as they worsen, wouldn't it be more humane
if we did something else with them, if we could come up with some other solution, maybe
a final one because we can't let them out.
Another type you see is where they've couched it into something that appears to be compassionate
But it's only because they don't understand the situation or they know they're lying.
A lot of people say, well, this is how we have to stop human trafficking.
Is it?
Regardless of what the president says, the man who doesn't read the reports and doesn't
attend intelligence briefings, human trafficking doesn't actually really occur through migrants
claiming asylum.
That's not a thing.
That is not a thing.
Now there are rare cases where it can happen, no doubt.
Letting them out and just processing them quickly and sending them through, that's
no guarantee that there won't be human trafficking.
But what's the worst thing that can happen when somebody's human trafficked?
abuse, being kept confined in squalid conditions, covered in their own bodily fluids, no, letting
them out is not a guarantee that human trafficking will not occur and that horrible things will
not happen to these people, but confining them is a guarantee that it will happen.
The discussion over this has gotten ridiculous.
Nobody's actually talking about the root causes of any of this stuff anymore.
Nobody wants to talk about our foreign policy that's creating most of these
migrants, most of these asylum seekers, we don't wanna talk about that.
Well, we just wanna find a way to stop it.
Doesn't work that way, does not work that way.
If there's anything that history has taught us,
it's that the drive for freedom and safety, it'll find a way.
Even if it has to come violently, it will find a way.
But rather than talking about making more visas and making it easier and being able
to filter it out, the bad elements, the bad ombres, so to speak, and increasing the number
that can come, we're not talking about that.
We just want to stop it.
We're not talking about removing the caps from certain countries or maybe diverting
them from countries where there aren't problems, like in Western Europe.
We're not going to divert them from there and move them to Central and South America.
And then when that comes up, people are worried about overpopulation.
You know, we got a no vacancy sign out.
No, we don't.
This country is nowhere near overpopulated.
If you are worried about overpopulation and you are not worried about overconsumption,
you just don't like poor people, and in this case, poor brown people, it's really what
it boils down to.
So this is a look at the bad side of virtue signaling.
The next video, we're going to talk about the positive aspects of it.
And I definitely think to get this country back to a semblance of a humane society, we're
going to need a lot of positive virtue signaling.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}