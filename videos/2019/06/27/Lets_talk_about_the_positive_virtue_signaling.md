---
title: Let's talk about the positive virtue signaling....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=3rO_-oCBCNA) |
| Published | 2019/06/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Virtue signaling has positive aspects and is a significant part of human communication.
- The ice bucket challenge, despite criticism of being empty, raised over a hundred million dollars.
- Compassionate campaigns supporting asylum seekers are failing due to how they're framed, not their message.
- Campaigns framed in opposition to Trump may not produce the best results.
- Beau is concerned about stopping the abuse of kids in detention facilities and preventing drownings.
- Loyalty to political party often outweighs loyalty to principles, allowing wrong actions to be discounted.
- Beau calls for campaigns reminding people of America's virtues and what they should stand for.
- He finds it hard to believe that half the population tolerates sexual assault on kids and inhumane treatment of children in detention.
- Beau believes most Americans do not want kids to be abused but may justify it due to loyalty to Trump.
- Campaigns should focus on the morality of the situation and not political arguments.

### Quotes

- "We need more virtue signaling, a lot of it."
- "I believe that the average American does not actually want kids to be sexually assaulted."
- "We need campaigns that focus on the morality of what's actually happening."
- "We're funding rape rooms."
- "We need to show America what a virtue is."

### Oneliner

Beau talks about the positive impact of virtue signaling, the failure of compassionate campaigns, and the need for morality-focused campaigns to address critical issues like abuse and inhumane treatment.

### Audience

Advocates for social change

### On-the-ground actions from transcript

- Develop and support campaigns reminding people of America's virtues (implied)
- Share suggestions for campaigns focused on addressing critical issues like abuse and inhumane treatment (implied)

### Whats missing in summary

The emotional impact and Beau's call for action are best understood by watching the full transcript.


## Transcript
Well howdy there internet people, it's Bo again.
So today we're gonna talk about the positive aspects
of virtue signaling.
Virtue signaling is a real thing.
Signaling is a huge part of our communication as humans.
Signaling a virtue is done all the time.
I think the most memorable...
I'm ready for the ice bucket challenge.
And the criticism of this was that it was empty, it was hollow, you weren't really
doing anything for the cause, you were just showing that you supported something that
was universally good.
And it spread like wildfire despite that criticism.
Now the question is, even with those criticisms, was it ineffective?
No, it was extremely effective.
campaign raised more than a hundred million dollars. So say what you want
about virtue signaling, it works. So we need more virtue signaling, a lot of it.
This is going to be one of the few videos in which I put out a call to action.
And the criticisms of being compassionate made me realize why a lot
A lot of the campaigns in support of asylum seekers are failing.
They are not generating the traction they should.
It's because of how they're framed, not what they're saying.
That criticism of that it's something that's universally good, everybody knows that it's
good.
That's not how they're being framed.
These campaigns are being framed in opposition to Trump.
Now, anybody who knows me knows that I don't necessarily think opposition to Trump is a
bad thing, however, it's not going to produce the best results.
And right now, I'm far more concerned about stopping kids that are being abused in detention
facilities and stopping people from drowning.
That's my goal.
I am more concerned about that at the moment.
Because the two-party system has done what it has in this country, it has become more
important to signal your loyalty to the party than it is to signal loyalty to your principles.
So what you have is you have people in the Republican Party that know this is wrong being
able to discount the fact that it's wrong simply because they're following Trump.
That outweighs it.
So we just remove him from the equation.
We need several campaigns, and I've put out the call to people that are more creative
than I am and that can be more widely received than I am, to help develop some.
I would like to hear your suggestions, but we need several campaigns designed to remind
people of what America's virtues are supposed to be.
we were promised they were. I find it hard to believe that half the population has decided
that it's okay to sexually assault kids. I find it hard to believe that half the population
has decided that it's okay and that it's a virtue to allow children to be housed the
the way they were, the way they probably still are.
I never thought we would reach the point where saying kids shouldn't be abused is a virtue.
That seems like it should be the default.
I believe that for most people it is.
I believe despite all evidence to the contrary lately, I believe that the average American
does not actually want kids to be sexually assaulted.
They don't want kids to be covered in their own bodily fluids and kept in a cage.
They're only able to justify it because of a loyalty to Trump.
The only way they're able to do it.
So we remove him from that equation.
We need campaigns that focus on the morality of what's actually happening, not the political
arguments, not the political talking points, just about the fact that people and the paid
employee of the U.S. government are sexually assaulting kids.
We're funding it.
We're funding rape rooms.
I want to hear your suggestions on campaigns with this goal in mind.
We need to show America what a virtue is.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}