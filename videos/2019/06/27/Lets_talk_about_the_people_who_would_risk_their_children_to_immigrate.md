---
title: Let's talk about the people who would risk their children to immigrate....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=OMrHm4GFvpg) |
| Published | 2019/06/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau was asked a question that led him to research and provide an insightful answer about the kind of people who immigrate.
- He shares a first-person account from the 1700s detailing the harsh conditions immigrants faced, similar to present-day situations.
- Immigrants arriving before the mid-1800s faced challenges like being purchased off ships and enduring sickness while waiting to come ashore.
- Families often had to give their sick children to strangers to save their lives during immigration.
- The conditions for immigrants improved slightly in the 1840s but were still challenging.
- Beau points out that throughout history, people came to the U.S. as refugees fleeing various crises.
- He questions individuals who suggest fixing their corrupt home countries instead of seeking better opportunities elsewhere.
- Beau criticizes those who blame immigrants for the country's issues while failing to address systemic problems like lack of basic necessities and democracy concerns.
- He challenges the notion of leaving the country if one has issues with it, citing the true embodiment of the American spirit in those enduring hardships at the border.
- Beau underscores the courage and spirit of immigrants and urges embracing them as additions to the American tapestry rather than condemning them.

### Quotes

- "People are still taking that risk today."
- "The spiritual sons and daughters of the people who built this country are the people dying down there on the border."
- "No self-evaluation will ever be as valuable or as accurate as an enemy's evaluation of you."
- "Who if you have the courage."
- "You'd be welcoming these people as new additions to the American tapestry, not crying and saying that their children deserve to drown."

### Oneliner

Beau dives into history to explain the courage of immigrants and challenges individuals to rethink their views on immigration and refugees.

### Audience

Advocates, Activists, Citizens

### On-the-ground actions from transcript

- Welcome and support immigrants and refugees as new additions to your community (implied)
- Challenge misconceptions about immigrants and refugees in your circles (implied)
- Advocate for humane treatment and support for those seeking refuge (implied)

### Whats missing in summary

The emotional depth and historical context of Beau's message can be best experienced by watching the full transcript.

### Tags

#Immigration #Refugees #AmericanSpirit #History #Humanity


## Transcript
Well, howdy there internet people it's Bo again
somebody asked a
question today and sent me down a rabbit hole. You know, my first response was just to dismiss the question, but
after thinking about it, it's a fair question and it deserves an answer even though I'm barely certain it was a
rhetorical question.  question and the question was what kind of person would put their kids in that
kind of danger to immigrate. It is a fair question. So after a little bit of
research I found a first-person account of one of those trips. I'm gonna read it
now. Terrible misery, stench, fumes, horror, vomiting, many kinds of sickness, fever,
dysentery, headache, heat, constipation, boil, scurvy, cancer, mouth rot and the
like all of which come from old and sharply salted food and meat also very
bad and foul water so that many die miserably. Wait, scurvy? I'm reading the
wrong one. This is how your family got here if you came from Europe. This is
somebody that took the trip in the 1700s. Now when this guy showed up what would
happen the ship would stay offshore and somebody would have to come along and
kind of purchase you off the ship, pay a little bit of more money, get you off of
it. This took time and the sick people were still on the ship. In fact if they
were really sick their families wouldn't come to get them. A lot of people died
waiting to come ashore, the risk of contagion was higher. So a lot of people
took their kids and gave them to strangers to save their lives. Sounds
familiar doesn't it? Sounds familiar. So this was in the mid 1700s. It stayed like
this and these were the conditions until about the mid 1800s. Then they got a
a little bit better and indentured servitude ended somewhere along the way, but they got
a little bit better in the 1840s, but not too much.
But let's just go ahead and face facts.
If you came, your family came after 1840, well, you were a refugee fleeing famine, World
World War I, you were an economic refugee dealing with the hard times caused by the
Depression, fleeing World War II, the Soviet Union trying to get out from behind the Iron
Curtain or worried about the coming conflict.
And this is, well, I mean that's pretty much all of history.
Maybe the fall of the Soviet Union, a lot came then too.
If you came from Asia, the trip was a little worse, but very, very similar.
If you came from Africa, it was obviously a whole lot worse.
Everybody that came to this country, whether voluntarily or involuntarily, suffered coming
here.
Some more than others.
But everybody suffered.
Everybody took that risk.
That's how this country was founded.
People are still taking that risk today.
Now the follow-up question, of course, or not a question, but a statement was that,
well, if it was my country, I'd stay and fix it.
If my country was this corrupt, I would stay and fix it.
We're waiting.
Where are you at?
You've got cities that didn't have potable water.
We've got studies showing that the democracy, the republic, is dead and that we live in an oligarchy.
Need I remind you, we have cages filled with toddlers who are covered with bodily fluids.
Don't have enough to eat.
Where are you at?
I mean, you're not even having to fight armed narco-terrorists and gangs or a government propped up by the CIA.
I mean, what are you doing?
you're too busy enjoying your favorite flavor, boot leather, and I know somebody
right now is saying, probably typing the comment, you know, that if I have all
these problems with the country, if I don't like it, why don't I leave. Think
about that for a little bit before you hit enter. The spiritual sons and
daughters of the people who built this country are not the citizens who say
stuff like this.
The spiritual sons and daughters of the people who built this country are the
people dying down there on the border.
That's who truly embodies the American spirit still.
Not the people who say stuff like this.
You know, no self-evaluation will ever be as
valuable or as accurate as an enemy's
evaluation of you. Their appraisal is going to be more truthful.
Everybody knows that Hitler wrote Mein Kampf. He also wrote another book and in it he expressed his fear about fighting
the United States.  Because to him what he saw
was that the bravest and the boldest of all of these villages all over the world,
world, those people that would not settle for a substandard life, that would not kowtow
to some dictator, well they all left and they came here, they came to the U.S. and he was
terrified of finding the United States because of that, he knew that that spirit would shine
through and he was right, he didn't fare too well.
So I guess the answer to that question, who would make this kind of trip and endanger
their family like this?
You if you have the courage.
If you are the sons of the pioneers, the daughters of Rosie the Riveter, you'd be welcoming
these people as new additions to the American tapestry, not crying and saying that their
children deserve to drown.
It's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}