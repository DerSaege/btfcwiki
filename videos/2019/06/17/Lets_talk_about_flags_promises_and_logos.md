---
title: Let's talk about flags, promises, and logos....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=vB8I9uQTTx0) |
| Published | 2019/06/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits
Beau says:

- Questions the symbolism of flag burning as a constitutional amendment banning it gains attention.
- Points out the irony of claiming freedom in a nation with high incarceration rates and fear-driven policies.
- Emphasizes that the founding fathers spoke more of ideas like freedom and liberty than nationalism.
- Challenges if society truly upholds promises of equality, liberty, and justice for all.
- Questions the real meaning of symbols like the American flag and who truly understands its promises.
- Criticizes those who aim to criminalize flag burning while ignoring the essence of freedom and equality.
- Urges patriots to actively work towards equal rights and justice for all to truly embody American values.
- States that symbols like the flag lose meaning if promises of equality and liberty are not upheld.
- Condemns those in power who prioritize protecting a flag over fulfilling the country's foundational ideals.
- Expresses concern over the dwindling efforts to ensure equality for all in society.

### Quotes
- "If you don't act on those promises, this means nothing."
- "The person burning this flag, they didn't disgrace it. You did when you failed to keep the promises."
- "It's just a thought. Y'all have a good night."

### Oneliner
Beau questions the true symbolism of the American flag amidst debates over flag burning, urging patriots to actively uphold promises of equality and justice to give meaning to national symbols.

### Audience
Patriots, Activists, Americans

### On-the-ground actions from transcript
- Work towards equal rights and justice for all in your community (implied)
- Actively uphold the promises of equality and liberty in your daily actions (implied)

### Whats missing in summary
The full transcript provides a thought-provoking analysis of the symbolism of national flags and challenges individuals to actively work towards fulfilling the promises of equality and justice in society.

### Tags
#FlagBurning #NationalSymbols #Equality #Justice #AmericanValues


## Transcript
Well howdy there internet people, it's Bo again.
We got us a hot topic today.
Constitutional amendment banning flag burning.
It's a good way to drum up the base, I guess.
But it seems odd to me.
What does this symbolize?
It's a symbol.
What's it symbolize?
Well, the land of the free and the home of the brave,
Of course, the land of the free with the highest incarceration rate in the world and the home
of the brave who is so terrified of the other they want to cower behind a wall.
Not buying it.
Try again.
Well it symbolizes our country.
Okay that's a good route.
Go the nationalist route.
Bring up the founding fathers and all of that.
The problem is when the founding fathers spoke of country they weren't talking about the
United States.
and I don't mean that in some linguistic quirk.
I mean, it didn't exist yet.
When they said country, when they spoke of country,
they meant the country, their countryside, their neighbors,
the people who are so often forgotten
in conversations like this.
That's who they were talking about.
They didn't speak that way very often, either.
They're pretty bright guys.
They understood that when you're trying
to get a whole bunch of people to commit treason,
it's not a good idea to talk like a nationalist.
They spoke of ideas.
They spoke of freedom and liberty.
That's what they talked about.
That's what stirred people to action.
They made promises to the future and created
the machinery for change.
Well, it symbolizes those promises that it should.
I will give you that, it certainly should, but does it?
Do we as a society move forward in hopes of fulfilling those promises?
Do we try to make sure that while there is the ability to pursue happiness, that all
men are created equal, that there's liberty and justice for all?
Or do we do everything we can to make sure that the other is painted as less than equal?
Do we turn a blind eye to grave injustices and try to justify them in some manner?
It's horrible what's happening to those kids, but they just should have done whatever.
The people who want this criminalized, they're not doing anything to fulfill those promises,
so I can't believe that that's what it symbolizes to them.
It symbolizes those who fought and died for our freedom, maybe at one time.
Maybe at one time.
Today, how did our freedom get in Iraq, Afghanistan, Libya, Mali, Sudan, Panama, Grenada?
How did our freedom get there?
As is evidenced by this, the only people who can take away your freedom is your government.
That's who can take away your freedom, unless we're talking about an invasion, and we haven't
been at risk of an invasion in this country in a very, very long time.
Well fine, they weren't, you know, fighting for freedom exactly, but they were protecting
the American way of life, yes, yes they were.
And that's what this symbolizes today.
It symbolizes the American way of life, absolutely.
It does.
Now let's say you had a problem with an aspect of the American way of life and you wanted
to express your displeasure.
How would you do it?
What would be a good symbol to use?
Not violently, not hurting anybody, and that's the ultimate ha in this, it's the ultimate
punch line.
people who would burn the American flag it has more meaning to them than those
who simply slap a bumper sticker on their truck. They understood the
promises. They want to improve the American way of life and this is a way
to express their displeasure.
They understand what it was supposed to be.
And instead, as our understanding of these promises has been whittled away, those who
consider themselves patriots, they spit on those ideas.
If seeing someone burn your symbol of freedom causes you to decide that they need to have
their freedom taken away, well your symbol doesn't do any good, does it?
It doesn't remind you of what it's supposed to and that's the whole job of a symbol.
Our country, if you want to be a patriot today, you have to be working for equal rights to
make sure that all men are created equal.
You have to be looking out for the downtrodden.
You have to be hoping to help the huddled masses yearning to breathe free.
All of those things that make up the American dream and are supposed to make up the American
character.
You have to do that if you want to be a patriot.
Otherwise they're just tag lines beneath the logo.
No more sacred than the logo of GE or McDonald's.
If you don't act on those promises, this means nothing.
It means nothing.
You have to act on those promises.
And the main idea of it was to make sure that all men could stand under this one day as
equals.
That was the idea.
But today it's a logo.
Just like all of the other logos.
really mean much at all. I can't fault somebody for burning a logo. At least to
them it means something.
At the end of the day, if you're not fighting to keep those promises, how can you really
care about the symbol of those promises.
You've got these people up in D.C. who want to criminalize this and rewrite the Constitution
so they can do it because, you know, they're interested in keeping the promises.
And the thing is, they're spitting on the very ideas that are supposed to be America.
And what's worse is that they know a large portion of this country who consider themselves
patriots.
They lost the plot too.
They're just using this to play to the base, to play to people who think they're patriots.
That's what they're doing, because they know this isn't going to go anywhere.
It's got to get through the House, the Senate, and be ratified by the states.
It's just to drum up support from the people they know don't understand this country and
what it was supposed to be.
There were points in time where we worked as a society to make sure people could stand
under this as equals.
The problem is those high points in the American experience, they're growing fewer and further
apart.
Meanwhile there's more flags than ever.
The person burning this flag, they didn't disgrace it.
They didn't desecrate it, you did, when you failed to keep the promises.
When you forgot what it was supposed to be.
When you turned your back on the very ideas that this country was founded on.
And it's happening all over the country.
And it's being cheered on by the same people who want to change the Constitution to protect
a cloth. A logo. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}