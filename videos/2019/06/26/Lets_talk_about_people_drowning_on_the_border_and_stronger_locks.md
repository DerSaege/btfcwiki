---
title: Let's talk about people drowning on the border and stronger locks....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=GhWZqhAPZXU) |
| Published | 2019/06/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the portrayal of drowned individuals as "illegal immigrants invading our house."
- Explains that these people were fleeing from a country, El Salvador, with a corrupt government supported by the US.
- Describes El Salvador as being overrun by a gang that originated in the United States.
- Mentions issues in El Salvador leading to the ban on a police force as part of a peace agreement.
- Talks about how the gang, with US-trained leadership, became a transnational threat known as MS-13.
- Criticizes policies at the border and compares supporting them to committing a heinous act.
- Points out the role of the US in the situation and the consequences faced by the fleeing individuals.
- Emphasizes the luck and geographic advantage enjoyed by people in the US.
- Condemns the actions that led to the deaths of individuals including a little girl and her family.
- Concludes by reflecting on the impact of these events.

### Quotes

- "We set their house on fire and we're taking pot shots at them as they leave, and then when they die, well, it happens."
- "If you support these policies that are going on down at the border, it's this moral equivalent, is if you took that little girl, put your boot on her head, and held her head under that water until the bubble stopped, you killed her."
- "We killed them. We killed that little girl. We killed her father."
- "That's America today."
- "We live here, we won the geographic lottery, and we can look at these things that happen in other countries and act like, well, it doesn't have anything to do with us."

### Oneliner

Beau addresses the misrepresentation of drowned individuals as "illegal immigrants invading our house" and reveals the harsh reality of their situation, condemning US policies and acknowledging America's role in the tragic events.

### Audience

Policy Advocates

### On-the-ground actions from transcript

- Support organizations aiding asylum seekers (suggested)
- Advocate for policy changes regarding immigration and border control (implied)

### Whats missing in summary

The emotional impact of Beau's words and the call to action for viewers to acknowledge their country's involvement in the suffering of others.

### Tags

#Immigration #USPolicy #Injustice #HumanitarianCrisis #MS13


## Transcript
Well, howdy there, internet people, it's Bo again.
I know a lot of people are expecting the second part
to that virtue signaling thing, and we will do that,
but I wanna talk about something else first,
because some people drowned and I wanna talk about that
because it's being portrayed as,
oh, here come those illegal immigrants
trying to invade our house,
we need a better lock on our door,
and the idea and the theme that they're coming up here
for benefits and all of the normal stuff.
Okay, so let's get to reality then.
These people were fleeing El Salvador.
El Salvador is a country run by a corrupt government that the US props up as their little
puppet down there.
The country is overrun by a gang, a gang that originated in the United States.
And see, a while back, there were some issues in El Salvador, and as part of the peace agreement,
they weren't allowed to have a police force.
And when they didn't have a police force, that's when we chose to send this gang back
to El Salvador. So of course, with no opposition, they established a really good foothold.
But they didn't have any good leadership. Well, we took care of that, too. Let's talk
about a guy named Satan. He's the guy that took over that gang. He was trained by the
US military and the intelligence community at a place called the School of Americas.
We trained him, and he went back to that penny-anny street gang and turned it into the transnational
threat that it is today. What you know is MS-13. MS-13 is not an El Salvadoran gang.
It originated in the United States, it was transplanted there, and the leadership was
trained by the US military and the intelligence community. No, they're not invading our home
and we need a bigger lock on our door. We set their house on fire and we're taking
pot shots at them as they leave, and then when they die, well, it happens.
Make no mistake about it, if you support these policies that are going on down at the border,
it's this moral equivalent, is if you took that little girl, put your boot on her head,
and held her head under that water until the bubble stopped, you killed her.
You killed her.
See we're insulated here in the U.S. because we're lucky.
We live here, we won the geographic lottery, and we can look at these things that happen
in other countries and act like, well, it doesn't have anything to do with us.
We did this every step of the way, from the reasons they were fleeing to the reason they
were in that part of the river.
We wanted to deter them, stop them from getting here.
You can't deter the drive for safety.
We killed them.
We killed that little girl.
We killed her father.
We permanently scarred the mother who got to watch from the shoreline as her entire
family drowned.
That's America today.
Anyway it's just a thought.
and I'll have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}