---
title: Let's talk about bridges, roads, and parallel power structures....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=fBbd0Gj11SU) |
| Published | 2019/06/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Introduces the concept of parallel power structures in relation to freedom and responsibility.
- Emphasizes the need for individuals to take responsibility for their freedom.
- Suggests that limiting the power of the state can be achieved by creating parallel power structures.
- Gives an example of building a road as a parallel power structure to showcase real freedom.
- Explains how individuals can come together to build infrastructure without relying on the government.
- Shares a historical example of people building a 300-mile road in a single day through community effort.
- Points out that parallel power structures can lead to government change or improvements in responsiveness.
- Mentions how social programs like free lunch programs may have been inspired by grassroots initiatives like the Black Panthers.
- Encourages using social media and community engagement to create change and build parallel power structures.

### Quotes

- "Freedom comes with responsibility."
- "You want that kind of freedom, you have to take that kind of responsibility."
- "There's nothing that the government can do that you can't."
- "Parallel power structures are the road to a future that has real freedom."
- "It's probably time to start building bridges so you can build your road."

### Oneliner

Beau introduces parallel power structures as a path to real freedom, using the example of building a road through community effort to showcase individual responsibility and empowerment.

### Audience

Community organizers, activists

### On-the-ground actions from transcript

- Gather volunteers to work on community projects (exemplified)
- Utilize social media to mobilize support for grassroots initiatives (exemplified)

### Whats missing in summary

The full transcript dives deeper into historical examples and the potential impact of parallel power structures on government responsiveness and societal change.


## Transcript
Well, howdy there internet people, it's Bo again.
So today, we're going to talk about bridges, roads,
and parallel power structures.
Parallel power structures are a concept
that I've touched on in a lot of videos,
but I've never really dove into the subject too deep.
People who want freedom, and I mean real freedom,
understand that freedom comes with responsibility, a lot of it.
You know, if you've ever been out in the woods, like really out there, you know what I'm talking
about.
Yeah, you're free.
You can do anything you want, including getting yourself killed.
So if you want that kind of freedom, you have to take that kind of responsibility.
And if you can take that kind of responsibility, you can limit the power the state has.
And this is where normally people get a little confused, because they're like, well, how
do we limit the government?
And a lot of times they try to get the government to limit itself.
That rarely works out in the long run.
A lot of times you can defeat an overreaching or incompetent government by ignoring it,
by doing the things that need to be done outside of them, making the improvements that need
to be made without their assistance.
In today's world, people push back against that idea.
And it's mainly because the things that most people want to fix are seen as infrastructure.
And infrastructure is the purview of the state.
It's the government's job.
That's what they do.
People can't do that on their own.
There's nothing that the government can do that you can't.
The government itself is just people.
You are a person, you can do it.
And these parallel power structures are the road to a future that has real freedom.
And in that light, we're going to use a road as our example.
Let's say you want to build a road.
We're ambitious.
We're not talking about some penny-ante road in a subdivision.
No, you want to build like a highway, 100 miles or more.
So what do you have to do?
What do you really have to do to do this?
Now the way we're taught is that, well, we need to go to the government, ask them, then
they'll do a survey, and then they'll seize land from people, and they'll build the road.
road. That's one way to do it. Or, before you build that road, you build some bridges.
Maybe reach out to people in the media. Get them on your side. Get them to pitch the idea
to the people in the towns along the way. The farmers that own the land explain that
it's going to help everybody. But we all need to pitch in a little bit. And it'll work.
work doing it that way. I know it'll work because it did. In the winter of 1909, some
people whose names have been lost to history, they had a couple of people that worked at
a newspaper reach out for them, build those bridges. Now, of course, we know the journalist
names because, well, journalists will always make sure their name is in it, but they went
They went town to town, and in the towns they'd be like,
okay, well, we're gonna let the Democrats take care
of this section of the road, y'all are gonna build this.
The Republicans are gonna take care of this section.
And they used that competition,
they actually used the government against itself.
It's fantastic.
They talked to the farmers, they gathered the volunteers,
and they did all this over the winter.
They got the material together, everybody volunteering,
knowing that it's going to help society as a whole. Then in June they built it
in a day. In a day they built it. That alone is impressive. It's more impressive when you find out the road was
more than 300 miles long and ran from state line to state line across the
entire width of the state. It's called the River to River Road. Now with a feat
that amazing, obviously legend has taken hold, there are some accounts that say
it was built in an hour. That's probably not true, but by all accounts it was built
in a single day. Ran all the way across, across the entire state. That is a
parallel power structure and one of two things happens once one's built either
a it operates and the government structure ceases to be important or the
government changes it's what happened in this case when Iowa started numbering
their roads well the River to River Road I think it became six maybe seven the
The government took that idea and it made one little step towards the idea that people
wanted.
Made it more responsive.
It was very clear that the people wanted roads.
They built one.
Government's like, okay, I guess we need to do this now.
That's one way it can go down.
Or the government could just stay out of it.
A lot of programs came into being this way, not just infrastructure, but social programs.
There are a lot of signs that indicate the free lunch program or free breakfast program
at a lot of schools was modeled after something
that the Black Panthers did.
We'll never know because it came out a few years later.
And at that period in time, it's not like the establishment
would give the Black Panthers credit for, well, anything.
But there's a lot of indications that that's
the way it went down.
These little incremental steps and these parallel power
structures, they can be built.
today it's really easy. You don't have to reach out and get the media on your side because
you have social media. You can do it yourself. You can get those volunteers. You can change
society. If you have an idea like this, it's probably time to start building bridges so
you can build your road.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}