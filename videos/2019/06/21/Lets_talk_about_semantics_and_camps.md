---
title: Let's talk about semantics and camps....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=l6pYSp4OjNE) |
| Published | 2019/06/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the controversy around certain terms used for current detention facilities.
- Comparing the conditions inside detention facilities to forms of torture.
- Mentioning sleep deprivation, temperature extremes, solitary confinement, and more as torture methods.
- Noting instances of nooses in cells, sex abuse complaints, and attempts to destroy records.
- Bringing up the government lawyer's argument about not needing to provide basic necessities like beds, toothbrushes, toothpaste, and soap.
- Referring to experts like George Takei and Andrea Pitzer who identify the facilities as concentration camps.
- Explaining that concentration camps don't necessarily mean death camps but are a part of a network.
- Suggesting differentiating between German concentration camps and current ones by adding "Trump" to the name.
- Defending ICE's lack of providing soap due to limited supply and potential misuse by employees.
- Expressing concern about the dehumanizing treatment and potential future consequences.
- Ending with a reference to the manipulation and deception faced by detainees.

### Quotes

- "Sleep deprivation, temperature extremes, solitary without calls. These are all forms of torture."
- "I will defend ICE on one thing. I do understand why they're not quick to provide soap to the detainees."
- "You can trick them and tell them they're going to take a shower, right? It's America."

### Oneliner

Beau unpacks the comparison between current detention facilities and concentration camps, urging differentiation and reflecting on potential future repercussions.

### Audience

Activists, Advocates, Individuals

### On-the-ground actions from transcript

- Contact organizations supporting detainees (implied)
- Advocate for humane treatment of detainees (implied)
- Educate others on the conditions in detention facilities (implied)

### Whats missing in summary

The emotional impact and detailed examples can be better understood by watching the full transcript.

### Tags

#DetentionFacilities #ConcentrationCamps #HumanRights #DetaineeSupport #Advocacy


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're going to talk about wordplay.
We're going to talk about semantics,
because there are a lot of people that
are getting really upset over certain terms being used.
We need to figure out if that term is appropriate,
because saying that our current detention facilities mirror
detention facilities historically, well,
That's become an insult to the American pride
because there's no way that it can happen here, right?
So we're going to talk about it.
Are they concentration camps?
First, we're going to go over the conditions inside of them.
Now, the things that I'm going to list, all you have to do
is take the term that I say and add ice to it.
And you will find a source if you go through your favorite
search engine.
Sleep deprivation, temperature extremes, solitary without calls.
These are all forms of torture, by the way.
Nooses in cells, sex abuse complaints numbering in the thousands.
They're trying to destroy those records, by the way.
An unknown number of deaths.
The conditions themselves, the more basic conditions, contaminated food, rotten meat,
the lawyer for the government was just arguing that they don't really have an obligation
to provide them with sleep or beds or toothbrushes, toothpaste, soap.
Don't need to provide them with that.
Those are pretty horrible conditions.
Those are pretty horrible conditions.
What do the experts say?
Are they concentration camps?
George Takei, who was in a concentration camp as a guest, he says they're concentration
camps.
Andrea Pitzer, who literally wrote the book on concentration camps, one of the world's
foremost experts on the subject says they're concentration camps, says that
they fit nicely into the definition and wants people to know the same thing is
happening here and now in reference these detention facilities and the
German ones. Mike Godwin of Godwin's Law, the guy who
created an adage because he didn't like false comparisons to the Holocaust. He
says they're concentration camps. Now the argument is, but we're not killing anybody.
Well that's fine, we're not calling them death camps. The concentration camp
network was built first, the death camps were added later, yes, they are concentration camps but I do agree we
need to find some way to differentiate between the German ones and our own. I think that's important. So rather
than simply calling them concentration camps, we need to call them Trump concentration camps or the Trump network
concentration camps. In 20 years when those plaques go up like those that are
at the former Japanese internment camps, that's what they need to say. He wanted a
monument to himself. He wanted a legacy. That's why he wanted that stupid wall.
He can have his name on these. They're more fitting of his legacy. But I will
defend ICE on one thing. I do understand why they're not quick to provide soap to
the detainees. I get that. That makes sense. You've got a limited supply and I
am certain that the employees are taking it home with them because I would
imagine after working here you feel pretty dirty at the end of the day and
And if you don't, yeah, when it goes to the next step,
if it doesn't stop, you'll put them on boxcars.
You'll follow orders.
And the whole time, you'll say it's different,
because it can't happen here.
Hey, but I guess if they don't have soap, it'll be easier and they'll be more eager.
You can trick them and tell them they're going to take a shower, right?
It's America.
Anyway it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}