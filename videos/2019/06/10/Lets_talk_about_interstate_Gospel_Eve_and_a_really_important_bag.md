---
title: Let's talk about interstate Gospel, Eve, and a really important bag....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=OnsPQw23QsY) |
| Published | 2019/06/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau Garg says:

- Driving to Pensacola on I-10, encountering billboards with scripture, including one targeting abortion.
- Billboards in the South costing a few hundred bucks in rural areas and thousands in major markets.
- Hundreds of billboards all over the South, with millions spent on lobbying and informational literature for the pro-life cause.
- Questioning the effectiveness of the money spent on billboards and propaganda.
- Mentioning the artificial uterus, artificial womb technology successfully maturing a lamb at 24 weeks.
- Research on artificial wombs could potentially end the abortion debate.
- Suggesting a shift in funding towards research on artificial wombs if the goal is truly about life preservation.
- Noting the significant number of children in foster care and the thousands aging out of the system annually.

### Quotes

- "Maybe, if it really is about the preservation of life, that money should go to research on these artificial wombs."
- "I think for a lot of people this isn't about the preservation of life. I think it's about controlling other people."
- "Tens of thousands will age out of the system this year."

### Oneliner

Beau Garg questions the effectiveness of pro-life spending, suggesting research on artificial wombs as a solution, while pointing out the reality of children in foster care.

### Audience

Pro-choice advocates

### On-the-ground actions from transcript

- Support research on artificial wombs to potentially address the abortion debate (implied).

### Whats missing in summary

The full transcript provides a deeper exploration of the financial aspects and potential solutions to the abortion debate.

### Tags

#Abortion #ArtificialWombs #ProLife #FosterCare #Research


## Transcript
Well, howdy there, internet people, it's Bo Garg.
I drove to Pensacola the other day on I-10.
Now, of course, that means that I got my interstate gospel
along the way.
If you have not driven in the South,
there are billboards every few miles with scripture on it.
It's just part of living down here.
And as you drive towards Pensacola,
But it's a little different.
It isn't just the normal John 316.
Well, it gets real targeted real quick
because Pensacola is the location of one of Florida's
most, let's say, productive abortion clinics.
So along the way, as you get closer to Pensacola,
there are more and more messages targeted specifically
towards that.
And one of them was, what if Eve had had an abortion?
And man, it got me thinking immediately.
Because I follow Eve, not the Eve they're talking about, but it definitely got me thinking.
Now these billboards, a rural billboard out in the country, it runs just a few hundred
bucks.
As you get closer to major markets, it gets $10,000, $15,000 a month.
There are hundreds of these billboards all over the South, all over the country, really,
but they're more predominant in the South, hundreds.
So let's just say a thousand bucks a month offset the cost rule versus near the market.
Hundreds of thousands of dollars a month spent on billboards.
Then you have to think of the money for lobbying for the pro-life cause, the money to produce
the informational literature that they put out. Infomercials. All that stuff. It's millions.
Millions a month. What has it accomplished? What has it done? Nothing. Nothing. Not really
in the grand scheme of things, nothing, it's accomplished absolutely nothing.
It's interesting.
Now the Eve that I follow, well it's a bag, it's a bag, it's an artificial uterus, artificial
womb, it's crazy.
They have been able to successfully mature a lamb that if it was in human gestation,
it would be 24 weeks, it's crazy, brought it all the way to maturity, no problems, they've
done it a lot, 24 weeks, 97% of abortions occur 20 weeks or before, they are super close and
they're inching their way towards it. Now if you're in the pro-life crowd you
certainly saw that ectopic pregnancy that was removed went viral that image
that was at nine weeks removed intact so the techniques there technology is
almost there. I wonder what's holding them back. Research, which costs what? Money. Millions
of dollars a month. On what amounts to propaganda that has so far proven to have been ineffective.
Maybe, if it really is about the preservation of life, that money should go to research
on these artificial wombs.
Maybe Eve.
Maybe that's a sign that it's named that.
So that in its own way could end abortion, at least the part that is said to be the problem.
know the ending of what they consider human life. It's extracted, placed in the
bag, allowed to mature, then it can be adopted, put into a circus, whatever it is
you plan on doing with them. No longer the mother's responsibility. See there's
the rub, because I think for a lot of people this isn't about the preservation
of life. I think it's about controlling other people. I can be wrong, and if I am wrong
we should probably see a shift in funding. But we'll see. We'll see. As we head down
On that road though, it might be a good thing to remember that currently there are 400,000
or so children in foster care.
About 100,000 are adoptable at this moment.
Tens of thousands will age out of the system this year.
Well, we'll see.
We'll see what happens.
Anyway, it's just a thought.
have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}