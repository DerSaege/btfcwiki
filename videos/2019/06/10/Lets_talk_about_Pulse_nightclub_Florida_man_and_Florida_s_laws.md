---
title: Let's talk about Pulse nightclub, Florida man, and Florida's laws....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=iXN8eypHbVQ) |
| Published | 2019/06/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Raises concern about pastors planning to go to the Pulse nightclub in Florida on the anniversary of the attack.
- Expresses dismay at the pastors' history of celebrating deaths and saying gay people don't deserve to live.
- Warns that the pastors' actions could be seen as a threat of bodily harm and instigating violence.
- Explains the concept of "Florida man" and how the state's unique environment influences behavior.
- Quotes the law allowing deadly force in certain circumstances to prevent harm or forcible felony.
- Advises the pastors to reconsider their plans as their actions could legally lead to harm in Florida.
- Urges them to call off their trip and conduct their activities in a more appropriate setting.
- Condemns the pastors' behavior as morally, ethically, and legally wrong, potentially leading to forcible felonies.
- Criticizes the pastors for mocking the deaths of those murdered at Pulse nightclub.
- Ends with a strong statement against the pastors and their actions, questioning their alignment with Christian values.

### Quotes

- "Y'all need to take this to another state."
- "What you're doing is wrong. It's wrong morally. It's wrong on every level."
- "And in Florida, oh, it's legally wrong."
- "You're going to travel across the country so you can have your little bigoted conference and mock the deaths of people who were murdered? Yeah, that's what Jesus would do."

### Oneliner

Beau warns pastors planning to visit Pulse nightclub in Florida, criticizing their morally and legally wrong actions and urging them to call it off before someone gets hurt.

### Audience

Community members, activists.

### On-the-ground actions from transcript

- Contact local authorities to raise awareness about the pastors' threatening behavior (implied).
- Support LGBTQ+ organizations in Florida to ensure a safe environment during sensitive anniversaries (implied).

### Whats missing in summary

The emotional intensity and passion in Beau's delivery, which adds power to his message, are missing in the summary.

### Tags

#Florida #PulseNightclub #LGBTQ+ #Threat #CommunitySafety


## Transcript
Well, howdy there, internet people, it's Bo again.
I don't do hot takes.
I don't do reaction videos, it's not my thing.
However, just become aware of something
and I don't want anybody to get hurt, okay?
I am not up on the whole saga here.
I have no clue why I have a pumpkin sitting back there
to be completely honest,
but this is something that needs to be addressed.
There are some pastors, and I use that term loosely,
that plan on coming down here to Florida and going to the Pulse nightclub on the anniversary of the attack,
and instigating something, taking them up on their dare, quote, picking a fight, starting an argument,
after threatening people, and their videos, if you look through their videos in the past, they are ridiculous.
Celebrating the deaths of people, saying that gay people, you know, don't deserve to live,
to live and celebrating the executioners and saying they belong six feet under and all of
this stuff. It is something that a reasonable person would believe is a threat, a threat of bodily harm.
They're going to go down there and instigate something. I am certain that you clowns have
some image in your head of what a gay man is like. There is nothing prohibiting a gay man
from also being Florida man and overseas viewers have asked me to explain what
Florida man is I haven't done a video on it Florida man is a unique creature
that can only exist in a state where the wildlife the weather the water the
terrain the bugs everything including the people a lot of times are trying to
kill you and our legislation reflects that in that vein I would like to read
A person is justified in using or threatening to use deadly force if he or she reasonably
believes that using threatening to use such force is necessary to prevent imminent death
or great bodily harm to himself or herself or another person, or to prevent the imminent
commission of a forcible felony.
I've looked in your little groups, a lot of what you're talking about, that's a forcible
felony.
Y'all need to take this to another state.
Y'all are traveling across the country to go to the one place where this behavior can
legally get you shot.
I'm telling you this because I don't want to see anybody hurt.
What you're doing is wrong.
It's wrong morally.
It's wrong on every level.
And in Florida, oh, it's legally wrong.
There is no reasonable person that would look at those videos and think you would not cause
them great bodily harm.
And if you act any of the ways that I've seen talked about, oh yeah, those are forcible
felonies.
Y'all need to call this off.
You need to take this to a state where it might be tolerated.
This is not going to go well.
What I'm telling you is because I don't want to see anybody hurt.
And as far as you pastors go, you're the reason people don't go to church anymore.
You're going to travel across the country so you can have your little bigoted conference
and mock the deaths of people who were murdered?
Yeah, that's what Jesus would do.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}