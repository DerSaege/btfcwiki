---
title: Let's talk about the right and left in America....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ufzwfNWOmZ4) |
| Published | 2019/06/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Points out the incorrect usage of "left" and "right" in the US and explains common terms.
- Differentiates between Democrats as liberals and not leftists, more center-right.
- Mentions the lack of a viable left-wing party in the United States.
- Provides an example of a Facebook post and tweet supporting amnesty for undocumented individuals.
- Acknowledges potential backlash for his statement on amnesty.
- References Ronald Reagan's stance on amnesty, contrasting it with President Trump's wall policy.
- Criticizes the right for not having a hard line like the left, attributing it to the lack of theocratic elements.
- Encourages not to shy away from radical ideas and to defend them.
- Suggests that without the radical left, society tends to move further right.
- Emphasizes the importance of advocating for a fair and just society to move towards the left globally.

### Quotes

- "Without the radical left, well, we're all just right-wing."
- "History has a well-known leftist bias."

### Oneliner

Beau clarifies the misusage of "left" and "right," distinguishing Democrats as liberals, not leftists, and advocates for defending radical ideas to prevent society from moving further right.

### Audience

Activists, Progressives, Liberals

### On-the-ground actions from transcript

- Defend radical ideas when you hear them (advocated)
- Advocate for a fair and just society (advocated)

### Whats missing in summary

The full transcript includes a deep dive into political terminologies, historical biases, and the importance of advocating for radical ideas to prevent society from shifting further right.

### Tags

#PoliticalTerminology #Advocacy #LeftVsRight #RadicalIdeas #SocialJustice


## Transcript
Well, howdy there, internet people.
It's Bo again.
So tonight we are going to talk about the right and left,
at least in relation to how we use the terms commonly
in the US.
Generally, we use them incorrectly.
But we're going to go with the common usage here.
I've often said that we do not have a viable left-wing party
in the United States.
and it tends to confuse people because they point to the Democrats.
The Democrats are not leftists.
They're liberals.
They're center-right.
Europeans will back me up on this.
Now, as an illustration, I made a Facebook post and a tweet today.
And I'm going to read it to you.
I believe in the idea of amnesty for those who have put down roots
lived here, even though some time back they may have entered illegally.
They brought with them the courage and the values of family, work, and freedom.
Let us pledge to each other that we can make America great again."
Now I haven't checked, but I'm sure that if it hasn't happened yet, somebody will show
up to call me a leftist over this statement.
amnesty for illegals, that is a radical left idea
today.
Now to those who sent me messages saying,
oh, that's not how you talk and that's not how you write,
what is this that I'm reading?
You guys were right.
Quote, two quotes actually from the same guy
where the break is, that's the separation of the two quotes.
Ronald Reagan, right-wing conservative darling, those are his words, amnesty, and
yeah, it is very funny to me that President Trump, the man who wants to
build a wall, stole his Make America Great Again marketing campaign from the
guy who said, Mr. Gorbachev, tear down this wall. That's funny. But the point remains.
This was the Republicans. Can you imagine a Republican saying this today? Can you imagine
a Democrat saying this today? The U.S. left. No, it's too radical. It's too upsetting.
The reason we have moved this far right is because the right is very very very good at
defending its own.
They don't punch right, ever.
They punch down.
So what happens is some crazy right winger says something that is just completely insane
like let's build a wall and other right wingers are like well you know that's not so bad and
The idea gets mainstreamed.
So the bulk of the right wing moves closer to this crazy idea.
Now those on the American left, they're over there going, nobody's going to believe that.
Nobody's going to want to do that.
That's stupid.
We don't have a country of idiots.
And then the moderates, well, they are in search of that ever elusive middle ground,
that compromise.
We're not going to build a wall.
What about offense? A barrier? And here we are. That's how it works. The left doesn't
stick to its guns. They don't. They don't have that hard line like the right does, mainly
because the left doesn't have a theocratic element to it, the way the right wing does.
They don't have a book that they can point to and say, the book, we got to live by this
and change whatever it is they want it to say as it suits their needs.
The point of all of this is don't be afraid to say the radical ideas.
afraid to say the radical things. And when you see one that it may not be good today,
but it may be good in a few years, don't discount it. Don't throw that person under the bus.
Don't get rid of that idea too quickly. Because without the radical left, well, we're all
just right-wing. We all move further and further to the right. Now the way we use
those terms in the US is incorrect because there's more depth to it than
just left and right. An authoritarian right-wing regime is almost indistinguishable
from an authoritarian left-wing regime, except in rhetoric. The
The actions are pretty much the same.
A far-right libertarian and bi-libertarian, I don't mean the libertarian party in the
US, I mean anti-authoritarian, and far-left anti-authoritarians, it's pretty much the
same except in rhetoric.
The ideas are more congruent than I think a lot of people would like to believe.
Because we've been programmed left and right.
When it's not left versus right, it's authority versus you.
Anyway, defend the radical ideas when you hear them.
a long enough timeline, the world as a whole moves to the left.
History has a well-known leftist bias, but we're delaying that process on our own.
We're slowing it by not advocating for a fair and just society.
Anyway, it's just a thought.
y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}