# All videos from June, 2019
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2019-06-30: Let's talk about what we can learn from AOC getting caught in a PR stunt.... (<a href="https://youtube.com/watch?v=coZzVfYiNf8">watch</a> || <a href="/videos/2019/06/30/Lets_talk_about_what_we_can_learn_from_AOC_getting_caught_in_a_PR_stunt">transcript &amp; editable summary</a>)

Conservative news sites spread a false story about AOC visiting a detention center, revealing the dangers of manufactured opinions and the importance of personal fact-checking.

</summary>

"This is how opinions are manufactured."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Conservative news sites are spreading a misleading story about AOC visiting an empty parking lot and pretending it's a detention center.
The photos in question were actually taken a year before AOC became a representative, during a large protest at a detention center.
No children were visible at the fence line because they were not allowed that close to the edge of the detention center.
The story originated from InfoWars and was not fact-checked by reputable outlets.
Beau urges people to fact-check information themselves, as news sites may push false narratives to fit their agenda.

Actions:

for media consumers,
Fact-check stories spread by news sites (suggested)
Be vigilant about misinformation and false narratives (implied)
</details>
<details>
<summary>
2019-06-28: Lets talk about my response to Carey's #ThisIsVirtue challenge.... (<a href="https://youtube.com/watch?v=1Yf8P5APf5g">watch</a> || <a href="/videos/2019/06/28/Lets_talk_about_my_response_to_Carey_s_ThisIsVirtue_challenge">transcript &amp; editable summary</a>)

Beau challenges viewers to recite the "New Colossus" sonnet and participate in the "This Is Virtue challenge" to remind people of American virtues, encouraging them to take action and use their voices against injustice.

</summary>

"Give me your tired, your poor, your huddled masses yearning to breathe free."
"What are you going to tell them? Are you going to tell them that you used your voice?"
"The idea is to remind people what American virtues are, what they're supposed to be."

### AI summary (High error rate! Edit errors on video page)

Recites "New Colossus" sonnet, focusing on America's promise of welcoming immigrants.
Raises questions about what people are doing to uphold American virtues and values.
Challenges people to participate in the "This Is Virtue challenge" by reciting the sonnet and sharing it on social media with the hashtag #thisisvirtue.
Encourages individuals to use their voices and be proactive in standing against policies that harm vulnerable populations.
Points out the importance of not getting lost in trivial arguments when significant issues are at stake.
Calls for action to remind people of American virtues and the country's promised values.
Urges viewers to think about how they will answer future generations when asked about their actions during this era.

Actions:

for social media users,
Recite and share the "New Colossus" sonnet on social media with the hashtag #thisisvirtue (suggested).
</details>
<details>
<summary>
2019-06-28: Let's talk about the lawsuit brought by federal workers.... (<a href="https://youtube.com/watch?v=Zg7W3rmR2bo">watch</a> || <a href="/videos/2019/06/28/Lets_talk_about_the_lawsuit_brought_by_federal_workers">transcript &amp; editable summary</a>)

Federal asylum officers challenge Trump's dictatorial control over the "remain in Mexico policy," condemning it as fundamentally immoral.

</summary>

"The fact that they oppose Trump is not news. What is news is who brought the lawsuit."
"It most certainly is."
"It's intentionally barring asylum officers from doing their job."
"It's about stopping people and then slowing down the rate at which we process asylums claims so they die."
"People who are used to making life and death decisions. People who are totally comfortable sending people back to squalor. They're saying that this is immoral."

### AI summary (High error rate! Edit errors on video page)

Lawsuit brought by federal asylum officers, not politicians like former Secretary of Homeland Security and James Clapper.
Federal asylum officers listen to harrowing stories daily and make life-and-death decisions on asylum claims.
Lawsuit is against the "remain in Mexico policy" or migrant protection protocols, considered a violation of international and domestic law.
Migrant protection protocols were dictated by Trump and cause widespread violations of rights.
Asylum officers are barred from asking asylum seekers if they fear for their safety in Mexico under the current system.
Policy intentionally slows down asylum claims processing to the point where people suffer and die.
Beau criticizes Trump's actions as dictatorial, bypassing constitutional checks and controlling concentration camps.
Federal asylum officers challenge Trump's actions, despite their usual role in making tough decisions.
They condemn the policy as immoral and fundamentally against the moral values of the nation.

Actions:

for advocates for human rights,
Support organizations advocating for asylum seekers' rights (implied)
Raise awareness about the violations caused by the "remain in Mexico policy" (implied)
Advocate for policy changes to protect asylum seekers (implied)
</details>
<details>
<summary>
2019-06-27: Let's talk about the positive virtue signaling.... (<a href="https://youtube.com/watch?v=3rO_-oCBCNA">watch</a> || <a href="/videos/2019/06/27/Lets_talk_about_the_positive_virtue_signaling">transcript &amp; editable summary</a>)

Beau talks about the positive impact of virtue signaling, the failure of compassionate campaigns, and the need for morality-focused campaigns to address critical issues like abuse and inhumane treatment.

</summary>

"We need more virtue signaling, a lot of it."
"I believe that the average American does not actually want kids to be sexually assaulted."
"We need campaigns that focus on the morality of what's actually happening."
"We're funding rape rooms."
"We need to show America what a virtue is."

### AI summary (High error rate! Edit errors on video page)

Virtue signaling has positive aspects and is a significant part of human communication.
The ice bucket challenge, despite criticism of being empty, raised over a hundred million dollars.
Compassionate campaigns supporting asylum seekers are failing due to how they're framed, not their message.
Campaigns framed in opposition to Trump may not produce the best results.
Beau is concerned about stopping the abuse of kids in detention facilities and preventing drownings.
Loyalty to political party often outweighs loyalty to principles, allowing wrong actions to be discounted.
Beau calls for campaigns reminding people of America's virtues and what they should stand for.
He finds it hard to believe that half the population tolerates sexual assault on kids and inhumane treatment of children in detention.
Beau believes most Americans do not want kids to be abused but may justify it due to loyalty to Trump.
Campaigns should focus on the morality of the situation and not political arguments.

Actions:

for advocates for social change,
Develop and support campaigns reminding people of America's virtues (implied)
Share suggestions for campaigns focused on addressing critical issues like abuse and inhumane treatment (implied)
</details>
<details>
<summary>
2019-06-27: Let's talk about the people who would risk their children to immigrate.... (<a href="https://youtube.com/watch?v=OMrHm4GFvpg">watch</a> || <a href="/videos/2019/06/27/Lets_talk_about_the_people_who_would_risk_their_children_to_immigrate">transcript &amp; editable summary</a>)

Beau dives into history to explain the courage of immigrants and challenges individuals to rethink their views on immigration and refugees.

</summary>

"People are still taking that risk today."
"The spiritual sons and daughters of the people who built this country are the people dying down there on the border."
"No self-evaluation will ever be as valuable or as accurate as an enemy's evaluation of you."
"Who if you have the courage."
"You'd be welcoming these people as new additions to the American tapestry, not crying and saying that their children deserve to drown."

### AI summary (High error rate! Edit errors on video page)

Beau was asked a question that led him to research and provide an insightful answer about the kind of people who immigrate.
He shares a first-person account from the 1700s detailing the harsh conditions immigrants faced, similar to present-day situations.
Immigrants arriving before the mid-1800s faced challenges like being purchased off ships and enduring sickness while waiting to come ashore.
Families often had to give their sick children to strangers to save their lives during immigration.
The conditions for immigrants improved slightly in the 1840s but were still challenging.
Beau points out that throughout history, people came to the U.S. as refugees fleeing various crises.
He questions individuals who suggest fixing their corrupt home countries instead of seeking better opportunities elsewhere.
Beau criticizes those who blame immigrants for the country's issues while failing to address systemic problems like lack of basic necessities and democracy concerns.
He challenges the notion of leaving the country if one has issues with it, citing the true embodiment of the American spirit in those enduring hardships at the border.
Beau underscores the courage and spirit of immigrants and urges embracing them as additions to the American tapestry rather than condemning them.

Actions:

for advocates, activists, citizens,
Welcome and support immigrants and refugees as new additions to your community (implied)
Challenge misconceptions about immigrants and refugees in your circles (implied)
Advocate for humane treatment and support for those seeking refuge (implied)
</details>
<details>
<summary>
2019-06-26: Let's talk about people drowning on the border and stronger locks.... (<a href="https://youtube.com/watch?v=GhWZqhAPZXU">watch</a> || <a href="/videos/2019/06/26/Lets_talk_about_people_drowning_on_the_border_and_stronger_locks">transcript &amp; editable summary</a>)

Beau addresses the misrepresentation of drowned individuals as "illegal immigrants invading our house" and reveals the harsh reality of their situation, condemning US policies and acknowledging America's role in the tragic events.

</summary>

"We set their house on fire and we're taking pot shots at them as they leave, and then when they die, well, it happens."
"If you support these policies that are going on down at the border, it's this moral equivalent, is if you took that little girl, put your boot on her head, and held her head under that water until the bubble stopped, you killed her."
"We killed them. We killed that little girl. We killed her father."
"That's America today."
"We live here, we won the geographic lottery, and we can look at these things that happen in other countries and act like, well, it doesn't have anything to do with us."

### AI summary (High error rate! Edit errors on video page)

Addresses the portrayal of drowned individuals as "illegal immigrants invading our house."
Explains that these people were fleeing from a country, El Salvador, with a corrupt government supported by the US.
Describes El Salvador as being overrun by a gang that originated in the United States.
Mentions issues in El Salvador leading to the ban on a police force as part of a peace agreement.
Talks about how the gang, with US-trained leadership, became a transnational threat known as MS-13.
Criticizes policies at the border and compares supporting them to committing a heinous act.
Points out the role of the US in the situation and the consequences faced by the fleeing individuals.
Emphasizes the luck and geographic advantage enjoyed by people in the US.
Condemns the actions that led to the deaths of individuals including a little girl and her family.
Concludes by reflecting on the impact of these events.

Actions:

for policy advocates,
Support organizations aiding asylum seekers (suggested)
Advocate for policy changes regarding immigration and border control (implied)
</details>
<details>
<summary>
2019-06-25: Let's talk about the evils of virtue signaling.... (<a href="https://youtube.com/watch?v=pCTllwHd5RQ">watch</a> || <a href="/videos/2019/06/25/Lets_talk_about_the_evils_of_virtue_signaling">transcript &amp; editable summary</a>)

Virtue signaling, both effective and divisive, influences societal norms and political rhetoric, shaping perceptions and policies.

</summary>

"Virtue signaling is extremely effective. It causes societal change."
"Calling out virtue signaling can also be a form of virtue signaling."
"The normalization of inhumane practices, such as in ICE's statement on immigration policy, can be seen as a form of virtue within certain groups."

### AI summary (High error rate! Edit errors on video page)

Virtue signaling is the expression of a positive social behavior to your in-group, not necessarily an action rooted in compassion.
Virtue signaling can be effective in causing societal change, even though it may come across as empty or hollow.
Examples of virtue signaling include the creation of the "warrior cop" and gestures of loyalty to certain dictators in history.
Calling out virtue signaling can also be a form of virtue signaling, conveying superiority and rationality within a group.
Experts have expressed concern over how Trump's rhetoric normalized dehumanization and spread through political parties and government agencies.
The normalization of inhumane practices, such as in ICE's statement on immigration policy, can be seen as a form of virtue within certain groups.
Conflicting views on immigration policies and human trafficking often overlook the root causes and focus solely on containment rather than solutions.
Society needs positive virtue signaling to steer towards a more humane society by addressing underlying issues and promoting compassion.

Actions:

for activists, advocates, observers,
Challenge dehumanizing narratives within your community (exemplified)
Advocate for addressing root causes of issues (implied)
Promote compassionate solutions in policy and discourse (exemplified)
</details>
<details>
<summary>
2019-06-24: Let's talk about the 300 kids.... (<a href="https://youtube.com/watch?v=aJ_5SO1VR_w">watch</a> || <a href="/videos/2019/06/24/Lets_talk_about_the_300_kids">transcript &amp; editable summary</a>)

300 children removed from overcrowded Border Patrol facility to another, facing neglect and inhumane conditions, a crisis overlooked on American soil.

</summary>

"Do you want to wait until we have a bunch of deaths?"
"This is a travesty. This is ridiculous."
"They're livestock. They're a product."
"This is not even surprising anymore."
"They have no idea where their family is."

### AI summary (High error rate! Edit errors on video page)

300 children were removed from a Border Patrol facility in Clint, Texas, designed for 100 people but housed 350.
Visitors described the facility as having a stench.
The children lacked adequate food, hygiene items like soap and toothpaste, and hadn't showered in almost a month.
Their clothing had bodily fluids ranging from breast milk to urine.
Many children don't know the whereabouts of their families.
Flu outbreaks were present, and there was a lack of beds.
Border Patrol had younger children taking care of toddlers.
The children have not been rescued; they were moved to another facility, which Beau calls a "Trump concentration camp."
Deaths in concentration camps were often due to disease and lack of food caused by poor hygiene.
Beau questions when people will call their senators and address the crisis.
Children are being treated like livestock, not as human beings.
The situation is so dire that it's no longer shocking, which Beau finds tragic.
The media's portrayal of the children being "removed" is misleading; they remain in the custody of the same agency that neglected them.
These children are seen as numbers on a board, not as individuals facing a humanitarian crisis.

Actions:

for advocates for human rights,
Contact your senator to demand immediate action to address the inhumane conditions faced by children in Border Patrol facilities (suggested)
Advocate for policy changes to ensure proper care and treatment of detained children (implied)
Organize or participate in protests to raise awareness about the crisis and pressure authorities for change (implied)
</details>
<details>
<summary>
2019-06-24: Let's talk about a ship getting attacked.... (<a href="https://youtube.com/watch?v=4FRpZZPcQfs">watch</a> || <a href="/videos/2019/06/24/Lets_talk_about_a_ship_getting_attacked">transcript &amp; editable summary</a>)

Beau reveals the truth behind the Vietnam War, a conflict built on deception and lies, leading to immense loss of life and a stark reminder of the consequences of manufactured narratives.

</summary>

"The worst war in American history was fought over a lie."
"All of those graves were filled over a lie."
"The Vietnam War was based on a lie."

### AI summary (High error rate! Edit errors on video page)

Tonight, I'm sharing links on a topic most Americans might not know or believe.
The U.S. Senate repealed the Gulf of Tonkin Resolution on June 24, 1970.
This resolution authorized the Vietnam War based on a false incident.
The Gulf of Tonkin incident was used to justify the war, but the truth was different.
Operation De Soto and Opland 34A were running simultaneously in Vietnam.
The North Vietnamese attack on the Maddox was a response to a previous US action.
Fabricated evidence was used to justify the Vietnam War.
Many lives were lost based on this lie.
The war monument stands as a reminder of the cost of this deception.
Similar pretexts for war have been sought since then, like tying Iran to al-Qaeda post-9/11.

Actions:

for history buffs, truth-seekers, activists,
Share this history lesson with others (suggested)
Educate yourself and others on the truths behind past conflicts (implied)
</details>
<details>
<summary>
2019-06-23: Let's talk about what Radio London can teach us about today.... (<a href="https://youtube.com/watch?v=-mMCQRiuB9g">watch</a> || <a href="/videos/2019/06/23/Lets_talk_about_what_Radio_London_can_teach_us_about_today">transcript &amp; editable summary</a>)

Allies' WWII broadcasts inspire messages of hope amidst current immigrant community support and LAPD's refusal to cooperate, reminding that history rhymes with present challenges.

</summary>

"History doesn't repeat. It rhymes."
"The front line is everywhere."
"No, it's not the same as Nazi Germany."
"We are in occupied territory."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Allies in World War II broadcasted messages to the resistance in occupied territories using various frequencies and codes to provide information and hope.
A reenactment of one of those broadcasts is presented, including personal messages, music, and messages of hope.
Messages of hope address the targeting of immigrant families by the federal administration, providing support and information to communities.
Various mayors, including San Francisco, L.A., Atlanta, Denver, Baltimore, and D.C., stand in solidarity with immigrant communities and against inhumane practices.
LAPD released a statement refusing to cooperate with ICE raids, distancing themselves from such actions.
The president's threats and ultimatums are compared to past historical events, with a reminder that history rhymes but does not repeat itself.

Actions:

for community members,
Stand in solidarity with immigrant communities (implied)
Provide support and information to immigrant families (implied)
Refuse to cooperate with inhumane practices (implied)
</details>
<details>
<summary>
2019-06-22: Let's talk about a golden moment with Iran.... (<a href="https://youtube.com/watch?v=alstUioeuG0">watch</a> || <a href="/videos/2019/06/22/Lets_talk_about_a_golden_moment_with_Iran">transcript &amp; editable summary</a>)

Understanding Iran's history is key to making informed decisions for a peaceful future, necessitating the strengthening of its democratic side while keeping conflict at bay.

</summary>

"War isn't good for anybody."
"This cluster that was caused by the president pulling out of the deal may actually be a blessing in disguise."
"We're at this golden moment where we can actually take another step forward."
"In order to say that though, we've got to keep the hawks at bay."
"Strengthening the democratic side of the government there needs to be the primary mission."

### AI summary (High error rate! Edit errors on video page)

Iran's history dating back to the late 40s and early 50s is vital to understanding current events.
In 1953, the CIA orchestrated the overthrow of Iran's elected government in Operation Ajax.
The Shah consolidated power with the help of the brutal intelligence agency, SAVAK.
SAVAK operated with vague laws, no legal representation, and severe consequences like imprisonment or execution.
Resentment grew among the populace, particularly targeting dissidents like Azerbaijanis and Kurds.
Economic prosperity from oil sales in the early 70s did not meet expectations, leading to dissatisfaction.
Rising Islamic nationalism in the Middle East fueled revolutionary sentiments.
The 1979 revolution caught the intelligence community off guard, ushering in the current government.
Iranians view their government as a necessary defense against the West, providing relative prosperity and regional power.
Strengthening the democratic side of the Iranian government is key to improving relations and avoiding conflict.

Actions:

for policymakers, activists, concerned citizens,
Strengthen the democratic side of the Iranian government by supporting diplomatic efforts and engaging with moderate voices (suggested).
Advocate for peaceful resolutions and diplomacy over aggression in dealing with Iran (implied).
</details>
<details>
<summary>
2019-06-21: Let's talk about semantics and camps.... (<a href="https://youtube.com/watch?v=l6pYSp4OjNE">watch</a> || <a href="/videos/2019/06/21/Lets_talk_about_semantics_and_camps">transcript &amp; editable summary</a>)

Beau unpacks the comparison between current detention facilities and concentration camps, urging differentiation and reflecting on potential future repercussions.

</summary>

"Sleep deprivation, temperature extremes, solitary without calls. These are all forms of torture."
"I will defend ICE on one thing. I do understand why they're not quick to provide soap to the detainees."
"You can trick them and tell them they're going to take a shower, right? It's America."

### AI summary (High error rate! Edit errors on video page)

Addressing the controversy around certain terms used for current detention facilities.
Comparing the conditions inside detention facilities to forms of torture.
Mentioning sleep deprivation, temperature extremes, solitary confinement, and more as torture methods.
Noting instances of nooses in cells, sex abuse complaints, and attempts to destroy records.
Bringing up the government lawyer's argument about not needing to provide basic necessities like beds, toothbrushes, toothpaste, and soap.
Referring to experts like George Takei and Andrea Pitzer who identify the facilities as concentration camps.
Explaining that concentration camps don't necessarily mean death camps but are a part of a network.
Suggesting differentiating between German concentration camps and current ones by adding "Trump" to the name.
Defending ICE's lack of providing soap due to limited supply and potential misuse by employees.
Expressing concern about the dehumanizing treatment and potential future consequences.
Ending with a reference to the manipulation and deception faced by detainees.

Actions:

for activists, advocates, individuals,
Contact organizations supporting detainees (implied)
Advocate for humane treatment of detainees (implied)
Educate others on the conditions in detention facilities (implied)
</details>
<details>
<summary>
2019-06-20: Let's talk about the bigger question in the Bella Thorne scandal.... (<a href="https://youtube.com/watch?v=nisumVzhGSs">watch</a> || <a href="/videos/2019/06/20/Lets_talk_about_the_bigger_question_in_the_Bella_Thorne_scandal">transcript &amp; editable summary</a>)

Beau addresses the unfair stigma surrounding leaked intimate photos, advocating for a shift in societal attitudes towards privacy and normal behavior.

</summary>

"She's a victim of a crime."
"This is extremely common behavior."
"You're talking about consenting adults exchanging images of themselves."
"She didn't do anything wrong, and she certainly didn't do anything wrong simply because of the way she was dressed."
"Don't turn this into a scandal."

### AI summary (High error rate! Edit errors on video page)

Bella Thorne's intimate photos were hacked and threatened to be released by a blackmailer.
Thorne took control by posting the photos herself on Twitter.
Whoopi Goldberg criticized Thorne, implying celebrities should expect hacking if they take such photos.
Beau questions the victim-blaming mentality behind Goldberg's statement.
He points out the unfair societal expectations placed on celebrities.
The scandal around Thorne's photos stems from unrealistic expectations of public figures.
Beau mentions the commonality of taking such photos, especially among young people.
He criticizes the stigmatization of normal behavior when it comes to celebrities.
There's a focus on the pressure for public figures to appear perfect all the time.
Beau suggests removing the scandalization of leaked photos to combat blackmail and violations.

Actions:

for social media users,
Challenge societal norms around privacy and body autonomy (implied)
Advocate for the destigmatization of common behaviors (implied)
Support individuals affected by privacy violations (implied)
</details>
<details>
<summary>
2019-06-20: Let's talk about Iran, lies, and confusion.... (<a href="https://youtube.com/watch?v=ASaT7RHA_Ow">watch</a> || <a href="/videos/2019/06/20/Lets_talk_about_Iran_lies_and_confusion">transcript &amp; editable summary</a>)

Beau predicts war, questions credibility, rejects war for political gain, and calls for valuing lives over politics.

</summary>

"You're not going to be able to fool anybody this time."
"That accusation is a confession."
"Get on those instead of standing on the graves of American troops and innocent Iranians."

### AI summary (High error rate! Edit errors on video page)

Predicts a storm coming and addresses the urgency of discussing Iran.
Mentions the inevitability of war when discussed in a previous video.
Questions the credibility of claims linking Iran to sponsoring Al-Qaeda.
Criticizes the lack of believability in the narrative presented to Congress.
Raises doubts about the U.S. Navy's claim of a drone being shot down in international waters.
Reminds of past incidents like the U.S. shooting down Iran Air 655 without consequences.
Rejects the idea of going to war over the recent drone incident.
Calls out politicians, including Donald Trump, for using Iran as a tool for political gain.
Challenges the idea of going to war for political motives at the expense of American soldiers and innocent Iranians.
Ends with a thought-provoking message about not sacrificing lives for political gains.

Actions:

for concerned citizens,
Stand against war for political gain (implied)
</details>
<details>
<summary>
2019-06-19: Let's talk about mass removals and freedom.... (<a href="https://youtube.com/watch?v=uCt60ViLh7M">watch</a> || <a href="/videos/2019/06/19/Lets_talk_about_mass_removals_and_freedom">transcript &amp; editable summary</a>)

The Statue of Liberty, removals, and the call for amnesty – a poignant reminder of the humanity behind immigration policies.

</summary>

"You're going to see they're real people."
"Amnesty, amnesty, amnesty. the law. Amnesty."
"Let's help the tempest-tossed, the huddled masses."

### AI summary (High error rate! Edit errors on video page)

Statue of Liberty arrived in New York in 1885, without the famous inscription.
The President plans to target undocumented people for removal, including those who committed no other crime than crossing a line.
ICE is hesitant about this approach due to bad optics and potential public backlash.
Removals are typically done at job sites to avoid public scrutiny and dehumanization.
The impact of seeing families torn apart during removals will be significant.
The punishment for undocumented immigrants seems harsh compared to other violations like watching a movie without a ticket.
Children of undocumented parents face dire situations if their families are targeted for removal.
Cheering on mass removals contradicts the fear of government overreach.
Amnesty is suggested as a humane solution to prevent the destruction of millions of lives.
Prioritizing removals over going after real criminals like human traffickers is questioned.

Actions:

for advocates, activists, citizens,
Advocate for humane immigration policies (implied)
Support organizations working to protect undocumented individuals (implied)
Educate others on the realities of immigration enforcement (implied)
</details>
<details>
<summary>
2019-06-18: Let's talk about my friend who was at the shooting today.... (<a href="https://youtube.com/watch?v=gxEg2-bWM9M">watch</a> || <a href="/videos/2019/06/18/Lets_talk_about_my_friend_who_was_at_the_shooting_today">transcript &amp; editable summary</a>)

Beau challenges the myth of the "good guy with a gun" and addresses the cultural issues around gun ownership, advocating for a shift in mindset away from toxic masculinity.

</summary>

"Guns don't kill people. People kill people."
"The gun doesn't make the man."
"It's a bandaid on a bullet wound."

### AI summary (High error rate! Edit errors on video page)

Beau shares a personal experience of a friend involved in a shooting incident at a gun-free zone.
The friend secured his weapon in the car, heard gunfire, and moved to a safer location instead of engaging the threat.
Beau challenges the myth of the "good guy with a gun," stating that untrained individuals are more likely to die in combat than successfully intervene.
He mentions the prevailing myth of guns not killing people, but rather people killing people, and how it leads to a dangerous mindset.
Beau refers to the conditioning required for individuals to kill and how only 4% of people are naturally wired for it.
He talks about the flawed belief that arming more people will stop mass shootings and dismantles the idea of a "good guy with a gun."
Beau criticizes the cultural glorification of guns and stresses the need to address the root cultural issues rather than rely on legislation.
He mentions the case of the Parkland cop and the different standards applied due to training and duty to intervene.
Beau concludes by stressing that the concept of a "good guy with a gun" is part of the problem, not the solution, and signifies toxic masculinity.

Actions:

for advocates for cultural change,
Educate others on the dangers of the "good guy with a gun" myth (suggested)
Challenge the glorification of guns in culture (implied)
</details>
<details>
<summary>
2019-06-17: Let's talk about flags, promises, and logos.... (<a href="https://youtube.com/watch?v=vB8I9uQTTx0">watch</a> || <a href="/videos/2019/06/17/Lets_talk_about_flags_promises_and_logos">transcript &amp; editable summary</a>)

Beau questions the true symbolism of the American flag amidst debates over flag burning, urging patriots to actively uphold promises of equality and justice to give meaning to national symbols.

</summary>

"If you don't act on those promises, this means nothing."
"The person burning this flag, they didn't disgrace it. You did when you failed to keep the promises."
"It's just a thought. Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Questions the symbolism of flag burning as a constitutional amendment banning it gains attention.
Points out the irony of claiming freedom in a nation with high incarceration rates and fear-driven policies.
Emphasizes that the founding fathers spoke more of ideas like freedom and liberty than nationalism.
Challenges if society truly upholds promises of equality, liberty, and justice for all.
Questions the real meaning of symbols like the American flag and who truly understands its promises.
Criticizes those who aim to criminalize flag burning while ignoring the essence of freedom and equality.
Urges patriots to actively work towards equal rights and justice for all to truly embody American values.
States that symbols like the flag lose meaning if promises of equality and liberty are not upheld.
Condemns those in power who prioritize protecting a flag over fulfilling the country's foundational ideals.
Expresses concern over the dwindling efforts to ensure equality for all in society.

Actions:

for patriots, activists, americans,
Work towards equal rights and justice for all in your community (implied)
Actively uphold the promises of equality and liberty in your daily actions (implied)
</details>
<details>
<summary>
2019-06-16: Let's talk about Phoenix PD and Barbies.... (<a href="https://youtube.com/watch?v=rN1wEwOHkAA">watch</a> || <a href="/videos/2019/06/16/Lets_talk_about_Phoenix_PD_and_Barbies">transcript &amp; editable summary</a>)

Phoenix PD's credibility is questioned as Beau exposes discrepancies, lack of training, and accountability in a troubling incident involving excessive force.

</summary>

"You threaten to kill a mother in front of her kids. Yeah. Nobody cares."
"If you drag this out it's just going to get worse."
"Your credibility is already shot. You need to make your decision on what you're going to do."
"You're going to get somebody killed."
"You have the same propensity for violence as an addict looking for a fix."

### AI summary (High error rate! Edit errors on video page)

Phoenix PD released a fact sheet light on facts, attempting to justify their actions with no use of force mentioned, despite contradicting evidence in original reports.
The credibility of the Phoenix PD is questioned, given the discrepancies in their statements.
Even if the facts were true, the situation worsens due to the presence of children during the incident.
Beau questions the justification for excessive force and violence by the Phoenix PD, even in hypothetical scenarios like shoplifting.
The core issues lie in the lack of proper training and tactics displayed by the officers involved.
Failure to follow standard procedures like time, distance, and cover, and conflicting commands led to a chaotic and dangerous situation.
The officers' actions instilled fear and panic unnecessarily, leading to a traumatic experience for the individuals involved.
Beau criticizes the lack of de-escalation attempts and the absence of body cameras, which are vital for accountability and learning from such incidents.
The safety of civilians, especially children, was compromised by the actions of the Phoenix PD.
Beau calls for accountability within the department, suggesting that all officers on scene should be removed due to negligence and lack of accurate reporting.

Actions:

for community members, activists,
Demand accountability from Phoenix PD (suggested)
Advocate for policy changes regarding the use of force (exemplified)
Support civilians standing against police misconduct (implied)
</details>
<details>
<summary>
2019-06-15: Let's talk about the right and left in America.... (<a href="https://youtube.com/watch?v=ufzwfNWOmZ4">watch</a> || <a href="/videos/2019/06/15/Lets_talk_about_the_right_and_left_in_America">transcript &amp; editable summary</a>)

Beau clarifies the misusage of "left" and "right," distinguishing Democrats as liberals, not leftists, and advocates for defending radical ideas to prevent society from moving further right.

</summary>

"Without the radical left, well, we're all just right-wing."
"History has a well-known leftist bias."

### AI summary (High error rate! Edit errors on video page)

Points out the incorrect usage of "left" and "right" in the US and explains common terms.
Differentiates between Democrats as liberals and not leftists, more center-right.
Mentions the lack of a viable left-wing party in the United States.
Provides an example of a Facebook post and tweet supporting amnesty for undocumented individuals.
Acknowledges potential backlash for his statement on amnesty.
References Ronald Reagan's stance on amnesty, contrasting it with President Trump's wall policy.
Criticizes the right for not having a hard line like the left, attributing it to the lack of theocratic elements.
Encourages not to shy away from radical ideas and to defend them.
Suggests that without the radical left, society tends to move further right.
Emphasizes the importance of advocating for a fair and just society to move towards the left globally.

Actions:

for activists, progressives, liberals,
Defend radical ideas when you hear them (advocated)
Advocate for a fair and just society (advocated)
</details>
<details>
<summary>
2019-06-14: Let's talk about a midnight revival.... (<a href="https://youtube.com/watch?v=YBQhFPXaO2o">watch</a> || <a href="/videos/2019/06/14/Lets_talk_about_a_midnight_revival">transcript &amp; editable summary</a>)

Beau addresses a sermon calling for mass murder against the LGBTQ community, urging individuals to reject complicity in hate and choose resistance against oppressive ideologies.

</summary>

"He's carried it, he struggled with it for so long for too long the sacrifice makes a stone of the heart and that is what happened when people talked about him he's giving himself over."
"I thought of another congregation a long time ago, back in the 30s. It was in Hamburg, another firebrand comes to town. Preaching the same hate ends the same way, advocating for the government to take care of the undesirables, take care of those who just don't fit."
"We are fast approaching a time in this country where we have a choice. We can become brown shirts. We can become white roses."

### AI summary (High error rate! Edit errors on video page)

Beau sets the scene for a midnight revival outside, prompted by a congregation member's distress over a sermon.
The congregation member, labeled a heretic, brought to Beau's attention a sermon filled with hate speech and calls for mass murder against the LGBTQ community.
The sermon in question was delivered by a former detective from Knox County, Tennessee.
The detective in the sermon uses anecdotes of drug abuse and rape to demonize the LGBTQ community, sparking Beau's reflection on the potential to vilify any community with such anecdotes.
Beau condemns the sermon's message as rooted in hatred and points out the speaker's long-standing struggle with carrying that hate.
He criticizes the congregation's silent complicity in listening to calls for mass murder and likens it to historical instances of silent consent to atrocities.
Beau invokes the image of August Landmesser, a lone dissenter in a crowd, as a symbol of defiance against hateful ideologies.
He warns of the country's choice between becoming brown shirts or white roses, referencing historical symbols of complicity versus resistance.
Beau ends with a thought-provoking message about the need for individuals to stand up against hate and choose the path of resistance.

Actions:

for congregation members, activists,
Stand against hate speech and discriminatory ideologies within your community (exemplified)
Speak out against calls for violence and mass murder targeting marginalized communities (exemplified)
</details>
<details>
<summary>
2019-06-12: Let's talk about dog whistles, murder rates, and country music.... (<a href="https://youtube.com/watch?v=vbUbEnm-mh4">watch</a> || <a href="/videos/2019/06/12/Lets_talk_about_dog_whistles_murder_rates_and_country_music">transcript &amp; editable summary</a>)

Beau explains dog whistles, urges dismantling hidden meanings to combat harmful ideologies, and advocates focusing solely on exposing the core messages behind them.

</summary>

"You continually hammer on that one statement and get them to address that one thing."
"You destroy those pillars, those things that hold up the rest of the ideology, those things that are so commonly believed that everybody can recognize them."
"Engage those dog whistles every chance you get and you focus on that dog whistle."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of dog whistles, which are statements with hidden meanings that give the speaker plausible deniability.
Dog whistles are used to convey messages to a specific group while appearing innocent to others.
Beau admits to using dog whistles in his videos, not necessarily racist ones, but to communicate with certain groups.
Talks about how to identify and respond to dog whistles by focusing solely on the hidden meaning and not letting the speaker divert the attention.
Mentions examples like the statement "if no government is best, then maybe no government is best" as a dog whistle for those who believe in a stateless society.
Emphasizes the importance of dismantling these hidden messages to expose the true intentions behind them.
Compares the use of dog whistles to racist jokes and suggests challenging the speaker to explain the hidden meaning behind their statements.
Provides examples like statistics on crime rates among different groups to illustrate how dog whistles work.
Encourages engaging with dog whistles to prevent others from being influenced by harmful ideologies.
Stresses the need to focus on the core message of dog whistles to dismantle the underlying beliefs.
Beau delves into the nuances of crime statistics and challenges stereotypes associated with criminal behavior.
Talks about the prevalence of dog whistles in culture, including music, and how they perpetuate harmful stereotypes.
Urges readers to confront dog whistles and dismantle the pillars of harmful ideologies.
Beau concludes by encouraging active engagement with dog whistles to combat harmful beliefs in society.

Actions:

for online activists, community organizers,
Challenge dog whistles by focusing solely on dismantling the hidden messages (suggested).
Engage actively with dog whistles to prevent harmful ideologies from spreading (suggested).
Address core messages behind dog whistles to expose harmful beliefs in society (suggested).
</details>
<details>
<summary>
2019-06-12: Let's talk about a meeting at a burger joint, the scars of history, and today.... (<a href="https://youtube.com/watch?v=naKy0o23uZU">watch</a> || <a href="/videos/2019/06/12/Lets_talk_about_a_meeting_at_a_burger_joint_the_scars_of_history_and_today">transcript &amp; editable summary</a>)

Beau witnessed a poignant encounter with a recently released elderly Black man, leading to reflections on learned behaviors and societal injustices still prevalent today.

</summary>

"Learned behavior, something he learned as a kid, fell back on it immediately, 50 years later."
"Sat there and watched the scars of history burst open in front of me."
"Hope I never hear it again."

### AI summary (High error rate! Edit errors on video page)

Witnessed an encounter at a fast-food joint involving an elderly Black man just released from prison, triggering reflections on societal dynamics and learned behaviors.
Noticed the man's defensive tone, reminiscent of interactions with racists in the 60s, which saddened him.
Offered the man a ride to his halfway house, assuming he was on his way to meet his probation officer.
Realized the man had already been to the halfway house and was waiting for his probation officer.
Learned the man had been incarcerated since the early 70s and was now in his 70s.
Felt heartbroken realizing his own actions had triggered the defensive tone in the man due to past experiences.
Reflected on societal norms and learned behaviors that persist despite the passage of time.
Expresses anger at the continued mistreatment in ICE facilities, including sexual abuse complaints and inhumane conditions for children.
Contemplates the long-lasting impact of current mistreatment on future generations' learned behaviors.
Acknowledges the scars of history and expresses frustration at the perpetuation of such injustices.
Questions the defense mechanisms and learned responses being instilled in today's youth that may resurface in the future.
Considers the lasting effects of historical trauma and ongoing mistreatment.
Expresses hope to never hear the defensive tone from the elderly man again.
Ends with a thoughtful message wishing everyone a good night.

Actions:

for community members, activists.,
Support organizations working to improve conditions in ICE facilities (implied).
Educate others on the systemic issues leading to mistreatment in detention facilities (implied).
Advocate for humane treatment of all individuals in detention (implied).
</details>
<details>
<summary>
2019-06-10: Let's talk about interstate Gospel, Eve, and a really important bag.... (<a href="https://youtube.com/watch?v=OnsPQw23QsY">watch</a> || <a href="/videos/2019/06/10/Lets_talk_about_interstate_Gospel_Eve_and_a_really_important_bag">transcript &amp; editable summary</a>)

Beau Garg questions the effectiveness of pro-life spending, suggesting research on artificial wombs as a solution, while pointing out the reality of children in foster care.

</summary>

"Maybe, if it really is about the preservation of life, that money should go to research on these artificial wombs."
"I think for a lot of people this isn't about the preservation of life. I think it's about controlling other people."
"Tens of thousands will age out of the system this year."

### AI summary (High error rate! Edit errors on video page)

Driving to Pensacola on I-10, encountering billboards with scripture, including one targeting abortion.
Billboards in the South costing a few hundred bucks in rural areas and thousands in major markets.
Hundreds of billboards all over the South, with millions spent on lobbying and informational literature for the pro-life cause.
Questioning the effectiveness of the money spent on billboards and propaganda.
Mentioning the artificial uterus, artificial womb technology successfully maturing a lamb at 24 weeks.
Research on artificial wombs could potentially end the abortion debate.
Suggesting a shift in funding towards research on artificial wombs if the goal is truly about life preservation.
Noting the significant number of children in foster care and the thousands aging out of the system annually.

Actions:

for pro-choice advocates,
Support research on artificial wombs to potentially address the abortion debate (implied).
</details>
<details>
<summary>
2019-06-10: Let's talk about Pulse nightclub, Florida man, and Florida's laws.... (<a href="https://youtube.com/watch?v=iXN8eypHbVQ">watch</a> || <a href="/videos/2019/06/10/Lets_talk_about_Pulse_nightclub_Florida_man_and_Florida_s_laws">transcript &amp; editable summary</a>)

Beau warns pastors planning to visit Pulse nightclub in Florida, criticizing their morally and legally wrong actions and urging them to call it off before someone gets hurt.

</summary>

"Y'all need to take this to another state."
"What you're doing is wrong. It's wrong morally. It's wrong on every level."
"And in Florida, oh, it's legally wrong."
"You're going to travel across the country so you can have your little bigoted conference and mock the deaths of people who were murdered? Yeah, that's what Jesus would do."

### AI summary (High error rate! Edit errors on video page)

Raises concern about pastors planning to go to the Pulse nightclub in Florida on the anniversary of the attack.
Expresses dismay at the pastors' history of celebrating deaths and saying gay people don't deserve to live.
Warns that the pastors' actions could be seen as a threat of bodily harm and instigating violence.
Explains the concept of "Florida man" and how the state's unique environment influences behavior.
Quotes the law allowing deadly force in certain circumstances to prevent harm or forcible felony.
Advises the pastors to reconsider their plans as their actions could legally lead to harm in Florida.
Urges them to call off their trip and conduct their activities in a more appropriate setting.
Condemns the pastors' behavior as morally, ethically, and legally wrong, potentially leading to forcible felonies.
Criticizes the pastors for mocking the deaths of those murdered at Pulse nightclub.
Ends with a strong statement against the pastors and their actions, questioning their alignment with Christian values.

Actions:

for community members, activists.,
Contact local authorities to raise awareness about the pastors' threatening behavior (implied).
Support LGBTQ+ organizations in Florida to ensure a safe environment during sensitive anniversaries (implied).
</details>
<details>
<summary>
2019-06-09: Let's talk about the censorship of hate speech (Part 2).... (<a href="https://youtube.com/watch?v=UTEb5cok6Bw">watch</a> || <a href="/videos/2019/06/09/Lets_talk_about_the_censorship_of_hate_speech_Part_2">transcript &amp; editable summary</a>)

Beau questions the concentration of power in regulating information and suggests decentralization to prevent echo chambers and control over content.

</summary>

"Should anybody have this much power?"
"Maybe the solution is less power in more hands rather than a whole lot of power in one set of hands."
"Instead, much like this, we attempt to influence the regulation rather than stop it."
"We're reaching a point with technology where we could very easily head into a black mirror type of situation."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Part two discussing censorship of hate speech, with three sides in the comments: two with a plan and one with a slogan.
The slogan side advocates for "free speech for all, all the time," which sounds good but raises questions about implementation.
There's a side advocating for each website determining its own content, leading to the current situation.
Another side argues for government regulation of the internet, likening it to a public utility, but Beau questions giving such power to those already holding the monopoly on violence.
Doubts if the First Amendment can truly protect in case of government censorship, pointing out precedents from other countries.
Raises concerns about government control over information, including the potential for censoring critical content and promoting propaganda.
Questions whether anyone should have such immense power over information, suggesting a need for less concentrated power.
Proposes exploring alternatives to centralized platforms like YouTube, advocating for less power in more hands.
Notes the disparity in outrage over censorship by platforms compared to other powerful entities regulating everyday lives.
Suggests that decentralization could make spreading harmful ideas harder by disrupting echo chambers and targeting specific audiences rather than broad social media users.

Actions:

for internet users,
Advocate for decentralized platforms to prevent echo chambers (suggested)
Question concentration of power in regulating information (implied)
</details>
<details>
<summary>
2019-06-08: Let's talk about the censorship of hate speech.... (<a href="https://youtube.com/watch?v=cLEZrTmkixY">watch</a> || <a href="/videos/2019/06/08/Lets_talk_about_the_censorship_of_hate_speech">transcript &amp; editable summary</a>)

Beau supports YouTube's censorship of hate speech, encourages building alternative platforms, defends valuable journalism, and acknowledges mistakes in the process.

</summary>

"You want to be a bigot? You got a right to be a bigot. The second you advocate for violence, discrimination, or some kind of negative thing to be visited upon whoever it is you're bigoted against, well then that's a problem."
"Freedom of speech protects you from the government, not from a private company and not from public outcry."
"Sunlight is a really good disinfectant. It makes it easier to debunk."
"The market has spoken. We don't want you."
"There are people that don't have access to your stuff. Video evidence of those speeches provides intent."

### AI summary (High error rate! Edit errors on video page)

Expresses support for YouTube's efforts to clean up its platform by censoring hate speech.
Defines hate speech as advocating for violence, discrimination, or negative actions against a group.
Emphasizes that freedom of speech protects from the government, not private companies.
Encourages individuals to build their own platforms if they disagree with censorship policies.
Acknowledges mistakes made by YouTube in banning channels, including a history professor and an organization called News to Share.
Defends the value of journalism like that done by Ford Fisher, documenting extremist activities and protests.
Argues that documenting extremist activities can help identify individuals inciting violence versus those exploiting the situation for financial gain.
Points out the importance of video evidence in intelligence work to determine intent and identify perpetrators.
Stresses the value of News to Share's journalism, even though mistakes were made in its censorship.
Mentions Ben Shapiro's support for Ford Fisher and the importance of Fisher's work in journalism.

Actions:

for content creators,
Contact Ford Fisher to offer support and solidarity (implied)
Support independent journalism that exposes extremist activities (implied)
</details>
<details>
<summary>
2019-06-06: Let's talk about the facts behind making things quieter.... (<a href="https://youtube.com/watch?v=COj1gl_fdsI">watch</a> || <a href="/videos/2019/06/06/Lets_talk_about_the_facts_behind_making_things_quieter">transcript &amp; editable summary</a>)

Beau explains the science behind suppressors, debunking myths and discussing their legitimate uses among law-abiding gun owners, while addressing the potential effectiveness of a ban.

</summary>

"It doesn't make anything silent."
"Decibels are a logarithmic system."
"They reduce recoil by about a third."
"If you're just letting off one shot, that's going to sound like anything other than a gunshot."
"They're very, very law-abiding people."

### AI summary (High error rate! Edit errors on video page)

Receives questions about making something quieter from both Second Amendment advocates and gun control advocates.
Explains the difference between suppressors and silencers, debunking myths.
Describes how suppressors work by reducing the decibels of a gunshot through a tube with baffles.
Notes that suppressors reduce recoil by about a third and help mask the location of a shooter by changing the sound profile.
Mentions that suppressors also reduce muzzle flash and are primarily used for military purposes to avoid raising alarms.
Addresses the illegal use of suppressors, stating that statistically, they are not significant in crimes.
Emphasizes that suppressors have legitimate uses, particularly for hearing protection, and are popular among law-abiding gun owners.
Explains the limitations of homemade suppressors compared to commercial ones.
Concludes that a ban on suppressors could be effective but acknowledges that it targets law-abiding citizens who are unlikely to use them for criminal activities.
Touches on the debate regarding the necessity of suppressors under the Second Amendment.

Actions:

for gun owners,
Contact local officials to advocate for sensible gun regulations (implied).
Educate others on the legitimate uses of suppressors for hearing protection (implied).
Support organizations promoting responsible gun ownership (implied).
</details>
<details>
<summary>
2019-06-05: Let's talk about how it can't happen here, but it can happen in Alabama.... (<a href="https://youtube.com/watch?v=NJo9vcwlI80">watch</a> || <a href="/videos/2019/06/05/Lets_talk_about_how_it_can_t_happen_here_but_it_can_happen_in_Alabama">transcript &amp; editable summary</a>)

Contrasts belief in immunity to atrocities with the disturbing advocacy for mass murder by a government official, calling for his resignation and criticizing the lack of action in Alabama.

</summary>

"The only way to change it would be to kill the problem out."
"They want your resignation. That's what they want."
"Your prospective soldiers, they think you're a threat."
"They see you as a fascist thug."
"The fact that there are no wills churning already to remove this person says everything you need to know about the great state of Alabama."

### AI summary (High error rate! Edit errors on video page)

Contrasts the belief that certain atrocities can't happen in the United States with historical evidence suggesting otherwise.
Reads a disturbing social media post by the mayor of Carbon Hill, Alabama, advocating for killing minorities.
Comments on the mayor's apology after being exposed, stating that it doesn't excuse his actions.
Calls for the mayor's resignation rather than accepting his apology.
Raises concerns about the mayor's influence over law enforcement and treatment of minority groups.
Mentions that he learned about the issue not through the news but from a far-right constitutionalist.
Criticizes the lack of action from the governor and others to remove the mayor from office.
Condemns the mayor's advocacy for mass murder and lack of due process.
Urges for the removal of the mayor to send a message that such behavior is unacceptable.
Expresses disappointment in the state of Alabama for allowing this situation to persist.

Actions:

for alabama residents,
Call for the mayor of Carbon Hill, Alabama, Mark Chambers, to resign (suggested)
Pressure the governor and other officials to take action to remove Mayor Chambers from office (exemplified)
</details>
<details>
<summary>
2019-06-04: Let's talk about tariffs, driving, Scooby Doo, and climate change.... (<a href="https://youtube.com/watch?v=Zg3HbiXeFjc">watch</a> || <a href="/videos/2019/06/04/Lets_talk_about_tariffs_driving_Scooby_Doo_and_climate_change">transcript &amp; editable summary</a>)

Beau breaks down the impacts of tariffs, the urgency of climate change, and the vital role of the younger generation in taking action for our planet's future.

</summary>

"We're talking about a border crisis now. Worrying about refugees now. Wait until there's a billion of them."
"Those meddling kids, when they finally get that villain, and they pull the mask off of them just like Scooby-Doo, they're gonna find out it's some old dude trying to make some money."
"It threatens the premature extinction of Earth originating intelligent life."

### AI summary (High error rate! Edit errors on video page)

Tariffs are beginning to have real impacts, leading to price increases at major retailers like Costco, Walmart, Dollar General, Family Dollar, and Dollar Tree.
Despite warnings from experts about the consequences of tariffs, many Americans allowed politicians to interpret the facts for them.
The timeline for climate change is compared to driving a car, where we must act before passing the point of no return.
By 2050, we are projected to reach a critical point in climate change, leading to the collapse of society.
Climate change is described as an existential national security threat and a risk to Earth's intelligent life.
A mere two-degree increase in temperature could displace a billion people globally.
The younger generation is emphasized as the key fighters against climate change, needing to take significant action.
Planting trees and other small measures can buy time but won't solve the climate crisis.
A World War II-style mobilization is recommended to address climate change effectively.
Individuals are urged to vote with their dollars and hold both multinational corporations and governments accountable for their environmental impact.

Actions:

for climate activists, young generation,
Mobilize for climate action like in World War II (implied)
Vote with your dollar to support environmentally responsible companies (implied)
Hold governments accountable for their impact on the environment (implied)
</details>
<details>
<summary>
2019-06-03: Let's talk about bridges, roads, and parallel power structures.... (<a href="https://youtube.com/watch?v=fBbd0Gj11SU">watch</a> || <a href="/videos/2019/06/03/Lets_talk_about_bridges_roads_and_parallel_power_structures">transcript &amp; editable summary</a>)

Beau introduces parallel power structures as a path to real freedom, using the example of building a road through community effort to showcase individual responsibility and empowerment.

</summary>

"Freedom comes with responsibility."
"You want that kind of freedom, you have to take that kind of responsibility."
"There's nothing that the government can do that you can't."
"Parallel power structures are the road to a future that has real freedom."
"It's probably time to start building bridges so you can build your road."

### AI summary (High error rate! Edit errors on video page)

Introduces the concept of parallel power structures in relation to freedom and responsibility.
Emphasizes the need for individuals to take responsibility for their freedom.
Suggests that limiting the power of the state can be achieved by creating parallel power structures.
Gives an example of building a road as a parallel power structure to showcase real freedom.
Explains how individuals can come together to build infrastructure without relying on the government.
Shares a historical example of people building a 300-mile road in a single day through community effort.
Points out that parallel power structures can lead to government change or improvements in responsiveness.
Mentions how social programs like free lunch programs may have been inspired by grassroots initiatives like the Black Panthers.
Encourages using social media and community engagement to create change and build parallel power structures.

Actions:

for community organizers, activists,
Gather volunteers to work on community projects (exemplified)
Utilize social media to mobilize support for grassroots initiatives (exemplified)
</details>
<details>
<summary>
2019-06-01: Let's talk about power and potato chips.... (<a href="https://youtube.com/watch?v=vS4RQfkDl_M">watch</a> || <a href="/videos/2019/06/01/Lets_talk_about_power_and_potato_chips">transcript &amp; editable summary</a>)

Beau questions the state's authority, urging individuals to follow conscience over blind obedience and exposing hypocrisy in societal norms.

</summary>

"If something is beyond the power of your own conscience, it should be beyond the power of the state."
"You don't have the authority to give something you don't have as individuals."
"Infringing on the freedom of others is something that has to be taken very seriously."
"Maybe it is time for this country to start following its conscience instead of simply following orders."
"You have a right to be a bigot I guess. Whatever. But does that mean that you have the power?"

### AI summary (High error rate! Edit errors on video page)

Questions the source of power and authority that the state derives from the governed.
Raises concerns about delegating authority to the state that individuals do not possess.
Draws parallels between giving authority to the state and giving something one does not have personally.
Challenges the concept of majority rule justifying violations of others' rights.
Advocates for taking infringement on others' freedom seriously and tying it to one's conscience.
Suggests that if individuals lack the authority to do something on their own, they cannot delegate it to the government.
Differentiates between a justice system based on conscience and a legal system.
Criticizes the hypocrisy in immigration debates and the treatment of asylum seekers.
Questions the morality of preventing others from seeking a better life based on arbitrary distinctions.
Encourages following one's conscience over blindly obeying authority figures.

Actions:

for citizens, activists, voters,
Follow your conscience in decision-making (implied)
Challenge unjust authority and norms (implied)
Advocate for policies based on morality and conscience (implied)
</details>
