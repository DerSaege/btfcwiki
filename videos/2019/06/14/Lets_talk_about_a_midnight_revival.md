---
title: Let's talk about a midnight revival....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=YBQhFPXaO2o) |
| Published | 2019/06/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau sets the scene for a midnight revival outside, prompted by a congregation member's distress over a sermon.
- The congregation member, labeled a heretic, brought to Beau's attention a sermon filled with hate speech and calls for mass murder against the LGBTQ community.
- The sermon in question was delivered by a former detective from Knox County, Tennessee.
- The detective in the sermon uses anecdotes of drug abuse and rape to demonize the LGBTQ community, sparking Beau's reflection on the potential to vilify any community with such anecdotes.
- Beau condemns the sermon's message as rooted in hatred and points out the speaker's long-standing struggle with carrying that hate.
- He criticizes the congregation's silent complicity in listening to calls for mass murder and likens it to historical instances of silent consent to atrocities.
- Beau invokes the image of August Landmesser, a lone dissenter in a crowd, as a symbol of defiance against hateful ideologies.
- He warns of the country's choice between becoming brown shirts or white roses, referencing historical symbols of complicity versus resistance.
- Beau ends with a thought-provoking message about the need for individuals to stand up against hate and choose the path of resistance.

### Quotes

- "He's carried it, he struggled with it for so long for too long the sacrifice makes a stone of the heart and that is what happened when people talked about him he's giving himself over."
- "I thought of another congregation a long time ago, back in the 30s. It was in Hamburg, another firebrand comes to town. Preaching the same hate ends the same way, advocating for the government to take care of the undesirables, take care of those who just don't fit."
- "We are fast approaching a time in this country where we have a choice. We can become brown shirts. We can become white roses."

### Oneliner

Beau addresses a sermon calling for mass murder against the LGBTQ community, urging individuals to reject complicity in hate and choose resistance against oppressive ideologies.

### Audience

Congregation members, activists

### On-the-ground actions from transcript

- Stand against hate speech and discriminatory ideologies within your community (exemplified)
- Speak out against calls for violence and mass murder targeting marginalized communities (exemplified)

### Whats missing in summary

The full transcript provides a detailed narrative of Beau's impassioned plea to reject hate speech and complicity, drawing parallels to historical instances and urging individuals to take a stand against oppressive ideologies.

### Tags

#HateSpeech #LGBTQ+ #Resistance #Community #Activism


## Transcript
Well howdy there internet people, it's Bo again.
We're outside tonight because we want to have us a midnight revival.
Issue brought to me by a member of the congregation, a heretic,
somebody y'all see all the time down below, speaking out against the truth of the church.
And he comes to me and he says, Brother Bo, I need your help, and I say hallelujah.
has repent. No, no, not at all. He just came across something so foul, so unclean, so unrighteous
that as he said, it cannot stand. And he said, if you're willing, I'll send you this link
and you can look on this own righteousness with your own eyes. And I said, Brother Hedrick,
send me that link. And I click on the link and I look and I'm greeted with a sermon from
Knox County, Tennessee, great state of Tennessee, a sermon by a lawman, a detective, former
detective, sheriff put a stop to that quick fast and in a hurry, and this sermon calling
for mass murder, mass executions, putting to death of the LGBTQ community.
He uses anecdotes in his sermon.
Talks about drug abuse, rape, all kinds of horrible things.
And he uses these anecdotes to vilify the whole community.
He's in law enforcement.
Could those same anecdotes not be used
to vilify your whole community?
And people look at that video and they want to talk about him.
They see that hatred.
And I see that hatred.
That's what it is.
it is hatred.
I see something else
I see a man that has carried something with him for so long
He doesn't even know what to do anymore he's given himself over to the hate
he's carried it, he struggled with it for so long
for too long the sacrifice makes a stone of the heart
and that is what happened
when people talked about him he's giving himself over
It's done
I want to talk about people in those pews
I wanna talk the people that fill that congregation
the people that sat there
and listen to a man
call for mass murder in Jesus' name and said nothing except for a chuckle
every now and again.
You know, it brings up that time-honored question, what would Jesus do?
I don't know. Not in every situation, but I can assure you there is no situation in
which he would engage in mass murder.
This doesn't seem fitting.
As I watched that congregation
listen to the silence, watch them signal their support.
I thought of another congregation a long time ago,
back in the 30s.
It was in Hamburg, another firebrand comes to town.
Preaching the same hate ends the same way,
advocating for the government to take care of the undesirables,
take care of those who just don't fit.
And the people in the crowd, they did that same thing silently,
signaled their consent.
All but one man.
One man stood there, arms folded in defiance.
A man named August Landmesser, iconic photo.
You can't see it, but if you are a believer,
I can assure you Jesus is standing right beside him.
He's not anywhere else in F.O. though.
We are fast approaching a time in this country where we have a choice.
We can become brown shirts.
We can become white roses.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}