---
title: Let's talk about tariffs, driving, Scooby Doo, and climate change....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Zg3HbiXeFjc) |
| Published | 2019/06/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Tariffs are beginning to have real impacts, leading to price increases at major retailers like Costco, Walmart, Dollar General, Family Dollar, and Dollar Tree.
- Despite warnings from experts about the consequences of tariffs, many Americans allowed politicians to interpret the facts for them.
- The timeline for climate change is compared to driving a car, where we must act before passing the point of no return.
- By 2050, we are projected to reach a critical point in climate change, leading to the collapse of society.
- Climate change is described as an existential national security threat and a risk to Earth's intelligent life.
- A mere two-degree increase in temperature could displace a billion people globally.
- The younger generation is emphasized as the key fighters against climate change, needing to take significant action.
- Planting trees and other small measures can buy time but won't solve the climate crisis.
- A World War II-style mobilization is recommended to address climate change effectively.
- Individuals are urged to vote with their dollars and hold both multinational corporations and governments accountable for their environmental impact.

### Quotes

- "We're talking about a border crisis now. Worrying about refugees now. Wait until there's a billion of them."
- "Those meddling kids, when they finally get that villain, and they pull the mask off of them just like Scooby-Doo, they're gonna find out it's some old dude trying to make some money."
- "It threatens the premature extinction of Earth originating intelligent life."

### Oneliner

Beau breaks down the impacts of tariffs, the urgency of climate change, and the vital role of the younger generation in taking action for our planet's future.

### Audience

Climate activists, Young Generation

### On-the-ground actions from transcript

- Mobilize for climate action like in World War II (implied)
- Vote with your dollar to support environmentally responsible companies (implied)
- Hold governments accountable for their impact on the environment (implied)

### Whats missing in summary

The full transcript provides additional insights on the urgency of climate change and the need for collective action to address the impending crisis.

### Tags

#ClimateChange #Tariffs #EnvironmentalAction #YouthActivism #PoliticalAccountability


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're gonna talk about tariffs,
driving, Scooby-Doo, and what they can teach us
about climate change.
The tariffs are starting to hit.
The effects are starting to be felt.
Costco, Walmart, Dollar General, Family Dollar, Dollar Tree,
they all said they're gonna have to raise prices.
And people all over the country are going, what?
The experts in the field all said this is what was going to happen and how it was going
to play out, but a lot of Americans chose to allow their chosen politician to interpret
the facts for them.
And those politicians, they don't care.
They made little jokes.
We can't know if the world was supposed to end in 1990.
No, that's not what it said.
said that we would pass the point of no return across that threshold in 1990 if we didn't
make changes.
And we did make changes.
So now that threshold has been moved back.
It's a lot like driving a car.
When the car in front of you passes that light pole, you want to be able to count 1001, 1002
before you pass that light pole.
Maybe it's time to stop, because cars don't stop on a dime, neither does climate change.
The new predictions say that by 2050 we will cross that point of no return, and that is
when we will start to see the beginning of the collapse of society.
The report referred to it as an existential national security threat, and that's what
the media is kind of going on. That's the terminology they're using. I like a
different quote in that report. It threatens the premature extinction
of Earth originating intelligent life. Premature extinction of Earth originating
intelligent life. Those are the stakes. The higher-end models put us quote on a
on a path to the end of human civilization.
It's important stuff.
Just two degrees, two degrees warmer,
a billion people get displaced.
It's hard to imagine a billion people.
It's hard to imagine a billion.
It's a huge number.
If you wanted to count a million seconds,
it would take about 12 days.
If you wanted to count a billion seconds, well it would take till about 2050, 31.7 years.
We're talking about a border crisis now.
Worrying about refugees now.
Wait until there's a billion of them.
It's a serious thing.
And these are the experts in the field.
Hope you trust them this time.
What we know is that this is the younger generation,
so this is their fight.
They're going to have to know more about this than we do,
certainly more about it than I do.
I mean, I can read the reports.
I can understand what they're saying.
But I don't know all the nuances.
And they're going to be told what they can do
to fight climate change.
And they're going to be told to plant a tree.
Yeah, it'll help.
Plant a tree.
Make sure it's a fruit tree.
It'll help recycle.
It'll help, but it's not going to solve the problem.
These small measures, well, that bought us a little time.
The report recommends a World War II-style mobilization.
Basically, it recommends the New Green Deal, really.
I mean, that's what it's talking about.
what it's saying is going to solve the problem. You're really gonna have to get
involved because it's multinational corporations and it's your government.
Those are the biggest polluters, not you, not you as an individual. So you've got
to vote with your dollar and you've got to rein in your government. And at the
end of the day, the youth, those meddling kids, when they finally get that villain,
and they pull the mask off of them just like scooby-doo they're gonna find out
it's some old dude trying to make some money anyway it's just a thought y'all
Well, have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}