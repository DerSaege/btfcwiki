---
title: Let's talk about what Radio London can teach us about today....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-mMCQRiuB9g) |
| Published | 2019/06/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Allies in World War II broadcasted messages to the resistance in occupied territories using various frequencies and codes to provide information and hope.
- A reenactment of one of those broadcasts is presented, including personal messages, music, and messages of hope.
- Messages of hope address the targeting of immigrant families by the federal administration, providing support and information to communities.
- Various mayors, including San Francisco, L.A., Atlanta, Denver, Baltimore, and D.C., stand in solidarity with immigrant communities and against inhumane practices.
- LAPD released a statement refusing to cooperate with ICE raids, distancing themselves from such actions.
- The president's threats and ultimatums are compared to past historical events, with a reminder that history rhymes but does not repeat itself.

### Quotes

- "History doesn't repeat. It rhymes."
- "The front line is everywhere."
- "No, it's not the same as Nazi Germany."
- "We are in occupied territory."
- "Y'all have a good night."

### Oneliner

Allies' WWII broadcasts inspire messages of hope amidst current immigrant community support and LAPD's refusal to cooperate, reminding that history rhymes with present challenges.

### Audience

Community members

### On-the-ground actions from transcript

- Stand in solidarity with immigrant communities (implied)
- Provide support and information to immigrant families (implied)
- Refuse to cooperate with inhumane practices (implied)

### Whats missing in summary

The emotional impact of historical comparisons and the importance of community unity and support.

### Tags

#WWII #Resistance #ImmigrantRights #CommunitySupport #HistoricalComparison


## Transcript
Well, howdy there, internet people.
It's Beau again.
Bonjour, les infos notes.
During World War II, the Allies would broadcast messages
to the resistance, to those in occupied territory,
those at risk.
They would use a shortwave, medium wave.
I mean, they'd use everything to get it off,
and they would constantly change the frequency.
Um, sometimes they would use codes.
You've probably heard some of those in movies.
They'd also play music and send messages of hope at times.
And we're going to do a little bit of a reenactment of one of those tonight.
So before we begin, please listen to these personal messages.
John has a long mustache. John has a long mustache.
The chair is against the wall, the chair is against the wall.
The tower bell rings at three, the tower bell rings at three.
Now for some music, Beethoven's fifth symphony, please pay attention to the first four notes.
Now on to some messages of hope, it is unconscionable that the federal administration is targeting
innocent immigrant families with secret rage that are designed to inflict as much fear
and pain as possible.
We want our entire community to be prepared.
No Angelenos should ever have to fear being snatched from their loved ones.
We are doing everything we can to provide immigrant families with info and support ahead
of the announced ICE deportation sweeps.
Atlanta stands with our immigrant communities.
Please be prepared.
We'll always stand with families fleeing violence
and do whatever we can to prevent the inhumane practice
of family separation.
Threats from this White House
are only a distraction from its failings.
Won't weaken our resolve.
proud that Baltimore is committed to upholding the American values of respecting the rights
and dignity of every resident.
Presidents should understand that not only are these threats cruel and antithetical to
our American values, they are making our communities less safe.
In order, these statements were prepared by the mayors of San Francisco, L.A., Atlanta,
Denver, Baltimore, and D.C.
It should be noted that LAPD put out a separate statement saying that they were certainly
not going to cooperate with ISIS raids because, well, they're not Vichyssois.
Can you imagine how reprehensible something has to be for even LAPD to be like, no, we're
not getting involved in this?
Most of these messages, they came off of Twitter.
I guess that's the new Radio London.
London is calling. Like many criminals before him, the president has issued a
threat, an ultimatum. Comply in two weeks or we go after the families. That's the
American way, I guess. The one thing I think everybody should take away from
this is that, no, it's not the same as Nazi Germany. History doesn't repeat
repeat. It rhymes. And this is certainly carrying the tune. We are in occupied territory. The
front line is everywhere.
Anyway, it's just a thought.
y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}