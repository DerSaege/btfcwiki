---
title: Let's talk about the censorship of hate speech (Part 2)....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=UTEb5cok6Bw) |
| Published | 2019/06/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Part two discussing censorship of hate speech, with three sides in the comments: two with a plan and one with a slogan.
- The slogan side advocates for "free speech for all, all the time," which sounds good but raises questions about implementation.
- There's a side advocating for each website determining its own content, leading to the current situation.
- Another side argues for government regulation of the internet, likening it to a public utility, but Beau questions giving such power to those already holding the monopoly on violence.
- Doubts if the First Amendment can truly protect in case of government censorship, pointing out precedents from other countries.
- Raises concerns about government control over information, including the potential for censoring critical content and promoting propaganda.
- Questions whether anyone should have such immense power over information, suggesting a need for less concentrated power.
- Proposes exploring alternatives to centralized platforms like YouTube, advocating for less power in more hands.
- Notes the disparity in outrage over censorship by platforms compared to other powerful entities regulating everyday lives.
- Suggests that decentralization could make spreading harmful ideas harder by disrupting echo chambers and targeting specific audiences rather than broad social media users.

### Quotes

- "Should anybody have this much power?"
- "Maybe the solution is less power in more hands rather than a whole lot of power in one set of hands."
- "Instead, much like this, we attempt to influence the regulation rather than stop it."
- "We're reaching a point with technology where we could very easily head into a black mirror type of situation."
- "Y'all have a good night."

### Oneliner

Beau questions the concentration of power in regulating information and suggests decentralization to prevent echo chambers and control over content.

### Audience

Internet users

### On-the-ground actions from transcript

- Advocate for decentralized platforms to prevent echo chambers (suggested)
- Question concentration of power in regulating information (implied)

### Whats missing in summary

Full context and depth of Beau's analysis on the dangers of concentrated power over information and the need for decentralization to prevent manipulation and echo chambers.

### Tags

#Censorship #FreeSpeech #GovernmentRegulation #Decentralization #EchoChambers


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight is part two about the censorship of hate speech.
If you look at the comments last night, you see three sides.
Two sides that have a plan and one side that has a slogan.
The side with the slogan is free speech for all, all the time.
It's absolute.
And that's cool as a slogan.
When you try to apply it, how does that work out?
Does that mean I can go to a pro-life website
and post my pro-choice videos, and they're forced to host them?
Who's going to force them?
Or is it just places that allow uploads,
which is almost every website?
Doesn't really make sense.
And then you have the side that says, well, each website determines its own content.
Okay.
And that gives us the situation we have.
And then you have the side that says, well, the government should regulate it, regulate
the internet.
It should be like a public utility.
Not sure how I feel about giving the people with the power of violence, a monopoly on
violence, a monopoly on information too.
That doesn't seem wise.
And I know you're thinking, well the First Amendment will protect me.
Will it though?
Don't know that it has in other countries.
And if the government did decide to censor you, who would you tell?
And how?
Maybe they don't have to censor you, maybe they just throttle it.
Don't put your information out there.
And they could also go the other way with it.
Be very easy for a government to remove fake news, like anything critical of the government,
and to boost news they want you to know, like that all-important news about Iraq's Weapons
of Mass Destruction program.
very dangerous game.
And as we sit here and argue over who should have this
power, I think we're missing the bigger question.
Should anybody have this much power?
I think that's the real question, which takes us back a
few videos to that video about parallel power structures.
Somebody right now is going, well, there's no
alternative to YouTube.
Maybe there should be.
The market has spoken.
Maybe the solution is less power in more hands rather than a whole lot of power in one set
of hands.
And this idea, well, it applies to other power structures too.
There's all of this outrage over what amounts to mostly racists and conspiracy theorists
getting videos deleted or being demonetized.
Why don't we have that level of outrage when other power structures that have monopolies
regulate our everyday lives?
Instead, much like this, we attempt to influence the regulation rather than stop it.
And I know somebody out there said, well, if we decentralized, then the Nazis would
still be out there.
They'd still be able to do this.
They'd have their videos up.
They would.
They certainly would.
But it would be just like the real world prior to social media.
They'd have to find the people and bring them to those videos rather than being able
to use social media to intentionally target the disaffected and those searching for an
identity and siphon them off.
make their job a whole lot harder would really stop the spread of a lot of bad
ideas, because it wouldn't be reinforced in an echo chamber.
We're reaching a point with technology where we could very easily head into a
black mirror type of situation. It wouldn't be hard at all. It's already happening in
China with the social credit. But I guess it can't happen here. Anyway, it's just a
thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}