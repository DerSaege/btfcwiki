---
title: Let's talk about today in Trump world....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=5sdbnGAbliA) |
| Published | 2019/11/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recapping the noteworthy events from Trump world, including a probe into Giuliani's potential personal benefit from a Ukrainian energy company.
- Giuliani questioned about whether Trump might betray him in the impeachment hearing and his cryptic reference to having "very, very good insurance."
- President Trump's intimidating tweet about witness Yovanovitch, adding to his list of impeachable offenses.
- Witness David Holmes testified about a call where Trump specifically inquired about the Biden investigation, eliminating plausible deniability.
- Roger Stone convicted on all counts for obstructing the Russia Trump investigation, signaling potential future criminal prosecutions post-administration.
- Republicans are warned about their defense of Trump, as future administrations will uncover the truth, tarnishing their legacies.
- Criticizing Trump's self-absorbed nature and lack of concern for the country's well-being.
- The episode ends with a foreboding message about the consequences for those blindly supporting Trump.

### Quotes

- "You're throwing away your legacies to protect somebody who is not going to protect you."
- "Certainly painting Trump as a very self-absorbed person who does not care about the United States."
- "Those in Congress, in the Senate, that are defending him will forever be remembered, for the rest of their very short political careers, as idiots who got conned by a real estate mogul."

### Oneliner

Beau recaps notable events from Trump world, warning Republicans of their legacy's impending tarnish for blindly supporting Trump's actions.

### Audience

Political observers, Republicans

### On-the-ground actions from transcript

- Hold elected officials accountable for their support of actions that harm the country (implied).
- Prepare for potential criminal prosecutions post-administration by staying informed and engaged (implied).

### Whats missing in summary

Insights on the potential long-term consequences for those involved in the controversies surrounding Trump.

### Tags

#Trump #Impeachment #Republican #Legacy #Accountability


## Transcript
Well, howdy there, internet people, it's Bo again.
So today's going to be a little different.
We're just going to do a quick recap, a little run down of all the crazy stuff that went
on in Trump world today that is actually of note.
While the whole article headline thing was pretty funny, it wasn't really that important
in the grand scheme of things.
The feds are apparently conducting a probe about Giuliani and whether or not he's
stood to personally benefit from a Ukrainian energy company that was being pitched alongside
his Biden investigation about profiting from a Ukrainian energy company.
You can't make that up, that's amazing.
Also involving Giuliani, he was questioned about whether or not he thought the president
was going to throw him under the bus in the impeachment hearing.
To which he said that he didn't think so, but he does have very, very good insurance.
So if he does, all my hospital bills will be paid.
And of course his lawyer, who was on the phone, chimed in to say, he's joking, he's joking.
Of course, because he certainly wouldn't be threatening the United States president with
blackmail because that's just how they roll now.
In addition to this, President Trump tweeted about Yovanovitch, who was a witness today.
This is the same witness he said was going to go through some things.
And she said it was intimidating.
So you can add that to his list of impeachable offenses.
Witness David Holmes testified about a call that Sondland had with the president in which
which the president specifically asked about the Biden investigation, so there's no more
plausible deniability there.
President can't say he didn't know.
Giuliani wasn't acting alone.
It's going to be really hard for the president to throw him under the bus, even if he wanted
to, even with whatever insurance Giuliani may or may not have.
The aide, Holmes, asked Sondland about it.
And Sondland said that Trump only cares about big stuff.
The aide was like, well that's, you know, there's war, that's big stuff.
He said, no, big stuff that benefits the president.
Certainly painting Trump as a very self-absorbed person who does not care about the United
States.
Now, Roger Stone was convicted on all counts today for his obstruction of the
Russia Trump investigation. That does not bode well for the criminal prosecutions
that are likely to come after this administration ends. Republicans, please
take note. Like all other administrations, this one will eventually end and when
it does, the next administration will get to the bottom of what's going on.
what has happened. When that happens, those in Congress, in the Senate, that are
defending him will forever be remembered, for the rest of their very short political
careers, as idiots who got conned by a real estate mogul. You're throwing away
your legacies to protect somebody who is not going to protect you and we still
got a whole bunch more to go can't wait till tomorrow's episode anyway it's just
Just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}