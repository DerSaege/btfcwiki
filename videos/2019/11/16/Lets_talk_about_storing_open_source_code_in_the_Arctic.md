---
title: Let's talk about storing open source code in the Arctic....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=OyiW1GRyKM8) |
| Published | 2019/11/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Microsoft is storing the world's open source code in a vault in the Arctic, similar to the crypt of civilization built in the 40s at Oglethorpe University in Georgia.
- The crypt at Oglethorpe University contains microfilm and was created due to the lack of information about ancient civilizations after their fall.
- The Arctic World Archive near the global seed vault in Norway houses the open source code on a microfilm-like substance.
- Time capsules aim to send information to the future, acknowledging that future civilizations may not have certain knowledge.
- There's a mix of useful information like open source code and encyclopedic knowledge, but also trivial items like the secret sauce recipe for McDonald's in the Vatican archives.
- Beau questions the arrogance behind presuming to know what future civilizations will need, suggesting a focus on preserving basic human knowledge instead.
- He proposes multiple vaults containing vital information on medicine, food preparation, and agriculture as a more practical approach to ensuring humanity's survival.
- The reality is that governments, large companies, and institutions like Microsoft and McDonald's foresee a world reset within 750 years, the lifespan of the microfilm.
- Beau concludes by suggesting the idea of preparing for such a collapse by disseminating fundamental human knowledge.

### Quotes

- "Perhaps, given humanity's seeming unwillingness to work together to avoid a fall, maybe we should focus on building vaults all over the place."
- "Basic information about medicine, food preparation, agriculture, that's what humanity needs to survive."
- "World governments and large companies foresee a world reset within 750 years because that's how long that film lasts."

### Oneliner

Microsoft stores open source code in Arctic vault, questioning human arrogance and proposing basic knowledge preservation for humanity's survival.

### Audience

Concerned citizens, futurists

### On-the-ground actions from transcript

- Build community-led vaults with basic human information for survival (suggested)
- Preserve knowledge on medicine, food preparation, and agriculture in accessible formats (suggested)

### Whats missing in summary

Importance of preparing for potential civilization collapse and the need for humility in preserving vital human knowledge.

### Tags

#Preservation #Knowledge #Humanity #Future #Vaults


## Transcript
Well, howdy there, internet people, it's Bo again.
So Microsoft is basically placing all of the world's open source code into a vault in the
Arctic.
You know, back in the 40s, at Oglethorpe University in Georgia, the crypt of civilization was built.
It's a vault, full of a whole bunch of microfilm.
kinds of stuff. It's slated to be opened in 8,113, 6,100 years from now. The reason
it was put together is because Dr. Thornwell Jacobs, when he was teaching
about ancient civilizations and researching them, he was just beside
himself at the lack of information so he wanted to put something together because the lack
of information stemmed from these civilizations falling.
Implicit in the project is the belief that one day this civilization will fall.
Now near the global seed vault in Norway is the Arctic World Archive.
The open source code isn't on disks or hard drives, it's on a microfilm-like substance
on giant reels.
Time capsules are unique in the idea that we're going to be sending information into
the future that hopefully they already know.
the reason we're doing it is because they may not. While some of the stores, some of the things that
are put into these vaults and capsules are useful, the open source code, encyclopedic knowledge,
stuff like that, useful, some of the care for humanity's future behind this dark acceptance
that we will one day fall, it gets lost with human arrogance. Right next to the open source
code in the Vatican archives is the secret sauce recipe for McDonald's.
The presumption that we would know what the future would want or need is the same arrogance that will
likely lead us to fall. Perhaps, given humanity's just seeming unwillingness to work together
to avoid a fall, maybe we should focus on building vaults all over the place, multiple
ones, that contain basic human information, the basic knowledge that humanity needs to
survive. If there is a collapse, the open source code on microfilming
doesn't do much good. But basic information about medicine, food
preparation, agriculture, that would. That would. There's a terrifying reality behind
these sites, and that is that world governments, large companies like
Microsoft, the Vatican, McDonald's foresee a world reset occurring within the next
750 years because that's how long that film lasts. Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}