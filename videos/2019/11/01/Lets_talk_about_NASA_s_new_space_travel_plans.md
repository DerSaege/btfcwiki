---
title: Let's talk about NASA's new space travel plans....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=DRgFRink3YE) |
| Published | 2019/11/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- NASA plans to put a crew of four on the moon for two weeks and launch an intentional interstellar space probe.
- Despite producing 150% of the food needed to feed everyone, a billion people go hungry nightly due to food distribution issues.
- Beau challenges the notion that there are logistical barriers to distributing food when transportation systems are already in place.
- He questions the lack of incentive to distribute food efficiently, pointing out the benefits of stability and reduced defense spending.
- Beau advocates for prioritizing basic needs like food over space exploration, suggesting it's a moral and monetary imperative.
- He stresses the importance of addressing hunger beyond America's borders and treating all people equally.

### Quotes

- "We're on the cusp of a new adventure in humanity, conquering the stars."
- "We produce 150% of what it would take to feed everybody."
- "NASA can plan to put a crew of four on the moon for two weeks and plan to launch an interstellar probe, the rest of us, we can figure out how to move some food."

### Oneliner

NASA plans space exploration while a billion go hungry; Beau questions food distribution priorities. 

### Audience

Global citizens

### On-the-ground actions from transcript

- Coordinate transportation networks to distribute excess food efficiently (implied)
- Advocate for stable governments through food aid to reduce conflict and defense spending (implied)

### Whats missing in summary

The full transcript provides an in-depth perspective on the contrast between space exploration ambitions and global hunger, urging a reevaluation of priorities.

### Tags

#NASA #SpaceExploration #FoodDistribution #GlobalHunger #SocialResponsibility


## Transcript
Well, howdy there, internet people.
It's Bo again.
So we got some out of this world news today from out of this world.
NASA.
And NASA has plans to put a crew of four on the moon for two weeks.
They also have plans to launch the first intentional Interstellar
space probe, this thing will leave the solar system, leave
the heliosphere.
First time we've ever set out to do it.
I know somebody's going to bring a Voyager.
That was an accident.
Nobody expected that equipment to last that long.
Got to the point where they're like, well, what else can we
do with it?
But we're setting out with that goal in mind this time.
Think about that.
We're on the cusp of a new adventure in humanity, conquering the stars.
And as I'm reading about this, the audio that's playing in the background reminds me that
a billion people will go hungry tonight, that go to bed hungry.
And it's not that we don't have the food.
We produce 150% of what it would take to feed everybody.
The answer, what you hear, is that, well, we don't have the
food distribution network.
We can't get it there.
Crew of four on the moon for two weeks, interstellar space
travel, and we can't move some bags of rice here on the
planet.
Some bags of rice here on the planet. Yeah, I'm not buying that not for a second. I
Don't think that's true
Just we don't want to there's nothing in it for us
The way it's looked at it's not true
But that's the way it's looked at
It would be too hard. Now, you have ships,
trains, planes, criss-crossing the globe right now.
Not all of them are full,
and some of them are headed to places where people need things.
And,
you know, that whole thing, what's in it for us? Can you imagine how much
less we would spend on defense?
If a lot of these countries had food, water, medicine, the basics, the countries would
be more stable, the governments would be more stable.
Less instability, less conflict, less money spent on defense.
I mean, so if the moral idea of, hey, people should be able to eat doesn't appeal to you,
there's one, a monetary reason.
And this isn't what aboutism.
I'm not saying that NASA should stop their programs until we feed everybody.
I'm saying that NASA can plan to put a crew of four on the moon for two weeks and plan
to launch an interstellar probe, the rest of us, those of us that aren't in NASA, we
can figure out how to move some food.
Beyond America's borders do not live a lesser people.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}