---
title: Let's talk about impeachment, being uncomfortable, and saving lives....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=fcmXcPYvSnk) |
| Published | 2019/11/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- GOP senators are divided on watching the impeachment hearing due to their base not researching.
- The GOP operates by latching onto ideas and repeating them, creating a mythical enemy.
- Georgia and Kentucky are introducing bills to stop gender-affirming surgeries in children based on misinformation.
- Social transitioning for children does not involve surgery but may include puberty blockers in their teens.
- The idea of children undergoing surgery for gender affirmation is false.
- Some politicians oppose gender-affirming treatments supported by medical associations.
- Misinformation leads to scapegoating marginalized groups like children and their parents.
- Over 41% of the LGBTQ+ community attempts suicide due to lack of acceptance.
- Ignorance and refusal to understand lead to discomfort and immorality judgments.
- Medical professionals support gender-affirming treatments, urging the Republican Party to research and understand.

### Quotes

- "I don't care if Tommy plays with Barbie, if that means that Tommy gets to live."
- "Imagine if it was about who you are, not just your state and their education."
- "Let's get that statistic a little higher, 41%. Those are rookie numbers."

### Oneliner

GOP senators refuse to research, spreading misinformation and harming marginalized groups, while medical professionals support gender-affirming treatments for children.

### Audience

Republicans, Medical Professionals

### On-the-ground actions from transcript

- Contact GOP senators urging them to research and understand the importance of gender-affirming treatments (suggested).
- Support and advocate for gender-affirming treatments for children by spreading accurate information and fighting misinformation (exemplified).

### Whats missing in summary

The full transcript provides a detailed breakdown of the misinformation surrounding gender-affirming treatments for children and the impact of ignorance on marginalized communities.

### Tags

#GOP #Misinformation #GenderAffirming #MedicalProfessionals #SuicidePrevention


## Transcript
Well, howdy there, internet people.
It's Bob again.
So today we're going to talk about Republicans and research.
Don't laugh.
So GOP senators are divided over whether or not
they should watch the impeachment hearing.
Of course they are.
Their base doesn't research.
Why should they?
Seriously.
They know that they can just say something and repeat it enough,
and their base will believe it because their base won't check it out.
There's no due process in the impeachment hearing
and they get away with that
because their base has never read the Constitution.
This idea, this tactic
of latching onto something
and just pushing it and pushing it
to
create a scapegoat,
create a mythical enemy,
That's how the GOP operates.
This bleeds over into a lot of stuff.
And it's all based on not researching.
It's all coming from a place of ignorance.
Right now, Georgia and Kentucky, and I'm sure more states will
follow, are introducing bills to stop gender-affirming surgeries
and drug treatments in children.
Tell you what.
Let me take care of that for you.
Done.
That's not a thing.
That doesn't exist.
Not in any statistically significant manner.
That is not a thing.
That's not true.
I heard this story about an eight-year-old who
was transitioning, social transitioning.
That means using he instead of she.
That means letting them play with the toys of the gender
they identify with, letting them dress that way, that's what that means.
There's no surgery in that.
Now as they get older and they're still, when they hit their teens, they may be given a
puberty blocker.
That's used for all kinds of things.
But in this case, it's used because they don't want to give minors surgery.
If the child goes through the social transitioning and reaffirms that, yeah, this is my gender
identity, then they may be given a puberty blocker.
And then later, we're talking late teens, probably after they turn 18, then maybe they'll
start it in their teens where they'll start with the hormone replacement and
that kind of stuff or what Bing called that. So the idea that some eight-year-
old is being willed into an OR, that's not happening. That's not a
thing. But I mean we got to have a new bathroom bill, right? We got to have
something to scare people with. Got to find a way to create a group to scapegoat and hate.
What better than children or their parents? So who supports the idea of the social transitioning,
the puberty blocker, and then maybe, possibly, the other stuff later?
The American Medical Association,
the American Academy of Child and Adolescent Psychiatry,
and the American Academy on Pediatrics.
Who opposes it?
Couple of representatives from Georgia and Kentucky.
Right now, there's people from Georgia and Kentucky going,
hey, don't disparage the whole state.
We're not all like that.
Yeah, being a scapegoat, it's not real cool, is it?
Having a narrative pushed about you isn't really cool, is it?
Imagine if it was about who you are, not just your state
and their education.
The reason that narrative exists is because of stuff like this.
At the end of the day, there's one really important statistic
that should override all of this.
Now, this information is compiled based
on people who went through this prior to people
trying to understand it, people trying to be accepting,
people trying to figure it out.
41% of that community attempts to end it all, 41%.
So the method of saying, oh, you'll grow out of it.
Don't worry about it.
No, you can't play with Barbie.
That's for boys.
That method results in 41%.
So this method, 10 years from now,
we may find out that there are better ways to do it.
And this isn't the exact right way.
That's completely possible.
But I'm pretty sure it's not going
have that statistic with it. At the end of the day, this boils down to, well, this whole
thing, I don't understand it, I'm afraid of it, I refuse to learn about it, I find
it immoral, whatever. It makes me uncomfortable. That's on one hand. The other hand is a living,
not a fetus, pro-life crowd.
That's what we're talking about.
I don't care if you're uncomfortable.
Yeah, it's unusual.
Okay, and what?
I'm sure there was a time when people were like,
I don't really feel comfortable about
comfortable about injecting radioactive substances into myself. Time's changed.
I thought conservatives wanted to keep the government out of health care. You
have every major group that's involved in this topic in agreement. The medical
professionals, the people that actually deal with this, they all agree. But hey,
we need a scapegoat, we need to pick on kids. Let's get that statistic a little
higher, 41%. Those are rookie numbers. Let's get rid of them all, right?
What's the worst that can happen?
I implore the Republican Party to start doing some research on their own.
And research isn't just looking for things that agree with you, it's looking for the
other side.
I got asked by five or six people to talk about this, and they're on different sides.
This is what I found out.
I don't care if Tommy plays with Barbie, if that means that Tommy gets to live.
It's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}