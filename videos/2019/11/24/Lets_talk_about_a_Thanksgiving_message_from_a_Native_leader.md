---
title: Let's talk about a Thanksgiving message from a Native leader....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xrGl0lOFzzY) |
| Published | 2019/11/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Reading a heartfelt message reflecting on Thanksgiving and the indigenous experience in America.
- Expressing gratitude for those fighting for environmental protection and indigenous rights.
- Encouraging generosity towards those in need and speaking out against injustice.
- Longing for freedom and a return to nature.
- Thanking supporters and hoping for a day of liberation.
- Acknowledging the importance of remembering ancestors' sacrifices.
- Describing a vision of freedom, fresh air, and connection to nature.
- Emphasizing the need to support indigenous communities.
- Urging listeners to be kind, generous, and brave in the face of injustice.
- Ending with a message of gratitude and hope for freedom.

### Quotes

- "If you see someone hurting and in need of a kind word or two, be that person who steps forward and lends a hand."
- "Thank you for listening to whomever is voicing my words."
- "There isn't a minute in any day that passes without me hoping that this will be the day I will be granted freedom."
- "I long for the day when I can smell clean, fresh air, witness the clouds as their movement hides the sun."
- "Thank you for continuing to support and believe in me."

### Oneliner

Beau expresses gratitude, encourages kindness and generosity, and longs for freedom, reflecting on Thanksgiving and indigenous experiences in America.

### Audience

Supporters of indigenous rights

### On-the-ground actions from transcript

- Support indigenous communities by donating resources or volunteering to help (implied)
- Speak up against injustice and confront it bravely in your community (implied)
- Extend kindness and generosity to those in need by offering help or resources (implied)

### Whats missing in summary

The full transcript delves deep into the emotions and reflections of an indigenous person longing for freedom and connection to nature while urging kindness, generosity, and support for indigenous communities.

### Tags

#IndigenousRights #Thanksgiving #Gratitude #Support #Freedom


## Transcript
Well, howdy there, internet people, it's Beau again.
So tonight's gonna be a little different.
I'm just gonna read something.
I looked for it on YouTube first.
Looked to see if anybody had read this.
I couldn't find it.
I've seen it published.
But I'm not sure that the written word does it justice.
The year of 2019 is coming to a close.
And when it comes to the day most Americans set aside as a day for Thanksgiving.
As I let my mind wander beyond the steel bars and concrete walls, I try to imagine what
the people who live outside the prison gates are doing, and what they are thinking.
Do they ever think of the indigenous people who were forced from their homelands?
Do they understand that with every step they take, no matter the direction, that they are
walking on stolen land. Can they imagine, even for one minute, what it was like to watch
the suffering of the women, the children and babies, and yes, the sick and elderly, as
they were made to keep pushing west and freezing temperatures with little or no food. These
were my people and this was our land. There was a time when we enjoyed freedom and were
able to hunt the buffalo and gather the foods and sacred medicines. We were able
to fish and we enjoyed the clean clear water. My people were generous. We shared
everything we had including the knowledge of how to survive the long
harsh winters or the hot humid summers. We were appreciative of the gifts from
our Creator and remember to give thanks on a daily basis. We had ceremonies and
special dances that were a celebration of life. With the coming of foreigners to
our shores, life as we knew it would change drastically. Individual ownership
was foreign to my people. Offenses? Unheard of back then. We were a communal
people. We took care of each other. Our grandparents weren't isolated from us.
They were the wisdom keepers and storytellers and were an important link
in our families. The babies, they were and are our future. Look at the brilliant
young people who put themselves at risk, fighting to keep our water and
environment clean and safe for the generations yet to come. They're willing
confront the giant multinational corporations by educating the general public of the devastation
being caused.
I smile with hope when I think of them.
They are fearless and ready to speak the truth to all who are willing to listen.
We also remember our brothers and sisters of Bolivia who are writing in support of the
first indigenous president, Ivo Morales.
This commitment to the people, the land, their resources, and protection against corruption
is commendable.
We recognize and identify with that struggle so well.
So today, I thank all of the people who are willing to have an open mind, those who are
willing to accept the responsibility of planning for seven generations ahead, those who remember
the sacrifices made by our ancestors so we can continue to speak in our own language,
practice our own way of thankfulness in our own skin, and that we always acknowledge and
respect the indigenous lineage that we carry.
For those of you who are thankful that you have enough food to feed your families, please
give to those who aren't as fortunate.
If you are warm and have a comfortable shelter to live in, please give to those who are cold
and homeless.
If you see someone hurting and in need of a kind word or two, be that person who steps
forward and lends a hand.
And especially when you see injustice anywhere, please be brave enough to speak up to confront
it.
I want to thank all who are kind enough to remember me and my family in your thoughts
and prayers.
Thank you for continuing to support and believe in me.
There isn't a minute in any day that passes without me hoping that this will be the day
I will be granted freedom.
I long for the day when I can smell clean, fresh air, when I can feel the gentle breeze
in my hair, witness the clouds as their movement hides the sun, and when the moon shines the
light on the path to the sacred Inipi.
That would truly be a day I could call a day of thanksgiving.
Thank you for listening to whomever is voicing my words.
spirit is there with you in the spirit of crazy horse under peltier it's just
the thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}