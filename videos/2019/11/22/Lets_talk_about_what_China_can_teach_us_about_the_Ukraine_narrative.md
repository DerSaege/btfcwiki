---
title: Let's talk about what China can teach us about the Ukraine narrative....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=6ookSSqyC1c) |
| Published | 2019/11/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Explains the concept of Ukraine interfering in the 2016 election.
- Draws a parallel to a fabricated story about tearing down the Great Wall of China in 1899.
- Mentions a made-up person, Frank C. Lewis, and how journalists created a false narrative.
- Talks about how the fake Chinese story spread and led to the manufacturing of evidence.
- Debunks the conspiracy theory about Ukraine's interference in the election.
- Points out that CrowdStrike, a Ukrainian company, was falsely accused, though it's American.
- Criticizes the spread of misinformation by right-wing news outlets.
- Compares the repetition of false stories to how misinformation spreads.
- Raises concerns about the impact of repeated lies on public perception and trust.

### Quotes

- "Ideas travel faster than bullets."
- "How could they be this dumb?"
- "It doesn't even make sense, but it got repeated just like this story."

### Oneliner

Beau explains the debunked Ukraine election interference theory by comparing it to a historic fake story, showcasing how misinformation spreads without verification, impacting public trust.

### Audience

Information Seekers

### On-the-ground actions from transcript

- Verify information before sharing (implied).

### Whats missing in summary

The full transcript provides a detailed breakdown of the debunked conspiracy theory regarding Ukraine's interference in the 2016 election and sheds light on how misinformation can spread and impact public perception.

### Tags

#Ukraine #ElectionInterference #Misinformation #DebunkedTheory #PublicTrust


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're gonna talk about the notion
that Ukraine interfered in our election in 2016.
In order to really understand it,
we have to take kind of a roundabout way to get there,
so stick with me.
In the summer of 1899, stick with me,
a guy from Chicago, an engineer named Frank C. Lewis,
got interviewed at the Oxford Hotel in Denver. Journalists were interested in
him because he was on his way to China with a very unusual job. The Chinese
government had asked him to tear down the Great Wall of China and use the
stones to build a road. Cool story. The Denver Post, Times, and Republican ran
that story on June 25th, 1899. A couple days later, Fort Wayne Sentinel found out
that some of the stones were going to be used to plug up the Yangtze River and
shortly thereafter other outlets were able to get their hands on illustrations
of all of this. Now we know this never happened because the Great Wall of China
China's there, but why? Well, that was filled in for us by a guy named Harry Lee Wilbur
in 1939. He told everybody that basically the idea that foreigners were going to tear
down the wall, it inflamed anti-foreign sentiment in China, and that's why they had the Boxer
rebellion. This story got repeated until at least 1981 when none other than Paul
Harvey, good day, repeated it. Here's the problem. Frank C. Lewis never existed. Not
a real person. Never existed. Journalists in Denver were up against a deadline so
they just made up a story. Something far away. Nobody could verify. Check it out.
out. They had no idea other outlets were going to run with it and add to it. How could they?
It's not real. My guess is Guy in Fort Wayne Sentinel. Guy at that paper, he's like, hey,
I got three sources, it's got to be true. Just embellish it a little, add my own twist
to it, boom, easy story, get my paycheck, good to go. So they don't plug up the Yang
Xi with some of the rocks. Who cares? I mean, they just ran out of rocks. It'd be easy to explain.
Because the story spread, people started manufacturing evidence.
The thing about it sparking the Boxer Rebellion? None of that's true. None of that is true. It's just made up.
There's actually no evidence that this story ever even made it to China by that.
This is what happened with this idea of Ukraine interfering in our elections.
Somebody made up a story.
See this wealthy Ukrainian, this Ukrainian company called CrowdStrike, they got the server,
They know it was Ukrainian interference, not Russian.
Okay that's cool, except for the fact that the guy that owns CrowdStrike is an American
in California, and even if he went by his heritage, he's Russian, not Ukrainian.
So even if President Trump's little conspiracy theory was true, it would still be Russian
interference.
They're desperate to get the idea that it wasn't Russian interference out there.
That makes them look bad, but let's just do a quick scan of the facts.
So to be clear, this conspiracy theory revolves around the idea that Ukraine, our ally, who
was trying to cement ties with us, depended on us for aid, they interfered
in our election to make sure the guy who was running as America first not going
to help anybody not going to get involved in any of these other countries
to make sure he won because that's in their interests. It doesn't even make
sense, but it got repeated just like this story, so people believed it.
Hundred years from now, people are going to look at the Congressional testimony and make
fun of these Congress people that asked about this.
How could they be this dumb?
It was repeated enough, it was repeated enough by people who they trusted.
is how people like this, these right-wing news outlets, how they get their story
across. And it works just like this. Somebody makes something up and then
somebody else builds off of it without verifying it. That's how it happens. You
You know, ideas travel faster than bullets.
That saying is not reserved for true ideas.
Anyway, it's just a thought.
have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}