---
title: Let's talk about silver, gold, Vikings, Kings, history, and tradition....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ANOEbHqv2aw) |
| Published | 2019/11/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Two individuals stumbled upon a Viking treasure stash of coins and jewelry in 2015, with coins dating back 1100 years.
- The stash was worth roughly 3.8 million dollars in the US.
- Instead of alerting authorities, they decided to keep and eventually sell the treasure on the open market.
- Only 31 out of the 300 coins have been recovered, some of which have significant historical value.
- Some coins reveal an alliance between Alfred the Great of Wessex and Caelwulf II of Mercia, challenging traditional historical narratives.
- The coins suggest a different story where Caelwulf II was actively allied with Alfred the Great, contrary to the usual portrayal of him as a puppet of the Vikings.
- The disappearance of Caelwulf II from historical records around 879 raises suspicions about how Alfred the Great gained control of Mercia.
- Beau draws a parallel to the series "The Last Kingdom" on Netflix for those unfamiliar with the historical context.
- The discovery of these coins has the potential to rewrite history and challenge established traditions.
- Beau questions the intrinsic value of silver and gold, likening it to the value placed on the history and traditions of a nation.

### Quotes

- "Two guys stumbled across a viking stash of coins and jewelry."
- "The histories at the time, well they're a little murky."
- "This is upending history and tradition."
- "Silver and gold, even today, survivalists will tell you to keep some around because it's always valuable."
- "It's just a thought, y'all have a good night."

### Oneliner

Beau reveals a treasure trove of Viking coins challenging historical narratives and traditions, urging reflection on the value of history over precious metals.

### Audience

History enthusiasts

### On-the-ground actions from transcript

- Research local archaeology groups to support preservation efforts (suggested)
- Visit historical sites or museums to learn more about ancient civilizations (implied)

### Whats missing in summary

The full transcript provides a deeper dive into the significance of historical artifacts and their impact on our understanding of the past.


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're gonna talk about silver and gold
and kings and vikings and tradition and history.
In 2015, two guys with their metal detectors
and hermiture stumbled across a viking stash
of coins and jewelry.
Vikings, like people for thousands of years,
stored their wealth in precious metals.
The coins these guys found were 1100 years old
and they're standing there looking down in this hole
at 300 coins worth roughly 3.8 million dollars US.
Now if you make a fine that significant
you're supposed to alert the authorities.
Destiny is all.
They're looking down in this hole with this money and they decided to keep it, tried to
sell it on the open market.
They just got convicted.
From an archaeological standpoint, this is travesty.
Out of these 300 coins, 31 have been recovered.
Some of those are rewriting history.
Some of them are what is called two emperor coins, marking an alliance between Alfred
the Great of Wessex and Caelwulf II of the King of Mercia.
And that's very surprising because traditionally the King of Mercia is kind of viewed as an
unimportant figure, only reigned for a couple of years and is often cast as a puppet of
the Vikings. These coins tell a very different story. These coins suggest that he was in
an active alliance with Alfred the Great, fighting them. Now, the histories at the time
well they're a little murky. About 879, the sky just disappears and Alfred has
his land. Alfred has control of Mercia. It's almost as if Alfred had him deleted,
had him wrote out of history. It's kind of like maybe these coins are suggesting
that it is very possible that Alfred the Great did something not so great in obtaining this
kingdom.
You know, something like that.
That seems like something you would want to record in your history.
How you wound up with this.
It's not really there.
It's not there.
It's leading to a lot of speculation.
It is upending history and tradition.
understand for Americans that may not know and if you don't and want to catch
an easy view of it, there's a series called The Last Kingdom on Netflix.
But this would be like finding out that Benedict Arnold was actually working for
us the whole time. This is this is the formation, this is the beginning of the
United Kingdom as we know it, and England. And there's a huge gap, there's
something that is not right, so that tradition has to be changed. Silver and
gold, even today, survivalists will tell you to keep some around because its always valuable.
Has intrinsic value they say, but does it, its just metal.
Or is it more like the history and traditions of a nation?
value because we've believed in it for so long anyway it's just a thought y'all have a good night

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}