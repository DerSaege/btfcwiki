---
title: Let's talk about COPPA (Not Legal Advice)....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=OUid1YxgwsU) |
| Published | 2019/11/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains COPPA, a law impacting YouTube creators due to child privacy protection.
- Mentions emotional reactions from creators, some not fully understanding the law's implications.
- Notes the potential impact on wholesome kids' content on YouTube.
- Describes the practical implications of COPPA, including marking videos for kids and potential fines for non-compliance.
- Outlines factors determining kids' content, clarifying misunderstandings around the list provided by YouTube.
- Raises concerns about the subjective nature of compliance with COPPA.
- Emphasizes the severe consequences for creators if they mislabel their content.
- Suggests actions to address the issues with COPPA, including contacting the FTC and legislators to push for changes.
- Urges support for kid content creators through Patreon or merchandise purchases.
- Advocates for getting rid of COPPA altogether, citing its outdated nature and negative impact on content creation.
- Encourages viewers to take action by contacting representatives and engaging with the issue.

### Quotes

- "It's dead. There's no comment section. They won't get a notification. The monetization is severely limited."
- "What they're doing is they're going to limit their child's entertainment and educational opportunities, what they can see for viewing."
- "We don't need to succumb to learned helplessness. We don't just have to roll over and take this."

### Oneliner

Beau explains COPPA's impact on YouTube creators, urging action to address the negative consequences for wholesome kids' content and creators.

### Audience

Content Creators, YouTube Users

### On-the-ground actions from transcript

- Contact the FTC and leave comments to advocate for changing COPPA (implied).
- Reach out to representatives and senators to explain the outdated nature of the law and push for revisions (implied).
- Support kid content creators by signing up for their Patreon or buying their merchandise (implied).
- Tweet the president to raise awareness and potentially influence the FTC's decisions (implied).

### Whats missing in summary

The full transcript provides a comprehensive understanding of COPPA's implications on YouTube creators, including practical steps to address the challenges and advocate for changes. Viewing the full transcript can offer additional insights and details on the issue.

### Tags

#COPPA #YouTube #ContentCreators #ChildPrivacy #Advocacy


## Transcript
Well howdy there internet people, it's Bo again.
So today we're gonna talk about COPPA.
I'm doing this cause like 50 people sent me messages
about it asking about it.
There's a lot of videos out there on YouTube right now.
Some of them are from creators
that are definitely gonna be impacted by it.
And they are understandably emotional.
Some are from creators that read the list
and didn't really understand what it says
And they think they're going to be impacted by it.
And then there's some from people who fully understand
the situation but are being overdramatic for the sake
of clicks.
I am not going to be severely impacted by this.
I've got to change the title of a couple of videos
to be way over-compliant.
So to answer the big question, is this the end of YouTube?
No, of course not.
But it might be the end of wholesome kids content.
It really might.
That is, if you make kids content on YouTube,
this is going to hurt you a lot, a lot.
If you make content that uses a framing device that
looks like it might appeal to kids,
it's going to hurt you a lot.
If you want to hear more about that,
I talked to Merkin Johnson from Non-Complete,
that channel, last night on the podcast.
So what is COPPA?
It's a 1998 law designed to protect child privacy.
That's what it is.
The times have changed.
Times have changed.
When this was written, YouTube didn't exist.
Every kid in the world didn't have a smartphone or a tablet.
Times have changed.
It's not an obsolete law.
It needs to go.
So what does it mean practically?
On our end, when we upload a video now, we have to mark whether or not it's for kids.
And the fine for non-compliance with this, especially my guess would be if you mark it
as not for kids, and it is for kids, so you would be collecting data, is pretty steep.
It's like 40 grand per video.
It's a big deal.
However, there's no reason to do that.
So a kid is defined by under 13.
That's what it means in this legislation.
Now they determine whether or not it's kids' content by a list of factors and this is the
part that people are misunderstanding.
Just because you have one of these doesn't mean you have kids' content.
That isn't what it means.
In fact, on the little guidance thing that YouTube put out, if you click the little thing
below it, it actually tells you that.
people saw, that list freaked out, heard $40,000 fine, and didn't investigate any further.
What's the list?
Subject matter.
Obviously, if you are talking about kids' subjects, it's kids' content.
Your intended or actual audience.
If you make videos that are for adults, but they're songs set to nursery rhymes, but they're
about adult topics, but most of your viewers end up being kids, it's kids' content.
Self-explanatory.
Child actors in it, but that doesn't necessarily mean just because you have a kid in the video,
it's kids' content.
Imagine a video about child abuse.
That's obviously not kids' content, but it may have a child actor in it.
Characters, celebrities, toys that appeal to kids, including animated characters.
That's a big one that people are concerned about, and they should be.
Because that one, I can see that causing issues as far as how the FTC interprets it.
Language of the video, if you talk the way we do on this channel, you don't have to
worry about it.
However, if you use small kid words for everything, it's going to be kids' content.
in the videos such as play acting, simple songs, that's one. Song stories, poems for
kids again should be obvious. Empirical evidence of the audience, meaning if all of your audience,
your actual audience is kids, it's kids content. Now understand nothing I'm saying in this
video is legal advice, it's not. My take on this, because YouTube is saying you can't
use the analytics to make this determination. That seems to me like
that's them covering their themselves. Now when we look at our analytics we get
a breakdown by age. We don't get 13 and under because of childhood privacy. But
it's safe to say that if almost all of your viewers are 13 to 17, which is a
bracket we get, then it's probably kids content because you probably have a
whole bunch that you're not getting information on that are below that. If
If your actual audience is 35 to 50,
probably doesn't apply to you.
But one of these things means nothing.
It's a balance of them.
And it's very subjective, and people
are going to argue that it's void for vagueness,
and it probably is.
But somebody's going to have to take that to court.
And with $40,000 on the line, I don't
know that there's going to be a lot of people
willing to do that.
So if you market as not for kids, and it is for kids, you're subject to the fine.
If you market as for kids, your content's dead.
It's dead.
There's no comment section.
They won't get a notification.
The monetization is severely limited.
Estimates put it up to 90% reduction.
Now for creators like me, I've got low overhead.
doesn't take much to do what I do monetarily. However, if you're making kids
content and you're producing cartoons, that costs money. If the monetization
ends, the content ends. And that's a real worry. This thing that is supposed to
protect kids is gonna get rid of wholesome kids content on YouTube. It
just doesn't make any sense at all. What does this mean for you guys? If you
have kids and they watch kids content or you watch kids content, you're gonna see
a massive decline in the amount of content that's put out because creators
are gonna stop creating. That's really what's gonna happen. There will be
thousands of kid content creators that go out of business. There's a lot of people
without jobs to accomplish nothing. Now if you want to help them, you know, you
like Blippi or Ryan or whoever, you can sign up for their patreon, buy their
merchandise and that'll help float them for a little bit, but it's not going to
solve the problem. To solve the problem you got to contact the FTC, leave them a
comment, get the rules changed. That's option one. That's the one everybody
seems to be pushing. To me that's the least effective. Contact your
representatives and your senators. Explain to them this law was made in
1998. It's obsolete. Imagine running your computer with software from 1998. It's not
going to make any sense trying to do anything we do today. You guys know I use YouTube with
my kids. If they're interested in a topic, they get their tablet, they pull it up. In
today's world, I actually want YouTube's creepy algorithm sending them to similar videos.
It makes my life easier to keep them on task. Yeah, they get targeted ads. The FTC seems
to be under the impression that they won't get ads directly targeted to children if they
do this.
That doesn't even make sense to me.
Do you want them to see beer ads?
But beside that, it's not going to stop them from getting more kid ads because the advertisers
are going to understand that the untargeted ads now are all going to go to kids.
It doesn't change anything.
makes it a little less specific but that's it. So to me the goal here should
be to get rid of compa altogether. 1998 law needs to go. It needs to go. It
doesn't make any sense anymore. Yeah it had a good purpose to protect the
privacy of children but in today's world you know what if you're that concerned
about it we got cookie blockers. Use them. Use them. Parents can't rely on Big
daddy government to protect them.
What they're doing is they're going to limit their child's entertainment and educational
opportunities, what they can see for viewing.
It just doesn't make any sense.
And I would tweet, I can't believe I'm going to say this, I would tweet the president.
I would tweet the president.
The president has a lot of sway over the FTC.
And I don't know that President Trump right now wants thousands of people without jobs.
Those would be the things that I would do if I was in this boat.
That's what I would be directing people to do.
I will do a video specifically for content creators and lay out a strategy later tonight
hopefully.
But if you want to get a head start on it, I'll put some links below.
At the end of the day, the law needs to be changed, revised, updated, something.
Because this is hurting kids, it's not helping them.
It's hurting people who are actually trying to put out good content for kids.
And this is coming from somebody that this doesn't impact.
I have no personal stake in this whatsoever.
It's not going to affect me.
My content is clearly for adults.
Anyway, we don't need to succumb to learned helplessness.
We don't just have to roll over and take this.
There are options available, get the rules changed.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}