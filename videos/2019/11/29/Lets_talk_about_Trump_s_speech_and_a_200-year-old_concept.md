---
title: Let's talk about Trump's speech and a 200-year-old concept....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_Nhlu4IAlIc) |
| Published | 2019/11/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Reverse format for this historical event.
- Hanging out with veterans and ex-contractors.
- Watching Trump's speech to troops in Afghanistan.
- Trump falsely claiming they kept oil in Syria.
- Beau not fact-checking, focusing on civilian control.
- Emphasizing civilian leadership over the military.
- President as a civilian, not a rank like general.
- Concept of civilian control of the military.
- President's power for strategic decisions towards peace.
- Importance of preventing military dictatorship.
- Historic taboo on presidents saluting troops.
- Reagan starting the tradition of saluting troops.
- Concerns about blurring the line between civilian and military leadership.
- Keeping Syria's oil as a potential war crime.
- Importance of Pentagon's integrity in addressing such issues.

### Quotes

- "The reason it exists is twofold."
- "He's a civilian, he's not a general, period, full stop."
- "When the head of state starts to be seen as the general, the next thing that happens is war crimes."
- "Keeping Syria's oil, that's a war crime."
- "It's illegal under the Geneva Conventions and the U.S. War Crimes Act."

### Oneliner

Beau explains the importance of civilian control over the military, cautioning against blurring the line and potential war crimes.

### Audience

Citizens, policymakers, military personnel.

### On-the-ground actions from transcript

- Ensure awareness and understanding of the concept of civilian control over the military (suggested).
- Advocate for integrity within the Pentagon to uphold international and U.S. laws regarding war crimes (exemplified).

### Whats missing in summary

The full transcript delves into the historical significance of civilian leadership over the military and the potential consequences of blurring this line, urging vigilance against actions that could lead to war crimes.

### Tags

#CivilianControl #WarCrimes #MilitaryLeadership #Ethics #HistoricalSignificance


## Transcript
Well, howdy there, Internet people, it's Bo again.
So tonight, we're going to do things in reverse.
I normally talk about a historical event
and then bring it into modern context.
We're going to do that backwards because it's weird.
Tonight, I was hanging out with everybody there was either
a vet or an ex-contractor.
For whatever reason, somebody had the president's speech
to the troops in Afghanistan playing on their phone.
It's a normal Trump speech.
I'm the greatest man in the world type of thing.
Until about eight minutes in.
And then everybody stopped and looked at the phone.
First he said, well, we kept the oil in Syria.
No, we didn't.
That's not true.
But I'm not here to fact check the president about that.
He goes on to say, during the campaign, I kept saying that,
keep the oil, keep the oil.
But they didn't have to listen to me.
I was a civilian then.
You're a civilian now, the president is a civilian now, the title of commander in
chief exists solely because he's a civilian, it's not a rank, that's not
something you can get promoted into, it exists because the U S has a centuries
old concept of civilian control of the military and it's really important and
it's a line that's getting blurred more and more.
The reason it exists is twofold.
The first being that in theory, Congress and the president exercise the will of the people.
They have to get together, work together, and decide to deploy the military.
Get a declaration of war, an authorization for military force, something like that.
And that that should reflect the will of the people.
the president has strategic control what targets what main targets to hit how
much collateral damage we're willing to accept what the mission parameters are
stuff like that the reason the president has that power is because in theory they
should be working towards peace that's why it exists the other reason is to
stop the U S from devolving into some two bit military dictatorship under the
leadership of general Lisa Moe Trump, Trump is a civilian, he's not a
general, period, full stop, no discussion on this, he is a civilian, he
is not part of the military, he has control, but is separate from it, it's a
really important concept, it's so important that if you look back at
American history, for the first almost 200 years of this country's existence,
it was taboo for the president to salute the troops because he's a civilian.
You look back at Eisenhower, Kennedy, Roosevelt, these guys did not salute the
troops with any regularity whatsoever.
You'll find some instances of them doing it
when it's a Medal of Honor recipient
or something else extraordinary like that.
The only person that did it with any regularity whatsoever
was Grant when he was drinking, and he drank a lot.
It wasn't until Reagan that that became a thing.
And it also had to do with alcohol.
Reagan and the Marine Corps Commandant were having drinks.
Reagan was complaining that the Marines, they had to hold that salute the whole time.
He didn't understand why he couldn't just salute them and let them drop their hand.
And the Commandant, who's had a couple in him, was like, you're the president, do what
you want.
I mean, that's kind of what he said, and he was like, I don't think anybody's saying anything.
And that's where the tradition started.
And that in and of itself is kind of harmless.
But it's the beginning of this line being blurred.
And historically, when that line gets blurred in a country that had civilian control of
the leadership, of the military, when the head of state starts to be seen as the general,
the next thing that happens is war crimes.
It always goes bad.
Incidentally, the real reason everybody looked at the phone is because keeping Syria's oil,
that's a war crime.
It's called pillaging, not just for pirates.
It's illegal under the Geneva Conventions and the U.S. War Crimes Act.
That's not happening.
If it is, the Democrats can stop looking for their high crimes and misdemeanors.
They found it.
I would really hope that the Pentagon would have the integrity to tell the President that
that is not happening.
Any military commander that participated in that would be open to prosecution.
That well belongs to Syria.
At least the pretense is that we do not have a colonial military.
We do not conquer other nations and take their natural resources.
It would be a war crime as outlined by international law, US law, treaties backed up by the force
of the U.S. Constitution.
This is why the Commander in Chief's real control over the military has traditionally
been limited to strategic decisions because they shouldn't get involved in the day to
day because they don't know enough about it.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}