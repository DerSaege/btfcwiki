---
title: Let's talk about Veterans Day (Part 1)....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=JYvL5cUj5ds) |
| Published | 2019/11/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introducing a Veterans Day special to talk about America's conflicts, including lesser-known ones.
- Listing major conflicts such as the Revolutionary War, War of 1812, Mexican-American War, and more.
- Noting that history often overlooks minor conflicts like the Cherokee American War, Shays Rebellion, and others.
- Explaining each conflict briefly and providing insights, such as tax revolts and internal wars.
- Mentioning wars like the Barbary Wars, Creek War, and the Seminole Wars that shaped American history.
- Describing events like the Texas Indian Wars involving land grabs and ethnic cleansing over 55 years.
- Sharing lesser-known conflicts like the Aegean Sea operations, Winnebago War, and Sumatran expeditions.
- Recounting sad events like the Black Hawk War and conflicts like the Pork and Beans War over borders.
- Touching on unique expeditions like the Ivory Coast expedition and discussing motivations behind conflicts.
- Concluding by reflecting on the vast array of conflicts often excluded from mainstream historical narratives.

### Quotes

- "History often overlooks minor conflicts."
- "Veterans Day is probably a good day to talk about all of these."
- "We don't like why the war started. We don't like the outcome."
- "Some of these weren't major, but some of them were huge."
- "It's just a thought."

### Oneliner

Beau introduces a Veterans Day special to shed light on America's lesser-known conflicts throughout history, reflecting on the vast array of wars often excluded from mainstream narratives.

### Audience

History enthusiasts, Veterans

### On-the-ground actions from transcript

- Research and learn about the lesser-known conflicts mentioned by Beau (implied)
- Take time to commemorate and acknowledge the sacrifices made in all conflicts, major and minor (implied)

### Whats missing in summary

Exploration of the impact of lesser-known conflicts on shaping American history.

### Tags

#VeteransDay #AmericanHistory #LesserKnownConflicts #Memorialize #Education


## Transcript
Well howdy there internet people, it's Bo again.
So tonight we are filming our Veterans Day special
and it's gonna be a multi-part series
in which we're going to discuss all of America's conflicts,
all the United States' conflicts.
When we think of our conflicts,
we think of the Revolutionary War, the War of 1812,
the Mexican-American War, Civil War, Spanish-American War,
World War I, World War II, Korea, Vietnam, Desert Storm, and then the War on Terror.
That's a tiny fraction of our conflicts.
When history gets recorded, the minor ones get left out.
Fifty years from now, nobody's going to know about Somalia, the Tango Tango Ambush, Grenada,
Panama.
None of this is going to be talked about.
So what we're going to do today in various parts because it would get way too long is
we're going to talk about all of them.
I have written on a whiteboard in front of me the names of the wars and the years.
I'm going to give you a little bit of insight, a little bit of commentary on most of them.
Keep in mind I haven't actually taken a class on this stuff in like 17 years.
So if one of them interests you, you might want to fact check it a little bit.
OK, so we're going to start off with the Revolutionary War, 1775 to 1783.
The Cherokee American War, 1776 to 1795.
This is almost 20 years of minor skirmishes.
If you want to know something cool about this, look up a guy named Dragon Canoe.
The Northwest Indian War, 1785 to 1793.
This was a war over the Northwest Territory.
Keep in mind, this was the Northwest Territory at the time.
So we're not talking about Washington and Oregon,
we're talking about Ohio.
We have Shays Rebellion, 1786, 1787.
That's an uprising over debts and taxes.
The Whiskey Rebellion, 1791, 1794, also about taxes.
The Quasi-War at 1798 to 1800, at the end
the American Revolution, we owed the French money. When they had their
revolution, we were like, it's a new government, we're not paying you. They
were like, yeah you are. We had some naval battles. It didn't grow too
much, but it was kind of a de facto quiet war, a quasi war. You have Frey's
Rebellion in 1799 to 1800, another tax revolt. The Barbary Wars, first Barbary
war was 1801 to 1805, pirates, pirates along the Med. The German coast uprising in 1811,
this was a slave uprising near New Orleans, there were two whites that were killed and
in response we killed a hundred black people. Tecumseh's war, also 1811. Now this was the
end of the 60 years war. That was a war for the control of the Great Lakes. This
is where we get the slogan, Tippecanoe and Tyler II, and this pretty much
calls the War of 1812. The War of 1812. In the U.S. this is a major event. This is
one of the few wars we actually learn about. American historians will refer to
it as America's Second Revolution because it secured our place on the
international stage. Meanwhile, everywhere else in the world, this is a
sideshow. This is a minor theater to the Napoleonic Wars. The Creek War, that's
1813 to 1814. This was an internal war with the creek. The Spanish and the
British were supporting Red Stick's side and the Americans were supporting the
the opposing creeks. The big thing to come out of this, Andrew Jackson became a
hero, became a war hero. Not such a great thing. The Second Barbary War, 1815, also
pirates. First Seminole War, 1817 to 1818. This is when the Seminoles were sent to
the reservation in central Florida. Prior to that, they were up in northwest
Florida near Pensacola. The Texas Indian Wars, it's 1820 to 1875. To the real
history buffs out there, yes I understand some of the other conflicts that I am
going to talk about could just be filed under this, but I want to highlight that
we're talking about 55 years of land grabs and ethnic cleansing. It was
the settlers versus natives. Let's see the era Karl war that's 1823. I think this
is one of the few native wars that wasn't over land like directly. I want to
say this had to do with the fur trade. What I do know is it was the first time
the US military was active west of the Missouri. You have the Aegean Sea
operations in 1825 to 1828. More pirates. The Winnebago War 1827. Minor skirmishes.
This one didn't get too crazy. The first Sumatran expedition in 1832. Some
indigenous Indonesians attacked a merchant ship and the US Navy went there
and exacted reprisal. The Black Hawk War in 1832. This one's actually really sad.
There was some land that had been ceded to the U.S. government, and a native leader sees it, it's not being used, so he
settles there with his people.  The Americans in the area thought it was a prelude to an attack, and war commenced.
The Second Seminole War, 1835 to 1842, this was when Jackson decided to move the Seminoles to Oklahoma.
Second Sumatran Expedition, 1838. It's an exact replay of the first. A merchant ship was attacked,
U.S. Navy went there to exact reprisals. The Pork and Beans War in 1838 and 1839. The United States
and the United Kingdom go to war over the border between Maine and New Brunswick, as if that's
worth fighting over. So this was a joke even at the time. In this war, nobody died.
We took some British soldiers, POW, and I think some Canadians might have got
injured, but nobody died. The Ivory Coast expedition in 1842. That's pirates again,
but this is a little different. Merchant ships were getting attacked, but they're
getting attacked along the Ivory Coast by people in war canoes.
They might have had a good reason to fear white ships, call me crazy.
The squadron of ships that was there was actually there conducting anti-slavery operations because
by 1842 as far as the federal government was concerned, the transatlantic slave trade should
stop.
So that's a weird one.
The Mexican-American War, 1846 to 1848.
We annexed Texas, it triggered war.
Let's see, the Cuyos War, that's 1847 to 1855, same old story, land grab.
So now we're at 1850, we're not even to the Civil War yet.
First 75 years, a lot more conflicts than get talked about.
Granted, some of these weren't major, but some of them were huge.
And they just get left out, because we don't like why the war started.
We don't like the outcome.
We don't like how it paints us.
So it doesn't get included.
I think Veterans Day is probably a good day to talk about all of these.
So part two of this will be coming very, very soon, because we're getting over our normal
time limit.
Anyway, it's just a thought.
y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}