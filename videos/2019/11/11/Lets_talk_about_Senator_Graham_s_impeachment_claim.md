---
title: Let's talk about Senator Graham's impeachment claim....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=jJ5kEXWC96s) |
| Published | 2019/11/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Senator Graham threatened that impeachment is dead on arrival if the whistleblower is not named, attempting to bully the House into revealing the whistleblower's identity.
- The Senator's demand is baseless, as the House has sole power over impeachment and the Senate's duty is to conduct a trial, not dismiss impeachment without due process.
- Beau criticizes Senator Graham's attempt to bully and manipulate the process by demanding the whistleblower's identity, likening it to the behavior that led to the President's impeachment.
- He questions the Senator's logic by sarcastically suggesting that Crime Stoppers and anonymous tip lines should be banned if anonymity invalidates due process.
- The importance of whistleblowers remaining anonymous is emphasized, as they fear being attacked by individuals who prioritize party loyalty over the country's well-being.
- Senator Graham's actions are seen as undermining the Constitution and the rule of law to protect a guilty party, raising concerns about the Senate's trustworthiness and integrity.
- Beau accuses the GOP of engaging in corrupt behavior by attempting to overlook wrongdoing in favor of reelection prospects, rather than upholding their oath and duty to uphold the Constitution.
- He argues that failing to uphold their oath and attempting to undermine the Constitution will have more significant negative consequences for reelection than acknowledging any illegal actions committed by the President.


### Quotes

- "You're trying to bully… Kind of sounds like what the president's getting impeached for, doesn't it?"
- "Whistleblowers and people who like to remain anonymous, do so because they don't want to be smeared by an attorney or some politician who has put his party over his country."
- "It certainly appears that Senator Graham is willing to undermine the Constitution and the rule of law to protect somebody he knows is guilty."
- "Failing to uphold your oath and your duty and attempting to undermine the Constitution through some rhetoric that we all see through, that's going to hurt your re-election chances a whole lot more than simply saying, yeah, Trump broke the law."

### Oneliner

Senator Graham's attempt to bully the House into revealing the whistleblower's identity undermines the Constitution and raises concerns about Senate integrity.

### Audience

Politically engaged citizens

### On-the-ground actions from transcript

- Contact your representatives to express support for protecting whistleblowers and upholding due process (exemplified)

### Whats missing in summary

The full transcript provides a detailed analysis and criticism of Senator Graham's actions regarding the impeachment process, showcasing the importance of upholding constitutional values and accountability in government.

### Tags

#SenatorGraham #Whistleblower #Impeachment #Constitution #Corruption


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're going to talk about what Senator Graham said.
And basically what he said was the impeachment was dead on arrival if they don't name the whistleblower.
So he's trying to bully them into doing that.
Now what he actually said here was, I consider any impeachment in the House that doesn't
allow us to know who the whistleblower is to be invalid, because without the whistleblower
or complaint, we wouldn't be talking about any of this.
Now first, I would
say that this is somebody who
hasn't read the Constitution. However, this is a Senator.
He's read it, or he should have.
The House has sole power over impeachment.
You, Senator, get to conduct a trial, and if the House impeaches, you are duty-bound
to conduct a trial.
It's not dead on arrival.
That would be a violation of your oath.
I mean, when you really think about it, you're trying to bully...
When you really think about it, you're telling a group,
hey, if you don't do this favor for me,
I won't do what I've promised.
Kind of sounds like what the president's getting impeached for, doesn't it?
There's no due process violation
in any of this.
If he wants to stick to this line of reasoning, I will be eagerly awaiting his
legislation that he will have to introduce that bans Crime Stoppers,
the organization that has resulted in half a million arrests and
the recovery of $4 billion worth of stolen property.
I'm sure that same legislation will also ban the federal government from
operating all of its anonymous tip lines,
as well as state and local agencies.
Because you know, you can't have due process.
It can't be fair if the initial complaint came
from somebody who was anonymous.
It's not how this works.
If the evidence can be confirmed through other means,
such as all the testimony that is now on record,
you don't need the whistleblower.
And you know this.
Whistleblowers and people who like
remain anonymous, do so because they don't want to be smeared by an attorney or some
politician who has put his party over his country.
And that's the reason you want the name.
Because you want to make it about that person rather than the president's actions.
You want to attack the process because you can no longer defend what the president did.
At this point, it certainly appears that Senator Graham is willing to undermine the Constitution
and the rule of law to protect somebody he knows is guilty.
That's what it seems like.
And if he'll do that, how can we trust the Senate at all to do anything?
It certainly appears that the corruption that exists in the Trump administration has now
filtered in to the Senate through the GOP because they're engaging in the same type
of behavior and they're attempting to allow this act to go unanswered simply because it
will hurt their reelection chances.
I don't know about everybody, but failing to uphold your oath and your duty and attempting
to undermine the Constitution through some rhetoric that we all see through, that's going
to hurt your re-election chances a whole lot more than simply saying, yeah, Trump broke
the law.
Anyway, it's just a thought.
I'll have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}