---
title: Let's talk about Veterans Day (Part 3)....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xSgJnZddJaE) |
| Published | 2019/11/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Outlines various instances where veterans were turned into combat veterans throughout history.
- Mentions interventions and wars from early 1900s to present, including conflicts in Nicaragua, Haiti, Dominican Republic, Russian Civil War, World Wars, Korean War, Vietnam War, Bay of Pigs, and more.
- Talks about the U.S. involvement in different civil wars and conflicts globally, showcasing a pattern of intervention.
- Criticizes the continuous cycle of turning veterans into combat veterans and the impact it has had.
- Points out the importance of recognizing and reflecting on the history of U.S. military interventions.
- Suggests that the best way to honor Veterans Day is to stop turning veterans into combat veterans.

### Quotes

- "The best thing we can do for Veterans Day is to stop turning veterans into combat veterans."
- "This isn't even all of it."
- "Maybe go back through this list and try to figure out how many years the U.S. was at peace out of its entire existence."
- "Y'all have a good night."

### Oneliner

Beau outlines numerous instances of U.S. military interventions, urging to stop turning veterans into combat veterans and reflecting on the country's history of conflict.

### Audience

History enthusiasts, anti-war advocates

### On-the-ground actions from transcript

- Research and understand the history of U.S. military interventions (suggested)
- Advocate for policies that prioritize peace over war (implied)

### Whats missing in summary

The emotional impact of continuous warfare on veterans and the importance of acknowledging past mistakes to prevent future conflicts.

### Tags

#VeteransDay #USMilitaryInterventions #AntiWar #History #PeaceBuilding


## Transcript
Well, howdy there, Internet people, it's Bo again.
And welcome to part three of our Veterans Day special,
where we are outlining all of the times
we've turned veterans into combat veterans.
If you haven't watched the first two parts,
go ahead and go watch those first.
We are in the early 1900s now.
I will tell you that once we get to the Cold War,
have pretty much just given up on outlining all of them so I just hit the
ones that that I wasn't confident people were familiar with. To put it bluntly us
destabilizing Iran didn't even make the list. Okay so early 1900s the occupation
in Nicaragua ran from 1912 to 1933. Now we were there in 1909 at request of the
regime and power, but in 1912 U.S. investments started becoming at risk. So
we intervened a little bit more heavily on the side of the right wing that was
involved in the factional fighting there. By 1927 a full-blown civil war had
erupted and we stayed until 1933. Then you've got the Bluff War which ran 1914
to 1915. This is a native war. The 1900s still going on. Vera Cruz is 1914. We
We inserted ourselves into the Mexican Revolution under the flimsiest of pre-Texas.
The occupation of Haiti, 1915 to 1934, the people there took out a dictator that we liked.
We were worried about German influence because they had a lot of the business.
So we took it over.
of the Dominican Republic, 1916 to 1924.
It was politically unstable, we were worried about German influence, so we took it over.
World War I, don't think I need to go over that one, here's one that I think is going
to surprise most people.
Russian Civil War 1918 to 1920 the US deployed more than 10,000 troops to
invade Russia. We were there operating on the side of the Tsar and this is really
the beginning of the Cold War. Why would the Soviets not like us? Because we
fought them before they even had control of the country. If you want to look up
more on that look up the polar bear expedition. Let's see the last Indian
uprising 1923 then we get to World War two Korean War it's 1950 to 53
Laotian Civil War 1953 to 1975. We interjected ourselves into that. Lebanon
1958. We intervened by request on this one and for once it worked out. That one
went pretty well. The Bay of Pigs in 1961. US-sponsored Cuban exiles trying to
take out Castro. The Simba Revolution in 1964. This is communist versus capitalist, Cold
War, Masters of the Universe type stuff. The Vietnam War, communist insurgency in Thailand
in 1965 to 1983. Here's one for you. The second Korean War. 1966 to 1969.
So this is also known as the Korean-DMZ conflict.
It's often said, well, there's no peace treaty about the Korean War.
That's kind of true.
There was an armistice, though.
There was an armistice signed and did hostilities.
Problem is it had a clause about not introducing nuclear weapons.
No new weapons could be brought in.
The U.S. wanted to put nukes there because it could reach China and the Soviet Union.
Even though that clause was there, we just violated it and put the nukes there.
That started a low-level conflict that lasted a couple of years.
The Dominican Civil War, 1965 to 1966, there was a coup.
We inserted ourselves.
The Cambodian Civil War, 1967 to 1975.
same communist versus capitalist stuff. The U.S. in that case supported the Kingdom of
Cambodia and the Khmer Republic, just anybody but the communists. The war in southern Zaire
in 1978, more communist versus capitalist stuff, a mine got taken. That's really why
We got involved.
Lebanon, 1982 to 84, that was a multinational peacekeeping force.
It was an utter failure that time.
Grenada, 1983.
So Grenada had just become an independent country in 79.
There was a coup, and then in 83, there was a coup to take out the guy
who took the last guy out with a coup.
Reagan was worried that medical students there
would get taken hostage.
Wasn't actually worried about them,
was worried about how it would make him look
because of the Iran hostage crisis under Carter.
Libya, 1986, there was a bombing as a reprisal
to an attack on a disco in West Berlin.
Tanker Wars, 1987 to 88, Iraq provoked, Iraq attacked Iran in an attempt to provoke them
into an overreaction, and it worked, and it worked, and the U.S. began limiting Iranian
oil tankers.
I got my tan off the coast of Iran and all of that.
Panama, 1989 to 90, there were a whole bunch
of reasons given for this, drugs, human rights,
whole bunch of stuff.
The reality was they were worried
that Noriega would not be neutral when he came to the canal.
So he had to go, it was about money.
The first Gulf War, Somalia, 92 to 95, Black Hawk down.
Bosnia in war, 92 to 95.
Haiti again, 94 to 95.
This is a cool one.
You wanna talk about gunboat diplomacy.
Our diplomat, trying to get the guy who staged a coup,
trying to get him to step down, literally just showed him
a video of US troops flying in.
We're already loaded up.
We're gonna invade your country.
If you step down, you'll save a lot of lives.
And he did.
And that actually went, I mean, as well as can be expected.
Kosovo, 98 to 99, Operation Infinite Reach in 98, that was cruise missile attacks targeting
the organization responsible for September 11th, this unmitigated failure.
He got Afghanistan, 2001 to present, Iraq, 2003 to present, Pakistan, 2004 to present,
Interestingly enough, when this all gets reframed in a history book, it will probably be Pakistan that becomes the
important one,  because that's the first real drone war.
Most of the operations there were drone, so it will have some historical significance.
Somalia, again, 2007 to present, mostly drones and spec ops raids. Ocean Shield,
2001 to 2016, that's the Indian Ocean. Pirates again. Libya, 2011 to present. Unmitigated
failure. Uganda, 2011 to 2017. Syria, 2006 to present. We've propagandized. The populace
started a rebellion, destabilized the country, primarily for economic reasons. And then we've
We've got Yemen 2015 to present, where we have light involvement in an advisory role.
So this is it.
This is it.
And this isn't even all of it.
I think the best thing that we can do for Veterans Day is to stop turning veterans into
combat veterans.
might be the best thing we can do. And as extra credit, maybe go back through this
list and try to figure out how many years the U.S. was at peace out of its
entire existence. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}