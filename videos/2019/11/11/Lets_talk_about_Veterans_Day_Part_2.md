---
title: Let's talk about Veterans Day (Part 2)....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=c_fiqEuwXuE) |
| Published | 2019/11/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Veterans Day special covering America's conflicts, not just major ones in history books.
- Apache Wars lasted from 1852 to 1924, consisted of constant skirmishes.
- US military deployed 5,000 men to deal with Geronimo and his 30 followers.
- Bleeding Kansas (1854-186) determined if Kansas was to be a slave state.
- Puget Sound War (1855-1856) part of the Akimlu War, notable for a chief's execution.
- First Fiji expedition in 1855 due to a civil war incident involving an American businessman.
- Rogue River Wars (1855-1856) in Oregon marked another land grab.
- Various conflicts like Third Seminole War, Yakima War, Second Opium War, Utah War, and Navajo Wars involved land grabbing.
- Second Fiji expedition in 1859 rumored to involve two Americans being eaten by Natives.
- Dakota War, Colorado War, Shimonoseki campaign, Snake War, Powder River War, and Red Cloud's War were all land grabs.
- The Great Sioux War (1876), Buffalo Hunters War, Pierce War, Bannock War, Cheyenne War, Sheep Eaters War, and White River War were all about seizing land.
- Pine Ridge Campaign (1890-1891) led to the Wounded Knee massacre.
- Spanish-American War (1898) triggered by the USS Maine incident.
- Philippine-American War (1899-1902) followed the Spanish-American War.
- Boxer Rebellion (1899-1902) and Crazy Snake Rebellion (1909) occurred due to various reasons such as stolen meat.
- Border War (1910-1916) involving Pancho Villa, and the armed uprising of independence of color in Cuba (1912).

### Quotes

- "We're always at war. We just don't always talk about them."
- "Looking back at it through clearer eyes, a lot of this wasn't just."

### Oneliner

Beau covers America's lesser-known conflicts, exposing a history of constant war and injustice.

### Audience

History enthusiasts, activists

### On-the-ground actions from transcript

- Research and learn more about the lesser-known conflicts in American history (suggested)
- Advocate for educational reforms to include a comprehensive history curriculum (suggested)

### Whats missing in summary

The emotional impact of realizing the continuous warfare and historical injustices in American history.

### Tags

#AmericanHistory #VeteransDay #Injustice #LandGrab #Activism


## Transcript
Well, howdy there, internet people, it's Bo again.
So this is part two of our Veterans Day special
where we are going through all of America's conflicts,
at least all the ones I can think of.
Not just the major ones that are in history books.
So we're just going to jump right back in where we left off.
The Apache Wars ran from 1851 until about 1924.
Wasn't constant.
It was skirmishes over and over again.
Interesting piece of this is Geronimo, of course.
And the US military put 5,000 men in the field
to deal with Geronimo and his 30 guys.
All right.
We've got the bleeding of Kansas, bleeding Kansas.
That's 1854 to 1861.
This was the Civil War's rehearsal.
It was to determine whether or not Kansas was going to be a slave state.
You have the Puget Sound War from 1855 to 1856.
Now, this is seen as a part of the Akimlu War and separate from it.
It's weird.
One of the most notable things about this is that there was a chief who was executed
for murder during the war.
The U.S. was really bad about Native rights in almost every respect.
However, typically, typically, they were seen as combatants and they were treated like that.
This is a case where that didn't happen.
They had to try him twice.
The first one ended in a hung jury, and the second they convicted him.
You have the first Fiji expedition, that's 1855.
So there was a civil war going on, and an American agent, American businessman, his
house caught fire, was burned, something like that, and the U.S. Navy showed up and demanded
money.
The Rogue River Wars, that's 1855 to 1856,
that was another land grab up in Oregon.
The Third Seminole War, the Yakima War, the wider one,
more land grabbing.
The Second Opium War, 1856 to 1859.
Real history buffs right now are going,
we weren't involved in that.
And that's right, they're right.
For some reason, it is now kind of being taught
that we were, we weren't.
We were there at the same time, the US Navy was there.
A merchant US ship was fired upon.
So the US Navy took some forts and then said,
no, we're neutral in the war, but at the same time,
secretly helped out the French and British.
Let's see.
The Utah War, 1857 and 1858.
This was minor skirmishes between Mormons and the US military, no joke.
The Navajo Wars, that's 1858 to 1866.
This ended with the Long Walk.
I didn't talk about the Trail of Tears.
remember that all native groups that got moved they all went through that and the
in various ways this was the Navajo the long walk anyway second Fiji expedition
1859 I can be wrong about this one I'm a little hazy on this but I want to say
that there was a rumor that two Americans were literally had for lunch by some Natives I think.
Let's see, John Brown's raid 1859. Let's see, the first and second Cortina War. That's 1859 to 1861.
And that was a ranch war, the Civil War, of course, 1861 to 1865, the Dakota War, 1862,
the land grab, the Colorado War, 1863 to 1865 land grab, even during the Civil War this
was still going on.
Let's see, the Shimonoseki campaign in 1863 to 1864.
The Tokugawa Shogunate was pretty friendly to outside influence, however, other elements
within Japan were not, and at some point the kind of command went out to expel the barbarians.
That's us.
And in the process of that, a merchant ship was attacked.
US Navy got involved. Let's see, we've got the Snake War that's 1864 to 1868,
Land Grab, Powder River War 1865, Land Grab, Red Clouds War 1866 to 1868.
Interesting thing about this one, the natives won. The Formosa Expedition in
1867, that's pirates in Taiwan. The Comanche Campaign 1867 to 1875 wasn't
only against Comanches though. The Korean expedition in 1871, we were in their
waters. They shot at us and we shot at them. The Modoc campaign, that's 1872 and
1873. The Red River War 1874-1875, that ended the Texas Indian Wars. Let's see,
The Great Sioux War 1876, Buffalo Hunters War 1876-177, Pierce War 1877, Bannock War
1878, Cheyenne War 1878-1879, Sheep Eaters War 1879, White River War 1879-1880.
All of these are land grabs.
Pine Ridge Campaign, 1890 to 1891, that's Wounded Knee, that's when that happened.
Second Samoan Civil War, 1898 to 1899, that was at the end of this, Germany, the UK, and
the US divided up the area.
The Spanish-American War, 1898, remember the main, and then come to find out it probably
wasn't attacked by the Spanish, but that triggered that war.
So during that war, we supported the Filipinos, and immediately after that war ends, you have
the Philippine-American War, which is 1899 to 1902, because they were not really happy
about the idea of just having us become their new rulers.
They kind of thought that they were going to get freedom after kicking the Spanish out.
See Boxer Rebellion 1899 to 1901, Crazy Snake Rebellion in 1909, that's an interesting one.
So there was the accusation that either the natives or their African American friends
stole some smoked meat.
And the sheriff formed a posse to go get them, and both the natives and the African Americans
realized they're probably not going to get justice, so it sparked a little rebellion.
The border war, 1910 to 1916, think Pancho Villa.
The armed uprising of independence of color.
This was in 1912 in Cuba.
Basically black Cubans had had enough.
They were done, and they fought back, and they did really well against the Cuban military,
and then their president contacted the U.S. and the Marines went down.
This is what I would consider the start of the Banana Wars, and this seems like a good
place to take a little break in this series.
We'll be back shortly with the rest of it.
We are going to highlight the next 100 years, but again, we're showcasing that as much
as we talk about other countries being warmongering, you can't find many years of peace in US history.
We're always at war.
We just don't always talk about them.
militaristic as we are, we like to forget about a lot of this because looking back
at it through clearer eyes, a lot of this wasn't just. Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}