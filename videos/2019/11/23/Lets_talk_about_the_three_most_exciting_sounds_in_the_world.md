---
title: Let's talk about the three most exciting sounds in the world....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=SwkdHcm19Iw) |
| Published | 2019/11/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says: 

- Beau talks about the exciting sounds that signal adventure like the whistle of a train, an anchor chain, and an airplane engine.
- He mentions how in the internet age, people can see everything online, losing the desire for adventure and contact with strangers in strange places.
- Americans travel abroad and leave their state very little, missing out on experiencing their own communities.
- Beau encourages exploring one's community, even if it's close by, to see something worth experiencing.
- He points out that travel broadens the mind not just by moving from one place to another but by being present in a different community.
- Beau suggests that being part of a community and experiencing it can have a significant impact, especially in today's detached society.
- Taking kids to places like fairs or zoos can be mundane for adults but a fascinating experience for children as they interact with new things.
- Amid the division portrayed in the news, Beau believes it's vital to realize that people aren't as divided and hostile towards each other as it may seem.
- He hints at the importance of reconnecting with one's community, appreciating the simple sounds and experiences around us.

### Quotes

- "Those sounds signal that adventure is coming, travel is coming, we're going to see something new."
- "It might be time to visit your own community and experience it rather than just see it."

### Oneliner

Beau talks about the importance of exploring and experiencing one's community to broaden the mind and combat societal detachment.

### Audience

Community members

### On-the-ground actions from transcript

- Visit a local landmark or special place in your community (implied)
- Take your kids to new places like fairs or zoos to let them interact with different things (implied)

### Whats missing in summary

The full transcript includes insightful reflections on the impact of the internet age on people's desire for adventure and connection with their communities. Viewing the full transcript can provide a deeper understanding of Beau's message.

### Tags

#Community #Adventure #LocalExploration #Connection #Travel


## Transcript
Well howdy there internet people, it's Bo again.
You know they say the most
exciting sounds in the world
are the whistle of a train,
an anchor chain,
and the sound of an airplane engine.
Probably true.
It's probably true.
Those sounds
signal that
adventure is coming, travel is coming, we're going to see something new.
It's one of those things that we're losing
with the
internet age because we can see everything
online.
We can just pull it out and look.
I tend to agree with the
professor
from Goodwill Hunting
in that regard.
And you can look at a photo of the Sistine Chapel, but you won't be able to tell me
what it smells like in there, or how your voice echoes, or your footsteps.
One of the problems that is coming with the Internet Age is that we are losing our desire
for adventure.
And with that, we're losing contact with strangers in strange places.
We understand and we can look up how people act in different places, but we don't experience
it.
There's a lot of stats about how little Americans travel abroad.
There's a lot of stats about how little Americans leave their state.
But the thing is, to get that experience, you don't have to travel that far.
You don't have to, you don't have to leave.
Because the odds are somewhere, where you're at, whatever the community is, there's something
worth seeing that you never have.
But we don't leave anymore.
We don't get out in our community.
It doesn't matter where the community is.
If you're in a major city, you know exactly what I'm talking about though.
You have a friend come to town, and they want to see Golden Gate Bridge.
They want to go to the Statue of Liberty, and you've never gone, because you live there.
You take it for granted that you can go any time.
It's like living near the beach.
We don't go that much.
right there. And we do. We take it for granted because we always think that we can go tomorrow.
There may not be a tomorrow. No matter where you live, there's something worth seeing.
There's a landmark. Even if it's not a major one. Even if it's just something that is significant
and special to your community, you can get out and go there, and you can experience it.
And that's why those sounds are exciting.
People say travel broadens the mind.
It's not really travel.
It's not the act of going from one place to another.
It's the act of actually being in a community.
It's the act of being present somewhere different.
And it may not seem like it, but with how detached we've become from our communities.
We can get that same effect and that same feeling a lot of times right where we live
because we aren't part of a community anymore, not really.
It could be something as simple as going to a fair.
You know, one of the things I love is taking my kids somewhere.
It's something that for most of us, mundane.
But the first time they see a fair, a zoo, something like that, and they can interact
with these different things that they've only seen on TV, you have to watch the phenomenon
that I'm talking about take place right in front of you.
It might be time with as divided as the news and all things coming out right now, as divided
as it makes us seem, it might be a good idea to get out and realize we're not really at
each other's throats.
We're not really on the verge of civil war.
Not even close.
It might be a good time to hear one of those three sounds or even just the hum of your
engine.
The steps of your feet on the sidewalk, whatever.
It might be time to visit your own community and experience it rather than just see it.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}