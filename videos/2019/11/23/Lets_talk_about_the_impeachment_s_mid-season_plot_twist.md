---
title: Let's talk about the impeachment's mid-season plot twist....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=5J5Prg2bV98) |
| Published | 2019/11/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Excited to share about the impeachment episode and the unexpected plot twists.
- Overview of the alleged attempts by the President to extort Ukraine into investigating Hunter Biden.
- Mention of Ukraine as a strategic partner for the United States.
- Description of Nunez as the President's biggest defender during the impeachment inquiry.
- Details about Ukrainians tied to Rudy Giuliani and Fraud Guarantee getting arrested for campaign finance issues.
- Reference to evidence implicating Nunez from the beginning and the House Democratic Coalition filing an ethics complaint.
- Mention of emails linking Giuliani to the White House and Secretary of State Pompeo in efforts to smear the ambassador in Ukraine.
- Suggestion to grant immunity to a Ukrainian involved and have him testify publicly.
- Criticism of corruption within the administration and lack of trust in impeachment proceedings.
- Reflection on valuing substance and leadership in politics.

### Quotes

- "I didn't have this on my impeachment bingo card."
- "I gotta know how this season ends."
- "I'd rather have this Ukrainian sitting in the Oval Office than the President."
- "It's not just a symptom of the Republican Party, it's a symptom of us not valuing substance, not valuing leadership."
- "I'm hooked on this show."

### Oneliner

Beau gives an overview of the impeachment episode, pointing out twists and suggesting granting immunity to a key Ukrainian figure for testimony amid corruption concerns.

### Audience

Politically Engaged Individuals

### On-the-ground actions from transcript

- Grant full immunity to the Ukrainian figure implicated in the events for public testimony (suggested).
- Keep a close eye on the individual involved (suggested).

### Whats missing in summary

Insights on the potential consequences of corruption and the importance of valuing substance in politics.

### Tags

#Impeachment #PoliticalAnalysis #Corruption #Leadership #Ethics


## Transcript
Well howdy there internet people, it's Bo again.
I know most of y'all won't watch this till tomorrow,
but I'm just excited.
I already saw the episode.
I am really glad that they used M. Night Shyamalan
as the director for the impeachment,
because I did not see this mid-season plot twist coming.
I did not have this on my impeachment bingo card.
Um, but to catch everybody up on the season thus far, the president of the
United States is alleged to have attempted to extort Ukraine into
announcing an investigation into Hunter Biden in order to smear former vice
president Biden didn't matter if the investigation actually took place.
They just wanted the announcement in order to facilitate this.
withheld military aid and a meeting with the president that would have shown that the United
States cares about the Ukrainian people.
Ukraine is a pretty important strategic partner for the United States.
That is true.
So this is what triggered the impeachment inquiry.
Now, during the inquiry and hearings, the president's biggest defender is a guy
named Nunez, and this whole time, he's, I mean, basically just in disbelief.
This is ridiculous.
This is preposterous.
This is some wild theory.
Nobody would be so bizarre as to attempt to dig up dirt on the Bidens through Ukraine.
It's ridiculous.
Fair enough.
Now, so that's one plot line.
Another plot line is that a couple of Ukrainians that are tied to Rudy Giuliani and a company
called Fraud Guarantee, well, they get popped for some campaign finance stuff and get arrested.
The President of the United States says that he doesn't know them, and that's fine.
This apparently angered one of those Ukrainians who offered to provide evidence to the impeachment
inquiry.
And that evidence is said to include proof that Nunez was involved from the very beginning.
And his congressional travel records seem to support this claim.
When he was questioned about it by the press, he said, I'm not going to answer your questions.
I don't acknowledge your questions in this life or the next.
I mean, I don't know about you, but that to me just screams he's innocent.
So the House, the House Democratic Coalition of the House, something like that, has filed
an ethics complaint, obviously.
It would be highly unethical for him to sit and depose witnesses in an action that he
was apparently a part of.
Yeah, I didn't see that one comment.
That is something else.
And it's also cast doubt on every single other Republican that has defended the President.
That's the end result of this.
Yeah.
That's crazy.
Now, the next part of the show was already a little bit
foreshadowing going on.
Right before midnight tonight, under a judge's order,
the State Department released a whole bunch of emails.
Now, I will tell you, I have not read them all,
but my friends that have are telling me
that it pretty much shows a direct link
from Giuliani to the White House
to Secretary of State Pompeo, in regards to the effort to smear our own ambassador in
Ukraine, because she was getting in the way of them extorting a foreign country into providing
a thing of value for his campaign.
I can't wait to see what's going to happen next.
In that vein, I would suggest that we hand this Ukrainian full immunity, allow him to
come in before the impeachment inquiry, before the hearings, publicly, and just tell us what
he knows.
I mean, it's campaign finance.
Nobody really got hurt.
Give him full immunity.
Let's hear what he has to say.
My understanding is that this man believed he was on a secret mission for the President
of the United States attempting to help secure his new country and he's doing
his patriotic duty and now the president has disavowed him. He's probably a little
angry about that but welcome to being a secret agent that's what kind of what
happens if you not watch the movies. But I say we give this guy immunity and we
let him talk because at this point I gotta know how this season ends. I gotta
to know what's coming next. I'm hooked. I am hooked on this show. I would also
suggest we keep an eye on this individual. I hate for him to be
upstained. So anyway, we're at a point where we can't trust even the people who
are conducting the impeachment hearings. The corruption from the administration
has bled so far that even members in the House are now being swept up in it.
This is, and it's not just a symptom of the Republican Party, it's a symptom of us
not valuing substance, not valuing leadership, us looking for talking
points and sound bites instead of those people who truly care about the country.
Honestly, if what I understand is true, I'd rather have this Ukrainian sitting in the
Oval Office than the President, because at least he cared about the U.S.
Anyway, it's just a thought.
Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}