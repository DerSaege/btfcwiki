---
title: Let's talk about a statistic out of California and blue-collar skills....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=aZXIDiiZQ6U) |
| Published | 2019/11/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about statistics from California regarding homemade firearms.
- Mentions the significance of the data but notes that it's often overlooked.
- Raises the issue of individuals building firearms at home to bypass bans.
- Mentions the ease of manufacturing firearms at home, referencing the ATF report.
- Points out that 30% of firearms in California are homemade, indicating a significant trend.
- Notes that homemade firearms are used to circumvent California's restrictions.
- Emphasizes that manufacturing firearms at home is easier than dealing with neighboring states' gun laws.
- States that a variety of firearms, from pistols to AKs, are being produced at home.
- Explains the process of making firearms at home, focusing on manufacturing the receiver.
- Stresses the importance of addressing root causes rather than focusing on banning specific parts.

### Quotes

- "Now we do because the information is out."
- "It tells us a lot and it tells us that this is something that has to be taken into consideration anytime legislation is proposed."
- "So this shows us again that we have to address root causes."
- "People in the United States have way too much of a desire to own a firearm."
- "Y'all have a good night."

### Oneliner

Beau reveals the significant trend of homemade firearms in California, underscoring the need to address root causes rather than focus on bans.

### Audience

Legislators, Gun Control Advocates

### On-the-ground actions from transcript

- Advocate for comprehensive legislation addressing root causes (suggested)
- Raise awareness about the implications of homemade firearms (implied)

### Whats missing in summary

The full transcript provides in-depth insights into the prevalence and implications of homemade firearms, urging a focus on root causes rather than reactive measures.

### Tags

#Firearms #Homemade #California #Legislation #RootCauses


## Transcript
Well, howdy there, internet people, it's Beau again.
So tonight, we're gonna talk about some information
that came out of California, some statistics.
And this data is very important, but for some reason,
I've only seen it buried at the bottom of articles.
I'm not sure people understand how much information
can be extrapolated from this one little figure.
It's something we've talked about on this channel before
in relation to what could happen if people,
the government banned certain types of items.
People would build them at home.
And I talked about the fact that it's very easy.
And people ask, well, is this a skill set
that only blue collar people have?
Is this a skill set that's only in the rural areas?
Would criminals go to these lengths to do this?
And I'm over here like, I have no idea.
I don't know.
But now we do. Now we do because the information is out. The ATF is telling us that 30% of the firearms they are pulling
out of California now are homemade.
That is a huge number. It's a huge number and it tells us a whole lot about whether or not that's going to be something
that is widely done.  First, will it be done to get around gun bans?
Apparently, because California doesn't have an outright gun ban.
They have a lot of restrictions.
It's heavily restricted.
But there's still a wide variety of firearms that can be purchased legally.
It will be used to get around that, because it is being used to get around that.
Then it's easy.
It's easy.
has either been developed or existed. We know that it's easy because Arizona and
Nevada, two nearby states, have very lax gun laws. I think in 2020 Nevada will
start doing background checks on private cells but as it stands they don't. What
this tells us is that it's easier to manufacture them at home than it is to
drive a couple hours and lie about where you live. That tells us a lot and it tells us
that this is something that has to be taken into consideration anytime legislation is
proposed. The types that are being made range from semi-automatic pistols to AKs. There
There does not seem to be a limit on what can be manufactured at home.
When I discussed this with somebody earlier, they're like, well, can't we just ban the
parts?
No, not really.
The skill set is now there.
They understand, they will completely grasp that most of this is available at Home Depot.
This is not, you don't have to use the finished parts, which a lot of these, a lot of the
California did. It really looks like they're manufacturing one piece, the
receiver. It looks like they're making that at home and buying everything else
on the market. The thing is, the receiver is one of the more complicated pieces.
That's the piece that's that is actually considered the gun. If they can
manufacture that, if they can complete that from a from a template, whatever,
they can do the rest of it. So all of these parts are available at your
hardware store. It's just metal. So this shows us again that we have to address
root causes. Anything else is not going to work. People in the United States have
way too much of a desire to own a firearm it's tied to the DNA of the
country it's gonna have to be removed here up in somebody's head before you
ever gonna be able to get it out of people's hands anyway it's just a
thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}