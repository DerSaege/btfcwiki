---
title: Let's talk about learning from sliced bread and Gibraltar....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=EBCSjUkywBM) |
| Published | 2019/11/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Examines the lessons to learn from the sacrifices made by the greatest generation during WWII.
- Describes the lesser-known but significant actions taken by individuals facing a global threat.
- Recounts the story of six individuals who volunteered to stay in secret chambers in Gibraltar to report back if it fell to the opposition.
- Reveals the commitment of these individuals who were willing to be sealed inside the chambers with years worth of supplies.
- Mentions Operation Tracer, a compartmentalized plan involving multiple teams willing to stay sealed in rooms for years.
- Contrasts the minor inconveniences faced in the U.S. during WWII, like the temporary ban on sliced bread, to contribute to the war effort.
- Draws parallels between the sacrifices made by the greatest generation and the lack of willingness in today's society to make similar sacrifices in the face of a global threat.
- Criticizes the reluctance of current generations to give up conveniences like single-use straws when compared to the sacrifices of those before them.
- Emphasizes the importance of both dramatic and mundane actions in addressing present-day global threats.
- Calls for a shift in mindset towards tackling real global issues with the same dedication and sacrifice as seen in the past.

### Quotes

- "Yeah, these things aren't as dramatic as storming a machine gun nest on D-Day, but it's those little things that could truly alter the course of history."
- "Today, people don't want to give up single-use straws. That's too much to ask when confronted with a global threat."
- "These were people who lived through the Depression. They knew what hard times were."
- "We have a global threat and it's a real one. Doesn't matter what the study from the oil company tells you."
- "It's going to take both the dramatic and the mundane to solve it anyway."

### Oneliner

Beau examines the sacrifices of the greatest generation and criticizes the modern reluctance to make similar sacrifices in the face of global threats.

### Audience

Activists, History Enthusiasts

### On-the-ground actions from transcript

- Form a local community group dedicated to environmental conservation and taking small, impactful actions. (implied)
- Educate others about the historical sacrifices made during times of crisis to inspire collective action. (implied)

### Whats missing in summary

The full transcript provides a compelling comparison between the sacrifices made by past generations during crises and the lack of willingness in modern times to make similar sacrifices in the face of global threats.

### Tags

#WWII #Sacrifice #GlobalThreats #EnvironmentalConservation #CommunityLeadership


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight, I wanna talk about the greatest generation
and what we can learn from them.
Because when we think about them today,
we think about the dramatic.
We think about D-Day, stuff like that.
We don't think about the minute, and maybe we should.
We don't think about the smaller things
that people were willing to do,
sacrifices they were willing to make in the face of a global threat, something that was
going to alter the course of humanity.
Six guys agreed to do something that today wouldn't even really be asked.
France had fallen, France had fallen, and there was real worry in the United Kingdom.
One of the worries was that this two square mile rock, Gibraltar, was going to fall.
Now today, it's not strategically important.
Then, it was a big deal.
It was a big deal.
It provided a lot of observation posts.
there was a real worry.
So in the event that it fell, the British government came up with a plan.
They built secret chambers and tunnels on this rock that overlooked the Mediterranean.
Six guys agreed to stay behind in these secret chambers and watch if it fell to the opposition.
They were going to record and transmit back what was going on.
The chambers were stocked with years worth of supplies.
about that. It's a pretty big commitment. Even more so when you find out they were
going to be sealed inside. They couldn't leave if they wanted to. They were going
to wait until Gibraltar was retaken. Sick people agreed to do this and there's a
possibility that Moore agreed to do it, we just don't know about it yet, was
called Operation Tracer. It was compartmentalized, so the one area that
was found, the one chamber that was found, they may not know, but it does appear that
there were other teams that were going to do this, people that were going to stay
for years sealed in a room. In the U.S., minor conveniences, sliced bread was
banned for real. Didn't last long, but basically the government thought that if
If they got rid of that, people would eat less and that would help conserve.
That would help the war effort.
An entire world united because there was a global threat.
People were literally willing to give up sliced bread.
were willing to seal themselves in chambers for years to accomplish this
goal. Yeah, these things aren't as dramatic as storming a machine gun nest
on D-Day, but it's those little things that would truly alter the course of
history. Today, people don't want to give up single-use straws. That's too much to
ask when confronted with a global threat. And normally, it's those people who truly
idolize the greatest generation but they do it for all the wrong reasons. These
were people who lived through the Depression. They knew what hard times
were. They were willing to take those tough steps to do those hard things so
their kids and grandkids doesn't have to. Today, why do I need to worry about the
environment? I'll be dead by then. Let's go watch Saving Private Ryan. We have a
global threat and it's a real one. Doesn't matter what the study from the
oil company tells you. It's a real one and it's going to take both the dramatic
and the mundane to solve it anyway it's just a thought y'all have a good night

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}