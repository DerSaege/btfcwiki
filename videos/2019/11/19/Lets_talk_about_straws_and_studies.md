---
title: Let's talk about straws and studies....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=TTkSFu1OQOI) |
| Published | 2019/11/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduced the topic of straws and studies, addressing the controversy surrounding giving up plastic straws.
- Pointed out that most people misinterpret information and discussed the impact of 100 companies producing 71% of emissions.
- Raised concerns about how the U.S. might be defeated by a simple plastic straw due to lack of suitable alternatives.
- Proposed using plant-based, biodegradable straws as a viable solution, mentioning their existence and cost.
- Urged for individual responsibility and action in tackling environmental issues, contrasting popular misconceptions about emissions.
- Advocated for a cultural shift away from profit-driven motives and emphasized the power of individual action over waiting for governmental intervention.
- Stressed the importance of sending letters to major corporations like McDonald's to drive change and mentioned the effectiveness of boycotting.
- Encouraged viewers to believe in the impact of their voices and emphasized the significance of individual actions in inciting change.

### Quotes

- "We can't be defeated by a straw."
- "Your voice matters more than you think."
- "It requires individual action."

### Oneliner

Beau addresses misconceptions on emissions, advocates for plant-based alternatives, and stresses the power of individual action in driving environmental change.

### Audience

Environmental activists, conscious consumers

### On-the-ground actions from transcript

- Contact major corporations like McDonald's to advocate for sustainable alternatives (suggested)
- Boycott companies to drive change towards sustainability (exemplified)

### Whats missing in summary

The full transcript provides detailed examples and explanations on the impact of individual actions on environmental issues. Viewing the full transcript will offer a comprehensive understanding of Beau's arguments and suggestions.

### Tags

#Environment #PlasticStraws #IndividualAction #ClimateChange #CorporateResponsibility


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, I guess we're going to talk about straws and studies.
That last video I mentioned giving up plastic straws, and man, that opened a can of worms.
So before we really get into this, I want to say that if you were in that comment section,
understand that most people don't know what I'm about to say.
people interpret that information the way you guys are interpreting it.
But it's not right.
And I'm talking about that 100 companies produce 71% of emissions.
We'll get to that.
But the real problem, the real spark was this.
We're going to be defeated by this.
The United States of America will be defeated by a plastic straw.
We can't figure a way around it because the metal ones are dangerous on impact.
The glass ones can break in your mouth.
The paper ones, admittedly, are not very good.
And I mean, you got silicone.
That's what you got.
Those are all substitutes for this.
Why can't we just make this?
Why can't we make that out of something else?
Maybe a plant-based straw that's biodegradable.
It already exists.
Single-use plant-based straws already exist.
A company called Plasticless, there's actually
other companies that make it.
I'm more familiar with them.
You wouldn't know whether this was one of theirs or not
by looking at it.
Normal plastic straws cost $2 for $100.
Theirs cost about $4, so they're more expensive.
But that's because they're not being widely used.
If they were scaled up in use, production
scale up and the price would drop. It's that simple. We are the U.S. and we are not going
to be defeated by a straw. We can do a lot of things. When I upload this video, people
on the other side of the world are going to see it seconds later. But we can't make this.
Oh, I don't believe that. Not for a second. But there's always the reason. There's always
a reason not to get involved. There's always a reason to say, well, let the government
handle it, to find some other way than accepting responsibility for it, because
it's scary to accept responsibility for it, admittedly, but we all have
responsibility for it. That study that gets quoted so often, a hundred companies
responsible for 71% of the emissions, that is not what that study says. What
that study says is that 71% of emissions can be traced back to a hundred
companies. So when I fill up my truck at Exxon and I drive down the road under
that study, Exxon gets credit for me driving down the road. So those numbers
prove that the individual is actually the person that can make the difference.
not the opposite. It's touted as the opposite by a lot of news outlets that
have shared interests with companies that don't want change. And anytime
somebody comes up with something, electric car, well then you're just going to burn
coal instead of oil. Well, coal needs to go too. Look at that. We're gonna get rid
of a couple things at once. Well then that's gonna get... what about coal miner
jobs? Well, we're gonna get retrained. We're gonna get you retrained. You're
gonna do the renewable stuff now. And it's okay because education is gonna be
universal. When I say we need a culture shift, a true cultural revolution in this
country, I mean it. We need to change the way we look at everything. Profit
should not be the motivator. And it's gonna start with us. We can't say, oh the
corporations own all these politicians, these big companies they just buy them
off and then say don't worry the government's gonna fix it. That doesn't
even make sense. It's up to you. It's up to me. It doesn't require legislation. It
It requires individual action.
Yeah there are a lot of people that are economically out of the game where $2 means a lot and they
can't do it.
But that's not everybody.
That is not everybody.
We cannot base our strategy on the outliers.
There's no need to ban straws.
We just have to start acting.
We have to send letters to McDonald's, whatever the biggest fast food joint is.
Get them to switch.
And then boom, it happens.
Boycott them if you have to.
Think it won't work?
All companies care about is profit.
Look at Chick-fil-A.
Look at Chick-fil-A's sudden change.
Your voice matters more than you think.
Individual action, that's the key here.
We can't wait for the government, they're not going to do anything.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}