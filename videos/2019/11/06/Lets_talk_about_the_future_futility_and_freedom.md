---
title: Let's talk about the future, futility, and freedom....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=yjMEC8KTqLc) |
| Published | 2019/11/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the essence of conservatism, freedom, and futility, reflecting on the scary future and the theme of the universe being wild and free.
- Describes how the future should bring more freedom for more people, encouraging innovation and letting go of old ideas to become better.
- Compares the progress of freedom to waves on a beach, with each wave reaching a bit further ashore.
- Mentions how some people find the advancing freedom scary as it challenges their beliefs, leading them to reach back to preserve dated ideas.
- Shares the story of Americana City, where people moved to Brazil to preserve their culture but ultimately assimilated into the new society.
- Examines the Make America Great Again voter demographic, born in 1966 or before, seeking clarity and purpose in a changing world.
- Points out how the older generation's upbringing during segregation influences their views, even if they don't overtly hold those ideas anymore.
- Talks about the fear of the unknown and reluctance to change among certain groups, contrasting them with the younger, more progressive minds joining the electorate.
- Emphasizes the inevitable progression towards a future where all individuals are free and equal, predicting the fading of old ideas and institutions.
- Concludes by acknowledging the unstoppable nature of progress towards a more inclusive and equitable society, despite resistance from some individuals.

### Quotes

- "The future is more freedom for more people."
- "That wild and free future cannot be stopped."
- "The future is a future where women, racial minorities, gender and sexual minorities are free and equal."

### Oneliner

Beau dives into conservatism, freedom, and the pushback against progress, envisioning a future of increasing freedom and equality despite resistance from some.

### Audience

Progressive individuals

### On-the-ground actions from transcript

- Connect with younger individuals to encourage progressive thinking and inclusivity (implied)
- Challenge outdated ideas within communities and advocate for equality (implied)

### Whats missing in summary

The full transcript provides a deeper exploration of the resistance to change and the inevitability of progress towards a more inclusive society.


## Transcript
Well, howdy there internet people, it's Bo again.
So tonight we're gonna talk about reaching back,
the essence of conservatism, freedom, and futility.
The future is scary, the future is a scary place.
You know, the theme of the universe is very wild and free.
Our future reflects that, it really does.
The future is more freedom for more people.
Should be.
So what we should be aiming for is we
cast off the old ways and innovate and try to become better.
It's like a beach
when the tide comes in.
Each wave of freedom gets a little bit further ashore.
Yeah, it recedes every once in a while, a little bit.
But see, with the universe, there's no outgoing tide.
It just keeps crashing further and further inland.
And to some people, that is scary.
That is scary.
It forces them to challenge their beliefs.
So they want to reach back.
And I don't mean reach back to their primal instincts.
That's good.
Getting in touch with your animal nature is a good thing.
Understanding that we are just advanced animals
is a good thing.
helps you become wild and free. I'm talking about wanting to hold on to dated ideas, ideas
that were tried and failed. You know, in 1865, there at the end, Dom Pedro II, I think, you
I might want to Google that one, sent recruiters to the Southern United States
because he wanted to bring those technical skills to Brazil, offered him
tax breaks, all kinds of stuff, trying to get them to move.
And a lot of them did.
10,000 took off, left the South and went further South, moved to
different continent, moved to Brazil, and set up a little town called Americana.
Now about 60% of them came back in real short order when they realized it wasn't exactly
the same.
But a decent enough number stayed because they wanted to hold on to that idea so bad.
They wanted to preserve their culture, that culture.
The funny thing happened though, by the second generation, they were speaking Portuguese
and Intermarian.
By the third generation, they had completely assimilated.
That town, Americana City, it's got like 100,000 people in it, still exists, it's still there,
And it still has festivals that are southern in origin, kind of.
But those ideas, those institutions, that culture they wanted to preserve, it's gone
with the wind, it's just gone.
All of that effort, fighting the future, fighting what was inevitable.
Let's turn our attention to the Make America Great Again voter.
If you look at the demographics from the 2016 election, Trump does not get a majority of
voters until you hit age 50 or older.
means they were born in 1966 or before. Cold War kids. Cold War kids. They were ingrained
with that propaganda. Fighting the evil empire and all that stuff. Gave them purpose. Gave
them clarity. And then it just disappeared on them. Can you imagine that? It's got to
to be tough.
And that's what they want to reach back to.
That clarity, that hierarchy, good versus evil, that conflict, you can see it in their
social media posts.
It's why they use the language that they do.
And then, you know, people ask and they wonder, how do they not see the bigotry in their movement?
Really?
1966 or before, they don't see the bigotry in their movement because most of the voters
were alive during segregation.
So even if they don't hold those ideas overtly anymore, it's how they grew up.
Unless they put forth effort to change, it's still there.
It is still there.
That's why they don't notice it.
Because it's a whole lot less than what they grew up with.
So to them it's still an improvement, but that's what they want.
They want that clarity, that hierarchy.
They're desperate for an enemy to give them purpose.
That's why they fall for that simple rhetoric.
rhetoric that mimics Cold War rhetoric. It's an invasion. They're socialists.
The humorous part about that is most of this demographic of people look back on
the early 1940s as like the pinnacle of American achievement. They might want to
look at who was in the White House during that period. Social Democrats like
Like AOC, that's who was leading the country during your greatest generation.
They fear globalist, never understanding that a nationalist is just a low-ambition globalist.
It's the same thing, just on a smaller geographic scale.
There's no difference.
They're governed by this fear of the unknown, by this reluctance to change.
The thing is though, every election cycle, more of them permanently age out of voting.
And every election cycle, more young, untamed, free, wild, unafraid, unconquered, un-propagandized
minds join the electorate.
That wild and free future cannot be stopped.
It's coming one way or another.
The old institutions will pass, just like slavery, just like segregation, just like
the Soviet Union.
They'll disappear.
They will disappear.
The future is a future where women, racial minorities, gender and sexual minorities are
free and equal.
That's where we're headed.
It's inevitable.
It's what's going to happen.
You can fight against it if you want, but there's nothing you can do.
The Make America Great Again voter is a sandcastle on that beach and that tide is coming in one
way or another and yeah they may build a wall around their sandcastle but every year there's
less and less grains of sand to hold it together.
Those ideas are going to fade, they're going to go away.
Maybe I'm wrong.
Maybe they will be successful in terrifying their children and making them believe that
they have to have an enemy.
Passing on that desire to have a hierarchy, to have somebody you're better than, fearing
the other.
It could happen, it doesn't seem like a goal I would pursue, but it could happen.
Why would you want it to, even if these were things that you believed?
Why would you want that world to continue when your youth, the children, don't want
They're tired of it.
Anyway, it's just a thought.
thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}