---
title: Let's talk about the 5th of November....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=9F2PzR6o0_g) |
| Published | 2019/11/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the history of the 5th of November and the Gunpowder Plot.
- Queen Elizabeth I's conflicts with the Catholic Church and James I's actions.
- Details the plot by Guy Fawkes and Robert Catesby to blow up Parliament.
- Mentions the failed attempt due to an anonymous letter and the subsequent capture of the plotters.
- Talks about how the modern image of Guy Fawkes comes from the comic book and movie "V for Vendetta."
- Examines how symbols like Guy Fawkes have transformed into broader symbols of resistance.
- Raises the point about symbols throughout history that resonate without full understanding.
- Beau muses on the evolution of historical symbols and their meanings in modern times.

### Quotes

- "Probably not."
- "But at some point, Guy Fawkes and that mask became more than history, became something else, became a symbol, a symbol of resistance."
- "These slogans, these symbols, these ideas, they resonate for whatever reason."
- "It seems funny that the reality is, for Americans, Guy Fawkes Day is not about Guy Fawkes."
- "It's about a comic character and somebody uniting the faceless masses against an oppressive government."

### Oneliner

Beau dives into the history of the 5th of November, the Gunpowder Plot, and how symbols like Guy Fawkes have evolved into broader symbols of resistance, resonating for unknown reasons.

### Audience

History enthusiasts

### On-the-ground actions from transcript

- Research and learn more about historical events like the Gunpowder Plot and their significance (suggested).
- Engage in dialogues about the transformation of historical symbols into modern-day representations (exemplified).

### Whats missing in summary

Beau's engaging storytelling and historical analysis can be best appreciated by watching the full transcript.

### Tags

#GuyFawkes #History #Symbols #Resistance #Evolution


## Transcript
Well, howdy there, internet people, it's Bo again.
Remember, remember the 5th of November, gunpowder treason and plot.
In the US, we know that.
But do we have any idea what we're remembering?
Probably not.
Those in the UK probably don't need this history lesson.
But to me, this has always been interesting because it shows how a
symbol of resistance gets created and a lot of times those who rally around that symbol
may not really know what it means.
So Queen Elizabeth I was on the throne and let's just say she had some issues with the
Catholic Church. She was excommunicated in 1570, and she in many ways engaged in a
lot of repression of Catholics under her rule. Now when she died, James
I took over, and a lot of people, a lot of Catholics, thought that was going to be
the end of it. They thought that he was going to introduce some reforms and they
were wrong. He didn't. He didn't. In fact, he expelled the priests as well. So when
they realized they weren't going to get a reprieve, there were a lot of
unsuccessful attempts on him. And then in May of 1603, Guy Fawkes, also known as
Guido met with Robert Catesby and some other people. Catesby was the leader, by the way,
not Guy. I think they met in a bar and they hatched this plot and they were going to take
out Parliament, like all of it. They rented a cellar underneath Parliament and they planned
on a day that James I was going to be there, his eldest son was going to be there, as well
as the House of Lords and the House of Commons. Pretty ambitious. And then once the boom had
boomed, Catesby and the other guys, well they were going to start a Catholic uprising. They
They were going to snatch James' daughter and marry her off to a Catholic.
Put a Catholic back on the throne.
That was the plan.
But on October 26th, an anonymous letter warned somebody, don't go.
And that tipped off the authorities that there was a plot.
Maybe, maybe.
There are some people who believe the authorities already knew and they were just letting it
go forward up until right before to further create animosity towards Catholics.
We'll probably never know the truth there.
Now Fox and company, all of them, they ceased to be shortly thereafter November the 5th.
Some of them when they were about to be captured and some of them later.
November 5th, he's not the hero of that day.
He burned an effigy that the kids will trick or treat with him, take him around their effigy
oven and say, hey, a penny for the guy.
The current version that we know in the United States does not come from history.
its image rehabilitation. It comes from a comic book, V for Vendetta, which was
turned into the movie. That's where the image comes from in the U.S. as the
symbol of resistance. And yeah, I guess it was. I guess he was resistance back in
the day, but it's probably not one that Occupy Wall Street or Anonymous would really choose
to be associated with, backing the Catholic Church.
Probably not.
But at some point, Guy Fawkes and that mask became more than history, became something
else, became a symbol, a symbol of resistance.
There are a lot of things throughout history that have happened like this, a whole bunch
of symbols that we may not really understand the meaning of, but they're all around us,
they're all around us.
Not just symbols of resistance, it's everything.
This is something I've always found interesting.
Remember the Maine.
It's not talking about the state.
Tipper Canoe and Tyler Too.
These slogans, these symbols, these ideas, they resonate for whatever reason.
times, they resonate. We don't even know why. It seems funny that the reality is, for Americans,
Guy Fawkes Day is not about Guy Fawkes. That mask is not about him. It's about a comic
character and somebody uniting the faceless masses against an oppressive government.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}