---
title: Let's talk about Canada's water....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2DbS-41Rgt4) |
| Published | 2019/11/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Canadian journalism students and others conducted a massive investigation revealing a national water crisis worse than Flint, with hundreds of thousands unknowingly drinking contaminated water with high lead levels.
- Lead levels in several Canadian cities were consistently higher than Flint's, with 33% of tested water sources exceeding the Canadian guideline of 5 parts per billion.
- There is no safe level of lead, as even a single glass of highly contaminated water can lead to hospitalization, especially harmful to children by lowering IQ, causing focus problems, and damaging the brain and kidneys.
- Schools in Canada are not regularly tested for lead, and even if high levels are found, there is no obligation to inform anyone.
- Approximately half a million service lines in Canada are lead, affecting a significant number of people due to lead pipes corroding and contaminating the water.
- Aging infrastructure in both Canada and the United States is a key issue, necessitating costly repairs to prevent further lead contamination.
- Some Canadian jurisdictions like Quebec are taking steps to address the crisis, with plans to replace both public and private lead pipes.
- Beau questions the priorities of developed countries like Canada and the U.S., urging citizens to care about how government funds are allocated to prevent such crises.
- Beau advocates for prioritizing fixing infrastructure issues like lead pipes over military spending, questioning the government's ability to provide basic services like clean water.

### Quotes

- "There is no safe level of lead. It's all bad."
- "If a government cannot provide the most basic of services, water. If a jurisdiction can't provide that, if a government can't provide that, what good is it?"
- "I personally rather pay to have all of the pipes in Flint fixed rather than send some more bombs overseas."

### Oneliner

Canadian water crisis reveals widespread lead contamination worse than Flint, urging citizens to hold officials accountable and prioritize fixing infrastructure for clean water.

### Audience

Canadians, Americans

### On-the-ground actions from transcript

- Hold local officials accountable for ensuring clean water (implied)
- Advocate for regular testing of schools for lead contamination (implied)
- Support initiatives to replace lead pipes in affected areas (implied)

### Whats missing in summary

Importance of citizen involvement and oversight in ensuring access to clean and safe drinking water.

### Tags

#WaterCrisis #LeadContamination #Infrastructure #GovernmentAccountability #PublicHealth


## Transcript
Well, howdy there, internet people, it's Beau again.
So tonight we're going to talk about Canada's water.
Americans, stick around.
Understand, everything I'm saying for Canada
is true for the United States.
We just don't have a massive investigation that
uncovered it.
We have the same problems.
So Canadian journalism students, university faculty members, a whole bunch of people banded
together and conducted this massive investigation of pretty much the entire country.
And they determined that they are having a water crisis on a national level that is worse
than Flint.
If you did not follow that story and you're a Canadian, we have the same problems.
Flint was a city that got highlighted because it was exceptionally bad.
In Canada, they found that they have hundreds of thousands of Canadians who
have been unknowingly drinking contaminated water, drinking water that
had high levels of lead in it. In several cities, the levels were consistently and
considerably higher than Flint's. I'm intentionally not saying the worst
contaminated cities because if you are a Canadian you need to go look at the list
because even if you're not one of the worst off you you're probably still in
trouble. You need to look at this and you need to hold your local officials
accountable. 33% of tested water sources had lead levels higher than 5 parts per
billion, which is the Canadian guideline. Now in the US we have a limit of 15
parts per billion. They don't have a limit, they have a guideline established
by their federal government, but each jurisdiction handles it in its own way.
There's no unified response to this.
There really is no safe level of lead.
It's all bad. It is all bad.
A single glass of highly contaminated water
can lead to hospitalization.
It's all bad.
In kids, it lowers IQs,
causes problem focusing, it damages the brain, kidneys. It's just all bad. Schools
there are not regularly tested and by my reading, and I might be wrong on this
because this is just something I couldn't really believe, even if the
local jurisdiction tests a school and finds out it has high levels, they're
They're under no obligation to tell anybody is the way I read that.
I might be wrong, but that's certainly the way it appeared.
One of the estimates said that there were half a million service lines that are led.
That's a huge problem.
Service lines service multiple homes or apartment complexes.
That's a lot of affected people.
The water itself, in situations like this, the water is fine.
What happens is that old lead pipes corrode a little bit and chunks of lead break free
and they end up in the water.
And the only way to fix it is to replace it.
There are anti-corrosives that could be put into the water, but it's a Band-Aid.
It doesn't really solve the problem, it just kicks the can a little bit further down the
road. And that's how we, Canada and the U.S., wound up in this situation to begin with.
We have aging infrastructure. It's got to be fixed. It has to be fixed. And it's not
just water lines, it's bridges, it's everything. We have aging infrastructure. But it costs
money. It costs money. And we don't want to spend it, I guess. In Canada, there is no
unified response because it is done by jurisdiction. Some of the jurisdictions
like Quebec, they came out, they're like, we're fixing this now, they
definitely seem to understand the severity of the issue. My understanding
is that, I think it was Montreal, is actually going to replace both the public
side and the private pipes, the pipes that are on private property and let the
homeowner pay them back like anytime in like the next decade and a half when
they get the cash. They're just going to fix it. When we have issues
like this, it just, it boggles the mind.
We're talking about developed countries, countries that are seen as some of the most powerful
on the planet.
But until we as citizens care how our money is spent, stuff like this is going to continue
to happen.
I would personally rather pay to have all of the pipes in Flint fixed rather than send
some more bombs overseas.
Call me crazy.
But here we are.
And at the end of the day, if a government cannot provide the most basic of services,
water. If a jurisdiction can't provide that, if a government can't provide that,
what good is it? If it can't do that, how can it be trusted to do anything? Anyway,
It's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}