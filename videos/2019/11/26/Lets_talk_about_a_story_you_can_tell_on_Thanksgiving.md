---
title: Let's talk about a story you can tell on Thanksgiving....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=igktJc1ufM4) |
| Published | 2019/11/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Tells a story fit for Thanksgiving with themes of poverty, sacrifice, duty, political conflict, and environmental consciousness.
- Narrates the tale of Nikolai Vavilov, a man from a rural town dedicated to ending famine by gathering seeds worldwide.
- Vavilov's pursuit led to the creation of the first seed bank in the Soviet Union.
- Despite his contributions, Vavilov was denounced by a rival with Stalin's favor and sent to the gulags, where he died of starvation.
- During World War II, scientists in Leningrad protected the seed bank through a siege, sacrificing their lives by not consuming the stored food.
- The dedication of these scientists ensured the preservation of vital crop samples for future generations.
- The seeds from this bank have influenced the food on our tables, connecting us to this impactful story of sacrifice and dedication.
- The narrative showcases individuals who prioritize the greater good over their own survival, symbolizing the essence of Thanksgiving.
- Beau leaves listeners with a reflection on the significance of being thankful for food and the enduring legacy of those who work to address global challenges.

### Quotes

- "When you really think about it, people that dedicated to helping others, they literally just waste away right next to food."
- "The idea of Thanksgiving, being thankful for food, I think it fits."

### Oneliner

Beau narrates the inspiring tale of Nikolai Vavilov and Leningrad scientists, embodying sacrifice and dedication for the greater good, tying the story seamlessly to the essence of Thanksgiving.

### Audience

History enthusiasts, environmentalists, storytellers

### On-the-ground actions from transcript

- Preserve local plant varieties and seeds to protect biodiversity (implied)
- Support seed banks or initiatives that focus on crop preservation (implied)

### Whats missing in summary

The emotional depth and impact of the sacrifices made by individuals like Vavilov and the Leningrad scientists in the face of adversity.

### Tags

#Thanksgiving #NikolaiVavilov #SeedBank #Sacrifice #EnvironmentalConsciousness


## Transcript
Well, howdy there, Internet people, it's Beau again.
So tonight I'm gonna tell you a story you can tell on Thanksgiving.
As we get older, those grade school stories, well, they just don't mean that much anymore.
But I still like the idea of Thanksgiving.
So we got a different story, and it is a great story.
It's not from this continent, but it has everything.
The story of somebody coming from poverty to achieve great things, the ultimate sacrifice
and dedication to duty, political end fighting, environmental consciousness, it has it all.
It's a cool story.
Okay, on to the story.
this guy and he grew up in this rural town and growing up, well this guy knew
hunger. He knew hunger. Crop failures had hit his town. He didn't want anybody to
experience that again so he dedicated his life to ending famine. Pretty big
goal there. In pursuit of that he taught himself over the years 15 languages so
he could talk to local farmers as he traveled the world trying to figure it
out and he was one of the first people that really understood what the loss of
biodiversity would mean for the world. Because of that he started gathering
samples, seeds from all over the world, wheat, corn, cereal foods, all kinds of stuff.
He said that it said that he's the greatest plant explorer and he took these and these
wound up being the basis for the first seed bank.
Now this took place in the Soviet Union.
His name was Nikolai Vavilov.
Well there was another plant guy and that guy had Stalin's ear.
And eventually he denounced like 3,000 people.
Vavilov was one of them.
And he was sent to the gulags with a 20-year sentence where he starved to death.
Something straight out of Shakespeare right there, but the story is not over.
His seed bank, while it was put in Leningrad during World War II, if you do not know, Leningrad
was under siege.
It was said that Leningrad must starve.
That seed bank was occupied by a whole bunch of scientists.
And they weren't there to eat the food, they were there to protect it.
And through the darkest days of the siege, they didn't eat one grain of rice, beans,
nothing.
And one by one, nine of them died of starvation, surrounded by stuff they could eat, because
it was that important to keep those samples.
Interestingly enough, some of the food that's going to be on your table on Thanksgiving,
more than likely, is something that was cross-bred with seeds from that seed bank.
It's a great story.
When you really think about it, people that dedicated to helping others, they literally
just waste away right next to food.
Guy travels the world to gather samples because he sees a problem that in reality there's
There's still people today that still don't understand how big it is.
The idea of Thanksgiving, being thankful for food, I think it fits.
Anyway, it's just a thought.
Now have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}