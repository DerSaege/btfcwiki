---
title: We talked about emergency preparedness....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=SCtLFnnyHEE) |
| Published | 2019/11/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Urges everyone to be prepared for natural disasters due to delayed government response.
- Criticizes the inefficiency of federal agencies in disaster response.
- Encourages families to create emergency kits with essentials like water, food, and first aid supplies.
- Emphasizes the importance of being self-reliant and having a plan for evacuation.
- Advocates for teaching survival skills to children in a fun and engaging way.
- Shares examples of people overlooking readily available supplies during emergencies.
- Stresses the value of improvisation and critical thinking learned through survival skills.
- Encourages seeing the world differently by recognizing possibilities and the importance of change.
- Concludes by underlining the significance of learning survival skills for the youth.

### Quotes

- "We're not the sons of the pioneers and the daughters of Rosie the Riveter anymore."
- "It's up to you. If not you, who? Who's gonna solve the problems if it's not you?"
- "Just take care of this stuff and you're going to be able to help people."
- "When people start to learn survival skills, they start to look at the world in a very, very different way."
- "Supplies are pretty much everywhere. You just have to know where to look and how to think about them."

### Oneliner

Be prepared for disasters, be self-reliant, teach survival skills, and change how you see the world - Beau covers essentials from emergency readiness to mindset shifts.

### Audience

Families, Communities

### On-the-ground actions from transcript

- Prepare an emergency kit with essentials like water, food, first aid supplies (suggested)
- Teach children survival skills in a fun and engaging way (suggested)
- Encourage critical thinking and improvisation skills through survival training (suggested)
- Practice looking at components as improvisational tools (implied)

### Whats missing in summary

The full transcript provides detailed insights on disaster preparedness, self-reliance, teaching survival skills, and changing perspectives on the world.

### Tags

#DisasterPreparedness #SelfReliance #SurvivalSkills #CommunityResilience #MindsetShifts


## Transcript
Hurricanes, wildfires, earthquakes, blizzards,
there is something in your area that
can happen very, very quickly that you're
going to need to be prepared for.
As you probably know and have seen with your own eyes
from the last few major natural disasters,
help doesn't come immediately.
It takes time.
how long it takes depends on your state and how good your state agencies are and your
local agencies are.
Because the federal agencies are pretty much worthless.
Now that's excluding DOD's efforts.
Everybody else, they'll get here eventually.
But you need to have yourself covered for a few days, depending on the type of disaster.
Because they're not going to be here right away.
Now here we are a week into it.
I've still yet to see FEMA.
I saw one red cross truck.
The only federal agency that I have actually seen was Border Patrol and they were getting
out, they were leaving.
We are not the sons of the pioneers and the daughters of Rosie the Riveter anymore.
Because we're not going to fix the problem ourselves.
We're going to wait for somebody else to do it.
Wait for government.
daddy government. The pioneers, right? It's what we like to cast ourselves as. We're
Americans, the sons of the pioneers. Are we anymore? Or have we had it so good
for so long that we've forgotten what that is and we become afraid of
everything? Everything. If government could help, if government could really
solve the problems. I wouldn't be running supplies in from 30 or 40 miles away.
You can't count on the government response. You can't. In fact, some of the things that
your government agencies may do will make it worse for you.
Putting together a kit, a bag, for emergencies is something that every family should do.
It doesn't matter if you're a soccer mom or you're an X Ranger, you need to stay up to date and you need to have your
stuff together.
And this is for everybody. This isn't for survivalists, this isn't for preppers, this is for everybody.
It doesn't take long. It's not a big process. We're not talking about building a bunker and preparing for doomsday.
We're just talking about enough to survive a week or two until things normalize.
OK, so the first thing, water, most important thing, OK?
I would say two gallons of water per person per day,
however long you expect to be without help.
And I know people are saying, no, well,
you don't really need that much, you
can survive on a lot less you can.
You can survive on a lot less than that.
However, you're going to want to cook.
You may be able to flush your toilet
if you dump water into it.
You may need to clean a wound.
You may want to wash your hands.
You're going to need water.
Get gallon jugs.
Or if you have a collagen thing at your house, get some
extra five gallon things, keep it on hand.
People think that I've got a case of
Dasani at the house.
That's enough.
It's not.
It's not even close to enough.
You're going to burn through that so fast.
Make sure you have water.
Aside from that, you can also get tablets to purify water.
you can get filters, which are really cool, like the LifeStraw or the Sawyer.
Food.
Now, when you think emergency and food, you think MREs, mills ready to use by the army.
Do you need those?
No.
Of course not.
Get canned goods, get rice, get things that last a really long time, that are things you
wouldn't normally eat and that they have a long shelf life, because then you can put
them in your pantry and they'll stay there, because they're not really, you're not going
to be like, oh I really want to eat that. No, it's just gonna hang out. That way
you have a backup food supply and again you're not looking for the tastiest
thing in the world, you're just looking for something that will keep you alive
and that is easy to cook. And because you're doing that you also need a can
opener, a first aid kit, a good one. Okay, not the blue box you pick up at
Walmart that says 153 pieces and 148 of them are different sized band-aids. If
they can be fixed with a band-aid it probably would have fixed itself on its
own. Then you also need your meds. If you are on meds of any kind that you have to
take regularly you need a supply of those in this bag because pharmacies
aren't going to be open. And then obviously if you have pets you need food
for them, their meds if they need any water for them as well. Fire and fire
includes a flashlight and you need to know how to build a fire. You need
lighters, you need fire strikers, that type of thing. Matches, waterproof matches
would be great. Then you need shelter. You need to assume that the structure
you're in is going to be damaged. Tarps, you know if you don't want to go out and
spend a bunch of money on a tent, tarps or those little mylar, the silver blankets,
They're not great, but they work.
A knife, a multi-tool, like a Leatherman
or something like that, and you can even get a cheap one.
It's got a knife on it, but it also has pliers
and a can opener, you know you're gonna need that.
It has all of those other things, a Swiss Army knife.
The last thing is a battery-operated,
AM, FM, weather would be great, radio.
Um, the internet's gonna be down.
You're not going to get reliable news
accept through the radio. When relief does come you're gonna find out about
the pickup points to get supplies via the radio or from somebody else who has
a radio. Go ahead and get your own. Make sure you have plenty of batteries for
all of this stuff. People say I have all this stuff in my house that's great but
after the hurricane or the earthquake or whatever are you gonna be able to find
Keep it all together. It's nothing new.
Beyond that you will need an evacuation plan
You need to know where you are going to go
and you'll need a backup for that plan.
Once you have the plan, your evacuation plan will follow
In addition to all of the stuff,
anyone that is going to use Have This Animals clerks
anybody that's part of a plan has to know where them plan is.
You need to know where you're here you will meet in a secondary location
in case you can't meet up with the first animal
Then you need to have the evac location
the place you are going to head if you can't shelter.
evac location, the place you're going to go to if you can't shelter in place, and
then a second one.
And everybody needs to know about this.
If you have this stuff, you're able to help other people because you're already taken
care of.
And in a situation like this, it's just your community.
This is putting on, this is being on an airplane and putting your mask on before you put the
person next to you before you help them put their mask on. Just take care of this stuff
and you're going to be able to help people. There are people who will come to help, but
the more self-reliant you are, the less stress you're putting on them because they can focus
on those people who could not help themselves. We're in a small town and my two-year-old
spots a fire truck sitting out in front of the fire station. And he loves them.
I mean, he's about ready to crawl out the window. He said, oh, rescue team. Then he
notices that the fire station's damaged. He's like, oh no, the rescue team's hurt.
Who's gonna help? My four-year-old doesn't miss a beat. Us. And that's why I
take them. That is why I take them. You can't tell a kid to be a good person. You got to show them.
Four-year-old understands that at some point, and at some times, it's just us. There is no
Big Daddy government coming to help you. Here's the thing, guys. It's up to you. It is up to you.
If not you, who? Who's gonna solve the problems if it's not you? You personally.
Not your representative. You. You got a problem in your community? Fix it. Do it
yourself. Do not wait for a leader. Don't wait for some savior to arrive. Don't be
afraid, put out the flame with your foot. Get involved. You want to make America
great? It isn't gonna happen in DC. It's gonna happen at the local level. It's
not a matter of somebody else is gonna help him. That may not happen. If not you,
who? Make sure that somebody else is helping before you just walk on by. There
are people still out there like that. This little supply depot I've been
going to. It's at a place called Project Hope Incorporated. Now under normal
circumstances these guys work with vets and they use horses. Kind of cool to kind
of provide a cathartic experience. Interesting little concept and they do
good work according to everybody that I've talked to that has ever had
anything to do with them. They've turned their entire facility in into a supply
depot to help people and need to think about that. Supply is coming in to a
non-government location. Non-government people get into where it needs to go.
Now for those who don't think that this is something their kids need to learn,
what are the side effects of learning survival skills? You'll learn to
critically think, problem-solve, think on your feet, improvisation, self-reliance.
These are the side effects. It's worth doing.
How do you engage the youth in learning survival skills? We're gonna start off
with the don'ts and there's just one. There's just one that I see commonly
that people do. Don't try to toughen up your kid. Don't. Just don't. Teach them the
skills, make it fun. If they have to suffer, they just have to suffer. In a survival suit,
you don't have to practice how to suffer. So what's the first thing you should do? Get
a copy of the U.S. Army Survival Manual and read it. You can buy it at any local bookstore.
If you just want to download it, you can get it for free at archive.org. I would suggest
getting a hard copy and keeping it just just in case. We want to make it fun.
Your kid sits around and plays video games all day. Don't yell at him to get
off and go outside. What are you playing? Fallout? You want to learn how to do that
for real? Play with their interests rather than just trying to hand them a
new one. Just find some way to relate it. That's it's all you have to do and then
Then it makes it a little bit interesting and then it's a little less of a problem getting
them involved.
And then from there, you've read the book, so come up with hip pocket classes, little
activities that can be done at the drop of a hat, organize them ahead of time, little
things that you can do to teach them little bits and pieces of it at a time.
So it's not a classroom setting, it's them doing something with you.
A good example is you're out, you're away from your home.
Right now, where can you find food, water, fire, shelter, a knife and a first aid kit?
There are little chemistry sets, STEM sets, where you actually learn to build something.
build a battery or you start a fire with a chemical reaction or whatever. Those
little things, they're cheap. They're like five bucks. Get them, do that one. Use that
to teach something else along the way. Just make it fun and for God's sakes do
not try to toughen up your kid. They don't need to be tough physically. They
They need to be tough mentally, and they'll figure that out along the way.
When people start to learn survival skills, they start to look at the world in a very,
very different way.
They see the impossible as possible.
They see what things could be if you looked at them the right way, and they learn what's
really important in life.
Supplies are pretty much everywhere.
You just have to know where to look and how to think about them.
After Michael, there was a group of people that was sheltering in a restaurant and they
needed medical supplies, nothing major, but they had an injury.
It never occurred to them that there was a first aid kit in the kitchen because they
were up in the front of the house.
There was a first aid kit.
There was what they needed 15 feet away, but we're so used to that consumer life that if
If we didn't buy it, it doesn't exist type of thing, that they never thought about it.
Prior to Michael, I watched two people argue over water in a store.
And they were between a display of Gatorade and a cooler full of Milo sweet tea.
If you were just looking for something cheap to flush your toilet, the sweet tea would
have worked.
If you were looking for something potable water, the Gatorade would have worked.
people get in that mindset of I have to get water. You have to get something wet. Beyond
that it teaches you to improvise and that's one of the most important skills in life in
my opinion. It's one of those things that lets you see what things could be rather than
what they are. And when you do that you start to see everything around you. This is a battery
if you can't tell, as components, rather than some finished consumer product, you learn
to improvise.
And when you do that, you learn to see the world in a different way, because things can
be something else.
It's really important for the youth of today to learn survival skills for that reason.
Because I think with everything going on in the world today it might be really important
for them to understand that the impossible is possible.
And that the way things are isn't what they have to be.
That they can change it.
That they can use the tools around them to make something else.
Anyway it's just a thought.
Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}