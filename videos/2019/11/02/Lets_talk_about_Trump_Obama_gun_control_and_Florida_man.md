---
title: Let's talk about Trump, Obama, gun control, and Florida man....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wbSOB-tFdXc) |
| Published | 2019/11/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Debunks the widely held belief among older Republicans that Donald Trump is a defender of the Second Amendment.
- Compares Trump's gun control actions to Obama's, revealing surprising facts.
- Trump supported banning assault weapons, longer waiting periods, banning suppressors, universal background checks, and increased federal firearm prosecutions.
- Trump set a record for federal firearm prosecutions, surpassing Obama and even Bush.
- Obama received an F rating from the Brady campaign, indicating his stance on gun control.
- Trump continued and expanded upon Obama's actions, releasing military surplus weapons and allowing guns in national parks.
- Contrasts the perception of Obama as a gun grabber with his actual actions in expanding gun rights.
- Criticizes Trump's stance on guns, quoting his willingness to "take the guns first, worry about due process second."
- Draws parallels between Trump's approach to guns and his views on impeachment.
- Concludes by challenging the perception of Trump as a defender of gun rights.

### Quotes

- "President Obama expanded gun rights. He wasn't a gun grabber."
- "Trump, on the other hand, well, take the guns first. Worry about due process second."

### Oneliner

Debunks the myth that Trump is a Second Amendment defender, contrasting his actions with Obama's and revealing surprising truths about gun control.

### Audience

Second Amendment supporters

### On-the-ground actions from transcript

- Share this information with fellow Republicans to challenge misconceptions about Trump's stance on the Second Amendment. (implied)

### Whats missing in summary

The full transcript provides an in-depth analysis of Trump and Obama's actions concerning gun control, urging viewers to re-evaluate commonly held beliefs.

### Tags

#SecondAmendment #GunControl #Trump #Obama #Republican


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're gonna talk about another one
of those false ideas, one of those things
that everybody believes is true,
but may not be if you actually sit down and look at it.
It happens a lot when something just gets repeated
over and over and over and over and over and over again.
Eventually people will start to believe it,
and that is definitely what has happened here.
If you're a Republican, if you're a Second Amendment supporter, you need to watch this
whole thing, no matter how painful it is for you.
Okay, so this particular belief is widely held among older Republicans who think they
support the Second Amendment.
Who's the defender of the Second Amendment?
Well, Donald J. Trump, of course.
Alright, maybe.
But in order to test that theory, we need to have somebody to compare him to.
So let's compare him to gun grabber Obama.
Who else?
Seems fair.
Let's start with a quote.
You get to guess as to who it is, I generally oppose gun control, but I support the ban
on assault weapons and I also support a slightly longer waiting period to purchase a gun.
Is that Trump or Obama?
That is Donald J. Trump.
Well he's changed a lot since then.
That was a while ago.
Maybe.
Maybe that's true.
How does he feel about suppressors?
I don't like them, quote, signaled support for banning them, signaled support for universal
background checks, did ban bump stocks, bragged, listen to this, we've increased federal firearm
prosecutions by 44% compared to the last two years of the previous administration.
This is a record, a new record.
It is a new record.
It is.
But the previous record holder was not Obama.
It was Bush.
Man that's a lot to swallow.
That's hard right there.
Just up to this point.
But there's more.
I know what you're saying.
Obama did all that stuff with NICs.
If you're outside the gun crowd,
NICs is the system for background checks.
He did, that's true, that is true.
So that's a mark against him.
But that's not surprising because Obama got an F
from the Brady campaign.
Trump took everything Obama did and then went a step further
fix Nick's act. I think the only part of the Nick stuff that Obama did that Trump
got rid of was the Social Security office having to report people who were
adjudicated mentally incompetent so they could buy a gun because that seemed like
a great idea. Well, Trump released all those M1s in 1911s as military surplus
weapons, put those on the civilian market for us. He did, he did do that and those
weapons were authorized for release by President Obama. Obama also allowed
Amtrak passengers to bring their guns with them, allowed firearms in the
national parks. It's crazy. It's almost like what you believe is just because it
was repeated, not because it's true. President Obama expanded gun rights. He
wasn't a gun grabber. He does not deserve that title. Trump, on the other hand, well,
take the guns first. Worry about due process second. Quote. It's funny because
that's how I feel about impeachment. Impeach first, worry about due process
over in the Senate during the trial. His stance on guns is a lot like him saying
that he's Florida man now. He lives in Florida. We all know he's a New Yorker
and when it comes to gun control, he's a New York liberal. Anyway, it's just a
fault. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}