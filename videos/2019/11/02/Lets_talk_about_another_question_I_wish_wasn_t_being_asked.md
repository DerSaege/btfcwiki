---
title: Let's talk about another question I wish wasn't being asked....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=7S5dUoJXcmY) |
| Published | 2019/11/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the rise in questions about arming oneself due to increasing anti-trans sentiment, especially since the last presidential election.
- Expressing feeling unsafe and contemplating buying a gun despite not being a "gun person" previously.
- Mentioning the ease of purchasing a gun in their state and considering taking a safety class to handle it responsibly.
- Responding to the question of whether LGBT individuals should arm themselves, stating it's not more likely to escalate violence against them.
- Advising taking multiple safety courses until feeling extremely comfortable with handling a firearm.
- Emphasizing the seriousness of owning a gun for the purpose of self-defense and the responsibility that comes with it.
- Sharing advice on being certain of the intent before ever drawing a weapon in a potentially confrontational situation.
- Noting the importance of understanding the tool and learning how to employ it effectively in combat situations.
- Encouraging individuals to only purchase a firearm if they are willing to put in the effort to learn how to use it properly.
- Mentioning the lack of understanding among many gun owners on how to use firearms beyond paper target shooting, stressing the need for training to reduce potential violence.

### Quotes

- "If you're dumb enough to pull a gun, you better be smart enough to pull the trigger."
- "You're talking about purchasing a firearm for the express purpose of killing someone."
- "If you're willing to put in the effort to learn how to do that, It's a good idea."
- "When you learn how to use one to win, there's a new respect that comes along with it for the tool."
- "I think we'd see less violence if people understood how to employ it better."

### Oneliner

Beau addresses the rise in questions about arming oneself due to escalating anti-trans sentiment and provides critical advice on firearm ownership.

### Audience

LGBT Community

### On-the-ground actions from transcript

- Take multiple safety courses to handle firearms responsibly (suggested).
- Put in the effort to learn how to use a firearm effectively in combat situations (suggested).
- Understand the responsibility and seriousness that come with owning a gun for self-defense (implied).

### Whats missing in summary

The full transcript provides in-depth advice on the considerations and responsibilities associated with purchasing and owning a firearm for self-defense purposes.


## Transcript
Well howdy there internet people, it's Bo again.
So tonight I'm going to read a message and it's a long message, I'm going to read the
whole thing except for the identifying information in it because there are things that I think
need to be addressed other than just the final question.
I feel weird getting so many of these questions lately because you know, I don't own any
anymore, but it's, I think it's something that needs to be addressed anyway.
So I think I'll ask here, since it is sort of relevant and I couldn't find an email to
contact you with, let me stop for a second, Twitter, Facebook, you can go to the fifth
column main page and send a message through there and eventually I'll get it, or you can
leave in the comments section. It may take two weeks, but eventually I'll see it. I try
to read all the comments.
Okay, I'm trans and I've been out of the closet for 27 years and in transition for 21. In
that time, I've often felt unsafe and noped out of a lot of situations. However, I have
never felt more unsafe in my life than I do now and have since the last presidential election.
I do not recall a period in my life with this much anti-trans sentiment.
Up until these past few years, I've never felt that I needed more self-defense gear
than a can of mace and a butterfly knife.
Now though, I have been seriously contemplating buying a gun.
I live in a state where all you need to buy a gun is a smile and the money.
And you can legally walk out with it hidden on your person, a constitutional carry state
and an effective stand your ground state.
I've done a lot of reading on the subject lately, and I suppose since Trump left office,
there's been a slowdown of gun sales overall, but a serious uptick in gun purchases by
LGB and especially T people.
That's true.
I'm not a gun person, and I know which end to point at the ground, but other than that,
would need to take a safety class to feel comfortable with it in my home, even
unlocked or unloaded and locked in a safe with no ammunition on the premises.
I suppose I'm wondering what your thoughts are on this. Is it a bad idea
for LGBT people to be arming ourselves? Is it only more likely to escalate
violence against us. Okay, last question first. No, it is not more likely to escalate violence
against you. These people hate you because they've been told to. They've been inflamed
by rhetoric. Most of them, they don't understand. They haven't been exposed. They're not familiar
with it, and people fear things they're not familiar with.
And then you have certain elements inflaming that, substantially.
But you are not responsible for the violence that would be visited upon you by bigots.
I mean, that's not your fault.
And nothing you are going to be able to do, one way or another, is going to change them.
you can't make them like you.
That's going to have to be a societal push.
It's not something that you as an individual can do.
OK.
Safety course.
Yeah, definitely, absolutely.
If you're not comfortable handling one,
definitely take a safety course.
Take two.
Take as many as you need to until you're
extremely comfortable with it.
It is a very dangerous tool.
but beyond that
you need to learn how to win
you're talking about purchasing a firearm
for the express purpose
of killing someone
doesn't matter that it's self defense
at the end of the day
that's what you're buying it for
because
you can't buy it to scare people
or to ward people off when they're doing something
that will escalate things
Some of the best advice I ever got was,
if you're dumb enough to pull a gun,
you better be smart enough to pull the trigger.
The second it comes out, the whole dynamic changes.
Now, about a year ago, sadly, for the very same reasons,
after the synagogue incident, I had somebody
send me a very similar question, an anti-gun person who
had a change of heart because of the current political climate.
I made a very, very in-depth video about it
and what it takes to win and what you need to learn.
It's going to pop up over here at the end of this video.
And then below it will be the follow-up,
because I kept up with her.
I would suggest watching both of those.
There are some differences, because you're
talking about carrying it on your person
to deal with confrontations initiated
by people that, well, you don't know their intent.
You don't know if they're just gonna walk up
and say something stupid,
or they're gonna walk up and do something stupid.
And you have to be certain of that
before that thing ever leaves the holster.
You have to know, you have to be certain.
If you're not, that's when bad things happen.
If that comes out and
it wasn't going to be a violent situation,
it has suddenly become one.
have to be very, very certain of that. You have to understand the intent. And the problem
is that's going to be a surprise for you. You don't get to know ahead of time, as in
her situation, she had a pretty good idea. When you watch those other videos, you'll
hear the phrase, speed, surprise, and violence of action. Those are the things you need to
and it'll go into more in-depth about it but surprise is gonna be lacking you're
not gonna have that they do because it's just some random person feeding off of
ignorance feeding off of bigotry do I think this is a good idea overall yeah
if you take it seriously if you're willing to put in the effort it takes to
learn to win not just be safe because it's a very dangerous tool it is an
extremely dangerous tool it's like down here in Florida after every hurricane a
bunch of people that you know have never used a power tool in their life go out
by a chainsaw. You can see them in Home Depot and Lowe's and you know that if you go to the emergency
room that night you'll see some of them because they can read the safety manual and they can get
a good idea of how to be safe with it in theory but they don't know how to use it for its intended
purpose.
And that's where it becomes dangerous.
The same thing is true with a firearm.
Especially when you're not talking about target shooting.
You're not talking about hunting.
You're talking about combat.
Combat. It is the same thing. You're talking about life and death.
So,
if you're willing to put in the effort
to learn how to do that,
It's a good idea.
It's a good idea.
If you're not, don't do it.
Don't purchase one.
It'll only lead to bad things.
One of the big problems in the United States is a whole bunch
of people own firearms, and very few of them really know
how to use them on anything other than paper.
When you learn how to use one to win, there's a new
respect that comes along with it for the tool.
That's one of the things that's missing.
Oddly enough, I think we'd see less violence if people
understood how to employ it better.
I think that makes sense.
But anyway, it's just a thought.
y'all have a good night

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}