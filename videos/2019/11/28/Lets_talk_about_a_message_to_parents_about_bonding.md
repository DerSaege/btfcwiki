---
title: Let's talk about a message to parents about bonding....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=4ir46_N23ls) |
| Published | 2019/11/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A single mother's message and a sports player taking his boys to the range sparked the topic.
- Criticism arose regarding the reasons for taking the boys, things said, and teaching techniques.
- Despite the criticism, bonding experiences between parents and children were brought up.
- Beau shared his childhood experience of learning about military tactics and small arms from his father.
- He chose not to teach his son about violence but instead sent him elsewhere for unarmed defense lessons.
- Beau values memories of childhood involving cooperation, creation, and building over destruction.
- He emphasized the importance of teaching children cooperation and creation instead of violence.
- Beau discussed the quote about preparing for the next war rather than focusing on the last war.
- He stressed the need for future generations to study fields like mathematics, philosophy, and engineering.
- Beau emphasized that the next war for humanity's survival will require engineers, architects, and philosophers.

### Quotes

- "I don't want to bond over violence if it's really bonding."
- "Cooperation, creation, building, that's what they're going to have to struggle to learn."
- "The next war for humanity's survival is not going to be fought by soldiers or sailors and pilots."

### Oneliner

A reflection on parenting, violence, and preparing for the future in a changing world.

### Audience

Parents, educators, community leaders

### On-the-ground actions from transcript

- Teach children cooperation and creation through hands-on activities (exemplified)
- Encourage children to study fields like mathematics, philosophy, engineering (exemplified)

### Whats missing in summary

The importance of guiding children towards cooperation and creation rather than violence.

### Tags

#Parenting #Violence #Childhood #Education #FuturePreparation


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're going to talk about something brought on, in part, by a
message from a single mother, rather touching one.
And in part, because some sports ball player got dragged on Twitter the day
before yesterday, because he took his boys to the range, which I personally
have no problem with.
The reasons he took them, the things that were said, and his teaching
techniques, well, there's a lot of room for criticism there.
A lot.
But rather than relive all of that criticism, I noticed when
he was responding, he talked about the bonding experience.
And I get that.
You're some famous whatever sport.
You spend a lot of time on the road.
You want to create memories of value with your kids.
I understand that.
There's a movie called Major Pain.
If you ever wondered about my dad, that's him.
I mean, that is him.
A lot of this is going to sound like a joke.
It's not.
Before I left elementary school, I
could tell you the difference between a fish bed, a flanker,
and a fox bat, a T-62 and a T-72.
I had a sand table that most times was used for my Hot Wheels and GI Joe, but it was also
used to teach me the difference between strategic, operational, and tactical, how to conduct
ambushes and cut supply lines.
I could break down most NATO or Warsaw Pact small arms.
By the time I left middle school, I had shot most of them.
Now because of my life path, a lot of that information actually turned out to come in
pretty handy.
I'm older and slower today, but I'm still more than qualified to teach unarmed defense.
So when my son, my eldest, wanted to learn about it, you know what I did.
I took him outside and sent him to somebody else.
It's not the kind of contact I want to have with my kid.
I don't want to bond over violence if it's really bonding.
I value a lot of memories from childhood,
building the models of those tanks and planes,
physically building the sand table,
using newspaper and masking tape and spray paint to create the terrain.
I value that.
I really do.
But if you only have a limited amount of time
to spend with your kid,
what kind of memories do you really want to create?
Do you really want that
positive feeling that they get
from spending time with you
to be about destruction, competitiveness, destruction, all of this stuff.
The entire world is going to teach them that.
You don't have to.
Cooperation, creation, building, that's what they're going to have to struggle to learn.
That's where they need you.
One of the most important things I think I learned in my childhood was that you don't
win the next war by preparing for the last.
There's a quote, most times when this quote is presented, it's cut short and it completely
changes the meaning because they cut it off.
I must study politics and war, that my stones may have liberty to study mathematics and
philosophy.
That's where the quote normally ends, gives the impression that each generation has to
keep up on politics and war to kind of keep it alive.
not the whole quote. I must study politics and war that my sons may have
the liberty to study mathematics and philosophy. My sons ought to study
mathematics and philosophy, geography, natural history, naval architecture,
navigation, commerce, and agriculture in order to give their children a right to
study painting, poetry, music, architecture, statuary, tapestry, and porcelain.
Adams today everybody everybody most men anyway seem to want their kid to be a
warrior everybody wants their kid to be a warrior right up until the point it's
time for them to leave and go do warrior stuff. Then you don't want it anymore.
You realize it's not a game. The thing is that's preparing for the last war. The
next war for humanity's survival is not going to be fought by soldiers or
sailors and pilots. It's going to be fought by engineers, architects, and
philosophers. We need to stop preparing for the last war because the next war is
going to be different. We don't need more killers. We need more engineers. You want
them to win the next war. Focus on that. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}