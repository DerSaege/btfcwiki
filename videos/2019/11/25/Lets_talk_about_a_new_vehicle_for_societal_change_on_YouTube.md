---
title: Let's talk about a new vehicle for societal change on YouTube....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=kkxPjpYFgLM) |
| Published | 2019/11/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The FTC is enforcing a 1998 law on childhood privacy, affecting kids' content on YouTube.
- Creators of kids' content may see a drop in income due to the removal of targeted ads.
- Kid-directed content involves high production costs with visual effects, sound effects, travel, location shooting, and toy unboxing.
- Major kid content creators have the potential to influence change by educating their viewers about laws.
- There is a sense of learned helplessness among people when it comes to government actions.
- Despite having millions of subscribers, major creators may not utilize their influence for change.
- Starting a kids' content YouTube channel with low overhead costs could be profitable as YouTube is profit-driven.
- Creating content like story time teaching children with morals can be financially viable with minimal overhead.
- Beau suggests creating content that is more educational like Mr. Rogers instead of traditional cartoons.
- Using a YouTube channel for societal change is emphasized by Beau, encouraging those interested to get involved.

### Quotes

- "They can influence change, but they're probably not going to."
- "If you have that appearance or that voice that would appeal to children, you want to get involved, this is something you can do."
- "The driver's door just opened. You can really get somewhere with this."
- "You can literally raise the children that don't have the influence from the parents they need."
- "I think you can create a better world with a camera and a YouTube channel."

### Oneliner

The FTC's enforcement of a law on childhood privacy affects kids' content on YouTube, urging major creators to utilize their influence for societal change.

### Audience

Content Creators

### On-the-ground actions from transcript

- Start a kids' content YouTube channel with low overhead costs to adapt to the changes in kid-directed content (suggested)
- Create educational content like story time teaching children with morals to make an impact (implied)
- Use your appearance and voice to appeal to children and get involved in creating content for societal change (implied)

### Whats missing in summary

The full transcript provides detailed insights into the impact of FTC regulations on kids' content creators and the potential for societal change through educational YouTube channels.

### Tags

#YouTube #FTC #KidsContent #SocietalChange #ContentCreators


## Transcript
Well, howdy there, internet people, it's Bo again.
So we're going to talk about kids' content on YouTube again,
because it is still a major issue that's being discussed.
If you're not familiar with the fallout, what's happened,
the FTC has decided to start strictly enforcing a 1998
obsolete law about childhood privacy.
The end result of this is going to be no more targeted ads
kids that means that the rate that creators are going to get paid for that
kind of content is going to drop this is an issue because that kind of content is
expensive to make if you look at the most popular kids YouTube channels
there's a lot of visual effects there's a lot of sound effects there's a lot of
travel and shooting on location there is a lot of unboxing of toys all of this
stuff costs money. Now the panic that happened the FTC has released another
clarification and yeah go read it yourself if you have one of those
elements that they said could mean that your content is kid-directed they're not
talking about you they don't care about you they care about content aimed at
kids. Does that mean the law is good? No, of course not. It needs to go away. And the
fact is, if the major kid content creators decided tomorrow that they were
going to give a civics lesson to their viewers and say, hey it's Blippi and this
is how the American government works and if you don't get your parents to get
involved in this process, I'm gonna go away. You need to tell your parents to
call your rep or your senator, and get rid of this law.
That could happen.
Probably not going to, though.
When it comes to the government, a lot of people
have succumbed to learned helplessness,
where they just don't think they can do anything.
These creators, when you add up the major creators,
you were talking about millions upon millions of subscribers.
They can influence change, but they're probably not going to,
which means a lot of them are probably going to go off the air.
Now, I know this sounds weird, but what that means is that right now is the time to start a kids content YouTube
channel.
They're going to go off the air because their format requires a lot of money.
If you come up with a low overhead method of developing kids content, you're still going to make money.
YouTube is a profit driven machine.
They will find a way to make money off of your content, which means you'll get a cut.
Is it going to be the same rates that people were making last year?
No.
But if your kid's content was story time hour, teaching children with stories that have morals,
you could probably make some money because you don't need a lot of overhead to do it.
more Mr. Rogers instead of cartoons. More importantly, the reason I'm talking
about on this channel is because this is a vehicle for societal change. It could
be done. If you have that appearance or that voice that would appeal to children,
you want to get involved, this is something you can do. Not just will you be
able to do it. It'll be self-sustaining and it can have a massive impact. If you
get an audience of children and you're discussing themes about bettering the
world buried in stories you will be looking seven generations ahead you will
be shaping the world with a YouTube channel it can be done and if you're
worried about coming up with content understand if you're doing something
like this feel free to straight up rip off my content I don't care literally
do not care, you are more than welcome to do it. If you can find a way to reframe the
content on this channel and make it kid friendly, I fully support that. If I didn't look the
way I do and sound the way I do, I would be doing it. I think for those people that want
a vehicle for societal change, the driver's door just opened. The driver's door just opened.
but all you gotta do is climb in and start driving.
You can really get somewhere with this
because for a lot of the people that are out there today
that have antiquated views,
they're in large part, they're being raised
by content like this.
You can literally raise the children
that don't have the influence from the parents they need.
This is something that should really be looked at if this is something that you're interested
in.
If you have this kind of background, you can do this and you can find a way to appeal to
children without the massive expenditures.
Not just do I think you'll make money, I think you can create a better world with a camera
and a YouTube channel.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}