---
title: Let's talk about the Secretary of the Navy, tweets, and consequences....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NfwTPc_mKnw) |
| Published | 2019/11/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Secretary of Navy's resignation due to discrepancy between public statements and private actions.
- Administration prioritizes truthfulness and transparency, sarcastically mentioning their stellar reputation.
- Secretary of Navy's refusal to obey an order believed to violate his oath to support and defend the Constitution.
- Admirals' concern about good order and discipline after the President interfered in military proceedings.
- President's tweet expressing dissatisfaction with Navy Seal Eddie Gallagher's trial outcome.
- Gallagher was acquitted on major charges but found to have posed with an enemy prisoner's corpse.
- Beau questions the seriousness of posing with a corpse, explaining its impact on propaganda for the opposition.
- Beau stresses the repercussions of such actions on local populace and counterinsurgency efforts.
- Concerns raised about the President's orders being delivered via tweet to a wide audience.
- Beau criticizes the lowering of moral standards and its implications on military operations.
- Beau underlines the psychological impact of such actions on public perception and counterinsurgency success.
- The lasting effects of setting a standard where posing with a corpse is considered acceptable.
- Beau points out the significant damage done to the country's moral compass by such actions.

### Quotes

- "One guy has done more damage to this country's moral compass than any other president in history."
- "The standard has been set. This isn't a big deal."
- "You better fight harder."
- "Changed the way you looked at things."
- "We lowered the bar."

### Oneliner

Secretary of Navy resigns, President interferes in military proceedings, posing with a corpse deemed acceptable - damaging moral compass and counterinsurgency efforts.

### Audience

Military personnel, policymakers, activists

### On-the-ground actions from transcript

- Challenge and question actions that compromise moral standards (exemplified).
- Advocate for transparent and ethical leadership in the military (exemplified).
- Educate others on the psychological impact of actions on public perception (implied).

### Whats missing in summary

The emotional weight and depth of analysis present in the full transcript.

### Tags

#SecretaryOfNavy #MilitaryLeadership #MoralCompass #Transparency #Counterinsurgency


## Transcript
Well, howdy there, internet people, it's Bo again.
So, Secretary of Navy's out, outgoing Secretary of Navy, I guess, now, and there is a little
bit of a discrepancy as to why.
The administration says that his public statements did not match his private actions, and I can
see that as being important from this administration that has always been so truthful and transparent.
They're very keen on such things.
And they wouldn't want anything to jeopardize that stellar reputation that they have.
This is completely believable.
He tells a different story.
He says, I cannot in good conscience obey an order that I believe violates the sacred
oath I took in the presence of my family, my flag, and my faith to support and defend
the Constitution of the United States.
And this is interesting.
discrepancy is probably worth looking into for historical sake. But it's moot.
Doesn't matter anymore. The reason these admirals took this stand was because
they were worried about good order and discipline. That's what they were
worried about. They were worried about the message that was sent when the president,
the commander-in-chief interfered, and if you believe it was justified, use whatever
Whatever term you want to use in these proceedings.
They were worried the message it was going to send.
They were worried how the rank-and-file troops were going to see it.
But that doesn't matter anymore.
It really doesn't.
Four hours ago, the President of the United States, the Commander-in-Chief, tweeted,
I was not pleased with the way that Navy Seal Eddie Gallagher's trial was handled by the
Navy.
treated very badly but despite this was exonerated on all major charges. I mean
yeah I guess in a relative sense that's true. I mean when you're comparing things
to murder I mean yeah everything else seems kind of minor. The reality is he
was found to have posed with an enemy prisoner's corpse. Not a major thing. Not
Not a big deal, not even worthy of a demotion.
And I know, there might be some people saying, well, is it a big deal?
Those photos are taken for bragging rights.
They're shown around.
Eventually they get out, and when they do, they create propaganda for the opposition.
Look what they did.
crystallizes the fighters, crystallizes their resolve.
You don't want to end up like that.
You better fight harder.
The local populace, when they see that, what do they think?
Do they side with their countrymen?
Or do they side with the people posing with a teen?
The thing is, it's not the Secretary of Defense, it's not the Commander in Chief that has to
deal with the fallout from that tweet because this went out to everybody because the President,
the Commander in Chief, gives orders via tweet.
Everybody saw this.
country, they're now all aware of it. And yeah, sure, somebody's gonna say, well, you
know, they were doing it already. Yeah, some of them were. Now all of them will.
We lowered the bar. We can't pretend that we're different. Can't pretend we have a
moral high ground. It's gonna be very hard to run counterinsurgency operations
now. It's going to be hard to gain the support of the local populace because, hey, it's not
a big deal. And I know there might be somebody saying, is there really that much psychology?
Does it really play into that much? Yeah, it does. If you don't believe me, how did
you feel when the enemy prisoner that was killed and a knife was used was an American
in an orange jumpsuit. How'd you feel about those photos? Bet it altered your
perception. Changed the way you looked at things. It'll happen. It will happen.
So yeah, while the discrepancy is interesting, doesn't matter. The standard
has been set. This isn't a big deal. No demotion, nothing. They're not even, not
even gonna bounce you out of the unit.
One guy, one guy has done more damage to this country's moral compass than any
other president in history. Anyway, it's just a thought. Y'all have a good
night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}