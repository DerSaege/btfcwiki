---
title: Let's talk about what talking robots can tell us about ourselves....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=jo13NLjIEZM) |
| Published | 2019/11/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about a study from Carnegie Mellon University where people played a game against a robot named Pepper.
- Some players were encouraged by the robot while others were insulted.
- Players who were encouraged performed better, while those who were insulted played worse.
- Even though the players knew it was a robot and programmed to act that way, it affected their emotions and gameplay.
- AI researchers find this interaction between robots and humans extremely interesting.
- Beau raises the point that if robots can impact adults like this, the power of words on children must be considered.
- He mentions the influence of internet comments and how they can affect individuals more than they realize.
- Beau urges viewers to understand the impact their comments can have on others, as they are real people reading them.
- He stresses the importance of being mindful of the words we put out into the world.
- Beau concludes by reminding viewers that their words, even seemingly harmless ones, can have a significant effect on others.

### Quotes

- "Words have a lot of power."
- "You have a choice in what you put out into the world."
- "Your words will have an effect on those people that read them."
- "Even if you think it's just a harmless comment..."
- "The reality is that's a real person."

### Oneliner

Beau from Carnegie Mellon shows how a robot's words affect gameplay, urging us to understand the power of our own words, especially towards children and online.

### Audience

Online users

### On-the-ground actions from transcript

- Choose words carefully (implied)
- Be mindful of comments' impact (implied)

### Whats missing in summary

The full transcript provides a deeper insight into the study on robot-human interaction and the implications for communication in various contexts.


## Transcript
Well howdy there internet people, it's Bo again.
So tonight we're going to talk about robots, talking robots,
and what they can tell us about pretty much all
of our communication.
Carnegie Mellon University did a really interesting study.
They took 40 people and they had them play a game
against a robot named Pepper, some of them would be encouraged while they were playing
the game by the robot.
Some of them would be insulted with such horrible statements as, I have to say you're a terrible
player.
What they found out was that those who were encouraged played better and those who were
insulted, played worse. These are people who knew it was a robot. Some of them
understood that it was programmed to do that. They couldn't be mad at the robot,
but it still got in their head. It still caused them discomfort. It still affected
their gameplay. Now, AI researchers, they're all over this. To them, this is
This is extremely interesting and very telling, you know.
Robots, even when you know it's a robot, can affect your emotions.
And yeah, that's definitely a takeaway from this.
But it tells us a whole lot more than that.
How do we talk to our kids?
it would be better to be encouraging, wouldn't it?
I mean, it's very clear that
if a robot
can affect an adult like that
and they know it's a robot
and they know it's programmed to do that
imagine what our words to our kids do
words have a lot of power
imagine
the effect
that internet comments have
like the ones that are going to show up below this video, surely
they probably have more
of an effect
than people really give them credit for
because the reality is
That's not a robot, that's a real person, while most times it's not a robot.
That's a real person.
Those are real views.
So before
you leave a comment like that,
understand that a lot of people are going to read it.
And it's going to affect them.
You have a choice
in what you
put out into the world and what you put out is going to be taken in by somebody
else even if it's even if you think it's just a harmless comment I must say
you're a terrible player from a robot had an effect your words will have an
effect on those people that read them anyway it's just a thought y'all have a
good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}