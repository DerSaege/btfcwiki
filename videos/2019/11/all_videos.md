# All videos from November, 2019
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2019-11-30: Let's talk about that new bill in Ohio.... (<a href="https://youtube.com/watch?v=NO5VVA0ElB8">watch</a> || <a href="/videos/2019/11/30/Lets_talk_about_that_new_bill_in_Ohio">transcript &amp; editable summary</a>)

Legislators in Ohio propose a controversial bill allowing doctors to remove ectopic pregnancies, facing criticism for ignorance and arrogance, urging public action to oppose such dangerous legislation.

</summary>

"It's a combination of arrogance and ignorance, otherwise this bill never would have existed."
"You cannot let this stand."
"This is everything that is wrong with America's leadership."
"They know nothing of the subject matter and incidentally, this is the second time it's come up."
"They were willing to put you at risk of execution without even reading the bill."

### AI summary (High error rate! Edit errors on video page)

Legislators in Ohio are proposing a bill that suggests doctors should remove ectopic pregnancies from the fallopian tubes and implant them in the uterus.
The bill implies that doctors who don't comply can be arrested for a crime called aggravated abortion murder, punishable by death.
Beau criticizes the bill as legislating medicine without a license, labeling it as aggravated arrogance murder due to ignorance and arrogance.
Despite Ohio facing hunger issues affecting 500,000 kids, legislators are focusing on controversial bills like this one.
The bill has garnered support from 21 sponsors in the House of Representatives in Ohio.
Beau questions the sponsors' understanding of the bill's implications, especially since it carries the death penalty.
He calls for action against the sponsors, urging people to campaign against them regardless of political affiliations.
Beau expresses disbelief at the lack of knowledge displayed by the sponsors regarding the bill's subject matter.
He condemns the level of arrogance and ignorance displayed by the legislators willing to pass such a bill.
Beau stresses the importance of removing such individuals from positions of power to enact positive change in the state.

Actions:

for ohio residents, activists,
Campaign against the sponsors of the bill in Ohio (suggested)
Advocate for informed and compassionate legislation (implied)
</details>
<details>
<summary>
2019-11-29: Let's talk about Trump's speech and a 200-year-old concept.... (<a href="https://youtube.com/watch?v=_Nhlu4IAlIc">watch</a> || <a href="/videos/2019/11/29/Lets_talk_about_Trump_s_speech_and_a_200-year-old_concept">transcript &amp; editable summary</a>)

Beau explains the importance of civilian control over the military, cautioning against blurring the line and potential war crimes.

</summary>

"The reason it exists is twofold."
"He's a civilian, he's not a general, period, full stop."
"When the head of state starts to be seen as the general, the next thing that happens is war crimes."
"Keeping Syria's oil, that's a war crime."
"It's illegal under the Geneva Conventions and the U.S. War Crimes Act."

### AI summary (High error rate! Edit errors on video page)

Reverse format for this historical event.
Hanging out with veterans and ex-contractors.
Watching Trump's speech to troops in Afghanistan.
Trump falsely claiming they kept oil in Syria.
Beau not fact-checking, focusing on civilian control.
Emphasizing civilian leadership over the military.
President as a civilian, not a rank like general.
Concept of civilian control of the military.
President's power for strategic decisions towards peace.
Importance of preventing military dictatorship.
Historic taboo on presidents saluting troops.
Reagan starting the tradition of saluting troops.
Concerns about blurring the line between civilian and military leadership.
Keeping Syria's oil as a potential war crime.
Importance of Pentagon's integrity in addressing such issues.

Actions:

for citizens, policymakers, military personnel.,
Ensure awareness and understanding of the concept of civilian control over the military (suggested).
Advocate for integrity within the Pentagon to uphold international and U.S. laws regarding war crimes (exemplified).
</details>
<details>
<summary>
2019-11-28: Let's talk about a message to parents about bonding.... (<a href="https://youtube.com/watch?v=4ir46_N23ls">watch</a> || <a href="/videos/2019/11/28/Lets_talk_about_a_message_to_parents_about_bonding">transcript &amp; editable summary</a>)

A reflection on parenting, violence, and preparing for the future in a changing world.

</summary>

"I don't want to bond over violence if it's really bonding."
"Cooperation, creation, building, that's what they're going to have to struggle to learn."
"The next war for humanity's survival is not going to be fought by soldiers or sailors and pilots."

### AI summary (High error rate! Edit errors on video page)

A single mother's message and a sports player taking his boys to the range sparked the topic.
Criticism arose regarding the reasons for taking the boys, things said, and teaching techniques.
Despite the criticism, bonding experiences between parents and children were brought up.
Beau shared his childhood experience of learning about military tactics and small arms from his father.
He chose not to teach his son about violence but instead sent him elsewhere for unarmed defense lessons.
Beau values memories of childhood involving cooperation, creation, and building over destruction.
He emphasized the importance of teaching children cooperation and creation instead of violence.
Beau discussed the quote about preparing for the next war rather than focusing on the last war.
He stressed the need for future generations to study fields like mathematics, philosophy, and engineering.
Beau emphasized that the next war for humanity's survival will require engineers, architects, and philosophers.

Actions:

for parents, educators, community leaders,
Teach children cooperation and creation through hands-on activities (exemplified)
Encourage children to study fields like mathematics, philosophy, engineering (exemplified)
</details>
<details>
<summary>
2019-11-26: We talked about emergency preparedness.... (<a href="https://youtube.com/watch?v=SCtLFnnyHEE">watch</a> || <a href="/videos/2019/11/26/We_talked_about_emergency_preparedness">transcript &amp; editable summary</a>)

Be prepared for disasters, be self-reliant, teach survival skills, and change how you see the world - Beau covers essentials from emergency readiness to mindset shifts.

</summary>

"We're not the sons of the pioneers and the daughters of Rosie the Riveter anymore."
"It's up to you. If not you, who? Who's gonna solve the problems if it's not you?"
"Just take care of this stuff and you're going to be able to help people."
"When people start to learn survival skills, they start to look at the world in a very, very different way."
"Supplies are pretty much everywhere. You just have to know where to look and how to think about them."

### AI summary (High error rate! Edit errors on video page)

Urges everyone to be prepared for natural disasters due to delayed government response.
Criticizes the inefficiency of federal agencies in disaster response.
Encourages families to create emergency kits with essentials like water, food, and first aid supplies.
Emphasizes the importance of being self-reliant and having a plan for evacuation.
Advocates for teaching survival skills to children in a fun and engaging way.
Shares examples of people overlooking readily available supplies during emergencies.
Stresses the value of improvisation and critical thinking learned through survival skills.
Encourages seeing the world differently by recognizing possibilities and the importance of change.
Concludes by underlining the significance of learning survival skills for the youth.

Actions:

for families, communities,
Prepare an emergency kit with essentials like water, food, first aid supplies (suggested)
Teach children survival skills in a fun and engaging way (suggested)
Encourage critical thinking and improvisation skills through survival training (suggested)
Practice looking at components as improvisational tools (implied)
</details>
<details>
<summary>
2019-11-26: Let's talk about a story you can tell on Thanksgiving.... (<a href="https://youtube.com/watch?v=igktJc1ufM4">watch</a> || <a href="/videos/2019/11/26/Lets_talk_about_a_story_you_can_tell_on_Thanksgiving">transcript &amp; editable summary</a>)

Beau narrates the inspiring tale of Nikolai Vavilov and Leningrad scientists, embodying sacrifice and dedication for the greater good, tying the story seamlessly to the essence of Thanksgiving.

</summary>

"When you really think about it, people that dedicated to helping others, they literally just waste away right next to food."
"The idea of Thanksgiving, being thankful for food, I think it fits."

### AI summary (High error rate! Edit errors on video page)

Tells a story fit for Thanksgiving with themes of poverty, sacrifice, duty, political conflict, and environmental consciousness.
Narrates the tale of Nikolai Vavilov, a man from a rural town dedicated to ending famine by gathering seeds worldwide.
Vavilov's pursuit led to the creation of the first seed bank in the Soviet Union.
Despite his contributions, Vavilov was denounced by a rival with Stalin's favor and sent to the gulags, where he died of starvation.
During World War II, scientists in Leningrad protected the seed bank through a siege, sacrificing their lives by not consuming the stored food.
The dedication of these scientists ensured the preservation of vital crop samples for future generations.
The seeds from this bank have influenced the food on our tables, connecting us to this impactful story of sacrifice and dedication.
The narrative showcases individuals who prioritize the greater good over their own survival, symbolizing the essence of Thanksgiving.
Beau leaves listeners with a reflection on the significance of being thankful for food and the enduring legacy of those who work to address global challenges.

Actions:

for history enthusiasts, environmentalists, storytellers,
Preserve local plant varieties and seeds to protect biodiversity (implied)
Support seed banks or initiatives that focus on crop preservation (implied)
</details>
<details>
<summary>
2019-11-25: Let's talk about what talking robots can tell us about ourselves.... (<a href="https://youtube.com/watch?v=jo13NLjIEZM">watch</a> || <a href="/videos/2019/11/25/Lets_talk_about_what_talking_robots_can_tell_us_about_ourselves">transcript &amp; editable summary</a>)

Beau from Carnegie Mellon shows how a robot's words affect gameplay, urging us to understand the power of our own words, especially towards children and online.

</summary>

"Words have a lot of power."
"You have a choice in what you put out into the world."
"Your words will have an effect on those people that read them."
"Even if you think it's just a harmless comment..."
"The reality is that's a real person."

### AI summary (High error rate! Edit errors on video page)

Talks about a study from Carnegie Mellon University where people played a game against a robot named Pepper.
Some players were encouraged by the robot while others were insulted.
Players who were encouraged performed better, while those who were insulted played worse.
Even though the players knew it was a robot and programmed to act that way, it affected their emotions and gameplay.
AI researchers find this interaction between robots and humans extremely interesting.
Beau raises the point that if robots can impact adults like this, the power of words on children must be considered.
He mentions the influence of internet comments and how they can affect individuals more than they realize.
Beau urges viewers to understand the impact their comments can have on others, as they are real people reading them.
He stresses the importance of being mindful of the words we put out into the world.
Beau concludes by reminding viewers that their words, even seemingly harmless ones, can have a significant effect on others.

Actions:

for online users,
Choose words carefully (implied)
Be mindful of comments' impact (implied)
</details>
<details>
<summary>
2019-11-25: Let's talk about the Secretary of the Navy, tweets, and consequences.... (<a href="https://youtube.com/watch?v=NfwTPc_mKnw">watch</a> || <a href="/videos/2019/11/25/Lets_talk_about_the_Secretary_of_the_Navy_tweets_and_consequences">transcript &amp; editable summary</a>)

Secretary of Navy resigns, President interferes in military proceedings, posing with a corpse deemed acceptable - damaging moral compass and counterinsurgency efforts.

</summary>

"One guy has done more damage to this country's moral compass than any other president in history."
"The standard has been set. This isn't a big deal."
"You better fight harder."
"Changed the way you looked at things."
"We lowered the bar."

### AI summary (High error rate! Edit errors on video page)

Secretary of Navy's resignation due to discrepancy between public statements and private actions.
Administration prioritizes truthfulness and transparency, sarcastically mentioning their stellar reputation.
Secretary of Navy's refusal to obey an order believed to violate his oath to support and defend the Constitution.
Admirals' concern about good order and discipline after the President interfered in military proceedings.
President's tweet expressing dissatisfaction with Navy Seal Eddie Gallagher's trial outcome.
Gallagher was acquitted on major charges but found to have posed with an enemy prisoner's corpse.
Beau questions the seriousness of posing with a corpse, explaining its impact on propaganda for the opposition.
Beau stresses the repercussions of such actions on local populace and counterinsurgency efforts.
Concerns raised about the President's orders being delivered via tweet to a wide audience.
Beau criticizes the lowering of moral standards and its implications on military operations.
Beau underlines the psychological impact of such actions on public perception and counterinsurgency success.
The lasting effects of setting a standard where posing with a corpse is considered acceptable.
Beau points out the significant damage done to the country's moral compass by such actions.

Actions:

for military personnel, policymakers, activists,
Challenge and question actions that compromise moral standards (exemplified).
Advocate for transparent and ethical leadership in the military (exemplified).
Educate others on the psychological impact of actions on public perception (implied).
</details>
<details>
<summary>
2019-11-25: Let's talk about a new vehicle for societal change on YouTube.... (<a href="https://youtube.com/watch?v=kkxPjpYFgLM">watch</a> || <a href="/videos/2019/11/25/Lets_talk_about_a_new_vehicle_for_societal_change_on_YouTube">transcript &amp; editable summary</a>)

The FTC's enforcement of a law on childhood privacy affects kids' content on YouTube, urging major creators to utilize their influence for societal change.

</summary>

"They can influence change, but they're probably not going to."
"If you have that appearance or that voice that would appeal to children, you want to get involved, this is something you can do."
"The driver's door just opened. You can really get somewhere with this."
"You can literally raise the children that don't have the influence from the parents they need."
"I think you can create a better world with a camera and a YouTube channel."

### AI summary (High error rate! Edit errors on video page)

The FTC is enforcing a 1998 law on childhood privacy, affecting kids' content on YouTube.
Creators of kids' content may see a drop in income due to the removal of targeted ads.
Kid-directed content involves high production costs with visual effects, sound effects, travel, location shooting, and toy unboxing.
Major kid content creators have the potential to influence change by educating their viewers about laws.
There is a sense of learned helplessness among people when it comes to government actions.
Despite having millions of subscribers, major creators may not utilize their influence for change.
Starting a kids' content YouTube channel with low overhead costs could be profitable as YouTube is profit-driven.
Creating content like story time teaching children with morals can be financially viable with minimal overhead.
Beau suggests creating content that is more educational like Mr. Rogers instead of traditional cartoons.
Using a YouTube channel for societal change is emphasized by Beau, encouraging those interested to get involved.

Actions:

for content creators,
Start a kids' content YouTube channel with low overhead costs to adapt to the changes in kid-directed content (suggested)
Create educational content like story time teaching children with morals to make an impact (implied)
Use your appearance and voice to appeal to children and get involved in creating content for societal change (implied)
</details>
<details>
<summary>
2019-11-24: Let's talk about a Thanksgiving message from a Native leader.... (<a href="https://youtube.com/watch?v=xrGl0lOFzzY">watch</a> || <a href="/videos/2019/11/24/Lets_talk_about_a_Thanksgiving_message_from_a_Native_leader">transcript &amp; editable summary</a>)

Beau expresses gratitude, encourages kindness and generosity, and longs for freedom, reflecting on Thanksgiving and indigenous experiences in America.

</summary>

"If you see someone hurting and in need of a kind word or two, be that person who steps forward and lends a hand."
"Thank you for listening to whomever is voicing my words."
"There isn't a minute in any day that passes without me hoping that this will be the day I will be granted freedom."
"I long for the day when I can smell clean, fresh air, witness the clouds as their movement hides the sun."
"Thank you for continuing to support and believe in me."

### AI summary (High error rate! Edit errors on video page)

Reading a heartfelt message reflecting on Thanksgiving and the indigenous experience in America.
Expressing gratitude for those fighting for environmental protection and indigenous rights.
Encouraging generosity towards those in need and speaking out against injustice.
Longing for freedom and a return to nature.
Thanking supporters and hoping for a day of liberation.
Acknowledging the importance of remembering ancestors' sacrifices.
Describing a vision of freedom, fresh air, and connection to nature.
Emphasizing the need to support indigenous communities.
Urging listeners to be kind, generous, and brave in the face of injustice.
Ending with a message of gratitude and hope for freedom.

Actions:

for supporters of indigenous rights,
Support indigenous communities by donating resources or volunteering to help (implied)
Speak up against injustice and confront it bravely in your community (implied)
Extend kindness and generosity to those in need by offering help or resources (implied)
</details>
<details>
<summary>
2019-11-23: Let's talk about the three most exciting sounds in the world.... (<a href="https://youtube.com/watch?v=SwkdHcm19Iw">watch</a> || <a href="/videos/2019/11/23/Lets_talk_about_the_three_most_exciting_sounds_in_the_world">transcript &amp; editable summary</a>)

Beau talks about the importance of exploring and experiencing one's community to broaden the mind and combat societal detachment.

</summary>

"Those sounds signal that adventure is coming, travel is coming, we're going to see something new."
"It might be time to visit your own community and experience it rather than just see it."

### AI summary (High error rate! Edit errors on video page)

Beau talks about the exciting sounds that signal adventure like the whistle of a train, an anchor chain, and an airplane engine.
He mentions how in the internet age, people can see everything online, losing the desire for adventure and contact with strangers in strange places.
Americans travel abroad and leave their state very little, missing out on experiencing their own communities.
Beau encourages exploring one's community, even if it's close by, to see something worth experiencing.
He points out that travel broadens the mind not just by moving from one place to another but by being present in a different community.
Beau suggests that being part of a community and experiencing it can have a significant impact, especially in today's detached society.
Taking kids to places like fairs or zoos can be mundane for adults but a fascinating experience for children as they interact with new things.
Amid the division portrayed in the news, Beau believes it's vital to realize that people aren't as divided and hostile towards each other as it may seem.
He hints at the importance of reconnecting with one's community, appreciating the simple sounds and experiences around us.

Actions:

for community members,
Visit a local landmark or special place in your community (implied)
Take your kids to new places like fairs or zoos to let them interact with different things (implied)
</details>
<details>
<summary>
2019-11-23: Let's talk about the impeachment's mid-season plot twist.... (<a href="https://youtube.com/watch?v=5J5Prg2bV98">watch</a> || <a href="/videos/2019/11/23/Lets_talk_about_the_impeachment_s_mid-season_plot_twist">transcript &amp; editable summary</a>)

Beau gives an overview of the impeachment episode, pointing out twists and suggesting granting immunity to a key Ukrainian figure for testimony amid corruption concerns.

</summary>

"I didn't have this on my impeachment bingo card."
"I gotta know how this season ends."
"I'd rather have this Ukrainian sitting in the Oval Office than the President."
"It's not just a symptom of the Republican Party, it's a symptom of us not valuing substance, not valuing leadership."
"I'm hooked on this show."

### AI summary (High error rate! Edit errors on video page)

Excited to share about the impeachment episode and the unexpected plot twists.
Overview of the alleged attempts by the President to extort Ukraine into investigating Hunter Biden.
Mention of Ukraine as a strategic partner for the United States.
Description of Nunez as the President's biggest defender during the impeachment inquiry.
Details about Ukrainians tied to Rudy Giuliani and Fraud Guarantee getting arrested for campaign finance issues.
Reference to evidence implicating Nunez from the beginning and the House Democratic Coalition filing an ethics complaint.
Mention of emails linking Giuliani to the White House and Secretary of State Pompeo in efforts to smear the ambassador in Ukraine.
Suggestion to grant immunity to a Ukrainian involved and have him testify publicly.
Criticism of corruption within the administration and lack of trust in impeachment proceedings.
Reflection on valuing substance and leadership in politics.

Actions:

for politically engaged individuals,
Grant full immunity to the Ukrainian figure implicated in the events for public testimony (suggested).
Keep a close eye on the individual involved (suggested).
</details>
<details>
<summary>
2019-11-23: Let's talk about a statistic out of California and blue-collar skills.... (<a href="https://youtube.com/watch?v=aZXIDiiZQ6U">watch</a> || <a href="/videos/2019/11/23/Lets_talk_about_a_statistic_out_of_California_and_blue-collar_skills">transcript &amp; editable summary</a>)

Beau reveals the significant trend of homemade firearms in California, underscoring the need to address root causes rather than focus on bans.

</summary>

"Now we do because the information is out."
"It tells us a lot and it tells us that this is something that has to be taken into consideration anytime legislation is proposed."
"So this shows us again that we have to address root causes."
"People in the United States have way too much of a desire to own a firearm."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Talks about statistics from California regarding homemade firearms.
Mentions the significance of the data but notes that it's often overlooked.
Raises the issue of individuals building firearms at home to bypass bans.
Mentions the ease of manufacturing firearms at home, referencing the ATF report.
Points out that 30% of firearms in California are homemade, indicating a significant trend.
Notes that homemade firearms are used to circumvent California's restrictions.
Emphasizes that manufacturing firearms at home is easier than dealing with neighboring states' gun laws.
States that a variety of firearms, from pistols to AKs, are being produced at home.
Explains the process of making firearms at home, focusing on manufacturing the receiver.
Stresses the importance of addressing root causes rather than focusing on banning specific parts.

Actions:

for legislators, gun control advocates,
Advocate for comprehensive legislation addressing root causes (suggested)
Raise awareness about the implications of homemade firearms (implied)
</details>
<details>
<summary>
2019-11-22: Let's talk about what China can teach us about the Ukraine narrative.... (<a href="https://youtube.com/watch?v=6ookSSqyC1c">watch</a> || <a href="/videos/2019/11/22/Lets_talk_about_what_China_can_teach_us_about_the_Ukraine_narrative">transcript &amp; editable summary</a>)

Beau explains the debunked Ukraine election interference theory by comparing it to a historic fake story, showcasing how misinformation spreads without verification, impacting public trust.

</summary>

"Ideas travel faster than bullets."
"How could they be this dumb?"
"It doesn't even make sense, but it got repeated just like this story."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of Ukraine interfering in the 2016 election.
Draws a parallel to a fabricated story about tearing down the Great Wall of China in 1899.
Mentions a made-up person, Frank C. Lewis, and how journalists created a false narrative.
Talks about how the fake Chinese story spread and led to the manufacturing of evidence.
Debunks the conspiracy theory about Ukraine's interference in the election.
Points out that CrowdStrike, a Ukrainian company, was falsely accused, though it's American.
Criticizes the spread of misinformation by right-wing news outlets.
Compares the repetition of false stories to how misinformation spreads.
Raises concerns about the impact of repeated lies on public perception and trust.

Actions:

for information seekers,
Verify information before sharing (implied).
</details>
<details>
<summary>
2019-11-22: Let's talk about silver, gold, Vikings, Kings, history, and tradition.... (<a href="https://youtube.com/watch?v=ANOEbHqv2aw">watch</a> || <a href="/videos/2019/11/22/Lets_talk_about_silver_gold_Vikings_Kings_history_and_tradition">transcript &amp; editable summary</a>)

Beau reveals a treasure trove of Viking coins challenging historical narratives and traditions, urging reflection on the value of history over precious metals.

</summary>

"Two guys stumbled across a viking stash of coins and jewelry."
"The histories at the time, well they're a little murky."
"This is upending history and tradition."
"Silver and gold, even today, survivalists will tell you to keep some around because it's always valuable."
"It's just a thought, y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Two individuals stumbled upon a Viking treasure stash of coins and jewelry in 2015, with coins dating back 1100 years.
The stash was worth roughly 3.8 million dollars in the US.
Instead of alerting authorities, they decided to keep and eventually sell the treasure on the open market.
Only 31 out of the 300 coins have been recovered, some of which have significant historical value.
Some coins reveal an alliance between Alfred the Great of Wessex and Caelwulf II of Mercia, challenging traditional historical narratives.
The coins suggest a different story where Caelwulf II was actively allied with Alfred the Great, contrary to the usual portrayal of him as a puppet of the Vikings.
The disappearance of Caelwulf II from historical records around 879 raises suspicions about how Alfred the Great gained control of Mercia.
Beau draws a parallel to the series "The Last Kingdom" on Netflix for those unfamiliar with the historical context.
The discovery of these coins has the potential to rewrite history and challenge established traditions.
Beau questions the intrinsic value of silver and gold, likening it to the value placed on the history and traditions of a nation.

Actions:

for history enthusiasts,
Research local archaeology groups to support preservation efforts (suggested)
Visit historical sites or museums to learn more about ancient civilizations (implied)
</details>
<details>
<summary>
2019-11-21: Let's talk about COPPA (Not Legal Advice).... (<a href="https://youtube.com/watch?v=OUid1YxgwsU">watch</a> || <a href="/videos/2019/11/21/Lets_talk_about_COPPA_Not_Legal_Advice">transcript &amp; editable summary</a>)

Beau explains COPPA's impact on YouTube creators, urging action to address the negative consequences for wholesome kids' content and creators.

</summary>

"It's dead. There's no comment section. They won't get a notification. The monetization is severely limited."
"What they're doing is they're going to limit their child's entertainment and educational opportunities, what they can see for viewing."
"We don't need to succumb to learned helplessness. We don't just have to roll over and take this."

### AI summary (High error rate! Edit errors on video page)

Explains COPPA, a law impacting YouTube creators due to child privacy protection.
Mentions emotional reactions from creators, some not fully understanding the law's implications.
Notes the potential impact on wholesome kids' content on YouTube.
Describes the practical implications of COPPA, including marking videos for kids and potential fines for non-compliance.
Outlines factors determining kids' content, clarifying misunderstandings around the list provided by YouTube.
Raises concerns about the subjective nature of compliance with COPPA.
Emphasizes the severe consequences for creators if they mislabel their content.
Suggests actions to address the issues with COPPA, including contacting the FTC and legislators to push for changes.
Urges support for kid content creators through Patreon or merchandise purchases.
Advocates for getting rid of COPPA altogether, citing its outdated nature and negative impact on content creation.
Encourages viewers to take action by contacting representatives and engaging with the issue.

Actions:

for content creators, youtube users,
Contact the FTC and leave comments to advocate for changing COPPA (implied).
Reach out to representatives and senators to explain the outdated nature of the law and push for revisions (implied).
Support kid content creators by signing up for their Patreon or buying their merchandise (implied).
Tweet the president to raise awareness and potentially influence the FTC's decisions (implied).
</details>
<details>
<summary>
2019-11-20: Let's talk about impeachment, today, and tomorrow.... (<a href="https://youtube.com/watch?v=GRzfXOkHbk4">watch</a> || <a href="/videos/2019/11/20/Lets_talk_about_impeachment_today_and_tomorrow">transcript &amp; editable summary</a>)

The impeachment trial's historical significance transcends partisan politics as the Senate's decision shapes the balance of power between branches.

</summary>

"His initial testimony conflicts with what was given by pretty much all of the other witnesses."
"It's not about the immediate political fallout. It's about the long-term fallout."
"The Senate will go down in history one way or the other."
"Allowing foreign nationals to influence our election, it's going to be mighty hard to complain about foreign treaties."
"There's a whole bunch riding on this and it goes far, far beyond partisan politics."

### AI summary (High error rate! Edit errors on video page)

The impeachment trial is coming up, with a key testimony from Sondland tomorrow.
Even witnesses called by Republicans provided damaging evidence against the president.
Regardless of Sondland's testimony tomorrow, the evidence against the president is already strong.
Asking for a statement from a foreign national for a campaign is a crime, regardless of quid pro quo.
Historians will focus on the evidence presented in the impeachment trial.
The Senate's decision on impeachment will have long-term historical significance.
The Senate could choose to convict and remove the president or dismiss the trial altogether.
Republicans should be mindful of the precedent set for future presidents in terms of executive power.
Allowing foreign influence in elections sets a dangerous precedent for future administrations.
The Senate's decision will shape its historical legacy.

Actions:

for political observers, voters, historians,
Contact senators to urge them to prioritize the long-term implications of their impeachment trial decision (suggested).
Stay informed about the impeachment trial proceedings and outcomes (implied).
</details>
<details>
<summary>
2019-11-20: Let's talk about a hero you've probably never heard of.... (<a href="https://youtube.com/watch?v=jP4KFyqua3w">watch</a> || <a href="/videos/2019/11/20/Lets_talk_about_a_hero_you_ve_probably_never_heard_of">transcript &amp; editable summary</a>)

Beau introduces Martha Gellhorn, an exceptional correspondent who defied norms, inspiring societal change.

</summary>

"They are wrong in all of the right ways, and it moves our society forward."
"It's changing because of people like her."
"A lot of things in society change because people just do it anyway."

### AI summary (High error rate! Edit errors on video page)

Introduces one of his heroes, Martha Gellhorn, as one of the most interesting and prolific correspondents of the 20th century.
Martha Gellhorn covered significant events like the Spanish Civil War, the rise of a German dictator, D-Day, camps during WWII, Vietnam, Israeli-Arab conflicts, wars in Central America, and almost went to Bosnia.
Shares a fascinating story of Martha Gellhorn sneaking ashore on D-Day by impersonating a stretcher bearer.
Mentions Martha Gellhorn's detention and escape after being arrested for going ashore without authorization on D-Day.
Talks about Martha Gellhorn's marriage to Ernest Hemingway and how she challenged societal norms in combat correspondence.
Recognizes Martha Gellhorn as someone who changed the rules and broke societal barriers by being wrong in the right ways.
Acknowledges that many individuals who challenge societal norms often do not receive the recognition they deserve.

Actions:

for journalists, historians, activists,
Read about Martha Gellhorn's extraordinary life and contributions (suggested)
</details>
<details>
<summary>
2019-11-20: Let's talk about SEALs and deals.... (<a href="https://youtube.com/watch?v=HGvlgfBUK0Q">watch</a> || <a href="/videos/2019/11/20/Lets_talk_about_SEALs_and_deals">transcript &amp; editable summary</a>)

Chief Gallagher's potential removal from the SEALs could deepen divides between the Navy, Defense Department, and the President, showcasing a unique political development.

</summary>

"The fallout of this action will further drive a wedge between the Department of Defense, specifically the Department of the Navy, and the President."
"SEALs are supposed to be a cut above everybody, in every respect."

### AI summary (High error rate! Edit errors on video page)

Chief Gallagher, a SEAL accused of misconduct, was cleared by the President after being demoted.
Admiral Greene, along with other top Navy officials, plan to remove Gallagher from the SEALs.
The media reports suggest Gallagher may lose his trident badge symbolizing SEAL qualification.
The potential expulsion of Gallagher is within the Navy's power and may spark a political firestorm.
SEALs are held to high standards, with past instances of even DUIs leading to expulsion.
Gallagher, if removed, will likely take on different duties, preventing a repeat of the alleged misconduct.
The fallout from Gallagher's case could deepen the divide between the Department of Defense and the President.
This situation could lead to further isolation of the White House, a unique development in American history.
Beau anticipates a strong reaction, possibly in the form of a tweet storm, if Gallagher is indeed expelled.

Actions:

for military personnel, political analysts,
Watch for updates and developments in Chief Gallagher's case (implied)
Stay informed about the impact of military decisions on political dynamics (implied)
</details>
<details>
<summary>
2019-11-19: Let's talk about straws and studies.... (<a href="https://youtube.com/watch?v=TTkSFu1OQOI">watch</a> || <a href="/videos/2019/11/19/Lets_talk_about_straws_and_studies">transcript &amp; editable summary</a>)

Beau addresses misconceptions on emissions, advocates for plant-based alternatives, and stresses the power of individual action in driving environmental change.

</summary>

"We can't be defeated by a straw."
"Your voice matters more than you think."
"It requires individual action."

### AI summary (High error rate! Edit errors on video page)

Introduced the topic of straws and studies, addressing the controversy surrounding giving up plastic straws.
Pointed out that most people misinterpret information and discussed the impact of 100 companies producing 71% of emissions.
Raised concerns about how the U.S. might be defeated by a simple plastic straw due to lack of suitable alternatives.
Proposed using plant-based, biodegradable straws as a viable solution, mentioning their existence and cost.
Urged for individual responsibility and action in tackling environmental issues, contrasting popular misconceptions about emissions.
Advocated for a cultural shift away from profit-driven motives and emphasized the power of individual action over waiting for governmental intervention.
Stressed the importance of sending letters to major corporations like McDonald's to drive change and mentioned the effectiveness of boycotting.
Encouraged viewers to believe in the impact of their voices and emphasized the significance of individual actions in inciting change.

Actions:

for environmental activists, conscious consumers,
Contact major corporations like McDonald's to advocate for sustainable alternatives (suggested)
Boycott companies to drive change towards sustainability (exemplified)
</details>
<details>
<summary>
2019-11-19: Let's talk about learning from sliced bread and Gibraltar.... (<a href="https://youtube.com/watch?v=EBCSjUkywBM">watch</a> || <a href="/videos/2019/11/19/Lets_talk_about_learning_from_sliced_bread_and_Gibraltar">transcript &amp; editable summary</a>)

Beau examines the sacrifices of the greatest generation and criticizes the modern reluctance to make similar sacrifices in the face of global threats.

</summary>

"Yeah, these things aren't as dramatic as storming a machine gun nest on D-Day, but it's those little things that could truly alter the course of history."
"Today, people don't want to give up single-use straws. That's too much to ask when confronted with a global threat."
"These were people who lived through the Depression. They knew what hard times were."
"We have a global threat and it's a real one. Doesn't matter what the study from the oil company tells you."
"It's going to take both the dramatic and the mundane to solve it anyway."

### AI summary (High error rate! Edit errors on video page)

Examines the lessons to learn from the sacrifices made by the greatest generation during WWII.
Describes the lesser-known but significant actions taken by individuals facing a global threat.
Recounts the story of six individuals who volunteered to stay in secret chambers in Gibraltar to report back if it fell to the opposition.
Reveals the commitment of these individuals who were willing to be sealed inside the chambers with years worth of supplies.
Mentions Operation Tracer, a compartmentalized plan involving multiple teams willing to stay sealed in rooms for years.
Contrasts the minor inconveniences faced in the U.S. during WWII, like the temporary ban on sliced bread, to contribute to the war effort.
Draws parallels between the sacrifices made by the greatest generation and the lack of willingness in today's society to make similar sacrifices in the face of a global threat.
Criticizes the reluctance of current generations to give up conveniences like single-use straws when compared to the sacrifices of those before them.
Emphasizes the importance of both dramatic and mundane actions in addressing present-day global threats.
Calls for a shift in mindset towards tackling real global issues with the same dedication and sacrifice as seen in the past.

Actions:

for activists, history enthusiasts,
Form a local community group dedicated to environmental conservation and taking small, impactful actions. (implied)
Educate others about the historical sacrifices made during times of crisis to inspire collective action. (implied)
</details>
<details>
<summary>
2019-11-18: Let's talk about the wall and Constitutional obligations.... (<a href="https://youtube.com/watch?v=hmFGSiAWmRE">watch</a> || <a href="/videos/2019/11/18/Lets_talk_about_the_wall_and_Constitutional_obligations">transcript &amp; editable summary</a>)

Beau on lack of border wall progress, potential crimes, and constitutional duty to remove the president amidst turning base.

</summary>

"There was already a wall there. Where's the new wall?"
"It is now the Senate's constitutional obligation to remove the president."
"His base is shrinking rapidly because he keeps insulting them."
"All evidence shows that he violated the law more than likely."
"It's more than branding and that's what Trump is good at, branding."

### AI summary (High error rate! Edit errors on video page)

Down on the Mexican border, searching for Trump's wall, Beau gets lost and may have ended up in Mexico.
Beau questions the lack of progress on the border wall, noting that not a single new mile has been built in three years.
He criticizes the government for disrupting processes and seizing land without fair compensation under the Declaration and Taking Act.
Beau points out the failure of Mexico to pay for the wall, despite promises.
He addresses public opinion on the Ukraine call, suggesting that regardless of intentions, a crime may have been committed.
Beau argues that the Constitution mandates the Senate to remove the president if he committed crimes, including bribery and extortion.
The lack of progress on the wall, military decisions in Syria, and trade wars are factors causing even his base to turn against the president.
Beau questions Trump's ability to handle the job as president, mentioning his focus on branding and economic issues.
Despite some successes in avoiding recession, Beau believes all evidence points to Trump violating the law and suggests it's the Senate's obligation to remove him.

Actions:

for voters, concerned citizens,
Contact your senators to express your views on potential violations of the law by the president (suggested).
Join or support organizations advocating for government accountability (exemplified).
Organize community dialogues on constitutional obligations and holding leaders accountable (implied).
</details>
<details>
<summary>
2019-11-18: Let's talk about party loyalty and what happened in Louisiana.... (<a href="https://youtube.com/watch?v=Q524bP2gMQg">watch</a> || <a href="/videos/2019/11/18/Lets_talk_about_party_loyalty_and_what_happened_in_Louisiana">transcript &amp; editable summary</a>)

Republicans lost in Louisiana because loyal Republicans stuck to their values, showing that voting means becoming morally responsible for the candidate you support.

</summary>

"You don't co-sign evil out of tradition."
"Voting when you vote for someone, you become morally responsible."
"You become complicit in everything they do."
"If you vote for Trump, past, present, and future, Trump is you."
"You own that. You co-signed it."

### AI summary (High error rate! Edit errors on video page)

Republicans lost in Louisiana, a deep red state, because loyal Republicans stuck to their values.
If your party doesn't offer a candidate worthy of your vote, you can choose to abstain from voting.
Voting is not just about party loyalty; it's about becoming morally responsible for the actions of the candidate you support.
By voting for a candidate, you become complicit in everything they do, past, present, and future.
Voting for a candidate means you are endorsing and supporting all their actions.
Beau questions whether individuals who support Trump through their vote truly agree with actions like putting kids in cages or weakening environmental protections.
Voting for a candidate means taking ownership of their actions and decisions.
Beau urges people to think critically about the actions of the candidates they support rather than blindly following party loyalty.
Supporting a candidate through voting means endorsing everything they stand for and everything they do.
You can choose not to vote if the candidate presented by your party conflicts with your values and principles.

Actions:

for voters,
Re-evaluate your loyalty to a political party based on the actions and values of the candidates they present (exemplified)
</details>
<details>
<summary>
2019-11-16: Let's talk about today in Trump world.... (<a href="https://youtube.com/watch?v=5sdbnGAbliA">watch</a> || <a href="/videos/2019/11/16/Lets_talk_about_today_in_Trump_world">transcript &amp; editable summary</a>)

Beau recaps notable events from Trump world, warning Republicans of their legacy's impending tarnish for blindly supporting Trump's actions.

</summary>

"You're throwing away your legacies to protect somebody who is not going to protect you."
"Certainly painting Trump as a very self-absorbed person who does not care about the United States."
"Those in Congress, in the Senate, that are defending him will forever be remembered, for the rest of their very short political careers, as idiots who got conned by a real estate mogul."

### AI summary (High error rate! Edit errors on video page)

Recapping the noteworthy events from Trump world, including a probe into Giuliani's potential personal benefit from a Ukrainian energy company.
Giuliani questioned about whether Trump might betray him in the impeachment hearing and his cryptic reference to having "very, very good insurance."
President Trump's intimidating tweet about witness Yovanovitch, adding to his list of impeachable offenses.
Witness David Holmes testified about a call where Trump specifically inquired about the Biden investigation, eliminating plausible deniability.
Roger Stone convicted on all counts for obstructing the Russia Trump investigation, signaling potential future criminal prosecutions post-administration.
Republicans are warned about their defense of Trump, as future administrations will uncover the truth, tarnishing their legacies.
Criticizing Trump's self-absorbed nature and lack of concern for the country's well-being.
The episode ends with a foreboding message about the consequences for those blindly supporting Trump.

Actions:

for political observers, republicans,
Hold elected officials accountable for their support of actions that harm the country (implied).
Prepare for potential criminal prosecutions post-administration by staying informed and engaged (implied).
</details>
<details>
<summary>
2019-11-16: Let's talk about storing open source code in the Arctic.... (<a href="https://youtube.com/watch?v=OyiW1GRyKM8">watch</a> || <a href="/videos/2019/11/16/Lets_talk_about_storing_open_source_code_in_the_Arctic">transcript &amp; editable summary</a>)

Microsoft stores open source code in Arctic vault, questioning human arrogance and proposing basic knowledge preservation for humanity's survival.

</summary>

"Perhaps, given humanity's seeming unwillingness to work together to avoid a fall, maybe we should focus on building vaults all over the place."
"Basic information about medicine, food preparation, agriculture, that's what humanity needs to survive."
"World governments and large companies foresee a world reset within 750 years because that's how long that film lasts."

### AI summary (High error rate! Edit errors on video page)

Microsoft is storing the world's open source code in a vault in the Arctic, similar to the crypt of civilization built in the 40s at Oglethorpe University in Georgia.
The crypt at Oglethorpe University contains microfilm and was created due to the lack of information about ancient civilizations after their fall.
The Arctic World Archive near the global seed vault in Norway houses the open source code on a microfilm-like substance.
Time capsules aim to send information to the future, acknowledging that future civilizations may not have certain knowledge.
There's a mix of useful information like open source code and encyclopedic knowledge, but also trivial items like the secret sauce recipe for McDonald's in the Vatican archives.
Beau questions the arrogance behind presuming to know what future civilizations will need, suggesting a focus on preserving basic human knowledge instead.
He proposes multiple vaults containing vital information on medicine, food preparation, and agriculture as a more practical approach to ensuring humanity's survival.
The reality is that governments, large companies, and institutions like Microsoft and McDonald's foresee a world reset within 750 years, the lifespan of the microfilm.
Beau concludes by suggesting the idea of preparing for such a collapse by disseminating fundamental human knowledge.

Actions:

for concerned citizens, futurists,
Build community-led vaults with basic human information for survival (suggested)
Preserve knowledge on medicine, food preparation, and agriculture in accessible formats (suggested)
</details>
<details>
<summary>
2019-11-14: We talked about masculinity.... (<a href="https://youtube.com/watch?v=PflmaMpuGEc">watch</a> || <a href="/videos/2019/11/14/We_talked_about_masculinity">transcript &amp; editable summary</a>)

Beau talks about the fluidity of masculinity, the concept of Shibumi, and the importance of defining masculinity for oneself while challenging toxic masculinity and advocating for women's autonomy.

</summary>

"Masculinity is something you're going to define for yourself."
"The fact that you don't find her attractive doesn't matter. What you think doesn't matter."
"All women have to be two things and that's it. Who and what they want."

### AI summary (High error rate! Edit errors on video page)

Talks about the ridiculousness of guides designed to help young American males find their masculinity.
Shares a personal story about a Japanese concept called Shibumi explained by an old Japanese man.
Describes masculinity as subtle, understated, in control of nature, and effortless perfection.
Asserts that masculinity cannot be quantified or charted, and each person's masculinity is unique.
Emphasizes masculinity as a journey of introspection and self-improvement, not a checklist or conforming to societal norms.
Criticizes toxic masculinity and its origins in glorified violence and immature behavior.
Challenges the idea of toxic masculinity glorification by some men.
Counters arguments against feminism and patriarchy, pointing out the need for equal representation and the existence of patriarchal systems.
Advocates for respecting women's autonomy and choices, regardless of personal opinions or insecurities.

Actions:

for men, feminists, advocates,
Challenge toxic masculinity by uplifting others and focusing on self-improvement (implied).
Advocate for equal representation of women in positions of power (implied).
Respect women's autonomy and choices, regardless of personal opinions (implied).
</details>
<details>
<summary>
2019-11-14: Let's talk about thoughts and prayers.... (<a href="https://youtube.com/watch?v=wOfpAfIppk4">watch</a> || <a href="/videos/2019/11/14/Lets_talk_about_thoughts_and_prayers">transcript &amp; editable summary</a>)

Beau dives into the societal glorification of violence and the urgent need for non-violent solutions to address the underlying issues leading to tragic events.

</summary>

"We have a systemic problem."
"Our high school students are talking like rangers after the first time they were in combat."

### AI summary (High error rate! Edit errors on video page)

Discussed the reactions after seeing something trending on Twitter.
Mentions the common responses to tragic events: better security, arming teachers, and gun control.
Critiques thoughts and prayers as not being a solution.
Explores the impracticality of extreme security measures at schools.
Raises the issue of the underlying problem being kids wanting to harm others.
Points out the cycle of violence being perpetuated by societal norms.
Calls for comments on solutions that do not involve additional violence.
Urges for a cultural shift away from glorifying violence.
Expresses the need for a systemic change rather than relying on thoughts and prayers.
Shares the perspective of a student feeling unprepared despite drills in schools.

Actions:

for community members, activists,
Share and create solutions that do not involve additional violence (suggested).
Advocate for a cultural shift away from glorifying violence (implied).
</details>
<details>
<summary>
2019-11-14: Let's talk about the happiest prisoner.... (<a href="https://youtube.com/watch?v=6Dl5kYKBGxk">watch</a> || <a href="/videos/2019/11/14/Lets_talk_about_the_happiest_prisoner">transcript &amp; editable summary</a>)

Beau shares the story of Joe, the happiest prisoner on death row in the 1930s, urging action on Rodney Reed's case in Texas within a week.

</summary>

"His name was Joe Aradie, known as the happiest prisoner on death row."
"Took me too long to see a toy train."

### AI summary (High error rate! Edit errors on video page)

Beau shares a personal anecdote about his son playing with a toy train, leading into a story he wanted to talk about.
He introduces the story of Joe, known as the happiest prisoner on death row, in the 1930s.
Joe, with an IQ of 45, faced a challenging life and was picked on before being arrested for vagrancy in Cheyenne, Wyoming.
Sheriff Carroll questions Joe about crimes in Pueblo, and Joe falsely confesses to be with another man named Frank.
Despite inconsistencies in Joe's confession and lack of identification by the surviving sister, he is convicted and sentenced to death.
Joe's attorney tries to help with appeals, but Joe is executed without a pardon.
Joe's last moments before execution involve playing with a toy train, showing innocence and ignorance of his impending death.
Beau mentions Rodney Reed's case in Texas and urges viewers to research and possibly take action within a week.
Beau concludes with a reflective note, leaving the audience with something to ponder.

Actions:

for advocates for justice,
Research Rodney Reed's case and contact the governor within a week (implied)
Advocate for justice by raising awareness about cases like Rodney Reed's (implied)
</details>
<details>
<summary>
2019-11-13: Let's talk about impeachment, being uncomfortable, and saving lives.... (<a href="https://youtube.com/watch?v=fcmXcPYvSnk">watch</a> || <a href="/videos/2019/11/13/Lets_talk_about_impeachment_being_uncomfortable_and_saving_lives">transcript &amp; editable summary</a>)

GOP senators refuse to research, spreading misinformation and harming marginalized groups, while medical professionals support gender-affirming treatments for children.

</summary>

"I don't care if Tommy plays with Barbie, if that means that Tommy gets to live."
"Imagine if it was about who you are, not just your state and their education."
"Let's get that statistic a little higher, 41%. Those are rookie numbers."

### AI summary (High error rate! Edit errors on video page)

GOP senators are divided on watching the impeachment hearing due to their base not researching.
The GOP operates by latching onto ideas and repeating them, creating a mythical enemy.
Georgia and Kentucky are introducing bills to stop gender-affirming surgeries in children based on misinformation.
Social transitioning for children does not involve surgery but may include puberty blockers in their teens.
The idea of children undergoing surgery for gender affirmation is false.
Some politicians oppose gender-affirming treatments supported by medical associations.
Misinformation leads to scapegoating marginalized groups like children and their parents.
Over 41% of the LGBTQ+ community attempts suicide due to lack of acceptance.
Ignorance and refusal to understand lead to discomfort and immorality judgments.
Medical professionals support gender-affirming treatments, urging the Republican Party to research and understand.

Actions:

for republicans, medical professionals,
Contact GOP senators urging them to research and understand the importance of gender-affirming treatments (suggested).
Support and advocate for gender-affirming treatments for children by spreading accurate information and fighting misinformation (exemplified).
</details>
<details>
<summary>
2019-11-12: Let's talk about Native schools, today's schools, and what we all need to do.... (<a href="https://youtube.com/watch?v=P4uUEOENwp0">watch</a> || <a href="/videos/2019/11/12/Lets_talk_about_Native_schools_today_s_schools_and_what_we_all_need_to_do">transcript &amp; editable summary</a>)

Beau brings attention to the brutal history of Native American schools, the modern-day impact of assimilation in education, and the importance of fostering curiosity to combat uniformity in learning systems.

</summary>

"The schools that were imposed on the natives, they were something else."
"The schools, they kill that individuality, they really do, it's the way they're designed."
"We have to create that curiosity so that they can take that base knowledge that they're getting to the next level."
"You try to impart that lesson. I guarantee you, you're never going to oppose a raise for teachers again."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Native American Heritage Month, discussing the brutal history of native schools imposed on Indigenous peoples, designed to crush their spirit and eradicate their culture.
Schools historically aimed to assimilate Indigenous children, forbidding them from speaking their native tongue, using their real names, or practicing their culture.
The impact of these schools on Indigenous communities has been long-lasting, with repurposed facilities sometimes revealing unmarked graves of children.
Modern-day schools, while not as brutal, still prioritize assimilation and uniformity, stifling curiosity and individuality.
Teachers are often overworked and focused on meeting standardized test requirements, leading to students learning what to think rather than how to think.
As parents and community members, it's vital to supplement the education provided by the state and encourage curiosity and critical thinking in children.
Beau shares an anecdote about encouraging his child's curiosity by exploring topics together, utilizing resources like YouTube to foster learning opportunities.
The education system prioritizes uniformity over individuality, hindering the development of critical thinking skills in students.
Beau stresses the importance of fostering curiosity in children to ensure they can apply their knowledge effectively and think for themselves.
While acknowledging the challenges teachers face, Beau encourages a shift towards promoting critical thinking skills and individuality in education systems.

Actions:

for parents, community members,
Foster curiosity in children through supplementary learning opportunities (exemplified)
Encourage critical thinking skills and individuality in education systems (implied)
Utilize resources like YouTube to support children's interests and curiosity (exemplified)
</details>
<details>
<summary>
2019-11-12: Let's talk about DACA, Stephen Miller, and the future.... (<a href="https://youtube.com/watch?v=bj3dKW_FtFA">watch</a> || <a href="/videos/2019/11/12/Lets_talk_about_DACA_Stephen_Miller_and_the_future">transcript &amp; editable summary</a>)

700,000 Americans are at risk, led by a senior advisor exposed as a racist fascist, while the nation turns a blind eye to illegal actions.

</summary>

"700,000 Americans, known as dreamers, are at risk of having their lives destroyed."
"The country is being led by a senior White House advisor with disturbing emails that expose his true nature."
"We're letting it happen right now."

### AI summary (High error rate! Edit errors on video page)

700,000 Americans, known as dreamers, are at risk of having their lives destroyed due to policies recommended by Stephen Miller.
Steve Miller's emails reveal him as a racist and fascist, yet the President and Republicans continue to support him.
The country is being led by a senior White House advisor with disturbing emails that expose his true nature.
The president solicited a thing of value from a foreign national for his campaign, which is illegal, but his supporters turn a blind eye.
While harmless individuals are at risk, the country is focused on destroying lives for political gain.
The nation's leadership is lacking, and it will require ideologues to step forward to rebuild and heal the country.
There is a need to undo the damage caused by current leadership and work towards healing the nation.

Actions:

for americans,
Protest in support of the 700,000 individuals at risk (implied)
Step forward as ideologues to rebuild and heal the country (implied)
</details>
<details>
<summary>
2019-11-11: Let's talk about Veterans Day (Part 3).... (<a href="https://youtube.com/watch?v=xSgJnZddJaE">watch</a> || <a href="/videos/2019/11/11/Lets_talk_about_Veterans_Day_Part_3">transcript &amp; editable summary</a>)

Beau outlines numerous instances of U.S. military interventions, urging to stop turning veterans into combat veterans and reflecting on the country's history of conflict.

</summary>

"The best thing we can do for Veterans Day is to stop turning veterans into combat veterans."
"This isn't even all of it."
"Maybe go back through this list and try to figure out how many years the U.S. was at peace out of its entire existence."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Outlines various instances where veterans were turned into combat veterans throughout history.
Mentions interventions and wars from early 1900s to present, including conflicts in Nicaragua, Haiti, Dominican Republic, Russian Civil War, World Wars, Korean War, Vietnam War, Bay of Pigs, and more.
Talks about the U.S. involvement in different civil wars and conflicts globally, showcasing a pattern of intervention.
Criticizes the continuous cycle of turning veterans into combat veterans and the impact it has had.
Points out the importance of recognizing and reflecting on the history of U.S. military interventions.
Suggests that the best way to honor Veterans Day is to stop turning veterans into combat veterans.

Actions:

for history enthusiasts, anti-war advocates,
Research and understand the history of U.S. military interventions (suggested)
Advocate for policies that prioritize peace over war (implied)
</details>
<details>
<summary>
2019-11-11: Let's talk about Veterans Day (Part 2).... (<a href="https://youtube.com/watch?v=c_fiqEuwXuE">watch</a> || <a href="/videos/2019/11/11/Lets_talk_about_Veterans_Day_Part_2">transcript &amp; editable summary</a>)

Beau covers America's lesser-known conflicts, exposing a history of constant war and injustice.

</summary>

"We're always at war. We just don't always talk about them."
"Looking back at it through clearer eyes, a lot of this wasn't just."

### AI summary (High error rate! Edit errors on video page)

Veterans Day special covering America's conflicts, not just major ones in history books.
Apache Wars lasted from 1852 to 1924, consisted of constant skirmishes.
US military deployed 5,000 men to deal with Geronimo and his 30 followers.
Bleeding Kansas (1854-186) determined if Kansas was to be a slave state.
Puget Sound War (1855-1856) part of the Akimlu War, notable for a chief's execution.
First Fiji expedition in 1855 due to a civil war incident involving an American businessman.
Rogue River Wars (1855-1856) in Oregon marked another land grab.
Various conflicts like Third Seminole War, Yakima War, Second Opium War, Utah War, and Navajo Wars involved land grabbing.
Second Fiji expedition in 1859 rumored to involve two Americans being eaten by Natives.
Dakota War, Colorado War, Shimonoseki campaign, Snake War, Powder River War, and Red Cloud's War were all land grabs.
The Great Sioux War (1876), Buffalo Hunters War, Pierce War, Bannock War, Cheyenne War, Sheep Eaters War, and White River War were all about seizing land.
Pine Ridge Campaign (1890-1891) led to the Wounded Knee massacre.
Spanish-American War (1898) triggered by the USS Maine incident.
Philippine-American War (1899-1902) followed the Spanish-American War.
Boxer Rebellion (1899-1902) and Crazy Snake Rebellion (1909) occurred due to various reasons such as stolen meat.
Border War (1910-1916) involving Pancho Villa, and the armed uprising of independence of color in Cuba (1912).

Actions:

for history enthusiasts, activists,
Research and learn more about the lesser-known conflicts in American history (suggested)
Advocate for educational reforms to include a comprehensive history curriculum (suggested)
</details>
<details>
<summary>
2019-11-11: Let's talk about Veterans Day (Part 1).... (<a href="https://youtube.com/watch?v=JYvL5cUj5ds">watch</a> || <a href="/videos/2019/11/11/Lets_talk_about_Veterans_Day_Part_1">transcript &amp; editable summary</a>)

Beau introduces a Veterans Day special to shed light on America's lesser-known conflicts throughout history, reflecting on the vast array of wars often excluded from mainstream narratives.

</summary>

"History often overlooks minor conflicts."
"Veterans Day is probably a good day to talk about all of these."
"We don't like why the war started. We don't like the outcome."
"Some of these weren't major, but some of them were huge."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Introducing a Veterans Day special to talk about America's conflicts, including lesser-known ones.
Listing major conflicts such as the Revolutionary War, War of 1812, Mexican-American War, and more.
Noting that history often overlooks minor conflicts like the Cherokee American War, Shays Rebellion, and others.
Explaining each conflict briefly and providing insights, such as tax revolts and internal wars.
Mentioning wars like the Barbary Wars, Creek War, and the Seminole Wars that shaped American history.
Describing events like the Texas Indian Wars involving land grabs and ethnic cleansing over 55 years.
Sharing lesser-known conflicts like the Aegean Sea operations, Winnebago War, and Sumatran expeditions.
Recounting sad events like the Black Hawk War and conflicts like the Pork and Beans War over borders.
Touching on unique expeditions like the Ivory Coast expedition and discussing motivations behind conflicts.
Concluding by reflecting on the vast array of conflicts often excluded from mainstream historical narratives.

Actions:

for history enthusiasts, veterans,
Research and learn about the lesser-known conflicts mentioned by Beau (implied)
Take time to commemorate and acknowledge the sacrifices made in all conflicts, major and minor (implied)
</details>
<details>
<summary>
2019-11-11: Let's talk about Senator Graham's impeachment claim.... (<a href="https://youtube.com/watch?v=jJ5kEXWC96s">watch</a> || <a href="/videos/2019/11/11/Lets_talk_about_Senator_Graham_s_impeachment_claim">transcript &amp; editable summary</a>)

Senator Graham's attempt to bully the House into revealing the whistleblower's identity undermines the Constitution and raises concerns about Senate integrity.

</summary>

"You're trying to bully… Kind of sounds like what the president's getting impeached for, doesn't it?"
"Whistleblowers and people who like to remain anonymous, do so because they don't want to be smeared by an attorney or some politician who has put his party over his country."
"It certainly appears that Senator Graham is willing to undermine the Constitution and the rule of law to protect somebody he knows is guilty."
"Failing to uphold your oath and your duty and attempting to undermine the Constitution through some rhetoric that we all see through, that's going to hurt your re-election chances a whole lot more than simply saying, yeah, Trump broke the law."

### AI summary (High error rate! Edit errors on video page)

Senator Graham threatened that impeachment is dead on arrival if the whistleblower is not named, attempting to bully the House into revealing the whistleblower's identity.
The Senator's demand is baseless, as the House has sole power over impeachment and the Senate's duty is to conduct a trial, not dismiss impeachment without due process.
Beau criticizes Senator Graham's attempt to bully and manipulate the process by demanding the whistleblower's identity, likening it to the behavior that led to the President's impeachment.
He questions the Senator's logic by sarcastically suggesting that Crime Stoppers and anonymous tip lines should be banned if anonymity invalidates due process.
The importance of whistleblowers remaining anonymous is emphasized, as they fear being attacked by individuals who prioritize party loyalty over the country's well-being.
Senator Graham's actions are seen as undermining the Constitution and the rule of law to protect a guilty party, raising concerns about the Senate's trustworthiness and integrity.
Beau accuses the GOP of engaging in corrupt behavior by attempting to overlook wrongdoing in favor of reelection prospects, rather than upholding their oath and duty to uphold the Constitution.
He argues that failing to uphold their oath and attempting to undermine the Constitution will have more significant negative consequences for reelection than acknowledging any illegal actions committed by the President.

Actions:

for politically engaged citizens,
Contact your representatives to express support for protecting whistleblowers and upholding due process (exemplified)
</details>
<details>
<summary>
2019-11-09: Let's talk about why people in history fled the US.... (<a href="https://youtube.com/watch?v=NR0tN1_GaCA">watch</a> || <a href="/videos/2019/11/09/Lets_talk_about_why_people_in_history_fled_the_US">transcript &amp; editable summary</a>)

Beau challenges the romanticized American narrative by exposing historical refugee movements and criticizing the treatment of refugees in the US, urging Canada to reconsider asylum agreements.

</summary>

"We don't like to talk about American history, not the bad parts anyway."
"The U.S. is violating international law as far as the treatment of refugees."
"It's not safe. The amount of abuse that goes on in those detention centers is just unimaginable."
"As we talk about American history, it should be noted that Donald Trump gets to join the ranks of the Civil War and the draft as the few reasons people would flee this country."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Critiques the avoidance of discussing the negative parts of American history, particularly during National American History and Founders Month.
Points out the fallacy of the idea that people in the United States do not flee, citing historical instances like during the Civil War and Vietnam draft.
Describes a period when tens of thousands of people fled to Canada, challenging the narrative of Americans as problem-solvers.
Explains the loophole in the agreement between Canada and the US that allowed refugees to seek asylum in Canada.
Mentions upcoming court proceedings in Toronto led by Amnesty International Canada to challenge the US asylum system.
Argues that the US is violating international law in its treatment of refugees, including family separation and unwarranted detention.
Urges the Canadian government to recognize the flaws in the US asylum system and scrap the agreement due to the abuse in US detention centers.
Draws parallels between Donald Trump's policies and historical reasons for people fleeing the country.

Actions:

for advocates for refugee rights,
Support organizations like Amnesty International Canada in their efforts to challenge the US asylum system (suggested).
Advocate for refugee rights and fair treatment of asylum seekers in your community (implied).
</details>
<details>
<summary>
2019-11-08: Let's talk about NBC helping build a tunnel under the wall.... (<a href="https://youtube.com/watch?v=2YoOoMc4T7w">watch</a> || <a href="/videos/2019/11/08/Lets_talk_about_NBC_helping_build_a_tunnel_under_the_wall">transcript &amp; editable summary</a>)

Two men recruit an engineering student to build a tunnel to freedom, facing challenges and risks while aiding refugees on both sides of the border.

</summary>

"It's amazing how your views on these things are just based on which side of the wall you're on and what decade you're at."
"When Kennedy saw the footage, he cried."
"Defeated that wall."
"This is a good story to tell."
"If you want more information on it, you can find it under Tunnel 29."

### AI summary (High error rate! Edit errors on video page)

Two men approach a 22-year-old engineering student, seeking help to build a tunnel to the other side of the border.
The student, a refugee himself, agrees and they locate a factory to conceal the entrance.
As they dig towards their destination, realizing the need for more help, they recruit additional engineering students.
NBC News funds the tunnel construction after the group faces challenges and realizes the need for better tools.
A burst pipe floods the tunnel, causing a setback, but they manage to fix it without getting caught.
Discovering another tunnel nearby, they offer their assistance and help complete it successfully.
Despite the first tunnel's failure and arrest of others, the engineering student and his friends press on with their mission.
They redirect their tunnel to a different building due to increased border patrols, aiming to help refugees escape safely.
Using coded signals in bars, they coordinate the rescue operation, facing risks and uncertainty on both sides.
Refugees emerge from the tunnel, including a woman, a baby, and many others seeking freedom in West Berlin.

Actions:

for activists for refugee rights,
Support refugee organizations in your community by volunteering or donating (implied)
Advocate for humane immigration policies and support refugees seeking safety (implied)
</details>
<details>
<summary>
2019-11-07: We talked about how to change the world.... (<a href="https://youtube.com/watch?v=2deSJ61eg7o">watch</a> || <a href="/videos/2019/11/07/We_talked_about_how_to_change_the_world">transcript &amp; editable summary</a>)

Beau shares insights on building community networks, stressing the importance of conscious rebellion, mutual support, and self-sufficiency for societal change.

</summary>

"Being independent doesn't actually mean doing everything by yourself, it means having the freedom to choose how it gets done."
"Everybody has something to offer a network like this, everybody."
"If you make your community strong enough, it doesn't matter who gets elected, it doesn't matter who's sitting in the Oval Office."
"Not sure top-down leadership is the answer. Think maybe community building is the answer."
"You know what's best for your community and your area."

### AI summary (High error rate! Edit errors on video page)

Compares today's Western society to George Orwell's 1984, where control mechanisms limit thinking outside the box.
Emphasizes the need to become conscious before rebelling against societal norms.
Shares a personal story of needing help from a network, showcasing the importance of community.
Encourages building a network by evaluating personal skills and offering assistance.
Challenges stereotypes about age and expertise, urging everyone to contribute to a community network.
Suggests using social media to connect and build a digital community for introverts.
Stresses the value of expertise and the importance of not underestimating one's skills.
Recommends recruiting people from immediate circles, social media, and activist communities for a network.
Shares examples of structured networks using poker chips for favors or a commitment to help each other.
Advocates for increasing financial resources by starting low-cost businesses and investing within the network.
Mentions the concept of Stay Behind Organizations during the Cold War as an example of effective community networks.
Proposes that building a strong community can lead to self-sufficiency regardless of political leadership.
Rejects top-down leadership in favor of community building to empower local areas.

Actions:

for community builders, activists.,
Connect with immediate circles, social media, and activist communities to recruit members for a network (suggested).
Start a low-cost business or invest in someone else within the network to increase financial resources (exemplified).
Build a digital community for introverts using social media (implied).
Offer help based on personal skills and expertise to contribute to a community network (suggested).
Participate in community service activities to showcase the network's positive impact (implied).
</details>
<details>
<summary>
2019-11-07: Let's talk about what we can learn from Wounded Knee.... (<a href="https://youtube.com/watch?v=-M9QajRgSHI">watch</a> || <a href="/videos/2019/11/07/Lets_talk_about_what_we_can_learn_from_Wounded_Knee">transcript &amp; editable summary</a>)

General Miles' telegram reveals unfulfilled treaty obligations leading to the Wounded Knee massacre, showcasing American history's mistreatment of Native Americans and serving as a lesson for modern foreign policy.

</summary>

"It's Native American Heritage Month. But their heritage is our heritage."
"Ignorance, arrogance, supremacy, fear of the other. It's what caused it."
"We could learn a lot from our Native American heritage."

### AI summary (High error rate! Edit errors on video page)

General Miles sent a telegram to D.C. stating that the "difficult Indian problem" could not be solved without fulfilling treaty obligations.
Native Americans had signed away valuable portions of reservations to white people without receiving adequate support as promised.
Leading up to the Wounded Knee massacre, the US government continued to seize Native land and ignore treaty agreements.
The Ghost Dance movement, based on a vision of a native Jesus, scared settlers, leading to conflict with authorities.
Sitting Bull was targeted for arrest, leading to a violent confrontation where he was killed.
Natives fleeing reprisals were surrounded by the Seventh Cavalry, resulting in a massacre at Wounded Knee.
The Army awarded 20 Medals of Honor for the massacre, indicating a cover-up to justify the excessive force used.
The Wounded Knee massacre exemplifies ignorance, arrogance, supremacy, and fear of the other in US history and foreign policy.
Beau suggests reflecting on Native American heritage and how it relates to modern-day foreign policy and exploitation.
Native American history serves as a vital lesson on the consequences of colonialism, militarism, and exploitation.

Actions:

for history enthusiasts, activists,
Study Native American history and share the truth with others (suggested)
Support organizations advocating for Native American rights and reparations (exemplified)
</details>
<details>
<summary>
2019-11-07: Let's talk about Washington, Jefferson, and Henry.... (<a href="https://youtube.com/watch?v=lxNOs-J8STg">watch</a> || <a href="/videos/2019/11/07/Lets_talk_about_Washington_Jefferson_and_Henry">transcript &amp; editable summary</a>)

November is National American History and Founders' Month; Beau sheds light on the dark truths behind the popular myths surrounding George Washington, Thomas Jefferson, and Patrick Henry, urging a reevaluation of history and recognition of Native American Heritage Month.

</summary>

"Take some of the mythology out of it."
"Those probably aren't the stories you know about the Founding Fathers."
"You might be surprised."
"There's a lot of mythology."
"Just a thought, y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

November is National American History and Founders' Month proclaimed by Donald J. Trump, which initially sparked mixed emotions but Beau is warming up to the idea.
Beau stresses the importance of understanding American history and the founding fathers to debunk the mythology surrounding them.
George Washington, despite his military leadership, had a dubious past including surrendering due to not knowing French and allegedly committing war crimes.
George Washington's treatment of slaves included brutalities and separating families by selling them to traders from the West Indies.
Thomas Jefferson engaged in a relationship with one of his slaves, likely starting when she was 14 and he was in his 40s, bearing children he raised as slaves.
Patrick Henry, known for "give me liberty or give me death," imprisoned his wife in a cellar for four years, leading to her believed suicide due to postpartum depression.
The popular stories about the founding fathers, like George Washington's wooden teeth, are mostly myths; his teeth were reportedly from slaves.
Beau points out the significant historical inaccuracies and the need to also recognize Native American Heritage Month.

Actions:

for history enthusiasts, truth seekers,
Research and learn about the real stories of the founding fathers (suggested)
Educate others about the dark truths of American history (implied)
</details>
<details>
<summary>
2019-11-06: Let's talk about the future, futility, and freedom.... (<a href="https://youtube.com/watch?v=yjMEC8KTqLc">watch</a> || <a href="/videos/2019/11/06/Lets_talk_about_the_future_futility_and_freedom">transcript &amp; editable summary</a>)

Beau dives into conservatism, freedom, and the pushback against progress, envisioning a future of increasing freedom and equality despite resistance from some.

</summary>

"The future is more freedom for more people."
"That wild and free future cannot be stopped."
"The future is a future where women, racial minorities, gender and sexual minorities are free and equal."

### AI summary (High error rate! Edit errors on video page)

Talks about the essence of conservatism, freedom, and futility, reflecting on the scary future and the theme of the universe being wild and free.
Describes how the future should bring more freedom for more people, encouraging innovation and letting go of old ideas to become better.
Compares the progress of freedom to waves on a beach, with each wave reaching a bit further ashore.
Mentions how some people find the advancing freedom scary as it challenges their beliefs, leading them to reach back to preserve dated ideas.
Shares the story of Americana City, where people moved to Brazil to preserve their culture but ultimately assimilated into the new society.
Examines the Make America Great Again voter demographic, born in 1966 or before, seeking clarity and purpose in a changing world.
Points out how the older generation's upbringing during segregation influences their views, even if they don't overtly hold those ideas anymore.
Talks about the fear of the unknown and reluctance to change among certain groups, contrasting them with the younger, more progressive minds joining the electorate.
Emphasizes the inevitable progression towards a future where all individuals are free and equal, predicting the fading of old ideas and institutions.
Concludes by acknowledging the unstoppable nature of progress towards a more inclusive and equitable society, despite resistance from some individuals.

Actions:

for progressive individuals,
Connect with younger individuals to encourage progressive thinking and inclusivity (implied)
Challenge outdated ideas within communities and advocate for equality (implied)
</details>
<details>
<summary>
2019-11-05: Let's talk about the 5th of November.... (<a href="https://youtube.com/watch?v=9F2PzR6o0_g">watch</a> || <a href="/videos/2019/11/05/Lets_talk_about_the_5th_of_November">transcript &amp; editable summary</a>)

Beau dives into the history of the 5th of November, the Gunpowder Plot, and how symbols like Guy Fawkes have evolved into broader symbols of resistance, resonating for unknown reasons.

</summary>

"Probably not."
"But at some point, Guy Fawkes and that mask became more than history, became something else, became a symbol, a symbol of resistance."
"These slogans, these symbols, these ideas, they resonate for whatever reason."
"It seems funny that the reality is, for Americans, Guy Fawkes Day is not about Guy Fawkes."
"It's about a comic character and somebody uniting the faceless masses against an oppressive government."

### AI summary (High error rate! Edit errors on video page)

Explains the history of the 5th of November and the Gunpowder Plot.
Queen Elizabeth I's conflicts with the Catholic Church and James I's actions.
Details the plot by Guy Fawkes and Robert Catesby to blow up Parliament.
Mentions the failed attempt due to an anonymous letter and the subsequent capture of the plotters.
Talks about how the modern image of Guy Fawkes comes from the comic book and movie "V for Vendetta."
Examines how symbols like Guy Fawkes have transformed into broader symbols of resistance.
Raises the point about symbols throughout history that resonate without full understanding.
Beau muses on the evolution of historical symbols and their meanings in modern times.

Actions:

for history enthusiasts,
Research and learn more about historical events like the Gunpowder Plot and their significance (suggested).
Engage in dialogues about the transformation of historical symbols into modern-day representations (exemplified).
</details>
<details>
<summary>
2019-11-05: Let's talk about science, art, lasers, and waste.... (<a href="https://youtube.com/watch?v=_5ZVqFDXKoU">watch</a> || <a href="/videos/2019/11/05/Lets_talk_about_science_art_lasers_and_waste">transcript &amp; editable summary</a>)

Beau dives into nuclear waste management, warning messages, and a physicist's laser innovation, showcasing human ingenuity and the need for resource allocation in solving global challenges.

</summary>

"This place is a message and part of a system of messages."
"The danger is unleashed only if you substantially disturb this place physically."
"Understand human ingenuity is amazing and we can come up with some pretty brilliant stuff given the resources."
"An idea like this is out there for every problem that faces humanity."
"Sometimes we might should research just to research."

### AI summary (High error rate! Edit errors on video page)

Exploring the potential of nuclear power and its waste management.
Long-term storage of nuclear waste being a major issue.
The challenge of conveying a warning message about nuclear waste for 10,000 years.
Various creative ideas proposed to deter future generations from accessing the waste site.
The message to future generations about the danger of the nuclear waste.
A physicist's innovative idea to use lasers to alter nuclear waste drastically.
The potential impact of laser technology on reducing nuclear waste's danger.
The importance of human ingenuity in solving global problems.
The necessity of allocating resources towards creative solutions rather than destruction.
The role of funding and resources in supporting groundbreaking ideas.

Actions:

for innovators, policymakers, environmentalists.,
Support innovative solutions for nuclear waste management (suggested).
Advocate for proper funding for creative problem-solving initiatives (implied).
</details>
<details>
<summary>
2019-11-05: Let's talk about Canada's water.... (<a href="https://youtube.com/watch?v=2DbS-41Rgt4">watch</a> || <a href="/videos/2019/11/05/Lets_talk_about_Canada_s_water">transcript &amp; editable summary</a>)

Canadian water crisis reveals widespread lead contamination worse than Flint, urging citizens to hold officials accountable and prioritize fixing infrastructure for clean water.

</summary>

"There is no safe level of lead. It's all bad."
"If a government cannot provide the most basic of services, water. If a jurisdiction can't provide that, if a government can't provide that, what good is it?"
"I personally rather pay to have all of the pipes in Flint fixed rather than send some more bombs overseas."

### AI summary (High error rate! Edit errors on video page)

Canadian journalism students and others conducted a massive investigation revealing a national water crisis worse than Flint, with hundreds of thousands unknowingly drinking contaminated water with high lead levels.
Lead levels in several Canadian cities were consistently higher than Flint's, with 33% of tested water sources exceeding the Canadian guideline of 5 parts per billion.
There is no safe level of lead, as even a single glass of highly contaminated water can lead to hospitalization, especially harmful to children by lowering IQ, causing focus problems, and damaging the brain and kidneys.
Schools in Canada are not regularly tested for lead, and even if high levels are found, there is no obligation to inform anyone.
Approximately half a million service lines in Canada are lead, affecting a significant number of people due to lead pipes corroding and contaminating the water.
Aging infrastructure in both Canada and the United States is a key issue, necessitating costly repairs to prevent further lead contamination.
Some Canadian jurisdictions like Quebec are taking steps to address the crisis, with plans to replace both public and private lead pipes.
Beau questions the priorities of developed countries like Canada and the U.S., urging citizens to care about how government funds are allocated to prevent such crises.
Beau advocates for prioritizing fixing infrastructure issues like lead pipes over military spending, questioning the government's ability to provide basic services like clean water.

Actions:

for canadians, americans,
Hold local officials accountable for ensuring clean water (implied)
Advocate for regular testing of schools for lead contamination (implied)
Support initiatives to replace lead pipes in affected areas (implied)
</details>
<details>
<summary>
2019-11-04: Let's talk about ethically using resources.... (<a href="https://youtube.com/watch?v=OUu0VVLHYjI">watch</a> || <a href="/videos/2019/11/04/Lets_talk_about_ethically_using_resources">transcript &amp; editable summary</a>)

Beau advises using the opposition's resources strategically when behind enemy lines in altering society, focusing on benefiting people over establishments.

</summary>

"When you're talking about altering society, we're behind enemy lines."
"There are times when using the opposition's resources is your only option."
"You have no ethical obligation to correct that mistake for them."
"You just have to make sure that the benefit to the people outweighs the benefit to the establishment."
"As long as the money isn't affecting the content that you're putting out, as long as it's not going to change your speaker list, as long as it's not going to alter what they're going to talk about, you know..."

### AI summary (High error rate! Edit errors on video page)

Received a question from a person attending a university heavily funded by a nearby corporation.
The person opposes the corporation but was given the chance to organize speaking engagements funded by them.
Advises that as long as the funding doesn't affect the content or speakers, taking the money is acceptable.
Mentions how the Department of Defense teaches soldiers to use resources left behind by the opposition.
Argues that using the corporation's resources, even if it seems hypocritical, can be necessary in certain situations.
States that altering society is like being behind enemy lines, and using the opposition's resources can be strategic.
Emphasizes that taking the corporation's money for speakers who will speak out against them is not ethically wrong.
Points out that large corporations fund events to generate goodwill and that drawing attention to their practices can be a win.
Notes that in a society dominated by big corporations, it's challenging to completely avoid their influence.
Concludes with the idea that ethical consumption is rare, but the focus should be on ensuring benefits to people outweigh those to the establishment.

Actions:

for university organizers,
Organize speaking engagements funded by corporations (suggested)
Draw attention to questionable business practices of large companies (implied)
</details>
<details>
<summary>
2019-11-04: Let's talk about a camping story from the 1950s and what it can teach us.... (<a href="https://youtube.com/watch?v=4dbRGaPph-w">watch</a> || <a href="/videos/2019/11/04/Lets_talk_about_a_camping_story_from_the_1950s_and_what_it_can_teach_us">transcript &amp; editable summary</a>)

In 1950s camping experiment, shared struggles united warring boy scout groups, showing how common challenges can dissolve divisions and foster unity.

</summary>

"When the struggle comes, we will unite."
"Those petty divisions disappear. Skin tone, religion, borders, none of it matters because we'd all be united."
"We have the struggles. We just need somebody to leave the rope, set things in motion, get us to unite."
"The insults stopped. The grudges, well, they're gone. They became friends."
"It's been repeated pretty often."

### AI summary (High error rate! Edit errors on video page)

In 1954, two groups of 11 boy scouts who had never met before went camping at Robbers Cove State Park in Oklahoma.
The boys quickly formed bonds and a social order, but things took a competitive turn when they found out about another group nearby.
The competition escalated with sporting events, team challenges, and the winning team was promised medals and pocket knives.
Name-calling, stealing flags, and animosity grew between the two groups, known as the Rattlers and the Eagles.
The losing team stole the winning team's medals and pocket knives after the tournament, leading to continued grudges and anger.
When the camp lost water, all the boys worked together to fix the pipes and even collaborated to pull a stuck pickup truck using a rope from a tug of war.
The act of pulling together to solve a common problem led to the insults and grudges disappearing, transforming enemies into friends.
Psychologists intentionally set up this experiment with similar groups to observe how shared struggles could unite people.
A similar study in Beirut in 1963 with mixed Muslim and Christian teams did not go as planned, showing that shared struggles might not always overcome deep divisions.
Beau suggests that when faced with a common struggle like a stuck truck or lack of water, people can unite despite their differences, transcending barriers like religion, skin tone, and borders.

Actions:

for community members,
Organize community events that involve collaboration and teamwork (implied)
Encourage diverse groups to work together on common goals or projects (implied)
Support initiatives that address shared struggles like poverty, hunger, or climate change (implied)
</details>
<details>
<summary>
2019-11-03: Let's talk with Re-Education about journeys and lefties.... (<a href="https://youtube.com/watch?v=D-pHZIEvfpc">watch</a> || <a href="/videos/2019/11/03/Lets_talk_with_Re-Education_about_journeys_and_lefties">transcript &amp; editable summary</a>)

Aaron's journey from a conservative with racist influences to an anarcho-communist advocate, criticizing misrepresentation and advocating for direct action.

</summary>

"Everybody is different, and they're all going to come to their positions in a different way."
"It's about working people against the system that is corrupted and pushing down on all of us."
"News crews have entire groups of people that deal with editing, they deal with sound, they deal with all of these sorts of things."
"A lot of the military back coups done by the United States or done by any other major superpower always involves a certain amount of people on the ground that are actually angry and that actually have real concerns."
"Putting physical bodies in physical spaces is the only way that we're going to actually see any sort of real reform, any sort of real change in the world."

### AI summary (High error rate! Edit errors on video page)

Aaron talks about his journey from being a conservative-leaning person to eventually becoming an anarcho-communist.
He shares how his upbringing in a community with racist influences led him to adopt harmful beliefs and associate with neo-Nazis.
Aaron recounts incidents of violence and vandalism he witnessed and participated in during his past.
He describes his transition to rejecting his previous beliefs and embracing a more inclusive and empathetic worldview.
Aaron credits skepticism and exposure to alternative arguments for helping him change his perspective.
He explains his views on communism, socialism, and anarcho-communism, and distinguishes between them.
Aaron criticizes the misrepresentation of socialism and communism in the United States.
He provides insights into the differences between communism, socialism, social democracy, and democratic socialism.
Aaron shares his support for worker-owned co-ops as a practical solution for societal issues under capitalism.
He expresses the challenges of discussing complex global issues like protests in China due to propaganda and lack of in-depth knowledge.

Actions:

for activists, advocates,
Join protests and demonstrations to physically show support for social change (implied).
Support worker-owned co-ops as a practical solution for societal issues under capitalism (exemplified).
</details>
<details>
<summary>
2019-11-03: Let's talk about solutions to complex problems and slogans.... (<a href="https://youtube.com/watch?v=fDGBH42GibY">watch</a> || <a href="/videos/2019/11/03/Lets_talk_about_solutions_to_complex_problems_and_slogans">transcript &amp; editable summary</a>)

Beau addresses the preference for simple solutions over complex issues, calling for critical thinking and deeper examination of root causes.

</summary>

"Soliciting a thing of value from a foreign national is illegal, period, full stop."
"The only reason they'd do that is if it had value."
"The average American is not dumb."
"We'd rather cheerlead than think."
"If you understand history, you can understand the future."

### AI summary (High error rate! Edit errors on video page)

Americans prefer simple solutions for complex problems, but real issues require thought, planning, and multiple tactics.
Trump exploited perceived ignorance by proposing a wall as a simplistic solution to a complex issue.
The wall's failure resulted in wasted money, disrupted lives, and damaged America's image.
Pelosi's fear stems from underestimating Americans' ability to understand complex issues, especially regarding campaign finance laws.
Soliciting anything of value from a foreign national for a campaign is illegal, regardless of the specifics.
Americans are capable of understanding complex issues but often choose to defer to authority rather than think critically.
Beau calls for a deeper examination of history and policies to address root causes instead of relying on simple solutions.

Actions:

for american citizens,
Challenge yourself and others to think critically about complex issues (implied)
Educate yourself on history and policies to understand root causes (implied)
</details>
<details>
<summary>
2019-11-03: Let's talk about pronouns and grammar.... (<a href="https://youtube.com/watch?v=uOmj5Bva0YA">watch</a> || <a href="/videos/2019/11/03/Lets_talk_about_pronouns_and_grammar">transcript &amp; editable summary</a>)

Beau dives into the history of the English language and questions the necessity of rewriting it for one person's preference, advocating for the historically used gender-neutral pronoun "they."

</summary>

"We wouldn't really be able to read it today unless we were trained to do so."
"For the overwhelming majority of the history of the English
language, we had a gender neutral pronoun, a singular gender neutral pronoun, they."
"It's not actually rewriting the English langauge to suit one person's personal preference."
"That's how it happened. So they, them, theirs, that's actually the norm."
"I guess we can rewrite the entire langauge because of one person's preference."

### AI summary (High error rate! Edit errors on video page)

Explains the history of the English language, starting with Old English in 450 to 1100 when Germanic people mixed with locals.
Describes the transition to Middle English from 1100 to 1500, influenced by French brought over by William the Conqueror.
Talks about early modern English from 1500 to 1800, with shorter vowels and more foreign words due to colonization.
Mentions late modern English from 1800 to now, with a wider vocabulary from the industrial and technological revolutions.
Details the development of prescriptive grammar in the 1700s, which dictated proper speech through books like Rob Lough's on English grammar.
Explains how Sir Charles Coutts' expansion on Lough's work included using "he" and "him" as generic pronouns, later ratified by Parliament in 1850.
Points out that historically, the English language had a singular gender-neutral pronoun, "they," for the majority of its history.
Criticizes the idea of rewriting the entire English language for one person's preferences, as it historically happened through influential figures like Coutts.
Ends by questioning the necessity of changing the entire English language for personal preferences.

Actions:

for english speakers,
Advocate for the use of gender-neutral pronouns like "they" in everyday speech (exemplified)
</details>
<details>
<summary>
2019-11-02: Let's talk about another question I wish wasn't being asked.... (<a href="https://youtube.com/watch?v=7S5dUoJXcmY">watch</a> || <a href="/videos/2019/11/02/Lets_talk_about_another_question_I_wish_wasn_t_being_asked">transcript &amp; editable summary</a>)

Beau addresses the rise in questions about arming oneself due to escalating anti-trans sentiment and provides critical advice on firearm ownership.

</summary>

"If you're dumb enough to pull a gun, you better be smart enough to pull the trigger."
"You're talking about purchasing a firearm for the express purpose of killing someone."
"If you're willing to put in the effort to learn how to do that, It's a good idea."
"When you learn how to use one to win, there's a new respect that comes along with it for the tool."
"I think we'd see less violence if people understood how to employ it better."

### AI summary (High error rate! Edit errors on video page)

Addressing the rise in questions about arming oneself due to increasing anti-trans sentiment, especially since the last presidential election.
Expressing feeling unsafe and contemplating buying a gun despite not being a "gun person" previously.
Mentioning the ease of purchasing a gun in their state and considering taking a safety class to handle it responsibly.
Responding to the question of whether LGBT individuals should arm themselves, stating it's not more likely to escalate violence against them.
Advising taking multiple safety courses until feeling extremely comfortable with handling a firearm.
Emphasizing the seriousness of owning a gun for the purpose of self-defense and the responsibility that comes with it.
Sharing advice on being certain of the intent before ever drawing a weapon in a potentially confrontational situation.
Noting the importance of understanding the tool and learning how to employ it effectively in combat situations.
Encouraging individuals to only purchase a firearm if they are willing to put in the effort to learn how to use it properly.
Mentioning the lack of understanding among many gun owners on how to use firearms beyond paper target shooting, stressing the need for training to reduce potential violence.

Actions:

for lgbt community,
Take multiple safety courses to handle firearms responsibly (suggested).
Put in the effort to learn how to use a firearm effectively in combat situations (suggested).
Understand the responsibility and seriousness that come with owning a gun for self-defense (implied).
</details>
<details>
<summary>
2019-11-02: Let's talk about Trump, Obama, gun control, and Florida man.... (<a href="https://youtube.com/watch?v=wbSOB-tFdXc">watch</a> || <a href="/videos/2019/11/02/Lets_talk_about_Trump_Obama_gun_control_and_Florida_man">transcript &amp; editable summary</a>)

Debunks the myth that Trump is a Second Amendment defender, contrasting his actions with Obama's and revealing surprising truths about gun control.

</summary>

"President Obama expanded gun rights. He wasn't a gun grabber."
"Trump, on the other hand, well, take the guns first. Worry about due process second."

### AI summary (High error rate! Edit errors on video page)

Debunks the widely held belief among older Republicans that Donald Trump is a defender of the Second Amendment.
Compares Trump's gun control actions to Obama's, revealing surprising facts.
Trump supported banning assault weapons, longer waiting periods, banning suppressors, universal background checks, and increased federal firearm prosecutions.
Trump set a record for federal firearm prosecutions, surpassing Obama and even Bush.
Obama received an F rating from the Brady campaign, indicating his stance on gun control.
Trump continued and expanded upon Obama's actions, releasing military surplus weapons and allowing guns in national parks.
Contrasts the perception of Obama as a gun grabber with his actual actions in expanding gun rights.
Criticizes Trump's stance on guns, quoting his willingness to "take the guns first, worry about due process second."
Draws parallels between Trump's approach to guns and his views on impeachment.
Concludes by challenging the perception of Trump as a defender of gun rights.

Actions:

for second amendment supporters,
Share this information with fellow Republicans to challenge misconceptions about Trump's stance on the Second Amendment. (implied)
</details>
<details>
<summary>
2019-11-01: Let's talk about NASA's new space travel plans.... (<a href="https://youtube.com/watch?v=DRgFRink3YE">watch</a> || <a href="/videos/2019/11/01/Lets_talk_about_NASA_s_new_space_travel_plans">transcript &amp; editable summary</a>)

NASA plans space exploration while a billion go hungry; Beau questions food distribution priorities.

</summary>

"We're on the cusp of a new adventure in humanity, conquering the stars."
"We produce 150% of what it would take to feed everybody."
"NASA can plan to put a crew of four on the moon for two weeks and plan to launch an interstellar probe, the rest of us, we can figure out how to move some food."

### AI summary (High error rate! Edit errors on video page)

NASA plans to put a crew of four on the moon for two weeks and launch an intentional interstellar space probe.
Despite producing 150% of the food needed to feed everyone, a billion people go hungry nightly due to food distribution issues.
Beau challenges the notion that there are logistical barriers to distributing food when transportation systems are already in place.
He questions the lack of incentive to distribute food efficiently, pointing out the benefits of stability and reduced defense spending.
Beau advocates for prioritizing basic needs like food over space exploration, suggesting it's a moral and monetary imperative.
He stresses the importance of addressing hunger beyond America's borders and treating all people equally.

Actions:

for global citizens,
Coordinate transportation networks to distribute excess food efficiently (implied)
Advocate for stable governments through food aid to reduce conflict and defense spending (implied)
</details>
