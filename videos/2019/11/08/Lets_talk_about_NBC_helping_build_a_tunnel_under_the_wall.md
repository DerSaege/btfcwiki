---
title: Let's talk about NBC helping build a tunnel under the wall....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2YoOoMc4T7w) |
| Published | 2019/11/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Two men approach a 22-year-old engineering student, seeking help to build a tunnel to the other side of the border.
- The student, a refugee himself, agrees and they locate a factory to conceal the entrance.
- As they dig towards their destination, realizing the need for more help, they recruit additional engineering students.
- NBC News funds the tunnel construction after the group faces challenges and realizes the need for better tools.
- A burst pipe floods the tunnel, causing a setback, but they manage to fix it without getting caught.
- Discovering another tunnel nearby, they offer their assistance and help complete it successfully.
- Despite the first tunnel's failure and arrest of others, the engineering student and his friends press on with their mission.
- They redirect their tunnel to a different building due to increased border patrols, aiming to help refugees escape safely.
- Using coded signals in bars, they coordinate the rescue operation, facing risks and uncertainty on both sides.
- Refugees emerge from the tunnel, including a woman, a baby, and many others seeking freedom in West Berlin.

### Quotes

- "It's amazing how your views on these things are just based on which side of the wall you're on and what decade you're at."
- "When Kennedy saw the footage, he cried."
- "Defeated that wall."
- "This is a good story to tell."
- "If you want more information on it, you can find it under Tunnel 29."

### Oneliner

Two men recruit an engineering student to build a tunnel to freedom, facing challenges and risks while aiding refugees on both sides of the border.

### Audience

Activists for refugee rights

### On-the-ground actions from transcript

- Support refugee organizations in your community by volunteering or donating (implied)
- Advocate for humane immigration policies and support refugees seeking safety (implied)

### Whats missing in summary

The emotional impact of the refugees' journey towards freedom and the resilience shown in the face of adversity.

### Tags

#RefugeeRights #Tunnel29 #Freedom #Resilience #CommunityAid


## Transcript
Well, howdy there Internet people, it's Beau again.
So tonight we've got a weird story, but it's definitely interesting.
Our story starts off with two guys approaching a 22-year-old engineering student asking for
help.
They're like, hey, we've got friends on the other side of the border.
We want to build a tunnel, we need your help.
student, he's a refugee himself. So he says, okay. And they start looking and they find
a factory. And inside that factory they can put the entrance to their tunnel. Nobody will
see it. It's perfect. They start digging. It doesn't take them long to realize they
need help. So they recruit a couple more engineering students. And they've been digging for weeks
now trying to get to a house on the other side of the border that belongs to one of
their friends, they're just going to pop up in the middle of it.
He doesn't know they're doing this by the way.
And weeks go by, they still haven't even made it to the border, they realize they need better
tools, they need more diggers.
short, they need money. NBC News decides to pay for the tools, decides to fund
building this tunnel. Can you believe that? Once they get their tools, things go a
little bit smoother. They get going. They're about 150 feet away from where
they want to be when disaster strikes. A pipe burst floods the tunnel. So, them
being young and reckless, they just called the utilities and said, hey you got a bus
pipe and they came out and fixed it, they didn't get caught.
But the tunnel was already flooded, the tunnel was already flooded, so they had to wait for
it to dry out.
As they're sitting there waiting, sitting on their hands, they find out there's another
tunnel being dug right up the road, so they offer to help.
They got pretty much the same plan, and they're going to pop up in somebody's house.
The other tunnel was not built by an engineering student, so it needed some help to begin with.
But they get it working, and they get the tunnel built all the way over.
To let everybody know they sent somebody across the border, to let the refugees know, we
got to wait for you to get out.
The thing is, the person they sent was an informant.
Everybody on the other side got busted.
But that did not deter our engineering student and his friends, because that other tunnel
will let it dried out now.
So they went back to work there.
And rather than aim for their friend's house, they're just going to go to the nearest place,
nearest building and just pop up in it and get everybody out that they can and
they've got to be real careful though because there's a lot of patrols border
patrol running up and down that area so they get it dug and they send a friend
across the border to let the people know give them the signal that it's time to
move. She went into these different bars, you know, different signals in different bars to let
people know what was going on. I guess the one she bought, I think a matchbook maybe, that sounds
weird, something like that. And another one she was going to get a cup of coffee, but they were out
of coffee. So in order to make sure everybody knows what's going on and got the signal, she starts
like throwing a fit because they didn't have her coffee. Asked to speak to the regional manager and
everything. Now while all this is going on, the people that had dug the tunnels, they
popped up in this building and they're standing there and after what happened
last time they brought guns and they're standing there waiting and NBC had no
danger to their selves of course they're they're back on the other side at the
They exit to the tunnel now, filming this empty hole, and they're filming and filming
and waiting, having no idea what's going on on the other side, the risk these people
are under.
And all of a sudden, a woman pops out, covered in mud, then a baby, and refugees pour out
of this hole for an hour.
It said that when Kennedy saw the footage, he cried.
He cried as those people made it to safety in West Berlin, made it to freedom, defeated
that wall.
So the President of the United States declared this month American History and Founders Month.
They're like, this is a good story to tell.
If you want more information on it, you can find it under Tunnel 29.
BBC just did a special on it.
It's amazing how your views on these things are just based on which side of the wall you're
on and what decade you're at.
How much that decade actually values freedom.
Anyway it's just a thought.
have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}