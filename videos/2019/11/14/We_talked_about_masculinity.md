---
title: We talked about masculinity....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=PflmaMpuGEc) |
| Published | 2019/11/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the ridiculousness of guides designed to help young American males find their masculinity.
- Shares a personal story about a Japanese concept called Shibumi explained by an old Japanese man.
- Describes masculinity as subtle, understated, in control of nature, and effortless perfection.
- Asserts that masculinity cannot be quantified or charted, and each person's masculinity is unique.
- Emphasizes masculinity as a journey of introspection and self-improvement, not a checklist or conforming to societal norms.
- Criticizes toxic masculinity and its origins in glorified violence and immature behavior.
- Challenges the idea of toxic masculinity glorification by some men.
- Counters arguments against feminism and patriarchy, pointing out the need for equal representation and the existence of patriarchal systems.
- Advocates for respecting women's autonomy and choices, regardless of personal opinions or insecurities.

### Quotes

- "Masculinity is something you're going to define for yourself."
- "The fact that you don't find her attractive doesn't matter. What you think doesn't matter."
- "All women have to be two things and that's it. Who and what they want."

### Oneliner

Beau talks about the fluidity of masculinity, the concept of Shibumi, and the importance of defining masculinity for oneself while challenging toxic masculinity and advocating for women's autonomy.

### Audience

Men, feminists, advocates

### On-the-ground actions from transcript

- Challenge toxic masculinity by uplifting others and focusing on self-improvement (implied).
- Advocate for equal representation of women in positions of power (implied).
- Respect women's autonomy and choices, regardless of personal opinions (implied).

### Whats missing in summary

The full transcript provides a deep exploration of masculinity, toxic masculinity, feminism, and patriarchy, encouraging introspection and challenging societal norms regarding gender roles.

### Tags

#Masculinity #ToxicMasculinity #Feminism #Patriarchy #SelfImprovement


## Transcript
So all over the internet there are charts and books for sale and guides and youtube videos
designed to help the young American male become a man and find his masculinity.
Most are utterly ridiculous.
Rather than talking about those, we're going to talk about Japanese art and a Japanese concept.
Years ago, seems like a lifetime ago, I drew a rotation working in what amounts to a call
center that you hope the phone never rings.
Basically two guys sitting in a room staring at each other for hours on end.
As luck would have it, my turn of doing this for a month, I got the overnight shift.
Did it with this old Japanese guy.
And I mean old.
When we were off, we hung out with each other.
There weren't a lot of friendly people in the area.
So we did what a lot of men do.
We drank way too much, constantly.
Six, seven hours at a time playing chess and just getting hammered.
One night, we're drinking sake and he picks up the cup in front of him and he is just
out of his mind drunk and he starts rambling about
how perfect it is while pointing out all of the imperfections in it.
And then he grabs my cup and my cup's perfect too
but it has different imperfections. They're perfect because they don't conform
even though he could tell they were made by the same artist.
And he goes on to tell me that it's a Japanese concept called Shibumi.
And this term, this idea doesn't have a definition in Japanese.
It embodies characteristics, but it's a fluid definition.
And after telling me there is no definition for it,
he goes on to try to define it.
I want you to picture a very drunk old Japanese guy trying
to define a Japanese term that does not
have a definition in Japanese to an American that has no clue what he's
talking about. He says that it's subtle, understated, it's a part of nature but
separate from it and in control of it, exercising control over it. And then he
says the phrase that sticks in my mind. He says it's perfection without effort.
effortless perfection. Now that is a hard idea to get your head around. The second you try, you fail.
Subtle, understated, a part of nature and accepting of nature, but separate from it and in control of it,
and effortless. That's masculinity. Maybe masculinity can't be quantified the way
people try. Maybe it can't be charted. Yeah, there are natural characteristics that
you're going to embrace, but you're going to maintain control over them and you're
to be separate from it. And it's going to be effortless. It's going to be effortless
because you're not going to try to force yourself to fit some mold. It's going to be perfect
because you're not going to conform to a chart. Maybe your masculinity is going to be different
than mine. Masculinity is not a team sport. It requires a lot of introspection. It requires
knowing yourself. It's not a checklist. There is no guidebook for it. It's you. The constants
are that you're not trying to use your natural aggressiveness to subjugate others. You're
to lift them up. You know, today a lot of those who talk about masculinity, they
want to keep others down. Real masculinity doesn't care because real
masculinity likes a challenge. You know, if you help them get up and they're
better than you, well that just means you have to get better. And that's what it's
It's not about subjugating other people, it's about improving yourself.
Masculinity isn't a trait, it's a journey.
When you see somebody trying to be masculine, you see somebody trying to be a tough guy,
what happens?
You immediately know they're not, because it doesn't fit, it's not who they are.
If they were that guy, they wouldn't be trying, it would just be natural.
It would be effortless, subtle, understated, a part of nature and accepting of nature,
but separate from it and in control of it, and effortless.
That's masculinity.
Masculinity is something you're going to define for yourself.
So maybe instead of trying to look for role models to mimic and try to force yourself
to become like them, maybe the secret for the young American male in search of masculinity
is to accept what it is in you.
So I'm going to tell you three masculine role models
that I have.
The first, we started his career off as a police chief.
And he used to go undercover, dress like a poor citizen,
and try to catch his cops doing something wrong,
being corrupt, taking a bribe, taking advantage of people, whatever.
And if he did, well, he'd beat him up and fire him.
One legend says he beat the guy up, stripped him naked, threw him in a dumpster,
and then fired him.
Later on, he was working in the secretary of the Navy's office.
And he kind of started a war.
But rather than sitting in the comfort and safety of that office,
he went and fought on the front lines.
got the Medal of Honor for it later.
He was going to give a speech, would be assassin, walked up, shot him in the chest.
He gave the speech, bullet in his chest.
He was a huge conservationist, an explorer, he was a feminist.
When he was president, he had a boxing ring installed at the White House, was almost blinded
in a fight.
He was the first president to invite a black man to come have dinner at the White House.
Won the Nobel Peace Prize back when it actually mattered.
That was Teddy Roosevelt.
Definitely the pinnacle of American masculinity when it comes to the individualism and the
desire to uplift those who are downtrodden and the willingness to put his own rear on
the line when it mattered.
Another guy looked up to, a little bit different.
He was a sergeant during World War II in France and Africa.
After the war, he came back to his little small town.
His wife died.
He became sheriff, spent most of his time looking after his boy.
He was unique because Hollywood was going to make a movie about him.
They're going to call the movie The Sheriff Without a Gun or something along those lines
because he didn't carry one.
He had one in his car, but he didn't walk around with one.
He was very active in his community, and he tried to solve problems by humanizing the
two opposing parties in each other's eyes, using some very unique conflict resolution.
That was Sheriff Andy Taylor from the Andy Griffith Show.
What you would think of as a tough guy, but definitely a masculine figure.
That wholesome masculine.
Now on the other end of things, John F. Kennedy, always look up to him, same principles though.
He did put his rear on the line, but at the same time he was trying to uplift as president.
He was trying to free the oppressed.
took a lot
of vision
for some of the things that he proposed.
We're going to talk about toxic masculinity a little bit.
That didn't come from feminism, guys.
It came from the first MRAs.
That's where the term originated.
Toxic
is not an adjective,
it's a modifier.
It was a way for them to distinguish between
what they referred to as hyper-masculine, immature behavior, okay, and the deep masculine,
the stuff that mattered, didn't come from feminism.
And they believed that that toxic masculinity was the extremes.
The extremes, you know, the need to dominate everything.
people right now are going, well, no, masculinity means assertiveness and aggressiveness with cause,
reason. That reason's been lost. But it certainly seems like men have wanted to hold on
to the violence and to that dominating spirit. But they lost all the ideals that went with it.
They wanted to hang on to it because it's glorified. It had to be glorified. The system
had to glorify it. Otherwise, stupid men wouldn't run off to go to war. Nobody wants to go die.
But if it's glorified, well then, you know, you got a reason to do it.
And that glorification of violence has led to men wanting to use violence for everything,
rather than men becoming hyper-masculine or feminized.
Both happened.
happened. All of these men want to cry and want somebody else to take care of
them while they pretend that they're this aggressive dominant self-reliant
person. Men are more violent. That's that's fact. Statistically proven it's
That's fact, men are more violent.
And that, that violence is the reason that toxic masculinity has to be put in check.
You know, you've got the make me a sandwich, do my laundry, clean my house meme.
And then you have the feminism makes you ugly meme.
Okay guys, if you are so shallow that you're going to marry a woman because she can cook,
I would hope that your palate is a little bit more refined than simply
needing a BLT.
You're setting the bar pretty low.
I mean, I would hope that you would need a little bit more
skill in that department. But, anyway,
that's a beside the point. As far as
cooking, cleaning, doing your laundry, you probably already had a woman in your life
that did that for you, and that was your mommy.
See, my four-year-old can make his own sandwich.
It may be that the reason you have a problem with adult women has nothing to do with feminism
and it has to do with the fact that you're still a little boy.
That would be my guess.
But let's go on.
The feminism makes you ugly meme.
Now, if you haven't seen one of these, what it is is they find a feminist that they don't
find attractive and then they cyber stalk her. They go back through and they
find her senior portrait from high school which you know she looks all
wholesome. The photo was taken when she was like 17. Go ahead and marinate on
that for a second. Their ideal of attractiveness is a teenager. Maybe
you're still a little boy. And the after photo is of course one of her in college
where she's probably put on the freshman 15, like most do. Maybe she dyed her hair
a wild color, got a piercing, whatever. So what's the point of the meme? Don't
listen to this woman because of how she looks. The objectification of women, guys,
that is not, that's not an argument against feminism. That's why feminism
exists. That's one of the key issues. And then there's always the added
thing of, you know, the caption says something like, don't let this happen to
your daughter. See in that after photo, she's an adult. She is an adult, yet she's
still property of some other man because she's not married yet, right?
So she still belongs to her dad. Still need that parental figure. Maybe you're a
little boy. Then it goes on, there are other comments about male feminists and
how they're all soft and they are soy boys. Okay, where's your Medal of Honor
at Mr. Meme Maker? Teddy Roosevelt is pretty much the pinnacle of American
masculinity. Medal of Honor recipient, Nobel Peace Prize, he's also a founding
feminist. Why? Because masculine men, they're not afraid of independent women.
Now another argument against it is, you know, against feminism is that feminism
is no longer needed because the patriarchy is dead. And yeah, one
definition of patriarchy is a society in which a male ruler inherits his title
from his dad. That is one definition of patriarchy. You are correct. Another is a
a society in which men will get most of the power, and women are typically excluded to
a degree.
Name the last female president, no, vice president, no, okay.
There are 23 female U.S. Senators, and that I think is a record, I think that's as high
as it's ever been, I could be wrong, but I'm pretty sure that's a record.
Now, how many would they need for equal representation in our representative democracy?
50, twice as many.
House of Representatives has 87 female representatives, I believe.
For there to be equal representation there, they would need to be 217, closing in on three
times as many.
seems like men willed most of the power. So patriarchy does still exist. Now it's
a loaded term and it's a term a lot of people don't understand but it is still
there. Now this isn't to say that I agree with every tactic that radical feminists
use but they never asked me. I believe in diversity of tactics so I mean whatever
they're drawing attention to a cause that still needs some attention. At the
the end of the day, guys, all women have to be two things and that's it. Who and
what they want. What you think doesn't matter. The fact that you don't find her
attractive doesn't matter. The fact that she intimidates you because she is more
educated or because she makes more money or because whatever doesn't matter.
That's your hang-up, not hers.
Anyway, just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}