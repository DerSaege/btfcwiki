---
title: Let's talk about thoughts and prayers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wOfpAfIppk4) |
| Published | 2019/11/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Discussed the reactions after seeing something trending on Twitter.
- Mentions the common responses to tragic events: better security, arming teachers, and gun control.
- Critiques thoughts and prayers as not being a solution.
- Explores the impracticality of extreme security measures at schools.
- Raises the issue of the underlying problem being kids wanting to harm others.
- Points out the cycle of violence being perpetuated by societal norms.
- Calls for comments on solutions that do not involve additional violence.
- Urges for a cultural shift away from glorifying violence.
- Expresses the need for a systemic change rather than relying on thoughts and prayers.
- Shares the perspective of a student feeling unprepared despite drills in schools.

### Quotes

- "We have a systemic problem."
- "Our high school students are talking like rangers after the first time they were in combat."

### Oneliner

Beau dives into the societal glorification of violence and the urgent need for non-violent solutions to address the underlying issues leading to tragic events.

### Audience

Community members, activists

### On-the-ground actions from transcript

- Share and create solutions that do not involve additional violence (suggested).
- Advocate for a cultural shift away from glorifying violence (implied).

### Whats missing in summary

The emotional impact and urgency of addressing the root causes of violence in society.

### Tags

#Violence #CulturalChange #Solutions #CommunityPolicing #GunControl


## Transcript
Well, howdy there, internet people, it's Bo again.
So I was on Twitter, saw something trending,
and I was like, hey, I watch that show.
Thought I was gonna see adorable photos of Drew Barrymore.
It's not what it was.
And I wasn't surprised by what it was,
because that's not surprising anymore.
So let's run through it.
I mean, we know what happens now, right?
Half of you are gonna say, we need better security.
arm the teachers, the other half of them say gun control.
That's what people are gonna say.
And what I'm gonna hear, thoughts and prayers,
because that's what it is.
That's what it is.
It's not a solution, either one of them,
even if they were gonna happen, but they won't.
But let's say they do.
Let's say they do.
Okay, you got security.
You have turned that school into a castle.
It's a fortress.
Man, but they still got to get to and from school, they'll be bunched up outside.
I guess we could bring the buses in like through a sally port, like a prison, we could do that.
But then there's the bus stops, bulletproof bus stops, that's the answer, huh?
You follow that line of reasoning long enough, you end up with armed students.
Probably not the solution.
it's not addressing the problem and you look at gun control it's not gonna happen but let's
pretend that it will let's say it passes magic poof then what people don't give them up this
isn't other countries other countries didn't have more guns than people the US does so
people don't give them up then men with guns instead of coming to their school come to
their homes and we know how accurate police fire is if they're at the right
address. It's just changing who's taking them out. It's not stopping them because
it's not actually addressing the problem.
The problem isn't that kids are getting killed. I know that's hard to believe but
That's not the problem, that's the symptom.
The problem is that kids want to kill other kids.
Where do they get that idea from?
What are our solutions to this?
More violence, more guns.
Send men with guns to the home.
Arm the teachers.
Of course they think they solve all their problems
with violence.
It's what we've taught them.
We say otherwise, but you can't tell a kid anything.
You gotta show them.
And here we are, we're showing them.
You know, depending on how people did the math,
when I did that three-part series on America's conflicts,
I said, how many years of peace were there?
Three to 17 years.
The age range of the victims.
Of course we have a problem with violence. We glorify it at every turn.
This is how we solve all of our problems. So that's how they're going to.
We taught it to them. I
fail to believe that there's no solution
that doesn't involve more violence. We got 200,000 people watching this.
200,000 people on this channel and they're some smart people.
I'm dead serious when I say this, I would love to see comments with solutions that don't
involve additional violence.
Because no matter what, that's not going to be the solution.
Because we're just reinforcing the idea that that's how you solve the problems.
None of the stuff that gets thrown out ever addresses the motive.
Most of all we end up with are a bunch of kids that still want to kill other kids.
I can't believe that that's going to be the solution.
There's no magic lamp to rub here.
We're not going to be able to wish this away.
We have a systemic problem.
We need a complete culture change, but we're not going to get it as long as we keep wanting
thoughts and prayers.
A student, after this, after this last one, said that they can train you what to do in
a drill, but they can't prepare you for being scared.
I heard that almost exact sentence once.
Congratulations America.
Our high school students are talking like rangers after the first time they were in
combat.
We got a bigger problem than any of these minor solutions we've got to fix.
anyway. It's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}