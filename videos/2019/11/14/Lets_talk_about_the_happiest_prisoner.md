---
title: Let's talk about the happiest prisoner....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=6Dl5kYKBGxk) |
| Published | 2019/11/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau shares a personal anecdote about his son playing with a toy train, leading into a story he wanted to talk about.
- He introduces the story of Joe, known as the happiest prisoner on death row, in the 1930s.
- Joe, with an IQ of 45, faced a challenging life and was picked on before being arrested for vagrancy in Cheyenne, Wyoming.
- Sheriff Carroll questions Joe about crimes in Pueblo, and Joe falsely confesses to be with another man named Frank.
- Despite inconsistencies in Joe's confession and lack of identification by the surviving sister, he is convicted and sentenced to death.
- Joe's attorney tries to help with appeals, but Joe is executed without a pardon.
- Joe's last moments before execution involve playing with a toy train, showing innocence and ignorance of his impending death.
- Beau mentions Rodney Reed's case in Texas and urges viewers to research and possibly take action within a week.
- Beau concludes with a reflective note, leaving the audience with something to ponder.

### Quotes

- "His name was Joe Aradie, known as the happiest prisoner on death row."
- "Took me too long to see a toy train."

### Oneliner

Beau shares the story of Joe, the happiest prisoner on death row in the 1930s, urging action on Rodney Reed's case in Texas within a week.

### Audience

Advocates for justice

### On-the-ground actions from transcript

- Research Rodney Reed's case and contact the governor within a week (implied)
- Advocate for justice by raising awareness about cases like Rodney Reed's (implied)

### Whats missing in summary

The emotional impact and depth of the stories shared by Beau can best be experienced by watching the full video.


## Transcript
Well, howdy there, internet people, it's Bo again.
You know, when I decide I'm going to make a video,
when I decide what I want to talk about,
I just go about the course of my day,
and something will give me the right way to talk about it.
I'll see something.
I've wanted to talk about something
for the last couple of days,
and I couldn't come up with a way to convey it,
and it'd have an impact, and it needs to.
Tonight, my son is playing with this guy.
This guy in Thomas the Train.
I don't know this one's name.
He's orange and has an ax on the side of it.
My son tells me this one's not too smart.
I don't know if that's something from the show or what.
But either way, because of that, we're
going to talk about the happiest prisoner, a guy named Joe.
This happened back in the 30s.
Joe wasn't a bright guy, he had an IQ of about 45.
You can imagine what life was like for him in the 30s with an IQ of 45.
He was picked on, beat all the time.
One day he's had enough and he hops in a box car, takes off.
Then on August 26, 1936, he gets arrested in Cheyenne, Wyoming for vagrancy.
And the sheriff, Sheriff Carroll, I believe, where you coming from?
Where you been?
Oh, you came through Pueblo.
Some bad stuff just happened in Pueblo.
Couple girls got attacked.
One of them didn't make it.
Joe confesses.
So the sheriff rings up the chief of police in Pueblo.
Chief's a little confused.
We already got our guy.
We arrested him.
His name's Frank.
Joe remembers being with Frank.
They ship him back, ship him to Pueblo.
Frank had confessed.
He doesn't know Joe.
seen him before. So, Pueblo cops start talking to Joe and he confesses again. But he keeps
getting the details wrong. At first, he said it was done with a club, which is what the
cops thought. Then it turns out it was an axe and once the cops found that out, suddenly
Joe remembered. This is common during rough interrogations. People will tell you anything
just to make it stop. And I would imagine if you have an IQ of 45, just about any of the little
tactics that get used seem rough. The surviving sister, well, she ID'd Frank. She couldn't ID Joe.
But he did confess, so they convicted him anyway, gas.
Now one of the attorneys that helped him winds up getting elected, becoming attorney general.
He tries to help, but his appeals are out.
Gets him a couple stays, but no pardon.
He asked for ice cream for his last meal.
The warden, a guy named Roy Best, he said that the day of, Joe didn't know what was
going on.
He just sat in a cell playing with a toy train the warden had given him.
Warden tried to tell him.
Joe said, no, no, Joe's not going to die.
The warden walked him into the chamber, and Joe got nervous for a second, but the warden
held his hand and said, it'll be okay, because the warden knew he was innocent too.
That was 80 years ago, about.
His name was Joe Aradie, known as the happiest prisoner on death row.
He was pardoned in 2011, a lot of good it did him.
In Texas right now, there's a guy named Rodney Reed, I'll leave you to look up his case.
The governor probably needs to hear from you, but if you're going to do anything about it,
you got less than a week.
Took me too long to see a toy train.
Anyway, it's just a thought.
have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}