---
title: Let's talk about party loyalty and what happened in Louisiana....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Q524bP2gMQg) |
| Published | 2019/11/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republicans lost in Louisiana, a deep red state, because loyal Republicans stuck to their values.
- If your party doesn't offer a candidate worthy of your vote, you can choose to abstain from voting.
- Voting is not just about party loyalty; it's about becoming morally responsible for the actions of the candidate you support.
- By voting for a candidate, you become complicit in everything they do, past, present, and future.
- Voting for a candidate means you are endorsing and supporting all their actions.
- Beau questions whether individuals who support Trump through their vote truly agree with actions like putting kids in cages or weakening environmental protections.
- Voting for a candidate means taking ownership of their actions and decisions.
- Beau urges people to think critically about the actions of the candidates they support rather than blindly following party loyalty.
- Supporting a candidate through voting means endorsing everything they stand for and everything they do.
- You can choose not to vote if the candidate presented by your party conflicts with your values and principles.

### Quotes

- "You don't co-sign evil out of tradition."
- "Voting when you vote for someone, you become morally responsible."
- "You become complicit in everything they do."
- "If you vote for Trump, past, present, and future, Trump is you."
- "You own that. You co-signed it."

### Oneliner

Republicans lost in Louisiana because loyal Republicans stuck to their values, showing that voting means becoming morally responsible for the candidate you support.

### Audience

Voters

### On-the-ground actions from transcript

- Re-evaluate your loyalty to a political party based on the actions and values of the candidates they present (exemplified)

### What's missing in the summary

The full transcript provides a deeper exploration of the importance of holding politicians accountable for their actions and decisions, urging voters to prioritize values over blind party loyalty.


## Transcript
Well, howdy there, Internet people, it's Bo again.
So tonight, we're gonna talk about party loyalty
and why the Republicans lost in Louisiana,
a red state, a deep red state,
because they shouldn't have.
That's not how that should have gone down, not at all.
In that initial election,
52% of people voted Republican, 47% Democrat.
Nobody got a majority, though,
So they went ahead and had another election, and the Democrat won.
Does that mean that a whole bunch of Republicans decided to vote for a Democrat?
No, it means that some loyal Republicans still have morals.
They're still true to their values.
That's what it means.
I know that's hard to believe given all the news coverage lately, but
that's what it means. If your party doesn't have enough loyalty to you to give you a candidate
worthy of your vote, you're under no obligation to vote for them. It doesn't mean that you
have to change parties. It doesn't mean you have to vote for a Democrat. It just means
you stay home. That's what happened in Louisiana. In 2020, Republicans should remember this.
They really should.
If the party gives you a candidate you can't co-sign, don't, don't.
You don't have to leave the party.
You just have to stay home, abstain.
The candidates, the politicians you support, they do it all the time.
They don't want to vote on a bill, they just don't.
Nobody says they're un-American.
They just play that guilt trip on you to get you to stick to your party loyalty.
You don't co-sign evil out of tradition.
You don't undermine the Constitution because it's your party.
Consider for a second that Trump's a Democrat.
You still back his actions?
Of course not.
Of course not.
And right now somebody's saying, well, Democrats are just as bad, okay, so they're the same.
Why do you have party loyalty to one then?
Doesn't make any sense.
If that's the case, there's no reason to have any loyalty to any party.
You just vote for the candidate, which is kind of how it's supposed to be.
Voting isn't just mere words on social media.
Not defending your side to get internet debate points.
Voting when you vote for someone, you become morally responsible.
You become complicit in everything they do.
And in the case of an incumbent, everything they did.
You vote for Trump, past, present, and future, Trump is you.
You own that.
You co-signed it.
said this is all okay. Given the option, would you vote in favor of throwing kids in cages
in those conditions with that much abuse? And if you don't know what I'm talking about,
you need to look it up. The answer is no. You're human. You wouldn't vote for that.
But if you vote for Trump, you're cosigning it. Yeah, that's okay. Would you vote in favor
of selling out our allies, undermining the U.S. military ability to recruit indigenous
partners all over the world?
No, of course not.
But if you vote for Trump, you're co-signing that, because that's what he did.
Would you vote to weaken our trade position?
Would you vote to deregulate and get rid of the environmental protections that protect
your kids so his buddies can make some money?
Of course not.
She wouldn't vote for that.
Would you vote to deny aid to Americans but offer it to Russia?
Of course not.
But if you vote for Trump, that's what you're doing, because that's what he did.
That's what he'll continue to do.
You don't have to vote to make your voice heard, and you don't co-sign evil simply
because it infiltrated your party.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}