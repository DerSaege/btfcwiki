---
title: Let's talk about the wall and Constitutional obligations....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=hmFGSiAWmRE) |
| Published | 2019/11/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Down on the Mexican border, searching for Trump's wall, Beau gets lost and may have ended up in Mexico.
- Beau questions the lack of progress on the border wall, noting that not a single new mile has been built in three years.
- He criticizes the government for disrupting processes and seizing land without fair compensation under the Declaration and Taking Act.
- Beau points out the failure of Mexico to pay for the wall, despite promises.
- He addresses public opinion on the Ukraine call, suggesting that regardless of intentions, a crime may have been committed.
- Beau argues that the Constitution mandates the Senate to remove the president if he committed crimes, including bribery and extortion.
- The lack of progress on the wall, military decisions in Syria, and trade wars are factors causing even his base to turn against the president.
- Beau questions Trump's ability to handle the job as president, mentioning his focus on branding and economic issues.
- Despite some successes in avoiding recession, Beau believes all evidence points to Trump violating the law and suggests it's the Senate's obligation to remove him.

### Quotes

- "There was already a wall there. Where's the new wall?"
- "It is now the Senate's constitutional obligation to remove the president."
- "His base is shrinking rapidly because he keeps insulting them."
- "All evidence shows that he violated the law more than likely."
- "It's more than branding and that's what Trump is good at, branding."

### Oneliner

Beau on lack of border wall progress, potential crimes, and constitutional duty to remove the president amidst turning base.

### Audience

Voters, concerned citizens

### On-the-ground actions from transcript

- Contact your senators to express your views on potential violations of the law by the president (suggested).
- Join or support organizations advocating for government accountability (exemplified).
- Organize community dialogues on constitutional obligations and holding leaders accountable (implied).

### Whats missing in summary

Deeper insights into the impact of political decisions and messaging on public perception and support.

### Tags

#BorderWall #GovernmentAccountability #ConstitutionalObligations #PoliticalLeadership #PublicOpinion


## Transcript
Well, howdy there, internet people, it's Bo again.
We're down here live on the Mexican border
trying to find Trump's wall.
Got a little lost, to be honest.
I think we might be in Mexico.
So, I'm gonna make a phone call real quick.
Y'all just bear with me.
Hey man, where's the wall?
Wait a minute, what wall?
Trump's wall, 1,954 miles of great, big, beautiful wall.
Where is it?
There was already a wall there.
Where's the new wall?
You built the new wall where the old wall was, 78 miles.
You're telling me that in three years, there's not one new mile of wall, not one.
allocated, took money from DOD, disrupted the entire government process for 3 years,
and not a single mile a new wall.
What are y'all doing?
And don't get mad at me.
I don't make this stupid promise.
My viewers want to see a wall.
I'm not here trying to be a good presidential supporter, not like I'm being extorted into
it or anything.
Uh-huh, mm-hmm, see y'all are taking land,
trying to seize land under the Declaration and Taking Act.
That's the one where you can take ranchers' land
without even discussing paying them
until after you're already using it, right?
Yeah, yeah, so I guess Mexico didn't pay for the wall, huh?
Hey, man, can you come get me?
I might be lost out here.
I don't know where I'm at.
There's a bunch of cactuses and rocks with some blue paint on them.
Well, there was supposed to be a wall here.
So the poll came out, 70% of Americans
feel that the Ukraine call was wrong,
that the president did something wrong during it.
And regardless of his best intentions,
he most likely committed a crime,
and that's why people are seeing that.
Now, there are some people who are just saying,
did something wrong but we don't want him removed. Now the problem with that is
if you see that he did something wrong it's because you see bribery or
extortion or maybe you just think it's wrong for him to solicit something from
a foreign national for his campaign which is illegal by the way. Now the idea
of knowing that he did something wrong and not wanting him
impeached, well that presents a problem because all of those things are crimes
and that section of the Constitution doesn't say high crimes are Mr.
Beaters and can be removed. It says shall be. That means must be. It is now the
Senate's constitutional obligation to remove the president. Anything short of
of that is them failing to do their jobs and undermining the constitution
of the United States, why are people turning against the president?
It is really simple.
There's no wall.
They're seizing 450 miles of land, but there's still no wall.
This is affecting his base.
They see him as incapable of doing the simplest tasks.
It's a construction project, about one new mile.
He said that they had gotten rid of that bad guy over there.
That whole organization was gone, 100% totally defeated.
But we have to leave troops there to protect oil fields because they're defeated.
And we also sold out our allies who did the heavy lifting, but we need to leave the troops
there because they're completely, 100% totally defeated.
Even the most ignorant of his supporters sees through that.
His base is shrinking rapidly because he keeps insulting them.
The trade wars will win.
We're going to get a win.
They're going to buy soybeans.
at the level that they were buying him at before he started this stupid war.
This is why even his base is turning against him now.
Because they realize that his best intentions were worth nothing.
And they weren't good intentions to begin with.
They were about feathering his cap.
The job, as was said, the job is too big for Trump, he can't handle it.
It's not, you know, we give presidents a lot, a lot of flack while they're in office.
But do you ever look at how they age while they're in there?
Because there's a lot to it, there is a lot to it.
And it doesn't matter how they look at it, there's a lot of responsibility and I'm not
sure that most people understand that level of responsibility.
It's more than branding and that's what Trump is good at, branding.
You can't brand your way out of anything except an economic crisis.
And he's actually done really great at avoiding the recession he triggered, I'll give him
that. I think, I think he's doing an amazing job and I feel bad for the next
president because that's when it's gonna hit. But Trump saw the signs and did
everything he could to avert it and he did it well. I can't, I can't deny that.
But here's the thing at the end of the day, all evidence, all evidence shows
that he violated the law more than likely several shall be. It is now the
Senate's constitutional obligation to remove it. Anyway it's just a thought
Y'all have a, have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}