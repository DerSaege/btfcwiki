---
title: Let's talk about DACA, Stephen Miller, and the future....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=bj3dKW_FtFA) |
| Published | 2019/11/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- 700,000 Americans, known as dreamers, are at risk of having their lives destroyed due to policies recommended by Stephen Miller.
- Steve Miller's emails reveal him as a racist and fascist, yet the President and Republicans continue to support him.
- The country is being led by a senior White House advisor with disturbing emails that expose his true nature.
- The president solicited a thing of value from a foreign national for his campaign, which is illegal, but his supporters turn a blind eye.
- While harmless individuals are at risk, the country is focused on destroying lives for political gain.
- The nation's leadership is lacking, and it will require ideologues to step forward to rebuild and heal the country.
- There is a need to undo the damage caused by current leadership and work towards healing the nation.

### Quotes

- "700,000 Americans, known as dreamers, are at risk of having their lives destroyed."
- "The country is being led by a senior White House advisor with disturbing emails that expose his true nature."
- "We're letting it happen right now."

### Oneliner

700,000 Americans are at risk, led by a senior advisor exposed as a racist fascist, while the nation turns a blind eye to illegal actions.

### Audience

Americans

### On-the-ground actions from transcript

- Protest in support of the 700,000 individuals at risk (implied)
- Step forward as ideologues to rebuild and heal the country (implied)

### Whats missing in summary

The full transcript provides a deeper understanding of the urgency and importance of standing up for the rights and well-being of all individuals in the face of corrupt leadership.

### Tags

#Dreamers #StephenMiller #PoliticalCorruption #Rebuilding #CommunityHealing


## Transcript
Well, howdy there, internet people, it's Bo again.
I want you to imagine me trying to figure out exactly
how to explain that 700,000 Americans
are at risk of having their lives destroyed
and that they are Americans
when news of those emails pop up.
News of Steve Miller's emails.
700,000 Americans, you can call them dreamers.
You can call them whatever dehumanizing term you want to use,
but at the end of the day,
These are people who grew up here, went to school with our kids.
They're our neighbors and they've harmed no one and they're at risk of having their lives
destroyed because of policies recommended by Stephen Miller and his emails come out.
And I for one am completely shocked and amazed.
I can't believe that those emails are his.
If you haven't seen them, I suggest you go look at them, really read them and then go
Go ahead and click around on the sites that he recommends.
Take a good look.
That's what's leading the country.
That's a senior White House advisor.
So the guy who enacted all of those policies that people said were racist and fascist has
a whole bunch of emails that certainly appear to reveal him as a racist fascist.
And the thing is, even after this, the president's going to support him.
The Republicans in the Senate, they're going to continue to support the president, and
their base will continue to support them.
Because all of these people have bought into talking points come up with by a bigot.
They sold out their country for a red hat instead of a red armband.
Because at this point, it's the same thing.
becoming more and more clear. 700,000 Americans, 700,000 people, who have harmed no one, are
at risk of having their lives destroyed. And their case is at the highest court in the
land. And the messed up part is, we don't know how the court's going to decide. They've
They've done nothing.
They've hurt no one.
But hey, maybe we'll destroy their lives for a political talking point because that's what
America is now.
This partisan garbage, 700,000 people.
And you can say, well, we just want them to follow the law all you want, but you don't.
You don't care about the law, if you did you'd be calling for the resignation of the president.
He solicited a thing of value from a foreign national for his campaign, that's illegal.
That's not even up for debate.
He did that, it wasn't really about the investigation, that's why he kept saying make sure you go
to the microphones, we want the announcement.
The announcement was what was important because that's what would help his campaign, it's
a thing of value.
laws are about this. They have them. And he violated them. But you don't care. You'll
defend that. But the fact that some kid got drug across the border by his parents or her
parents and then came here and built a life, oh, that's a crime they're going to have
to answer for 20 years later. This country has lost its mind and it has lost its way.
You know, over the last couple of weeks I've done a lot of videos detailing some of the
stuff that has gone on in this country throughout history.
People don't really like to watch them.
And one of the things is how could this have happened?
How did they just let it happen?
We're letting it happen right now.
The fact that 700,000 people who did no harm to anybody are at risk, we're not going to
do anything.
Doesn't matter what the court decides, we're not going to protest, we're not going to get
out in the streets, we're not going to do anything to support 700,000 of our neighbors.
Maybe we will.
I don't know, but what I do know is that the leadership in this country is pretty dark.
It's lacking.
I'm not just talking about the White House.
I'm not just talking about the Senate.
We need some ideologues to step forward.
that actually want to be out there, because we're going to need it, because no matter
what happens during the next election, we're going to have to rebuild.
It may be a year from now, it may be five years from now, but we're going to have to
undo all of this damage.
And it's not just repealing the laws.
It's fixing this country.
It's healing it because we're sick.
We are sick.
No healthy nation would this even be a debate.
Would this even be a discussion?
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}