---
title: Let's talk about Native schools, today's schools, and what we all need to do....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=P4uUEOENwp0) |
| Published | 2019/11/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Native American Heritage Month, discussing the brutal history of native schools imposed on Indigenous peoples, designed to crush their spirit and eradicate their culture.
- Schools historically aimed to assimilate Indigenous children, forbidding them from speaking their native tongue, using their real names, or practicing their culture.
- The impact of these schools on Indigenous communities has been long-lasting, with repurposed facilities sometimes revealing unmarked graves of children.
- Modern-day schools, while not as brutal, still prioritize assimilation and uniformity, stifling curiosity and individuality.
- Teachers are often overworked and focused on meeting standardized test requirements, leading to students learning what to think rather than how to think.
- As parents and community members, it's vital to supplement the education provided by the state and encourage curiosity and critical thinking in children.
- Beau shares an anecdote about encouraging his child's curiosity by exploring topics together, utilizing resources like YouTube to foster learning opportunities.
- The education system prioritizes uniformity over individuality, hindering the development of critical thinking skills in students.
- Beau stresses the importance of fostering curiosity in children to ensure they can apply their knowledge effectively and think for themselves.
- While acknowledging the challenges teachers face, Beau encourages a shift towards promoting critical thinking skills and individuality in education systems.

### Quotes

- "The schools that were imposed on the natives, they were something else."
- "The schools, they kill that individuality, they really do, it's the way they're designed."
- "We have to create that curiosity so that they can take that base knowledge that they're getting to the next level."
- "You try to impart that lesson. I guarantee you, you're never going to oppose a raise for teachers again."
- "It's just a thought."

### Oneliner

Beau brings attention to the brutal history of Native American schools, the modern-day impact of assimilation in education, and the importance of fostering curiosity to combat uniformity in learning systems.

### Audience

Parents, Community Members

### On-the-ground actions from transcript

- Foster curiosity in children through supplementary learning opportunities (exemplified)
- Encourage critical thinking skills and individuality in education systems (implied)
- Utilize resources like YouTube to support children's interests and curiosity (exemplified)

### Whats missing in summary

The detailed examples and anecdotes shared by Beau provide a deeper understanding of the impact of assimilation in education and the importance of fostering curiosity in children.

### Tags

#NativeAmericanHeritage #Education #Assimilation #Curiosity #CriticalThinking


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're going to talk about native schools,
the schools we have today, and something we may
should be doing as parents or elders in the community,
whatever.
It is Native American Heritage Month.
And since I'm going to be talking a little about schools,
I figure I should bring this up, it fits nicely in perhaps an over dramatic way.
The schools that were imposed on the natives, they were something else.
They were something else.
They were designed to crush that spirit.
It's really what they were there for.
They couldn't speak their native tongue, had to cut their hair,
couldn't use their real names.
horrible. It was horrible and the conditions were appalling, really were.
Those schools probably had the longest lasting impact of any of the forms of
ethnic cleansing that we tried. And, you know, when these facilities get repurposed
today it's not uncommon for them to find random children buried unmarked. Horrible
places and they were designed to kill the Indian and leave the man but they
were so brutal sometimes I wound up killing both. All in the pursuit of
of creating an American, whatever that is.
Now the schools we send our kids to today,
they're not that brutal, they're not that severe,
but that desire
to create assimilation,
uniformity,
it's still there.
And this has nothing to do with the teachers.
It's the way the schools are designed.
It breeds
uniformity.
And it doesn't matter if you have one of those great
teachers that truly encourages curiosity on every level.
That teacher is competing with every other child in that
classroom who's going to try to stifle it.
They don't want extra questions.
got to get this done so we can go do whatever. It's their peer group and the
way our schools are set up that stifle that curiosity and that curiosity is
critical for education, for real education. You know we have kind of a
modified Prussian system. It's not quite a factory school setting. The curriculums
are developed by decentralized school districts in most cases, but they have
standards they have to meet. And those teachers who are overworked, have too
many kids in the class, all of that, they have to focus on making sure that they
can pass those tests. The end result of that is a bunch of kids learning what to
to think rather than how to and you know we expect so much from these teachers
and then we set all of the rules and all of the systems in opposition to all it's
kind of messed up so that's where we come in as parents as community members
whatever, we always have to be ready to supplement what the state provides.
The other day my five-year-old was watching the Magic School Bus rides again and I knew
something was up when he came up to me because he was talking about this cave that we had
gone to a few weeks ago.
And then all of a sudden it pops out, he wants to know what kind of rocks are best to find
fossils. I'm sitting there like, thanks Ms. Frizzle, you know now I'm gonna try to
access parts of my brain I haven't used since the sixth grade. And what I want to
say is this is a science question, you need to go talk to your mom, but I can't do
that because we want to encourage that curiosity. So we always need to have
have those hip pocket classes ready.
Whether it's a skill, general information, an idea, a life
lesson, whatever that you want to pass on, you need to have
your own lesson plan and have that ready for when the
opportunity arises.
We have that great resource.
How I dealt with that question.
We have YouTube.
So I wound up sitting there watching 40 minutes of stuff on fossils that I still don't remember,
but he will.
And I know as busy as we all are, the first reaction is to go over there, type it in for
him, let him watch it, pull it up, here's some stuff on fossils, where you should look
for and walk away and go about what we were doing. See the problem is if we do
that we become just another kid in that classroom. The one thing that I have
learned about children is you can't tell them anything. You got to show them. If your
If your child asks you a question and you don't know the answer, you go to YouTube.
Smart kids will put that together.
So if you type it into YouTube, the kid knows you don't know.
When you walk away, what are you telling him?
Whatever he was curious about, it's not important.
You don't need to know it.
The schools, they kill that individuality, they really do, it's the way they're designed.
There's not much that can be done in the system because the system has to cater to the middle.
They have to create that uniformity for them to be effective.
We have to create that curiosity so that they can take that base knowledge that they're
getting to the next level.
Because otherwise we're going to end up with a bunch of worker drones that can't think
for themselves.
You can't blame the teachers for this, you can't.
That may be the first reaction, but you put 30 kids in a room,
20 kids in a room, and you tell them to think for themselves.
You try to impart that lesson.
I guarantee you, you're never going to oppose a raise for
teachers again.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}