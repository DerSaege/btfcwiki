---
title: Let's talk about ethically using resources....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=OUu0VVLHYjI) |
| Published | 2019/11/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Received a question from a person attending a university heavily funded by a nearby corporation.
- The person opposes the corporation but was given the chance to organize speaking engagements funded by them.
- Advises that as long as the funding doesn't affect the content or speakers, taking the money is acceptable.
- Mentions how the Department of Defense teaches soldiers to use resources left behind by the opposition.
- Argues that using the corporation's resources, even if it seems hypocritical, can be necessary in certain situations.
- States that altering society is like being behind enemy lines, and using the opposition's resources can be strategic.
- Emphasizes that taking the corporation's money for speakers who will speak out against them is not ethically wrong.
- Points out that large corporations fund events to generate goodwill and that drawing attention to their practices can be a win.
- Notes that in a society dominated by big corporations, it's challenging to completely avoid their influence.
- Concludes with the idea that ethical consumption is rare, but the focus should be on ensuring benefits to people outweigh those to the establishment.

### Quotes

- "When you're talking about altering society, we're behind enemy lines."
- "There are times when using the opposition's resources is your only option."
- "You have no ethical obligation to correct that mistake for them."
- "You just have to make sure that the benefit to the people outweighs the benefit to the establishment."
- "As long as the money isn't affecting the content that you're putting out, as long as it's not going to change your speaker list, as long as it's not going to alter what they're going to talk about, you know..."

### Oneliner

Beau advises using the opposition's resources strategically when behind enemy lines in altering society, focusing on benefiting people over establishments.

### Audience

University organizers

### On-the-ground actions from transcript

- Organize speaking engagements funded by corporations (suggested)
- Draw attention to questionable business practices of large companies (implied)

### Whats missing in summary

Beau's insights on navigating ethical dilemmas and strategic decision-making in a society influenced by large corporations.

### Tags

#University #Funding #Ethics #StrategicDecisions #CorporateInfluence


## Transcript
Well, howdy there, Internet people, it's Bo again.
It's not a question sent to me over Twitter.
I can't read the whole thing, or really much of it at all,
because there's too much identifying information in it.
And I happen to know I've got a pretty big audience
in this geographic area.
But it's from a person who is attending a university.
And this university is heavily funded
and benefits a lot from a large corporation that happens to have its headquarters nearby.
He doesn't support this corporation, actively opposes it, but he was given the opportunity
to organize some talks, some speaking engagements, and then he finds out the facility they'd
be held at, was funded by this corporation, and that they'd be funding the event.
He asks, how can I take their money and fight them?
Man, if only there was a way to do that.
There is.
The way you just said.
As long as the money isn't affecting the content that you're putting out, as long as it's not
going to change your speaker list, as long as it's not going to alter what they're going
to talk about, you know, the Department of Defense has this course, not really a course,
more of an impromptu most times.
But they take guys and they teach them how to use PKs and sax horns and a bunch of stuff
the U.S. military doesn't use itself, it's not in their inventory.
But they teach our guys how to use it, why?
On the off chance the opposition ever leaves it behind, they know that when you're fighting
a battle, especially one as big as the one we're involved in, you never let a resource
the opposition gives you go to waste.
Yeah I'm sure there are some people that would say that it would be hypocritical to use their
resources.
Yeah well, ask them what they've done.
Ask them what they've done.
There are times when using the opposition's resources is your only option.
You would not fault a soldier behind enemy lines who picked up a foreign weapon.
It wouldn't even cross your mind.
And that's kind of the way we are in this fight.
When you're talking about altering society, we're behind enemy lines.
status quo is there. If a large corporation is willing to fund you, organizing speakers
that are going to speak out against them, well that's just bad decision making on their
part. You have no ethical obligation to correct that mistake for them. You take that money
and you smile and you put on your event.
The odds are your speakers are going to reach more people than whatever perceived PR value
there is.
And that's why these large companies do this.
Large companies, especially those with questionable business practices, they put a lot of money
into the local area and it's to generate goodwill.
If your speakers draw attention, or even inadvertently draw attention to some of those practices,
well that's a win.
At the end of the day, when you're talking about companies that are that big and that
pervasive in our society, there's no getting away from them.
Even if you held the event at a different location without their funding, you'd probably
still end up using their services. When you're talking about companies at that
size, it doesn't matter. You know, there's that catch-all saying now, you know, there
is no ethical consumption. I mean, there is, but it's pretty rare. Most things you
buy, most activities you become involved in, are in some way, shape, or form going
to benefit the establishment. You just have to make sure that the benefit to
the people outweighs the benefit to the establishment.
Anyway, it's just a thought.
y'all have a good night!

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}