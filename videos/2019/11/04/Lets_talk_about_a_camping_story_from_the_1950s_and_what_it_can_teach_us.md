---
title: Let's talk about a camping story from the 1950s and what it can teach us....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=4dbRGaPph-w) |
| Published | 2019/11/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- In 1954, two groups of 11 boy scouts who had never met before went camping at Robbers Cove State Park in Oklahoma.
- The boys quickly formed bonds and a social order, but things took a competitive turn when they found out about another group nearby.
- The competition escalated with sporting events, team challenges, and the winning team was promised medals and pocket knives.
- Name-calling, stealing flags, and animosity grew between the two groups, known as the Rattlers and the Eagles.
- The losing team stole the winning team's medals and pocket knives after the tournament, leading to continued grudges and anger.
- When the camp lost water, all the boys worked together to fix the pipes and even collaborated to pull a stuck pickup truck using a rope from a tug of war.
- The act of pulling together to solve a common problem led to the insults and grudges disappearing, transforming enemies into friends.
- Psychologists intentionally set up this experiment with similar groups to observe how shared struggles could unite people.
- A similar study in Beirut in 1963 with mixed Muslim and Christian teams did not go as planned, showing that shared struggles might not always overcome deep divisions.
- Beau suggests that when faced with a common struggle like a stuck truck or lack of water, people can unite despite their differences, transcending barriers like religion, skin tone, and borders.

### Quotes

- "When the struggle comes, we will unite."
- "Those petty divisions disappear. Skin tone, religion, borders, none of it matters because we'd all be united."
- "We have the struggles. We just need somebody to leave the rope, set things in motion, get us to unite."
- "The insults stopped. The grudges, well, they're gone. They became friends."
- "It's been repeated pretty often."

### Oneliner

In 1950s camping experiment, shared struggles united warring boy scout groups, showing how common challenges can dissolve divisions and foster unity.

### Audience

Community members

### On-the-ground actions from transcript

- Organize community events that involve collaboration and teamwork (implied)
- Encourage diverse groups to work together on common goals or projects (implied)
- Support initiatives that address shared struggles like poverty, hunger, or climate change (implied)

### Whats missing in summary

The full transcript provides a detailed exploration of how shared struggles can unite diverse groups, offering insights into the power of collaboration and overcoming divisions through common challenges.

### Tags

#Unity #Community #Collaboration #SharedStruggles #BoyScouts


## Transcript
Well, howdy there, I don't know, boobalids bow again.
So tonight we're going to talk about what we can learn from a group of boys who went
camping in the 1950s.
Stick with me on this one.
In 1954, a group of 11 boy scouts in the fifth grade who'd never met before went camping
at Robbers Cove State Park in Oklahoma they got there normal scouting trip set
up camp cooked for each other sporting events that sort of thing bonds were
formed they became friends social order quickly developed then they found out
Now, not too far away was another group of 11 boy scouts in the fifth grade.
So they asked to be put in competition with them.
And that competition ensued.
A tournament, sporting events and team challenges, normal boy scout stuff.
The winner of the tournament was every boy who was going to get a medal and a pocket
knife.
back in the 50s. So it didn't take long for each group to start to see the other
as their opposition. They took names the Rattlers and the Eagles and it kind of
got out of hand. I mean it started with name-calling and you know they'd glorify
their own actions and denigrate those of the other, but then they started sneaking
into each other's camp and like stealing their team flag, stuff like that.
When the tournament was over, the losing team snuck into the winning team's camp, stole
their medals and their pocket knives, and then they had to eat together at group meals.
The grudges, the anger, name calling, it was all still there.
It's all still there.
And then the camp lost water.
And all the boys worked together to go check the pipes.
Then a pickup truck got stuck.
A convenient nearby rope that they had used for a tug of war
was attached to the truck, and they literally all pulled
together to get that truck unstuck.
Then this crazy thing happened.
The insults stopped.
The grudges, well, they're gone.
They became friends.
When they were leaving, they actually spent money on each
other at a cafe on their way out.
And all of this is exactly what the psychologists had
hypothesized was going to happen when they set it up.
They intentionally chose two groups of 11 that had never met, put them near each other,
they arranged the competition, they left the rope there, put the truck there, turned the
water off, all to see what would happen.
And as predicted, they all pulled together.
Now there's a lot of criticisms of this study.
Some of them are well founded, some of them aren't.
One of the big ones is that these boys were pretty much all the same.
Similar backgrounds, religion, skin tone, they're all boy scouts, was easy for them
to come together after being enemies.
The study was repeated in 1963 in Beirut.
The teams were mixed, Muslim and Christian.
Didn't go as exactly as planned.
They had called the experiment off early, because the fighting
ensued in the second phase, and it got pretty intense.
But the thing was, the fighting was between the Red Genies and
Blue Ghosts, which is the team names they chose, that end group, that team, because
they had struggled together, it overrode everything.
Every other identifying trait, that struggle overrode it.
Wonder what happens when we start to work together.
of us from our various groups we all have to pull together because the truck
is stuck. The water's off. We all have to face a struggle together because it's
something that imperils us all. Those petty divisions, they disappear. Skin tone,
religion, borders, none of it would matter, because we'd all be united. The
thing is we have struggles that could do that. That we could all unite behind. Hunger,
poverty, getting everybody fresh water, we're not careful, climate. We have the
struggles. We just need somebody to leave the rope, set things in motion, get us to
unite. We know what's gonna happen when the struggle comes. We will unite. It's
been repeated pretty often.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}