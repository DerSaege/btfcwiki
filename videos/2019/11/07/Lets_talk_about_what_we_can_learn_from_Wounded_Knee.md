---
title: Let's talk about what we can learn from Wounded Knee....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-M9QajRgSHI) |
| Published | 2019/11/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- General Miles sent a telegram to D.C. stating that the "difficult Indian problem" could not be solved without fulfilling treaty obligations.
- Native Americans had signed away valuable portions of reservations to white people without receiving adequate support as promised.
- Leading up to the Wounded Knee massacre, the US government continued to seize Native land and ignore treaty agreements.
- The Ghost Dance movement, based on a vision of a native Jesus, scared settlers, leading to conflict with authorities.
- Sitting Bull was targeted for arrest, leading to a violent confrontation where he was killed.
- Natives fleeing reprisals were surrounded by the Seventh Cavalry, resulting in a massacre at Wounded Knee.
- The Army awarded 20 Medals of Honor for the massacre, indicating a cover-up to justify the excessive force used.
- The Wounded Knee massacre exemplifies ignorance, arrogance, supremacy, and fear of the other in US history and foreign policy.
- Beau suggests reflecting on Native American heritage and how it relates to modern-day foreign policy and exploitation.
- Native American history serves as a vital lesson on the consequences of colonialism, militarism, and exploitation.

### Quotes

- "It's Native American Heritage Month. But their heritage is our heritage."
- "Ignorance, arrogance, supremacy, fear of the other. It's what caused it."
- "We could learn a lot from our Native American heritage."

### Oneliner

General Miles' telegram reveals unfulfilled treaty obligations leading to the Wounded Knee massacre, showcasing American history's mistreatment of Native Americans and serving as a lesson for modern foreign policy.

### Audience

History enthusiasts, activists

### On-the-ground actions from transcript

- Study Native American history and share the truth with others (suggested)
- Support organizations advocating for Native American rights and reparations (exemplified)

### Whats missing in summary

Importance of acknowledging and learning from the dark chapters of history to create a more just future.

### Tags

#NativeAmerican #History #WoundedKnee #Colonialism #ForeignPolicy


## Transcript
Well, howdy there, internet people, it's Beau again.
So tonight, I'm going to start off by reading a telegram to kind of lay the scene.
Sent by General Miles back to D.C.
The difficult Indian problem cannot be solved permanently at this end of the line.
It requires the fulfillment of Congress of the treaty obligations that the Indians were
entreated and coerced into signing. They signed away a valuable portion of the
reservation and it is now occupied by white people for which they have
received nothing. They understood that ample provision would be made for their
support. Instead, their supplies have been reduced and much of the time they've
been living on half or two-thirds rations.
That's where they're at. Basically, this is American history. This is a perfect
example of the US government dealing with natives throughout all of American
history. So leading up to Wounded Knee, the US had continued to seize native
land. The bison are gone. Miners and settlers keep coming onto their land
even after giving up yet another chunk. Treaties weren't honored. They're never
honored. At this time the ghost dance starts and this happens because guy has
a vision. Jesus comes back as a native. When he does, all the white people are
gone and their grandfather, their ancestors, will come back and lead them
to hunting grounds that are plentiful. It's a dance, it's a religious thing, but
But the dance scared the settlers, they didn't know what it was, they didn't care to know
what it was, it scared them, they contacted the authorities.
Now the military, they have been down this road before, so they wanted to send in Buffalo
Bill who was friends with Sitting Bull.
The idea was to kind of take some of the chiefs into custody, find out what was going on,
maybe break it up if it was going to be a rebellion.
Well, a guy from Standing Rock Agency, Standing Rock,
overrode the military, sent in 40 native cops
to go arrest Sitting Bull.
Sitting Bull is Sitting Bull.
He didn't really just comply.
They used force, too much force, so much force that one of the natives watching fired, hit
one of the cops who in turn pulled the trigger on his pistol and shot Sitting Bull.
Then another cop finished him, shot him in the head.
Fearful of reprisals, a large group of those natives took off, went to join up with another
leader named Spotted Elk.
They were headed to Pine Ridge when Seventh Cav surrounded their camp.
They set up machine guns.
And their scouts and interpreters were telling them, do not try to disarm them.
don't try to disarm them, said it over and over.
They ignored it and they tried to disarm them.
Now actual chain of events is a little fuzzy through the weathers of history, but the most
commonly held chain of events is that one guy is doing a ghost dance while the soldiers,
Some of the soldiers are down trying to disarm everybody.
They approach a deaf native who can't hear him, doesn't know what's going on.
They grab him by both arms as a second native tries to intervene and his rifle goes off.
The deaf native's rifle goes off.
That was it.
And then over the next hour, those machine guns went to work on everybody.
Friendly fire was a major factor in this battle.
At the end of the day, 150 to 350 natives gone, women, children, didn't matter.
matter. They were buried in a mass grave. The Army, for its part, awarded 20 medals
of honor. The highest commendation that a soldier can get, 20 of them. Just so you know,
time you see a large number just an inordinate amount of decorations being
bestowed for a single incident that's a little weird to begin with no it's a
cover-up it's what it is. So that's it that's that's wounded me. Ignorance
arrogance, supremacy, fear of the other. It's what caused it and it's a perfect
example and a perfect teaching tool to explain today's US foreign policy. I told
you this because it's Native American Heritage Month. But their heritage is our heritage.
It's just the part we don't like to talk about because most of it is like this. It's not
good. But it could at least serve some good if we looked at this and evaluated this and
looked at it in terms of our foreign policy, how we take and exploit and give nothing back
except to the leadership who's in our pocket, except to the military, the native cops.
Because you always had the sitting bull.
For every sitting bull, there was some native who was willing to play along with the military.
It is our foreign policy, it's how it works today, and it's based on the same things.
Ignorance, arrogance, supremacy, and fear of the other.
We could learn a lot from our Native American heritage.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}