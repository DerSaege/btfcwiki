---
title: We talked about how to change the world....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2deSJ61eg7o) |
| Published | 2019/11/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Compares today's Western society to George Orwell's 1984, where control mechanisms limit thinking outside the box.
- Emphasizes the need to become conscious before rebelling against societal norms.
- Shares a personal story of needing help from a network, showcasing the importance of community.
- Encourages building a network by evaluating personal skills and offering assistance.
- Challenges stereotypes about age and expertise, urging everyone to contribute to a community network.
- Suggests using social media to connect and build a digital community for introverts.
- Stresses the value of expertise and the importance of not underestimating one's skills.
- Recommends recruiting people from immediate circles, social media, and activist communities for a network.
- Shares examples of structured networks using poker chips for favors or a commitment to help each other.
- Advocates for increasing financial resources by starting low-cost businesses and investing within the network.
- Mentions the concept of Stay Behind Organizations during the Cold War as an example of effective community networks.
- Proposes that building a strong community can lead to self-sufficiency regardless of political leadership.
- Rejects top-down leadership in favor of community building to empower local areas.

### Quotes

- "Being independent doesn't actually mean doing everything by yourself, it means having the freedom to choose how it gets done."
- "Everybody has something to offer a network like this, everybody."
- "If you make your community strong enough, it doesn't matter who gets elected, it doesn't matter who's sitting in the Oval Office."
- "Not sure top-down leadership is the answer. Think maybe community building is the answer."
- "You know what's best for your community and your area."

### Oneliner

Beau shares insights on building community networks, stressing the importance of conscious rebellion, mutual support, and self-sufficiency for societal change.

### Audience

Community builders, activists.

### On-the-ground actions from transcript

- Connect with immediate circles, social media, and activist communities to recruit members for a network (suggested).
- Start a low-cost business or invest in someone else within the network to increase financial resources (exemplified).
- Build a digital community for introverts using social media (implied).
- Offer help based on personal skills and expertise to contribute to a community network (suggested).
- Participate in community service activities to showcase the network's positive impact (implied).

### Whats missing in summary

The full transcript provides detailed examples and insights on the importance of community networks for societal change and individual empowerment.

### Tags

#CommunityBuilding #MutualSupport #SocietalChange #Empowerment #NetworkBuilding


## Transcript
1984, George Orwell wrote that they will never rebel until they've become conscious and they
can't become conscious until after they rebel.
The society we live in today in the Western world is very similar to 1984's fictional world.
It's not as brutal, it's not as drab, but there are certainly control mechanisms in place that
keep you thinking within the box. And you can't think outside of that box until you
become conscious. And then you can rebel. The hard part's over. You've become conscious.
You broke that cycle. Now you have to rebel. Now the good news is you don't need a rifle.
always going to need help. I'm a pretty independent guy and a lot of you may
know that after Hurricane Michael I was down in Panama City cutting down trees,
clearing debris, delivering food and medicine, whatever was needed meanwhile
back here yeah I cleared all the trees except one right near the house and it
was a big tree about five feet off the ground it split into two trunks and when
hurricane came through, it twisted it. One of the trunks leaned one direction, the
other one stood more or less straight up. Couldn't cut it down the whole thing at
once because I didn't know because of how the weight was distributed, I didn't
know where it was gonna fall and there weren't a lot of places for it to fall
that it wouldn't damage something. So I rattled my network, called this guy Jason.
So he comes over and he looks at the tree, he shakes it, walks out to his truck, grabs a strap, comes in, straps it up,
cuts the tree, both pieces, boom boom, land in this ten foot clearing, right on top of each other, don't hit anything.
How'd he do it? Magic. I don't know.
I needed that help though, because I never would have been able to do that.
I needed somebody with that skill set.
I needed that network, and if you want to be independent, you're going to need that
network.
Being independent doesn't actually mean doing everything by yourself, it means having the
freedom to choose how it gets done.
You are going to need that network and you're going to have to build it.
So how do you go about starting one of these things?
Well, you have to start off with a real self-evaluation.
What do you have to offer?
what do the people in your immediate circle have to offer that you can kind of use to
begin because that's how it has to start.
It has to start with you offering to do stuff and then in the hopes of it being reciprocated.
Everybody has something to offer a network like this, everybody.
But you need to do a real honest self-evaluation of your skills and what you can offer and
And you're going to need to know that before you start trying to build the network.
Gonna be pretty critical.
Okay, yeah, you built a birdhouse, but are you really a carpenter?
You know, that type of thing.
And then you have to get over your own hangups.
There was a comment on Facebook from somebody that had a medical issue.
And that's it.
And the overriding theme of it is something that comes from older people a lot.
It's basically, you know, at this point in my life, I'm broke.
I am broken, I can't go out and do anything.
I pretty much stay at home.
Okay, but you've got decades of life experience.
You have decades of experience at something.
And that expertise is valuable.
Sometimes it's not actually going out and swinging a hammer.
It's providing the advice to make that easier.
Don't sell yourself short.
And then there was a comment from a younger guy.
I want to say he was 17, 16, 17, something like that.
He's like, man, I want to do this.
But can I do it at my age?
You can definitely start it now.
You can definitely start it now.
And the thing is, even though you
don't have any skills to offer, it may seem that way.
It may seem, I'm a kid.
I don't have anything to offer.
Oh, yeah, you do.
If you were to start this now and were
to use social media to help link up with other people in your community that need something.
You can learn so much. You can get a free education. You can gather this wide array
of skills just by saying, yeah, I'll help you do that. The other group of people that
have a concern is introverts. You know, this is easy if you're a social butterfly. Not
so much if you're an introvert. Yeah, but if you're an introvert, you probably have
a job that allows you to be alone most times which means you probably have
skills that other people don't have. I'm gonna guess I mean you use social media
okay so you can network that way at least in the beginning build a digital
community very similar and maybe you can provide computer services or whatever
it is that you do via that method so you don't have to actually meet these people.
And then as time goes on and you interact with them online, you may actually end up
meeting them.
Everybody though, everybody has something to offer a network like this, that there are
no exceptions to that, everybody.
So how do you recruit people to join your network?
Well obviously you start off with your immediate circle, that's where it begins.
And that could be your friends, your coworkers, people from school.
Maybe you're in a club that focuses on some hobby and you can expand it beyond that.
You can look on social media.
You know there's a lot of local based Facebook groups and you could look in those.
Then I mean another good place to find people that would be interested in this is activist
communities.
Those people look for the helpers.
Those people that are out there already doing it, getting nothing in return most times,
those people are definitely going to be interested in something like this.
The other thing you have to decide is the structure.
How are you going to structure your organization?
I know a group of Army guys, and what they do is they've got poker chips with their names
on them.
And when somebody comes and does a favor for you, you give them a poker chip.
And later on, they've got that poker chip with your name on it.
So there's no formal agreement.
There's no money changing hands or anything like that, but there's a marker.
And that's one way to do it, is to engage in some kind of barter system that's there.
The other way to do it is set it up kind of on a more social level, where everybody's
involved and everybody just makes the commitment to help if they can. Rule 303,
if you have the means at hand, you have the responsibility. You can go further
than that, you can turn it into monthly meetings, you can engage in community
service in your community that helps get the word out about your network and kind
of shows that you're a group of people worthy of being around.
If you're at war with the system, you have to act like it, and wars cost money, you know,
it's not for bullets and bombs, but you need resources to do just about anything.
Who are the people that are most aware of the failings of the system?
Those of limited means.
that don't have the resources to do much and you're kind of trained to be like
that. You really are. In school, learn this. You can get hired. You'll be
employable. Working for somebody else may not be the best answer especially if
you're looking for independence. The solution is increasing the amount of
money you have coming in. What happens when somebody outside of one of your
networks needs something that you know how to do and somebody inside your
network hears about it? Who do they recommend? You. And never forget that
sometimes the skills of the entire network complement each other. You know,
through a twist of fate, I know a group of firefighters who everybody in the
Station happened to know something about repairing houses. One was an electrician, one was a plumber,
one was a carpenter, one was a painter. So of course these guys are there together all day anyway.
So they started remodeling and flipping houses and they make a lot of money doing it and some years
they make more money doing that than they do as a firefighter. Even if you don't have a skill that is marketable,
you know, you're not a handyman or whatever.
There are a whole bunch of low startup or no startup businesses that can be done with
almost nothing.
There are entire websites dedicated to just listing the different types of businesses
that can be started for like under $500.
That is a way to increase the amount of money you have coming in.
And let's say you just don't have the time to do that.
You can always invest in somebody else in the network.
Let's say you've got a guy that wants to start a landscaping company, but he needs a trailer.
You can get him that trailer and say, OK, well, I'll get you the trailer, but 10%.
You want to break away from that system, you don't have to be employed by somebody else.
A lot of the comments on Facebook were concerned about the limitations of these types of networks.
The reason I first became interested in them and started really thinking about them was
because of something called Stay Behind Organizations.
During the Cold War, NATO created these little networks all over Europe and their job, as
the name suggests, was to stay behind in the event of a Soviet invasion.
What they did was they put people at railroad yards, hospitals, police stations, city hall,
all kinds of different places, and they figured they needed to do it beforehand because they
learned during World War II that if you waited until after the invasion, the foreign intelligence
service would be looking for new people in these places, so they had to get in place
beforehand.
A lot of these guys were military, some of them.
Some of them were intelligence, but a whole lot of them were civilians.
And I want you to think about what NATO believed they could accomplish.
They thought they could basically ferment a revolution from inside the country and they
could run resistance operations.
That entails a lot, that entails a whole lot.
You're talking about treating the wounded, gathering intelligence, running arms, maybe
engaging in military operations, sabotage, all kinds of stuff, all of which carries an
amazing amount of logistics behind it if you actually want to accomplish it.
NATO was convinced that these little networks could do it, and they're set up the same way.
It's a group of people come together for a cause, they accomplish it, and then they go
go on about their lives.
Now if you know anything about stay-behind organizations,
you're probably going, you got to talk about Gladio, Bo.
Yeah, OK.
So there was one of these, called Gladio,
that went off the rails.
They got way out of hand.
Even without an invasion, they carried out assassinations,
all kinds of stuff.
Not the best example.
But that was one of like 50 of these things.
And the thing is, though, if you take away
the horror of the situation, it shows you
exactly how effective they are and what they can accomplish.
The logistics behind some of the stuff that they carried out
was amazing.
Again, I'm not saying that what they did was good.
I'm just saying that it proved the theory.
Because not just can these little organizations
these little networks, help you with the everyday things, it can be translated into things much
larger than that.
When you build your network, those people are going to have friends, and then eventually
they'll be part of that network.
It'll grow to the point where it's no longer just a network, it's a community.
And as you build that community, you're building your community, you're making your community
stronger.
If you make your community strong enough, it doesn't matter who gets elected, it doesn't
matter who's sitting in the Oval Office, it doesn't matter if you hate Trump or Obama,
whoever it is, they're in power.
You're going to be fine because your community can take care of itself.
You're changing the world you live in.
You're changing the world by building your community.
People in the comments section joke about me running for office.
I'm just a guy.
I'm just as corruptible as anybody else that has ever been given power.
We've lived under rulers in this country for a really long time.
Not sure that's really where we need to go.
Not sure top-down leadership is the answer.
Think maybe community building is the answer.
You know what's best for your community and your area.
You know it more than anybody in your state capitol, a representative that shows
up to do a PR photo op once a year and certainly more than anybody that's
sitting in the Oval Office. Another quote from 1984 is that if there is any hope
It must lie in the proles.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}