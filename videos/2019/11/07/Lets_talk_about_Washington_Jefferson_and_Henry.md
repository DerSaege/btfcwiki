---
title: Let's talk about Washington, Jefferson, and Henry....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=lxNOs-J8STg) |
| Published | 2019/11/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- November is National American History and Founders' Month proclaimed by Donald J. Trump, which initially sparked mixed emotions but Beau is warming up to the idea.
- Beau stresses the importance of understanding American history and the founding fathers to debunk the mythology surrounding them.
- George Washington, despite his military leadership, had a dubious past including surrendering due to not knowing French and allegedly committing war crimes.
- George Washington's treatment of slaves included brutalities and separating families by selling them to traders from the West Indies.
- Thomas Jefferson engaged in a relationship with one of his slaves, likely starting when she was 14 and he was in his 40s, bearing children he raised as slaves.
- Patrick Henry, known for "give me liberty or give me death," imprisoned his wife in a cellar for four years, leading to her believed suicide due to postpartum depression.
- The popular stories about the founding fathers, like George Washington's wooden teeth, are mostly myths; his teeth were reportedly from slaves.
- Beau points out the significant historical inaccuracies and the need to also recognize Native American Heritage Month.


### Quotes

- "Take some of the mythology out of it."
- "Those probably aren't the stories you know about the Founding Fathers."
- "You might be surprised."
- "There's a lot of mythology."
- "Just a thought, y'all have a good night."

### Oneliner

November is National American History and Founders' Month; Beau sheds light on the dark truths behind the popular myths surrounding George Washington, Thomas Jefferson, and Patrick Henry, urging a reevaluation of history and recognition of Native American Heritage Month.

### Audience

History enthusiasts, truth seekers

### On-the-ground actions from transcript

- Research and learn about the real stories of the founding fathers (suggested)
- Educate others about the dark truths of American history (implied)

### Whats missing in summary

The full transcript provides a deep dive into the dark realities of the founding fathers, shedding light on often overlooked or whitewashed aspects of American history.

### Tags

#AmericanHistory #FoundingFathers #Mythology #Slavery #NativeAmericanHeritage


## Transcript
Well, howdy there, internet people, it's Beau again.
So November is National American History and Founders' Month,
as proclaimed by our great president, Donald J. Trump.
Now, when I first heard this, I had, let's just say, mixed emotions.
But I am warming to the idea.
I am warming to the idea.
We do, in fact, need to know more about American history,
and we do need to know more about our founding fathers.
Take some of the mythology out of it.
So tonight, we're going to talk about George Washington,
Thomas Jefferson, and Patrick Henry,
perhaps three of the best-known founding fathers.
George Washington was a man who believed
that he should be a military commander
because he read two, count them, two, one, two books on war.
The British Army thought that was a little silly.
But George Washington's brother didn't know the governor.
So he wound up getting a commission as a commander.
And they stuck him out in the middle of nowhere,
where he couldn't do any harm.
Then the French and Indian War popped off.
after what is best described as a comically one-sided battle, George
Washington is forced to surrender to the French. Because, it gets better, because
he is too embarrassed to admit that he can't read French, he just signs the
surrender document, which not only surrendered his forces, but also admitted
that he committed war crimes and assassinated a French diplomat. There is
a utterly preposterous idea that some people, some slave owners, were nice to
their slaves. First, that wasn't a thing. Even if it was, it certainly didn't apply
to George Washington. In addition to the normal beatings and all of the horrible stuff that
went along with it, if you bothered him and you were his slave, he would go out of his
way to find traders from the West Indies to sell you to so you could never see your family
again. When abolition began, there was the gradual
Abolition Act of 1780 in Pennsylvania. Washington used loopholes and technicalities to make sure
that, while his slaves weren't freed, he would go as far as to make sure they were never in
Pennsylvania for more than six months. He would take them out of the state so he could restart the clock.
Now, there are some people who had a different kind of relationship with their slaves.
Thomas Jefferson would be one of them.
Presumably after the death of his wife, he began a relationship, I don't know what to
it with one of his slaves who happened to be her half-sister. They had several
children who he raised as slaves. I should also mention that when this
relationship most likely started she was 14 and he was in his 40s. Patrick Henry
is best known for saying give me liberty or give me death, but that apparently does
not apply if you are suffering from postpartum depression, which is what his
wife was most likely suffering from after the birth of their I think sixth
child when he locked her in a cellar for four years. It is believed that without
liberty she chose death and ended herself. She is buried in an unmarked
grave. Those probably aren't the stories you know about the Founding Fathers. You
know about George Washington's wooden teeth. They weren't wooden, they were the
teeth of slaves. There's a lot of mythology. We get a lot of history
wrong. November is normally and still is Native American Heritage Month. I'm going
to put a link in the comments down below to show that we get those stories wrong
too. Anyway, so happy National American History and Founders Month. Definitely
look up a little bit about our founders. You might be surprised. There's one guy,
Dr. Rush, who spread yellow fever all over Philadelphia.
Anyway, it's just a thought.
Just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}