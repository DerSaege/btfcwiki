---
title: Let's talk with Re-Education about journeys and lefties....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=D-pHZIEvfpc) |
| Published | 2019/11/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Aaron says:

- Aaron talks about his journey from being a conservative-leaning person to eventually becoming an anarcho-communist.
- He shares how his upbringing in a community with racist influences led him to adopt harmful beliefs and associate with neo-Nazis.
- Aaron recounts incidents of violence and vandalism he witnessed and participated in during his past.
- He describes his transition to rejecting his previous beliefs and embracing a more inclusive and empathetic worldview.
- Aaron credits skepticism and exposure to alternative arguments for helping him change his perspective.
- He explains his views on communism, socialism, and anarcho-communism, and distinguishes between them.
- Aaron criticizes the misrepresentation of socialism and communism in the United States.
- He provides insights into the differences between communism, socialism, social democracy, and democratic socialism.
- Aaron shares his support for worker-owned co-ops as a practical solution for societal issues under capitalism.
- He expresses the challenges of discussing complex global issues like protests in China due to propaganda and lack of in-depth knowledge.

### Quotes

- "Everybody is different, and they're all going to come to their positions in a different way."
- "It's about working people against the system that is corrupted and pushing down on all of us."
- "News crews have entire groups of people that deal with editing, they deal with sound, they deal with all of these sorts of things."
- "A lot of the military back coups done by the United States or done by any other major superpower always involves a certain amount of people on the ground that are actually angry and that actually have real concerns."
- "Putting physical bodies in physical spaces is the only way that we're going to actually see any sort of real reform, any sort of real change in the world."

### Oneliner

Aaron's journey from a conservative with racist influences to an anarcho-communist advocate, criticizing misrepresentation and advocating for direct action.

### Audience

Activists, Advocates

### On-the-ground actions from transcript

- Join protests and demonstrations to physically show support for social change (implied).
- Support worker-owned co-ops as a practical solution for societal issues under capitalism (exemplified).

### Whats missing in summary

The full transcript provides a detailed account of Aaron's personal growth from holding harmful beliefs to embracing an inclusive worldview, advocating for social change, and critiquing misrepresentations in political ideologies.

### Tags

#PersonalGrowth #AnarchoCommunist #SocialChange #DirectAction #InclusiveWorldview #Skepticism #WorkerOwnedCoops #Misrepresentation #Protests #Advocacy


## Transcript
So, Aaron, why don't you tell us a little bit about yourself?
Sure.
Well, first of all, I live out in the middle of the country,
so internet is very sparse down here.
So my name is Aaron.
I'm from the show Re-Education.
I do a show on skepticism, socialism, anarchism, communism,
that sort of thing.
And we talk a lot about politics.
We talk a lot about how we can reform and basically
use revolutionary tactics towards finding a better system
or making a better system.
I like it.
Sounds good.
So you had an interesting beginning in your journey
to where you're at now.
How did you get here?
Right, so OK.
So it's a long story.
When I was growing up, I'm probably going to say,
I changed my ideology mostly around 2012, something
like that.
But up until that point, I was very much a conservative leaning person.
I went so far as to say that I was alt-light in a couple of videos, but I would actually
now that I've thought about it a little bit more, I'd even go further and say that I was
very much alt-right when I was growing up.
And that came about from basically living in a community that was full of a lot of people
that had very mixed ideas on how race and gender and class and that sort of thing worked.
My parents were, for lack of a better term, really quite racist people, so I kind of adopted
all of those stereotypes and just kind of fell into the wrong crowd.
A lot of the people that I associated with were bonafide neo-Nazis, skinheads, the kind
guys that would go around erecting and burning crosses and that sort of thing. And I've been
around a couple of times when they when they've done that sort of stuff. And it was it was intense
to say the least. But back in those days, I was, you know, rambunctious kid. I was really interested
in a lot of that kind of stuff. I thought that it was edgy. I thought that it was cool. I thought
thought that it was interesting and taboo. So it made me want to
join with them and kind of roll with those kinds of people. And
yeah, it it basically turned into being a very difficult
situation for a lot of people. Just give you an idea of the
kind of people that I'm talking about. My one friend, I'm going
to not use any names obviously here, but one of the friends
I had actually had an incident where them and a couple of their buddies went around the town
finding people that were married, you know, to different colored people, interracial marriages
and so forth, and planted shrapnel bombs on the front of their steps so they would blow up when
these people came to their door. Luckily, all of the people that were doing that, these neo-Nazi
kids were about as dumb as bricks so they weren't able to actually make the um make the
shrapnel bombs go off properly but it is uh some people did end up getting hurt from it so
weren't good guys it was a really bad yeah it was a really bad situation and lots of other things
too like it was just it was really rough like a lot of vandalism lots of stuff like that so
it was a really rough crowd um and i i thought to myself that all of this stuff is just you know
kind of normal, right? Like this is the normal that I grew up with, this is just the way things were.
And it wasn't until I started getting out into the real world, into like the workforce and that sort
of thing, and really started to talk to other people that I realized that everybody is just
a human being, right? It's not about somebody being a certain color or somebody having a certain
religion or somebody believing a certain thing. It's about us all being people and it's all having
to work together and live together on this planet, and we're just trying to get by. And
I lost a lot of those ideas when I started to progress further into the workforce,
and talking to more people, and moving around a lot, and that sort of thing, so.
Wow, yeah. Planning explosives is definitely a step up in the escalation factor there.
Yeah, yeah, yeah, looking, looking back at that and then looking at the things that are
going on now, like with milkshaking and stuff, I'm just like, yeah, okay, shaking my head
a little bit when I'm thinking about it.
And then so you kind of leave this group and you start thinking about things a little bit
more in depth.
And how did that like I've still I mean, that transition, it's, I mean, we all make them.
I mean, that's a big one.
Yeah, that is a long way to go, isn't it?
Right. So, OK.
So I guess that's really the beginning of the whole transition
through being that way, being very much a racist borderline
Nazi character to finally becoming what I am today,
more of a revolutionary, more of a person that's on the side of
we all need to get along and thinking more communistically
about things.
But getting there was a long, long road, right?
And it took a lot of different factors
to actually get me to that point.
A lot of people think that there's some magic bullet out
there, that there's some sentence that somebody
is going to be able to say that's just going to convince
everybody around that this is the right way
to go about things, this is the right way to do things.
And there's not, right?
Everybody is different, and they're
all going to come to their positions in a different way.
My journey had a lot to do with skepticism, right?
I was a big-time skeptic all growing up.
So one of the things that I noticed through YouTube
and that sort of thing is a lot of these people
that I used to idolize that were skeptics
weren't holding to the same kind of skepticism
when it came to other things, when it came to race,
when it came to gender, when it came to anything
basically other than religion.
And I started to notice that because I'm like you, Bo,
I'm one of the kind of guys that'll go out there
and I'll actually fact check stuff.
And I'll try, sorry, we're trying not to swear here.
Well, I'll actually go and I'll try to fact check stuff
and I'll try to find out if it's true.
So when I hear something that I don't think
is necessarily correct, then I'll go and I'll check it out.
I'll find out if it is true
and I'll make a decision from there.
And I try to be intellectually rigorous even back then.
And if I found an argument that convinced me
that was a good argument, good sound argument,
I would want to change my mind,
but that's not everyone, man, right?
Like a lot of people have all of these ideas made up
over the course of an entire lifetime.
And that lifetime is really what defines that person
as a human being.
So when you already have all of these things
making you up as that person,
and then you have that worldview challenged, right?
That's not only going to disrupt your,
the way you think about things,
that's going to make you angry,
it's going to make you lash out.
And a lot of people are very much that way.
So luckily I was already this skeptically minded person.
So when I heard alternative arguments
to the things that I was saying,
I thought that they were rather convincing.
My entire journey back when I was very much right leaning,
one of the things that I was arguing for a lot
was the fact that one of my friends
couldn't wear a t-shirt in school that had a great big swastika on it. Now, I was arguing
back then that I think that it's a good thing that he should be able to go and wear this
because he should be able to wear things that say white pride or that sort of thing, swastikas,
that sort of thing, because it's not only freedom of speech, but also on top of that.
Oh, hopefully I didn't lose you.
Oh, that's right.
So I was fighting for this friend of mine
so he could be able to wear this t-shirt with a swastika on it.
And one of the arguments that I was always making
was that he should be allowed to wear that because people
in my high school, they were all Native people.
They were all wearing Native pride t-shirts.
So I thought, well, that's absolutely ridiculous.
You can't just be wearing a Native pride shirt
and get away with that and not allow somebody that's white
to be wearing a white frige,
allowing them to get away with that.
But the problem is though,
is that I didn't realize the cultural connotations
that came along with this.
This is all coming from the broken mental faculties
of a young teenage boy who doesn't understand the world,
who's looking at everything through the lens
of somebody who lives a relatively privileged life.
So I don't understand that there's a lot of things
that are involved in just, not just racism,
but the power dynamic between white people and black people
or white people and native people and that sort of thing.
So I remember being back in summer school,
fighting for this and almost getting
into a physical confrontation with the teacher,
actually trying to fight for this friend of mine
to be able to wear these sorts of things.
And it wasn't until later on in life
that I heard other arguments like the one I gave you
a moment ago about how there is that power dynamic
and there is that serious division between people
that is reinforced by state structures
and that sort of thing that really makes
not just these racist statements hurtful,
but it perpetuates a problem throughout the rest of society
that continuously pushes down on these certain individuals
and makes their lives harder.
So even just me as an individual going out and doing
that definitely does make things harder for those.
So that is how my journey basically started.
And after listening to a lot more skepticism
and that sort of thing, I found myself mostly
doing this journey online because that's where I live.
I'm very much an online person.
And I found people like Richard the Dick Coughlin.
He was making really good arguments against people like Stefan Molyneux.
And I thought that that was really interesting.
I've seen people like Kevin Logan and Christy Winters who were, if your audience doesn't
know who any of these people are, that's fine.
There are people who, a lot of people would be calling SJWs or social justice warriors.
And I thought that a lot of the things that they were saying, though it was aggravating
to me and sometimes cringy, I thought that they made fairly decent arguments and it started
pushing me towards that leftist side. And there's a lot of things that did that as well.
Learning more about social democracy and democratic socialism through the Bernie Sanders campaign
and other YouTubers that covered that sort of thing. Even watching some Young Turks videos,
which I would probably never do now, but back in those days it did help me hear a different
opinion and actually get a different idea of how those
sides think. Um, and eventually I started to realize that all of
this is basically just a bunch of people that are looking at
life through certain lengths, right? When you're born, you're
dumped into a box and everybody is told that they have to stand
there and defend and fight that fight that box. But none of
that makes any sense because when you're born, you're just a
being with different kinds of stresses, different kinds of pressures, different kinds of everything,
and everybody is going to come out of that in a different way.
So me thinking that all of these people were bad or not as good as I was or anything like
that all throughout high school and that sort of thing, once I started to hear about all
of these other arguments about how they had to fight for social equality, all of these
things with Martin Luther King and with fighting for civil rights and that sort of thing, that
started to put a fire in my belly, right? It started to make me really realize that
not only is the social justice thing, though I thought it was cringey at first, not only
is it a good thing, it's something that we should all be fighting. So I started to gear
a lot of the things that I was saying back in those days towards being more social justice
oriented, and trying to talk a little bit more about the world through the lens, somebody
who actually cares about things instead of just being cynical and saying that everything is tear.
So where are you at now? If you had to label yourself and you choose to, what would it be?
Yeah okay so I'm very open with my labels. I'm an anarcho-communist. Anarcho being the
abolishment of all unnecessary or unjustified hierarchies and communist is the worker control
and ownership of the means of production and I chose those labels because I feel
that they are the most fitting for the ideas that I've ideals that I've always
had so you will be able to answer a question that pops up in the comments
section all the time tell me the difference between communism and
socialism and anarcho-communism and and all of it. Break it down for us. Okay so all right so perfect
I'll let you know. All right so um communism and socialism are the same yet different right. When
you're in the United States basically everybody considers them to be the same exact thing uh those
two words are interchangeable with one another but the way that they were initially uh spoken about
was I'm not going to say initially but basically the the best way to explain it is uh communism is
basically the goal whereas socialism is a path towards that goal right so communism is for those
who don't know who don't think or for those of you who uh have heard about it you probably think that
it's the spooky thing that the soviets and the chinese do and it's terrifying and you don't want
anything to do with it because it's an oppressive regime. Well, originally it wasn't meant to be
that. Originally it was meant to be a stateless, classless, moneyless society where the workers
own the means of production. That means that they own all of the factories, all of the goods,
and all of the tools that are used to produce goods inside of a system. And they run off of
the axiom from each according to their ability to each according to their need. So that's what
communism is and that's the goal that people want to eventually aspire to.
Socialists for the most part want an eventual communist society but to get
there you need a certain pathway right and that's where socialism comes in. Now
there's a lot of people that are gonna be listening to me right now saying
that's not what any of this means these mean totally different things those
aren't the right words that this is the incorrect definitions. You're probably
right there are millions of different definitions for socialism and communism
For every different socialist country that there is, there's a different version of socialism being instituted there,
just like capitalism.
There's a different type of capitalism in every different country, so just like in Dubai, you're going to have a
capitalist country that is radically different from the one that you see in the United States, so socialism is the same
way.
But generally, socialism is the pathway towards getting that stateless, moneyless, classless
society. And so it's basically looked at as a transitional state. If you're going to put this
in economic terms, socialism is the social ownership of the means of production, whereas
communism is the worker ownership of the means of production. Social ownership can mean in many
different ways so you can actually have socialist states that are partly socialist, partly capitalist,
you can have social programs which often get confused with socialism itself, which actually
lands more on the side of the capitalist side than post-capitalist. So there are a lot of different
types of socialism, there are a lot of different isms when it comes to all of this, and it's
It's extremely complicated, so I'm sorry if it is a little bit boring, but that's basically
what that is.
So what is AOC?
AOC, okay, so AOC, that's a great question.
AOC is something called a social democrat.
Now she'll call herself a democratic socialist, but that's not what she is.
She's a social democrat, and that is because she is still technically a capitalist.
She says that she wants social programs, social change, that sort of thing.
But when she's talking about those things, she's talking about them in a reformist way.
She's talking about them in a way where we can take the structure that we have now and
tweak it and alter it and change it and do all of these technocratic solutions so we
can make it a little bit more palatable to everyday people.
Which if you ask anybody who's post-capitalist, they'll tell you that's not going to work.
It's just like putting a bandaid on a bullet hole.
It's just not going to help you at all.
But that's basically what AOC is.
And that's the same thing with Bernie Sanders as well.
Bernie Sanders is also a social Democrat.
All right, so if you're an Ancom and an anarcho-communist, how's that different from the original version
of communism?
Or is it?
It's not.
Right?
As far as I'm concerned, we're more pure about things, right?
We like to think of things as more or less like,
we believe in the original unfettered version,
uncorrupted version of communism,
and that involves the structure of power as well, right?
So when you're talking about communism
and when you're talking about anarchism,
you're talking about two different but connected things.
Communism is talking specifically about the economy
and how the economy works,
whereas anarchism is talking about power structures, right?
So communism, that's how you deal with your goods.
And anarchism is basically the abolition
of all unjustified or unnecessary hierarchies.
It doesn't mean getting rid of all hierarchies.
It just means finding all of the ones that are good
and making sure that we keep those
and eliminating all of the bad ones.
Right, right. So you are a real leftist, like by the real term.
Well, I hope so. And but you know what, here's the thing, though, is that is technically, yes,
technically, I'm a leftist. But the way that I like to look at things is that it isn't necessarily
about left or right. It's about working people against the system that is corrupted and pushing
down on all of us.
Right.
I don't like to sow any kind of division.
I don't like to say conservatives are wrong because they're conservatives.
I like to say conservatives are wrong because they're fed the same kind of
propaganda that everybody else is they're.
Just wrong in a different way.
Right I think that liberals are wrong too.
They're just wrong in a different way.
We need to be more welcoming and come together as a people come together as all working people
and fight for the rights of everybody rather than just allowing this system to
continuously pushed down on. So if you had one solution to offer that would help solve most or
at least you know a good portion of society's problems it would be workers of the world unite
is that what I'm hearing? Yeah I would definitely say that would be useful but a practical solution
would be using something called worker-owned co-ops right. Now this is a more reformist solution but I
definitely like to push this idea because it's really smart. A worker-owned co-op is basically
a company that instead of having one owner, right, the capitalist that goes in and buys
the business and hires a bunch of workers, you have a group of people that get together
and work together to own and control that business themselves. So that allows them to
decide what they'll produce, when they'll produce, how they'll produce it, and what
they're going to do with the profit, which is fantastic, because if that's the case you
you don't have one CEO, 100 miles away,
collecting all of your money,
all of the surplus labor value,
the money on top of all of the money that you've made
that covers your labor cost
and the cost that it takes to run the business,
they're taking, instead of them taking all of that money
and putting it in their pocket,
in a worker co-op,
that money is spread out through all of the workers.
So you actually make more
and you get that added bonus
of feeling like you own something.
And I think that, as far as I'm concerned,
is the number one solution that all post-capitalists should
be pushing towards right now, as far as reformist ideas go.
Yeah, and we're actually starting
to see a lot of those pop up, even here, like in the boonies.
So I've got to ask, because I know
it's going to show up in the comments section,
in this worker-owned co-op, isn't there going to be a boss?
Yeah, absolutely. Yeah. So, it depends on how you want to set it up. So if you are a group of people, say you're just a
couple of people that are making shoes, or jewelry, or whatever it is, a commodity, and you all know basically what you
need to do, and there's only three or four of you, and you can basically work together, then no, you don't really need a
boss.
But that's where this whole justified hierarchy system comes in.
If you find out that there is the necessity of having somebody there to organize, somebody
there to delegate, somebody there to point people in a direction and say, this is what
you need to do, this is how you need to do it, and this is where it needs to go, that's
when you get somebody that would be a boss.
But instead of just having that person appointed by the owner or whatever it is, these people
can be democratically elected into positions of authority. It's called democracy in the workplace
and it allows all of the workers to have democracy not just in their daily lives, in
their everyday space voting on whatever it is your mayor or your president or whatever, you now get
to vote on your management, on your bosses, on your supervisors, on all of those sorts of people.
that way if they make a mistake they are 100% not only responsible for it to you
they're responsible to the owners and to all of the employees as well so it's a
much better system it's much more democratic system and at the end of the
day they still have to wash their dishes in the sink just like everybody else
studs. So give me a topic of the day that you haven't done a YouTube video on
that you want to and expound. Give us some some new content.
Oh boy, right off the top of my head. It takes me hours every morning to think about what I'm going to talk about.
You know, so I've been really wanting to do a video about the protests that have
been going on in China. But my problem with doing a video about, and I've been kicking around the
ideas in my head a lot, but the problem with doing a video about that is there is so much propaganda
on all sides that I just can't make heads or tails of it. And I'm not like you where I'm
steeped in all of this international politics and just knowing how all that stuff works.
When I want to go research something about a country, I have to sit there for hours learning
everything about that country because I've never looked it up before because our education system
is broken, right? And we don't learn about these sorts of countries. We don't learn about their
systems. So if I wanted to do a video on it, I'd have to do a whole bunch of, uh, this extra
research. The problem with China is that the United States wants to make China look bad.
They want to make sure that all of the narrative that is pushed in the media makes sure that the
government, the communist government, is looking terrible while all of these protesters are
looking like heroes and that sort of thing, not saying that they aren't. I think that anybody
fighting against a repressive regime is absolutely fantastic, but there's no way for me to just know
there because we're just fed all of this propaganda, so much propaganda, all of the time
that I don't even know really know heads or tails where to start on them. Well if
it makes you feel any better there are contractors who are looking at that and
going man because there are a lot of there are a lot of telltale signs that
looks like there's a lot of let's just say outside influence in some of the
protests and you know nobody wants to come forward and be like yeah do that
and then find out that a large portion of it was pushed by a US agency when
then most of, most of the guys I know and still talk to have kind of decided not to
work for the government anymore. And they don't, they don't want to get, they don't
want to cross that line again. But there's definitely, you know, from the, from the area
experts and people that we know that, that really are into China, there's definitely
a huge organic component to it. So, you know, I feel kind of bad because I'm like, this,
seems real. This seems real on every level. You have a whole bunch of real complaints,
real people out there in the street, but because we're seeing the fingerprints of outside manipulation,
everybody's kind of holding back. And, you know, you don't want to go full bore into supporting it.
And in a way, I feel bad because I'm like, you know, this is something, the organic protests
should definitely be supported, but how do you separate it from what's being manipulated?
It's it's difficult. It is that that's a tough one. I haven't done a video on it yet
yeah, exactly not a lot of people have because it's such a touchy subject to touch right because you want to get out
there and you  Want to be supportive of?
Personally, I want to be supportive of everybody that is fighting against any power structure anywhere. That's what we
need to be doing  Right. We need to be getting ready. We need to be getting we need to learn how to do that, right?
So and and I think that that's wonderful
But is it astroturf? Is it being led on? And that's the thing, right? Is a lot of the military
back coups done by the United States or done by any other major superpower always involves a certain
amount of people on the ground that are actually angry and that actually have real concerns.
That's how they push those ideas as being legitimate. With every lie, if you have a
certain amount of truth in it it makes it more real so and yeah that is a really tough one to
talk about but it's telling that that's the one that we see everybody talking about but we don't
see the genocide that's happening in Yemen we don't see any of the attacks that are happening
throughout the rest of the entire world we're only seeing these things that well for it benefits the
the United States to have that portrayed on their media.
It's just the way it is.
Oh, absolutely.
I mean, absolutely.
There's no doubt that the UK and the US
are coming out way ahead in this battle,
more so than even if the protesters come out
ahead with their grievances.
The US and the UK are still going
to be the ones that really come out ahead.
But yeah.
So what are you planning on doing in the future?
Your channel, it's growing.
I see it.
So where are you headed?
You know, okay, so I started off my channel
just trying to do everything as like relaxed
and as easy as I possibly could,
just trying to like get videos out there,
sit down, record something and go.
And the way that I am,
I make things a little bit more complicated
and a little bit more complicated
and a little bit more complicated
until the point where now I just finished filming
special Halloween special. That's basically like a full on movie. It's like 23 minutes long. I went
and shot all across the entire province. It was a lot of fun to do. But yeah, things get more
complicated. So I'm just hoping that my little cell phone camera videos aren't going to get more
complicated because that's already hard enough as it is. I honestly don't know how you put out a
video every day man like I don't sleep yeah you can't sleep that's the thing
right you got to be on on it all the time always researching always knowing
right that's the thing about doing online stuff too I'll just say is that
you have this expectation to be perpetually correct with everything that
you say and that you always have to have some sort of opinion on everything and
It's like, sometimes I wake up and I'm like, man, I don't care.
I don't care.
Like, I just, I wake up sometimes and I'm like, it's all F'd.
There's just, the only way to fix things is through a full on revolution.
Why am I talking about, like, this petty grievance that's happening between a
mayor and some other guy, like, a hundred miles away?
None of this matters, but it does matter because even though politics is very
stupid. It is very important and it's something that we unfortunately have to pay attention to
even though every single day feels like someone is opening up a full fire hose into your mouth
of information that is just impossible to deal with. Yeah yeah I can definitely feel you on that
one too. Yeah there have been a couple of times especially right after the latest thing with
of the WPAC daddy, you know, my inbox immediately filled up
because that's, that is my area, you know, and like, they're
wanting like an immediate reaction to what's going on and
what's gonna happen. And, and I'm like, you know, he's not
even cold yet. You know, give me a minute, let me figure it out.
Let me, let me talk to some people find out what's going on.
But there is that pressure to always be 100% correct. And
you're extrapolating stuff from different sources
and trying to come up with the best guess that you can.
And actually, when I finally came down to that one,
I'm like, any of these three things can happen.
But because we don't have the information yet,
we don't really know what happened.
And that's like what I was saying
just before we started the stream, right?
Is that news crews, and what we're doing
is kind of similar to like a news thing, right?
It's commentary, but it is kind of news.
And news crews have entire groups of people
that deal with editing, they deal with sound,
they deal with all of these sorts of things.
They find you stories and that sort of thing.
So it's so difficult for independent creators
like you, like me, like Peter Coffin and other people
to actually get out there and produce really well-researched,
really well-thought out content on a regular basis
without having that support behind us like they do, right?
like Fox News or Blaze TV or whatever it is,
they have millions, millions of dollars behind them.
And I'm just some sitting here in my office
like hoping that I'm getting things right, you know?
Well, you know, that's actually one of the directions
we're taking the podcast now is we're going,
I've noticed that the,
we're getting the best solutions from the ground level. You know, they've got millions of dollars
of research and millions of dollars at their disposal and every channel is the same. There
should be some variation. You know, somebody should be coming up with something ahead rather
than just regurgitating the same talking points. You know, and the thing is too, is it actually
reminds me of the way that guerrilla forces operate when they're fighting against an
overwhelming power right like the United States military or any other kind of
military you have to not think in the same ways that somebody with unlimited
money can think in right you have to be light on your feet the the kid with the
bolt-action rifle on the top of the hill is gonna be able to get an entire
military base to expend millions of dollars worth of ammunition at them by
just throwing it at them just by shooting a couple of shots at them
right with a bolt-action rifle that's the kind of way that we have to think
about those sorts of things as guerrilla tactics. We have to be able to be light
on our feet and running out there with our running shoes on and not combat
boots and hopefully we'll be able to maneuver around it and actually get you
know a good solid audience.
All right so parting shot you got anything you want to say to the audience?
You know what? Not a whole lot outside of that. I am a huge proponent of direct
action. If I could ask anybody out there to do anything, it would be to not
subscribe, not to donate, though those things definitely do help the
production of the shows and stuff like that. But it is to actually get out in
the street and make yourself physically shown. Putting physical bodies and
physical spaces is the only way that we're going to actually see any sort of
real reform any sort of real change in the world and if things are bothering
you out there right now if things are making it difficult for you to live and
you have the time right a lot of us don't have the time but if you do get
out there and actually try to make a difference because it's up to all of us
and we can't do it alone so we have to have the support from everybody out there
so definitely do that and if you do get a chance check out my channel it's
It's a re-education on YouTube.
And we do a lot of really interesting stuff there.
Some of it's pretty dramatic.
Some of it's lighthearted.
And some of it's just me talking off the cuff
and swearing for about 20 minutes.
So.
All right.
All right, sounds good.
So everybody, that's the conclusion
of the interview portion.
We'll be back shortly here in just a minute.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}