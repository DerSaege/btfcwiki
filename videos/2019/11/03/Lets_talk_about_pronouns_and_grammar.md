---
title: Let's talk about pronouns and grammar....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=uOmj5Bva0YA) |
| Published | 2019/11/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the history of the English language, starting with Old English in 450 to 1100 when Germanic people mixed with locals.
- Describes the transition to Middle English from 1100 to 1500, influenced by French brought over by William the Conqueror.
- Talks about early modern English from 1500 to 1800, with shorter vowels and more foreign words due to colonization.
- Mentions late modern English from 1800 to now, with a wider vocabulary from the industrial and technological revolutions.
- Details the development of prescriptive grammar in the 1700s, which dictated proper speech through books like Rob Lough's on English grammar.
- Explains how Sir Charles Coutts' expansion on Lough's work included using "he" and "him" as generic pronouns, later ratified by Parliament in 1850.
- Points out that historically, the English language had a singular gender-neutral pronoun, "they," for the majority of its history.
- Criticizes the idea of rewriting the entire English language for one person's preferences, as it historically happened through influential figures like Coutts.
- Ends by questioning the necessity of changing the entire English language for personal preferences.

### Quotes

- "We wouldn't really be able to read it today unless we were trained to do so."
- "For the overwhelming majority of the history of the English
  language, we had a gender neutral pronoun, a singular gender neutral pronoun, they."
- "It's not actually rewriting the English langauge to suit one person's personal preference."
- "That's how it happened. So they, them, theirs, that's actually the norm."
- "I guess we can rewrite the entire langauge because of one person's preference."

### Oneliner

Beau dives into the history of the English language and questions the necessity of rewriting it for one person's preference, advocating for the historically used gender-neutral pronoun "they."

### Audience

English Speakers

### On-the-ground actions from transcript

- Advocate for the use of gender-neutral pronouns like "they" in everyday speech (exemplified)

### Whats missing in summary

The full transcript provides a detailed historical background on the development of the English langauge and challenges modern perspectives on grammar and pronouns.


## Transcript
Well, howdy there internet people, it's Beau again.
So tonight we're going to talk about grammar and pronouns and the history of the English
language.
Had a guy tell me today that he didn't think we should have to rewrite the entire English
language because of the personal preferences of one person.
Fair enough, fair enough.
But that got me thinking about something I knew but didn't really know.
So I started reading about the development of the English language.
So it started with Old English.
That's 450 to 1100.
It's when the Germanic people first came over mixed with the locals that were already there
And that's where that language came from.
We wouldn't really be able to read it today unless we were trained to do so.
Middle English was next, and that's 1100 to 1500, and that's the addition of French.
French came in because a guy named William the Conqueror from Normandy came over.
He brought French with him.
It became the language of the royal court.
There was a class divide.
The peasants used English and the ruling elites used French.
Then you get early modern English which is 1500 to 1800 and the vowels became shorter
and there was the introduction of more foreign words because of colonization.
Then you get late modern which is 1800 to now which is wider vocabulary because of the
industrial revolution and the technological revolution.
needed words to describe things like cell phones and then still more addition
of foreign words. So that's the history of the English language. Now in the
1700s, in the middle of 1700s, a fad developed called prescriptive grammar and basically
they were little books that told you the proper way to speak rather than descriptive grammar
which is the way people actually talk.
Now one of these books was written by a guy named Rob Lough, and he later became Bishop
of London, and he wrote a short intro to English grammar.
Shortly thereafter, a guy named Sir Charles Coutts expanded on this volume, and he included
his personal preference of using he and him and his as the generic pronoun.
In 1850, Parliament ratified this, codified it, turned it into law.
The rest of the time, for the overwhelming majority of the history of the English
language, we had a gender neutral pronoun, a singular gender neutral pronoun, they.
It's not actually rewriting the English language to suit one person's personal preference.
It's stopping that because that's what happened with the expansion because that book became
a textbook. So all of the ruling elites became trained in this manner and his personal preference
bled through the entire language. That's how it happened. So they, them, theirs, that's
actually the norm. That's the norm. For the overwhelming majority of the history of the
English language we had a singular gender neutral pronoun but I mean I guess
we can rewrite the entire language because of one person's personal
preference. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}