---
title: Let's talk about solutions to complex problems and slogans....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=fDGBH42GibY) |
| Published | 2019/11/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Americans prefer simple solutions for complex problems, but real issues require thought, planning, and multiple tactics.
- Trump exploited perceived ignorance by proposing a wall as a simplistic solution to a complex issue.
- The wall's failure resulted in wasted money, disrupted lives, and damaged America's image.
- Pelosi's fear stems from underestimating Americans' ability to understand complex issues, especially regarding campaign finance laws.
- Soliciting anything of value from a foreign national for a campaign is illegal, regardless of the specifics.
- Americans are capable of understanding complex issues but often choose to defer to authority rather than think critically.
- Beau calls for a deeper examination of history and policies to address root causes instead of relying on simple solutions.

### Quotes

- "Soliciting a thing of value from a foreign national is illegal, period, full stop."
- "The only reason they'd do that is if it had value."
- "The average American is not dumb."
- "We'd rather cheerlead than think."
- "If you understand history, you can understand the future."

### Oneliner

Beau addresses the preference for simple solutions over complex issues, calling for critical thinking and deeper examination of root causes.

### Audience

American citizens

### On-the-ground actions from transcript

- Challenge yourself and others to think critically about complex issues (implied)
- Educate yourself on history and policies to understand root causes (implied)

### Whats missing in summary

The full transcript provides a nuanced perspective on American society's tendency towards simple solutions and the importance of critical thinking and historical understanding in addressing complex issues.

### Tags

#ComplexProblems #CriticalThinking #CampaignFinanceLaws #AmericanSociety #History


## Transcript
Well, howdy there, internet people, it's Beau again.
So tonight we're gonna talk about solutions.
As Americans, we like simple solutions.
We like easy solutions, but the problem with that is
our problems are often complex, multi-faceted,
have a lot of moving pieces.
Solving complex problems often takes a lot of thought.
planning, and multiple phases, different tactics to address an issue.
It's not done in sound bites.
If there's a problem, real or imagined or manufactured, that has persisted for decades,
it probably won't be solved with an easy fix, with a campaign slogan that can be chanted
and repeated. It requires us as people to look deeper into these issues, into the underlying
root causes of them sometimes. It requires us to educate ourselves, to question our betters.
Because those in DC right now, they've lost faith in the American people. They don't believe
that Americans are smart enough or vested enough to understand simple issues that may
have a complex root.
And I'm not just talking about Trump right now, I'm talking about Trump and Pelosi.
Their reactions were very different to this same realization.
Trump, he prayed on it.
played on that perceived ignorance and created a crisis with a simple solution, a wall.
A great, big, invulnerable, impenetrable, perfect wall.
And that wall was defeated by a re-sip-sol.
whole setup maybe 80 bucks maybe 80 bucks wait until those guys get their
hands on one of those saws they used to extract people from cars you'll lose 60
feet of wall in two minutes it's not that simple solution what did it do
billion spent, land taken, lives disrupted, our image forever changed in
the eyes of the world, money diverted, a political show, the degrading of American
values for a chant, for a simple solution so we could cheerlead and it didn't work
and everybody knew it wasn't going to work now on the other side
you have Pelosi. Her reaction was different. She's terrified that Americans
are dumb. That's what it boils down to. She's worried that Americans won't be
able to understand the case against the president. She's worried that there's too
many people, too many moving pieces, that we won't be able to follow it. When at
the end of the day, it's not complex. It is simple. Under campaign
campaign finance laws.
Soliciting a thing of value from a foreign national is illegal, period, full stop.
Doesn't matter why you did it.
Doesn't matter if you engaged in quid pro quo, none of that matters.
That alone, soliciting a thing of value for your campaign from a foreign national is against
law.
It's unlawful.
Can't do it.
And he did that and admitted it and provided a transcript he wants us all to read.
Her concern is that Americans aren't smart enough to follow that line because when you
get to the end of it, well, is it a thing of value?
How is an investigation money?
Because it doesn't have to have a monetary value attached to it.
An endorsement is a thing of value.
If this wasn't something valuable to Trump's campaign, why did he have his personal lawyer
and all of these other people engaging in this massive, I don't want to say conspiracy,
effort to obtain it?
The only reason they'd do that is if it had value.
They wouldn't do it for nothing.
It's open and shut.
It's very simple.
And it doesn't matter which side of the aisle you're on, if you're honest with yourself,
you know that's true.
It's not that Americans can't understand complex issues.
It's not that we can't follow along.
The average American is not dumb.
Average Americans are pretty smart.
It's that we'd rather cede our power to our betters.
We'd rather cheerlead than think.
We'd rather not address any of the other issues with the law, you go back and look at that.
We don't want to talk about our interventions.
We don't want to talk about our foreign and domestic policies that create the conditions
these people to leave and want to come here. We don't want to talk about any of that. We
want a simple solution. We want a cheer lead. We don't want to talk about the history. And
the fact is, if you understand history, you can understand the future. Anyway, it's just
a fault. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}