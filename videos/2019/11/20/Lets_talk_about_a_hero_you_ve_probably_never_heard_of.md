---
title: Let's talk about a hero you've probably never heard of....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=jP4KFyqua3w) |
| Published | 2019/11/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces one of his heroes, Martha Gellhorn, as one of the most interesting and prolific correspondents of the 20th century.
- Martha Gellhorn covered significant events like the Spanish Civil War, the rise of a German dictator, D-Day, camps during WWII, Vietnam, Israeli-Arab conflicts, wars in Central America, and almost went to Bosnia.
- Shares a fascinating story of Martha Gellhorn sneaking ashore on D-Day by impersonating a stretcher bearer.
- Mentions Martha Gellhorn's detention and escape after being arrested for going ashore without authorization on D-Day.
- Talks about Martha Gellhorn's marriage to Ernest Hemingway and how she challenged societal norms in combat correspondence.
- Recognizes Martha Gellhorn as someone who changed the rules and broke societal barriers by being wrong in the right ways.
- Acknowledges that many individuals who challenge societal norms often do not receive the recognition they deserve.

### Quotes

- "They are wrong in all of the right ways, and it moves our society forward."
- "It's changing because of people like her."
- "A lot of things in society change because people just do it anyway."

### Oneliner

Beau introduces Martha Gellhorn, an exceptional correspondent who defied norms, inspiring societal change.

### Audience

Journalists, historians, activists

### On-the-ground actions from transcript

- Read about Martha Gellhorn's extraordinary life and contributions (suggested)

### Whats missing in summary

The full transcript provides a detailed insight into the remarkable life of Martha Gellhorn and how she challenged societal norms, inspiring change.

### Tags

#MarthaGellhorn #Journalism #ChallengingNorms #SocietalChange #Heroes


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about one of my heroes.
A, somebody who I think is probably one of the most interesting people of the 20th century.
And certainly one of the most prolific correspondents of the 20th century.
A life full of stories, each story could be made a movie.
Odds are, though, you've probably never heard the name.
Career started covering the Spanish Civil War,
hanging out with none other than Ernest Hemingway.
Went on to cover the rise of a certain German dictator.
Was there.
On D-Day.
Went ashore.
On D-Day.
Was one of the first journalists to report from the camps.
That alone, that is an impressive career.
That's enough for any correspondent.
That's amazing.
Went on to cover Vietnam, the Israeli-Arab conflicts,
the wars in Central America.
Panama almost went to Bosnia.
That point, too old.
The Spanish Civil War to Panama.
dad as a person with some stories right there.
In fact, one of the coolest stories to me
was didn't have authorization to go ashore on D-Day.
So went aboard a nurse's ship
under the guise of doing interviews,
locked away and hid in a bathroom
when the time came
and impersonated a stretcher bearer
to get ashore.
filed the report, and then the military police probably showed up and they got arrested.
Got arrested because, I mean, obviously the stowing away part was a problem, but also
because women weren't allowed to do that.
Her name was Martha Gellhorn, and even then, in detention because she did that, she still
had a job to do. She escaped, convinced a British pilot to fly her out. She didn't
just hang out with Hemingway, she married him. And then Hemingway chased her around
trying to get her to quit. Sent her some pretty direct letters asking her if she was a correspondent
or his wife.
This is a person who led
an amazing life
and changed the rules.
Even today,
combat correspondence,
that's a boy's club.
It's changing, slowly, but it's a boy's club.
It's changing because of people like her.
A lot of things in society change because people just do it anyway.
It's not what's acceptable, but they just do it.
They are wrong in all of the right ways, and it moves our society forward.
Thing is, most of them never get the recognition they should.
Now she has, especially within the journalist community, but very interesting life, certainly
worth a read if you're bored today.
And just never forget that as we move forward there will be other people who do things that
against the social norm and that later they'll be looked at as heroes. Anyway
It's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}