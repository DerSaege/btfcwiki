---
title: Let's talk about SEALs and deals....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=HGvlgfBUK0Q) |
| Published | 2019/11/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Chief Gallagher, a SEAL accused of misconduct, was cleared by the President after being demoted.
- Admiral Greene, along with other top Navy officials, plan to remove Gallagher from the SEALs.
- The media reports suggest Gallagher may lose his trident badge symbolizing SEAL qualification.
- The potential expulsion of Gallagher is within the Navy's power and may spark a political firestorm.
- SEALs are held to high standards, with past instances of even DUIs leading to expulsion.
- Gallagher, if removed, will likely take on different duties, preventing a repeat of the alleged misconduct.
- The fallout from Gallagher's case could deepen the divide between the Department of Defense and the President.
- This situation could lead to further isolation of the White House, a unique development in American history.
- Beau anticipates a strong reaction, possibly in the form of a tweet storm, if Gallagher is indeed expelled.

### Quotes

- "The fallout of this action will further drive a wedge between the Department of Defense, specifically the Department of the Navy, and the President."
- "SEALs are supposed to be a cut above everybody, in every respect."

### Oneliner

Chief Gallagher's potential removal from the SEALs could deepen divides between the Navy, Defense Department, and the President, showcasing a unique political development.

### Audience

Military personnel, political analysts

### On-the-ground actions from transcript

- Watch for updates and developments in Chief Gallagher's case (implied)
- Stay informed about the impact of military decisions on political dynamics (implied)

### Whats missing in summary

Insights into the potential implications of Chief Gallagher's case on military discipline and political relationships

### Tags

#Military #ChiefGallagher #Navy #Politics #SEALs


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're going to talk about Chief Gallagher, which was a very requested video.
Chief Gallagher is the SEAL who was accused of a list of things.
He was found to have done some of them.
He was demoted.
Then the president cleared him of wrongdoing and reversed his demotion.
I think that's what people want an opinion on.
It doesn't really matter.
That is completely within the president's powers.
That is at his discretion.
He can do that.
But there's been an interesting development.
Admiral Greene, this is the commander of all SILs, along with the CNO, the Chief of Naval
Operations and the Secretary of Navy.
They reportedly plan to boot Gallagher out of the SILs.
The media is saying they're going to take his trident.
That's the badge that SILs wear.
I don't know that they can actually do that.
That's a qualification thing.
was probably said symbolically. They're going to boot him out. That is firmly within their
power. I would expect a political firestorm if that occurs. The White House is going to
rightfully see that as a single-fingered salute to them and to the President. I think Admiral
Green is right. I think he has the best interest of the Naval Special Warfare community at
heart. Sills are supposed to be a cut above everybody, in every respect. There
was a time when a DUI would get you booted.
Certainly something of this magnitude that deals directly with duties is
something that a commander is going to have to take into consideration. I know
there's a lot of people that want a lot more punishment. This is the system as
it stands. It is what it is. But if this does occur, understand that he will be a
sailor. He'll be on a ship or submarine or maybe give an admin duty, something
like that, he would no longer be in a position to do anything like this again, any of the
stuff that he was accused of.
The fallout of this action will further drive a wedge between the Department of Defense,
specifically the Department of the Navy and the President.
The White House is becoming more and more isolated, and it's a unique
development in American history, it's not something we've seen a lot of, but this
development is something that we should all watch, because I'm eagerly awaiting
the tweet storm that follows, if they follow through with this, anyway, it's
just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}