---
title: Let's talk about impeachment, today, and tomorrow....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=GRzfXOkHbk4) |
| Published | 2019/11/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The impeachment trial is coming up, with a key testimony from Sondland tomorrow.
- Even witnesses called by Republicans provided damaging evidence against the president.
- Regardless of Sondland's testimony tomorrow, the evidence against the president is already strong.
- Asking for a statement from a foreign national for a campaign is a crime, regardless of quid pro quo.
- Historians will focus on the evidence presented in the impeachment trial.
- The Senate's decision on impeachment will have long-term historical significance.
- The Senate could choose to convict and remove the president or dismiss the trial altogether.
- Republicans should be mindful of the precedent set for future presidents in terms of executive power.
- Allowing foreign influence in elections sets a dangerous precedent for future administrations.
- The Senate's decision will shape its historical legacy.

### Quotes

- "His initial testimony conflicts with what was given by pretty much all of the other witnesses."
- "It's not about the immediate political fallout. It's about the long-term fallout."
- "The Senate will go down in history one way or the other."
- "Allowing foreign nationals to influence our election, it's going to be mighty hard to complain about foreign treaties."
- "There's a whole bunch riding on this and it goes far, far beyond partisan politics."

### Oneliner

The impeachment trial's historical significance transcends partisan politics as the Senate's decision shapes the balance of power between branches.

### Audience

Political observers, voters, historians

### On-the-ground actions from transcript

- Contact senators to urge them to prioritize the long-term implications of their impeachment trial decision (suggested).
- Stay informed about the impeachment trial proceedings and outcomes (implied).

### Whats missing in summary

Beau's passionate delivery and detailed analysis of the potential historical consequences of the impeachment trial. 

### Tags

#Impeachment #Senate #HistoricalSignificance #ExecutivePower #PoliticalLegacy


## Transcript
Well, howdy there, internet people, it's Bo again.
So we're going to talk about the impeachment.
Tomorrow's the big day, Songlin's going to testify.
Today did not go well for the president.
Even the witnesses the Republicans called,
presented evidence and testimony that was damaging to the president.
The big question that's being asked in the media right now is,
now is whether or not Sondland will be believed tomorrow, it doesn't matter.
It doesn't matter.
What he says matters a little, but not much, not really, because the rest of the testimony
is already in.
We have a very clear picture of the President's actions at this point.
Take quid pro quo out of it.
Take all of that stuff out of it.
Understand that simply asking for the statement is a crime.
That's a crime.
You cannot solicit a thing of value from a foreign national for a campaign.
The fact that it was about a statement announcing the investigation and not the investigation
itself is kind of telling it's about influencing the domestic election.
All of this is on the record.
That's what historians are going to look at.
And from this point forward, it's not about the immediate political fallout.
It's about the long-term fallout.
This Senate can be viewed as the one that put a check on executive power.
It can be the one that put party politics aside and said, no, this is not what our republic
is supposed to be.
It can be that.
It's not the way they're leaning, but it could be that.
When it gets to the Senate, they could choose to convict and remove.
or not, or not, they have sole power over the trial, they could choose to do nothing
and dismiss it.
However, historians will look back at this and they will see the testimony that has already
been entered and that's what it will be judged by and as executive power grows, this will
be the turning point.
You know, people are saying that tomorrow is the big day.
Everything hinges on Sondland's testimony, it doesn't, it's already done.
Sondland's testimony is an afterthought.
It really determines whether or not he goes down with the president in history, that's
what it determines.
His initial testimony conflicts with what was given by pretty much all of the other
witnesses.
He has already had to revise that testimony, okay?
But tomorrow he can go in and say, sorry, I remember everything now, this is what happened.
And come clean.
Tell the truth, whatever it is.
And that will determine his place when historians look back on this.
The Senate is the one that's really under the microscope at this point.
historical microscope, the long-term microscope, the one that is going to be
talked about. Mitch McConnell may go down in history as the guy that allowed the
executive branch to become all-powerful. Now the bit that Republicans need to
remember as the suburbs flee the president in the electorate and just
kind of now we're kind of done with him, is that there will be a Democrat
president soon and the executive power that Trump has been able to usurp will
then belong to that president. If the current president can get away with
everything that has been done and there is no check. The next Democratic
President will too. It's something that Republicans should really consider.
Long-term, it is in the Republicans' best interest to remove the President. If they
They don't when Elizabeth Warren or maybe one day Nancy Pelosi or whoever occupies that
office and uses the executive powers that have been ceded from the legislative branch
and does things the Republicans don't like, they can't say anything and they won't be
able to do anything because the precedent is being set right now.
Allowing foreign nationals to influence our election, it's going to be mighty hard to
complain about foreign treaties.
There's a whole bunch riding on this and it goes far, far beyond partisan politics.
The Senate, this Senate will go down in history one way or the other.
It will be the one that stopped the hemorrhaging of power from the legislative branch to the
executive branch or it will be the one that turned the Senate and the legislative
branch as a whole into a rubber stamp for whatever authoritarian decides to
occupy that office. I don't think that history is going to look kindly on a lot
of the senators that are going to have to vote.
I'm certain it's not going to, there's not going to be a lot of favorable things written
about the Republicans that are currently involved in the impeachment hearings.
They're going to have a real bad luck.
legacy will be that of cronies who would try to defend the president no matter his actions.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}