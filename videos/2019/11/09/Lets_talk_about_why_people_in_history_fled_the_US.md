---
title: Let's talk about why people in history fled the US....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NR0tN1_GaCA) |
| Published | 2019/11/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Critiques the avoidance of discussing the negative parts of American history, particularly during National American History and Founders Month.
- Points out the fallacy of the idea that people in the United States do not flee, citing historical instances like during the Civil War and Vietnam draft.
- Describes a period when tens of thousands of people fled to Canada, challenging the narrative of Americans as problem-solvers.
- Explains the loophole in the agreement between Canada and the US that allowed refugees to seek asylum in Canada.
- Mentions upcoming court proceedings in Toronto led by Amnesty International Canada to challenge the US asylum system.
- Argues that the US is violating international law in its treatment of refugees, including family separation and unwarranted detention.
- Urges the Canadian government to recognize the flaws in the US asylum system and scrap the agreement due to the abuse in US detention centers.
- Draws parallels between Donald Trump's policies and historical reasons for people fleeing the country.

### Quotes

- "We don't like to talk about American history, not the bad parts anyway."
- "The U.S. is violating international law as far as the treatment of refugees."
- "It's not safe. The amount of abuse that goes on in those detention centers is just unimaginable."
- "As we talk about American history, it should be noted that Donald Trump gets to join the ranks of the Civil War and the draft as the few reasons people would flee this country."
- "Y'all have a good night."

### Oneliner

Beau challenges the romanticized American narrative by exposing historical refugee movements and criticizing the treatment of refugees in the US, urging Canada to reconsider asylum agreements.

### Audience

Advocates for Refugee Rights

### On-the-ground actions from transcript

- Support organizations like Amnesty International Canada in their efforts to challenge the US asylum system (suggested).
- Advocate for refugee rights and fair treatment of asylum seekers in your community (implied).

### Whats missing in summary

The emotional impact of discussing the mistreatment of refugees in the US and the importance of holding countries accountable for human rights violations.

### Tags

#RefugeeRights #USAsylumSystem #HumanRights #History #Canada #Critique


## Transcript
Well, howdy there, internet people.
It's Bo again.
So tonight, I'm not going to try to pull a fast one on you,
where I'm pretending like I'm talking about something that's
happening now, and it's something
that happened 100 years ago.
I think people are starting to see through that.
And I think people understand the reason I did that.
It was to kind of highlight the absurdity
of National American History and Founders Month.
We don't like to talk about American history, not the bad parts anyway.
So tonight I'm going to talk about something we don't like to talk about because it flies
in the face of our national identity.
We are the place where people come.
People come here to build a better life.
People in the United States do not flee.
We don't do it.
That's the idea.
Is it true?
No, of course not.
Of course not.
Hundreds of thousands of people fled during the Civil War.
People fled as recently as Vietnam, to get away from the draft.
Then there was another time, and that's the time I want to talk about tonight, tens of
thousands of people fled every year for a couple of years.
Most of them were headed to Canada, and they were leaving out of New York.
I know we don't do that though, we're the sons of the pioneers, we're Rosie the Riveter,
we fix our problems, right?
Sounds good, but tens of thousands of people made that journey.
Now their trip was complicated by the fact that Canada and the US have this bizarre agreement
where if the person was in the U.S. first, they can't apply for asylum in Canada and vice-versa.
But the Canadians, being good people and actually valuing freedom, they found a loophole.
Well, if you come through a regular border crossing, we got to return you to the U.S.
But if you come through 20 yards up the road, here's your paperwork.
So, that loophole was used for a couple of years and then some intrepid people stepped
forward and decided to put the US asylum system on trial.
I know people are like, when did this happen?
Next week.
Amnesty International Canada is spearheading the charge there.
the court proceedings will take place in Toronto.
The argument, and it is a very sound argument, is that the U.S.
is no longer a safe third country because the U.S.
is violating international law as far as the treatment of refugees.
Family separation is obviously a form of punishment,
which you can't do to people seeking asylum.
the unwarranted detention, so on and so forth.
The Canadians are attempting to get out of this agreement.
I hope they are smart enough.
I hope the Canadian government is smart enough to understand they need to.
They need to.
As President Trump heads to Central America and tries to hammer out similar
agreements with countries that are not safe, it would be
In the best interest of the world for the Canadian government to say the U.S. system
is not safe.
It's not.
The amount of abuse that goes on in those detention centers is just unimaginable and
they claim they're not responsible for it.
I really hope that this agreement gets scrapped.
And as we talk about American history, it should be noted that Donald Trump gets
to join the ranks of the Civil War and the draft as the few reasons people
would flee this country.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}