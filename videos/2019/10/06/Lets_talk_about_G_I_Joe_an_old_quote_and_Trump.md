---
title: Let's talk about G.I. Joe, an old quote, and Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=DdkoyU1Zdsg) |
| Published | 2019/10/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Takes a day off to play with kids, recalls G.I. Joe show from the 80s and its plot dynamics.
- Draws parallels between G.I. Joe's preemptive actions against Cobra and real-life situations.
- Mentions Lavrenty Beria, a powerful figure in the Soviet Union, known for his infamous quote.
- Criticizes President involving himself in corruption cases without identifiable crimes.
- Questions the process of finding a person to hang and then looking for a crime to pin on them.
- Talks about the dangerous thought process of "Show me the man, I'll show you the crime."
- References Beria's role as chief of the NKVD secret police and his history of heinous actions.

### Quotes

- "Show me the person and I'll show you the crime."
- "Doing it any other way is extremely dangerous."
- "Find a person that we want to hang, and then we go find the crime to hang on them."
- "Show me the man, I'll show you the crime."
- "He is accused of doing some pretty horrible things using that thought process."

### Oneliner

Beau points out dangerous parallels between historical power dynamics and current events, warning against the implications of targeting individuals before crimes exist.

### Audience

Those concerned about justice.

### On-the-ground actions from transcript

- Investigate and stay informed about cases where individuals are targeted without identifiable crimes (implied).
- Advocate for due process and fair investigations in political matters (implied).

### Whats missing in summary

The nuance and historical context provided by Beau in the full transcript.

### Tags

#Justice #PowerDynamics #Corruption #HistoricalParallels #Accountability


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, I kind of took the day off.
Played with my kids.
Played Legos.
G.I. Joe.
Sitting there, start thinking about G.I. Joe, the TV show.
The cartoon back in the 80s.
Funny thing about it, you know, Cobra, the bad guys, if
you're not familiar with the show, they'd show up
And before they ever even committed a crime, G.I. Joe was like deploying tanks and aircraft carriers and all kinds of
stuff.
Eventually just snatch one of them up. Pretty much plot line.
Anyway, so I'm sitting there and I'm putting the pieces together, playing with Legos.
And I thought of that. I start thinking about this guy named Lavrenty Beria.
area. Not a super well-known historical figure, but a very, very powerful person. He was a
very powerful person in the Soviet Union. He was at the Yalta Conference. It's that
famous photo of Churchill, Roosevelt, and Stalin all sitting together. He was at that.
He walked up and introduced himself to Roosevelt, and Stalin walks up, got pats on his head,
and was like, yeah, this is our Himmler.
Man, that's not a way I would want to be introduced.
But it was a different time, I guess.
But yeah, so I'm thinking about this guy and putting the pieces together, plainly goes.
The President of the United States found it fitting to involve himself in two cases that
he calls corruption cases.
Neither case has an identifiable crime to be investigated.
The guy's son got a job he wasn't qualified for, coming from the administration that has
a shoe designer running around playing diplomat. I'm not buying that that's a serious concern.
Both cases involve the same person. Definitely got me thinking about that guy. That guy Beria.
You know, he had a famous quote. And it's funny because the quote is often misattributed.
It's often said that Stalin said it.
So you've got these two investigations and do a person with no specific allegations,
corruption widely, but there's no crime that's been identified, nothing that we can show
and that's how it's supposed to work.
a crime is identified, you go find the person who did it.
That's how it's supposed to work.
Doing it any other way is extremely dangerous.
It's not how it's working lately, though.
We find a person that we want to hang,
and then we go find the crime to hang on them.
Barry's famous quote, the one that gets misattributed,
Show me the person and I'll show you the crime.
The idea was that if you investigated a person long enough,
you could either find something they did or make something up.
Two cases, both involving the same person,
a major political rival to the president.
seems on. Show me the man, I'll show you the crime. Very dangerous thought process. Incidentally,
Beria was chief of the NKVD, their secret police. And he is accused of doing some pretty
horrible things using that thought process anyway it's just a thought y'all have a good night

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}