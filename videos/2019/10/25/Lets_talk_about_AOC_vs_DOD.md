---
title: Let's talk about AOC vs DOD....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=GKDbGc3g9KA) |
| Published | 2019/10/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- AOC and the Department of Defense released dueling reports on climate change.
- AOC's report predicts rising sea levels displacing 600 million people, infectious diseases spreading, decreased food security, extreme weather incidents, and stress on the power grid.
- The Department of Defense report, with input from NASA and the Defense Intelligence Agency, describes climate change as a huge threat to be dealt with immediately.
- The U.S. Army is considering a culture change to reduce their carbon footprint and create fresh water from humidity for warfighters.
- Beau fabricated AOC's report to draw attention to the seriousness of climate change and the need for action.
- The Department of Defense, NASA, and environmental scientists all warn about the real threat of climate change.
- Beau contrasts listening to experts versus dismissing climate change as fake news promoted by oil companies.

### Quotes

- "AOC is right. The environmentalists are right and the Department of Defense is co-signing that."
- "This is a real threat. The people who are paid to decide what threats are, they say it is."
- "It's your choice. You can listen to the Department of Defense, NASA, the Defense Intelligence Agency, environmental scientists."

### Oneliner

AOC and the Department of Defense present contrasting reports on climate change, with experts warning of imminent threats and the urgent need for action.

### Audience

Climate activists, policymakers

### On-the-ground actions from transcript

- Contact local policymakers to advocate for climate action (implied)
- Join environmental organizations working towards climate solutions (implied)
- Support initiatives that reduce carbon footprint in your community (implied)

### Whats missing in summary

The full transcript provides detailed insights on the contrasting reports by AOC and the Department of Defense on climate change, urging immediate action to mitigate the threats outlined.

### Tags

#ClimateChange #AOC #DepartmentOfDefense #UrgentAction #EnvironmentalThreats


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we're going to talk about two dueling reports.
AOC published her little report on what's
going to happen in the next 50 years due to climate change.
Problem is she released it at the same time DOD
released theirs.
And DOD, the people that actually
get paid to study threats and come up with solutions,
They enlisted the help of NASA and the Defense Intelligence
Agency.
It's a pretty comprehensive report.
So what we're going to do is we're going to go through them
both.
We're going to start with AOCs.
And we're going to look at her fear-mongering.
And then we'll look at the DOD one and even compare the two.
So she says that changes in climate have already occurred
and are likely to worsen in the next 50 years, which
is the horizon, the window of the study.
Rising sea levels will displace 600 million people and make them refugees.
Now that my friends is a border crisis.
Infectious diseases will spread all over the United States.
It's going to require the army to come out and help.
These diseases are going to spread because insects have increased range because it's
warmer.
So ticks, mosquitoes, stuff like that will carry malaria, Lyme, Zika, Dengue, whatever.
There will be decreased freshwater availability but increased demand because it's hotter.
There will be decreased food security because crops aren't going to do so well and the
food system, the distribution system is actually going to break down.
There will be increased incidence of extreme weather.
And the stress to the power grid, because our power grid is aging, is going to be so
significant that it will result in the loss of perishable food, medicine, water, purification
systems will go down, communications will go down, fuel distribution will go down.
All of this if something isn't done.
Man that is doomsday stuff right there.
That is a doomsday scenario.
It's crazy.
I don't know how she can get away with publishing stuff like this.
So now let's look at the woman from DOD and what they have to say.
Oh I'm sorry, that's the one I just read.
Everything I just said was published by the Department of Defense in a study, Implications
of Climate Change for the U.S. Army.
Really did involve people from NASA and the Defense Intelligence Agency.
They did this because it is a threat.
This is no longer just some liberal plot, some hippie environmentalist thing.
The Department of Defense is telling you it is a huge threat and it has to be dealt with
immediately or all of this stuff is going to happen within the next 50 years.
AOC did not publish a report.
I made that up just to suck you guys in, to get you all to watch this because I know a
lot of y'all would watch anything if it made fun of her.
But now you've heard this, you can't un-hear it.
The situation is so significant that the U.S. Army is talking about an entire institutional
culture change to become better stewards of the environment because they have such a large
carbon footprint.
They're tasking the Research, Development, and Engineering Command with coming up with
ways to literally suck humidity out of the air and turn it into fresh water for warfighters
because they're going to need it. It's not a joke. It's not made up. It's not
fake science. It's not fake news. This is a real threat. The people who are paid
to decide what threats are, they say it is. And understand the Army has no
interest in this in the sense of it's not good for them. They're gonna have to
change all kinds of things and if you know anything about the Army, they're
kind of reluctant to change, but they don't have a choice. They don't have a
choice. It's a real threat and it's going to have to be dealt with
immediately. AOC is right. The environmentalists are right and the
Department of Defense is co-signing that. So it's your choice. You can listen to
to the Department of Defense, NASA, the Defense Intelligence Agency, environmental scientists.
You can listen to all of these people or you can listen to the stable genius who's telling
you it's fake news.
You can listen to the oil companies who stand to lose money from the infrastructure changes.
Sounds like an easy decision on who to believe to me, but, I don't know, it's just a fault.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}