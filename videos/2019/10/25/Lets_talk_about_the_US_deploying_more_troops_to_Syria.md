---
title: Let's talk about the US deploying more troops to Syria....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=LCGFQcvE3eQ) |
| Published | 2019/10/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The US is sending armor, including tanks, to Syria to protect a gas plant, contradicting the idea of a withdrawal.
- Sending tanks without ground troops makes them vulnerable to anti-tank missiles in Syria.
- Ground troops, maintenance crews, supply guys, infantry, surveillance units, drones, and jets are needed for this operation.
- Despite claims of de-escalation, more troops are being sent to Syria than withdrawn.
- The true motive behind these actions is money and creating a U.S. zone in northeastern Syria.
- The President suggested Kurds move to oil regions, complicit in ethnic cleansing by allowing Turkish military forces in.
- The actions taken do not represent anti-imperialism or anti-colonialism but serve other interests.
- The US is prioritizing protecting gas plants despite becoming a net energy exporter soon.
- Beau criticizes the administration for selling out powerful Middle Eastern actors for questionable foreign policy decisions.

### Quotes

- "Sending armor to Syria contradicts the idea of a withdrawal."
- "More troops are being sent than withdrawn. It's about money and creating a U.S. zone."
- "The President is complicit in ethnic cleansing by allowing Turkish military forces in."
- "It's not anti-imperialist or anti-colonialist, just packaged differently for supporters."
- "We sold out powerful Middle Eastern actors for questionable foreign policy decisions."

### Oneliner

The US is sending more troops and armor to Syria, contradicting claims of withdrawal, revealing ulterior motives, and risking complicity in ethnic cleansing.

### Audience

Activists, policymakers.

### On-the-ground actions from transcript

- Contact policymakers to express opposition to military actions in Syria (suggested).
- Support organizations advocating for the protection of vulnerable groups in conflict zones (exemplified).
- Join local movements opposing questionable foreign policy decisions (implied).

### Whats missing in summary

Deeper insights into the geopolitical implications and consequences of the US military actions in Syria. 

### Tags

#Syria #USMilitary #ForeignPolicy #EthnicCleansing #Geopolitics


## Transcript
Well, howdy there, and hello people, it's Beau again.
So today we're gonna talk about what a great success
that withdrawal from Syria is, how that's going.
Because now the US is about to send a bunch of armor there,
a bunch of tanks and other armor to Syria
to protect the Kenoko gas plant, more than likely.
Yeah, so that's going great.
the country we're bringing them home from, we're sending more troops to.
And armor is going to mean more troops than was withdrawn because SF troops are light,
Special Forces troops are light.
They don't require much.
They're very autonomous.
That's part of the way that they're designed.
They don't need much.
They rely heavily on what's there.
Tanks aren't the same way.
If you just drop a bunch of tanks and their crews there, well, that $4.3 million Abrams,
that's just a target.
It's a sitting duck because there's a bunch of Javelin and Saxhorn anti-tank missiles
in Syria that can take those out.
So two guys can take those tanks out.
So you're going to need ground troops.
You're going to need infantry guys.
They're going to be in Strikers or Bradleys.
going to need maintenance crews for all of this stuff.
You're going to need supply guys for all of this stuff,
because an Abrams, well, that takes a lot of gas.
It takes a lot of parts.
You're going to need to run surveillance over the entire
area, and that's going to require a different kind of
group.
You're going to need scout units.
You're going to need guys to maintain the infrastructure to
make sure the bridges and so on can hold the
weight of the tanks.
You're going to need the drones to help run the surveillance and because there are other
state actors in the area, we don't deploy our armor without air cover.
So we're going to need jets as well.
Those may not have to be stations in Syria.
Those could probably be ran from Iraq though.
The rest of the stuff, it's going to have to be there.
So bringing them home, no, we didn't deescalate.
We're putting more troops in, more troops than left.
But at least the mask is off in this little Scooby-Doo mystery.
We know what it's about now, money.
Same thing it's always about.
Mask comes off and it's a bunch of rich old white guys trying to make some money and not
caring who they hurt in the process.
And the thing is, it's even going to be bigger than this because it looks like the goal is
to kind of create a U.S. zone in northeastern Syria because we're anti-imperialist, anti-colonial.
And that's great, you know, that's fantastic, that's whatever.
But it's not just going to stop with this gas plant because you're going to have to
protect the road heading into Iraq as well.
So all of these numbers they're projecting now, expect them to go up because they're
going to realize that if you just protect the plant, well, it doesn't do any good because
the second anything leaves it becomes a target.
Now the other thing that happened was the President tweeted, perhaps it's time for
the Kurds to start heading to the oil regions.
The President of the United States is complicit in attempting to remove an ethnic group from
geographic area by force. There's a word for that. There's a term for that. The
force is the Turkish military and he allowed that force in there by moving
the troops that aren't being withdrawn, were not actually scaling down. That was
just something to sell to his base. They're trying to cleanse an area from
of an ethnic group and the US president is complicit in it.
That's where we're at this morning.
More troops and complicit in something is pretty horrible.
So nope, not bringing them home.
It's not anti-imperialist.
It's not anti-colonialist.
It's not ending foreign entanglements.
is what it is. It's more of the same, just packaged in a different way by a guy who
can sell his brand and his supporters and base. Bought it, hook, line, and sinker.
In the process, we sold out the most powerful non-state actor in the Middle
East. But hey, we're gonna protect the gas plants even though by what the last
quarter of 2020, we're going to be a net exporter of energy, but our foreign
policy is still guided like OPEC can cut us off at any moment. Anyway, it's just a
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}