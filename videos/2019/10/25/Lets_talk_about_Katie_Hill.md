---
title: Let's talk about Katie Hill....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=vzs8ggPww94) |
| Published | 2019/10/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Congressperson Hill's scandalous photos surfaced, sparking a national debate.
- The photos show adults engaged in consensual activity and Hill using a substance that was once illegal.
- Hill has a tattoo that may have been an iron cross, a symbol associated with various groups.
- She is the first bisexual representative from California and these photos confirm her admission.
- There's ethical concern about her dating an employee, although not illegal.
- Beau questions why these photos are circulating; suggests it's due to Hill being a pretty woman.
- He challenges the double standard women face regarding purity and behavior.
- Beau points out the lack of harm in Hill's actions and questions the public interest.
- He criticizes the misplaced priorities of focusing on these photos rather than more critical issues.
- Beau contrasts the attention on Hill's photos with the President's lawyers arguing for his right to kill people.

### Quotes

- "Women are still held to this false idea, this standard that they need to remain pure and that they need to behave in a certain way."
- "None of this is any of our business."
- "This country has its priorities mixed up."
- "It's just a thought."
- "Y'all have a good night."

### Oneliner

Beau points out the double standard faced by women in politics through the scandal surrounding Congressperson Hill's photos, urging a shift in priorities towards more critical issues.

### Audience

Political activists

### On-the-ground actions from transcript

- Support organizations advocating for gender equality and against slut-shaming (implied)
- Engage in critical discourse on gender bias and double standards in politics (implied)

### Whats missing in summary

The full transcript provides a deeper analysis of gender biases in politics and society, urging viewers to question societal standards and focus on more critical issues.

### Tags

#GenderEquality #PoliticalBias #DoubleStandards #SocialJustice #ChangePriorities


## Transcript
Well, howdy there, internet people, it's Bo again.
So we gotta talk about Congressperson Hill.
Because some photos surfaced.
Scandalous.
Salacious even.
We have to talk about it, it's very pressing.
It's a very important matter for our country
to be discussing right now.
I'll go through the list of what's in the photos,
and I'll pause to give y'all time to gasp
in the appropriate locations.
So, unbelievably, these photos appear to show adults engaged in consensual activity.
They also appear to show Congressperson Hill using a substance that at the time might have
been illegal, maybe, depending on the circumstances.
But that substance was in such widespread use that now it's not illegal.
She also has a tattoo in these photos that might possibly have been, at one time, an
iron cross.
And that's a symbol, in case you don't know, that's used by bikers, and skaters,
other unsavory sorts in the US military and a whole lot of people. Normally when
it is used by the unsavory types it's mixed in with other symbols. It's not
just an iron cross. Oh, gasp. Sorry. Then there's this other thing. Did you
know that Ms. Hill is the first bisexual representative from California? And that
these photos appear to show her engaged in activity that she's admitted to?
Now there is the ethical question about dating an employee. It's not smart. It's
not illegal, but it's not smart. There should probably be a conversation about
that. It's one of those things a lot of people do it I did it when I was younger
it was stupid then it's stupid now. But let's move on to the real point. Why are
we even seeing these photos? What's it mean other than revenge? What's the only
reason these photos are being circulated? Because she's a pretty woman. You think
if it was Mitch McConnell in these poses, we'd be seeing these photos? No, of course
not. Sorry for that image, for those that visualize that. Nobody would want to look
at it. Nobody should really want to look at these either. See, women are still held to
this false idea, this standard that they need to remain pure and that they need to behave
in a certain way. It doesn't make sense. There are not many men who would look at
that situation and think, man I'd never want to do that, but they're going to
condemn the women that do. None of this is any of our business. None of it is any
of our business, what they do, what people do in the privacy of their own homes, or hotels,
doesn't matter.
Nobody's being harmed by any of these actions.
But I do see why it's a pressing issue for the country.
This is far more important than the President's lawyers arguing in court that he has the legal
right to murder people in the street.
This country has its priorities mixed up.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}