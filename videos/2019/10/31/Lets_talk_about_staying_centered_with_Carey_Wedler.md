---
title: Let's talk about staying centered with Carey Wedler....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=3ydf0cohiNU) |
| Published | 2019/10/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Carrie Wedler says:

- Started activism due to disillusionment with politics after supporting Obama in 2008 and realizing the continuation of wars.
- Believes in evolution over revolution, advocating for agorism and peaceful protest against systemic injustices.
- Trauma from her parents' divorce shaped her emotional responses, leading her to pursue healing and introspection.
- Recommends yoga and meditation for maintaining calm amidst chaotic news cycles.
- Emphasizes the importance of addressing deeper emotional wounds to prevent reactive responses.
- Advocates for skepticism towards all media sources, both mainstream and independent, promoting critical thinking.
- Plans to continue making videos challenging political authority and advocating for emotional exploration.
- Suggests planting trees as a simple yet impactful solution to mitigate carbon dioxide and contribute positively to the environment.
- Encourages curiosity and mindfulness towards emotional reactions as a pathway to healing and self-awareness.

### Quotes

- "My solution is not violence in the streets and taking down the government with guns as much as I support gun rights."
- "I think this is a really sweet one, and I think it's so symbolic of new beginnings."
- "Just start to become curious about what comes up for you."
- "I'm banned from Twitter, but my ha ha ha, it's so funny that I'm banned from Twitter. It's hilarious."

### Oneliner

Carrie Wedler advocates for agorism over violent revolution, prioritizing emotional healing, mindfulness, and curiosity to navigate turbulent times and advocate for positive change.

### Audience

Activists, Community Members

### On-the-ground actions from transcript

- Plant trees or support organizations that focus on tree planting to mitigate carbon dioxide (implied)
- Incorporate yoga and meditation practices into daily routines for emotional well-being (implied)
- Cultivate curiosity and mindfulness towards emotional responses as a pathway to healing (implied)

### Whats missing in summary

The full transcript captures Carrie Wedler's journey from political disillusionment to activism, advocating for agorism, mindfulness, and emotional healing as paths to personal growth and positive change.

### Tags

#Activism #Agorism #Mindfulness #EmotionalHealing #TreePlanting #CommunityEngagement


## Transcript
All right, and we are here with Carrie Wedler.
And she's gonna talk to us tonight
about how to stay calm with all the news that's going on
and everything else and stuff like that.
And so why don't we start off with some of the questions?
And I think it would be appropriate to start with
who is Carrie Wedler?
We'll edit that.
Who are you?
Tell us about yourself.
So I don't even know what to call myself anymore.
For a long time, I was editor in chief of the anti-media.org.
We no longer exist in the capacity we used to
due to some bans on Facebook and Twitter last year.
But I found myself in activism several,
I actually wanna say seven, eight years ago,
I started making videos.
I had voted for Barack Obama very proudly in 2008.
And come 2011, the Arab Spring was coming up,
things were happening in the world.
and I realized I have no idea what was going on.
I had voted, I had participated in the system,
but I was not keeping track of the system.
And when I started realizing that Barack Obama
had expanded a lot of the wars George Bush had started,
I decided that I cared
because I came from a democratic background
and from Los Angeles,
I was raised just to believe that blue was good
and red was bad.
And the faith I had put in Obama,
he didn't live up to it, I'll say that.
and I just felt like it was somebody's responsibility
to start speaking out.
I had always wished I'd grown up in the 1960s
so I could have protested the Vietnam War.
So war was always a really big issue for me
and it continued even when a Democrat was president.
And it went from there.
And it went from there.
And all right, so that goes right into
one of the next questions, one of the other questions.
uh wow okay so this is i always see her critiquing the corporate establishment
gun control foreign policy etc i wonder what her vision for the future is like did she just
realize that government was full of it after obama and then fell into libertarian and and cap ism
is she not revolutionary minded at all but just wants more old school conservatism also
So who hurt you, Kerry?
You always seem mad.
Okay, you have to keep me on track here because there's a lot of moving parts to this question.
I think there's a little confusion because whoever asked this question seems to think
I'm both a libertarian and a conservative.
Those two don't coincide to me.
Just as much as libertarians can coincide with people on the left, they can coincide
with people on the right economically, whereas on the left it tends to be a little bit more
civil liberties oriented.
But nonetheless, let's see.
Am I not revolutionary minded at all?
I feel like I am.
I believe I prefer evolution more than I prefer revolution.
My solution is not violence in the streets
and taking down the government with guns
as much as I support gun rights.
I don't think that that's going to be
a sustainable long-term solution.
I'm a big proponent of agorism.
If anyone in your audience has heard of it,
I'm sure they have, where you're basically just participating
and counter economic activity to challenge a lot
of the injustice that's been perpetuated by the system.
So for example, if you look at occupational licensing laws
these are terrible and they put a really big burden
on the poor, often minorities.
It keeps people from making a living
because they have to jump through all these hoops
and all this government required training.
So just a quick example, what if I got my hair cut
by someone who didn't have the correct government license?
That would be an active defiance.
And I know maybe it sounds silly and it sounds stupid
but all of these acts adding up someone, you know
parent using CBD to treat their child with seizures in a state
where it's illegal, that would be agorism, you're participating
in something that not only undermines the pharmaceutical
industry, which is very entrenched with government, but
you're also you're just taking a different way, you're breaking
the law at the end of the day, it's civil disobedience. And I
think that all of these actions can add up to really make a
difference and not so much revolutionize our paradigm and
tear down the government, but instead just create a more
cooperative community-based way of living
as opposed to relying on a giant federal government
and even state governments, local governments
that can be very corrupt.
And I didn't forget the question, who hurt me?
This is actually very essential to the entire conversation
I think we're supposed to be having.
Who hurt me first?
That would be my parents when they got divorced.
And a lot of my life and a lot of my emotional
healing process has been dealing with these childhood wounds.
And I don't want to be presumptuous,
but I feel like this is the case for a lot of people.
Children are very sensitive.
We are very prone to being misunderstood
and not having our needs met
because we're being raised by other human beings
who probably also didn't have their needs met.
And so this was not something that had occurred to me
or mattered to me at all when I started making videos.
But over the last few years,
this person said, I'm always mad.
Yeah, sometimes I am in my videos,
but I started to realize
that that's not a sustainable way to live.
I don't wanna be angry all the time.
And I think for me, it's been an interesting line to walk
and an interesting balance to try to strike
because on one hand, it's very legitimate to be angry
about everything that's going on in the world.
But on the other hand, I had to start examining
that younger trauma, those younger experiences I had
and really start to get real with myself
about how much of my reactions
to what was going on in the world
were actually something that was triggered in me
on something that has nothing to do with government,
but there were some common themes.
So for example, I just talked about this in a speech,
so I'll use this.
When we lost the anti-media last year,
one of the feelings that was coming up for me
was like hopeless.
I feel silenced, imagine that.
Those were feelings that were coming up for me
when my parents got divorced,
that I only came back and realized years and years later.
So for me, who hurt me is actually a really big question
and it's really important, in my opinion,
moving forward as a society,
as we try to get more freedom and justice
and peace in this world.
All right. Any thoughts on that bow? Oh, no, no, that was that was a lot in a real short
amount of time. I mean, that was that was a whole man. I hope your parents don't watch
this. No, I'm joking. No, they know they know. And that's the beauty of the healing process
is they're human too. I'm human. I love my parents have always loved my parents and they
love me so much. And it's been a really beautiful process to try to heal from that.
All right. So one of the questions I get a lot is, you know, how do you stay centered and how do you do this with
everything that's going on in the world? How do you not just end up angry throwing stuff at the camera? And I don't have
an answer to that. So that's why you're here. Help people right now. Make them make them feel better. How what are some
right this moment, fix everything? How?
Give me some solutions on how to maintain center with all of this going on.
Sure.
Well, so for people who don't know, Bo is my personal friend, and I'm very, very lucky
and grateful for that, but he knows that I've had a hard time the last year.
So I don't want to speak from a place of having all the answers and knowing every solution.
And like, I have this recipe for how to be happy and how to change the world.
I don't, not at all.
All I can share is what has helped me.
the first thing that ever helped me was I found yoga. So I'm from Los Angeles and there's a big
yoga community here and I've been making videos, I don't know, not more than six months when I found
yoga, but I just found myself really agitated and really antsy and I went to my first yoga class
that really resonated with me. I'd gone before and it just didn't click, so if you go to a yoga class
and you don't like the teacher, go to another one before you click. That's my first advice, but it
just resonated and what yoga is essentially is it's a moving meditation, so it's really a meditation
practice because you're taking the time to be present, to focus on your body, to focus on your
breath, and that helps bring a little bit of calm. So by the time you finish a yoga class, whether
it's like a workout yoga class or a deep stretching yoga class, if you put in even just the tiniest
little bit of effort to be present, that can be intensely calming. The same goes for an actual
meditation practice. So I meditate daily, that I started doing three years ago maybe, because again,
I was getting online, I was just waking up, pulling out my phone, pulling up Facebook,
and it was agitating. It was not fun. It made me angry from the second I woke up. And I'm sure a
lot of people can relate to that. So I implemented a practice where I meditate. It's gone up over the
years, but I started with 10 minutes a day in the morning before I did anything else. No Instagram
for me, no Facebook. And that really sets the tone for the day. And it sort of helps you
to realize what's worth it, you know?
So I used to be very engaged in comments
and I used to be very reactive.
I was gonna use a dirty word, but I'll say reactive.
I was very reactive.
And by focusing on this presence,
on trying to cultivate just a calmer mindset,
it has made it easier for me to be like,
you know what, I don't need to respond to that person.
And if I do, I'm gonna reread the comment before I post it.
I'm gonna take out any fascinists.
I'm gonna take out any sarcasm.
However, so I can recommend yoga,
I can recommend meditation
and these are wonderful day-to-day tools,
but to bring it back to who hurt me,
I really think that unless we're getting
to the deeper sources of our conflict and our suffering,
we're gonna be reactive no matter what.
I can do all the yoga in the world
and I did do all the yoga in the world.
I became a yoga teacher and I still found myself struggling
with much deeper issues and after a while,
the high of the yoga, the high of the meditation wears off.
So you kind of, you have to get real with yourself.
And I'm saying this as someone who's still learning
how to do that.
So it's a day-to-day process,
in addition to these mindfulness practices
that I think are really helpful.
And I'd like to recommend, before I forget,
because we're talking about these things,
I think someone that a lot of your viewers would appreciate,
her name is Tara Brock, B-R-A-C-H,
and she is a former therapist.
She's a meditation teacher.
She's very deep into Buddhism,
but she does a lot of great podcasts
talking about how to cope.
and how to deal with the current state of the world.
I'm fairly confident she shares
a lot of the political persuasions
as your audience probably does.
So I think that would be a wonderful jumping off point.
I mean, that's my go-to when I am just like stressing out
and yoga is not cutting it.
I just light some candles.
I do some deep stress yoga.
I put my headphones in and I listen to Tara.
And she really helps me through it.
And she's really big on, you know,
we're talking about being angry at the world
and something that she talks about
that I think is incredibly helpful and useful
and really gets to the root of healing
is looking at what's underneath the anger.
So when you're finding yourself angry,
this is an example she uses in a podcast.
She talks about the Iraq war in 2003.
And she was so angry.
She just was waking up in the morning
and she felt hatred flowing through her until she paused.
It's called a sacred pause in this practice.
And she stopped to think about
the more vulnerable emotions underneath.
Well, what was she feeling?
She was feeling really scared
that a bunch of innocent people were gonna die.
She was scared that power could be this out of control.
And by tapping into that softer emotion,
she was able to connect more deeply with others around her.
And that's a big one for me,
because if you have seen my videos,
you know, I can be very, you know,
I've had to work on that a lot.
Oh, you are, you are so West Coast.
So bring that, the idea of meditation,
bring that to people more like me.
I mean, you've had this conversation with me,
Meditation doesn't necessarily mean sitting on the floor
with candles around you, legs crossed and humming.
What are some other examples that are maybe more practical
for people that live this side of the Mississippi?
All right, well, I'm not that familiar
with outside of the country, but I would say,
yeah, like meditation doesn't have to be sitting
on a cushion and sitting up straight
and being uncomfortable because your back hurts
because you've been sitting for 15 minutes straight.
For me, meditation is anything that brings you
into the present moment that brings you out
of that mental chatter and that loop.
And I have a lot of mental chatter and a lot of loops.
They go all the time.
So it could be something like going for a hike.
Do you like nature?
Do you feel present in nature?
Is that something that really makes you feel
like you're experiencing the moment?
That is a meditation to me.
Don't tell other meditation teachers I said that
because I don't know.
But for me, that is a meditation.
Whatever brings, maybe it's playing music.
Maybe it's writing music.
Just something that you feel a sense of center
And it's hard to explain, but meditation in its essence,
it sounds so esoteric,
but it really is just bringing attention
to the present moment.
And when I used to teach yoga, I would tell my students,
because I needed to hear this,
it doesn't matter if your mind wanders
a thousand times in 20 minutes,
all that matters is that you notice.
And that there is the meditation.
It's not keeping a quiet mind
and never having a thought run through it,
and then you have peace, not at all.
It's simply being aware of it and accepting that it's there.
because I went through a long phase
where I was meditating every day,
but I was coming out of the meditations more irritated
because I couldn't shut my mind up.
So I'd sit there for 10 minutes
and I'd be like trying to focus on the breath,
but then I'd just be mean to myself.
Like, Carrie, what's wrong with you?
How come you can't do this?
And then what does the mind wanna do more?
It wants to go even more.
So a big part of my practice
has been just accepting what's here,
acknowledging it and gently guiding yourself back.
Like it doesn't help me to be mean to myself.
That is a lesson I've learned very well over the years.
not going to add to anything to your life. It's only going to worsen the suffering.
All right. Okay, so I want to jump back to some more of these questions, because there's some,
I mean, there's some good ones. Let's see. Your opinion on how to make people understand they
need to take a skeptical view of any media sources. How do you, how do you tell people that they need
to not trust the Washington Post or whoever?
Well, I think it essentially it's not about whether it's a corporate outlet,
it's not about whether it's independent, because something I've noticed a lot in independent media
is a lot of the audiences and a lot of the people who create the media themselves tend to think,
well, if it's not mainstream media, it's good. They must be telling the truth. They're going
against the narrative. Therefore, I'm not going to fact check anything they say. I'm just going to
assume that it's true because it confirms my biases. And the same thing goes for the mainstream
media. I did see a comment saying that I shouldn't call them the lying mainstream media. And I do
want to address that only because as an anti-war activist, yeah, they have perpetuated lies many
times over the decades, very much without question. So that doesn't mean that everything they publish
is false. Absolutely not. In fact, I'm sure that the vast majority of what they're reporting is
true, it might have some political spin on it. And the same goes for independent media. But for me,
it's, it's just been a long, long trajectory of learning to think for myself and know the value
in that. Because, again, I was raised as a Democrat, I didn't question anything. I was one of those kids
who, like, love to sit up straight in school with my hands folded on the desk, you know, and, like,
wait for the teacher to tell me I was good. Like, I got a lot of validation from that. I really,
I'm not your typical anarchist. Like, most people are like, I always knew it was all a joke and a
a lie, but like not me, I didn't know I really took it seriously. And I got a lot of I really
liked I really liked being in line. So for me, I've had to challenge myself and it's
been a I went to public school all through school. I went to UCLA. That's another public
university like I I'm fully steeped in the public education system. And it doesn't mean
that everything I learned there is wrong. But it does mean that it's been very helpful upon
asking questions, just to leave room for the fact that maybe just because someone works for
a blue check organization that's branded truthful by the establishment, that doesn't necessarily
mean they're right. And the same goes for independent media. Like I have had many,
I've had many battles within the independent media circle about fact checking and making sure
that we're reporting responsibly and without sensationalism. So I don't want to come off like
like I'm one of those, like, I, I,
another lesson for me has been not everything
is black and white.
So it's not mainstream media bad,
independent media good, not at all.
I think there's good in both.
And that's the beauty of it is as individuals,
we have the power and the capability
to check things for ourselves,
but we just have to be serious about it.
All right.
So what are, okay, so what are you doing now?
Like you're, you're at, you're where you are now.
You were editor in chief of anti-media.
Where are you going now?
Because I heard that you were going to edit the fifth column.
Did you hear that?
I heard that.
No, still not happening.
All right, worth the shot.
I have been editing some videos for this YouTube sensation.
You may have heard of him.
So I would really like to make more videos.
I've really taken this time this past year.
It's been over a year since we lost the internet media.
And I didn't intend to take this much time off.
and to myself and really take a break from videos.
But it turned out that that's what I needed.
So I've just been tending to myself
because I was told that this interview was going
to be about coping and emotions.
And so I really want to bring it back to that because that's,
I can't do anything else if I'm not taking care of myself.
And I realized I burned myself out constantly
making content constantly.
Oh my gosh.
I'm talking to somebody who makes a video a day.
And I used to make one a week at my peak.
But nonetheless, that's a lot for me.
But yeah, I've been taking the time to really go inward.
And I really think that's important
for the long-term healing of the world.
So many of us are acting unconsciously.
And I don't mean that as a judgment.
I just mean there isn't much pause
between what we think or we feel
and how we release it into the world.
And I'm so guilty of that.
I just thought of video today where there's some sass,
and we can't be perfect all the time,
but I would like to continue my emotional exploration.
I'd like to share more of that with my audience
because I'm so strictly political now,
but that's really not all of who I am.
I'd say that this internal exploration
is just as much if not more,
but I would like to get back to making videos.
And I have some in the works actually,
and a lot of them are just challenging authority,
political authority.
That's philosophically what matters to me
because I found that being out of the news cycle
is actually kind of nice for my mental health.
I mean, it's important to be informed,
but it's nice not to be on Reddit
and like 17 other sites every day,
just like, oh, look at these horrible things
happen to all the they're always happening right but there's also a lot
of positive things so I also like to highlight solutions as well. So as far
as we're moving forward and in the US and everything and eventually Trump will
be gone now all the stuff we're talking about with coping is there something
that's gonna parallel to that on a collective level on a national level and
And if so, you have any idea on how we as individuals
can help that healing process, who hurt us and all of that?
Well, I mean, judging by the way people reacted
to Marianne Williamson, I don't think that we're gonna
find a way to blend spirituality and emotional healing
with politics, that just doesn't seem likely to me,
it would be really cool, but I just don't,
I kind of see them as opposed.
So, I mean, at the end of the day,
what it really comes down to is the individual level,
I'm sure there are people who are gonna be like,
she's a libertarian,
of course she thinks it's all individualism,
but to bring it back to Tara Brock,
I mean, this is someone who, she's not a libertarian,
I can guarantee you that.
But it's still at the end of the day,
we have to be responsible for our own healings,
just as much as we have to be responsible
for what goes on in the world.
And it can seem like we can't make much of a difference,
but I'm sorry I don't have a more systemic answer
because, and I wanna explain for a second,
just because the reason I think
that we can't blend emotional healing and spirituality
with government and politics
and political so-called solutions
is that politics derives its authority through violence.
I mean, why do you do what the police tell you to?
Because they have a gun, right?
Like, why do you obey?
Assuming that you're not committing,
you're not harming someone,
it's because you can be thrown in a cage
if you violate the rules.
So I don't see an institution that inherently
is based on violence or the threat of it.
I don't see that really being a great arena
enough for becoming more peaceful as individuals. But yeah, I think community really does matter
though. And I think oftentimes in our political paradigm, community is confused for the electorate.
Whereas community can be the people you live around. It can be the entire world. I like to
think of every human being as my community. But it really does come down to finding people that you
can have as a support system who share values with you, but also freely taking responsibility
for yourself because I have spent a lot of time in my life blaming other people for why I was
unhappy and why I couldn't find peace and as it turned out it was all within me and I know that
sounds like California hippie stuff but it it's it's been true for me and all I can do is speak
what's true for me. All right well that actually jumps right into this other question over here.
How is anti-statism possible without violence?
Well, we actually kind of touched on it. So I'm talking about agorism, where you don't have to
be violent to participate in markets that are outside of government control. I think that's a
big one. But I also, for me, the question is, how can anti-statism be violent? And I know you
probably disagree with me. But if we're talking about the nature of violence that really colors
the state, and we look at human history. And maybe you can correct me here. I'm totally open to it,
but I am yet to see a situation where violently overthrowing a government leads to anything other
than another violent government that eventually devolves into corruption and injustice.
By all means, correct me, because I feel like you know more than I do about violent revolution.
No, you're right. I mean, that should be the very, very, very last tool in the tool box.
because if you employ violence,
the best you can hope for
is to get a little bit closer to your goal.
You're not actually gonna get to utopia.
You're just gonna maybe take one step there if you're lucky,
or you may end up with something even worse.
All right.
Also, just real quick before you,
I don't know if we're changing the subject,
but I do just wanna say as far as,
I know that I keep talking about emotions
and healing and childhood trauma,
but I mean, if you look at some
of the most corrupt people in politics,
like I don't have to like Donald Trump
to be able to understand
that he probably had a tough time growing up.
Like with whatever kind of father he had,
with whatever kind of experiences,
I think he was acting out pretty young.
And maybe if someone had come to him and been like,
hey, Donnie, like, what are you feeling right now?
Oh my gosh, of course you're feeling angry about this.
Like to have validated his feelings,
and like let him be seen as a child.
And again, this doesn't mean I support anything
Donald Trump does, but trying to look at it
without my politics coloring anything else.
I mean, I think most people who do bad things
have some sort of trauma.
So before we move on, I just wanted
to stick that in there as far as violent authoritarians
and people in power go.
OK.
Well, one of the things I'm going to add,
yeah, I don't know about all of that,
but that's why other people are coming on,
because we need some other viewpoints here.
So one of the things I'm going to ask everybody that comes on
is give me a solution to something,
some problem that the world is facing,
the country is facing, whatever,
give me a solution that is going to one thing
that people can do that will help solve some of it,
most of it, a big piece of it, something.
Sure, well, my big one is emotional healing,
but I feel like we have already covered that in depth.
So I'm gonna go with something really simple
that I have discovered as I've gone searching
for positive news.
I hope it doesn't sound meaningless,
but honestly planting trees.
I don't know if anyone saw the recent study or analysis
that showed that that is actually an incredibly effective way
to mitigate carbon dioxide
and know it is not the only solution.
It's not like, oh, we each plant a seed
and all of a sudden no more climate change
or all of a sudden the environment is fixed,
but it's actually very accessible.
You can either do it yourself
or you can support any range of organizations
that will put proceeds towards planting trees.
So there are specific environmental groups.
And then there's one, for example, like a search engine.
I don't know how to pronounce it.
It's like Ecosia, Ecosia, Ecosia.
And they take the ad revenue from your searches
and they put it towards planting trees.
And I think that's such a simple and lovely solution.
And I know that it's not gonna fix everything.
I know that I've been yelled at in comments many, many times
no matter what solutions I put forth, somebody's mad.
And somebody wants to tell me
that it's not gonna fix everything.
But I think this is a really sweet one.
And I think it's so symbolic of new beginnings.
Have you seen the whole team trees thing
that's taking over YouTube?
No.
Okay, so short version for those people that don't know.
There's a guy named Mr. Beast,
who I kind of made fun of
when I actually made my little video about it.
But after looking at it,
because people are like, you're reading this guy wrong.
And I looked, I'm like, yeah, okay.
So he does actually do a lot of charity work
with the money that he makes,
but he does all these crazy stunts.
Currently, he's trying to raise $20 million
to plant 20 million trees with the Arbor Day Foundation.
And the last time I checked,
I think they were almost to six,
and it's only been going on like three days.
So, yeah, everybody check out that hashtag.
It's on Twitter, it's on YouTube, everywhere.
And yeah, it's definitely great.
And I, of course, would add fruit trees to that,
make fruit trees so you can help
with food insecurity as well.
Okay, so parting shot. What do you got? What do you what do you want to tell everybody?
Well, and I actually this came quite smoothly because of what you said. Were you just saying
that they raised more money than they were anticipating than their goal for the tree?
They're at six million. They want to do 20 million, but they've only been doing it like four days.
So they're gonna get there. Exactly. And that, you know, I know, you know, I can be sassy and
my videos. But to me, like, that is so inspiring. And that's so
it gives me so much optimism, because that's not the only
place we see individuals taking collective action on their own
without anyone forcing them to. I believe many of your viewers
or listeners will remember after the election, how many people
raised how many millions of dollars for Planned Parenthood
because they thought that it was going to be threatened. What a
wonderful way to organize society because when things are
essential, when people care, they take action and they
support it. Look at the Cajun Navy. I know you've been very
active bow in all of your support for hurricane victims. And it's, there's so many decentralized
solutions going on so many people who care and I think that we see the political system and we see
all the bickering and we see the venom really people are there's so much contempt for each
other. But when you look outside of politics and you look at people just living their lives day to
day and expressing that they care about things, I think there's so much hope and there's so much
room for innovation and helping others. I just signed up to volunteer to Dog Shelter. So that's
my latest thing. My one little drop in the bucket, my next one, that's what I'm going to be doing.
That's awesome. Okay. Well, I guess that's it. Let me scan through these questions one more time.
Okay. Let's see if there's anything else. It was get family and friends off of CNN, MSNBC, Fox.
That's a good question that you might have some insight into. Why does America continuously push
its core issues down the road and on to generation after generation rather than face its problems
head-on and conquer them for everybody's betterment?
That is a wonderful question that I feel could be answered in a thousand different ways that
would all be correct.
But something I see actually, I wanted to make a video about it at some point and surprise,
surprise, I never did.
But I think there's a very strong culture in this country of avoidance, like I don't
want to deal with that right now, deal with it later.
You're sad, take a pill for that.
You know, you don't, you don't, you want to be skinny, take a pill for that, you know,
And I'm not saying that pills are inherently bad,
that's not what I'm saying,
but everything seems like a quick fix
where everything's a bandaid.
And I don't know which came first, the chicken or the egg.
I don't know if it's politics that have turned people
to look for quick solutions or whether it's a culture
that then pushes people to want them.
But it seems like as the question acknowledges,
this has been happening for a very long time.
I don't know if it's the nature of politics
because the problems tend to just keep mounting.
I mean, how big is the debt now?
It's like $22.5 trillion.
That could be relevant, since we're going to have to pay for it.
But I don't see many candidates talking about it.
Certainly, Donald Trump isn't talking about it.
Donald Trump is bragging about or pushing for negative interest
rates with the Fed, and then saying he's taking on the Fed.
But his supporters actually think that he's challenging
the Federal Reserve by saying that there should
be negative interest rates.
And I'm not an economist, but I don't
that that's how you get your way out of debt. So it is systemic. I'm not going to say I know exactly
why. I don't like to pretend like I know things I don't, but I do think it's in the culture,
that kind of avoidance. Are you still a supporter of cryptocurrency? I am. Yeah, I'm not active in
the cryptocurrency community, but I have my little sum of crypto that I hoard and I hodl.
And I do think, speaking that, you know, we were just talking about the Federal Reserve,
that's another form of agorism.
This is a competing currency.
The dollar is not really serving everyone well, you know?
The way it's been distributed, the way it's controlled.
So I think options like cryptocurrency, especially in developing countries, that takes out the
middleman.
It takes out the bank.
That's huge.
And there's a greater level of privacy, even though there's a blockchain.
I mean, you don't have a bank that has all of your records.
So yeah, I love cryptocurrency.
I'm not an expert.
I'm not an expert, but I do appreciate the potential.
All right, so one more.
OK.
Give us something else on healing, coping,
something like that.
And we'll kind of close out on that.
And when are you going to start doing videos on that topic,
which is something I heard somebody told you to do?
Yes.
I actually have a script written for that,
but I'm going to have someone else record the audio for me.
So that'll be coming out when she does that.
But I think the one thing I want to share,
it's so basic, but it's also so not taught to us, anything.
Forget everything I said.
Honestly, if anything could help bring you,
and this is, of course, in my experience,
but if anything could help bring you
towards a little bit more calm,
towards a little bit more healing,
a little bit more awareness, a less reactivity,
I would say just start to become curious
about what comes up for you.
say you're watching Donald Trump
and he's just saying something awful
that like really makes you mad, you know?
Like the kind of thing that just makes you
wanna record a video right away
and like yell at the internet, you know?
Like when that comes up, whether it's about Donald Trump,
whether it's about someone in your personal life,
just setting the intention to pause and explore that.
Where am I feeling it?
What am I feeling?
I tend to feel things in my chest, like right in my heart.
A lot of people feel it in their stomach.
They feel it in their throat.
just becoming curious about that inner world
and the experience we have.
And instead of fighting it,
instead of being like, oh no, I'm angry,
I gotta get rid of it, instead just be like,
okay, I'm angry, that's here right now.
And that way, when you add that little bit
of mindfulness to it, there's a much smaller chance
that you're gonna put it back out into the world.
And that is a big lesson for me.
And for me, it's been emotions 101.
So if you're someone who doesn't like
to get into their feelings or isn't familiar
with their feelings, that would be a great place to start.
All right.
OK, so plug whatever it is you want to plug,
and we'll get out of here.
Sure, you can find me on YouTube, Kari Wedler, C-A-R-E-Y,
W-E-D as in dog, L-E-R. You can find me on Instagram.
I am banned from Twitter, but my ha ha ha,
it's so funny that I'm banned from Twitter.
It's hilarious.
I'm also on Steemit and Minds.
I like to support those alternative,
decentralized social media options.
That's another form of fighting the establishment.
You can find me on all those.
All right.
All right, everybody.
And we will be, if you're listening on the podcast,
we'll be back with something, I'm not sure what's next,
right after this.
And if you're gonna be watching this on YouTube,
y'all have a good night.
Okay, we're done.
Not yet, hang on..

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}