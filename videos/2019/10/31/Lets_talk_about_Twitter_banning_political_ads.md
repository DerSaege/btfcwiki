---
title: Let's talk about Twitter banning political ads....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=rplRofOFFKQ) |
| Published | 2019/10/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Twitter decided to ban political ads on its platform, signaling a shift towards valuing ideas over money as speech.
- The move is seen as a step in the right direction, contrasting with Facebook's approach of exempting political ads from fact-checking.
- Twitter aims for ideas to gain reach organically through user interaction rather than through paid promotion.
- Paid advertising can make false information seem true through repetition, shaping beliefs regardless of facts.
- Beau warns of potential loopholes where individuals with platforms may be influenced by politicians or supporters offering money to push certain agendas.
- While some may argue this move limits free speech, Beau believes it actually empowers individuals to control the spread of ideas based on their interactions.
- Supporting this change could potentially reduce the influence of campaign contributions and make politicians more accountable to the people.
- Beau suggests following candidates and commentators on Twitter to ensure their ideas appear in your feed organically.
- By promoting the worth of ideas over financial backing, social media platforms could diminish the impact of campaign contributions on elections.

### Quotes

- "Twitter has decided to ban political ads across its platform. That's cool."
- "Ideas should stand and fall on their own."
- "They can't use repetition as a weapon."
- "I think it is going to help return a little bit of power to the average person if they use it."
- "If those campaign contributions can't buy the platforms that swing elections, they're not that important."

### Oneliner

Twitter's ban on political ads shifts focus from money to ideas, empowering users to control the spread of information and potentially reducing the influence of campaign contributions.

### Audience

Social media users

### On-the-ground actions from transcript

- Follow candidates and commentators on Twitter to support their organic reach (implied).

### Whats missing in summary

The full transcript provides a deeper insight into the potential impact of Twitter's ban on political ads and the importance of individuals actively engaging with content to shape the online narrative.

### Tags

#Twitter #PoliticalAds #SocialMedia #CampaignContributions #Empowerment


## Transcript
Well, howdy there, internet people, it's Bo again.
It's like I woke up this morning, rubbed a magic lamp, and Jack Dorsey popped out and
decided to give me a wish.
Twitter has decided to ban political ads across its platform.
That's cool.
That is cool.
That is a private company unilaterally deciding
that money is not speech.
That's what's happening.
There are some concerns because Twitter does not have a perfect record of
implementing ideas like this, but it's a step in the right direction.
Now they're doing it in their own self-interest, and it's certainly a shot
across the bow of Facebook who has decided to kind of exempt political ads
from fact checking.
Twitter's going the other way with it, banning political ads outright.
And the idea behind it is that a good idea would get reach on its own.
The supporters of that idea would retweet it and like it and comment and help spread
it.
This eliminates the idea that you can pay the company to repeat a talking point so people
believe it.
There's a whole bunch of things that people believe that just aren't true, but they've
They've heard it so often that it becomes true to them.
It's completely not factual, but it becomes truth because they've heard it so much.
That comes through a big budget.
That comes through an advertising budget and saying the same thing over and over and over
again.
So it's a step in the right direction.
Ideas should stand and fall on their own.
Now, does this mean that it's over on Twitter, that we're not going to have money influencing
politics?
Of course not.
They're going to find a way around it.
And I'll tell you who to watch, who to suddenly be leery of.
People like me.
People with platforms of some sort, because it won't be long until politicians or their
supporters come to them and say, hey, give you some money if you talk about this in this
way. It will happen. I can tell you I will not take money from a political
candidate, even one I support. However, there are a whole lot of people that
create content and content creation really doesn't pay that well. So a check
from a political party might be pretty enticing. So what you need to start
looking for is the same talking point coming from various commentators, especially if it
seems out of character for them.
And I imagine we'll start seeing it pretty quickly because this way they can still advertise.
They're just paying other people to say it for them rather than giving the money to Twitter.
So be leery of that.
And there are going to be arguments against this.
stifling free speech, it's this, it's that, no.
You can still make your tweet.
Your candidate can still make that tweet.
They just can't pay to have it repeated over and over and over again and brainwash people.
They can't use repetition as a weapon.
That idea has to carry on its own.
So overall, I think this is going to be a net win for us.
I think it is going to help return a little bit of power to the average person if they
use it, if they exercise it.
That ability to decide what ideas get out there and what doesn't.
Just based on your retweet, your like.
What that also means is right now you need to go follow on Twitter, if you use Twitter,
You need to go follow any candidate you support, any commentator you support.
Because if you don't, the ideas, they're not going to show up in your news feed, not necessarily.
Nobody's going to be paying to put them there simply because you kind of support them through
the algorithm and Twitter thinks you might be interested.
So anyway, overall, this is something we should support.
It's probably something Facebook should mimic.
Because if across all social media platforms, ideas have to be carried based on their worth,
those campaign contributions don't mean as much.
Social media swings elections today.
So, if those campaign contributions can't buy the platforms that swing elections, they're
not that important, not as important as they once were, which could, in theory, return
a little bit more power to the people.
Could make those politicians a little bit more responsive, maybe.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}