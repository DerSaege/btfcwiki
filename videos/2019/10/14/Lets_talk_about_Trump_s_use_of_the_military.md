---
title: Let's talk about Trump's use of the military....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=whTgLSevdc8) |
| Published | 2019/10/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Sitting with a recently separated veteran who was upset after watching the President of the United States brag about accepting payment to deploy US military forces overseas.
- Criticizing the notion that recent actions were about bringing troops home, pointing out that it was more about anti-imperialism and enabling other countries' ambitions.
- Expressing concern that the US military is now seen as a mercenary force for Saudi Arabia due to financial agreements.
- Questioning the morality of deploying US military forces overseas as an elective option that can be bought, rather than a duty required for national security and freedom.
- Mentioning the uncertainty surrounding Trump's statements and the potential impact on how the US military is perceived in the Middle East.
- Indicating that US soldiers are now viewed as mercenaries being bought and sold, serving as bait for conflicts in the Middle East.
- Noting the historical perspective that war has always been intertwined with money and power, with soldiers essentially being mercenaries.
- Quoting General Butler, a highly decorated Marine, who referred to himself as a "gangster for capitalism" and acknowledged serving as muscle for big business interests.
- Describing how the Department of Defense has effectively become a private military contractor, sending troops overseas based on financial incentives rather than national interests.
- Suggesting that individuals considering joining the military should be aware that private firms may offer better pay and more competent leadership.

### Quotes

- "Deploying US military forces overseas should not be elective. It should be the only alternative."
- "The might of the U.S. military, well, it's on the market. It can be bought."
- "War as a racket always has been. It is possibly the oldest, easily the most profitable, surely the most vicious."
- "US servicemen have been bought and sold for a very, very long time. It's just normally not done this cheaply."
- "The Department defense has become the world's largest private military contractor."

### Oneliner

Beau criticizes the commodification of US military forces, exposing how they are being viewed as mercenaries for hire rather than defenders of national security.

### Audience

Veterans and anti-war advocates.

### On-the-ground actions from transcript

- Contact organizations supporting veterans' rights and well-being (implied).
- Educate yourself on the impacts of militarization in foreign policy (generated).
- Advocate for policies that prioritize national security over financial incentives (implied).

### Whats missing in summary

The full transcript provides a deep dive into the financial motivations behind US military deployments and challenges the narrative of military interventions solely for national security purposes.

### Tags

#USMilitary #WarProfiteering #AntiImperialism #Veterans #NationalSecurity


## Transcript
Well, howdy there, internet people, it's Beau again.
So I was sitting with a recently separated veteran.
Just got out when they heard the news and they got to watch the President of
the United States, the Commander in Chief, brag and
boast about accepting payment to deploy US military force.
forces overseas.
I can't repeat what was said next in polite company.
You know, over the last few days, there's been a lot of
buzzwords, a lot of slogans thrown around.
Bring him home.
It was never about bringing him home.
It was never about bringing him home.
Well, he's just getting rid of those foreign entanglements.
How's that working out for you?
No, he's not.
It was never an objective of his.
It was anti-imperialism.
No, it wasn't.
It was making a hole
so Turkey could exercise their imperialist ambitions. And now the U.S.
military has become the hired goon
of Saudi Arabia's imperialist ambitions.
Why? Because Saudi Arabia has agreed to pay us for everything we're doing.
quote. You know, if it was in the interest of the American people, if it was in the interest
of national security, if it was in defense of American freedom, no payment would be necessary.
It would be duty, right? It would be something that was required, something we had to do,
not something that would be done for the heist bitter.
Deploying US military forces overseas should not be elective.
It should be the only alternative.
It should be the last option on the table.
But it's not.
And now it's something that can be bought.
At least that's the impression.
That's how this reads.
And the thing is, it's Trump.
So we don't even know if this is true.
He could just be making it up.
him talking about a deal, his statements about his other deals haven't been entirely accurate,
but it doesn't matter because he said it, and it's already been translated.
It's already all over the Middle East.
And how do you think it reads?
The might of the U.S. military, well, it's on the market.
It can be bought.
The lives of U.S. soldiers can be bought and sold.
It doesn't matter and the U.S. military is now viewed throughout the Middle East as Saudi
Arabia's do-boy.
That's how this reads, we're the hired help.
And why, because they can be a lightning rod, they're bait.
I know it's being sold as a deterrent against Iran because the thousands of other troops
we have in the Middle East isn't a deterrent.
They're a lightning rod, they're bait for anybody that has an issue with the kingdom,
anybody that has a problem with Saudi Arabia, because now they've got a juicy target.
Two birds with one stone, show they're tough.
Maybe we should thank Trump though, because he certainly pulled the veil back, let everybody
see what it is, because it's always been like this.
It's always been about money and power, war is just a continuation of politics by other
means, right?
It has always been like this.
The difference is before, it could be cloaked in the defense of American freedom.
It was harder to cast U.S. soldiers as mercenaries, not anymore.
They're literally being bought.
War as a racket always has been.
It is possibly the oldest, easily the most profitable, surely the most vicious.
It's the only one that's international in scope and it's the only one profits are reckoned
in dollars and losses in lives.
Pretty unpatriotic statement, right?
Y'all can hold your hate mail.
That was a guy named General Butler.
And before you get too angry, he's one of the most decorated Marines in history, received
the Medal of Honor, twice.
Give me another quote from him.
I spent 33 years and four months in active military service
as a member of this country's most agile fighting force,
the Marine Corps.
I served in all commissioned ranks,
from second lieutenant to major general.
And during that period, I spent most of my time
as a high class muscle man for big business, for Wall Street,
and for the bankers.
In short, I was a racketeer.
a gangster for capitalism.
And for the record, this guy is actually a hero.
There was an attempted coup in the United States at one point, and the planners actually
reached out to this guy, and he is alleged to have responded by saying however many troops
they raised, he would raise the same number to oppose them because his only interest was
protecting democracy.
The thing is, what Butler realized is that US servicemen have been bought and sold for
a very, very long time. It's just normally not done this cheaply. There's normally something
in it for the American people, something that could be cast as a benefit to them. Now this
time that's just money. We're sending them over there because they paid us. The Department
defense has become the world's largest private military contractor and understand those people
who are looking to sign up, they can no longer delude themselves with images of bomb and
apple pie and patriotism because of the President of the United States flat out said, well,
if they pay us enough, we'll send you.
If you're going to be a contractor, understand that there are a lot of firms that pay way
more than the US military does.
And generally, in those firms, the guy over you who is sending you somewhere, at least
he knows what he's doing.
It's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}