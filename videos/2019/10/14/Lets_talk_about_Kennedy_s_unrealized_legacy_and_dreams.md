---
title: Let's talk about Kennedy's unrealized legacy and dreams....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=A1qOu5rEIeQ) |
| Published | 2019/10/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Kennedy is often mythologized as the most significant president in American history, associated with Camelot and the Golden Age.
- Kennedy's famous quote "Ask not what your country can do for you, ask what you can do for your country" has a great impact.
- Kennedy's vision aimed at using education programs to empower people to stand on their own.
- The Green Berets, an organization that flourished under Kennedy, could have been utilized differently according to his original vision.
- In 1960, Kennedy introduced the Peace Corps, envisioning Americans traveling globally to do good.
- The Peace Corps, originally intended for humanitarian purposes, eventually became a tool for US foreign policy.
- Beau advocates for a shift towards asking what you can do for society rather than focusing solely on what society can do for you.
- He stresses the importance of community networks at the local level to build communities without government intervention.
- Beau dreams of individuals helping each other across borders and without government interference, envisioning a world where people help people.
- He encourages action towards building community networks and helping others without borders or government involvement.

### Quotes

- "Ask not what society can do for you, but ask what you can do for society."
- "Just people helping people."
- "Choose to do it. Not because it's easy, but because it's hard."

### Oneliner

President Kennedy's legacy, from the Peace Corps to community networks, inspires a vision of people helping people beyond borders and government involvement.

### Audience

Global citizens

### On-the-ground actions from transcript

- Build community networks at the local level to empower communities (suggested)
- Help others without borders or government intervention (implied)

### Whats missing in summary

The full transcript provides a deeper insight into President Kennedy's legacy, the Peace Corps' transformation, and the importance of community networks in fostering a world where individuals help each other across borders without government interference.

### Tags

#PresidentKennedy #PeaceCorps #CommunityNetworks #GlobalCitizenship #PeopleHelpingPeople


## Transcript
Well, howdy there, internet people, it's Bo again.
You know, President Kennedy is easily the most mythologized
president in American history, Camelot.
The Golden Age, and his quotes, man, are great.
Ask not what your country can do for you.
ask what you can do for your country
sounds good
promise that dream got corrupted
country
became synonymous with government
doing something for your country
became synonymous with doing something for your betters
that's messed up
because
Kennedy's legacy was a whole bunch of programs that were built to use education to help people
stand on their own.
And yeah, with everything going on today and in the headlines, I could certainly talk about
the Green Berets, again, an organization that came into their own under Kennedy.
I don't want to do that because it just shows another way his legacy has been undermined
because I think in Kennedy's vision, they would be training people in a lot of the countries
that we're currently supporting.
Let me talk about something else.
Because today, in 1960, was the first time he mentioned the Peace Corps.
time he publicly brought it up at a speech at Michigan.
But that's another one that's got this unrealized legacy.
The idea, just amazing.
Americans traveling the world doing good.
Problem is, government is not in the business of doing good.
not in the business of going around and helping people.
It's really not.
It's in the business of advancing
its own national interests.
So the Peace Corps very predictably
became a tool of US foreign policy.
What was supposed to be the world's EMTs
became another way to open the door for the world's policemen.
And it's messed up because, you know, the common argument against this is that war
is big business, infrastructure is big business too, it really is.
We don't have to hurt people to make money.
infrastructure is huge would make just as much money as war just have to
retool society to do it. Ask not what your country can do for you. It's got
that inherent nationalism in it and that was the problem. It limited it. It gave it
geographic region that it was supposed to benefit. I'd say ask not what society
can do for you, but ask what you can do for society. You know, I talk about
community networks, networks at the local level that help build communities from
the ground up a lot. And yeah, we're going to go around next summer and help people
build these and that's definitely on the agenda and once it's done I would love
to see a community network in Atlanta send somebody to Chicago to help them
build community gardens or whatever see the see one in Colorado send somebody to
village in Mexico to help them get clean water and all of it done without
government and without borders just people helping people. Can you imagine
that? I know people often say you know you got this audience now what are you
gonna do. I need a call to action, right? That's it. That's it. And I know, it's a dream.
It's definitely not easy. But I think we should choose to do it. Not because it's easy, but
because it's hard.
Anyway, it's just a thought.
y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}