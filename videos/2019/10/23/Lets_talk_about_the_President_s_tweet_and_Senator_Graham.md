---
title: Let's talk about the President's tweet and Senator Graham....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xg_LnDyhKmE) |
| Published | 2019/10/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President mentioned Emmett Till, leading to the term "lynching" surfacing in political discourse.
- Joe Biden used the term 20 years ago, but it was still wrong.
- Republicans claimed impeachment proceedings were like a lynching, which is false.
- Senator Lindsey Graham echoed this sentiment, showing ignorance of history and the Constitution.
- Graham failed to understand that due process in impeachment occurs during the Senate trial.
- The impeachment inquiry is not extrajudicial; it follows a constitutional process.
- Wealthy individuals can't halt investigations, unlike what some may be accustomed to.
- President Trump's involvement in an actual lynching-like scenario was advocating for the execution of innocent people.
- Lynchings are not ancient history, with the most recent recorded case in 2011.
- Over 3,400 recorded lynchings of Black Americans occurred, dispelling the idea that it's ancient history or insignificant in magnitude.

### Quotes

- "The impeachment process is exactly nothing like a lynching."
- "The closest thing to a lynching that President Trump has been involved in is when he took out a full-page ad advocating for the execution of people who turned out to be innocent."
- "Over 3,400 recorded lynchings of Black Americans occurred."

### Oneliner

Beau clarifies the misuse of the term "lynching" in political discourse and sheds light on the historical and constitutional inaccuracies surrounding it.

### Audience

Politically aware individuals

### On-the-ground actions from transcript

- Educate others on the historical significance and brutality of lynchings (implied)

### Whats missing in summary

The emotional impact of using historical atrocities for political gain

### Tags

#EmmettTill #Lynching #Impeachment #HistoricalInaccuracy #Constitution


## Transcript
Well, howdy there, internet people, it's Bob again.
Sometimes it feels like the president actually watches this.
Brought up a young boy named Emmett Till the other day, and then lo and behold,
a certain word has found its way into the president's vocabulary, and
then into his tweets, and then when the rightful outcry occurred,
Senators attempted to defend it.
Now, to be honest, he's not the first politician to use this language.
Joe Biden did 20 years ago, 20 years ago.
It was stupid and horrible then, because not much changed.
So what did he say?
He said, so someday if a Democrat becomes president and
the Republicans win the House, even by a tiny margin,
they can impeach the president without due process or fairness or any legal rights.
All Republicans must remember what they are witnessing here...a lynching.
Feel like this should go without saying, but obviously it doesn't.
The impeachment process is exactly nothing like a lynching.
That's the premeditated extrajudicial informal public execution of someone.
Doesn't seem like what's going on here.
To reiterate, this is nothing like lynching in any sense.
Enter Senator Lindsey Graham from the great state of South Carolina who said this is a
lynching in every sense because he's completely ignorant of history.
He goes on to say he's never seen a situation in which the accused can't confront the accuser
and whines about due process, which is also in the president's tweet because Senator
Graham has just become a parrot for the president.
The thing is, had either of these men ever read the Constitution, they would understand
that what parallels due process in impeachment proceedings occurs in the Senate during the
trial.
So if he doesn't get it, Senator Graham, that would be your fault.
The fact that somebody has to explain that to a senator and the president is appalling,
especially considering I think Graham is like chair of the Judiciary Committee.
He should really know that.
This isn't extrajudicial.
This is exactly how the process is supposed to work.
I understand there is nothing even stipulating that the House has to inform
the President that an impeachment investigation is going on.
And that's what this is, it's an inquiry.
I understand that the moneyed classes are used to being able to intervene
during an investigation and get something stopped
before it goes too far?
That's not how it works for most of the population.
That's not how it works in the impeachment process.
Let's just cut to the chase.
The closest thing to a lynching that President Trump
has been involved in is when he took out a full-page ad
advocating for the execution of people who
turned out to be innocent.
And that's the thing about lynchings.
In most cases, the people,
the victims,
are innocent.
We uh,
we all read that statement today.
Doesn't appear that he is.
But hey, don't worry,
he'll get his due process.
Now one of the other things that I noticed today was people seem to think that this is
ancient history or that it wasn't that big of a problem.
So 3,437 recorded lynchings of black Americans, probably
another 1,000 or so whites.
The most recent recorded one was in 2011,
a man named James Craig Anderson.
It's not ancient history, and those are just the ones that are found out.
Those are the ones where they know what happened.
It's not ancient history.
It's certainly not something that should be used as political jargon to help invigorate
your bigot base.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}