---
title: Let's talk about storming the impeachment inquiry....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=eWH0_TE_JaE) |
| Published | 2019/10/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republican lawmakers stormed the impeachment inquiry to disrupt the process.
- McConnell instructed them to attack the process instead of defending the President's actions.
- Closed-door proceedings, like the one in question, are common and not unusual.
- Those excluded from the hearing are either on the wrong committees or lack security clearance knowledge.
- The double standard exists as normal individuals engaging in similar actions face severe consequences compared to lawmakers.
- The President's lawyer argued for immunity from prosecution, suggesting a disregard for the law.
- The focus on attacking the process signifies a prioritization of party over upholding the Constitution and justice.

### Quotes

- "They care more about their party than they do the Constitution."
- "They care more about their party than you."

### Oneliner

Republican lawmakers disrupt impeachment inquiry, prioritizing party over Constitution and justice.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Challenge political double standards and demand accountability for actions (implied)
- Stay informed and advocate for transparency in political processes (implied)

### Whats missing in summary

Insight into the specific details of the closed-door proceedings and the testimony that was deemed "devastating."

### Tags

#Impeachment #PoliticalDoubleStandards #Accountability #Transparency #Justice


## Transcript
Well, howdy there, Internet people, it's Bo again.
So we got us a bona fide circus up there on Capitol Hill.
Should have seen this coming.
Republican lawmakers stormed the impeachment inquiry,
because what they had to say was that dire, that important,
that they stormed a skiff carrying cell phones.
Skiff is a bug-proof room, by the way, electronic devices aren't allowed inside.
But we should have known this was going to happen, should have seen this coming.
Because McConnell just told them, Mitch McConnell just told them, attack the process, attack
the tactics, because they can't defend the President's actions anymore.
Yesterday's testimony was devastating.
today, attack the process. What does that mean? It means undermine the Constitution
because that's where the process is laid out. It means obstruct Congress. It means
violate their oaths. And like good little foot soldiers, they did exactly what they
were told. And what was this dire complaint that all these theatrics were over? Well,
we're not allowed in the hearing, but other Republicans are. Just, you're not, because
Because you're not on the right committee.
This is incredibly common, happens all the time, happened a lot during Benghazi.
Closed door proceedings are very common.
Now you're not allowed in because, well, in addition to being on the wrong committees,
you're too dumb to know you can't carry a cell phone into a skiff.
I would assume that that would preclude you.
Now, for us normal folk, us commoners, had we done this, we would have been arrested,
thrown in jail, been rendered ineligible for security clearance for the rest of our lives.
That's what would have happened.
What's going to happen to them?
Probably not that, not much.
Probably won't do anything because we're not a nation of laws anymore.
It's become very, very obvious.
In fact, the president's lawyer was in court today arguing that if he had murdered someone
on Fifth Avenue they couldn't investigate it. He was immune from prosecution until he
left office. We're not in national laws anymore. So, while we would go to jail, our betters,
nothing will happen to them, very unlikely, because it is a big club and they're in it.
We can expect more of this. Since they can't defend the President's actions, they have
They have to attack the process.
They have to attack the foundations of the country.
They have to attempt to undermine the Constitution
because they care more about their party
than they do the Constitution.
They care more about their party
than they do Ukrainian lies.
They care more about their party than justice.
They care more about their party than you.
Anyway.
It's just the thought.
Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}