---
title: Let's talk about the foreign policy we should have and the one we do....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=id54nN0Lh90) |
| Published | 2019/10/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces the topic of foreign policy and questions the foreign policy the country should have.
- He refers to the Declaration of Independence, focusing on life, liberty, pursuit of happiness, and self-determination as fundamental principles.
- Beau points out that promoting happiness through enforcement is impractical in foreign policy.
- He distinguishes between freedom and liberty, noting that not everyone values liberty over safety.
- Beau advocates for promoting life and self-determination in universal foreign policy.
- He contrasts the ideal foreign policy focused on promoting life and self-determination with the current dominance-oriented foreign policy.
- Beau uses the example of Syria to illustrate how foreign policy stances should evolve based on changing situations while prioritizing life and self-determination.
- He criticizes actions that go against the principles of life, liberty, pursuit of happiness, and self-determination in foreign policy decisions.
- Beau stresses the importance of evolving positions based on reality and new information in foreign policy advocacy.
- He concludes by reminding viewers to adapt their opinions as situations evolve and to prioritize the well-being and rights of individuals in foreign policy decisions.

### Quotes

- "Life, liberty, pursuit of happiness, and self-determination."
- "Ideas stand and fall on their own."
- "You have to continue to evolve your position based on the reality."
- "A whole bunch of people are going to lose their life, liberty, pursuit of happiness and self-determination."
- "Y'all have a good night."

### Oneliner

Beau delves into the essence of foreign policy, advocating for promoting life and self-determination while critiquing the dominance-focused current approach.

### Audience

Policy advocates, activists

### On-the-ground actions from transcript

- Advocate for foreign policies that prioritize life and self-determination (advocated)
- Evolve positions based on changing realities and new information in foreign policy advocacy (advocated)

### Whats missing in summary

The full transcript provides a detailed analysis of how foreign policy should be guided by principles of life, liberty, pursuit of happiness, and self-determination, with the need to adapt positions based on evolving situations.


## Transcript
Well howdy there internet people, it's Bo again.
So tonight we're going to talk about foreign policy.
We're going to talk about the foreign policy we have and the foreign policy we should have.
That sounds crazy.
How do you know what foreign policy we should have?
How can we figure that out?
We could look at our first foreign policy document and that would tell us a lot and
it would give us good goals because it was pure.
was pure because nobody had power yet, so nobody was trying to maintain it.
We hold these truths to be self-evident, that all men are created equal, that they are endowed
by their Creator with certain unalienable rights, that among these are life, liberty,
the pursuit of happiness, that to secure these rights, governments are instituted among men,
deriving their just power from the consent of the governed.
That whenever any form of government becomes destructive to these ends, it is the right
the people to alter or abolish it. It's part of the Declaration of Independence.
So what do you have there? Life, liberty, pursuit of happiness, and self-determination.
Those are the four things. Pursuit of happiness, that varies widely by
culture what people think of as happiness. That's very subjective. I'm
gonna go around raising armies to enforce happiness. You can promote it,
but you can't enforce it. So that kind of comes off the table as far as foreign
policy as we think of it today.
Then, liberty.
Liberty is slightly less subjective.
In the United States, we talk about freedom.
Talk about it.
A few people really remember what it means, but we talk about it.
Freedom and freedom aren't really the same thing.
There are a lot of people who value safety more than liberty.
I know there's a bunch of people that are going to put a founding father quote down
there and that's fine.
But we can't enforce our idea of liberty on people, otherwise it's not really liberty
now is it?
Not only that, it flies in the face of self-determination.
And then life.
That one's obvious, and that one is universal.
So on a universal level, you have life and self-determination.
That's what our foreign policy should be promoting.
That's what it should be about.
And if we did that, man, it'd be great because it was so pure, there was no power to be maintained
at this point, it was just ideas.
Had we stuck to this, think of the outcomes.
How would the Native Americans have been treated if we actually stuck to this?
Life, liberty, pursuit of happiness, and self-determination.
There would have been no trail of tears.
Had we stuck to this, slavery couldn't have existed because it relied on a transatlantic
trade that violated it.
They became corrupted almost immediately after drafting this because then there was power
to be had.
The most radical revolutionary becomes a conservative the day after the rebellion type of thing.
But at this moment, it was pure, it was pure, it was just about people.
So this is the foreign policy we should promote.
The problem with that is that we exist in the real world, and this isn't the foreign
policy we currently have.
Foreign policy we currently have is about dominance.
It's about maintaining global power.
So what that means is that if you're advocating for this kind of foreign policy, your opinion
on any given situation has to change as the situation on the ground changes.
an example since it's fresh in everybody's mind Syria when we started
propagandizing for regime change this is before the protests because understand
we triggered that no we shouldn't do that we should not do that
self-determination it's an issue don't do it not only that you're probably going
start a civil war which are always the worst as far as loss of life, especially for civilians.
Shouldn't do that. Civil war pops off.
Okay, so it's already started. Maybe we arm the Kurds' self-determination.
We shouldn't get involved. Let it resolve as quickly as possible.
Boots on the ground. Decisive. Be decisive. Do everything as quickly as possible.
Take out leadership. Don't arm the moderate rebels.
The situation stabilizes, you want to preserve life, you keep it stable, maybe you help the
Kurds, self-determination.
You don't withdraw, retreat, and destabilize or create a power vacuum and initiate a new
offensive which creates more loss of life.
Those are the evolutions of my stances on Syria because my vision of foreign policy
is guided by that, life, liberty, pursuit of happiness, self-determination.
Now there are some people who will say, no, well, we have to protect Syria's sovereignty,
their self-determination.
Those borders were drawn up because they didn't have self-determination, because the people
there weren't considered.
Their colonial borders literally don't care about them.
They're not important to me.
The people in those areas, that's important.
And if you have a section, say the northeast, that doesn't want to be a part of Syria,
forcing it to be a part is imperialist.
And it flies in the face of life, liberty, pursuit of happiness, and self-determination.
It's hard because you end up changing your position.
And that's one of the things that people as a whole don't like to do.
We don't like to be seen as being indecisive.
But if you're advocating for something that doesn't exist, and the thing that does exist
flies in the face of it.
You have to continue to evolve your position based on the reality.
It's real easy to say, well we shouldn't have gotten involved.
Well that's great, but we did.
And now, if we're reckless, a whole bunch of people are going to die.
A whole bunch of people are going to lose their life, liberty, pursuit of happiness
and self-determination.
Now, in this case, because it does appear that the Syrian government and the Russian
government were very aware of what was going to happen, the loss of life so far has been
minimal, that doesn't mean that it's going to stay that way.
Probably won't.
When you take a stance, understand that new information should change your opinion.
Ideas stand and fall on their own, and as the situation on the ground, in any situation
evolves, your opinion should evolve with it. Anyway, it's just a thought. Y'all have a
Good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}