---
title: Let's talk about the Ben Shapiro vs Beto feud....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=H9h6uwxvuR4) |
| Published | 2019/10/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Responding to a trending campaign promise, Beau disapproves of the sensationalist rhetoric used by Ben Shapiro to incite fear.
- Ben Shapiro creates a false dilemma by suggesting that exposing children to certain academic content will make them emulate it.
- The slippery slope argument leads to an extreme scenario where armed men are at the door to take Shapiro's children, a situation Beau finds absurd.
- Beau criticizes Shapiro's lack of responsible coverage and suggests using constitutional mechanisms to challenge laws instead of inciting fear.
- Beau questions the masculinity-driven tough guy rhetoric that Shapiro employs, pointing out the real issue with using violence as a solution.
- Beau admonishes Shapiro for irresponsibly advocating for picking up a gun as a solution to perceived problems with schools, calling it embarrassing and dangerous.
- Violence is not always the answer according to Beau, who likens it to a tool that must be used responsibly to avoid destructive consequences.

### Quotes

- "Schools are the problem, pick up a gun."
- "You should be embarrassed."
- "It's not the solution. It's not even a problem."
- "That violence is always the answer. It's not."
- "If you keep trying to use that one tool, you're going to destroy your home."

### Oneliner

Responding to sensationalist fear-mongering, Beau disapproves of violence as a solution, criticizing irresponsible rhetoric and advocating for responsible action.

### Audience

Internet users, viewers

### On-the-ground actions from transcript

- Challenge unconstitutional laws using constitutional mechanisms (implied)

### Whats missing in summary

The full transcript provides a deeper insight into the dangers of irresponsible rhetoric and the importance of advocating for responsible action in the face of sensationalism.

### Tags

#Sensationalism #IrresponsibleRhetoric #Violence #ResponsibleAction #ChallengeLaws


## Transcript
Well, howdy there, internet people, it's Beau again.
So first, let me start off by saying I'm doing this video under duress.
I don't like giving people who use this kind of rhetoric additional buzz.
But a whole lot of people sent me messages about it,
mainly because I think they understood that, yeah, this is a perfect example of
what I've been talking about.
It is, it is.
But I really didn't want to highlight it because I don't like
helping them achieve their goals, which is to be sensationalist and incite people and
stuff like that.
But I've noticed it's been trending on Twitter for like two days now.
So my silence isn't really going to help anything.
So what happened?
There was a campaign promise made, a campaign promise, and we know how often those come
to reality.
From there Ben Shapiro creates this straw man and then uses a slippery slope to create
a false dilemma.
The idea at the heart of it is that exposing children to something in an academic setting
will cause them to emulate it.
He doesn't believe this any more than I do.
I am certain that there is no objection from Shapiro to the school board over learning
about the Donner Party. He knows that this isn't true. And the sideline of it is that
somehow the schools will undo the values you've instilled in your children. I'm certain that
there are behaviors in the schools and society in general and on TV that he does not want
his children emulating. If you think that those influences are going to alter the values
you instill in your children and they're going to undermine them, that says a lot about how
you instill those values and how well you instill those values. And then there's the
idea at the heart of this that somehow people choose to be gay. We're just going
to skip right over that but suffice it to say there's a little bit of evidence
that suggests otherwise mountains of it. Okay so the original campaign promise
was something pretty benign I didn't even go look it up I think it was
something like if a religious institution had an anti-gay curriculum they would lose
their tax-exempt status or something like that, a religious school.
By the time the slippery slope is over, no joke, there are armed men at the door coming
to take Ben Shapiro's children.
This is the scene he creates.
No exaggeration.
At which point, he says, at that point, I only have two options, and that's to leave
the country or pick up a gun.
Okay.
So somebody who prides himself on being an intellectual, I guess homeschooling isn't
a thing.
That doesn't exist.
Or how about you use the mechanisms outlined in the Constitution to challenge an unconstitutional
law?
Because here's the thing, I think it would be great if bigoted organizations lost their
tax-exempt status. I think it would be fantastic, but I know that would never, never make it
pass the first appeal. That would get struck down immediately. The state cannot impose
an ideological purity test on a church. That's not going to happen. It's not going to happen.
So this whole thing is based off something that would never actually occur. It can't
happen. But rather than just saying that, he chose to incite people. You know, what
he could have said was, you know, politician X doesn't even understand the
Constitution, he made this stupid campaign promise, this would be a good
reason to get out and vote though so we don't have to go through the court
battle. That's it. That's the responsible coverage of this. It's not going to go
anywhere. But instead he chose to incite people, chose to scare people, chose to fear a
monger, and he used that rhetoric. I'm gonna get my gun. Okay then what? What
happens next? Let's follow your slippery slope. Let's follow your chain of events.
Armed men come to the door and you meet them at the door. The outcome of that
is one of two things. Neither of which is good for you or your children. This is
horrible. This is horrible. And Ben, you don't strike him as a warrior. And right
now there's a whole bunch of people out there going, oh he insulted his manhood.
No I didn't. No I didn't. The ability to harm people in combat has nothing to do
with masculinity. The problem is in the United States we think it does. It doesn't. That
statement should be no more of an affront to his manhood than saying, Ben, I don't
think you're a farmer. I don't think you're a fisherman. I don't think you're a painter.
That's a profession. And yes, by saying you're going to meet them at the door where they
They have a 180 degree field of fire on you, I'm pretty sure it's not your profession.
But it's that tough guy rhetoric that feels like it's important, because it drives
clicks because people have lost what masculinity really is.
And at the end of the day, what's the takeaway from his message?
Schools are the problem, pick up a gun.
How irresponsible is that?
In this day and age, with everything going on, this is what you use your platform for.
Schools are going to do this.
Pick up a gun.
You should be embarrassed.
embarrassed for you. It's not the solution. It's not even a problem. This is never going
to happen. But you're out there inciting people. And somebody, somewhere, takes what you say
seriously. You have a responsibility to your audience. I guess it's not going to be you
that pays the price for it though.
It's that idea that violence is always the answer.
It's not.
It's a tool like any other.
And it's like if you're trying to fix your house and the only tool you have is a hammer.
Yeah you can drive a few nails, maybe fix a few items, but if you keep trying to use
that one tool, you're going to destroy your home and that's what's going to happen here
because people just can't behave responsibly.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}