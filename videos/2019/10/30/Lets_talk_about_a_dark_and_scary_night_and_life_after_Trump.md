---
title: Let's talk about a dark and scary night and life after Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=e34LFUNheYY) |
| Published | 2019/10/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Dark and stormy nights are when real monsters, meaning other men, roam the earth to harm people.
- The conditions during dark and stormy nights, like rain, limited visibility, and dulled senses, make it easier for predators to attack.
- Groups of indoctrinated men are ingrained with the idea of never quitting and always accomplishing the mission.
- After Trump leaves office, the real work begins for the community to build the society they want.
- The next president won't have to do much to be better than Trump, so it's up to the community to push, guide, and lead them.
- It's not just about resistance; it's about building helpful ideas and creating power structures for a better society.

### Quotes

- "Dark and stormy nights is when it happens, even today."
- "We're waiting for our dark and stormy night to do it."
- "It's not just resistance, it's not destroying the ideas that are harmful, it's building the ideas that are helpful."
- "It's up to us."

### Oneliner

Dark and stormy nights make it easier for predators to harm, but after Trump, the community must build a better society by guiding and pushing the next leaders.

### Audience

Community members

### On-the-ground actions from transcript

- Guide, push, and lead the next president to build a better society (implied)
- Create local power structures to influence change (implied)

### Whats missing in summary

The full transcript provides a deeper insight into the importance of community action in shaping a better society post-Trump.

### Tags

#CommunityAction #BuildingSociety #GuidingLeadership #DarkAndStormyNights


## Transcript
Well, howdy there internet people, it's Bo again and it's a dark and stormy night
You ever wonder why we're afraid of dark and stormy nights
It's because since the beginning of time man has known
That's the time
That is the time that is the moment
when the real monsters roam the earth and by real monsters I mean other men.
Other men set out to do something bad to other people.
Doesn't matter if you're in a cave defending your fire
or you're in a compound somewhere
defending your ideology. Dark and stormy nights is when it happens, even today. Groups of
crazy and special men will wait, if they can, for all the conditions to be right. The moon,
weather, everything. It's not magic, it's not an appeal to some mystical force, it's
just taking advantage of everything that's available. See, on dark and stormy nights,
your senses are dulled. If you're the person that the monsters are coming to get, well,
Well, the rain falling, that ambient noise, makes them harder to hear.
The ground being wet, no dry leaves to crinkle.
The cold water falling through the air keeps smells down.
It's dark, your visibility is limited.
You want to stay inside because it's wet, so you're not really looking.
All of these things come together and they will wait for the right moment if they can.
The funny thing about that is that all of those groups, they're all indoctrinated, ingrained
with the idea that you do not quit under any circumstance,
you accomplish the mission.
Night stalkers don't quit.
Though I be the lone survivor, it's there.
It's part of the institutional culture of these groups.
We have to adopt it because something's
going to happen eventually.
And when it does, some people are going to think it's over.
The mission is complete, but it's not.
See, at some point in the future,
our long national nightmare will end.
President Trump will leave office.
But that's when the real work is going to start.
See, right now, our mission, short-term,
yeah, Trumpers out.
That didn't mean much.
Right now we're focused on harm reduction, harm mitigation.
We're wanting to destroy those policies and those ideas
that would destroy this country if left alone,
would destroy this society.
Once he's gone, well, that's our dark and stormy night.
We waited, we waited for the right moment,
But that's when we have to move forward, and it's going to have to be us, it's not going
to be some savior candidate.
The next president is not going to have to do much to get reelected.
Trump has lowered the bar so much that as long as they're not an active harm to the
world, they're going to be better than the last guy.
So that means it's up to us.
It's up to us.
going to have to do the work, we're going to have to switch gears, no longer just
resist, but go on the initiative. Take the initiative, seize that, move forward, try
to build that society that we want, the one that was promised. So it's not just
resistance, it's not destroying the ideas that are harmful, it's building the
ideas that are helpful. It's creating those power structures. And we're waiting
for our dark and stormy night to do it. We're waiting for that moment when the
main obstacle is out of the way. But we can't rely on the next president. We're
going to have to hold them to the same standard that we're holding President
Trump to. If not, it doesn't matter. They'll improve things a little, but the
new norm, this, it's going to stick around. They have to be amazing and we have to
make them amazing no matter who it is. We have to push them, guide them, lead them
instead of being ruled by them. You've got to create those local power
structures. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}