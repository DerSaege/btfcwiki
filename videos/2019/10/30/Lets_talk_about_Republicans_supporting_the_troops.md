---
title: Let's talk about Republicans supporting the troops.....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ZNmsGvWBdfU) |
| Published | 2019/10/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Questioning the GOP's supposed support for troops.
- Contrasting the treatment of Tongo-Tongo ambush and Benghazi incident.
- Republicans walking out of a bipartisan bill for women veterans due to amendments.
- Creation of whistleblower protection office in the VA not protecting whistleblowers.
- Accusing Republicans of caring more about appearances than veterans.
- Criticizing Trump's actions regarding veterans and the VA.
- Criticizing Republicans for siding with Trump over Lieutenant Colonel testimony.
- Suggesting that recent actions indicate Republicans don't truly support the troops.
- Pointing out the GOP's stance on gun rights and restrictions under Trump compared to Obama.
- Encouraging viewers to look beyond political talking points and observe actions.

### Quotes

- "Maybe they don't really support the troops."
- "They care about appearances."
- "You should stop listening to the talking points and actually look at what they do."

### Oneliner

Beau questions GOP's support for troops, criticizes their actions towards veterans, and urges viewers to look beyond political talking points.

### Audience

Veterans, political activists

### On-the-ground actions from transcript

- Contact organizations supporting veterans' rights (implied)
- Join advocacy groups for whistleblowers in the VA (implied)
- Organize community events to raise awareness about mistreatment of veterans (implied)

### Whats missing in summary

Full context and emotional depth from Beau's commentary.

### Tags

#GOP #Veterans #Whistleblowers #GunRights #SupportTheTroops


## Transcript
Well, howdy there, internet people, it's Beau again.
So tonight we're going to talk about an idea.
It's a very commonly held idea.
We think we may want to examine it a little bit.
Because the idea is that the GOP, the Grand Old Party, well,
they will always support the troops.
They will back the troops no matter what.
Republicans will back the troops. But is there any truth to that? I mean we hear it all the
time but is there any truth to that? I mean sure sometimes I mean they make a spectacle
about it occasionally. Y'all remember that incident in Africa? U.S. service people out
there on what we would just say, let's just call it an unpublicized operation. They weren't
supposed to be out there overnight, but they were. Pretty bad planning on the whole thing.
They had no air support, and it started to go bad.
When it first started it wasn't a huge deal, just a few opposition fighters on motorcycles.
And they pushed them back past the burning building into that massive onslaught.
Fighting lasted hours.
And when it was over, they were stripped and left there in the dirt.
And there were hearings about it, right?
No, there weren't.
Right now, I imagine you probably think I'm talking about Benghazi.
I'm not.
Talking about the Tongo-Tongo ambush.
The what?
Exactly.
Very similar scenarios.
similar scenarios. But one got hearings, closed-door hearings, sham hearings I guess is what we
would call them today. The other one, I think, what was it, what was the president accused
of saying? They know what they signed up for, he knew what he signed up for, something like
that. One guy turned into a circus, the other one got nothing. The difference is who was
in power. Tongo Tongo happened under Trump. Benghazi happened under Obama.
Today there was a bipartisan bill that would have helped women veterans. And it had support,
but all of a sudden the Republicans wanted to add some amendments to it there at the
end and Democrats weren't having it so the Republicans stormed out walked out
without voting because they don't actually care about the veterans it
would have helped they wanted to politicize it this was something
everybody supported and understand the amendments that they wanted to add from
where I'm sitting they probably something need to be talked about but do
Do you care so little about U.S. veterans that you would risk upending the whole thing
because you can't get everything you want?
Sounds a little crazy to me.
But they did create that whistleblower protection office there in the VA, which as we have come
to find out, does the exact opposite, didn't protect whistleblowers.
In fact, their names were not shielded, and they suffered repercussions, because they
talked about it.
Because the truth is, they don't care about the treatment of veterans.
That's not something they care about.
What they care about is making it look like they did something.
So they wanted to stop those stories from coming out.
Sounds a lot like what's going on on the national stage right now.
this was just in the VA. I actually remember that whole little ceremony
that Trump put on when he did this because it was his campaign promise. He
was going to fix the VA. No, all he did was shut him up so we couldn't hear
about it. Don't actually fix the problems. You know, they took money from DOD to
build that stupid wall, apparently taking money from foreign governments and renting
the troops out like they're rider trucks, stab our allies and our friends in the back.
Commander in chief does.
And then the Republicans in Congress come along and grab the knife and go hide it for
Make it seem like it wasn't a big deal.
And then today, Lieutenant Colonel, Purple Heart recipient, he's called a spy, working
with the NSC.
And they had the gall to try to slander him and libel him because his testimony is going
to be damaging to the president, just like the whistleblower protection office.
They don't care about the truth.
They don't care about veterans.
They don't care about national security.
They care about appearances.
And they're going to slander and libel a man who gave decades of his life to this country,
to protect Trump.
The funny thing about calling him a spy is that he was actually able to pass the background
checks that Trump's team couldn't.
So is it true?
I don't know.
Recent actions seem to suggest otherwise.
Maybe they don't really support the troops.
that's not the party for the veterans you know aside from that the only other
thing veterans seem to universally care about are gun rights and the reality is
that in three years President Trump has placed more restrictions on firearms
than Obama did in eight I mean what did Obama do expanded concealed carry to
national parks and put a bunch of surplus 1911s on the market, gasp! Yeah, he's the
gun grabber everybody made him out to be. And then you got Trump, has his Attorney General
wandering Capitol Hill trying to find out what kind of gun control they can pass. Maybe
You should stop listening to the talking points and actually look at what they do.
Anyway, that's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}