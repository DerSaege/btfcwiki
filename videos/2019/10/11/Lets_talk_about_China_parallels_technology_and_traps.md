---
title: Let's talk about China, parallels, technology, and traps....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=04Edv148XiE) |
| Published | 2019/10/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces a comment about learning about cultures only when something bad happens to them, specifically mentioning at-risk cultures.
- The parallel between China and the United States is discussed, focusing on technology becoming a trap through the example of the Uyghurs.
- The Uyghurs, residing in Xinjiang, China, have a rich history and were part of the Republic of East Turkestan before becoming part of China in 1949.
- The Uyghurs practice Sufism, a mystical component of Islam, which fundamentalists may not approve of due to its newer traditions and rituals.
- Beijing's uncomfortable relationship with the Uyghurs intensified after some Uyghurs were found in a training camp in Afghanistan, leading to riots, bombings, and a security clampdown in the 1990s and early 2000s.
- Beijing justified their actions as benevolent civilizing efforts, but they actually subjugated and relocated the Uyghurs, bringing in millions of Han Chinese for settlement.
- Mass surveillance on the Uyghurs began in 2016 with technologies like smartphones, DNA scanning, facial recognition, and more, under the guise of preventing extremism.
- A million adults are currently in camps termed "vocational centers" by China, facing erasure of their culture and ethnicity through forced assimilation.
- Uyghur children are being taught Chinese culture instead of their own, further erasing their identity.
- Beau draws parallels between the Uyghur experience and the native experience in the United States, reflecting on the rapid progression from technological advancements to mass internment camps.

### Quotes

- "We only learn about it when something bad happens to them."
- "Beijing was worried about extremification."
- "They're erasing the culture. They're erasing the ethnicity."
- "Mass surveillance started. Eight years later there's camps."
- "So that's a little brief primer on who they are."

### Oneliner

Beau introduces the issue of learning about cultures only after tragedies and delves into the parallels between China and the U.S. through the lens of the Uyghurs' plight, illustrating rapid cultural erasure through mass surveillance and internment camps.

### Audience

Activists, Humanitarians

### On-the-ground actions from transcript

- Contact human rights organizations to support the Uyghur cause (suggested)
- Join protests or awareness campaigns advocating for the rights of the Uyghur community (exemplified)
- Support initiatives providing aid to Uyghur refugees or families affected by the crisis (implied)

### Whats missing in summary

The full transcript provides a comprehensive understanding of the cultural erasure and human rights violations faced by the Uyghur community in China, urging viewers to take action and stand against such injustices.

### Tags

#Uyghurs #HumanRights #CulturalErasure #MassSurveillance #InternmentCamps #China #Activism #Awareness


## Transcript
Well, howdy there, internet people, it's Bo again.
So the other day on Twitter, I saw this comment.
And it basically said, you know, the sad part is that we
only ever really learn about these cultures when something
bad happens to them.
The person was talking about at-risk cultures.
Man, he's right.
He's right, and that's part of the problem.
We only learn about it when something bad happens to them,
Which means, in order to get people to care,
we have to tell them who they are first.
And that takes time.
And meanwhile, that bad thing is still happening.
He's right.
It's a big problem.
So let's fix it.
So tonight, we're going to talk about some parallels that
exist between China and the United States
that some people may not know about.
And we're going to see how technology
can become a trap, a literal trap.
And to highlight this, we are going to talk about the Uyghurs.
Now, if you don't know who they are,
I promise you've seen headlines like U-I-G-H-U-R.
Now, they live in an area of China called Xinjiang.
Literally translated, that means a new frontier.
It's called that because in the 1800s,
the Han started settling it under the Qing dynasty.
And the parallels
between what was happening then
and what was happening in the United States at the same time,
they're pretty striking. It was pretty much the same thing. Go west, young man.
And when you get out there,
well, you run into this
bizarre, strange culture
people that aren't like you.
of course, what do you do? Well, you have to civilize them, right? And that's what
happened. Now, the Uyghurs, for their part, they've been there pretty much forever.
They're descendants from the traders that Marco Polo saw on his journey.
And because they're on this transcontinental trade route, you get to see
something unique in that part of the world when you're walking through those
towns. Blonde hair, blue eyes, green eyes, stuff like that. It's kind of odd. Now in
1933 they set up their own country. It's called the Republic of East Turkestan. It
did not last long. When the communists took over in 1949, well that pretty much
that was it. It became part of China. Now as far as religion, they practice a
version of Islam. It's not really a version of Islam, it's Sufism. It adds a
mystical component. I've heard people describe it as Islam with extra credit.
it. Now because a lot of these traditions and these rituals are newer, the
fundamentalists don't really like it that much and we'll just leave it at
that. Now this region that they're in, it's oil rich. Of course it is. Otherwise
wouldn't matter, right? They are technically in what is called an
autonomous zone, but it is being rapidly settled by the Han, which is the ethnic
minority, white people in the US.
This whole time the relationship between the Uyghurs and Beijing has been uncomfortable,
to put it mildly.
And then, well, a couple dozen Uyghurs were discovered at a training camp in Afghanistan.
And that certainly caught Beijing's attention.
Now, to be fair, prior to this, there had been some riots and some bombings in the 90s,
in the 1990s and early 2000s.
That led to a security clampdown, a pretty massive one.
Now, Beijing, of course, cast this as them being the benevolent civilizer.
They did what all benevolent civilizers do.
They subjugated them, they relocated them, and they brought in their own people, in this
case by the millions.
This happened in the 1990s and 2000s.
The Han that were moved in, almost all of them worked in oil and natural gas extraction,
so there was a monetary motive as well.
Then this thing that didn't really seem like a big deal at the time happened.
The Uyghurs got 3G, 3G cell service, they got smartphones, and mass surveillance came
with it.
Beijing was worried about extremification, and that was in 2011.
By 2016, they were gathering DNA, scanning faces, getting voice prints, conducting stop
and frisk, and you had to open up your cell phone.
They mapped people's relationships, their social networks, everything, became a massive
data collection operation.
They interviewed millions of people to determine who was good, which ones were the good ones,
who was trustworthy.
To do this, they brought in 90,000 cops, almost all of which were Han, who view the weaker
as primitive.
They also brought in about a million other government employees.
Now that was 2016, where we have today camps, about a million adults in camps.
China is currently calling them vocational centers.
Prior to them being outed by journalists, they just denied it.
When it was just rumors, they're like, no, these things don't exist.
Right now they admit they do, but their casting is kind of voluntary, which is not true.
Journalists have been allowed to go to some of the camps, but it is very staged.
It is ridiculously staged.
It's more staged than that show Border Patrol put on.
It's obviously faked.
So we're talking about 10% of the adult population, a million people.
about the kids? Well of course they're in government run schools with Han
administrators. They are taught Chinese culture rather than Uighur. They don't
learn Arabic script which is how their people typically write. They're erasing
the culture. They're erasing the ethnicity. There's a word for that. So
So it definitely parallels the native experience in the United States.
The other thing I think people should notice is how quickly that progression happened.
2011 they got 3G.
Mass surveillance started.
Eight years later there's camps.
Really quick.
So that's a little brief primer on who they are.
And I got a feeling they may become the next cause of the day, where everybody gasps at
something that happened.
So when and if it does, at least you'll know who they are.
Eight years.
Anyway, it's just a thought.
y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}