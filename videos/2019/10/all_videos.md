# All videos from October, 2019
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2019-10-31: Let's talk about staying centered with Carey Wedler.... (<a href="https://youtube.com/watch?v=3ydf0cohiNU">watch</a> || <a href="/videos/2019/10/31/Lets_talk_about_staying_centered_with_Carey_Wedler">transcript &amp; editable summary</a>)

Carrie Wedler advocates for agorism over violent revolution, prioritizing emotional healing, mindfulness, and curiosity to navigate turbulent times and advocate for positive change.

</summary>

"My solution is not violence in the streets and taking down the government with guns as much as I support gun rights."
"I think this is a really sweet one, and I think it's so symbolic of new beginnings."
"Just start to become curious about what comes up for you."
"I'm banned from Twitter, but my ha ha ha, it's so funny that I'm banned from Twitter. It's hilarious."

### AI summary (High error rate! Edit errors on video page)

Started activism due to disillusionment with politics after supporting Obama in 2008 and realizing the continuation of wars.
Believes in evolution over revolution, advocating for agorism and peaceful protest against systemic injustices.
Trauma from her parents' divorce shaped her emotional responses, leading her to pursue healing and introspection.
Recommends yoga and meditation for maintaining calm amidst chaotic news cycles.
Emphasizes the importance of addressing deeper emotional wounds to prevent reactive responses.
Advocates for skepticism towards all media sources, both mainstream and independent, promoting critical thinking.
Plans to continue making videos challenging political authority and advocating for emotional exploration.
Suggests planting trees as a simple yet impactful solution to mitigate carbon dioxide and contribute positively to the environment.
Encourages curiosity and mindfulness towards emotional reactions as a pathway to healing and self-awareness.

Actions:

for activists, community members,
Plant trees or support organizations that focus on tree planting to mitigate carbon dioxide (implied)
Incorporate yoga and meditation practices into daily routines for emotional well-being (implied)
Cultivate curiosity and mindfulness towards emotional responses as a pathway to healing (implied)
</details>
<details>
<summary>
2019-10-31: Let's talk about Twitter banning political ads.... (<a href="https://youtube.com/watch?v=rplRofOFFKQ">watch</a> || <a href="/videos/2019/10/31/Lets_talk_about_Twitter_banning_political_ads">transcript &amp; editable summary</a>)

Twitter's ban on political ads shifts focus from money to ideas, empowering users to control the spread of information and potentially reducing the influence of campaign contributions.

</summary>

"Twitter has decided to ban political ads across its platform. That's cool."
"Ideas should stand and fall on their own."
"They can't use repetition as a weapon."
"I think it is going to help return a little bit of power to the average person if they use it."
"If those campaign contributions can't buy the platforms that swing elections, they're not that important."

### AI summary (High error rate! Edit errors on video page)

Twitter decided to ban political ads on its platform, signaling a shift towards valuing ideas over money as speech.
The move is seen as a step in the right direction, contrasting with Facebook's approach of exempting political ads from fact-checking.
Twitter aims for ideas to gain reach organically through user interaction rather than through paid promotion.
Paid advertising can make false information seem true through repetition, shaping beliefs regardless of facts.
Beau warns of potential loopholes where individuals with platforms may be influenced by politicians or supporters offering money to push certain agendas.
While some may argue this move limits free speech, Beau believes it actually empowers individuals to control the spread of ideas based on their interactions.
Supporting this change could potentially reduce the influence of campaign contributions and make politicians more accountable to the people.
Beau suggests following candidates and commentators on Twitter to ensure their ideas appear in your feed organically.
By promoting the worth of ideas over financial backing, social media platforms could diminish the impact of campaign contributions on elections.

Actions:

for social media users,
Follow candidates and commentators on Twitter to support their organic reach (implied).
</details>
<details>
<summary>
2019-10-30: Let's talk about a dark and scary night and life after Trump.... (<a href="https://youtube.com/watch?v=e34LFUNheYY">watch</a> || <a href="/videos/2019/10/30/Lets_talk_about_a_dark_and_scary_night_and_life_after_Trump">transcript &amp; editable summary</a>)

Dark and stormy nights make it easier for predators to harm, but after Trump, the community must build a better society by guiding and pushing the next leaders.

</summary>

"Dark and stormy nights is when it happens, even today."
"We're waiting for our dark and stormy night to do it."
"It's not just resistance, it's not destroying the ideas that are harmful, it's building the ideas that are helpful."
"It's up to us."

### AI summary (High error rate! Edit errors on video page)

Dark and stormy nights are when real monsters, meaning other men, roam the earth to harm people.
The conditions during dark and stormy nights, like rain, limited visibility, and dulled senses, make it easier for predators to attack.
Groups of indoctrinated men are ingrained with the idea of never quitting and always accomplishing the mission.
After Trump leaves office, the real work begins for the community to build the society they want.
The next president won't have to do much to be better than Trump, so it's up to the community to push, guide, and lead them.
It's not just about resistance; it's about building helpful ideas and creating power structures for a better society.

Actions:

for community members,
Guide, push, and lead the next president to build a better society (implied)
Create local power structures to influence change (implied)
</details>
<details>
<summary>
2019-10-30: Let's talk about Republicans supporting the troops..... (<a href="https://youtube.com/watch?v=ZNmsGvWBdfU">watch</a> || <a href="/videos/2019/10/30/Lets_talk_about_Republicans_supporting_the_troops">transcript &amp; editable summary</a>)

Beau questions GOP's support for troops, criticizes their actions towards veterans, and urges viewers to look beyond political talking points.

</summary>

"Maybe they don't really support the troops."
"They care about appearances."
"You should stop listening to the talking points and actually look at what they do."

### AI summary (High error rate! Edit errors on video page)

Questioning the GOP's supposed support for troops.
Contrasting the treatment of Tongo-Tongo ambush and Benghazi incident.
Republicans walking out of a bipartisan bill for women veterans due to amendments.
Creation of whistleblower protection office in the VA not protecting whistleblowers.
Accusing Republicans of caring more about appearances than veterans.
Criticizing Trump's actions regarding veterans and the VA.
Criticizing Republicans for siding with Trump over Lieutenant Colonel testimony.
Suggesting that recent actions indicate Republicans don't truly support the troops.
Pointing out the GOP's stance on gun rights and restrictions under Trump compared to Obama.
Encouraging viewers to look beyond political talking points and observe actions.

Actions:

for veterans, political activists,
Contact organizations supporting veterans' rights (implied)
Join advocacy groups for whistleblowers in the VA (implied)
Organize community events to raise awareness about mistreatment of veterans (implied)
</details>
<details>
<summary>
2019-10-28: We talked about impeachment.... (<a href="https://youtube.com/watch?v=_VJoSOPUQVc">watch</a> || <a href="/videos/2019/10/28/We_talked_about_impeachment">transcript &amp; editable summary</a>)

Beau explains the constitutional validity of the impeachment process, dismantles misleading analogies, and warns against unchecked presidential power.

</summary>

"The impeachment process is exactly nothing like a lynching."
"They have to attack the foundations of the country."
"It’s not a coup. It’s the way the process is laid out."
"This argument that is being made is the legal argument to turn Trump into a dictator."
"Trump becomes Tsar, and all men are slaves to the Tsar."

### AI summary (High error rate! Edit errors on video page)

Explains that the impeachment process is not unconstitutional and follows the rules outlined in the Constitution.
Clarifies the difference between impeachment proceedings and a criminal prosecution.
Addresses the comparison made by President Trump and Senator Lindsay Graham to a lynching, explaining the inaccuracy of the analogy.
Points out that the impeachment proceedings are not a criminal prosecution, so due process applies at a later stage.
Describes the closed-door nature of the impeachment proceedings to prevent witnesses from coordinating stories.
Criticizes Republican lawmakers for storming the impeachment inquiry and obstructing the process.
Raises concerns about the legal argument that the President is immune from all criminal liability, potentially leading to unchecked power and abuse.

Actions:

for activists, constitutional scholars, concerned citizens,
Contact elected officials to express support for upholding constitutional processes (suggested)
Educate others on the differences between impeachment proceedings and criminal prosecutions (exemplified)
Organize community forums to raise awareness about the implications of unchecked presidential power (implied)
</details>
<details>
<summary>
2019-10-28: Let's talk about bagging the Big Baghdadi Wolf.... (<a href="https://youtube.com/watch?v=3NhSJ4SdA4A">watch</a> || <a href="/videos/2019/10/28/Lets_talk_about_bagging_the_Big_Baghdadi_Wolf">transcript &amp; editable summary</a>)

The big bad wolf may be captured, but disrupting command structures without clarity on succession can have dangerous consequences, as history shows.

</summary>

"It's the end, right? They're defeated. Maybe not."
"If they take out all of them, if they get rid of all of the politically savvy people, you have an incredibly dangerous organization."
"You didn't defeat us."

### AI summary (High error rate! Edit errors on video page)

The big bad wolf has been captured, but the organization may not be entirely defeated.
Disrupting command structures can be risky if the next leaders are unknown.
Historical examples, like the Irish Rebellion of 1916, show the consequences of eliminating leadership without a clear plan for succession.
Two key figures, Devalera and Collins, exemplify the different types of leaders within organizations.
The strategy of eliminating only one faction within an organization to render it combat ineffective but still intact.
The goal is to force politically savvy individuals towards peace negotiations due to incompetent military leadership.
The U.S. strategy may involve targeting key figures to destabilize organizations.
The operation against the captured individual was expedited due to the president's actions.
Success in neutralizing competent commanders will lead to minimal activities from the group.
Failure to eliminate key figures may result in infighting or the rise of a more dangerous organization.

Actions:

for strategists, policymakers, analysts.,
Analyze the leadership structures within organizations to understand potential outcomes (implied).
Monitor for signs of infighting or power struggles within groups (implied).
Stay informed about international events to understand the implications of strategic decisions (implied).
</details>
<details>
<summary>
2019-10-28: Let's talk about a baseball chant, a hashtag, Chicago, and Trump.... (<a href="https://youtube.com/watch?v=lVfLKfPMsdU">watch</a> || <a href="/videos/2019/10/28/Lets_talk_about_a_baseball_chant_a_hashtag_Chicago_and_Trump">transcript &amp; editable summary</a>)

President's approval should be soaring, facing backlash from veterans, baseball stadium booing, and Chicago demonstration, urging for inflammatory rhetoric to understand dangers of political climate and prevent descent into dictatorship.

</summary>

"Be patriotic. This is un-American. Don't do this. Just sit there, be a good German. It'll all work out. No, we know how that works out. We're not doing it again."
"You're sending us there."
"I'm not going to be a person that just follows orders."

### AI summary (High error rate! Edit errors on video page)

President's approval should be soaring due to recent wins but it's not; facing backlash from veterans, baseball stadium booing, and Chicago demonstration.
Media is trying to pacify Americans, labeling dissent as un-American and unpatriotic.
Beau argues that inflammatory rhetoric is necessary to understand the dangers of the current political climate.
Warning against the potential for a dictatorship if civil liberties are disregarded.
Beau criticizes conservatives for only now opposing dangerous rhetoric when it's aimed at them.
The time for civil debate ended when Trump's lawyers argued he was above the law in court.
Beau believes it's time for people to understand the reality of the current political situation and not shy away from inflammatory debate.
Criticism towards those who followed Trump blindly, selling out the country for a red hat and slogan.
Emphasizes the importance of using strong rhetoric to prevent a descent into dictatorship.
Beau expresses readiness to stand against authoritarianism, even if it means going beyond rhetoric.

Actions:

for concerned citizens,
Challenge dangerous rhetoric and stand against authoritarianism (exemplified)
Engage in open, honest, and potentially inflammatory debate to address the current political situation (exemplified)
</details>
<details>
<summary>
2019-10-28: Let's talk about #TeamTrees.... (<a href="https://youtube.com/watch?v=f5xaL23roXg">watch</a> || <a href="/videos/2019/10/28/Lets_talk_about_TeamTrees">transcript &amp; editable summary</a>)

Mr. Beast collaborates with Arbor Day Foundation to plant 20 million trees by December 2022, setting a goal of raising $20 million, already reaching $5 million in two days.

</summary>

"Your donation to them [Arbor Day Foundation] would be tax deductible in most cases."
"Definitely kind of jumping on team trees here."
"It's actually going to create some meaningful impact."

### AI summary (High error rate! Edit errors on video page)

Mr. Beast, a 21-year-old YouTuber known for silly stunts, is involved in a unique project to raise $20 million for planting trees.
Partnered with the Arbor Day Foundation, the goal is to plant 20 million trees across the country by December 2022.
The foundation, with a 50-year history, will ensure that each dollar donated equals one tree planted.
The project aims to plant native species, promoting environmental impact.
Within just two days, $5 million has already been raised for the cause.
Beau encourages viewers to donate to the Arbor Day Foundation, mentioning that donations may be tax-deductible.
Beyond donating, Beau suggests planting native fruit trees in personal yards to enhance food security, reduce grocery bills, and foster community spirit.
Beau acknowledges the project's success in engaging a wide audience, with various creative videos showcasing the tree-planting efforts.
Unlike typical online challenges, this initiative is praised for its potential significant impact on the environment.
Beau urges viewers to support the cause by contributing a dollar or two.

Actions:

for environmental enthusiasts, tree advocates,
Donate a dollar or two to the Arbor Day Foundation to support tree-planting efforts (suggested).
Plant native fruit trees in your yard to enhance food security, reduce grocery bills, and build community spirit (implied).
</details>
<details>
<summary>
2019-10-26: Let's talk about a little hope and solutions.... (<a href="https://youtube.com/watch?v=uhzuYC5M6yo">watch</a> || <a href="/videos/2019/10/26/Lets_talk_about_a_little_hope_and_solutions">transcript &amp; editable summary</a>)

Beau addresses the prevalence of negativity, challenges the reliance on future leaders, and calls for individual action and unity to bring about real change.

</summary>

"We have everything we need. It's us."
"It's us, the individuals down here on the bottom, the commoners that are going to solve it."

### AI summary (High error rate! Edit errors on video page)

Addressing solutions and how to reach them.
Noting the prevalence of negative content online.
Emphasizing the importance of identifying problems.
Acknowledging the lack of action despite watching videos.
Stating the impact of even a few individuals taking action.
Describing the constant presence of negativity in daily life.
Pointing out issues like environmental degradation and corruption.
Challenging the belief that the next leader will solve everything.
Calling for leadership over rulership.
Asserting that each person has the capacity to lead and solve problems.
Expressing gratitude for Patreon support.
Planning to expand the podcast with diverse guest speakers.
Emphasizing the need for collaboration and unity for real change.

Actions:

for community members,
Reach out to diverse individuals to join in discussing and seeking solutions (suggested)
Collaborate with people from various backgrounds to address today's issues (implied)
Come together with fellow community members to drive real change (implied)
</details>
<details>
<summary>
2019-10-25: Let's talk about the US deploying more troops to Syria.... (<a href="https://youtube.com/watch?v=LCGFQcvE3eQ">watch</a> || <a href="/videos/2019/10/25/Lets_talk_about_the_US_deploying_more_troops_to_Syria">transcript &amp; editable summary</a>)

The US is sending more troops and armor to Syria, contradicting claims of withdrawal, revealing ulterior motives, and risking complicity in ethnic cleansing.

</summary>

"Sending armor to Syria contradicts the idea of a withdrawal."
"More troops are being sent than withdrawn. It's about money and creating a U.S. zone."
"The President is complicit in ethnic cleansing by allowing Turkish military forces in."
"It's not anti-imperialist or anti-colonialist, just packaged differently for supporters."
"We sold out powerful Middle Eastern actors for questionable foreign policy decisions."

### AI summary (High error rate! Edit errors on video page)

The US is sending armor, including tanks, to Syria to protect a gas plant, contradicting the idea of a withdrawal.
Sending tanks without ground troops makes them vulnerable to anti-tank missiles in Syria.
Ground troops, maintenance crews, supply guys, infantry, surveillance units, drones, and jets are needed for this operation.
Despite claims of de-escalation, more troops are being sent to Syria than withdrawn.
The true motive behind these actions is money and creating a U.S. zone in northeastern Syria.
The President suggested Kurds move to oil regions, complicit in ethnic cleansing by allowing Turkish military forces in.
The actions taken do not represent anti-imperialism or anti-colonialism but serve other interests.
The US is prioritizing protecting gas plants despite becoming a net energy exporter soon.
Beau criticizes the administration for selling out powerful Middle Eastern actors for questionable foreign policy decisions.

Actions:

for activists, policymakers.,
Contact policymakers to express opposition to military actions in Syria (suggested).
Support organizations advocating for the protection of vulnerable groups in conflict zones (exemplified).
Join local movements opposing questionable foreign policy decisions (implied).
</details>
<details>
<summary>
2019-10-25: Let's talk about Katie Hill.... (<a href="https://youtube.com/watch?v=vzs8ggPww94">watch</a> || <a href="/videos/2019/10/25/Lets_talk_about_Katie_Hill">transcript &amp; editable summary</a>)

Beau points out the double standard faced by women in politics through the scandal surrounding Congressperson Hill's photos, urging a shift in priorities towards more critical issues.

</summary>

"Women are still held to this false idea, this standard that they need to remain pure and that they need to behave in a certain way."
"None of this is any of our business."
"This country has its priorities mixed up."
"It's just a thought."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Congressperson Hill's scandalous photos surfaced, sparking a national debate.
The photos show adults engaged in consensual activity and Hill using a substance that was once illegal.
Hill has a tattoo that may have been an iron cross, a symbol associated with various groups.
She is the first bisexual representative from California and these photos confirm her admission.
There's ethical concern about her dating an employee, although not illegal.
Beau questions why these photos are circulating; suggests it's due to Hill being a pretty woman.
He challenges the double standard women face regarding purity and behavior.
Beau points out the lack of harm in Hill's actions and questions the public interest.
He criticizes the misplaced priorities of focusing on these photos rather than more critical issues.
Beau contrasts the attention on Hill's photos with the President's lawyers arguing for his right to kill people.

Actions:

for political activists,
Support organizations advocating for gender equality and against slut-shaming (implied)
Engage in critical discourse on gender bias and double standards in politics (implied)
</details>
<details>
<summary>
2019-10-25: Let's talk about AOC vs DOD.... (<a href="https://youtube.com/watch?v=GKDbGc3g9KA">watch</a> || <a href="/videos/2019/10/25/Lets_talk_about_AOC_vs_DOD">transcript &amp; editable summary</a>)

AOC and the Department of Defense present contrasting reports on climate change, with experts warning of imminent threats and the urgent need for action.

</summary>

"AOC is right. The environmentalists are right and the Department of Defense is co-signing that."
"This is a real threat. The people who are paid to decide what threats are, they say it is."
"It's your choice. You can listen to the Department of Defense, NASA, the Defense Intelligence Agency, environmental scientists."

### AI summary (High error rate! Edit errors on video page)

AOC and the Department of Defense released dueling reports on climate change.
AOC's report predicts rising sea levels displacing 600 million people, infectious diseases spreading, decreased food security, extreme weather incidents, and stress on the power grid.
The Department of Defense report, with input from NASA and the Defense Intelligence Agency, describes climate change as a huge threat to be dealt with immediately.
The U.S. Army is considering a culture change to reduce their carbon footprint and create fresh water from humidity for warfighters.
Beau fabricated AOC's report to draw attention to the seriousness of climate change and the need for action.
The Department of Defense, NASA, and environmental scientists all warn about the real threat of climate change.
Beau contrasts listening to experts versus dismissing climate change as fake news promoted by oil companies.

Actions:

for climate activists, policymakers,
Contact local policymakers to advocate for climate action (implied)
Join environmental organizations working towards climate solutions (implied)
Support initiatives that reduce carbon footprint in your community (implied)
</details>
<details>
<summary>
2019-10-24: Let's talk about the President's legal argument against all criminal liability.... (<a href="https://youtube.com/watch?v=Ce3f_2dV7sA">watch</a> || <a href="/videos/2019/10/24/Lets_talk_about_the_President_s_legal_argument_against_all_criminal_liability">transcript &amp; editable summary</a>)

President's legal team argued for total immunity, implying Trump could commit crimes without consequences, potentially paving the way for unchecked dictatorial powers.

</summary>

"He is total immunity from all forms of criminal liability."
"This argument that is being made is the legal argument to turn Trump into a dictator with absolute power."
"It's terrifying because that's not how it's being framed."
"Trump becomes Tsar, and all men are slaves to the Tsar."
"Meanwhile he's attempting to gain the power to do whatever he wants with no legal ramifications."

### AI summary (High error rate! Edit errors on video page)

President's legal team argued for total immunity from criminal liability.
Even judge found the argument bizarre and proposed a scenario of the president shooting someone on Fifth Avenue.
The legal argument implies the president could commit murder without facing any consequences.
Lack of criminal liability could allow the president to shut down voting stations or commit voter fraud with impunity.
Impeachment by Congress may not be a safeguard if the president has absolute power.
The argument aims to grant Trump dictatorial powers with no checks or balances.
Trump could become a dictator with unchecked authority if the argument is accepted.
Judges deciding on the president's immunity from criminal liability are essentially determining the fate of the country.
Granting total immunity could lead to the abuse of power and Trump acting as a Tsar.
This legal argument threatens to give Trump unrestricted power and evade legal consequences.

Actions:

for american citizens,
Contact elected representatives to express concerns about the implications of granting total immunity to the president (implied).
</details>
<details>
<summary>
2019-10-24: Let's talk about secret impeachment hearings and cop shows.... (<a href="https://youtube.com/watch?v=8yRxM-Z1tpo">watch</a> || <a href="/videos/2019/10/24/Lets_talk_about_secret_impeachment_hearings_and_cop_shows">transcript &amp; editable summary</a>)

Beau explains the rationale behind secret impeachment hearings and why attacking the process is the Republican's only defense strategy.

</summary>

"The hearings are in secret so the other witnesses don't know what's been said already. It's a way to keep them honest."
"They're trying to suppress evidence because they can't defend the president's actions. It's really that simple."

### AI summary (High error rate! Edit errors on video page)

Explains the reason behind holding impeachment hearings in secret compared to past cases like Clinton and Nixon where facts were already established.
Compares the secretive hearings to a cop show where witnesses are separated to prevent collaboration in testimonies.
Mentions that leaking testimonies is akin to a cop revealing information to suspects to test their honesty.
Notes that the Republicans attack the process because they cannot defend the President's actions.
Draws a parallel between defense strategies in criminal cases and the Republicans trying to suppress evidence.
Concludes by stating that the testimonies in the current impeachment process are critical compared to past cases where events were clear.

Actions:

for political enthusiasts, voters, activists,
Contact your representatives to advocate for transparent and fair impeachment proceedings (suggested)
Stay informed about the impeachment process and share accurate information with others (implied)
</details>
<details>
<summary>
2019-10-23: Let's talk about the foreign policy we should have and the one we do.... (<a href="https://youtube.com/watch?v=id54nN0Lh90">watch</a> || <a href="/videos/2019/10/23/Lets_talk_about_the_foreign_policy_we_should_have_and_the_one_we_do">transcript &amp; editable summary</a>)

Beau delves into the essence of foreign policy, advocating for promoting life and self-determination while critiquing the dominance-focused current approach.

</summary>

"Life, liberty, pursuit of happiness, and self-determination."
"Ideas stand and fall on their own."
"You have to continue to evolve your position based on the reality."
"A whole bunch of people are going to lose their life, liberty, pursuit of happiness and self-determination."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Beau introduces the topic of foreign policy and questions the foreign policy the country should have.
He refers to the Declaration of Independence, focusing on life, liberty, pursuit of happiness, and self-determination as fundamental principles.
Beau points out that promoting happiness through enforcement is impractical in foreign policy.
He distinguishes between freedom and liberty, noting that not everyone values liberty over safety.
Beau advocates for promoting life and self-determination in universal foreign policy.
He contrasts the ideal foreign policy focused on promoting life and self-determination with the current dominance-oriented foreign policy.
Beau uses the example of Syria to illustrate how foreign policy stances should evolve based on changing situations while prioritizing life and self-determination.
He criticizes actions that go against the principles of life, liberty, pursuit of happiness, and self-determination in foreign policy decisions.
Beau stresses the importance of evolving positions based on reality and new information in foreign policy advocacy.
He concludes by reminding viewers to adapt their opinions as situations evolve and to prioritize the well-being and rights of individuals in foreign policy decisions.

Actions:

for policy advocates, activists,
Advocate for foreign policies that prioritize life and self-determination (advocated)
Evolve positions based on changing realities and new information in foreign policy advocacy (advocated)
</details>
<details>
<summary>
2019-10-23: Let's talk about the President's tweet and Senator Graham.... (<a href="https://youtube.com/watch?v=xg_LnDyhKmE">watch</a> || <a href="/videos/2019/10/23/Lets_talk_about_the_President_s_tweet_and_Senator_Graham">transcript &amp; editable summary</a>)

Beau clarifies the misuse of the term "lynching" in political discourse and sheds light on the historical and constitutional inaccuracies surrounding it.

</summary>

"The impeachment process is exactly nothing like a lynching."
"The closest thing to a lynching that President Trump has been involved in is when he took out a full-page ad advocating for the execution of people who turned out to be innocent."
"Over 3,400 recorded lynchings of Black Americans occurred."

### AI summary (High error rate! Edit errors on video page)

President mentioned Emmett Till, leading to the term "lynching" surfacing in political discourse.
Joe Biden used the term 20 years ago, but it was still wrong.
Republicans claimed impeachment proceedings were like a lynching, which is false.
Senator Lindsey Graham echoed this sentiment, showing ignorance of history and the Constitution.
Graham failed to understand that due process in impeachment occurs during the Senate trial.
The impeachment inquiry is not extrajudicial; it follows a constitutional process.
Wealthy individuals can't halt investigations, unlike what some may be accustomed to.
President Trump's involvement in an actual lynching-like scenario was advocating for the execution of innocent people.
Lynchings are not ancient history, with the most recent recorded case in 2011.
Over 3,400 recorded lynchings of Black Americans occurred, dispelling the idea that it's ancient history or insignificant in magnitude.

Actions:

for politically aware individuals,
Educate others on the historical significance and brutality of lynchings (implied)
</details>
<details>
<summary>
2019-10-23: Let's talk about storming the impeachment inquiry.... (<a href="https://youtube.com/watch?v=eWH0_TE_JaE">watch</a> || <a href="/videos/2019/10/23/Lets_talk_about_storming_the_impeachment_inquiry">transcript &amp; editable summary</a>)

Republican lawmakers disrupt impeachment inquiry, prioritizing party over Constitution and justice.

</summary>

"They care more about their party than they do the Constitution."
"They care more about their party than you."

### AI summary (High error rate! Edit errors on video page)

Republican lawmakers stormed the impeachment inquiry to disrupt the process.
McConnell instructed them to attack the process instead of defending the President's actions.
Closed-door proceedings, like the one in question, are common and not unusual.
Those excluded from the hearing are either on the wrong committees or lack security clearance knowledge.
The double standard exists as normal individuals engaging in similar actions face severe consequences compared to lawmakers.
The President's lawyer argued for immunity from prosecution, suggesting a disregard for the law.
The focus on attacking the process signifies a prioritization of party over upholding the Constitution and justice.

Actions:

for politically engaged individuals,
Challenge political double standards and demand accountability for actions (implied)
Stay informed and advocate for transparency in political processes (implied)
</details>
<details>
<summary>
2019-10-22: Let's talk about goats, confusion, oil, and novels.... (<a href="https://youtube.com/watch?v=pKCurn1bO94">watch</a> || <a href="/videos/2019/10/22/Lets_talk_about_goats_confusion_oil_and_novels">transcript &amp; editable summary</a>)

US foreign policy in Syria under Trump: predictable chaos or calculated moves?

</summary>

"It is completely predictable if you accept the one assessment all of US intelligence agreed upon from the very beginning."
"Either he's a complete idiot or he's a useful one."
"We're living in a Tom Clancy novel and the President of the United States is compromised."

### AI summary (High error rate! Edit errors on video page)

US troops may stay in Syria to protect oil fields, contradicting arguments of supporters.
Troops leaving Syria are just moving to Western Iraq, waiting to go back.
Trump is perceived as anti-interventionist but allows US troops to protect natural resources in Syria.
Despite being labeled a peace lover, Trump threatens war with Iran.
Critics find Trump's foreign policy unpredictable, but Beau argues it is completely predictable.
Betraying allies and inability to recruit indigenous forces overseas have negative effects.
US actions benefit non-state actors and strengthen certain groups in Syria.
US decisions have made it harder to gain trust and support from other countries.
Russia can use US actions as propaganda against other nations.
Turkey's meeting with Russia shows a shift in power dynamics in the Middle East.
Beau questions whether Trump is incompetent or a useful tool in shaping foreign policy.

Actions:

for foreign policy analysts,
Question US foreign policy decisions (implied)
Stay informed on geopolitical shifts and implications (implied)
</details>
<details>
<summary>
2019-10-21: We talked about Trump's betrayal of the Kurds.... (<a href="https://youtube.com/watch?v=P3Eg-BvRUJg">watch</a> || <a href="/videos/2019/10/21/We_talked_about_Trump_s_betrayal_of_the_Kurds">transcript &amp; editable summary</a>)

The President's decision to allow Turkey's invasion of Syria and abandon Kurdish allies reveals the harsh realities of war and foreign policy.

</summary>

"War is just a continuation of politics by other means, right? It has always been like this."
"The Department of Defense has become the world's largest private military contractor."
"The only thing that's happening over there right now is waste."
"The American people. I got a feeling we're going to have to look at what we've done."
"We are not going to use and abuse indigenous forces like this and to just hang them out to dry when it's politically expedient."

### AI summary (High error rate! Edit errors on video page)

The President of the United States allowed Turkey to invade Syria, abandoning Kurdish allies who have fought alongside American soldiers for 15 years.
Turkish-backed militants committed atrocities against the Kurds, driving them to seek support from Russia.
The Kurds are a significant ally in the Middle East, not a country but an ethnic group who have been betrayed and sold out openly.
The U.S. military moved a small number of troops, enabling Turkey's invasion of Syria, leading to Russian and Syrian troops filling the power vacuum left by U.S. forces.
ISIS could potentially grow stronger due to recent events, posing a threat that might lead to U.S. military intervention in the future.
The term "special forces" refers specifically to Green Berets, who train indigenous forces and stand out as instructors in hostile environments.
The Department of Defense is seen as a private military contractor, with the U.S. military viewed as serving Saudi Arabia's interests in the Middle East.
War is depicted as a profitable and vicious enterprise that exposes soldiers to being bought and sold for money and power.
The current situation in the Middle East is characterized by waste, with slogans like "End the war" and "Anti-imperialism" failing to match the reality of the conflict.
The speaker calls for a reevaluation of how indigenous forces are used and discarded for political convenience.

Actions:

for american citizens,
Support organizations advocating for the protection and rights of indigenous forces (implied)
Advocate for a more transparent and accountable foreign policy that prioritizes ethical considerations (implied)
</details>
<details>
<summary>
2019-10-21: Let's talk about what we can learn from Lego bricks.... (<a href="https://youtube.com/watch?v=_R33lcUUs4g">watch</a> || <a href="/videos/2019/10/21/Lets_talk_about_what_we_can_learn_from_Lego_bricks">transcript &amp; editable summary</a>)

Beau at Legoland compares society to Lego sculptures, urging individuals to think for themselves and build a more inclusive community.

</summary>

"It might be time for bricks in the local community to start connecting and start building stuff on their own."
"The power is in the brick."
"Most bricks have an innate desire to create, rather than destroy."

### AI summary (High error rate! Edit errors on video page)

Took a break and visited Legoland with his kids, describing it as an amusement park with Lego-themed massive sculptures like elephants and dragons.
Compares Legoland sculptures to society and history, where the focus is often on individuals rather than the collective effort of every "brick."
Talks about how society often directs individuals on what to build, leaving out those who don't conform, and suggests it's time for "bricks" to start thinking for themselves.
Points out that the power lies in individuals connecting and building together, creating more colorful and inclusive images if they build for themselves.
Encourages local communities to connect, build their own images, and create something different and useful for all instead of just conforming to existing structures.

Actions:

for local community members,
Connect with local community members to start building projects together (implied)
Encourage creativity and inclusivity in community building efforts (implied)
</details>
<details>
<summary>
2019-10-21: Let's talk about steel and Emmett Till.... (<a href="https://youtube.com/watch?v=x5Iiw39Jbbc">watch</a> || <a href="/videos/2019/10/21/Lets_talk_about_steel_and_Emmett_Till">transcript &amp; editable summary</a>)

Beau delves into the story of Emmett Till, from his tragic murder to his enduring legacy represented by a new bulletproof steel sign.

</summary>

"His legacy was bulletproof, his mom was still unwavering, unflinching, just like that new sign."
"It doesn't matter if somebody shoots the sign or spray paints it or takes it. It's there. It's carved in stone and blood in American history."

### AI summary (High error rate! Edit errors on video page)

Introduction to discussing Emmett Till and his legacy with a focus on a new sign made of steel.
Emmett Till, a 14-year-old boy, was murdered in 1955 for allegedly whistling at a white woman in Mississippi.
The woman whose accusation led to Emmett Till's murder later admitted she fabricated parts of the story, and the accused men were acquitted.
Emmett Till's mother displayed incredible strength by having an open casket funeral for her son, revealing the brutal reality of his murder.
Photos of Emmett Till's body were published and became a catalyst for the civil rights movement.
Rosa Parks credited Emmett Till with strengthening her resolve in the fight for civil rights.
The acquittal of Emmett Till's murderers marked a turning point where the Black community realized they couldn't rely on the courts for justice.
Emmett Till's legacy continues to inspire, symbolized by a new bulletproof sign made of steel in his honor.
The significance of the steel sign lies in its resilience, mirroring the unwavering strength of Emmett Till's mother and the enduring impact of Emmett Till's story on American history.

Actions:

for history enthusiasts, social justice advocates,
Visit historical sites related to civil rights movements (implied)
Support organizations working towards racial justice (implied)
</details>
<details>
<summary>
2019-10-18: Let's talk about generals, business, and a coup.... (<a href="https://youtube.com/watch?v=_cd9317BgKM">watch</a> || <a href="/videos/2019/10/18/Lets_talk_about_generals_business_and_a_coup">transcript &amp; editable summary</a>)

General Butler's refusal to lead a coup against FDR prevented a potential fascist dictatorship in the U.S., showcasing the immense impact of individual choices on history.

</summary>

"One guy refused to take command. One guy refused. One person. Literally changed the entire course of human history."
"Assuming that the allegations are true, the world owes General Butler the entire world."

### AI summary (High error rate! Edit errors on video page)

Beau dives into the story of General Butler, who was a highly respected Marine with extensive experience in various wars.
FDR's progressive reforms didn't sit well with the wealthy business class, leading them to plan a coup to install a fascist government.
Wealthy and powerful figures like J.P. Morgan and Prescott Bush were implicated in the coup plan.
McGuire acted as an intermediary, gradually escalating the plan to have Butler lead a march on Washington and seize control.
Butler, being a staunch anti-capitalist, was the wrong choice for the coup leaders.
Butler exposed the plan by going straight to Congress, but newspapers dismissed it as a hoax initially.
Even though the truth came out later, no one was prosecuted for the coup attempt.
The coup attempt serves as a reminder of the dangers of political power plays and the importance of individuals' choices.
If Butler had agreed to lead the coup, the course of history could have been drastically altered.
The story underscores the significance of one person's decision in shaping the world's future.

Actions:

for history enthusiasts, advocates for democracy.,
Support and defend democratic institutions (exemplified).
Stay vigilant against threats to democracy (exemplified).
</details>
<details>
<summary>
2019-10-17: Let's talk about the constitutionality of impeachment.... (<a href="https://youtube.com/watch?v=H3tk7EMOmpY">watch</a> || <a href="/videos/2019/10/17/Lets_talk_about_the_constitutionality_of_impeachment">transcript &amp; editable summary</a>)

Beau explains the constitutionality of impeachment, debunking claims of unconstitutionality and clarifying the process with a focus on indictment and limited punishment.

</summary>

"The House has sole power over impeachment proceedings."
"There is nothing unconstitutional occurring."
"It's not a coup. It's the way the process is laid out."

### AI summary (High error rate! Edit errors on video page)

Explains the constitutionality of impeachment amid claims of unconstitutionality.
Emphasizes the impeachment process as the indictment phase.
Quotes Article 3 Section 2, which stipulates that impeachment trials are not criminal prosecutions.
Clarifies the limited punishment the Senate can impose: removal from office and disqualification from holding future office.
Points out that the President's right to pardon does not apply in cases of impeachment.
Mentions the requirements for the Senate during impeachment trials: under oath, Chief Justice presides, and a two-thirds majority is needed to convict.
Stresses that the President's right to confront accusers applies in a criminal trial, not impeachment.
Asserts that there is nothing unconstitutional about the current impeachment proceedings.
Addresses claims of denial of due process by explaining its inapplicability at this stage.
Concludes by debunking notions of a coup and affirming the legitimacy of the impeachment process.

Actions:

for citizens, activists, voters,
Stay informed on the impeachment proceedings and the constitutional aspects involved (suggested).
Educate others on the impeachment process and the constitutional guidelines (suggested).
</details>
<details>
<summary>
2019-10-17: Let's talk about black community defense groups.... (<a href="https://youtube.com/watch?v=T3A6hqI-Oxg">watch</a> || <a href="/videos/2019/10/17/Lets_talk_about_black_community_defense_groups">transcript &amp; editable summary</a>)

Beau advocates for establishing community defense organizations with a focus on community development, stressing the importance of winning the PR battle to avoid negative portrayal and scrutiny.

</summary>

"Creating that redundant power, creating that localized power structure is really important."
"The PR battle is what's gonna be important here."
"The first battle is a PR battle."
"And I don't think that they will become a target as long as they win the PR war up front."
"The US has changed, it's gotten better, but it hadn't changed that much."

### AI summary (High error rate! Edit errors on video page)

There is a call to action in minority communities post the Jefferson shooting, where a woman was shot by a cop in her own home.
The call to action is to establish a community defense force, reminiscent of the original days of the Black Panther Party.
Beau advocates for localized community defense organizations for all communities, not just particular racial or ethnic groups.
He believes such organizations can avoid the pitfalls the Black Panthers faced and should focus on community development beyond just defense.
Beau suggests avoiding a militant structure like the Black Panthers had to prevent negative media portrayal and law enforcement scrutiny.
Winning the PR battle is critical for such organizations to avoid being labeled negatively by the media or law enforcement.
Beau acknowledges the concerns and challenges but believes that with proper messaging and community support, these organizations can succeed.
He encourages the establishment of organizations that create redundant and localized power structures to benefit communities.
Beau stresses the importance of steering away from a paramilitary image to maintain a positive perception and prevent becoming a target.
The focus should be on community development activities like microloans, mentoring, childcare, and skill development within these organizations.

Actions:

for community organizers,
Establish localized community defense organizations with a focus on community development (implied)
Ensure proper messaging to win the PR battle and prevent negative portrayal (implied)
Seek support from organizers within the community for setting up these organizations (exemplified)
</details>
<details>
<summary>
2019-10-16: Let's talk about fallout from the President's decision.... (<a href="https://youtube.com/watch?v=dDRSXmd9LAQ">watch</a> || <a href="/videos/2019/10/16/Lets_talk_about_fallout_from_the_President_s_decision">transcript &amp; editable summary</a>)

Eight days after the president's decision led to chaos in Syria, US forces moved for Turkey to invade, potentially resulting in catastrophic consequences if not contained immediately.

</summary>

"This will go down as the worst foreign policy decision in American history."
"It is spitting on the grave of every U.S. soldier who died in the last 15 years over there."
"All because the president would not listen to people that knew more than him."

### AI summary (High error rate! Edit errors on video page)

Eight days since the president's decision, fallout is evident.
US forces moved for Turkey to invade Syria, leading to atrocities by Turkish-backed militants.
Kurds turned to Russia for support, with Russian and Syrian troops filling the power vacuum.
ISIS staged jailbreaks, potentially growing stronger.
US forces nearly hit by artillery from a NATO ally.
Air Force scrambling to move 50 nuclear weapons from Incirlik Air Force Base.
White House discussing nuclear weapons before removal is alarming.
Turkish perspective sees potential superpower status by overtaking US base.
B61 nuclear weapon can yield up to 340 kilotons of power.
Beau warns of catastrophic impact if such weapons are used, referencing Hiroshima.

Actions:

for policy advocates, global citizens,
Contact policymakers to urge immediate containment of the situation (suggested)
Support organizations aiding those affected by the fallout in Syria (exemplified)
</details>
<details>
<summary>
2019-10-15: Let's talk about growing and changing as a person.... (<a href="https://youtube.com/watch?v=nrzNTJaq1rw">watch</a> || <a href="/videos/2019/10/15/Lets_talk_about_growing_and_changing_as_a_person">transcript &amp; editable summary</a>)

Beau advises on moving forward without explaining past beliefs, embracing growth, and not letting the past define you.

</summary>

"You cannot allow your past to define you."
"People change. If you don't look back on the person you were [...] and just cringe at how stupid you were, you're not growing as a person."
"Everybody believed something problematic at one point in time. And everybody still does."
"Growing as a person is a lot like those PT requirements."
"Just continue growing and showing that through actions and people will understand."

### AI summary (High error rate! Edit errors on video page)

Received a message about past racist beliefs and desire to make a positive impact.
Encourages moving forward without explaining past beliefs to unchanged friends.
Acknowledges past problematic beliefs and ongoing growth for everyone.
Emphasizes exposure to new perspectives as catalyst for change.
Shares personal experience of evolving views on accessibility.
Urges continuous learning and evolving perspectives as a natural process.
Compares personal growth to physical training - setting new goals and getting stronger mentally.
Assures that growth is ongoing and that past mistakes do not define a person.
Mentions example of ex-Nazi YouTuber who changed for the better.
Stresses the importance of reflecting on past beliefs and embracing personal growth.

Actions:

for individuals seeking guidance on personal growth and overcoming past beliefs.,
Continuously expose yourself to new perspectives and information (implied).
Embrace personal growth by setting new goals and evolving perspectives (implied).
Show through actions that you have evolved from past beliefs (implied).
</details>
<details>
<summary>
2019-10-15: Let's talk about John Bolton's closet.... (<a href="https://youtube.com/watch?v=FHy9JYILFvg">watch</a> || <a href="/videos/2019/10/15/Lets_talk_about_John_Bolton_s_closet">transcript &amp; editable summary</a>)

Beau reveals John Bolton's shady past and questions his sudden ethical objection to Trump administration's actions, cautioning against turning him into a hero.

</summary>

"They're true believers. Bolton is a true believer."
"True believers are the most dangerous people on the planet."
"Even in his most noble act, deceit was at its core."

### AI summary (High error rate! Edit errors on video page)

Surprised by John Bolton's ethical objection to the Trump administration's actions.
Bolton referred to Trump administration's actions as a "drug deal."
Bolton previously covered up the Iran Contra affair as assistant attorney general in the 80s.
Claimed happiest moment was getting the U.S. out of the International Criminal Court to protect U.S. war criminals.
Accused of derailing a conference on stopping weapons of mass destruction and later using that as a pretext for invasion.
Allegations of blackmailing the chief of the Organization for the Prohibition of Chemical Weapons.
Describes Bolton as a true believer who rationalizes his actions to protect American interests.
Three options presented: Bolton grew a conscience, Trump's actions were too shady even for Bolton, or the actions truly jeopardized national security.
Beau criticizes Bolton's role in American foreign policy and warns against turning him into a hero even if he brings down Trump.
Concludes by pointing out Bolton's deceit in not coming forward about the transgressions he knew of.

Actions:

for viewers, activists, critics,
Hold officials accountable for their actions (implied)
</details>
<details>
<summary>
2019-10-14: Let's talk about Trump's use of the military.... (<a href="https://youtube.com/watch?v=whTgLSevdc8">watch</a> || <a href="/videos/2019/10/14/Lets_talk_about_Trump_s_use_of_the_military">transcript &amp; editable summary</a>)

Beau criticizes the commodification of US military forces, exposing how they are being viewed as mercenaries for hire rather than defenders of national security.

</summary>

"Deploying US military forces overseas should not be elective. It should be the only alternative."
"The might of the U.S. military, well, it's on the market. It can be bought."
"War as a racket always has been. It is possibly the oldest, easily the most profitable, surely the most vicious."
"US servicemen have been bought and sold for a very, very long time. It's just normally not done this cheaply."
"The Department defense has become the world's largest private military contractor."

### AI summary (High error rate! Edit errors on video page)

Sitting with a recently separated veteran who was upset after watching the President of the United States brag about accepting payment to deploy US military forces overseas.
Criticizing the notion that recent actions were about bringing troops home, pointing out that it was more about anti-imperialism and enabling other countries' ambitions.
Expressing concern that the US military is now seen as a mercenary force for Saudi Arabia due to financial agreements.
Questioning the morality of deploying US military forces overseas as an elective option that can be bought, rather than a duty required for national security and freedom.
Mentioning the uncertainty surrounding Trump's statements and the potential impact on how the US military is perceived in the Middle East.
Indicating that US soldiers are now viewed as mercenaries being bought and sold, serving as bait for conflicts in the Middle East.
Noting the historical perspective that war has always been intertwined with money and power, with soldiers essentially being mercenaries.
Quoting General Butler, a highly decorated Marine, who referred to himself as a "gangster for capitalism" and acknowledged serving as muscle for big business interests.
Describing how the Department of Defense has effectively become a private military contractor, sending troops overseas based on financial incentives rather than national interests.
Suggesting that individuals considering joining the military should be aware that private firms may offer better pay and more competent leadership.

Actions:

for veterans and anti-war advocates.,
Contact organizations supporting veterans' rights and well-being (implied).
Educate yourself on the impacts of militarization in foreign policy (generated).
Advocate for policies that prioritize national security over financial incentives (implied).
</details>
<details>
<summary>
2019-10-14: Let's talk about Kennedy's unrealized legacy and dreams.... (<a href="https://youtube.com/watch?v=A1qOu5rEIeQ">watch</a> || <a href="/videos/2019/10/14/Lets_talk_about_Kennedy_s_unrealized_legacy_and_dreams">transcript &amp; editable summary</a>)

President Kennedy's legacy, from the Peace Corps to community networks, inspires a vision of people helping people beyond borders and government involvement.

</summary>

"Ask not what society can do for you, but ask what you can do for society."
"Just people helping people."
"Choose to do it. Not because it's easy, but because it's hard."

### AI summary (High error rate! Edit errors on video page)

President Kennedy is often mythologized as the most significant president in American history, associated with Camelot and the Golden Age.
Kennedy's famous quote "Ask not what your country can do for you, ask what you can do for your country" has a great impact.
Kennedy's vision aimed at using education programs to empower people to stand on their own.
The Green Berets, an organization that flourished under Kennedy, could have been utilized differently according to his original vision.
In 1960, Kennedy introduced the Peace Corps, envisioning Americans traveling globally to do good.
The Peace Corps, originally intended for humanitarian purposes, eventually became a tool for US foreign policy.
Beau advocates for a shift towards asking what you can do for society rather than focusing solely on what society can do for you.
He stresses the importance of community networks at the local level to build communities without government intervention.
Beau dreams of individuals helping each other across borders and without government interference, envisioning a world where people help people.
He encourages action towards building community networks and helping others without borders or government involvement.

Actions:

for global citizens,
Build community networks at the local level to empower communities (suggested)
Help others without borders or government intervention (implied)
</details>
<details>
<summary>
2019-10-13: Let's talk about the Ben Shapiro vs Beto feud.... (<a href="https://youtube.com/watch?v=H9h6uwxvuR4">watch</a> || <a href="/videos/2019/10/13/Lets_talk_about_the_Ben_Shapiro_vs_Beto_feud">transcript &amp; editable summary</a>)

Responding to sensationalist fear-mongering, Beau disapproves of violence as a solution, criticizing irresponsible rhetoric and advocating for responsible action.

</summary>

"Schools are the problem, pick up a gun."
"You should be embarrassed."
"It's not the solution. It's not even a problem."
"That violence is always the answer. It's not."
"If you keep trying to use that one tool, you're going to destroy your home."

### AI summary (High error rate! Edit errors on video page)

Responding to a trending campaign promise, Beau disapproves of the sensationalist rhetoric used by Ben Shapiro to incite fear.
Ben Shapiro creates a false dilemma by suggesting that exposing children to certain academic content will make them emulate it.
The slippery slope argument leads to an extreme scenario where armed men are at the door to take Shapiro's children, a situation Beau finds absurd.
Beau criticizes Shapiro's lack of responsible coverage and suggests using constitutional mechanisms to challenge laws instead of inciting fear.
Beau questions the masculinity-driven tough guy rhetoric that Shapiro employs, pointing out the real issue with using violence as a solution.
Beau admonishes Shapiro for irresponsibly advocating for picking up a gun as a solution to perceived problems with schools, calling it embarrassing and dangerous.
Violence is not always the answer according to Beau, who likens it to a tool that must be used responsibly to avoid destructive consequences.

Actions:

for internet users, viewers,
Challenge unconstitutional laws using constitutional mechanisms (implied)
</details>
<details>
<summary>
2019-10-12: Let's talk about President Trump, truth, and fiction.... (<a href="https://youtube.com/watch?v=Ow1fNC-oSMI">watch</a> || <a href="/videos/2019/10/12/Lets_talk_about_President_Trump_truth_and_fiction">transcript &amp; editable summary</a>)

Beau outlines the President's actions, suggesting they benefit Russia more than the United States, prompting consideration and investigation by journalists.

</summary>

"Every one of these moves benefits Russia."
"Maybe something to investigate."
"Y'all are the journalists."

### AI summary (High error rate! Edit errors on video page)

Beau expresses concern about being followed and introduces himself as Deep Goat.
He outlines the President's actions since taking office, focusing on various decisions and their impact.
The President withdrew from international treaties, diverted military resources to monitor migrants, and undermined different communities.
Beau mentions how the President's actions drove away valuable allies like the Kurds and disrupted international relationships.
The President's decisions include pulling out of the Iran deal, withholding security funds from Ukraine, and engaging in a trade war with China.
Beau implies that these actions have ultimately benefited Russia rather than the United States.
He does not explicitly label the President as a Russian asset but suggests that it's something worth investigating.

Actions:

for journalists,
Investigate the President's actions and their potential impacts on national interests (suggested)
</details>
<details>
<summary>
2019-10-11: Let's talk about China, parallels, technology, and traps.... (<a href="https://youtube.com/watch?v=04Edv148XiE">watch</a> || <a href="/videos/2019/10/11/Lets_talk_about_China_parallels_technology_and_traps">transcript &amp; editable summary</a>)

Beau introduces the issue of learning about cultures only after tragedies and delves into the parallels between China and the U.S. through the lens of the Uyghurs' plight, illustrating rapid cultural erasure through mass surveillance and internment camps.

</summary>

"We only learn about it when something bad happens to them."
"Beijing was worried about extremification."
"They're erasing the culture. They're erasing the ethnicity."
"Mass surveillance started. Eight years later there's camps."
"So that's a little brief primer on who they are."

### AI summary (High error rate! Edit errors on video page)

Beau introduces a comment about learning about cultures only when something bad happens to them, specifically mentioning at-risk cultures.
The parallel between China and the United States is discussed, focusing on technology becoming a trap through the example of the Uyghurs.
The Uyghurs, residing in Xinjiang, China, have a rich history and were part of the Republic of East Turkestan before becoming part of China in 1949.
The Uyghurs practice Sufism, a mystical component of Islam, which fundamentalists may not approve of due to its newer traditions and rituals.
Beijing's uncomfortable relationship with the Uyghurs intensified after some Uyghurs were found in a training camp in Afghanistan, leading to riots, bombings, and a security clampdown in the 1990s and early 2000s.
Beijing justified their actions as benevolent civilizing efforts, but they actually subjugated and relocated the Uyghurs, bringing in millions of Han Chinese for settlement.
Mass surveillance on the Uyghurs began in 2016 with technologies like smartphones, DNA scanning, facial recognition, and more, under the guise of preventing extremism.
A million adults are currently in camps termed "vocational centers" by China, facing erasure of their culture and ethnicity through forced assimilation.
Uyghur children are being taught Chinese culture instead of their own, further erasing their identity.
Beau draws parallels between the Uyghur experience and the native experience in the United States, reflecting on the rapid progression from technological advancements to mass internment camps.

Actions:

for activists, humanitarians,
Contact human rights organizations to support the Uyghur cause (suggested)
Join protests or awareness campaigns advocating for the rights of the Uyghur community (exemplified)
Support initiatives providing aid to Uyghur refugees or families affected by the crisis (implied)
</details>
<details>
<summary>
2019-10-10: Let's talk about an interview with a Special Forces officer and one word.... (<a href="https://youtube.com/watch?v=oNv0fjY4qjQ">watch</a> || <a href="/videos/2019/10/10/Lets_talk_about_an_interview_with_a_Special_Forces_officer_and_one_word">transcript &amp; editable summary</a>)

A Special Forces officer's interview sheds light on the realities and emotions behind military actions, urging the American people to confront their government's decisions and treatment of indigenous forces.

</summary>

"To free the oppressed."
"We did it because we have allowed our government to get out of control."
"They are not robots. They have emotions."
"Their actions, if they happened, are completely excusable."
"This is probably the time to address it."

### AI summary (High error rate! Edit errors on video page)

Special Forces officer's interview on Fox News raised eyebrows.
Special Forces are green berets, not Seals or rangers.
They teach, weaponize education, and train indigenous forces.
Special Forces motto: "To free the oppressed."
Their image in popular culture stems from misconceptions.
Special Forces are intelligent and not like robots.
Officer on Syrian Turkish border expressed displeasure with senior command and commander-in-chief's decision.
Officer used the word "atrocities" to describe events happening worldwide.
American people need to be ready for what may be revealed.
Officer criticized senior command and commander-in-chief's decisions in the interview.
Elite unit members may disregard orders if witnessing atrocities.
Special Forces are not robots; they have emotions and connections with local forces.
Actions of Special Forces should not be a reflection on them but on the Oval Office.
The American people need to address the use and abuse of indigenous forces.
A moment of truth is approaching for the American people to confront their actions worldwide.

Actions:

for american citizens,
Spread awareness about the true nature of Special Forces and their emotions (suggested)
Advocate for the fair treatment of indigenous forces (suggested)
Start a discourse on government accountability and responsible decision-making (suggested)
</details>
<details>
<summary>
2019-10-10: Let's talk about a UBI study.... (<a href="https://youtube.com/watch?v=EH83zaWikI4">watch</a> || <a href="/videos/2019/10/10/Lets_talk_about_a_UBI_study">transcript &amp; editable summary</a>)

Beau talks UBI, referencing a Stockton study on $500 monthly payments to 125 individuals, hinting at the transformative potential of large-scale implementation and importance of public opinion, particularly from younger generations like Lily.

</summary>

"Being in poverty is a lack of cash, not a lack of character."
"They're the ones that are going to implement it, or not. They're the ones that will live with it."
"Revolutionary ideas are coming out; this is an old idea. But implementing it on a scale that Yang is talking about, that's a pretty big undertaking."
"People have said it before, being in poverty is a lack of cash, not a lack of character."
"Matters what Lily thinks."

### AI summary (High error rate! Edit errors on video page)

Wishes a happy birthday to a young viewer before discussing UBI and Andrew Yang.
Mentions an 18-month study in Stockton, California, where 125 people received $500 a month, a boost for Yang's $1000 plan.
Researchers from the University of Tennessee and University of Pennsylvania label the study as anecdotal, focusing more on storytelling than gathering hard data due to the limited sample size.
Study aims to measure impact on mental and physical health, with results pending release.
Breakdown of recipients: 43% working, 2% unemployed, 8% retired, 20% disabled, and 10% stay-at-home caregivers.
Spending breakdown: 40% on food, 24% at stores like Walmart, Dollar General, 11% on utilities, 9% on auto repairs and gas, and 16% on medical expenses and insurance.
Beau acknowledges the limitations of the sample size but appreciates the storytelling aspect supporting Yang's plan.
Emphasizes the importance of younger generations like Lily in implementing such ideas that could revolutionize society.
Acknowledges concerns about people's spending habits and stereotypes related to poverty.
Concludes by hinting at the transformative potential of large-scale UBI implementation and the significance of public opinion, particularly from younger individuals like Lily.

Actions:

for policy advocates, ubi supporters,
Advocate for UBI implementation in your community (implied)
Support research on UBI impacts on mental and physical health (implied)
Challenge stereotypes about poverty and spending habits (implied)
</details>
<details>
<summary>
2019-10-09: Let's talk about unmatched wisdom, slogans, and waste.... (<a href="https://youtube.com/watch?v=U3CaDZRHo4k">watch</a> || <a href="/videos/2019/10/09/Lets_talk_about_unmatched_wisdom_slogans_and_waste">transcript &amp; editable summary</a>)

Toy soldiers line up, slogans overshadow lethal consequences, waste prevails - a bad move here means mass graves there.

</summary>

"Ended US imperialism because when Turkey and the US got together and decided how best to carve up a chunk of a third country, that was anti-imperialism."
"What's a bad move over here is a mass grave over there."
"The only thing that's happening over there right now is waste."

### AI summary (High error rate! Edit errors on video page)

Toy soldiers lined up, game pieces on the board, watching the live play-out from a safe distance.
Two groups of people ready to face off, at the mercy of their superiors, playing "Masters of the Universe."
Turkish troops preparing to cross the border, Kurds needing more troops as theirs are occupied guarding suspected IS fighters.
Guards from facilities likely to join the front, increasing the risk of jailbreaks.
President's claim of defeating a side contradicted; potential for U.S. intervention.
Slogans and campaign points overshadowing the disastrous consequences of decisions.
Expansion of war and fresh combatants, not the end of imperialism.
Decision's immediate fallout overlooked, resulting in lethal consequences overseas.
Bad moves in one place lead to mass graves in another.
The reality of waste amidst slogans and talking points.

Actions:

for concerned citizens,
Contact organizations providing aid to affected regions (implied)
Support initiatives offering assistance to refugees and displaced individuals (implied)
</details>
<details>
<summary>
2019-10-09: Let's talk about the Kurds as Americans see them.... (<a href="https://youtube.com/watch?v=EhuYF5vgX68">watch</a> || <a href="/videos/2019/10/09/Lets_talk_about_the_Kurds_as_Americans_see_them">transcript &amp; editable summary</a>)

Beau provides historical context on the Kurds, criticizes shallow discourse, and calls for meaningful support beyond memes.

</summary>

"I think we can muster a little more support than just sharing a meme on Facebook."
"There's 30 million people out there without a country."
"We'd rather string them along in hopes of it, rather than actually do something that could promote peace."
"I think this time we think beyond memes and realize there's 30 million people out there without a country."
"I don't think that arguing with other commentators does a lot of good."

### AI summary (High error rate! Edit errors on video page)

Received a screenshot of a tweet referencing his bias in favor of the Kurds due to wearing a Kurdish scarf in a video about them.
Explains that he tries to remain objective but acknowledges his bias.
Disagrees with starting debates with other commentators, believing they reinforce existing ideas rather than lead to productive discourse.
Expresses understanding and frustration over a tweet questioning the sudden interest in the Kurds by those who previously showed no concern for them.
Provides historical context on the Kurds, dating back to the 7th century, their struggles with colonial powers, and the lack of recognition of their independence.
Points out the unique aspect of Kurdish identity, where nationality comes before religion, unlike in many other Middle Eastern countries.
Addresses common questions about the Kurds, including their religious diversity and presence across multiple countries like Syria, Turkey, Iraq, Iran, and Armenia.
Talks about the geopolitical significance of the Kurds, their desire for independence, and how they are viewed by Western powers.
Criticizes the notion that Turkey is strategically vital to the US, questioning the importance of air bases and troop numbers in NATO.
Emphasizes the true strategic value lies in assets on the ground, like human intelligence and local support, which the Kurds provide.
Expresses concern over the US's treatment of the Kurds and how it may push them towards other superpowers like Russia.
Condemns the lack of action in granting the Kurds a homeland and promoting peace in the region.
Advocates for recognizing the agency of Kurdish people in determining their future rather than oppressing them through other nations.
Criticizes the American public's shallow knowledge of the Kurds, reduced to memes about their governance and attractive women warriors.
Calls for more substantial support for the Kurds beyond social media sharing, considering the significant population without a homeland.

Actions:

for advocates and activists,
Educate yourself on the history and struggles of the Kurdish people (suggested).
Support organizations advocating for Kurdish rights and independence (implied).
Raise awareness about the plight of the Kurds in your community (implied).
</details>
<details>
<summary>
2019-10-08: Let's talk about a lockdown with no locks.... (<a href="https://youtube.com/watch?v=Acxto-W5of4">watch</a> || <a href="/videos/2019/10/08/Lets_talk_about_a_lockdown_with_no_locks">transcript &amp; editable summary</a>)

Beau explains psychological effects, praises proactive students during a lockdown without locks, and challenges misconceptions about safety protocols.

</summary>

"The purpose of a lockdown is to get as many people as possible somewhere secure to maintain their safety."
"You made the right decision with the information you had at hand."
"A lockdown is a guideline. It's there to keep you safe, okay?"
"You get somewhere safe."
"It's not there to order you to sit in a room that's unlocked."

### AI summary (High error rate! Edit errors on video page)

Introduces psychological effects: Milgram experiments and bystander effect.
Milgram experiments show people tend to follow authority figures.
Bystander effect means people are less likely to act in a group.
California incident at Cal State, Long Beach involved a lockdown without locks.
Some students barricaded doors during the lockdown.
Others did nothing and waited in unlocked rooms.
Beau praises proactive students who took action.
He criticizes those who did nothing during the lockdown.
Beau challenges law enforcement's approach to lockdowns.
Urges schools to install locks immediately for safety.
Warns that the school without locks is now a target.
Emphasizes that lockdowns are guidelines for safety, not suicide pacts.

Actions:

for students, school staff, community members,
Install locks immediately in facilities without adequate security (implied)
Take proactive measures during emergencies (implied)
</details>
<details>
<summary>
2019-10-08: Let's talk about Trump's next move if he's smart.... (<a href="https://youtube.com/watch?v=M4KoqH9-1QY">watch</a> || <a href="/videos/2019/10/08/Lets_talk_about_Trump_s_next_move_if_he_s_smart">transcript &amp; editable summary</a>)

Beau criticizes Trump's handling of diplomatic situations, suggests releasing phone call audio, and warns of a negative legacy if action isn't taken.

</summary>

"Release the audio of those phone calls."
"He's scared of everything."
"You didn't vote for the guy who's going to hide in the White House."
"He made the U.S. military look the way it does now."
"The base is going to want to see him out there taking these hoaxsters and scamsters to task."

### AI summary (High error rate! Edit errors on video page)

Criticizes Trump's administration and advisors for giving bad advice.
Talks about the chain of events starting in Turkey and Syria, where Trump ordered troops out of the way, leading to a betrayal of allies who fought alongside American soldiers.
Points out that Trump's base defended him, but it made Trump look worse than Obama in handling diplomatic situations.
Mentions the obstruction of a State Department employee from testifying in the impeachment inquiry.
Suggests that Trump release the audio of phone calls and testify before the House to set the record straight.
Warns that Trump's legacy will be worse than Obama's if he doesn't take action.

Actions:

for political activists,
Release the audio of phone calls and testify before the House (suggested)
Hold hoaxsters and scamsters accountable (implied)
</details>
<details>
<summary>
2019-10-07: Let's talk about the UK, apologies, the Kurds, and friends.... (<a href="https://youtube.com/watch?v=qyG910MsU4s">watch</a> || <a href="/videos/2019/10/07/Lets_talk_about_the_UK_apologies_the_Kurds_and_friends">transcript &amp; editable summary</a>)

An American diplomat's wife avoids accountability using diplomatic immunity while the US betrays its Kurdish allies in Syria, revealing a deeper theme of establishment versus the people.

</summary>

"It's always portrayed as left versus right, as your demographic versus another equal or lesser maybe just above demographic. At the end of the day, it's not what it is."
"It's establishment versus you. They will always write you off to protect themselves. It's always what happens."
"George Carlin said it best. It's a big club and you ain't in it."

### AI summary (High error rate! Edit errors on video page)

An American diplomat's wife, protected by diplomatic immunity, caused the death of a 19-year-old in the UK by driving the wrong way.
The family of the victim is left with no closure as she used immunity to return to the US without facing consequences.
The Kurds, a vital ally of the US in the Middle East, are being betrayed as the US greenlights Turkey's incursion into Syria.
Despite the Kurds' loyalty and support to the US, they are at risk during this invasion with no country of their own.
The US is actively supporting the invasion, unlike past betrayals that were more discreet.
Beau connects these seemingly unrelated events, revealing a deeper theme of establishment versus the people.
He criticizes the establishment for always protecting themselves at the expense of the people.

Actions:

for global citizens,
Contact local representatives to advocate for justice for victims of diplomatic immunity (implied).
Support organizations aiding Kurds and raise awareness about their plight (implied).
</details>
<details>
<summary>
2019-10-06: Let's talk about G.I. Joe, an old quote, and Trump.... (<a href="https://youtube.com/watch?v=DdkoyU1Zdsg">watch</a> || <a href="/videos/2019/10/06/Lets_talk_about_G_I_Joe_an_old_quote_and_Trump">transcript &amp; editable summary</a>)

Beau points out dangerous parallels between historical power dynamics and current events, warning against the implications of targeting individuals before crimes exist.

</summary>

"Show me the person and I'll show you the crime."
"Doing it any other way is extremely dangerous."
"Find a person that we want to hang, and then we go find the crime to hang on them."
"Show me the man, I'll show you the crime."
"He is accused of doing some pretty horrible things using that thought process."

### AI summary (High error rate! Edit errors on video page)

Takes a day off to play with kids, recalls G.I. Joe show from the 80s and its plot dynamics.
Draws parallels between G.I. Joe's preemptive actions against Cobra and real-life situations.
Mentions Lavrenty Beria, a powerful figure in the Soviet Union, known for his infamous quote.
Criticizes President involving himself in corruption cases without identifiable crimes.
Questions the process of finding a person to hang and then looking for a crime to pin on them.
Talks about the dangerous thought process of "Show me the man, I'll show you the crime."
References Beria's role as chief of the NKVD secret police and his history of heinous actions.

Actions:

for those concerned about justice.,
Investigate and stay informed about cases where individuals are targeted without identifiable crimes (implied).
Advocate for due process and fair investigations in political matters (implied).
</details>
<details>
<summary>
2019-10-04: Let's talk about the Biden-Ukraine timeline and journalism 101.... (<a href="https://youtube.com/watch?v=Q4XvIM046po">watch</a> || <a href="/videos/2019/10/04/Lets_talk_about_the_Biden-Ukraine_timeline_and_journalism_101">transcript &amp; editable summary</a>)

Beau breaks down the Biden-Ukraine timeline, debunks misconceptions, and stresses the need for concrete evidence in journalism.

</summary>

"If you are attempting to pass something off as objective and you have a bias, you should disclose it to your readers or your viewers, that is Journalism 101."
"You need evidence, physical evidence, call logs, emails, something."
"Unless there's physical evidence that you can present, there's no story here."
"You need evidence to change this. And it might be out there."
"But you need evidence, not any window."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic of the Biden-Ukraine event and the importance of journalism in the narrative.
Talks about disclosure in journalism and the basic principles of Journalism 101.
Explains the relevance of laying out a timeline in investigative journalism.
Provides a detailed timeline of events related to the Biden-Ukraine issue, starting from 2012.
Addresses the misconception surrounding the investigation into Biden's son and the energy company.
Mentions the concerns about corruption in Ukraine and the role of various Western powers in addressing it.
Describes Vice President Biden's involvement in dealing with corruption issues in Ukraine.
Points out the bipartisan effort within the United States to address corruption in Ukraine.
Talks about the change in prosecutors in Ukraine in 2016 and the reopening of the investigation.
Emphasizes the importance of evidence in journalism and refutes claims without substantial proof.
Concludes by stressing the need for physical evidence to support any narrative changes.

Actions:

for journalism students, truth-seekers,
Fact-check information shared on social media platforms (implied)
Support investigative journalism by subscribing to reputable sources (implied)
</details>
<details>
<summary>
2019-10-04: Let's talk about climate change cuisine.... (<a href="https://youtube.com/watch?v=1QkrOiTWpeY">watch</a> || <a href="/videos/2019/10/04/Lets_talk_about_climate_change_cuisine">transcript &amp; editable summary</a>)

AOC faces absurd suggestion at town hall, revealing Republican reactions, fake news, and deeper racial implications of climate change fears, while Beau advocates for hope and rational solutions.

</summary>

"There's always hope."
"Their solution is eat the babies."
"If you look at the climate issue and you think the solution is population and not consumption? Well, one, you're evil. Two, you're racist."
"Fake news has hit the president yet again."
"y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

AOC held a town hall where unfiltered questions were asked, a departure from the norm of politicians listening only to those who pay them.
During the town hall, a woman suggested eating babies to save the climate, sparking wild reactions from Republicans.
The woman was later revealed to be a La Roche PAC operative, not a genuine proposal.
Republicans caring about human life after birth due to this incident was noted.
President Trump joined in calling AOC names and spreading fake news, without verifying facts.
AOC responded with composure and hope, focusing on the goal of reaching net zero emissions.
Beau criticizes those who panic about climate change to the point of advocating depopulation as a solution.
He points out the flawed logic of prioritizing population control over consumption reduction, calling it evil and racist.
Beau recommends a video by YouTuber Mexi for further insight into the racial undertones of this issue.
Despite the prevalence of fake news, Beau ends on a note of hope and encourages further reflection on the topic.

Actions:

for climate change advocates,
Watch the video by YouTuber Mexi to gain further insights into racial undertones related to climate change (suggested).
</details>
<details>
<summary>
2019-10-03: Let's talk about recessions and headlines.... (<a href="https://youtube.com/watch?v=vimH_tZWCHg">watch</a> || <a href="/videos/2019/10/03/Lets_talk_about_recessions_and_headlines">transcript &amp; editable summary</a>)

Beau contradicts claims of no recession, pointing to historical indicators and political motivations to delay economic downturn for electoral reasons, implying Trump's policies hastened the impending recession.

</summary>

"They don't want you to think about how they might have tanked the economy."
"A recession is natural in some ways, a lot of times."
"It's really that simple."
"I am not a trained economist."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Participated in a telethon for the Trevor Project.
Addressed questions about an upcoming recession.
Contradicts headlines claiming no recession is imminent.
Cites headlines from 2007-2008 before the last recession.
Manufacturing report indicates lowest levels since the previous recession.
Suggests the current administration aims to delay recession until after the election.
Points out the natural occurrence of recessions after periods of growth.
Expresses belief that Trump's economic policies contributed to the impending recession.
Speculates that the government aims to maintain consumer confidence to delay recession.
Acknowledges not being a trained economist and presents his views as thoughts.

Actions:

for voters, concerned citizens.,
Stay informed about economic indicators and historical patterns (implied).
Support policies that prioritize long-term economic stability over short-term gains (implied).
</details>
<details>
<summary>
2019-10-03: Let's talk about Trump on the South Lawn.... (<a href="https://youtube.com/watch?v=LZ0YVdiV_Ks">watch</a> || <a href="/videos/2019/10/03/Lets_talk_about_Trump_on_the_South_Lawn">transcript &amp; editable summary</a>)

Beau breaks down Trump's impeachment support, his response, and the irony of fake news in politics.

</summary>

"45%, those are rookie numbers, we need to pump those numbers up."
"I guess Trump saw that and was like, you know what, 45%, those are rookie numbers, we need to pump those numbers up."
"The man that told actual journalists that they were fake news is going to be brought down by faking it."
"And they say poetic justice is dead. It's not. It's alive and well."
"You can't write stuff like this."

### AI summary (High error rate! Edit errors on video page)

Shares the chain of events in the morning following a poll on impeachment support.
Trump's response to the poll results by soliciting interference in the election live on TV.
Beau expresses his belief that Trump's actions are impeachable.
Mentions Trump being a victim of the fake news media.
Compares the Biden theory to a fabricated storyline about JFK's assassination involving Jackie Kennedy.
Criticizes right-wing conspiracy sites for spreading misinformation.
Talks about Giuliani showing the fabricated story to the president, who acts on it.
Points out the irony of Trump being brought down by fake news after attacking journalists for the same.
Beau humorously comments on the situation, suggesting everyone could use a good laugh.

Actions:

for political observers,
Fact-check and verify information before sharing (implied)
</details>
<details>
<summary>
2019-10-03: Let's talk about Botham and justice.... (<a href="https://youtube.com/watch?v=-N2MKhD1GVQ">watch</a> || <a href="/videos/2019/10/03/Lets_talk_about_Botham_and_justice">transcript &amp; editable summary</a>)

Beau addresses disappointment in falling short of perceived justice, societal conditioning to accept subpar outcomes, racial sentencing disparities, and the challenging forgiveness displayed by victims' families.

</summary>

"Disappointed. Disappointed."
"We're conditioned now to accept something that is less than okay, and just be like, well it's not right, but it's better than I was expecting."
"Sentencing disparity is a very, very real thing."
"I yield to them."
"They are better people than I am."

### AI summary (High error rate! Edit errors on video page)

Addressing the aftermath of a certain event and the feelings surrounding it, particularly disappointment.
Expressing disappointment at falling short of perceived real justice by a slim margin.
Noting society's tendency to accept less-than-ideal outcomes as long as they exceed expectations.
Sharing personal views on prison being rehabilitative, disapproving of mandatory minimums and non-violent crime incarcerations.
Acknowledging a leaning towards retribution over justice for violent crimes, especially of a certain magnitude.
Acknowledging racial sentencing disparities and the role of income inequality in access to quality legal representation.
Speculating that being a police officer may have influenced the leniency of the sentence.
Mentioning potential challenges for the convicted individual in a prison environment not welcoming to law enforcement.
Contemplating whether the outcome constitutes justice and deferring to the victim's family for that judgment.
Acknowledging the family's disappointment and anger while also appreciating their forgiveness and ability to embody compassion.
Expressing difficulty in comprehending the level of forgiveness displayed by Botham's brother towards the convicted individual.

Actions:

for justice seekers,
Stand against sentencing disparities by supporting legal aid organizations that work towards fair representation for all (implied).
Advocate for prison reform that focuses on rehabilitation rather than punishment (implied).
Show empathy and support for victims' families in their pursuit of justice and healing (implied).
</details>
<details>
<summary>
2019-10-02: Let's talk about reactions to the Guyger verdict.... (<a href="https://youtube.com/watch?v=PicasjT5mSE">watch</a> || <a href="/videos/2019/10/02/Lets_talk_about_reactions_to_the_Guyger_verdict">transcript &amp; editable summary</a>)

Beau reacts to the Dallas verdict, criticizing leniency advocates in a case where a woman fatally shot an unarmed person in his own home, stressing the responsibility that comes with owning firearms.

</summary>

"A lack of situational awareness does not provide grounds for lethal force."
"If she was confused and that disoriented, she shouldn't have been armed."
"What she says doesn't matter."
"Rights come with responsibilities."
"Y'all have me second-guessing my stance on gun control."

### AI summary (High error rate! Edit errors on video page)

Reacting to the verdict of a trial in Dallas where a woman entered the wrong apartment and fatally shot an unarmed person.
Expresses surprise at the satisfactory verdict regarding the incident.
Observes a divide among gun owners in their reactions to the case, particularly regarding gun control.
Criticizes those who advocate for leniency in this case, stating they are not responsible enough to own firearms.
Emphasizes the indisputable facts of the case: the woman entered someone else's home and killed an unarmed person.
Argues that confusion or lack of situational awareness does not justify the use of lethal force.
Points out the disregard for safety protocols and responsibilities in handling a firearm.
Challenges justifications for leniency based on the perpetrator's background.
Questions what the reaction might have been if the demographics were reversed in the case.
Concludes that rights, such as owning a weapon, come with responsibilities.

Actions:

for gun owners,
Reassess your stance on gun control and gun ownership (exemplified).
Challenge biases and prejudices, especially in cases of racial implications (exemplified).
</details>
<details>
<summary>
2019-10-01: Let's talk about engaging the youth and survival.... (<a href="https://youtube.com/watch?v=5pBXMov6lNU">watch</a> || <a href="/videos/2019/10/01/Lets_talk_about_engaging_the_youth_and_survival">transcript &amp; editable summary</a>)

Teach survival skills to youth by making it engaging, fun, and tailored to their interests, promoting critical thinking and problem-solving while spending quality time together as a family.

</summary>

"Teach them the skills. Make it fun."
"If you can survive a zombie apocalypse, you can certainly survive a hurricane."
"Just make it fun and for God's sakes do not try to toughen up your kid."

### AI summary (High error rate! Edit errors on video page)

Techniques to teach youth survival skills can be applied to any topic, not just survival.
Outward Bound is a good option for training if you have the budget.
Instructors who teach survival skills well go beyond practical skills to include critical thinking and problem-solving.
Many instructors make a lot of money by training corporate executives.
Beau encourages individuals to take on the role of the instructor themselves.
Learning survival skills teaches critical thinking, problem-solving, improvisation, and self-reliance.
Beau advises against trying to toughen up kids physically; mental toughness develops naturally.
He recommends reading the U.S. Army Survival Manual as a starting point.
Beau suggests making learning fun by not fighting popular culture and incorporating the youth's interests.
Teaching survival skills through activities like escape rooms, chemistry sets, fishing, and camping can be engaging.
Spending quality time with family while teaching these skills is a valuable aspect of the process.

Actions:

for parents, educators, mentors,
Read and familiarize yourself with the U.S. Army Survival Manual (suggested).
Incorporate survival skill activities into family time like camping, fishing, or learning chemistry sets (exemplified).
Organize hip-pocket classes and activities for hands-on learning (exemplified).
Use escape rooms or paper and pencil puzzles to develop problem-solving skills (exemplified).
</details>
