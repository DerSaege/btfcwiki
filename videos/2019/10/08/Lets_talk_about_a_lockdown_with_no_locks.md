---
title: Let's talk about a lockdown with no locks....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Acxto-W5of4) |
| Published | 2019/10/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces psychological effects: Milgram experiments and bystander effect.
- Milgram experiments show people tend to follow authority figures.
- Bystander effect means people are less likely to act in a group.
- California incident at Cal State, Long Beach involved a lockdown without locks.
- Some students barricaded doors during the lockdown.
- Others did nothing and waited in unlocked rooms.
- Beau praises proactive students who took action.
- He criticizes those who did nothing during the lockdown.
- Beau challenges law enforcement's approach to lockdowns.
- Urges schools to install locks immediately for safety.
- Warns that the school without locks is now a target.
- Emphasizes that lockdowns are guidelines for safety, not suicide pacts.

### Quotes

- "The purpose of a lockdown is to get as many people as possible somewhere secure to maintain their safety."
- "You made the right decision with the information you had at hand."
- "A lockdown is a guideline. It's there to keep you safe, okay?"
- "You get somewhere safe."
- "It's not there to order you to sit in a room that's unlocked."

### Oneliner

Beau explains psychological effects, praises proactive students during a lockdown without locks, and challenges misconceptions about safety protocols.

### Audience

Students, school staff, community members

### On-the-ground actions from transcript

- Install locks immediately in facilities without adequate security (implied)
- Take proactive measures during emergencies (implied)

### Whats missing in summary

Importance of taking proactive safety measures and being prepared for emergencies.

### Tags

#Lockdown #SchoolSafety #EmergencyPreparedness #CommunityPolicing


## Transcript
Well, howdy there, internet people, it's Beau again.
So tonight, we've got a teachable moment.
We're gonna start off talking about
a couple of psychological effects,
and then we're gonna talk about
how they came into play today out in California.
Um, okay, the first of this is the Milgram experiments.
If you're not familiar with them, definitely read about them.
very interesting to say the least. The summary is that if a dude in a lab coat
holding a clipboard tells you to do something, you're probably going to do it.
And this can be expanded to suggest that if someone who's perceived to be in a
position of authority tells you to do something, you're probably going to do it.
That can be dangerous. The other is the bystander effect, the diffusion of
responsibility. And the way that works is the more people that are around, the
less likely you are to believe that something is your personal responsibility.
That you personally have to intervene in something, that you have to act because
certainly somebody else will do it. The cool thing about that one is just
hearing that and understanding that it is in effect kind of inoculates you
against it. It makes it makes you less likely to succumb to it. Over the years
as that study has been repeated, the percentage of people that don't act
drops because it's become kind of a well-known fact. It's becoming more and
more well-known. So people are less likely to just stand there and wait. Okay
So what happened out in California?
Cal State, Long Beach, had a lockdown.
Problem is there were no locks.
A lot of the rooms with students in them had no locks.
Now some of the students in some of the classrooms,
they barricaded the doors.
They used a computer cord to tie off the doors.
They acted.
They did something.
They were proactive.
Good for you guys.
You might feel silly now because nothing happened.
But you didn't know that then.
You made the right decision with the information
you had at hand.
In other rooms, they did nothing.
They sat in an unlocked room and waited.
That's pretty terrifying.
Don't do that.
The purpose of a lockdown is to get as many people as possible somewhere secure to maintain
their safety.
If a lockdown was ordered and you were in the hallway, would you stay in the hallway?
Of course not.
You would get somewhere safe.
Same principle needs to apply.
Now I know there will be objections from a very, very small minority of law enforcement
is just used to saying something and they're gonna say you know that makes
our job harder you just need to comply
no you don't get to tell people to sit in a room and wait to die because it
makes your job harder
it's not how that works if you truly believe that you need to quit you need to
change professions
Now, the other side to this, and this is really important, this was covered nationwide, coast
to coast.
If that school is not installing locks right now, and I mean right now in the middle of
the night, anything that happens at that school should be the personal liability of those
running it.
It should be very apparent now, you're the number one target, you were the number one
target because of the way this was covered.
The people that do these sorts of things, they study others beforehand.
They're now going to be looking for this.
So if you run another facility and you have rooms that don't have locks on it, that needs
to be solved immediately, immediately.
And to students, please, please understand, a lockdown is a guideline.
It is a guideline.
It's there to keep you safe, okay?
It's not there to order you to sit in a room that's unlocked, a person in position of authority
coming over a loudspeaker and telling you the schools on lockdown is not a suicide pact.
You get somewhere safe.
Anyway, it's just a thought.
y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}