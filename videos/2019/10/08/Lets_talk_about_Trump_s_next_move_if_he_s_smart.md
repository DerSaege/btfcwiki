---
title: Let's talk about Trump's next move if he's smart....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=M4KoqH9-1QY) |
| Published | 2019/10/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Criticizes Trump's administration and advisors for giving bad advice.
- Talks about the chain of events starting in Turkey and Syria, where Trump ordered troops out of the way, leading to a betrayal of allies who fought alongside American soldiers.
- Points out that Trump's base defended him, but it made Trump look worse than Obama in handling diplomatic situations.
- Mentions the obstruction of a State Department employee from testifying in the impeachment inquiry.
- Suggests that Trump release the audio of phone calls and testify before the House to set the record straight.
- Warns that Trump's legacy will be worse than Obama's if he doesn't take action.

### Quotes

- "Release the audio of those phone calls."
- "He's scared of everything."
- "You didn't vote for the guy who's going to hide in the White House."
- "He made the U.S. military look the way it does now."
- "The base is going to want to see him out there taking these hoaxsters and scamsters to task."

### Oneliner

Beau criticizes Trump's handling of diplomatic situations, suggests releasing phone call audio, and warns of a negative legacy if action isn't taken.

### Audience

Political activists

### On-the-ground actions from transcript

- Release the audio of phone calls and testify before the House (suggested)
- Hold hoaxsters and scamsters accountable (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of Trump's actions and their implications, urging for accountability and transparency.

### Tags

#Trump #Diplomacy #Impeachment #Legacy #Accountability


## Transcript
Well howdy there Internet people, it's Bo again.
So we gotta talk about what's going on in the White House.
Because man, he's getting some bad advice.
It is casting him in a light that I don't think
he can see up there.
I don't think he can see it from the White House.
I don't think his advisors can see it.
It's a chain of events.
And at all points, one direction.
I'm not a fan of the Trump administration.
I don't think anybody's gonna be surprised to hear that.
But I don't think the light he's being cast in is fair."
So it was a chain of events.
It started in Turkey, Syria, that whole fiasco,
where he ordered our troops out of the way
and grain-lit an attack on allies who have fought
and died beside American soldiers for 15 years.
He sold them out.
He betrayed them openly and publicly.
That's bad.
But when his base dutifully defended him, he had to do it because Turkey was going to
attack.
Yeah, I mean it sounds good, but the reality is when they say that, they know.
Turkey made the US military move?
Come on, it's the U.S. military.
They don't have to move for anybody, certainly not Turkey.
They were ordered to move because the president couldn't handle it.
They were ordered out of the way, and that casts him in a light, and we're going to get
to that light in a second, but along the way, it also forces his base to admit that Obama
was a better diplomat.
was able to keep the allies together this whole time. That whole time, he was in office,
he did it, no problems. We didn't have NATO allies attacking the Kurds. Are you kidding
me? And they know that. They're not saying it. But they know, man. And to them, even
Obama could have handled that. It's making Trump look worse than Obama. They won't say
it publicly, but they're thinking it and it's true.
And then he moved U.S. soldiers, U.S. special operations out of the way for our allies to
be attacked.
Man.
That's rough.
That is rough.
If you are an Arden Trump supporter, that is rough.
I don't know how you'd defend that.
And then because of that, his major allies, Robertson, Graham, a whole bunch people spoke
out against him.
You know, calling people traitors, that only goes so far.
You can't really get into the heart of your side of the aisle when you're doing that.
You're going to call Colin Powell a traitor?
I don't think so.
Not if you want to get reelected.
So then you've got that, and then you have what happened today.
Him ordering a State Department employee who was going to voluntarily testify before the
impeachment inquiry who said he had evidence on his device, ordering them not to show.
Yeah, it's obstruction.
It's obstruction.
Nobody's going to care about that on his side of the aisle.
They've given up on the rule a long time ago.
They don't care about that.
But they care about something else.
They care about image.
That maverick tough guy image.
all of these events. They make him look like a coward. They make him look like a coward.
He backed down from Turkey? Seriously? Sold out our allies to Turkey? Gonna let him get
slaughtered. Yeah, I'm sure that's playing really well with the SOCOM base right there.
And from there, he has to suffer these attacks and he can't say anything like he used to.
He can't attack his own base.
He can't attack these people at the heart of the movement.
And they're trashing him, rightfully so.
And now, this thing that he sat there and called a scam the whole time.
There's nothing to hide.
It was a perfect phone call.
Oh, no, no, no, don't go talk about it.
It does.
It makes him look like a carrot.
I don't think he is.
I think he's getting horrible advice from somebody.
I don't think he would do this on his own.
Up till now, he's been very politically shrewd.
This?
This is amateur hour.
So because of this, he's only got one option.
Release the audio of those phone calls.
know it exists, release the audio, those phone calls, all of them, of your
perfect phone calls, and you go testify before the House, the President of the
United States, go set the record straight.
If you don't do that, understand the President of the United States will go
down in history as the guy who was worse than Obama and scared of everything.
That's going to be his legacy, that's going to be his image.
That's what's going to get recorded, even if it doesn't play that way to his base at this exact moment.
Historically, you had all of these presidents before him. You had Bush and Obama that were able to hold the allies
together.
And then in comes Trump, and they're attacking each other. And he's moving out of the way for them to do it.
Yeah. So he's a worse diplomat than Obama.
And then he's scared of everything. That's how it plays. That's how it plays to anybody who thinks about it.
And even those on the right who are going to argue, oh no, that's not true. That's not how I feel.
Yeah, it is. Yeah, it is. I know what you'll say publicly.
But deep down, you know that this isn't the guy you voted for.
You didn't vote for the guy who's going to hide in the White House.
the White House, you want the guy who's going to go out there and call her
nervous Nancy. But he won't do it to her face just when he's safe behind Twitter.
And you're starting to see that now because it's coming out. And he made the
U.S. military look the way it does now. Like they're just willing to stand idly
by and watch those people who fought with them the day before get killed.
Because the President couldn't handle a phone call.
If he cares about his legacy, he's got to come forward.
This softball thing at the Senate with Giuliani, man, that's not going to cut it.
The base is going to want to see him.
They're going to want to see him out there taking these hoaxsters and scamsters to task.
They're going to want to see that great unmatched wisdom in action.
Not him cowering behind Twitter.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}