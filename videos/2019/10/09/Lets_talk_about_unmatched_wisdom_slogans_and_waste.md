---
title: Let's talk about unmatched wisdom, slogans, and waste....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=U3CaDZRHo4k) |
| Published | 2019/10/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Toy soldiers lined up, game pieces on the board, watching the live play-out from a safe distance.
- Two groups of people ready to face off, at the mercy of their superiors, playing "Masters of the Universe."
- Turkish troops preparing to cross the border, Kurds needing more troops as theirs are occupied guarding suspected IS fighters.
- Guards from facilities likely to join the front, increasing the risk of jailbreaks.
- President's claim of defeating a side contradicted; potential for U.S. intervention.
- Slogans and campaign points overshadowing the disastrous consequences of decisions.
- Expansion of war and fresh combatants, not the end of imperialism.
- Decision's immediate fallout overlooked, resulting in lethal consequences overseas.
- Bad moves in one place lead to mass graves in another.
- The reality of waste amidst slogans and talking points.

### Quotes

- "Ended US imperialism because when Turkey and the US got together and decided how best to carve up a chunk of a third country, that was anti-imperialism."
- "What's a bad move over here is a mass grave over there."
- "The only thing that's happening over there right now is waste."

### Oneliner

Toy soldiers line up, slogans overshadow lethal consequences, waste prevails - a bad move here means mass graves there.

### Audience

Concerned citizens

### On-the-ground actions from transcript

- Contact organizations providing aid to affected regions (implied)
- Support initiatives offering assistance to refugees and displaced individuals (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the consequences of political decisions on the ground, urging viewers to look beyond slogans and talking points to understand the real impact of actions.

### Tags

#ForeignPolicy #Imperialism #Consequences #PoliticalDecisions #Slogans


## Transcript
Well howdy there internet people, it's Beau again.
So the toy soldiers are all lined up, game pieces are on the board.
We get to watch it play out live
from over here in safety.
While half a world away
two groups of people who do not reason why
are ready to face off
at the behest of their betters.
people intent on playing Masters of the Universe,
because the offensive has started.
Right now it's air operations, ground operations are sure to follow
in short order.
And when those Turkish troops
pour across that border,
the Kurds are going to need more troops, because a lot of theirs are tied up doing
silly things right now, like guarding suspected IS fighters.
That's where they're going to get them, those guards,
because they're going to need them on the front.
They wouldn't be able to order them to stay anyway, because these facilities are pretty
close to where the fighting is going to be.
And those people at those facilities are not going to stand there and let their friends
go alone right up the road.
They're not the United States.
But when those facilities become undermanned, the likelihood of jailbreak increases exponentially.
And they'll have help from the outside because contrary to what the president has said, that
side is not defeated, they're still out there.
But hey, once they're back out, well that'll give good clear pretext for the U.S. to go
back in in force.
I guess if you're one of the guards, there is a more proactive permanent solution to
that problem, but it requires a little moral flexibility.
Because they are suspects after all.
of them will be innocent. There's about to be a whole lot of gray area over there
and something like that's pretty likely to happen. Shows exactly how horrible
this decision was in his unmatched wisdom. Couldn't see the immediate fallout.
out. But hey, we got slogans, we got campaign talking points, ending the war. We
ended the war by expanding its scope and adding a new stream of fresh combatants.
Ended US imperialism because when Turkey and the US got together and decided how
best to carve up a chunk of a third country, that was anti-imperialism, which
there was a word for that kind of action. I'm pretty sure it's imperialism. See
when all this comes into focus over here, we feel silly, maybe a little dumb, maybe
a little guilty. It was a bad move over here. Over there, it's lethal. It is lethal. What's
a bad move over here is a mass grave over there. But we got talking points, we got slogans.
ended the war. Anti-imperialism. Sounds good, but none of that's happening. The only thing
that's happening over there right now is waste. Waste. Probably going to be a lot of it. Anyway,
It's just a thaw, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}