---
title: Let's talk about the Kurds as Americans see them....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=EhuYF5vgX68) |
| Published | 2019/10/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Received a screenshot of a tweet referencing his bias in favor of the Kurds due to wearing a Kurdish scarf in a video about them.
- Explains that he tries to remain objective but acknowledges his bias.
- Disagrees with starting debates with other commentators, believing they reinforce existing ideas rather than lead to productive discourse.
- Expresses understanding and frustration over a tweet questioning the sudden interest in the Kurds by those who previously showed no concern for them.
- Provides historical context on the Kurds, dating back to the 7th century, their struggles with colonial powers, and the lack of recognition of their independence.
- Points out the unique aspect of Kurdish identity, where nationality comes before religion, unlike in many other Middle Eastern countries.
- Addresses common questions about the Kurds, including their religious diversity and presence across multiple countries like Syria, Turkey, Iraq, Iran, and Armenia.
- Talks about the geopolitical significance of the Kurds, their desire for independence, and how they are viewed by Western powers.
- Criticizes the notion that Turkey is strategically vital to the US, questioning the importance of air bases and troop numbers in NATO.
- Emphasizes the true strategic value lies in assets on the ground, like human intelligence and local support, which the Kurds provide.
- Expresses concern over the US's treatment of the Kurds and how it may push them towards other superpowers like Russia.
- Condemns the lack of action in granting the Kurds a homeland and promoting peace in the region.
- Advocates for recognizing the agency of Kurdish people in determining their future rather than oppressing them through other nations.
- Criticizes the American public's shallow knowledge of the Kurds, reduced to memes about their governance and attractive women warriors.
- Calls for more substantial support for the Kurds beyond social media sharing, considering the significant population without a homeland.

### Quotes

- "I think we can muster a little more support than just sharing a meme on Facebook."
- "There's 30 million people out there without a country."
- "We'd rather string them along in hopes of it, rather than actually do something that could promote peace."
- "I think this time we think beyond memes and realize there's 30 million people out there without a country."
- "I don't think that arguing with other commentators does a lot of good."

### Oneliner

Beau provides historical context on the Kurds, criticizes shallow discourse, and calls for meaningful support beyond memes.

### Audience

Advocates and Activists

### On-the-ground actions from transcript

- Educate yourself on the history and struggles of the Kurdish people (suggested).
- Support organizations advocating for Kurdish rights and independence (implied).
- Raise awareness about the plight of the Kurds in your community (implied).

### Whats missing in summary

The emotional depth and personal connection Beau brings to the discourse can be better understood by watching the full transcript.

### Tags

#Kurds #Geopolitics #Support #Advocacy #Community #History #Oppression #Activism


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight's going to be a little different,
because a whole bunch of people sent me
a screenshot of a tweet, and by a whole bunch, I mean like 11.
And I get why they sent it.
I get why they sent it.
In that video about the Kurds, I wore a Kurdish scarf.
That was my attempt at a visual disclosure notice.
Yeah, I'm going to try to be objective,
But I've got a massive bias in favor of the Kurds.
That was my giant red flag.
Well, red, yellow, and green.
So I understand why people sent this tweet.
I think they were trying to start a feud between David
Packman and myself.
I don't think that arguing with other commentators does a
lot of good.
I think debates just tend to reinforce the ideas of those
people watching.
They already had their minds made up and they're just looking for their side to make points.
And the other thing is, I feel this frustration, I feel this frustration in the tweet.
Okay, so the tweet is, the concerns about the Kurds are obviously warranted, but it's
still funny to hear people who have never cared about them and barely know who they
are, talk about it like it's been their issue for a decade.
As somebody who's, it has been their issue for more than a decade, I feel the frustration.
I get his starting point, I think he stopped a little too soon though, but I get his starting
point.
It is frustrating because it is annoying to only see the Kurds brought up when it is politically
advantageous for one party or the other it is especially if you're trying to
advocate for them and the American base of knowledge in general is really shallow
it's based on memes and we'll get to the memes in a little bit but I do feel the
annoyance but rather than just say I can't believe you just learned about
them tonight we're gonna talk about the Kurds who they are we're gonna use that
That is a jumping off point because it is annoying so let's fix it.
Kurds have been around since about the 7th century.
They are a very old people.
Very old people.
Over that time they've been conquered and divided up by colonial powers, then oppressed
by countries set up by colonial powers.
The countries were just formed because guys drew a line on a map with no respect to the
people who live there. That's why you see a lot of sympathy and loyalty from the Irish,
from the Basque, from other people who have had centuries long struggles to obtain independence.
Now when you talk to Americans about the Kurds, one of the first questions you get is, well
are they? It took me a long time to actually understand the question because what they're
really asking is what religion are they? And it makes sense, it's the Middle East. That's
one of the unique things about the Kurds. You know, if you ask a Saudi, you know, he's
right, right away, I'm Sunni, you know, you will get that information. You'll get the
religion more than likely before you get to nationality when you ask what are you?
With the Kurds, they're Kurds, they're Kurdish, they are Kurds before they are Shia, before
they are Sunni.
And that is unique, it doesn't happen a lot in the Middle East.
Now to answer the question, I think they're predominantly Sunni, but there are Shia, there
are Yazidi, there are Christians, there are religions that most Americans have never heard
of, because they are a very, very old people.
Now another question that pops up when you start talking about them is, well, why did
they go to Syria?
Because in the American mindset, we know about them from Iraq.
We know about them from Iraq, and that's it.
They're in Syria because they've always been in Syria, and Turkey, and Iraq, and Iran,
Armenia. They're spread out all over these countries and when the maps were
drawn they got left out in the cold. There's a lot of hot spots on that list too
isn't there? That's why they're important geopolitically. They look to the West
because they want independence and the West looks at them like a ready-made
made insurgent force. That is the geopolitical reality. In this recent
incident the objection or the justification for Trump's actions was
that you know Turkey is an important strategic partner. This isn't the Cold
war. No they're not. What? They're not. They're irrelevant. They've been
irrelevant for a very long time. Now the objections to that statement are that
well one, air bases and two, the second largest military in NATO. Okay, the air
bases aren't that important anymore. The US military conducts business and by
business I mean war through force projection. That's the ability to put
troops anywhere in the world and sustain them. And some people would say that
Incirlik, a large base there, plays a part in that. Kind of, kind of, but it doesn't
have to be in Turkey and there are actually other bases that are better.
There's nothing that goes on in Incirlik that couldn't be moved in a month. Not
really that significant. The other part of it is that as part of the way the US
military conducts business, we don't need a big air base because we're going to take
yours.
It's one of the first things that's going to happen.
The Rangers are going to show up in the middle of the night and take over the airports.
That's what they excel at.
It's actually creepy if you've ever seen a demonstration.
So that's not a real strategic benefit coming from Turkey.
The other thing, the second largest military in NATO, that's based off troop numbers.
Those aren't important anymore, really.
That's not significant.
This isn't the 1940s.
Yes, during World War II, a company of men, that was a huge obstacle.
Today, that's something for air support to take out en route to the main objective.
What matters on today's battlefield is technology, and Turkey is reliant on others for technology.
They are not that strategically important, not in that region.
Do you know what is strategically important in that region?
Assets on the ground, human intelligence, locals that will help, in short, the Kurds.
A lot of people are like, you know, Trump gave Putin Syria on his birthday.
gave him something far more valuable than that. He gave him the Kurds because the Kurds
are never going to trust the US the way they did and they're going to look to another superpower
and that superpower is probably going to be Russia. All of those hot spots and there are,
were people very loyal to the United States, and they're gone.
They're no longer going to have that level of loyalty.
That's why they don't have a homeland.
That's why they don't have a country of their own.
Because we'd rather string them along in hopes of it, rather than actually do something
that could promote peace.
let's say the Kurdish region in northern Iraq was suddenly Kurdistan, a country, you
really think the people 30 miles on the other side of the Syrian border are gonna
fight and die? No, they're gonna move. Can't have that, can't actually do anything to
promote peace I guess and I know somebody in response to that is gonna
say well we're just supposed to redraw the borders in the Middle East again, huh?
The reason we have so many problems in the Middle East is because we drew the borders
the first time and paid no attention to the people who lived there.
Turkey, for example, in the Kurdish scenario, I believe it was the Treaty of Lusanne in
1923, it handed Turkey a peninsula where all the Kurds lived.
That's why there's the trouble there is, because we didn't pay attention.
Now we don't have to redraw the maps, but we can give them the agency to do it themselves.
But because we want to have the ability to stabilize these nations at any point in time,
we want to keep the Kurds oppressed through other countries.
with the American people don't normally know about the Kurds. What do they know?
The memes. On the left you know that they experiment in various systems of
governance, which is cool, and they're egalitarian for the most part. On the
right, and I'm not going to put too fine a point on this, the Kurds have a very
high density of ridiculously attractive women. I mean it is what it is. And they're
egalitarian. Therefore, you get photos of women warriors that are attractive with
weapons. That's what the right gets out of it. They get to look at the, hmm, there's
a term, fighting blank doll. Anyway, they get to look at that archetype and they
get to feel like they're supporting them, but they only care enough to share a meme.
I would love if the fact that the Kurds have entered the main political discourse in the
United States, that this time it has some staying power, that this time we think beyond
memes and realize there's 30 million people out there without a country.
There's 30 million people that are stateless for all intents and purposes.
I think that we can muster a little more support than just sharing a meme on Facebook.
Anyway, it's just a fault. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}