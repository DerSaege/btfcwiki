---
title: Let's talk about an interview with a Special Forces officer and one word....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=oNv0fjY4qjQ) |
| Published | 2019/10/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Special Forces officer's interview on Fox News raised eyebrows.
- Special Forces are green berets, not Seals or rangers.
- They teach, weaponize education, and train indigenous forces.
- Special Forces motto: "To free the oppressed."
- Their image in popular culture stems from misconceptions.
- Special Forces are intelligent and not like robots.
- Officer on Syrian Turkish border expressed displeasure with senior command and commander-in-chief's decision.
- Officer used the word "atrocities" to describe events happening worldwide.
- American people need to be ready for what may be revealed.
- Officer criticized senior command and commander-in-chief's decisions in the interview.
- Elite unit members may disregard orders if witnessing atrocities.
- Special Forces are not robots; they have emotions and connections with local forces.
- Actions of Special Forces should not be a reflection on them but on the Oval Office.
- The American people need to address the use and abuse of indigenous forces.
- A moment of truth is approaching for the American people to confront their actions worldwide.

### Quotes

- "To free the oppressed."
- "We did it because we have allowed our government to get out of control."
- "They are not robots. They have emotions."
- "Their actions, if they happened, are completely excusable."
- "This is probably the time to address it."

### Oneliner

A Special Forces officer's interview sheds light on the realities and emotions behind military actions, urging the American people to confront their government's decisions and treatment of indigenous forces.

### Audience

American citizens

### On-the-ground actions from transcript

- Spread awareness about the true nature of Special Forces and their emotions (suggested)
- Advocate for the fair treatment of indigenous forces (suggested)
- Start a discourse on government accountability and responsible decision-making (suggested)

### Whats missing in summary

The deep emotional impact and human connection behind Special Forces actions can be best understood by watching the full transcript.

### Tags

#SpecialForces #MilitaryActions #GovernmentAccountability #IndigenousForces #AmericanPeople


## Transcript
Well, howdy there, Internet people, it's Bo again.
So tonight we're going to talk about the fact that a Special Forces officer gave an interview
to Fox News.
And in that interview, he used a single word that really raised my eyebrows.
And I think it's something that the American people need to be ready for.
But before we get to that, we need to go through some background information.
The term special forces gets misused a lot in the United States.
Seals are not special forces, rangers are not special forces.
Special forces means green berets.
Those are the only people who are special forces.
They teach.
They've weaponized education.
They are instructors.
They train indigenous and local forces how to stand on their own.
That's why they're so feared.
12 guys show up, a month or two later they've got 1,000 men with them.
If you are a foreign regime, that is terrifying.
Even their motto, traditional reading is to free the oppressed.
Literal translation is from an oppressed man, a free one.
I like that better, because most of the time,
the Special Forces guys aren't doing the heavy lifting.
They're giving those tools to the indigenous forces
and allowing them to obtain freedom
from an oppressed person, a free one.
They're giving them those tools.
I like that reading better.
What does this tell you about their image
in popular culture?
Their image comes from Rambo, a robot
who's kind of dumb.
It's not what these guys are.
They don't take dummies.
They are certainly not robots, and they
are incredibly intelligent.
They have to be able to operate on their own.
They're not robots.
They have a lot of autonomy when they're out there in the field.
So now that we have the background information,
let's get to the interview.
Now, in this interview, this person,
the special forces officer is currently on the Syrian Turkish border and he's
been in he's been in the special forces a while he said that he has trained
people on multiple continents so he had been around first this is exceedingly
rare second he had some let's just say he expressed expressed his displeasure
with senior command and the commander-in-chief's decision he referred to
to it as insanity.
He gave quotes that every outlet is going to love.
The liberal outlets are certainly going to play up the fact that, he said for the first
time in his career, he felt ashamed.
The right-wing outlets are certainly going to play up the fact that he is concerned about
the security of the prisons.
He's worried about a jaw break.
all fine and good. It's all worthy of discussion. But he used a word and it really kind of turned
my stomach. He used the term atrocities and he said they were happening. These guys do
this all over the world in very hostile environments. What most of us would consider something that
falls under that term, that's just part of it.
These are not men known for hyperbole.
That term, and that it's happening, we as the American people need to be ready for that.
Because when the photos or footage shows up, don't look away, because we did it.
We did it because we have allowed our government to get out of control.
we've put too much power in the executive branch.
Now the fact that this officer gave this interview tells me he does not care anymore.
Criticizing his senior command and the commander in chief in an interview, he ran out of things
to give.
It would not surprise me to learn that the US Army's most elite unit, at least elements
within it, decided to give a single finger salute to the commander in chief and disregard
his order, especially if they witnessed an atrocity.
not surprise me to find out that they engaged the advancing forces.
And we need to be ready to defend their actions.
I have seen a whole lot of berets and flashes as profile pics in the comments section.
There are a lot of long tabbers watching this video right now.
You need to get the word out.
They are not robots.
They have emotions.
If they witnessed something, then their actions are probably excusable, should be viewed as
a crime of passion.
And we need to get that message out now.
Because I don't know that I could stand seeing the President of the United States, after
They're thrusting them into the middle of the greatest foreign policy failure in modern
history tweeting and saying they're guilty of treason and you know it would happen.
They are not robots.
They have emotions just like anybody else and when they are conducting this training
they are eating with the local forces.
They are living with the local forces.
They become their friends.
build that camaraderie. If they exceeded their orders, it happened. Fine. In this case, it
needs to be viewed as excusable. Should not be a reflection on the Special Forces. Should
be a reflection on the Oval Office. The first rule of command is to never give an order
that is going to be disobeyed. Thrusting them into this situation with no warning is inexcusable.
Their actions, if they happened, are completely excusable.
In fact, I would say they're justifiable.
I'd be worried if it didn't happen.
We've got a moment of truth coming, the American people.
I've got a feeling we're going to have to look at what we've done.
what we've done and it mirrors what we've done all over the world.
This is probably the time to address it.
This is the time to commit to the idea that we are not going to use and abuse indigenous
forces like this, and to just hang them out to dry when it's politically expedient.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}