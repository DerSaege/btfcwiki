---
title: Let's talk about a UBI study....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=EH83zaWikI4) |
| Published | 2019/10/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Wishes a happy birthday to a young viewer before discussing UBI and Andrew Yang. 
- Mentions an 18-month study in Stockton, California, where 125 people received $500 a month, a boost for Yang's $1000 plan.
- Researchers from the University of Tennessee and University of Pennsylvania label the study as anecdotal, focusing more on storytelling than gathering hard data due to the limited sample size.
- Study aims to measure impact on mental and physical health, with results pending release.
- Breakdown of recipients: 43% working, 2% unemployed, 8% retired, 20% disabled, and 10% stay-at-home caregivers.
- Spending breakdown: 40% on food, 24% at stores like Walmart, Dollar General, 11% on utilities, 9% on auto repairs and gas, and 16% on medical expenses and insurance.
- Beau acknowledges the limitations of the sample size but appreciates the storytelling aspect supporting Yang's plan.
- Emphasizes the importance of younger generations like Lily in implementing such ideas that could revolutionize society.
- Acknowledges concerns about people's spending habits and stereotypes related to poverty.
- Concludes by hinting at the transformative potential of large-scale UBI implementation and the significance of public opinion, particularly from younger individuals like Lily.

### Quotes

- "Being in poverty is a lack of cash, not a lack of character."
- "They're the ones that are going to implement it, or not. They're the ones that will live with it."
- "Revolutionary ideas are coming out; this is an old idea. But implementing it on a scale that Yang is talking about, that's a pretty big undertaking."
- "People have said it before, being in poverty is a lack of cash, not a lack of character."
- "Matters what Lily thinks."

### Oneliner

Beau talks UBI, referencing a Stockton study on $500 monthly payments to 125 individuals, hinting at the transformative potential of large-scale implementation and importance of public opinion, particularly from younger generations like Lily.

### Audience

Policy advocates, UBI supporters

### On-the-ground actions from transcript

- Advocate for UBI implementation in your community (implied)
- Support research on UBI impacts on mental and physical health (implied)
- Challenge stereotypes about poverty and spending habits (implied)

### Whats missing in summary

The full transcript provides detailed insights into a small-scale UBI study's findings and implications, urging viewers to contemplate the transformative potential and societal impact of large-scale implementation. 

### Tags

#UBI #AndrewYang #Poverty #Society #Community


## Transcript
Well, howdy there, internet people, it's Bo again.
So we're gonna talk a little bit more about UBI here in a minute, but first, I have to
wish one of our youngest viewers a happy birthday.
Happy birthday, Miss Lily.
Okay, so Andrew Yang got a little boost from a study coming out of California.
It's an 18-month study of 125 people who were each given $500 a month.
His plan is to give people $1,000.
This happened in Stockton.
The researchers are from the University of Tennessee and the University of Pennsylvania,
and they admit that this is anecdotal.
It's more for storytelling than gathering hard data on how it's going to impact the
economy because 125 people is just not enough to really test it out. And they
freely admit that. I guess their real goal is to try to measure the impact it
has on mental and physical health, which is interesting. Those results will be
released later. So who'd they give the money to? What were the type of people?
43% were working, 2% were unemployed and not looking, 8% were retired, 20% were
disabled and 10% were stay-at-home caregivers, stay-at-home moms who were
taking care of an aging parent, something like that. Pretty good cross-section.
Okay, what'd they spend the money on? 40% of it went to food. All right. 24% of it
went to places like Walmart, Dollar General. 11% went to utilities, 9% went
went to auto repairs and gas.
That's 84% right there, all pretty reasonable spending.
The rest, the other 16%, went to stuff like medical expenses,
insurance.
I'm sure there's some recreation in there as well.
I mean, that speaks well for Yang's plan, it really does.
I know that this isn't a large enough sample size to actually matter, but the storytelling
is there.
The storytelling is certainly there.
I mean, I am, personally, still not convinced.
Still have a lot of reservations about this, but the truth of the matter doesn't really
matter what I think.
Matters what Lily thinks.
Matters what people her age think.
They're the ones that are going to implement it, or not.
They're the ones that will live with it.
like this, and a lot of the new ideas that are coming out, they're revolutionary.
Yeah, this is an old idea.
But implementing it on a scale that Yang is talking about, that's a pretty big undertaking.
It is something that would transform American society.
Whether or not that's the way to go, well, that's up to people like Lilly.
actually curious to hear what she has to say about it. But it does give a little
bit of a boost to his theory. You know one of the big concerns with it is that
people are just gonna blow it, spend it on stupid things. And in that is kind of
the idea that people who don't have money don't know how to budget or that they somehow
wound up without money for that reason.
People have said it before, being in poverty is a lack of cash, not a lack of character.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}