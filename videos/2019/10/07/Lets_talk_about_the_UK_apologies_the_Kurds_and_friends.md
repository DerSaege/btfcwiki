---
title: Let's talk about the UK, apologies, the Kurds, and friends....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=qyG910MsU4s) |
| Published | 2019/10/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- An American diplomat's wife, protected by diplomatic immunity, caused the death of a 19-year-old in the UK by driving the wrong way.
- The family of the victim is left with no closure as she used immunity to return to the US without facing consequences.
- The Kurds, a vital ally of the US in the Middle East, are being betrayed as the US greenlights Turkey's incursion into Syria.
- Despite the Kurds' loyalty and support to the US, they are at risk during this invasion with no country of their own.
- The US is actively supporting the invasion, unlike past betrayals that were more discreet.
- Beau connects these seemingly unrelated events, revealing a deeper theme of establishment versus the people.
- He criticizes the establishment for always protecting themselves at the expense of the people.

### Quotes

- "It's always portrayed as left versus right, as your demographic versus another equal or lesser maybe just above demographic. At the end of the day, it's not what it is."
- "It's establishment versus you. They will always write you off to protect themselves. It's always what happens."
- "George Carlin said it best. It's a big club and you ain't in it."

### Oneliner

An American diplomat's wife avoids accountability using diplomatic immunity while the US betrays its Kurdish allies in Syria, revealing a deeper theme of establishment versus the people.

### Audience

Global citizens

### On-the-ground actions from transcript

- Contact local representatives to advocate for justice for victims of diplomatic immunity (implied).
- Support organizations aiding Kurds and raise awareness about their plight (implied).

### Whats missing in summary

The emotional impact on the victim's family and the Kurds, and the need for accountability and support for those affected by diplomatic actions.

### Tags

#DiplomaticImmunity #USForeignPolicy #KurdishAllies #EstablishmentVsPeople #GlobalJustice


## Transcript
Well, howdy there, Internet people, it's Bo again.
So tonight we're going to talk about two seemingly unrelated pieces of international news, but
we're going to talk about them because both of them speak to the heart of an overriding
theme on this channel and the things that we talk about.
They get right to the meat of it.
The first one happened in the United Kingdom.
The wife of an American diplomat is leaving a Royal Air Force base that has been leased
to the United States.
It's a spy outpost now and handles communications for Europe.
Turns the wrong way down a road and a 19-year-old paid the price.
Lost his life.
According to the police there, she initially talked to them and said she wasn't going anywhere.
And then she used diplomatic immunity to get on a plane and come back to the United States.
So that family gets no closure.
None.
They don't even really know what happened, at least it's not in the papers.
And she may be back here, crying herself to sleep at night, overcome with remorse, but
the family will never know that.
The family will never know that because the very first thing that the lawyers certainly
told her was to not apologize because it shows guilt.
So they're going to be left in limbo.
Now in a different part of the world, the President of the United States has green lit
an incursion by Turkey into Syria.
The Kurds are certainly going to oppose this.
The Kurds are our greatest ally in the Middle East, greatest ally of the United States.
I'm certain, depending on which political party you are, you're saying, no, one of
two countries is our greatest ally in the Middle East.
Make out a list.
Make out a list of every time those countries have committed their troops to fight alongside
U.S. troops. Then compare it to the Kurds. In the Kurds they're not even a country. It's
an ethnic group. They don't have a country. These are volunteers. No country of their
own. No friends but the mountains. And as one of them told me tonight, we're waiting
for the mountains to betray us. Because that's what's happening. We're selling them out.
Again, they will pay the price during this incursion.
That's what makes this worse than the other times.
In the past, we have sold them out, but we did it tactfully.
We just didn't help or didn't help much or undermined them via economic methods.
This time we're actively encouraging and cheering on the invasion of their homes.
I know those two things don't seem like they have much to do with each other.
reality is they do a lot. It's always portrayed as left versus right as your
demographic versus another equal or lesser maybe just above demographic. At
the end of the day it's not what it is. It never is. It's establishment versus
you. They will always write you off to protect themselves. It's always what happens.
George Carlin said it best. It's a big club and you ain't in it. I'm not sure he understood
that that club was international.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}