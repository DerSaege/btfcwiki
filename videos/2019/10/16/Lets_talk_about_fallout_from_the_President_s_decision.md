---
title: Let's talk about fallout from the President's decision....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=dDRSXmd9LAQ) |
| Published | 2019/10/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Eight days since the president's decision, fallout is evident.
- US forces moved for Turkey to invade Syria, leading to atrocities by Turkish-backed militants.
- Kurds turned to Russia for support, with Russian and Syrian troops filling the power vacuum.
- ISIS staged jailbreaks, potentially growing stronger.
- US forces nearly hit by artillery from a NATO ally.
- Air Force scrambling to move 50 nuclear weapons from Incirlik Air Force Base.
- White House discussing nuclear weapons before removal is alarming.
- Turkish perspective sees potential superpower status by overtaking US base.
- B61 nuclear weapon can yield up to 340 kilotons of power.
- Beau warns of catastrophic impact if such weapons are used, referencing Hiroshima.

### Quotes

- "This will go down as the worst foreign policy decision in American history."
- "It is spitting on the grave of every U.S. soldier who died in the last 15 years over there."
- "All because the president would not listen to people that knew more than him."

### Oneliner

Eight days after the president's decision led to chaos in Syria, US forces moved for Turkey to invade, potentially resulting in catastrophic consequences if not contained immediately.

### Audience

Policy Advocates, Global Citizens

### On-the-ground actions from transcript

- Contact policymakers to urge immediate containment of the situation (suggested)
- Support organizations aiding those affected by the fallout in Syria (exemplified)

### Whats missing in summary

The full transcript provides a detailed breakdown of the aftermath of a significant foreign policy decision, with warnings of potential catastrophic consequences if not addressed promptly.

### Tags

#ForeignPolicy #Syria #NuclearWeapons #USForces #Turkey #Russia


## Transcript
Well, howdy there, internet people, it's Bo again.
So it has been eight days since the president's fateful decision and we're seeing the fallout.
So we're going to talk about the fallout from his decision and then we're going to talk
about fallout because, I mean, that's something we have to talk about now.
It started when the president of the United States moved a small number of US forces out
the way to a different location in Syria so Turkey could invade. That's how it
started. That started with air operations followed shortly by ground
operations with Turkish-backed militants committing atrocities on the side of the
road. This drove the Kurds firmly into the arms of Russia who was waiting with
open arms because they are the most powerful non-state actor in the Middle
East, they will remain in Russia's arms for decades to come Russian and Syrian
troops quickly advanced to fill that power vacuum into what positions that
were held by US forces happened so quickly it's almost like they knew it
was coming. IS staged a series of jail breaks to get their comrades out, and
they are probably now stronger than when President Trump took office.
US forces have almost been hit by artillery fired by a NATO ally.
The Air Force is reportedly scrambling to try to get 50 B61s out of
Incirlik Air Force Base. B61s are nuclear weapons.
Whoever talked to the press about this from the White House needs to be fired.
This should not have been talked about until they were removed.
They should have been removed the moment the Turkish president indicated interest in obtaining his own weapons.
Most people thought they had been removed and that it was a show like they were still there.
But I guess not.
The fact that this was discussed before they were removed illustrates why you don't override the counterintelligence
guys
hey this person doesn't need a security clearance because if you look at this
situation from the Turkish perspective now they are facing EU sanctions they're
facing US sanctions they have no allies outside of NATO and their position in
NATO is not exactly secure anymore however they could become a superpower
by overrunning one US base within their territory.
What is a B61?
It is a variable yield device.
In English, that means it's adjustable.
You can tell it how powerful to be.
It can range anywhere from a third of a kiloton
to 340 kilotons.
To give you an idea of what it will do,
we will use 300 kilotons because the math is easy.
It's not the top end, but it's pretty close.
Creates a fireball that is 200 million degrees
and a mile wide.
At the edge of that mile, concrete is literally melting.
The winds from the blast are 750 miles an hour.
Think of that in relation to a hurricane
with 100 mile an hour winds.
only at seven and a half times stronger,
and the hurricane's on fire.
At three miles out, clothing will burst into flames.
Creates a lethal environment that is roughly 50 to 60 square miles.
Another good point of reference would be Hiroshima. That was a 15
kiloton device. This has the capability
of delivering 340.
It does not need a missile.
It can be delivered by any plane with some pretty simple modifications.
Make America great, right?
At least now we have a time frame, a reference. Now we know what they're talking about.
1950s and 60s when everybody was in bomb shelters.
Because if these devices get out, that's what's going to happen.
We're going to be back in that situation.
And as the president undermines
And while everything, Republicans in Congress are silent, saying nothing because they are
terrified of the President's Twitter raff, and they have forgotten that they're supposed
to be leading a country as a co-equal branch of government.
They're not just supposed to be the guys that go out to defend the President's stupid decisions.
This will go down as the worst foreign policy decision in American history.
It will have effects for decades to come.
And it is spitting on the grave of every U.S. soldier who died in the last 15 years over
there.
Because all of this will spread if it is not immediately contained.
Everything that happened over there will have been for nothing.
It started as something bad, but there were little bits of good that came out of it.
And all of that's going to be undone.
All because the president would not listen to people that knew more than him.
I would strongly suggest the next time we elect a president, we don't choose a failed
reality TV star with an ego that can't accept the fact that he doesn't know everything.
This is what Mattis warned everybody about.
This is why he left.
This is a horrible decision and it will get worse if it is not immediately contained and
The President of the United States is not up to the task.
He has shown that and the Turkish President has said he will not meet with Vice President
Pence, only President Trump because now, because of the President's weakness that he showed
on the phone, the Turkish President believes he has the upper hand.
And he does...anyway...it's just a thought...y'all try to have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}