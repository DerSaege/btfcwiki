---
title: We talked about impeachment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_VJoSOPUQVc) |
| Published | 2019/10/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains that the impeachment process is not unconstitutional and follows the rules outlined in the Constitution.
- Clarifies the difference between impeachment proceedings and a criminal prosecution.
- Addresses the comparison made by President Trump and Senator Lindsay Graham to a lynching, explaining the inaccuracy of the analogy.
- Points out that the impeachment proceedings are not a criminal prosecution, so due process applies at a later stage.
- Describes the closed-door nature of the impeachment proceedings to prevent witnesses from coordinating stories.
- Criticizes Republican lawmakers for storming the impeachment inquiry and obstructing the process.
- Raises concerns about the legal argument that the President is immune from all criminal liability, potentially leading to unchecked power and abuse.

### Quotes

- "The impeachment process is exactly nothing like a lynching."
- "They have to attack the foundations of the country."
- "It’s not a coup. It’s the way the process is laid out."
- "This argument that is being made is the legal argument to turn Trump into a dictator."
- "Trump becomes Tsar, and all men are slaves to the Tsar."

### Oneliner

Beau explains the constitutional validity of the impeachment process, dismantles misleading analogies, and warns against unchecked presidential power.

### Audience

Activists, Constitutional scholars, concerned citizens

### On-the-ground actions from transcript

- Contact elected officials to express support for upholding constitutional processes (suggested)
- Educate others on the differences between impeachment proceedings and criminal prosecutions (exemplified)
- Organize community forums to raise awareness about the implications of unchecked presidential power (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of the impeachment process's constitutionality, dispels misconceptions, and underscores the importance of upholding democratic principles.

### Tags

#Impeachment #Constitution #DueProcess #PresidentialPower #Accountability


## Transcript
So we got us a bona fide circus up there on Capitol Hill.
A lot of people are running around saying that what's being done is unconstitutional.
People are saying that the process is unconstitutional.
That's patently false.
The impeachment process is the indictment phase.
Okay.
What the house is doing, that's the indictment phase.
If they render a verdict to impeach, then it goes over to the Senate to convict.
They hold the impeachment trial, that's Article 1, Section 3.
The Senate has sole power over impeachment trials.
Now it does say that the person convicted is still liable to indictment, trial, judgment
and punishment in a criminal prosecution, but that's not the Senate.
The Senate is not going to send the President to prison.
That may happen afterward.
to section four. That's the part that says high crimes and misdemeanors. Now, what that
actually says is that the president shall be removed for these types of charges. Shall
be is legalese, must be removed. The House and Senate can impeach and convict for pretty
much anything they want, but for these things the punishment has to include removal.
Article 3, section 2, trial of all crimes except impeachment has to be done by jury.
So this goes along with the rules for the Senate about the limitations on punishment
and then clarifying that the person could be tried later.
What it's doing is it's separating and it's saying that an impeachment trial is not a
criminal prosecution.
It's not the same thing.
So it's getting rid of the idea of double jeopardy and stuff like that.
Okay, so that's it.
The main document, the Constitution, that's all it says about impeachment.
Those are the rules.
None of those are being violated.
None.
There's nothing unconstitutional about the proceedings right now.
A certain word has found its way into the President's vocabulary and then into his
tweets.
So what did he say?
He said,
So someday, if a Democrat becomes President and the Republicans win the House even by
a tiny margin, they can impeach the President without due process or fairness or any legal
rights.
All Republicans must remember what they are witnessing here.
lynching. Enter Senator Lindsay Graham from the great state of South Carolina, who said this is
a lynching in every sense, because he's completely ignorant of history. What parallels due process
in impeachment proceedings occurs in the Senate during the trial. So if he doesn't get it Senator
Graham that would be your fault. There's no requirement for due process here.
First, we're still in the impeachment phase. They don't even have to tell him
it's happening. More importantly, that comes from the Fifth Amendment. No
person shall be deprived life, liberty, or property without due process. What is the
max punishment? Removal from office and disqualification. His life, liberty, and
property are not in jeopardy. The impeachment process is exactly nothing like a lynching.
That's the premeditated extrajudicial informal public execution of someone. Doesn't seem
like what's going on here. Let's just cut to the chase. The closest thing to a lynching
that President Trump has been involved in is when he took out a full page ad advocating
for the execution of people who turned out to be innocent.
And that's the thing about lynchings.
In most cases, the people, the victims, are innocent.
Doesn't appear that he is.
But hey, don't worry, he'll get his due process.
The President has a right to confront his accusers.
That's the Sixth Amendment.
That amendment starts with the phrase, in all criminal prosecutions, as outlined in
Article 1, Section 3, this is not a criminal prosecution. Now, he may get to confront his
accusers. That may happen, but it's going to be after the Senate convicts, and then he's
indicted. And during the trial, a criminal trial, he would be able to do that.
Article 1, Section 2, it says that the House shall appoint its officers, you know, the Speaker of
the House and whatever.
And then they have sole power over impeachment.
So to be clear, if they wanted to do the entire thing in secret, not even inform the president
that it was happening and do a surprise vote, that's completely constitutional.
I was on Twitter and somebody brought something up and it just kind of blew my mind.
The person said that Republicans weren't concerned about the impeachment process being
unconstitutional, that was just the word they were using, that they were really concerned
because it's different than Clinton and Nixon, that it's being held in secret.
So what's the difference between them?
Yeah I mean, yes, in this case there are going to be some international secrets that you
don't want public.
Yeah, that's true, okay.
But that's not the reason they're being held in secret.
the whole reason anyway, not even the main reason. See in the other two, facts
were already established generally. They had physical evidence, reports, articles.
They knew what went down. They're just putting it on record. In this case, it's
not what's going on. It's not what's going on. They have a general
framework of what happened, but they don't know exactly what went down and
who was involved. I want you to think about your favorite cop show, or if you
don't have one, think about Law and Order. Everybody's seen an episode. So there's
the cop, there's a shooting, let's say. There are four people in the car. He's
certain one of them did it. One person drove and knew that it was going to go
go down, the other two are innocents. So what's he do? Cuffs them up, takes them
all back to the station, and puts them all in a room together and starts asking
them questions, right? No, of course not, that would be stupid. They'd be able to
cook up testimony between each other right there because they know what the
the person sat.
The hearings are in secret so the other witnesses don't know what's been said already.
It's a way to keep them honest.
That's why.
That's why it's done that way.
It's why it's done in criminal cases when they divide people up so they can't cook up
a story.
Any defense attorney will tell you, if your client is just guilty of sin, the only thing
you can do is go after the cops.
Go after the chain of custody.
Go after the process.
Try to get it thrown out on technicality.
Get evidence suppressed.
That's kind of what the Republicans are trying to do.
Republican lawmakers stormed the impeachment inquiry.
And what was this dire complaint that all these theatrics were over?
Well, we're not allowed in the hearing, but other Republicans are.
Just you're not, because you're not on the right committee.
This is incredibly common, happens all the time.
Closed door proceedings are very common.
Mitch McConnell just told them.
Attack the process, attack the tactics,
because they can't defend the President's actions anymore.
Attack the process.
What does that mean?
Means undermine the Constitution,
because that's where the process is laid out.
Means obstruct Congress.
Means violate their oaths.
And like good little foot soldiers,
they did exactly what they were told.
They have to attack the foundations of the country.
They have to attempt to undermine the Constitution,
because they care more about their party
then they do the Constitution.
There is nothing unconstitutional occurring.
It's not a coup.
It's the way the process is laid out.
Anybody who says that it's a coup,
anybody who says that it's unconstitutional, all they're doing is proving
they've never read the Constitution.
In an attempt to stop his tax returns from being released,
the President's lawyers made the argument
that he is immune from all criminal liability.
All.
He can't be investigated.
He can't be indicted.
He can't be prosecuted.
He can't be processed.
Nothing.
He is total immunity from all forms of criminal liability
because the argument made in federal court
was that he could literally commit murder.
That's pretty high up in our hierarchy of crimes.
That's one of the bad ones.
If he can do that and suffer no criminal liability, what's to stop him from shutting down voting
stations in locations that oppose him or engaging in massive voter fraud?
What would stop him?
There's no criminal liability.
Nobody can investigate it.
Well, certainly Congress would impeach, right?
I guess so, unless he has him imprisoned, or worse,
because no criminal liability.
This argument that is being made
is the legal argument to turn Trump into a dictator.
It's terrifying, because that's not how it's being framed.
Being framed is this crazy argument that the president's lawyers made.
But no man that would allow his lawyers to make that argument on his behalf should be
in public office.
These judges, whether they know it or not, are deciding the fate of the country.
If they decide that, yes, the president is completely immune from all criminal liability,
that's it.
It's over.
No checks, no balances, absolute power, and that power will be abused.
Trump becomes Tsar, and all men are slaves to the Tsar.
That's the reality.
And it's not a slippery slope argument.
That's literally what they're arguing in court.
In federal court, this is the argument that was made.
We sit and listen as people try to say that there's a coup against the president.
Meanwhile, he's attempting to gain the power to do whatever he wants, with no legal ramifications.
So there's your scary Halloween story.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}