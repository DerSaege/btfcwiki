---
title: Let's talk about bagging the Big Baghdadi Wolf....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=3NhSJ4SdA4A) |
| Published | 2019/10/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The big bad wolf has been captured, but the organization may not be entirely defeated.
- Disrupting command structures can be risky if the next leaders are unknown.
- Historical examples, like the Irish Rebellion of 1916, show the consequences of eliminating leadership without a clear plan for succession.
- Two key figures, Devalera and Collins, exemplify the different types of leaders within organizations.
- The strategy of eliminating only one faction within an organization to render it combat ineffective but still intact.
- The goal is to force politically savvy individuals towards peace negotiations due to incompetent military leadership.
- The U.S. strategy may involve targeting key figures to destabilize organizations.
- The operation against the captured individual was expedited due to the president's actions.
- Success in neutralizing competent commanders will lead to minimal activities from the group.
- Failure to eliminate key figures may result in infighting or the rise of a more dangerous organization.

### Quotes

- "It's the end, right? They're defeated. Maybe not."
- "If they take out all of them, if they get rid of all of the politically savvy people, you have an incredibly dangerous organization."
- "You didn't defeat us."

### Oneliner

The big bad wolf may be captured, but disrupting command structures without clarity on succession can have dangerous consequences, as history shows.

### Audience

Strategists, policymakers, analysts.

### On-the-ground actions from transcript

- Analyze the leadership structures within organizations to understand potential outcomes (implied).
- Monitor for signs of infighting or power struggles within groups (implied).
- Stay informed about international events to understand the implications of strategic decisions (implied).

### Whats missing in summary

The full transcript provides historical context and strategic insights that can deepen understanding of organizational dynamics and leadership impact.

### Tags

#Leadership #OrganizationalStrategy #SuccessionPlanning #ConflictResolution #InternationalRelations


## Transcript
Well, howdy there, internet people, it's Bo again.
So they got the big bad wolf.
They got him.
The president is taking credit and victory laps and all is well.
It's the end, right?
They're defeated.
Mm, maybe not.
Maybe not.
So now what?
What's going to happen now?
Personifying an organization like this, with the leadership, it's not really a good idea.
It is not a good idea.
These organizations, like all organizations, they have structure.
They have plans for a transfer of power.
And meeting this kind of end in that field, it's predictable.
They know who the next guy in charge is going to be.
They're not necessarily defeated simply because their command and control was disrupted.
And that's what they did with this, that they took out the command structure, hopefully.
When you're dealing with a conventional force, taking out the command structure is pretty
much always a good idea.
an unconventional force, it's only a good idea if you know who's coming next.
Sometimes disrupting the command and control if you don't know who's coming next is actually
really, really bad for your side.
I'll give you an example.
Easter of 1916 was the beginning of the Irish Rebellion.
American viewers this is it's like July 4th only the fighting started like right
that second at the end of that engagement the British won and they went
through and they literally were like that guy's a leader that guy's a leader
that guy's a leader and they took him and they put him on the wall they got
rid of them. They didn't know who was coming next and it proved a big mistake
for their side because the next crop was very different. They were very very
different. The entire crop. We're going to focus on two people because they're
good examples of what we're going to be talking about here. You had a guy named
named Devalera and a guy named Collins. Those two are the examples we'll focus on.
Now if you're familiar with this story from the movie Michael Collins, Devalera
is portrayed as this whiny, sniveling guy and whatever, maybe that's historically
accurate, but he was politically savvy. Extremely politically savvy and it
doesn't really show that except in bad light in the movie. Collins was a
military juggernaut. They didn't like each other. They didn't like each other
and this holds true through almost every type of organization like this. You have
these two competing groups of people in command. So what happened once of those
Once that crop took over, and there were a whole bunch of other people, what, 1921, the
British were asking for a truce and the Irish Free State was born.
It didn't take long under this new leadership and a wave of unconventional warfare.
So drawing on lessons like that and learning from lessons like that, the current theory
is to only eliminate one half.
You only get rid of one half of them.
You get rid of the Collins side, people that were like him.
You leave the Devalaris alone.
Even if you have the opportunity, you let him go.
Now the reason you do this is because it leaves the organization intact, but combat ineffective.
They don't have the military minds.
But the organization is still there, so people who are sympathetic to the cause, they look
and they're like, I want to join that organization.
And the opposition, they're already looking at it.
You're already looking at this group, so when the new people show up, they're going through
them.
went to college, he's mad at his dad, wants an adventure, don't need to worry
about him, this guy spent six years in the army and two years in prison, this
one's got to go. And then that guy gets taken out. And if you do this long
enough, the whole organization moves because the politically savvy people
realize their military operations are always failures because they
only have incompetent commanders, so their only option is peace and
it brings them to the negotiation table.
There are a lot of organizations that have been altered in this way.
They've been realigned.
There is a lot of evidence to suggest that is what the US
was doing, was attempting to do, there have been a lot of not huge names, but pretty big
names that have met similar ends recently, and the president's own comment, he kind
of said it offhand, you know, we've taken out other terrorists, but no big names.
He was kept in the loop and up to date on these because they were part of the overall
strategy, but nobody really probably took the time to explain it to him because they're worried he
would say it or they just didn't think he'd understand it. So, there is evidence to suggest
that's what was going on. Now, we've also found out that the operation against this guy was moved
up. It was done ahead of schedule. It was done hastily because of the president's withdrawal.
They had to do it like right now, rather than waiting for the time they wanted.
That leaves us with the question, how far did they get in cleaning house?
If they got all the way, then this is a win. This is a win and we're not going to see much
more from these guys.
If not, well, there's a lot of variables at play.
So let's go through the three options
of what can happen next.
If they were successful and they took out
the competent commanders before they got this guy,
we're going to see a whole lot of talk, a whole lot of videos,
and some minor operations that aren't going to do too well.
They'll be, they probably won't even make the news here.
They're not going to be big events.
If they only got partially through,
only got like halfway done, you're going to see infighting.
You're going to start seeing these groups fight
inside each other.
And you'll see the Collins guys realizing
Now that they're outnumbered by the politically savvy guys, they're going to start taking
them out.
Because normally the politically savvy guys are also very corruptible.
They're not true believers.
They are not liked by the competent military commanders.
So they'll start trying to even the odds.
Now where you have to worry is if they take out all of them, if they get rid of all of
the politically savvy people, you have an incredibly dangerous organization.
You have a group that is now staffed and led by nothing but competent military commanders
and nobody to hold them back.
They become a very efficient machine.
That's the worst case scenario.
Now let's just say they didn't get his chosen successor.
We're going to see a spectacular response, some kind of coordinated statement, some kind
of activity that is going to probably result in a lot of casualties.
And it's a way of saying, we're still here.
You didn't defeat us.
So those are the three future outcomes.
And based on that, we'll be able to tell how far they got in this strategy.
So there you go.
That's what we're looking at now as far as the future of this group.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}