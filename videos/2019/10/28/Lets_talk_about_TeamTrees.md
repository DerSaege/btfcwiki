---
title: 'Let''s talk about #TeamTrees....'
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=f5xaL23roXg) |
| Published | 2019/10/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Mr. Beast, a 21-year-old YouTuber known for silly stunts, is involved in a unique project to raise $20 million for planting trees.
- Partnered with the Arbor Day Foundation, the goal is to plant 20 million trees across the country by December 2022.
- The foundation, with a 50-year history, will ensure that each dollar donated equals one tree planted.
- The project aims to plant native species, promoting environmental impact.
- Within just two days, $5 million has already been raised for the cause.
- Beau encourages viewers to donate to the Arbor Day Foundation, mentioning that donations may be tax-deductible.
- Beyond donating, Beau suggests planting native fruit trees in personal yards to enhance food security, reduce grocery bills, and foster community spirit.
- Beau acknowledges the project's success in engaging a wide audience, with various creative videos showcasing the tree-planting efforts.
- Unlike typical online challenges, this initiative is praised for its potential significant impact on the environment.
- Beau urges viewers to support the cause by contributing a dollar or two.

### Quotes

- "Your donation to them [Arbor Day Foundation] would be tax deductible in most cases."
- "Definitely kind of jumping on team trees here."
- "It's actually going to create some meaningful impact."

### Oneliner

Mr. Beast collaborates with Arbor Day Foundation to plant 20 million trees by December 2022, setting a goal of raising $20 million, already reaching $5 million in two days.

### Audience

Environmental enthusiasts, Tree advocates

### On-the-ground actions from transcript

- Donate a dollar or two to the Arbor Day Foundation to support tree-planting efforts (suggested).
- Plant native fruit trees in your yard to enhance food security, reduce grocery bills, and build community spirit (implied).

### Whats missing in summary

The full transcript provides more detailed insights on the impact of planting native species and how viewers can actively participate beyond financial contributions.

### Tags

#MrBeast #ArborDayFoundation #TreePlanting #EnvironmentalImpact #CommunitySpirit


## Transcript
Well howdy there internet people, it's Bill again.
So today we're going to talk about a really weird stunt going on on YouTube.
You might have seen something about it.
There's a guy, he's like 21 years old, and most of his channel is just stupid stunts.
Stuff like we bought a boat, last person to keep their hand on it gets to keep the boat,
type of thing. It's more or less mindless stuff. So when somebody was like, hey, you
know, Mr. Beast has got this thing going on and you want to get involved in it, I'm like,
yeah, sure I do. Well, it's $20 million. Well, that doesn't really seem like something. They're
going to plant 20 million trees. You have my attention. So he's partnered up with the
Arbor Day Foundation. One dollar equals one tree. So they got to raise 20 million
dollars. It's been going on for about two days and they've already raised about
five. Pretty cool. Arbor Day Foundation if you don't know. They've been around
like 50 years. They've been doing this a very very long time. Your donation to
them would be tax deductible in most cases. Talk to your accountant. I'll put
the link down below. So basically the idea is between now and December of 2022
I think. They're going to plant 20 million trees all over the country and
they're going to plant native species which is what they should be doing and
everything from everything I've seen it's perfect. It is perfect. So I'm
definitely kind of jumping on team trees here. I would also like to take a moment
to remind everybody that yeah donating is very easy dollar five dollars twenty
dollars whatever puts a tree in the ground. I would also suggest you do it in
your yard and I would suggest you do a native fruit tree and if you can convince
other people in your neighborhood to do it. One it helps with food security, two
it lowers everybody's grocery bill and three it helps create that community
spirit that we always talk about. This is engineered to have an impact and
reduce some environmental factors and it looks like it's been
done really really well. There have been a lot of people involved in it. There's a
whole bunch of videos out there on it that are going to be far more
more entertaining with this than this everything from seed cannons to drones
are doing it all kinds of stuff since it's kind of taking over YouTube but for
once it's not a stupid challenge it's actually going to create some meaningful
impact 20 million trees there there are infographics about it it's it will have
substantial impact. So anyway, there you go. Definitely consider throwing a dollar
or two their way. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}