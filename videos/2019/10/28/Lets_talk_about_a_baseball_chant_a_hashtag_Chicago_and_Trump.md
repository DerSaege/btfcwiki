---
title: Let's talk about a baseball chant, a hashtag, Chicago, and Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=lVfLKfPMsdU) |
| Published | 2019/10/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President's approval should be soaring due to recent wins but it's not; facing backlash from veterans, baseball stadium booing, and Chicago demonstration.
- Media is trying to pacify Americans, labeling dissent as un-American and unpatriotic.
- Beau argues that inflammatory rhetoric is necessary to understand the dangers of the current political climate.
- Warning against the potential for a dictatorship if civil liberties are disregarded.
- Beau criticizes conservatives for only now opposing dangerous rhetoric when it's aimed at them.
- The time for civil debate ended when Trump's lawyers argued he was above the law in court.
- Beau believes it's time for people to understand the reality of the current political situation and not shy away from inflammatory debate.
- Criticism towards those who followed Trump blindly, selling out the country for a red hat and slogan.
- Emphasizes the importance of using strong rhetoric to prevent a descent into dictatorship.
- Beau expresses readiness to stand against authoritarianism, even if it means going beyond rhetoric.

### Quotes

- "Be patriotic. This is un-American. Don't do this. Just sit there, be a good German. It'll all work out. No, we know how that works out. We're not doing it again."
- "You're sending us there."
- "I'm not going to be a person that just follows orders."

### Oneliner

President's approval should be soaring, facing backlash from veterans, baseball stadium booing, and Chicago demonstration, urging for inflammatory rhetoric to understand dangers of political climate and prevent descent into dictatorship.

### Audience

Concerned citizens

### On-the-ground actions from transcript

- Challenge dangerous rhetoric and stand against authoritarianism (exemplified)
- Engage in open, honest, and potentially inflammatory debate to address the current political situation (exemplified)

### Whats missing in summary

The full transcript provides a deep dive into the necessity of challenging dangerous rhetoric and understanding the risks of political complacency.

### Tags

#PoliticalClimate #Authoritarianism #InflammatoryDebate #Dictatorship #CivicEngagement


## Transcript
Well, howdy there, Internet people, it's Bo again.
We are in a weird, weird world.
We are through the looking glass at a time when the president's
approval rating should be soaring.
And I mean soaring because of his recent wins, it's not.
Instead, he's dealing with a hashtag trending on Twitter,
veterans for impeachment.
The veterans are a community that generally are very supportive, especially after a win
like that.
He's dealing with a baseball stadium, booing him and chanting, lock him up.
He's dealing with a demonstration in Chicago, it's very strange.
And out comes the media to pacify Americans.
This is un-American.
This is un-patriotic.
Be good.
Be civil.
And I get the argument, I've made it myself in the past, I have been against this kind
of rhetoric for a very, very long time.
They're doing a horrible job of explaining the point.
So let me explain the point and then tell you why it's changed.
New information should change your views on things and this is the time when it needs
to happen.
The idea is that that kind of rhetoric is dangerous.
It sends us down a road towards a tempeh dictatorship.
That's the idea.
Lock her up.
Lock him up.
Lock her up.
Lock him up.
fun and games and while everybody's chanting and then somebody does it.
Not because of any crime the person committed, but simply because they're of another political
party.
They're opposition, so they lock them up and then it's over.
The great American experiment is gone.
That's the danger and it's a real danger.
With Trump, it's even more of a danger because if he believes that when he leaves office,
going to be prosecuted, it's going to be really hard to get him out of that oval.
He may employ means that we haven't really considered to stay in power.
It's dangerous.
It really is.
It's not rhetoric that should be used.
But the time to make that argument was years ago when it started, not when the rhetoric
Because that's what's happened.
A lot of conservatives are all of a sudden saying, oh, no, don't say this, this is bad, this could lead there, yeah,
yeah, it could, it could, and I'm sorry that it took that rhetoric being aimed at you to understand that.
rhetoric being aimed at you to understand that the time to make that
argument was years ago. You don't get to sit there and say hey be patriotic this
is un-American don't do this just sit there be a good German it'll all work
out no we know how that works out we're not doing it again. The time to make that
argument is passed. The time to make that argument stopped when the president's
lawyers walked into a courtroom and created another argument. And that other
argument was that he was above the law. That he could not be investigated, could
not be charged, could not be prosecuted until somebody forced him out of power.
Because with that power, if that's there, he can do anything he wants to, including
suspend elections because there's no criminal liability and that's the
argument they made. When that argument was made, the idea of civil rhetoric and
civil debate, it ended. Yeah, it's time for inflammatory debate. It's time for
people to understand exactly what we're dealing with here. We've been saying it
since he took office, since before he took office. This is the type of person
he is. This is the kind of person who's going to scapegoat an entire group of
people. Tear their families apart and throw them in cages so he can say, look
you're better than them, stick with me and I'll keep you better than them.
Meanwhile I'm gonna bleed the country dry. But he didn't say that last part out
loud. And people followed him. The media and the Republican Party do not get to
walk out now and say, oh be patriotic. This is uncivil. Simply because people in
a baseball stadium had the spine and courage to make the argument that you failed to.
You followed this guy.
You sold out your country for a red hat and a stupid slogan.
The rest of the country doesn't have to follow you.
The fact that people in that baseball stadium understand what this country is supposed to
be more than you, well that's really how it should be.
Because you're supposed to take your cues from them, not the other way around.
You're not a ruler.
You're not supposed to be.
Yeah, this rhetoric, it's the time to employ it.
Because if we don't, it's going to have to go beyond rhetoric.
And nobody wants that.
Even the people that think they do and they're talking about it on Twitter, well, I'm ready.
No, you're really not.
You don't understand what it is.
I know that most people who think that when that comes, it's all going to be Muslim, illegal
immigrant, Obama-loving communists that you have to fight.
That's not the way this works.
It's your neighbor.
It's the parents of the child who your kid plays with.
So yeah, it's the time to use that rhetoric so that doesn't become a possibility.
The president's committed crimes.
To assert that people can't say he should go to prison for it.
That is the road to dictatorship.
It's not a possible road that may occur because of rhetoric.
You're sending us there.
I'm not going to be a person that just follows orders.
And I'm glad there's a stadium full of people that feel the same way.
Anyway, it's just a fault.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}