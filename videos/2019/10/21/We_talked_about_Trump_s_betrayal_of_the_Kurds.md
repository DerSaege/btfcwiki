---
title: We talked about Trump's betrayal of the Kurds....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=P3Eg-BvRUJg) |
| Published | 2019/10/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The President of the United States allowed Turkey to invade Syria, abandoning Kurdish allies who have fought alongside American soldiers for 15 years.
- Turkish-backed militants committed atrocities against the Kurds, driving them to seek support from Russia.
- The Kurds are a significant ally in the Middle East, not a country but an ethnic group who have been betrayed and sold out openly.
- The U.S. military moved a small number of troops, enabling Turkey's invasion of Syria, leading to Russian and Syrian troops filling the power vacuum left by U.S. forces.
- ISIS could potentially grow stronger due to recent events, posing a threat that might lead to U.S. military intervention in the future.
- The term "special forces" refers specifically to Green Berets, who train indigenous forces and stand out as instructors in hostile environments.
- The Department of Defense is seen as a private military contractor, with the U.S. military viewed as serving Saudi Arabia's interests in the Middle East.
- War is depicted as a profitable and vicious enterprise that exposes soldiers to being bought and sold for money and power.
- The current situation in the Middle East is characterized by waste, with slogans like "End the war" and "Anti-imperialism" failing to match the reality of the conflict.
- The speaker calls for a reevaluation of how indigenous forces are used and discarded for political convenience.

### Quotes

- "War is just a continuation of politics by other means, right? It has always been like this."
- "The Department of Defense has become the world's largest private military contractor."
- "The only thing that's happening over there right now is waste."
- "The American people. I got a feeling we're going to have to look at what we've done."
- "We are not going to use and abuse indigenous forces like this and to just hang them out to dry when it's politically expedient."

### Oneliner

The President's decision to allow Turkey's invasion of Syria and abandon Kurdish allies reveals the harsh realities of war and foreign policy.

### Audience

American citizens

### On-the-ground actions from transcript

- Support organizations advocating for the protection and rights of indigenous forces (implied)
- Advocate for a more transparent and accountable foreign policy that prioritizes ethical considerations (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the consequences of political decisions on the ground and calls for a reevaluation of how military forces are deployed and utilized.


## Transcript
This will go down as the worst foreign policy decision in American history.
The President of the United States has greenlit an incursion by Turkey into Syria.
He ordered our troops out of the way and
greenlit an attack on allies who have fought and
died beside American soldiers for 15 years.
That started with air operations, followed shortly by ground operations,
with Turkish-backed militants committing atrocities on the side of the road.
The Kurds are our greatest ally in the Middle East.
The Kurds, they're not even a country, it's an ethnic group.
They don't have a country, these are volunteers.
He sold them out, he betrayed them openly and publicly.
In the past, we have sold them out, but
We did it tactfully, we just didn't help, or didn't help much, or undermined them via
economic methods.
This time, we're actively encouraging and cheering on the invasion of their homes.
When his base, Duda Filipe, defended him, he had to do it because Turkey was going to
attack. Yeah, I mean it sounds good, but the reality is when they say that, they
know. Turkey made the US military move? Come on, it's the US military. They don't have to
move for anybody. Certainly not Turkey.
We're seeing the fallout when the President of the United States moved a
a small number of US forces out of the way to a different location in Syria, so
Turkey could invade.
How it started, that started with air operations, followed shortly by ground
operations with Turkish backed militants committing atrocities on the side of the
road.
This drove the Kurds firmly into the arms of Russia, who was waiting with open
arms, because they are the most powerful non-state actor in the Middle East, they
will remain in Russia's arms for decades to come, Russian and Syrian troops quickly
advanced to fill that power vacuum into what positions that were held by U.S.
forces, happened so quickly, it's almost like they knew it was coming, and when
When those Turkish troops pour across that border, the Kurds are going to need more troops
because a lot of theirs are tied up doing silly things right now like guarding suspected
IS fighters.
But when those facilities become undermanned, the likelihood of jailbreak increases exponentially.
And they'll have help from the outside because contrary to what the President has said, that
side is not defeated.
They're still out there.
But hey, once they're back out, that'll give good, clear pretext for the U.S. to go
back in, in force.
IS staged a series of jailbreaks to get their comrades out, and they are probably now stronger
than when President Trump took office.
But hey, we got slogans, we got campaign talking points, ending the war.
We ended the war by expanding its scope and adding a new stream of fresh combatants.
ended US imperialism because when Turkey and the US got together and decided how best to
carve up a chunk of a third country, that was anti-imperialism.
Wish there was a word for that kind of action.
I'm pretty sure it's imperialism.
Before we get to that, we need to go through some background information.
The term special forces gets misused a lot in the United States.
SEALs are not special forces, Rangers are not special forces, special forces means Green
Berets.
Those are the only people who are special forces.
They teach, they've weaponized education, they are instructors, they train indigenous
and local forces how to stand on their own.
He had some, let's just say he expressed his displeasure with senior command and the
commander in chief's decision.
He referred to it as insanity.
He used the term atrocities and he said they were happening.
These guys do this all over the world in very hostile environments.
What most of us would consider something that falls under that term, it's just part of it.
These are not men known for hyperbole.
That term, and that it's happening, we as the American people need to be ready for that.
Because when the photos or footage shows up, don't look away, because we did it.
We did it because we have allowed our government to get out of control.
We have put too much power in the executive branch.
It would not surprise me to learn that the US Army's most elite unit, at least elements
within it, decided to give a single finger salute to the commander in chief and disregard
his order, especially if they witnessed an atrocity.
not surprise me to find out that they engaged the advancing forces.
And we need to be ready to defend their actions.
So I was sitting with a recently separated veteran.
got out when they heard the news and they got to watch the President of the United States,
the Commander in Chief, brag and boast about accepting payment to deploy US military forces
overseas.
might of the US military, well, it's on the market.
It can be bought.
The lives of US soldiers can be bought and sold.
It doesn't matter.
And the US military is now viewed throughout the Middle East as Saudi Arabia's do-boy.
That's how this reads.
Where the hired help, US servicemen, have been bought and sold for a very, very long
time.
It's just normally not done this cheaply.
There's normally something in it for the American people, something that could be cast as a
benefit to them.
Now this time that's just money.
We're sending them over there because the pay this.
The Department of Defense has become the world's largest private military contractor.
There's been a lot of buzzwords, a lot of slogans thrown around.
Bring him home.
It was never about bringing him home.
It was never about bringing him home.
Well, he's just getting rid of those foreign entanglements.
Yeah, how's that working out for you?
No, he's not.
It was never an objective of his.
It was anti-imperialism.
No, it wasn't.
It was making a hole so Turkey could exercise their imperialist ambitions.
And now the U.S. military has become the hired goon of Saudi Arabia's imperialist ambitions.
But we got talking points, we got slogans.
End of the war.
Anti-imperialism.
good, but none of that's happening.
The only thing that's happening over there right now is waste.
Waste.
Probably gonna be a lot of it.
Maybe we should thank Trump though,
because he certainly pulled the veil back.
Let everybody see what it is,
because it's always been like this.
It's always been about money and power.
War is just a continuation of politics by other means, right?
It has always been like this.
The difference is before it could be cloaked
in the defense of American freedom.
It was harder
to cast U.S. soldiers as mercenaries.
Not anymore.
They're literally being bought.
War is a racket.
Always has been.
It is possibly the oldest,
easily the most profitable,
surely the most vicious.
It's the only one that's international in scope.
And it's the only one profits are reckoned in dollars
and losses in lives.
Pretty unpatriotic statement, right?
Y'all can hold your hate mail.
That was a guy named General Butler.
And before you get too angry, he's
one of the most decorated Marines in history,
received the Medal of Honor twice.
We've got a moment of truth coming.
The American people.
I got a feeling we're going to have to look at what we've done.
And it mirrors what we've done all over the world.
This is probably the time to address it.
This is the time to commit to the idea
that we are not going to use and abuse indigenous forces like this and to just hang them out
to dry when it's politically expedient.
It's just a thought.
Now have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}