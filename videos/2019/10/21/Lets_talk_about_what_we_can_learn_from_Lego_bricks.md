---
title: Let's talk about what we can learn from Lego bricks....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_R33lcUUs4g) |
| Published | 2019/10/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Took a break and visited Legoland with his kids, describing it as an amusement park with Lego-themed massive sculptures like elephants and dragons.
- Compares Legoland sculptures to society and history, where the focus is often on individuals rather than the collective effort of every "brick."
- Talks about how society often directs individuals on what to build, leaving out those who don't conform, and suggests it's time for "bricks" to start thinking for themselves.
- Points out that the power lies in individuals connecting and building together, creating more colorful and inclusive images if they build for themselves.
- Encourages local communities to connect, build their own images, and create something different and useful for all instead of just conforming to existing structures.

### Quotes

- "It might be time for bricks in the local community to start connecting and start building stuff on their own."
- "The power is in the brick."
- "Most bricks have an innate desire to create, rather than destroy."

### Oneliner

Beau at Legoland compares society to Lego sculptures, urging individuals to think for themselves and build a more inclusive community.

### Audience

Local community members

### On-the-ground actions from transcript

- Connect with local community members to start building projects together (implied)
- Encourage creativity and inclusivity in community building efforts (implied)

### Whats missing in summary

The full transcript provides a reflective look at society through the lens of Lego sculptures, encouraging individuals to embrace creativity and collaboration in community building efforts.

### Tags

#CommunityBuilding #Creativity #Inclusivity #Empowerment #LocalCommunity


## Transcript
Well howdy there internet people, it's Bo again.
So, took a couple days off, took my kids down to Legoland.
If you've never been there, it's an amusement park.
It is an amusement park, a normal amusement park,
roller coasters, stuff like that, carousels.
But it's Lego themed.
Throughout the entire park there are these massive sculptures.
There's no other word for it.
Life-size elephants, giraffes, dragons that are three times my size.
Cityscapes with skyscrapers as big as I am.
It's pretty impressive to look at, because they're built out of these.
Little baby bricks, by themselves or nothing, just a little brick.
But when they come together, that image shows up.
sculpture. And in that way it's a lot like society. It's a lot like the way we teach
history. You know, when we teach history we personify. We talk about those people
who are larger than life. Those people whose individual existence somehow
altered the course of history. At least that's the story we tell. Sometimes
that's deserved. More often than not though, it's not the figures, it's the
bricks. It's what makes up every image.
Whether it be a city, country, a culture, it's the brick. It's the individual brick
coming together. Most times these bricks are directed from the outside. They're
told what to build. And when that happens bricks that don't conform, bricks that
aren't part of what the designer wants, well they get left out. Bricks that are
the wrong color bricks that are shaped a little bit differently, function a little in a different
way. Those don't make it. Those get stuffed in a box set off to the side. They don't become
part of the image. They get wasted. I think it might be time for bricks to start thinking
for themselves. Because this is where all the power is. The power is in the brick. One
brick meets another, connects. Two like-minded bricks, well that's the foundation. And then
from there, they can build whatever. I have a feeling if bricks started building images
for themselves. They'd be a lot more colorful. A lot more inclusive in more ways. A lot of
different shapes. Wouldn't be as uniform, but I have a feeling that if Brick started
doing that, people walking by, they'd see it and they'd want to replicate it.
I also think that if bricks started doing that, they'd be a whole lot less likely to
end up stuffed in a box and sold, or sold and then stuffed into a box.
It might be time for bricks in the local community to start connecting and start building stuff
on their own.
Start building their own images because if, you know, you take a brick, you take this
brick and you add it to some giant cityscape, it's not changing anything, it's not altering
the image, it's still what it was before.
bricks just becoming a part of it, being assimilated into it, becoming a cog in it.
But with another brick that's like-minded, you can start to build something different,
something that's useful for all bricks rather than just those watching and designing and
selling.
Think most bricks would be just fine on their own.
Most bricks would cooperate.
have an innate desire to create, rather than destroy.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}