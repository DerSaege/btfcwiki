---
title: Let's talk about steel and Emmett Till....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=x5Iiw39Jbbc) |
| Published | 2019/10/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduction to discussing Emmett Till and his legacy with a focus on a new sign made of steel.
- Emmett Till, a 14-year-old boy, was murdered in 1955 for allegedly whistling at a white woman in Mississippi.
- The woman whose accusation led to Emmett Till's murder later admitted she fabricated parts of the story, and the accused men were acquitted.
- Emmett Till's mother displayed incredible strength by having an open casket funeral for her son, revealing the brutal reality of his murder.
- Photos of Emmett Till's body were published and became a catalyst for the civil rights movement.
- Rosa Parks credited Emmett Till with strengthening her resolve in the fight for civil rights.
- The acquittal of Emmett Till's murderers marked a turning point where the Black community realized they couldn't rely on the courts for justice.
- Emmett Till's legacy continues to inspire, symbolized by a new bulletproof sign made of steel in his honor.
- The significance of the steel sign lies in its resilience, mirroring the unwavering strength of Emmett Till's mother and the enduring impact of Emmett Till's story on American history.

### Quotes

- "His legacy was bulletproof, his mom was still unwavering, unflinching, just like that new sign."
- "It doesn't matter if somebody shoots the sign or spray paints it or takes it. It's there. It's carved in stone and blood in American history."

### Oneliner

Beau delves into the story of Emmett Till, from his tragic murder to his enduring legacy represented by a new bulletproof steel sign.

### Audience

History enthusiasts, social justice advocates

### On-the-ground actions from transcript

- Visit historical sites related to civil rights movements (implied)
- Support organizations working towards racial justice (implied)

### Whats missing in summary

The emotional impact of the brutal murder of Emmett Till and its role in fueling the civil rights movement.

### Tags

#EmmettTill #CivilRights #Legacy #RacialJustice #History


## Transcript
Well howdy there internet people, it's Bo again.
So tonight we're going to talk about Steele
and a 14-year-old boy.
Steele and a 14-year-old boy named Emmett Till.
He's a name that a lot of people will recognize.
And he has had a number of signs over the years.
And he's had to have a number of markers because people keep defacing them, stealing them,
shooting them, whatever.
And people know generally the story, more or less, they know a part of it at least.
I don't know that everybody knows the whole story, so that's what we're going to talk
about.
In the summer of 1955, a 14-year-old boy named Emmett Till was murdered for whistling at
a white woman, engaging in some form of social interaction with a white woman.
That's generally the story that people know.
What I don't think that most people know is that in 2008, the woman that set this chain
of events into motion admitted that she made large parts of it up or that the people who
did it were acquitted in a court of law and then the next year went on to give interviews
talking about what they did because double jeopardy applied and they couldn't be retried.
got away, got away with it. I don't know that most people know that his mother was
a woman of steel, unwavering, unflinching. Afterward, he was returned to Chicago, and
His mother had an open casket.
Keep in mind, yeah, he was the victim of a brutal murder, but he was also in the water
for three days.
In the summer, in Mississippi, it's a pretty horrible sight.
It is a pretty horrible sight.
She said she did it because if the death of my son can mean something to the other unfortunate
people all over the world, then for him to have died a hero would mean more to me than
for him to have just died.
Photographs were taken of that boy, and they were published in magazines all over the country.
And those photos still resolve.
They galvanized people.
It's very hard to remain neutral after seeing something like that.
He was a catalyst for the civil rights movement.
People say that he was the sacrificial lamb for the civil rights movement.
I don't know that people understand how true that statement is.
Because it's not just that he died, and that was the catalyst.
He was a sacrificial lamb, the lamb is without blemish, it's innocent.
Rosa Parks credited him with stealing her resolve.
And although the black community in the United States already knew it, that acquittal became
the moment they knew it.
They knew they were not going to get justice in a courtroom.
They knew they couldn't rely on the courts, so they had to take other action.
And it was the Civil Rights Movement.
Fourteen-year-old boy.
And there are a number of ways you can look at this new sign.
You can look at it that it is horrible, that half a century later, there are still people
who would deface a marker for an innocent teenage boy who was murdered and shoot it.
You can look at it like that, but see I love it when something good, poetic even, comes
out of something bad.
His new sign is made out of AR-500, it's bulletproof, it's steel, literally bulletproof.
Signs should always have been steel, his legacy was.
His legacy was bulletproof, his mom was still unwavering, unflinching, just like that new
new sign. 500 pounds now. Gonna make it real hard to move it and take it. Think
that's very fitting because this legacy, this story, doesn't matter if somebody
shoots the sign or spray paints it or takes it. It's there. It's carved in stone
and blood in American history. Now it's carbon and steel.
Science should always been steel. Anyway, it's just a thought. Y'all have a good
night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}