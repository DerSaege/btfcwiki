---
title: Let's talk about reactions to the Guyger verdict....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=PicasjT5mSE) |
| Published | 2019/10/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Reacting to the verdict of a trial in Dallas where a woman entered the wrong apartment and fatally shot an unarmed person.
- Expresses surprise at the satisfactory verdict regarding the incident.
- Observes a divide among gun owners in their reactions to the case, particularly regarding gun control.
- Criticizes those who advocate for leniency in this case, stating they are not responsible enough to own firearms.
- Emphasizes the indisputable facts of the case: the woman entered someone else's home and killed an unarmed person.
- Argues that confusion or lack of situational awareness does not justify the use of lethal force.
- Points out the disregard for safety protocols and responsibilities in handling a firearm.
- Challenges justifications for leniency based on the perpetrator's background.
- Questions what the reaction might have been if the demographics were reversed in the case.
- Concludes that rights, such as owning a weapon, come with responsibilities.

### Quotes

- "A lack of situational awareness does not provide grounds for lethal force."
- "If she was confused and that disoriented, she shouldn't have been armed."
- "What she says doesn't matter."
- "Rights come with responsibilities."
- "Y'all have me second-guessing my stance on gun control."

### Oneliner

Beau reacts to the Dallas verdict, criticizing leniency advocates in a case where a woman fatally shot an unarmed person in his own home, stressing the responsibility that comes with owning firearms.

### Audience

Gun Owners

### On-the-ground actions from transcript

- Reassess your stance on gun control and gun ownership (exemplified).
- Challenge biases and prejudices, especially in cases of racial implications (exemplified).

### Whats missing in summary

The emotional weight and intensity of Beau's message can be better understood by watching the full video.

### Tags

#DallasVerdict #GunOwnership #Responsibility #Bias #Justice


## Transcript
Well, howdy there, Internet people, it's Bo again.
So tonight we're gonna talk about Dallas.
I did a video on it at the time
when the incident first happened.
I have to say, I'm pretty pleased with the verdict.
I'm pretty pleased with the outcome there.
I'm a little surprised, to be honest.
It's sad, we live in a society where,
when the wheels of justice appear to be spinning
the right direction. It is surprising, but it is what it is. We'll have to wait
till sentencing to see if justice is truly served or not. I want to talk about
the reactions to the verdict, not the ones you're probably thinking about. Yes,
there is a huge racial divide in how people are viewing this. It's not really
what I want to talk about. I want to talk about the divide between gun owners
because I'm watching y'all right now. I've never seen so many gun owners
hand ammunition to gun control advocates in my entire life.
I've never seen it. It boggles the mind.
I'm going to start by saying that if you somehow feel that this deserves a lesser charge or leniency in some way,
you're not responsible enough to own a firearm, and to be honest,
the replies and comments that I've seen, y'all have me second-guessing my stance on gun control.
My stance on gun control.
Y'all make me nervous.
Let's go through the facts real quick.
She walked into someone else's home, pulled her weapon, and shot and killed an unarmed
person.
Those are the facts.
Full stop.
End of story.
That's what happened.
Indisputable.
For people who are very fond of saying stuff like facts over feelings and facts don't care
about your feelings. Y'all are all up in your feelings right now. Well, she was confused.
She was confused. And? So what? A lack of situational awareness does not provide grounds
for lethal force. She was confused. If she was confused and that disoriented, she shouldn't
been armed. Well, she thought she was in the right place, but she wasn't, so she wasn't.
That's providing a very easy defense to any home invader at that point. Well, I thought
this was my house. Providing a very easy defense to anybody who's delusional. Well, I thought
thought the EMS were aliens trying to abduct me.
Doesn't matter.
What you thought does not matter.
The facts matter.
The facts, she had no business being there.
She had no right to be there, period.
Well we have to take into account her background and that warrants a little leniency.
I am taking into account her background.
That makes it worse, not better.
What is rule one before you pull that trigger?
Be certain what you're shooting, right?
Have a real good sight picture, know what you're doing, understand what's happening.
That's rule one.
She didn't follow that.
If she had, we wouldn't be having this conversation because he'd still be alive.
Well she said, let me see your hands.
First, we don't know that.
And second, he's under absolutely no obligation to comply with somebody who just walks into
his home and pulls a weapon.
He has no obligation to do that.
And as a police officer, she should understand what auditory exclusion is.
So if she walked in, surprised him, she should understand that it's going to take a moment
to register what's happening because adrenaline's pumping.
He may not have even heard her say that if she did.
Well it was an accident.
No, no it wasn't.
It was not an accident.
It was not.
She intended to pull her weapon.
She intended to pull the trigger.
An accident is when you are shooting at the home invader after exercising due care and
around goes wide and hits a civilian. That's an accident. This wasn't an
accident. It wasn't premeditated. Why'd she pull her weapon? Intent to use lethal
force in a place she had no business being. None. Doesn't matter that she
She thought she was in the right spot.
What she says doesn't matter.
If you can look at this case and somehow find a way to say, well, it needs a lesser charge.
We need to show leniency.
You need to turn in your weapons.
You need to sell your firearms.
You're not responsible enough to own a weapon.
not. You are providing just cases of ammo to gun control advocates because you are making
it very clear that firearm owners are irresponsible, that they can justify walking into someone's
home and shooting an unarmed person.
Mike makes right I guess, yeah, no, there's no mitigating factors here.
She was tired, doesn't matter, doesn't matter.
If you're that disoriented, you shouldn't have a weapon on you and if you're not responsible
enough to know that you shouldn't own a weapon and you certainly shouldn't be in law enforcement.
And here's where that other component comes in.
What if the demographic information was switched?
What if a black guy had walked into a white woman's apartment, gunned her down while she
She was unarmed and then said, oh, I'm sorry, I was tired.
What would you be saying then?
This is a moment when you may really want to look at your biases.
There's a lot of people out there who truly don't believe that they have any biases whatsoever,
that they're not racist, that they're colorblind, whatever.
In cases like this it comes out.
Sometimes you may not even recognize your own biases, but if you can justify this, I'm
willing to bet there's one at play.
Because no responsible gun owner is going to look at this and say, oh yeah, slap on
the wrist, can happen to anybody.
If you think this could happen to anybody, you don't need a weapon.
not responsible enough for it.
Rights come with responsibilities.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}