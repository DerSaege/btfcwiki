---
title: Let's talk about engaging the youth and survival....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=5pBXMov6lNU) |
| Published | 2019/10/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Techniques to teach youth survival skills can be applied to any topic, not just survival.
- Outward Bound is a good option for training if you have the budget.
- Instructors who teach survival skills well go beyond practical skills to include critical thinking and problem-solving.
- Many instructors make a lot of money by training corporate executives.
- Beau encourages individuals to take on the role of the instructor themselves.
- Learning survival skills teaches critical thinking, problem-solving, improvisation, and self-reliance.
- Beau advises against trying to toughen up kids physically; mental toughness develops naturally.
- He recommends reading the U.S. Army Survival Manual as a starting point.
- Beau suggests making learning fun by not fighting popular culture and incorporating the youth's interests.
- Teaching survival skills through activities like escape rooms, chemistry sets, fishing, and camping can be engaging.
- Spending quality time with family while teaching these skills is a valuable aspect of the process.

### Quotes

- "Teach them the skills. Make it fun."
- "If you can survive a zombie apocalypse, you can certainly survive a hurricane."
- "Just make it fun and for God's sakes do not try to toughen up your kid."

### Oneliner

Teach survival skills to youth by making it engaging, fun, and tailored to their interests, promoting critical thinking and problem-solving while spending quality time together as a family.

### Audience

Parents, educators, mentors

### On-the-ground actions from transcript

- Read and familiarize yourself with the U.S. Army Survival Manual (suggested).
- Incorporate survival skill activities into family time like camping, fishing, or learning chemistry sets (exemplified).
- Organize hip-pocket classes and activities for hands-on learning (exemplified).
- Use escape rooms or paper and pencil puzzles to develop problem-solving skills (exemplified).

### Whats missing in summary

The full transcript provides detailed insights into engaging youth in learning survival skills through interactive activities and fostering mental toughness in a fun and relatable way.


## Transcript
Well, howdy there, internet people, it's Bo again.
First, let me start off by saying if you are not
particularly interested in the specific topic,
understand that the techniques I'm gonna talk about
to use to engage the youth in learning something
can be used for any topic, not necessarily,
doesn't have to be this one.
Now, we got a question via the podcast,
and the question is basically,
how do you engage the youth in learning survival skills?
And where would I recommend going for training?
If you have a boatload of cash and you don't want to do it
yourself, Outward Bound.
Outward Bound is really good.
And they teach it the way I think it should be taught.
That's the problem you're going to find with most really
good instructors who teach it well and teach it beyond just
learning the practical skills.
There's a whole other side to it.
The thing is, those instructors can make a ton of cash taking corporate executives out
and doing that.
So you have to compete with that price point to get them to do it.
I know a couple of guys that make an obscene living, basically babysitting executives walking
up and down the Appalachian Trail.
So aside from those who's going to be the best instructor, you, you will, you will.
You will do as good a job as them and better than the bargain basement guys.
Just trust me.
Now for those who don't think that this is something their kids need to learn, what are
the side effects of learning survival skills?
You'll learn to critically think, problem solve, think on your feet, improvisation, self-reliance.
These are the side effects.
It's worth doing.
Even if you think that nothing bad will ever happen, it's worth doing.
Okay, so we're going to start off with the don'ts, and there's just one.
There's just one that I see commonly that people do.
Don't try to toughen up your kid.
Don't, just don't.
That was a common thing in the 80s and 90s.
You know, you wanted to toughen the kid up.
Don't do that.
Teach them the skills.
Make it fun.
If they have to suffer, they just have to suffer.
In a survival suit, you don't have
to practice how to suffer.
If you're in that situation, it's going to happen.
You don't have to toughen them up, not physically.
Mentally, it happens along the way.
You don't have to go out of your way to make this hard on them.
So what's the first thing you should do?
Get a copy of the U.S. Army Survival Manual and read it.
You can buy it at any local bookstore.
It's in our Amazon store.
If you just want to download it, you can get it for free at archive.org.
I would suggest getting a hard copy and keeping it just in case, but you can get it in print
it out from archive.org. Okay, so you want to make it fun. How are you going to do that? You can't
even get them engaged. Don't fight popular culture. Don't fight popular culture. Don't fight the things
they want to do. I've mentioned before, I use zombies as a teaching tool for survival.
The reason is, aside from it being popular and the skill sets being the same, if you
can survive a zombie apocalypse, you can certainly survive a hurricane.
The metaphor is there.
People who aren't trained, that don't have a good idea of what to do in an emergency,
what do they do?
They follow the mob, they just try to satisfy basic instincts, they don't really have a
plan.
I mean, it's a good metaphor, it's there.
But let's say that doesn't resonate with whatever youth you're trying to impart this to.
Find something they're interested in that does.
You know, your kid sits around and plays video games all day.
Don't yell at him to get off and go outside.
What are you playing?
Fallout?
You want to learn how to do that for real?
with their interests rather than just trying to hand them a new one. Just find
some way to relate it. That's all you have to do. And then it makes it a
little bit interesting and then it's a little less of a problem getting them
involved. And then from there, you've read the book, so come up with hip-pocket
classes, little activities that can be done at the drop of a hat. Organize them
ahead of time little things that you can do to teach them little bits and pieces
of it at a time so it's not a classroom setting it's them doing something with
with you a good example is you're out you're away from your home right now
Where can you find food, water, fire, shelter, a knife, and a first aid kit?
And then they have to think of it. That's important because it helps them think on
their feet. It increases situational awareness. It makes them get to know the
area. And those are all positive things. Those are all positive things. There are
Or little chemistry sets, stem sets, where you actually learn to build something.
You build a battery or you start a fire with a chemical reaction or whatever.
Those little things, they're cheap, they're like five bucks.
Get them, do that one.
Use that to teach something else along the way.
hip pocket classes, little activities that can be done in downtime.
Escape rooms are good for helping develop that mindset of seeing that things aren't
always what they are.
You can make it something else.
Those can get kind of pricey going and participating in those little rooms.
They have paper and pencil ones that you can pick up at big box stores for like $5-$10.
Other than that, wholesome activities, you know, take them fishing, camping.
Next time you're going to barbecue, yeah we're going to barbecue but we're going to do it
over an open flame, not in a grill, we're going to do it over a campfire, which they
built the fire, they've now learned how to do that.
How are you going to cook an egg?
Heat a flat rock if you're wondering.
That again, that alone, what are you doing?
You're doing something that doesn't happen in this day and age anymore.
You're spending time together as a family.
So the best instructor is going to be you because you're going to understand them.
You're going to be able to relate to them on a much better level than some random person.
Those instructors that are, that have figured out that that's an important part of it, they're
ridiculously expensive.
More importantly, this is something that could be fun.
That quality time, that ever elusive quality time that you don't get to spend with your
family anymore. There it is. Just make it fun and for God's sakes do not try to
toughen up your kid. They don't need to be tough physically. They need to be tough
mentally and they'll figure that out along the way. Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}