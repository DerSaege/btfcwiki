---
title: Let's talk about the Biden-Ukraine timeline and journalism 101....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Q4XvIM046po) |
| Published | 2019/10/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the topic of the Biden-Ukraine event and the importance of journalism in the narrative.
- Talks about disclosure in journalism and the basic principles of Journalism 101.
- Explains the relevance of laying out a timeline in investigative journalism.
- Provides a detailed timeline of events related to the Biden-Ukraine issue, starting from 2012.
- Addresses the misconception surrounding the investigation into Biden's son and the energy company.
- Mentions the concerns about corruption in Ukraine and the role of various Western powers in addressing it.
- Describes Vice President Biden's involvement in dealing with corruption issues in Ukraine.
- Points out the bipartisan effort within the United States to address corruption in Ukraine.
- Talks about the change in prosecutors in Ukraine in 2016 and the reopening of the investigation.
- Emphasizes the importance of evidence in journalism and refutes claims without substantial proof.
- Concludes by stressing the need for physical evidence to support any narrative changes.

### Quotes

- "If you are attempting to pass something off as objective and you have a bias, you should disclose it to your readers or your viewers, that is Journalism 101."
- "You need evidence, physical evidence, call logs, emails, something."
- "Unless there's physical evidence that you can present, there's no story here."
- "You need evidence to change this. And it might be out there."
- "But you need evidence, not any window."

### Oneliner

Beau breaks down the Biden-Ukraine timeline, debunks misconceptions, and stresses the need for concrete evidence in journalism.

### Audience

Journalism students, truth-seekers

### On-the-ground actions from transcript

- Fact-check information shared on social media platforms (implied)
- Support investigative journalism by subscribing to reputable sources (implied)

### Whats missing in summary

Full context and detailed insights on the Biden-Ukraine timeline can be better understood by watching the full transcript.

### Tags

#Biden-Ukraine #Journalism #FactChecking #Corruption #Evidence


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're gonna go down the rabbit hole
into the wonderful world of journalism.
In the process, you are going to get a complete timeline
of the Biden-Ukraine event.
Some of you are going to get to see me
like you have never seen me before.
I've made videos like this in the past.
They have normally stayed within the journalist community, been accused of pushing other journalists
under the bus when I make these videos.
Some of you need to step back from the curb.
Okay, I need to start off by saying, I don't think that Biden is the strongest choice for
the Democratic party.
That's what's called a disclosure notice.
If you are attempting to pass something off as objective
and you have a bias, you should disclose it to your readers
or your viewers, that is Journalism 101.
What is also Journalism 101 is what's in your deck,
for those that have forgotten, that's that first paragraph,
that first little short thing, answer some questions, who,
what, when, where, and why, right?
Your headline often takes care of the who and the what.
Bo lost it.
That would be a good one for this.
The date line will take care of the when sometimes,
but if you're doing investigative journalism,
you need to be more clear
because you're talking about stuff in the past.
That's why you often have articles that start off
with something like late Thursday night,
Bo lost it, releasing a video on the internet
because he feels that journalists
abandoned long standing practices. One sentence, who, what, when, where, and why.
So with the Biden-Ukraine thing, let's just jump straight in to when. Let's go there. Most people
know the where, Ukraine, and the who, and the what, kind of, maybe. Let's go to the when,
Because once you lay this out, and if you were going to write an article about this, the timeline is really important.
You'll learn some things when you lay out the timeline.
When did the investigation into this energy company start?
2012. 2012.
When did Biden's son get his do-nothing job with the board of directors of this energy company?
April of 2014, two pieces of the timeline in place, and all of a sudden that narrative
is getting real shaky.
The investigation into Biden's son started two years before he got there?
That's crazy talk.
Maybe it's because it wasn't an investigation into Biden's son, it was an investigation
into the oligarch that owned the company.
He just happened to sit on the board of directors.
At the time they were trying to put together this star-studded board of directors and in
cases like that, a lot of companies do it.
Those people have nothing to do with the daily operations of the company, which means they
wouldn't be implicated in anything, which means Vice President Biden would have no reason
to stop an investigation.
But let's continue because there's some other things.
In 2015, Shokin, this is the guy that gets fired, he becomes Prosecutor General.
And he inherited that investigation.
And it remained dormant.
Nothing was being done on it.
Just sat there.
Nothing happened.
The West in 2015 becomes concerned about corruption in Ukraine, a lot of it focusing on this Prosecutor
General.
not just Biden, it's not just the Obama administration, it's not just the US, it's most European
powers, the IMF, the US, basically every major Western power, because remember that whole
Euromaidan thing went on.
We're trying to cement ties, context, something you should include for your readers.
So all of that's going on and they're worried about corruption.
This prosecutor is enabling corruption.
It's the way the West sees it.
So they want him gone.
Everybody's making appeals to get rid of him.
Biden, in December of 2015,
shows up and he becomes the hammer.
Okay?
And he's the one that finally gets him gone.
Now, there's that footage, that clip of him.
That's locker room talk.
That's not actually how it went down.
But you know what?
Let's say that it is.
Okay?
Let's just roll with it.
He said it, he can own it.
The reality that goes along with his gunboat diplomacy is that it was a bipartisan effort
within the United States to get rid of this guy, to deal with the corruption in Ukraine.
Don't believe me?
Google something called the Senate-Ukraine Caucus.
You're going to find a whole bunch of Republican senators who signed off on this.
was a tool of foreign policy at this point. That's what happened. Now, March
2016, that general prosecutor, he gets canned. He's gone. A new prosecutor is
appointed. Do you know what he does with that dormant investigation? That thing
that nothing had been happening on? He reopens it. Biden switching
prosecutors reopened the investigation into the company that his son was sitting on the
board of directors of.
This narrative doesn't make a whole lot of sense, guys.
That seems a little odd, seems a little odd.
And given the dismissed prosecutor's propensity for corruption, it would have made a whole
lot more sense if Biden just wanted to pay him off, right?
rather than get rid of them, risk getting a new one, who might reopen the investigation.
It's almost like Biden, Vice President Biden, understood that as a member of a board of
directors of this type, there's no way you'd be implicated.
And guess what the new prosecutor found out?
Nothing on Hunter Biden, nothing.
These are the established facts.
This is the established story.
These are the facts as we know it.
Does that mean that's where the story has to end and it always has to be this?
Of course not.
You know the first tagline at the fifth column was we write the first draft of history because
it's expected to change as more evidence comes out.
But if you want to tackle an established narrative of this sort after this long, do you know
what you need?
Evidence, physical evidence, call logs, emails, something.
Citing an anonymous source off of freedomeagle.ru is not journalism.
It's not evidence.
It's innuendo, it's propaganda.
It's something that requires a disclosure notice because you haven't
been able to set your biases aside.
And yes, I am aware that this prosecutor who was dismissed,
he said that he has evidence on Hunter Biden,
and that that's the reason he got fired.
But he hasn't released it, huh?
All this time.
In all this time, he hasn't released it.
That's crazy. It's crazy.
It's almost like he's just saying that
because Hunter Biden's daddy got him fired.
Seems like motive to me.
What do I know?
I don't know.
There's a whole lot of people right now going,
well, why hasn't the news talked about this?
If this is true?
Well, the news did talk about it.
Where do you think the footage of Biden came from?
I know that conservatives like to hold on to the past.
The news doesn't.
The news focuses on current events.
and they'll go back if they have to address something.
The problem is now
the news is having to spend a whole lot of time
going back to clear up
garbage reporting.
And that's what this is.
Unless there's physical evidence that you can present,
there's no story here.
These are the established facts. This is what we know.
You need evidence to change this.
And it might be out there.
I personally think that leasing out your name
to a board of directors, I think that's shady.
I would say that it's probably unethical.
I think it's something that probably should be illegal,
but it's not.
But given the fact that he did something I consider shady,
maybe he did do something illegal.
But you need evidence, not any window.
Anyway, so there's a timeline.
And there's a little crash course in how journalism
is supposed to work.
And for those of you who wonder why I have stopped writing as
much as I used to, I'm tired of cleaning up after you.
Anyway, it's just a thought.
Y'all have a good night.
I want to show under a bus, I guess.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}