---
title: Let's talk about climate change cuisine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=1QkrOiTWpeY) |
| Published | 2019/10/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- AOC held a town hall where unfiltered questions were asked, a departure from the norm of politicians listening only to those who pay them.
- During the town hall, a woman suggested eating babies to save the climate, sparking wild reactions from Republicans.
- The woman was later revealed to be a La Roche PAC operative, not a genuine proposal.
- Republicans caring about human life after birth due to this incident was noted.
- President Trump joined in calling AOC names and spreading fake news, without verifying facts.
- AOC responded with composure and hope, focusing on the goal of reaching net zero emissions.
- Beau criticizes those who panic about climate change to the point of advocating depopulation as a solution.
- He points out the flawed logic of prioritizing population control over consumption reduction, calling it evil and racist.
- Beau recommends a video by YouTuber Mexi for further insight into the racial undertones of this issue.
- Despite the prevalence of fake news, Beau ends on a note of hope and encourages further reflection on the topic.

### Quotes

- "There's always hope."
- "Their solution is eat the babies."
- "If you look at the climate issue and you think the solution is population and not consumption? Well, one, you're evil. Two, you're racist."
- "Fake news has hit the president yet again."
- "y'all have a good night."

### Oneliner

AOC faces absurd suggestion at town hall, revealing Republican reactions, fake news, and deeper racial implications of climate change fears, while Beau advocates for hope and rational solutions.

### Audience

Climate change advocates

### On-the-ground actions from transcript

- Watch the video by YouTuber Mexi to gain further insights into racial undertones related to climate change (suggested).

### Whats missing in summary

The full transcript provides a deeper dive into the intersection of climate change, fake news, political reactions, and racial implications, offering a nuanced perspective on these complex issues.

### Tags

#ClimateChange #AOC #Republicans #FakeNews #Hope


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're going to talk about climate change.
So AOC, for those overseas, she's a representative here, up and comer.
Those are her initials.
She held a town hall, and this town hall obviously was not pre-screening questions that were
given to her, which is nice.
It shows that she's actually concerned about what the people think, because normally American
politicians only listen to those people who pay them. So that is a nice change.
We know that the questions weren't pre-screened because a woman stood up and asked if it was
time to eat the babies to save climate. And Republicans went wild. Look, look, look how
evil they are. They want to eat the babies. They're crazy, they're insane, they're stupid,
evil. Just look at it all. Look at it. It should be noted that even the President of
the United States got in on the act calling AOC a wacko and tweeting footage of it as
if she's responsible for everything that her supporters do. There's probably some people
in Texas that have questions for you, Mr. President.
Now, of course, this is made up, it's not true.
The person was a La Roche PAC operative, they've claimed credit for it.
But I do find it funny because it was marks the first time that I'm aware of that Republicans
en masse have cared about a little bit of human after they were born.
That's also a nice change.
But it reveals something.
Number one, the president has yet again bought into fake news designed to support him and
believed it.
So either he's dumb or he's intentionally lying to his own supporters and knows this
to be false.
On second glance, it should be real easy to tell it's false.
She's actually wearing a t-shirt that says something like, save the climate, eat the
babies.
people in crisis don't have t-shirts made that's what AOC for her part that's
what she thought was going on she thought she was witnessing a an episode
from someone and it was interesting because her response was you know we
have time we have more than a few months we you know all we have to do is get to
net zero by this point, there's always hope.
And handled it with more decorum than I can imagine any other politician dealing with
it.
But it's interesting.
There's always hope.
These are the climate change alarmists, right?
But from the other side, those that are so afraid of climate change that they've induced
panic in themselves, the inability to self-help.
And they just refuse to address the issue because they're so scared of it, because
they're so terrified of it.
They don't want to look into it because of the doomsday scenarios they've been told.
Their solution is depopulation.
Their solution is eat the babies.
What it's really saying, that's what they think it's going to come to deep down.
It's interesting because if you look at the climate issue and you think the solution is
population and not consumption? Well, one, you're evil. Two, you're racist. I know that
sounds crazy, right? I was going to do a video on this, but in my research I ran across one
by a YouTuber named Mexi, and I'm going to put the link to that down below. Did a really
good job with it. I actually can't add anything to it, so there's no reason to reinvent the
will on that. I might have presented it a little different, but the facts are all
there. The information is there. And if you still have trouble believing that
there might be racial undertones to this, just go ahead and look into the LaRoche
Pax founder a little bit. It might tell you a lot. And then aside from that, I
would just like to point out, fake news has hit the president yet again, but
But there is always hope.
Anyway it's just a thought.
y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}