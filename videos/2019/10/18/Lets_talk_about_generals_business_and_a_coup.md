---
title: Let's talk about generals, business, and a coup....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_cd9317BgKM) |
| Published | 2019/10/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau dives into the story of General Butler, who was a highly respected Marine with extensive experience in various wars.
- FDR's progressive reforms didn't sit well with the wealthy business class, leading them to plan a coup to install a fascist government.
- Wealthy and powerful figures like J.P. Morgan and Prescott Bush were implicated in the coup plan.
- McGuire acted as an intermediary, gradually escalating the plan to have Butler lead a march on Washington and seize control.
- Butler, being a staunch anti-capitalist, was the wrong choice for the coup leaders.
- Butler exposed the plan by going straight to Congress, but newspapers dismissed it as a hoax initially.
- Even though the truth came out later, no one was prosecuted for the coup attempt.
- The coup attempt serves as a reminder of the dangers of political power plays and the importance of individuals' choices.
- If Butler had agreed to lead the coup, the course of history could have been drastically altered.
- The story underscores the significance of one person's decision in shaping the world's future.

### Quotes

- "One guy refused to take command. One guy refused. One person. Literally changed the entire course of human history."
- "Assuming that the allegations are true, the world owes General Butler the entire world."

### Oneliner

General Butler's refusal to lead a coup against FDR prevented a potential fascist dictatorship in the U.S., showcasing the immense impact of individual choices on history.

### Audience

History enthusiasts, advocates for democracy.

### On-the-ground actions from transcript

- Support and defend democratic institutions (exemplified).
- Stay vigilant against threats to democracy (exemplified).

### Whats missing in summary

The full transcript provides a detailed account of General Butler's role in foiling a coup attempt, shedding light on the fragility of democracy and the power of individual decisions.

### Tags

#GeneralButler #CoupAttempt #Democracy #Fascism #IndividualChoice


## Transcript
Well, howdy there, internet people, it's Beau again.
So in a recent video, I used some quotes from a guy named
General Butler, the war is a racket quotes.
And I kind of off-handedly mentioned
that he had stopped a coup, and man, I
got a lot of questions about that,
so we're going to talk about it tonight.
But before we get into it, we need
to know who General Butler was.
So this was one squared away Marine.
As I mentioned in that video, he received the Medal of Honor twice, Order of the Black
Star.
He was something else.
He fought in the Spanish-American War, the Philippine-American War, the Boxer Rebellion,
the Banana Wars, the Mexican Revolution, and World War I.
Very experienced gentleman.
If you were looking for somebody to lead a coup, he'd probably be at the top of your
list, but very respected, especially since he had already kind of backed a march on Washington
by veterans, the bonus army.
He would seem like the ideal candidate.
So what happened, according to the story, is FDR gets elected and he starts enacting
very progressive reforms.
And the business class, the wealthy, wealthy, wealthy business class, well they didn't
like it.
And they were looking to Europe at how things had shaped up there with veterans or groups
kind of taking control of the government and installing a different form of government.
So they, according to the story, kind of hatched a plan.
And when I say wealthy businessmen, I'm talking about very, very powerful people, men that
even though this happened in the 30s, you'd still recognize the names.
J.P. Morgan, Prescott Bush, that's dad and granddad to the presidents, the DuPont family,
talking about extremely powerful people.
Now as the story goes, as outlined by Butler, a guy named McGuire was kind of the intermediary.
And he approached Butler.
at first, it was pretty innocent, hey, we want you to, you know, kind of lead the American Legion.
And then, hey, why don't you kind of draft a resolution saying we need to go to the gold
standards day on that. And then eventually it turned into, oh, we want you to lead a
march on Washington of veterans and seize control of the government from FDR and keep him as a
figurehead, but basically you'd be a dictator. We want to install a fascist government.
That's the story.
The problem is, although he seemed like the ideal candidate,
he was a staunch anti-capitalist. He was really the wrong choice for this.
So, upon hearing of the plan, he goes straight to Congress with it.
Now, when this story broke at the time, the newspapers were immediately like,
this is a giant hoax, it never happened, ha, crazy old general.
But keep in mind, the people implicated were very powerful people.
people. The House of Representatives convened hearings, and it was kept secret.
The findings were kept secret until the 21st century. And then when they were
released, everybody was kind of like, wow, that's crazy, because they
were able to confirm pretty much everything that Butler said, and nobody
was ever prosecuted for it. The idea, the way it was framed, was, yeah, okay, so they
talked about it and they planned it and maybe they took a few overt steps, but they didn't
really try it, is kind of how it was framed. And at the end of the day, it's definitely
something we need to remember, it's something that we need to keep in mind, because assuming
that the allegations are true and there are some people who still to this day believe
that they aren't, that it was just Butler exaggerating or being so much of an anti-capitalist
that he was trying to show the dangers of it.
But it's definitely something we need to keep in mind because at the end of the day,
It boils down to one poor recruitment decision.
Had they approached just about any other general at that time, the economies and shambles,
veterans were not treated well upon the return from World War I.
They probably would have got what they wanted.
And then can you imagine that?
The dominoes that would fall would change everything we know.
Because if this had gone down and the U.S. was a fascist dictatorship at the start of
World War II, the outcome probably would have been different.
So if it's true, the world owes General Butler the entire world.
Because one guy refused to take command.
One guy refused.
One person.
Literally changed the entire course of human history.
Or maybe he's just a crazy old general.
Anyway, it's just a thought.
y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}