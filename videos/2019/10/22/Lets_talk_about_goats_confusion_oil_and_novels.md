---
title: Let's talk about goats, confusion, oil, and novels....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=pKCurn1bO94) |
| Published | 2019/10/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- US troops may stay in Syria to protect oil fields, contradicting arguments of supporters.
- Troops leaving Syria are just moving to Western Iraq, waiting to go back.
- Trump is perceived as anti-interventionist but allows US troops to protect natural resources in Syria.
- Despite being labeled a peace lover, Trump threatens war with Iran.
- Critics find Trump's foreign policy unpredictable, but Beau argues it is completely predictable.
- Betraying allies and inability to recruit indigenous forces overseas have negative effects.
- US actions benefit non-state actors and strengthen certain groups in Syria.
- US decisions have made it harder to gain trust and support from other countries.
- Russia can use US actions as propaganda against other nations.
- Turkey's meeting with Russia shows a shift in power dynamics in the Middle East.
- Beau questions whether Trump is incompetent or a useful tool in shaping foreign policy.

### Quotes

- "It is completely predictable if you accept the one assessment all of US intelligence agreed upon from the very beginning."
- "Either he's a complete idiot or he's a useful one."
- "We're living in a Tom Clancy novel and the President of the United States is compromised."

### Oneliner

US foreign policy in Syria under Trump: predictable chaos or calculated moves?

### Audience

Foreign policy analysts

### On-the-ground actions from transcript

- Question US foreign policy decisions (implied)
- Stay informed on geopolitical shifts and implications (implied)

### Whats missing in summary

Deeper analysis on the geopolitical implications and potential long-term consequences.

### Tags

#US #ForeignPolicy #Syria #Trump #Geopolitics


## Transcript
Howdy internet people, it's Deep Goat again.
So, looks like US troops may stay in Syria to protect the oil fields.
Kind of throws the arguments of his supporters into shambles, doesn't it?
And bring them home, they said, but they're not coming home.
Even those that are leaving Syria, they're just going right across the border into Western
Iraq to wait because they got to go back and they know it.
He didn't want to participate in an illegal occupation.
I guess not because he's anti-interventionist.
imperialist and anti-colonialist Donald Trump the great leftist right yeah he's
an anti-imperialist as long as US troops can stay and protect the natural
resources that the US wants to extract from the country pretty sure there's a
term for that. He's just a peace lover as he threatens war with Iran again. All of
this it has the pundits on TV, the political analysts, it has them all just
going crazy saying that it's ad hoc, it's helter-skelter, it's confusing, his foreign
policy. It's just unpredictable. It's completely predictable. Look at the effects. Look at the
effects. The largest non-state actor was driven away. Who does that benefit? We publicly and
shamefully betrayed our allies in their time of need. Who does that benefit? Makes it almost
impossible for us to recruit indigenous forces overseas.
Who does that benefit?
Creates propaganda to use against the United States in relation to state actors so Russia
can tell Greece, hey, better be careful, look what the U.S. will do to you.
Who does that benefit?
the entire U.S. position overseas, strengthens one group of bad guys inside Syria, giving
the U.S. a black eye.
They couldn't even defeat this rabble.
In fact, their decisions led to them being strengthened.
that benefit when they move back into other countries? Do you think those countries are
going to look to the U.S. again for help and therefore advance U.S. interests? Or do you
think they're going to look to another country? And then it turned the only other superpower
are present into the power broker of the Middle East.
Turkey's meeting with Russia.
No, this is not unpredictable.
It is not helter-skelter.
It is not confusing.
It is not ad hoc.
It is completely predictable if you accept the one
assessment all of US intelligence
agreed upon from the very beginning.
So your choices are, well, either the U.S. president is so utterly incompetent that he
can destroy U.S. foreign policy with a single decision and be so disorganized and uncommunicative
the Pentagon that they're terrified now and they're drawing up random plans for contingencies
in case the president just loses it again.
So either he's a complete idiot or he's a useful one.
Either he's just really that incompetent or we're living in a Tom Clancy novel and
the President of the United States is compromised.
Anyway it's just a thought.
have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}