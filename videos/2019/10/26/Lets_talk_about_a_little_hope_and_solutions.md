---
title: Let's talk about a little hope and solutions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=uhzuYC5M6yo) |
| Published | 2019/10/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing solutions and how to reach them.
- Noting the prevalence of negative content online.
- Emphasizing the importance of identifying problems.
- Acknowledging the lack of action despite watching videos.
- Stating the impact of even a few individuals taking action.
- Describing the constant presence of negativity in daily life.
- Pointing out issues like environmental degradation and corruption.
- Challenging the belief that the next leader will solve everything.
- Calling for leadership over rulership.
- Asserting that each person has the capacity to lead and solve problems.
- Expressing gratitude for Patreon support.
- Planning to expand the podcast with diverse guest speakers.
- Emphasizing the need for collaboration and unity for real change.

### Quotes

- "We have everything we need. It's us."
- "It's us, the individuals down here on the bottom, the commoners that are going to solve it."

### Oneliner

Beau addresses the prevalence of negativity, challenges the reliance on future leaders, and calls for individual action and unity to bring about real change.

### Audience

Community members

### On-the-ground actions from transcript

- Reach out to diverse individuals to join in discussing and seeking solutions (suggested)
- Collaborate with people from various backgrounds to address today's issues (implied)
- Come together with fellow community members to drive real change (implied)

### Whats missing in summary

The full transcript elaborates on the importance of taking action as individuals and working collectively to address societal issues effectively.

### Tags

#Solutions #Leadership #Community #Unity #Change


## Transcript
Well, howdy there, internet people, it's Beau again.
So tonight, we're going to talk about solutions.
At least, how to get to some solutions.
People talk about the videos.
I've noticed a lot lately.
There's just so much doom and gloom in the world.
There is.
First step is identifying a problem.
and understand tens of thousands of people watch that video.
And maybe 10 got an idea.
10 got an original idea, something
they're going to put into action.
That's enough.
That's enough.
We go through our daily lives, and we are beset by doom
and gloom all the time, every day.
Environmental degradation, flashes of conflict in 10-second clips, seeing the brass fly through
the air.
Feel-good stories on the nightly news that if you stop to think about them for even a
split second, it just makes you realize how bad things are.
Corruption at the highest level.
This seems overwhelming, seems like there's nothing we can do.
So we go about our day fooling ourselves.
That next person, that next one, that's going to be the one to fix it.
I have some bad news for you.
Statistically speaking, the next president is probably going to be pretty bad too.
Think about it, how many good Presidents can you name, four, five?
So we've got about a 90% chance that the next President is going to be pretty bad, probably
not as bad as this guy, but probably pretty bad.
It's not really their fault though, they can't really lead from the White House, they can't.
You can rule from there.
Problem is at this point in time, we don't need rulers.
We need leaders.
And every one of us is a leader in our own way.
Every one of us has something we're bringing to the table, a problem we can solve.
We have everything we need.
It's us.
us. I know, sounds crazy. But, thanks to the people on Patreon, I'm going to prove it.
Got the equipment to take the podcast to the level that I want it, which is bringing in
guests and talking, talking. And tonight, I drew up the list of the people I'm going
be reaching out to first. There's a PhD, there's a high school dropout, there's a
radical leftist here in the United States, and a comic book creator, a former adult
actress, a CIA whistleblower, a pastor, a videographer, a yoga instructor, varied
backgrounds, just people talking about today's issues, discussing, looking for
solutions, not debating, hoping to win points, because as
overwhelming as all of this seems, it's us, it's us, the
individuals down here on the bottom, the commoners that are
going to solve it.
We have the machinery for change, for real change, in
United States. All we have to do is come together. It's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}