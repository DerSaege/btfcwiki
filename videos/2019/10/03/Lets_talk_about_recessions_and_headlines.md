---
title: Let's talk about recessions and headlines....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=vimH_tZWCHg) |
| Published | 2019/10/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Participated in a telethon for the Trevor Project.
- Addressed questions about an upcoming recession.
- Contradicts headlines claiming no recession is imminent.
- Cites headlines from 2007-2008 before the last recession.
- Manufacturing report indicates lowest levels since the previous recession.
- Suggests the current administration aims to delay recession until after the election.
- Points out the natural occurrence of recessions after periods of growth.
- Expresses belief that Trump's economic policies contributed to the impending recession.
- Speculates that the government aims to maintain consumer confidence to delay recession.
- Acknowledges not being a trained economist and presents his views as thoughts.

### Quotes

- "They don't want you to think about how they might have tanked the economy."
- "A recession is natural in some ways, a lot of times."
- "It's really that simple."
- "I am not a trained economist."
- "Y'all have a good night."

### Oneliner

Beau contradicts claims of no recession, pointing to historical indicators and political motivations to delay economic downturn for electoral reasons, implying Trump's policies hastened the impending recession.

### Audience

Voters, concerned citizens.

### On-the-ground actions from transcript

- Stay informed about economic indicators and historical patterns (implied).
- Support policies that prioritize long-term economic stability over short-term gains (implied).

### Whats missing in summary

The full transcript provides detailed insights into economic indicators and political motivations behind recession denial, offering a critical perspective on current economic policies and their potential impacts.

### Tags

#Recession #Trump #Economy #PoliticalMotivations #ConsumerConfidence


## Transcript
Well, howdy there, internet people, it's Bo again.
So last night I was on a live stream.
I was kind of a telethon to raise money
for the Trevor Project, and while we were doing it,
we were taking questions from the audience.
A question came in about the coming recession,
the one I see coming, and I answered it.
And then today I got questions from people about it,
saying, hey, you know, the headlines are saying
there's not a recession coming.
Explain yourself.
Fair question, fair enough.
So I'm gonna read some headlines.
I'm going to tell you where they're from, so you know they're not a fly by night
organization, and then we're going to talk about it, um, U S not headed for
recession report that's from Reuters, no recession CNBC, the first line of this
is a federal reserve chairman and treasury secretary, both acknowledged
problems in the U S economy Thursday, but both said they believe the nation
will avoid falling into recession, CNN money boom continues, there is no
recession national review no recession but big challenge NPR that is pretty
straightforward I mean that that that's pretty certain right there truth is I
edited those headlines to remove any reference to the people involved because
those headlines are from 2007 and 2008 late 2007 early 2008 which is right
before the last recession why do these headlines exist the indications are all
there. We are headed to recession. In fact, we just got another one. The
manufacturing report says that we are at our lowest level since right before the
last recession. So how can headlines keep popping up saying that it's not going to
happen? Because just like then, we are at the end of an administration's term. And
what they want to do is delay it. Delay the recession until after the election
so you're not thinking about it when you walk into the voting booth. They don't
want you to think about how they might have tanked the economy.
So they're rubbing the magic lamp, they're making wishes, they're being hopeful, painting
a rosier picture than is actually there, saying things that are kind of untruthful, dishonest
at least.
Now a recession is natural in some ways, a lot of times.
It's a correction that occurs after a lot of growth, which is why they still have figures
to point to to say look we have a lot of growth. Of course you do. That's when
recessions occur. When taken with the other indications that's not saying that
there's not going to be one that's actually another indication that there
will be. And you know I do believe that Trump's economic policies hastened this.
Probably would have happened anyway but he definitely didn't help and they don't
want you thinking about that. And they want to keep consumer confidence high by pointing
to all the good things so you keep spending money so it delays the recession. So it occurs
after their party is already re-elected and stays in power. It's really that simple. There's
not much to it. Now, at the same time, I should point out that I am not a trained economist
And after all, this is just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}