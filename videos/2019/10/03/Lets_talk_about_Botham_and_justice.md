---
title: Let's talk about Botham and justice....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-N2MKhD1GVQ) |
| Published | 2019/10/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the aftermath of a certain event and the feelings surrounding it, particularly disappointment.
- Expressing disappointment at falling short of perceived real justice by a slim margin.
- Noting society's tendency to accept less-than-ideal outcomes as long as they exceed expectations.
- Sharing personal views on prison being rehabilitative, disapproving of mandatory minimums and non-violent crime incarcerations.
- Acknowledging a leaning towards retribution over justice for violent crimes, especially of a certain magnitude.
- Acknowledging racial sentencing disparities and the role of income inequality in access to quality legal representation.
- Speculating that being a police officer may have influenced the leniency of the sentence.
- Mentioning potential challenges for the convicted individual in a prison environment not welcoming to law enforcement.
- Contemplating whether the outcome constitutes justice and deferring to the victim's family for that judgment.
- Acknowledging the family's disappointment and anger while also appreciating their forgiveness and ability to embody compassion.
- Expressing difficulty in comprehending the level of forgiveness displayed by Botham's brother towards the convicted individual.

### Quotes

- "Disappointed. Disappointed."
- "We're conditioned now to accept something that is less than okay, and just be like, well it's not right, but it's better than I was expecting."
- "Sentencing disparity is a very, very real thing."
- "I yield to them."
- "They are better people than I am."

### Oneliner

Beau addresses disappointment in falling short of perceived justice, societal conditioning to accept subpar outcomes, racial sentencing disparities, and the challenging forgiveness displayed by victims' families.

### Audience

Justice seekers

### On-the-ground actions from transcript

- Stand against sentencing disparities by supporting legal aid organizations that work towards fair representation for all (implied).
- Advocate for prison reform that focuses on rehabilitation rather than punishment (implied).
- Show empathy and support for victims' families in their pursuit of justice and healing (implied).

### Whats missing in summary

A deeper exploration of the nuances surrounding forgiveness, justice, and societal conditioning in response to disappointing outcomes.

### Tags

#Justice #SentencingDisparities #PrisonReform #Forgiveness #CommunitySupport


## Transcript
Well howdy there internet people, it's Bo again. So tonight we're going to talk
about both of them. We're going to talk about the sentence. We're going to talk
about Dallas. I had a whole lot of people, more than I would have imagined, asked for
my opinion, asked how I felt about it. And when I was reading the questions I
realized they were asking more than just how I felt about it. There was a lot of
nuance in the questions. So we're gonna answer them all but how did I feel about
it? Disappointed. Disappointed. We're so close to what I would have perceived as
real justice. So close but it fell short and in that the question isn't long
enough. It's funny, because if the sentence had been one year less, I think the tone of
this video would be very different. I think it would be anger rather than disappointment.
And that in and of itself is sad and disappointing. We're conditioned now to accept something
that is less than okay, and just be like, well it's not right, but it's better than
I was expecting.
That's a problem in and of itself.
Then another one of the questions when you're actually reading it is an adequate punishment.
I don't know.
I believe prison should be rehabilitative in nature.
I don't believe in mandatory minimums.
I don't believe in incarceration for non-violent crimes, so I don't know.
I would say that while all of that is true when it comes to violent crimes, especially
those of this magnitude, I tend to lean the other way, and I know that's not right, but
I tend to be looking more for retribution than justice.
So I don't know that I would be a good person to ask.
Would a black person get the same sentence if there was a demographic switch?
Of course not.
Of course not.
I'm fairly certain the people that ask this know this.
I think they just want it said.
Sentencing disparity is a very, very real thing.
It is a very real thing.
I think it has a lot to do with the ability to get a good lawyer, which has to do with
income inequality.
systemic. It's something that's very present. The evidence is everywhere for it. And the
efforts to reduce it have been lacking. There have been attempts, but they haven't been
very good. They haven't been very successful. Do I think she got off light because she's
cop. Probably. Probably. That probably had something to do with it.
If you're looking at it from the punishment aspect, I would take solace in the fact that
those types of institutions where she's heading, they're not very welcoming to law enforcement.
Staying in general population is going to be bad.
The alternative is protective custody, which is basically putting yourself in solitary
confinement, which is also not good.
So if you're looking at it from a punishment aspect, I think that's probably going to be
met.
I would be willing to bet that most people who are familiar with these institutions would
rather do ten years as themselves than five years as a cop.
And then there's the real question, the underlying question.
Was this justice?
I don't know.
That's up to the family.
I yield to them.
I yield to them.
And I am sure there is disappointment and there is anger.
And they are more than entitled to feel that way.
It's not even my family and I feel that way.
But then you have to look at their actions.
You know, the statement of hoping it's time to give her to reflect on her life.
And then Botham's brother saying, you know, I forgive you and I don't even want you to
go to jail.
hug. I can't even fathom that, to be honest. I can't. I can't imagine doing that. He was
trying to embody his brother. And I can't, I cannot picture myself doing that. They are
better people than I am. And you can look at that hug two ways. I do, anyway. There's
the part of me that looks at it and says, you know, we should all be more like both
them. This is what he would have wanted. We should all be more like that. And then
there's a part of me that looks at that and it's just a reminder of the type of
person she took away. And it makes me angry. And I know that's not the intent
But, like I said, they are better people than I am.
It's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}