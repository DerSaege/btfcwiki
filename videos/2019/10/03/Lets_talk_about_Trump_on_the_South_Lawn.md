---
title: Let's talk about Trump on the South Lawn....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=LZ0YVdiV_Ks) |
| Published | 2019/10/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Shares the chain of events in the morning following a poll on impeachment support.
- Trump's response to the poll results by soliciting interference in the election live on TV.
- Beau expresses his belief that Trump's actions are impeachable.
- Mentions Trump being a victim of the fake news media.
- Compares the Biden theory to a fabricated storyline about JFK's assassination involving Jackie Kennedy.
- Criticizes right-wing conspiracy sites for spreading misinformation.
- Talks about Giuliani showing the fabricated story to the president, who acts on it.
- Points out the irony of Trump being brought down by fake news after attacking journalists for the same.
- Beau humorously comments on the situation, suggesting everyone could use a good laugh.

### Quotes

- "45%, those are rookie numbers, we need to pump those numbers up."
- "I guess Trump saw that and was like, you know what, 45%, those are rookie numbers, we need to pump those numbers up."
- "The man that told actual journalists that they were fake news is going to be brought down by faking it."
- "And they say poetic justice is dead. It's not. It's alive and well."
- "You can't write stuff like this."

### Oneliner

Beau breaks down Trump's impeachment support, his response, and the irony of fake news in politics.

### Audience

Political observers

### On-the-ground actions from transcript

- Fact-check and verify information before sharing (implied)

### Whats missing in summary

The full transcript provides a humorous take on the unfolding political events, offering a moment of levity amidst the seriousness of the situation.

### Tags

#Impeachment #FakeNews #PoliticalCommentary #Satire #ElectionInterference


## Transcript
Well howdy there internet people, it's Bo again.
Man, did y'all see that?
So the chain of events this morning,
poll comes out, 45% of Americans support impeachment,
only 38% are opposed.
I guess Trump saw that and was like, you know what,
45%, those are rookie numbers,
we need to pump those numbers up.
So he walked out to the South Lawn
and live on TV solicited for an interference
the election. Not just should Ukraine investigate Biden but China should too. I am not a constitutional
lawyer but I'm fairly certain that's impeachable. I don't think there's any difference between
sending the message live on TV and doing it via phone call. You know, but at the same
time I feel for Trump, I do because he is a victim of the fake news media, and I'm not
joking.
The man who ran around screaming fake news is being brought down by the fake news media.
Dead serious.
Because that theory about Biden, it's not real.
It's a bunch of events pushed together to make it seem like it's a storyline and it's
not. This would be a lot like if I said, you know, I know who killed Kennedy. I do.
Just remember that Jackie knew that JFK had cheated on her with Marilyn, okay?
Just assume that, remember that. Do you know where she was the day he was killed?
She was in Dallas. And do you know that she had armed men around her the whole
time she was there. Now for those that don't know, Jackie was sitting next to
Kennedy when he was shot, and the armed men are her secret service agents. But if
you leave that part out, man, it sounds suspicious. And that's kind of what
happened here. There was this manufactured narrative that there is
literally no evidence to support. It doesn't, it's not there. Even the
The Ukrainians have said that Biden's son did nothing wrong.
And so without any evidence, these right-wing conspiracy sites, that's what they are.
They're political sites though.
They're really, they're not conspiracy sites in the sense that they believe this stuff.
They're just pumping out misinformation to get clicks.
They're coming up with something that's sensational, that shouldn't be believed by any rational
person and it gets clicks, it gets people mad, keeps the base fired up, that's what
it's there for.
And then somehow this fabricated story, this fake news, winds up in I guess Giuliani's
hands who then shows the president who believes it and then acts on it.
calls the president of Ukraine to invest, I mean he didn't even know who the owner
of the country, I mean it was just all bad, I mean from, to be the president of the United
States and have access to one of the most expansive intelligence networks in human history
and not have the basic facts for this is kind of embarrassing, but setting that aside, he
He calls and does this.
The end result is that the president that ran around screaming fake news is going to
be brought down because he believed fake news.
The man that told actual journalists that they were fake news is going to be brought
down by faking it. And they say poetic justice is dead. It's not. It's alive and well. It's
just not in fiction anymore. It's in real life. You can't write stuff like this. Anyway,
I'm not really sure I have a thought today. I just think everybody could use a good laugh
after the last few days and this made me laugh. So anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}