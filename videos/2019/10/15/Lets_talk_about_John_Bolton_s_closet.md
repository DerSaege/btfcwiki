---
title: Let's talk about John Bolton's closet....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=FHy9JYILFvg) |
| Published | 2019/10/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Surprised by John Bolton's ethical objection to the Trump administration's actions.
- Bolton referred to Trump administration's actions as a "drug deal."
- Bolton previously covered up the Iran Contra affair as assistant attorney general in the 80s.
- Claimed happiest moment was getting the U.S. out of the International Criminal Court to protect U.S. war criminals.
- Accused of derailing a conference on stopping weapons of mass destruction and later using that as a pretext for invasion.
- Allegations of blackmailing the chief of the Organization for the Prohibition of Chemical Weapons.
- Describes Bolton as a true believer who rationalizes his actions to protect American interests.
- Three options presented: Bolton grew a conscience, Trump's actions were too shady even for Bolton, or the actions truly jeopardized national security.
- Beau criticizes Bolton's role in American foreign policy and warns against turning him into a hero even if he brings down Trump.
- Concludes by pointing out Bolton's deceit in not coming forward about the transgressions he knew of.

### Quotes

- "They're true believers. Bolton is a true believer."
- "True believers are the most dangerous people on the planet."
- "Even in his most noble act, deceit was at its core."

### Oneliner

Beau reveals John Bolton's shady past and questions his sudden ethical objection to Trump administration's actions, cautioning against turning him into a hero.

### Audience

Viewers, Activists, Critics

### On-the-ground actions from transcript

- Hold officials accountable for their actions (implied)

### Whats missing in summary

The full video provides a deep dive into John Bolton's history and actions, shedding light on the dangers of true believers in positions of power.

### Tags

#JohnBolton #TrumpAdministration #ForeignPolicy #TrueBelievers #NationalSecurity


## Transcript
Well howdy there internet people, it's Beau again.
If you've been watching this channel any length of time,
you've probably picked up on the fact that I am pretty up on foreign affairs.
There are very, very few things that surprise me in that realm.
I have to admit, today's headlines did surprise me.
I did not see John Bolton raising an ethical objection
anything, really, but certainly not to the Trump administration's actions that
I didn't see that coming.
But if we are to believe the headlines, which at first I did not, I thought it
was satire when I first saw them.
And then I checked to make sure nobody'd spiked my drink.
And then I came here to John Bolton's closet.
But if we are to believe the headlines, John Bolton sent out word that he had
nothing to do with what he classified as, quote, a drug deal that the Trump
administration was cooking up, he had nothing to do with it, which is a funny
phrase for him to use, given the fact that as assistant attorney general during
the eighties, he ran interference and tried to make sure that nobody knew
about the Iran Contra affair, which means, I mean, he covered up literal drug bills.
This is a man who said that his happiest moment in his professional career was getting the U.S. out of the ICC, the
International Criminal Court, because this is a man that does not want U.S. war criminals to be held to account.
And that makes sense. I mean, because he would probably end up there.
He would probably end up there.
In 2001, he derailed a conference that was designed to stop the spread of weapons of
mass destruction and then shortly thereafter used the spread of weapons of mass destruction
as a pretext to invade the country.
He was also, he's been accused of blackmailing the chief of the Organization for the Prohibition
of Chemical Weapons.
The allegation is that he actually threatened the man's children.
Now with all of this information, it's easy to write people like Bolton off as psychopaths,
sociopaths. Whatever. See, that's not it though. It's scarier than that. They're true
believers. Bolton is a true believer. He truly believes that what he does protects the American
way of life. Believes that he's that thin pinstripe line between democracy and whatever.
True believers are the most dangerous people on the planet.
They can rationalize and justify anything.
That's how he can have a resume like this and still sleep soundly.
With that information, we're left with three options, assuming this testimony is true.
The first is that Bolton grew a conscience while working for the Trump administration.
That seems pretty unlikely.
The second is that the Trump administration was engaged in something so shady that John
Bolton, a guy who was the architect of a war of 500,000 people, well that was just a line
he wouldn't cross.
Seems pretty unlikely.
Or whatever they were doing truly jeopardized national security in the American way of life.
Those are the options.
I definitely think the third is probably most likely.
Now that that's been said, I would like to point out that Bolton is a horrible actor
on the international stage.
He should have nothing to do with American foreign policy, period, ever.
So even if he becomes the man who brought down Trump, we need to make sure we don't
turn him into a hero.
At the end of the day, he knew about it, and he didn't come forward.
Whatever this transgression was, that even registered in John Bolton's mind is wrong.
He didn't come forward about it, he kept it quiet.
Even in his most noble act, deceit was at its core.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}