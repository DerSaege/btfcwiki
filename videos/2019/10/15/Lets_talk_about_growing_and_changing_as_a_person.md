---
title: Let's talk about growing and changing as a person....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=nrzNTJaq1rw) |
| Published | 2019/10/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Received a message about past racist beliefs and desire to make a positive impact.
- Encourages moving forward without explaining past beliefs to unchanged friends.
- Acknowledges past problematic beliefs and ongoing growth for everyone.
- Emphasizes exposure to new perspectives as catalyst for change.
- Shares personal experience of evolving views on accessibility.
- Urges continuous learning and evolving perspectives as a natural process.
- Compares personal growth to physical training - setting new goals and getting stronger mentally.
- Assures that growth is ongoing and that past mistakes do not define a person.
- Mentions example of ex-Nazi YouTuber who changed for the better.
- Stresses the importance of reflecting on past beliefs and embracing personal growth.

### Quotes

- "You cannot allow your past to define you."
- "People change. If you don't look back on the person you were [...] and just cringe at how stupid you were, you're not growing as a person."
- "Everybody believed something problematic at one point in time. And everybody still does."
- "Growing as a person is a lot like those PT requirements."
- "Just continue growing and showing that through actions and people will understand."

### Oneliner

Beau advises on moving forward without explaining past beliefs, embracing growth, and not letting the past define you.

### Audience

Individuals seeking guidance on personal growth and overcoming past beliefs.

### On-the-ground actions from transcript

- Continuously expose yourself to new perspectives and information (implied).
- Embrace personal growth by setting new goals and evolving perspectives (implied).
- Show through actions that you have evolved from past beliefs (implied).

### Whats missing in summary

The full transcript provides a nuanced exploration of personal growth, acknowledging past mistakes, and embracing change.

### Tags

#PersonalGrowth #OvercomingBeliefs #Reflection #Evolution #CommunityUnderstanding


## Transcript
Well, I'd either internet people as Bo again.
I got a message and I'm gonna remove
a lot of information from it
because it was personally identifying
and I'm gonna answer it publicly
because I think a whole lot of people
could benefit from hearing it.
Um, okay.
So, starts off with, we're from the same area.
I spent eight years in the SF. You seem to understand. I was medically discharged.
I'm back home now around my friends from high school. Before I enlisted I was
racist. Like really racist. They still are. They keep telling me how I've changed
and it bothers me. What am I supposed to do? How do I reconcile my past beliefs?
How do I explain my current ones? I want to get active and make the world a better
place and I think I have the skill set to do it and I find myself leaning
towards the word you won't say. How can I possibly get active with my past? I'll
get cancelled if they find out. I've been through a lot of this. For me it wasn't
in eight years, it was five.
But same thing, come back to a small town,
man, you've changed.
The question, my friend, is not, why have you
changed in eight years?
The question is, why haven't they changed in eight years?
You need new friends.
You don't need to explain anything to them.
Nothing.
You don't owe them an explanation at all.
don't. You're a racist, of course you are, who you were from the hometown you grew up in. That
pretty much goes without saying. I've been there a few times. Now you're not. Travel will do that
to you. Travel will do that to you. You get exposed to different things along the way. Race, ethnicity,
language, these things, they slip away as dividers. So yeah, that changes. You've evolved, good. You're
You're going to keep evolving too.
Your past is your past.
Everybody's got one.
Everybody believed something problematic at one point in time.
And everybody still does.
Nobody is as enlightened as they think they are.
Everybody has beliefs that are wrong.
Everybody, you, me, everybody watching this video,
there's something that they believe that is just dead wrong.
It is what it is.
you're going to keep growing.
In your case, being SF, I'm willing to bet that you are
incredibly ableist.
Yeah, race, ethnicity, gender, orientation, country of
origin, these things mean nothing to you anymore.
There's no bias there.
You're done with that.
But the best and brightest got a job to do, man.
You weren't exposed to people that are disabled.
That's what changed it.
It wasn't that you had some epiphany.
You were exposed to these different things.
You saw that all men are pretty much the same.
That's what changed.
Because you weren't exposed to this, you're still going to have those biases.
And it's not that you look down on people.
It's that you're not conscious of it the way you are the other things.
And this is one I can cop to.
It wasn't even that long ago, a couple months ago, a friend of mine, he's redoing his bathroom
and we're standing there and they're talking about how they've got to widen his door to
make it wheelchair accessible and, you know, nobody in his house is in a wheelchair and
we're standing there talking about just how stupid it is and it wasn't the architect,
it wasn't the contractor, it wasn't a carpenter, it was literally a day laborer and I love
of that, because I'll tell you something else you probably still have in you, is a class.
You may need to work on your class consciousness a little bit.
But this guy, he stands up and interjects into the conversation.
And he's like, hey, well, what we're doing is we're kind of retrofitting all the houses
when people update.
You're updating your bathroom, so we're going to widen the door.
It's going to cost you like half a percent extra.
You're not even going to notice it in the end price.
And the problem is that right now, you know, all the doors are this standard size and we
want them to be wider so people with wheelchairs can get in and out and then this will become
the standard size because right now people in wheelchairs, they got to retrofit any house
that they get and it's kind of like a tax on disabled people.
Yeah, yeah, that makes complete sense.
That actually sounds like a good idea.
on, you know? We had never thought of it because we weren't exposed to it. We had
no idea that this was even an issue. You're gonna run into stuff like that
all the time. You will continue to grow. You will continue to see things in a
different light as long as you allow yourself to. New information should
change the way you see things. Growing as a person is a lot like those PT
requirements. I'm sure there was a time when that 2-mile run in 15 minutes seemed
daunting. After a while, it was nothing. You set a new goal. You got stronger. Same thing
is going to happen with your mind and the way you think and the way you evolve as a
person. Right now, you think you're at your peak. You're not. Nobody is. Everybody's
going to get better. Now, as far as your past, everybody's got a past. People understand
You know, just say, you know, these were your beliefs then, these are your beliefs now.
Most people understand that people grow. Everybody has skeletons in their closets.
Some of us have mass graves. It's okay. The point is that you're growing.
Now if you were to hold on to those, yeah, that may lead you to getting canceled.
But as long as you're continuing to move, it's probably not going to be a problem.
problem. Yeah, there's going to be some people that don't like it. Some people are going to say,
well, he didn't really change. He's just saying that or whatever, and that's fine. Don't worry
about them. It's not a competition. It's not who's the most woke. Just continue growing and
showing that through actions and people will understand. People will get it. Everybody has
has a problematic past in some way. Everybody. But as long as you keep showing that your
current actions are different than that, people aren't gonna care. People are not gonna care.
You cannot allow your past to define you. One of the YouTubers I watch a lot is a guy
A guy named Aaron, his channel is called Re-Education, he's an ex-Nazi.
He now is somebody you will probably really like.
So don't think that just because your past is bad that people don't want you to get
better.
people would be more supportive than you think. People change. If you don't look
back on the person you were five, ten, two years ago and just cringe at how stupid
you were, you're not growing as a person and you're failing at life because
That's the whole point.
Anyway, it's just a thought.
You have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}