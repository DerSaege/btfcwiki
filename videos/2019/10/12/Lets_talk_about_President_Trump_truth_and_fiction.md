---
title: Let's talk about President Trump, truth, and fiction....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Ow1fNC-oSMI) |
| Published | 2019/10/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau expresses concern about being followed and introduces himself as Deep Goat.
- He outlines the President's actions since taking office, focusing on various decisions and their impact.
- The President withdrew from international treaties, diverted military resources to monitor migrants, and undermined different communities.
- Beau mentions how the President's actions drove away valuable allies like the Kurds and disrupted international relationships.
- The President's decisions include pulling out of the Iran deal, withholding security funds from Ukraine, and engaging in a trade war with China.
- Beau implies that these actions have ultimately benefited Russia rather than the United States.
- He does not explicitly label the President as a Russian asset but suggests that it's something worth investigating.

### Quotes

- "Every one of these moves benefits Russia."
- "Maybe something to investigate."
- "Y'all are the journalists."

### Oneliner

Beau outlines the President's actions, suggesting they benefit Russia more than the United States, prompting consideration and investigation by journalists.

### Audience

Journalists

### On-the-ground actions from transcript

- Investigate the President's actions and their potential impacts on national interests (suggested)

### Whats missing in summary

The full transcript provides detailed examples of how the President's decisions have favored Russia over the United States, urging further investigation and critical thinking.


## Transcript
Howdy there, Wayward, Berenstain, that's close enough.
That's close enough, y'all sure you weren't followed?
Tonight I want to talk to you about the President of the United States
and his greatest accomplishments.
Well, what should we call you?
Y'all can just call me Deep Goat.
Since the President took office,
You're sure you weren't followed, right?
He's done a number of things.
He withdrew us from the Open Skies Treaty,
withdrew us from the Intermediate Nuclear Forces Treaty,
which is a treaty Vladimir Putin's been trying
to get out of for years.
He actually said the original Russian signers of it were naive.
He wanted out of it that bad.
He diverted resources, military resources,
DOD to go down to the border and monitor unarmed migrants like they were some kind of national
security threat. Then he diverted finances from DOD, undermining their ability to recruit and
readiness to build a wall. He's undermined the intelligence community, both at home domestically
and overseas. He undermined the counterintelligence community by asking foreign countries to
double check their work. He drove away the most valuable non-state actor in the Middle
East into the arms of another country. He drove away the Kurds. They'd been fighting
with us for 15 years, and when he did that and betrayed them, he made it almost impossible
for the United States to advance its interests by recruiting and partnering with indigenous
forces the world over because nobody's going to trust us anymore.
He pulled out of the Iran deal, forcing Iran to cozy up to another country for protection.
He withheld security funds in the middle of a war from Ukraine.
And then he started this seemingly never-ending trade war with China that has weakened the
economies of both the United States and China.
These are his greatest accomplishments, and yeah, they all benefit a country, but it's
not the United States.
Every one of these moves benefits Russia.
Well, what are you saying? That the President is a Russian asset?
No, I'm not saying anything. Not saying anything at all.
It's just something to think of. Something to think about.
Maybe something to investigate. Y'all are the journalists.
To me, well, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}