---
title: Let's talk about secret impeachment hearings and cop shows....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8yRxM-Z1tpo) |
| Published | 2019/10/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the reason behind holding impeachment hearings in secret compared to past cases like Clinton and Nixon where facts were already established.
- Compares the secretive hearings to a cop show where witnesses are separated to prevent collaboration in testimonies.
- Mentions that leaking testimonies is akin to a cop revealing information to suspects to test their honesty.
- Notes that the Republicans attack the process because they cannot defend the President's actions.
- Draws a parallel between defense strategies in criminal cases and the Republicans trying to suppress evidence.
- Concludes by stating that the testimonies in the current impeachment process are critical compared to past cases where events were clear.

### Quotes

- "The hearings are in secret so the other witnesses don't know what's been said already. It's a way to keep them honest."
- "They're trying to suppress evidence because they can't defend the president's actions. It's really that simple."

### Oneliner

Beau explains the rationale behind secret impeachment hearings and why attacking the process is the Republican's only defense strategy.

### Audience

Political enthusiasts, voters, activists

### On-the-ground actions from transcript

- Contact your representatives to advocate for transparent and fair impeachment proceedings (suggested)
- Stay informed about the impeachment process and share accurate information with others (implied)

### Whats missing in summary

Insights on the potential consequences of allowing evidence suppression and attacking due process in high-stakes political proceedings.

### Tags

#Impeachment #RepublicanParty #DefenseStrategy #PoliticalProcess #Transparency


## Transcript
Well, howdy there, Internet people, it's Bo again.
So, tonight I was on Twitter, and somebody brought something up,
and it just kind of blew my mind.
The person said that Republicans weren't concerned
about the impeachment process being unconstitutional.
That was just the word they were using.
That they were really concerned because it's different than Clinton and Nixon,
it's being held in secret. So what's the difference between them? Yeah, I mean, yes,
in this case there are going to be some international secrets that you don't
want public. Yeah, that's true, okay, but that's not the reason they're being held
in secret, not the whole reason anyway, not even the main reason. See, in the
The other two facts were already established, generally.
They had physical evidence, reports, articles.
They knew what went down.
They're just putting it on record.
In this case, it's not what's going on.
It's not what's going on.
They have a general framework of what happened, but they don't know exactly what went down
who was involved. I want you to think about your favorite cop show, or if you
don't have one, think about Law and Order. Everybody's seen an episode. So
there's the cop. There's a shooting, let's say. There are four people in the car. He's
certain one of them did it. One person drove and knew that it was going to go
down, the other two are innocent. So, what's he do? Cuffs them up, takes them
all back to the station, and puts them all in a room together and starts asking
them questions, right? No, of course not, that would be stupid. They'd be able to
cook up testimony between each other right there, because they know what the
the person said.
The hearings are in secret so the other witnesses don't know what's been said already.
It's a way to keep them honest.
That's why.
That's why it's done that way.
It's why it's done in criminal cases when they divide people up so they can't cook up
a story.
about the leaks? Why is he leaking testimony then? Think back to that cop
show. There's always that scene. The cop walks in. You know what your buddy just
told us? It's exactly what's happening. Only they're using the media to do it.
It's keeping everybody honest.
No sense in lying about this, we already know.
That's why it's in secret and that's why they're keeping
Congress people out that have no business being there.
And to be clear, they're not just keeping Republicans out,
they're keeping Democrats out
that have no business being there.
Now the Republican Party has of course decided to frame this
a plot of deep state lizard people or whatever, that's not the case.
But they have to frame it that way.
They have to attack the process because they can't defend the President's actions and
they know that.
So the only option they have is to attack the process.
What happens in criminal cases?
Any defense attorney will tell you, if your client is just guilty of sin, the only thing
you can do is go after the cops.
Go after the chain of custody.
Go after the process.
Try to get it thrown out on technicality.
Get evidence suppressed.
That's kind of what the Republicans are trying to do.
They're trying to suppress evidence because they can't defend the president's actions.
It's really that simple.
No great conspiracy.
I mean, we've all seen this play out on TV.
You divide up witnesses.
You don't let them hear each other's testimony.
You'd have to be stupid to do that.
the difference is that in this case the testimonies are critical. In the other
cases, everybody already knew what happened. It was all very evident. I hope
that sheds some light on things. But anyway, it's just a thought. Y'all have a
Good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}