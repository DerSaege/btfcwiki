---
title: Let's talk about the President's legal argument against all criminal liability....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Ce3f_2dV7sA) |
| Published | 2019/10/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President's legal team argued for total immunity from criminal liability.
- Even judge found the argument bizarre and proposed a scenario of the president shooting someone on Fifth Avenue.
- The legal argument implies the president could commit murder without facing any consequences.
- Lack of criminal liability could allow the president to shut down voting stations or commit voter fraud with impunity.
- Impeachment by Congress may not be a safeguard if the president has absolute power.
- The argument aims to grant Trump dictatorial powers with no checks or balances.
- Trump could become a dictator with unchecked authority if the argument is accepted.
- Judges deciding on the president's immunity from criminal liability are essentially determining the fate of the country.
- Granting total immunity could lead to the abuse of power and Trump acting as a Tsar.
- This legal argument threatens to give Trump unrestricted power and evade legal consequences.

### Quotes

- "He is total immunity from all forms of criminal liability."
- "This argument that is being made is the legal argument to turn Trump into a dictator with absolute power."
- "It's terrifying because that's not how it's being framed."
- "Trump becomes Tsar, and all men are slaves to the Tsar."
- "Meanwhile he's attempting to gain the power to do whatever he wants with no legal ramifications."

### Oneliner

President's legal team argued for total immunity, implying Trump could commit crimes without consequences, potentially paving the way for unchecked dictatorial powers.

### Audience

American citizens

### On-the-ground actions from transcript

- Contact elected representatives to express concerns about the implications of granting total immunity to the president (implied).

### Whats missing in summary

The full transcript provides a detailed explanation of the dangerous implications of granting total immunity to the president, outlining how it could lead to unchecked abuse of power and potential dictatorial authority.

### Tags

#Trump #Dictatorship #LegalArgument #Immunity #AbuseOfPower


## Transcript
Well, howdy there, Internet people.
It's Bo again.
So tonight we're going to talk about the president's legal team and
the argument they made in federal court.
It's been in the headlines, but I'm not really sure that it's being discussed
the way it should, at least not the way I think it should, in an attempt to stop
tax returns from being released. The president's lawyers made the argument
that he is immune from all criminal liability. All. He can't be investigated,
he can't be indicted, he can't be prosecuted, he can't be processed.
Nothing. He is total immunity from all forms of criminal liability. That was the
argument and it wasn't just... the argument was so bizarre that even the
judge wanted clarification and proposed the Fifth Avenue scenario. He shoots
someone in the street and used that and asked the attorney and the attorney's
like no that's correct. He can't be prosecuted. Law enforcement cannot
intervene federal state or local can't intervene you know I don't like slippery
slope arguments and that's what this is going to sound like but it's not because
the argument made in federal court was that he could literally commit murder
that's pretty high up in our hierarchy of crimes that that's one of the bad
ones. If he can do that and suffer no criminal liability, what's to stop him
from shutting down voting stations in locations that oppose him or engaging in
massive voter fraud? What would stop him? There's no criminal liability. Nobody can
Well, certainly Congress would impeach, right?
I guess so, unless he has him imprisoned, or worse, because no criminal liability.
As long as he stays in power, well, we'd get rid of him in four years, would we?
How can you guarantee that?
There's no criminal liability.
This argument that is being made is the legal argument to turn Trump into a dictator with
absolute power.
No checks, no balances.
He can do whatever he wants.
It's terrifying because that's not how it's being framed.
framed as this crazy argument that the president's lawyers made.
And yeah, it is.
But no man that would allow his lawyers to make that argument on his behalf should be
in public office.
These judges, whether they know it or not, are deciding the fate of the country if they
They decide that, yes, the president is completely immune from all criminal liability.
That's it, it's over, no checks, no balances, absolute power.
And that power will be abused.
Trump becomes Tsar, and all men are slaves to the Tsar.
That's the reality.
And it's not a slippery slope argument.
That's literally what they're arguing in court.
In federal court, this is the argument that was made.
We sit and listen as people try to say that there's a coup against the president.
Meanwhile he's attempting to gain the power to do whatever he wants with no legal ramifications.
So there's your scary Halloween story.
anyway. It's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}