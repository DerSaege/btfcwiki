---
title: Let's talk about the constitutionality of impeachment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=H3tk7EMOmpY) |
| Published | 2019/10/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the constitutionality of impeachment amid claims of unconstitutionality.
- Emphasizes the impeachment process as the indictment phase.
- Quotes Article 3 Section 2, which stipulates that impeachment trials are not criminal prosecutions.
- Clarifies the limited punishment the Senate can impose: removal from office and disqualification from holding future office.
- Points out that the President's right to pardon does not apply in cases of impeachment.
- Mentions the requirements for the Senate during impeachment trials: under oath, Chief Justice presides, and a two-thirds majority is needed to convict.
- Stresses that the President's right to confront accusers applies in a criminal trial, not impeachment.
- Asserts that there is nothing unconstitutional about the current impeachment proceedings.
- Addresses claims of denial of due process by explaining its inapplicability at this stage.
- Concludes by debunking notions of a coup and affirming the legitimacy of the impeachment process.

### Quotes

- "The House has sole power over impeachment proceedings."
- "There is nothing unconstitutional occurring."
- "It's not a coup. It's the way the process is laid out."

### Oneliner

Beau explains the constitutionality of impeachment, debunking claims of unconstitutionality and clarifying the process with a focus on indictment and limited punishment.

### Audience

Citizens, Activists, Voters

### On-the-ground actions from transcript

- Stay informed on the impeachment proceedings and the constitutional aspects involved (suggested).
- Educate others on the impeachment process and the constitutional guidelines (suggested).

### Whats missing in summary

Detailed breakdown of specific articles and sections of the Constitution relevant to impeachment.

### Tags

#Impeachment #Constitutionality #DueProcess #Senate #Punishment


## Transcript
Well, howdy there, internet people, it's Beau again.
So, uh, we're going to talk about the constitutionality of impeachment
because a lot of people are running around saying that what's being done is
unconstitutional to include the president.
But as we have learned before, just because he tweets something, doesn't mean
it's true, even though a lot of other elected officials who have sworn an oath
to support and defend the constitution are running around saying the same thing
just proving they never read the document they swore an oath
to support and defend but anyway we will cover everything that the
constitution says about impeachment it's not really that much
okay people are saying that the process is unconstitutional that's patently
false. Just plainly, on its face, it's false. The impeachment process is the indictment
phase. What the House is doing, that's the indictment phase. And what the Constitution
has to say about that is in Article 1, Section 2, it says that the House shall appoint its
officers, you know, the Speaker of the House, and whatever. And then they have sole power
over impeachment.
So to be clear, if they wanted to do the entire thing
in secret, not even inform the president
that it was happening, and do a surprise vote,
that's completely constitutional.
If they decided that their procedures require a witness
to wear a Mickey Mouse hat and saying,
it's a small world first, that's constitutional.
The House has sole power over impeachment proceedings.
If they render a verdict to impeach, then it goes over to the Senate to convict.
They hold the impeachment trial.
That's Article 1, Section 3.
The Senate has sole power over impeachment trials.
Now, the Senate, there are a few rules for them.
Not many, though.
They have to be under oath or affirmation.
If they're impeaching the President, the Chief Justice has to preside,
preside and they need a two-thirds majority to
Convict those are the rules
Now they have rules as far as punishment as well
Judgment is limited to removal from office and
Disqualification of holding future office
That's it. That's as far as the Senate can go
During an impeachment trial. That's it. Now it does say
that the person convicted is still liable to indictment, trial, judgment, and punishment
in a criminal prosecution. But that's not the Senate. The Senate is not going to send
the President to prison. That may happen afterward.
Okay, Article 2, Section 2, discusses the President's right to pardon people, and he
and he can't pardon in cases of impeachment so that cute little meme
president gets impeached removed Pence pardons him it's not gonna work that way
Article 2 section 4 that's the part that says high crimes and misdemeanors now
what that actually says is that the president shall be removed for these
types of charges shall be as legalese must be removed the house and Senate
can impeach and convict for pretty much anything they want but for these things
the punishment has to include removal article 3 section 2 trial of all crimes
except impeachment has to be done by jury. So this goes along with the rules
for the Senate about the limitations on punishment and then clarifying that the
person could be tried later. What it's doing is it's separating and it's
saying that an impeachment trial is not a criminal prosecution. It's not the same
thing so it's getting rid of the idea of double jeopardy and stuff like that.
Okay, so that's it. The main document, the Constitution, that's all it says about impeachment.
Those are the rules. None of those are being violated. None.
There's nothing unconstitutional about the proceedings right now.
Now, we have a couple other things that get thrown up that sound constitutional,
and they had to do with the Bill of Rights, and it says that the President is being denied due process.
Well, first, we're still in the impeachment phase.
They don't even have to tell them it's happening.
There is no requirement for due process here.
More importantly, that comes from the Fifth Amendment.
No person shall be deprived life, liberty, or property without due process.
What is the max punishment?
Removal from office and disqualification.
life, liberty, and property are not in jeopardy. It doesn't even apply. The
president has a right to confront his accusers. That's the Sixth Amendment.
That amendment starts with the phrase, in all criminal prosecutions, as outlined
in Article 1, Section 3, this is not a criminal prosecution. Now he may get to
confront his accusers. That may happen, but it's going to be after the Senate
convicts and then he's indicted and during the trial, a criminal trial, he
would be able to do that. That's it. There is nothing unconstitutional occurring.
It's not a coup. It's the way the process is laid out. Anybody who says that it's a
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}