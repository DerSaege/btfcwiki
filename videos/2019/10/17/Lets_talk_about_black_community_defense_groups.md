---
title: Let's talk about black community defense groups....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=T3A6hqI-Oxg) |
| Published | 2019/10/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- There is a call to action in minority communities post the Jefferson shooting, where a woman was shot by a cop in her own home.
- The call to action is to establish a community defense force, reminiscent of the original days of the Black Panther Party.
- Beau advocates for localized community defense organizations for all communities, not just particular racial or ethnic groups.
- He believes such organizations can avoid the pitfalls the Black Panthers faced and should focus on community development beyond just defense.
- Beau suggests avoiding a militant structure like the Black Panthers had to prevent negative media portrayal and law enforcement scrutiny.
- Winning the PR battle is critical for such organizations to avoid being labeled negatively by the media or law enforcement.
- Beau acknowledges the concerns and challenges but believes that with proper messaging and community support, these organizations can succeed.
- He encourages the establishment of organizations that create redundant and localized power structures to benefit communities.
- Beau stresses the importance of steering away from a paramilitary image to maintain a positive perception and prevent becoming a target.
- The focus should be on community development activities like microloans, mentoring, childcare, and skill development within these organizations.

### Quotes

- "Creating that redundant power, creating that localized power structure is really important."
- "The PR battle is what's gonna be important here."
- "The first battle is a PR battle."
- "And I don't think that they will become a target as long as they win the PR war up front."
- "The US has changed, it's gotten better, but it hadn't changed that much."

### Oneliner

Beau advocates for establishing community defense organizations with a focus on community development, stressing the importance of winning the PR battle to avoid negative portrayal and scrutiny.

### Audience

Community organizers

### On-the-ground actions from transcript

- Establish localized community defense organizations with a focus on community development (implied)
- Ensure proper messaging to win the PR battle and prevent negative portrayal (implied)
- Seek support from organizers within the community for setting up these organizations (exemplified)

### Whats missing in summary

The full transcript provides more context on the challenges and importance of community defense organizations in marginalized communities.

### Tags

#CommunityDefense #BlackPantherParty #PRBattle #LocalizedPower #CommunityDevelopment


## Transcript
Well, howdy there, internet people, it's Bo again.
So we have another message I'm gonna respond to.
And the same thing, I'm gonna take out
the identifying information in it
and just focus on the overall concept in it.
Okay, there is a call to action that is coming out
in minority communities following the Jefferson shooting.
That is the shooting of the woman
who was killed in her own home
after being shot through the window by a cop.
And while I believe in its ideals, I think the reality would have grave consequences.
First to clarify a few things as they relate to my question, I'm an African American man
born, raised, and back living again in a rural part of the United States.
I have not served in the military or police.
are a lot of organizations that are unfriendly to minority groups in the area. I've been
subjected to racially motivated violence, both verbal and physical, throughout most
of my teen years. I have since taken it upon myself to seek combat training, both hand
to hand and with a firearm, and I'm about to embark on a wilderness first responder
course to better be able to protect and assist myself and others.
The call to action that I'm seeing is a call back to the original days of the Black
Panther Party in establishing what is essentially a community defense force, giving folks an
alternative to 9-1-1 to call for non-medical help or protection.
I believe in the ideal of localized community defense and I think that with the technology
today, it would be very straightforward to coordinate nationally, albeit under observation.
But of course, we all know the story arc of the Black Panther Party, despite all of the good they did.
I'd love to know what you think an organization like this, or how you think an organization like this would fare in
today's world, and potentially after November 2020.
Okay, so the inherent question in this is, do I think Black people should set up organizations like this?
No, I think every community should set up organizations
like this, creating that redundant power.
With marginalized communities, with minority communities,
it becomes that much more important.
So I mean, yeah, this is something
I advocate for everybody.
I don't necessarily think it needs
to be just a particular race or ethnic group.
I would prefer they be geographically centered.
At the same time, in your situation,
not everybody in your geographic area
is going to think highly of this.
So I can understand why it would be limited in scope.
That makes sense.
Do I think the organization would farewell?
Yeah.
Yeah, in today's world, I do.
I think it would be fine.
I think that it can avoid the pitfalls
that the Black Panthers fell into.
too.
When they came about, they were a very publicly militant organization.
You know, the berets, the uniforms, all of that, I would avoid that.
I would avoid that today, and I would expand far beyond just like they did.
You know, it's community defense, but at the same time, they had a lot of other stuff
going on.
I think these organizations should do the same thing.
These little community networks engage in microloans for small businesses,
assistance for the needy, mentoring, childcare, all kinds of stuff, skill development.
There's so much that can be done once these networks are in place.
And to me, that would be the focus.
The other side of it, the community defense aspect of it,
and giving people an alternative to call,
that makes sense, and it should be part of it, no doubt.
But I wouldn't adopt that militant structure
that the Black Panthers had.
And that's not a criticism of them at the time.
When they came about, they didn't have a choice.
It's easy to look back and say,
well, they were kind of militant,
and that made them a target.
Yeah, and if they weren't militant,
still would have been targets. So that doesn't matter. I don't think today that that posture
is necessary. I could be wrong, but I don't think it's necessary today. And I think as
long as that posture is avoided, you know, you don't adopt military style uniforms or
a rank structure that parallels the military, anything like that. I think it would be seen
more as a really advanced church group that also has this community defense
component rather than a paramilitary organization and that's where the grave
concerns would come in because the second it gets seen like that then it's
a target for the media. You're the boogie man and you're out to get everybody. So
when you're talking about doing something like this especially especially
especially when it is going to be benefiting one particular community and there's already
a storyline that says, oh, they're militant.
The first battle is a PR battle.
You have to make it clear that that's, you're not there to do anything wrong.
And from this message, you're not, you're wanting to help people.
It's got to be the messaging that goes out.
Because if it's not, you do become a target for the media, law enforcement will start
looking at you because they're going to think that you know what they're going to think.
I think it's a great idea and I do understand the concerns.
They exist, they're real, they're warranted, but I think the pitfalls can be avoided, I
really do.
I know that there are organizers in your community
that can put it together.
I'm positive of that.
You don't need help from the outside,
but if you want it, I can assure you
there's a whole bunch of people of all skin tones
that would be more than willing to help.
Um, so, yes, I think organizations like this
are important, creating that redundant power,
Creating that localized power structure is really important.
And I don't think that they will become a target as long
as they win the PR war up front.
As long as the first stories that come out about them
in the newspaper are about them helping a needy family,
or setting up a mentoring program for children,
or something like that.
The PR battle is what's gonna be important here.
If that battle isn't won, then yeah,
all of your fears, they're very real.
They are very real.
Because the US has changed, it's gotten better,
but it hadn't changed that much.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}