---
title: Let's talk about young women and beauty standards....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8zaNWEVlzGM) |
| Published | 2019/09/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Shifts focus from the White House to a disturbing incident at a school involving a 12-year-old girl.
- Describes how the girl was allegedly assaulted on the playground, with racial slurs and degrading comments.
- Mentions the school's strict regulations on marriage and homosexuality in contrast to its failure in regulating bullying.
- Questions the toxic beauty standards imposed, particularly in the United States, and their impact on self-worth.
- Encourages not to conform to unrealistic beauty standards and to recognize one's true worth beyond physical appearance.
- Emphasizes that those who judge based on looks reveal more about their values than the person judged.
- Criticizes the school's role in fostering intolerance and producing intolerant children.
- Condemns the devaluation of individuals based on exploitable traits, evident from the incident.
- Advocates for individuals not to let others determine their worth and to recognize intrinsic value beyond superficial standards.

### Quotes

- "Never let anybody tell you your worth."
- "If you're not pretty, you're nothing. You're trophy. Trust me, it's not the guy you want."
- "Never, never let a man determine your worth, because most men don't know value."

### Oneliner

Beau addresses a disturbing school incident, criticizes toxic beauty standards, and advocates for recognizing intrinsic worth beyond superficial judgments.

### Audience

Students, parents, educators

### On-the-ground actions from transcript

- Contact the school administration to address bullying and intolerance (suggested)
- Educate students on diversity and inclusion through workshops or programs (suggested)

### Whats missing in summary

Deeper insights on the societal impact of toxic beauty standards and the need for promoting self-worth beyond appearance.

### Tags

#Bullying #BeautyStandards #SelfWorth #Education #Tolerance


## Transcript
Well, howdy there, internet people, it's Beau again.
I'm sure everybody still wants to talk about the White House
and what's going on there, but something else came up.
And it's honestly not going to matter to most of you.
I looked at the analytics, the demographic
that I'm going to be talking to here.
It's only about 3,000 of you.
Only about 3,000 of you.
But if it resonates with one, it is worth it.
So, the other thing I have to start off by saying is everything here that I'm about to
talk about is alleged.
It's alleged.
I don't even have a copy of the police report yet.
I normally don't like to do things just off of news articles, but this is more of a jumping
off point to a wider discussion.
So, what happened?
Allegedly, a 12-year-old girl was pushed to the ground.
school on the playground had her mouth covered as a group of boys cut off
pieces of her hair, called her nappy, said she was ugly and that she never
should have been born. This is said to have happened at Emmanuel Christian
School. This is the ever-tolerant school that in their new hire kit makes
teachers say that everything is wrong. Marriage is just between a man and a
woman. No cohabitation before marriage. No premarital homosexuality. Like
everything is wrong. They spent a lot of time in their new hire kit regulating,
attempting to regulate people's bedrooms. Maybe they should spend more time
attempting to regulate the playground. This is the school where Mike Pence's
His wife works, the Vice President's wife works here.
There's a whole lot to unpack there.
You have the obvious alleged assault, complete with racist overtones, but that's not what
I want to talk about.
about that other thing. Ugly never should have been born. Beauty standards are garbage,
especially in the United States. We're a very diverse country, yet the beauty standards
are very, very Eurocentric, and they change. Beauty standards aren't standard by any means.
If you go back and look at what was like the top 50 celebrities in 1990, they're going
to be very different than today. It's changing, it's becoming more diverse, it's becoming
more inclusive. There's more exposure to people of color, so more of them make it
into that little chart. But that's what those standards are based on. We forget
that none of that's real. None of that is real. It's all fake. And there are
people trying to live up to it, and especially within the demographic I'm
talking to right now. People are trying to live up to it. There's no reason to.
There's no reason to. Because the people that care about that, well, we'll get there.
Never let anybody tell you your worth. Now if you are in this age bracket, I
understand beauty standards are important to your self-worth right now.
In the long run they don't matter. Just trust me, the straight-A violinist, as she was in this case,
she's going to have a better life than the plastic Becky in her class.
I don't want to put too much of a spoiler out there, but just make sure you go to your high school reunion, it'll be
entertaining.
thing. The other thing is, the people that would say something like this, ugly, never
should have been born, they're telling you their perception of your value as a person.
If you're not pretty, you're nothing. You're trophy. Trust me, it's not the guy you want.
It's not the guy you want.
If it's happening this young,
the age this is reportedly happening,
that's so deeply rooted, it's never going away.
It's never going away.
It's one of those unique things where
you can look at how somebody values other people.
And in the end, it ends up telling you
a whole lot more about their value.
We have a long way to go in this country, a long way to go, so much wrong with this.
But at the end, it's the devaluation of people, summing them up by what can be exploited from
Apparently that's already taken root in the sixth grade.
I would say that this is a very strong indictment
of that school.
I mean, go figure.
An intolerant school produces intolerant children.
Very Christian.
Never, never let a man determine your worth, because most men don't know value.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}