---
title: Let's talk about recession indicators and a ray of sunshine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=pxTPtnDholA) |
| Published | 2019/09/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Indicates recession indicators like the bond curve inversion, gold and silver price increase, and copper price decrease signaling hard economic times.
- Mentions that consumer confidence has dropped significantly, with the University of Michigan's data from August contradicting old data from the Bureau of Economic Analysis in July.
- Points out slowing growth in GDP, lowest manufacturing growth levels in a decade, and significant drop in freight shipments.
- Private domestic investments have fallen by 5%, indicating a significant economic shift.
- S&P 500 estimates of earnings growth are constantly dropping, a strong signal of an impending recession.
- Normal people may face layoffs but could benefit from cheaper goods and opportunities to start businesses or invest in property during a recession.

### Quotes

- "Toys get cheaper, and by toys I don't mean toys for children."
- "If you've been planning on bootstrapping and starting your own company, now may not be the brightest time to start it, but it may be the best time to start getting the equipment."
- "It's one of those things where if we continue fighting something that is probably inevitable, we may end up hurting ourselves."

### Oneliner

Beau warns of recession indicators like dropping consumer confidence and slowing GDP, offering insights on how normal people can navigate economic downturns.

### Audience

Economic observers

### On-the-ground actions from transcript

- Buy equipment for planned businesses or investments (exemplified)
- Look for deals on property during a recession (exemplified)

### Whats missing in summary

Detailed explanation of the impact of trade wars on the economy.

### Tags

#Recession #EconomicIndicators #ConsumerConfidence #Investing #TradeWars


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're going to talk about recession indicators
again and leave you with a ray of sunshine there at the end.
So I'm not an economist.
I really only looked at a few indicators for me.
And that's my personal thing, one of which
is the bond curve, which has inverted.
That is a sign of an upcoming recession.
The other ones I got from my grandma, she was not an economist either, but she's also
never wrong with this.
If gold and silver go up and copper goes down, you're heading into hard economic times.
That has happened now.
So the reason for that is gold and silver are a way to take your money and put it into
into something tangible that is safe.
They've kind of taken your money
out of the financial game for a little bit
and putting it into something that's gonna hold value.
Copper is an industrial metal
to used in a lot of new construction.
So if you see the price falling,
that means demand is falling,
which means there's less construction.
Okay, so all that's happened.
So when that happened,
I went and started talking to people
and looking at the indicators
that they use and then got online and looked
at what the actual economists are saying.
It's pretty much all bad.
Just go ahead and spoiler alert.
OK, so the University of Michigan
does a survey of consumers.
This is that consumer confidence you hear people talk about
and nobody knows what it means.
They basically ask people, how do you feel it's going?
The numbers dropped like a rock.
I know somebody out there that is in favor of the trade wars
going to say but the Bureau of Economic Analysis says otherwise and Trump said that consumer
confidence is fine. Yeah, their numbers are from July. They're using old data. Michigan's
numbers are from August. Okay, the GDP, which is the total value of finished goods and services
That is at a 2% growth for the second quarter of 2019.
It was at 3% during the first quarter.
So it's slowing down.
We see that.
Manufacturing growth is at the lowest levels in 10 years.
That's a really good indication.
Freight shipments have dropped like rock.
The people in that industry are predicting a negative GDP
that industry are predicting a negative GDP, as in the economy is actually shrinking by
the end of the year.
Private domestic investments have fallen by 5%.
That's from a real economist.
I really don't actually know how significant that is, but it seems significant at the time
when I was looking at it.
The other one is that the S&P 500, which is a index, they take the 500 largest companies,
they weight them by capitalization and a whole bunch of other stuff, and they use that to
project how the stock market is doing.
Now, they put out estimates of earnings growth, so how much more money they're going to make,
And they revise these estimates constantly.
And they are dropping every time they revise them, the numbers are going down.
And that is also a very, very good indication that we are headed into a recession.
So we're on our way there.
What does that mean for us, for normal people?
You know, when you hear most of this, this is measuring the sense of the economy by how
well the rich are doing.
What does it mean for normal folk?
Normally wages don't get cut, but layoffs occur.
At the same time, there are some benefits to those that are on the lower end of the
socioeconomic ladder.
The first is that toys get cheaper, and by toys I don't mean toys for children.
know your neighbor that bought two jet skis and took them out three times in the
last five years, probably gonna sell them really cheap. So if you have some money
put away, stuff like that, you'll be able to get deals on. The other thing is that
if you've been planning on bootstrapping and starting your own company, now may
not be the brightest time to start it, but it may be the best time to start
getting the equipment. I know a guy that during the great recession of 2008, he
uh people thought he lost his mind because he went out and bought two work
trucks. He had been squirreling away money for years to start his own
landscaping company. Every time one went out of business, well he would show up
and buy their weed whackers and lawnmowers or whatever really cheaply
because he knew they were well maintained, he knew they were industrial
grade, and basically he got his company started with less investment than he had planned.
So you do have that opportunity if you have planned this may be the time to get in the
game, so to speak, to get in the financial game.
The other thing is getting a home.
Now I'm not talking about a dream home, I'm not talking about the home in the good neighborhood
that is completely remodeled and everything, I'm not talking about that one, that one's
not going to drop more than likely, but the less than perfect home, the home that needs
to be updated because it has cabinets from the 70s, those, they're going to go down in
value which means you may be able to get your starter home, your first home purchase, you
may be able to get that done during the recession.
So leaving on a happy note, in some ways, in other ways, you do have to understand that
somebody is losing out on those deals, but this is all over this trade war, and the trade
war is based on the idea that we can't have a trade deficit.
Most economists will tell you a trade deficit is not necessarily a bad thing.
The other thing that they keep harping about is that China's economy will eventually be
bigger than ours.
They have four times the population.
I may not be an economist, but I would imagine that on a long enough timeline, that's a certainty.
It's one of those things where if we continue fighting something that is probably inevitable,
we may end up hurting ourselves.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}