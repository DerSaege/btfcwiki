---
title: Let's talk about the primaries, impeachments, and power....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KjF8SqP9veg) |
| Published | 2019/09/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the first Republican primary debates between Weld and Walsh, which most people missed.
- Accuses the Republican party of rigging the election in favor of Donald Trump by shutting down primaries and not promoting debates.
- Points out the party's support for Trump amid impeachment proceedings, predicting the Senate will never convict him.
- Criticizes the lack of accountability and integrity in politics, focusing on the pursuit of power over truth and justice.
- Emphasizes that the solution lies with local communities and individuals who genuinely believe in their values.
- Challenges the notion that corruption in politics is a bug in the system rather than inherent to it.
- Expresses concern over Americans being misled by corrupt politicians who prioritize self-interest over serving the people.
- Condemns the Senate's potential refusal to hold the President accountable, questioning the fitness of all Republican senators for office.
- Urges people to recognize the widespread deceit and corruption in politics rather than accepting it as normal.
- Calls for a shift in mindset to acknowledge the truth about politicians' motives and representation of the public.

### Quotes

- "Your solution is not in D.C., it's local."
- "The fact that we don't realize that is why we're in trouble."
- "That is corruption on a wide level."
- "If that statement is true… none of them are fit to hold office."
- "Because they tell us they're liars and cheats."

### Oneliner

Beau challenges the accepted corruption in politics, urging a shift towards local solutions and a rejection of deceitful politicians prioritizing power over truth.

### Audience

American voters

### On-the-ground actions from transcript

- Reach out to local communities and individuals who prioritize truth and justice (suggested)
- Challenge corrupt practices within your local political sphere (exemplified)
- Reject the normalization of deceit and corruption in politics (implied)

### Whats missing in summary

The full transcript provides in-depth insights on the corrupt nature of politics, urging individuals to seek solutions locally and question the integrity of elected officials.


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight, today, we're gonna talk about the first
Republican primary debates.
What's the matter, you missed them?
Yeah, most people did.
But they occurred between Weld and Walsh.
You didn't hear about them because the Republican party is
rigging the election in favor of crooked Donald Trump.
Trump, shutting down primaries, not promoting the debates, you know, normal stuff.
They're standing by him as impeachment looms.
But don't worry, the Senate will never convict.
What an odd statement to just be accepted as fact.
To just be, yeah, that's true.
No matter what the outcome is, no matter what evidence is presented, because it hasn't been
seen yet, the Senate will never convict.
What does that tell you?
This is the problem with trusting your betters.
They don't care about you, they don't care about truth, they don't care about justice,
they don't care about law.
They never did.
They care about power.
And that's it.
That's completely evident in that statement.
The Senate will never convict.
The Senate will never convict because they're Republicans.
And there's a Republican president.
So they will overlook whatever his crimes are.
As we argue with our family, friends, and co-workers over what American ideals should
They display corruption on a party-wide level.
They don't care about you, and they never did.
They never did.
Your solution is not in D.C., it's local.
It's your neighbor, your friend, your family, your co-worker, so it's people who actually
believe what they say rather than just say it in pursuit of tricking the commoners so
they can maintain power.
I'm sure somebody's already stopped.
Are you saying Democrats wouldn't do that?
Of course not.
There's a lot of straw men out there.
No, that's not what I'm saying.
I'm saying that this isn't a bug in the system.
This is the system.
And the fact that we don't realize that is why we're in trouble.
Why are the 99% of Americans in trouble?
Because when they tell us they are corrupt, when they tell us they don't care about the
law, when they tell us they don't care about justice, we don't believe them, oh that's
just politics.
No, that's corruption.
That is corruption on a wide level.
If that statement is true, and the Republicans in the Senate will not vote against the President
because he's a part of the same party, none of them are fit to hold office, none of them.
This is why we're in trouble.
Because they tell us they're liars and cheats.
And we're so deluded that we still think they represent us.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}