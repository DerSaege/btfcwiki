---
title: Let's talk about being edgy and offending people....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=SF7m9xf4To8) |
| Published | 2019/09/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Expresses no desire to change speech despite Democratic candidates' coarsening.
- Believes in reaching people with thought, not shock value.
- Views being edgy as a poor substitute for personality.
- Shares an incident on Twitter where he misunderstood a tweet and sought clarification.
- Emphasizes the importance of common courtesy and avoiding intentional offense.
- Stresses the value of reaching out for understanding.
- Mentions starting a podcast with reruns of older videos to introduce himself to the community.
- Talks about potential sponsored videos, stating they must fit the channel's theme.
- Mentions plans for workshops in the South to build community networks and possibly going on the road next summer.

### Quotes

- "Being edgy is not a substitute for having a personality."
- "It's almost always better to be nice."
- "Y'all have a good night."

### Oneliner

Beau believes in thoughtful speech over shock value, stresses common courtesy, and plans community workshops.

### Audience

Content creators

### On-the-ground actions from transcript

- Set up workshops to help build community networks (planned)
- Attend workshops to learn from others (planned)

### Whats missing in summary

Details about the podcast content and potential future episodes

### Tags

#Speech #CommunityBuilding #Podcast #ContentCreation #Workshops


## Transcript
Well, howdy there, internet boobalizbo again.
Let me start off by saying that tonight,
after the normal sign-off, I'm gonna answer a few questions
about the channel, about the platform.
So if you want, stick around for that.
But tonight, we are going to talk about being edgy
and offending people, because I've
got a question asking me if I was
going to change my speech now that Democratic candidates have
started coarsening theirs.
No, no.
I have no desire to do that.
I want to reach as many people as possible with thought and
provoke as much thought as possible.
I think my current speech or the lack of certain kinds of speech is helpful to that.
So I have no desire to change.
I actually think the Democrats are making a mistake.
The idea in theory should be to eliminate the Trump effect, not reincarnate it.
I want to reach people with thought.
want to provoke thought, not because I said something offensive or edgy or, you
know, shocking. It's not what I want to do. When you go down that road, you end
up getting edgier and edgier until you're saying things you don't even
really believe. I don't want to do that. I don't want those clicks. I don't want
those views, to be completely honest. I have no desire to offend people for no
reason and I know somebody out there right now is going well it's not your
fault if you say something and people get offended they need to be in control
of their own emotions hmm I hear that a lot I don't know that that's true I don't
know that that's true I mean sure you can say something unintentionally
offensive if you don't know if you don't know you can do that but if you do know
you're making a conscious choice, you're saying something that you know can be offensive
for whatever reason.
I don't know why you would want to do that.
I don't know why you would want to offend people you don't know.
Being edgy is not a substitute for having a personality.
But you can, I guess at times, offend people unintentionally.
In fact, I'll give you an example.
there was an incident on Twitter involving a YouTube creator, ContraPoints, and I'm reading
her tweet, and I'm like, my read of this is not matching the reaction, but I don't know
this community.
I don't know the ins and outs of this community that well, so I reached out to somebody else
because that sent off warning bells, you know, and I'm like, this is my read on this.
Am I close?
Am I missing something?
And they're like, you're not even close.
You're nowhere near close.
And then they broke it down for me.
I'm like, oh, OK.
So I didn't make a comment.
I mean, all it takes is that extra little step.
And you can avoid problems before they start.
Now that I've said this, I'm sure somebody's
going to ask my opinion.
My opinion is that the one thing that conversation does not
need is the opinion of the straight white guy that
didn't understand the conversation to begin with.
I don't see how I can add anything of value
to that discussion.
But all it takes is a little bit of common courtesy.
I've never understood the desire to say something offensive
just for being, just for the sake of being offensive.
It doesn't make sense to me.
I don't know what you would have against people you don't know,
other than your own bigotry.
In which case, if that's why you're saying it,
then it is your fault. It's completely your fault. It was intentional. You set
out to do it.
And in most cases,
if you're not sure, you don't understand something, you can reach out pretty
quickly.
And you'll learn something in the process.
It's almost always better to be nice.
Anyway, it's just a thought.
Y'all have a good night.
Now, if you're sticking around for the questions,
the first question is, you hit the goal on Patreon today.
When does the podcast start?
Three weeks ago.
I've apparently not done a good job of advertising that.
The first few episodes are reruns.
We've stripped the audio from some of the older videos.
So the podcast community can get to know me from the beginning.
Because the podcast community, and the YouTube community,
and the Facebook community, they're not all the same.
There's some overlap.
But they're different people.
And this will help them get to know me from the beginning.
So it will be an older video, the audio from an older
video with a little bit of additional commentary thrown in.
We've already had a few questions about asking for transcripts.
Eventually, I'm going to need to get somebody in here
to help me first.
So that will happen, but it will be a while.
The next question, I've had this asked three different ways.
This topic has come up in three different ways.
One is people asking me to do it.
One is people offering me the opportunity to do it.
And one is people telling me I would be a sellout
if I ever did it.
And basically, it's will I ever do sponsored videos
on YouTube, do like product reviews and stuff like that.
When the channel crossed 100,000 subscribers,
we started getting offers.
I've seen like 200 of these opportunities.
I have done zero.
That doesn't mean I wouldn't ever do it.
It just means that it would have to be extremely organic.
It would have to fit in to what we normally
talk about on this channel.
Out of all of those, I replied to one.
And it's a company that has Grow, like GrowKits.
We talk about food not launched, self-reliance,
planting fruit trees, that kind of stuff.
When it comes up, one of the main comments
and one of the main obstacles people face
is they don't have room or they don't have a yard.
So if this company wanted to send me that thing
and have me set it up and put it back there
so you guys could see it work, I would probably do that.
But it would have to directly fit with what we normally
talk about on the channel.
So you can stop sending me offers
to represent your dating app.
And I promise you, if I ever do it,
I will tell you flat out, hey, this company
wanted me to do this.
There will be no surprises on that front.
The only exception to it being organic,
it would have to be something ridiculous.
If Jeep called me up tomorrow and was like, hey,
we're going to give you a new Gladiator if you
make a video about it, you can bet
you would see my fat rear sitting in that driver's seat.
But it would have to be something like that.
And then the last question is about going on the road.
And are we going to?
Yeah.
It's planned.
We're setting up little workshops here in the South
to help people build community networks.
And then hopefully, if everything
goes according to plan, next summer we will go on the road
And we will do the same thing, help people set up community networks to do little events,
film them, put them online so everybody can learn from other people doing the same thing
they are.
Anyway, so there you go.
Man, it feels weird not ending this by saying it's just a thought.
y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}