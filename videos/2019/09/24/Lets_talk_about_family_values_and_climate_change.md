---
title: Let's talk about family values and climate change....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=lQr1SqB0jEo) |
| Published | 2019/09/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recounts an experience in a cave with his family and a Spanish-speaking family.
- Observes young kids charging ahead during a guided cave tour.
- Criticizes adults who dismiss teenagers' political opinions.
- Points out the lack of guidance offered to youth about education.
- Expresses disagreement with the conclusions of the Parkland teens but respects their effort.
- Compares Parkland teens to adults making jokes about guns on social media.
- Emphasizes that youth speak up because adults won't.
- Draws a parallel between cave instinct and adults not leading in society.
- Calls out parents who neglect educating themselves and their kids about climate change.
- Condemns parents who prioritize politics over their children's future.
- Encourages parents to prioritize educating their children over political allegiance.
- Talks about the impact of Greta Thunberg's speech on children's awareness.
- Urges parents to prioritize their children over political parties for a better future.

### Quotes

- "You aren't working to protect your kids. You're leaving it to them to figure out."
- "Your kid will betray your ideology in her name. Because they're smarter. Because they're more willing to learn."
- "If you want teens out of the political discussion, you've got to step up."

### Oneliner

Beau criticizes adults for failing to guide youth and encourages parents to prioritize education over political allegiance to empower the next generation.

### Audience

Parents, Adults

### On-the-ground actions from transcript

- Educate yourself on climate change and other critical issues affecting your children's future (suggested)
- Prioritize educating your children over political allegiance (suggested)
- Encourage open-mindedness and learning in your children (suggested)

### Whats missing in summary

The emotional impact of parents prioritizing politics over their children's well-being and future.

### Tags

#Youth #Parenting #Education #ClimateChange #PoliticalEngagement


## Transcript
Well, hi there, Internet people, it's Beau again.
A couple days ago, I had my family in a cave.
It's not an allegory.
We were really in a cave, us and a Spanish-speaking family.
There's a cave network nearby, and the state gives guided tours.
When we were down there, kids from both families, both big families, they were charging ahead,
trying to get in front of the guide even, and there we are pulling them back, pulling
them back.
A lot of people today are asking why there's a bunch of teens now in politics giving their
opinion and they just need to shut up.
We don't need to hear from them, they don't know anything.
There it is.
No guidance offered.
You wonder where the youth of today learns to not put a value on education.
From their parents.
They're teaching them that.
They're teaching them to place no value on education.
No value on learning.
Just shut up.
mind. Become a good cog in the wheel. Let your betters make your decisions for you.
I obviously do not agree with all of the conclusions that most of the Parkland teens came to. I
don't. I'd never tell them to shut up. They're out there trying to come up with something.
I don't agree with what they're coming up with, but they're trying. And I tell you,
I prefer them a whole lot more than those guys that are standing there in their social
media photos with their plate carriers and their rifles.
Making jokes about them.
We're supposed to take their opinion and they don't know the difference between an AR-15
and an L1A1.
Ha ha ha, dumb kids.
Yeah that may be true, they may not know that.
But they've been shot at a whole lot more than most of you that are standing there talking
about your grouping at the range.
Why are they speaking up?
Because they know the adults aren't going to.
They know their parents aren't going to.
Not most of them.
Not most of them.
They know that the adults of this world are more concerned about protecting their party
than they are about protecting their kids.
Think back to that cave.
Those kids ran forward and we pulled them back.
Why?
Instinctual.
Even though that place is safe.
It is.
maybe there's uneven terrain ahead, maybe a rock's fallen, maybe something's
loose. You need to protect them because that's your primary mission in life as a parent.
But the adults of this country and of this world, they're not leading. They're not upfront to face
the danger. So the kids are stepping up, the teens are stepping up, the youth is stepping up. So
they're leading. Why are there so many kids in politics today espousing their
opinions? Because there are a whole lot of bad parents. And it's not necessarily
the parents of those speaking up.
I don't want to put too fine a point on that so let me pause for a second. If you
have kids and you haven't sat down and taken the hour or two that it takes to
get a pretty good grasp on climate change.
You're a bad parent.
You're a bad parent.
This is something that's going to affect your children.
If you haven't gone through and looked at it, without just
looking for things to support your side so you can win some
stupid internet debate, you're a bad parent.
You care more about donkeys and elephants
than your own kids.
That's pretty messed up.
That is pretty messed up.
You aren't working to protect your kids. You're leaving it to them to figure out.
Yeah, so of course they're going to make mistakes along the way.
But they're working. They're moving forward.
And they're ignoring you.
You know, that's the best part about the Republicans attacking her the way they did.
Because all those Fox News viewers, they're going to talk about it tonight.
And their kids are going to hear about it.
But see, their kids, they're not indoctrinated yet.
Not all of them.
Some of them.
They're going to hop online.
They're going to listen to that speech, and then they're going to want to know what she's
talking about, and they're going to look it up.
That's the funny part about this.
By the end of the week, Greta, she's going to be in your kid's head.
And your kid will betray your ideology in her name.
Because they're smarter.
Because they're more willing to learn.
Because they haven't given up hope.
Because they haven't sold out to some political party.
That's why there are kids in politics, because they see that the adults aren't doing anything.
By and large, there are exceptions.
You can talk about their handlers and you can make up whatever conspiracy theory you
want to.
It doesn't matter, it doesn't matter, because she's already in your kid's head, and you
put her there. You want a world without teens and political discussion. You can have it,
but you have to start reading and you have to start, well, you have to start putting
your kids above your political party. Something that shouldn't have to be said. It's funny.
You trust politicians to make decisions for your kids.
You don't trust them for anything else, but you're going to literally trust your children's
lives to them and to their political campaign contributors, because that's who's making
the decisions.
Yeah.
Yeah, I'm sure that's going to work out well.
If you want teens out of the political discussion,
you've got to step up.
You've got to start fulfilling your duties as parents.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}