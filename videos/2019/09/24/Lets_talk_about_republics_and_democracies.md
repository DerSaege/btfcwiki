---
title: Let's talk about republics and democracies....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=qYScSvpqZXU) |
| Published | 2019/09/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the difference between a republic and a democracy, addressing common misconceptions and why people draw that distinction.
- The United States is a democracy that happens to be a republic, not a direct democracy.
- Describes a republic as a representative democracy, different from a direct democracy.
- Talks about the misconception that a republic protects against mob rule, but points out that rights are not granted by the government.
- Emphasizes that rights existed before the government and were protected by the Bill of Rights due to historical government attacks.
- Warns against the dangerous idea that rights are always guaranteed, as amendments can be repealed.
- Challenges the notion that a republic is better than a direct democracy due to requiring a larger mob.
- Stresses the importance of an educated and informed citizenry for democracy to function properly.
- Rejects the idea that common folk are too dumb to make decisions, advocating for individual autonomy in decision-making.
- Encourages active participation to safeguard democracy in the face of modern challenges like misinformation on social media.

### Quotes

- "The United States is a democracy that happens to be a republic."
- "Rights were endowed by the Creator or are self-evident. They existed beforehand."
- "Democracy of any kind is going to be in danger as long as voters have the Facebook feeds they do."

### Oneliner

Beau explains the nuances between a republic and a democracy, stressing that democracy's survival hinges on an educated citizenry and active participation.

### Audience

Citizens, Voters, Educated Individuals

### On-the-ground actions from transcript

- Educate yourself and others on the differences between a republic and a democracy (implied).
- Actively participate in the democratic process by staying informed and engaged in political decisions (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the distinctions between a republic and a democracy, urging individuals to uphold democracy through education and active involvement.

### Tags

#Republic #Democracy #Rights #Education #CitizenEngagement


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're gonna talk about republics and democracies.
Why?
Because under that video
about exporting democracy on YouTube, there were a whole bunch of comments
about it.
And because there were a whole bunch of comments about it,
I got a whole bunch of messages about it.
Some people wanted to know what the difference was,
and some people were wondering why people were drawing that distinction.
To say that the United States is not a democracy, it's a republic,
that's false.
That isn't true.
The United States is a democracy that happens to be a republic.
Most people who say this know this already.
What they're really saying
is that we're not a direct democracy.
We are a republic, which is a representative democracy.
Now, to a lot of people, that's like me saying, hey,
you got a car?
I need a ride.
No, I don't have a car.
I have a Jeep.
So why do people like to draw the distinction?
When it's a republic, it gives the image
that it defeats the two strongest arguments
against democracy.
The weaker of the two arguments against democracy is that it is mob rule.
You know, two wolves and a sheep deciding what to have for dinner.
The strongest argument against democracy
is scrolling the average voter's Facebook feed.
When people say this, you will often see it in the
in the verbiage of
We have a constitutional republic, therefore it's not mob rule.
That lamb is safe.
Our rights are protected.
That little lamb is going to be okay.
Is it though?
I mean really, is it?
It's convenient fiction and it leads to a dangerous idea that rights are granted by
the government.
They're not.
The guys who wrote all this stuff said that rights were endowed by the Creator.
Or if you're an atheist, they are self-evident.
They existed beforehand, before the Bill of Rights.
And if you want an interesting read that's kind of off-topic, go take a look at the Ninth
Amendment and just give that a plain reading.
So the reason the Bill of Rights exists is that it specifically protected certain things
that governments of the time were known for attacking.
That's why it's there.
Because of that, people seem to think that those rights are always going to be there,
and therefore we're protected.
No, no, no, no, no.
The second you start believing that government granted those rights, it is mob rule.
It is mob rule.
You just need a bigger mob, amendments can be repealed.
Amendments can be repealed.
And because I said that, those that really like to draw that distinction are going to
say, well, you need a bigger mob, therefore it is better than a direct democracy.
There's nothing saying that a direct democracy can't require 80% of people vote in favor
of something prior to it becoming law and you need a bigger mob.
Do you though?
In most cases?
Sure with amendments, but in most cases do you?
You don't.
There's 153 or so million voters in the United States.
Do you think you could gather 80 million voters that would tell you that Congress
deserves the pay it gets, the benefits it gets, and should be able to take money from
special interest groups?
Probably not.
But it's law.
Because the mob is actually smaller.
It is smaller.
Now that's the first argument.
The first argument, it's not really true.
It is my rule.
Democracies of all kind are.
The idea is that democracy is advanced citizenship, to throw that term out there again.
You have to be educated.
You have to be informed.
And that's the second argument.
Inherent in the idea of a republic is that us common folk are stupid.
We're too dumb to make our own decisions, therefore we need a representative.
who will say, I know that's what you think you want, but let me weigh that out for you.
And we need to defer to our betters.
You know, people in the comments section ask a lot about me running for office.
Nah, I'm good.
I'm good.
I don't know how to run your life better than you.
Got no clue.
Wouldn't even know where to start.
I'd also like to keep what's left of my soul intact.
I don't think that's going to happen in D.C., so I would just rather abstain from the whole
process like that.
Anyway, that's a look at that discussion, republic versus democracy.
A republic is a form of democracy, it is a representative democracy.
There's a lot of bashing direct democracy, I'm not so sure it's well deserved.
At the end of the day though, democracy of any kind is going to be in danger as long
as voters have the Facebook feeds they do.
It's up to us.
If you want democracy to work, you have to work at it.
Anyway, it's just a thought.
y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}