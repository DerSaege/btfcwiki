---
title: Let's talk about Greta....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=TI-6x7GLwN8) |
| Published | 2019/09/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Woke up to news about Greta, a teenager leading the charge against climate change.
- Conservative and Republican pundits faced backlash for cyberbullying Greta.
- Expresses surprise at the backlash received by the pundits.
- Points out that bullying a teenager isn't the worst thing they've done.
- Mentions the lack of decency standards being held for the pundits.
- Reads comments on Twitter attacking Greta's character.
- Criticizes attacking Greta's looks and making fun of her hair.
- Comments on the ridiculousness of the attacks compared to discussing climate change mitigation ideas.
- Notes the hypocrisy of those accusing Greta of emotional child abuse.
- Points out the absurdity of attacking a teenager personally instead of discussing ideas.
- Acknowledges Greta's efforts in trying to save the world.
- Notes the irony of calling Greta a fear monger while promoting fear themselves.
- Encourages Greta to keep going despite the criticism.

### Quotes

- "She's better than you. So you try to attack her character."
- "Greta, you're judged in this life by your enemies more than your friends."

### Oneliner

Beau points out the hypocrisy and lack of decency in cyberbullying a teenager leading the fight against climate change.

### Audience

Social media users

### On-the-ground actions from transcript

- Show support for teenagers like Greta who are advocating for climate change action (implied)

### Whats missing in summary

The full transcript provides a detailed look at the cyberbullying faced by Greta and the importance of focusing on ideas rather than personal attacks.


## Transcript
Well, howdy there, Internet people, it's Bo again.
So I woke up this morning to news about Greta, for those of you who aren't familiar, and
that's the teenager who is currently leading the charge against climate change.
I woke up to the news that conservative and Republican pundits and commentators are surprised
By the backlash that they received for cyberbullying a teenager, I mean, who saw that come?
I mean, to be honest, I didn't.
I didn't know that we were holding them to any standards of decency whatsoever anymore.
I just thought we'd given up on them.
I just want to point out that objectively, them bullying a teenager like this, it's
not the worst thing they've done. Not by a long shot. I'm actually just happy they
haven't advocated invading Sweden to protect our freedoms or advocated hitting
her with a drone on behalf of the Saudis. I'm actually cool with where
it's at right now. They're just picking on her on Twitter and on their national
platforms. So I made the mistake of first thing in the morning scrolling
Twitter and reading some of these comments.
She's unhinged, they say, the people cyberbullying a teenager because she spoke.
She's unhinged, not done.
Are we not allowed to discuss her now that she's in the public domain?
sure that term doesn't mean what you think it means, but it would make more
sense to discuss the ideas, talk about ways to mitigate climate change, or I
mean God forbid discuss the science behind it, rather than attacking her
looks and making fun of her hair. To be honest that reads really creepy just
Just throwing that out there.
She's indoctrinated, say the people who honestly believe those in the Middle East hate us for
our freedoms.
You can't make this stuff up.
I'm worried about her because she is suffering emotional child abuse, says somebody who has
spent the last three years defending families being ripped apart and children
being thrown in cages and being emotionally, psychologically, and
physically abused. Yeah, I'm not buying that you actually care. It just seems
like a way to pick on her because you can't assail her ideas. She's hysterical.
It says the person using their national platform to attack a teenager on a personal level rather
than discussing the ideas.
Because you can't.
You can't discuss the ideas.
She's better than you.
So you try to attack her character.
Her emotional state, fine, maybe she's hysterical.
She's trying to save the world.
What's your excuse?
She's a fear monger, say the people who have spent the last three years trying to convince
Americans to cower and hide behind a wall because there are people with tans and accents
coming.
Greta, you're judged in this life by your enemies more than your friends.
You're doing just fine.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}