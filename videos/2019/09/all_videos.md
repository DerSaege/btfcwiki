# All videos from September, 2019
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2019-09-30: Let's talk about Biden, deplatforming Rudy, and Scooby-Doo.... (<a href="https://youtube.com/watch?v=uQH93_BfwK4">watch</a> || <a href="/videos/2019/09/30/Lets_talk_about_Biden_deplatforming_Rudy_and_Scooby-Doo">transcript &amp; editable summary</a>)

Beau explains how Scooby-Doo logic can reveal the true motives behind the administration's investigations, urging transparency for election integrity over political gains.

</summary>

"In the world of alternative facts, we're going to say it's all true."
"Every time he does, he reveals more."
"Your candidacy isn't as important as maintaining the integrity of the election process."

### AI summary (High error rate! Edit errors on video page)

Explains how Scooby-Doo logic can be applied to the administration's investigations into Joe Biden and his son.
Mentions Biden's aides asking news networks not to platform Rudy Giuliani.
Questions the approach of denying allegations and suggests embracing the world of alternative facts.
Talks about Hunter Biden's activities in Ukraine and speculates on unethical behavior without evidence of illegality.
Describes a scenario where Biden allegedly pressured the Ukrainian government to protect his son.
Criticizes the current administration for setting up investigations based on false information.
Points out inaccuracies regarding CrowdStrike's nationality.
Comments on the back channel investigation undermining U.S. intelligence.
Quotes Rudy Giuliani discussing the true motive behind the investigations.
Stresses the importance of maintaining election integrity over political candidacy.

Actions:

for politically engaged citizens,
Trust the American people to analyze evidence and prioritize election integrity (implied).
</details>
<details>
<summary>
2019-09-28: Let's talk about hearsay, credibility, and whistleblowers.... (<a href="https://youtube.com/watch?v=UM6YgRNxYnE">watch</a> || <a href="/videos/2019/09/28/Lets_talk_about_hearsay_credibility_and_whistleblowers">transcript &amp; editable summary</a>)

Republican senators attempting to discredit whistleblowers through hearsay will likely backfire given the significance of the information and the integrity of those involved.

</summary>

"His actions undermine the integrity of the system used to protect national security."
"The intelligence community that is so good at keeping secrets and keeping their mouths shut, those are the people blowing the whistle."

### AI summary (High error rate! Edit errors on video page)

Republican senators are trying to undermine the credibility of the whistleblower report by calling it hearsay.
The whistleblowers had access to highly classified information and are trusted individuals.
The senators, many of whom are lawyers, know that the whistleblower report is not hearsay.
Hearsay typically refers to statements intended to prove something, not entire reports or narratives.
The focus should be on the content of the report, not on disputing intent.
The Republican senators are treating this as a political scandal rather than a serious breach of security.
Overclassifying documents, as seen in this case, can be an attempt to obstruct justice.
The whistleblowers, who understand the significance of the information, are key players in this scandal.
The actions of the President undermine the integrity of the system designed to protect national security.
The intelligence community, experts at keeping secrets, are the ones raising the alarm on this issue.

Actions:

for senators, public,
Contact Senators to demand accountability for undermining the credibility of whistleblowers (implied)
Support transparency and accountability in government actions (implied)
</details>
<details>
<summary>
2019-09-27: Let's talk about young women and beauty standards.... (<a href="https://youtube.com/watch?v=8zaNWEVlzGM">watch</a> || <a href="/videos/2019/09/27/Lets_talk_about_young_women_and_beauty_standards">transcript &amp; editable summary</a>)

Beau addresses a disturbing school incident, criticizes toxic beauty standards, and advocates for recognizing intrinsic worth beyond superficial judgments.

</summary>

"Never let anybody tell you your worth."
"If you're not pretty, you're nothing. You're trophy. Trust me, it's not the guy you want."
"Never, never let a man determine your worth, because most men don't know value."

### AI summary (High error rate! Edit errors on video page)

Shifts focus from the White House to a disturbing incident at a school involving a 12-year-old girl.
Describes how the girl was allegedly assaulted on the playground, with racial slurs and degrading comments.
Mentions the school's strict regulations on marriage and homosexuality in contrast to its failure in regulating bullying.
Questions the toxic beauty standards imposed, particularly in the United States, and their impact on self-worth.
Encourages not to conform to unrealistic beauty standards and to recognize one's true worth beyond physical appearance.
Emphasizes that those who judge based on looks reveal more about their values than the person judged.
Criticizes the school's role in fostering intolerance and producing intolerant children.
Condemns the devaluation of individuals based on exploitable traits, evident from the incident.
Advocates for individuals not to let others determine their worth and to recognize intrinsic value beyond superficial standards.

Actions:

for students, parents, educators,
Contact the school administration to address bullying and intolerance (suggested)
Educate students on diversity and inclusion through workshops or programs (suggested)
</details>
<details>
<summary>
2019-09-26: Let's talk about whistleblowers and treason.... (<a href="https://youtube.com/watch?v=AuVTVhY0KEY">watch</a> || <a href="/videos/2019/09/26/Lets_talk_about_whistleblowers_and_treason">transcript &amp; editable summary</a>)

Beau clarifies misconceptions about treason and national security, stressing the administration's role in damaging intelligence efforts.

</summary>

"Words have definition. And treason doesn't mean did something the president doesn't like."
"The administration damaged national security. The administration aided spies, not the whistleblower."

### AI summary (High error rate! Edit errors on video page)

Beau addresses the LA Times report on the President's private breakfast where he questioned who provided the whistleblower with information, likening them to a spy.
He clarifies that those who shared the information were simply doing their jobs and providing oversight to Congress.
Beau explains that treason has a specific legal definition, and providing a whistleblower complaint is not treason.
He points out that damaging national security was not done by those who shared the information, but rather by the administration itself.
The over-classification of information can weaken the significance of true national security interests.
Beau asserts that the administration's actions have harmed national security and intelligence efforts.

Actions:

for concerned citizens, activists,
Contact your representatives to demand accountability and transparency in government actions (implied)
Educate others on the importance of protecting whistleblowers and upholding oversight committees (implied)
Join advocacy groups working to strengthen national security measures and protect intelligence gathering capabilities (implied)
</details>
<details>
<summary>
2019-09-25: Let's talk about the primaries, impeachments, and power.... (<a href="https://youtube.com/watch?v=KjF8SqP9veg">watch</a> || <a href="/videos/2019/09/25/Lets_talk_about_the_primaries_impeachments_and_power">transcript &amp; editable summary</a>)

Beau challenges the accepted corruption in politics, urging a shift towards local solutions and a rejection of deceitful politicians prioritizing power over truth.

</summary>

"Your solution is not in D.C., it's local."
"The fact that we don't realize that is why we're in trouble."
"That is corruption on a wide level."
"If that statement is true… none of them are fit to hold office."
"Because they tell us they're liars and cheats."

### AI summary (High error rate! Edit errors on video page)

Talks about the first Republican primary debates between Weld and Walsh, which most people missed.
Accuses the Republican party of rigging the election in favor of Donald Trump by shutting down primaries and not promoting debates.
Points out the party's support for Trump amid impeachment proceedings, predicting the Senate will never convict him.
Criticizes the lack of accountability and integrity in politics, focusing on the pursuit of power over truth and justice.
Emphasizes that the solution lies with local communities and individuals who genuinely believe in their values.
Challenges the notion that corruption in politics is a bug in the system rather than inherent to it.
Expresses concern over Americans being misled by corrupt politicians who prioritize self-interest over serving the people.
Condemns the Senate's potential refusal to hold the President accountable, questioning the fitness of all Republican senators for office.
Urges people to recognize the widespread deceit and corruption in politics rather than accepting it as normal.
Calls for a shift in mindset to acknowledge the truth about politicians' motives and representation of the public.

Actions:

for american voters,
Reach out to local communities and individuals who prioritize truth and justice (suggested)
Challenge corrupt practices within your local political sphere (exemplified)
Reject the normalization of deceit and corruption in politics (implied)
</details>
<details>
<summary>
2019-09-24: Let's talk about republics and democracies.... (<a href="https://youtube.com/watch?v=qYScSvpqZXU">watch</a> || <a href="/videos/2019/09/24/Lets_talk_about_republics_and_democracies">transcript &amp; editable summary</a>)

Beau explains the nuances between a republic and a democracy, stressing that democracy's survival hinges on an educated citizenry and active participation.

</summary>

"The United States is a democracy that happens to be a republic."
"Rights were endowed by the Creator or are self-evident. They existed beforehand."
"Democracy of any kind is going to be in danger as long as voters have the Facebook feeds they do."

### AI summary (High error rate! Edit errors on video page)

Explains the difference between a republic and a democracy, addressing common misconceptions and why people draw that distinction.
The United States is a democracy that happens to be a republic, not a direct democracy.
Describes a republic as a representative democracy, different from a direct democracy.
Talks about the misconception that a republic protects against mob rule, but points out that rights are not granted by the government.
Emphasizes that rights existed before the government and were protected by the Bill of Rights due to historical government attacks.
Warns against the dangerous idea that rights are always guaranteed, as amendments can be repealed.
Challenges the notion that a republic is better than a direct democracy due to requiring a larger mob.
Stresses the importance of an educated and informed citizenry for democracy to function properly.
Rejects the idea that common folk are too dumb to make decisions, advocating for individual autonomy in decision-making.
Encourages active participation to safeguard democracy in the face of modern challenges like misinformation on social media.

Actions:

for citizens, voters, educated individuals,
Educate yourself and others on the differences between a republic and a democracy (implied).
Actively participate in the democratic process by staying informed and engaged in political decisions (implied).
</details>
<details>
<summary>
2019-09-24: Let's talk about family values and climate change.... (<a href="https://youtube.com/watch?v=lQr1SqB0jEo">watch</a> || <a href="/videos/2019/09/24/Lets_talk_about_family_values_and_climate_change">transcript &amp; editable summary</a>)

Beau criticizes adults for failing to guide youth and encourages parents to prioritize education over political allegiance to empower the next generation.

</summary>

"You aren't working to protect your kids. You're leaving it to them to figure out."
"Your kid will betray your ideology in her name. Because they're smarter. Because they're more willing to learn."
"If you want teens out of the political discussion, you've got to step up."

### AI summary (High error rate! Edit errors on video page)

Recounts an experience in a cave with his family and a Spanish-speaking family.
Observes young kids charging ahead during a guided cave tour.
Criticizes adults who dismiss teenagers' political opinions.
Points out the lack of guidance offered to youth about education.
Expresses disagreement with the conclusions of the Parkland teens but respects their effort.
Compares Parkland teens to adults making jokes about guns on social media.
Emphasizes that youth speak up because adults won't.
Draws a parallel between cave instinct and adults not leading in society.
Calls out parents who neglect educating themselves and their kids about climate change.
Condemns parents who prioritize politics over their children's future.
Encourages parents to prioritize educating their children over political allegiance.
Talks about the impact of Greta Thunberg's speech on children's awareness.
Urges parents to prioritize their children over political parties for a better future.

Actions:

for parents, adults,
Educate yourself on climate change and other critical issues affecting your children's future (suggested)
Prioritize educating your children over political allegiance (suggested)
Encourage open-mindedness and learning in your children (suggested)
</details>
<details>
<summary>
2019-09-24: Let's talk about Greta.... (<a href="https://youtube.com/watch?v=TI-6x7GLwN8">watch</a> || <a href="/videos/2019/09/24/Lets_talk_about_Greta">transcript &amp; editable summary</a>)

Beau points out the hypocrisy and lack of decency in cyberbullying a teenager leading the fight against climate change.

</summary>

"She's better than you. So you try to attack her character."
"Greta, you're judged in this life by your enemies more than your friends."

### AI summary (High error rate! Edit errors on video page)

Woke up to news about Greta, a teenager leading the charge against climate change.
Conservative and Republican pundits faced backlash for cyberbullying Greta.
Expresses surprise at the backlash received by the pundits.
Points out that bullying a teenager isn't the worst thing they've done.
Mentions the lack of decency standards being held for the pundits.
Reads comments on Twitter attacking Greta's character.
Criticizes attacking Greta's looks and making fun of her hair.
Comments on the ridiculousness of the attacks compared to discussing climate change mitigation ideas.
Notes the hypocrisy of those accusing Greta of emotional child abuse.
Points out the absurdity of attacking a teenager personally instead of discussing ideas.
Acknowledges Greta's efforts in trying to save the world.
Notes the irony of calling Greta a fear monger while promoting fear themselves.
Encourages Greta to keep going despite the criticism.

Actions:

for social media users,
Show support for teenagers like Greta who are advocating for climate change action (implied)
</details>
<details>
<summary>
2019-09-23: Let's talk about a teen questioning spreading democracy.... (<a href="https://youtube.com/watch?v=1nhsQAYa-E4">watch</a> || <a href="/videos/2019/09/23/Lets_talk_about_a_teen_questioning_spreading_democracy">transcript &amp; editable summary</a>)

A 15-year-old's profound query on democracy prompts Beau to reveal contradictions in US actions, urging critical thinking and questioning narratives with gaps.

</summary>

"Democracy is advanced citizenship, man. It's hard, you gotta want it."
"Democracy doesn't matter. If it did, we wouldn't be doing that."
"Most of them have holes you can walk through."
"You really should. You are thinking at a level far beyond what is expected of you."
"Democracy in retreat."

### AI summary (High error rate! Edit errors on video page)

A 15-year-old asks why the US government wants countries to be democratic, questioning if democracy can be forced.
Beau praises the question as advanced thinking for a teenager, stating that democracy requires active participation and desire.
He mentions that the US has a powerful war machine and has been spreading democracy globally.
Beau questions if the US truly spreads democracy or if it's just a narrative.
Referring to a study by Rich Whitney, Beau reveals that the US provides military aid to many dictatorships, contradicting the promotion of democracy.
He explains that intelligence operations prioritize American interests over idealistic values like democracy.
Beau points out that stability is paramount for the US, sometimes conflicting with the establishment of democracies.
Democracy is described as being in retreat, as per the 2019 Freedom House report.
Beau encourages critical thinking and questioning narratives with holes.

Actions:

for youth, critical thinkers,
Question narratives and seek the truth (implied)
Advocate for transparency in government actions (implied)
Engage in critical thinking and analyze information critically (implied)
</details>
<details>
<summary>
2019-09-23: Let's talk about Trump's move to protect historic sites.... (<a href="https://youtube.com/watch?v=Nh6bexLk9gU">watch</a> || <a href="/videos/2019/09/23/Lets_talk_about_Trump_s_move_to_protect_historic_sites">transcript &amp; editable summary</a>)

Beau criticizes the president's proposal to protect religious freedom, calling it a con and pointing out his history of undermining similar initiatives.

</summary>

"It's a con."
"There's nothing new in this. There's nothing visionary in this."
"He realizes he's losing. He realizes that people are turning against him, realizing that he's a con."
"If he actually cared about any of this stuff, he could have acted on it any time within the last three years."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Addresses the president's proposal to protect religious freedom by safeguarding religious sites, including those of historical significance.
Supports the idea of protecting religious and cultural sites, expanding beyond just religious places.
Mentions the existing organization UNESCO, which already works on this concept.
Criticizes the president for pulling out of UNESCO and undermining global efforts to protect cultural and religious sites.
Points out the importance of protecting religious freedom for individuals, as outlined in the Universal Declaration on Human Rights.
Criticizes the president for undermining human rights through relationships with dictators.
Calls out the president for failing to uphold treaty obligations regarding the treatment of refugees.
Labels the president's proposal as a con, stating that it is nothing new and that he has undermined similar initiatives throughout his administration.
Acknowledges the importance of protecting historical and cultural sites but criticizes the president for only now showing interest due to public backlash.
Concludes by suggesting that the president's recent actions are merely attempts to deceive the public in light of his declining popularity.

Actions:

for policy analysts,
Support UNESCO's efforts to protect cultural and religious sites (exemplified)
Advocate for upholding treaty obligations regarding the treatment of refugees (exemplified)
</details>
<details>
<summary>
2019-09-22: Let's talk about opinions on Trudeau and Yang.... (<a href="https://youtube.com/watch?v=4XwMiPZVrpA">watch</a> || <a href="/videos/2019/09/22/Lets_talk_about_opinions_on_Trudeau_and_Yang">transcript &amp; editable summary</a>)

Beau addresses controversies around Trudeau's blackface and Yang's stereotypes, urging to prioritize the voices of those directly impacted over irrelevant opinions in societal discourse.

</summary>

"Blackface is bad, okay? Southern American blackface is evil. It is evil."
"If Jimmy insults Suzie, you don't ask Billy if the apology was sufficient."
"Sometimes it's better to get the opinion of people that are actually involved."
"Not everything requires your personal response."
"Your personal response may drown out the opinions of those people that need to be heard more."

### AI summary (High error rate! Edit errors on video page)

Addresses questions about Trudeau and Yang, both involving a common component.
Mentions the controversy surrounding Trudeau's blackface incident and the apology.
Describes Southern American blackface as evil due to its malicious nature and historical context.
Comments on the relevance of his opinion regarding Trudeau's apology, suggesting the focus should be on those directly impacted.
Shares responses from people of color regarding Trudeau, including differing opinions on his actions and apology.
Emphasizes the importance of understanding varying perspectives within impacted communities.
Responds to questions about Yang and stereotypes, particularly regarding Asians excelling in math and becoming doctors.
Advocates for seeking the opinions of those directly involved in a situation rather than assuming all opinions carry equal weight.
Critiques the societal pressure to have an opinion on everything in the age of social media.
Encourages considering whose voices need to be amplified and heard more in various situations.

Actions:

for social media users,
Reach out to and amplify the voices of those directly impacted by controversies rather than focusing on irrelevant opinions (implied).
Engage in meaningful dialogues with communities affected by stereotypes to understand their perspectives (implied).
</details>
<details>
<summary>
2019-09-22: Let's talk about one of my favorite weeks.... (<a href="https://youtube.com/watch?v=r1w9eF3Egxo">watch</a> || <a href="/videos/2019/09/22/Lets_talk_about_one_of_my_favorite_weeks">transcript &amp; editable summary</a>)

Beau introduces Banned Books Week, encourages reading banned books to uncover truths and controversial ideas independently, and offers an alternative book club through an Amazon influencer store.

</summary>

"Fiction gets to truth sometimes a whole lot faster than fact does."
"Truth isn't told, it's realized on your own."
"Go find a book that somebody doesn't want you to read."
"It's just you in the book."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Introduces Banned Books Week, celebrating books like "Of Mice and Men," "Catcher in the Rye," "To Kill a Mockingbird," and others that have been banned or challenged in libraries and schools.
Encourages people to pick up one of the banned books to learn about the norms and controversies of the time they were written.
Points out that fiction can sometimes reveal truth faster than facts and expose readers to controversial ideas in a non-debate setting.
Expresses the importance of individuals realizing truth on their own while reading, without outside influence or defense mechanisms.
Explains his alternative to a book club by setting up an Amazon influencer store where viewers can access book lists without being priced out, supporting emergency preparedness gear as well.
Invites viewers to participate by recommending books for the store, promoting engagement and collective reading.
Urges viewers to participate in Banned Books Week by reading a book that someone doesn't want them to read, encouraging exploration of diverse perspectives.

Actions:

for book lovers, advocates of free speech,
Visit Beau's Amazon influencer store to access book lists and support emergency preparedness gear (suggested)
Recommend books in the comments for Beau's store, promoting collective reading (suggested)
</details>
<details>
<summary>
2019-09-21: Let's talk about Upton Sinclair and your next Bacon purchase.... (<a href="https://youtube.com/watch?v=fmsdS9f0M4A">watch</a> || <a href="/videos/2019/09/21/Lets_talk_about_Upton_Sinclair_and_your_next_Bacon_purchase">transcript &amp; editable summary</a>)

Beau warns of the dangerous implications of allowing slaughterhouses to self-police, prioritizing profits over consumer safety.

</summary>

"It's going to make it more profitable for them to staff the safety inspectors than it is for the government to do it."
"In my ideal world, yeah companies like this would police themselves, but in my ideal world they'd police themselves and we know that isn't what's going to happen here."
"It's something that we might want to keep in mind."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic of Upton Sinclair, an American author known for writing almost 100 books and winning the Pulitzer Prize for fiction.
Upton Sinclair's books, often journalism disguised as fiction, had a significant impact in the United States.
"The Jungle," one of Sinclair's famous works, written in 1906 about the meatpacking industry, led to the passing of the Pure Food and Drug Act and the Meat Inspection Act.
Upton Sinclair is credited with being a driving force behind better policing in slaughterhouses due to his impactful book.
The USDA is removing prohibitions on line speed in slaughterhouses, allowing them to self-police, which can have detrimental effects on food safety.
The industry, not just Trump, has been lobbying for removing regulations on line speed for profitability reasons.
Despite past failures in pilot programs, the plan to let slaughterhouses develop their own standards is still moving forward.
Companies prioritizing profits over safety may lead to a rise in foodborne illnesses from pork.
Beau questions the effectiveness of companies self-policing and raises concerns about potential consequences.

Actions:

for food safety advocates,
Contact local representatives to advocate for stricter regulations on slaughterhouses (implied)
Join or support organizations that focus on food safety and regulation (implied)
</details>
<details>
<summary>
2019-09-20: Let's talk about the Climate Strike, hope, Trudeau, and stepping stones.... (<a href="https://youtube.com/watch?v=9QmxW4YUWSc">watch</a> || <a href="/videos/2019/09/20/Lets_talk_about_the_Climate_Strike_hope_Trudeau_and_stepping_stones">transcript &amp; editable summary</a>)

Trudeau's scandal and the global climate strike inspire hope by uniting millions across borders for positive change, signaling a shift towards global unity and activism.

</summary>

"Man, that's cool. Gives me hope."
"They're thinking about the planet as a whole."
"Millions of people tomorrow will be acting together because they started thinking broader than a border."
"We might one day see that world I want."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Trudeau's scandal sparked hope in unexpected ways.
Beau's son questioned the appropriateness of blackface in 2001, leading to a hopeful realization.
Young American teens discussing Canadian politics signifies a positive change.
The global climate strike, with millions participating, is seen as the largest protest in history.
Companies granting employees paid time off for the climate strike indicates a shift towards social responsibility.
Grassroots movements unite millions globally for a common cause, transcending borders.
Technology allows today's youth to connect and mobilize internationally, diminishing nationalism.
The upcoming generation is less likely to be divided by nationalism and xenophobia.
Individuals are coming together across borders to urge governments to act on climate change.
Millions are thinking beyond borders and considering the planet as a whole, paving the way for a borderless mindset.

Actions:

for global citizens,
Join or support grassroots movements for climate action (implied)
Connect with individuals internationally to advocate for global change (exemplified)
</details>
<details>
<summary>
2019-09-19: Let's talk about a message to the Republican Party.... (<a href="https://youtube.com/watch?v=_zRjUCY3KRc">watch</a> || <a href="/videos/2019/09/19/Lets_talk_about_a_message_to_the_Republican_Party">transcript &amp; editable summary</a>)

Beau delivers a message to the Republican Party, expressing disappointment in their stances on economics, military involvement, gun control, and values, signaling a growing dissatisfaction among individuals like him.

</summary>

"I certainly don't think that come next fall I should see a gold star in front of his parents' house because you guys want to increase your stock portfolios."
"And the other 60 to 70%, I can answer that in one sentence, I don't care about somebody's skin tone, country of origin, religion, gender, orientation, or the language they speak."
"The reality is now, there's a whole bunch of guys, look like me, sound like me, real sick of you."
"But I believe they believe it. That's more than I can say for you."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Sends a message to the Republican Party as a blue-collar, rural, swing state resident.
Expresses disappointment in the economic platform and stance on free trade and trickle-down economics.
Questions the military involvement with Saudi Arabia and the arms sales to them.
Criticizes the Republican Party for not upholding Second Amendment rights while enacting more gun control measures under the current administration.
Calls out the GOP for canceling primaries, supporting the current administration's actions, and diverting funds from critical projects.
Emphasizes that he doesn't base his opinions on skin tone, origin, religion, gender, orientation, or spoken language.
States that he is not a bigot and focuses on judging individuals based on character rather than demographics.
Points out a growing dissatisfaction among individuals like him with the Republican Party.
Acknowledges differences in beliefs with progressive figures like AOC and the squad but respects their authenticity.
Concludes with a note of concern for the Republican Party's loss of support from individuals like him.

Actions:

for blue-collar voters,
Reach out and connect with disillusioned individuals in your community who share similar concerns with political parties (implied).
Engage in open dialogues with individuals holding diverse viewpoints to understand their perspectives and concerns (implied).
</details>
<details>
<summary>
2019-09-18: Let's talk about the Trump cheerleaders.... (<a href="https://youtube.com/watch?v=l0K0U3F5IGY">watch</a> || <a href="/videos/2019/09/18/Lets_talk_about_the_Trump_cheerleaders">transcript &amp; editable summary</a>)

Beau explains the controversy around Trump-supporting cheerleaders, advocating for meaningful political discourse in schools within the bounds of free speech.

</summary>

"Students do not shed their rights at schoolhouse gate, period."
"Given the fact that they were in uniform, yeah, that's probably fitting."
"I think that is something that needs to change and in the meantime we need to defend what little ability students have to discuss politics at school."
"We have to defend it as much as we can within the bounds of a civilized society."
"Whether or not I agree with it shouldn't matter."

### AI summary (High error rate! Edit errors on video page)

Responds to a message about Trump cheerleaders being punished for displaying a banner supporting Trump 2020 before a game at North Stanley High School in North Carolina.
North Stanley High School has a policy against political signs, but the issue is whether the banner was disruptive to the school, not the political nature of the sign.
Points out that students have rights at school, and the test is whether their actions are disruptive.
Explains that the cheerleaders, as representatives of a government entity receiving government money, can't display political messages while in uniform.
Clarifies that the punishment for the cheerleaders was probation, meaning they were simply told not to do it again and faced no significant consequences.
Emphasizes that allowing students to have meaningful political discourse in schools is vital to avoid the rise of candidates like Trump.
Encourages open, non-confrontational discourse to understand why the cheerleaders support Trump.
Advocates for defending free speech within the bounds of a civilized society, even if one disagrees with the message being conveyed.

Actions:

for students, educators, community members,
Contact the cheerleaders for non-confrontational political discourse (suggested)
</details>
<details>
<summary>
2019-09-18: Let's talk about the First Amendment.... (<a href="https://youtube.com/watch?v=eJmh0qql0FA">watch</a> || <a href="/videos/2019/09/18/Lets_talk_about_the_First_Amendment">transcript &amp; editable summary</a>)

Beau breaks down the misconception around free speech, exposing the far right's desire for a platform over true freedom of expression, critiquing their attempts to dictate speech through government intervention.

</summary>

"The far right doesn't want speech. They don't want free speech. They want a platform."
"You don't need the government's gun. You better let this guy talk."
"A good idea will find a platform. Doesn't need one handed to him."
"You can be deplatformed and if the idea is solid, another platformer will arise where you can create your own."
"That's the right and y'all are utter failures at it."

### AI summary (High error rate! Edit errors on video page)

People were mad about squelching free speech rights, exemplified by criminalizing pipeline protests in red states and Trump's suggestion to strip citizenship for flag burning.
Republicans support making it illegal to burn the flag, prohibiting offensive statements against police or military, and banning face coverings.
Republicans show less support for extending free speech courtesies to the LGBTQ community.
Republicans favor stopping construction of new mosques and arresting hecklers rather than disciplining them.
Republicans believe the press has too much freedom and express a negative view towards journalists.
Beau dismisses claims of a free speech crisis on campuses, noting that professors being fired for political speech are mostly liberals, not conservatives.
Beau explains that universities provide platforms for dissenting viewpoints, and students protesting speeches is protected by the First Amendment.
The far right seeks a platform, not free speech, and universities are more accepting of dissenting viewpoints.
Free speech means the right to speak, not entitlement to a platform or equal time for all ideas.
Beau criticizes the far right for wanting to use the government to dictate speech and actions, contrasting it with the principles of free speech and dissent.

Actions:

for activists, free speech advocates,
Stand up for true free speech by advocating for platforms for diverse voices (implied).
Educate others on the distinction between a platform and free speech (implied).
Support institutions that uphold principles of free speech and dissent (implied).
</details>
<details>
<summary>
2019-09-17: Let's talk about what Judd Gregg said Trump was.... (<a href="https://youtube.com/watch?v=xrW2wLqOxJk">watch</a> || <a href="/videos/2019/09/17/Lets_talk_about_what_Judd_Gregg_said_Trump_was">transcript &amp; editable summary</a>)

Beau expresses support for Trump, refutes claims of socialism in Trump's policies, and questions the article's assertions about the President's core policies, ending with a thought for the audience to ponder.

</summary>

"He is not helping to make America great."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Beau expresses his longstanding support for President Trump and his surprise at an article labeling Trump as socialist.
He questions the characterization of Trump's policies as socialist and sees them more as a blend of corporate and government interests.
Beau becomes defensive of Trump, dismissing the idea that his policies are socialist and getting upset at the article's claims.
He reacts strongly to the suggestion that Trump lacks core policies, defending Trump's nationalist stance.
Beau ends by leaving it as a thought for his audience, questioning the claims made in the article.

Actions:

for trump supporters,
Question media narratives and do independent research to form your own opinions (implied).
</details>
<details>
<summary>
2019-09-17: Let's talk about President Trump being wishy-washy.... (<a href="https://youtube.com/watch?v=8iOi5yCrg4Q">watch</a> || <a href="/videos/2019/09/17/Lets_talk_about_President_Trump_being_wishy-washy">transcript &amp; editable summary</a>)

Trump's wishy-washy stance stems from his own ignorance, while complex geopolitics challenge his diplomatic footing, leaving no easy way out.

</summary>

"What the American public doesn't know is what makes them the American public."
"Geopolitics in the region is a little bit more complicated than building a golf course."
"There's no reason American lives should be risked and wasted because they didn't fulfill their duties."
"The American public needs running the country and the largest war machine on the planet."
"It may just be a reminder that the American public needs running the country."

### AI summary (High error rate! Edit errors on video page)

Trump is wishy-washy due to his own making and lack of intelligence reports.
Multiple past incidents were ignored due to not appearing on Fox News.
The situation involving Iran and Saudi Arabia is complex and requires a nuanced understanding.
Scrapping previous deals with Iran puts Trump in a difficult diplomatic position.
Talking points blaming Iran for supplying weaponry are weak due to US support of Saudis.
Geopolitics in the region is intricate, contrasting with Trump's usual dealings.
Blaming Saudis for failing to protect critical infrastructure could be Trump's way out.
Lack of sharp advisors on Trump's staff poses a challenge.
Uncertainty looms over future actions and viable options for the administration.
Running a country differs greatly from selling a brand, hinting at the responsibilities at hand.

Actions:

for political analysts,
Contact political analysts to provide insights and strategies (suggested)
Join organizations advocating for informed decision-making in geopolitics (implied)
</details>
<details>
<summary>
2019-09-16: Let's talk about being edgy and offending people.... (<a href="https://youtube.com/watch?v=SF7m9xf4To8">watch</a> || <a href="/videos/2019/09/16/Lets_talk_about_being_edgy_and_offending_people">transcript &amp; editable summary</a>)

Beau believes in thoughtful speech over shock value, stresses common courtesy, and plans community workshops.

</summary>

"Being edgy is not a substitute for having a personality."
"It's almost always better to be nice."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Expresses no desire to change speech despite Democratic candidates' coarsening.
Believes in reaching people with thought, not shock value.
Views being edgy as a poor substitute for personality.
Shares an incident on Twitter where he misunderstood a tweet and sought clarification.
Emphasizes the importance of common courtesy and avoiding intentional offense.
Stresses the value of reaching out for understanding.
Mentions starting a podcast with reruns of older videos to introduce himself to the community.
Talks about potential sponsored videos, stating they must fit the channel's theme.
Mentions plans for workshops in the South to build community networks and possibly going on the road next summer.

Actions:

for content creators,
Set up workshops to help build community networks (planned)
Attend workshops to learn from others (planned)
</details>
<details>
<summary>
2019-09-15: Let's talk about Smokey Bear and fear.... (<a href="https://youtube.com/watch?v=HlOydP2DASE">watch</a> || <a href="/videos/2019/09/15/Lets_talk_about_Smokey_Bear_and_fear">transcript &amp; editable summary</a>)

Beau celebrates Smokey the Bear's birthday, encounters different animals with his son, and shares insights on overcoming fear through education to reduce conflict globally.

</summary>

"Most fear can be killed with education."
"That kind of fear is a gift, use it wisely."
"If everybody did that, there'd be less fear. There'd be less fear in the world."

### AI summary (High error rate! Edit errors on video page)

Took his kids to the Tallahassee Museum to celebrate Smokey the Bear's 75th birthday.
Smokey the Bear is a fire prevention mascot born during World War II when firefighters joined the military.
The museum is outdoors with animal enclosures, including wolves, a Florida Panther, and a bear.
Beau's son wanted to play with the bear despite knowing they are dangerous animals.
Encountered a person dressed as Smokey the Bear, and Beau's son was scared due to unfamiliarity.
Beau talks about different kinds of fear: rational fear, irrational fear, and subconscious fear.
Education can help overcome fear by learning and understanding the subject.
Beau encourages researching things we fear to eliminate that fear and reduce conflict in the world.

Actions:

for parents, educators, individuals,
Research something you fear to understand and overcome it (implied).
</details>
<details>
<summary>
2019-09-14: Let's talk about getting an education from Roo.... (<a href="https://youtube.com/watch?v=TD-kywHwzLQ">watch</a> || <a href="/videos/2019/09/14/Lets_talk_about_getting_an_education_from_Roo">transcript &amp; editable summary</a>)

An educational chatbot fills the void left by parents and schools in providing vital information and support to teens, reflecting broader societal failures in communication and education.

</summary>

"Teens don't make the wisest decisions even when they have all the information."
"Teens are turning to the internet, turning to a chat bot to get advice that they should very, that they should feel comfortable getting from their parents."
"This is not a topic that you want teens making decisions without all the information."

### AI summary (High error rate! Edit errors on video page)

Introduces an educational chatbot named Roo that provides information many parents are uncomfortable discussing.
Only 24 states in the U.S. require comprehensive sexual education, with just 13 of those states mandating medically accurate content.
Roo is capable of answering a wide range of questions, from puberty to relationship advice, and has had over a million interactions in nine months.
Acknowledges that many teens turn to Roo because they lack someone in their lives to talk to about sensitive topics.
Emphasizes the importance of open communication between parents and children to prevent misinformation and poor decision-making.
Criticizes the irony of individuals opposing funding for organizations like Roo while their own children benefit from its services.
Views the reliance on Roo for critical information as a reflection of societal failures in providing adequate education and support for teens.
Stresses the significance of education in empowering young people to make informed choices and navigate complex issues.

Actions:

for parents, educators, policymakers,
Support and advocate for comprehensive sexual education in all states (implied)
Encourage open communication with children on sensitive topics (implied)
Promote resources like educational chatbots for teens lacking support systems (implied)
</details>
<details>
<summary>
2019-09-13: Let's talk about your obligation to humanity.... (<a href="https://youtube.com/watch?v=TuSUycij6jU">watch</a> || <a href="/videos/2019/09/13/Lets_talk_about_your_obligation_to_humanity">transcript &amp; editable summary</a>)

Beau challenges nationalism, advocating for helping beyond borders and finding common ground with those in need.

</summary>

"Your obligation to humanity does not end at the border."
"We have to start thinking broader than a border."
"Those colors. So much of what you believe is a person and how you identify yourself, all hinges on what colors are flying on that flagpole outside of the hospital you were born in."
"Me and that guy from the Bahamas, oh, we sit down and have a drink. We can relate on a meaningful level because we're kind of the same."
"Your obligation to humanity does not end at the border."

### AI summary (High error rate! Edit errors on video page)

Responds to comments about helping the Bahamas, questioning why we can't help our own people.
Believes in helping those in need regardless of skin tone, religion, or cultural differences.
Challenges the idea of "othering" people based on nationality or location.
Compares his relatability to a person from the Bahamas over politicians on Capitol Hill.
Encourages thinking beyond borders and nationalism.
Compares nationalism to gang mentality, questioning the difference.
Emphasizes the commonalities between people born in different places rather than their differences.
Stresses the importance of humanity and helping beyond borders.

Actions:

for global citizens,
Connect with individuals from different countries to understand their experiences (implied)
Challenge nationalist rhetoric and stereotypes in your community (implied)
Volunteer or donate to organizations supporting international aid efforts (implied)
</details>
<details>
<summary>
2019-09-12: Let's talk about the Bahamas and talking points.... (<a href="https://youtube.com/watch?v=U-trVlTIB14">watch</a> || <a href="/videos/2019/09/12/Lets_talk_about_the_Bahamas_and_talking_points">transcript &amp; editable summary</a>)

Beau details the devastation in the Bahamas post-Cat 5 hurricane, criticizing the Trump administration for refusing aid and urging people to pressure representatives for help.

</summary>

"This is a humanitarian disaster of biblical proportions."
"We are abandoning our responsibilities as human beings."
"We are leaving people to die."
"If you want to help, you need to put pressure on your representatives."
"It's that simple."

### AI summary (High error rate! Edit errors on video page)

The Bahamas were hit by a Cat 5 hurricane, with winds up to 220 miles per hour and a storm surge of 20 feet, causing widespread destruction.
Estimates indicate 70,000 people are homeless and displaced, a significant portion of the Bahamas' 400,000 population.
Thousands are missing, and the human toll is still unknown.
Despite the massive scale of the disaster, the Trump administration refuses to grant temporary protected status to the victims.
Beau draws parallels to the administration's treatment of refugees at the southern border, where they are also denied refugee status.
He criticizes the lack of empathy and humanitarian response, accusing the administration of fostering a climate that denies help to those in need.
Beau points out that the administration could use parole to admit those affected without full documentation, given the circumstances.
He expresses dismay at the abandonment of responsibilities towards fellow humans and the disregard for basic human decency.
Beau condemns the administration's actions as un-American, unprecedented, and disgraceful.
He calls for people to pressure their representatives to take action and provide assistance to the Bahamas.
Beau urges against merely spending money at major resorts but rather directs people to seek ways to help through bahamas.com/relief and by contacting their representatives.

Actions:

for global citizens,
Pressure representatives to provide assistance (suggested)
Support relief efforts through bahamas.com/relief (suggested)
</details>
<details>
<summary>
2019-09-12: Let's talk about superstitions, Jurassic Park, dreaming, and rhinos.... (<a href="https://youtube.com/watch?v=pauyvIE8tQc">watch</a> || <a href="/videos/2019/09/12/Lets_talk_about_superstitions_Jurassic_Park_dreaming_and_rhinos">transcript &amp; editable summary</a>)

Beau shares the hopeful story of reviving the Northern White Rhino and suggests that with the same mentality, humanity can solve various global issues alongside saving species.

</summary>

"It's amazing. It's amazing and it gives me hope for a lot of the other problems we have in this world."
"We produce more food than we need. We just got to figure out how to get it where it needs to be."
"We're an advanced species. If we can believe in superstitions like this, and this kind of technology, if we can do both of those at the same time, we can certainly work on world hunger, disease, climate change, homelessness, all of this stuff, and save the rhinos."

### AI summary (High error rate! Edit errors on video page)

The Northern White Rhino is basically extinct, with only two left - a mother and daughter.
Scientists harvested eggs from the rhinos and created embryos from frozen samples of male northern white rhinos.
Two embryos have been created and frozen in liquid nitrogen to be transported and inserted into a southern white rhino surrogate mom to bring back the species.
The reason behind the extinction of these rhinos is the demand for their horns, believed to have magical healing powers due to superstition.
Despite this ancient superstition, efforts are being made to revive the species, showcasing the strange yet hopeful nature of humans.
Beau draws parallels between this conservation effort and solving other global issues like world hunger, disease, and homelessness.
He believes that with the same mentality applied to food distribution, these problems can also be solved.
Beau advocates for addressing multiple issues simultaneously, suggesting that as an advanced species, humans can work on conservation and other challenges concurrently.
He expresses hope and confidence in humanity's ability to tackle various issues if there is a collective desire to do so.
Beau encourages maintaining dreams and optimism in the face of challenges.

Actions:

for conservation enthusiasts,
Support conservation efforts (exemplified)
Advocate for addressing global issues simultaneously (implied)
</details>
<details>
<summary>
2019-09-12: Let's talk about North Carolina.... (<a href="https://youtube.com/watch?v=YeW5fTcUR68">watch</a> || <a href="/videos/2019/09/12/Lets_talk_about_North_Carolina">transcript &amp; editable summary</a>)

House Republicans in North Carolina attempt a budget veto override during a 9/11 memorial, exposing a pattern of subverting democracy and sparking the decline of the party.

</summary>

"They went out of their way to subvert the democratic process."
"You're watching the death of a majority."
"These representatives are more dangerous than any terrorist."

### AI summary (High error rate! Edit errors on video page)

Yesterday marked an anniversary nobody celebrates but acknowledges, reflecting on changes and perspectives shaped by political beliefs.
House Republicans in North Carolina attempted a veto override of the budget while Democrats were at a 9/11 memorial.
Republicans in North Carolina have a history of subverting democracy to maintain power.
Despite past instances of gerrymandering being struck down, Republicans continue to manipulate districts.
The passing of the override in North Carolina is seen as an attack on democracy.
Deb Butler's passionate attempt to stop the override made headlines.
The passing of the budget override signals the decline of the Republican Party in North Carolina.
Beau predicts the Republican Party's decline due to fairer districts in the future.
Beau compares the actions of representatives in North Carolina to those who perpetrated the 9/11 attacks, stressing the danger posed by political figures.
Beau ends by remarking on the power politicians hold in shaping the country's foundation.

Actions:

for north carolina residents,
Support fair districting in North Carolina (implied)
Stay informed and engaged in local politics (implied)
</details>
<details>
<summary>
2019-09-11: Let's talk about ending higher education and Tennessee Senator.... (<a href="https://youtube.com/watch?v=okB22EuoKgA">watch</a> || <a href="/videos/2019/09/11/Lets_talk_about_ending_higher_education_and_Tennessee_Senator">transcript &amp; editable summary</a>)

Tennessee Senator calls for abolishing higher education, believing it breeds liberalism, while overlooking policy reflections and opting for uneducated workforce to bolster GOP control.

</summary>

"The stupid stuff that our kids are being taught is absolutely ridiculous."
"Rather than take that information and say, hey, are our policies outdated? Have we lost touch? Have we not kept up with the growth of new information in the world? No, we need to ban schools."
"They want to create that permanent underclass, that uneducated worker class that can't think for themselves, the proles, the good workers for Big Brother and their corporate donors."

### AI summary (High error rate! Edit errors on video page)

Tennessee Senator Kerry Roberts expressed the belief that higher education institutions should be abolished because they are "liberal breeding grounds" that teach ridiculous things contradicting American values.
Roberts criticized a woman with higher education who spoke out against white supremacy and oppression, believing such views go against the country's values.
Instead of reflecting on whether Republican policies are outdated, Roberts proposed banning schools and eliminating institutions of higher education.
Beau mentions that individuals with higher education, or those who read more, are less likely to support Republican policies.
Conservative figure George Will referred to the GOP as "the dumb party," indicating a disconnect with young people.
Roberts' approach of promoting uneducated workers who can't think for themselves is seen as a strategy to maintain power and control over a permanent underclass.
This view suggests a desire to create a workforce easily manipulated by corporate interests.
The proposal to abolish higher education institutions serves the agenda of perpetuating a class of individuals reliant on authority figures for direction.
Beau implies that the GOP's salvation, as perceived by Roberts, lies in limiting education to maintain control over the population.
The focus appears to be on securing a compliant workforce rather than encouraging critical thinking and informed decision-making.

Actions:

for advocates for education,
Advocate for accessible and inclusive education (implied)
Support policies and initiatives that prioritize education and critical thinking (implied)
</details>
<details>
<summary>
2019-09-09: Let's talk about Trump and the First Step Act.... (<a href="https://youtube.com/watch?v=48Tzh6G9kNI">watch</a> || <a href="/videos/2019/09/09/Lets_talk_about_Trump_and_the_First_Step_Act">transcript &amp; editable summary</a>)

Trump signed a small first step in criminal justice reform, but underfunded and undermined it, while failing to address root causes and perpetuating harmful systems.

</summary>

"A bandaid on a bullet wound."
"You want a participation trophy."
"You've done nothing other than increase militarization of police."

### AI summary (High error rate! Edit errors on video page)

Acknowledges Trump's signing of the First Step Act but questions the follow-up actions.
Trump felt personally attacked by the news and lashed out on Twitter, making himself a national embarrassment.
The First Step Act was described as a small first step in criminal justice reform.
Despite its importance to those directly impacted, Beau likens it to a bandaid on a bullet wound in the grand scheme.
Provides cost details of the First Step Act and contrasts it with the allocated budget amount.
Points out the massive scope of the incarceration problem in America, with the highest rate globally.
Trump's legislation is criticized for not addressing the root causes of the issue.
Beau accuses Trump of increasing police militarization, expanding incarceration centers, and benefiting lobbyists profiting from human suffering.
Mocks Trump for wanting recognition for minimal efforts in criminal justice reform.
Concludes by calling out Trump's actions as insufficient and damaging, despite signing a bill.

Actions:

for reform advocates,
Advocate for comprehensive criminal justice reform (implied)
Support organizations working towards true systemic change (implied)
Hold elected officials accountable for meaningful reform (implied)
</details>
<details>
<summary>
2019-09-09: Let's talk about George Orwell.... (<a href="https://youtube.com/watch?v=QAPEBdzwhQg">watch</a> || <a href="/videos/2019/09/09/Lets_talk_about_George_Orwell">transcript &amp; editable summary</a>)

Beau introduces George Orwell, dissects his quotes through a satirical lens, and reveals Orwell's true beliefs against totalitarianism and for democratic socialism.

</summary>

"In a time of deceit, telling the truth is a revolutionary act."
"Journalism is printing what someone else does not want printed."
"If you want a picture of the future, picture a boot stomping on a human face forever."
"Orwell was a unique person. His writing, a lot of it was satire, it was parody."
"Every line of serious work that I have written since 1936 has been written directly or indirectly against totalitarianism and for democratic socialism."

### AI summary (High error rate! Edit errors on video page)

Beau introduces the topic of George Orwell in relation to Banned Books Week.
He explains how people often misinterpret Orwell's quotes by not understanding his use of satire and parody in his writings.
Orwell believed that all issues are political issues and warned against ignoring political matters until they directly impact individuals.
Beau talks about war propaganda and how it is typically perpetuated by those who are not directly involved in the fighting.
Orwell's focus on truth over fact and his use of fiction to convey truths are discussed.
Beau delves into the concept that telling the truth in a time of deceit is a revolutionary act.
The importance of journalism in printing what others do not want printed is emphasized.
Beau draws parallels between Orwell's writing and journalism, particularly in "Homage to Catalonia."
He contrasts Orwell's famous quote about a boot stomping on a human face forever with the true intention behind it.
Beau reveals Orwell's stance against totalitarianism and for democratic socialism post-1936.

Actions:

for readers, bookworms, orwell enthusiasts,
Share Orwell's true beliefs against totalitarianism and for democratic socialism on social media to spread awareness (suggested).
Research more about George Orwell and his works to understand his messages better (suggested).
</details>
<details>
<summary>
2019-09-08: Let's talk about the lack of primaries on the campaign trail.... (<a href="https://youtube.com/watch?v=-9g5VkROBys">watch</a> || <a href="/videos/2019/09/08/Lets_talk_about_the_lack_of_primaries_on_the_campaign_trail">transcript &amp; editable summary</a>)

The GOP is canceling primaries out of fear of challenging Trump internally, risking revealing his weak support and policies, causing dissent within the party.

</summary>

"They're scared of the dissension within their own party."
"It's going to tear down the entire GOP establishment."
"If the election goes the way I think it might, it's going to be fantastic."
"Your opinion doesn't matter. It's what happens when authoritarianism infects a party."
"It's going to be a dumpster fire of just epic proportions."

### AI summary (High error rate! Edit errors on video page)

The GOP is canceling primaries across the country to prevent challenges to Trump, showcasing their fear of internal dissension.
Rules were bent in South Carolina to cancel the primary, possibly leading to legal issues.
The GOP establishment worries that if Trump doesn't win by a large margin in the primaries, it will reveal the weakness of his support and policies.
Recent shifts in support are evident, with once fervent Trump supporters now concealing their loyalty.
Trump's ineffective leadership and controversial actions have led to declining support even within his base.
The GOP establishment's strategy is to prevent challenges to Trump in the primaries and rely on party loyalty during the election.
Similar tactics failed for Democrats in the past, and Beau predicts a similar outcome for Republicans if they continue to support Trump.
The GOP establishment's decision to protect Trump is causing more dissent within the party, with many Republicans fearing electoral losses.
Beau suggests Democrats stay quiet and let the GOP make mistakes that could lead to the downfall of the establishment.
Republicans who have distanced themselves from Trump may face challenges in influencing party decisions due to authoritarian tendencies within the GOP.

Actions:

for voters, political activists,
Support candidates who challenge the status quo within the GOP (exemplified)
Stay informed about GOP decisions and internal dynamics (suggested)
Participate in local politics to influence party directions (implied)
</details>
<details>
<summary>
2019-09-08: Let's talk about how Trump makes the best deals.... (<a href="https://youtube.com/watch?v=MuIQ4OlIuGo">watch</a> || <a href="/videos/2019/09/08/Lets_talk_about_how_Trump_makes_the_best_deals">transcript &amp; editable summary</a>)

Beau expresses lack of surprise at the president's international incompetence, questions negotiations with the Taliban, and accuses the administration of selling out allies for political gain.

</summary>

"What kind of people would kill so many in order to seemingly strengthen their position? Well for one, the Taliban."
"Everybody knows this and everybody knows that we're selling out our allies."
"The only thing we're doing right now is determining the body count."
"I did for the first month or two, but at this point, every time they do something, I'm like, Now that wasn't as bad as I thought it was going to be."
"It is going to happen."

### AI summary (High error rate! Edit errors on video page)

Expresses lack of surprise at the president's incompetence and hypocrisy on the international stage.
Criticizes the choice of a real estate lawyer for Mideast peace negotiations.
Calls out the president's decision to cancel peace negotiations with the Taliban.
Questions the moral difference between the Taliban's actions and the president's deportation policies.
Points out the futility of negotiating with the Taliban when the U.S. has announced its withdrawal.
Accuses the administration of selling out allies in Afghanistan for domestic political leverage.
Criticizes the president's negotiations with the Taliban while innocent people are dying.
Compares President Trump's actions to his past criticism of negotiating with the Taliban.
Condemns the administration's actions and questions if anyone is genuinely shocked by them.

Actions:

for critically engaged citizens,
Protest against decisions that prioritize political gain over human lives (implied)
Advocate for transparent and ethical international negotiations (implied)
</details>
<details>
<summary>
2019-09-07: Let's talk about my son and Andrew Yang.... (<a href="https://youtube.com/watch?v=TLIbgU3L7wE">watch</a> || <a href="/videos/2019/09/07/Lets_talk_about_my_son_and_Andrew_Yang">transcript &amp; editable summary</a>)

Beau dives into Universal Basic Income (UBI), discussing its historical roots, potential impact, criticisms, and the need for open debate on its viability in a capitalist framework.

</summary>

"UBI could work. So you're letting your own ideological biases interfere with you making videos about something that could work and change the world."
"It's not new, it's not radical."
"So when you hear criticisms of this, just remember that not all of them are ideological. You need to consider the source."
"This isn't socialism. This is firmly rooted in capitalism."
"It's worth talking about."

### AI summary (High error rate! Edit errors on video page)

Beau's son prompted him to talk about candidate Y's proposal X and Universal Basic Income (UBI).
UBI is not a new concept; it dates back to the 1600s, proposed by Thomas Paine.
UBI involves giving $1000 a month to all US citizens over 18, eliminating most social safety nets.
Studies show positive outcomes of UBI, but criticism is often influenced by the establishment's interests.
Andrew Yang advocates for UBI due to the threat of automation eliminating jobs.
UBI could be funded by replacing social safety nets with a Value Added Tax (VAT).
The potential impact of UBI on different sectors, like housing, is discussed.
Beau acknowledges reservations about politicians using UBI to manipulate citizens.
UBI is not socialism but rooted in capitalism and requires capitalism to function.
Beau concludes that while he has reservations, UBI is a workable plan that deserves consideration.

Actions:

for citizens, policymakers, voters,
Engage in open, constructive dialogues about UBI's implications and feasibility (implied)
Support candidates who advocate for innovative social policies like UBI (implied)
</details>
<details>
<summary>
2019-09-06: Let's talk about the history of the Second.,... (<a href="https://youtube.com/watch?v=K_Aq8ZIbDQQ">watch</a> || <a href="/videos/2019/09/06/Lets_talk_about_the_history_of_the_Second">transcript &amp; editable summary</a>)

Beau dives into the history and intention behind the Second Amendment to clarify its purpose and significance in protecting weapons of war and maintaining parity between individuals and soldiers against government tyranny.

</summary>

"The Second Amendment was designed to give parity of arms between the average person and a soldier."
"The Second Amendment was specifically designed to protect weapons of war."
"Guns don't kill people. Governments do."

### AI summary (High error rate! Edit errors on video page)

Exploring the history and meaning of the Second Amendment to clear confusion.
George Mason's role in the writing of the Second Amendment and its ties to the Virginia Declaration of Rights.
Differences between the Virginia Declaration and the Second Amendment, particularly regarding standing armies.
The intention behind the Second Amendment was to give parity of arms between the average person and a soldier.
The Second Amendment was specifically designed to protect weapons of war.
The Supreme Court's ruling in US v. Miller (1939) regarding the relationship of weapons to militia efficiency.
The purpose of the Second Amendment was to allow locals to defend against federal or foreign troops.
The Founding Fathers' foresight in including the Second Amendment as a hedge against government tyranny.
The belief that guns don't kill people; governments do.

Actions:

for history buffs, second amendment advocates,
Study the historical context and debates surrounding the Second Amendment (suggested)
Advocate for responsible gun ownership and understanding of the Second Amendment's original intent (exemplified)
</details>
<details>
<summary>
2019-09-06: Let's talk about a school house in Michigan that rocks.... (<a href="https://youtube.com/watch?v=j8zm3pasNlA">watch</a> || <a href="/videos/2019/09/06/Lets_talk_about_a_school_house_in_Michigan_that_rocks">transcript &amp; editable summary</a>)

Beau explains a Michigan school's innovative design to enhance safety, criticizing "magic pill" solutions for gun violence while advocating for holistic approaches.

</summary>

"That school will save lives."
"The magnitude of the problem we are facing in this country is a lot bigger than I think most people realize."
"There's no magic pill."
"We need to make mental health care available early."
"We are going to have to go down a long road to fix this problem."

### AI summary (High error rate! Edit errors on video page)

Discussed a school in Michigan designed to mitigate the effects of an attack with a $48 million cost.
Shared frustrations about the difficulties of defending schools due to their design flaws.
Acknowledged the distressing nature of a teacher seeking advice on being armed at school.
Praised the innovative design of the Michigan school for addressing security concerns effectively.
Emphasized the importance of solutions like the Michigan school while addressing root causes like mental health and misconceptions about masculinity.
Stressed the ineffectiveness of a "magic pill" solution like banning guns in a society with a vast number of firearms.
Illustrated the impracticality of removing guns from society by using numerical examples.
Urged for multiple solutions like the Michigan school design to save lives in the face of a complex societal problem.
Called for early mental health care availability and addressing misconceptions about masculinity as part of the solution.
Criticized politicians for promising quick fixes akin to a "magic pill" for the deep-rooted issue of gun violence.

Actions:

for community members, policymakers.,
Advocate for early mental health care availability and challenge misconceptions about masculinity (implied).
Support the implementation of innovative security designs in schools (implied).
</details>
<details>
<summary>
2019-09-06: Let's talk about Thoreau and the President.... (<a href="https://youtube.com/watch?v=n5AvTeF-aj8">watch</a> || <a href="/videos/2019/09/06/Lets_talk_about_Thoreau_and_the_President">transcript &amp; editable summary</a>)

Beau talks about Thoreau's deliberate living, influence on civil disobedience, and standing against unjust governments.

</summary>

"Under a government which imposes any unjustly, the true place for a just man is prison."
"Civil disobedience is, you can download it for free on Google."
"When confronted with a government that is behaving immorally, he said that a man cannot without disgrace associate with it."

### AI summary (High error rate! Edit errors on video page)

Beau introduces the topic of Henry David Thoreau leaving Walden Pond and rejoining society 172 years ago.
Thoreau was Harvard-educated but rejected traditional careers, choosing to live deliberately and self-reliantly.
Thoreau's connection with Ralph Waldo Emerson, a transcendentalist, influenced his beliefs in rejecting materialism and valuing spiritual over material.
Thoreau lived at Walden Pond in a cabin, working one day a week and focusing on self-discovery.
Thoreau believed that technology and social interactions were often used to fill inner voids.
Thoreau appreciated nature, saw man as part of nature, and emphasized living deliberately before death.
He wrote an essay "Civil Disobedience" criticizing President James K. Polk's administration and advocating for peaceful resistance.
"Civil Disobedience" influenced figures like Gandhi, Martin Luther King Jr., and Sophie Scholl.
Thoreau refused to pay taxes to a government supporting unjust practices like slave patrols, leading to his imprisonment.
He believed that under an unjust government, a just man's place is in prison.

Actions:

for history enthusiasts, activists,
Download and read "Civil Disobedience" for free on Google (suggested).
</details>
<details>
<summary>
2019-09-04: Let's talk about what we can learn from zombies.... (<a href="https://youtube.com/watch?v=o-plIVJo0nw">watch</a> || <a href="/videos/2019/09/04/Lets_talk_about_what_we_can_learn_from_zombies">transcript &amp; editable summary</a>)

Beau compares surviving natural disasters to zombie movies, urging a shift in perspective to see possibilities amidst challenges and empowering youth to reshape their world.

</summary>

"The rules are the same."
"You see what things could be, if you looked at them in the right way."
"People get in that mindset of, I have to get water."
"The impossible is possible."
"They can use the tools around them to make something else."

### AI summary (High error rate! Edit errors on video page)

Compares zombie movies to surviving natural disasters and life in general, noting that the rules are the same.
Suggests framing natural disaster preparedness for kids as a zombie apocalypse to make it less scary.
Explains that learning survival skills changes how people view the world and what's truly valuable in life.
Outlines key rules for surviving a natural disaster by drawing parallels to a zombie outbreak.
Emphasizes the importance of staying with a group, finding safety, and maintaining communication in a crisis.
Points out that most people in disaster situations are good but cautions about individuals looking to exploit vulnerabilities.
Shares a real-life example where people overlooked needed supplies during a crisis due to societal norms.
Stresses the importance of improvisation and seeing things differently to navigate challenging situations effectively.
Encourages teaching youth survival skills to empower them to change their circumstances and use resources creatively.
Concludes by underscoring the potential for individuals to reshape their world by realizing the possible amidst the seemingly impossible.

Actions:

for parents, educators, disaster preparedness advocates,
Teach children survival skills through fun activities and games (implied)
Organize community workshops on disaster preparedness and improvisation techniques (implied)
Encourage youth to think creatively and problem-solve in challenging scenarios (implied)
</details>
<details>
<summary>
2019-09-04: Let's talk about an appeal to the gun crowd.... (<a href="https://youtube.com/watch?v=FyngQ4xNHFA">watch</a> || <a href="/videos/2019/09/04/Lets_talk_about_an_appeal_to_the_gun_crowd">transcript &amp; editable summary</a>)

Beau outlines practical steps for gun owners to derail gun legislation, focusing on responsible ownership and ending the drug war.

</summary>

"Secure your firearms."
"Stop behaving like children."
"Just because you can do something doesn't mean that you should."
"The gun crowd is its own biggest enemy."
"They have no idea what it's for."

### AI summary (High error rate! Edit errors on video page)

Points out how owners can impede the push for gun legislation by taking individual actions.
Stresses the importance of securing firearms to prevent access by unauthorized individuals, especially kids.
Recommends against open carry and advocates for concealed carry permits for tactical advantages.
Talks about ending the drug war to reduce gun violence and eliminate the pretext for gun control.
Encourages responsible gun ownership, including securing weapons, ensuring background checks for private sales, and using good judgment.
Suggests supporting the intent behind the Second Amendment by selling or trading guns with known individuals.
Criticizes the gun culture that focuses on looking tough rather than responsible ownership.
Urges gun owners to prioritize being responsible over just following poorly written gun laws.
Emphasizes the negative impact of glorifying violence and the importance of discouraging such behavior.
Concludes by criticizing the gun crowd for prioritizing image over safety and responsible ownership.

Actions:

for gun owners,
Secure your firearms to prevent unauthorized access (implied)
Support ending the drug war to reduce gun violence (implied)
Obtain a concealed carry permit for tactical advantages (exemplified)
Prioritize responsible gun ownership over just following laws (implied)
</details>
<details>
<summary>
2019-09-04: Let's talk about Nixon, Trump, and the media.... (<a href="https://youtube.com/watch?v=EKhYeII14XM">watch</a> || <a href="/videos/2019/09/04/Lets_talk_about_Nixon_Trump_and_the_media">transcript &amp; editable summary</a>)

President Trump demands complete subservience from the media, leaving even former allies like Fox News facing consequences for not taking a stand earlier against his actions.

</summary>

"Yeah, you do."
"You standing up to Trump now means nothing."
"When the president branded other journalists as fake news, that's when you should have said something."
"Now you want to reclaim some of that objectivity. Doesn't matter, nobody's buying it."
"Without Trump, what have they got?"

### AI summary (High error rate! Edit errors on video page)

President Trump's strained relationship with the media began early in his campaign, attacking them consistently.
Trump demands total subservience, compliance, and complicity from news outlets, similar to a dictator.
Fox News, previously spared from Trump's attacks, is now facing his wrath for not fully supporting his agenda.
Some Fox News anchors are standing up to Trump, pointing out that they don't work for him.
Beau criticizes Fox News for justifying and promoting the administration's heinous acts to the American people.
Trump's targeting of CNN and now Fox News shows his disregard for any dissenting voices in the media.
Beau points out that Fox News is facing consequences for not taking a stand against Trump earlier when he attacked other journalists and threw children in cages.
The attempt by some personalities at Fox News to reclaim objectivity now that they are under attack is seen as insincere by Beau.
Beau finds it entertaining that Trump's actions are leaving former allies like Fox News on the sidelines.
Comparisons are drawn between Trump and Nixon in terms of their controversial relationships with the media and desire for revenge upon leaving office.

Actions:

for media consumers,
Stand up to demands for subservience from any source, whether political or otherwise (implied)
Take a stand against attacks on journalistic ethics and freedom of the press (implied)
</details>
<details>
<summary>
2019-09-03: Let's talk about recession indicators and a ray of sunshine.... (<a href="https://youtube.com/watch?v=pxTPtnDholA">watch</a> || <a href="/videos/2019/09/03/Lets_talk_about_recession_indicators_and_a_ray_of_sunshine">transcript &amp; editable summary</a>)

Beau warns of recession indicators like dropping consumer confidence and slowing GDP, offering insights on how normal people can navigate economic downturns.

</summary>

"Toys get cheaper, and by toys I don't mean toys for children."
"If you've been planning on bootstrapping and starting your own company, now may not be the brightest time to start it, but it may be the best time to start getting the equipment."
"It's one of those things where if we continue fighting something that is probably inevitable, we may end up hurting ourselves."

### AI summary (High error rate! Edit errors on video page)

Indicates recession indicators like the bond curve inversion, gold and silver price increase, and copper price decrease signaling hard economic times.
Mentions that consumer confidence has dropped significantly, with the University of Michigan's data from August contradicting old data from the Bureau of Economic Analysis in July.
Points out slowing growth in GDP, lowest manufacturing growth levels in a decade, and significant drop in freight shipments.
Private domestic investments have fallen by 5%, indicating a significant economic shift.
S&P 500 estimates of earnings growth are constantly dropping, a strong signal of an impending recession.
Normal people may face layoffs but could benefit from cheaper goods and opportunities to start businesses or invest in property during a recession.

Actions:

for economic observers,
Buy equipment for planned businesses or investments (exemplified)
Look for deals on property during a recession (exemplified)
</details>
<details>
<summary>
2019-09-02: Lets talk about nurses, cops, and appearances.... (<a href="https://youtube.com/watch?v=7eVhgGbzzTY">watch</a> || <a href="/videos/2019/09/02/Lets_talk_about_nurses_cops_and_appearances">transcript &amp; editable summary</a>)

Former Olympic skier turned nurse intervenes to protect patient rights, leading to officer's firing, settlement, and controversial return to work as civilian corrections assistant.

</summary>

"Nurses tend to see the best in people."
"She stood in the way of an officer abusing his authority."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Former Olympic skier turned nurse arrested for obstruction of justice at University of Utah Hospital in 2017.
Nurse intervened when police officer demanded blood draw from unconscious patient without consent.
Nurse explained patient rights and was later released without charges.
Arresting officer fired and lieutenant demoted after viral body cam footage of arrest.
Nurse settled for $500,000, part of which went towards making body camera footage public.
Officer sued for $1.5 million, claiming negativity from people who hate cops.
Nurse Wubbles listed as one of the top physicians of 2017 for standing against officer abuse of authority.
Officer started working as a civilian corrections assistant on August 9th.
Opinions vary on officer's actions and whether he should be working in a facility without patient advocates.
Beau leaves it open for viewers to share their thoughts on the situation.

Actions:

for advocates for patient rights.,
Support organizations advocating for patient rights (implied)
Advocate for transparency in law enforcement through body camera footage (implied)
</details>
<details>
<summary>
2019-09-01: Let's talk about the Great Fire of London and what it foreshadows.... (<a href="https://youtube.com/watch?v=AGJJs_nTow8">watch</a> || <a href="/videos/2019/09/01/Lets_talk_about_the_Great_Fire_of_London_and_what_it_foreshadows">transcript &amp; editable summary</a>)

Beau delves into the Great Fire of London, drawing striking parallels to today's climate crisis and the repercussions of profit-driven denial.

</summary>

"It's funny. Americans are the way we are."
"Take your arguments against it, your go-to arguments. Type it into Google, followed by the word debunked, and just read."
"People denying it when anybody who looks into it can see that it's very evident."
"Just like this."
"We don't want to waste money on something."

### AI summary (High error rate! Edit errors on video page)

Talks about the Great Fire of London in 1666, starting with a bake house near Pudding Lane.
Explains that a spark fell into fuel, setting off a massive fire due to the kindling-like surroundings.
Around 60 to 100,000 people were left homeless, with a third of London destroyed.
Despite claims of only six to eight recorded deaths, the true toll remains unknown.
Describes the lack of infrastructure and planning that exacerbated the disaster.
Mentions the warnings and knowledge of potential fire spread, but profit interests hindered preventative measures.
Points out the parallels between this historical event and today's climate crisis.
Challenges those who deny climate change to research and acknowledge the overwhelming evidence against their arguments.
Emphasizes the disproportionate impact of such crises on the powerless and poor.
Raises the issue of prioritizing profits over necessary investments in infrastructure and planning.

Actions:

for climate change activists,
Research the overwhelming evidence supporting climate change and share it with skeptics (suggested)
Advocate for prioritizing infrastructure and planning for climate resilience in your community (implied)
</details>
