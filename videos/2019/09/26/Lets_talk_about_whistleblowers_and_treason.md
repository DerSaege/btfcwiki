---
title: Let's talk about whistleblowers and treason....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=AuVTVhY0KEY) |
| Published | 2019/09/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses the LA Times report on the President's private breakfast where he questioned who provided the whistleblower with information, likening them to a spy.
- He clarifies that those who shared the information were simply doing their jobs and providing oversight to Congress.
- Beau explains that treason has a specific legal definition, and providing a whistleblower complaint is not treason.
- He points out that damaging national security was not done by those who shared the information, but rather by the administration itself.
- The over-classification of information can weaken the significance of true national security interests.
- Beau asserts that the administration's actions have harmed national security and intelligence efforts.

### Quotes

- "Words have definition. And treason doesn't mean did something the president doesn't like."
- "The administration damaged national security. The administration aided spies, not the whistleblower."

### Oneliner

Beau clarifies misconceptions about treason and national security, stressing the administration's role in damaging intelligence efforts.

### Audience

Concerned citizens, activists

### On-the-ground actions from transcript

- Contact your representatives to demand accountability and transparency in government actions (implied)
- Educate others on the importance of protecting whistleblowers and upholding oversight committees (implied)
- Join advocacy groups working to strengthen national security measures and protect intelligence gathering capabilities (implied)

### Whats missing in summary

Full context and detailed analysis of the LA Times report and its implications.

### Tags

#Treason #NationalSecurity #Whistleblower #Oversight #Government


## Transcript
Well, howdy there, internet people, it's Beau again.
I know in the world of alternative facts,
some of what I'm about to say isn't gonna matter
to a lot of people, but I feel the need to say it anyway.
Um, so today we're gonna talk about the L.A. Times report
about the president's private breakfast
and what he said there.
So we're going to talk about that report, some definitions, and classification.
That's what we're going to talk about today.
So the LA Times is reporting that the President of the United States had a private breakfast
at which he said, I want to know who's the person.
Who's the person who gave the whistleblower the information?
Because that's close to a spy.
You know what we used to do in the old days when we were smart, right?
spies and treason. We used to handle it a little differently than we do now.
Wow. Okay. First, the people who provided this information and made sure it found its
way to congressional oversight were doing their jobs. They were doing their jobs. They
They weren't spies.
That's what they're supposed to do.
That's why we have a Senate Oversight Committee or a House Oversight Committee.
They are supposed to provide oversight.
That's what they do.
As far as treason and the implied penalty there, well, that's something else in and
of itself.
I mean, that's crazy.
Words have definition.
And treason doesn't mean did something the president doesn't like.
That's not what it means.
The United States actually has a legal definition of it.
Whoever owing allegiance to the United States levies war against them or adheres to their
enemies giving them aid and comfort within the United States or elsewhere is guilty of
treason.
Congress is not the enemy of the United States.
They're an equal branch of government.
So following the law and providing this whistleblower complaint, that's not treason, it's not.
Treason would be more like, I don't know, attempting to get your hands on a server so
you could further obscure a Russian cyber attack.
That would be closer to treason than this.
And while we're on that topic, as the president runs around and says, Biden broke the law
and Biden's son did this, where's the evidence?
He's making this claim repeatedly, and yet the whole reason we're talking about this
is because he didn't have any evidence and he was trying to strong-harm Ukraine into
giving it to him, or manufacturing some, or turning over a server that they may or may
not even have.
These people did not damage national security.
National security was damaged by the administration.
And now as far as the over-classification of that transcript, the reason there are different
levels of classification is because they're treated differently.
If everything gets treated and everything gets classified, including the president's
dirty laundry, gets classified as compartmentalized information, well then that classification
doesn't mean anything anymore.
It doesn't get treated the same way.
There's only so much resources you can put into protecting a secret.
And when you treat everything as if it is top, top, top secret, well then nothing is.
you don't have the resources to do it and those who have access to it when
they start to see stuff like this it casts doubt on everything else that
bears that classification because this is not a national security interest but
does that mean that the rest of the stuff isn't too it makes it a whole lot
easier for people to slip up because there is a difference between classified, secret,
top secret, and compartmentalized information.
There's a reason it's structured that way.
Overclassifying something helps the opposition, those people trying to obtain secrets.
The administration damaged national security.
The administration aided spies, not the whistleblower.
I think that's something that people need to be aware of.
That beyond all of the other stuff that's coming out right now, the actions of this
This administration damaged national security, damaged intelligence gathering capabilities,
damaged our counterintelligence efforts.
And that's not arguable.
It happened.
If it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}