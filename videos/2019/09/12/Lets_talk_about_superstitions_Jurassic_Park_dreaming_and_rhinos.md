---
title: Let's talk about superstitions, Jurassic Park, dreaming, and rhinos....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=pauyvIE8tQc) |
| Published | 2019/09/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Northern White Rhino is basically extinct, with only two left - a mother and daughter.
- Scientists harvested eggs from the rhinos and created embryos from frozen samples of male northern white rhinos.
- Two embryos have been created and frozen in liquid nitrogen to be transported and inserted into a southern white rhino surrogate mom to bring back the species.
- The reason behind the extinction of these rhinos is the demand for their horns, believed to have magical healing powers due to superstition.
- Despite this ancient superstition, efforts are being made to revive the species, showcasing the strange yet hopeful nature of humans.
- Beau draws parallels between this conservation effort and solving other global issues like world hunger, disease, and homelessness.
- He believes that with the same mentality applied to food distribution, these problems can also be solved.
- Beau advocates for addressing multiple issues simultaneously, suggesting that as an advanced species, humans can work on conservation and other challenges concurrently.
- He expresses hope and confidence in humanity's ability to tackle various issues if there is a collective desire to do so.
- Beau encourages maintaining dreams and optimism in the face of challenges.

### Quotes

- "It's amazing. It's amazing and it gives me hope for a lot of the other problems we have in this world."
- "We produce more food than we need. We just got to figure out how to get it where it needs to be."
- "We're an advanced species. If we can believe in superstitions like this, and this kind of technology, if we can do both of those at the same time, we can certainly work on world hunger, disease, climate change, homelessness, all of this stuff, and save the rhinos."

### Oneliner

Beau shares the hopeful story of reviving the Northern White Rhino and suggests that with the same mentality, humanity can solve various global issues alongside saving species.

### Audience

Conservation enthusiasts

### On-the-ground actions from transcript

- Support conservation efforts (exemplified)
- Advocate for addressing global issues simultaneously (implied)

### Whats missing in summary

The full transcript provides a deeper exploration of the interconnectedness between conservation efforts and solving global challenges.


## Transcript
Well, howdy there internet people, it's Bob again.
So today we got good news.
Today we're going to start off with something uplifting, certainly more uplifting than the
stuff I normally talk about because I read something and it just boggled my mind.
Love it, love it.
So the Northern White Rhino, it's basically extinct, it's gone.
For all intents and purposes, it's extinct.
There's only two left out there and they're a mother and daughter.
That's it, end of species, game over, right?
Well no, because scientists harvested their eggs and they had frozen samples of male northern
white rhinos that are no longer with us.
And the daughter's eggs were receptive and they have created two embryos out of this
that are now currently frozen in liquid nitrogen, and they're going to be transported back,
and they're going to be inserted into a surrogate mom, a southern white rhino, to hopefully
carry on the species and bring the species back.
It's amazing to me, and it's really weird at the same time, because man, we are a weird
species, humans, we are strange.
The reason these things are targeted, the reason these things are going extinct, the
reason they are in the danger they are in is because they're horns which are
basically made out of the same thing your fingernails are but due to
superstition people believe they have magical mystical healing powers that's
what they think and that's why they were targeted and that's why we're in this
situation this ancient superstition that's just not true meanwhile while
that's going on while there are still people who believe that we are basically
Jurassic Parking these things back into existence. It's amazing. It's amazing and
it gives me hope for a lot of the other problems we have in this world. You know
we look at problems like world hunger. Oh it's impossible we'll never be able to
fix it. No, we can't. We just have to get this same mentality to the distribution
system. We produce more food than we need. We just got to figure out how to get it
where it needs to be. Disease, homelessness, all of this stuff, it can all be solved. It
can. We just have to want to. We just have to want to. And this isn't one of those false
dichotomy things. I'm like, well, why are we trying to bring back the rhino when we
have all of these other problems? No. We're an advanced species. If we can believe in
superstitions like this, and this kind of technology, if we can do both of those at
the same time, we can certainly work on world hunger, disease, climate change, homelessness,
all of this stuff, and save the rhinos.
I think we can do it all.
We just have to want to, anyway, we just have to keep dreaming.
It's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}