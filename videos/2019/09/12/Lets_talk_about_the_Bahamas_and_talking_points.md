---
title: Let's talk about the Bahamas and talking points....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=U-trVlTIB14) |
| Published | 2019/09/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Bahamas were hit by a Cat 5 hurricane, with winds up to 220 miles per hour and a storm surge of 20 feet, causing widespread destruction.
- Estimates indicate 70,000 people are homeless and displaced, a significant portion of the Bahamas' 400,000 population.
- Thousands are missing, and the human toll is still unknown.
- Despite the massive scale of the disaster, the Trump administration refuses to grant temporary protected status to the victims.
- Beau draws parallels to the administration's treatment of refugees at the southern border, where they are also denied refugee status.
- He criticizes the lack of empathy and humanitarian response, accusing the administration of fostering a climate that denies help to those in need.
- Beau points out that the administration could use parole to admit those affected without full documentation, given the circumstances.
- He expresses dismay at the abandonment of responsibilities towards fellow humans and the disregard for basic human decency.
- Beau condemns the administration's actions as un-American, unprecedented, and disgraceful.
- He calls for people to pressure their representatives to take action and provide assistance to the Bahamas.
- Beau urges against merely spending money at major resorts but rather directs people to seek ways to help through bahamas.com/relief and by contacting their representatives.

### Quotes

- "This is a humanitarian disaster of biblical proportions."
- "We are abandoning our responsibilities as human beings."
- "We are leaving people to die."
- "If you want to help, you need to put pressure on your representatives."
- "It's that simple."

### Oneliner

Beau details the devastation in the Bahamas post-Cat 5 hurricane, criticizing the Trump administration for refusing aid and urging people to pressure representatives for help.

### Audience

Global citizens

### On-the-ground actions from transcript

- Pressure representatives to provide assistance (suggested)
- Support relief efforts through bahamas.com/relief (suggested)

### Whats missing in summary

The full transcript provides more in-depth insights into the humanitarian crisis in the Bahamas post-hurricane, including detailed criticisms of the Trump administration's response and suggestions for concrete actions to help.

### Tags

#Bahamas #HumanitarianCrisis #TrumpAdministration #Refugees #AidEfforts


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we're going to talk about the Bahamas, what
happened, what's happening now, and what it can teach us
about talking points in the United States.
OK, so what happened?
A Cat 5 hurricane, one of the strongest on record.
Winds had gusts up to 220 miles an hour.
That's a tornado that's tens of miles wide.
The storm surge was 20 feet, 3 feet of rainfall on top of that.
Whole communities were destroyed.
The current estimates put it at 70,000 homeless and displaced.
Keep in mind, the Bahamas only has 400,000 people, 15,000 with no food and displaced,
thousands missing, no clue as to what the human toll of this disaster is yet.
This is a humanitarian disaster of biblical proportions.
This is huge.
This is a massive issue.
And the Trump administration will not grant them temporary protected status.
I guess they're not real refugees either, right?
See we've heard this before.
Those coming up on the southern border, well they're not real refugees.
They're just seeking asylum as a loophole.
They're not real.
It's not really, you know, they're not really in a bad way.
You can see this live.
You can see this live on the Weather Channel.
You know how bad it is, but we're not going to give them status, okay.
And the administration is saying, well, it wasn't their call.
Maybe, maybe that's true.
Maybe they just fostered the environment within DHS that makes it seem as though a call like
this would be unappreciated by the president to grant them status.
Maybe they didn't make it.
Maybe Trump didn't make it.
That's a possibility.
He just fostered the climate to deny everything.
That is totally possible.
They're acting as though their hands are tied.
That is not true.
There's a thing called parole.
And with parole, if they were to grant them that status, they can admit them without,
quote, totally proper documentation.
Because you know, when your home gets destroyed, you may not have access to that.
So that's where we're at.
That's where we're at.
That's what's happening in the Bahamas.
We are abandoning our responsibilities as human beings.
We are engaging in something that is just un-American, unprecedented and disgraceful.
We are leaving people to die.
People will die and what, we're going to send people back?
Refuse to grant them status and send them back to islands with no sanitation, yeah.
Now a lot of the news outlets are saying, well what you need to do is go down there
to the places that are open and spend money because a lot of those major resorts are only
owned by investors in the United States who are friends with the owners of the news outlets.
But since none of those investors thought enough of the people of the Bahamas to reach
out to their hotel owner buddy president and demand help, maybe that's not the best thing
to do.
Maybe it's not.
Maybe instead you go to bahamas.com slash relief and figure out what to do with there.
out to what you can do to help from there.
Going down there and ostensibly spending money that is just going to be filtered back out
of the country isn't going to help, it isn't going to help.
If you want to help, you need to put pressure on your representatives.
That's what you need to do.
This is a disaster of Biblical proportions and we are ignoring it because the President
of the United States is a xenophobe.
It's that simple.
Anyway, it's just a fall.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}