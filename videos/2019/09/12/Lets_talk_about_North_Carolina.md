---
title: Let's talk about North Carolina....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=YeW5fTcUR68) |
| Published | 2019/09/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Yesterday marked an anniversary nobody celebrates but acknowledges, reflecting on changes and perspectives shaped by political beliefs.
- House Republicans in North Carolina attempted a veto override of the budget while Democrats were at a 9/11 memorial.
- Republicans in North Carolina have a history of subverting democracy to maintain power.
- Despite past instances of gerrymandering being struck down, Republicans continue to manipulate districts.
- The passing of the override in North Carolina is seen as an attack on democracy.
- Deb Butler's passionate attempt to stop the override made headlines.
- The passing of the budget override signals the decline of the Republican Party in North Carolina.
- Beau predicts the Republican Party's decline due to fairer districts in the future.
- Beau compares the actions of representatives in North Carolina to those who perpetrated the 9/11 attacks, stressing the danger posed by political figures.
- Beau ends by remarking on the power politicians hold in shaping the country's foundation.

### Quotes

- "They went out of their way to subvert the democratic process."
- "You're watching the death of a majority."
- "These representatives are more dangerous than any terrorist."

### Oneliner

House Republicans in North Carolina attempt a budget veto override during a 9/11 memorial, exposing a pattern of subverting democracy and sparking the decline of the party.

### Audience

North Carolina residents

### On-the-ground actions from transcript

- Support fair districting in North Carolina (implied)
- Stay informed and engaged in local politics (implied)

### Whats missing in summary

The full transcript provides additional context on the historical actions of Republicans in North Carolina and the significance of fair districting for democracy.

### Tags

#NorthCarolina #Democracy #RepublicanParty #Gerrymandering #FairDistricts


## Transcript
Well, howdy there, internet people, it's Beau again.
See, yesterday was that day.
Yesterday was that day, that anniversary that nobody really celebrates, but we all kind
of mark.
We definitely acknowledge that it happened.
A lot of things have changed since then, and it depends on how old you are, what you were
doing at the time, your political beliefs, on how you view those changes.
A group of people intent on destroying the very foundation of the United States,
intent on subverting America's great experiment, the democratic process.
They laid an ambush.
They laid in wait
in North Carolina.
I'm not talking about what happened in 2001.
I'm talking about what happened yesterday.
While the governor and a lot of Democrats were literally at a 9-11 memorial,
House Republicans in North Carolina
attempted to push through a veto override
of the budget.
They did it then because they knew they wouldn't have the votes any other way.
They went out of their way to subvert the democratic process.
They went out of their way
to ignore the will of the people of North Carolina.
And I know, that's a
old claim there. I mean, maybe they just made a mistake. In 2017, the voting districts in
North Carolina were struck down by the Supreme Court because they were gerrymandered twice.
In 2018, the new ones, they got struck down by the state court the first time they were
racially gerrymandered the second time they were by party.
The Republicans in North Carolina have a long and sordid history of doing everything they
can to maintain power rather than respect the will of the people.
It is what it is.
It's what they've done for a very, very long time and it's what they attempted to do yesterday.
Well, I guess they did, that they passed it through the house.
Now the only reason we really probably know about this, because something like this is
it's North Carolina.
North Carolina is a crooked state, and to be honest, I've never seen anything this crooked
in my entire life, and I lived in Alabama.
But the only reason this really made headlines was because one woman, Deb Butler, lost her
her mind on the House floor and I say that as a compliment. If you get the
chance to watch her attempt to stop this sham, attempt to stop this outright
attack on democracy, definitely do it. Definitely do it. She had a mouthful. My
favorite part was when she went off about cowardice. But yeah, so that's what
What happened, and what you're watching is the death of a majority.
You're watching the Republican Party die in North Carolina because the new districts,
hopefully they'll be fair.
And that 2% win they're all clamoring and clapping about that occurred during that special
election, yeah, that's going to evaporate when the districts aren't gerrymandered.
And they know it.
know it and I know some people are going to have a problem with me comparing them to those
who perpetrated the 9-11 attacks and it is an unfair comparison I will give you that
because only a politician actually has the power to destroy the foundations of this country.
No outside attacker can do that.
These representatives are more dangerous than any terrorist.
Anyway it's just a thought.
Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}