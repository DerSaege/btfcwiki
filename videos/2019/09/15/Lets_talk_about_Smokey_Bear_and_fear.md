---
title: Let's talk about Smokey Bear and fear....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=HlOydP2DASE) |
| Published | 2019/09/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Took his kids to the Tallahassee Museum to celebrate Smokey the Bear's 75th birthday.
- Smokey the Bear is a fire prevention mascot born during World War II when firefighters joined the military.
- The museum is outdoors with animal enclosures, including wolves, a Florida Panther, and a bear.
- Beau's son wanted to play with the bear despite knowing they are dangerous animals.
- Encountered a person dressed as Smokey the Bear, and Beau's son was scared due to unfamiliarity.
- Beau talks about different kinds of fear: rational fear, irrational fear, and subconscious fear.
- Education can help overcome fear by learning and understanding the subject.
- Beau encourages researching things we fear to eliminate that fear and reduce conflict in the world.

### Quotes

- "Most fear can be killed with education."
- "That kind of fear is a gift, use it wisely."
- "If everybody did that, there'd be less fear. There'd be less fear in the world."

### Oneliner

Beau celebrates Smokey the Bear's birthday, encounters different animals with his son, and shares insights on overcoming fear through education to reduce conflict globally.

### Audience

Parents, educators, individuals

### On-the-ground actions from transcript

- Research something you fear to understand and overcome it (implied).

### Whats missing in summary

The emotional journey and personal growth experienced by Beau and his son while exploring the museum and encountering different animals, leading to a reflection on facing fears through education.

### Tags

#Fear #Education #OvercomingFear #Community #Celebration


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, I took my kids to the Tallahassee Museum.
When we were there, when we got there, the place was packed,
like crazy.
We found out it is Smokey the Bear's 75th birthday.
Smokey the Bear is 75 years old now.
And the Forestry Service was putting on an event
to celebrate his birthday, had a cake and everything.
For those overseas, Smokey the Bear is our fire prevention mascot.
We got him during World War II.
All the firefighters signed up to join the military, leaving the forests unprotected.
So we needed something to encourage personal responsibility.
At first Disney loaned the image of Bambi and we used that and they realized that animal
messengers were a good thing and Smokey the Bear was born and the message of
only you can prevent forest fires. So while we're there we walk around the
animal enclosures. The Tallahassee Museum is not the kind of museum you're
thinking of. It's all outdoors and my boy has been around wolves. They're not tamed
wolves but they're familiar with people you know and he's had good experiences
with them. So when he sees the wolves he wants to get in the enclosure with them.
I'm like you know that's you know these guys can be... I know I respect them
they'll respect me. Alright Steve Irwin and from there we go and we see the
Florida Panther that's a cougar and he's over there. Now this thing's a couple
hundred pounds he's over there calling it a kitty cat and then we get to the
bear and the bear's asleep and this bothers my son because he sees a ball
in the enclosure and wants to get in there and play with the bear and the
ball and he tries to wake him up you know it was it was it was funny and I
talked to him I'm like you understand these are dangerous animals right they
It can be.
Okay.
And then this funny thing happened.
As we're leaving, we run into Smokey.
Smokey the bear, guy in a Smokey the bear costume.
My son has never met a stranger.
He will talk to anybody.
Would not go near this guy.
Why?
Familiarity.
watches a lot of animal shows and he's probably knows more about wolves and
bears and Panthers than I do and he's seen a lot of these animals in the wild
he's familiar with them he's not familiar with Smokey though he's too
young for school and that's when you meet him so there's that fear fear of the
unknown, has no idea what this thing is. A little bit of education and introducing
him and all of a sudden my son's wearing one of those funny hats. To me, I don't
know if there's any psychological basis for this, but to me there's three kinds
of fear. There's rational fear and that's like it's New Year's Eve, the bars just
let out, probably not the best time to get on the road. Rational fear could
otherwise be described as an abundance of caution.
There's irrational fear coming from the unknown.
You don't know what this is so you're afraid of it.
That's most fear in the world.
That is most fear.
And then there's the subconscious fear, you know, that fear that just creeps up on you.
get an uneasy feeling about somebody or a location you're in, you know, you start to
notice things, you become a little bit more vigilant, always pay attention to
that fear. That fear is always right. Even when it's wrong, it's right. That kind
of fear is a gift, use it wisely. But most fear can be killed with education.
Learning about the subject matter, whatever it is, whether you're afraid of a certain event happening, or a kind of
person, or whatever.  Education can normally kill that fear.
So, in that vein, this week, when you're bored, think of something you're afraid of, and research it.
Research it. Learn about it. Kill that fear. Hopefully, whatever it is, you won't find
out something to reinforce it. If everybody did that, there'd be less fear. There'd be
less fear in the world. If there was less fear in the world, there would be less conflict.
Most conflict is because of a fear of the unknown. So, let's make more things known.
Anyway, it's just a thought.
Y'all have a good night.
you

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}