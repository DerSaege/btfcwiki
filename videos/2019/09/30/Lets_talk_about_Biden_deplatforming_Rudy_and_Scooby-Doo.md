---
title: Let's talk about Biden, deplatforming Rudy, and Scooby-Doo....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=uQH93_BfwK4) |
| Published | 2019/09/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains how Scooby-Doo logic can be applied to the administration's investigations into Joe Biden and his son.
- Mentions Biden's aides asking news networks not to platform Rudy Giuliani.
- Questions the approach of denying allegations and suggests embracing the world of alternative facts.
- Talks about Hunter Biden's activities in Ukraine and speculates on unethical behavior without evidence of illegality.
- Describes a scenario where Biden allegedly pressured the Ukrainian government to protect his son.
- Criticizes the current administration for setting up investigations based on false information.
- Points out inaccuracies regarding CrowdStrike's nationality.
- Comments on the back channel investigation undermining U.S. intelligence.
- Quotes Rudy Giuliani discussing the true motive behind the investigations.
- Stresses the importance of maintaining election integrity over political candidacy.

### Quotes

- "In the world of alternative facts, we're going to say it's all true."
- "Every time he does, he reveals more."
- "Your candidacy isn't as important as maintaining the integrity of the election process."

### Oneliner

Beau explains how Scooby-Doo logic can reveal the true motives behind the administration's investigations, urging transparency for election integrity over political gains.

### Audience

Politically engaged citizens

### On-the-ground actions from transcript

- Trust the American people to analyze evidence and prioritize election integrity (implied).

### Whats missing in summary

The full transcript provides a detailed breakdown of how embracing transparency and prioritizing election integrity is paramount, even if it means sacrificing political gains.


## Transcript
Well, howdy there, internet people, it's Beau again, so tonight we're
going to talk about what Scooby Doo can teach us about the administration's
investigations into Joe Biden and his son.
So to start this off, apparently Biden's aides have asked major news networks not
the platform Rudy Giuliani anymore.
Okay.
And I mean, I understand that on one level there, Giuliani is kind of
dragging their candidate through the mud, I get that, but we're
going to use Scooby-Doo logic thing about Scooby-Doo is if you play along
with the plot long enough, eventually the mask comes off.
So tonight, rather than doing what the Biden
campaign has been trying to do and saying, none of this is true.
We're denying it.
This is, this is false allegations.
No, we're going to go the other way with it.
We're in the world of alternative facts.
Now we're going to say it's all true.
Okay, now for the record, I've looked into Hunter Biden's activities
a little bit in Ukraine.
I've found some things that I personally think are probably kind of unethical.
I couldn't find anything that was illegal though, okay, but we're going to say that
he broke the law, where we say he did something illegal, there's evidence of it, don't know
what that law is, but we're just going to say that it happened.
Okay and then in order to protect him from this unknown statute and unknown law breaking,
Vice President Biden put pressure on the Ukrainian government to get rid of a prosecutor to protect
his son and that's the reason he did it. He didn't do it because, like most Western powers
at that time, the US wanted this prosecutor gone. Not because the other, you know, a bunch
of EU member states wanted him gone. The INF wanted him gone. Not necessarily good institutions,
but other Western institutions wanted him gone. He didn't do it for that reason. He
did it to protect his son. And because of this, the current administration set up this
Ukraine to investigate an American company.
It doesn't matter how many times they say it.
CrowdStrike is not Ukrainian.
It's based in California.
Well, the founder and owner, he's Ukrainian.
No, he's not.
He's an American citizen.
He's an American citizen of Russian descent, not Ukrainian.
But whatever.
We are in the world of alternative facts.
So we're going to change this nationality.
We're going to change the way we think.
going to change his nationality, this guy is Ukrainian, he's a wealthy
Ukrainian, Biden did this to protect his son because Hunter Biden broke
some unknown law, okay.
Just it's all true.
We don't have evidence to say it's true, but it's true, accept it, move on.
So they set up this back channel investigation undermining U S
intelligence, because contrary to what has been said, U S intelligence did
actually look at the evidence and concurred with CrowdStrike forget that
too they're setting up this investigation then what they're gonna
look into this giant scheme by having the Ukrainians investigate an American
company okay so what happens next in the plot line what happens next the US
courts are going to take evidence obtained by Ukraine, a country that has corruption
issues, especially within its justice department, and they're going to let that be admissible.
That seems unlikely.
But whatever, there's some agreement.
There's some agreement that says, yeah, sure, that's going to happen.
But what does it prove and what does it do?
Where does it go from there?
See the only logical conclusion here is that it wasn't a criminal investigation.
That it wasn't actually about Joe Biden or getting Joe Biden in trouble.
Read you a quote.
This is not about getting Joe Biden in trouble.
This is about proving that Donald Trump was framed by the Democrats.
Rudy Giuliani. That's Rudy Giuliani. It was political. Just like with Scooby-Doo, if you
play along with the plot long enough, the crazy old white guy is going to take off his
mask and tell you what he was doing. And it happened. It happened. The other thing to
keep in mind is that even if all of this was true, it does not excuse the alleged
wrongdoing of the administration, the current administration, even if all
of this is true, it doesn't now to the Biden campaign, I understand that to
you, your candidate's election is the most important thing, it's not to the
country, to the country, the most important thing is finding out what
actually happened. It is safeguarding the election process. That's the most
important thing. And in furtherance of that, allowing Rudy Giuliani to continue
to get on TV and talk is really important. Because every time he does, he
reveals more. He reveals more. I know it's hard for politicians to do, but have
little faith in the American people. Trust them to wade through the evidence.
It's not hard, it's not complicated, not really. Trust they'll do it. But even if
your candidacy suffers and you don't get the nomination because of this, so be it.
So be it. Your candidacy isn't as important as maintaining the integrity
of the election process as a whole anyway it's just a thought y'all have a good night

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}