---
title: Let's talk about Trump and the First Step Act....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=48Tzh6G9kNI) |
| Published | 2019/09/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Acknowledges Trump's signing of the First Step Act but questions the follow-up actions.
- Trump felt personally attacked by the news and lashed out on Twitter, making himself a national embarrassment.
- The First Step Act was described as a small first step in criminal justice reform.
- Despite its importance to those directly impacted, Beau likens it to a bandaid on a bullet wound in the grand scheme.
- Provides cost details of the First Step Act and contrasts it with the allocated budget amount.
- Points out the massive scope of the incarceration problem in America, with the highest rate globally.
- Trump's legislation is criticized for not addressing the root causes of the issue.
- Beau accuses Trump of increasing police militarization, expanding incarceration centers, and benefiting lobbyists profiting from human suffering.
- Mocks Trump for wanting recognition for minimal efforts in criminal justice reform.
- Concludes by calling out Trump's actions as insufficient and damaging, despite signing a bill.

### Quotes

- "A bandaid on a bullet wound."
- "You want a participation trophy."
- "You've done nothing other than increase militarization of police."

### Oneliner

Trump signed a small first step in criminal justice reform, but underfunded and undermined it, while failing to address root causes and perpetuating harmful systems.

### Audience

Reform advocates

### On-the-ground actions from transcript

- Advocate for comprehensive criminal justice reform (implied)
- Support organizations working towards true systemic change (implied)
- Hold elected officials accountable for meaningful reform (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of Trump's actions regarding criminal justice reform and calls for a deeper examination of systemic issues beyond superficial legislation.

### Tags

#CriminalJusticeReform #Trump #FirstStepAct #Incarceration #PoliceMilitarization


## Transcript
Well, howdy there, internet people, it's Bo again.
So I want to start off by giving Trump credit where credit is due.
He did sign the First Step Act.
Then what happened?
So he watched the news again and felt personally attacked by it again and took to Twitter again,
lashing out at people again, making himself a national embarrassment again.
That's what happened.
He did not feel like he was given the credit he deserves.
We're going to make sure he gets that credit today, right now.
He signed the First Step Act.
He did.
What was the First Step Act?
A first step.
A small first step.
We're talking about the scale of the problem.
This was a small first step.
Was certainly not a giant leap for mankind when it comes to criminal justice reform.
It was a tiny piece of legislation in the grand scheme of things.
Now make no mistake, those people directly impacted by it was pretty important to them.
But overall, this was a bandaid on a bullet wound.
What's it cost?
$375 million over the next five years to be put out $75 million a year.
How much was allocated to it in Trump's budget?
$14 million.
So he passed a first step act, signed it, signed it into law, he did, and then his budget
made certain it would fail.
You can have credit for that if you would like.
The scope of the problem is huge.
We have 2.2 million people in America's prisons and jails.
We have the highest incarceration rate on the planet.
If you were born after 2001, you stand a little bit better of a 10% chance of getting locked
up during the course of your life if you're a man.
And that varies widely by race.
If you are black, you stand a, what is it, almost 33% chance?
None of this legislation and none of Trump's legislation in general has done anything to
address the root causes of all of this. Nothing. Nothing. So, at the end of the day, the legislation
to start with was a Band-Aid on a bullet wound. It wasn't a crowning achievement of anything.
It was a Band-Aid on a bullet wound. And then he underfunded it and poked a hole in the
Band-Aid. And he wants credit for that. He wants to be heralded for that. You want a
participation trophy is what you want. You showed up, you did the minimum. You don't
fix anything, but just like the spoiled little kid you are, you're crying out that you weren't
recognized. Fine. There's your recognition. That's what you did. At the end of the day,
you've done nothing other than increase militarization of police across this country, increase the
police state, expand the network of incarceration centers across this country, and help the
lobbyists from those industries, the industry of profiting off of human suffering, you've
helped expand them.
That's what you've done for criminal justice reform.
Oh, and you signed a bill and they didn't fund it.
Yeah, you're a real hero, dude.
Anyway, it's just a thought.
y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}