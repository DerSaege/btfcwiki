---
title: Let's talk about George Orwell....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=QAPEBdzwhQg) |
| Published | 2019/09/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces the topic of George Orwell in relation to Banned Books Week.
- He explains how people often misinterpret Orwell's quotes by not understanding his use of satire and parody in his writings.
- Orwell believed that all issues are political issues and warned against ignoring political matters until they directly impact individuals.
- Beau talks about war propaganda and how it is typically perpetuated by those who are not directly involved in the fighting.
- Orwell's focus on truth over fact and his use of fiction to convey truths are discussed.
- Beau delves into the concept that telling the truth in a time of deceit is a revolutionary act.
- The importance of journalism in printing what others do not want printed is emphasized.
- Beau draws parallels between Orwell's writing and journalism, particularly in "Homage to Catalonia."
- He contrasts Orwell's famous quote about a boot stomping on a human face forever with the true intention behind it.
- Beau reveals Orwell's stance against totalitarianism and for democratic socialism post-1936.

### Quotes

- "In a time of deceit, telling the truth is a revolutionary act."
- "Journalism is printing what someone else does not want printed."
- "If you want a picture of the future, picture a boot stomping on a human face forever."
- "Orwell was a unique person. His writing, a lot of it was satire, it was parody."
- "Every line of serious work that I have written since 1936 has been written directly or indirectly against totalitarianism and for democratic socialism."

### Oneliner

Beau introduces George Orwell, dissects his quotes through a satirical lens, and reveals Orwell's true beliefs against totalitarianism and for democratic socialism.

### Audience

Readers, Bookworms, Orwell Enthusiasts

### On-the-ground actions from transcript

- Share Orwell's true beliefs against totalitarianism and for democratic socialism on social media to spread awareness (suggested).
- Research more about George Orwell and his works to understand his messages better (suggested).

### Whats missing in summary

Deeper understanding of George Orwell's writings and beliefs requires further exploration beyond this summary.

### Tags

#GeorgeOrwell #BannedBooksWeek #Satire #DemocraticSocialism #Truth


## Transcript
Well, howdy there, internet people, it's Beau again.
So tonight we're gonna talk about George Orwell.
We're going to talk about George Orwell
because one, we're coming up on Banned Books Week,
and it seems fitting.
Two, when people talk about Orwell, they don't.
They don't actually talk about the man,
they talk about some of his writings.
They pull quotes from those writings,
and they use those quotes to justify
whatever it is they believe. And that might work with a lot of authors. The
problem is that Orwell used a whole lot of satire and parody in his writing. So
what you think that quote means may not be. In fact, one of his famous quotes
we'll get to that. We're going to talk about him and talk about what he
believes by going through some of his quotes, some that actually reflect how he thought.
And there's going to be one that I promise surprises you, and we are going to save it
to the end, but first one, in our age, there is no such thing as keeping out of politics.
All issues are political issues.
This means a couple of different things.
One, that government is becoming so totalitarian that it's involved in everything, and two,
it's kind of a warning to those who only care about the pebble in their shoe, only care
about those things that impact them directly.
By the time that political issue that you abstain from getting involved in reaches you,
well it's too late to really do anything about it.
This is basically that poem, first they came for the socialist and I said nothing because
I wasn't a socialist, it's that poem condensed into one line and that certainly impacted
a lot of how he thought and a lot of what he wrote.
All the war propaganda, all the screaming and lies and hatred comes invariably from
those who are not fighting.
true today. It's true then, it's very true today as well. When the next military
intervention arises, the talking heads that convince you that it is absolutely
necessary and if we don't do it, it'll be the end of the world, when it starts
they will be safely on set watching their stocks in Raytheon rise. The very
Every concept of objective truth is fading out of the world.
Lies will pass into history.
That's true, but it's always been true.
This is not a particularly profound quote from Orwell, really.
This isn't something groundbreaking on his part, but it explains a lot about why he wrote
the way he did.
In many ways, he wasn't worried about fact, he was worried about truth, and he used fiction
to get to that truth.
In a time of deceit, telling the truth is a revolutionary act.
This is definitely something he believes and truly reflected him, but I think people short
sell this quote.
They think it means Edward Snowden is a good person, and yeah, it could be applied that
way, but see, I think this is really prophetic.
I think what he's saying, really, is that the real battlefield of the future is that
four inch space inside your skull, and that with the control systems that governments
have becoming so advanced, like mass media as showcased in 1984, that if they are successful
in using deceit to shape your beliefs and your ideology, they'll never face a revolution
unless somebody tells the truth.
That to me is pretty profound.
Journalism is printing what someone else does not want printed.
Everything else is just public relations.
And that is true.
Journalism is collecting other people's enemies as a hobby.
This explains a lot about how he wrote in Homage to Catalonia.
There are entire sections to that book that could be dropped, completely irrelevant to
the plot really, but they were included.
In fact, they were criticized as being journalism within it.
He was in a position to let people know what was going on, and he did.
You know, Hunter S. Thompson is seen as the father of gonzo journalism, and that's definitely
true, but maybe Orwell's the grandfather, because a lot of what he did was in that same
vain. He was telling things that others did not want told by hiding them a
little bit, obscuring the facts a little, and putting it into a storyline. Now the
next one we're going to go into is probably one of his best-known quotes
and certainly the most misunderstood. If you want a picture of the future, picture
a boot stomping on a human face forever. Man, that's chilling. That is chilling and
And he did not see a good future for us.
The problem is that's not him, that's not him.
I mean, he wrote the words, but they're not his thoughts.
They're the bad guys in 1984.
This has been cast as his prophetic last warning
to the people of the world.
No, it wasn't.
Orwell wasn't a pessimist.
He was a realist.
What this is saying in the context of how it was really presented was that the establishment,
the elites are betters.
They see themselves as so far above us, they're not even human, and that they will use whatever
means necessary to maintain power.
This is about kicking down, stomping down in this case.
Orwell didn't believe that.
Orwell did not believe that, and he didn't think it would work, not really, because in
his words, in his voice, when he was talking about misusing metaphors, he said, in real
life, it is always the anvil that breaks the hammer, those taking the hits, saying that
But it's not those that can dish out the most that win, it's those that can take the most.
If there was a sequel to 1984, it would probably be the proles finding their power, realizing
the power they have because of their numbers after taking so much abuse and staging a revolution.
Now this is the one that is going to shock a lot of people and is definitely going to
the most entertaining. 1936-1937 turned the scale and thereafter I knew where I
stood. Every line of serious work that I have written since 1936 has been written
directly or indirectly against totalitarianism and for democratic
socialism. Put that on a picture of AOC and share it. Orwell was a unique person. His
writing, a lot of it was satire, it was parody. In order to understand it, you have to understand
what was going on at the time, and you have to understand him.
The idea that he thought the future was hopeless, and that that was what was going to happen,
no, no, definitely not.
If he did, he wouldn't have spent so much time trying to tell truth in an age of deceit.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}