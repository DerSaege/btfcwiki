---
title: Let's talk about the Climate Strike, hope, Trudeau, and stepping stones....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=9QmxW4YUWSc) |
| Published | 2019/09/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trudeau's scandal sparked hope in unexpected ways.
- Beau's son questioned the appropriateness of blackface in 2001, leading to a hopeful realization.
- Young American teens discussing Canadian politics signifies a positive change.
- The global climate strike, with millions participating, is seen as the largest protest in history.
- Companies granting employees paid time off for the climate strike indicates a shift towards social responsibility.
- Grassroots movements unite millions globally for a common cause, transcending borders.
- Technology allows today's youth to connect and mobilize internationally, diminishing nationalism.
- The upcoming generation is less likely to be divided by nationalism and xenophobia.
- Individuals are coming together across borders to urge governments to act on climate change.
- Millions are thinking beyond borders and considering the planet as a whole, paving the way for a borderless mindset.


### Quotes

- "Man, that's cool. Gives me hope."
- "They're thinking about the planet as a whole."
- "Millions of people tomorrow will be acting together because they started thinking broader than a border."
- "We might one day see that world I want."
- "It's just a thought."


### Oneliner

Trudeau's scandal and the global climate strike inspire hope by uniting millions across borders for positive change, signaling a shift towards global unity and activism.


### Audience

Global citizens


### On-the-ground actions from transcript

- Join or support grassroots movements for climate action (implied)
- Connect with individuals internationally to advocate for global change (exemplified)


### Whats missing in summary

The full transcript provides deeper insights into the impact of youth activism and global connectivity on shaping a borderless, united world.


### Tags

#Hope #YouthActivism #ClimateStrike #GlobalUnity #BorderlessWorld


## Transcript
Well, howdy there, internet people, it's Bo again.
So we're going to talk about how the Trudeau situation gave me hope in a really weird way.
And before we get into this, to our Canadian viewers, I understand you are in a tough spot
now.
I get it.
Because you've got him, and then you've got, yeah.
I feel your pain.
So why this gave me hope is because I'm standing there,
and my son walks up to me out of nowhere.
Was blackface appropriate in 2001 in this really accusatory tone?
I'm like, what?
Of course not.
No.
What?
And he just walks off.
Walks off because he's talking to his friends.
Talking to his friends.
I still don't know what's going on at this point.
15 minutes later, I'll pull out my phone, get on Twitter,
and I see the photo, and I was just overcome
with this feeling of hope, because young American teens
were standing around outside of school talking
about Canadian politics, talking about politics
another country. Think about it. When you were 13 to 15 years old, did you even know
who the Canadian Prime Minister was, much less what scandal he was currently embroiled
in? Man, that's cool. Gives me hope. Then you've got what's happening tomorrow. Global
climate strike, well I guess today by the time y'all watch this, 150 countries, millions
of people in the streets, supposed to be the largest protest in human history.
You have companies giving their employees paid time off to go, and yeah, they're the
companies you would expect, they're the ones with long histories of trying to be socially
responsible. That's cool. That's cool. It is being heralded as something amazing. And
it is partially for the reasons that people are talking about. Oh man, I see something
else. I see something else. Grassroots, AstroTurf, it doesn't matter. It does not matter. Yeah,
millions of people, a lot of them youth, taking to the streets for a united goal.
And the technology today is going to allow teens in this country to see teens on the
other side of the world doing the exact same thing they are.
like that, that is liable to kill nationalism, xenophobia.
You understand how hard it's going to be to other people in ten years?
The next generation, they're just not going to take it, it's fantastic, it's fantastic.
Yeah, it's cool, because action on climate change needs to happen, but it's so much
cooler that you have individuals coming together across borders to tell their
governments to catch up. That's cool. That's cool and it's just a stepping
stone. It's just a start. There's a whole group of people, millions, who are
currently, at least in their minds, living without borders. They're thinking
about the planet as a whole. Yeah, I'm sure in some of them they're still
nationalist urges. But they're fading. They're fading.
teens today in the U.S. understand that what happens in Canada or Sudan or New Zealand
impacts them.
And they understand that they have the technology in the palm of their hand to reach out.
They don't need a government.
They don't need an embassy, millions of people tomorrow will be acting together because they
started thinking broader than a border.
Man that gives me hope, gives me hope.
We might one day see that world I want.
It's just a thought.
thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}