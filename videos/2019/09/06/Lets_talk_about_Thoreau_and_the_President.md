---
title: Let's talk about Thoreau and the President....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=n5AvTeF-aj8) |
| Published | 2019/09/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces the topic of Henry David Thoreau leaving Walden Pond and rejoining society 172 years ago.
- Thoreau was Harvard-educated but rejected traditional careers, choosing to live deliberately and self-reliantly.
- Thoreau's connection with Ralph Waldo Emerson, a transcendentalist, influenced his beliefs in rejecting materialism and valuing spiritual over material.
- Thoreau lived at Walden Pond in a cabin, working one day a week and focusing on self-discovery.
- Thoreau believed that technology and social interactions were often used to fill inner voids.
- Thoreau appreciated nature, saw man as part of nature, and emphasized living deliberately before death.
- He wrote an essay "Civil Disobedience" criticizing President James K. Polk's administration and advocating for peaceful resistance.
- "Civil Disobedience" influenced figures like Gandhi, Martin Luther King Jr., and Sophie Scholl.
- Thoreau refused to pay taxes to a government supporting unjust practices like slave patrols, leading to his imprisonment.
- He believed that under an unjust government, a just man's place is in prison.

### Quotes

- "Under a government which imposes any unjustly, the true place for a just man is prison."
- "Civil disobedience is, you can download it for free on Google."
- "When confronted with a government that is behaving immorally, he said that a man cannot without disgrace associate with it."

### Oneliner

Beau talks about Thoreau's deliberate living, influence on civil disobedience, and standing against unjust governments.

### Audience

History enthusiasts, activists

### On-the-ground actions from transcript

- Download and read "Civil Disobedience" for free on Google (suggested).

### Whats missing in summary

Exploration of Thoreau's impact on civil disobedience movements throughout history.

### Tags

#HenryDavidThoreau #CivilDisobedience #Activism #SelfReliance #Influence


## Transcript
Well, howdy there, internet people, it's Bo again.
And today, we're gonna talk about Henry David Thoreau.
Why?
Because 172 years ago today,
he walked out of Walden Pond, a rejoined society.
If you've heard of him, that's probably why.
He probably had to read Walden in high school.
So if the name is ringing a bell, that's why.
But who was he?
He was a Harvard-educated man who just rejected all normal careers.
He was like, not going to do it, not going to conform.
He met a guy and made friends with a guy named Ralph Waldo Emerson, who was a transcendentalist.
It's kind of a weird Americanization of Buddhism.
It emphasized the spiritual over the material.
Emerson owned land near this pond, Walden, and he let Thoreau build a cabin there, and
Thoreau went there and lived alone.
He wanted to live deliberately.
He wanted to make sure he lived before he died, is what it boiled down to.
While he was there, he rejected materialism.
He worked one day a week and strived for self-reliance in every way, shape, and form.
He felt that it was a journey of self-discovery, and along the way he decided that technology
didn't make you happy, and that company, friends, social interactions, were normally
just used to fill holes in ourselves, holes in our souls, holes in our inner being.
Look at our relationship with social media today, that might be true.
While he was out there, he learned to appreciate the environment, found that nature itself
was spiritual, realized that man was a part of nature rather than being apart from it.
He's a very interesting guy, and people know him because of Walden.
I want you to picture a president, utterly obnoxious, a bigot, wanting to assert his
dominance over Mexico.
I'm not talking about Trump, I'm talking about James K. Polk.
Thoreau hated him, and he wrote an essay called Civil Disobedience, and it was about that
administration, really. Thoreau was the first hashtag resistance. Civil
Disobedience was published in 1849, check. It highlighted peaceful
means, nonviolent means, of disrupting a bad administration and it questioned the
nature of what it meant to be a good citizen. This 40-page essay influenced
Gandhi, Sophie Scholl of the White Rose Society, Martin Luther King. Pretty
impressive list, and it's sorely unknown, I mean, it's not known as well as it should
be, I don't think.
But the heart of the question asked is what does it mean to be a good citizen, and what
should a good citizen do when confronted with a government like that?
He suggested that true patriots follow reason and their own conscience.
He wound up going to prison or jail because he didn't pay his taxes.
He was like, I'm not going to pay for slave patrols.
I'm not doing it.
So he wound up in jail.
He wasn't ashamed of it.
He said under a government that, under a government which imposes any unjustly, the true place
for a just man is prison.
Very wise guy.
Civil disobedience is, you can download it for free on Google.
It comes in a PDF format.
You should read it.
I think, I don't do book recommendations very often, but this is only 40 pages or so,
and it's worth it.
Even if you don't agree with it, it's worth it.
One of the things that has always kind of stuck with me about it was that when confronted
with a government that is behaving immorally, he said that a man cannot without disgrace
associate with it.
Anyway, it's just a thought.
y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}