---
title: Let's talk about the history of the Second.,...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=K_Aq8ZIbDQQ) |
| Published | 2019/09/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the history and meaning of the Second Amendment to clear confusion.
- George Mason's role in the writing of the Second Amendment and its ties to the Virginia Declaration of Rights.
- Differences between the Virginia Declaration and the Second Amendment, particularly regarding standing armies.
- The intention behind the Second Amendment was to give parity of arms between the average person and a soldier.
- The Second Amendment was specifically designed to protect weapons of war.
- The Supreme Court's ruling in US v. Miller (1939) regarding the relationship of weapons to militia efficiency.
- The purpose of the Second Amendment was to allow locals to defend against federal or foreign troops.
- The Founding Fathers' foresight in including the Second Amendment as a hedge against government tyranny.
- The belief that guns don't kill people; governments do.

### Quotes

- "The Second Amendment was designed to give parity of arms between the average person and a soldier."
- "The Second Amendment was specifically designed to protect weapons of war."
- "Guns don't kill people. Governments do."

### Oneliner

Beau dives into the history and intention behind the Second Amendment to clarify its purpose and significance in protecting weapons of war and maintaining parity between individuals and soldiers against government tyranny.

### Audience

History buffs, Second Amendment advocates

### On-the-ground actions from transcript

- Study the historical context and debates surrounding the Second Amendment (suggested)
- Advocate for responsible gun ownership and understanding of the Second Amendment's original intent (exemplified)

### Whats missing in summary

In-depth analysis of the historical context and debates surrounding the Second Amendment.

### Tags

#SecondAmendment #History #GeorgeMason #USvMiller #GovernmentTyranny


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're gonna do an in-depth exploration of a topic.
We're going to talk about something
and we're gonna look at the history of it
and we're going to discuss it
because there's a lot of confusion
about the Second Amendment on both sides,
on both sides of this debate.
So we're going to find out what does it mean.
In order to do that, we're going to have to know where it came from.
Along the way, we're going to find out who it applies to, what it protects, and then
we can talk about whether or not we should keep it.
So what is the Second Amendment?
A well-regulated militia being necessary to the security of a free state, the right of
the people to keep and bear arms shall not be enfranchised."
One sentence, and because it's a little murky, there's been debate over it for years.
If only there was some document we could look back to, to find out what it really meant.
If only there was a historical record that could clear this up for us.
There is.
while Thomas Jefferson gets a lot of credit for writing a lot of things, the
reality is there's a guy named George Mason who kind of got swept from history
a little bit. There are a lot of references to him still but he's not
somebody we hold up as a founding father very often and we'll find out why along
the way. But Mason, what he did was he wrote a lot of stuff then kind of handed
to Jefferson and was like hey here's my homework change it up a little bit so
the teacher doesn't notice. So one more time the Second Amendment is a well
regulated militia being necessary to the security of a free state the right of
the people to keep and bear arms shall not be infringed. Okay stop me when this
sounds familiar. A well-regulated militia composed of the body of the people trained to arms is the
proper natural and safe defense of a free state, that standing armies in time of peace should be
avoided as dangerous to liberty, and that in all cases the military should be under
strict subordination to and governed by the civil power. That is section 13 of the Virginia
Declaration of Rights written before the Second Amendment, it's written by George Mason.
So what's different?
Well the most obvious part is they took out that part about standing armies being dangerous.
Now why did they do that?
Article 1, Section 8, Clause 12 of the U.S. Constitution grants the government the power
to have a standing army.
It wouldn't make a whole lot of sense to have something in the Bill of Rights saying
that was dangerous, so it got left out, even though they still believed it was dangerous.
This is evidenced by the fact that, although they allowed a standing army, well, they could
only fund it for two years at a time, same clause.
The other part is what has become so murky.
Composed of the body of the people got changed to the people.
And that became confusing because it mentions a militia as well.
This bothered Mason a lot.
One of the reasons that we don't hear about him as much is because he didn't sign the
Constitution.
In fact, he went back home to his home state and argued against ratifying it.
Part of the reason was the ambiguity of some of the things in it.
He also felt it gave the feds too much power.
I can understand him being angry about his work being a little bit more clouded because
Because the other people he gave his homework to, well they made it more clear, not less.
Let me know if this sounds familiar.
Rather than Section 13, it is Article 13 of Pennsylvania's.
That the people have a right to bear arms for the defense of themselves and the state
and as standing armies in the time of peace are dangerous to liberty, they ought not to
be kept up, and that the military should be kept under strict subordination to and governed
by the civil power, they didn't even change that last part, but they did change the beginning.
They added that protect themselves.
So it's definitely an individual right.
It was certainly intended to be that.
Historically there's not a lot of evidence of any kind whatsoever to suggest otherwise.
It is definitely an individual right.
Now, you don't actually have to take my word for it because George Mason himself, when
he was arguing against adopting something, he answered the question, who is the militia?
And this is really cool because you're also going to get a dose of class consciousness
from the 1700s.
I ask, who are the militia?
They consist now of the whole people, except a few public officers, but I cannot say who
will be the militia of the future day if that paper on the table gets no alteration.
The militia of the future day may not consist of all classes, high and low, and rich and
poor, but they may be confined to the lower and middle classes of people, granting exclusion
to the higher classes of people.
With bone spurs.
OK, I made that last part up.
But it was very clear.
The guy who wrote it, it's the whole people.
It's an individual right extending low to high,
everybody.
Now at the time, this just meant guys that
looked like me who owned land.
But as our country has evolved, it has begun.
It now applies to everybody, because now everybody
is supposed to have full rights in this country.
So now we know who it applies to.
Now the next question is, what does it protect?
And in order to understand that, we have to address that lingering question.
If they decided to have standing armies, why on earth do we need the Second Amendment?
Why would we need a militia if we're going to have a standing army?
That doesn't make sense.
We're going to have the militia to fight the standing army if need be.
That's really why it was there.
The idea was that the locals would be able to defeat the federal troops or foreign troops
or whoever it may be because they know the terrain.
So it was left in there.
The Second Amendment was designed to give parity of arms between the average person
and a soldier.
Parity of arms.
What that means is that the Second Amendment
was specifically designed
to protect weapons of war.
I know we hear that a lot today. We don't want weapons of war on our streets
and maybe we don't
but that's what the Second Amendment
was designed to protect.
And you don't have to take my word for that.
In the 1930s, two guys bought some shotguns.
Their barrels were too short, and the federal government
had made a regulation saying you couldn't
have short-barreled shotguns, barrels
with less than 18 inches.
They claimed it was their Second Amendment right.
In US versus Miller, 1939, this is
what the Supreme Court had to say about it.
In the absence of any evidence tending
to show that possession or use of a shotgun,
having a barrel less than 18 inches in length,
at this time has some reasonable relationship
to the preservation or efficiency
of a well-regulated militia,
we cannot say that the Second Amendment guarantees
the right to keep and bear such an instrument.
Short version, since that shotgun is not a combat shotgun,
It is not useful in combat.
It's not protected.
The Second Amendment is there to protect weapons of war.
Now one of the funny questions you always get asked, the rhetorical question, is
what does that mean that you should be able to buy a tank
in a strict reading of it? Yeah.
That is what it means. However,
I like to think most people are sane and that might be a little bit of overkill.
And I believe that it's important to note
that any group of people facing off
against a modern military would use asymmetrical tactics.
They wouldn't need a tank.
They wouldn't need semi-automatic rifles, though.
So that's where we're at.
And then the follow-up to that is always, you know,
well, there's no way the Founding Fathers could have
predicted semi-automatic rifles.
They couldn't have predicted the weapons of war
that we have today.
Now, the standby answer to that from the gun community
is, well, they couldn't have predicted the internet.
Does that mean you don't have free speech on the internet?
I don't think that's a good argument.
I think there's a better one out there.
And the better one is, oh, they totally
could have predicted it because there were weapons that
rapid-fire at the time. Check out the Puckle Gun, P-U-C-K-L-E, the Belton Flintlock, the
Pepper Box Revolver, and then there's also an air rifle that I can't remember the name
of. It's Italian. There were what people today would consider semi-automatic weapons at the
time. In fact, George Washington commissioned 100 Belton Flintlocks for his army. They never
got delivered because Belton wanted way too much money.
So that brings us to the last question.
Why keep it?
Why do we need this?
The founding fathers made a lot of mistakes, but they were smart about a lot of things.
This is one of them.
We keep it for the same reason they wrote it in there 200 years ago, as a hedge against
Because it doesn't matter how many mass shooters you're talking about, which year, which decade you want to add up.
It doesn't come close to the number of people killed by government, by democide.
Guns don't kill people. Governments do.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}