---
title: Let's talk about a school house in Michigan that rocks....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=j8zm3pasNlA) |
| Published | 2019/09/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Discussed a school in Michigan designed to mitigate the effects of an attack with a $48 million cost.
- Shared frustrations about the difficulties of defending schools due to their design flaws.
- Acknowledged the distressing nature of a teacher seeking advice on being armed at school.
- Praised the innovative design of the Michigan school for addressing security concerns effectively.
- Emphasized the importance of solutions like the Michigan school while addressing root causes like mental health and misconceptions about masculinity.
- Stressed the ineffectiveness of a "magic pill" solution like banning guns in a society with a vast number of firearms.
- Illustrated the impracticality of removing guns from society by using numerical examples.
- Urged for multiple solutions like the Michigan school design to save lives in the face of a complex societal problem.
- Called for early mental health care availability and addressing misconceptions about masculinity as part of the solution.
- Criticized politicians for promising quick fixes akin to a "magic pill" for the deep-rooted issue of gun violence.

### Quotes

- "That school will save lives."
- "The magnitude of the problem we are facing in this country is a lot bigger than I think most people realize."
- "There's no magic pill."
- "We need to make mental health care available early."
- "We are going to have to go down a long road to fix this problem."

### Oneliner

Beau explains a Michigan school's innovative design to enhance safety, criticizing "magic pill" solutions for gun violence while advocating for holistic approaches.

### Audience

Community members, policymakers.

### On-the-ground actions from transcript

- Advocate for early mental health care availability and challenge misconceptions about masculinity (implied).
- Support the implementation of innovative security designs in schools (implied).

### Whats missing in summary

The detailed explanations and context provided by Beau in the full transcript.

### Tags

#GunViolence #SchoolSafety #CommunitySolutions #MentalHealth #PolicyChange


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're gonna talk about a school in Michigan.
We're gonna talk about a school in Michigan
and we are going to talk about solutions
because one of you tweeted me the plans to this school.
Cost $48 million.
After seeing the article, I went and found every article,
every piece of information I could find about it.
Read the comment sections under those articles.
There are two kinds of replies to it.
One was, this is sad, and it is, and the other was mocking it.
The school is specifically designed to mitigate the effects of an attack.
Back in January, I got a message from a teacher asking about being armed at school.
Sometimes making these videos, I get frustrated, I get angry.
This is the only one I've ever been distressed because this person was looking for advice.
And schools, the way they're designed, it makes it very, very, very hard to defend them.
hard to mitigate anything that happens there. Those long hallways, they're what we would call
fatal funnels. It's not a good place to be. Those large open areas, there's no cover.
You can hear the distress in my voice because I'm trying to be positive and give the best advice
Yes I can, but deep down I know this is completely ineffective.
This design in Michigan fixes all of this.
To the person who designed it, your schoolhouse rocks.
Fixes all of that.
They curve the hallways.
They curve the hallways.
Limits the line of sight and limits how far a bullet can travel.
It's genius.
They put in cover.
I'll put the video in the comments section.
You can listen to it.
Every single design flaw I pointed out, they fixed.
The architect went out and found somebody who knew what
they were talking about and said, tell me every problem
with the school's design, and I'm going to fix it.
And then they did.
They did.
That school will save lives.
did. That school will save lives. That design will save lives if there's an incident there.
I'm certain that other schools will copy it. So that architect saved lives. There's no doubt in my
mind. They came up with a solution. They came up with something to mitigate a problem that we are
facing and I know right now somebody is saying well the simple solution is take
the guns. What if we're so sick as a society that there is no magic pill?
An accountant recently messaged me and told me I needed to use more numbers,
quantify things better. I'm going to try that now. The US has more guns than
people. Our current population is 327 million. We're going to round down and say 320 million
guns. Magic pill. Guns are banned. Half of Americans decide to voluntarily turn in their firearms.
160 million off the street. Bang. Now keep in mind when Rhode Island banned bump stocks,
precisely zero people turn them in. When New Jersey banned bump stocks, precisely zero
people turn them in. Massachusetts did better, they got three. But we're going to say, because
it's for the kids, half of Americans are going to turn them in. So we got 160 million left
out on the street. It's important, the feds are going to pay for 30,000 new cops who are
We're going to work seven days a week, and they're going to get a gun every single day.
Impossible task, but we're going to say it's happening.
Even with all of that, a kindergartner today will be a junior in college before the guns
are gone.
The magic pill isn't so magic.
The magnitude of the problem we are facing in this country is a lot bigger than I think
most people realize.
We aren't the other countries that were successful at doing this.
Our problem is a lot bigger.
We have to come up with as many solutions like this school as we can while we start
to address the root causes, when we start to address the fact that people don't know
how to cope with little bumps in the road in their life, stressors.
And when they get multiple ones, some of them, if they have the right background, they end
up going down this road.
We need to make mental health care available early, not just once there's an identifiable
problem. We need to address the misconceptions about masculinity in this
country. All of this is going to take time, but it's going to take less than
15 years. And in the meantime, we need every solution like this we can come up
with. This is going to save lives. It is sad that it's necessary, but we didn't get to
this point in one fell swoop. We're not going to get out of it with one fell swoop. And
there's a lot of politicians out there that right now are running around promising this
magic pill. Reminds me of that old saying, you know, the guy's like, well it's gonna
be $600. I don't have that kind of cash on me. Can't write a check? Oh, you'll take
a check. I thought you wanted money. They can promise anything, but there's no magic
the pill. We are going to have to go down a long road to fix this problem. And if we're
going to go down a long road, why not go down that long road and actually address the root
causes rather than just the means anyway it's just a thought y'all have a good
day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}