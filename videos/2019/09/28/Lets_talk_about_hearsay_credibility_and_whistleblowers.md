---
title: Let's talk about hearsay, credibility, and whistleblowers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=UM6YgRNxYnE) |
| Published | 2019/09/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republican senators are trying to undermine the credibility of the whistleblower report by calling it hearsay.
- The whistleblowers had access to highly classified information and are trusted individuals.
- The senators, many of whom are lawyers, know that the whistleblower report is not hearsay.
- Hearsay typically refers to statements intended to prove something, not entire reports or narratives.
- The focus should be on the content of the report, not on disputing intent.
- The Republican senators are treating this as a political scandal rather than a serious breach of security.
- Overclassifying documents, as seen in this case, can be an attempt to obstruct justice.
- The whistleblowers, who understand the significance of the information, are key players in this scandal.
- The actions of the President undermine the integrity of the system designed to protect national security.
- The intelligence community, experts at keeping secrets, are the ones raising the alarm on this issue.

### Quotes
- "His actions undermine the integrity of the system used to protect national security."
- "The intelligence community that is so good at keeping secrets and keeping their mouths shut, those are the people blowing the whistle."

### Oneliner
Republican senators attempting to discredit whistleblowers through hearsay will likely backfire given the significance of the information and the integrity of those involved.

### Audience
Senators, Public

### On-the-ground actions from transcript
- Contact Senators to demand accountability for undermining the credibility of whistleblowers (implied)
- Support transparency and accountability in government actions (implied)

### Whats missing in summary
The full transcript provides a detailed analysis of the whistleblower report, the implications of calling it hearsay, and the potential consequences for national security.

### Tags
#Whistleblower #NationalSecurity #Hearsay #RepublicanSenators #Integrity


## Transcript
Well, howdy there, Internet people, it's Beau again.
So today we are going to talk about credibility and hearsay.
Why?
Because Republican senators have decided to call the whistleblower report hearsay
in an attempt to undermine the credibility of the whistleblower or blowers.
First, I want to start off by saying I don't think that's going to go the way
you think it is.
Understand these are people that had access to code word level secret stuff.
They are literally the most trusted men and women in the country.
Senators most of you do not have access to code word level secret stuff.
Calling into question their credibility is probably going to backfire on you, just saying.
But we're going to go a little bit further into it because most, well not most, a lot
of these senators are lawyers.
They know that this is not hearsay.
I mean, number one, it's not in the courtroom.
Number two, it's a tip.
it's a complaint. It's something to begin an investigation. Pretty much all tips or
complaints are what would be called hearsay, if you wanted to use the terms they're using.
But if you believe that is wrong, you have to go talk to every parent whose child's
life was saved by a student repeating what another student said before something horrible
happened.
These types of complaints are good things, Senators.
Now beyond that, hearsay itself, the idea behind it is that the statement possesses
the intent to prove whatever the statement is.
Donald Trump hates America.
It would be hearsay if that was designed to be taken as proof that Donald Trump hates
America. A report or narrative as a whole cannot be classified as hearsay, period.
There's actually a Supreme Court case on this, Williamson v. the United States, 1994.
You have to go through it line by line. So, Senators, do that. Go through it line by line
and classify what you want to call hearsay. And as you do it, make sure you don't include
to anything that Giuliani has already admitted, Trump has already admitted, or the White House
staff has already admitted.
Pretty much all of it.
They're not disputing the facts that are contained in it, they're disputing the intent.
They're not saying they didn't do these things, they're saying that it's not a crime, that
it's not wrong.
Well, I have some funny news for them about federal statutes.
In the elements of offense, intent is not really there in most of them.
What you want it to do doesn't matter, the acts do.
The Republican senators are still viewing this as a political scandal, a normal run-of-the-mill
political scandal.
And sure, that's part of it, the corruption aspect of it, yeah, that's one thing.
But there's a whole other can of worms here.
See I did that video on overclassifying documents.
And a couple of people said I was overreacting, or I didn't know what I was talking about,
which is funny, but the next day, Panetta, that's former Secretary of Defense, Panetta,
and former Director of Central Intelligence, Panetta, if there is somebody in the country
who understands secrets in the classification system, it would be him, he echoed everything
I said and added stuff, the most notable being that there's only one reason somebody would
do this, and that's to obstruct justice.
He's right, I didn't think of that, but he's right.
See the thing is guys, there's not going to be an Oliver North moment in this scandal.
You're not going to have an intelligence officer fall on their sword.
Look at where the information is coming from.
Look at the whistleblowers.
They're people who trade in secrets daily because they understand the significance of
what he did.
they understand how it undermined the very integrity of the system used to
protect their life over his dirty laundry. You know, had he just deleted or
shredded this transcript, we probably never would have heard about it. But he
put it in that system. And there's a funny thing about playing with stuff you
don't understand is that it might come back to get you. And in this case, I
think it's going to. Now, we all know this transcript was not supposed to be
classified at the level it was, but the thing is it was classified at that level, which
means there are a whole bunch of procedures that have to go in place to protect that information.
If you saw this, your name's recorded.
It is recorded that you saw it.
You cannot stonewall Congress and say, well, I don't know.
There's a record of it.
And if there is no record detailing every single person that saw this information, guess
Guess what?
That's a crime too.
Classifying something at this level, it's compartmentalized information.
That means only a select group of people get to see it.
Why?
Because if it gets out, gets into foreign hands, the counterintelligence guys need to
know where to start looking.
So there should be a record of that, and incidentally there is.
There's another one that even the White House can't get to, which is funny.
So that's the problem with overclassifying stuff.
There's a lot of security procedures that are in place, and it's especially a problem
if you're doing it to protect dirty laundry because it's going to come out.
Now the other aspect of this, we're calling it hearsay, it's kind of implying that it
It wasn't in the course of the whistleblowers' official duties.
Again, I'm not sure that's a can of worms you want to open.
So compartmentalized information was being discussed in the White House with people who
weren't cleared to know it.
That's a whole other set of crimes.
Lacking the credibility of the whistleblowers in this case is a really bad idea.
It's a really bad idea.
And if you were one of the Republican senators who is still attempting to downplay or protect
the president, understand his actions, not the Ukraine stuff, but the cover-up stuff,
his actions undermine the integrity of the very system used to protect national security,
used to protect the identities of intelligence officers, their assets, their agents, used
to protect national defense as a whole, our economic health, everything.
This system is designed to protect the identity of the mole we have in the Kremlin.
It's not designed to protect the President of the United States asking a foreign leader
to dig up dirt on a domestic rival.
The intelligence community that is so good at keeping secrets and keeping their mouths
shut, those are the people blowing the whistle.
Because they know how dangerous this is.
This isn't going to play out like a lot of other scandals.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}