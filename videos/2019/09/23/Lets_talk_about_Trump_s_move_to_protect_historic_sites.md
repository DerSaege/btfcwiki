---
title: Let's talk about Trump's move to protect historic sites....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Nh6bexLk9gU) |
| Published | 2019/09/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Addresses the president's proposal to protect religious freedom by safeguarding religious sites, including those of historical significance.
- Supports the idea of protecting religious and cultural sites, expanding beyond just religious places.
- Mentions the existing organization UNESCO, which already works on this concept.
- Criticizes the president for pulling out of UNESCO and undermining global efforts to protect cultural and religious sites.
- Points out the importance of protecting religious freedom for individuals, as outlined in the Universal Declaration on Human Rights.
- Criticizes the president for undermining human rights through relationships with dictators.
- Calls out the president for failing to uphold treaty obligations regarding the treatment of refugees.
- Labels the president's proposal as a con, stating that it is nothing new and that he has undermined similar initiatives throughout his administration.
- Acknowledges the importance of protecting historical and cultural sites but criticizes the president for only now showing interest due to public backlash.
- Concludes by suggesting that the president's recent actions are merely attempts to deceive the public in light of his declining popularity.

### Quotes

- "It's a con."
- "There's nothing new in this. There's nothing visionary in this."
- "He realizes he's losing. He realizes that people are turning against him, realizing that he's a con."
- "If he actually cared about any of this stuff, he could have acted on it any time within the last three years."
- "It's just a thought."

### Oneliner

Beau criticizes the president's proposal to protect religious freedom, calling it a con and pointing out his history of undermining similar initiatives.

### Audience

Policy Analysts

### On-the-ground actions from transcript

- Support UNESCO's efforts to protect cultural and religious sites (exemplified)
- Advocate for upholding treaty obligations regarding the treatment of refugees (exemplified)

### Whats missing in summary

Beau's passionate delivery and detailed analysis can be fully appreciated by watching the full video. 

### Tags

#ReligiousFreedom #HumanRights #CulturalHeritage #TrumpAdministration #UNESCO


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we're going to talk about the president's great idea.
And it is.
We've got to give credit where credit is due.
And he's got an idea that's worth talking about.
He's proposing a program to protect religious freedom.
OK, big keynote of this is to protect religious sites.
Man, I think that's a great idea.
I really do.
I'm not joking, I know people think I'm being sarcastic right now, I'm not.
I think it's a great idea.
Protect the birthplace of Jesus, the Church of the Nativity,
sites important to all of the various religions.
Currently practicing and those that have faded away. I think it's a great idea.
In fact, I think we should take that idea and expand it
and protect sites of cultural significance as well, and historical significance.
protect all of these religious places as well as ancient sites that are important
to understanding who we are as a people across the world. We could protect the
Statue of Liberty and the Cahokia Mounds. I don't see why I had to stop at
religion. I think it's a great... Oh wait, wait! That organization already exists. It's
called UNESCO and Trump pulled us out of it. His great brilliant visionary idea
has been done. It's going on without him. And he undermined it. As far as the other
aspects of that, of his little proposal, that's all fine and good. Protecting religious freedom
for individuals. It's a good idea. It's something that should happen. That's why we have the
Universal Declaration on Human Rights, which he constantly undermines by his relationships
with Duarte, and the Saudis, and all of these dictators that constantly undermine not just
religious freedom, but freedoms of all kind.
It's also protected as far as when things go bad, they're protected by the protocol
on the treatment of refugees, which he constantly undermines, fails to live up to the treaty
obligations.
This is a con.
It's a con.
There's nothing new in this.
There's nothing visionary in this.
It's all been done.
And he's undermined it every step of his administration.
And now he wants credit for coming up with it.
It is something that is very typical of a Trump move.
At the end of the day, yeah, he's right.
These things need to be protected.
They absolutely do.
They're critical to understanding our history as a people, people of the world.
He's right.
The thing is, they were already being done.
And up until now, he's done everything he could to stop it.
But now, well, he realizes he's losing.
He realizes that people are turning against him, realizing that he's a con.
So in typical fashion, he attempts to perpetrate another con, because that's what this is.
If he actually cared about any of this stuff, he could have acted on it any time within
the last three years.
In fact, all he had to do was not hamper it, but he did.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}