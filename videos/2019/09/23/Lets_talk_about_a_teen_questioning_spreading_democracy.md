---
title: Let's talk about a teen questioning spreading democracy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=1nhsQAYa-E4) |
| Published | 2019/09/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A 15-year-old asks why the US government wants countries to be democratic, questioning if democracy can be forced.
- Beau praises the question as advanced thinking for a teenager, stating that democracy requires active participation and desire.
- He mentions that the US has a powerful war machine and has been spreading democracy globally.
- Beau questions if the US truly spreads democracy or if it's just a narrative.
- Referring to a study by Rich Whitney, Beau reveals that the US provides military aid to many dictatorships, contradicting the promotion of democracy.
- He explains that intelligence operations prioritize American interests over idealistic values like democracy.
- Beau points out that stability is paramount for the US, sometimes conflicting with the establishment of democracies.
- Democracy is described as being in retreat, as per the 2019 Freedom House report.
- Beau encourages critical thinking and questioning narratives with holes.

### Quotes

- "Democracy is advanced citizenship, man. It's hard, you gotta want it."
- "Democracy doesn't matter. If it did, we wouldn't be doing that."
- "Most of them have holes you can walk through."
- "You really should. You are thinking at a level far beyond what is expected of you."
- "Democracy in retreat."

### Oneliner

A 15-year-old's profound query on democracy prompts Beau to reveal contradictions in US actions, urging critical thinking and questioning narratives with gaps.

### Audience

Youth, Critical Thinkers

### On-the-ground actions from transcript

- Question narratives and seek the truth (implied)
- Advocate for transparency in government actions (implied)
- Engage in critical thinking and analyze information critically (implied)

### Whats missing in summary

The full transcript provides a deeper insight into the contradictions between the promotion of democracy and US actions, urging individuals to question narratives and think critically.


## Transcript
Well howdy there internet people, it's Bobo again.
So tonight we're going to talk about a question from a 15 year old.
And I'm going to post it in the comments.
Man, what a question.
Dad, why does the US government want countries to be democratic?
I thought people have to want it.
You can't give it or force it.
Man, it's a great question, it is a great question.
At 15 years old, you are asking a question that adults don't normally ask.
You're right, you're absolutely right.
Democracy is advanced citizenship, man.
It's hard, you gotta want it.
You've got to want it.
You've got to educate yourself, you've got to be involved.
Doesn't seem like something you can force on people.
Seems like something that
has to be grown.
The thing is
we have the greatest
war machine
that has ever been on this planet.
And we've been spreading democracy for a really long time, right?
You think they'd make a mistake?
See, you're there, you're there.
You're so close.
You can see this giant gaping hole in this narrative, in this story that you've been told.
But you've got to step through.
The question isn't, why do we do it, the question is, do we do it?
Or is it just a story, just an idea, just something we tell people your age so they
believe it when they're mine.
There's a guy named Rich Whitney and he did something I love, I love it when people do
this because this is how you can find real truth. He took two authoritative sources that
didn't talk to each other and he compared them to find something out. There's an organization
called Freedom House and they put out a yearly report and they rank countries on how free
they are. Freedom House actually does try to promote democracy. They've been doing it since 1941.
Their 2015 report had 49 countries that ranked a 6 or a 7 in political rights. In English,
they're dictatorships. They're varying kinds. In some cases, it's just one guy. In some cases,
It's a little ruling party, in some cases it's the military.
But they are certainly not democracies.
They're not free countries by any stretch of the imagination.
There were 49 of them, well, then he went and he got US government documents to see
if we gave military aid to any of them.
We gave military aid to 36 of them, almost three quarters.
Democracy doesn't matter.
If it did, we wouldn't be doing that.
It's a story, it's a fairytale, it's a justification.
We're spreading democracy.
It's like right now, they're going to go fight for your freedom in Saudi Arabia.
How did your freedom get there?
It's a story. It's a slogan and he caught it.
He caught it at 15. Awesome.
See, we have this idea about spies, you know, the intelligence officers that do this kind of stuff.
Engage in regime change, political realignment, whatever the buzzword of the day is.
is. And this image says that they are all about truth, justice, the American way,
democracy, apple pie, and not tying. And that may be true when they first sign up.
But by the time they're in a position where they're making decisions about the
the fate of a country, they're very pragmatic.
All that idealism is lost.
They care about one thing and one thing only, and that is American interests.
And you'll hear that term now, you'll start picking it out when you hear the news.
What does that mean?
is economic interests, military interests, political interests, and stability.
Because without stability, the rest of it doesn't matter because we are trying to
maintain dominance in those three areas.
So stability is important.
Sure, I bet these guys would love it if these countries could be democracies and maintain
all of this other stuff.
But democracy is very unpredictable.
As we've talked about in other videos, intent, determining intent is what intelligence work
is about.
That stability.
If any of these four things fly in the face of that country having a democracy, oh it
It doesn't get won.
We'll give military aid to the dictatorship and preserve that stability.
We don't do it.
And you are right.
You have to want it.
You have to want it.
And that's why democracy is in retreat.
Democracy in retreat.
That is the title of the 2019 report from Freedom House.
There's a whole section on the United States.
It's why democracies die, because people don't ask the question you just did.
It may sound weird, but you should be really proud of yourself.
You really should.
You are thinking at a level far beyond what is expected of you.
expected of you. Never lose that. Never lose that desire to question those
narratives because most of them have holes you can walk through. Anyway, it's
It's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}