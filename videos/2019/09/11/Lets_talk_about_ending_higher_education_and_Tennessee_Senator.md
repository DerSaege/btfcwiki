---
title: Let's talk about ending higher education and Tennessee Senator....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=okB22EuoKgA) |
| Published | 2019/09/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Tennessee Senator Kerry Roberts expressed the belief that higher education institutions should be abolished because they are "liberal breeding grounds" that teach ridiculous things contradicting American values.
- Roberts criticized a woman with higher education who spoke out against white supremacy and oppression, believing such views go against the country's values.
- Instead of reflecting on whether Republican policies are outdated, Roberts proposed banning schools and eliminating institutions of higher education.
- Beau mentions that individuals with higher education, or those who read more, are less likely to support Republican policies.
- Conservative figure George Will referred to the GOP as "the dumb party," indicating a disconnect with young people.
- Roberts' approach of promoting uneducated workers who can't think for themselves is seen as a strategy to maintain power and control over a permanent underclass.
- This view suggests a desire to create a workforce easily manipulated by corporate interests.
- The proposal to abolish higher education institutions serves the agenda of perpetuating a class of individuals reliant on authority figures for direction.
- Beau implies that the GOP's salvation, as perceived by Roberts, lies in limiting education to maintain control over the population.
- The focus appears to be on securing a compliant workforce rather than encouraging critical thinking and informed decision-making.

### Quotes

- "The stupid stuff that our kids are being taught is absolutely ridiculous."
- "Rather than take that information and say, hey, are our policies outdated? Have we lost touch? Have we not kept up with the growth of new information in the world? No, we need to ban schools."
- "They want to create that permanent underclass, that uneducated worker class that can't think for themselves, the proles, the good workers for Big Brother and their corporate donors."

### Oneliner

Tennessee Senator calls for abolishing higher education, believing it breeds liberalism, while overlooking policy reflections and opting for uneducated workforce to bolster GOP control.

### Audience

Advocates for education

### On-the-ground actions from transcript

- Advocate for accessible and inclusive education (implied)
- Support policies and initiatives that prioritize education and critical thinking (implied)

### Whats missing in summary

The full transcript includes additional context on the GOP's views on education and workforce dynamics that provide a comprehensive understanding of the implications of anti-intellectualism.

### Tags

#Education #RepublicanParty #CriticalThinking #PoliticalViews #Workforce


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we've got to talk about a Tennessee senator named Kerry Roberts.
Guess in the middle of his podcast, livestream, radio show, whatever, he had an epiphany.
He realized that the more education somebody had, the less likely they were to agree with
GOP policies, with Republican policies.
Now this might be the time for soul searching among most people, but not Senator Roberts.
He's got another idea.
The one thing that we can do to save America today is to get rid of our institutions of
higher education right now and cut the liberal breeding ground off.
Good grief.
The stupid stuff that our kids are being taught is absolutely ridiculous and this is a woman
who is a product of higher education.
She learned all of this stuff that flies in the face of what we stand for as a country.
The woman he is talking about is a woman who had the gall, the audacity to try to have
her voice heard in what is supposed to be a representative government.
But what is the stuff that flies in the face of what we stand for as a country?
We got some woman in there who just goes off and it's all about pick every, every liberal
bit of indoctrination that you can get in a university setting today.
left I mean you've got all these intersectionalities and white supremacy
and oppressive this and every the thing she spoke out against white supremacy
and oppression and that is what stands flies in the face of what we stand for
as a country speaking out against white supremacy and oppression thank you for
illuminating a mission from the GOP. That's, that's, wow, that's great. That's
fantastic. So rather than taking this bit of information that people who have a
higher education, and I would suggest that it's not just formal higher
education, people who read more are less likely to support policies of the
Republican Party, rather than take that information and say, hey, are our policies
outdated? Have we lost touch? Have we not kept up with the growth of new information
in the world? No, we need to ban schools. We need to get rid of that. This is why
George Will, conservative icon, a real conservative, this is why he said that
young people today are starting to see the GOP as, quote, the dumb party. That's
why. But to be honest, Roberts is on the right track because it's the only thing
that's going to save the GOP. It's the only thing that is going to save them
and it's what they want. They want to create that permanent underclass, that
uneducated worker class that can't think for themselves, the proles, the good
workers for Big Brother and their corporate donors.
That's what they want.
Anyway it's just a thought.
Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}