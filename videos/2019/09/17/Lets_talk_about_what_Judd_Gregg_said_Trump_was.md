---
title: Let's talk about what Judd Gregg said Trump was....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xrW2wLqOxJk) |
| Published | 2019/09/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau expresses his longstanding support for President Trump and his surprise at an article labeling Trump as socialist.
- He questions the characterization of Trump's policies as socialist and sees them more as a blend of corporate and government interests.
- Beau becomes defensive of Trump, dismissing the idea that his policies are socialist and getting upset at the article's claims.
- He reacts strongly to the suggestion that Trump lacks core policies, defending Trump's nationalist stance.
- Beau ends by leaving it as a thought for his audience, questioning the claims made in the article.

### Quotes

- "He is not helping to make America great."
- "Y'all have a good day."

### Oneliner

Beau expresses support for Trump, refutes claims of socialism in Trump's policies, and questions the article's assertions about the President's core policies, ending with a thought for the audience to ponder.

### Audience

Trump supporters

### On-the-ground actions from transcript

- Question media narratives and do independent research to form your own opinions (implied).

### Whats missing in summary

Beau's emotional tone and personal investment in defending President Trump are best captured by watching the full transcript.


## Transcript
Well, howdy there, internet people, it's Bo again.
Now, I know a lot of y'all know I have been a huge, huge supporter of President Trump
since the very beginning.
I was on the Trump train day one, always wanting him to win and succeed because I truly believe
that that is the man.
He's the man, he can do it, because I was real surprised when I
I'm reading The Hill, and I find this article by this guy named Judd Gregg, whoever that
is, and he's saying that the president, that he's a bit of a socialist.
Fake news, whatever, but he got me, he click baited me, I read it, and this guy starts
listing all these policies of Trump.
The big one he pointed to was when Trump tried to direct the economy from on high, when he
is like, hey, you know, all you American companies
need to get out of China, and this guy Judd,
well he said that that was a blunt exercise in socialism.
I don't know if that's true, I don't know if that's true.
To me, that's just the blending
of corporate and government interests.
I don't see how that's necessarily socialism.
It's just the blending of business interests and government.
No big deal.
And he goes on and lists more.
more Trump's policies.
He says that weakening the dollar is a socialist thing,
and that expanding the crop subsidies the way he did,
the anti-free trade stuff,
the hidden tax within the tariffs,
the tariffs in general,
that all of this stuff is socialist.
And at this point, I'm getting a little mad.
I'm like, what?
I'm like, okay, who is this granola-eating liberal
out here lying on our president like this?
He is not helping to make America great.
And then I find out Judd, Judd Gregg is a former Republican governor and a three-time
Republican senator, and he'd been around a while.
But that didn't mean anything, that didn't mean anything.
But I did keep reading, and he said that the President didn't have any core policies.
It was just whatever he could dream up, whatever he thought was best.
And again, I don't see how this is socialism.
This is just everything within the state and nothing against it.
That's not necessarily socialism and how he doesn't tolerate disloyalty.
That doesn't mean he's a socialist.
I mean, come on, this is getting all out of hand.
And to be honest, none of this even matches up.
None of this even matches up.
We know Trump was on live TV and said that he was a nationalist.
I'm a nationalist. He said it.
What? He's some kind of social nationalist? That's not even a thing. That doesn't even make sense.
What? He's a national social...
All signs point to yes.
Anyway,
it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}