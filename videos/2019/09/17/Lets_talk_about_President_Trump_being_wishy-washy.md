---
title: Let's talk about President Trump being wishy-washy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8iOi5yCrg4Q) |
| Published | 2019/09/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump is wishy-washy due to his own making and lack of intelligence reports.
- Multiple past incidents were ignored due to not appearing on Fox News.
- The situation involving Iran and Saudi Arabia is complex and requires a nuanced understanding.
- Scrapping previous deals with Iran puts Trump in a difficult diplomatic position.
- Talking points blaming Iran for supplying weaponry are weak due to US support of Saudis.
- Geopolitics in the region is intricate, contrasting with Trump's usual dealings.
- Blaming Saudis for failing to protect critical infrastructure could be Trump's way out.
- Lack of sharp advisors on Trump's staff poses a challenge.
- Uncertainty looms over future actions and viable options for the administration.
- Running a country differs greatly from selling a brand, hinting at the responsibilities at hand.

### Quotes

- "What the American public doesn't know is what makes them the American public."
- "Geopolitics in the region is a little bit more complicated than building a golf course."
- "There's no reason American lives should be risked and wasted because they didn't fulfill their duties."
- "The American public needs running the country and the largest war machine on the planet."
- "It may just be a reminder that the American public needs running the country."

### Oneliner

Trump's wishy-washy stance stems from his own ignorance, while complex geopolitics challenge his diplomatic footing, leaving no easy way out.

### Audience

Political Analysts

### On-the-ground actions from transcript

- Contact political analysts to provide insights and strategies (suggested)
- Join organizations advocating for informed decision-making in geopolitics (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of Trump's diplomatic challenges and geopolitical nuances, offering a deeper understanding of the situation. 

### Tags

#Trump #Geopolitics #Diplomacy #Iran #SaudiArabia #AmericanPublic


## Transcript
Well, howdy there, internet people, it's Bo again.
Man, I got a lot of people asking me,
why is Trump being so wishy-washy?
Oh, man, I feel for him.
I feel for him right now, I really do.
I've got a lot of sympathy for him
because he is coming to terms with some stuff
and it cannot be easy.
It cannot be easy for him.
He's in a no-win situation of his own making right now.
He can't really exercise a military response, and the reason he can't is his own fault.
He doesn't read his intelligence reports.
He watches Fox News, and he gets his information at the same time as the American public.
You know that old saying, what the American public doesn't know is what makes them the
American public.
This has been kind of cast as this crazy, spectacular, unprecedented event at this refinery.
Except in August, they hit an oil field.
In July, they hit an airport.
In June, they hit an airport.
In May, they hit a pipeline.
Earlier in May, they took out a Patriot system.
January bunch of generals in May of the previous this goes on but he didn't
respond to any of those because they didn't make Fox News he didn't even
happen more likely if he responds to this one after not responding to the
others what is it it's war for oil it's that simple that puts him on a pretty
bad footing to try to build a coalition around that.
Then when it comes to diplomacy, he's got a whole other issue.
Contrary to the popular belief of many of his supporters, not all Muslims are the same.
I'm willing to bet that the president was unaware of the geopolitical issues surrounding
Iran and Saudi Arabia, one being a Sunni regional superpower and one being a Shia regional superpower.
The Saudis, they've been kind of kept in check because of a long relationship with
the US.
arms sales, oil purchases, they're semi-friendly.
Iran, they were a wild card for a while.
Then they got better.
If only there was some deal that was keeping them in check.
There was, and he scrapped it.
And now to return to the diplomatic table, well, it kind of makes him look like Obama,
doesn't it?
Especially because he's going to have to get rid of the sanctions to even sit down.
Man, that's got to be a tough pill to swallow for him.
I mean, that's got to hurt deep down.
And then the talking points that came out, well, Iran has to be held responsible because
they supplied the weaponry, the technology.
And I guarantee you, in that room, there were some regional analysts going, so we supplied
the Saudis at the same time, and they did some pretty bad stuff with what we gave them.
They might not be a good talking point to try to rally support, because if we go that
route, well, our weapons supplies will be examined, but you can't go there either.
The talking points are bad.
He doesn't have options, because geopolitics on this level in that region, it's a little
bit more complicated than building a golf course.
He's in a situation of his own making and there's no easy way out of it.
There is, there is, but I don't know that his administration listens to weirdos, but
hopefully there is some weirdo in that room right now going, you know the real question
is with all of the arms deals that we've done with the Saudis, why were they unable to protect
their critical infrastructure? That's his way out. His only way out really is to basically blame the
Saudis for it and point out that this is critical infrastructure. It was all telegraphed. They knew
that these type of incidents were going to occur, and they didn't protect it.
There's no reason American lives should be risked and wasted because they didn't fulfill
their duties.
That's his way out.
Now, I don't know if he has any advisors that will tell him that.
The people on his staff do not seem to be the sharpest.
So as far as the other question of what's going to happen, I don't have a clue.
I have no idea what they're going to do.
No clue.
But they don't have a lot of viable options that don't turn bad for them very, very quickly,
either politically or militarily.
It may just be a reminder that the American public needs running the country and the largest
war machine on the planet is probably a little different than being able to sell your brand.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}