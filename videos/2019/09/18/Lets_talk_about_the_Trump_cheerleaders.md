---
title: Let's talk about the Trump cheerleaders....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=l0K0U3F5IGY) |
| Published | 2019/09/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Responds to a message about Trump cheerleaders being punished for displaying a banner supporting Trump 2020 before a game at North Stanley High School in North Carolina.
- North Stanley High School has a policy against political signs, but the issue is whether the banner was disruptive to the school, not the political nature of the sign.
- Points out that students have rights at school, and the test is whether their actions are disruptive.
- Explains that the cheerleaders, as representatives of a government entity receiving government money, can't display political messages while in uniform.
- Clarifies that the punishment for the cheerleaders was probation, meaning they were simply told not to do it again and faced no significant consequences.
- Emphasizes that allowing students to have meaningful political discourse in schools is vital to avoid the rise of candidates like Trump.
- Encourages open, non-confrontational discourse to understand why the cheerleaders support Trump.
- Advocates for defending free speech within the bounds of a civilized society, even if one disagrees with the message being conveyed.

### Quotes

- "Students do not shed their rights at schoolhouse gate, period."
- "Given the fact that they were in uniform, yeah, that's probably fitting."
- "I think that is something that needs to change and in the meantime we need to defend what little ability students have to discuss politics at school."
- "We have to defend it as much as we can within the bounds of a civilized society."
- "Whether or not I agree with it shouldn't matter."

### Oneliner

Beau explains the controversy around Trump-supporting cheerleaders, advocating for meaningful political discourse in schools within the bounds of free speech.

### Audience

Students, educators, community members

### On-the-ground actions from transcript

- Contact the cheerleaders for non-confrontational political discourse (suggested)

### Whats missing in summary

The full transcript provides a detailed analysis of the situation with Trump-supporting cheerleaders and the importance of allowing political discourse in schools while defending free speech.

### Tags

#Students #FreeSpeech #PoliticalDiscourse #Schools #Trump


## Transcript
Well, howdy there, internet people, it's Bo again.
So I got a message asking me,
and said, is it right that the Trump cheerleaders
were punished?
I didn't even know what that meant.
If you're gonna send me a message,
please include a little bit more details than that,
because I had to scowl to kind of find this and look it up.
So I'm assuming this isn't gonna make national news,
so let me fill you in.
Some cheerleaders at North Stanley High School
in North Carolina prior to a game held a banner that said Trump 2020, Trump
re-election banner, the squad was put on probation, is that right?
Okay.
So the school has a policy that says no, uh, political signs at school.
That's probably not constitutional.
Supreme court has been really clear on this.
Students do not shed their rights at schoolhouse gate, period.
The test is whether or not it is disruptive to the school.
So it's not the type of speech that is in question, it's whether or not it disrupts
the school.
So a blanket policy against political signs probably won't stand up.
At the same time, this was not a political sign, it was an election sign.
that they were in uniform representing the school, which is a government entity which
receives government money, they can't play politics.
They're running into an issue that probably is not something your average teenager has
thought about, government entities can't support a political candidate.
Is it right that they were punished?
Punishment consisted of probation.
What does that mean?
Literally means they were told, hey, don't do this again.
They didn't miss any games.
They weren't punished in any meaningful way.
Given the fact that they were in uniform, yeah, that's probably fitting.
They're being told not to do this again in uniform is what they should be told.
The schools issued a statement, because the cheerleaders were in uniform and were acting
as representatives of the school, the display of the sign could be perceived as the school
or school system endorsing a political campaign, for this reason, the only action Stanley County
Schools has taken is to ask the cheerleaders not to display the sign again.
And that's where it stops.
It should be, again, in uniform.
If they want to come to school tomorrow with the banner in their normal clothing and take
a photo in front of the flagpole, they should be allowed to.
That should be protected speech, it's not disrupting the school.
I know a lot of you are like, why on earth would you go out of your way to say something
about this?
I think the reason we end up with candidates like Trump is because we don't allow meaningful
political discussion in our schools.
So I think that that is a, I think that's something that needs to change and in the
meantime we need to defend what little ability students have to discuss politics at school.
I mean I have some questions about this.
I would like to know exactly how an entire cheerleading team backs one candidate.
That seems really odd.
And I would like to know why they support it.
If you're in this photo, I would love to talk to you in a non-confrontational way.
I would love to hear why you support President Trump's re-election.
But we do need to keep in mind that no matter how much the right wants to squelch free speech,
We have to defend it as much as we can within the bounds of a civilized society.
You can't be advocating violence or anything like that, the whole shouting, screaming fire
in a crowded theater type of thing.
This wasn't that.
This was legitimate political speech.
Whether or not I agree with it shouldn't matter.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}