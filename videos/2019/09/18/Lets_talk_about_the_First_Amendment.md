---
title: Let's talk about the First Amendment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=eJmh0qql0FA) |
| Published | 2019/09/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- People were mad about squelching free speech rights, exemplified by criminalizing pipeline protests in red states and Trump's suggestion to strip citizenship for flag burning.
- Republicans support making it illegal to burn the flag, prohibiting offensive statements against police or military, and banning face coverings.
- Republicans show less support for extending free speech courtesies to the LGBTQ community.
- Republicans favor stopping construction of new mosques and arresting hecklers rather than disciplining them.
- Republicans believe the press has too much freedom and express a negative view towards journalists.
- Beau dismisses claims of a free speech crisis on campuses, noting that professors being fired for political speech are mostly liberals, not conservatives.
- Beau explains that universities provide platforms for dissenting viewpoints, and students protesting speeches is protected by the First Amendment.
- The far right seeks a platform, not free speech, and universities are more accepting of dissenting viewpoints.
- Free speech means the right to speak, not entitlement to a platform or equal time for all ideas.
- Beau criticizes the far right for wanting to use the government to dictate speech and actions, contrasting it with the principles of free speech and dissent.

### Quotes

- "The far right doesn't want speech. They don't want free speech. They want a platform."
- "You don't need the government's gun. You better let this guy talk."
- "A good idea will find a platform. Doesn't need one handed to him."
- "You can be deplatformed and if the idea is solid, another platformer will arise where you can create your own."
- "That's the right and y'all are utter failures at it."

### Oneliner

Beau breaks down the misconception around free speech, exposing the far right's desire for a platform over true freedom of expression, critiquing their attempts to dictate speech through government intervention.

### Audience

Activists, Free Speech Advocates

### On-the-ground actions from transcript

- Stand up for true free speech by advocating for platforms for diverse voices (implied).
- Educate others on the distinction between a platform and free speech (implied).
- Support institutions that uphold principles of free speech and dissent (implied).

### Whats missing in summary

The full transcript provides a detailed breakdown of free speech misconceptions and the far right's pursuit of platforms over genuine freedom of expression. Viewing the full transcript offers a comprehensive understanding of these issues. 

### Tags

#FreeSpeech #FarRight #PlatformOverSpeech #Dissent #GovernmentIntervention


## Transcript
Well, howdy there Internet people, it's Bo again. So that last video I guess people had questions about it. People were
mad.
Said the right was squelching constitutional protections for free speech, the First Amendment.
People had questions.
Give me some examples. What are you talking about? Sure, I'll give you some examples.
I would start by saying criminalizing the pipeline protest in red states. That would be a big one. Trump saying that
anybody who burned the American flag should be stripped of their citizenship.
that would be a good one. 72% of Republicans agreeing with making it illegal to burn the flag,
that would be one. 36% of Republicans saying that mean and offensive statements that hurt
the feelings of police officers or the military should be prohibited by law. That would be one,
I would suggest, especially considering that's exactly the type of speech that the First
Amendment was designed to protect. Incidentally, less than a quarter of those would extend those
those same courtesies to the LGBTQ community.
67% of Republicans in favor of banning face coverings,
religion is speech, my friends.
50% of Republicans being in favor of stopping construction of new mosques in
their neighborhoods.
Republicans being twice as likely to want hecklers arrested
rather than just disciplined by the universities.
50% of them saying that the press has too much freedom,
they have too much First Amendment.
certainly seems like it's the right that is squelching the First Amendment. Call me crazy.
63% said that journalists are an enemy of the people. You have sold out to this con man so bad
you are betraying the founding principles of this country.
Oh, but free speech on campus! Professors are getting fired!
You know what? If you actually knew anything about higher education or universities,
you would know that if something was happening to professors on any kind of level,
There would be a college-level study about it, and there is and you're right
Professors do occasionally get fired for controversial political speech the funny thing about it. They're mostly
liberals not conservatives  There is no free speech crisis happening on campuses. That's not a thing. It's made up
It's made up to appeal to people who've never been there
Who don't know
Oh, but they're protesting the speeches
Yeah, they are.
That's the First Amendment in action.
Let me break this down into crayons for all of you ardent patriots and First Amendment
supporters that have apparently never read it.
The First Amendment protects you from the government.
It doesn't protect you from your neighbor saying what you're saying is bad.
That's not a thing.
The thing is, the far right doesn't want speech.
They don't want free speech.
They want a platform.
that simple. That's how they wound up at universities to begin with. Why? Because
they went out trying to find platforms. They went out looking for locations and
they couldn't get them. Nobody would let them come because private institutions
don't have to. They have no reason to do it. They have no reason to do it.
Universities are more accepting of dissenting viewpoints. That's how they
wound up there. There is no free speech crisis. And then the students, well, they
don't want that idea. They don't want to hear it. And they staged their own event,
the protest, which is First Amendment. It's protected by that. Nobody owes you a
platform. Free speech doesn't mean you're gonna be listened to. It doesn't mean
your idea is gonna be accepted. How entitled are you? That's not a thing.
Free speech means you get to speak, period. Doesn't mean that somebody has to put you
on stage. And the other idea is that, well, you have to give equal time to different ideas.
Who told you that? That's not true. That is not true. That's not a thing either. You have
to give equal time to equal ideas to be ethical. Your idea isn't equal. It's not. It's ancient.
It's obsolete. Institutional bigotry failed. People didn't like it. Wars were fought over
it. It's not coming back. And to expect institutions of higher learning, which
typically look forward to help you sway people to your ideas from the 30s is a
little silly to me, but whatever. And you don't get to cry about people being
deplatformed either. That's not something you get to do. What did y'all want done
to cap? Football man didn't stand up for the flag, boycott the NFL, and what
What happened?
He's still out there, right?
He believed in what he was doing.
You never heard him cry.
He believed in what he was doing.
He kept talking.
A good idea will find a platform.
Doesn't need one handed to him.
You don't need the government's gun.
You better let this guy talk.
Yes, the right is the one that wants to use the government's gun to come out and say,
You have to let this person talk or you better not talk.
Don't build your religious facility.
You better put this person on stage.
Cap proved that's not the way it works.
You can be deplatformed and if the idea is solid, another platformer will arise where
you can create your own.
Yeah it's coming from the right.
Now there are Democrats who propose things occasionally that curtail the First Amendment.
And I'm equally as frustrated with them.
But they don't pretend that they're all about the Constitution.
That they're all about preserving the founding principles of this country.
No.
That's the right and y'all are utter failures at it.
Anyway, it's just a thought y'all have a good day

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}