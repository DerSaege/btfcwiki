---
title: Let's talk about what we can learn from zombies....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=o-plIVJo0nw) |
| Published | 2019/09/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Compares zombie movies to surviving natural disasters and life in general, noting that the rules are the same.
- Suggests framing natural disaster preparedness for kids as a zombie apocalypse to make it less scary.
- Explains that learning survival skills changes how people view the world and what's truly valuable in life.
- Outlines key rules for surviving a natural disaster by drawing parallels to a zombie outbreak.
- Emphasizes the importance of staying with a group, finding safety, and maintaining communication in a crisis.
- Points out that most people in disaster situations are good but cautions about individuals looking to exploit vulnerabilities.
- Shares a real-life example where people overlooked needed supplies during a crisis due to societal norms.
- Stresses the importance of improvisation and seeing things differently to navigate challenging situations effectively.
- Encourages teaching youth survival skills to empower them to change their circumstances and use resources creatively.
- Concludes by underscoring the potential for individuals to reshape their world by realizing the possible amidst the seemingly impossible.

### Quotes

- "The rules are the same."
- "You see what things could be, if you looked at them in the right way."
- "People get in that mindset of, I have to get water."
- "The impossible is possible."
- "They can use the tools around them to make something else."

### Oneliner

Beau compares surviving natural disasters to zombie movies, urging a shift in perspective to see possibilities amidst challenges and empowering youth to reshape their world.

### Audience

Parents, educators, disaster preparedness advocates

### On-the-ground actions from transcript

- Teach children survival skills through fun activities and games (implied)
- Organize community workshops on disaster preparedness and improvisation techniques (implied)
- Encourage youth to think creatively and problem-solve in challenging scenarios (implied)

### Whats missing in summary

The full transcript provides a detailed analogy between surviving natural disasters and zombie outbreaks, stressing the importance of preparedness, group cohesion, resourcefulness, and a shift in mindset towards possibilities.

### Tags

#NaturalDisasters #ZombieMovies #SurvivalSkills #YouthEmpowerment #DisasterPreparedness


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're gonna talk about zombie movies, kind of.
We're gonna talk about what they can teach us
about life in general and about surviving natural disasters
because the rules are the same.
The rules are the same.
I've mentioned before that if you wanna talk to your kids
about natural disasters and how to prepare
for hurricanes, earthquakes, this type of thing,
you can frame it as a conversation about zombies
And oddly enough, talking to children about the zombies
is less scary than talking to them about hurricanes,
because zombies aren't real.
And they know that, but the rules are the same.
More importantly, when people start
to learn survival skills, they start
to look at the world in a very, very different way.
They see the impossible as possible.
They see what things could be, if you
looked at them in the right way.
And they learn what's really important in life.
So what are the rules?
Zombie movie.
You're surviving the zombie outbreak.
What do you have to do?
You never travel alone.
You stay with your group.
You get somewhere safe, and you stay put.
That's what you need to do in a natural disaster.
Communications with the outside world.
It's critical.
What you're really trying to do, you're
trying to raise the other guys.
Get them on the radio, talk to them,
let people know you're alive,
what you're doing in a natural disaster.
Most of the people you come across are gonna be good people,
just in a bad situation like you are.
Every once in a while, you're gonna meet that person
that's trying to exploit things.
Supplies are pretty much everywhere,
and you just have to know where to look
and how to think about them.
After Michael, there was a group of people
that was sheltering in a restaurant,
and they needed medical supplies.
Nothing major, but they had an injury.
It never occurred to them that there was a first aid kit in the kitchen because they
were up in the front of the house.
Those rules of society, they were still in place.
They didn't go back into the kitchen for whatever reason.
They stayed up front.
There was a first aid kit.
There was what they needed 15 feet away.
But we're so used to that consumer life that if we didn't buy it, it doesn't exist type
of thing that they never thought about. I assure you there are people right now in the Bahamas that
are, I don't want to say looting because that's not the right word, they're surviving however they
have to, and they are finding supplies in odd places. You know and even prior to the event,
prior to Michael, I watched two people argue over water in a store and they were between a display
of Gatorade and a cooler full of Milo sweet tea.
If you were just looking for something cheap to flush your toilet, the sweet tea would
have worked.
If you were looking for something potable water, the Gatorade would have worked.
But people get in that mindset of, I have to get water.
You have to get something wet.
And beyond that, it teaches you to improvise.
And that's one of the most important skills in life,
in my opinion.
It's one of those things that lets you see
what things could be, rather than what they are.
And when you do that, you start to see everything around you.
This is a battery, if you can't tell.
As components, rather than some finished consumer product,
you learn to improvise.
And when you do that, you learn to see the world in a different way because things can
be something else.
I think it's really important for the youth of today to learn survival skills for that
reason.
Because I think with everything going on in the world today, it might be really important
for them to understand that the impossible is possible.
that the way things are isn't what they have to be.
That they can change it.
That they can use the tools around them to make something else.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}