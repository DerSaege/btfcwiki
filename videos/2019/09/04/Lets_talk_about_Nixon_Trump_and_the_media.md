---
title: Let's talk about Nixon, Trump, and the media....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=EKhYeII14XM) |
| Published | 2019/09/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Trump's strained relationship with the media began early in his campaign, attacking them consistently.
- Trump demands total subservience, compliance, and complicity from news outlets, similar to a dictator.
- Fox News, previously spared from Trump's attacks, is now facing his wrath for not fully supporting his agenda.
- Some Fox News anchors are standing up to Trump, pointing out that they don't work for him.
- Beau criticizes Fox News for justifying and promoting the administration's heinous acts to the American people.
- Trump's targeting of CNN and now Fox News shows his disregard for any dissenting voices in the media.
- Beau points out that Fox News is facing consequences for not taking a stand against Trump earlier when he attacked other journalists and threw children in cages.
- The attempt by some personalities at Fox News to reclaim objectivity now that they are under attack is seen as insincere by Beau.
- Beau finds it entertaining that Trump's actions are leaving former allies like Fox News on the sidelines.
- Comparisons are drawn between Trump and Nixon in terms of their controversial relationships with the media and desire for revenge upon leaving office.

### Quotes

- "Yeah, you do."
- "You standing up to Trump now means nothing."
- "When the president branded other journalists as fake news, that's when you should have said something."
- "Now you want to reclaim some of that objectivity. Doesn't matter, nobody's buying it."
- "Without Trump, what have they got?"

### Oneliner

President Trump demands complete subservience from the media, leaving even former allies like Fox News facing consequences for not taking a stand earlier against his actions.

### Audience

Media consumers

### On-the-ground actions from transcript

- Stand up to demands for subservience from any source, whether political or otherwise (implied)
- Take a stand against attacks on journalistic ethics and freedom of the press (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of President Trump's relationship with the media and the consequences faced by news outlets like Fox News for not taking a stand against his actions sooner.

### Tags

#Media #Politics #FreedomOfPress #Journalism #Trump


## Transcript
Well, howdy there, internet people, it's Beau again, so tonight we're going to talk
about two presidents and their relationship with the media, with the news.
President Trump has never had a great relationship with the media as a whole.
He began attacking them early.
It was part of his campaign.
There was only one outlet that was spared his wrath, and over the last week or so, we
have seen the President turn on him.
Fox News is discovering that Trump, like any would-be dictator, demands complete subservience,
complete compliance, and complete complicity with his agenda.
news organizations, it's what he wants.
In a show of journalistic ethics that has been sorely lacking over
at Fox for the last few years, a few of their anchors have attempted
to point out that they don't work for him.
Yeah, you do.
You did anyway, for the last few years, you justified and promoted
Every heinous act this administration could dream up.
That network promoted these ideas to the American people.
And now that they are discovering, like all people with his ilk, that, well,
First, he came for CNN, and I didn't say anything
because I wasn't CNN.
Yeah, well now he's come for you.
You stepped too far out of line.
So now you have to suffer his wrath.
Pretty soon you will be branded fake news like everybody else.
And you're going to have to answer for that, the outlet
as a whole, and all of the personalities on it.
Because you didn't take a stand for what was right until it affected you personally.
You standing up to Trump now means nothing.
It means nothing.
When the president branded other journalists as fake news, that's when you should have
said something.
When the president threw children in cages, that's when you should have said something.
Now, it means nothing.
It's just a show that you see the winds changing, the political winds are shifting and you don't
want to be left holding the bag supporting a loser.
So now you want to reclaim some of that objectivity.
Doesn't matter, nobody's buying it.
You were a pet and now like many people in his administration, he's left you on the side
the road. Well, that's your problem. It is entertaining to the rest of us, to be
honest. There was another president that had a controversial and adversarial
relationship with the media, and in his last press conference he said, just think
how much you'll be missing. You won't have Nixon to kick around anymore. And I
I find that quote just absolutely hilarious, because for whatever reason, I read an article
that's teasing Bill O'Reilly's new book.
I can't remember the name, I don't care to look it up.
He interviewed the president, and the president said, when I leave office, I'll have my revenge.
Without Trump, what have they got?
Man.
He shows you the similarities in their personalities, doesn't it?
Almost as if he was paraphrasing them intentionally.
I mean, he wasn't.
We know that because in order to do that, Trump would have to know something about American
history, and we know he doesn't, but he goes to show that separation that should have always
been there, should have always been there.
Biden didn't have a favorite news outlet.
He hated them all because they were all out to get him.
Trump did because there was one outlet that didn't do their job.
Anyway it's just a thought.
y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}