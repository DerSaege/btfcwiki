---
title: Let's talk about an appeal to the gun crowd....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=FyngQ4xNHFA) |
| Published | 2019/09/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Points out how owners can impede the push for gun legislation by taking individual actions.
- Stresses the importance of securing firearms to prevent access by unauthorized individuals, especially kids.
- Recommends against open carry and advocates for concealed carry permits for tactical advantages.
- Talks about ending the drug war to reduce gun violence and eliminate the pretext for gun control.
- Encourages responsible gun ownership, including securing weapons, ensuring background checks for private sales, and using good judgment.
- Suggests supporting the intent behind the Second Amendment by selling or trading guns with known individuals.
- Criticizes the gun culture that focuses on looking tough rather than responsible ownership.
- Urges gun owners to prioritize being responsible over just following poorly written gun laws.
- Emphasizes the negative impact of glorifying violence and the importance of discouraging such behavior.
- Concludes by criticizing the gun crowd for prioritizing image over safety and responsible ownership.


### Quotes

- "Secure your firearms."
- "Stop behaving like children."
- "Just because you can do something doesn't mean that you should."
- "The gun crowd is its own biggest enemy."
- "They have no idea what it's for."


### Oneliner

Beau outlines practical steps for gun owners to derail gun legislation, focusing on responsible ownership and ending the drug war.


### Audience

Gun owners


### On-the-ground actions from transcript

- Secure your firearms to prevent unauthorized access (implied)
- Support ending the drug war to reduce gun violence (implied)
- Obtain a concealed carry permit for tactical advantages (exemplified)
- Prioritize responsible gun ownership over just following laws (implied)


### Whats missing in summary

The full transcript provides in-depth insights into responsible gun ownership and the impact of the gun culture on legislation.


### Tags

#GunOwners #ResponsibleOwnership #Legislation #SecondAmendment #EndingViolence


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we're going to talk about what owners can do
to completely derail the drive for legislation.
And I know that sounds weird,
because there's a whole bunch of lobbying groups out there
that have you conditioned to believe
that you need them to defend your rights.
You don't, you don't.
There's a whole bunch of little things you can do
as an individual to completely derail
the drive for legislation,
to completely end the desire to have gun control.
And we're gonna go through them.
For those who don't know me, I don't own.
I don't.
I don't own a gun today.
15 years ago, I was that guy.
I was that guy that had the locked room
with an arsenal in it.
Today, I don't own, but I still understand
the dynamics of this argument.
I'm still an avid supporter of the Second Amendment.
So here you go.
Let's start off.
Secure your firearms.
Lock up your weapons.
Every time there's an incident at some school, the kid got the firearm from somebody else.
They're too young to buy one.
They get the weapon from somebody else.
Secure your firearms.
Now the real reason you should want to do this is because it's going to save kids'
lives.
That's the good reason for wanting to do it.
I have come to realize that to many in the gun crowd, that doesn't matter.
So the alternative reason is that, well, it's going to take away that talking point.
You're going to eliminate that reason for wanting gun control.
Secure your firearms.
And yes, I am aware that First Sergeant Whatever on his YouTube channel said that you had to
be prepared at all times.
Yeah, he's drawing on his experiences in Fallujah.
You live in Falls Creek, North Dakota.
The situations are a little bit different.
Secure your firearms and if you aren't going to train to develop the fine motor skills
to open a safe quickly, you're going to lose the firefight anyway because you're not training.
So I don't want to hear about you needing to be ready.
Secure your firearms.
Private sales in a lot of areas, that's legal.
The intent.
We like to talk about the intent of the Second Amendment, right?
What's the intent behind private sales for you to sell or trade a weapon with somebody
that you know?
Somebody you know their background, you understand their mental state.
It's not to sell one out of the back of a truck or buy one out of the back of a truck.
Stop making that cottage industry profitable because you know that person isn't going to
exercise good judgment about who they're selling to.
And that's how bad people end up with weapons.
People that really shouldn't have them.
Loaning a gun, same rules apply.
It's not, you're not supposed to loan a gun to the guy you see at the range once a month.
That doesn't even make sense.
For friends, somebody who's going hunting doesn't have the right rifle for the type
the game they're going after, or somebody who's being stalked and you want to loan
them a firearm, somebody you know.
That's what it's for.
That's why it's legal in some areas.
Use good judgment.
Stop being so concerned about being a law-abiding gun owner and be a responsible one.
Here's one for you.
Support ending the drug war.
What does that have to do?
Anytime the gun control conversation comes up, what's the pro-gun side say?
They point to major cities and they say, look, they have gun control and it doesn't work.
Of course it doesn't work.
There's a black market.
There's a profit motivator for violence.
Get rid of it.
Get rid of it.
You eliminate those gun deaths, there's less outcry for gun control.
It's funny because it's normally the people on the right who are Second Amendment supporters
and they also support the drug war. They support taking somebody else's rights away,
the right to determine what goes into your body, and then they're surprised
when the effects of that get used as a pretext to take away their rights. Here's
a crazy idea. Give freedom a chance. Stop supporting the drug war. Stop open
caring. I know it's legal in a lot of places. Stop. Unless you're in a rural
area where everybody is comfortable with firearms around you. Stop. Again, it has to do with
being a responsible gun owner. If you don't have the judgment necessary to know that walking
into a big box store shortly after a tragedy carrying a rifle and wearing a plate carrier,
if you don't know that's not a good idea, you shouldn't own a firearm because you have
poor judgment. And since scaring people and the effects on other people doesn't
really register with the gun crowd, we'll do it another way.
Tactically, it's better for it to be concealed. Get your permit, carry concealed.
You're giving away the element of surprise when you open carry. You just
become the first target. It's about that image. You want to look tough. I got my
gone. Clint Eastwood, no you're not. No, you are not. You're not John Wayne's son.
Image and that has a lot to do with it. That has a whole lot to do with it more than I
think a lot of people would like to admit because it's those same guys that have that
photo of them at the range with their plate carrier that's never been dirty, that has
has 300 rounds of ammo on it, no first aid kit,
holding their rifle like this as their profile
photo on social media.
Because the gun makes the man look tough.
No, you don't.
You're carrying more kit to go to the range
than a contractor in Iraq.
You don't look tough.
You look stupid.
And see, the thing is, other people see those images.
young people who maybe are searching for an identity and what are you teaching
them? The gun makes the man. Those people who are a little unhinged, what are you
teaching them? Violence is the answer. Stop. Stop. You're sending the wrong
message and you don't look tough. To those, the gun doesn't make the man. To
those people who know, you're the weapon. The firearm is just a tool. It's the way
it's supposed to be anyway. Stop glorifying violence and you will see less
of it. You shouldn't want to hurt people. Stop calling yourself law-abiding gun
Stop. Stop focusing on being law-abiding.
We all know that gun laws in general are poorly written, and they don't have the desire, they
don't have the impact that those who wrote them desired. We know that every ruling that
comes down from the ATF is just idiotic. Doesn't even make sense, right? So why would you assume
the other side of that is true? Why would you assume what you're allowed to do by the
law is the right thing. Stop being concerned about being a law-abiding gun owner and be
concerned about being a responsible one. Just because you can do something doesn't mean
that you should. Use good judgment. If the gun crowd exercised good judgment and stopped
behaving like children in search of some symbol of masculinity to prove that they're a man,
the desire for gun control, it would evaporate.
The gun crowd is its own biggest enemy.
Stop behaving like children.
Use good judgment.
And if you don't have good judgment, you shouldn't own a firearm.
I am as pro-Second Amendment as they come.
But the more I watch the gun crowd, the more I'm like, you know what, they've completely
lost the plot.
They have no idea what it's for.
They just think it's to look cool.
And I care less and less about it.
I don't care about defending your rights as much.
Why?
Because you don't care about anybody else.
You just want an image.
You want something to put on Instagram or Facebook so you look tough.
You don't.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}