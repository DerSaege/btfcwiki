---
title: Let's talk about the Great Fire of London and what it foreshadows....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=AGJJs_nTow8) |
| Published | 2019/09/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the Great Fire of London in 1666, starting with a bake house near Pudding Lane.
- Explains that a spark fell into fuel, setting off a massive fire due to the kindling-like surroundings.
- Around 60 to 100,000 people were left homeless, with a third of London destroyed.
- Despite claims of only six to eight recorded deaths, the true toll remains unknown.
- Describes the lack of infrastructure and planning that exacerbated the disaster.
- Mentions the warnings and knowledge of potential fire spread, but profit interests hindered preventative measures.
- Points out the parallels between this historical event and today's climate crisis.
- Challenges those who deny climate change to research and acknowledge the overwhelming evidence against their arguments.
- Emphasizes the disproportionate impact of such crises on the powerless and poor.
- Raises the issue of prioritizing profits over necessary investments in infrastructure and planning.

### Quotes

- "It's funny. Americans are the way we are."
- "Take your arguments against it, your go-to arguments. Type it into Google, followed by the word debunked, and just read."
- "People denying it when anybody who looks into it can see that it's very evident."
- "Just like this."
- "We don't want to waste money on something."

### Oneliner

Beau delves into the Great Fire of London, drawing striking parallels to today's climate crisis and the repercussions of profit-driven denial.

### Audience

Climate change activists

### On-the-ground actions from transcript

- Research the overwhelming evidence supporting climate change and share it with skeptics (suggested)
- Advocate for prioritizing infrastructure and planning for climate resilience in your community (implied)

### Whats missing in summary

The emotional impact and urgency conveyed by Beau as he draws a powerful connection between historical negligence and present-day climate crisis.

### Tags

#ClimateChange #HistoricalDisaster #Infrastructure #Planning #CommunityAction


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we're going to talk about the Great Fire of London in 1666.
So what happened?
Short version.
There was a bake house near Pudding Lane.
The story is that it was on Pudding Lane.
It wasn't.
It was just off of it on a fish yard, a little cove called Fish Yard.
Spark fell into some fuel, it's really that simple.
In a nearby building there was pitch, everything around it was kindling to begin with, it was
all wood and thatched roof, it was a little windy.
The claim is that only six to eight people died during the fire.
That's six to eight important people who had their names recorded.
We have no idea how many people actually died because they didn't keep records of the unimportant
people.
A third of London was destroyed.
60 to 100,000 people rendered homeless.
Fire raged for days.
People had to flee.
Some of them just went outside the city gates.
Others became refugees, went to other cities.
There were so many that an edict had to be put out saying,
you have to take them.
Let them come in and do their jobs, work in their trade.
was a disaster. Rebuilding didn't go the way it could have had there been a little
bit of planning. Why did it happen? There were warnings, there were warnings, tons
of them. The fact that if a fire started that it would spread through the whole
city was completely known, in fact so well known that there were plots by
people to do it. And those plots had been disrupted. So the establishment was
aware of the problem. They really were. And they had little things to make you
feel better. They had the equivalent of fire engines, but some of them didn't have
wheels. They couldn't get them to where they were needed. There weren't enough of
them. They were just there for peace of mind and small problems. There was no
infrastructure. There was no infrastructure. There were no fire breaks, for example, breaks
between buildings to stop the fire from spreading. It didn't happen. They didn't have them.
They knew this was an issue, but they didn't have them because people didn't want to tear
down their buildings and lose their land because that would interrupt their profits. Makes
sense. Even during the fire, when those fighting the fire said, you know, we can tear down
these buildings and stop it from spreading. No. Didn't want to do that initially. Didn't
want to do that initially. It had to get real bad before the establishment let it interfere
with their profits. No access to the river. There were a bunch of stuff. A bunch of problems.
All that could have been fixed and all that were widely known. The problem is that nobody
believed it. Not enough to interfere with their profits. Not enough to make the changes
because it was too expensive.
All they needed was infrastructure.
By the time they admitted they needed it, well, it was too late.
It was too late, a third gone.
Man, if that is not perfect foreshadowing for today's climate problem, right?
ignoring it for the sake of profits, people denying it when anybody who looks into it
can see that it's very evident.
All it takes is infrastructure and planning, but that costs money.
We don't want to waste money on something.
Wasn't a matter of if, it was a matter of when, just like this.
And at the end of the day, when it happens, well, there'll be a few, six or eight important
people that have a problem, but they'll be fine.
It'll be the poor, those people with no power, those will be the people that suffer the most.
I have a challenge for those people that dismiss this crisis.
Take your arguments against it, your go-to arguments.
Type it into Google, followed by the word debunked, and just read.
There's not a single argument put forth by the side that denies this exists that
hasn't been completely destroyed.
It's funny.
Americans are the way we are.
We disbelieve a lot of things when it comes from authorities,
when it comes from large corporations, especially oil
companies.
But for some reason, this particular issue, yes, every
scientists, journalists, environmentalists, climatologists...everybody in the world is
lying to you except for the oil companies and the politicians they pay...anyway it's
It's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}