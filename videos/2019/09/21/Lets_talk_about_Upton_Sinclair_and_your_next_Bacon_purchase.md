---
title: Let's talk about Upton Sinclair and your next Bacon purchase....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=fmsdS9f0M4A) |
| Published | 2019/09/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the topic of Upton Sinclair, an American author known for writing almost 100 books and winning the Pulitzer Prize for fiction.
- Upton Sinclair's books, often journalism disguised as fiction, had a significant impact in the United States.
- "The Jungle," one of Sinclair's famous works, written in 1906 about the meatpacking industry, led to the passing of the Pure Food and Drug Act and the Meat Inspection Act.
- Upton Sinclair is credited with being a driving force behind better policing in slaughterhouses due to his impactful book.
- The USDA is removing prohibitions on line speed in slaughterhouses, allowing them to self-police, which can have detrimental effects on food safety.
- The industry, not just Trump, has been lobbying for removing regulations on line speed for profitability reasons.
- Despite past failures in pilot programs, the plan to let slaughterhouses develop their own standards is still moving forward.
- Companies prioritizing profits over safety may lead to a rise in foodborne illnesses from pork.
- Beau questions the effectiveness of companies self-policing and raises concerns about potential consequences.

### Quotes

- "It's going to make it more profitable for them to staff the safety inspectors than it is for the government to do it."
- "In my ideal world, yeah companies like this would police themselves, but in my ideal world they'd police themselves and we know that isn't what's going to happen here."
- "It's something that we might want to keep in mind."

### Oneliner

Beau warns of the dangerous implications of allowing slaughterhouses to self-police, prioritizing profits over consumer safety.

### Audience

Food safety advocates

### On-the-ground actions from transcript

- Contact local representatives to advocate for stricter regulations on slaughterhouses (implied)
- Join or support organizations that focus on food safety and regulation (implied)

### Whats missing in summary

The emotional impact of potential food safety risks and the call to action for individuals to advocate for safer food practices.

### Tags

#FoodSafety #Regulations #Slaughterhouses #ConsumerAdvocacy #UptonSinclair


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're gonna talk about Upton Sinclair.
A lot of Americans just went, oh, God.
We're forced to read him in high school here.
For those overseas, who is an American author,
very prolific, wrote almost 100 books,
won the Pulitzer Prize for fiction,
Time Magazine described him as having every gift
except for humor and silence.
It's funny that he won the Pulitzer Prize for fiction
because most of his books are journalism,
thinly disguised as fiction.
Because his books were written in that manner,
a lot of his journalism got out, got further,
had more of an impact.
A lot of his books had wide-ranging impact
in the United States.
The Brass Check is one of his more famous ones, and it is widely credited as being the
catalyst for the code of ethics for journalists, the first one in the US.
One most people are forced to read in high school is The Jungle, it was written in 1906
about the meatpacking industry.
A lot of his books dealt with working conditions, the coal industry, the auto industry, everything.
But this one dealt with the meatpacking industry and it talked about how the
speed of the line that the pork moves down had a lot to do with whether or not
it was clean. Line speeds. Talked about how the animals were sometimes covered in
things you don't want your food covered in and it created an outcry. Shortly
thereafter the Pure Food and Drug Act was passed. Shortly after that the Meat
Inspection Act. Upton Sinclair is widely credited as being a driving force behind
that because of his book. It's when slaughterhouses started getting policed
a little more.
Now fast forward to today, this week.
The USDA is removing prohibitions on line speed.
They're going to let slaughterhouses self-police again.
Yeah, these are not companies that are socially responsible, they actually have a long track
record of doing things that are, let's just say, not in the best interest of
consumers and society in general. Now, it would be easy to blame this on Trump,
and there is a lot of this that it is his fault because it is going through
under him. However, it should be noted in the interest of fairness, the industry
has been lobbying for this for a very long time.
I know it was going on under Obama,
I think it was even going on under Bush.
They've been trying for this for a long time because,
well, it's going to make it more profitable.
I want you to think about that.
It's going to be more profitable for them
to staff
the safety inspectors
than it is
for the government to do it
they're going to have to pay people to do this but it's going to be
more profitable
oh forgot to tell you they get to develop their own standards
in a lot of cases
basically foodborne illness
from pork
it's a matter of time
something you might want to be aware of. There was a pilot program that tested
this out. Almost everybody that was involved with it that spoke out said
that it was just an utter failure. It was an utter failure, but we're going ahead
with it anyway. It's something that we might want to keep in mind. You know in
my ideal world, yeah companies like this would police themselves, but in my ideal
world they would police themselves and we know that isn't what's going to
happen here because it's going to be cheaper to settle the lawsuits than it
is to pay employees.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}