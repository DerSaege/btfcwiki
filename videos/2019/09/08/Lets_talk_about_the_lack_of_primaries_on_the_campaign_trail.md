---
title: Let's talk about the lack of primaries on the campaign trail....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-9g5VkROBys) |
| Published | 2019/09/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The GOP is canceling primaries across the country to prevent challenges to Trump, showcasing their fear of internal dissension.
- Rules were bent in South Carolina to cancel the primary, possibly leading to legal issues.
- The GOP establishment worries that if Trump doesn't win by a large margin in the primaries, it will reveal the weakness of his support and policies.
- Recent shifts in support are evident, with once fervent Trump supporters now concealing their loyalty.
- Trump's ineffective leadership and controversial actions have led to declining support even within his base.
- The GOP establishment's strategy is to prevent challenges to Trump in the primaries and rely on party loyalty during the election.
- Similar tactics failed for Democrats in the past, and Beau predicts a similar outcome for Republicans if they continue to support Trump.
- The GOP establishment's decision to protect Trump is causing more dissent within the party, with many Republicans fearing electoral losses.
- Beau suggests Democrats stay quiet and let the GOP make mistakes that could lead to the downfall of the establishment.
- Republicans who have distanced themselves from Trump may face challenges in influencing party decisions due to authoritarian tendencies within the GOP.

### Quotes

- "They're scared of the dissension within their own party."
- "It's going to tear down the entire GOP establishment."
- "If the election goes the way I think it might, it's going to be fantastic."
- "Your opinion doesn't matter. It's what happens when authoritarianism infects a party."
- "It's going to be a dumpster fire of just epic proportions."

### Oneliner

The GOP is canceling primaries out of fear of challenging Trump internally, risking revealing his weak support and policies, causing dissent within the party.

### Audience

Voters, Political Activists

### On-the-ground actions from transcript

- Support candidates who challenge the status quo within the GOP (exemplified)
- Stay informed about GOP decisions and internal dynamics (suggested)
- Participate in local politics to influence party directions (implied)

### Whats missing in summary

Insights on potential impacts of GOP's strategy on future elections and party dynamics.

### Tags

#GOP #Primaries #Trump #RepublicanParty #PoliticalStrategy


## Transcript
Well, howdy there, Internet people, it's Beau again.
So today we're going to talk about the campaign trail and the lack of primaries in the GOP.
The Republican Party is canceling primaries across the country.
They're trying to make sure that nobody can challenge Trump because they're scared.
They are scared.
They're scared of the dissension within their own party.
They've spun it to make it say, oh, well, we want to save our money for the real election
and there's no legitimate challengers.
Oh yeah, there are legitimate challengers, there's a bunch of them.
And it's not about money.
It's about showcasing the weakness of Trump within the Republican Party.
They're afraid to do it.
South Carolina, the executive committee of the GOP there voted to cancel the primary,
and that's all fine and good.
However the rules say that they have to do it at the state convention, and they didn't.
They didn't.
They're going out of the way.
bending rules they know exist, and they're probably going to get sued over it to be honest.
You have to wonder why.
It's not that they're worried he's going to lose, they're worried, it seems like they're
worried that he's not going to win by much.
That's going to show exactly how many conservatives refused to become light fascists and defected,
moved away from it.
They made the moral choice and they're like, no, no, no, no, no, not going to do this.
Fool me once, shame on you.
That's what they're worried about because if his numbers aren't high in these primaries,
and they probably won't be, if the numbers aren't high, well then campaign contributions
across the Republican Party are going to slip because the GOP establishment tied their wagons
to Trump's campaign trail.
They adopted all of these policies, and it's going to show how weak those policies are.
Florida just had a shakeup in the GOP, in the state GOP, and they brought in a new guy
to run it who was handpicked by Trump.
And when it happened, one of the quotes that went out in a press release was that they're
going to work to deliver Trump for Florida.
Yeah, you're going to have to.
You've probably seen the same thing that I have.
A year ago, up here in the Panhandle, this is MAGA country.
A year ago you couldn't drive across town without seeing half a dozen Trump bumper stickers.
Town's like two miles wide.
Today you can drive around all day and not see one.
Most have been covered up by bumper stickers supporting the local high school band or something,
anything to conceal that once fervent support for the President.
But it's slipping, of course it's slipping.
He's trying to take funding from a school on a military base that serves the dependents
of active duty to fund a wall nobody really wants.
And everybody knows it's going to be ineffective, it's just a symbol, it's a monument to himself,
everybody knows that. It was a good chant. That was it. They've seen the ineffective
leadership and they don't support him anymore. So they're going to try to stop the primaries
so that isn't highlighted. I think Kansas is doing it too. At the end of the day, the
The GOP establishment doesn't want these primaries, because they know that Trump's ego can't take this.
If he doesn't win these primaries by a
huge margin, well, he's going to double down and try to appeal to that ever-shrinking base again.  base again, just
keep going after, keep saying crazier and crazier things, keep doing things  that are morally repugnant, and drive more
and more people away.  So their hope is that since they're already on the campaign trail with them, wagons are
tied together, well we're going to circle those wagons, and we're not going to let
anybody challenge Trump, and then they're going to count on Republicans showing up to
the polls and just voting the party line.
The thing is, I've seen this strategy before.
The Democrats tried it in a recent election.
Despite massive opposition to a candidate, they ran her anyway, and they just expected
the Democrats to show up and vote for her, and they stayed home.
Trump won.
I have a feeling that that may happen again.
We may see that play out in reverse with a whole bunch of Republicans just sitting this
one out.
Because well, they can't associate with it.
They can't continue to support him because of the policies that he has shown.
The funny part about it is that this is upsetting rank-and-file Republicans even more.
The GOP establishment is making this call to circle the wagons around Trump, and it's
causing more dissension because there's a whole lot of Republicans that know that if
Trump is at the top of that ballot, they're going to get slaughtered.
People are going to refuse to vote for him, and that's going to affect how many people
vote for them, and they know it.
There's a whole lot of newer congressmen and senators out there that don't have any power
within the establishment that know they're going to lose because the GOP establishment
You can't learn from other people's mistakes.
Now if you're a Democrat, my suggestion, when you hear about the scandals, South Carolina
broke their rules and don't say anything.
Don't say anything.
Just let them do it.
Let them do it.
Because if the election goes the way I think it might, it's going to tear down the entire
GOP establishment.
It's going to be fantastic, it's going to be a dumpster fire of just epic proportions,
it's going to be great.
So let them do it.
And if you are a Republican who is one of the moral ones that backed away, you can make
noise and you can try, you can do anything you want to, but the odds are they're not
going to let you primary them.
already made the decision for you because it's a great republic and your
opinion doesn't matter. It's what happens when authoritarianism infects a party.
The party members, well they don't get a say anymore. Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}