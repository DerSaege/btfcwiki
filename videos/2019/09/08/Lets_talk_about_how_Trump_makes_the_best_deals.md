---
title: Let's talk about how Trump makes the best deals....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=MuIQ4OlIuGo) |
| Published | 2019/09/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Expresses lack of surprise at the president's incompetence and hypocrisy on the international stage.
- Criticizes the choice of a real estate lawyer for Mideast peace negotiations.
- Calls out the president's decision to cancel peace negotiations with the Taliban.
- Questions the moral difference between the Taliban's actions and the president's deportation policies.
- Points out the futility of negotiating with the Taliban when the U.S. has announced its withdrawal.
- Accuses the administration of selling out allies in Afghanistan for domestic political leverage.
- Criticizes the president's negotiations with the Taliban while innocent people are dying.
- Compares President Trump's actions to his past criticism of negotiating with the Taliban.
- Condemns the administration's actions and questions if anyone is genuinely shocked by them.

### Quotes

- "What kind of people would kill so many in order to seemingly strengthen their position? Well for one, the Taliban."
- "Everybody knows this and everybody knows that we're selling out our allies."
- "The only thing we're doing right now is determining the body count."
- "I did for the first month or two, but at this point, every time they do something, I'm like, Now that wasn't as bad as I thought it was going to be."
- "It is going to happen."

### Oneliner

Beau expresses lack of surprise at the president's international incompetence, questions negotiations with the Taliban, and accuses the administration of selling out allies for political gain.

### Audience

Critically engaged citizens

### On-the-ground actions from transcript

- Protest against decisions that prioritize political gain over human lives (implied)
- Advocate for transparent and ethical international negotiations (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the administration's international actions and their consequences.

### Tags

#InternationalRelations #TalibanNegotiations #PoliticalCritique #HumanRights #EthicalLeadership


## Transcript
Well, howdy there, internet people, it's Beau again.
So at what point do we stop pretending we're shocked by the president's utter
incompetence and hypocrisy?
I, for one, am just flat out of the ability to pretend I'm surprised at anything
he does, this administration does, especially on the international level.
To catch you up on Mr. Art of the Deals week, he sent our homophobic vice president to go
meet the leader of Iceland who has a history of trolling homophobic heads of state.
That went over well.
pick to secure a Mideast peace deal between the Palestinians and Israelis quit, perhaps
the real estate lawyer for your extensive enterprises was not the best choice to tackle
one of the most difficult situations in international relations.
And then of course you have today's events, which days before the anniversary of September
11th, you know, I'm just going to read the tweets unbeknownst to almost
everyone, the major Taliban leaders and separately, the president of
Afghanistan were going to secretly meet with me at Camp David on Sunday they
were coming to the United States tonight and still, I'm not surprised unfortunately
in order to build false leverage, they admitted to an attack in Kabul that
killed one of our great, great soldiers and 11 other people, I immediately canceled the
meeting and called off peace negotiations.
What kind of people would kill so many in order to seemingly strengthen their position?
Well for one, the Taliban.
That's kind of what they're known for.
honestly not read any of the briefings he's given. Aside from that, I don't
know Mr. President, why'd you do it? Why'd you do it? Do you think there is some
moral difference between the leaders of the Taliban and the actions of their
subordinates killing innocent people and you ordering innocent people deported
back to their deaths?
There's not.
Of course you guys were going to meet.
Your moral compasses line up.
He goes on.
They didn't.
They only made it worse.
If they cannot agree to a ceasefire during these very important peace talks and would
even kill 12 innocent people, then they probably don't have the power to negotiate a meaningful
agreement anyway.
How many more decades are they willing to fight?
As long as they have to.
You would know this had you read the intelligence briefings.
They're the Taliban, that's again, kind of what they do.
There were some breaks in the movement, but they've literally been doing this for generations,
waiting, waiting for the occupation to leave.
And they have no reason to negotiate a meaningful agreement with the United States because you
already told them we were leaving.
When you did that, you removed any, any reason they had to care what we said.
They don't care.
That's why they're acting the way they are.
They know we're leaving.
So they're just biding their time and waiting.
It is not them that lacks the power to negotiate a meaningful agreement.
At the end of the day, we're leaving.
We are leaving.
You have made this abundantly clear.
Everybody knows this and everybody knows that we're selling out our allies.
That's why the negotiations over there taking place without the Afghan government.
And that's why you were meeting with the Afghan president separately, right?
We're selling them out.
Everybody knows this.
It's not a secret.
It is not a secret.
So the only reason to attempt to negotiate an agreement is so later you can pretend like
you didn't know they were going to break it.
Of course they're going to the second we leave.
So this whole charade is going on so you can get some leverage in elections here at home.
Getting leverage to maintain control of the country.
And while these negotiations are going on, innocent people are dying.
I don't know what kind of person would do that.
What's going to happen is going to happen.
It is going to happen.
The only thing we're doing right now is determining the body count.
And the longer you pretend that this agreement matters, the higher it's going to get.
Now, as always, there's a tweet from President Trump, and of course, it's completely fitting.
On January 13th, 2012, real Donald Trump tweeted, while Barack Obama is slashing the military,
he is also negotiating with our sworn enemy, the Taliban, who facilitated 9-11.
And of course, this tweet resurfaces while Donald Trump is slashing the military, pillaging
funds appropriated to it to pay for that stupid wall.
He is also negotiating with our sworn enemy, the Taliban, who facilitated 9-11 days before
the anniversary of 9-11.
Even with all of this, I'm still not surprised.
I am still not surprised.
So I am genuinely curious if this still shocks anybody.
If anybody looks at his actions on the international stage, at this administration's actions on
the international stage, and goes, wow, I can't believe they did that.
I did for the first month or two, but at this point, every time they do something, I'm like,
Now that wasn't as bad as I thought it was going to be.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}