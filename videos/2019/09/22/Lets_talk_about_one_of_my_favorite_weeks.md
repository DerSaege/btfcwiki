---
title: Let's talk about one of my favorite weeks....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=r1w9eF3Egxo) |
| Published | 2019/09/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces Banned Books Week, celebrating books like "Of Mice and Men," "Catcher in the Rye," "To Kill a Mockingbird," and others that have been banned or challenged in libraries and schools.
- Encourages people to pick up one of the banned books to learn about the norms and controversies of the time they were written.
- Points out that fiction can sometimes reveal truth faster than facts and expose readers to controversial ideas in a non-debate setting.
- Expresses the importance of individuals realizing truth on their own while reading, without outside influence or defense mechanisms.
- Explains his alternative to a book club by setting up an Amazon influencer store where viewers can access book lists without being priced out, supporting emergency preparedness gear as well.
- Invites viewers to participate by recommending books for the store, promoting engagement and collective reading.
- Urges viewers to participate in Banned Books Week by reading a book that someone doesn't want them to read, encouraging exploration of diverse perspectives.

### Quotes

- "Fiction gets to truth sometimes a whole lot faster than fact does."
- "Truth isn't told, it's realized on your own."
- "Go find a book that somebody doesn't want you to read."
- "It's just you in the book."
- "Y'all have a good day."

### Oneliner

Beau introduces Banned Books Week, encourages reading banned books to uncover truths and controversial ideas independently, and offers an alternative book club through an Amazon influencer store. 

### Audience

Book lovers, advocates of free speech

### On-the-ground actions from transcript

- Visit Beau's Amazon influencer store to access book lists and support emergency preparedness gear (suggested)
- Recommend books in the comments for Beau's store, promoting collective reading (suggested)

### Whats missing in summary

The emotional connection and personal growth that can come from exploring banned books and controversial ideas, as well as the impact of independent reading on understanding societal norms and historical contexts.

### Tags

#BannedBooksWeek #Fiction #Truth #Reading #FreeSpeech


## Transcript
Well, howdy there, internet people, it's Bo again.
So today is the beginning of one of my favorite weeks,
one of my favorite weeks.
It's a week where we celebrate of mice and men,
catcher in the rye, huck, fend, kill a mockingbird,
the color purple, and perks of being a wallflower.
No, it's not Great Literature Week.
It's Banned Books Week.
The American Library Association puts out a list of the most commonly banned or challenged
books in libraries and schools.
It's really cool.
It is really cool.
I definitely think it's something that people should do.
Pick up one of the books on their list.
One reason is that fiction gets to truth sometimes a whole lot faster than fact does.
Second is that it tells you about the norms of the time when the book was written, tells
you what people got offended by, what they flipped out about.
And that can tell you a lot about society in general at the time.
They expose you to controversial ideas in a non-debate setting.
And I think that's important because truth is not, truth isn't told, it's realized on
your own.
And when you're reading one of these books, it's just you in the book.
There's no outside influence, there's no defense mechanism you have to enact to show that your
side is right.
It's just you in the book.
So an idea you might reject if you came across it on the internet, you might accept, it might
take root because your guard's down.
Now this ties into a little bit of channel news too.
A lot of you over the last couple of months have asked for a book club.
I looked into it and we can't do it, not really.
The easiest way to do it is like a box subscription service, and by the time it's all said and
done, y'all are paying $24 for an $8 book, it also becomes a full-time job for me, and
I don't have that kind of time.
So I came up with an alternative, I set up an Amazon influencer store, and basically
the way that works, I should tell you that if you buy anything through it, I do get money,
you don't have to buy through it. You can go there, the lists are available for
anyone to see. You can look at the books and pick it up at your public library,
find it on LibriVox, whatever. And this way nobody gets priced out of being able
to do it. But if you go there now, it's amazon.com slash shop slash Boe of the
fifth column. If you go there now you'll find three lists. The first is, let's talk
about books to read which is based on the video that I did and it's a list of
books that fit into the little categories I talked about. The second is
banned or challenged books and then the third is stuff emergency preparedness
gear because people had asked a lot about that and the same thing applies.
It's just by the time you put it together it's it's too expensive and I
I don't have the time to pack boxes like that.
So those are the three that are already there.
I want to add a fourth one, which
is your book recommendation.
So put them in the comments.
And this way, I get something to read out of it, too.
But it is banned books week.
And I definitely think it's something
that you should participate in.
So go find a book that somebody doesn't want you to read.
see what's in it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}