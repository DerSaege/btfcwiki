---
title: Let's talk about opinions on Trudeau and Yang....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=4XwMiPZVrpA) |
| Published | 2019/09/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses questions about Trudeau and Yang, both involving a common component.
- Mentions the controversy surrounding Trudeau's blackface incident and the apology.
- Describes Southern American blackface as evil due to its malicious nature and historical context.
- Comments on the relevance of his opinion regarding Trudeau's apology, suggesting the focus should be on those directly impacted.
- Shares responses from people of color regarding Trudeau, including differing opinions on his actions and apology.
- Emphasizes the importance of understanding varying perspectives within impacted communities.
- Responds to questions about Yang and stereotypes, particularly regarding Asians excelling in math and becoming doctors.
- Advocates for seeking the opinions of those directly involved in a situation rather than assuming all opinions carry equal weight.
- Critiques the societal pressure to have an opinion on everything in the age of social media.
- Encourages considering whose voices need to be amplified and heard more in various situations.

### Quotes

- "Blackface is bad, okay? Southern American blackface is evil. It is evil."
- "If Jimmy insults Suzie, you don't ask Billy if the apology was sufficient."
- "Sometimes it's better to get the opinion of people that are actually involved."
- "Not everything requires your personal response."
- "Your personal response may drown out the opinions of those people that need to be heard more."

### Oneliner

Beau addresses controversies around Trudeau's blackface and Yang's stereotypes, urging to prioritize the voices of those directly impacted over irrelevant opinions in societal discourse.

### Audience

Social media users

### On-the-ground actions from transcript

- Reach out to and amplify the voices of those directly impacted by controversies rather than focusing on irrelevant opinions (implied).
- Engage in meaningful dialogues with communities affected by stereotypes to understand their perspectives (implied).

### Whats missing in summary

Beau's nuanced perspective and call for prioritizing the voices of those directly involved in controversial issues can best be appreciated through the full transcript.

### Tags

#Controversy #Opinions #PrioritizeVoices #SocialMedia #CommunityEngagement


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight I'm gonna answer a couple questions.
I'm gonna answer these questions
because I got each one of them more than once
from different people and they share a component,
at least in the answer anyway.
One's about Trudeau, the other's about Yang.
And, you know, I talked about the Trudeau thing
and I didn't get my opinion on it.
I think that surprised people.
So, alright, let me start off by saying that when I read the headline, I was picturing
Southern American blackface.
If you don't know what that is, good for you, good for you.
Blackface is bad, okay?
Southern American blackface is evil.
It is evil.
There's no way to do it without malice.
You know, it's not just a lack of understanding.
It's not just being a little insensitive, it's evil.
It has a long and horrible tradition here in the South.
So my opinion of it was very tainted by that.
But all of the questions I got involving Trudeau,
with the exception of one, included a sub-question.
Do I think the apology's enough?
I know there were a lot of jokes about the black delegation or the Latino delegation
trading to get me, and that's funny, that's cool, but I am still white.
He didn't dress up like me, I'm not part of the demographic that was depicted.
opinion on his apology is completely irrelevant. It doesn't matter. If Jimmy insults Suzie,
you don't ask Billy if the apology was sufficient. I think we should probably focus on the people
that were directly impacted. I reached out to every person of color I know north of the
border, both of them. I know that's not a huge sample size, but I got two
responses. One was that they believe he's a closet racist, but his racism doesn't
find its way into his legislation, so in a lesser of two evils kind of move,
they're willing to overlook it because they think the other guy is just an open
racist. So there's that. And then the other person kind of gave me a lecture on, you know,
I should know more than anybody that people can change over 20 years. And yeah, fair enough.
What I would point out is that even within that demographic there are varying views of
his apology and whether or not it was sufficient. I don't know that clouding the discussion
with the opinion of somebody that is completely irrelevant to the discussion
is a wise thing. I think we should probably focus on those that were
impacted. The other question was about Yang and whether or not I thought his
playing into stereotypes was acceptable. Now let me start by saying I may not
have all the information here I'm not sure what prompted this flood of
questions the only two that I'm aware that he played into is that Asians are
good at math and Asians become doctors a lot that's it okay so if this is about
anything else, disregard everything I'm about to say. In-group jokes, that's like 30% of
my content. So I obviously don't have a problem with in-group jokes in general. However, as
far as how the Asian American community feels, or the Asian community in general feels about
it, you'd have to talk to them. You'd have to talk to them. You know, there's
this idea, especially in the United States, that all opinions matter. And that
sounds cool. I don't know that that's true though. You know, if you're driving
down the road and there's a car that's flipped over and you stop to help and
And you pull the guy out, and the surgeon who also stopped said that you need to do
a needle decompression on his chest, and the investment banker that's walking by says
to amputate his leg, what are you doing?
Sometimes it's better to get the opinion of people that are actually involved, that
know a little bit more.
You know, I think that all of, well, I think most opinions should be heard.
That doesn't mean they all get the same weight.
And in today's world, where everybody has access to social media and can, can spread
their message.
I think people feel like they have to have an opinion on everything.
You don't.
Not everything requires your personal response.
And sometimes your personal response may drown out the opinions of those people that need
to be heard more.
It's just something to keep in mind.
Anyway, it's just a thought.
y'all have a good night!

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}