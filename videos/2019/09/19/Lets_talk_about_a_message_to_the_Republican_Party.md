---
title: Let's talk about a message to the Republican Party....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_zRjUCY3KRc) |
| Published | 2019/09/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Sends a message to the Republican Party as a blue-collar, rural, swing state resident.
- Expresses disappointment in the economic platform and stance on free trade and trickle-down economics.
- Questions the military involvement with Saudi Arabia and the arms sales to them.
- Criticizes the Republican Party for not upholding Second Amendment rights while enacting more gun control measures under the current administration.
- Calls out the GOP for canceling primaries, supporting the current administration's actions, and diverting funds from critical projects.
- Emphasizes that he doesn't base his opinions on skin tone, origin, religion, gender, orientation, or spoken language.
- States that he is not a bigot and focuses on judging individuals based on character rather than demographics.
- Points out a growing dissatisfaction among individuals like him with the Republican Party.
- Acknowledges differences in beliefs with progressive figures like AOC and the squad but respects their authenticity.
- Concludes with a note of concern for the Republican Party's loss of support from individuals like him.

### Quotes

- "I certainly don't think that come next fall I should see a gold star in front of his parents' house because you guys want to increase your stock portfolios."
- "And the other 60 to 70%, I can answer that in one sentence, I don't care about somebody's skin tone, country of origin, religion, gender, orientation, or the language they speak."
- "The reality is now, there's a whole bunch of guys, look like me, sound like me, real sick of you."
- "But I believe they believe it. That's more than I can say for you."
- "Y'all have a good night."

### Oneliner

Beau delivers a message to the Republican Party, expressing disappointment in their stances on economics, military involvement, gun control, and values, signaling a growing dissatisfaction among individuals like him.

### Audience

Blue-collar voters

### On-the-ground actions from transcript

- Reach out and connect with disillusioned individuals in your community who share similar concerns with political parties (implied).
- Engage in open dialogues with individuals holding diverse viewpoints to understand their perspectives and concerns (implied).

### Whats missing in summary

The full transcript provides deeper insights into the frustrations and shifting allegiances of blue-collar voters towards the Republican Party.


## Transcript
Well, howdy there, Internet people, it's Bo again.
So tonight we got a message straight to the Republican Party.
See, I don't know if you guys know this, but in my entire life, I've only gotten out
and supported one Democratic candidate, one.
He was running for county sheriff and had been personally known to my family since him
and my great-grandfather ran Shine together, one.
But I'm not a Republican.
See, I am that t-shirt, blue jeans,
and work boot wearing, truck driving,
white heterosexual male that lives
in the rural area of a swing state.
I'm the GOP's target demographic.
So what you got on the platform for me?
I, like most Americans, know that free trade is good
and trickle-down economics doesn't work.
So your economic platform's out.
I don't think the 19-year-old kid from down the road who just signed up should have to
go fight for the Saudis.
I don't think we should be arming the Saudis.
I don't think we should be allied with the Saudis.
I don't even know why we're doing this anymore.
We're a net exporter.
I certainly don't think that come next fall I should see a gold star in front of his parents'
house because you guys want to increase your stock portfolios.
I'm not a gun owner, but the Second Amendment is an important right, and you guys, at least
in rhetoric, have always been able to be counted on to defend that.
But now you've got that guy in the Oval Office who's enacted more restrictions on firearms
in two or three years than Obama did in eight.
And y'all had the chance to get rid of him, but you canceled the primaries.
That's you co-signing everything that he does.
While his attorney general wanders around Capitol Hill talking to you guys, asking y'all
What kind of gun control are you going to support?
What else do you do?
Approve projects without the funds to complete it, like that criminal justice reform bill
or that stupid wall, and then you take the money from DOD making that gold star in front
of his parents' house a whole lot more likely.
That's 30 to 40% of your platform right there.
Nothing in it for me.
And the other 60 to 70%, I can answer that in one sentence, I don't care about somebody's
skin tone, country of origin, religion, gender, orientation, or the language they speak.
I'm not a bigot.
I don't care if they're a good person, more power to them.
I don't care.
And that takes out the other 60 to 70% of your platform.
I know what you're thinking.
Is this the guy that called into C-Span?
No, and that should worry you.
Because you've always been able to count on guys like us.
The reality is now, there's a whole bunch of guys,
look like me, sound like me, real sick of you.
No, we certainly don't believe all of the things
that AOC and the squad believe.
But I believe they believe it.
That's more than I can say for you.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}