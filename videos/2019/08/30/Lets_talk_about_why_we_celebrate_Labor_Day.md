---
title: Let's talk about why we celebrate Labor Day....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=4aYjWs4I7Co) |
| Published | 2019/08/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Labor Day marks the official end of hot dog season and holds different meanings for different cultures.
- Americans associate Labor Day with the fashion rule of not wearing white after.
- Labor Day originated as a celebration of labor, union activity, and socialist ideals by blue-collar workers.
- The first Labor Day parade was on September 5th, 1852 in New York organized by the Central Labor Union.
- In the 1880s, organized labor intensified with events like the Haymarket riot, influencing the emergence of May Day.
- In 1894, the Pullman Sleeping Car Company layoffs triggered a strike due to wage cuts without rent or price reductions in the company town.
- President Grover Cleveland, appearing pro-labor, enacted Labor Day legislation before sending federal troops to crush the strike violently.
- Labor Day was designed as a distraction from the strike's violent suppression.
- Labor Day is historically linked to both celebrating labor and suppressing labor movements.
- Beau warns of the dangers during Labor Day weekend, urging caution and awareness of its risks.

### Quotes

- "This is a day to celebrate lefty ideas."
- "Labor Day is historically linked to both celebrating labor and suppressing labor movements."

### Oneliner

Labor Day carries dual meanings of celebrating labor and suppressing labor movements, rooted in historical events like the Haymarket riot and violent strike suppression.

### Audience

History enthusiasts

### On-the-ground actions from transcript

- Celebrate labor and socialist ideals within your community (implied)
- Educate others on the origins of Labor Day and its historical significance (implied)
- Support local labor movements and initiatives (implied)

### Whats missing in summary

The full transcript provides a detailed historical context and insight into the origins and complex nature of Labor Day celebrations.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're gonna talk about Labor Day.
What is it, other than the official end of hot dog season?
Really, that's a thing, and Labor Day is really the end of it.
To Europeans and pretty much everybody else in the world, it's May Day.
This is when we celebrate May Day.
To Americans, it's the day we don't wear white after.
There's three theories about that rule.
One is that the upper class, well, the old money
didn't like the new money, so they came up
with crazy fashion rules so they could
spot the new money coming.
This was one of them.
The other is that it bookends the seasons.
So you don't need summery white clothing after Labor Day.
The third is that fashion magazines kind of came up
with the rule because this is when they start running
ads for fall clothing.
What is it really?
How did it really start?
depends on whether or not you believe in the book of Peter or Matthew Maguire. Two guys,
both proposed the same thing around the same time. Both were union guys. And it was a celebration
of labor, of union activity, of blue collar workers, of socialist ideals. The first Labor
day was September 5th, 1852 in New York, and it was a parade put on by the Central Labor
Union.
And it carried on like that for a number of years.
And then in the 1880s, well, organized labor started getting really organized.
That's when you had the Haymarket riot, and that's kind of where May Day comes from.
And in 1894, the Pullman Sleeping Car Company, well, they laid a bunch of people off.
And those people that got to keep their jobs, well, their wages were cut by up to a third.
But they didn't lower the rent or the prices in the company town.
This caused a strike.
Grover Cleveland was president.
He liked to be seen as a friend to labor.
So he signed legislation enacting Labor Day just before he sent federal troops to this
strike to crush it, where they wound up opening fire on the strike and killing 30 about, wounding
a whole bunch of others.
We have Labor Day because it was supposed to be a cover.
People were supposed to be talking about this rather than him crushing the strike.
He didn't know the outcome of the strike and how it was going to be put down quite as violently
as it was.
That's where Labor Day comes from.
You guys be careful this weekend.
It is one of the most dangerous weekends in the year.
I think second only to Memorial Day.
So be careful.
Enjoy your hot dogs.
And remember, this is a day to celebrate lefty ideas.
Really.
Anyway, it's just a thought.
Y'all have a good day.
Okay.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}