---
title: Let's talk about Martin County High School again....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=LfrL7y63YiU) |
| Published | 2019/08/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Students at Martin County High School in Kentucky wore pro-LGBTQ shirts to support a fellow student and were made to change by the administration.
- The administration expressed concern about bullying, but the students had already decided to wear the shirts again as a form of solidarity.
- Despite facing bullying every day, the administration fails to take action to address the issue.
- A hit list circulated at the school, causing students to stay home, only to find the administrator and police officer joking about it upon their return.
- The lack of seriousness and support from the administration is appalling to Beau, especially considering the safety and security of the students.
- Beau questions the administration's ability to provide a safe environment for the students and offers the assistance of friends from Fort Campbell or Wright-Patt to step in.
- The superintendent claimed a miscommunication had been resolved, but students were left in the dark without any communication from them.
- Students are labeled as attention-seeking by faculty when they are actually seeking support and tolerance.
- The students, despite facing cynicism and opposition, express their desire for acceptance and tolerance within the school.
- Beau encourages the students to continue fighting for what they believe is right and assures them of his support and that of others watching.

### Quotes

- "We just want tolerance because we tolerate everyone around us and we deserve that too."
- "Students are asking for your help, too."
- "You don't need help, y'all need backup, that's it, and you've got it."

### Oneliner

Students at Martin County High School face bullying daily, seeking support and tolerance from an administration that fails to take action.

### Audience

Students, LGBTQ community, supporters

### On-the-ground actions from transcript

- Support the students at Martin County High School by wearing pride flags or showing solidarity in any way possible (implied)
- Encourage tolerance and acceptance within your own community and schools (implied)

### Whats missing in summary

The full transcript provides a detailed look at the challenges faced by students at Martin County High School and the lack of support from the administration, showcasing the importance of standing up for what is right and supporting those facing discrimination.

### Tags

#Students #Bullying #LGBTQ #Support #Tolerance


## Transcript
Well, howdy there, Internet people, it's Bo again.
So today we're gonna talk about
Martin County High School in Kentucky.
Recap, some students wore some pro-LGBTQ shirts
to support a fellow student at the school.
The administration made them change
and told them they were worried about them getting bullied.
When I first heard about it, I'm like,
well, they're probably already getting bullied.
That's why they're wearing the shirts, to kinda own it.
Kinda say, fine, nobody else is gonna help us.
we're going to stick together and do it ourselves.
Turns out, yep, that's it.
The administration does not even know what's going on in their own school.
Or maybe they do.
Those students, because of some other things I'm about to discuss,
they had already made the decision to wear the shirts again today.
So as I film this, they're getting dressed,
putting those shirts back on to go back to that school.
because they are getting bullied, quote, every single day.
And the administration does nothing.
On Tuesday, there was a hit list floating around the school.
And students stayed home.
Students stayed home from school to come back
to find the administrator and the police officer joking
about it.
It's what they're telling me.
That's pretty appalling, isn't it?
That is pretty appalling.
In this day and age, to be joking about an incident
like that, that's awful.
That is awful.
Shows you don't really take it seriously.
Now, if you are unable or unwilling to provide for the
safety and security of these students, I am certain that
I've got some friends at Fort Campbell or Wright-Patt who
would be happy to come down and do your job for you.
I do not understand how you have a school."
The superintendent said that the miscommunication had been resolved.
Students have no idea what that means because the superintendent has never talked to them
according to them.
The faculty is telling them that they're just seeking attention.
Maybe there's an element of truth to that.
They're getting bullied every single day.
They're calling out for help and you're letting them down.
You're doing nothing about it.
Instead you say, well, don't draw attention to yourself and the bully won't pick on you.
Seriously?
Their parents are supportive though.
They do have that going for them.
But the amount of cynicism that I witnessed is just appalling.
At 15, 16, 17 years old, you're supposed to be idealistic.
You can change the world.
You can do anything.
What is the attitude a school is supposed to foster instead?
We just want people to know that this isn't attention seeking and we are asking you to
support us or wear pride flags because we know where we come from and they most likely
won't change.
We just want tolerance because we tolerate everyone around us and we deserve that too.
And we want Ms. Williams to realize that she is wrong and she is hurting her students by
trying to hide us away simply because she does not agree with us.
We want the school to be accepting and welcoming, not a place of bias.
Most likely won't change.
That jaded, that young.
Students are wrong about that.
going to change and they're going to change it. Now to the administration, this incident has
caught the attention of the LGBTQ community. It has also caught the attention of a bunch of rednecks
who really support the Bill of Rights. I would strongly suggest that the county attorney take a
look at Tinker versus Des Moines and really kind of weigh it because with the reports of the
constant bullying that was overlooked by the administration with the reports that under
normal circumstances when somebody violates the dress code they're just told not to do
it again, they're not drug out and forced to change.
With the response to a hit list targeting them, this certainly seems like a civil rights
case to me.
Certainly seems like a pattern.
But what do I know?
And I'm just a redneck with a lot of time on my hands.
To the students of this school, you guys keep doing what you think is right.
You know, my initial, my initial reason for wanting to contact you was to see if you needed
any guidance.
If you needed anything.
I didn't know about your family being as supportive as they were.
I didn't know about any of this.
Y'all don't need help.
You don't need help, y'all need backup, that's it, and you've got it.
You've got it.
To everybody else who's watching us, these students are asking for your help, too.
Anyway, it's just a fault. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}