---
title: Let's talk about thanking Pia Klemp....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=zf-xXnirnCM) |
| Published | 2019/08/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces the concept of "Thank a Criminal Day" in his video.
- He shares a quote from Pia Klemp, a German ship captain who has saved thousands of migrants in the Mediterranean Sea.
- Pia Klemp is facing accusations in Italy for assisting illegal immigration and working with human traffickers.
- Italy is not the only government opposed to Pia's actions, with Paris offering her a medal which she turned down.
- Pia's response to the medal offer is scathing, criticizing the authorities for their treatment of migrants and asylum seekers.
- She rejects the notion of authorities deciding who is a hero and who is illegal, asserting that everyone is equal.
- Beau expresses gratitude towards Pia Klemp for her actions and her powerful statement.
- He commends Pia's sentiment and describes it as a wonderful statement.
- Beau concludes the video by wishing his viewers a good night.

### Quotes

- "We do not need authorities deciding about who is a hero and who is illegal."
- "Thank you, Pia Klemp."

### Oneliner

Beau introduces "Thank a Criminal Day," shares Pia Klemp's powerful stance on equality, and appreciates her actions.

### Audience

Advocates for equality

### On-the-ground actions from transcript

- Support organizations aiding migrants and asylum seekers (suggested)
- Advocate for fair treatment of migrants and asylum seekers (implied)

### Whats missing in summary

The full video provides a deeper exploration of the concept behind "Thank a Criminal Day" and the importance of challenging unjust laws to bring about more freedom.

### Tags

#ThankACriminalDay #Equality #Activism #Immigration #HumanRights


## Transcript
Well, howdy there internet people, it's Bo again.
And welcome to, I guess, the second installment
of Thank a Criminal Day.
Didn't plan on making this video,
but I ran across a quote today that was said yesterday,
I guess, and it is beautiful.
And it is incredibly fitting.
If you haven't seen the first video,
you're not aware of the idea behind Thank a Criminal Day.
The premise is that criminals obtain
most of your freedom for you. Other people may protect it, but it's obtained by
criminals. People who push the envelope, people who live on the edge of society,
people who refuse to bow to an unjust law and break it. And eventually thought
changes and then the law changes and that brings about more freedom. That's
idea. The quote I found is from a woman that I can truly just truly support. Her
name is Pia Klemp. She's German. She's a ship captain and as part of her work with
SeaWatch International she has saved thousands of migrants who were lost in
the med in the Mediterranean thousands below estimate is 1000 the high
estimate is 14,000 doesn't matter where which numbers true that that's amazing
that is amazing and of course there are some governments that are opposed to her
actions they do not look kindly on what she is doing one of them is Italy where
she is reportedly facing 20 years and they have accused her of assisting
illegal immigration, working with human traffickers, all kinds of things. Paris on
the other hand offered her a medal for her actions and she turned it down. She
She turned it down, and this was her response.
Your police still blankets from people that you force to live on the streets, while you
raid protests and criminalize people that are standing up for rights of migrants and
asylum seekers.
You want to give me a medal for actions that you fight in your own ramparts.
Scathing.
But there's more.
We do not need authorities deciding about who is a hero and who is illegal.
In fact, they are in no position to make this call because we are all equal.
Thank you, Pia Klemp.
Thank you, Pia Klemp.
That is certainly worthy.
That is a wonderful statement, it is a wonderful sentiment, even if it is just a thought.
y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}