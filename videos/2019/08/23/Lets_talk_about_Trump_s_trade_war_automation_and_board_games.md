---
title: Let's talk about Trump's trade war, automation, and board games....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Dz-QFGhjhco) |
| Published | 2019/08/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Chinese tariffs on another $75 billion imposed.
- President orders American companies to find alternative to China.
- Blending corporate and government interests.
- President's exact words: American companies must start looking for alternative to China, including bringing companies home and producing in the US.
- Unlikely for companies to return production to the US; more likely to move to Vietnam or similar countries.
- Bringing production facilities back won't bring back jobs as automation has replaced manual labor.
- Automation is cheaper and more reliable than human labor.
- U.S. losing in trade war against China.
- Move increases chance of recession and income inequality.
- Need for a new system not built on human exploitation.
- Trump's scapegoating in blaming Jay Powell for economic issues.
- Loss of largest trading partner (China) without valid reason.
- Beau points out the need for a more equitable society where everyone can "stay in the game."
- Contrasts Monopoly game dynamics with real life inequality and exploitation.
- Calls for a fair society where everyone starts off equal.

### Quotes

- "Bringing production facilities back won't bring back jobs as automation has replaced manual labor."
- "Need for a new system not built on human exploitation."
- "People get so far ahead that they start mocking those in poverty."
- "Upheaval, the pitchforks come out."
- "Build a society that is more equitable, that is more fair, a society in which everybody can stay in the game."

### Oneliner

Beau criticizes the economic decisions affecting American companies, warns of increased inequality, and calls for a fair society where everyone can participate equally.

### Audience

Economic Activists, Social Justice Advocates

### On-the-ground actions from transcript

- Advocate for fair trade policies (implied)
- Support movements for income equality (implied)
- Engage in community organizing for equitable systems (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the impact of trade decisions on American companies, the role of automation in job loss, and the need for a more equitable society.

### Tags

#TradeWar #Automation #IncomeInequality #EconomicJustice #Equity #CommunityOrganizing


## Transcript
Well, howdy there, internet people, it's Bo again.
So the Chinese hit us with tariffs on another $75 billion
or something like that.
The president's response was to order, order,
American companies to look for an alternative to China.
Order.
Everything inside the state.
The blending of corporate and government interests.
If only there was a term for this form of government.
His exact words, what he said was,
our great American companies are hereby
ordered to immediately start looking
for an alternative to China, including
bringing your companies home, all capital letters,
and making your products in the US.
That's not going to happen.
They're going to go to Vietnam or another similar country.
They're not going to bring them back here.
even production facilities that get moved to the United States, it's not going to bring back the
jobs the way people think. This isn't the 1940s. Factories are not hundreds of people standing in
a line working on on a conveyor belt. That's not what happens anymore. It's automated. Those jobs
are gone. In the United States, those jobs are gone. Automation is cheaper than people and it's
more reliable. This is like having a campaign to bring back switchboard
operators. Technology eliminated those jobs. Yes, they use them over there. Why?
Because over there people are cheaper than automation. It's not true here. It's
not true here. The other thing this border represents is that it signifies
the U.S. loss of this trade war that was going to be so easy to win. Trump isn't
getting what he wants so he's taking his ball or more accurately the balls of
American corporations and going home or to Vietnam. That's what's happening you
know he's sitting there saying we don't need the Chinese. They're our largest
trading partner, they are our largest trading partner.
This move increases the chance of recession, will increase income inequality.
Income inequality and poverty are the two most visible symbols of the fact that we need
a new system here.
We need a new system, one that isn't built on human exploitation.
We're so wrapped up in the idea of winners are losers.
You know, people are running around, well, China's economy is getting more powerful.
If one day it might match ours, the country with four times our population might have
as much economic activity.
color me shocked. Of course it will. That's going to happen and if we shut
them out, well, we're not going to be having a cut of their economic activity.
We are losing our largest trading partner for no reason and the president
sat there and needed a scapegoat so he wondered aloud if Jay Powell was a
bigger enemy than China chairman of the Federal Reserve maybe maybe he is who
nominated him who put him in that position President Trump but his base is
so willfully ignorant they won't know that so he's the perfect scapegoat perfect
scapegoat. You know I am not I'm not a socialist I'm not but I saw that game
socialist monopoly or whatever and it's funny it cracked me up because the
first time I played Monopoly with my boys they loved it. They loved it rolling
the dice, building the houses and the hotels, building their little empires, and when one
of them ran out of money, the other ones voluntarily gave them cash because they wanted to continue
to enjoy it.
And everybody stayed in the game.
stayed in the game, through voluntary means, because people did what was right.
Cooperation over competition.
What happened and what happens when a bunch of young kids normally play Monopoly?
What's the normal outcome?
Somebody starts to get ahead and they start mocking the other players.
And then literal upheaval, somebody gets mad, flips over the board.
It's funny because in that way, Monopoly is a perfect, perfect example of our current
system and the system that has been retried, the system based on the exploitation of humans.
Because that's what happens.
People get so far ahead that they start mocking those in poverty.
And then upheaval, the pitchforks come out.
Happened in France, it happened in Russia, and it will happen here.
If we do not build a society that is more equitable, that is more fair, a society in
in which everybody can stay in the game, you know?
And people like to point to that as, you know,
this is how America should work.
And that's all fine and good, but that's not how it works.
See, at the beginning of that game,
everybody starts off equal.
That's not how it works in real life.
In real life, somebody starts off with a whole lot more.
And they can use that to keep everybody else down.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}