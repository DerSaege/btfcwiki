---
title: Let's talk about a wholesome story for 2019 in Arkansas....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=RFaEIabn4qA) |
| Published | 2019/08/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Four boys, ages 15 and 16 in Arkansas, finished football practice and started fundraising in August 2019.
- The boys, African-American, were selling discount cards door-to-door when a woman confronted them with a gun, mistaking them for suspicious individuals.
- The woman called the cops, describing the boys as suspicious because they were African-American in a white neighborhood.
- Despite one of the cops recognizing the boys as school resource officer, the woman lectured them on "horseplay" instead of apologizing after the misunderstanding.
- Beau draws parallels to the infamous Dred Scott case, pointing out the persistent racist attitudes that lead to incidents like this.
- The woman, Jerry Kelly, 46, was charged with four counts of aggravated assault, false imprisonment, and endangering the welfare of a minor.
- Kelly's connection to law enforcement through her previous employment and her husband's position raised concerns about preferential treatment in the case.
- The incident underscores the ongoing racial prejudices and dangers faced by African-American youth, even in 2019.
- Beau questions the progress made in society when incidents like this continue to occur.
- The boys, engaging in a wholesome activity, faced a traumatic experience due to racial bias and ignorance.

### Quotes

- "Four teenage boys had a revolver shoved in their face because they were fundraising for football in a white neighborhood."
- "Man, that is some Dred Scott stuff right there."
- "How could this happen? But we have to say it every day."
- "Because well, I mean, they don't have any real rights."
- "Anyway, it's just a fault, y'all have a good day."

### Oneliner

Four African-American boys fundraising for football faced racial bias and a gun in a white neighborhood in 2019, leading to charges against the aggressor with ties to law enforcement.

### Audience

Community members

### On-the-ground actions from transcript

- Support community initiatives promoting racial equality and understanding (suggested)
- Advocate for fair treatment and justice for victims of racial discrimination (implied)
- Stand against racial prejudices and biases in your community (implied)

### Whats missing in summary

The full transcript provides a deeper exploration of racial biases and injustices that persist despite societal advancements.

### Tags

#RacialBias #Youth #CommunityPolicing #Justice #Equality #Arkansas


## Transcript
Well, howdy there internet people, it's Bo again. Got a wholesome story for you today
Something great for 2019 comes out of Arkansas happened on August 3rd
Four boys age 15 and 16. They just finished football practice. It's the beginning of the year. You know what that means
You know what you got to go do time to go fundraise
Now my day we sold
Chocolate bars, I guess today is a little different
They're selling some kind of discount card, and they start going door to door, you know.
They're taking turns as they go, because it's embarrassing at 16, they're 15, and just like
any good wholesome story, there's a scene with a dog.
They're walking along, a big black dog comes out, they dive in the back of a pickup truck
because they're terrified of it, owner comes out, it's like, ah, dog's actually friendly,
they get out and play with it and they go up to the house. Then they walk next door
and before they can even get up to the door, a woman comes out with a chrome-plated revolver,
sticks it in their face, tells them to lay on the ground, starts yelling and cussing
at them demanding to see their IDs. She had already called the cops and informed them
that there were suspicious people in the neighborhood. When she did that, she told them that she
saw him going up to a house and that all of the males were African-American and I
know this residence to be white." Quote. So she's laying there. Cops show up a few
minutes later. Luckily one of the cops is a school resource officer who
recognizes the kids. He's like, y'all can go wait by my squad car. And they talk to
her about what happened. And then this weird thing happened. She called them
over, back over, and you would expect this to be the point where she apologizes, no,
she lectures them about horseplay and says that if you're going to be out selling cards,
you need to be men about it, quote.
Man, that is some Dred Scott stuff right there.
If you're not familiar with that case, it's got a very infamous passage in it in this
case, it says that black people are of an inferior order and altogether unfit to associate
with the white race, either in social or political relations, and so far inferior that they had
no rights which the white man was bound to respect.
Very old case, but as we can see, it still rings true.
That thought process is still in people's minds.
Why'd she call the cops?
Four black males approaching a white residence.
It's not like they could be socially associating.
Too inferior for that.
And then after laying them on the ground, pointing a firearm at children, out doing
one of the most wholesome activities in America, she has the nerve to lecture them.
Because well, I mean, they don't have any real rights.
They need that white paternal thing to kick in and tell them how they should behave.
I guess playing with a dog is too much.
We say this all the time, you know, it's 2019.
How could this happen?
But we have to say it every day.
Because yeah, it's 2019, maybe where you are, maybe where I am, but in a lot of places it's
still 1860.
Now, so the follow up, what happened here?
What happened after this?
It's been a few days.
The woman accused of this, and this is all alleged,
has not been convicted, is Jerry Kelly, age 46.
The sheriff did not hold back with the charges.
There were some rumors initially that she was getting preferential treatment
because she was able to come back and take her mug shot at a later date.
I'm judging by the fact that the department charged her with four counts of aggravated
assault, false imprisonment, and endangering the welfare of a minor.
They probably aren't showing much preferential treatment.
Why did people believe she was going to get preferential treatment?
She used to be employed by a law enforcement agency and her husband is a jail administrator.
also fits pretty well with 2019.
So here we are, moving into the new school year,
and this is what we've got to look forward to.
We can talk about how much it has changed,
and it's easy for people that look like me to say that.
But four teenage boys had a revolver shoved in their face because they were fundraising
for football in a white neighborhood.
Anyway, it's just a fault, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}