---
title: Let's talk about a bizarre working theory....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=yZkFtTQjF5A) |
| Published | 2019/08/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing demographic information to identify potential perpetrators before they commit violent acts.
- Majority of mass shooters are male, white, and display concerning behaviors.
- Early childhood trauma, inciting incidents, and a planning phase precede violent acts.
- Characteristics of mass shooters include abusive behavior, mental health concerns, and leaked plans.
- Mass shooters often have personal grievances blended with other grievances.
- They have a desire for control and notoriety, similar to serial killers.
- Mass shooters may be more closely related to serial killers than terrorists.
- Need to respond, not react, to prevent future violent incidents.
- Current strategies focusing on political violence may not be effective.
- Urges a shift in perspective and approach to addressing mass shootings.

### Quotes

- "If somebody tells you they're going to do it, believe them."
- "We need to respond rather than react."
- "Mass shooters are more closely related to serial killers than they are terrorists."

### Oneliner

Analyzing demographic info to prevent violent acts, mass shooters more like serial killers, urge to respond not react.

### Audience

Policy makers, law enforcement, psychologists

### On-the-ground actions from transcript

- Contact local authorities or FBI to report suspicious behavior (implied)
- Advocate for standardized reporting methods for potential threats (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of mass shooters' characteristics, urging a shift in perspective towards prevention rather than reaction.

### Tags

#MassShooters #Prevention #Demographics #ResponseVsReaction #ViolencePrevention


## Transcript
Well, howdy there, internet people, it's Beau again.
So at the end of this, I'm going to present a very, very bizarre working theory.
Watch this whole video.
Even if you don't care about the beginning parts, you're going to need that information.
So we're going to go through the demographic information and we're going to talk about
who these people are and how we can identify them before they do something, hopefully.
And from that, we're going to come up with a working theory.
Something you need to know about this, if you have seen the jokes in the comments section
about Inwalk's weirdo, there was a time in my life when if somebody had a question about
political violence, my phone would ring.
It's my area of expertise that I don't normally bring up my background.
It becomes important tonight though.
Okay.
So we have 50 years of data from the violence project.
And what is consistent is early childhood trauma, inciting incident, no coping skills,
planning phase begins.
Now as we go through the demographic information that I'm about to present, there are going
to be some surprises.
There certainly were for me.
Stick with me.
I promise it's not going where you think.
Okay.
They're almost all male, almost.
by almost, I mean like really almost, like 95% and up, almost all male.
Recently, they've almost all been white, but that's not true historically.
From 2000 to 2013, the demographics of mass shooters, as far as race,
almost perfectly matched the demographics of the United States.
Surprise, I was.
57% are single.
22% are divorced or separated, 35% have an adult conviction of some sort, 62% displayed
abusive, harassing or oppressive behavior, 27% did something that could be considered
domestic violence or stalking.
77% engaged in a planning phase that lasted longer than a week.
Some of them lasted years.
More than 50% leaked their plans.
They told somebody, they dropped hints or clues.
If somebody tells you they're going to do it, believe them.
More than 50% killed a family member or intimate partner during their spree.
62% had mental health concerns.
There's a footnote to this one.
What that means is the FBI believed they were displaying symptoms of depression or anxiety.
Only 25% had a diagnosed mental illness of any kind.
They studied other shooters.
They didn't have an inciting incident.
They had three to four stressors at once.
That's an important variation.
They prepared a kit.
They had tools that they were going to bring on their spree.
They blended their grievances.
They had personal grievances, they had government grievances, they had workplace grievances,
whatever.
They blended it all together.
That's the demographic information.
I took this, I parsed it down by what was most common, and this is the list I came up
with.
Early childhood trauma, inciting incidents, plural, multiple stressors, multiple grievances.
They studied others.
The planning phase in which they dropped hints or clues.
They had a kit, and when we talk about the kit, you have to understand that it's bizarre.
The shooter in Dallas that didn't get anywhere, he was an ex-infantryman, and there's that
photo of him coming around the corner.
If you hand that to anybody in the military, they would not think that guy was in the military.
Just going off of the shoelaces alone, they weren't prepared for combat.
The kits aren't for combat, they're for something else.
Some quotes that I ran across that are very relevant and they come from an FBI agent who
is in charge of studying pre-attack behavior.
They have the desire for omnipotent control, that desire for infamy and notoriety.
OK, cutting to the chase, that's not political violence.
That's how we're framing it right now,
because in the last couple of years,
they've all fallen into that niche.
This is not characteristic of people
engaged in political violence in any way.
At this point, I realize I'm outside of my scope.
So I called a fellow former weirdo.
Yeah, you don't want to be my friend.
I will call you in the middle of the night
the most bizarre questions. And when I got him on the phone I started reading
him the list. Early childhood trauma, inciting incidents, blended their
grievances against persons is what I said. Slight variation there. Studied
others who did the same type of thing. Spent a lot of time planning and would
drop hints or clues and they had a prepared kit. At this point he stops me
And he says, yeah, I know I know what they do. What do you want? It's it's late
He's not an expert on political violence. He's an expert on serial killers
Desire for Omnipotent control if only for a moment seeking notoriety
variety. Now, that's unique, but there's something else. In 1989, which was the peak
of the serial killer thing, there were 193 known serial killers operating in the United
States. In this decade, the average has been about 40. Huge decline. At the same time,
We're seeing a huge increase in mass shooters.
This explains a lot, a whole lot.
It explains the inconsistencies with the manifestos.
People who are really engaged in political violence, they're steeped in theory.
They know what they're talking about, especially anybody that's going to do something this
extreme.
you look at the manifestos coming from these guys? Not so much. In fact, the one
from El Paso was just rephrased Trump tweets really. But we've been looking at
it through that scope of political violence because for the last two years
or so that's what it's been. But they blend their grievances. They have somehow
found a way to match their personal grievances with this ideology.
looking at this all wrong. We're looking at this as political violence and if we
deploy counter-terrorism tactics and counter-terrorism strategies against
this it's going to fail. We don't need to be talking to the counter-terrorism task
force. We need to be talking to the people who used to profile serial
killers. That's who we need to be talking to right now because I'm going to
suggest that mass shooters are more closely related to serial killers than
they are terrorists.
We need to respond rather than react.
And that's what happens in these cases, you know, if there was a school shooting tomorrow,
we know very well that on Fox News, you'd have a pundit saying, we need to arm all teachers
and janitors and everybody gets a gun.
And on CNN, they'd be saying we need gun control, because it's a reaction.
over and over again, we trot out the same ideas. Nobody's responding. Nobody's looking
at all of the new data that's coming in and analyzing it. I looked online to see if anybody
else had come up with this little theory. And yeah, the problem is they're outliers.
They're people outside of the norm and it's almost treated as a joke. It's not. It's not
a joke. This makes more sense than anything else. We need to change the way we're looking
at this. We can't treat them as political violence perpetrators, even though that's
part of their motive, their stated motive, their personal reasons for doing it and their
behavior say otherwise. Now I don't have any actionable idea on that other than
it needs a lot more research and we may need to bring in other kinds of people
to look at this and talk about it. Now as far as the characteristics we know what
they are now okay hopefully you can spot this. The problem is I went to look up
the way to report this, so I can tell you guys, it's different everywhere. In some places it varies
by county. There is no method of doing this that is standardized. In fact, the FBI has a tip line
for it, but it's their standard tip line. Those people that are just craving legislation, start
there. That needs to be fixed immediately. Anyway, as we're doing this, we need to remember, we need
need to respond not react it's weird right out walks weirdo anyway it's just
far. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}