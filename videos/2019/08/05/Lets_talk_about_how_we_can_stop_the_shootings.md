---
title: Let's talk about how we can stop the shootings....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Ht5ldqQdioM) |
| Published | 2019/08/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing solutions to help stop the plague of violence sweeping the nation.
- Noting the cultural roots of the issue rather than blaming specific factors like firearms or pharmaceuticals.
- Sharing findings from a study by the Violence Project funded by the National Institute of Justice.
- Identifying four common factors among mass shooters that are almost universally present.
- Emphasizing early childhood trauma and its long-lasting impacts.
- Explaining the importance of identifying crisis points to intervene before shootings occur.
- Describing how shooters often study other shooters, contributing to a socially transmitted disease.
- Pointing out the prevalence of obtaining firearms from family members in mass shooting incidents.
- Advocating for social reform to address underlying issues like coping skills and trauma.
- Urging responsible gun ownership and securing firearms to prevent access by potential shooters.

### Quotes

- "Early childhood trauma. This is bullying, parental suicide, child abuse, exposure to violence at an early age."
- "There's an identifiable moment where the decision was made. That was the point of no return."
- "We need to let kids be kids, but that's going to require pretty wide-ranging social reform."
- "Secure your firearms and don't tell your favorite nephew the code."
- "Don't name them. Make them a non-person."

### Oneliner

Beau addresses the cultural roots of violence, advocating for social reform, coping skills education, responsible gun ownership, and de-glorifying shooters to stem the tide.

### Audience
Community members

### On-the-ground actions from transcript

- Secure your firearms to prevent unauthorized access (implied)
- Educate yourself and others on identifying crisis points in individuals in crisis situations (implied)
- Advocate for social reform to address early childhood trauma and improve coping skills education (implied)

### Whats missing in summary
In-depth analysis of the study by the Violence Project and the implications for addressing mass shootings comprehensively.

### Tags
#ViolencePrevention #SocialReform #CopingSkills #ResponsibleGunOwnership #CommunitySafety


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we're gonna talk about solutions.
We're gonna talk about things we can actually do
to help stem the tide and help stop the plague
that is sweeping the nation.
When we wake up every morning and we see the toll,
we feel hopeless, we feel helpless
because there's nothing we can do about it.
And everybody's looking for some magic pill.
You know, some people want to
point directly to the firearm itself. This is what's to blame.
Some people want to point to pharmaceuticals
because there's there's a trend there, too.
Some people want to point to a whole bunch of different things.
With that logic,
we're never going to get there because this is a cultural issue. It's something
I've said for a long time.
and it's pretty widespread. Pharmaceuticals, for example. Yeah, there's a trend there.
There is.
The thing is, that's also just a symptom.
People don't wake up, you know, get born
with a prescription.
There's a reason
that they were taking them.
So, I had my own theories and I've talked about them in videos.
And the thing is,
I would never present them as fact, I would present them as my opinion, because they were
based on my observations from me looking at cases that interested me, I never did it in
a scientific manner, so I never had any hard data to back it up, until today.
The LA Times ran an article about the violence project, it's funded by the National Institute
of Justice.
It's been going on two years, it's a study, and it's a little different than other studies.
These guys didn't set out with the idea of, okay, well, we want to see if 30-round magazines
are to blame.
We want to see if psychotropic drugs are to blame, it's not how they did it.
They went back and they cataloged the life histories of the shooters, and they did this
for every mass shooter going back to 1966 and every workplace religious institution
or school shooter going back to 1999, whether or not it was a mass incident or not.
They identified four things that are almost universally present.
That's where we have to start.
So what are the four things?
First one, it's one we all know.
We call it bullying, but it's more than that.
Early childhood trauma.
This is bullying, this is parental suicide, this is child abuse, sexual abuse, this is
exposure to violence at an early age, all of these things.
Early childhood trauma.
The next is that there's an identifiable, I think they call it a crisis point rather
than a flash point.
There's an identifiable moment where in the weeks leading up to the shooting, the months
leading up to the shooting, something happened and that's when the decision was made.
That was the point of no return.
There was time after that incident and before the shooting where those people around the
shooter maybe could have done something.
Maybe they knew.
Maybe they knew something was off, but because they don't know how to reach out to a professional
or they don't know how to really identify it and didn't really know what they were
looking at it, nothing happened.
The third thing is that they all studied other shooters.
They looked at other shooters because this is a socially transmitted disease, which means
in the days of social media, it can reach epidemic proportions.
People that lose hope, people that reach this conclusion, they can find notoriety after
their death.
The fourth thing is kind of a no-brainer.
This was the one that I didn't really think about but did and actually said in videos
but never realized how prevalent it was means,
yeah, if you're going to be an active shooter,
you need a firearm, right?
In the cases that I talked about,
I pointed out that school shooters don't go and buy the gun.
They get it from a family member's gun cabinet.
Turns out that's not just true in the cases
that I talked about.
That's true in 80% of them.
80%.
And then what they found out was that a workplace shooting
was normally a handgun that was legally owned.
And that public space shootings were normally illegal purchases.
So what does this tell you when you really
sit down and think about it. It's not going to be the answer. Just like the first one.
The talking points that we're looking at as a country, they're all wrong. All of them.
That's what this study is telling us. Boldly, they're all wrong. So what can we actually
Early childhood trauma, we can't do anything about that, really can't, especially to stem
attacks that are going to happen tomorrow, because that happened 20 years ago.
There's nothing we can do about it.
In the future, we need to let kids be kids, but that's going to require pretty wide-ranging
social reform.
crisis point. Yeah, we need to learn how to identify people in crisis. We need to
learn how to identify that. We need to learn what we're looking for and we need
to learn what to do if it comes up because the first instinct may be to
call the cops and hopefully some psychologists can chime in on this but
But that may not be the best answer from where I'm sitting, because if they've already purchased
the weapon and the cops show up, it may not give you the outcome you're looking for.
But there is something we can do as far as this one.
Most of the incidents that triggered it, they were minor, it's a lack of coping skills.
It is a lack of coping skills.
If the shooter themselves had access to an inventory of methods to cope with whatever
that inciting incident was, well they wouldn't be shooting up places.
So we need to start teaching that.
And we need to start, as unpopular as this statement is going to be, we need to start
with white males age 16 to 35. Why? Because that's mainly who the shooters are. Expand
out from there, but that's the priority. There is one thing we can do with means. You know,
legislation with the, these stats tell us is the legislation about who's purchasing
the weapon isn't going to do any good. It's really not. But there's something we can do
about school shootings. Lock up your weapons. Secure your firearms. And this is on the gun
crowd. You gotta do it. Can't just say you're going to do it. You have to do it. Secure
your firearms and don't tell your favorite nephew the code.
That would have stopped 80% of school shootings.
They didn't purchase the weapon, it wasn't theirs, they got it from somebody else.
Secure your firearms.
You may have noticed in the past that I rarely name the shooter, because of that third reason.
They studied other shooters and what they found out was that they can get notoriety
after death.
Don't name them, don't name them.
It leads to some pretty depressing comments when I do it because people have to ask what
shooting are you talking about, use the city location or the names of a victim.
When you talk about it, don't use their name, anything else but their name.
As Orwellian as it sounds, make them a non-person.
That's what we can do.
That's how we can stem this tide.
your firearms, start teaching people coping skills, and stop using their name.
Stop glorifying them.
Even though it's not glorification the way you think of it, they become notable.
They make it onto a Wikipedia page.
Deny them that.
Now, since I have you here, there is one thing I want to point out about the El Paso shooting.
Seeing a lot of news outlets say that, well, he went there to kill illegals.
No.
No, he didn't.
He did not.
I've read just about every report.
Not a single one of them says that he checked visas.
He went there to kill brown people.
It was racism.
Don't make him sound like a vigilante.
It was just pure racism.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}