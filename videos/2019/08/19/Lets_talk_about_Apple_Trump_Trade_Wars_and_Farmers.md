---
title: Let's talk about Apple, Trump, Trade Wars, and Farmers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ufjjjqwJnQs) |
| Published | 2019/08/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Tim Cook exposed President Trump's trade war during a dinner meeting at the president's golf club.
- Apple pays tariffs on Chinese components, making it hard to compete with companies not involved in the trade war like Samsung.
- President Trump found Cook's argument compelling and is reconsidering the trade war.
- Beau questions why American farmers didn't receive similar consideration during the trade war.
- The Secretary of agriculture dismissed farmers' concerns as whining.
- President Trump's administration favors the wealthy, with policies benefiting those with high net worth.
- The administration's policies contradict the rhetoric aimed at common people.
- Beau criticizes Trump for prioritizing personal gain over the well-being of Americans.
- Trump's presidency is characterized by self-interest and exploitation of taxpayers.
- Beau concludes by asserting that Trump never cared about the American people.

### Quotes

- "He doesn't care about you, he never did, and he never will."
- "It was just stuff he said to get people to follow him, that's it, that's all it was."
- "That's President Trump's administration."

### Oneliner

Tim Cook exposes Trump's trade war, revealing the administration's favoritism towards the wealthy over American farmers.

### Audience

American voters

### On-the-ground actions from transcript

- Support policies that prioritize the well-being of all Americans, not just the wealthy (implied).

### Whats missing in summary

The emotional impact of Trump's policies on everyday Americans and the call to prioritize their interests.

### Tags

#TradeWar #TimCook #PresidentTrump #AmericanFarmers #PolicyPriorities


## Transcript
Well, howdy there, internet people, it's Bo again.
So Tim Apple, Tim Cook to everybody else,
the CEO of Apple, just blew President Trump's trade war
wide open, completely exposed it,
exposed the rhetoric for what it is in one dinner.
Friday night, Tim Cook went and sat down with the president
at the president's golf club and explained to him
that it was really hard for Apple to compete with companies like Samsung who
aren't involved in the trade war because Apple has to pay the tariffs on the
components it's importing from China.
Yes, the American company pays the tariffs, not the Chinese.
And that makes it hard to compete.
It's not fair.
And President Trump said that he made a compelling argument.
And so he's thinking about it.
Where was that consideration for the American farmer?
Where was that consideration for the American farmer in Iowa, the soy farmer, who was having
to compete with Brazilian farmers who aren't involved in the trade war?
I guess that consideration is only given to people who are worth $100 million or more.
Somebody who can go to the President's golf club and sit down, have a dinner.
Because when the American farmer said the same thing and had the same problem and lost
their family farm, which in most cases was also their family home, well, the Secretary
of agriculture said that they were whiners, it's just the cost to make an America great, right?
This is a crystal clear example of President Trump's entire administration.
The rhetoric?
Oh, that's for us commoners.
The policies?
That's for people whose net worth has nine figures after it.
dinner, and President Trump is reconsidering.
Thousands of farmers in trouble, well that doesn't matter, because President Trump was
never a representative of the American worker.
That was just rhetoric.
It was just stuff he said to get people to follow him, that's it, that's all it was.
He's not there to make America great.
He's there to feather his own nest, to collect as many favors from other rich guys as he
can, to deal them out so when he gets out of office, those favors can be repaid, and
to milk the American taxpayer out every dime he can by constantly going to his own resorts.
That's President Trump's administration.
He doesn't care about you, he never did, and he never will.
anyway. It's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}