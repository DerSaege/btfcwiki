---
title: Let's talk about the future of Afghanistan....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=RFU6jw9iyCU) |
| Published | 2019/08/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Four main groups operating in Afghanistan: U.S., Afghan government, Taliban, and I.S.
- Taliban trying to reestablish itself as U.S. leaves, leading to dynamics changing.
- U.S. selling out the Afghan government by talking directly to the Taliban without them.
- Negotiators have no leverage with Taliban because they know the U.S. is leaving.
- Taliban likely to reassert control over Afghanistan; U.S. probably needs to give them supplies to follow preferred route.
- Staying in Afghanistan prolongs violence and doesn't help stabilize the country.
- Violence will continue after the U.S. leaves, and Taliban might reassert control.
- U.S. mindset of military solutions won't clean up the mess in Afghanistan.
- Insurgents have historically won despite facing technologically advanced military forces.
- Giving jets to the Afghan government is risky as Taliban or IS might end up controlling them.
- Helicopters could be valuable aid as neither Taliban nor IS have significant air capabilities.
- Need to learn from the mistakes in Afghanistan and plan military operations with the aftermath in mind.

### Quotes

- "We are leaving, and we are leaving behind a mess."
- "The time to figure that out is over. It's going to happen."
- "Giving jets to the Afghan government is risky."
- "Helicopters could help change the course of the war."
- "This region of the world, this is where empires go to die."

### Oneliner

The U.S. is leaving Afghanistan, leaving behind a mess and facing the reality of Taliban resurgence, with no easy solutions in sight.

### Audience

Policy Makers, Activists

### On-the-ground actions from transcript

- Provide humanitarian aid to Afghan civilians (implied)
- Advocate for diplomatic solutions to stabilize Afghanistan (implied)

### Whats missing in summary

Insights on the historical context and implications of U.S. military involvement in Afghanistan.

### Tags

#Afghanistan #MilitaryWithdrawal #Taliban #USPolicy #HumanitarianAid


## Transcript
Well, howdy there internet people, it's Bo again.
So today we're going to talk about Afghanistan because I got a question.
Bo can we get an in walks weirdo breakdown of what's happening in Afghanistan?
Why is it happening now?
Can we just give them jets and leave?
Okay, so what is happening in Afghanistan?
Over the weekend, one of the four main groups that is operating in the country tried to
reestablish itself and show that it is still viable prior to the dynamics changing and
the dynamics are about to change because the U.S. is leaving.
There are four groups.
You have the U.S., you have the Afghan government, you have the Taliban, and you have I.S.
I.S. is the weaker of the groups.
So before the U.S. left, they had to show that they were still viable.
They had to defeat the U.S. by conducting these attacks.
That's what it was about.
All of those people died to show that they were still relevant.
What's happening behind the scenes?
The U.S. is talking directly to the Taliban without the Afghani government.
Why?
Because we're selling the Afghani government out.
That's what's happening.
We can frame it any way we want.
We can try to church it up.
The reality is we're selling them out.
We're hedging our bets because the reality is we're leaving.
We are leaving.
The Afghani government could not subdue the Taliban with our help.
They're probably not going to be able to do it without it, which means the Taliban is
probably going to reassert control over Afghanistan.
That's what's going on.
Now the problem is that although President Trump is right, not a joke, we do need to
leave, he's publicly said repeatedly that he wants to leave.
That kind of handicapped the negotiators, the people that are sitting down and talking
with the Taliban right now, they have no leverage, they've got nothing.
The Taliban knows that we're leaving.
They have no incentive to work with us.
So we're going to have to give them something if we want them to follow our preferred route.
And the preferred route is probably that the Taliban work with the Afghani national government
to take out IS before they start shooting at each other.
So to get that, we're probably going to have to give them supplies.
I don't know that that's happening, but that would be my guess, because otherwise they
have no reason to do it.
And that is why the Afghani government is not privy to the conversations, because they
would be really upset at the idea of us giving supplies to the Taliban, who are eventually
going to fight the Afghani government.
Now, there are going to be a lot of people who look at this weekend's events and they're
going to say, well, we need to stay.
We need to help stabilize this country.
We messed it up.
We've got to fix it.
We're not a stabilizing force.
We're not, we've been running counterinsurgency operations in the country poorly.
And that has nothing to do with the guys on the ground.
It has to do with the strategies used.
They were bad.
The airstrikes created a lot of collateral damage, they killed a lot of innocent people.
Those innocent people had families.
They of course felt animosity towards the United States and by extension the Afghan
government because it is rightly seen as a US puppet.
So who do they flock to?
The Taliban or IS.
A staying hurts, doesn't help, as far as the life of the average Afghani.
A staying just prolongs the violence.
There is going to be violence after we leave.
There's nothing that we can do to stop that now.
We should have thought of that, thought of an exit strategy, prior to going into the
country more than a decade ago.
The time to figure that out is over.
It's going to happen.
It's going to happen.
more than likely the Taliban will re-assert control.
There's no peace with honor in this one.
Well, we're leaving, and we are leaving behind a mess.
But we can't clean the mess up.
Doesn't matter how much we want to, because we are locked into the mindset
of doing it militarily.
That's not going to change.
You know, we have this idea that we have to defeat them by force.
It's not true.
In fact, it's not even the most effective route.
The insurgents almost always win.
The guys with the rifles that are 50 years old have brought the most technologically
advanced military to the table, and they beat them.
I mean, there's no other way to say that.
They won.
We are withdrawing.
And we are withdrawing with a national government that is in peril and a strengthening Taliban.
They won.
Now as far as giving them jets, and I'm assuming you're talking about the national government,
we can't because the Taliban is probably going to win.
So who would control those jets afterward?
the Taliban, or worse, the IS.
If there was any aid we could give them that would matter, it would be helicopters.
They don't even have to be good helicopters because neither of the other groups have any
air to speak of.
The helicopters could help change the course of that war, the war that will come after
we leave.
This incident and as we watch what unfolds in Afghanistan after we leave, we need to
let this kind of burn into our memory because no matter what happens, we have to maintain
a clear picture when we conduct a military operation and a big part of that picture has
to be what happens when it's over. When is it over? How do we get out? This region of
the world, this is where empires go to die. And we walked in there like it was the okay
corral. It didn't work out. It didn't work out. Objectively, it would be better for most
Afghan people if the national government stayed in place, but it's probably not
going to. And there's nothing that we can do realistically to change that.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}