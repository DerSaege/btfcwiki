---
title: Let's talk about Trump, Shell, and Representative Democracy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=bkFyFxKpIhk) |
| Published | 2019/08/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Critiques the blending of corporate and state interests in the Shell Trump photo-op.
- Explains how Shell contractors faced unfair choices during the event.
- Mentions leaked memos revealing instructions for workers to appear as props.
- Describes how corporations repay politicians by using workers as props.
- Points out the false image created by such propaganda.
- Analyzes how politicians manipulate public perception through divisive tactics.
- Questions the dominance of certain professions in running the country.
- Calls for more representation of working-class individuals in positions of power.
- Advocates for unity among the working class for better governance.
- Suggests the need for genuine representative democracy.

### Quotes

- "You're not actually going to be doing any work you don't need your safety gear except for that shirt so it looks like you're working class because you're a prop nothing more."
- "What we really need is a lot more bartenders, carpenters, plumbers, people like that in the House of Representatives, people that actually represent us."
- "We need a little bit more working class unity."

### Oneliner

Beau criticizes the Shell Trump photo-op, exposes unfair treatment of workers, and calls for more working-class representation in government for true democracy.

### Audience

Working-class Americans

### On-the-ground actions from transcript

- Advocate for more working-class representation in government (implied)
- Foster unity and communication among the working class (implied)

### Whats missing in summary

Full context and detailed analysis of the Shell Trump photo-op and its implications.

### Tags

#CorporateInterests #WorkingClass #GovernmentRepresentation #Propaganda #Unity


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we're gonna talk a little bit
about that Shell Trump photo-op.
Perfect example of the blending
of corporate and state interests.
The blending of corporate and state interests.
That's a key element in a certain ideology
we like to pretend isn't creeping into the United States
at an ever quickening pace, but it is.
everything inside the state.
If you don't know what happened, some shell contractors,
they told their employees they had three choices.
They could, A, take paid time off, B, don't show up
and don't get paid, but keep in mind,
they work on prefigured schedules,
and those schedules include 56 hours.
So that last day, that's overtime for these people.
We're talking about $700 that they could end up losing for some of them.
And then the third option was to show up, get paid, to be a prop in a presidential campaign
speech that you as a taxpayer are paying for.
Those were the options.
Now how do we know that it wasn't really Trump showing up and a whole bunch of working class
guys gathering around and listening to the president. Well in this case we got
the leaked memo and it had everything in it about how they'd be bussed in and all
of that stuff but it also included some other interesting tidbits that I haven't
really seen mentioned. Told them not to wear still toes, don't wear your work
boots, but wear that high visibility clothing. You're not actually going to be
doing any work you don't need your safety gear except for that shirt so it
looks like you're working class because you're a prop nothing more this is how
corporations large companies repay their debt to the politicians the ones that
cut them deals and allow them to extract minerals and natural resources from
taxpayer-owned land. This is how they do it. It's a trade. And this kind of
propaganda creates the image that the government is working for the people, the
working class, and that the working class supports the government. Neither of these
is true. You know, if you ask people what they think about the government,
incompetent, inept, inefficient, a whole bunch of stuff. But then if you say, well
what about this politician? Oh well that one's going to save the world, because they've got us
trained. Red versus blue, left versus right, R versus D, and that's how they get away with it.
That's how they feather their own nest at the expense
of the working class. When you really break it down, who runs this country? What professions?
lawyers, when you're talking about the entertainment side of things, the
cultural side of things, producers, judges, bankers, lobbyists, and religious
figures. That's who runs the country. Overall, how do you feel about those
professions? Most of them are pretty scummy, right? I mean, let's be honest. Even
And some of the ones that you don't think of as scummy, well, they are.
You know, people seem to have a lot of respect for judges.
What is a judge?
A lawyer turned politician, that's what a judge is.
So the idea that we should let this class of people, these professions, run our country,
these are the people that are going to lead us, doesn't make a whole lot of sense.
These are the people who can divide us.
These are the people who can play us against each other.
What we really need is a lot more bartenders,
carpenters, plumbers, people like that
in the House of Representatives, people that actually represent us.
We need a little bit more working class unity.
That's what this country needs.
If we get that and we start talking to each other,
left and right as it is in the United States, you're going to realize you don't have much
to separate you, other than talking points.
We can sit here and argue over what's best for the interests of other people, or we can
start to seek that representative democracy we were promised.
Anyway, it's just a thought.
Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}