---
title: 'Let''s talk about Episode 1: Education'
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=s-DdK7BqWa8) |
| Published | 2019/08/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Teachers asking for help to clear their lists for supplies.
- The President's idea of nuking a hurricane won't work due to heat energy.
- The root cause of underfunded schools lies with those in power, not the uneducated.
- Policy makers prioritize creating worker bees over critical thinkers.
- Society values job skills over a comprehensive education.
- Increasing funding for schools may improve outcomes, but it's more about valuing education.
- Education is an ongoing, daily benefit, not just about graduation.
- The impact of a great teacher surpasses that of a great school.
- Education should focus on instilling a love for learning, not just obtaining a diploma.
- Stereotypes discourage men and women from valuing education.
- Real tough guys, like military leaders, prioritize education.
- Education enriches lives and should be valued by all.

### Quotes

- "No teacher should have to appeal to the public for tools they need when they should be provided."
- "Education is not its own reward, we focus on graduation, not the value of learning itself."
- "No education is wasted, never stop learning, always stay curious."

### Oneliner

Beau dives into the interconnected issues of underfunded schools, the value of education, and the stereotypes that discourage learning.

### Audience

Educators, policymakers, community members.

### On-the-ground actions from transcript

- Advocate for comprehensive education that includes arts, music, and humanities (implied).
- Support educational causes like Pencils of Promise that build schools in underserved communities (implied).
- Challenge stereotypes and encourage a love for learning in both men and women (implied).

### Whats missing in summary

The full transcript covers a wide range of historical events and personal anecdotes that underscore the importance of education and continuous learning.

### Tags

#Education #SchoolFunding #TeacherSupport #CommunityBuilding #Stereotypes #ContinuousLearning


## Transcript
Well howdy there internet people, it's Bo again.
Two things kept running across my news feed yesterday
and I can't help but think that they're related.
The first
was
teachers
asking people to help them clear the lists.
Teachers appealing to the public
for the supplies they need to do their job.
That's sad.
That is sad.
No teacher should have to do that.
The other was the President of the United States
wanting to nuke a hurricane.
Wow.
OK, I'm going to pause for a second.
That won't work.
Just go ahead and clear that up now.
That will not work.
The heat energy released by a hurricane
is a whole lot more than a nuke, a whole lot more
in several nukes per minute. If you nuke a hurricane you get a radioactive
hurricane. Anyway, I think those two things are related and I know what
you're saying. Trump didn't attend a woefully underfunded school. He's just
like that. Now, Trump isn't a product of those woefully underfunded schools. He
and those like him are the cause of it. They are the cause of it. Why? Because we
love the uneducated. Remember him saying that? I love the uneducated. America as a
whole loves the uneducated. The establishment does. Because the
establishment has no use for educated people. Might decide that you can run
your own life. You don't need them. You might interfere with their profit
So, policy makers demand that schools create worker bees,
not thinkers, teach them just enough so they can get a job.
We're hearing that a lot now, right?
The marketability of the skills, job skills.
And the educator in the classroom, well, they can't really do much beyond that.
They can't fight all of society, and all of society is now geared towards
society is now geared against them.
It took us 250 years, less than, to go from Thomas Jefferson
saying, educate and inform the whole mass of the people they
are our only sure reliance for the preservation
of our liberty, to parents asking teachers, well,
why does my kid need to know this,
and silently wondering why they are losing their freedom?
But money isn't really the answer.
Not on its own.
Don't get me wrong.
No teacher should ever have to appeal to the public
and rely on the generosity of the public
to get the tools they need to do their job when those tools
should already be provided by the system
that we're currently in.
that shouldn't happen
but money alone isn't the answer teachers need more resources yes
but throwing money at it
isn't a magic bullet
like most forms of magic
doesn't exist although that's what we always look for when we have a problem
in the united states we look for that one thing that'll solve it
it's not how it works
it's not how it works and i know right now somebody's going buh-boh
that he showed that schools with more money produce better outcomes.
Yeah, that's true, but that is true.
But what if that is a side effect of the cure, rather than the cure itself?
During World War II, there was a guy named Abraham Wald.
He was a mathematician.
The Air Force had gone through and drawn up diagrams showing where their bombers were
getting shot at, shot up, where all the bullet holes were. He got his hands on them, got
his hands on those diagrams. Some of the planes, well they were just riddleable bullets in
certain areas. And it looked like it was pretty much the same areas in all of the diagrams.
And on some of them, they weren't shot up in some areas at all. No bullets ever hit
in these areas. The Air Force wanted to armor those that were riddled with bullets. Armor
those areas that were riddled with bullets. Makes sense, right? Well, Abraham had a different
idea. He's like, guys, you're wrong. You need to armor the areas that were never shot.
He was right, took him a minute and he explained, guys you're looking at the planes that made
it back.
So rather than showing the frequency of where bullets hit, it showed where planes could
take bullets and still return.
Those planes and those diagrams that showed all those empty spaces that never got shot
up, it's because those planes crashed after getting shot.
They armored the areas that didn't get shot up in the diagrams, saved thousands of lives.
do the same thing when we're talking about public education. Yeah, more money increases
outcomes of those who graduate. We're looking at the planes that made it back, not those
that crashed. If you look at those that crashed, you have four main reasons. There's others,
there are four main ones. I was failing too many classes. That's number one. So
education was not important. Didn't put forth the effort. Couldn't put forth the
effort because you didn't have the support structure at home. Whatever.
Education was not really that important. I was bored. Education is not its own
reward. Education is not important. Number three, I became a caregiver, had a baby.
Number four, school wasn't relevant to my life. Education is not important. When
you look at those and understand the three that deal with the view of
education, that's more than 70% gave those reasons. It's crazy. It is crazy because
what it shows is that it's the attitude towards education. Because education is
no longer its own reward, we focus on graduation, getting that piece of paper.
the reality is the benefit of education occurs every day and with our society
the way it is being instant gratification you're asking kids to
plan for a goal 12 years from now wonder why it's not working can't really
imagine why, we need to stop focusing on the paper and start focusing on the education
itself.
The education itself is what is important because we as a society have just talked about
that paper.
Bill Gates said that research shows that there is only half as much variation in student
achievement between schools as there is among classrooms in the same school.
If you want your child to get the best education possible, it is actually more important to
get him assigned to a great teacher than a great school.
And that makes sense.
That makes sense because that teacher, well, they can instill a belief in the value of
education as a whole rather than pursuing that diploma.
teacher can combat society and the views that we instill in the children. I think
that's a little much to be asking of our teachers. I don't think they should have
to fight society, a society that is geared against them, to do their job.
That's a tall order. So when you look at those studies that say, well more money
spent produces better outcomes, it's because the people in those communities
have shown that they value education. They're willing to pay for it. They're
willing to spend more. That idea, that belief was probably passed down to their
kids. So they value education as a whole. It's not money. The reasons for people
dropping out have nothing to do with money.
And that is focusing on the extreme. That is focusing on those that drop out and
And we do need a wider approach.
Education is a community asset.
A successful society would want everyone educated to the highest extent possible.
But instead we talk about job skills.
Yeah.
And right now we're looking at this as this is the whole problem.
Yes.
We need to change our attitude towards education as a whole.
And we need to do it quick.
In the meantime, oh yeah, clear the lists.
Clear the lists.
Give these teachers tools they need to do their job.
But at the same time, advocate to teach the arts,
to teach music, to teach humanities,
to teach all the things that won't get them a job.
All those skills that aren't marketable.
Because that's what makes things interesting.
That's what gives value to education.
have got to change our entire attitude towards education as a whole anyway it's
just a thought now this is normally where the show ends but this kind of
pilot episode for our podcast thank the people over on patreon I said when we
got to a thousand subscribers we would start doing it we're like eight hundred
and something high eight hundreds so I figured I better get my ducks in a row
So, you're about to get to see what it's going to look like, I would like your input,
and if you like what you see, head over to Patreon.
So this week in history, August 26, 1429, Joan of Arc made her triumphant entry into
Paris.
On the same date in 1789 in Versailles, France, the Declaration on Human Rights was approved.
August 31st, 1919, Workers of the World Unite.
The Communist Labor Party was founded in Chicago.
August 26th, 1920, the 19th Amendment to the Constitution is ratified.
Women have the right to vote.
August 27th, 1928, 15 nations sign on to the Kellogg-Brandt PAC.
Iran, another 47 nations would join. It outlawed war. It said we would seek negotiations and
arbitration to solve our problems. Sadly, it just caused countries to stop declaring
war and continuing to fight anyway. August 29, 1957, the U.S. Congress passes the Civil
Rights Act after the longest filibuster in Senate history put on by Strom Thurmond.
August 31, 1961, citizens of Berlin woke up to see the barbed wire fence changed to a
concrete wall, which would later be known as the Berlin Wall.
August 28, 1963, one of the biggest demonstrations in U.S. history happened.
It was called the March on Washington for Jobs and Freedom, and it ended with a charismatic
southern preacher telling us that he had a dream.
August 30th, 1963, a hotline was established between Moscow and Washington, D.C. to deal
with nuclear crisis.
August 30th, 1967, Thurgood Marshall was confirmed as the first black Supreme Court Justice.
August 26th, 1970, there was a nationwide strike for women's equality on the 50th
of the passage of the 19th Amendment. August 31st, 1994, the Irish Republican
Army, the IRA, announced a complete secession of military operations and
paved the way to peace for the first time in 25 years. And now it is time for
today's afterthought. Education is the passport to the future for tomorrow
belongs to those who prepare for it today Malcolm X no education is wasted
none it's all important it doesn't matter what it is it is applicable to
other things in your life that is something that we need to start in
stealing in society as a whole in society as a whole but see there are
things that are playing against us. There are images and stereotypes that discourage
young people from taking an active interest in their education. The stereotypes are different
and the idea is the things that push back against it are different among men and women.
Men seem to be the biggest culprits of failing to want to educate themselves. So we're going
I'm going to talk about them first in probably a little bit of a greater detail.
The idea that holds most men back in school from educating themselves, well, they don't
want to be a nerd.
They don't want to be weak.
They don't want to get picked on.
Tough guys aren't brainiacs.
They're not smart.
When's the last time you heard of a real tough guy with a master's?
General Chaos, Mad Dog Mattis himself.
If there is somebody in popular culture today that is the tough guy icon, it's probably
Mattis, right?
His other nickname is the Warrior Monk and I know people who have worked for him.
The memes are all true.
He is that tough, he is that abrasive and he will knife hand you to death.
His bachelor's is in history, his master's is in international relations with a focus
on security.
tough guys the real tough guys those that are held up as the icons yeah they
are educated some of them formally educated there's another guy his name is
Richard Marcinko demo dick shark man of the Delta this is the guy who founded
and built SIL Team 6. When you think of SILs today, the modern image of a SIL, it's coming
from SIL Team 6. And he built it. His bachelor's is in international relations, his master's
is in political science. Got another one for you. This guy's a little different. He didn't
go on and get higher, higher education. He went to the University of Georgia, I think,
If I'm wrong about that, sorry.
But he didn't go on to get advanced degrees or anything like that because he joined the
Army and he was part of Operation Hotfoot in Laos, just a lowly field officer.
After that, he did a tour with the SAS, Special Air Service in the United Kingdom.
He went over there and he learned from them and he went to Malaysia, I believe, and helped
them in their counterinsurgency operations.
There he contracted a disease and was triaged as beyond help.
In other words, don't waste your resources on this guy, he's going to die.
He survived.
But that time with SAS appears to have been transformative.
He came back to 7th Special Forces, and he was just appalled that unconventional warriors
didn't know anything about conventional tactics, that their training was wrong, their education
was wrong.
We need to fix everything, and he did.
He came up with a new training program, that training program today is known as the Q-Course.
And if you want to get a green beret today, you have to go through it.
Education became really important.
After completely transforming U.S. Army Special Forces, he headed back to Southeast Asia,
went to Vietnam, took a 50 caliber round to the abdomen, was triaged as beyond help.
waste your resources, he's gonna die. And he survived again. That would normally be
enough to put somebody out of the military and say, I'm done with this. Nope. He decided
that there were other units that needed his help. So he revamped Ranger School next, the
Florida phase of Ranger School. It was based on stuff from World War II, it wasn't useful,
We need a better education.
We needed to apply things that we had learned since then, because education never ends.
So he did that.
And then he went on to develop something else.
He founded something that in official terminology was what became First Special Forces Operational
detachment Delta. To most of you, Delta Force. Guy founded a unit that has become the pinnacle
of the US Army. Guy was so tough that in the 80s when they made a movie about him they
They needed somebody to embody them, Chuck Norris, I mean, come on.
And at the end of the day, what is he really known for other than founding Delta Force?
Putting a value on education within the special operations community of the United States.
That's really his crowning achievement because even if he had only done that, Delta would
come into existence.
That education is what makes those tough guys tough guys.
The idea of the weak nerd needs to die.
It's stupid.
If you fancy yourself as a tough guy, you need to understand that if you ever want to
compete among the real tough guys, you better get an education. You better get
an education. Being smart does not make you weak, does not. Now with women it's a
different story because society tends to elevate the superficial over substance,
Beauty over brains type of thing.
Women, I've seen it time and time again
and it is very annoying.
Women have become conditioned to play dumb
because they don't want to intimidate men.
Don't do that.
Never do that, never do that.
Especially if you're talking about somebody
who you're interested in dating.
Because you can't hide your intelligence forever.
It's just gonna cause problems down the road.
More importantly, do you want somebody who can't challenge you?
Of course not, of course not.
And the best men I know, they all married smart women.
But to be honest, that's not a good reason to do it.
Because education enriches your life, your life.
Your motivation should be what's good for you.
how to land a man. You're a prize if you want to go with that mindset of your
purpose is to find a man. You're a prize. If you can't keep up, find someone
better. Do not let society's glorification of the Paris Hiltons and
And can Kardashians of the world convince you that you don't need an education?
You do.
And it's not just a matter of need, it's good, it's good for you, it's good for everybody.
No education is wasted, never stop learning, always stay curious.
Anyway this is just a thought.
And welcome to our Do Gooder Showcase, since we're talking about education we're going
to talk about one of my favorite educational causes. It's called Pencils of Promise, and
they build schools. They've built 500 or so of them in countries like Honduras, Ghana,
places like that. Hundreds of thousands of students have gone through there, and it provides
benefit it helps the education helps lift these communities and helps create
self-sustaining communities makes them harder to exploit in that vein over on
our teespring shop there's a new t-shirt hurricane nuke team 50% of the proceeds
to go to Pencils of Promise. So now it's time for Q&A's. You said you have an
ideological problem with Yang's UBI. What is it? I look at things through a longer
scope. I don't believe in the benevolence of government and I think that the UBI
as an idea sounds great until you realize it is going to help create worker
bees. I don't think that the government is necessarily there to help you. The
United States government is extremely corrupt and I don't know that relying
on an institution that corrupt is a great thing. Cost of living will go up
and that UBI will stay the same. Are you a Democrat, Republican, Green, or
Libertarian? No. I just graduated high school. I've watched you since you started.
You're a father figure, not in a creepy daddy issues way. Give me some life
changing advice. What you believe doesn't matter. What you believe doesn't matter.
What you do matters. Conviction is worthless unless it is converted into
to conduct, go change the world.
How can you support wounded warriors, your anti-imperialists?
For the same reason, I have a tendency to hire ex-cons.
I don't believe that decisions made when somebody is 18 or 19 should preclude them
from help, especially if they're hurt.
So we're going to close out tonight with a story.
some of you have heard this story before. It's a story I heard when I was a kid. It
has stayed with me and it seems very relevant to the theme of education. There's a guy
and he just loved astronauts, anything to do with space. Loved it. And he gets on a
plane and sure enough, an astronaut, I can't remember which one it was, but one of the
famous ones, ends up sitting right next to him. So he keeps trying to strike up a
conversation with him and he keeps kind of making a fool of himself, you know.
And eventually to get away from this guy more than likely, the astronaut pulls out
book and starts reading. And the book is on Portuguese art. And the guy asks him,
you're an astronaut, why are you reading a book on Portuguese art? And the
astronaut responded by saying, because I don't know anything about it. That is a
a reason to read a book. That is a reason. No education is wasted. Anyway, y'all have
Have a good week.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}