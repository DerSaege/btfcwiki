---
title: Let's talk about a teachable life-saving moment in Tennessee....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=vgIziXFYK_c) |
| Published | 2019/08/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing an incident in Tennessee involving two 18-year-olds breaking into a school and stealing an AR-15 and vests, stressing the need to learn from it.
- Emphasizing the importance of acknowledging warning signs and making changes before harm occurs.
- Criticizing the downplaying of the incident by officials and the lack of notification to parents about the danger students were in.
- Advocating for proper gun safes and keeping ammunition on school resource officers to prevent unauthorized access to weapons.
- Challenging misconceptions about firearms, particularly around the use of AR-15s in school shootings.
- Arguing for practical solutions like real gun safes, better security measures, and more training with firearms to improve safety in schools.

### Quotes

- "You have to err on the side of caution."
- "If it can be defeated with this, the weapon's not secured."
- "They can make the decision whether or not to send their kid to school."
- "Keep the ammo on you."
- "Real gun safes. Keep the ammo on you."

### Oneliner

Addressing a Tennessee incident involving stolen firearms, Beau stresses learning from warning signs to prevent harm and advocates for better gun safety measures in schools.

### Audience

Parents, educators, community members

### On-the-ground actions from transcript

- Keep ammunition on school resource officers for immediate access (implied)
- Implement real gun safes for secure storage of weapons (implied)
- Educate on firearm safety and training for effective use (implied)

### Whats missing in summary

The full transcript provides detailed insights on firearm safety, addressing misconceptions, and advocating for practical solutions to prevent harm in schools.

### Tags

#SchoolSafety #FirearmSafety #CommunityAction #Prevention #Education


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we've got to talk about an incident out of Tennessee.
And we've got to talk about it because it seems like the people involved
are trying to sweep it under the rug and downplay it.
We can't let that happen.
And it's not because I think the people involved need their careers destroyed.
I think there were mistakes made, but nobody got hurt, it's a teachable moment.
We can learn from this.
When one of these incidents happens, we always look back and say,
But look at all the warning signs about this particular incident.
We don't do it on a national level.
This is a national warning sign.
We can learn from it.
We can make changes before people get hurt.
So what happened?
Two 18-year-olds broke into a school, whereupon they stole an AR-15 and two vests.
Nothing else was taken.
That says a lot about the intent involved here.
There was a lockdown triggered.
Administration officials are currently saying, but students weren't in danger.
Yes, they were.
Just because now you know the intent was something other than that, they were in danger at the
time.
You didn't know that.
You have to err on the side of caution.
You have to err on the side of caution.
At the moment it was gone, students were in danger.
You have to act as though that's the case because you don't know unless you have them
in custody.
Unless you have the suspects in custody.
Parents weren't notified.
That needs to happen.
No matter how embarrassing it is, that needs to happen.
They can make the decision whether or not to send their kid to school.
They may not send their kid to school for no reason.
So what?
If there is an incident, you want less kids there.
don't know how the suspects got past the gun safe. Okay, well let me help you out there.
If it was a real gun safe and you're looking at it right now and there's no way to know
how they got past it, well that's probably because it wasn't locked or they knew the
code. There aren't a lot of 18-year-old kids that can bypass something like that without
leaving a lot of evidence. My guess, though, is that we're not talking about a
real gun safe, are we? We're talking about a Pelican case or maybe one of those
aluminum school storage lockers. If it can be defeated with this, the weapon's
not secured. You know what you're saying? Yeah, it is. It was locked. As evidenced by the
weapon winding up into 18-year-old kids hands, it wasn't secured. There needs to
be changes. 80% of the weapons used in these incidents are taken from
someone else. They're taken from a family member or they're stolen like this. 80%.
This needs to be addressed. You also need to come clean about what was
taken? An AR-15 in two vests. Okay, so I'm guessing an AR-15, a Kevlar vest, and a
tactical vest, right? And in that tactical vest, what was there? A bunch of magazines
and ammo. They got everything they needed in this incident. So, just from that,
what's an immediate solution to that? 3 AR-15 mags, not terribly heavy, right?
They make pouches, keep the weapon in a gun safe, keep the ammo on you. It's that
simple. It's that simple. These can be easy fixes. Now from the other side of the
coin here, I've seen a lot of quotes from people who have bought into their own
propaganda and that makes you completely ineffective.
One of the questions, well why was there an AR-15 there?
Is that the best weapon to use for something like this?
No, no.
They should have MP5s.
But they're not going to train with them if you give it to them and they don't know how
to use them.
So an AR-15 is probably your best bet because it's the most popular rifle in the United
States.
That's why it gets used so much.
It's the most popular rifle in the United States.
They already know how to use them.
My understanding was that this was the deputy's personal weapon, which is also very common.
A lot of sheriff's departments aren't really well funded, and the weapons and money they
get through DOD, it gets earmarked.
Or the sheriff just decided it has to go to the SWAT team rather than the school resource
officers.
Now, I want to read a quote, if you are in a situation where you're trying to take out
a bad guy and there are kids everywhere, I would think that you would want the most precise
weapon possible and not one that was designed by the military to kill them even if you nick
them.
Don't believe your own propaganda.
This is from an anti-gun violence advocate.
Don't believe your own propaganda because now anybody that knows about firearms is looking
that your opinion is worthless.
At the ranges that a school shooting takes place at, an AR-15, it's pretty precise.
I mean, it's going to go, the bullets are going to go where the rifle tells it to, it
is pretty precise.
And no, it isn't designed to kill someone even if you nick them, that's just not true.
That's not true.
The AR-15 is going to be what most commonly comes to rescue those kids.
So you need to stop demonizing it and understand it's a tool.
It can be used either way.
Most times, when Sheriff's Department shows up, that's what's in their hands.
Are there better weapons out there?
Yeah, but they're more expensive and they don't know how to use them because they never
trained with them.
They had one because it's the most popular rifle in the United States.
It's stuff like this that kills common sense gun control.
This kind of propaganda because then a shooter, somebody who knows about guns, they're not
going to listen to anything else you have to say.
No an AR-15 isn't the best, but it's the best you're going to get.
Realistically, that's the best you're going to get.
So common sense from this point, real gun saves, because I got a feeling it wasn't a
real gun save.
We don't know because they won't come clean.
And as far as that goes, SROs, please, school resource officers, keep your ammo on you.
You're going to go to your weapon anyway to get it, keep your ammo on you.
That way there's no way the kid can grab it.
kid can get into your office and it's shown that it can happen. Get your rifle
and start shooting. Keep your ammo on you. It doesn't take long to slap that
magazine in and chamber it. You're gonna spend more time than that putting on all
your cool molly gear anyway. Gun safes. Real gun safes. Keep the ammo on you. That's what
we can learn from this. And that could save lives. If these were shooters, they wouldn't
have known. Because by the time the school resource officer gets there, they could have
already started anyway it's just a thought y'all have a good night

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}