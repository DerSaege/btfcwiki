---
title: Let's talk about an old atlas, colonization, and the future....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Nab7UB7bbGA) |
| Published | 2019/08/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the Italian occupation of Ethiopia in 1935-1936 and how it was not a functioning colony.
- Mentions the conflict during the Italian occupation of Ethiopia.
- Questions why an old Collier's Atlas from 1940 marks Italian East Africa as if giving the fascists and Italians a win.
- Points out the propaganda and biased narratives in historical accounts and atlases.
- Differentiates between two types of propaganda: nationalist and the narrative of Western supremacy.
- Emphasizes the importance of challenging the image of the uncivilized African perpetuated by colonial powers.
- States that colonization never truly ended, just evolved into wealth extraction through different means.
- Criticizes the creation of Africa Command by the Department of Defense (DOD) in 2007 and its mission centered around military intervention.
- Argues that African nations desire business partnerships rather than exploitative military presence.
- Warns about the consequences of not seeking the truth from current powers and victors, especially in relation to Africa.

### Quotes

- "The victor will never be asked if he's told the truth."
- "Colonization never ended, it never stopped, it just changed a little bit."
- "It's still about wealth extraction."
- "The people of Africa, they don't want our troops. They want our business."
- "We have to start seeing through it or our reluctance to look at the facts is going to cost a lot of lives."

### Oneliner

Beau delves into the nuances of colonization, propaganda, and Western supremacy, urging a critical reevaluation of historical narratives for a more equitable future.

### Audience

History enthusiasts, activists

### On-the-ground actions from transcript

- Challenge biased narratives in history books and educational materials (implied)
- Support African nations in building their economies through fair business practices (implied)
- Advocate for transparency and accountability from current powers and victors (implied)

### Whats missing in summary

Beau's engaging analysis prompts reflection on the pervasive influence of propaganda, challenging individuals to seek the truth beyond biased narratives for a more just society.

### Tags

#Colonialism #Propaganda #WesternSupremacy #AfricanNations #Truth


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're gonna talk about what an old atlas
can teach us about
colonization
and victors.
Two videos back
I mentioned Ethiopia
as an Italian colony. I included that as a jumping off point, not anticipating so
many
African colonial history PhDs being in the comments section.
on you guys. You're right, it was never really a colony. Not really, it was never a functioning
colony, it was an occupied territory. There were some colonies around it, but it all got lumped
together as Italian East Africa. But Ethiopia, well it was just occupied, attacked in 1935, 1936,
something like that. And the Italians only held it for a few years, and that entire time there was
was conflict. But see the victor will never be asked if he told the truth. I've got a
Collier's Atlas, copyright 1940. I'm not sure if you can actually see this, but there's
Italian East Africa right there, and it's marked. It is marked. It seems like an odd
thing for Atlas in the US to do. I mean why would we give the fascists that win?
Why would we give the Italians that win? We weren't technically at war yet but
we'd already picked our side. It's interesting. Now I'm sure somebody out
there is thinking well they're just marking who's ruling it, who's governing
at the time. No, that's not what they're doing, because here's Europe. 1940, I see Poland on
there. The border's never changed. It doesn't show any of the German advances.
Why do you think that is?
There's two sets of propaganda that run around us at all times.
And we look at that quote.
The victor will never be asked if he's told the truth.
We look at that as a singular event.
If you win this event, you're not
going to be asked if you're telling the truth.
The thing is, there are always winners and losers.
There are always victors.
And those victors are always producing
Stuff like this book.
That propaganda is immense, and it's around us all the time.
There's two kinds.
You have the nationalist kind that you see all the time.
You see it everywhere.
USA, USA, we're number one.
Love it or leave it.
America's number one in everything.
It's why you had comments about uneducated Africans talking
about Tunisia, where the literacy
rate, the functional literacy rate, is higher than the US and the dropout rate is lower.
The other kind is this overriding narrative of Western supremacy.
This Atlas shows it perfectly because there it is.
We'll recognize our opposition's claim to those others, but not in Europe.
Europe. We can fight amongst ourselves and well we'll figure it out later but
down there in the global south it's all up for grabs. That image of the
uneducated African, mud huts and all of that, it's very important for the powers
that be, the victors of today. It's very important for that image to remain. It's
extremely important because it allows us to see our actions over there as saving
them from themselves, civilizing them. Homework assignment. Hit Google Images
and pull up the capital of an African nation. Look at those skyscrapers, doesn't
look a whole lot like mud huts does it they don't need us but this image this
idea of this uncivilized barbaric land it has to remain because colonization
never ended it never stopped it just changed a little bit what was
colonization it's about wealth extraction that's what it was about period
full stop. It's about getting the money. Nothing's changed. We swapped out flags
for corporate logos and ongoing long-term military occupations for
airstrikes and puppet governments. That's all that happened. There was no real
change in philosophy, just in tactics. It's still about wealth extraction. In
In 2007, DOD created AFRICOM, that's Africa Command.
The Department of Defense now has a specific command to deal with Africa.
What's its mission?
Africa Command, with partners, disrupts and neutralizes transnational threats, protects
U.S. personnel and facilities, prevents and mitigates conflicts, and builds African partner
defense capability and capacity in order to promote regional security, stability, and
prosperity.
It's always about money, and DOD, the U.S. in general, is clinging to this idea.
They're holding to it with a death grip that it has to be done militarily.
We still want to be the world's policemen rather than the world's EMT.
The thing is, the people of Africa, they don't want our troops.
They don't.
They want our business and not of an exploitive nature.
That's what they want.
They want to be able to build their countries and not be under the thumb of a Western power.
So much so that their colonization is recognized when it never really happened.
It's important that we recognize this propaganda and that it's all around us all the time and
that it influences the way we think.
If we don't, here in the next 10 years, in the US, in the UK, oh, we're going to start
losing a lot of troops in Africa because as these countries grow and develop, and they
have, they're going to want their piece of their own pie and they're going to fight for
it.
That's why Africa Command was created.
The whole job of that command is to keep Africa under the US's thumb.
That's what it's there for.
They're not going to stay there willingly, not for long.
That's why it's really important that we have to ask the victors for the truth.
The victors of today, the powers that be today, we have to get the truth.
We can't rely on them, or their atlases, or their textbooks, to give us an objective
picture of the world.
My most prized possessions are old history books from different countries.
Because it shows you, yeah, the nationalist story is different in each one, but the one
thing that remains is Western supremacy. It's in all of them. It is in all of them and yeah
you could switch out that W word in Western supremacy for another W word. It's pretty much
the same thing. It doesn't really change much. We've got to start seeing through it or our
Our reluctance to look at the facts and how things really played out is going to cost
a lot of lives.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}