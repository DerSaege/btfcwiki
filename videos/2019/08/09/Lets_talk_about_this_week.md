---
title: Let's talk about this week....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=qlwmvqp0E-I) |
| Published | 2019/08/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recaps recent events, including Pentagon surveillance balloons in Wisconsin for total information awareness.
- Mentions the Walmart incident where thankfully no one got hurt.
- Talks about ice raids resulting in hundreds of people disappearing, leaving kids at daycare and coming home to empty houses.
- Shares a story of a man deported to Iraq where he eventually died, despite never having been there and not speaking Arabic.
- Describes a disturbing incident of a child's skull being literally cracked over not removing a hat during the National Anthem.
- Comments on an immigration activist caught with weapons standing by a Trump mural on his truck.
- Addresses the evolution of a gesture from a joke to a white nationalist symbol.
- Criticizes blind loyalty to symbols over their true meanings and the dangerous consequences.
- Warns about the increasing violence and threat faced by minority groups in the current political climate.
- Urges everyone, especially non-white and non-straight individuals, to be vigilant and cautious in the deteriorating situation.

### Quotes

- "If you were out there to own the libs, yeah, you are now forever identified as a white nationalist."
- "Imagine being so in love with an idea of a symbol that you start to hate what it's supposed to represent."
- "People are dying. Children are having their skulls cracked."
- "As things start to deteriorate in the economy, you're going to see them start to become disillusioned and it's going to make them more violent."
- "Winning comes at a cost, and it's gonna be the minority groups that's who's gonna be hit the most."

### Oneliner

Beau recaps recent events, warns of escalating violence against minority groups, and urges vigilance in the face of deteriorating political and economic conditions.

### Audience

Activists, minorities, allies

### On-the-ground actions from transcript

- Stay vigilant and aware of your surroundings (implied)
- Be cautious and careful in the current political climate (implied)
- Keep your eyes open for signs of escalating violence (implied)

### Whats missing in summary

The full transcript provides a detailed and emotional analysis of recent events, warning of the increasing violence and threats faced by minority groups in a deteriorating political climate.

### Tags

#CurrentEvents #Violence #MinorityRights #Vigilance #Activism


## Transcript
Well, howdy there, internet people, it's Bo again.
It's been quite a week, hasn't it?
I mean, a lot's gone on.
Let's run through some of it.
Let's run through some of it.
Let's talk about it.
Talk about the USA Today.
Those Pentagon surveillance balloons going up over what,
Wisconsin, I think?
Total information awareness.
They're going to be documenting everything.
don't worry it's just an experiment now but these balloons will go up and they
will be able to track everything if you go to a labor meeting well they'll know
because they'll track your car intercept cell phones intercept everything total
information awareness straight out of some dystopian novel it of course had
the Walmart incident and it looks like another one nowhere near as bad and it looks like
nobody got hurt, thankfully. He had the ice raids. Hundreds of people disappeared. Just
gone. Just gone. Their kids come home to empty houses on the first day of school. You got
to be real dedicated to your job to do something like that to somebody.
Kids left at daycare because there was nobody coming to pick him up.
Disappeared.
You had a guy who got deported to Iraq.
He'd never been there, didn't speak Arabic, type 1 diabetic.
He's dead.
He died.
I guess he was a Greek national or that's where he grew up and then he came to the U.S.
pretty much his whole life here, but I guess his parents were Iraqi, so that's
where they sent him, having never been there, yet the guy who was alleged to
have literally cracked a child's skull for not removing his hat during the
National Anthem, you know somebody, the first time I saw that get mentioned was
in the comments section and that that's a that's an idiom down here crack skull
I thought somebody got punched I didn't know somebody hit it literally cracked a
child's skull over a song then you had a guy just got caught outside of the
organization space for some immigration activists wearing I think nitrile gloves
had a knife firearm whole nine yards. There's a photo of him standing by his
truck. It's got a big Trump mural on the tailgate and he's standing there giving
the okay sign. See I actually remember when that was just a joke. That started
for real, that started as a way to troll Democrats, troll liberals, own the libs.
They memed it into existence though, like it was a joke in the beginning.
We can get liberals to believe anything is part of the white nationalist movement or
whatever.
That's how it started.
And then it became part of it, and see, here's the part about that, I'm not sure people get.
If you were part of that initial thing, where it was just a joke and you were out there
to own the libs, yeah, you are now forever identified as a white nationalist.
The internet is forever.
Those photos of you, they're forever.
And you memed it into existence.
It's a real thing now.
See it stops being a joke.
actually long before, but it stops being a joke for certain when people start
dying. And then this guy, there's no question about whether or not Trump's
rhetoric inspires violence now amongst the brown shirts. It's what these are,
shock troops. That's his defense. The guy who cracked this kid's skull, well the
commander-in-chief said it was okay. Man. Man. Imagine so being so in love with an
idea of a symbol. That you start to hate the idea that that symbol is supposed to
represent. Fall in love with that symbol so much that what it is supposed to
represent means nothing to you. That you have fallen for a cult of personality so
strong, you would literally harm a child over a song. Over a song. And I don't care how
patriotic you are, it's a song. It is just a song. And every one of these incidents makes
that flag and that song mean less and less child school literally cracked over
a song so here we are you know people always say this is how it starts no
this isn't how it starts this is way deep into it the face of tyranny is
always mild at first. It's not mild anymore. It's not mild anymore. People are dying.
People are dying. Children are having their skulls cracked. Children are coming home to
empty houses. Their parents are non-persons.
We keep heading down this road and we're making winds and the tide is turning.
But as that tide turns, everybody needs to be real careful because I have a very strong
feeling that those that the tide is turning against, like most people caught in a tide,
they're going to thrash violently.
need to be aware of what's going on around you, especially if you are any form of a minority
group.
Any form.
If you are a member of any demographic that is not within 100% within the norm, and by
norm I don't mean normal, I mean the norm, the median, the mode, the average, if you're
You're not white and straight, how about that?
You really, really need to be aware of your surroundings.
This is going to get worse.
As things start to deteriorate in the economy, and that's because that's what's bolstering
them right now.
The followers, the people that belong to this cult of personality, that's what's keeping
them loyal.
As the economy starts to falter, and it will, you're going to see them start to become disillusioned
and it's going to make them more violent.
Every step that we force the country to take back, away from the abyss that is fascism,
these people are going to get more and more violent.
You need to be careful, you need to be aware, don't be afraid.
Be aware, be careful, keep your eyes open.
it's winning comes at a cost sometimes winning comes at a cost sometimes and
it's gonna be the minority groups that that's who's gonna be hit it's most
guaranteed y'all just be careful anyway it's just a thought y'all have a good
night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}