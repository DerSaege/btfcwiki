---
title: Let's talk about Trump, Obama, Alejandra, friends, and enemies....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=nd7YhsJS5MI) |
| Published | 2019/08/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Criticizes the administration for focusing on rewriting the immigration laws instead of addressing more pressing issues.
- Points out the media's role in perpetuating misleading narratives about legal immigration.
- Contrasts Trump's deportation numbers with those of the Obama administration.
- Notes the ineffectiveness of Trump's efforts due to his fixation on stereotypes of undocumented immigrants.
- Explains how the Obama administration utilized the surveillance state for deportations.
- Tells the story of Alejandra Juarez, who self-deported after facing legal challenges despite being in the US for 20 years.
- Criticizes the administration's treatment of deported military spouses and its impact on combat veterans.
- Addresses the demonization of immigrants receiving benefits compared to corporate welfare.
- Calls out the administration for causing harm through policies like the trade war.

### Quotes

- "It's about framing it so you kick down. You blame those with less power than you for the mistakes or the deliberate attacks on you by those at the top."
- "They find somebody to scare you. Somebody for you to kick at. Most times isn't real."
- "This administration is not the friend of anybody in the working class."

### Oneliner

Beau criticizes the administration's immigration focus, deportation tactics, and treatment of deported military spouses while urging working-class solidarity and exposing harmful policies.

### Audience

Working-class Americans

### On-the-ground actions from transcript

- Support deported military spouses' communities (exemplified)
- Advocate for fair treatment of immigrants and combat stereotypes (suggested)
- Educate others on the impact of harmful policies on working-class communities (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of immigration policies, deportations, and their effects on individuals and communities, urging viewers to question narratives and stand in solidarity with marginalized groups.

### Tags

#Immigration #Deportation #WorkingClass #Solidarity #Policy


## Transcript
Well, howdy there, internet people, it's Bo again.
And we have a lot to talk about today.
You got that guy from the administration
rewriting New Colossus.
I'm sure, man, he thinks he's clever.
He probably spent all day working on that gem.
Send me those who can stand on their own two feet.
He's funny.
Because that's America's biggest concern right now,
is that maybe at some point, sometime in the future, somebody who has granted a
permanent resident card may possibly access one of our safety nets that are
available to people.
Yeah, I'm not following, I don't see why this is a concern, but the media is
playing along with it because that's what the media does.
the headlines are saying Trump now curtailing legal immigration. Now. He's
been doing it the entire time, but the media makes no attempt to educate.
Asylum seeking is legal. It's a treaty obligation. When we violate that treaty
we violate the US Constitution because treaties are law of the land, but they
made no attempt to educate the American populace. So that talking point, it
kept going. It stayed alive, even though it shouldn't have. Trump isn't trying to curtail
legal immigration. That's not what it's about. It's not about stopping people who are going
to come. It's about finding people to deport. That's what it's about. It's about taking
people who would be okay and making them not okay, because all he cares about is his ratings.
When you look at his tweets, you see any time he talks about a TV show he doesn't like,
he talks about their failing ratings, or their viewership, or their readership if it's a
newspaper, whatever.
That's all he cares about.
One of the reasons I didn't support the Obama administration is because President Obama
was the deporter in chief.
He really was.
And Trump, with all of his rhetoric, with the militarization of the border, with people
having their doors kicked in, windows smashed and drug out of cars, ICE raids going on everywhere,
he can't match Obama's numbers.
That's driving him crazy because he looks like a failure to his base, because he is.
In cooking the books and counting asylum seekers who voluntarily turned themselves into border
patrol as border apprehensions, he still couldn't do it.
It's because he bought into his own stereotypes.
Well illegals are obviously brown people from south of the border.
So he focused all of his efforts on that.
than 40% of undocumented people in the United States, they arrived by air.
That wall isn't going to do anything unless it's 36,000 feet high.
There's another reason the Obama administration was more successful and why Trump is currently
just getting Obama's leftovers.
Obama used the surveillance state.
He used information sharing between local police departments and federal agencies to
find people to get out of here.
The people Trump is getting now are those that the Obama administration looked at and
were like, we're not going to worry about these people because they weren't that big
of a deal.
That surveillance state, it caused a backlash.
Generally speaking, Americans don't like the idea of ratting out their neighbor to the
Stasi.
So it was curtailed.
It was curtailed.
Local jurisdictions pulled back from that information sharing.
Makes Trump's job a little harder.
So he has to go off of the other information, the information that was gathered before all
of it was curtailed.
That's why you have stories like Alejandra Juarez.
Been here 20 years.
She had a traffic violation in 2013.
So her information got shared.
So she went through the process and fought and fought and tried to do things legally,
tried to do things the right way.
And of course she was denied every step of the way.
She's married to Maureen, I think he's out now.
But you know what, there's an article on CBS, I think I have it on my phone.
Alejandra ultimately decided to self-deport to Mexico rather than turn herself in to be
detained and then deported.
After 20 years in the United States, she no longer has family or friends in the country.
So she chose Merida, a city in the Yucatan, where a small community of deported military
spouses might help her.
A community of military spouses.
That's making America great again.
A community of deported military spouses, deporting the spouses of veterans.
That's how we're going to make America great.
those who kept the home fires burning while their spouse was all fighting a war, getting
rid of them, those bad hombres, ripping away the family structure of combat vets.
Oh I'm sure that's going to do wonders for the suicide rate.
It's all about the same thing.
It's about framing it so you kick down.
You blame those with less power than you for the mistakes or the deliberate attacks on
you by those at the top, those with the power.
Blaming Alejandra for the Trump administration's decisions.
That's what it's about.
Let's talk about the immigrants maybe getting benefits.
If you make $50K a year, you pay about $0.10 a day to fund food stamps, $36.50 a year to
feed 40 million people.
Yeah I can see that as being a huge concern.
I think a bigger concern would be the fact that we spend, if you're just talking about
direct corporate welfare, $870 to $907 a year, depending on the study.
If you include all of the indirect subsidies and all of that stuff, the average American
family pays six grand for corporate welfare.
Oh no, let's worry about the 36 bucks that maybe some of it might possibly be going to
an immigrant. They've got you kicking down. And then you've got these farmers and these
ranchers scratching their heads right now wondering why the Secretary of Agriculture
is making fun of them as they're losing their farms. Calling them whiners. Making jokes.
Because they never cared about you. They're not your friends. They don't represent you.
They had nothing, they didn't care about you at all.
You were just somebody they could target, use a stereotype, get you to hate someone
else so you'd put them in power.
That's it.
That's it.
This trade war that Trump started, it cost you your farm, your buddy his ranch, cost
the American taxpayers billions and increase the trade deficit must be
Alejandra's fault right they don't care about you when this is all over and they
have milked this country for every dime they can well they're all going back to
their gold toilets you you're gonna be working at one of those big corporate
farms because you lost yours. It's like Scooby-Doo. They find a boogeyman. They
find somebody to scare you. Somebody for you to kick at. Most times isn't
real. When it's all said and done, when you finally realize what's going on and
you've solved the mystery, you pull off that mask and you see some old dude in a
suit who you thought was your friend. This administration is not the friend of
anybody in the working class. It doesn't matter if you're a coal miner,
a farmer, an Amazon worker, it doesn't matter. They're there to extract as much
wealth from you as possible and move on. That's it. They're not your buddy and
and Alejandro isn't your enemy.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}