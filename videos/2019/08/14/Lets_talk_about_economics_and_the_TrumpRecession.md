---
title: 'Let''s talk about economics and the #TrumpRecession....'
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=B-Sh6jtGuUg) |
| Published | 2019/08/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Most Americans don't understand the economy, and a recession is looming.
- The yield curve inverted, signaling a recession.
- A recession is a decline in trade and industry activity, where GDP falls for two consecutive quarters.
- The former chairman of the Fed, Greenspan, warned about yields falling below zero on bonds.
- This recession is unique as it was triggered by tariffs, not the typical unemployment route.
- People on Twitter are already calling this the "Trump recession."
- Recommendations for lower economic classes include preparing by not making big purchases and saving money.
- Those with money to burn are encouraged to increase demand for goods and services within the US.
- History shows that tariffs triggered a recession leading into the Great Depression.
- Mitigating the effects of the recession caused by the administration is vital to prevent widespread harm.

### Quotes

- "Get prepared. Don't make any purchases."
- "The tariffs opposed on the other side limited the goods produced. That's what triggered this."
- "Burn through any reserves you have. Spend it."
- "We need to do what we can to mitigate the destruction that his administration has caused."
- "Don't cheer this on as much as you want to, as much as I want to. We've got to work to get out of it."

### Oneliner

Most Americans don't understand the looming recession triggered by tariffs; prepare financially and increase demand to mitigate its effects.

### Audience

Americans

### On-the-ground actions from transcript

- Prepare financially by avoiding big purchases and saving money (suggested)
- Increase demand for goods and services within the US by spending reserves (suggested)
- Work to mitigate the effects caused by the administration's policies (implied)

### Whats missing in summary

The full transcript provides a detailed explanation of the economic indicators and practical steps individuals can take to prepare for the impending recession.

### Tags

#Economy #Recession #Tariffs #FinancialPreparation #US


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we've got to talk about something
that most Americans don't really understand, the economy.
Nobody knows how it works.
OK, the reason we need to talk about it
is because we got a very, very, very, very, very, very
strong indication that there is a recession on the way.
People are already saying we're in one.
We're not, but it's coming.
What happened?
The yield curve inverted.
In English, the amount of money you make off
of a 10-year treasury bond, the interest rate,
it fell below what you would make off of a two-year bond.
What that means is that people have less faith
in what's going to happen 10 years down the line
than what they do in two years, which is pretty simple.
So that's a very strong indication
that we're headed into a recession.
how strong. Out of the last seven recessions we've had it predicted all of
them. It's an extremely strong indicator. It's not going to be different this
time. You normally get well the last one it was a two-year warning. I don't think
that it will happen you'll have two years before the recession hits on this
one though because this one is a little different. What is a recession? A
A recession is a decline in trade and industry activity.
Pretty simple, also means nothing, right?
Tells you nothing.
The GDP falls for two straight quarters, that's what it means.
What is the GDP, the gross domestic product?
That's the value of goods produced
and services rendered across the country.
That's what it means.
What is the difference between a recession and a depression?
Length, that's it.
When a recession hits two years, it becomes a depression.
I mean, depressions are typically a little bit more severe, but it's because they're
longer lasting.
Now one of the more chilling statements that has come out of this is the former chairman
of the Fed, Greenspan.
He made a point to say that there was no barrier to yields falling below zero on bonds.
He seems to be bracing people for the expectation that that happens, and that would be zero
confidence in what's happening two years or ten years down the line.
That's bad.
Unlike most previous recessions, this isn't a domino effect.
When you start looking into economics and you start reading about it, they're going
to say well typically a recession starts with unemployment being high and then
therefore less things are bought therefore trade declines. It's not
how this happened. This happened because of one thing and one thing only. The
tariffs. What do the tariffs do? And the tariffs opposed on the other side. They
limited the goods produced. That's what triggered this. People on Twitter are
calling this the Trump recession, it is, it is, that's who triggered it, the sad part
is that because it normally takes a while to take effect and start feeling it, he may
not catch the blame, it may be the next president that catches the blame for this, but it started
today, it started today, now because the tariffs are in effect, I think it may happen sooner
than two years. I think we might start feeling the effects. Okay, so what can you do if you
are on the lower rungs of the economic class system in the United States? Get prepared.
Get prepared. Don't make any purchases. You don't have to. Don't make any big purchases.
You don't have to. Start kind of squirreling away some money if you don't have it. You
You may end up losing your job.
If you are on the upper rungs, you've got money to burn, burn it right now.
That's the one thing that could stop this is increasing demand for goods and services
within the United States.
The tariffs aren't allowing it to be exported.
Burn through any reserves you have.
Spend it.
them produce more. So the last time tariffs triggered a recession of any
significance was about 80-90 years ago and it did go into the Great Depression.
As much as it may seem like the thing to do to kind of cheer this on so you
You can point and say, look, Trump did it.
We knew it was going to happen.
We did know it was going to happen.
If you look back through my videos, you discount the last month, go back all the way to beginning,
you may find one or two videos about the economy.
In the last month, there's been a whole bunch.
We did know this was coming.
And yeah, it's Trump's fault.
But at this point, you're on an airplane, and if it goes down, it goes down with everybody
on it.
We need to do what we can to mitigate the destruction that his administration has caused,
otherwise it's going to get bad for a lot of people.
So don't cheer this on as much as you want to, as much as I want to.
We've got to work to get out of it.
So this is kind of where we're at.
That's kind of a primer on the terms that are going to keep popping up.
You can expect stock markets to slide.
You can expect any bubbles to burst.
This is what's going to happen.
It's what always happens.
So kind of just be prepared, be ready for it.
Anyway, it's just a thought.
y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}