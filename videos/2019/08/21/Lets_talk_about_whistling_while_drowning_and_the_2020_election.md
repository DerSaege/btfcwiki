---
title: Let's talk about whistling while drowning and the 2020 election....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ApBJra1taIA) |
| Published | 2019/08/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recalls taking a wilderness first responder course with realistic training scenarios.
- Volunteers to rescue a drowning victim during training, facing unexpected challenges.
- Draws a parallel between rescuing drowning victims and the current state of the administration.
- Describes how panic affects drowning victims and likens it to the president's situation.
- Analyzes the president's strategy to energize his base for the upcoming election.
- Points out that negative voter turnout, not overwhelming support, led to the president's victory in the past.
- Identifies the president's base as bigoted old white men and explains his focus on energizing them.
- Criticizes the president's proposal to end birthright citizenship through executive order.
- Explains the historical significance of the 14th Amendment and its importance to certain demographics.
- Condemns the president's rhetoric around the 14th Amendment as a huge dog whistle to his base.
- Questions the need to indefinitely detain migrant children and points out the profit-driven motives behind it.
- Challenges the illogical reasoning behind holding children longer in detention facilities.
- Dispels common misconceptions about birthright citizenship and who actually engages in birth tourism.
- Warns of future attacks on marginalized groups by the president to rally his base and maintain power.
- Urges readiness to identify and address the dog whistles used by the president to appeal to his base.

### Quotes

- "He has to energize his base and he thinks his base is bigger than it is."
- "His base is not going to carry him to victory, but that's who he wants to energize."
- "We just need to be ready for them."
- "Either you are completely incompetent or your dog whistling to a racist base."
- "Is he stupid or is he a racist? That's it."

### Oneliner

Beau reveals parallels between rescuing drowning victims and the administration's tactics, warning of divisive dog whistles to energize a shrinking base in the upcoming election.

### Audience

Voters, Activists, Allies

### On-the-ground actions from transcript

- Recognize and challenge dog whistles in political rhetoric (suggested)
- Stay vigilant against divisive tactics targeting marginalized groups (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the president's tactics, urging vigilance against divisive rhetoric and calling out potential dog whistles to energize a specific voter base.

### Tags

#Election2020 #PoliticalStrategy #DogWhistles #MigrantRights #MarginalizedGroups


## Transcript
Well, howdy there, Internet people, it's Bo again.
So today, we're gonna talk about whistling while drowning
and what we can expect from the administration
heading into the 2020 election,
because the two things have a lot to do with each other.
Years ago, I took a wilderness first responder course
and I had great instructors.
They made everything as realistic as possible.
Um, it was fantastic.
I used that information a lot
throughout the course of my life.
But one day we're standing there on the dock
and we're about to learn how to pull people
out of the water who are drowning.
And the instructor is like, I need a volunteer.
She's gonna jump in the water, another instructor.
And she's gonna behave like a real drowning victim.
I need somebody to pull her out.
And I understand the drowning victims struggle.
I volunteer anyway.
I'm like, I got this.
I'm looking at her.
tiny woman. I got this. So she jumps in the water, starts to drown. I jump in after her.
She bites me. She busts my nose. She kicks me. It was quite an ordeal. I wound up basically
having to put her in a full Nelson to get her out of the water. People who are drowning
panic. People who are slipping below the surface panic. Panic, the inability to affect self
help. They panic. The president is drowning right now. His poll numbers are
slipping, his approval ratings are slipping, the position of his political
allies is weakening, the economy is going down. All of these things are weighing
on him. So in his mind what does he have to do? He has to energize his base. He
He has to energize his base and he thinks his base is bigger than it is.
He believes that his rhetoric carried him to victory.
The reality is there was a lot of negative voter turnout.
There were a whole bunch of people who showed up not to vote for Trump, but to vote against
Hillary Clinton.
That's how he won.
That had a whole lot to do with it.
His base is not going to carry him to victory, but that's who he wants to energize.
Who is his base?
Bigoted old white men.
That's his base.
That is his base.
There are other demographics that support him, but generally it's bigoted old white
men.
That is who he is going to try to energize.
He's going to propose all kinds of things that appeal to that crowd.
an example. We're going to end birthright citizenship. Okay, he says through executive
order, that's not going to happen. The only way this can be done is by repealing the 14th
Amendment because that's where birthright citizenship comes from. That's where it comes
from. That's how he would have to get rid of birthright citizenship would be to repeal
the 14th Amendment. Pretend you are part of his base. You are a bigoted old white man.
What is the 14th Amendment? Historically, how is it looked at? It was passed in 1868,
right after the Civil War. This is an amendment that, if you are part of his base, well, it
signaled the beginning of the end to your beliefs.
And here's the President of the United States calling it ridiculous.
Man, that is a huge dog whistle.
Huge dog whistle.
The other aspect, something else that just come up,
wants to indefinitely detain migrant children.
OK, why?
Pretend for a moment you are not part of his base.
Why on earth would they need to do this?
What possible reason does it serve?
If their asylum claim is invalid, they get sent back.
Why would you want to hold them longer?
I mean, we know that a lot of it has
to do with these probably ending up in contracted facilities,
and his friends are going to make money.
But there's no logical reason to want to hold them longer.
Either they're let out and they join the American family
or they're sent home.
If there's a problem and they can't get it done in a time frame,
well, then they need to increase efficiency, not hold children longer,
unless, of course, you believe that they should be punished
because they're different.
there's something other about them, right?
And the funny part about this is that
the people who actually use the 14th Amendment,
the birthright citizenship thing,
that's not people from south of the border.
They don't come across the border and have a baby
and say, ha ha, look, American citizen.
That's not a common theme.
They come here to work, they have a kid.
That's how that happens.
There are demographics that actually do
the birth tourism thing to get that other citizenship,
they're mainly from Eastern Europe.
But it doesn't matter because the president is a master
at getting people to kick down, getting them to kick down.
And that's what this is.
He wants to energize his base.
So you are going to see more and more and more attacks
on the marginalized, the others,
anybody that isn't part of his base.
That's what you're gonna see from the president.
If you're not part of that core group,
you're gonna get hit
because he's not about serving the American people.
He's about maintaining power.
So these dog whistles that are going to come out,
We just need to be ready for them.
We need to be ready for them.
We need to point them out every single time, address it the way it is.
Yeah, sure.
Either you are completely incompetent and believe an executive order can override an
amendment to the Constitution or your dog whistling to a racist base.
Those are the only options.
Is he stupid or is he a racist?
That's it.
Those are the only choices there.
There's no way to get around this.
It's very clear.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}