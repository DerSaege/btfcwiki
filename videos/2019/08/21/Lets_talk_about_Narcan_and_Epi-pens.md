---
title: Let's talk about Narcan and Epi-pens....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xT89xscuVt0) |
| Published | 2019/08/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses a question about a meme comparing the cost of Narcan to EpiPens.
- Clarifies that Narcan is not free; someone pays for it, and there are ways to cover the cost.
- Explains the process where EMS administers Narcan and bills for it, just like any other life-saving drug.
- Questions whether people want a scenario where insurance is checked before administering life-saving treatment.
- Mentions grants, insurance companies, treatment centers, and foundations that cover the cost of Narcan.
- Talks about free training available for Narcan administration on International Overdose Awareness Day.
- Debunks the argument that Narcan enables addiction, as it actually inhibits effects and can lead to recovery.
- Addresses the stigma surrounding substance abuse and how it influences perceptions of who should receive life-saving drugs.
- Comments on society's anger towards pharmaceutical companies for high prices, like $600 EpiPens with a manufacturing cost of $2.
- Conveys the message that societal anger often gets misdirected towards vulnerable groups like addicts.

### Quotes

- "Well, addicts should die."
- "It encourages the devaluation of human life."
- "Can't do anything about that. Need to kick down. That lowly addict."
- "No sane person is angry about that."
- "It's just a thought."

### Oneliner

Beau addresses misconceptions about Narcan and EpiPens, debunking myths and revealing societal attitudes towards addiction and pharmaceutical pricing.

### Audience

Advocates, Community Members

### On-the-ground actions from transcript

- Contact local EMS or substance abuse treatment centers to inquire about Narcan training opportunities (suggested).
- Support foundations that provide funding for Narcan distribution (exemplified).
- Advocate for pharmaceutical pricing transparency and fair drug pricing (implied).

### Whats missing in summary

The full transcript provides a detailed breakdown of the cost and accessibility of Narcan compared to EpiPens, as well as societal attitudes towards addiction and pharmaceutical pricing.

### Tags

#Narcan #EpiPens #SubstanceAbuse #Pharmaceuticals #Addiction


## Transcript
Well howdy there internet people, it's Bo again.
So tonight we're gonna talk about a question
that was sent in, it's about a meme.
I don't get through all the messages.
I try, but I don't get to all of them.
If it's important, send it again if I haven't
addressed it in a couple of days.
So the question was asking me to address a meme,
and I'm sure we've all seen it,
or a variation of it.
Why is Narcan free to addicts,
but EpiPens cost $600?
Okay, so starting at the top, Narcan's not free.
Somebody paid for that.
Narcan is not free.
Okay, in scenario one, EMS shows up,
hits the person with Narcan.
They bill them if they have insurance,
the insurance pays for it.
If not, they're supposed to pay for it out of pocket.
If they don't, the cost gets eaten.
The same as any other life-saving drug to include Epi.
That's how it works.
That's how the system works.
And would you want it to work any other way?
Some kid gets stung by a bee, is laying there by the soccer field, EMS shows up and looks
at his mom and says, I understand the kid is dying, but we need to see the insurance
card first.
No.
I've had this conversation a few times, so I know the next one is, well, cops have it
and they're cars.
They do, but you'd have to be a pretty bad law enforcement administrator to pay for that
out of pocket.
There are grants everywhere for Narcan.
Insurance companies will pay for it.
Substance abuse treatment centers will pay for it.
There are foundations set up by the parents of people who have OD'd that will pay for
it. It's out there. The Narcan is paid for by somebody. The follow-up is, well, what
about the training? August 31st is International Overdose Awareness Day or
something like that. American Addiction Centers is offering free training at all
of their locations nationwide. The training is out there for free as well. I
You know of two departments that use drug forfeiture money to pay for an arcana.
That seems kind of fitting to be honest.
Another argument that comes up is that it enables the addict.
The way this works is that it inhibits the effects, which sends the person into withdrawal.
If you have ever seen anybody in withdrawal, you know that's a pretty horrible experience.
It's also one of the things that leads to people getting clean.
There is no data that I have ever seen and I look tonight to suggest that Narcan availability
increases addiction.
That's just not a thing.
But this meme resonated because it plays on a stigma and the idea is that people with
substance abuse problems are poor so they can't pay for the Narcan so it's free.
That stigma exists and it creates the idea that well we should just let them die.
Seriously that's what it's saying because this is a life saving drug.
Well, people with substance abuse issues are not always poor.
That's not true.
Many of them have insurance.
That's how the scripts get filled.
What this is is a classic case of society training us to kick down.
Why are people really mad?
Is it because an addict got a dose that cost $37.50 and saved their life?
No no sane person is angry about that.
They're angry because EpiPens cost $600 and it cost two bucks to make.
I looked the most recent manufacturing cost I could find was $2.
In the past I found some that said it was 8 and one that said it was 30.
Any of those numbers, $600, that's a pretty huge markup.
That's why you're angry.
That's why people are mad.
But they feel powerless against the pharmaceutical company.
So the pharmaceutical company rips them off, charges them $600 for something that costs
$2.
Can't do anything about that.
Need to feel better, kick down.
that lowly addict.
And the message that gets conveyed is that, well, addicts should die.
That's what comes across.
It doesn't encourage pharmaceutical companies to lower their prices.
It encourages people to turn a blind eye to those in need.
and encourages the devaluation of human life.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}