---
title: Let's talk about socially conditioned responses to domestic violence....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=JvI41qfDYXg) |
| Published | 2019/08/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Urgently addressing a personal message that supersedes any news.
- Exploring social conditioning and the problematic association of protection with violence, especially for men in America.
- Sharing a story of a husband with substance abuse issues who beat up his wife, leading to questions of retaliation upon his release from jail.
- Challenging the concept of using violence as a solution and advocating for non-violent ways to ensure protection.
- Encouraging getting the wife out of the abusive situation as the primary goal.
- Addressing societal conditioning that perpetuates harmful norms, such as women feeling obligated to fix their partners or stay in abusive relationships.
- Emphasizing the importance of leaving an abusive situation, despite potential stigmas or economic considerations.
- Providing information about shelters and organizations that can help individuals in abusive situations.
- Asserting that once violence occurs, any obligation or debt to the abuser is immediately nullified.
- Advocating for seeking assistance and leaving the abusive environment for safety.

### Quotes

- "You've been socially conditioned to use violence to solve problems."
- "Get her out. That's the goal."
- "Nothing. You owe him nothing. Get out."
- "Everything that society has said and conditioned people to believe is the wrong thing."
- "Just go."

### Oneliner

Addressing harmful social conditioning around violence and protection, Beau advocates for non-violent solutions and prioritizing the safety of individuals in abusive situations, urging them to leave and seek assistance.

### Audience

Men in America

### On-the-ground actions from transcript

- Find and utilize organizations and shelters that can provide assistance in leaving an abusive situation (suggested).
- Prioritize getting individuals in abusive situations out to ensure their safety (implied).
- Challenge societal norms and conditioning by advocating for non-violent solutions to protection (implied).

### Whats missing in summary

Detailed personal anecdotes or specific statistics related to domestic violence situations

### Tags

#DomesticViolence #SocialConditioning #NonViolence #AbuseAwareness #Safety


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're going to talk about nothing in the news.
I got a message, and it is more pressing
than anything in the news.
I want to get this video out before one
of the people involved in the story is out of jail.
So before I get into this, I want
to say nothing I'm about to say is
an indictment of somebody involved in this story.
It's social conditioning.
And we've been trained to think about this situation
in one way, especially as men in America.
It's not the best way to think about it.
It's not even close to the best way to think about it.
But this is something we need to talk about.
So the message is, the story is there's a husband and wife.
Husband has a substance abuse issue, came home,
beat up the wife
her brother sent the message
the uh...
the husband is currently in jail
the question was basically
should i put boots on this guy when he gets out should should i beat him up
the idea of play here is that gender role
and we're not even going to get into that
The male protector thing, we're not even getting into that because it doesn't matter in this
situation right now in the immediate.
The male protector, the problem is we've begun to associate protection with violence.
It's not necessarily the case.
And I know there are people out there that think I'm a pacifist of some sort.
I heard that the other day and it kind of surprised me.
I'm not a pacifist.
I've just seen enough violence to know that violence isn't always the answer.
Violence begets violence.
Professionally executed violence ends violence.
That's true.
I have to operate under the assumption that you're not going to kill this dude.
Probably wouldn't be sending messages over the internet about it.
And that's the right call.
It's probably not necessary.
That's probably not the route you want to go.
If you're not going to kill him and you're going to use violence, what are you going to
do?
up a little bit, right? These guys have fragile little egos. That ego is going to take a beating
too. Who's he going to take it out on? It's the opposite of protection. In this situation,
if protection is the goal, what's the option? Get her out. Get her out. That's the goal.
You've been socially conditioned to use violence to solve problems, and this isn't just the
person that sent the message, this is a lot of people, and you've been conditioned to
do this by other people who aren't innately violent.
You're not a violent guy.
If you were, you wouldn't be asking, you just would have done it.
And it's not a bad thing to not be violent, that's a good thing.
So the goal has to be to get her out.
That's the goal.
And the same type of social conditioning is going to come into play and create resistance
to that.
Because just as you've been trained to use violence for everything, a lot of women have
been trained, you've got to fix your man, you need to stay and fix it, then you can
have the fairy tale.
No fairy tale has a black eye in it.
Get out.
Then you have the social conditioning that goes along with it as far as stigmas.
You're going to be a single mom.
So better a single mom than your kids being orphans.
This doesn't just end.
It doesn't just stop.
Get out.
economic considerations come into play. You don't want to deal with the stigma
of being on food stamps. Forty million Americans are on food stamps and nobody
cares. Nobody that matters. Get out. Get out.
There are shelters. There are organizations that will help you and
they have so many more resources than they did ten years ago. Just find one in
your area and use it. Get out. Now, I'm certain there is somebody in the comments
section that will say, you know, why did this once and then I got better? You know,
I had a substance abuse problem, I had a psychological issue, whatever. Fine, that's
great. You are the exception to the rule, good for you, but please keep in mind that
The person you were before that psychological issue or that substance abuse problem would
have wanted her to be safe and that requires getting out.
This is one of those things.
Everything that society has said and conditioned people to believe is the wrong thing.
Any obligation that you feel you may have to your husband, it ends the second he knocks
you out.
It's over.
Nothing.
You owe him nothing.
Get out.
Just go.
There are organizations that will help.
Find one.
Anyway, it's just a thought.
y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}