---
title: Let's talk about the history of immigration laws in the US....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=cKwYR_v5sLo) |
| Published | 2019/08/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the history of major US immigration laws to address the question of why wanting people to follow laws can be seen as racist.
- Points out that immigration laws prior to 1776 didn't apply since colonizers were not immigrants but rather replacing existing systems.
- Mentions that from 1787 to about 1880, there were no federal immigration laws due to debates about federal government's power over immigration.
- Notes the Chinese Exclusion Act of 1882 as the first law prohibiting entry based on nationality.
- Talks about the Immigration Act of 1917, which expanded exclusion to Asian countries and included other restrictions like barring sex workers and mentally defective individuals.
- Describes the Immigration Act of 1924, which introduced numerical caps, particularly affecting people from Asia.
- Mentions the 1965 Immigration and Nationality Act, which extended caps to all countries and marked the first limitations on immigration from south of the border.
- Suggests that illegal immigration as a concept emerged when caps were introduced, making it difficult for certain groups to enter legally.
- States that immigration laws have always been racist in origin, leading to the need for amnesty in the 1980s.
- Concludes that real immigration reform must eliminate these caps to address the racist elements embedded in the laws.

### Quotes

- "The law is bad. It's always been bad."
- "All of them are racist. They are racist in origin."
- "Any real immigration reform is going to have to get rid of these caps..."
- "Because it was enacted because of racism."
- "Y'all have a good day."

### Oneliner

Beau explains the history of US immigration laws to showcase their racist origins, calling for reform to eliminate caps and address systemic racism.

### Audience

Policy advocates, activists, educators

### On-the-ground actions from transcript

- Advocate for immigration reform by supporting the removal of numerical caps and addressing the racist elements in immigration laws (suggested).
- Educate others on the racist origins of US immigration laws and the need for reform (exemplified).

### Whats missing in summary

Deeper insights into the impact of US immigration laws on marginalized communities and the necessity for inclusive and equitable reform.

### Tags

#USImmigration #Racism #ImmigrationReform #History #SystemicInjustice #Colonization


## Transcript
Well, howdy there, Internet people, it's Bo again.
So today we're going to answer a question.
We're going to answer a question that always comes up
when we talk about immigration law,
that question that pops up from the law and order crowd.
I just want people to follow the law.
I just support the law.
I just want them to get in line.
I just want them to do it the right way.
I don't know why you have to call me names.
I'm saying is that I want people to follow our laws. Why is wanting them to
follow our laws racist? It's not necessarily unless of course the laws
are racist then it might be. So what we're going to do today is we're going
to go through a timeline of major US immigration laws, major changes in US
US immigration laws. Maybe we can see a pattern, pinpoint the issue for you. So
before 1776, those people in this context are not immigrants. Not really,
because they're colonizers. They're specifically coming to replace
the system that exists. So none of those provisions would really play into this.
this. From the point when the Constitution was signed in 1787 till about 1880 there was
no federal immigration law, none that prohibited who could come in anyway. The reason there
wasn't was because the founders were busy arguing over whether or not the federal government
even had power to regulate immigration law. A lot of them didn't think they did because
the 10th Amendment. If you ever sit down and read the Constitution, you will see
that the Constitution says that immigration is the purview of the
states individually. That really is what it says. And that's how it ran until the
late 1800s when a California immigration officer was mean to a Chinese person. The
case wound up in front of the Supreme Court and the federal government took
the power to regulate immigration. The first act that prohibited who could come was the Chinese
Exclusion Act, and that's exactly what it sounds like. I mean, it is what it is.
Then, the next big change was in 1917 with the Immigration Act of 1917 which created the
Asiatic barred zone. So basically, now it's not just Chinese people that are
excluded, it's anybody in Asia except for countries we own, quote, that's actually
in it. So it targeted most Asian countries. It also prohibited sex workers,
polygamists, people with contagious diseases, idiots, quote. The mentally
defective, which was widely interpreted to mean homosexuals, and that stayed
until 1990, by the way, as well as anarchists, who knew. Then in 1924 there
was a new Immigration Act and this one barred people who couldn't be naturalized
under the Naturalization Act of 1790 which again this is pretty much just
applied to people from Asia. Everybody else had already worked their way
through the court system and had the right to vote at least on paper but this
This one introduced a concept, and that is numerical caps.
They limited the number of people who come from various countries.
Obviously, because it is the United States, the caps from Northern and Western Europe,
well they were really, really high.
Not so much for Eastern or Southern Europe or other places.
And then in 1965, the Immigration and Nationality Act, well, that's when those caps got extended
to everybody else.
Prior to this point, there was no cap.
There was no cap on people from south of the border.
None.
They could come and go pretty freely.
was very, very easy up until that point.
So at the height of the civil rights movement
is when we finally, for the first time,
decided to limit immigration from south of the border.
It's almost like the government was like, hey,
if we let these people in, we might have
to treat them like people.
That was the first time it was limited.
There were some other geopolitical concerns
as far as destabilization of Central America, which
We were really busy doing.
But that's when this started.
When did the concept of illegal immigration becoming a problem?
When did that begin?
The same time.
Because prior to that, there wasn't any.
These caps are what created illegal immigration.
The law is bad.
It's always been bad.
This is why we had to have amnesty in the 80s.
The law is bad and it's racist.
All of them are racist.
They are racist in origin.
All of them.
This is why when you say, I just want my laws enforced, people think you're a racist.
Because they're racist laws.
It's really that simple.
So any real immigration reform is going to have to get rid of these caps in the way that
they're structured.
That's what you're going to have to do.
And that's how you're going to get rid of the racist elements in it.
But this is why people think you're a racist when you call for it.
Because it was enacted because of racism.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}