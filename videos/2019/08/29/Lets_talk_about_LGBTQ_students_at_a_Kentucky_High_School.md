---
title: Let's talk about LGBTQ students at a Kentucky High School....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=IGvyk3LxiHQ) |
| Published | 2019/08/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- High school students at Martin County High School in Kentucky wore pro-LGBTQ clothing and were forced to change by the school administrator.
- The school claimed they were worried about the students being bullied, but allowed students to wear Confederate flags.
- The students felt like they were silenced while the school didn't address the bullies.
- The administrator told the students that their clothing wasn't appropriate and that school wasn't the place to talk about sexual orientation.
- Despite the superintendent claiming the issue was resolved as miscommunication, a student fears further disciplinary action.
- Beau questions the lack of confirmation from the teens that the issue was truly resolved and urges them to speak up to prevent it from being ignored.
- Teens have the right to free speech and expression according to the Supreme Court, as stated in Tinker versus Des Moines.
- Beau encourages the superintendent and school administrator to leave the students alone.

### Quotes

- "Just do whatever the mean man says. Don't make yourself a target. Hide who you are."
- "Maybe it's been resolved. I don't know. But there's no quotes from the teens saying that it was. None."
- "They won't have any of those mean and hurtful words on them, and they will be completely in line with Tinker versus Des Moines."
- "My advice to the superintendent and the school administrator is to leave these kids alone."

### Oneliner

High school students forced to change for wearing LGBTQ clothing face silence instead of support, revealing a double standard in Kentucky's school system.

### Audience

Students, Educators, Activists

### On-the-ground actions from transcript

- Contact the students to offer support and ensure their voices are heard (implied).
- Advocate for inclusive policies and support systems within schools (implied).
- Stand up against discrimination and bullying in educational environments (implied).

### Whats missing in summary

The emotional impact on the students and the importance of standing up against discrimination in educational settings.

### Tags

#LGBTQ+ #SchoolDiscrimination #StudentRights #FreeSpeech #Equality


## Transcript
Well, howdy there, internet people, it's Bo again.
So some high school students at Martin County High School in
Kentucky, they wore some pro-LGBTQ clothing to school.
And they were forced to change by the school administrator.
Now, according to the articles, they were told that, well,
they were worried.
The school was worried they were going to get bullied.
It's Kentucky.
I'm fairly certain that these students were very well aware of what might happen, almost
like it's them screaming that they're not going to take it anymore, kind of a we're
here, get used to it sort of thing.
But see, this thing happened in a school that allows students to wear Confederate flags
to school.
You didn't talk to those who might bully them.
You instead just bullied the students into silence yourself.
It's the way it seems.
Didn't deal with the bullies.
You went after the victims.
Just do whatever the mean man says.
Don't make yourself a target.
Hide who you are.
Man, I bet that's a great school.
I'm certain that it is welcoming and friendly.
I'm certain that it is a positive learning environment.
The other thing I got these students say
that they were told was that the words
on the shirts weren't appropriate.
I guess lady lesbian is just too much, huh?
Too much for Kentucky in this day and age.
That's fine.
That's fine, I got a solution for that.
stay tuned. Also said that the administrator told them that school
wasn't the place to discuss sexual orientation. Yeah, keep children in the
dark. They always make wise decisions. I mean that's how it works, right? I mean if
you don't discuss it and you pretend like things aren't happening and you make
sure that they're not promoted here well then they just don't occur. You put
blinders on and everything will be fine. How's that working out for you with your
2.9 percent teen pregnancy rate in Kentucky? Your educators educate. Now the
superintendent says that this miscommunication was resolved.
miscommunication, that's how it was classified.
Thing is, there's another quote in our article saying that a
student is using a pseudonym because they don't want to
suffer further disciplinary action.
Further disciplinary action.
That certainly sounds like they suffered some for doing
nothing wrong.
For doing nothing wrong.
Maybe it's been resolved.
I don't know.
But there's no quotes from the teens saying that it was.
None.
And I looked.
So I would really love to hear from one of you guys
for a number of reasons.
One, I want to make sure this doesn't get swept under the rug.
Two, I happen to know a guy that has t-shirts made every once
in a while, and I am fairly certain that he would be more
than happy to have some made and sent up to you.
And don't worry.
They won't have any of those mean and hurtful words on them, and they will be completely
in line with Tinker versus Des Moines, which is a Supreme Court ruling that says that teens
do not shed their constitutional rights to free speech and expression at the schoolhouse
gates.
Don't worry, they won't have any mean words, but I think you'll be able to get your point
across.
My advice to the superintendent and the school administrator is to leave these kids alone.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}