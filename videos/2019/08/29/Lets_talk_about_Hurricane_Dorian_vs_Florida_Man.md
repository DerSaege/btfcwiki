---
title: Let's talk about Hurricane Dorian vs Florida Man....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=g-WqaIoTXN4) |
| Published | 2019/08/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing Floridians about Hurricane Dorian approaching the state, specifically central Florida.
- Warning about the potential impact of the hurricane, citing past surprises like Hurricane Michael.
- Urging people to take necessary precautions and evacuate if needed, especially for the elderly.
- Advising on essentials to have on hand: phone batteries, fuel for generators, food, water, first aid kit, and medications.
- Cautioning against risky behavior during the storm, calling out Gainesville residents.
- Noting the diversion of FEMA funds to immigration efforts, potentially affecting hurricane response.
- Mentioning the historical delays in federal funding post-hurricane, especially in the panhandle.
- Acknowledging Florida's preparedness due to previous experiences but warning of potential self-reliance needed.
- Mentioning the state of emergency declaration and the impending presence of the Florida National Guard.
- Connecting the lack of FEMA funding to immigration policies and questioning the effectiveness of a border wall.

### Quotes

- "Get your old people out."
- "Don't do anything stupid during the storm."
- "Be prepared to be on your own for a little while."
- "We know that 90 miles of ocean doesn't deter the drive for freedom."
- "Because of our experiences here in Florida, we know that wall isn't going to work."

### Oneliner

Beau warns Floridians about Hurricane Dorian's potential impact, urges necessary precautions, and questions FEMA funding diversion amid immigration policies.

### Audience

Floridians

### On-the-ground actions from transcript

- Evacuate if necessary and help others, especially the elderly, to do so (implied).
- Stock up on essentials like phone batteries, fuel, food, water, first aid kit, and medications (implied).
- Stay safe during the storm and avoid risky behavior (implied).
- Be prepared to be self-reliant for a few days post-hurricane (implied).
- Support local communities and organizations in hurricane preparation and response (implied).

### Whats missing in summary

Importance of community support and preparedness in facing natural disasters.

### Tags

#HurricaneDorian #Florida #Preparedness #Evacuation #CommunitySupport


## Transcript
Well, howdy there, internet people, it's Bo again.
Florida man versus Hurricane Dorian first
for the messages of concern.
Thank you.
However, Florida geography is very strange
if you've never been here.
This is nowhere near me.
It's on the other side of the state.
It's projected to hit at a cat three.
Now, my fellow Floridians,
I understand it's only a cat three.
However, if the projections are right,
this is gonna be the strongest hurricane
to hit central Florida in 30 years.
Also keep in mind that they told us
that Michael was gonna hit us at a three or four,
and it hit at a five.
Up here in the panhandle where we have a low population,
it's surprising, it's unlucky.
Down there in central Florida
where you have larger cities, that's unforgiving.
You guys all can't get on the road at the same time.
If you wanna get out, get out now.
To those who decide to stay, yeah, we're Floridians,
We know what to do.
Get your old people out.
Make sure that you have your battery packed
for your phone, charged, fuel for the generator,
food, water, first aid kit, any prescriptions
that you're on, because the pharmacy's gonna be closed.
Don't do anything stupid during the storm.
I'm looking at you, Gainesville, Florida.
And just generally stay safe.
Okay, we know what to do, we've done it before.
Just do it, take care of everything.
Also be prepared to do it
for a little bit longer than normal.
The president has been siphoning funds left and right from FEMA to pay for his war on
immigrants.
That's going to slow them down even more.
And keep in mind, up here in the panhandle, it took us eight or nine months to get federal
funding after Michael.
FEMA is not that great to begin with, as evidenced by their response in every hurricane in the
last 15 years.
are probably not going to do better with less funding. So just be prepared to be on your own
for a little while. On the other hand, the state of emergency has already been declared. Florida
National Guard will be on scene as soon as the dust settles and they did a wonderful job up here.
But be prepared to be on your own for a few days. And yes, I do understand the humor in the fact
that it is Florida that is having to deal with the shortfalls in FEMA. I understand that we are
suffering the brunt of all this money being transferred to the wall, the ICE effort, that we
because of our experiences here in Florida, we know that wall isn't going to work.
We've all seen that 90 miles of ocean doesn't deter the drive for freedom, for
safety, for opportunity, the desire to join the American family, the melting pot,
and become part of the American tapestry. We know that 90 miles of ocean won't do
that. I'm fairly certain that 30 feet of wall isn't going to do much, but it's the
situation we're in and because of that FEMA has less money so you just need to
be prepared everybody stay safe anyway it's just a thought y'all have a good
night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}