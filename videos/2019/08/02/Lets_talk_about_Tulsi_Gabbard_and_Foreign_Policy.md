---
title: Let's talk about Tulsi Gabbard and Foreign Policy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=gUxQY0xHEFY) |
| Published | 2019/08/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Candidates' backgrounds influence their positions, as seen with a candidate facing criticism over her Syria stance.
- Trump's policy decisions were not surprising, reflecting his approach to running companies for short-term gains and cashing out.
- Tulsi Gabbard's military background explains her nuanced stance on Syria, contrasting the dehumanization in infantry units with the empathy in medical units.
- Gabbard's attention to international affairs and understanding of the prologue in Syria sets her apart.
- The US involvement in Syria stemmed from a goal of destabilizing and ousting Assad, not supporting rebels or fighting extremists.
- Beau suggests a personal opinion that the US involvement in Syria may have been linked to a pipeline.
- Gabbard's stance acknowledges the US's role in triggering the conflict in Syria and the bloodshed.
- Beau cautions against viewing Assad solely through the US narrative, noting the nuances and authoritarian context of the Middle East.
- Gabbard's opposition to intervention should not be confused with support for Assad; she aims to oppose intervention while acknowledging Assad's actions.
- Understanding candidates' backgrounds and past actions is key to predicting their future decisions in office.

### Quotes

- "Candidates' backgrounds influence their positions."
- "Gabbard's stance acknowledges the US's role in triggering the conflict in Syria and the bloodshed."
- "Understanding candidates' backgrounds and past actions is key to predicting their future decisions in office."

### Oneliner

Candidates' backgrounds shape their positions, evident in Tulsi Gabbard's nuanced stance on Syria, challenging conventional narratives.

### Audience

Voters, Political Analysts

### On-the-ground actions from transcript

- Research candidates' backgrounds to understand their positions better (implied).

### Whats missing in summary

The full transcript provides in-depth insights into how candidates' backgrounds shape their foreign policy stances, urging voters to look beyond surface-level narratives.

### Tags

#Candidates #ForeignPolicy #Syria #USInvolvement #ElectionInsights


## Transcript
Well, howdy there, internet people, it's Beau again, so today we're going to talk
about how candidates' backgrounds affect their positions, it's important because
right now we see a candidate getting just dragged over her stance on Syria.
And her stance on Syria may make a whole lot more sense than you think, but to
show you what I'm talking about, let's look at Trump.
None of Trump's policy decisions are a surprise, really, and they may be shocking,
but they're not a surprise. We knew going into it that he was going to attempt to run a country
the way he ran his companies, which is come in, short-term gains, use whatever tactics you have
to to get those profits up after you take over a company or found one, and then cash out. That's
what he does. It's his mode of operation always has been. He's Richard Gere from Pretty Woman
in a lot of ways, actually.
So, nothing that he did was a surprise.
Tulsi Gabbard, her stance on Syria makes complete sense when you actually look at her background.
People say, well, she was in the military.
Yeah, she was.
She was in a medical unit, though, it's a little different than being an average infantryman.
An average infantryman, he dehumanizes the opposition, the civilians, everybody.
He has to to stay sane.
He has to be able to function and do his job, and we all know what that job is.
The medical profession in the military is a little different.
They treat the opposition, they treat the civilians, so they don't do that, not to
the same degree, not anywhere close.
If an infantry guy is out in the field and a woman breaks cover and runs through the
field of fire and gets hit. His response, idiot, moves on about his day. He can't
allow that tragedy to register. If he does, he's probably gonna get killed. It's
going to affect him. So they dehumanize the opposition so it doesn't affect him.
That's part of how it works. Now in the medical field, something
that he may not even remember, when that woman gets put on a stretcher and brought
into your ER, that may affect you on a deep level. That may be a defining moment in your
experience in that war. Because medical people, people in medical units, they see the cost
on both sides. Whereas if you're infantry, the opposition casualties, well those are
wins for you in the medical community, it's all bad.
It's all bad.
You see the suffering.
So that's something we have to keep in mind.
The other thing that I've noticed about her is that she tends to pay attention to international
affairs on a pretty deep level.
Most of her positions, not all, there's one standout from this that I've seen, but most
incredibly nuanced positions. You don't get a nuanced position by just following
the main US media narrative because that's our fault. That's our fault as the
American people. We like stories. We like just a normal storyline. International
relations, that's not how that works. There are plot holes everywhere. The other
thing is that we tend to come into an event during the rising action. We don't
get the prologue, her position on Syria tells me that she understands the prologue and most
Americans don't.
And that may explain why her position is what it is.
To the average American, at the earliest, Syria entered their narrative in 2010, somewhere
around there.
When did the U.S. decide to oust Assad?
2006.
Everything was behind the scenes, though.
It wasn't part of a major event.
It wasn't front-page news.
If you ask the average American why we went to Syria, it was either to fight the guys
with the black flag and white lettering.
I'm not saying the name because I've noticed something about the YouTube algorithm.
It was either to fight them or to help the rebels defeat Assad.
is that they rose up against a brutal dictator.
Okay, what's the reality?
From 2006 to the start of those spontaneous protests,
the US and the UK were pumping millions in propaganda
into the country to advocate regime change.
Those spontaneous protests were organized and orchestrated
by contractors working for Western intelligence agencies.
And this isn't conspiracy theory.
This is all fact.
This is all, like, all these stories have been broke, OK?
So our goal was never to help the rebels.
The rebels were a tool.
Our goal was never to defeat the guys with the black flag.
No.
I mean, that was a secondary goal once they got out of hand.
But if that was our goal, and that's why we were there,
why were we fighting Assad?
They were Assad's enemy.
We should have been teamed up with them, but we weren't.
Because our goal had nothing to do with defeating them, had nothing to do with the Syrian people.
It had to do with destabilizing and ousting Assad.
My personal opinion, everything up until now has been fact.
My personal opinion is that it had to do with a pipeline.
There are a number of geopolitical reasons that it could have been triggered.
But my personal opinion is that it had to do with a pipeline.
So when you look at her stance and she says some of the things that she says, it's because
she understands, we triggered this, we started this.
So when she talks about some of the blood being on our hands, she's right, she's right.
Even people that Assad killed, none of this would have happened had it not been for us.
And that's an important thing that people need to consider when she seems to be taking
a stance that is just crazy. No, she just understands the subject matter better. She's
right. But I do see her falling prey to something that a lot of anti-colonialist, anti-imperialist,
anti-interventionist, whoever, they fall prey to this syndrome. You want to oppose an intervention
and that's fine. That's good. But just because the US is doing something that isn't right
doesn't mean the other guy's a good guy.
He could be a bad guy, too.
It's very possible, and it happens a lot.
That's the case with Assad.
Is Assad as bad as the US narrative paints him to be?
No, no.
Not even close.
He doesn't have a halo, either.
He doesn't have a halo, either.
He wasn't a great freedom fighter in any sense.
He was definitely an authoritarian.
At the same time, you have to understand, it's the Middle East.
People that aren't authoritarian don't make it very far, and if you were to compare him
to other leaders in the Middle East, this dude is Bernie Sanders liberal.
He is not the evil dictator that he's being painted to be, but he's not this great guy
There's that middle ground, and we have to be realistic about that.
The other thing that needs to be shown is that a lot of the accusations against him
and his forces, they've been proven false.
But there's enough of them that are true to understand that he isn't this great benevolent
ruler.
and that's the problem that she seems to be having is showing that she completely
opposes this intervention that we're engaged in and this revolution and this
conflict that we created but she's not showing that she stands against some of
the things that Assad has done. She's not showing that clearly. She's made some
some comments, but not enough to belay that criticism of her.
But the important thing here is that we have to start looking at where these candidates
are coming from.
If we don't do that, we're never going to understand what they're going to do in office
because what they say means nothing.
It really doesn't.
Where they come from and their past actions do.
Now by and large, with the exception of her stances concerning Israel and Palestine, her
positions are pretty nuanced.
She needs to answer for that, and there are a lot of people that want to take her to task
for it, which is fine.
That particular stance of hers needs some refinement, no doubt.
But overall on the foreign policy aspect, her stances are nuanced to the level that
shows that she's going to listen to the advisors.
She's going to take into account everything that's going on in these countries.
She may not always be right, but she's going to at least listen to the people that know
what they're talking about, which is more than we have right now.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}