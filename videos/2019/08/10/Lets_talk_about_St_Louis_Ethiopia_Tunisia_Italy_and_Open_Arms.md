---
title: Let's talk about St. Louis, Ethiopia, Tunisia, Italy, and Open Arms
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=SF_F-d7p-a4) |
| Published | 2019/08/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the importance of understanding historical context to comprehend current events.
- Recounts the tragic story of the St. Louis ship during WWII carrying Jewish refugees rejected by multiple countries.
- Draws parallels to a modern-day situation involving the ship "Open Arms" with refugees off the Italian coast.
- Notes the refusal of the Italian government and other European countries to allow the refugees ashore.
- Links the desire of refugees to reach Italy to historical ties, such as Ethiopia's past as an Italian colony.
- Mentions the long-standing relationship between Tunisia and Italy, despite Tunisia not being an official Italian colony.
- Comments on the impact of extreme nationalism leading to the current plight of refugees.
- Suggests a solution by proposing a strategy to address attacks by welcoming refugees, contrasting it with the current stance.
- Warns of the lasting effects and backlash of current actions on children and citizens.
- Concludes with a reflection on the repercussions of present actions influencing the future.

### Quotes

- "Everything that happens changes what happens tomorrow."
- "The scars that we're creating today, they're gonna have backlash too."
- "You know, if Trump was smart, he could stop these attacks overnight."
- "That relationship between the people of the area that is now Tunisia and the area that is now Italy has been around since Italy was known as Rome."
- "It's estimated that about a quarter of them got caught and sent to death camps where they died."

### Oneliner

Beau explains the significance of historical context in understanding current events, reflecting on the rejection of refugees reminiscent of past tragedies and proposing a compassionate solution to address the crisis.

### Audience

Global citizens, activists

### On-the-ground actions from transcript

- Welcome refugees in your community (implied)

### Whats missing in summary

The full transcript provides a deep dive into historical context shaping current events, urging for empathy and action to address the refugee crisis and avoid repeating past mistakes.

### Tags

#History #Refugees #Italy #Mediterranean #Nationalism


## Transcript
Well, howdy there Internet people, it's Bo again.
So tonight we're going to talk about the scars of history.
There's a lot of times people look around
and look at a situation and be like, why is this happening?
Because they don't have the context.
It becomes really easy to understand
what's happening today and tomorrow if you
know what happened yesterday.
There's a situation that's unfolding
fits this in the Mediterranean right now. I think most people know the story of
the St. Louis, especially the people watching this channel. That's the name of
the ship that left Europe during World War II carrying 900 Jewish refugees.
First they went to Cuba and Cuba said no. Then they came to the United States, the
U.S. said no. Went to Canada, Canada said no. Nobody would take them. So the
The captain takes them back across Europe and drops them off in different countries.
It's estimated that about a quarter of them got caught and sent to death camps where they
died.
That's the story of the St. Louis.
Today it will be the ninth day by the time you guys watch this.
There's a ship that has been sitting idle for nine days off the Italian coast.
It's called the open arms, or open arms.
It's got 120-something refugees on it from primarily Ethiopia and Tunisia.
The Italian government won't let them in.
Neither will anybody else in Europe, apparently.
And a lot of people are asking, well, why do they want to go to Italy?
Scars of history.
Ethiopia was an Italian colony, so they're looking to Ethiopia for help, it's kind of
why the people from Honduras and El Salvador are coming here, they know us, there's that
relationship, we were down in their country, so they know us.
Tunisia is a little bit of a different story because it was never officially a colony of
Italy.
Tectorate, I think, during World War II for a year or two, but the only reason it wasn't
a colony was because it had been a colony in practice for so long that once colonization
started the Italians didn't think they needed to do it, and they got really mad when the
French did it.
In fact, that was one of Mussolini's goals during World War II.
That's why they're going there.
That's why they're looking to Italy for help.
That relationship has been around its transcended governments, types of governments.
That relationship between the people of the area that is now Tunisia and the area that
is now Italy has been around since Italy was known as Rome.
It's been around a really, really long time.
But now, because this extreme form of nationalism has reared its ugly head again, they're sitting
there off the coast.
The captain is being threatened.
It's insane.
It is insane.
You know, if Trump was smart, he could stop these attacks overnight.
Bring them here.
just go ahead and say for every person killed in one of these attacks well
we're gonna let in five refugees they would stop overnight they would stop
overnight and he's not gonna do that because I mean give a thumbs up in a
picture right the scars that we're creating today they're gonna have
backlash too. The children that are separated from their parents, they're
gonna remember it. They're gonna remember it. The American citizens that are growing up
right now and hearing these stories from their classmates and neighbors, they're
You're going to remember it.
Everything that happens changes what happens tomorrow.
way. It's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}