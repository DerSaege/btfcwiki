---
title: 'Let''s talk about #ThankACriminalDay and #FreeRoss....'
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=tvg2C2yZN-w) |
| Published | 2019/08/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Celebrates August 22nd, Criminal Day, created by Brian to honor criminals who have made a positive impact.
- Questions the notion that soldiers protect freedom, arguing that criminals actually obtain it for us.
- Mentions heroes like Martin Luther King Jr., whose actions were considered criminal at the time.
- Talks about Ross Ulbricht, the creator of Silk Road, an online platform for drug sales, and his controversial case.
- Points out that Ross Ulbricht received two life sentences plus forty years for a non-violent offense.
- Raises the question of whether prison is about rehabilitation or punishment in Ross's case.
- Describes Ross Ulbricht as a nerdy Eagle Scout who believed in the freedom for individuals to make their own choices.
- Emphasizes that Ross is not a danger to society and does not deserve his harsh sentence.
- Encourages viewers to visit freeross.org to learn more about Ross's case and support the petition for his clemency.
- Argues that if there is no victim, there is no crime, and advocates for a reevaluation of the justice system.

### Quotes

- "If there is no victim, there is no crime."
- "Life in the federal system means life."
- "That guy paid the price. The problem is, the price we're asking him to pay is too high."
- "Criminals have changed the world and they will continue to."
- "He won't get out. That's a little much for a website."

### Oneliner

Beau questions the criminal justice system's treatment of non-violent offenders and advocates for Ross Ulbricht's clemency, challenging the narrative around criminals on Criminal Day.

### Audience

Justice Reform Advocates

### On-the-ground actions from transcript

- Visit freeross.org to learn more and support the petition for Ross Ulbricht's clemency (suggested).
- Share the video with the hashtags #ThankCriminalDay and #FreeRoss (suggested).

### Whats missing in summary

The emotional impact of Ross Ulbricht's case and the broader implications for criminal justice reform.

### Tags

#JusticeReform #FreeRoss #CriminalDay #NonViolentOffenders #Injustice


## Transcript
Well howdy there internet people, it's Bo again.
Hey, he's wearing a different hat.
I am, and one of you is gonna get it.
Today is one of my favorite holidays.
Today is, think, a criminal day.
August 22nd.
It's the brainchild of a guy named Brian.
And Brian's thesis was that sure, maybe soldiers protect freedom, but it's criminals that obtain
it for you.
And that is a bold claim, but is it wrong?
Is it wrong?
Think about your heroes.
I'm willing to bet that a majority of them were criminals, heretics, depending on how
far back you go.
In the United States, Martin Luther King is more or less a universal hero.
You know when people ask me what they need to read to get him, I say letters from Birmingham
jail, yeah, he had a mug shot.
That doesn't do it for you, read the Declaration of Independence.
Every signature at the bottom of that thing belonged to a criminal.
Every one of them.
If you treat your kid's seizures with a substance that used to be illegal, you have a criminal
to thank for that, for not giving up the fight.
A very wise person once said that to change society, you have to change the law.
I disagree, I disagree.
To change society, you have to change thought.
the law will follow.
Now this holiday obviously doesn't mean that all criminals are good, but there's an underwriting
idea that if there's no victim, there's not really a crime.
And those are the criminals that are remembered on this day.
In that vein, I want to talk about a guy.
aren't going to know his name so before I talk about him I want to talk about
some of his supporters. Jesse Ventura, John McAfee, Jeffrey Tucker these are all
people that have spoken out on his behalf and yeah that's all one kind of
guy they all kind of line up a little bit.
Noam Chomsky, Neil Franklin, Keanu Reeves, we'll throw in a celebrity.
The guy's name is Ross Ulbrich, you probably don't know that name, probably doesn't ring
a bell, but his website will, Silk Road, you've probably heard of that, you've probably heard
all kinds of things about that. Most of them aren't true. But what was Silk Road? Well,
it turned into eBay for drugs. I mean, that's really what it turned into. But what was it
supposed to be? In Ross's own words, it was supposed to be about giving people the freedom
to make their own choices, to pursue their own happiness, however they individually saw
all fit. Consenting adults can do whatever. No victim, no crime. That's what it was supposed
to be. A little idealistic. He called himself naive later. What did he get for his efforts?
Two life sentences plus forty years for a first time non-violent offender, it's insane.
It is insane.
Brings up the question, is prison about rehabilitation or punishment?
If it's about punishment, we've got to let him out because the punishment does not fit
the crime.
If it's about rehabilitation, we need to take some other things into consideration.
I never met the guy.
I know some of his friends.
And I mean no offense when I say this, but I've read a lot of the things that he's written
and said.
He strikes me as a nerdy Eagle Scout that would build a website based on an idea.
That's how he strikes me.
Now he is staring down the barrel of two life sentences plus 40 years in a federal prison.
He's the very definition of scared straight.
This guy is not going to re-offend.
He's not a danger to society.
He never was.
He was a non-violent offender.
When people talk about the case and they say, Free Ross, they talk about the case, they
don't talk about him.
They talk about the case because it's interesting.
Let's just say there were some irregularities in it.
His sentence heavily relied on things that he was charged with but never convicted of.
And then after his sentence, well, those charges were dismissed with prejudice.
Some of the agents on his case wound up going to prison.
I don't have to go through all of this because you guys are you guys.
you're gonna go over to freeross.org
and you're gonna read about it
because there's a whole lot more crazy stuff than that.
It's an interesting read.
It is, but it's not relevant to think a criminal day.
Not really, what is is that when we finally accept the idea
that if there is no victim, there is no crime,
When that becomes enshrined in our thought,
and we understand that people can just be left alone,
we're going to have to look back at a kid who
built a website at 26 years old who wound up getting two life
sentences plus 40 years for an idea.
We're going to have to recognize him
as one of those thought changers,
is one of those people who made this possible,
who made that idea come forward.
He's gonna be one of those people that we look back at
and say, that guy paid the price.
The problem is, the price we're asking him to pay
is too high.
It is too high.
Life in the federal system means life.
It's not like the state system where you get a life sentence
and you can get out.
There's no number year that coincides with that.
Life means life.
There's a petition over on freeross.org, a little red bar
up at the top.
I want to say right now it's at 195,000, something like that.
They're trying to get to 200,000.
If he isn't granted clemency, he will die in a cage.
He won't get out.
He won't get out.
That's a little much for a website.
That's a little much.
If you're going to share this, share this video, share it with the hashtag, thank criminal
day and hashtag free Ross. If you decide to crack open a beer and celebrate, thank
a criminal day, remember if you're in the US you got a criminal to thank for the
fact that you can drink a beer. Criminals have changed the world and they will
continue to. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}