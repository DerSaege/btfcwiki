---
title: Let's talk about a joke from the President....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=sE336w15T_M) |
| Published | 2019/08/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing a joke made by the president and the gravity behind it.
- Exploring the context in which the president's statement was made.
- Examining the president's history of questionable actions and why his statement cannot be taken lightly.
- Comparing the president's statement to inducements to commit crimes in other scenarios.
- Addressing the danger of the president undermining the rule of law and placing his administration above it.
- Expressing concerns about the impact on federal agencies and the dangerous implications of unchecked power.
- Warning individuals in the administration about the potential consequences when the administration ends.
- Emphasizing the lack of protection for those not in the limelight within the administration.
- Concluding with a reflection on the dangerous nature of the situation and its implications.

### Quotes

- "Don't worry, I'll pardon you."
- "It's not a joke, it's an inducement to commit a crime, it's an overt act in furtherance of a criminal conspiracy."
- "Man, that kind of power is intoxicating, and it will be used and abused."
- "When it's over, you're the one that doesn't have a chair."
- "Anyway, it's just a thought."

### Oneliner

Beau delves into the dangerous implications of the president's "joke" and its undermining of the rule of law, warning of unchecked power and potential repercussions for those in his administration.

### Audience

Policy advocates

### On-the-ground actions from transcript

- Contact policymakers to demand accountability for actions that undermine the rule of law (implied)
- Stay informed on political developments and hold leaders accountable (implied)

### Whats missing in summary

In-depth analysis of the potential long-term consequences of unchecked power and the erosion of the rule of law.

### Tags

#RuleOfLaw #UncheckedPower #PoliticalAnalysis #Accountability #InducementToCrime


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we've got to talk about a joke, and when a joke isn't a joke, when it's coming
from the president.
Don't worry, I'll pardon you.
It's a scary phrase.
That is a scary phrase.
Now even his supporters, they're not saying that this wasn't said.
They're saying that it was just a joke.
Just a joke.
Of course.
Of course it was just a joke.
In what context?
Because I've been sitting here thinking, trying to come up with a scenario in which the President
of the United States would say that, that wasn't immediately preceded by him advocating
something that was illegal, or at least so ambiguous in nature that his aides were worried
they'd get indicted.
So maybe it's not illegal, but it's right on the line, it's probably unethical to say
the very least. It's the only time I can see it being said as a joke. Let's take the other
stuff in the conversation and look at that real quick. Take the land. Just take it. Okay.
He has a history of using eminent domain as a real estate developer. He has a history
of using this. Skirting environmental regulations. Everything else in the conversation, it's
not a joke. It's not a joke. He has a track record of it.
How can we assume that don't worry, I'll pardon you is a joke?
That's not the logical conclusion in any way, shape, or form.
It's not.
He has a history of doing all of this stuff.
Let's place this in another context.
Let's say a governor says, hey, go kill this guy, I'll pardon you.
What about a pharmaceutical company talking to a doctor?
Maybe those research results need to be a little bit different.
Don't worry, we'll pay your lawyers and we've got a real cush job for you.
Hey, I just need you to move this product for me, a lot of money in it for you.
It's not a joke, it's an inducement to commit a crime, it's an overt act in furtherance
of a criminal conspiracy.
pay your lawyers. He said that too before, hasn't he?" See, this is a very dangerous joke.
This is the president undermining the rule of law. And I know we're saying that's nothing new
coming from this administration. He has subverted the Constitution. He's undermined a lot of the law.
That's not a new thing, but this is different because it's the entire law, all of it.
It places agents of the Trump regime above the law for anything as long as they're doing
what he says.
Democrats up to this point have been very reluctant to impeach for political reasons
and I understand that.
I understand the optics involved.
I agreed for a different reason.
I felt that it was important to the demographics that Trump attacked directly.
I felt it was important for them to see him just crushed at the polls, so they
would know that that's not really what the average American wants, that those
people that voted for him the first time, some of them just fell for stupid
rhetoric, and some of them just voted because they didn't like Hillary, but they didn't
really want that.
I thought that would be really good for reuniting the country.
We may be beyond that now.
We may be beyond that because the version of the rule of law at this level, man it's
dangerous.
is dangerous because it is across the board and it trickles down in thought.
If his aides think this, their aides think it, and it goes down through
all of these federal agencies.
And then all of the things that Trump says, just as a joke, well, they become
de facto policy, because the message here is, do what I say, you'll keep your job, you
won't be punished, and nobody can stop you.
Man, that kind of power is intoxicating, and it will be used and abused.
Don't worry, I'll pardon you.
It's said before the crime is committed.
It's very, very dangerous.
And to those people in his administration,
and in these agencies,
I've played this version of musical chairs before.
And I'll just tell you right now,
if you're not a headline name,
if you're not a person that's in the news already,
when the music stops,
and this administration will stop,
When it's over, you're the one that doesn't have a chair.
You're not going to get pardoned.
You won't.
They will.
All those people asking you to do stuff, they'll get pardoned.
You, nah.
Anyway, it's just a thought.
have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}