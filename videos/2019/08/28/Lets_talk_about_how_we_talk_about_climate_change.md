---
title: Let's talk about how we talk about climate change....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=OrwIfQgxuao) |
| Published | 2019/08/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Climate change discourse is hindered by how it's framed and discussed, with media simplifying complex issues into sound bites.
- The 12-year timeline to combat climate change was derived from the need to reach net zero CO2 emissions by 2050, requiring emissions to decrease significantly by 2030.
- The urgency is not about a doomsday scenario in 2030 but about avoiding very negative outcomes by 2050.
- Climate change action is essentially an infrastructure project aimed at transitioning to clean energy, similar to past massive infrastructure projects like indoor plumbing or the internet.
- Failing to address climate change could lead to catastrophic consequences like irreversible melting of ice caps and extreme environmental disruptions.
- Trump's economic policies could inadvertently provide an ideal backdrop for implementing climate change initiatives as an economic stimulus or jobs program during a potential recession.
- The focus should be on utilizing the economic downturn as an opportune moment to push forward climate change actions, akin to the New Deal era.
- The urgency of addressing climate change cannot be understated, requiring a shift towards decisive actions rather than debating the validity of climate science.

### Quotes

- "We are running out of time to sit there and act as if all ideas are equal when it comes to the debate over climate change."
- "It's an infrastructure project to bring about clean energy. That's what it is."
- "When the recession hits, this is your economic stimulus package."
- "The urgency is not about a doomsday scenario in 2030 but about avoiding very, very bad outcomes."
- "We know what needs to be done; we just need to be ready for it."

### Oneliner

Beau stresses the urgency of framing climate change as an infrastructure project and seizing the economic stimulus potential during a recession to drive clean energy initiatives forward.

### Audience

Climate activists, policymakers, concerned citizens

### On-the-ground actions from transcript

- Prepare for and support climate-friendly policies during economic downturns (implied)
- Advocate for viewing climate change action as an infrastructure project (implied)
- Educate communities on the importance of transitioning to clean energy (implied)

### Whats missing in summary

The full transcript provides further details on the importance of framing climate change discourse accurately and leveraging economic challenges as opportunities for climate action.

### Tags

#ClimateChange #MediaFraming #InfrastructureProject #EconomicStimulus #CleanEnergy


## Transcript
Well, howdy there, internet people, it's Bo again.
Tonight, we're going to talk about how we talk about climate
change, because a large part of the problem,
and I think one of the reasons nothing's being done,
is because how we frame it, how we discuss it.
And it starts with the media.
The media, they get some scientists
and they interview them.
And then they take a sound bite, and they
put that sound bite out there.
And so people get the idea that we have 12 years
to do something.
What does that even mean?
Seriously.
In 12 years, does the world end?
Do we get flooded and microwaved?
What happens?
It's a complex subject, and it's not something
that can be done in sound bites.
That's the problem.
But Americans love sound bites.
They like things to be simple, clean, neat.
They like a plot they can follow.
And they want a magic bullet.
OK, so we've got 12 years to do something.
Well, it's really 11 now.
But this is how that number came about.
In 2018, the Intergovernmental Panel on Climate Change
at. And they set out to decide and come up with ways to stop the world's global
temperature, average temperature, from rising 1.5 degrees Celsius. They came up
with a timetable to get to net zero, no CO2 emissions, by 2050. And in order to do
that we have to be on a path to get there by 2030, that was 12 years at the
time, and we need to basically have emissions fall by about 45% by then. So
we could get to zero by 2050. 2050 is actually the important date, kind of. To
To put this in terms everybody's going to understand, the emissions are an 18-wheeler
that's driving down the road.
It's currently accelerating.
We have to start to slow down now.
So by the time we get to exit 2050, we're at zero because an 18-wheeler doesn't stop
on a dime and neither do carbon emissions.
Okay, so, is there Doomsday 2030?
No, no, things just start to get worse.
Things start to get worse, but the media portrays it as Doomsday rather than working towards
a goal we have to meet by 2050 to avoid very, very bad outcomes.
That would be the rational way to explain it.
we don't live in a rational world. When you tell people this they say well we've
heard this before we've heard projections about this or that or
whatever. Yeah and we did stuff. That's the real reason your gas mileage
improved. That's why you have countries using cleaner energies now. That's why
your car is really made out of plastic to lighten it, reduce emissions. There's a
whole bunch of things that have been done that's why the date keeps getting
moved back. Now as far as deadlines we're past some of them. We're already past
some of them and we're going to see bad outcomes from it. We're trying to
mitigate. So what does it take to mitigate? We can frame it as a giant
environmental effort. Nobody's going to care. Nobody's going to care. What is it at its core?
It's an infrastructure project. It is an infrastructure project to bring about clean energy.
It's what it is. We've had massive infrastructure projects before.
Indoor plumbing, getting ready for the automobile and paved roads,
The internet. All of this stuff are infrastructure projects. That's what it is.
That's what it is. Now if we don't do it, what do we end up with? The Antarctic
and Greenland, their ice sheets are probably already past the tipping point
point, at least to some degree.
If we lose all of the ice caps, we could get something called
a blue ocean event.
And that's just all bad.
Things go real bad from there.
We're talking like doomsday scenarios, Michael Bay movie
type things.
More immediately, habitats change.
The heat becomes unbearable.
People can't live in certain areas.
We get more refugees, rising sea levels, stuff like this.
That's what we're trying to prevent.
That's the negative outcome if we don't engage in an infrastructure project.
And that's how we have to frame it.
And the good news is that Trump gave us the perfect way to frame it and
the perfect way to get it done.
We are probably headed into a recession. The inverted yields that we talked about
the other day, those are bigger than they've been at any point since 2007
before the last recession. So that's probably going to happen and when it
does, we're not doing an environmental effort. We've got an economic stimulus
a jobs program. Call it whatever you want. But that's going to be the perfect time to do it.
That is going to be the perfect time to do it. A lot like the New Deal. A lot like the New Deal.
deal. The clean energy and the change in environment, well that's just a bonus. That's just a bonus.
It's not why we're doing it. We're doing it to help the economy. People care about
that, as sad as that is. So we've been handed a golden opportunity by the President of the
United States. A, he has taught us that the average American, especially those who
are still denying climate change or just don't want to look into it enough, they
don't look into anything. You can call this program whatever you want. They're
They're not going to look into it, they don't care, they don't care.
And the recession gave, it's going to give us the perfect cover to do it.
This is how it's going to have to be done.
We are running out of time to sit there and act as if all ideas are equal when it comes
to the debate over climate change, something else the media does, because they try to frame
it as if there's a discussion there's not the science on this stuff oh it's
real clear it is real clear and it's real simple so we have the opportunity
we know what needs to be done we just need to be ready for it and the next
administration whichever it is needs to be prepared to seize upon that moment
When the recession hits, this is your economic stimulus package.
This is how you're going to put people back to work.
And that is going to be an easier sell than trying to get people to read.
to read. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}