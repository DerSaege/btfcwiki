---
title: Let's talk about the tolerant left....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=yQFY8iosTno) |
| Published | 2019/08/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the lack of liberal armed groups, attributing it to most liberals being anti-gun.
- Contrasts right-wing militias, characterized by conventional tactics and structure, with leftists who don't style themselves as militias.
- Mentions armed leftist groups like the John Brown Gun Club and Redneck Revolt that don't have traditional militia setups.
- Talks about the historical challenges faced by armed leftists in the US, with FBI scrutiny in the past.
- Describes the security consciousness of armed leftist groups, mentioning a unique vetting process involving friends.
- Notes the shift in security culture and how some groups now ask more probing questions, leading to hesitation from potential members.
- Points out that armed leftists train in guerrilla-style operations rather than conventional tactics.
- States that firearms are seen as a tool by the left, unlike a status symbol on the right.
- Emphasizes the importance of keeping things peaceful and discourages violence in any form.
- Encourages responsible gun ownership for defense or recreational purposes but warns against offensive uses.

### Quotes

- "Ideas travel faster than bullets."
- "We need to keep this peaceful as much as humanly possible."
- "The idea of sparking that kind of conflict, it's never good."

### Oneliner

Beau explains the differences between left and right-wing armed groups, touches on historical challenges, and stresses the importance of peaceful approaches over violence.

### Audience

Activists, Gun Owners

### On-the-ground actions from transcript

- Join or support organizations like the John Brown Gun Club or Redneck Revolt (exemplified)

### Whats missing in summary

The full transcript provides a nuanced exploration of the historical context and current challenges faced by armed leftist groups in the US.

### Tags

#ArmedGroups #Leftist #GunOwnership #PeacefulAction #Community


## Transcript
Well, howdy there, internet people.
It's Bo again.
So tonight we're going to talk about everybody's favorite
subject, the tolerant left, at least the image of it.
I got a question asked of me over Twitter, and it's a great
jumping off point for this topic.
We have to adjust some terms, but it's a great
jumping off point.
The question was, why are there so few liberal or leftist militias?
So to adjust the terms, there's not a lot of liberal armed groups because most liberals
are anti-gun.
Not all, but most.
The term militia has a certain connotation, and that is a paramilitary organization.
This is a group that has adopted uniformed rank.
They have squads, platoons, companies, defined areas of operation, and they typically train
in conventional tactics.
In fact, if you're interested in this topic, news to share, just embedded with a right-wing
militia group during their annual training, and you can watch them train, and there's
a lot to critique.
But regardless of how you feel about how they did during their training, you can't dispute
that it was conventional, it was all conventional tactics.
Now when you get to leftists, actual leftists, and when we say leftists during this conversation,
we do not mean the American left, Democrats, it's not what we're talking about, we're
talking about actual leftists.
There are armed groups, there are.
They just don't style themselves as militias.
They don't have rank, uniforms, that type of thing.
John Brown Gun Club and Redneck Revolt are the two that are open and public and trying
to get publicity and hoping to get new members and expand.
Now they're the exception though because historically in the United States, being an armed leftist
didn't go well, it's become more acceptable to be a leftist and be armed. But there was
a time when that would get the FBI knocking on your door. Because of that history, armed
leftist groups are very security conscious for the most part. That's falling away today
slowly but surely, and there's a lot of growing pains with this. But they used to exercise
a lot of real tradecraft to find out who you were and make sure that you were who you said
you were before they let you in their little circle.
They would ask the applicant for the names of five of their friends.
And then they would go to those five individuals and ask each one of those five individuals
for the names of five more of their friends.
And those were the people they would actually try to talk to about you.
they know that that first group of five, they know they're going to vouch for you because
well, I mean, you gave them the names.
So they would just skip right over them.
It was a unique way of checking somebody out before the dawn of social media.
Now that security culture though, it still remains in a lot of ways.
And as it breaks down, there are people that go the other way with it, and it actually
becomes a little scary when they start asking questions.
Instead of asking, like, who are your friends, they're like, well, where have you been?
And what happens is, because the left is security conscious, when people start asking that question,
they back off immediately.
they don't want to join the group, which makes sense I guess.
So the end result of all of this is that you still have these small unnamed groups of friends
or acquaintances that go shooting together and they may actually train and when they
do train, they don't train in conventional tactics, they train in unconventional tactics,
trained in guerrilla style guerrilla style of operations. So that that's where
we're at but the reason it appears that there aren't many armed leftists out
there is because the left still tends to understand that a firearm is a tool. On
the right it's a status symbol so you get those pictures with the guys with
their rifles like this. On the left you don't have that outside of like the
tankies, but it's become a status symbol, a piece of the identity on the right.
On the left, it's not your lifestyle, it's just something you do on the
weekends. Now I've seen these questions pop up more and more and more often
about firearms and about forming armed groups, I just want to say, I don't own a gun and
ideas travel faster than bullets.
We need to keep it, we need to keep this peaceful as much as humanly possible.
Going violent in anything like this, that's never a good idea.
Now if you're just talking about you actually want to go shoot and go have fun, that kind
of thing, fine, more power to you, good for you.
If you're doing it for defense and you're worried about a natural disaster or something
like that, more power to you.
If you're looking at this as an offensive thing, you need to rethink, you need to stop,
And you need to seriously consider your actions.
The idea of sparking that kind of conflict, it's never good.
It's never good.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}