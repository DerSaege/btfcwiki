---
title: Let's talk about the American Farmer, Iowa, and China....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=TskYfNBE8go) |
| Published | 2019/08/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the impact of the trade war on American farmers, particularly with China no longer buying agricultural products.
- China bought $19.5 billion worth of goods from the U.S. in 2017, dropping to $9.2 billion in 2018, and now it's zero.
- Points out that food is a necessity, unlike luxury items like iPads, meaning the effects will be felt more intensely.
- Mentions deals between Brazil and China for soybeans, which used to make up 60% of U.S. soybean exports.
- Talks about the long-lasting effects of the trade war even after it ends, as American farmers will need to compete to regain lost business.
- Notes that subsidies for farmers might not be a preferred solution, as farmers want to rely on business rather than government aid.
- Predicts a significant impact on Trump in the 2020 elections due to economic repercussions felt by states like Iowa, known for soybean production.
- States that the trade war has resulted in $27 billion in extra costs for American taxpayers until June.
- Mentions potential store closings and job losses in companies like Joanne Fabrics and the American Textile Company due to increased prices and reduced demand.

### Quotes

- "They bought $19.5 billion worth. 2018, 9.2, a loss of $10 billion that was going to the American farmer."
- "It's going to have a long-lasting effect because food isn't like an iPad, you know?"
- "The problem with that is most farmers don't really want subsidies. They don't."
- "They know what their pocketbook looks like, and I think that's going to have a bigger impact than people are saying."
- "If it goes on much longer, they're looking at store closings and job eliminations."

### Oneliner

American farmers hit hard by China's halt in agricultural purchases, facing long-lasting consequences; trade war effects extend beyond economy to impact jobs and businesses.

### Audience

American citizens, farmers, policymakers

### On-the-ground actions from transcript

- Contact local representatives to advocate for fair trade policies that support American farmers (implied)
- Support local farmers' markets and businesses to help offset the impacts of trade wars (implied)

### Whats missing in summary

The full transcript provides detailed insights into the economic consequences of the trade war on American farmers and businesses. Viewing the complete video can offer a comprehensive understanding of the situation.

### Tags

#TradeWar #Agriculture #Economy #AmericanFarmers #ChinaTrade #PolicyImpact


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we're going to talk about the American pharma
and how they've got to be looking around right now.
It's saying that, well, it's getting
curiouser and curiouser.
So the trade war has been kind of overshadowed
by recent events.
But China is our third, fourth, or fifth largest
market for farm goods, depending on which economist you talk to.
They have said that they're going to no longer buy agricultural products.
That's huge.
How huge?
In 2017, before the tariffs took effect, they bought $19.5 billion worth.
2018, 9.2, a loss of $10 billion that was going to the American farmer.
Now that number is zero.
Now that number is zero.
That's huge.
And it's going to have a long-lasting effect because food isn't like an iPad, you know?
If you go to buy an iPad and it costs a little bit more, you wait a month.
You've got to buy food.
So today, already, there are deals that are in effect now between Brazil and China for
soybeans.
China bought 60% of U.S. soybean exports.
And now those exports are gone.
And these deals, they're not like for a week, they're for a year.
So once this trade war is over, even if it ends next month, the effects are going to
be felt for a long time.
And even once the trade war is over, now it's not a matter of we already have a deal with
these people, the American farmer has to compete to get that business back.
Business they had, and now it's gone.
zero. 19.5 billion to zero. Yeah there are subsidies that are going to help the
farmers. The problem with that is most farmers don't really want subsidies. They
don't. That's not how they want to live their life. Banks don't like subsidies.
They don't like to lend because they don't know if you're gonna get it again.
That's not how they like to do business. They want to see the books which means
It's going to be hard to get loans, which means John Deere is going to get hit.
This is going to have a big effect on Trump, I think, in 2020.
Political analysts are like, well, you know, they have red hats.
They're stupid.
Yeah, well, they know what their pocketbook looks like, and I think that's going to have
a bigger impact than people are saying.
A state like Iowa, who goes blue a lot.
pointed by a pretty huge margin in 2016. Iowa is soybean country and they have
John Deere factories. They're gonna get hit hard by this. Hard. And the tariffs
are taking their toll in other places. I guess from the start of the trade war up
until June it was 27 billion dollars in extra costs that the American taxpayer
wind up paying.
Joanne Fabrics says if it goes on much longer, they're looking at store closings and job
eliminations.
The CEO of the American Textile Company said that as prices go up and demand decreases,
it is a fact American jobs will be lost.
Anyway, if it's just a fault, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}