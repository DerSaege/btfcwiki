---
title: Let's talk about reputations and Chicago....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=VGX49N4I22w) |
| Published | 2019/08/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Shares a story about misconceptions based on reputation, using his friend's experience in Chicago as an example.
- Recalls a personal experience from high school where a fight led to him gaining a reputation that wasn't entirely deserved.
- Examines murder rates in various cities and states, comparing them to challenge the reputation of Chicago as a crime-ridden city.
- Points out that Chicago doesn't have one of the highest murder rates when compared to other cities and states.
- Mentions the role of propaganda in shaping false narratives about crime rates in Chicago, related to a former president and certain news networks.
- Concludes by urging caution when interpreting statistics and narratives, especially when they are manipulated to create fear.

### Quotes

- "Chicago is not Mogadishu."
- "Reputations are sometimes unearned."
- "Be very, very careful when you're looking at statistics and you're looking at narratives that are supposedly based on statistics."

### Oneliner

Beau challenges misconceptions about Chicago's crime reputation and warns against believing manipulated statistics and narratives.

### Audience

Community members, skeptics

### On-the-ground actions from transcript

- Challenge false narratives about communities (implied)
- Verify statistics and information before forming opinions (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of reputation, crime rates, and propaganda, offering a broader perspective on how perceptions can be skewed.

### Tags

#Reputation #Misconceptions #CrimeRates #Propaganda #CommunityAction


## Transcript
Well, howdy there, internet people, it's Bo again.
Today we're gonna talk about reputations.
Got a friend who, let's just say he's on vacation in Chicago.
And talked to him, he's like, man, this place is nothing like what I thought it was like.
What do you mean?
He's like, you know, you see the coverage of it, and I was expecting this place to be
pretty bad.
They're actually pretty welcoming.
I mean, they're rough neighborhoods, but even there, it's not.
bad as it seems on on the news. Fair enough. I haven't been to Chicago in a
number of years, but it got me thinking my reputations. When I was in high school,
very first year I went to high school, I went to a high school that was in a
relatively large city, had 4,000 people in it. That's bigger than the town that I
live in now. And you know the year after we wound up moving and I went back to
a high school that was so small it was a high school and a middle school but that
first year my freshman year there was this girl and she had just broken up
with her boyfriend and we went out and hung out not a romantic thing but
whatever he was an idiot him and two of his buddies caught me in a stairway now
the reality is they kicked the tar out of me but before they got me on the
the ground and started kicking me, I got a couple real good hits in on one of them.
Now a couple minutes into the melee, a coach walks in one flight down into the stairway.
They take off running, go one flight up.
I follow them because I don't want to get caught either.
They run out into the hallway, well people are changing classes.
What do the people in the hallway see?
They see three people run out of a stairway, one of them bloodied, and me follow out shortly.
after with blood on my hands. I got a reputation from that that was not entirely deserved.
It appeared that I just sent these guys running. That's not what happened.
Chicago seems to be in the same boat. You see it all the time. 51 killed, 51 shot, you
know, worst weekend ever. See it in the news. Let's look at murder rates. I'm going to
cities with the highest murder rates in order. East St. Louis, Chester
Pennsylvania, St. Louis, Missouri, Gary, Indiana, Baltimore, Maryland, Pine Bluff,
Arkansas, Wilmington, Delaware, New Orleans, Louisiana, Detroit, Michigan,
Baton Rouge, Louisiana, Flint, Michigan, Bessemer, Alabama, Petersburg, Virginia,
Jackson, Mississippi. They're not even in the top 15. Jackson, Mississippi,
Alexandria, Louisiana, Youngstown, Ohio, Huntington, West Virginia, Atlantic City.
It just goes on. Chicago's not even up here. If you look at murder rates...
I'm sure somebody right now is going,
well, it's the surrounding areas. It's not just Chicago.
Let's look at state murder rates. These aren't actually in any order. I just
I just pulled them to demonstrate the point.
Louisiana, 12.4, that is the highest.
Arkansas, 8.6, Missouri, 9.8, Alaska, 8.4,
Alabama, 8.3, Mississippi, 8.2.
You gotta go all the way down to 7.8
before you get to Illinois.
That's in line with South Carolina and Tennessee.
By the way, Florida is 5.0.
See, I can knock that Florida man stuff off.
Or maybe we're just better at hiding the body, I don't know.
By the way, Florida's murder capital is not Miami.
It's Riviera Beach.
That's like right by Mar-a-Lago.
I found that funny.
And I'm sure another response will be, well, you know,
you've got to compare just big cities.
It's 14th.
Cities over 100,000 people, it's 14th.
The reputation isn't well-earned, and in fact,
their crime rate is dropping.
They're trying to get their city back together.
May not help to have news outlets just running constant propaganda that is plainly false.
The reason this started is because we had a president from Chicago and the news network
that is constantly pushing this narrative, well they didn't like him.
That's where it came from.
Propaganda.
Two things we can learn from this.
Three things.
One, Chicago is not Mogadishu.
Two, reputations are sometimes unearned.
And three, be very, very careful when
you're looking at statistics and you're
looking at narratives that are supposedly based on statistics.
When you see things that aren't actually based on rate
but based on an amount, that's when
you need to start paying attention because it's a whole lot easier to scare people by
saying 51 people shot over this period of time than giving the rate in a city with almost
3 million people.
Anyway it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}