# All videos from August, 2019
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2019-08-31: Let's talk about the history of immigration laws in the US.... (<a href="https://youtube.com/watch?v=cKwYR_v5sLo">watch</a> || <a href="/videos/2019/08/31/Lets_talk_about_the_history_of_immigration_laws_in_the_US">transcript &amp; editable summary</a>)

Beau explains the history of US immigration laws to showcase their racist origins, calling for reform to eliminate caps and address systemic racism.

</summary>

"The law is bad. It's always been bad."
"All of them are racist. They are racist in origin."
"Any real immigration reform is going to have to get rid of these caps..."
"Because it was enacted because of racism."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains the history of major US immigration laws to address the question of why wanting people to follow laws can be seen as racist.
Points out that immigration laws prior to 1776 didn't apply since colonizers were not immigrants but rather replacing existing systems.
Mentions that from 1787 to about 1880, there were no federal immigration laws due to debates about federal government's power over immigration.
Notes the Chinese Exclusion Act of 1882 as the first law prohibiting entry based on nationality.
Talks about the Immigration Act of 1917, which expanded exclusion to Asian countries and included other restrictions like barring sex workers and mentally defective individuals.
Describes the Immigration Act of 1924, which introduced numerical caps, particularly affecting people from Asia.
Mentions the 1965 Immigration and Nationality Act, which extended caps to all countries and marked the first limitations on immigration from south of the border.
Suggests that illegal immigration as a concept emerged when caps were introduced, making it difficult for certain groups to enter legally.
States that immigration laws have always been racist in origin, leading to the need for amnesty in the 1980s.
Concludes that real immigration reform must eliminate these caps to address the racist elements embedded in the laws.

Actions:

for policy advocates, activists, educators,
Advocate for immigration reform by supporting the removal of numerical caps and addressing the racist elements in immigration laws (suggested).
Educate others on the racist origins of US immigration laws and the need for reform (exemplified).
</details>
<details>
<summary>
2019-08-30: Let's talk about why we celebrate Labor Day.... (<a href="https://youtube.com/watch?v=4aYjWs4I7Co">watch</a> || <a href="/videos/2019/08/30/Lets_talk_about_why_we_celebrate_Labor_Day">transcript &amp; editable summary</a>)

Labor Day carries dual meanings of celebrating labor and suppressing labor movements, rooted in historical events like the Haymarket riot and violent strike suppression.

</summary>

"This is a day to celebrate lefty ideas."
"Labor Day is historically linked to both celebrating labor and suppressing labor movements."

### AI summary (High error rate! Edit errors on video page)

Labor Day marks the official end of hot dog season and holds different meanings for different cultures.
Americans associate Labor Day with the fashion rule of not wearing white after.
Labor Day originated as a celebration of labor, union activity, and socialist ideals by blue-collar workers.
The first Labor Day parade was on September 5th, 1852 in New York organized by the Central Labor Union.
In the 1880s, organized labor intensified with events like the Haymarket riot, influencing the emergence of May Day.
In 1894, the Pullman Sleeping Car Company layoffs triggered a strike due to wage cuts without rent or price reductions in the company town.
President Grover Cleveland, appearing pro-labor, enacted Labor Day legislation before sending federal troops to crush the strike violently.
Labor Day was designed as a distraction from the strike's violent suppression.
Labor Day is historically linked to both celebrating labor and suppressing labor movements.
Beau warns of the dangers during Labor Day weekend, urging caution and awareness of its risks.

Actions:

for history enthusiasts,
Celebrate labor and socialist ideals within your community (implied)
Educate others on the origins of Labor Day and its historical significance (implied)
Support local labor movements and initiatives (implied)
</details>
<details>
<summary>
2019-08-30: Let's talk about Martin County High School again.... (<a href="https://youtube.com/watch?v=LfrL7y63YiU">watch</a> || <a href="/videos/2019/08/30/Lets_talk_about_Martin_County_High_School_again">transcript &amp; editable summary</a>)

Students at Martin County High School face bullying daily, seeking support and tolerance from an administration that fails to take action.

</summary>

"We just want tolerance because we tolerate everyone around us and we deserve that too."
"Students are asking for your help, too."
"You don't need help, y'all need backup, that's it, and you've got it."

### AI summary (High error rate! Edit errors on video page)

Students at Martin County High School in Kentucky wore pro-LGBTQ shirts to support a fellow student and were made to change by the administration.
The administration expressed concern about bullying, but the students had already decided to wear the shirts again as a form of solidarity.
Despite facing bullying every day, the administration fails to take action to address the issue.
A hit list circulated at the school, causing students to stay home, only to find the administrator and police officer joking about it upon their return.
The lack of seriousness and support from the administration is appalling to Beau, especially considering the safety and security of the students.
Beau questions the administration's ability to provide a safe environment for the students and offers the assistance of friends from Fort Campbell or Wright-Patt to step in.
The superintendent claimed a miscommunication had been resolved, but students were left in the dark without any communication from them.
Students are labeled as attention-seeking by faculty when they are actually seeking support and tolerance.
The students, despite facing cynicism and opposition, express their desire for acceptance and tolerance within the school.
Beau encourages the students to continue fighting for what they believe is right and assures them of his support and that of others watching.

Actions:

for students, lgbtq community, supporters,
Support the students at Martin County High School by wearing pride flags or showing solidarity in any way possible (implied)
Encourage tolerance and acceptance within your own community and schools (implied)
</details>
<details>
<summary>
2019-08-29: Let's talk about LGBTQ students at a Kentucky High School.... (<a href="https://youtube.com/watch?v=IGvyk3LxiHQ">watch</a> || <a href="/videos/2019/08/29/Lets_talk_about_LGBTQ_students_at_a_Kentucky_High_School">transcript &amp; editable summary</a>)

High school students forced to change for wearing LGBTQ clothing face silence instead of support, revealing a double standard in Kentucky's school system.

</summary>

"Just do whatever the mean man says. Don't make yourself a target. Hide who you are."
"Maybe it's been resolved. I don't know. But there's no quotes from the teens saying that it was. None."
"They won't have any of those mean and hurtful words on them, and they will be completely in line with Tinker versus Des Moines."
"My advice to the superintendent and the school administrator is to leave these kids alone."

### AI summary (High error rate! Edit errors on video page)

High school students at Martin County High School in Kentucky wore pro-LGBTQ clothing and were forced to change by the school administrator.
The school claimed they were worried about the students being bullied, but allowed students to wear Confederate flags.
The students felt like they were silenced while the school didn't address the bullies.
The administrator told the students that their clothing wasn't appropriate and that school wasn't the place to talk about sexual orientation.
Despite the superintendent claiming the issue was resolved as miscommunication, a student fears further disciplinary action.
Beau questions the lack of confirmation from the teens that the issue was truly resolved and urges them to speak up to prevent it from being ignored.
Teens have the right to free speech and expression according to the Supreme Court, as stated in Tinker versus Des Moines.
Beau encourages the superintendent and school administrator to leave the students alone.

Actions:

for students, educators, activists,
Contact the students to offer support and ensure their voices are heard (implied).
Advocate for inclusive policies and support systems within schools (implied).
Stand up against discrimination and bullying in educational environments (implied).
</details>
<details>
<summary>
2019-08-29: Let's talk about Hurricane Dorian vs Florida Man.... (<a href="https://youtube.com/watch?v=g-WqaIoTXN4">watch</a> || <a href="/videos/2019/08/29/Lets_talk_about_Hurricane_Dorian_vs_Florida_Man">transcript &amp; editable summary</a>)

Beau warns Floridians about Hurricane Dorian's potential impact, urges necessary precautions, and questions FEMA funding diversion amid immigration policies.

</summary>

"Get your old people out."
"Don't do anything stupid during the storm."
"Be prepared to be on your own for a little while."
"We know that 90 miles of ocean doesn't deter the drive for freedom."
"Because of our experiences here in Florida, we know that wall isn't going to work."

### AI summary (High error rate! Edit errors on video page)

Addressing Floridians about Hurricane Dorian approaching the state, specifically central Florida.
Warning about the potential impact of the hurricane, citing past surprises like Hurricane Michael.
Urging people to take necessary precautions and evacuate if needed, especially for the elderly.
Advising on essentials to have on hand: phone batteries, fuel for generators, food, water, first aid kit, and medications.
Cautioning against risky behavior during the storm, calling out Gainesville residents.
Noting the diversion of FEMA funds to immigration efforts, potentially affecting hurricane response.
Mentioning the historical delays in federal funding post-hurricane, especially in the panhandle.
Acknowledging Florida's preparedness due to previous experiences but warning of potential self-reliance needed.
Mentioning the state of emergency declaration and the impending presence of the Florida National Guard.
Connecting the lack of FEMA funding to immigration policies and questioning the effectiveness of a border wall.

Actions:

for floridians,
Evacuate if necessary and help others, especially the elderly, to do so (implied).
Stock up on essentials like phone batteries, fuel, food, water, first aid kit, and medications (implied).
Stay safe during the storm and avoid risky behavior (implied).
Be prepared to be self-reliant for a few days post-hurricane (implied).
Support local communities and organizations in hurricane preparation and response (implied).
</details>
<details>
<summary>
2019-08-28: Let's talk about how we talk about climate change.... (<a href="https://youtube.com/watch?v=OrwIfQgxuao">watch</a> || <a href="/videos/2019/08/28/Lets_talk_about_how_we_talk_about_climate_change">transcript &amp; editable summary</a>)

Beau stresses the urgency of framing climate change as an infrastructure project and seizing the economic stimulus potential during a recession to drive clean energy initiatives forward.

</summary>

"We are running out of time to sit there and act as if all ideas are equal when it comes to the debate over climate change."
"It's an infrastructure project to bring about clean energy. That's what it is."
"When the recession hits, this is your economic stimulus package."
"The urgency is not about a doomsday scenario in 2030 but about avoiding very, very bad outcomes."
"We know what needs to be done; we just need to be ready for it."

### AI summary (High error rate! Edit errors on video page)

Climate change discourse is hindered by how it's framed and discussed, with media simplifying complex issues into sound bites.
The 12-year timeline to combat climate change was derived from the need to reach net zero CO2 emissions by 2050, requiring emissions to decrease significantly by 2030.
The urgency is not about a doomsday scenario in 2030 but about avoiding very negative outcomes by 2050.
Climate change action is essentially an infrastructure project aimed at transitioning to clean energy, similar to past massive infrastructure projects like indoor plumbing or the internet.
Failing to address climate change could lead to catastrophic consequences like irreversible melting of ice caps and extreme environmental disruptions.
Trump's economic policies could inadvertently provide an ideal backdrop for implementing climate change initiatives as an economic stimulus or jobs program during a potential recession.
The focus should be on utilizing the economic downturn as an opportune moment to push forward climate change actions, akin to the New Deal era.
The urgency of addressing climate change cannot be understated, requiring a shift towards decisive actions rather than debating the validity of climate science.

Actions:

for climate activists, policymakers, concerned citizens,
Prepare for and support climate-friendly policies during economic downturns (implied)
Advocate for viewing climate change action as an infrastructure project (implied)
Educate communities on the importance of transitioning to clean energy (implied)
</details>
<details>
<summary>
2019-08-28: Let's talk about a joke from the President.... (<a href="https://youtube.com/watch?v=sE336w15T_M">watch</a> || <a href="/videos/2019/08/28/Lets_talk_about_a_joke_from_the_President">transcript &amp; editable summary</a>)

Beau delves into the dangerous implications of the president's "joke" and its undermining of the rule of law, warning of unchecked power and potential repercussions for those in his administration.

</summary>

"Don't worry, I'll pardon you."
"It's not a joke, it's an inducement to commit a crime, it's an overt act in furtherance of a criminal conspiracy."
"Man, that kind of power is intoxicating, and it will be used and abused."
"When it's over, you're the one that doesn't have a chair."
"Anyway, it's just a thought."

### AI summary (High error rate! Edit errors on video page)

Analyzing a joke made by the president and the gravity behind it.
Exploring the context in which the president's statement was made.
Examining the president's history of questionable actions and why his statement cannot be taken lightly.
Comparing the president's statement to inducements to commit crimes in other scenarios.
Addressing the danger of the president undermining the rule of law and placing his administration above it.
Expressing concerns about the impact on federal agencies and the dangerous implications of unchecked power.
Warning individuals in the administration about the potential consequences when the administration ends.
Emphasizing the lack of protection for those not in the limelight within the administration.
Concluding with a reflection on the dangerous nature of the situation and its implications.

Actions:

for policy advocates,
Contact policymakers to demand accountability for actions that undermine the rule of law (implied)
Stay informed on political developments and hold leaders accountable (implied)
</details>
<details>
<summary>
2019-08-27: Let's talk about the supporting the troops, the Hmong, and Iraq.... (<a href="https://youtube.com/watch?v=WjwASgtKRMk">watch</a> || <a href="/videos/2019/08/27/Lets_talk_about_the_supporting_the_troops_the_Hmong_and_Iraq">transcript &amp; editable summary</a>)

In modern warfare, supporting troops means prioritizing local communities, not slogans, while history repeats the betrayal of allies risking their lives.

</summary>

"The reality is, in modern war, your support doesn't matter."
"They are being left to die. That is what is going to happen to them."
"We learned nothing from Vietnam. We are making the same mistakes again."

### AI summary (High error rate! Edit errors on video page)

Exploring the concept of supporting the troops in modern times, beyond bumper stickers and slogans.
Emphasizing that supporting troops primarily means the local populace in areas of operations.
Providing a historical example of the Hmong people's betrayal after assisting the U.S. during the Vietnam War.
Criticizing the abandonment of Iraqi nationals who helped the U.S., facing visa backlogs and imminent danger.
Pointing out the plight of interpreters waiting for visas, with a drastic decline in approvals.
Expressing concern for individuals risking their lives and being left behind due to changing political tides.
Raising the issue of child soldiers and questioning the widespread nature of this practice.
Drawing parallels between past mistakes, like Vietnam, and the current abandonment of allies.

Actions:

for advocates for human rights,
Advocate for expedited visa processing for Iraqi nationals and interpreters (implied)
Support organizations aiding at-risk allies and interpreters (exemplified)
</details>
<details>
<summary>
2019-08-26: Let's talk about Episode 1: Education (<a href="https://youtube.com/watch?v=s-DdK7BqWa8">watch</a> || <a href="/videos/2019/08/26/Lets_talk_about_Episode_1_Education">transcript &amp; editable summary</a>)

Beau dives into the interconnected issues of underfunded schools, the value of education, and the stereotypes that discourage learning.

</summary>

"No teacher should have to appeal to the public for tools they need when they should be provided."
"Education is not its own reward, we focus on graduation, not the value of learning itself."
"No education is wasted, never stop learning, always stay curious."

### AI summary (High error rate! Edit errors on video page)

Teachers asking for help to clear their lists for supplies.
The President's idea of nuking a hurricane won't work due to heat energy.
The root cause of underfunded schools lies with those in power, not the uneducated.
Policy makers prioritize creating worker bees over critical thinkers.
Society values job skills over a comprehensive education.
Increasing funding for schools may improve outcomes, but it's more about valuing education.
Education is an ongoing, daily benefit, not just about graduation.
The impact of a great teacher surpasses that of a great school.
Education should focus on instilling a love for learning, not just obtaining a diploma.
Stereotypes discourage men and women from valuing education.
Real tough guys, like military leaders, prioritize education.
Education enriches lives and should be valued by all.

Actions:

for educators, policymakers, community members.,
Advocate for comprehensive education that includes arts, music, and humanities (implied).
Support educational causes like Pencils of Promise that build schools in underserved communities (implied).
Challenge stereotypes and encourage a love for learning in both men and women (implied).
</details>
<details>
<summary>
2019-08-25: Let's talk about trade, taxes, emergencies, and power.... (<a href="https://youtube.com/watch?v=OddtcH2rhoY">watch</a> || <a href="/videos/2019/08/25/Lets_talk_about_trade_taxes_emergencies_and_power">transcript &amp; editable summary</a>)

Beau warns about unchecked presidential power leading to potential dictatorship and advocates for limiting executive authority to safeguard democracy and prevent economic devastation.

</summary>

"President regrets not raising tariffs higher, implying he wishes to tax Americans more."
"He can destroy the economy with a pen stroke. He shouldn't, but he could."
"We're not living in a representative democracy because we subverted it."

### AI summary (High error rate! Edit errors on video page)

President regrets not raising tariffs higher, implying he wishes to tax Americans more.
Americans misunderstand tariffs; President taxes American importers on Chinese goods, passing the cost to consumers.
President has authority, under the International Emergency Economic Powers Act, to end trade with China.
This authority grants the presidency immense power, designed for catastrophic scenarios like nuclear war.
Beau believes the presidency has too much power, risking a dictatorship where a pen stroke alters lives.
Advocates for limiting presidential power to uphold democracy and prevent potential abuse by future leaders.
Urges rescinding executive powers to prevent authoritarian behavior in the future.
Warns that without checks on executive power, a leader like Trump could severely impact millions without resistance.
Acknowledges the president's authority to devastate the economy with a single decision.
Calls for action to address the excessive powers vested in the presidency for safeguarding democracy.

Actions:

for american citizens,
Advocate for legislative measures to limit presidential powers (implied)
Educate others on the implications of unchecked executive authority (implied)
</details>
<details>
<summary>
2019-08-25: Let's talk about Cherokee people and the House.... (<a href="https://youtube.com/watch?v=5bdWiqybsUg">watch</a> || <a href="/videos/2019/08/25/Lets_talk_about_Cherokee_people_and_the_House">transcript &amp; editable summary</a>)

The Cherokee Nation seeks to appoint a delegate to the House of Representatives, reflecting on historical treaties and the potential for Native representation in government.

</summary>

"The federal government has a horrible track record of honoring treaties with native nations."
"The idea of Native interest being represented on the house floor is going to be great."

### AI summary (High error rate! Edit errors on video page)

The Cherokee Nation wants to appoint a delegate to the House of Representatives as allowed by a treaty signed in 1835.
This treaty ultimately led to the Trail of Tears, a genocidal event that forcibly removed indigenous people from their land.
The federal government's relationship with indigenous cultures is framed as two sovereign nations interacting, but honoring treaties has been an issue.
Despite having voting rights, gerrymandering has significantly limited the voting power of the Cherokee Nation, consisting of 350,000 to 375,000 people.
The potential delegate from the Cherokee Nation, likely Kimberly Teehee, may not have voting powers but can represent Native nations' issues in the House.
Concerns exist that only the Cherokee Nation has this provision in their treaty, leading to worries about exclusive representation.
Kimberly Teehee is viewed as a front runner for the position and serves as a diplomat between the Cherokee Nation and the U.S. government.
While the process may take time, having Native interests represented in the House could significantly impact Native communities positively.
Beau believes that the representation of Native interests in the House will make C-SPAN more engaging and is worth the support.

Actions:

for advocates for native rights,
Support Native representation efforts within the government (suggested)
Stay informed about the developments in Native rights and representation (implied)
</details>
<details>
<summary>
2019-08-24: Let's talk about the Republicans trying to save the country.... (<a href="https://youtube.com/watch?v=HMlbWZkhYU4">watch</a> || <a href="/videos/2019/08/24/Lets_talk_about_the_Republicans_trying_to_save_the_country">transcript &amp; editable summary</a>)

Beau urges Joe Walsh to take action in primarying President Donald Trump to potentially save the Republican Party and secure his place in history.

</summary>

"If you're serious, do it."
"If you're gonna do it, do it. Save the country and save your party."
"Your only option is to primary the president because a lot of Republicans have seen what he is now."
"I don't particularly like you, to be honest, but as far as getting that man out of office, we're on the same page."
"Maybe you can save your party."

### AI summary (High error rate! Edit errors on video page)

Talks about a former Republican congressman turned conservative radio talk show host, Joe Walsh, who wants to primary President Donald Trump.
Urges Walsh to stop talking and actually take action if he is serious about challenging Trump.
Describes Walsh as the Republican Party's potential savior due to changing demographics and his ability to appeal to moderates.
Points out Walsh's social liberalism, clear communication skills, and lack of fascist tendencies as reasons he could make a difference.
Emphasizes the importance of Walsh's independent platform in reaching voters without GOP support.
Warns that the Republican Party is on life support and primarying Trump may be the only way to maintain it.
Suggests that many Republicans may not support Trump in the next election, potentially leading them to stay home instead of voting for him.
Acknowledges personal reservations about Walsh but supports the idea of removing Trump from office.
Concludes by stating that ousting Trump from the ticket could secure Walsh's place in history and potentially save the Republican Party.

Actions:

for republicans, political activists,
Primary President Donald Trump (implied)
Mobilize support for a potential challenge within the Republican Party (implied)
</details>
<details>
<summary>
2019-08-23: Let's talk about thanking Pia Klemp.... (<a href="https://youtube.com/watch?v=zf-xXnirnCM">watch</a> || <a href="/videos/2019/08/23/Lets_talk_about_thanking_Pia_Klemp">transcript &amp; editable summary</a>)

Beau introduces "Thank a Criminal Day," shares Pia Klemp's powerful stance on equality, and appreciates her actions.

</summary>

"We do not need authorities deciding about who is a hero and who is illegal."
"Thank you, Pia Klemp."

### AI summary (High error rate! Edit errors on video page)

Beau introduces the concept of "Thank a Criminal Day" in his video.
He shares a quote from Pia Klemp, a German ship captain who has saved thousands of migrants in the Mediterranean Sea.
Pia Klemp is facing accusations in Italy for assisting illegal immigration and working with human traffickers.
Italy is not the only government opposed to Pia's actions, with Paris offering her a medal which she turned down.
Pia's response to the medal offer is scathing, criticizing the authorities for their treatment of migrants and asylum seekers.
She rejects the notion of authorities deciding who is a hero and who is illegal, asserting that everyone is equal.
Beau expresses gratitude towards Pia Klemp for her actions and her powerful statement.
He commends Pia's sentiment and describes it as a wonderful statement.
Beau concludes the video by wishing his viewers a good night.

Actions:

for advocates for equality,
Support organizations aiding migrants and asylum seekers (suggested)
Advocate for fair treatment of migrants and asylum seekers (implied)
</details>
<details>
<summary>
2019-08-23: Let's talk about Trump's trade war, automation, and board games.... (<a href="https://youtube.com/watch?v=Dz-QFGhjhco">watch</a> || <a href="/videos/2019/08/23/Lets_talk_about_Trump_s_trade_war_automation_and_board_games">transcript &amp; editable summary</a>)

Beau criticizes the economic decisions affecting American companies, warns of increased inequality, and calls for a fair society where everyone can participate equally.

</summary>

"Bringing production facilities back won't bring back jobs as automation has replaced manual labor."
"Need for a new system not built on human exploitation."
"People get so far ahead that they start mocking those in poverty."
"Upheaval, the pitchforks come out."
"Build a society that is more equitable, that is more fair, a society in which everybody can stay in the game."

### AI summary (High error rate! Edit errors on video page)

Chinese tariffs on another $75 billion imposed.
President orders American companies to find alternative to China.
Blending corporate and government interests.
President's exact words: American companies must start looking for alternative to China, including bringing companies home and producing in the US.
Unlikely for companies to return production to the US; more likely to move to Vietnam or similar countries.
Bringing production facilities back won't bring back jobs as automation has replaced manual labor.
Automation is cheaper and more reliable than human labor.
U.S. losing in trade war against China.
Move increases chance of recession and income inequality.
Need for a new system not built on human exploitation.
Trump's scapegoating in blaming Jay Powell for economic issues.
Loss of largest trading partner (China) without valid reason.
Beau points out the need for a more equitable society where everyone can "stay in the game."
Contrasts Monopoly game dynamics with real life inequality and exploitation.
Calls for a fair society where everyone starts off equal.

Actions:

for economic activists, social justice advocates,
Advocate for fair trade policies (implied)
Support movements for income equality (implied)
Engage in community organizing for equitable systems (implied)
</details>
<details>
<summary>
2019-08-22: Let's talk about #ThankACriminalDay and #FreeRoss.... (<a href="https://youtube.com/watch?v=tvg2C2yZN-w">watch</a> || <a href="/videos/2019/08/22/Lets_talk_about_ThankACriminalDay_and_FreeRoss">transcript &amp; editable summary</a>)

Beau questions the criminal justice system's treatment of non-violent offenders and advocates for Ross Ulbricht's clemency, challenging the narrative around criminals on Criminal Day.

</summary>

"If there is no victim, there is no crime."
"Life in the federal system means life."
"That guy paid the price. The problem is, the price we're asking him to pay is too high."
"Criminals have changed the world and they will continue to."
"He won't get out. That's a little much for a website."

### AI summary (High error rate! Edit errors on video page)

Celebrates August 22nd, Criminal Day, created by Brian to honor criminals who have made a positive impact.
Questions the notion that soldiers protect freedom, arguing that criminals actually obtain it for us.
Mentions heroes like Martin Luther King Jr., whose actions were considered criminal at the time.
Talks about Ross Ulbricht, the creator of Silk Road, an online platform for drug sales, and his controversial case.
Points out that Ross Ulbricht received two life sentences plus forty years for a non-violent offense.
Raises the question of whether prison is about rehabilitation or punishment in Ross's case.
Describes Ross Ulbricht as a nerdy Eagle Scout who believed in the freedom for individuals to make their own choices.
Emphasizes that Ross is not a danger to society and does not deserve his harsh sentence.
Encourages viewers to visit freeross.org to learn more about Ross's case and support the petition for his clemency.
Argues that if there is no victim, there is no crime, and advocates for a reevaluation of the justice system.

Actions:

for justice reform advocates,
Visit freeross.org to learn more and support the petition for Ross Ulbricht's clemency (suggested).
Share the video with the hashtags #ThankCriminalDay and #FreeRoss (suggested).
</details>
<details>
<summary>
2019-08-21: Let's talk about whistling while drowning and the 2020 election.... (<a href="https://youtube.com/watch?v=ApBJra1taIA">watch</a> || <a href="/videos/2019/08/21/Lets_talk_about_whistling_while_drowning_and_the_2020_election">transcript &amp; editable summary</a>)

Beau reveals parallels between rescuing drowning victims and the administration's tactics, warning of divisive dog whistles to energize a shrinking base in the upcoming election.

</summary>

"He has to energize his base and he thinks his base is bigger than it is."
"His base is not going to carry him to victory, but that's who he wants to energize."
"We just need to be ready for them."
"Either you are completely incompetent or your dog whistling to a racist base."
"Is he stupid or is he a racist? That's it."

### AI summary (High error rate! Edit errors on video page)

Recalls taking a wilderness first responder course with realistic training scenarios.
Volunteers to rescue a drowning victim during training, facing unexpected challenges.
Draws a parallel between rescuing drowning victims and the current state of the administration.
Describes how panic affects drowning victims and likens it to the president's situation.
Analyzes the president's strategy to energize his base for the upcoming election.
Points out that negative voter turnout, not overwhelming support, led to the president's victory in the past.
Identifies the president's base as bigoted old white men and explains his focus on energizing them.
Criticizes the president's proposal to end birthright citizenship through executive order.
Explains the historical significance of the 14th Amendment and its importance to certain demographics.
Condemns the president's rhetoric around the 14th Amendment as a huge dog whistle to his base.
Questions the need to indefinitely detain migrant children and points out the profit-driven motives behind it.
Challenges the illogical reasoning behind holding children longer in detention facilities.
Dispels common misconceptions about birthright citizenship and who actually engages in birth tourism.
Warns of future attacks on marginalized groups by the president to rally his base and maintain power.
Urges readiness to identify and address the dog whistles used by the president to appeal to his base.

Actions:

for voters, activists, allies,
Recognize and challenge dog whistles in political rhetoric (suggested)
Stay vigilant against divisive tactics targeting marginalized groups (implied)
</details>
<details>
<summary>
2019-08-21: Let's talk about Narcan and Epi-pens.... (<a href="https://youtube.com/watch?v=xT89xscuVt0">watch</a> || <a href="/videos/2019/08/21/Lets_talk_about_Narcan_and_Epi-pens">transcript &amp; editable summary</a>)

Beau addresses misconceptions about Narcan and EpiPens, debunking myths and revealing societal attitudes towards addiction and pharmaceutical pricing.

</summary>

"Well, addicts should die."
"It encourages the devaluation of human life."
"Can't do anything about that. Need to kick down. That lowly addict."
"No sane person is angry about that."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Addresses a question about a meme comparing the cost of Narcan to EpiPens.
Clarifies that Narcan is not free; someone pays for it, and there are ways to cover the cost.
Explains the process where EMS administers Narcan and bills for it, just like any other life-saving drug.
Questions whether people want a scenario where insurance is checked before administering life-saving treatment.
Mentions grants, insurance companies, treatment centers, and foundations that cover the cost of Narcan.
Talks about free training available for Narcan administration on International Overdose Awareness Day.
Debunks the argument that Narcan enables addiction, as it actually inhibits effects and can lead to recovery.
Addresses the stigma surrounding substance abuse and how it influences perceptions of who should receive life-saving drugs.
Comments on society's anger towards pharmaceutical companies for high prices, like $600 EpiPens with a manufacturing cost of $2.
Conveys the message that societal anger often gets misdirected towards vulnerable groups like addicts.

Actions:

for advocates, community members,
Contact local EMS or substance abuse treatment centers to inquire about Narcan training opportunities (suggested).
Support foundations that provide funding for Narcan distribution (exemplified).
Advocate for pharmaceutical pricing transparency and fair drug pricing (implied).
</details>
<details>
<summary>
2019-08-20: Let's talk about PP and life after Title X.... (<a href="https://youtube.com/watch?v=dcGT848vJDY">watch</a> || <a href="/videos/2019/08/20/Lets_talk_about_PP_and_life_after_Title_X">transcript &amp; editable summary</a>)

Beau breaks down misconceptions around Planned Parenthood's funding and services, urging support to maintain vital healthcare access beyond abortion.

</summary>

"Well golly gee whiz, it seems like they do things other than abortion."
"Why abort when adoption is an option?"
"Congratulations, you played yourself."

### AI summary (High error rate! Edit errors on video page)

Explains the situation with Title X funding and Planned Parenthood, pointing out misconceptions and facts.
Describes how Planned Parenthood withdrew from Title X programs due to a rule prohibiting abortion referrals in exchange for funding.
Clarifies that the $60 million given up by Planned Parenthood was not used for abortions but for services like STD screenings and birth control under Title X.
Counters the idea of Planned Parenthood being defunded by putting the $60 million loss into perspective compared to their overall revenue of $1.67 billion.
Reveals that Planned Parenthood received $563 million in tax dollars last year, the majority of which came from Medicaid and was not used for abortions.
Notes that Planned Parenthood will need to increase fundraising by 10% to make up for the loss of $60 million.
Urges individuals to donate or purchase merchandise to support Planned Parenthood's health centers.
Emphasizes the various healthcare services provided by Planned Parenthood beyond abortion, including breast exams and birth control.
Points out the irony that reducing access to birth control can lead to more unplanned pregnancies and, consequently, more abortions.
Explains the importance of private donations in keeping Planned Parenthood health centers operational and serving underserved communities.

Actions:

for advocates, donors, supporters,
Support Planned Parenthood by donating directly or purchasing merchandise (suggested).
Increase donation to Planned Parenthood by 10% to help maintain their health centers (suggested).
Educate others on the misconceptions surrounding Planned Parenthood's funding and services (implied).
</details>
<details>
<summary>
2019-08-20: Let's talk about  nonsense and getting lost in Wonderland.... (<a href="https://youtube.com/watch?v=RA3H6CaGd1E">watch</a> || <a href="/videos/2019/08/20/Lets_talk_about_nonsense_and_getting_lost_in_Wonderland">transcript &amp; editable summary</a>)

Beau narrates the bizarre events in Wonderland under the rule of the erratic Jack of Diamonds, showcasing economic turmoil, communication chaos, and unfounded fears of invasion.

</summary>

"Must be a tradition here in Wonderland, I'm not really sure."
"He tends to blame foreigners for the loss of jobs rather than automation."
"Currently in the palace, Paranoia is running deep."
"A strange place here. It is a strange place."
"If I had a world of my own everything would be nonsense."

### AI summary (High error rate! Edit errors on video page)

Beau finds himself in Wonderland after falling down a hole with his dog, recounting the bizarre events unfolding.
The Jack of Diamonds has taken power through multiple marriages, mirroring the Queen of Hearts' style of ruling by beheading officials.
The leader has initiated a trade war with the Land of Oz, causing economic distress and material shortages in Wonderland.
Communication is chaotic, with messages delivered by borrowed birds from Snow White, leading to confusion and paranoia.
The Jack of Diamonds blames foreigners for job losses, inciting fear and deflecting from the real issues like automation.
In an attempt to help the poor, royal funds were given to the rich, who hoarded the money instead of aiding others.
Paranoia and witch hunts are rampant, prompting extreme security measures like contacting Elsa to build an ice wall.
Destruction of windmills and plans for clean coal reveal misguided environmental policies and uninformed decisions.
Attempts to purchase grain acres and suppress riots show a disconnect from reality and a reliance on authoritarian tactics.
The presence of a fearful lion immigrant illustrates misplaced fears of invasion by unarmed individuals seeking safety.

Actions:

for observers, wonderland residents,
Organize community meetings to address economic concerns and propose solutions (suggested)
Support local unions and workers affected by tariffs through donations or advocacy efforts (exemplified)
Educate fellow citizens on the importance of fact-checking and critical thinking in the face of misinformation (implied)
</details>
<details>
<summary>
2019-08-19: Let's talk about the future of Afghanistan.... (<a href="https://youtube.com/watch?v=RFU6jw9iyCU">watch</a> || <a href="/videos/2019/08/19/Lets_talk_about_the_future_of_Afghanistan">transcript &amp; editable summary</a>)

The U.S. is leaving Afghanistan, leaving behind a mess and facing the reality of Taliban resurgence, with no easy solutions in sight.

</summary>

"We are leaving, and we are leaving behind a mess."
"The time to figure that out is over. It's going to happen."
"Giving jets to the Afghan government is risky."
"Helicopters could help change the course of the war."
"This region of the world, this is where empires go to die."

### AI summary (High error rate! Edit errors on video page)

Four main groups operating in Afghanistan: U.S., Afghan government, Taliban, and I.S.
Taliban trying to reestablish itself as U.S. leaves, leading to dynamics changing.
U.S. selling out the Afghan government by talking directly to the Taliban without them.
Negotiators have no leverage with Taliban because they know the U.S. is leaving.
Taliban likely to reassert control over Afghanistan; U.S. probably needs to give them supplies to follow preferred route.
Staying in Afghanistan prolongs violence and doesn't help stabilize the country.
Violence will continue after the U.S. leaves, and Taliban might reassert control.
U.S. mindset of military solutions won't clean up the mess in Afghanistan.
Insurgents have historically won despite facing technologically advanced military forces.
Giving jets to the Afghan government is risky as Taliban or IS might end up controlling them.
Helicopters could be valuable aid as neither Taliban nor IS have significant air capabilities.
Need to learn from the mistakes in Afghanistan and plan military operations with the aftermath in mind.

Actions:

for policy makers, activists,
Provide humanitarian aid to Afghan civilians (implied)
Advocate for diplomatic solutions to stabilize Afghanistan (implied)
</details>
<details>
<summary>
2019-08-19: Let's talk about Trump, Shell, and Representative Democracy.... (<a href="https://youtube.com/watch?v=bkFyFxKpIhk">watch</a> || <a href="/videos/2019/08/19/Lets_talk_about_Trump_Shell_and_Representative_Democracy">transcript &amp; editable summary</a>)

Beau criticizes the Shell Trump photo-op, exposes unfair treatment of workers, and calls for more working-class representation in government for true democracy.

</summary>

"You're not actually going to be doing any work you don't need your safety gear except for that shirt so it looks like you're working class because you're a prop nothing more."
"What we really need is a lot more bartenders, carpenters, plumbers, people like that in the House of Representatives, people that actually represent us."
"We need a little bit more working class unity."

### AI summary (High error rate! Edit errors on video page)

Critiques the blending of corporate and state interests in the Shell Trump photo-op.
Explains how Shell contractors faced unfair choices during the event.
Mentions leaked memos revealing instructions for workers to appear as props.
Describes how corporations repay politicians by using workers as props.
Points out the false image created by such propaganda.
Analyzes how politicians manipulate public perception through divisive tactics.
Questions the dominance of certain professions in running the country.
Calls for more representation of working-class individuals in positions of power.
Advocates for unity among the working class for better governance.
Suggests the need for genuine representative democracy.

Actions:

for working-class americans,
Advocate for more working-class representation in government (implied)
Foster unity and communication among the working class (implied)
</details>
<details>
<summary>
2019-08-19: Let's talk about Apple, Trump, Trade Wars, and Farmers.... (<a href="https://youtube.com/watch?v=ufjjjqwJnQs">watch</a> || <a href="/videos/2019/08/19/Lets_talk_about_Apple_Trump_Trade_Wars_and_Farmers">transcript &amp; editable summary</a>)

Tim Cook exposes Trump's trade war, revealing the administration's favoritism towards the wealthy over American farmers.

</summary>

"He doesn't care about you, he never did, and he never will."
"It was just stuff he said to get people to follow him, that's it, that's all it was."
"That's President Trump's administration."

### AI summary (High error rate! Edit errors on video page)

Tim Cook exposed President Trump's trade war during a dinner meeting at the president's golf club.
Apple pays tariffs on Chinese components, making it hard to compete with companies not involved in the trade war like Samsung.
President Trump found Cook's argument compelling and is reconsidering the trade war.
Beau questions why American farmers didn't receive similar consideration during the trade war.
The Secretary of agriculture dismissed farmers' concerns as whining.
President Trump's administration favors the wealthy, with policies benefiting those with high net worth.
The administration's policies contradict the rhetoric aimed at common people.
Beau criticizes Trump for prioritizing personal gain over the well-being of Americans.
Trump's presidency is characterized by self-interest and exploitation of taxpayers.
Beau concludes by asserting that Trump never cared about the American people.

Actions:

for american voters,
Support policies that prioritize the well-being of all Americans, not just the wealthy (implied).
</details>
<details>
<summary>
2019-08-16: Let's talk about a wholesome story for 2019 in Arkansas.... (<a href="https://youtube.com/watch?v=RFaEIabn4qA">watch</a> || <a href="/videos/2019/08/16/Lets_talk_about_a_wholesome_story_for_2019_in_Arkansas">transcript &amp; editable summary</a>)

Four African-American boys fundraising for football faced racial bias and a gun in a white neighborhood in 2019, leading to charges against the aggressor with ties to law enforcement.

</summary>

"Four teenage boys had a revolver shoved in their face because they were fundraising for football in a white neighborhood."
"Man, that is some Dred Scott stuff right there."
"How could this happen? But we have to say it every day."
"Because well, I mean, they don't have any real rights."
"Anyway, it's just a fault, y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Four boys, ages 15 and 16 in Arkansas, finished football practice and started fundraising in August 2019.
The boys, African-American, were selling discount cards door-to-door when a woman confronted them with a gun, mistaking them for suspicious individuals.
The woman called the cops, describing the boys as suspicious because they were African-American in a white neighborhood.
Despite one of the cops recognizing the boys as school resource officer, the woman lectured them on "horseplay" instead of apologizing after the misunderstanding.
Beau draws parallels to the infamous Dred Scott case, pointing out the persistent racist attitudes that lead to incidents like this.
The woman, Jerry Kelly, 46, was charged with four counts of aggravated assault, false imprisonment, and endangering the welfare of a minor.
Kelly's connection to law enforcement through her previous employment and her husband's position raised concerns about preferential treatment in the case.
The incident underscores the ongoing racial prejudices and dangers faced by African-American youth, even in 2019.
Beau questions the progress made in society when incidents like this continue to occur.
The boys, engaging in a wholesome activity, faced a traumatic experience due to racial bias and ignorance.

Actions:

for community members,
Support community initiatives promoting racial equality and understanding (suggested)
Advocate for fair treatment and justice for victims of racial discrimination (implied)
Stand against racial prejudices and biases in your community (implied)
</details>
<details>
<summary>
2019-08-15: Let's talk about Trump telling the truth about the economy.... (<a href="https://youtube.com/watch?v=eHJlCZjLttc">watch</a> || <a href="/videos/2019/08/15/Lets_talk_about_Trump_telling_the_truth_about_the_economy">transcript &amp; editable summary</a>)

Beau exposes the president's dismissal of experts as "fake news" and warns against blind loyalty, urging scrutiny based on actions, not words.

</summary>

"There is no giant scheme and group of people out to get the President of the United States. He's not that important."
"The trust fund baby billionaire is not your friend."
"He's admitted that he used to get politicians money. So he, as a businessman, cut them out."
"The policies that are going to benefit them [billionaires] are going to hurt you."
"It's just a thought, y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Watching the president, Beau notes his claims of a strong economy, contrary to global recession predictions.
The president dismisses economists as "fake news" and untrustworthy for not supporting his narrative.
Beau points out the president's pattern of discrediting various experts and professionals as fake news.
He criticizes blind loyalty to the president despite his track record of unfulfilled promises and benefiting the wealthy.
Beau warns against the president's manipulation and urges people to re-evaluate their support based on his actions.

Actions:

for voters, citizens,
Re-evaluate support based on actions, not promises (implied)
Analyze the president's track record and promises kept (implied)
Question blind loyalty and scrutinize policies affecting the populace (implied)
</details>
<details>
<summary>
2019-08-14: Let's talk about economics and the #TrumpRecession.... (<a href="https://youtube.com/watch?v=B-Sh6jtGuUg">watch</a> || <a href="/videos/2019/08/14/Lets_talk_about_economics_and_the_TrumpRecession">transcript &amp; editable summary</a>)

Most Americans don't understand the looming recession triggered by tariffs; prepare financially and increase demand to mitigate its effects.

</summary>

"Get prepared. Don't make any purchases."
"The tariffs opposed on the other side limited the goods produced. That's what triggered this."
"Burn through any reserves you have. Spend it."
"We need to do what we can to mitigate the destruction that his administration has caused."
"Don't cheer this on as much as you want to, as much as I want to. We've got to work to get out of it."

### AI summary (High error rate! Edit errors on video page)

Most Americans don't understand the economy, and a recession is looming.
The yield curve inverted, signaling a recession.
A recession is a decline in trade and industry activity, where GDP falls for two consecutive quarters.
The former chairman of the Fed, Greenspan, warned about yields falling below zero on bonds.
This recession is unique as it was triggered by tariffs, not the typical unemployment route.
People on Twitter are already calling this the "Trump recession."
Recommendations for lower economic classes include preparing by not making big purchases and saving money.
Those with money to burn are encouraged to increase demand for goods and services within the US.
History shows that tariffs triggered a recession leading into the Great Depression.
Mitigating the effects of the recession caused by the administration is vital to prevent widespread harm.

Actions:

for americans,
Prepare financially by avoiding big purchases and saving money (suggested)
Increase demand for goods and services within the US by spending reserves (suggested)
Work to mitigate the effects caused by the administration's policies (implied)
</details>
<details>
<summary>
2019-08-14: Let's talk about Trump, Obama, Alejandra, friends, and enemies.... (<a href="https://youtube.com/watch?v=nd7YhsJS5MI">watch</a> || <a href="/videos/2019/08/14/Lets_talk_about_Trump_Obama_Alejandra_friends_and_enemies">transcript &amp; editable summary</a>)

Beau criticizes the administration's immigration focus, deportation tactics, and treatment of deported military spouses while urging working-class solidarity and exposing harmful policies.

</summary>

"It's about framing it so you kick down. You blame those with less power than you for the mistakes or the deliberate attacks on you by those at the top."
"They find somebody to scare you. Somebody for you to kick at. Most times isn't real."
"This administration is not the friend of anybody in the working class."

### AI summary (High error rate! Edit errors on video page)

Criticizes the administration for focusing on rewriting the immigration laws instead of addressing more pressing issues.
Points out the media's role in perpetuating misleading narratives about legal immigration.
Contrasts Trump's deportation numbers with those of the Obama administration.
Notes the ineffectiveness of Trump's efforts due to his fixation on stereotypes of undocumented immigrants.
Explains how the Obama administration utilized the surveillance state for deportations.
Tells the story of Alejandra Juarez, who self-deported after facing legal challenges despite being in the US for 20 years.
Criticizes the administration's treatment of deported military spouses and its impact on combat veterans.
Addresses the demonization of immigrants receiving benefits compared to corporate welfare.
Calls out the administration for causing harm through policies like the trade war.

Actions:

for working-class americans,
Support deported military spouses' communities (exemplified)
Advocate for fair treatment of immigrants and combat stereotypes (suggested)
Educate others on the impact of harmful policies on working-class communities (implied)
</details>
<details>
<summary>
2019-08-13: Let's talk about the Endangered Species Act, Chickens, and Painters.... (<a href="https://youtube.com/watch?v=4Qb0wLYv0jo">watch</a> || <a href="/videos/2019/08/13/Lets_talk_about_the_Endangered_Species_Act_Chickens_and_Painters">transcript &amp; editable summary</a>)

The Endangered Species Act changes put a price on species, urging everyone to become conservationists to prevent irreversible environmental damage.

</summary>

"Every species will have a dollar sign attached to it."
"We have to become environmentalists or conservationists, otherwise we're all just house painters putting it over time on August 8th, 1945 in Nagasaki."
"We're not trying to stop huge issues from occurring in the future. We're trying to mitigate, trying to slow it down, because it's going to happen."

### AI summary (High error rate! Edit errors on video page)

The Endangered Species Act is being changed, causing concern among conservationists worldwide.
Oil and coal lobbyists, David Bernhardt and Andrew Willer, along with Wilbur Ross, are involved in the changes.
The current administration's key environmental positions are held by individuals with backgrounds in oil, coal, and mining.
Under the new changes, economic impact will now be a significant factor in court decisions regarding the protection of species.
Every species will essentially have a price tag attached to it under the new considerations.
Conservationists are alarmed as over a million species face extinction, now subject to economic evaluations for protection.
Multinational corporations lack loyalty to any particular country and prioritize wealth extraction over environmental protection.
The impact of these corporations on the environment will directly affect those residing in the United States and the Western world.
Beau urges his audience to not just be activists but also become conservationists to combat the environmental challenges we face.
He stresses the urgency of placing individuals in power who can make significant environmental policy changes before it's too late.

Actions:

for environmental advocates,
Advocate for strong environmental protections through local initiatives and community action (implied)
Support and volunteer for conservation organizations working to protect endangered species and habitats (implied)
</details>
<details>
<summary>
2019-08-12: Let's talk about an old atlas, colonization, and the future.... (<a href="https://youtube.com/watch?v=Nab7UB7bbGA">watch</a> || <a href="/videos/2019/08/12/Lets_talk_about_an_old_atlas_colonization_and_the_future">transcript &amp; editable summary</a>)

Beau delves into the nuances of colonization, propaganda, and Western supremacy, urging a critical reevaluation of historical narratives for a more equitable future.

</summary>

"The victor will never be asked if he's told the truth."
"Colonization never ended, it never stopped, it just changed a little bit."
"It's still about wealth extraction."
"The people of Africa, they don't want our troops. They want our business."
"We have to start seeing through it or our reluctance to look at the facts is going to cost a lot of lives."

### AI summary (High error rate! Edit errors on video page)

Talks about the Italian occupation of Ethiopia in 1935-1936 and how it was not a functioning colony.
Mentions the conflict during the Italian occupation of Ethiopia.
Questions why an old Collier's Atlas from 1940 marks Italian East Africa as if giving the fascists and Italians a win.
Points out the propaganda and biased narratives in historical accounts and atlases.
Differentiates between two types of propaganda: nationalist and the narrative of Western supremacy.
Emphasizes the importance of challenging the image of the uncivilized African perpetuated by colonial powers.
States that colonization never truly ended, just evolved into wealth extraction through different means.
Criticizes the creation of Africa Command by the Department of Defense (DOD) in 2007 and its mission centered around military intervention.
Argues that African nations desire business partnerships rather than exploitative military presence.
Warns about the consequences of not seeking the truth from current powers and victors, especially in relation to Africa.

Actions:

for history enthusiasts, activists,
Challenge biased narratives in history books and educational materials (implied)
Support African nations in building their economies through fair business practices (implied)
Advocate for transparency and accountability from current powers and victors (implied)
</details>
<details>
<summary>
2019-08-12: Let's talk about a teachable life-saving moment in Tennessee.... (<a href="https://youtube.com/watch?v=vgIziXFYK_c">watch</a> || <a href="/videos/2019/08/12/Lets_talk_about_a_teachable_life-saving_moment_in_Tennessee">transcript &amp; editable summary</a>)

Addressing a Tennessee incident involving stolen firearms, Beau stresses learning from warning signs to prevent harm and advocates for better gun safety measures in schools.

</summary>

"You have to err on the side of caution."
"If it can be defeated with this, the weapon's not secured."
"They can make the decision whether or not to send their kid to school."
"Keep the ammo on you."
"Real gun safes. Keep the ammo on you."

### AI summary (High error rate! Edit errors on video page)

Addressing an incident in Tennessee involving two 18-year-olds breaking into a school and stealing an AR-15 and vests, stressing the need to learn from it.
Emphasizing the importance of acknowledging warning signs and making changes before harm occurs.
Criticizing the downplaying of the incident by officials and the lack of notification to parents about the danger students were in.
Advocating for proper gun safes and keeping ammunition on school resource officers to prevent unauthorized access to weapons.
Challenging misconceptions about firearms, particularly around the use of AR-15s in school shootings.
Arguing for practical solutions like real gun safes, better security measures, and more training with firearms to improve safety in schools.

Actions:

for parents, educators, community members,
Keep ammunition on school resource officers for immediate access (implied)
Implement real gun safes for secure storage of weapons (implied)
Educate on firearm safety and training for effective use (implied)
</details>
<details>
<summary>
2019-08-11: Let's talk about socially conditioned responses to domestic violence.... (<a href="https://youtube.com/watch?v=JvI41qfDYXg">watch</a> || <a href="/videos/2019/08/11/Lets_talk_about_socially_conditioned_responses_to_domestic_violence">transcript &amp; editable summary</a>)

Addressing harmful social conditioning around violence and protection, Beau advocates for non-violent solutions and prioritizing the safety of individuals in abusive situations, urging them to leave and seek assistance.

</summary>

"You've been socially conditioned to use violence to solve problems."
"Get her out. That's the goal."
"Nothing. You owe him nothing. Get out."
"Everything that society has said and conditioned people to believe is the wrong thing."
"Just go."

### AI summary (High error rate! Edit errors on video page)

Urgently addressing a personal message that supersedes any news.
Exploring social conditioning and the problematic association of protection with violence, especially for men in America.
Sharing a story of a husband with substance abuse issues who beat up his wife, leading to questions of retaliation upon his release from jail.
Challenging the concept of using violence as a solution and advocating for non-violent ways to ensure protection.
Encouraging getting the wife out of the abusive situation as the primary goal.
Addressing societal conditioning that perpetuates harmful norms, such as women feeling obligated to fix their partners or stay in abusive relationships.
Emphasizing the importance of leaving an abusive situation, despite potential stigmas or economic considerations.
Providing information about shelters and organizations that can help individuals in abusive situations.
Asserting that once violence occurs, any obligation or debt to the abuser is immediately nullified.
Advocating for seeking assistance and leaving the abusive environment for safety.

Actions:

for men in america,
Find and utilize organizations and shelters that can provide assistance in leaving an abusive situation (suggested).
Prioritize getting individuals in abusive situations out to ensure their safety (implied).
Challenge societal norms and conditioning by advocating for non-violent solutions to protection (implied).
</details>
<details>
<summary>
2019-08-10: Let's talk about St. Louis, Ethiopia, Tunisia, Italy, and Open Arms (<a href="https://youtube.com/watch?v=SF_F-d7p-a4">watch</a> || <a href="/videos/2019/08/10/Lets_talk_about_St_Louis_Ethiopia_Tunisia_Italy_and_Open_Arms">transcript &amp; editable summary</a>)

Beau explains the significance of historical context in understanding current events, reflecting on the rejection of refugees reminiscent of past tragedies and proposing a compassionate solution to address the crisis.

</summary>

"Everything that happens changes what happens tomorrow."
"The scars that we're creating today, they're gonna have backlash too."
"You know, if Trump was smart, he could stop these attacks overnight."
"That relationship between the people of the area that is now Tunisia and the area that is now Italy has been around since Italy was known as Rome."
"It's estimated that about a quarter of them got caught and sent to death camps where they died."

### AI summary (High error rate! Edit errors on video page)

Explains the importance of understanding historical context to comprehend current events.
Recounts the tragic story of the St. Louis ship during WWII carrying Jewish refugees rejected by multiple countries.
Draws parallels to a modern-day situation involving the ship "Open Arms" with refugees off the Italian coast.
Notes the refusal of the Italian government and other European countries to allow the refugees ashore.
Links the desire of refugees to reach Italy to historical ties, such as Ethiopia's past as an Italian colony.
Mentions the long-standing relationship between Tunisia and Italy, despite Tunisia not being an official Italian colony.
Comments on the impact of extreme nationalism leading to the current plight of refugees.
Suggests a solution by proposing a strategy to address attacks by welcoming refugees, contrasting it with the current stance.
Warns of the lasting effects and backlash of current actions on children and citizens.
Concludes with a reflection on the repercussions of present actions influencing the future.

Actions:

for global citizens, activists,
Welcome refugees in your community (implied)
</details>
<details>
<summary>
2019-08-09: Let's talk about this week.... (<a href="https://youtube.com/watch?v=qlwmvqp0E-I">watch</a> || <a href="/videos/2019/08/09/Lets_talk_about_this_week">transcript &amp; editable summary</a>)

Beau recaps recent events, warns of escalating violence against minority groups, and urges vigilance in the face of deteriorating political and economic conditions.

</summary>

"If you were out there to own the libs, yeah, you are now forever identified as a white nationalist."
"Imagine being so in love with an idea of a symbol that you start to hate what it's supposed to represent."
"People are dying. Children are having their skulls cracked."
"As things start to deteriorate in the economy, you're going to see them start to become disillusioned and it's going to make them more violent."
"Winning comes at a cost, and it's gonna be the minority groups that's who's gonna be hit the most."

### AI summary (High error rate! Edit errors on video page)

Recaps recent events, including Pentagon surveillance balloons in Wisconsin for total information awareness.
Mentions the Walmart incident where thankfully no one got hurt.
Talks about ice raids resulting in hundreds of people disappearing, leaving kids at daycare and coming home to empty houses.
Shares a story of a man deported to Iraq where he eventually died, despite never having been there and not speaking Arabic.
Describes a disturbing incident of a child's skull being literally cracked over not removing a hat during the National Anthem.
Comments on an immigration activist caught with weapons standing by a Trump mural on his truck.
Addresses the evolution of a gesture from a joke to a white nationalist symbol.
Criticizes blind loyalty to symbols over their true meanings and the dangerous consequences.
Warns about the increasing violence and threat faced by minority groups in the current political climate.
Urges everyone, especially non-white and non-straight individuals, to be vigilant and cautious in the deteriorating situation.

Actions:

for activists, minorities, allies,
Stay vigilant and aware of your surroundings (implied)
Be cautious and careful in the current political climate (implied)
Keep your eyes open for signs of escalating violence (implied)
</details>
<details>
<summary>
2019-08-08: Let's talk about the American Farmer, Iowa, and China.... (<a href="https://youtube.com/watch?v=TskYfNBE8go">watch</a> || <a href="/videos/2019/08/08/Lets_talk_about_the_American_Farmer_Iowa_and_China">transcript &amp; editable summary</a>)

American farmers hit hard by China's halt in agricultural purchases, facing long-lasting consequences; trade war effects extend beyond economy to impact jobs and businesses.

</summary>

"They bought $19.5 billion worth. 2018, 9.2, a loss of $10 billion that was going to the American farmer."
"It's going to have a long-lasting effect because food isn't like an iPad, you know?"
"The problem with that is most farmers don't really want subsidies. They don't."
"They know what their pocketbook looks like, and I think that's going to have a bigger impact than people are saying."
"If it goes on much longer, they're looking at store closings and job eliminations."

### AI summary (High error rate! Edit errors on video page)

Explains the impact of the trade war on American farmers, particularly with China no longer buying agricultural products.
China bought $19.5 billion worth of goods from the U.S. in 2017, dropping to $9.2 billion in 2018, and now it's zero.
Points out that food is a necessity, unlike luxury items like iPads, meaning the effects will be felt more intensely.
Mentions deals between Brazil and China for soybeans, which used to make up 60% of U.S. soybean exports.
Talks about the long-lasting effects of the trade war even after it ends, as American farmers will need to compete to regain lost business.
Notes that subsidies for farmers might not be a preferred solution, as farmers want to rely on business rather than government aid.
Predicts a significant impact on Trump in the 2020 elections due to economic repercussions felt by states like Iowa, known for soybean production.
States that the trade war has resulted in $27 billion in extra costs for American taxpayers until June.
Mentions potential store closings and job losses in companies like Joanne Fabrics and the American Textile Company due to increased prices and reduced demand.

Actions:

for american citizens, farmers, policymakers,
Contact local representatives to advocate for fair trade policies that support American farmers (implied)
Support local farmers' markets and businesses to help offset the impacts of trade wars (implied)
</details>
<details>
<summary>
2019-08-08: Let's talk about reputations and Chicago.... (<a href="https://youtube.com/watch?v=VGX49N4I22w">watch</a> || <a href="/videos/2019/08/08/Lets_talk_about_reputations_and_Chicago">transcript &amp; editable summary</a>)

Beau challenges misconceptions about Chicago's crime reputation and warns against believing manipulated statistics and narratives.

</summary>

"Chicago is not Mogadishu."
"Reputations are sometimes unearned."
"Be very, very careful when you're looking at statistics and you're looking at narratives that are supposedly based on statistics."

### AI summary (High error rate! Edit errors on video page)

Shares a story about misconceptions based on reputation, using his friend's experience in Chicago as an example.
Recalls a personal experience from high school where a fight led to him gaining a reputation that wasn't entirely deserved.
Examines murder rates in various cities and states, comparing them to challenge the reputation of Chicago as a crime-ridden city.
Points out that Chicago doesn't have one of the highest murder rates when compared to other cities and states.
Mentions the role of propaganda in shaping false narratives about crime rates in Chicago, related to a former president and certain news networks.
Concludes by urging caution when interpreting statistics and narratives, especially when they are manipulated to create fear.

Actions:

for community members, skeptics,
Challenge false narratives about communities (implied)
Verify statistics and information before forming opinions (implied)
</details>
<details>
<summary>
2019-08-07: Let's talk about money in politics and San Antonio.... (<a href="https://youtube.com/watch?v=d6feSYhzvYk">watch</a> || <a href="/videos/2019/08/07/Lets_talk_about_money_in_politics_and_San_Antonio">transcript &amp; editable summary</a>)

Representative Castro's tweet on Trump donors sparks debate on transparency in political funding, with Beau advocating for open disclosure of contributions to ensure accountability and support for good causes.

</summary>

"If you believe in what you're donating to, you don't care if it becomes public, especially if they're doing good."
"The only reason you'd be worried about it becoming public is if you know it's not doing good."
"Let's see who's funding, who's paying who off."
"We all are finding it. We all are doing good."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Representative Castro from Texas tweeted out a list of people from San Antonio who gave the maximum donation to President Trump's re-election campaign, causing anger.
People are angry about the public release of donor information, questioning why supporting a cause should be a secret.
Donations to political campaigns are driven by belief in the cause or creating political favor, but why hide support if it's for a good cause?
Kevin McCarthy criticized those funding Trump's campaign for supporting divisive rhetoric and dehumanizing others.
Beau suggests that all candidates should disclose who funds them to ensure transparency.
Beau lists various organizations he supports through donations, advocating for the public disclosure of contributions to ensure accountability and transparency.
He encourages the public release of donation information to reveal who is funding political campaigns.
Beau believes that if donations are made in good faith to support beneficial causes, there should be no shame in making them public.
Transparency in political donations is vital to ensure that the funds are used for good purposes.
Beau expresses support for creating a Twitter account to disclose funding sources, promoting accountability in political campaigns.

Actions:

for political activists,
Support organizations like Pencils of Promise, Habitat for Humanity Louisiana, Planned Parenthood, ACLU, among others (exemplified).
Create a Twitter account to disclose political campaign funding sources (suggested).
</details>
<details>
<summary>
2019-08-07: Let's talk about a bizarre working theory.... (<a href="https://youtube.com/watch?v=yZkFtTQjF5A">watch</a> || <a href="/videos/2019/08/07/Lets_talk_about_a_bizarre_working_theory">transcript &amp; editable summary</a>)

Analyzing demographic info to prevent violent acts, mass shooters more like serial killers, urge to respond not react.

</summary>

"If somebody tells you they're going to do it, believe them."
"We need to respond rather than react."
"Mass shooters are more closely related to serial killers than they are terrorists."

### AI summary (High error rate! Edit errors on video page)

Analyzing demographic information to identify potential perpetrators before they commit violent acts.
Majority of mass shooters are male, white, and display concerning behaviors.
Early childhood trauma, inciting incidents, and a planning phase precede violent acts.
Characteristics of mass shooters include abusive behavior, mental health concerns, and leaked plans.
Mass shooters often have personal grievances blended with other grievances.
They have a desire for control and notoriety, similar to serial killers.
Mass shooters may be more closely related to serial killers than terrorists.
Need to respond, not react, to prevent future violent incidents.
Current strategies focusing on political violence may not be effective.
Urges a shift in perspective and approach to addressing mass shootings.

Actions:

for policy makers, law enforcement, psychologists,
Contact local authorities or FBI to report suspicious behavior (implied)
Advocate for standardized reporting methods for potential threats (implied)
</details>
<details>
<summary>
2019-08-06: Let's talk about log cabins in Vietnam, tough guys, and solutions.... (<a href="https://youtube.com/watch?v=cHV7N-2y58Q">watch</a> || <a href="/videos/2019/08/06/Lets_talk_about_log_cabins_in_Vietnam_tough_guys_and_solutions">transcript &amp; editable summary</a>)

Beau introduces Dan Glenn's story of building a cabin in his mind as a coping mechanism during captivity, stressing the importance of effective coping strategies in preventing tragedies like mass shootings through education and understanding.

</summary>

"He built a cabin. Not physically, he built it in his mind."
"The toughest people in the U.S. military are taught coping skills at one of the toughest schools in the military."
"Good ideas normally don't require force, they require just education, just a thought."

### AI summary (High error rate! Edit errors on video page)

Introduces the story of Dan Glenn, a Navy pilot shot down in Vietnam in 1966 and held in prison camps until 1973.
Glenn built a cabin in his mind while in captivity, using ashes and rice water to sketch it out, a coping mechanism that saved lives and preserved sanity.
Upon returning to the United States, Glenn physically built the cabin in Oklahoma.
The story of Glenn's coping mechanism is taught at SEER, a survival school for the US military's tough guys.
Beau explains coping skills, categorizing them into emotion-based (like imagination), problem-based (working harder, learning from mistakes), and others.
Coping strategies aim to maintain a positive outlook, prevent overreactions, and accept situations objectively.
Having a positive social support network is vital in coping effectively.
Recognizing successes, learning from mistakes, developing self-control, and maintaining oneself are part of good coping strategies.
Beau points out a pattern of early childhood trauma, lack of coping skills, and a triggering incident leading to mass shootings.
The root solution to preventing mass shootings lies in developing effective coping strategies, addressing the motive rather than the means (such as limiting access to weapons).
Beau stresses that education and understanding are key to implementing this solution effectively.

Actions:

for individuals, educators, mental health advocates,
Learn about different coping strategies and mental health practices (suggested)
Educate others on the importance of coping skills and positive social support networks (implied)
</details>
<details>
<summary>
2019-08-06: Let's talk about it happening here.... (<a href="https://youtube.com/watch?v=NovuyaBMkBc">watch</a> || <a href="/videos/2019/08/06/Lets_talk_about_it_happening_here">transcript &amp; editable summary</a>)

Beau presents a stark reality-check on the potential for violence in the country, dissecting various gun control proposals and advocating for comprehensive solutions to prevent tragedies.

</summary>

"They're forgetting it because they're angry, they're frustrated because they don't know what to do, they're afraid."
"We're chaos driven. We are a violent country. We always have been."
"It can happen here."
"Sometimes the deterrent is what stops violence."
"We don't want violence."

### AI summary (High error rate! Edit errors on video page)

Addressing the urgency of the situation, Beau foregoes his usual style of civil discourse to present facts regarding the potential for violent events in the country.
In light of recent tragedies, people are forgetting the possibility of such events occurring in their own communities as they grapple with feelings of frustration, helplessness, and fear.
Beau points out a unique turn where individuals who previously spoke out against fascism are now advocating for disarmament.
Drawing attention to the El Paso shooter's motives and the strategic thinking of those planning violent acts, Beau underscores the need to analyze proposed gun control measures critically.
Beau dissects various gun control proposals, shedding light on how they may disproportionately affect economically disadvantaged and minority communities.
He challenges the effectiveness and potential unintended consequences of suggested measures like training requirements, insurance mandates, and ammunition rationing.
Beau also touches on the legislative side, discussing the feasibility and implications of banning body armor and high-capacity magazines.
Addressing mental health checks and the nuances of implementing such measures, Beau stresses the importance of a comprehensive and effective approach to preventing violence.
Beau advocates for solutions focusing on domestic violence indicators, responsible gun storage practices, coping skills education, and improved reporting methods for identifying individuals at risk of violence.
Lastly, Beau underscores the significance of maintaining a balance between gun rights and public safety, stressing the role of a well-armed civilian population as a deterrent against potential government overreach.

Actions:

for advocates for comprehensive gun control measures,
Advocate for expanded laws addressing domestic violence indicators to prevent firearm access (suggested)
Promote responsible gun storage practices and education on coping mechanisms (suggested)
Support improved reporting methods for identifying individuals at risk of violence (suggested)
</details>
<details>
<summary>
2019-08-05: Let's talk about how we can stop the shootings.... (<a href="https://youtube.com/watch?v=Ht5ldqQdioM">watch</a> || <a href="/videos/2019/08/05/Lets_talk_about_how_we_can_stop_the_shootings">transcript &amp; editable summary</a>)

Beau addresses the cultural roots of violence, advocating for social reform, coping skills education, responsible gun ownership, and de-glorifying shooters to stem the tide.

</summary>

"Early childhood trauma. This is bullying, parental suicide, child abuse, exposure to violence at an early age."
"There's an identifiable moment where the decision was made. That was the point of no return."
"We need to let kids be kids, but that's going to require pretty wide-ranging social reform."
"Secure your firearms and don't tell your favorite nephew the code."
"Don't name them. Make them a non-person."

### AI summary (High error rate! Edit errors on video page)

Addressing solutions to help stop the plague of violence sweeping the nation.
Noting the cultural roots of the issue rather than blaming specific factors like firearms or pharmaceuticals.
Sharing findings from a study by the Violence Project funded by the National Institute of Justice.
Identifying four common factors among mass shooters that are almost universally present.
Emphasizing early childhood trauma and its long-lasting impacts.
Explaining the importance of identifying crisis points to intervene before shootings occur.
Describing how shooters often study other shooters, contributing to a socially transmitted disease.
Pointing out the prevalence of obtaining firearms from family members in mass shooting incidents.
Advocating for social reform to address underlying issues like coping skills and trauma.
Urging responsible gun ownership and securing firearms to prevent access by potential shooters.

Actions:

for community members,
Secure your firearms to prevent unauthorized access (implied)
Educate yourself and others on identifying crisis points in individuals in crisis situations (implied)
Advocate for social reform to address early childhood trauma and improve coping skills education (implied)
</details>
<details>
<summary>
2019-08-04: Let's talk about backpacks, rhetoric, and paths.... (<a href="https://youtube.com/watch?v=SxmMBfVFvX8">watch</a> || <a href="/videos/2019/08/04/Lets_talk_about_backpacks_rhetoric_and_paths">transcript &amp; editable summary</a>)

Beau questions the President's ability to sleep at night, holding him accountable for the consequences of his divisive rhetoric and urging a change to save lives.

</summary>

"How do you go to sleep at night knowing that your words killed people and they did?"
"Dead bodies in Walmart built the wall. That's your legacy."
"You can change your rhetoric and you can save lives."
"I am a small-time YouTuber and I am more conscientious about what I say, than the President of the United States."
"How does he sleep?"

### AI summary (High error rate! Edit errors on video page)

Recent days have been serendipitous for him, with news stories allowing him to make wider points.
A native woman asked about bulletproof backpacks out of fear for her child being mistaken for a Latino due to the current climate in America.
Beau advised against bulletproof backpacks, suggesting using a laptop bag with a steel plate instead.
Beau receives questions related to his background but is selective in answering due to the dual-use nature of the information.
He questions how the President can sleep at night knowing his rhetoric has led to shootings and deaths.
Beau points out the President's role in dividing rather than uniting the country.
He urges the President to change his rhetoric to save lives and prevent further violence.
Beau expresses disappointment that as a small-time YouTuber, he is more conscientious about his words than the President.
He calls on the media to press the President on how he can sleep at night given the consequences of his rhetoric.
Beau stresses the need for the President to adjust his rhetoric and work towards unifying the country, as the divide is widening and leading to deaths.

Actions:

for media, concerned citizens,
Press the President to change his rhetoric and work towards unifying the country (implied)
</details>
<details>
<summary>
2019-08-03: Let's talk about the tolerant left.... (<a href="https://youtube.com/watch?v=yQFY8iosTno">watch</a> || <a href="/videos/2019/08/03/Lets_talk_about_the_tolerant_left">transcript &amp; editable summary</a>)

Beau explains the differences between left and right-wing armed groups, touches on historical challenges, and stresses the importance of peaceful approaches over violence.

</summary>

"Ideas travel faster than bullets."
"We need to keep this peaceful as much as humanly possible."
"The idea of sparking that kind of conflict, it's never good."

### AI summary (High error rate! Edit errors on video page)

Explains the lack of liberal armed groups, attributing it to most liberals being anti-gun.
Contrasts right-wing militias, characterized by conventional tactics and structure, with leftists who don't style themselves as militias.
Mentions armed leftist groups like the John Brown Gun Club and Redneck Revolt that don't have traditional militia setups.
Talks about the historical challenges faced by armed leftists in the US, with FBI scrutiny in the past.
Describes the security consciousness of armed leftist groups, mentioning a unique vetting process involving friends.
Notes the shift in security culture and how some groups now ask more probing questions, leading to hesitation from potential members.
Points out that armed leftists train in guerrilla-style operations rather than conventional tactics.
States that firearms are seen as a tool by the left, unlike a status symbol on the right.
Emphasizes the importance of keeping things peaceful and discourages violence in any form.
Encourages responsible gun ownership for defense or recreational purposes but warns against offensive uses.

Actions:

for activists, gun owners,
Join or support organizations like the John Brown Gun Club or Redneck Revolt (exemplified)
</details>
<details>
<summary>
2019-08-02: Let's talk about Tulsi Gabbard and Foreign Policy.... (<a href="https://youtube.com/watch?v=gUxQY0xHEFY">watch</a> || <a href="/videos/2019/08/02/Lets_talk_about_Tulsi_Gabbard_and_Foreign_Policy">transcript &amp; editable summary</a>)

Candidates' backgrounds shape their positions, evident in Tulsi Gabbard's nuanced stance on Syria, challenging conventional narratives.

</summary>

"Candidates' backgrounds influence their positions."
"Gabbard's stance acknowledges the US's role in triggering the conflict in Syria and the bloodshed."
"Understanding candidates' backgrounds and past actions is key to predicting their future decisions in office."

### AI summary (High error rate! Edit errors on video page)

Candidates' backgrounds influence their positions, as seen with a candidate facing criticism over her Syria stance.
Trump's policy decisions were not surprising, reflecting his approach to running companies for short-term gains and cashing out.
Tulsi Gabbard's military background explains her nuanced stance on Syria, contrasting the dehumanization in infantry units with the empathy in medical units.
Gabbard's attention to international affairs and understanding of the prologue in Syria sets her apart.
The US involvement in Syria stemmed from a goal of destabilizing and ousting Assad, not supporting rebels or fighting extremists.
Beau suggests a personal opinion that the US involvement in Syria may have been linked to a pipeline.
Gabbard's stance acknowledges the US's role in triggering the conflict in Syria and the bloodshed.
Beau cautions against viewing Assad solely through the US narrative, noting the nuances and authoritarian context of the Middle East.
Gabbard's opposition to intervention should not be confused with support for Assad; she aims to oppose intervention while acknowledging Assad's actions.
Understanding candidates' backgrounds and past actions is key to predicting their future decisions in office.

Actions:

for voters, political analysts,
Research candidates' backgrounds to understand their positions better (implied).
</details>
<details>
<summary>
2019-08-01: Let's talk about puppets on parade and the important part of this election.... (<a href="https://youtube.com/watch?v=4Y1RmIMF_b4">watch</a> || <a href="/videos/2019/08/01/Lets_talk_about_puppets_on_parade_and_the_important_part_of_this_election">transcript &amp; editable summary</a>)

Beau stresses the necessity of moving back towards representative democracy, holding officials accountable, and avoiding traits of dictatorships, urging caution in prosecuting Trump and advocating for international accountability mechanisms.

</summary>

"We need to move back towards representative democracy and away from totalitarian regimes."
"Bad judgment cost almost half a million lives; precludes you from higher office."
"Locking up the opposing political party is dangerous."
"Reestablish ties with the International Criminal Court to hold President Trump accountable."
"Turn away from fascism by using existing mechanisms for accountability."

### AI summary (High error rate! Edit errors on video page)

Analyzing the recent debates, it wasn't surprising as most candidates' positions were anticipated based on their campaign contributors and past actions.
Some candidates like Yang, Williamson, and Tulsi stand outside the political norm, but the majority were predictable.
Beau stresses the importance of the upcoming election not just to remove Trump from office but also to steer away from totalitarian regimes and fascism.
Holding elected officials accountable and veering clear of dictator-like traits are imperative for a functioning representative democracy.
Beau is surprised by Joe Biden owning his bad judgment regarding the Iraq war vote but believes that such significant errors should disqualify politicians from higher office.
He criticizes the calls to prosecute President Trump, citing the unlikelihood of a Republican Senate convicting him post-impeachment.
Beau acknowledges the obligation of the House to impeach Trump but questions the feasibility of prosecuting him once out of office due to potential pressures and the risk of setting a dangerous precedent.
Concerned about political party persecution, Beau references the dangers of locking up the opposing political party, even if he disagrees with their policies.
He suggests reestablishing ties with the International Criminal Court to hold President Trump accountable for his actions on the border, advocating for a legal process outside the U.S. to avoid traits of dictatorships.
Beau calls for a step away from fascism and towards justice through existing legal mechanisms like the ICC.

Actions:

for voters, activists, political analysts,
Contact elected officials to demand accountability and transparency (implied)
Support initiatives to uphold democratic values and prevent authoritarian tendencies (suggested)
Advocate for international accountability mechanisms like the International Criminal Court (suggested)
</details>
<details>
<summary>
2019-08-01: Let's talk about Grizzlies, Natives, and Liz Cheney.... (<a href="https://youtube.com/watch?v=sd9GhWFfIs8">watch</a> || <a href="/videos/2019/08/01/Lets_talk_about_Grizzlies_Natives_and_Liz_Cheney">transcript &amp; editable summary</a>)

Beau questions the Western way of life narrative, defends native practices, and sheds light on the sacred significance of grizzlies to indigenous communities.

</summary>

"That powerful creature is God."
"Your buddies not being able to go on high dollar trophy hunts is not destroying your way of life."
"The American way of life is not actually extracting and ripping every single thing you can out of the earth and giving nothing back."
"We could learn a lot from the natives."
"In the immortal words of your father, Miss Cheney, go have a good day."

### AI summary (High error rate! Edit errors on video page)

Beau usually avoids talking about native issues but made an exception due to a recent win regarding grizzly bear protection.
A coalition of native groups and conservationists worked hard to protect grizzlies in the Greater Yellowstone area.
Liz Cheney criticized the protection of grizzlies, referring to it as a threat to the Western way of life.
Beau questions the concept of "Western way of life" and implies it may refer more to European colonizers than true Americans.
Beau contrasts the lack of criticism towards Trump's jokes about genocide with the protection of natives, showing a selective approach.
Grizzlies hold deep significance for native people, representing a sacred connection akin to God.
Native practices involving grizzlies, like the bear dance, hold immense historical and spiritual importance.
The use of grizzly claws in necklaces by natives is sacred and not a mere fashion statement.
Despite the spiritual importance of grizzlies to natives, they had to resort to environmental law to protect them successfully.
Beau criticizes the insensitive and dismissive attitude towards native issues and practices in the legal system.

Actions:

for advocates for indigenous rights,
Respect and support indigenous-led conservation efforts (exemplified)
Educate yourself on the cultural significance of animals like grizzlies to native communities (suggested)
Advocate for fair treatment and representation of native issues in legal and political spheres (implied)
</details>
