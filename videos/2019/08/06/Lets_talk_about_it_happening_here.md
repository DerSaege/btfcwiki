---
title: Let's talk about it happening here....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NovuyaBMkBc) |
| Published | 2019/08/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the urgency of the situation, Beau foregoes his usual style of civil discourse to present facts regarding the potential for violent events in the country.
- In light of recent tragedies, people are forgetting the possibility of such events occurring in their own communities as they grapple with feelings of frustration, helplessness, and fear.
- Beau points out a unique turn where individuals who previously spoke out against fascism are now advocating for disarmament.
- Drawing attention to the El Paso shooter's motives and the strategic thinking of those planning violent acts, Beau underscores the need to analyze proposed gun control measures critically.
- Beau dissects various gun control proposals, shedding light on how they may disproportionately affect economically disadvantaged and minority communities.
- He challenges the effectiveness and potential unintended consequences of suggested measures like training requirements, insurance mandates, and ammunition rationing.
- Beau also touches on the legislative side, discussing the feasibility and implications of banning body armor and high-capacity magazines.
- Addressing mental health checks and the nuances of implementing such measures, Beau stresses the importance of a comprehensive and effective approach to preventing violence.
- Beau advocates for solutions focusing on domestic violence indicators, responsible gun storage practices, coping skills education, and improved reporting methods for identifying individuals at risk of violence.
- Lastly, Beau underscores the significance of maintaining a balance between gun rights and public safety, stressing the role of a well-armed civilian population as a deterrent against potential government overreach.

### Quotes

- "They're forgetting it because they're angry, they're frustrated because they don't know what to do, they're afraid."
- "We're chaos driven. We are a violent country. We always have been."
- "It can happen here."
- "Sometimes the deterrent is what stops violence."
- "We don't want violence."

### Oneliner

Beau presents a stark reality-check on the potential for violence in the country, dissecting various gun control proposals and advocating for comprehensive solutions to prevent tragedies.

### Audience

Advocates for comprehensive gun control measures

### On-the-ground actions from transcript

- Advocate for expanded laws addressing domestic violence indicators to prevent firearm access (suggested)
- Promote responsible gun storage practices and education on coping mechanisms (suggested)
- Support improved reporting methods for identifying individuals at risk of violence (suggested)

### Whats missing in summary

The full transcript provides a detailed breakdown of gun control proposals, their potential impacts on marginalized communities, and the importance of maintaining a balance between rights and safety.

### Tags

#GunControl #ViolencePrevention #CommunitySafety #GovernmentOversight #DomesticViolence


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight's going to be a little different.
One, it's going to be long, I imagine.
Two, it's going to be a little bit more straightforward.
I normally try to provoke civil discourse,
give you just a thought.
Tonight, I'm going to give you a whole lot of facts.
Over the last two years, there's a whole lot of us
that have been trying to convince
well-meaning, but misinformed people, that it can happen here.
You're probably one of those, if you're watching this, you're probably one of those that has
been trying to convince people of this.
It can happen here.
Now, because of some tragedies, people are forgetting that.
They're forgetting it because they're angry, they're frustrated because they don't know
what to do, they feel helpless, they feel hopeless, they're afraid.
And they have forgotten that it can happen here.
These same people who a few weeks ago were talking about concentration camps and our
march towards fascism are advocating disarming.
It's a unique turn of events.
Almost like somebody planned it.
Almost like that's the goal.
I want you to think back to that shooting at the mosque, think back to the writings of
the shooter.
What did he say he wanted to do?
Provoke a discussion about the Second Amendment in the United States.
Guy wasn't even in this country, but that was his goal.
Why?
Because he knows what the suggestions for gun control are.
You think the El Paso shooter's motive is much different?
These guys aren't dumb.
They have studied insurgency.
They understand that a security clamp down can typically favor the insurgent.
They understand that most gun control that is being suggested today disproportionately
impacts the poor, people that are economically disadvantaged.
Who does that disproportionately impact as far as race?
They're not stupid.
Please don't mistake them for being stupid.
They knew these suggestions were going to come.
Let's go through them.
Anything about taxes, fees, expensive safes.
What does that do?
If you're wealthy, it doesn't mean much to you, right?
But if you are their target, suddenly it's a whole lot harder to defend yourself.
sure that I can get behind the idea of stripping the means of defense from marginalized communities
who typically can't depend on the cops showing up.
Another one is training.
Now anybody who's watched my videos about purchasing a firearm knows that I am all about
training.
If you're going to purchase a weapon, you better know how to use it.
training is something that was proposed by people who stand to make money off of training
people and those expensive courses who can't afford them.
Think about it logically though, if this is really about mass shootings, why on earth
would you want a well-trained mass shooter?
Doesn't even make sense.
Sure, the safety portion of it, that might stop some negligent discharges, yeah, that's
possible.
I'll give you that.
But I don't understand the idea of wanting mass shooters while trained, or how training
would stop them from being mass shooters.
Doesn't even make sense.
Insurance, that's one that gets thrown out a lot, which was of course proposed by insurance
companies who stand to make money off of it, who does that disproportionately impact?
Their targets.
Who can't afford it?
Low income families are disproportionately minority groups.
They are their target groups.
And you want to take away their right to defend themselves.
want to remove that based on talking points provided by people who stand to make money
off of it.
And then what happens if they don't get the insurance but they keep the firearm?
We've got the school to prison pipeline, now we have the home to prison pipeline.
Hard pass on those.
Now those are the ones that deal with the financial side of it.
What about the legislative side, this is why I was trolling Facebook and Twitter and everywhere
else to get responses because I wanted to know what was out there.
These are the ones that kept popping up.
Ban body armor.
It's not the 1980s, it's not the 1990s.
Body armor is no longer Kevlar, it's a metal plate in a canvas carrier.
You can't ban that and I don't mean that as you shouldn't, I mean you can't, you can get
it anywhere. You can get that type of armor. You can get plate armor anywhere. Any metal
fabricator and he doesn't have to know what he's cutting. Skid plates from a Jeep. That's
a non-starter. It can't be done. All you'll do is waste the ATS time running around trying
to catch people with steel.
Mags, magazines, same thing.
It's sheet metal in a spring, you can't really ban them because they're so easily made.
And keep in mind there are literally hundreds of millions of them already out there unregistered.
Pandora's box is open on that one.
Now I did have somebody ask me an interesting question today that there is an exemption
to this.
When you're talking about 30 round magazines, those are normal.
Those are easy to make, and that is standard capacity, by the way.
When you start talking about drums, drum magazines, yeah, those you could probably effectively
ban.
They're a lot harder to manufacture.
They're a lot harder to manufacture and have them work.
So those you could probably get away with.
You could probably get away with banning that.
But a magazine change takes a second, two seconds.
It's not going to have the impact that you think.
Get rid of AR-15s.
The reason the AR-15 is used so often is because it is the most popular rifle in the United
States.
This would be like banning the Honda Civic because drunk drivers use it the most.
It's just a popular vehicle.
popular rifle.
That's why it gets used so often.
You ban it, get it off the street, it gets replaced with the M14 or the Grand,
which are more powerful.
Um...
Fine, if that won't work, let's get rid of semi-autos altogether.
That's every modern rifle designed in like the last hundred something years, but...
Okay, so
semi-autos are gone.
We're just going to wave a magic wand.
We're not going to talk about the logistics of getting them.
They're just gone.
Everybody volunteered to do it.
They're gone.
So we go back to our roots of mass shootings.
The first one that people think of is Charles Whitman, Texas
Clock Tower.
Killed 16 people, wounded 32.
primary weapon Remington 700 bolt action rifle with no detachable magazine. It's
not the design that does it. It's not the design. That's a talking point. Reality
says otherwise. Okay, gun show loophole. That's kind of a misnomer. It's not a
real thing in a way. Most sellers at gun shows are dealers. Dealers will always
conduct a background check because they're terrified of the ATF. I mean
they're not doing it out of the goodness of their hearts. They understand what
happens if they don't. There are things in some states called private sales that
do go under the radar and no background check is conducted at that point. Private
sales are a part of the firearms community. It would be really annoying to
have to go through a background check each time somebody sold one. At the same time I don't know
that you're going to have huge resistance against it. I don't know that it would do much good
because most people who actually purchased their firearm and attempted to go through a background
check passed. So I don't know that it's actually going to solve any problem but I don't know that
you're going to get a lot of resistance for it. Mental health checks. This is a big one because
Who would be against that?
Well, tell me what one is.
Where everybody's agreeing to it, but nobody knows what they're talking about.
What kind of mental health? Anxiety? Does that preclude you from purchasing a firearm?
It stigmatizes mental health.
Makes people not seek treatment, which may make them more likely
to do something like this.
The other side to this
is that
There probably should be some method of getting people who start to exhibit actual signs of
violence, which not all mental health problems cause violence, getting them away from firearms.
But mental health checks means nothing.
It's very subjective, and we know what happens when subjective laws get put on the books
and law enforcement enforces them.
Who gets hit with them the most?
targets. Rationing ammo, this is a popular one I'm not sure why, only allow
people to buy X number of rounds per month. Most of the rounds, the number of
rounds I've heard, are ridiculously low, 2250. If you are not shooting at least
150 rounds a month you don't need a firearm because you're not going to stay
proficient with it. 12 years ago I could dump a magazine into a 3x5 card while
moving I haven't shot in 12 years I can't do that anymore shooting is a
perishable skill if you don't use it you lose it if you're going to limit
people's ability to train you're just going to increase accidents and as far
as stopping mass shootings through this method I don't so they do what meth
cooks do and send somebody in to buy the Sudafed for them. It's it it's not a real
solution. These as much as the GOP gets criticized for saying it, this these are
thoughts and prayers because there's not a prayer that this is going to work. What
about other countries, something that pops up a lot. Other countries did not
have more guns than people, the United States does. When you talk about a lot of
these other countries, well they got some automatic rifles off the street, they
had 10,000, 100,000. We have tens of millions. Pandora's boxes opened. I also
find it interesting to see this argument coming from people who are normally very
sensitive about different cultures and understanding that comparing the
Japanese propensity and habit of seeking order and following the rules has
has absolutely nothing to do with what the United States does.
We're the exact opposite.
We're chaos driven.
We are a violent country.
We always have been.
Even these solutions, we sit there and people who propose this are like, we want peace,
we don't want guns.
Sure you do.
How do you think any of this is going to get enforced?
And those people will be glorified, which will just help cement that idea that violence
is always the answer. So, what can we do? There has to be some kind of solutions, right?
Domestic violence is a loophole. There is a law saying that if you commit even a misdemeanor
crime of domestic violence, you can't purchase a firearm anymore. It's a good law. It needs
to be expanded and it needs to be closed because there's a loophole in it. If the charge gets
knocked back, you don't have to register. So if you have a compliant DA, well, that's it.
Don't have to worry about it anymore. Domestic violence is a very good indicator of future
violence. Stalking is a very good indicator of future violence. That would work. That would
actually produce results. It would keep the guns out of the hands of the people you don't want to
have them.
OK, so expensive safes are what people are proposing,
blocking out the economically disadvantaged.
What about locks?
And I'm not talking about the breech lock that's
like a chain that goes through the barrel
and so you can't operate the slide that
can be cut with a bolt cutter in 10 seconds.
The type you see in high dollar gun stores,
there's a lock actually over the trigger.
Have it come with the gun.
Have it come with the gun and make it part of the culture.
Make it part of owning one that you're preventing mass shootings by using it.
And I know somebody will say, well, then I'm not ready.
I won't be ready.
Get a handgun with the keypad safe.
Put it in that.
And I mean, if it's in there, you can leave the magazine
and it chambered safety off.
and just practice the keypad.
It's just as fast as getting it out of your dresser drawer,
putting the magazine in, and chambering it.
I mean, of course, that would require you to train.
But as studies have shown us now,
coping skills, teaching coping skills.
That was going to be my video tonight,
was going through and explaining a bunch
of different mechanisms for coping,
because that's what happens in the weeks or months leading up
the incident, people that have childhood trauma, well they snap.
Something happens.
Something typically minor that they couldn't cope with.
We need to start teaching this on a wide level.
It needs to be everywhere.
We need to be able to identify when people go into that crisis mode and there will be
a video coming out about that too.
The next part about this is, though, what are we going to do once we identify them?
We need to have a method of reporting it that doesn't send a SWAT team to their house.
Nobody wants to get their friend killed, or if they already have the weapons, nobody wants
to trigger a firefight.
That's going to be something that law enforcement will have to be involved in.
There's got to be something that can be done to create a situation in which it can
be reported safely for everybody don't name them notoriety after death seems
to be a primary motivator don't name them use the city location age a lot of
people are opposed to this I don't really see the main issue raising it to
21 to help stop school shootings I understand there is one good argument
against it, the people that move out at 18 and can't get a firearm to protect
their home, maybe if they're 18 and out of school, they can get a waiver, I don't
know. But that seems like something that could do some good. We could examine
that at least. And don't surrender. That's what this is. This whole
conversation is surrender. People want to take semi automatics off the street.
they're combat effective. 360 million guns in civilian hands is a deterrent.
It is a deterrent against tyranny. And I know somebody out there is like, you can't fight
a drone or a tank or an A-10 with a rifle. That's not how insurgencies work. And that's
the reason you should really want this deterrent. Because if the deterrent isn't there and you
have to actually act, it's horrible. You don't fight the A-10 or the tank or the drone.
You find their family. That's how insurgencies work and that's why everybody should want
to avoid it. The deterrent of this many weapons in civilian hands helps keep the government
honest. If that was gone, there's nothing that can be done. And then when the government
oversteps its bounds, you don't have the deterrent. You have to actually do it. And nobody who's
ever seen that kind of violence wants that. Because as much as it sounds like fiction,
It can happen here.
We don't want it to.
We need to strive for peace in every way.
We don't want violence.
That's the whole reason we're having this conversation.
Sometimes the deterrent is what stops violence.
It can happen here.
Anyway, it's just a thought.
y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}