---
title: Let's talk about log cabins in Vietnam, tough guys, and solutions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=cHV7N-2y58Q) |
| Published | 2019/08/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the story of Dan Glenn, a Navy pilot shot down in Vietnam in 1966 and held in prison camps until 1973.
- Glenn built a cabin in his mind while in captivity, using ashes and rice water to sketch it out, a coping mechanism that saved lives and preserved sanity.
- Upon returning to the United States, Glenn physically built the cabin in Oklahoma.
- The story of Glenn's coping mechanism is taught at SEER, a survival school for the US military's tough guys.
- Beau explains coping skills, categorizing them into emotion-based (like imagination), problem-based (working harder, learning from mistakes), and others.
- Coping strategies aim to maintain a positive outlook, prevent overreactions, and accept situations objectively.
- Having a positive social support network is vital in coping effectively.
- Recognizing successes, learning from mistakes, developing self-control, and maintaining oneself are part of good coping strategies.
- Beau points out a pattern of early childhood trauma, lack of coping skills, and a triggering incident leading to mass shootings.
- The root solution to preventing mass shootings lies in developing effective coping strategies, addressing the motive rather than the means (such as limiting access to weapons).
- Beau stresses that education and understanding are key to implementing this solution effectively.

### Quotes

- "He built a cabin. Not physically, he built it in his mind."
- "The toughest people in the U.S. military are taught coping skills at one of the toughest schools in the military."
- "Good ideas normally don't require force, they require just education, just a thought."

### Oneliner

Beau introduces Dan Glenn's story of building a cabin in his mind as a coping mechanism during captivity, stressing the importance of effective coping strategies in preventing tragedies like mass shootings through education and understanding.

### Audience

Individuals, educators, mental health advocates

### On-the-ground actions from transcript

- Learn about different coping strategies and mental health practices (suggested)
- Educate others on the importance of coping skills and positive social support networks (implied)

### Whats missing in summary

The full transcript provides a detailed account of the importance of coping mechanisms in dealing with trauma and preventing tragic outcomes like mass shootings through education and understanding.

### Tags

#CopingSkills #MentalHealth #Prevention #Education #CommunitySafety


## Transcript
Well, howdy there, Internet people.
It's Bo again.
So today we're going to start off talking about a guy named Dan Glenn.
If you are the alumni of a certain institute of higher education, well, you probably already
know this story, but most people don't.
Glenn was a Navy pilot.
He was shot down in 1966 over Vietnam.
He was captured by the Vietnamese and was in prison camps until 1973, six years.
He was at the Hanoi Hilton.
He's been a roommate at 18.
If you're not familiar with these institutions, picture the worst thing you can imagine.
It's worse than that.
These were very, very rough places.
You know what he did when he was there?
He built a cabin.
Not physically, he built it in his mind.
If he could find some paper, he'd use ashes and rice water to sketch it out.
He'd destroy it immediately because that's the type of thing that could get you tortured.
He'd help other prisoners design cabins of their own, picture it every day, brick by
brick, plank by plank, figuring out where it was going to go, the big open floor plan,
the loft upstairs.
When he got back to the United States, he built it.
Oklahoma, it's in Oklahoma, up in the woods.
This story has saved lives, it has certainly preserved people's sanity.
I'm hoping that it can save more.
The story is taught at SEER, if you go to the physical installation, if you go to the
actual school, you'll hear this.
This here is a survival, evasion, resistance, and escape.
It's the school that the US military sends its tough guys to, guys that have a high probability
of being captured.
If you go to this school, you are a certified tough guy, right?
Now the reason I'm starting off with this is because our views of masculinity in this
country may lead some people to dismiss what I'm about to say, but understand the toughest
people in the U.S. military are taught coping skills at one of the toughest schools in the
military.
It's what can keep them alive.
Now what he did, building that house in his mind, that is an emotion-based coping skill.
That's imagination, flights of fantasy.
Another version or another way that falls into the emotion-based is venting or blaming.
Not all coping skills are good.
There's also problem-based, and that's where you work harder.
You learn from your mistakes.
ask for help, that type of thing. There's as many different coping mechanisms as there
are people. Some are good, some are bad. There are a lot of coping strategies and they mainly
revolve around maintaining a positive outlook in some way. Preventing yourself from overreacting
to situations. Taking a hard look at what just happened at this event. Is it really
that bad? Is it really that horrible? You have to look at it objectively. Are you
in the Hanoi Hilton? Probably not. Talking about the problems, accepting yourself,
your failures, accepting others, accepting the way the world is and that it isn't
always fair. Having a positive social support network, positive being the key
word there. If you're dealing with grief and you start shooting up around your
friends who are also doing the same thing, that's probably not a positive
social support network. Positive is a big part of that. Learning from your mistakes
so you don't repeat them and understanding that they happen. Accepting
that. Recognizing your successes. Taking a moment and saying, ha, I did that. Helps you
build some confidence. Gives you a win every once in a while. Learning a little bit and
developing some self-control and self-discipline. These are all good coping strategies. And
in maintenance, maintaining yourself. You know, and what I do, we look at the most horrible
things imaginable every day. It's our job. And, you know, one of my colleagues, she does
yoga constantly. Maintenance. Some people meditate. Some people have a weekly poker game. Some
Some people make videos for the internet, it's a late event.
The reason I'm saying all of this is because there is now that confirmed pattern, early
childhood trauma, and then a lack of coping skills.
And then somewhere there's that incident, it's not normally a big thing, you lose your
job, a girl rejects you, and that's the catalyst because you can't cope with it.
We don't have these strategies.
So you go down that road.
Everything takes over.
That overreaction comes.
And then people die.
That's the pattern of these mass shooters.
Now everybody is saying, well we need solutions.
This is the solution.
Yeah it's not sexy.
It's nowhere near as cool as calling for legislation.
But this is the solution.
All the other stuff.
It's addressing the means.
It's addressing the means.
Well we can limit their access to this, this, this.
There's always going to be a way around it.
Completely eliminate guns, fine, bombs.
There's always going to be something else.
This eliminates the motive, and if there is no motive, it doesn't matter.
The means can be anywhere, because there's no desire.
This is the solution.
It's not a talking point because you can't make money off of it.
Information is out there freely.
Google it, please, everybody, today, learn a little bit about it.
Good ideas normally don't require force, they require just education, just a thought.
Anyway, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}