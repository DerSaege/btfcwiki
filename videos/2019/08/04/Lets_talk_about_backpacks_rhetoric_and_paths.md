---
title: Let's talk about backpacks, rhetoric, and paths....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=SxmMBfVFvX8) |
| Published | 2019/08/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recent days have been serendipitous for him, with news stories allowing him to make wider points.
- A native woman asked about bulletproof backpacks out of fear for her child being mistaken for a Latino due to the current climate in America.
- Beau advised against bulletproof backpacks, suggesting using a laptop bag with a steel plate instead.
- Beau receives questions related to his background but is selective in answering due to the dual-use nature of the information.
- He questions how the President can sleep at night knowing his rhetoric has led to shootings and deaths.
- Beau points out the President's role in dividing rather than uniting the country.
- He urges the President to change his rhetoric to save lives and prevent further violence.
- Beau expresses disappointment that as a small-time YouTuber, he is more conscientious about his words than the President.
- He calls on the media to press the President on how he can sleep at night given the consequences of his rhetoric.
- Beau stresses the need for the President to adjust his rhetoric and work towards unifying the country, as the divide is widening and leading to deaths.

### Quotes

- "How do you go to sleep at night knowing that your words killed people and they did?"
- "Dead bodies in Walmart built the wall. That's your legacy."
- "You can change your rhetoric and you can save lives."
- "I am a small-time YouTuber and I am more conscientious about what I say, than the President of the United States."
- "How does he sleep?"

### Oneliner

Beau questions the President's ability to sleep at night, holding him accountable for the consequences of his divisive rhetoric and urging a change to save lives.

### Audience

Media, concerned citizens

### On-the-ground actions from transcript

- Press the President to change his rhetoric and work towards unifying the country (implied)

### Whats missing in summary

The full transcript provides a detailed and impassioned plea for accountability and change in political rhetoric to prevent further harm and division.

### Tags

#Accountability #PoliticalRhetoric #Unity #Change #MediaAttention


## Transcript
Well, howdy there, internet people, it's Bo again.
The last few days have been very serendipitous for me, anyway.
Not for the people involved in the news stories.
But for the last few days, if I wanted to talk about something,
it was showing up in the news.
I often use a news story to make a wider point.
And this little arc that I've been on lately, every day something came up to bring the story into focus and it all
culminated today.  So a few days ago, I was asked by a native woman about bulletproof backpacks,
because she's worried that her kid's going to get mistaken for a Latino and get shot because of the climate in today's
America.
And I told her, I was like, you know, those bags are normally rated for handguns, most
of them, and in most of the recent shootings they've been using rifles, they're not really
going to do much good.
You'd be better off getting like a laptop bag and putting a steel plate in it.
I get asked questions like that a lot.
Questions that are related to my background, I guess.
I don't mind answering, but I don't answer all of them, because I'm very conscientious
about the fact that a lot of this information is dual use. It can be used to hurt people
too. If you've ever asked me a question like this and I haven't answered, that's why.
It's because whatever it was that you asked could be used to harm people. And I understand
that's probably not your motivation, but I don't know all of you, so I don't answer.
Because I can't imagine something I said being the cause of innocent people getting
hurt. I've got enough trouble sleeping. I don't need that on my hands. So that kind
of brings me to my next question. Mr. President, how do you sleep at night? What number shooting
is this that is directly tied to your rhetoric? Or names you by name, the shooter named you
you by name in their writings?
It's not a rhetorical question either.
Seriously, there's a lot of people out here with blood on their hands that have trouble
sleeping.
I mean, you don't even drink.
How do you do it?
How do you go to sleep at night knowing that your words killed people and they did?
Do you not realize that people are going to act on what you say?
These people in Walmart are dead because of you, because of your rhetoric.
And it's not the first time, it's not even the second time, or the third time, or the
fourth time, or the fifth time.
There are piles of dead bodies that have your name on them.
You know, all American presidents in recent memory have blood on their hands, but it's
normally not the blood of innocent American shoppers at Walmart.
You chose to divide the country rather than unite it.
You didn't create this divide, you just exacerbated it.
played on it, you prayed on it. And see, and that's the thing, I don't even know
that you're a bigot. I really don't. There's a part of me that thinks you're
just, you're just playing on other people's racism to keep your base
motivated. I don't know though. The one thing I do know about you is that you
care about your legacy. Maybe more than anything. You care about your name.
name.
This is your legacy.
Dead bodies in Walmart built the wall.
That's your legacy.
When history records you, they're going to record you as the president who incited
terrorism against his own people.
That's going to be your legacy.
That's who you are.
what it's going to be.
And it's because you chose to divide.
You charted a path to division.
You used rhetoric that inflamed people, invaders, right?
It's upsetting.
upsetting on a number of levels. You got a few hundred days left in office, you can
change this. You can change your rhetoric and you can save lives. You can come out
and say no, no, okay, they're people looking for a better life. They're not
invaders, they're not diseased, they're not lesser, they're not other, they're
just people and that would save lives. You want to make America great then I'd
be happy if you just made it to where it was safe to shop again. These are your
bodies, your bodies. I am a small-time YouTuber and I am more conscientious
about what I say, than the President of the United States.
Sad.
That needs to be the question.
From everybody in the media, from everybody in the media, until we get an answer.
How does he sleep?
This rhetoric, it surfaces over and over again.
And we read it in the writings of people who gunned down innocents.
And the media is just giving them a pass on it.
They're not calling this.
I guess it's not important.
Sir, you really need to adjust your rhetoric.
You need to chart a course to bring this country back together.
to close the divide that you pushed open.
It was always there, it was, it was right under the surface, but it was healing.
It's not anymore, it's getting worse and worse.
People are dying because of what you say to get votes.
You have American citizens trying to figure out how to buy bullet resistant material for
their kids to protect them from people motivated by your words.
This is making America great.
This is your legacy, sir.
Anyway, it's just a fault.
Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}