---
title: Let's talk about puppets on parade and the important part of this election....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=4Y1RmIMF_b4) |
| Published | 2019/08/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing the recent debates, it wasn't surprising as most candidates' positions were anticipated based on their campaign contributors and past actions.
- Some candidates like Yang, Williamson, and Tulsi stand outside the political norm, but the majority were predictable.
- Beau stresses the importance of the upcoming election not just to remove Trump from office but also to steer away from totalitarian regimes and fascism.
- Holding elected officials accountable and veering clear of dictator-like traits are imperative for a functioning representative democracy.
- Beau is surprised by Joe Biden owning his bad judgment regarding the Iraq war vote but believes that such significant errors should disqualify politicians from higher office.
- He criticizes the calls to prosecute President Trump, citing the unlikelihood of a Republican Senate convicting him post-impeachment.
- Beau acknowledges the obligation of the House to impeach Trump but questions the feasibility of prosecuting him once out of office due to potential pressures and the risk of setting a dangerous precedent.
- Concerned about political party persecution, Beau references the dangers of locking up the opposing political party, even if he disagrees with their policies.
- He suggests reestablishing ties with the International Criminal Court to hold President Trump accountable for his actions on the border, advocating for a legal process outside the U.S. to avoid traits of dictatorships.
- Beau calls for a step away from fascism and towards justice through existing legal mechanisms like the ICC.

### Quotes

- "We need to move back towards representative democracy and away from totalitarian regimes."
- "Bad judgment cost almost half a million lives; precludes you from higher office."
- "Locking up the opposing political party is dangerous."
- "Reestablish ties with the International Criminal Court to hold President Trump accountable."
- "Turn away from fascism by using existing mechanisms for accountability."

### Oneliner

Beau stresses the necessity of moving back towards representative democracy, holding officials accountable, and avoiding traits of dictatorships, urging caution in prosecuting Trump and advocating for international accountability mechanisms.

### Audience

Voters, activists, political analysts

### On-the-ground actions from transcript

- Contact elected officials to demand accountability and transparency (implied)
- Support initiatives to uphold democratic values and prevent authoritarian tendencies (suggested)
- Advocate for international accountability mechanisms like the International Criminal Court (suggested)

### Whats missing in summary

The full transcript provides deeper insights into the risks of political persecution, the importance of international legal mechanisms for accountability, and the need to uphold democratic values in the face of authoritarian threats.

### Tags

#Election #Accountability #RepresentativeDemocracy #Fascism #InternationalJustice


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight, we're gonna talk about Puppets on Parade,
the debates.
There weren't a lot of surprises in it.
We kinda knew what everybody was gonna say going into it.
We've seen their campaign contributors,
we know who backs them, we know their positions,
we know what they're gonna say.
There are a few wild cards, you've got Yang,
and y'all, you guys can stop messaging me.
I will talk about him in depth at some point, just not tonight.
You've got Yang, you've got Williamson, you've got Tulsi.
You have some that are outside of the political norm.
You do.
But most, we knew what was coming.
I think that this is a very important election and not just for the reason of we have to
get Trump out of office.
That's a priority.
And I think it's very important that that happens, but there's something even more important.
We hit that slide.
We took that turn towards fascism, towards totalitarian regimes.
We need to do a U-turn.
We need to get away from that.
If we're going to have a representative democracy, we have to move back towards that.
That requires us to hold our elected officials accountable.
And to move away from any traits associated with those two-bit dictatorships.
It's very important that we do that.
In that light, two things surprised me.
Two things worth talking about.
The first was bad judgment.
Joe Biden owning his bad judgment over the Iraq war vote.
Okay, good for you.
There aren't a lot of politicians that would do that.
They'd blame it on anything else.
Anything other than themselves.
You owned it, good.
Now drop out of the race.
Bad judgment is, man, I shouldn't have had that ninth shot.
Bad judgment is seeing your ex after six months and going, man, what was I thinking?
Bad judgment is a fling with an intern.
The bad judgment you're talking about killed almost half a million people.
Killed almost half a million people.
I'm not sure that bad judgment is the right term.
And I'm not just holding Biden to this standard.
I don't think anybody that supported that war should still be in office.
They should be gone.
I do believe that judgment that costs the lives of almost half a million people should
preclude you from higher office.
The other thing that caught my ear wasn't really a surprise because, big surprise, the
cop wants to lock someone up.
The calls to prosecute President Trump.
I've had a lot of people ask me to talk about the Mueller thing.
I haven't done it because of what I'm about to say.
Regardless of what's right or wrong, it was never really going to happen.
We have a Republican Senate.
We have a Republican Senate.
Their duty is to set party politics aside, but they wouldn't.
So even if the House impeached, there's no way the Senate would convict.
So that portion of it was always a show.
I do believe that the House has an obligation to impeach.
I think they should do that.
I think it's very important that they do.
At the same time, I can understand politically why they're not doing it.
I don't think they're right, but I understand the motivation.
Now the next step of that is prosecuting him once he's out of office.
Okay, this is the way this plays out, more than likely.
So they try to get the indictment, which there's enough evidence.
If this was a normal person, oh yeah, oh yeah.
But it's not a normal person, it's the President of the United States.
And even once he's out of office, he's still the former president of the United States.
You're going to have his allies putting pressure on DOJ to make sure it doesn't happen.
Beyond that, you're going to have those people who are just interested in protecting the
institution of the United States, trying to make sure it doesn't happen.
Because it's an embarrassment.
I understand Trump alone is an embarrassment, but the idea of a former president sitting
in a criminal courtroom as a defendant, that's embarrassing.
But let's say it happens, for the sake of argument, they get the indictment.
Then what?
You have to find a jury with 12 people and not a single one of them can be a Trump supporter.
Because if one person says not guilty, what's happened?
Yeah, Trump walks, but beyond that, what's happened?
It set real precedent that the occupant of the Oval Office is truly above the law.
That's dangerous, that is very dangerous.
Now, beyond that, we're supposed to be moving away from the traits of two-bit dictatorships.
Locking up the opposing political party, that's one of those traits.
It's no secret I am not a fan of Hillary Clinton.
Do not like her policies.
I don't like a whole lot of things.
However, when I heard lock her up, lock her up, I cringed.
That's what it reminded me of.
It reminded me of all of those countries where when the new guy comes in, he locks everybody
up from the old party.
Also very dangerous.
And teetering back and forth with that as a campaign promise, eventually somebody's
going to do it.
Now does that mean that I think Trump should walk?
Oh no, not at all.
I think a priority for the first person, or the next person, that takes the Oval Office,
I think one of the first things they should do is reestablish ties with the International
Criminal Court.
Because I believe the ICC has a very strong case against the President for his actions
on the border.
And that's the purpose of the ICC.
They're there to prosecute when the nation in question is unable or unwilling to do so.
I understand the desire to see President Trump in an orange jumpsuit, I really do.
Does it have to be in the U.S.?
Can it be at the Hague?
Because then we can still move away from the traits of these dictatorships.
We can break away from that rhetoric.
We can turn around and move away from fascism rather than just taking baby steps towards
it.
we can use the mechanisms that exist for people like him.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}