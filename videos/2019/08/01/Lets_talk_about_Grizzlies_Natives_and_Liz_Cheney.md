---
title: Let's talk about Grizzlies, Natives, and Liz Cheney....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=sd9GhWFfIs8) |
| Published | 2019/08/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau usually avoids talking about native issues but made an exception due to a recent win regarding grizzly bear protection.
- A coalition of native groups and conservationists worked hard to protect grizzlies in the Greater Yellowstone area.
- Liz Cheney criticized the protection of grizzlies, referring to it as a threat to the Western way of life.
- Beau questions the concept of "Western way of life" and implies it may refer more to European colonizers than true Americans.
- Beau contrasts the lack of criticism towards Trump's jokes about genocide with the protection of natives, showing a selective approach.
- Grizzlies hold deep significance for native people, representing a sacred connection akin to God.
- Native practices involving grizzlies, like the bear dance, hold immense historical and spiritual importance.
- The use of grizzly claws in necklaces by natives is sacred and not a mere fashion statement.
- Despite the spiritual importance of grizzlies to natives, they had to resort to environmental law to protect them successfully.
- Beau criticizes the insensitive and dismissive attitude towards native issues and practices in the legal system.

### Quotes

- "That powerful creature is God."
- "Your buddies not being able to go on high dollar trophy hunts is not destroying your way of life."
- "The American way of life is not actually extracting and ripping every single thing you can out of the earth and giving nothing back."
- "We could learn a lot from the natives."
- "In the immortal words of your father, Miss Cheney, go have a good day."

### Oneliner

Beau questions the Western way of life narrative, defends native practices, and sheds light on the sacred significance of grizzlies to indigenous communities.

### Audience

Advocates for indigenous rights

### On-the-ground actions from transcript

- Respect and support indigenous-led conservation efforts (exemplified)
- Educate yourself on the cultural significance of animals like grizzlies to native communities (suggested)
- Advocate for fair treatment and representation of native issues in legal and political spheres (implied)

### Whats missing in summary

The emotional impact of Beau's defense of native practices and criticism of dismissive attitudes towards indigenous issues.

### Tags

#IndigenousRights #GrizzlyBears #Conservation #CulturalSignificance #EnvironmentalJustice


## Transcript
Well, howdy there internet people, it's Bo again.
I don't normally talk about native issues
because I get a little testy, okay?
We're gonna talk about one today
because it was a win for the most part.
The grizzly, effective Tuesday, last Tuesday,
is protected again in the Greater Yellowstone area.
It's back on the Endangered Species Act list there.
This was the result of some very hard work
by a coalition of native groups,
and I guess there were some conservationists
thrown in there, too, that's a win.
That is a win.
Liz Cheney, the third highest-ranking member
of the Republican Party, representative from Wyoming
at large, said that it was the result
of excessive litigation by people intent
destroying our Western way of life. Oh man. Oh man. There's a whole lot to talk
about there. Western way of life. I don't know what that means. I don't know what
that means. See, you must be talking about the European way of life. The
colonizers way of life. Because protecting this animal is certainly the
American way of life." Really interesting use of words there. Good way to exclude
them, because they're not your people, right? If you consider them people.
I notice you blasted Elizabeth Warren for her claim, which whatever. And yet, great
protector of the natives that you are, you said nothing about Trump's jokes
about the genocide. I guess we see why now. They're not your people. They're not part
of your way of life. It says a whole lot. You know, if you want to be part of the European
way of life, you could always go back to, see, I'm not going to do that. I'm not going
to do what your boss did and tell you to go back to where you came from. Because this
This is a teachable moment.
See the grizzly to the natives out there, that's not just some animal, that's God.
For lack of a better word, that's God.
That powerful creature is God.
There's not a lot of information out there about the bear dance, not really, but there's
There's a whole bunch out there about the ghost dance.
And they share a lot of the same elements.
You might want to look into that just to get a sense of how important this is.
There's a lot of history there.
You know they used to make necklaces, probably still do, out of the claws.
a whole lot of them in white hands though, because they were sacred. It wasn't a fashion
statement. That bear being so powerful, the claws offered in protection, was like a crucifix.
That's how important this animal is. Borders on a religious duty to them. But see, they've
They've learned long ago that their ways and their practices mean nothing in a courtroom,
religious or not, so they used environmental law, and they won, and somehow that's destroying
your way of life.
Your buddies not being able to go on high dollar trophy hunts is not destroying your
way of life.
Destroying somebody's way of life, well that's like snatching their kids, throwing them in
another facility changing their ways, making them speak English. You're still
doing it. Times haven't changed that much I guess. Just a different group of people
now.
Your choice of words was sickening. And it's not, you're not going to play this
off as a mistake. Because I mean the court case is called Crow Tribe. It's not
It's not like you didn't know, you're just making it clear they're not your people.
And that's fine.
That's fine.
The American way of life is not actually extracting and ripping every single thing you can out
of the earth and giving nothing back.
Shouldn't be anyway.
We could learn a lot, people that look like you and me, we could learn a whole lot from
the natives. We really could. They could teach us a whole lot if you let them.
Anyway, it's just a thought. And in the immortal words of your father, Miss Chaney, go have
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}