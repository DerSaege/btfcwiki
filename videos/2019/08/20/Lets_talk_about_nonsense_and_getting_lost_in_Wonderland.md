---
title: Let's talk about  nonsense and getting lost in Wonderland....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=RA3H6CaGd1E) |
| Published | 2019/08/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau finds himself in Wonderland after falling down a hole with his dog, recounting the bizarre events unfolding.
- The Jack of Diamonds has taken power through multiple marriages, mirroring the Queen of Hearts' style of ruling by beheading officials.
- The leader has initiated a trade war with the Land of Oz, causing economic distress and material shortages in Wonderland.
- Communication is chaotic, with messages delivered by borrowed birds from Snow White, leading to confusion and paranoia.
- The Jack of Diamonds blames foreigners for job losses, inciting fear and deflecting from the real issues like automation.
- In an attempt to help the poor, royal funds were given to the rich, who hoarded the money instead of aiding others.
- Paranoia and witch hunts are rampant, prompting extreme security measures like contacting Elsa to build an ice wall.
- Destruction of windmills and plans for clean coal reveal misguided environmental policies and uninformed decisions.
- Attempts to purchase grain acres and suppress riots show a disconnect from reality and a reliance on authoritarian tactics.
- The presence of a fearful lion immigrant illustrates misplaced fears of invasion by unarmed individuals seeking safety.

### Quotes

- "Must be a tradition here in Wonderland, I'm not really sure."
- "He tends to blame foreigners for the loss of jobs rather than automation."
- "Currently in the palace, Paranoia is running deep."
- "A strange place here. It is a strange place."
- "If I had a world of my own everything would be nonsense."

### Oneliner

Beau narrates the bizarre events in Wonderland under the rule of the erratic Jack of Diamonds, showcasing economic turmoil, communication chaos, and unfounded fears of invasion.

### Audience

Observers, Wonderland residents

### On-the-ground actions from transcript

- Organize community meetings to address economic concerns and propose solutions (suggested)
- Support local unions and workers affected by tariffs through donations or advocacy efforts (exemplified)
- Educate fellow citizens on the importance of fact-checking and critical thinking in the face of misinformation (implied)

### Whats missing in summary

The full transcript provides a detailed allegory on current political and social issues, urging viewers to critically analyze leadership and societal trends.

### Tags

#Wonderland #Satire #PoliticalCommentary #SocialIssues #CurrentEvents


## Transcript
Well, howdy there, internet people, it's Bo again.
Yeah, so today I was out with my dog,
we were chasing this rabbit,
fell down this hole,
and things have just gotten curiouser and curiouser
since then.
I know I normally update you guys
on what's going on in the United States.
I can't do that because I don't know what's happening there,
but I'm gonna tell you what's going on here in Wonderland.
Maybe that'll help.
The Jack of Diamonds has risen to the throne
after his third or fourth marriage
in an attempt to get there.
Much like the Queen of Hearts,
he runs around the palace
going to different government departments
screaming off with their heads.
Must be a tradition here in Wonderland,
I'm not really sure.
He started a trade war with the Land of Oz
and I spoke to the chief of the local Hatter's Union today
and he said that the tariffs are basically
ruining the economy sending us into recession and that he's unable to get
the materials they need to work. I mean he's just outright mad. The Jack of
Diamonds has the most expansive communication system in history but for
some reason he has borrowed birds from Snow White and tends to deliver messages
through those. Those messages are short, rambling, paranoid, delusional and most
times incoherent, almost as if they were written by a raven at a writing desk. He
tends to blame foreigners for the loss of jobs rather than automation and since
most of his supporters don't read anything that is longer than a tweet
that they tend to believe him. Now there was a problem near the palace with a
swamp and he did manage to drain that but he turned all the alligators loose
inside the palace where they are currently feasting on the treasury. In an
attempt to help out the little guy, he dispersed royal funds to the rich in the
hopes that they would throw coins at poor people for fun. They didn't. They of
course just kept the money for themselves. Currently in the palace,
Paranoia is running deep. There are witch hunts everywhere, if you ask the Jack.
part of that paranoia has led to an obsession with national security and he
has contacted Elsa to come in and build a wall of ice around the country and he
told her that Dora the Explorer would pay for it we're still not sure how
that's gonna play out in a move that surprised a lot of people he has torn
down Don Quixote's famous windmills, because apparently they cause cancer,
I guess, and they're going to be replaced with clean burning coal. In another
surprise move, he's attempting to purchase grain acres, even though he has
been told repeatedly it is not for sale. A couple years ago there was a riot over
We're at Charlotte's Web and recently one of the participants in that riot, well he
attempted to attack a religious community center but I'm sure there are still fine people
on both sides.
Incidentally at the same time, the locals seem to have trouble telling the difference
between freeze and antifreeze.
I guess they do the same thing, I don't know.
And earlier today I ran into a lion immigrant.
He came over from the land Oz and for some reason he's just terrified of some invasion
of unarmed men, women, and children who are apparently not part of a military and not
looking to cause violence.
But it's an invasion of some sort and he also seems to believe there's some kind of magical
line they can get in and then they'll be okay if they go through this line that
doesn't actually exist. It's a strange place here. It is a strange place. I mean
you don't I don't understand how these people believe this stuff to be honest.
The Jack of Diamonds is a lot like the Queen of Hearts with her croquet. He has
proclaimed himself master of 4d chess. I got to play chess with him earlier today.
He can't even play regular chess to be honest. If I had a world of my own
everything would be nonsense. Nothing would be what it is because everything
would be what it isn't.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}