---
title: Let's talk about PP and life after Title X....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=dcGT848vJDY) |
| Published | 2019/08/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the situation with Title X funding and Planned Parenthood, pointing out misconceptions and facts.
- Describes how Planned Parenthood withdrew from Title X programs due to a rule prohibiting abortion referrals in exchange for funding.
- Clarifies that the $60 million given up by Planned Parenthood was not used for abortions but for services like STD screenings and birth control under Title X.
- Counters the idea of Planned Parenthood being defunded by putting the $60 million loss into perspective compared to their overall revenue of $1.67 billion.
- Reveals that Planned Parenthood received $563 million in tax dollars last year, the majority of which came from Medicaid and was not used for abortions.
- Notes that Planned Parenthood will need to increase fundraising by 10% to make up for the loss of $60 million.
- Urges individuals to donate or purchase merchandise to support Planned Parenthood's health centers.
- Emphasizes the various healthcare services provided by Planned Parenthood beyond abortion, including breast exams and birth control.
- Points out the irony that reducing access to birth control can lead to more unplanned pregnancies and, consequently, more abortions.
- Explains the importance of private donations in keeping Planned Parenthood health centers operational and serving underserved communities.

### Quotes

- "Well golly gee whiz, it seems like they do things other than abortion."
- "Why abort when adoption is an option?"
- "Congratulations, you played yourself."

### Oneliner

Beau breaks down misconceptions around Planned Parenthood's funding and services, urging support to maintain vital healthcare access beyond abortion.

### Audience

Advocates, Donors, Supporters

### On-the-ground actions from transcript

- Support Planned Parenthood by donating directly or purchasing merchandise (suggested).
- Increase donation to Planned Parenthood by 10% to help maintain their health centers (suggested).
- Educate others on the misconceptions surrounding Planned Parenthood's funding and services (implied).

### Whats missing in summary

Full context and depth of Beau's breakdown on Planned Parenthood's funding and the potential impact of misconceptions.

### Tags

#PlannedParenthood #TitleX #Funding #Healthcare #Misconceptions


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we got to talk about Title X funding, revenue,
a whole bunch of facts and figures, campaign promises,
all kinds of things, because what we have
is a whole bunch of people who are extremely
passionate about something they know nothing about.
And that's how we wound up in this situation.
People listening to talking points
instead of actually looking at the numbers, which
readily available. I guess one of the biggest misconceptions coming out right
now is that somehow Donald Trump has kept a campaign promise by defunding
Planned Parenthood. Okay, so what really happened was Planned Parenthood withdrew
from Title X programs. In the course of that they're giving up 60 million
dollars. They withdrew from it because a rule from Health and Human Services says
that you can no longer provide abortion referrals. So if somebody walks in and
says I want to have an abortion, if you're taking this money you are
prohibited from telling them where to get it. It's that simple. They gave that up.
Now they get 60 million dollars, none of which has ever been able to be used for
abortion to provide Title X services, which are STD screenings, HIV screenings, birth
control, stuff like this.
They get $60 million because they provide 41% of Title X recipients with services.
Now is that $60 million defunding Planned Parenthood?
I mean that sounds like a lot of money. It would be a lot of money if you weren't
talking about an organization with a revenue of 1.67 billion dollars. Billion
with a B. That's 60 million dollars that accounts for about three and a half
percent of their revenue. Pretty sure the lights will be able to stay on. Well at
least they're not getting tax dollars now. Wrong again. Wrong again. Last year
they got $563 million in tax dollars. So you take out the $60 million you got
$503. How did they get that money? Via Medicaid. Medicaid with the exception of
cases where the mother's life is at risk or incest, rape, those kinds of things.
Well, that money is prohibited from going to abortion as well.
They got half a billion dollars for things other than abortion.
Well golly gee whiz, it seems like they do things other than abortion.
I mean, that kind of stands to reason, right?
Half a billion dollars, that's a lot of healthcare, especially when you're talking about procedures
that are relatively inexpensive and we'll get to what that healthcare is here in a minute.
So what is Planned Parenthood going to have to do to recoup this $60 million?
They're going to have to up their fundraising goal by 10%.
Planned Parenthood gets $630 million from private individuals and entities and disclosure
notice I'm one of them. Why? Because these people actually looked into what Planned Parenthood
does, and they realized that the talking points about it being an abortion mill aren't true.
So and just so you know, these t-shirts, they're available in our merchandise shop, half of
this money goes to Planned Parenthood. When you get one of these, half the profits go
to Planned Parenthood. Or you can donate yourself. If you normally donate to Planned Parenthood,
you need to up your donation by 10%. And then these health centers can stay open. What do
these health centers do? Last year alone, Planned Parenthood provided 570,000 breast
That's always one of those things.
Why abort when adoption is an option?
Yeah they do that too.
The ultimate irony in all of this
is that
as far as women in need
Planned Parenthood provides one-third of them
with birth control services
through these health centers.
What happens
with less birth control?
there are more what? Unplanned pregnancies, which means there are more abortions. Congratulations,
you played yourself. More than half of their health centers are in rural or underserved
communities. In counties where there is a health center, their health center
provides services to 68%. That is why all of a sudden the Department of Health
and Human Services is freaking out because they're shocked. They probably
didn't know the numbers either and when this rule came out they felt certain
That Planned Parenthood would go along with it. They won't. They didn't. So they're saying that they're abandoning their
patients, their  obligations to serve their patients.
No, actually they're intending on keeping these health centers open, just doing it without the federal money.
So, to recap,
none of this money ever went to abortion to begin with. That was your local politician getting you mad
had to get you to vote for him, pretending to be a Christian, I guess.
And then from there, the money's gone, the evil abortion provider, they're going to continue
providing the services.
And what do they need to do that?
They have to raise 10% more from private individuals.
That's what needs to happen in order to do this.
The end result of this, if Planned Parenthood gets defunded the way people think and they
were to actually shut down their health centers, there would be more abortions, not less.
If you're going to be passionate about something to the point where you want to propose legislation,
Maybe educate yourself on it.
Just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}