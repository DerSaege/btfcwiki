---
title: Let's talk about the supporting the troops, the Hmong, and Iraq....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=WjwASgtKRMk) |
| Published | 2019/08/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the concept of supporting the troops in modern times, beyond bumper stickers and slogans.
- Emphasizing that supporting troops primarily means the local populace in areas of operations.
- Providing a historical example of the Hmong people's betrayal after assisting the U.S. during the Vietnam War.
- Criticizing the abandonment of Iraqi nationals who helped the U.S., facing visa backlogs and imminent danger.
- Pointing out the plight of interpreters waiting for visas, with a drastic decline in approvals.
- Expressing concern for individuals risking their lives and being left behind due to changing political tides.
- Raising the issue of child soldiers and questioning the widespread nature of this practice.
- Drawing parallels between past mistakes, like Vietnam, and the current abandonment of allies.

### Quotes

- "The reality is, in modern war, your support doesn't matter."
- "They are being left to die. That is what is going to happen to them."
- "We learned nothing from Vietnam. We are making the same mistakes again."

### Oneliner

In modern warfare, supporting troops means prioritizing local communities, not slogans, while history repeats the betrayal of allies risking their lives.

### Audience

Advocates for human rights

### On-the-ground actions from transcript

- Advocate for expedited visa processing for Iraqi nationals and interpreters (implied)
- Support organizations aiding at-risk allies and interpreters (exemplified)

### Whats missing in summary

The emotional depth and personal stories shared by Beau can be best experienced in the full transcript. 

### Tags

#SupportTheTroops #BetrayalOfAllies #VisaProcessing #HumanRights #ModernWarfare


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about supporting the troops.
Because that is the American thing to do, we support the troops.
We do that, it's important.
We've got shirts that say it, bumper stickers on our trucks, we support the troops.
What does it mean?
In a modern war, what does it mean?
It's not like we went out and got a war job.
It's not World War II anymore.
Doesn't mean anything, not really, not today.
Yeah, there are some people who actively support troops and returning soldiers, but there's
a whole lot more bumper stickers.
It's a slogan, and it was a slogan designed to stop the anti-war effort.
That's really what it was, so you can get people to say, well, I oppose the war, but
I support the troops, which means I can't oppose the war too loudly because that would
undermine the troops, sloganeering at its best.
The reality is, in modern war, your support doesn't matter.
The support that matters is over there, wherever there is, wherever there is.
local populace in the area of operations. That's the support that matters. That's
who needs to support the troops. The problem is the US has a really bad
habit of selling those people out once the war is over. Good example is the
H-M-O-N-G. During the Vietnam War, they assisted U.S. forces. Now they were in Laos. Laos was
a neighboring country. They were neutral. So we couldn't put troops there. If we were going to,
it would have been probably another 40,000 people deployed. But we didn't have to because the Hmong
took care of it. They disrupted North Vietnamese supply lines, they rescued
downed pilots, all kinds of things in support of the US war effort. And then
when the war ended, we left them. And the Viet Cong descended upon them, slaughtered
them, put them in re-education camps, all kinds of horrible things, things that
can't talk about on YouTube, really bad things. Now, eventually, years later, we
get some of them out. They fled. They wound up in Thailand and we got some of
them out and most of them, I don't know if most of them, but a whole lot of them
are up in St. Paul. I don't know if you can imagine that going from Vietnam and
Laos to Thailand and then ending up in St. Paul. That has got to be a little bit
of a switch. But they are now part of the American tapestry. They're part of the
American family. And we learned from that, right? That was a national embarrassment
leaving our allies like that, leaving those that saved American lives behind.
That was embarrassing. We would never do that again except that we're doing it
right now. There is a backlog of tens of thousands of Iraqi nationals that helped
the U.S. and they're waiting for visas. There are thousands of interpreters and
the Terps are the difference between life and death. Literally. Communication
with the local populace is paramount. It is something that will keep you alive.
They have seen a 99% decline in approved visas.
Last year, out of the thousands waiting for one, two, two, not 200, not 2,000, two people
were given their visa.
In their home country, they're hunted.
all of their countrymen agree with what they did. They have bullseyes on them. A lot of
them have fled, gone to other countries while they wait for their visas. Some of them have
been waiting 10 years or more. But before, at least the line was moving. Now, it's all
but stopped. A lot of them were collateral damage during the war and now
they are collateral damage during Trump's war on immigration. They are being left
to die. That is what is going to happen to them. Two. Two were given visas. You can
say what you want about the geopolitical concerns surrounding the invasion of
Iraq, and there's a whole lot to be said there, there is.
But to the Iraqi on the ground, none of that mattered.
Not once the occupation started, they had to pick a side.
And these people did, and they sided with the U.S.
And now we're abandoning them for no reason.
These people saved American lives, they risked their lives, they would go out on patrols.
And incidentally, NBC ran an article about this, and in that I want to point something
out because I think NBC missed a bigger story.
Guy's telling his story about how he used to go out on patrols and all of that.
And it's quoted that he started when he was 17, 17, going out on combat patrols.
Even if he was unarmed, I am fairly certain that would qualify as engaging in direct hostilities.
That's what we call a child soldier.
That's probably a bigger story.
I wonder how widespread that practice was.
I had never heard of anything like that before.
But here we are.
We learned nothing from Vietnam.
We are making the same mistakes again.
But once again, just like with supporting the troops, it doesn't really affect us.
It doesn't mean anything to us.
It's those people over there.
It's those people over there that have to do it.
And it's those people over there that are being left behind because the political winds
have changed in the United States.
So they get to die.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}