---
title: Let's talk about Cherokee people and the House....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=5bdWiqybsUg) |
| Published | 2019/08/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Cherokee Nation wants to appoint a delegate to the House of Representatives as allowed by a treaty signed in 1835.
- This treaty ultimately led to the Trail of Tears, a genocidal event that forcibly removed indigenous people from their land.
- The federal government's relationship with indigenous cultures is framed as two sovereign nations interacting, but honoring treaties has been an issue.
- Despite having voting rights, gerrymandering has significantly limited the voting power of the Cherokee Nation, consisting of 350,000 to 375,000 people.
- The potential delegate from the Cherokee Nation, likely Kimberly Teehee, may not have voting powers but can represent Native nations' issues in the House.
- Concerns exist that only the Cherokee Nation has this provision in their treaty, leading to worries about exclusive representation.
- Kimberly Teehee is viewed as a front runner for the position and serves as a diplomat between the Cherokee Nation and the U.S. government.
- While the process may take time, having Native interests represented in the House could significantly impact Native communities positively.
- Beau believes that the representation of Native interests in the House will make C-SPAN more engaging and is worth the support.

### Quotes

- "The federal government has a horrible track record of honoring treaties with native nations."
- "The idea of Native interest being represented on the house floor is going to be great."

### Oneliner

The Cherokee Nation seeks to appoint a delegate to the House of Representatives, reflecting on historical treaties and the potential for Native representation in government.

### Audience

Advocates for Native rights

### On-the-ground actions from transcript

- Support Native representation efforts within the government (suggested)
- Stay informed about the developments in Native rights and representation (implied)

### Whats missing in summary

The full transcript provides historical context and insights into the potential impact of Native representation in government.


## Transcript
Well howdy there internet people, it's Beau again.
We have a very interesting development on the Native Rights Front in the United States.
The Cherokee Nation wants to appoint a delegate to the House of Representatives.
And by treaty they can.
In 1835, the U.S. government signed a treaty with the Cherokee Nation and it included a
a provision that allowed them to appoint a delegate to the House.
Now this treaty eventually led to the Trail of Tears.
For overseas viewers, if you don't know what it is, Google it, it's genocide on the move.
Pretty horrible, horrible thing.
And also for overseas viewers, the relationship the federal government has with indigenous
cultures is bizarre.
It is almost, because that's the way it's supposed to be, two sovereign nations dealing
with each other.
The Cherokee nation being its own nation dealing with the federal government, the United States
government.
And that's how it's supposed to operate.
Doesn't always operate that way because the U.S. government has a horrible track record
of honoring treaties with native nations.
They have the ability to vote.
However, gerrymandering has severely limited their voting power.
The Cherokee Nation is 350,000 to 375,000 people.
It's a huge voting block.
They're probably the largest native nation altogether.
I know they're the largest federally recognized one.
So how would this play out?
How would this play out?
Probably going to be a non-voting member of the House, which means they can't vote on
the House floor, but they can debate and they can represent the issues of Native nations
to the House of Representatives.
Now that's one of the criticisms, is that only the Cherokee have this in one of their
treaties.
couple of others, but certainly not all nations. So the worry is that the Cherokee will only
represent themselves. I don't know that that's actually going to be the case, but that's
a worry that's going on. The most likely candidate to be sent up is Kimberly Teehee. She's in
charge of picture her like the Secretary of State between the Cherokee Nation and the
U.S. government.
She's kind of the diplomat.
She's the front runner on that.
Now will this happen?
I think so, probably, but it's going to take a while.
But it's definitely worth the wait if you have ever been to a Native Council meeting
or watched natives debate.
The idea of Native interest being represented on the house floor is going to be great.
It's going to make C-SPAN a whole lot more interesting.
It is definitely worth the wait and worth the support.
Anyway, it's just a thought.
y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}