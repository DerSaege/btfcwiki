---
title: Let's talk about the Endangered Species Act, Chickens, and Painters....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=4Qb0wLYv0jo) |
| Published | 2019/08/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Endangered Species Act is being changed, causing concern among conservationists worldwide.
- Oil and coal lobbyists, David Bernhardt and Andrew Willer, along with Wilbur Ross, are involved in the changes.
- The current administration's key environmental positions are held by individuals with backgrounds in oil, coal, and mining.
- Under the new changes, economic impact will now be a significant factor in court decisions regarding the protection of species.
- Every species will essentially have a price tag attached to it under the new considerations.
- Conservationists are alarmed as over a million species face extinction, now subject to economic evaluations for protection.
- Multinational corporations lack loyalty to any particular country and prioritize wealth extraction over environmental protection.
- The impact of these corporations on the environment will directly affect those residing in the United States and the Western world.
- Beau urges his audience to not just be activists but also become conservationists to combat the environmental challenges we face.
- He stresses the urgency of placing individuals in power who can make significant environmental policy changes before it's too late.

### Quotes

- "Every species will have a dollar sign attached to it."
- "We have to become environmentalists or conservationists, otherwise we're all just house painters putting it over time on August 8th, 1945 in Nagasaki."
- "We're not trying to stop huge issues from occurring in the future. We're trying to mitigate, trying to slow it down, because it's going to happen."

### Oneliner

The Endangered Species Act changes put a price on species, urging everyone to become conservationists to prevent irreversible environmental damage.

### Audience

Environmental advocates

### On-the-ground actions from transcript

- Advocate for strong environmental protections through local initiatives and community action (implied)
- Support and volunteer for conservation organizations working to protect endangered species and habitats (implied)

### Whats missing in summary

The full transcript provides additional context on the impact of multinational corporations and the urgency of addressing environmental concerns through immediate action. 

### Tags

#EndangeredSpeciesAct #Conservation #EnvironmentalProtection #MultinationalCorporations #Activism


## Transcript
Well, howdy there, internet people, it's Bo again.
So the Endangered Species Act is being changed.
Conservationists all over the world are freaking out,
with good reason, with good reason.
You know who isn't freaking out right now?
Oil lobbyists, David Bernhardt, coal lobbyist Andrew Willer,
and the king of bankruptcy, Wilbur Ross.
They're not freaking out, sorry.
Those are their old jobs.
That's the secretary of the interior,
the director of the EPA, and the secretary of commerce.
Drain the swamp, huh?
Only if they're going to build a refinery on the site.
These are the people that are supposed
to be protecting the environment in the United States
right now.
an oil lobbyist, a coal lobbyist, and a man who made his money investing in mines.
Every conservationist has their pet reason to freak out right now.
And they're all good ones.
They're all good ones.
My personal big issue with the changes is that economic impact is now going to be considered
in court.
How this act applies will be gauged through bank accounts by how much money it's going
to cost.
Now what that means, at the end of the day, is that every species will have a price.
Every species will have a dollar sign attached to it.
And if whatever that project is, is going to make more than that, well sorry bud you
got to go.
at the point where we are currently facing the extinction of over a million species.
Now you're just going to have to pay it a little bit to do it.
Make sure you cut everybody in I guess.
Every species that includes keystone species which will directly impact you.
In that video with the old Alice, we talked about how colonization just changed.
It didn't really end.
It just changed, logos instead of flags, multinational companies instead of countries.
A severe downside to this, and there are a lot of them, but a severe downside to those
of us living in the United States or the Western world in general, is that these multinational
corporations, they have no loyalty to the mother country. They don't care. The
United States is just another wealth extraction point. The chickens have come
home to roost. We're going to see how these companies have treated the Global
South for a very, very long time. We're going to get to witness it in our own
backyard as they destroy our environment, as they cripple our earning potential by destroying
everything around us without regard to anything other than economic impact.
You know, most people that watch this channel, you're an activist of some kind.
You have some pet calls.
We all have to become at least a little bit of conservationists, of environmentalists.
We have to.
We all have a choice.
We have to get somebody in a position to where they can make drastic changes because we are
running out of time.
We are running out of time.
A lot of the environmental impacts that are occurring today, well, they're already past
the point of no return really.
Now we're not trying to stop huge issues from occurring in the future.
We're trying to mitigate, trying to slow it down, because it's going to happen.
We all have to become environmentalists or conservationists, otherwise we're all just
house painters putting it over time on August 8th 1945 in Nagasaki. It's not
going to matter much how we do on our other pet projects because what we're
fighting for may not be there. Anyway it's just a thought. Y'all have a good
night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}