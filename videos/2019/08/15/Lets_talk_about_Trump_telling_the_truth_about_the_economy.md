---
title: Let's talk about Trump telling the truth about the economy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=eHJlCZjLttc) |
| Published | 2019/08/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Watching the president, Beau notes his claims of a strong economy, contrary to global recession predictions.
- The president dismisses economists as "fake news" and untrustworthy for not supporting his narrative.
- Beau points out the president's pattern of discrediting various experts and professionals as fake news.
- He criticizes blind loyalty to the president despite his track record of unfulfilled promises and benefiting the wealthy.
- Beau warns against the president's manipulation and urges people to re-evaluate their support based on his actions.

### Quotes

- "There is no giant scheme and group of people out to get the President of the United States. He's not that important."
- "The trust fund baby billionaire is not your friend."
- "He's admitted that he used to get politicians money. So he, as a businessman, cut them out."
- "The policies that are going to benefit them [billionaires] are going to hurt you."
- "It's just a thought, y'all have a good night."

### Oneliner

Beau exposes the president's dismissal of experts as "fake news" and warns against blind loyalty, urging scrutiny based on actions, not words.

### Audience

Voters, citizens

### On-the-ground actions from transcript

- Re-evaluate support based on actions, not promises (implied)
- Analyze the president's track record and promises kept (implied)
- Question blind loyalty and scrutinize policies affecting the populace (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the president's dismissals of experts and professionals as "fake news," urging critical evaluation of his actions and policies for informed support or opposition.

### Tags

#Economy #FakeNews #CriticalThinking #PoliticalAnalysis #PresidentialPolicies


## Transcript
Well, howdy there, Internet people, it's Bo again.
So I was watching the president today, and he laid some stuff out there,
dropped some truth bombs on us, let us know what was really going on.
Because you have all these economists from all over the world talking about how
bad the economy is gonna get, talking about a global recession.
And he told us that wasn't true.
He told us the truth, he said that the economy is doing great, and
that we shouldn't worry.
And that those economists, they're just not telling the truth.
And he used the term, they're fake news.
That's right.
Economists are just like journalists.
Now, they're fake news.
They can't be trusted.
We need to remember that from now on
because economists are known for not telling the truth
because they're not invested in the economy at all.
They have no benefit whatsoever if it stays healthy.
They want to see the global economy crash.
That's what he said.
They're fake news.
They are fake news like journalists and climatologists
and historians and doctors and immigration experts
and meteorologists and biologists and farmers
and manufacturers and the textile reps and the retail reps,
his own generals, judges, and photographs.
All of them are fake news.
None of that stuff is true.
Ignore them, and really, they're just all out to get him.
They're all out to get him because they know
He's the one who can make America great again.
And we have to just support our president.
The president of the United States isn't gonna lie to us.
Presidents don't lie.
Politicians don't lie.
That's crazy talk.
There's no way that's true.
It's all these other people.
They're all lying.
Do you hear how crazy this sounds?
I mean, seriously, this is approaching
mental illness levels, the entire world.
Any time a field of experts comes forward
and says the president is making a mistake,
he comes out and says they're lying
and his base nods their head.
Yes, yes, they're lying, of course they're lying
because there's no way I could have made a mistake
in voting for this clown.
That's what he's counting on.
What you need to do if you still support this man
is you need to go back and look at all the people
he called fake news in the beginning.
Look at them, look at their predictions.
You see what happened, they all came true.
He doesn't know what he's talking about, he's bumbling through it and what he's counting
on is you continuing to support him through all of this so he can get re-elected and that
is when he's really going to rob you blind.
There is no giant scheme and group of people out to get the President of the United States.
He's not that important.
He's not that effective.
All he's done is make bad situations worse.
That's it.
That's all he's done.
There's no group out to undermine him because he's going to fix the country.
Go back through his campaign promises.
Has he kept any of them?
He's going to drain the swamp.
His cabinet is full of lobbyists.
He didn't drain them, he promoted them.
Of course not.
Why would you believe that he would?
The trust fund baby billionaire is not your friend.
All he did was cut out the middleman.
He's admitted that he used to get politicians money.
So he, as a businessman, cut them out.
And now he can make the policies that benefit him and his friends.
Are you friends with any billionaires?
You're not.
The policies that are going to benefit them are going to hurt you.
Anyway, it's just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}