---
title: The Roads to Realignment
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=kKr0Z3eIkW8) |
| Published | 2024/01/04 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explaining the concept of realignment or shaping in the context of recent developments involving a high-ranking member of Hamas being targeted.
- Realignment operations aim to realign leadership or shape it to be more interested in peace.
- Using oversimplified categories like hyper militant, interested in peace/pragmatic, and soldiers to illustrate the realignment process.
- Selective removal of elements of leadership to eventually lead to negotiation.
- Realignment or shaping is about disrupting the chain of command and choosing who gets moved up.
- Challenges in realignment operations include oversimplification, the need for accurate intelligence, and understanding the motivations of the opposition.
- Reasons why realignment operations are not frequently done include lack of public support, desire for revenge over a thoughtful response, and the time-consuming nature of intelligence work.
- Realignment operations have been viewed as best practices since World War II but are often avoided due to political reasons and the public demand for immediate reactions.
- Speculating on the possibility of realignment operations being initiated now, considering the ongoing events.
- Waiting for more evidence like unclaimed strikes and removal of militant leadership to confirm if realignment operations are indeed taking place.

### Quotes

- "The public demands that immediate reaction rather than the response."
- "Realignment or shaping is about disrupting the chain of command and choosing who gets moved up."
- "It meant be very precise. Not create a bunch of innocent loss."
- "This is what is viewed as best practices."

### Oneliner

Beau explains realignment operations and the challenges in implementing them effectively, including public expectations for immediate reactions over thoughtful responses.

### Audience

Military strategists

### On-the-ground actions from transcript

- Watch for unclaimed strikes and removal of militant leadership to potentially confirm realignment operations (implied).

### Whats missing in summary

In-depth analysis of historical context and geopolitical implications of realignment operations.

### Tags

#Realignment #Shaping #Leadership #ConflictResolution #MilitaryStrategy


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about realignment,
or shaping, which I guess is the new terminology on it.
Because there has been a development,
and it brought about the possibility
that that is something that is now beginning,
something that has started.
We don't know that it started.
In fact, I think it's unlikely.
But it's something that's on the table now,
So it is worth discussing.
OK, so the news that started this.
Somebody conducted a strike on a high-level, a very high-ranking
member of Hamas.
The assumption is that it's Israel.
Israel has denied involvement.
That prompted the question,
is this the beginning of realignment operations?
Maybe, maybe.
So we are going to talk about what those are
and how they work.
This strategy is viewed as best practice
when you are talking about an establishment force
dealing with a non-state actor.
The goal is to realign the leadership, make them more interested in peace, or I guess
shape the leadership.
We are going to use a very, very simple example, and we're going to go through how the process
works.
Understand, this is over simplistic.
We have it broken down into three categories of person.
In real life, there are dozens.
The categories we're going to use are somebody who is hyper militant, somebody who is interested
in peace or at least pragmatic, and soldier.
In real life, you have categories like hyper militant but inept, which means they're not
any good at their job, so you leave them alone.
Or you have somebody who's hyper militant, but they're gay.
So maybe they can be encouraged to be more pragmatic.
Maybe they have a substance issue, something like that.
This is very, very dirty stuff.
Okay, but in our oversimplistic example, we just have the three categories.
Red triangles, those are people who are hyper militant.
circles, interested in peace or pragmatic. Green squares, they're just soldiers, okay, and I know.
Nobody believes that the other side has people who are just fighting because they feel like they have
to, but they exist in every organization. Let's say this is the starting makeup of the leadership.
This is not an organization that would want to negotiate or come to
a piece in any way, shape, or form. So it is the selective removal of elements of leadership.
In this case, the red triangle at the top has the check by it, right? So that's removed.
Then you get this, okay? Not a whole lot of difference, right? Pretty similar.
But you can already see some changes on the bottom tier.
Still not an organization interested in peace.
But because security is higher now, because it's already happened once,
you can't get to the top person.
So you hit the second tier. What happens then?
The next generation looks like this.
Okay, now you don't have any red triangles on that second tier anymore.
So if you do it again, you end up with that.
This is an organization that would negotiate.
This is an organization that would be interested in peace.
Like any military organization, when the chain of command is disrupted, people get
moved up. Re-alignment or shaping is the opposition choosing who gets moved up.
So, the question is, this seems pretty simple. So why doesn't it get done? Number
of reasons. One, this is an oversimplistic example. In real life, it's not this
easy. This could be conducted with what three strikes which is one of the
reasons it's better for dealing with non-state actors because it doesn't
force generate for the other side. It doesn't increase recruitment for the
other side because of all of the extra strikes. It would probably take 30 in a
real organization. A lot of benefits but it's hard because you need real
intelligence, you have to understand who the opposition is, you know, that old know
your enemy thing, and you have to be able to accurately determine their
motivations, their intent. We've talked about it before. Holy grail of
intelligence work. Okay, so even with that in mind, why doesn't it get done?
Because this isn't just an Israel thing. The US doesn't do it well either. You have
two main reasons beyond it's hard. The first is the lack of forward movement. If
you're in the US, think about how many headlines you saw over the last 20 years
that said the number two of organization X was taken out. I mean that sounds good
unless the number two was a blue circle being replaced by a red triangle, then
And it's bad.
But those headlines allow the public to feel like there is forward movement going on.
That's one reason.
And part of this also feeds into an establishment force being more conventional and viewing
their opposition as a conventional military as well, where you just want to disrupt the
entire command and control. When you're talking about a non-state actor, it's
more effective to leave certain elements in place. And then there's the big reason
that this doesn't get used. Us, meaning the public. When an attack happens, what
is the public crying for? What are they out there saying has to
happen? What do they want? Do they want a well thought out response? Or do they
want revenge? That's why. And that's what keeps the cycle of violence going. Now I
know somebody's gonna say, oh yeah, somebody in the shed knows more about it
than, yeah, okay, this isn't me. This isn't me. The earliest you can find in the
modern era, the earliest you can find something similar to this being talked
about and kind of deployed was during World War II. It was refined by a
country in the Middle East, just go ahead and take a guess, yeah, and then they went
around and taught everybody else the refinements. It's not me. This is
what is viewed as best practices. It doesn't get used because politicians
don't like it because it takes time. That intelligence work in deciding who is
what category and what color and what shape and all of that stuff, it takes
time. The public demands that immediate reaction rather than the response. But
immediate reaction actually feeds the opposition. This is how it works. Do I think that this is
what's happening? Probably not. I don't actually think so. It's a little late in the game to be
deploying something that is this low footprint. In real life, this is very low footprint in
in comparison to everything that we have seen
over the last couple of months.
It would be weird to start it now,
but given everything else that's going on,
it's a possibility that they're starting it now.
I don't necessarily, I wouldn't stand here and say
that's what they're doing.
In fact, I think it's unlikely but it's not out
of the realm of possibility.
will know if there are more strikes that aren't claimed by anybody.
If you start seeing a bunch of people in the leadership structure of Hamas and they all
use a whole lot of wild rhetoric, or they are known for being incredibly militant, and
they are the ones that are being removed from the field, then yeah, that's what's going
on, which means they are shaping it for some kind of negotiation. But until we see a whole
lot more, we won't know that for sure. But when people were talking early on, people
who understand this type of conflict, and they were talking about realignment or shaping
or calling for restraint, this is what they were talking about doing. It didn't mean
do nothing. It meant be very precise. Not create a bunch of innocent loss. Stuff
like that. I don't know if that's what's going on. We're gonna have to wait and
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}