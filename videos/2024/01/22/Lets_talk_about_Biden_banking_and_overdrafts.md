---
title: Let's talk about Biden, banking, and overdrafts....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=sLBrUSUMOS8) |
| Published | 2024/01/22 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the Biden administration's push to revamp the way banks handle overdraft fees.
- Describes the current system where if you use your debit card without enough money, banks charge an overdraft fee of around $20 to $40.
- Mentions Biden administration's proposals to limit the fees to what it costs the banks to process overdrafts, possibly around $3 to $14.
- Points out that major US banks make about $8 billion annually from overdraft fees, disproportionately affecting those with limited financial resources.
- Emphasizes that this proposed change aims to provide relief to those struggling financially.
- Notes that there will be periods for industry and public commentary on the proposed changes.
- Acknowledges that despite potential pushback, it seems like the changes are already in progress, with details still being finalized.

### Quotes

- "Some of the wealthiest, most profitable institutions in the United States take billions of dollars per year from people that don't have any money."
- "The Biden administration is trying to find a way to curtail this."
- "The goal here is to just provide a little bit of relief."
- "It's not like this is going to seriously address income inequality or anything like that, but it's a little bit of relief."
- "It's probably not going to get the coverage it should."

### Oneliner

Beau explains Biden's push to revamp overdraft fees, aiming to provide relief to those financially vulnerable, despite pushback and lack of media coverage.

### Audience

Banking consumers

### On-the-ground actions from transcript

- Contact local representatives to voice support for limiting overdraft fees (implied)
- Participate in industry and public commentary periods on the proposed changes (implied)

### Whats missing in summary

Details about the potential impacts of overdraft fee changes on individual consumers and communities.

### Tags

#Banking #OverdraftFees #BidenAdministration #FinancialRelief #CommunityImpact


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about banking,
and Biden, and overdrafts, and why this is coming up,
why it's important to some people,
and why this is one of those things
that a whole lot of middle America
is just not really gonna get.
They're not gonna understand why it's important.
Okay, so if you have missed this whole storyline,
which wouldn't really be surprising,
it's not getting a lot of coverage
because it doesn't impact people
with lots of disposable income,
which is who the news is generally created for
because that's who the advertisers wanna reach.
Anyway, the Biden administration is pushing
to revamp the way banks do overdraft fees. Right now if you use your card, your
debit card, you don't have the money in there, it takes out whatever your purchase
was and then it adds an overdraft fee that ranges I don't know what 20 to 40
dollars now, somewhere in there, most around 30 bucks, I think.
And the Biden administration is trying to find a way to curtail this.
And the suggestions that are coming out the proposals are allowing banks to charge only
what it costs them to actually do that and basically run it at a zero profit line.
would be to allow it to function as like a short line of credit and then another would
be to give it a set amount but it be designed to still run it kind of a zero profit line.
So the proposed amounts are like three, six, seven, and fourteen dollars. The fourteen
one would be allowing banks to recoup everything to include the amount they
lose when an account goes into the negative and then never comes back out
and spreading that out among all of the accounts. There will be periods for
industry commentary and public commentary on this. Okay so why does this
matter? Okay because it's obviously going to be a question that comes up. The
biggest banks in the United States bring in about 8 billion dollars a year via
overdraft fees. Eight billion dollars per year. Another way of saying that is that
some of the wealthiest, most profitable institutions in the United States take
billions of dollars per year from people that don't have any money. This is a
a problem that generally impacts those that don't have a lot of resources, don't have
a lot of resources to fight it, certainly don't have the kind of resources you need
to get the attention of anybody in the Senate. So the goal here is to just provide a little
bit of relief. That's really all it is. It's not like this is going to seriously address
income inequality or anything like that, but it's a little bit of relief.
And realistically, it provides the relief at the point in time when people need it the
most.
It is self-selecting to provide that relief when they're literally out of money.
Obviously, there will be pushback against this, but it does appear like this is already
going to the comment section.
They have decided that they're going to do this.
Now they're just trying to figure out which method they're going to use or whether they're
going to allow banks to choose one of the three methods or allow two of them or whatever.
If you've been following this and you're getting conflicting reports as to what banks will
be allowed to do out of the three we talked about it's they haven't decided
yet I've seen some reporting that says oh it's going to be this it might be
that or it might be one of the others from what I can what I can find they
have not actually decided and they won't until all the commentary periods are
over but this is something that's on the horizon that again it's probably not
going to get the coverage it should. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}