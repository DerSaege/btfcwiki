---
title: Let's talk about Cicadas, life cycles, and weirdness....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=5BKIX-An96o) |
| Published | 2024/01/22 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about cicadas and the upcoming event of two large broods emerging at the same time.
- Explains the life cycle of cicadas, from eggs being laid on trees to the nymphs falling to the ground and going underground for several years.
- Mentions that this event, where billions of cicadas will emerge, happens every 221 years.
- Specifies that this phenomenon will be most observable in Iowa, Illinois, and Missouri.
- Describes how the male cicadas attract female cicadas through singing.
- Emphasizes that the mass emergence of cicadas at the same time guarantees the species' survival.
- Assures that although it may be a strange and unique occurrence, it won't turn into a horror movie scenario.
- Mentions that similar unusual events in nature might occur throughout the year.
- Concludes by wishing everyone a good day and suggesting to embrace and get used to these natural phenomena.

### Quotes

- "This is going to be most observable in Iowa, Illinois, and Missouri."
- "There are so many of them out, like this year there will be literally billions."
- "It kind of guarantees the reproduction of the species."
- "I'm pretty certain that it won't turn into some horror movie."
- "There's gonna be more stories like this throughout the year anyway."

### Oneliner

Beau explains the unique event where billions of cicadas will emerge in Iowa, Illinois, and Missouri, ensuring their species' survival without turning into a horror movie, part of many unusual occurrences in nature this year.

### Audience

Nature enthusiasts

### On-the-ground actions from transcript

- Observe and appreciate the extraordinary phenomenon of mass cicada emergence in Iowa, Illinois, and Missouri (suggested).

### Whats missing in summary

The detailed scientific aspects of the cicada life cycle and the potential impact on the environment and other species. 

### Tags

#Cicadas #Nature #Science #Phenomenon #Iowa #Illinois #Missouri


## Transcript
Well, howdy there, internet people.
Led Zebo again.
So today, we're going to talk about cicadas, and mommy and
daddy cicadas, and why all of this is going to happen,
because it's all over the news, and it has prompted a whole
lot of questions.
So we're just going to kind of run through it and do a little
bit of a science class, I guess.
OK, so if you have missed the news, something that occurs
only once every 221 years, I think, is about to happen.
And it has to do with cicadas.
There will be two large broods of cicadas coming out
at one time, close to each other.
This is going to be most observable in Iowa, Illinois,
and Missouri in that area because there
is the group of cicadas that emerge every 13 years
and the group that emerges every 17 years.
And it has synced up.
And they're all coming out at once this year.
This has led to a whole bunch of questions about cicadas.
So we're just going to go through it so people understand how this happens because I'm not
really sure why, but there's a lot of questions about this.
So when a mommy and daddy cicada love each other very much, the mommy puts eggs in a
scratch on a tree like a notch on a tree. The eggs get nutrients from that. Nymphs
come out of the eggs. They fall to the ground. They go underground and they get
nutrients from like the roots. They stay underground for years. Obviously up to
17 years, I actually think it's up to 19 years, and I think the shortest amount of
time is two years. So they stay underground for years. Then they come up
at their designated time. When they come up, they crawl up the tree, they molt
their exoskeleton, and now they have wings. They're adults and they have wings.
The males fly off and they kind of hang out together and sing, and they
attract female cicadas, and the whole thing starts all over again. They come
out like this all at once to basically ensure their their survival. There are
so many of them out, like this year there will be literally billions. There's so
many of them out that it kind of guarantees the reproduction of the
species. That's why it happens. So you will see it most in those three
states, Iowa, Illinois, and Missouri. Across the south, where you normally have
cicadas, you'll still have them, but if I understand this correctly, it will be
normal there. It'll be the normal amount. It'll only be up in those three states where you're
gonna have like this massive amount with multiple broods showing up at the same time and just being
everywhere. So that's what's going on hopefully. You know, I'm pretty certain that it won't like
turn into some horror movie, but that's what's occurring. There's a whole lot of weird stuff
happening this year when it comes to nature so just let me get used to it
there's there's gonna be more stories like this throughout the year anyway
It's just a thought y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}