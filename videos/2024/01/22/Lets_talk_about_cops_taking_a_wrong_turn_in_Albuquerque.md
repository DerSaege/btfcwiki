---
title: Let's talk about cops taking a wrong turn in Albuquerque....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=MXwvGD38DCQ) |
| Published | 2024/01/22 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recent developments point to a major issue within the Albuquerque Police Department related to DWIs and DUIs.
- A federal investigation is ongoing within the department, focusing on cops heavily involved in DWI arrests.
- Federal authorities have searched the homes of at least three cops and a law office, leading to the dismissal of 152 DWI-related cases.
- Speculation surrounds the nature of the investigation, including possibilities like an overtime scam or issues with calibration.
- There is a clear connection between the federal corruption investigation, Albuquerque Police Department, and the DWIs/DUIs.
- Public statements are anticipated in the coming week to shed light on the situation's depth and scope.
- Statements released so far have been cryptic, hinting at a stain on the police department without clear details.
- Questions remain unanswered regarding the reasons behind case dismissals and the extent of the issue.
- Clarity is expected soon as explanations are likely to be provided.
- The situation hints at ongoing developments that may reveal more about the issue.

### Quotes

- "My prosecutorial ethics demanded that I dismiss these cases."
- "It's going to be a stain on the police department."
- "There is some overlap between a federal corruption investigation, Albuquerque Police Department, and DUIs, DWIs."

### Oneliner

Recent developments indicate a major issue within the Albuquerque Police Department related to DWI arrests, prompting a federal investigation and raising questions about departmental integrity.

### Audience

Albuquerque residents

### On-the-ground actions from transcript

- Contact local authorities or community organizations for updates and ways to support needed reforms (suggested).
- Stay informed about the situation through local news sources and community updates (implied).

### Whats missing in summary

Full details and context on the ongoing federal investigation within the Albuquerque Police Department and its implications.

### Tags

#Albuquerque #PoliceDepartment #FederalInvestigation #DWI #DUI #Corruption


## Transcript
Well, howdy there, Internet people. Let's bow again.
So today we are going to talk about mysteries, and how it certainly appears that somebody took a wrong turn
in Albuquerque.
Okay, so there have been some developments over the last few days, and it is leading to the conclusion that there is a
major issue  within the Albuquerque Police Department, specifically related to those who have
been dealing a lot with DWIs, DUIs there. Okay, so what do we know? We know that
there has been an investigation going on, a federal investigation, into some
aspect of the department for some time. What exactly? We don't know, but it does
seem to center around cops who appear to be involved a lot in DWI arrests.
The reporting suggests that over the last, I don't know by the time this goes out, 24
maybe 48, that the federal government has investigated and searched the homes of at
least three cops and a law office as well. There were also a number of cases dismissed
when it comes to charges. Surprise, all of those cases, they had to do with DWIs. How
many? 152. So right now the local department, all they're saying is we're
cooperating with the feds. The feds, they're no commenting on everything. This
could be a whole list of things. It could be anything from an overtime scam
because of the way DUI, DWI cases work, the cops involved with those, they get a
lot of overtime. So it could be something like that. It could be them not
calibrating machines. It could be there's a whole bunch of different things it
could be, and there's tons of speculation out there. We don't know. Based on the
information available. We don't know, but it does appear that there is some
overlap between a federal corruption investigation, Albuquerque Police
Department, and it certainly appears to have something to do with DUIs, DWIs.
And I'm guessing that this isn't over. My guess is that sometime in the next week
or so, we'll get some kind of public statement that will better inform everybody as to what
exactly is going on and how widespread it is.
The statements that are coming out, it's all very cryptic.
It's like, this is going to be a stain on the police department, okay, well, what's
the stain?
Or it's a prosecutor saying, my prosecutorial ethics demanded that I dismiss these cases.
Why?
That seems to be the important question that really isn't getting answered at this point.
But I would imagine we will get some kind of explanation relatively soon.
Anyway, it's just a thought y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}