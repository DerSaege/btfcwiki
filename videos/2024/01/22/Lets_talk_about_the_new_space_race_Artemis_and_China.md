---
title: Let's talk about the new space race, Artemis, and China....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=3-rNsuU_IxI) |
| Published | 2024/01/22 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The United States and China are entering a new space race, despite some involved not viewing it that way.
- NASA's Artemis program aims to return humanity to the moon, with Artemis 2 delayed until 2025 and Artemis 3 until 2026.
- Politicians seem more concerned about timelines and national identity than crew safety.
- China has its own moon program, expected to reach the moon by 2030, but they often exceed their timelines.
- American politicians are framing this space competition as a matter of national importance, reminiscent of Cold War games.
- Scientists and astronauts involved prioritize safety and peaceful exploration, rather than competitive space races.
- Expect a surge in propaganda pushing high stakes and national pride as the competition escalates.

### Quotes

- "The United States and China are entering a new space race."
- "Politicians seem more concerned about timelines and national identity than crew safety."
- "Scientists and astronauts prioritize safety and peaceful exploration."

### Oneliner

The US and China are in a new space race, with politicians prioritizing national identity over crew safety, while scientists advocate for peaceful exploration.

### Audience

Space enthusiasts

### On-the-ground actions from transcript

- Stay informed about developments in space exploration (suggested)
- Support initiatives promoting safe and peaceful space exploration (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the emerging space competition between the US and China, touching on political motivations, safety concerns, and the perspective of scientists and astronauts involved.

### Tags

#SpaceRace #ArtemisProgram #US #China #SpaceExploration


## Transcript
Well, howdy there internet people, it's Beau again. So today we are going to talk about the new space race because
well
those who are involved with
space travel
tend to not like the term space race. It certainly appears that the United States and China are
entering into one, whether they want to or not.
The
scientists, the astronauts, those people who are actually involved, they may not
view it as a space race, but the politicians certainly seem to. Okay, so
what's going on? NASA has the Artemis program. Artemis is supposed to return
humanity to the moon, and it is an American program. It's through NASA. There
There has been a delay, Artemis 2 will not take place until 2025.
Artemis 3, which is the one that actually returns people to the moon, will not take
place until 2026.
That's the current schedule.
They could theoretically speed back up and get back on the previous schedule, or it could
move even slower.
So that's where things stand right now, 2026.
When there was a discussion about this involving politicians, the conversation immediately
changed to, you know, we're not the only country trying to get to the moon, right?
Like nobody seemed to really care much about the safety of the crew.
They certainly cared about that timeline though, because China also has a program, a goal of
getting people to the moon.
Now it is worth noting that the Chinese timeline doesn't put them there until 2030, but they
tend to do things the other way.
They set a long timeline and come in early.
Now whether or not they'll shave four years off, that's an entirely different topic.
But I think that there is a concern among American politicians that because we have
entered into this near-peer contest, they're trying to kind of replay all of the old Cold
War games and they're viewing this as something that is something that is
going to really matter to national identity. I don't know that it will but
from the politician standpoint it seems incredibly important. Odds are that the
same view is held in China. If they could get there first they would probably want
to. Now again, it's worth noting that the scientists and the astronauts and those
people behind it, they don't like looking at it through that framing. One, because
it rushes things and makes things less safe, and two, most of them are very
committed to the idea that what's out there should be pursued in peace. But
And regardless of best intentions and all of that stuff, I would get ready for a whole
lot of propaganda starting maybe even as early as the end of this year, pushing, pushing
high stakes behind going back to the moon, especially if the US is feeling real comfortable
about beating the Chinese there.
start playing it up more and more. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}