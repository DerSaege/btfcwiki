---
title: Let's talk about the Red Sea and 2 questions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=WlkFw66IUJM) |
| Published | 2024/01/13 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Updates on the situation in the Red Sea, focusing on the conflict involving the Houthis in Yemen targeting shipping to pressure Israel regarding Gaza.
- Recent airstrikes were initiated by a large group of countries, including the U.S., to stop interference with international shipping.
- Geopolitical dynamics involving regional powers like Israel, Saudi Arabia, and Iran influence support for non-state actors opposed to Israel.
- Contrary to common belief, attributing developments to Iran's perception is not about painting them as the bad guy but understanding normal geopolitics.
- Iran has shown restraint and prevented escalation into a regional conflict, despite supporting various non-state actors.
- Addressing a question about U.S. involvement in Yemen, historical context reveals consistent U.S. engagement dating back to Clinton's era.
- The current phase of involvement in Yemen is not entirely new, with every president since George W. ordering strikes there.
- Speculation on the future of the conflict suggests a potential back and forth pattern without a decisive resolution.
- A focus on making attacks on international shipping costly rather than eliminating the Houthi faction underpins the current approach.
- The U.S. and Western countries are unlikely to commit to ground warfare against the Houthi faction due to perceived challenges and lack of appetite.

### Quotes

- "It's not trying to make them out to be the bad guy, it's a statement of fact."
- "The U.S. and Western countries are unlikely to commit to ground warfare against the Houthi faction."
- "It's more of a deterrent. Make it too costly to do it."
- "Every president since George W. has ordered strikes on Yemen."
- "Iran has shown restraint and prevented escalation into a regional conflict."

### Oneliner

Beau provides insights on the conflict in the Red Sea, Iran's geopolitical influence, and U.S. involvement in Yemen, debunking misconceptions and clarifying historical context while predicting a back-and-forth pattern with a focus on deterrence rather than resolution.

### Audience

Policy analysts, geopolitical enthusiasts

### On-the-ground actions from transcript

- Contact policymakers to advocate for diplomatic solutions and international cooperation (implied)
- Support organizations working towards peace and conflict resolution in the Middle East (implied)

### Whats missing in summary

In-depth analysis of the historical context surrounding U.S. involvement in Yemen and the geopolitical influences shaping the conflict.

### Tags

#RedSea #YemenConflict #Geopolitics #Iran #USInvolvement #HouthiFaction


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to provide a little bit of an
update on the Red Sea.
We are going to answer two questions from you all.
One is humorous, in my way of thinking.
The other is something that people may
genuinely be misreading.
And we want to clear that up.
And you're going to learn a little bit of trivia and see a
link to the past.
Okay, so if you have no idea what's going on, very, very quick recap.
There is a situation developing in the Red Sea.
A group that is in the country Yemen called the Houthis, they have been hitting shipping
in the Red Sea in an attempt to put pressure on Israel to stop what's going on in Gaza.
A week ago, the U.S. and a pretty large group of other countries were like, hey, you've
got to stop interfering with international shipping.
You're messing with the money.
You're going to do that or else.
The or else started, airstrikes.
I said during the most recent video that the strikes were probably not going to work and
that it was going to turn into a back and forth.
It has turned into a back and forth.
That's where all of this is at now.
Moving on to the questions, we'll start with the genuine one first.
Why do you always tie every development to Iran?
Everything is based on Iran's perception.
Really want to make them the bad guy, huh?
No, that's not it at all.
There are three regional powers, Israel, Saudi Arabia, and Iran.
Israel and Iran, they are oppositional, they're adversarial.
So non-state actors that are opposed to Israel or allied to those who are opposed to Israel
will be allied with Iran.
In other words, it's normal geopolitics.
If a group is opposed to a regional power, and there's another regional power that is
opposed to that same country, they'll get support from them.
So all of the countries that are, or all of the non-state actors that are working together
against Israel get support from Iran.
That's why their perception of it is important.
So yes, most developments, whether or not it spirals, depends on Iran's perception.
That's not trying to make them out to be the bad guy, it's a statement of fact.
And just as a point of clarity, I'd like to point out that Iran has kind of been the
adult in the room.
And they could have pushed Hezbollah into action right away, and they didn't.
They're not happy, but they're not going off the deep end.
They aren't actually...
You need to remember that Iran is oppositional to the United States as well.
So there's a lot of propaganda about Iran.
actually not insane. There's, well, not all of them, there's multiple parts to
their government. It's kind of, it's multiple governments in like a trench
coat. They are, so far, they have very much been the adult in the room and done
their part to avoid this spreading into a regional conflict. It's not to portray
them as the bad guy. It's just statement of fact. The Houthis, Hezbollah, Hamas,
the militias in Iran or in Iraq that hit the U.S., all of them are backed by
Iran. Okay, so next question. I bet you miss Trump now. How's it feel seeing the
U.S. start involvement in another country? You seemed awfully calm about the U.S.
starting to get involved in Yemen? Is it just dark Brandon? Right, I mean the
inference here is that the person believes that Trump wouldn't be
involved in Yemen, wouldn't order strikes in Yemen, wouldn't put boots on the
ground in Yemen, wouldn't veto the resolution ending US involvement in
in Yemen.
Okay, so here's some trivia for you.
Who do you think the last president
who didn't order a strike
on Yemen was?
I'll give you a hint. You have to go all the way back to the nineteen hundreds.
It was Clinton.
This isn't the start
involvement. The U.S. actually back to the Saudis in a pretty in-depth conflict
there. As we have talked about before there are a lot of conflicts around the
world that really don't get the coverage that maybe they should in the United
States because news is a for-profit industry in the U.S. and if that
conflict doesn't have a built-in audience, most times it doesn't get
covered. Every president since George W. has ordered strikes on Yemen. It's not
it's not the start of involvement. It's not really new. This is a new phase and
it is for a different reason because they're going after the shipping, but
this isn't a massive departure from what has been occurring for almost a quarter of a century.
When it comes to the Middle East, the U.S. in particular, and this is true of most western
nations, but the US in particular, has been involved in most of them.
Yemen is one where it's pretty consistent, and if I'm not mistaken, even though Clinton
didn't order them, I think he actually entertained doing it.
I think that was something that was under consideration and he decided against it for
some reason.
So it is, maybe I was calm and matter of fact about it because it's not actually a huge
deviation.
As far as how this plays out, there's probably going to be a back and forth for a while.
Eventually, there will need to be some kind of resupply.
That resupply will probably come from Iran.
At that point, there might be a lull.
My guess is the US and the group of countries that are helping with this, at that point
they'll declare victory and kind of scale back. I do not anticipate any of
this truly disrupting the Huthy faction. They've been around a while. They're, they
are a resilient decentralized organization. It's, this is not something
that airstrikes is going to successfully solve. And I would also imagine because of their
sense of humor, my guess is that once the U.S., the West declares victory,
they'll probably do it again. Like even if, even if the reason for them doing it now is over they
would probably do it again just for the laughs, for lack of a better word, just to show you
didn't beat us.
It's more, from the US standpoint, this is probably more about making it costly to engage
in attacks on international shipping, rather than a belief that they're actually going
be able to stop them or disrupt the Houthi faction. It's just adding on to the bill, so to speak.
They, if the West was interested in actually stopping them, it would actually take boots on
the ground type stuff, and I do not believe that Biden wants to do that, and I don't think most
Western countries have the appetite to do that. Not against an organization that would easily,
it would take 10 years. If the U.S. goes in on the ground, it's going to be a decade. I don't see
that as likely. So I don't really believe that they think they're going to win and like end the
the Houthi faction or anything like that.
It's more of a deterrent.
Make it too costly to do it.
And so far, from what we have seen, the perception in Iran, yeah, they're still being the adult
in the room.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}