---
title: Let's talk about cold weather and what to remember....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=EYHYj6wllNo) |
| Published | 2024/01/13 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Large portions of the United States are bracing for very cold temperatures due to an Arctic blast.
- Temperatures are expected to drop to zero or below in parts of Texas, Oklahoma, and Arkansas.
- Even colder temperatures in the negatives will be experienced further north.
- In the deep south, single digits or teens are expected.
- Beau urges people to be prepared for the cold and offers practical advice.
- He recommends checking on those who may not have shelter during this period.
- Tips include creating makeshift shelters using a dining room table or building a pillow fort for warmth.
- Beau advises against burning things indoors for heat and running generators inside.
- He stresses the importance of keeping pets and children away from any sources of heat.
- The cold spell is anticipated to last a few days, with the worst hitting the middle of the country first before moving east.

### Quotes

- "If you see somebody like that, no you didn't. You don't want to be the person who sends somebody out into the elements and then them not make it."
- "And that will help keep the temperature up a little bit. It'll give you a few degrees and those few degrees are gonna matter."

### Oneliner

Large parts of the US are facing extreme cold temperatures; Beau provides tips on staying warm and safe during the Arctic blast.

### Audience

Communities

### On-the-ground actions from transcript

- Create makeshift shelters using a dining room table or build a pillow fort for warmth (suggested).
- Ensure you have necessary supplies and preparations for the cold weather (implied).
- Regularly checking on vulnerable individuals in your community (implied).

### Whats missing in summary

Additional details on specific preparations and supplies needed to endure the extreme cold.

### Tags

#ExtremeCold #ArcticBlast #CommunityPreparedness #StayWarm #SafetyTips


## Transcript
Well, howdy there internet people. Let's bow again. So today we are going to talk about the weather
and how to deal with very cold temperatures. We're going to do this because large portions
of the United States are about to experience very very very cold temperatures. And given the state
of infrastructure in a lot of states, some of this information is going to be needed. If you
If you miss the news, there's an Arctic blast headed into the US, and it's going to come
pretty far south.
How cold and where?
You are looking at temperatures at zero or below in parts of Texas, Oklahoma, and Arkansas.
If you are north of that, it's going to be even colder.
You will see temperatures in the negatives.
If you're in the deep south, you're still looking at single digits or teens.
And given the fact that most of us put on a hoodie at 60 degrees, it might be worth
brushing up on some things to remember.
First things first, if you work at a diner, a restaurant, grocery store, convenience store,
something like that.
And during this period of very cold weather, somebody comes in and they're just kind of
hanging out.
Maybe they get one cup of coffee and they've been sitting there for hours, something along
those lines.
And they don't look like they have a place to go, meaning they don't have a house, they
don't have somewhere to go to be warm.
If you see somebody like that, no you didn't.
You don't want to be the person who sends somebody out into the elements and then them
not make it.
These temperatures are life threatening.
Remember your pets.
Remember to check on your elders and young people.
They're the most susceptible.
If you lose power and you are facing incredibly cold temperatures and you haven't dealt with
it before. Something that has proven useful in the past, because this is
apparently still not an issue we've fixed, take a dining room table,
if you don't have a tent, take a dining room table into an interior room, a small
one, the smaller the better. Set it up and if you're using a tent, pile clothes, they
can be dirty clothes, it doesn't matter. Up on the tent, create a bunch of
insulation. Everybody hangs out in there. The body heat will keep it a little bit
warmer and if you seal off that room it is easier to keep a smaller area warm
than it is a larger area. If you don't have power this is incredibly useful. You
don't have a tent, take your dining room table or some other table into that room
and build a pillow fort. Same thing. Seal it up. Dirty clothes, tarps, blankets,
whatever you have. And everybody hang out in there. And that will help keep the
temperature up a little bit. It'll give you a few degrees and those few degrees
are gonna matter. Obviously normal reminders. Don't burn things in your home.
don't run a generator in your home, stuff like that. If you are at the point where
you have to heat things up to radiate heat, get get bundled up, go outside, do
the fire there, bring whatever it is in and do it safely. You don't want to have
a fire on top of, you know, you don't want to catch your house on fire on top of
everything else. And remember if you are heating stuff up to radiate heat, make
sure you keep your pets and little ones away from them because it's something new
and they're gonna want to check it out. You don't want to deal with the burns
either. So the length of time is gonna be a few days. It looks like the worst of
it is gonna hit the middle of the country over the weekend and then it's
gonna kind of move east. So just check the weather and make any preparations
that you can check on those people that might need to be checked on and just be
aware these the temperatures that are gonna be reached they're no joke. Anyway
It's just a thought y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}