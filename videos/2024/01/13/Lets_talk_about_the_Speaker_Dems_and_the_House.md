---
title: Let's talk about the Speaker, Dems, and the House....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=DA9CHSXfQqU) |
| Published | 2024/01/13 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The U.S. House of Representatives Speaker appears to be gearing up to take action that McCarthy should have taken earlier.
- The far-right faction within the Republican Party held leverage through the motion to vacate, allowing them to obstruct and grab headlines.
- The strategy for the Speaker is to render this faction irrelevant by passing bipartisan legislation with centrist Democrats.
- Johnson, the current Speaker, seems poised to push through a budget with bipartisan support to solidify his position.
- Moderate Democrats have indicated they may support Johnson against the far-right faction.
- Protecting the Speaker helps the Democratic Party pass a compromise budget.
- By neutralizing the far-right faction, the Speaker can lead the Republican Party effectively.
- Trump utilized the far-right faction to control the Republican Party.
- Johnson's actions could reshape dynamics within the House of Representatives and the Republican Party.
- It's emphasized that Johnson is not a friend to everyone, despite his current actions.

### Quotes

- "Render them irrelevant."
- "It's basically a win for everybody except for the Twitter faction and Trump."
- "Johnson is willing to do what McCarthy was not willing to do."
- "If they lose that leverage, they don't have anything because they don't have any actual policy."
- "Johnson's not your friend."

### Oneliner

The U.S. House Speaker gears up to neutralize the far-right faction's leverage, reshaping dynamics within the House and the Republican Party, with potential support from moderate Democrats.

### Audience

Politically engaged citizens

### On-the-ground actions from transcript

- Support bipartisan legislation (implied)
- Back moderate Democrats supporting Speaker Johnson (implied)
- Stay informed on political dynamics (implied)

### Whats missing in summary

Insight into the potential long-term impact on the political landscape. 

### Tags

#USHouse #Speaker #BipartisanLegislation #RepublicanParty #PoliticalStrategy #ModerateDemocrats


## Transcript
Well howdy there internet people, it's Beau again. So today we are going to talk about the
U.S. House of Representatives, the speaker, and him certainly appearing to gear up to do what
McCarthy should have done, you know, back when he was still speaker. Something that a lot of people
said was just flat out inconceivable. Okay, so we have been talking about this
on this channel since McCarthy was Speaker of the House. The Twitter faction of the Republican
Party, the MAGA, loyalist, Trumpist, far-right faction, they willed an outsized amount of power
because of them using the motion to vacate. Well, we'll just get rid of the speaker.
Because of that, they can obstruct anything they want and get headlines.
The move, if you're the Speaker of the House, is to render them irrelevant.
Even if you're a Republican Speaker, you have to remove that leverage from them.
The way to do it is to push through bipartisan legislation over their objections by building
a centrist coalition with the Democratic Party and the bipartisan
legislation goes through and then those centrist Dems, well, they protect the
speakership. All signs point to Johnson doing this right now. He did some little
test runs earlier. It looks like he's about to go full bore with it with the
budget, which is a smart thing to do it with, because it's must pass. If it
doesn't pass, well we end up with a big issue. So right now he is saying that the
spending deal that was worked out between House leadership, Senate
leadership, and the White House is on track, and that's what they're gonna do.
The Twitter faction of the Republican Party, they are losing their minds.
Moderate Dems have come out and said, well, if they do a motion to vacate, we
might support Johnson, meaning they will. So as long as the the budget goes through,
he can rely on a faction of the Democratic Party to protect him.
him from being ousted. This helps the Democratic Party because it gets a budget through that
it doesn't give them everything they want, but it's a good compromise. It's like an
actual political compromise. But most importantly, that Twitter faction of the Republican Party
that is a thorn in the speaker's side,
well, that's also the committee faction.
That's the faction of the Republican Party
that is constantly out there
dragging people before committees,
creating investigations into things
that have been investigated for years,
and just constantly drumming up outrage over nothing.
So they get to render that faction
the Republican Party irrelevant. The speaker gets to bring them back in line and actually lead the
Republican Party. It's basically a win for everybody except for the Twitter faction and Trump
because Trump uses them to exert control over the rest of the Republican Party.
Right now it certainly appears that Johnson is willing to do what McCarthy
was not willing to do. Now that could change because now that you have members
of the Democratic Party saying, yeah we'll back him, the Twitter faction they
going to just, they're going to go all in on this because by this point they have
to realize what's going on. If they lose that leverage, if they lose the ability
to say we're going to vacate the chair, they don't have anything because they
don't have any actual policy. If McCarthy had done this he'd still be speaker. So
So we'll have to wait and see if Johnson sticks to his guns on this one.
If he follows through with everything that's indicated, it's going to alter the dynamics
within not just the U.S. House of Representatives, but also the Republican Party as a whole.
And then as soon as this is over, everybody needs to remember Johnson's not your friend,
be clear on this. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}