---
title: Let's talk about Trump, the current proceedings, and a total show....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=MHFVpYhLCd0) |
| Published | 2024/01/18 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Updates on Trump proceedings were expected to be smooth but turned confrontational.
- Judge indicated Trump’s exclusion for distracting behavior during proceedings.
- Trump's response to possible exclusion was defiant and dismissive.
- Judge provided basic courtroom instructions to Trump's defense team.
- Trump’s team tried to bring in prejudicial evidence without success.
- Judge denied Trump’s team adjournment request for a funeral.
- Confrontational interactions between Trump's team and the judge occurred throughout.
- The judge redirected the defense team on proper courtroom procedures.
- Official court transcripts were not cited, but the reported interactions were close to actual events.
- Trump may face worsening circumstances if his team doesn't regroup and change tactics.

### Quotes

- "I understand you're probably very eager for me to do that. To which Trump said, I would love it."
- "I'm asking for an adjournment for a funeral. The judge said denied, sit down."
- "It's worth noting those are not from official transcripts, but the portions that I was able to check through AP stuff, that's very close to what was said."

### Oneliner

Updates on Trump's proceedings turned confrontational as the judge indicated possible exclusion due to distracting behavior, reflecting a need for Trump's team to regroup and adjust tactics.

### Audience

Legal Observers

### On-the-ground actions from transcript

- Contact legal representatives for accurate and official updates on the proceedings (suggested)
- Stay informed about courtroom procedures and legalities to understand the gravity of courtroom conduct (implied)

### Whats missing in summary

Insight into the potential consequences of the confrontational approach taken by Trump's team during the proceedings.

### Tags

#Trump #LegalProceedings #CourtroomDrama #Confrontation #LegalObservations


## Transcript
Well, howdy there internet people, it's Bo again.
So today we are going to talk a little bit
about the current proceedings involving Trump.
Even though I said we probably wouldn't need
to do any updates on that because I expected it
to kind of just roll along and it be
a relatively smooth process.
I was wrong about that.
That is not how it is shaping up.
So, we talked about how I had heard two things about this judge, that there were two things
that this judge did not like in the courtroom.
One of those things was courtroom shenanigans, and the other was a victim blaming.
Given the fact that we are doing an update immediately after I said we probably wouldn't
need to do any updates, you can probably guess what occurred.
I have some highlights from some of the conversations in the courtroom that I'm going to read through.
These are in no particular order, but we are going to start with the moment that the judge
indicated that excluding Trump from the rest of the proceedings was an option because Trump
was reportedly talking during other people's testimony and it was distracting.
Trump, Mr. Trump, I hope I don't have to consider excluding you from the trial.
I understand you're probably very eager for me to do that.
To which Trump said, I would love it.
The judge says, I know you would.
You just can't control yourself in these circumstances apparently.
And Trump said, you can't either.
I am not an attorney, but I am fairly certain that the I'm rubber, you're glue legal defense
strategy is not going to pay off for the former president.
Generally speaking, getting into this kind of conversation with the judge is, well, it's
not really something like they would teach at Harvard Law.
It's probably something that should be avoided.
Moving on, there's a whole series in which the judge is trying to instruct Trump's defense
team on how things work in a courtroom and just provide some basic information at times.
One of them was Carroll's team saying, let's show this, and then there was an objection.
The judge asked for grounds, Habba says, it's prejudicial.
The judge says, all evidence is prejudicial against the party it is offered against.
I mean that sounds, I mean that sounds true to me.
Again, not my area of expertise, but that makes sense.
Then there's this part where Trump's team says I would like to address what your honor
is recommending in writing overnight.
You're proposing an instruction, right?
The judge said I want briefs on Friday before 4 p.m.
Trump's team says my client and I wish to point out Ms. Carroll can sit in front of
the jurors every day, but my client has to choose between attending his mother-in-law's
funeral.
The judge at this point interjects to say, I have ruled, sit down.
Haba says, I don't like to be spoken to that way.
Please refrain.
I'm asking for an adjournment for a funeral.
The judge said denied, sit down.
There's also a portion where Trump's defense team tried to represent something.
They asked Carol, do you know if there is a communications team at the White House?
Carol says, I don't know.
Haba says, I represent to you that it does.
The judge says, you're not going to be representing anything or you'll be a witness.
A lot of this, most people know that that's not how a courtroom works, just from watching
like Matlock.
And then my favorite part, Trump's team says, many people called you a liar before the president
made his statement.
Carol's lawyer says, she's not asking a question.
Haba says, I wasn't finished.
It says, you're a pathetic old hag, at which point the judge interjects and says, it's
not in evidence.
Haba says, I'm trying to get it in.
The judge says, we are not going to read out loud a document not yet in evidence.
We are going to take a break right here to 3.30, and you're going to refresh your memory
about how you get a document in.
It's worth noting those are not from official transcripts, but the portions that I was able
to check through AP stuff, that's very close to what was said.
That is not a dynamic that most people would want in a courtroom.
Being confrontational to that level with the judge is generally not a good idea.
My guess is that unless the Trump team regroups, it's going to go from bad to worse for the
former president here.
It's worth remembering that they're not retrying the case.
The, all of the allegations that the first case, the previous case, that that jury decided
on, all of those findings, they stick in this case.
So at its core, what's being determined is really how much Trump is going to pay.
judge I would not want to create a situation where there is that much
confrontation if I was in Trump shoes but here we are. So my guess is that
without regrouping it's going to go from bad to worse and if they don't regroup
group and try a different track, maybe we will have more updates because this
judge does not seem inclined to tolerate some of the things that have been
allowed that really aren't things that normally happen in a courtroom. It does
not look like there is going to be a lot of courtesies extended that would not be
extended to any other person it doesn't it doesn't seem that Trump being a former
president really carries a lot of weight here anyway it's just a thought y'all
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}