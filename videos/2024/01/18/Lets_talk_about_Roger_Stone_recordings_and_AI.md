---
title: Let's talk about Roger Stone, recordings, and AI....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ZSd1uVeLNKo) |
| Published | 2024/01/18 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits
Beau says:

- Roger Stone, a long-time figure in the Republican Party, is implicated in a recording discussing harming prominent Democrats before the 2020 election.
- Stone dismisses the recording as AI manipulation and denies its authenticity.
- The involvement of law enforcement, including the FBI, in investigating this recording is noted.
- Beau anticipates a trend where subjects of incriminating stories claim AI manipulation, leading to delays in investigations.
- The rise of AI-created materials in news stories is predicted, necessitating caution during future elections.
- The current case involving Roger Stone is expected to unfold further, with potential ongoing investigations and developments.
- The need for improved technology to verify evidence in such cases is emphasized.

### Quotes
- "They said that one of the other, of the two Democrats, quote, has to die before the election."
- "Get ready for this to happen a lot."
- "It's also a sign of things to come."
- "During this election, the 2024 election, you need to be on guard for AI-created material."
- "Anyway, it's just a thought."

### Oneliner
Beau warns of the rise of AI manipulation in incriminating stories, exemplified by Roger Stone's case, signaling future challenges during elections.

### Audience
Media Consumers

### On-the-ground actions from transcript
- Stay informed about evolving technology and its implications for news authenticity (implied)
- Advocate for better technology to verify evidence in news stories (implied)

### Whats missing in summary
The full transcript provides a detailed analysis of the potential impact of AI manipulation on news stories and the need for enhanced technology to address this issue effectively.

### Tags
#AI #RogerStone #NewsManipulation #ElectionIntegrity #LawEnforcement


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Roger Stone
and recordings and AI and a chain of events
that we need to become accustomed to
because it is going to play out
again and again and again and again.
We talked about it as a possibility a few months ago.
We're here, it's occurring
and it's gonna occur more frequently.
Okay, so if you have no idea what I'm talking about,
Roger Stone, who is a person
who's been around a really long time,
as somebody who does things
in support of the Republican Party,
he's a political dirty trickster.
A recording has surfaced that was reportedly recorded
in the weeks before the 2020 election.
On that recording,
certainly appears that Stone and a former NYPD officer were having a
discussion about taking out two prominent Democrats and by taking out I
do not mean to dinner. One of the quotes that kind of puts it into focus is they
They said that one of the other, of the two Democrats, quote, has to die before the election.
From the context of the recording, it seems as though the idea of being presented was
well, take out one of them and the rest will fall in line, basically scare them into submission.
police are reportedly investigating this and there is some light reporting that
suggests the FBI is assisting with that investigation. That all tracks, I mean
generally speaking, this is something that law enforcement doesn't like. Okay
So, what does Stone say? It's an AI manipulation. Never happened. Absurd.
Okay, so with this particular case, we'll wait and see what the investigation
turns out, but when it comes to normal news stories, get ready for this to
to happen a lot. Get ready for this to happen a lot. Where a story comes out and the subject
of that story is just like, that's AI, that's not me. It is going to drag things out. Now
realistically it's not going to end any stories, but it will delay them. And it's important
remember that some of them probably will be AI. I mean, I would be willing to bet
that if you asked Roger Stone if he had the ability to create AI videos to
disparage Democratic candidates, he would say, yeah, I would absolutely do
that. Some of them will undoubtedly be AI and some of them won't. So what that
means is four stories involving physical evidence that is harder to pin down and
harder to corroborate, it's going to take more time. That's really what it boils
down to. Until there is better technology to sift through it, that's
where we're at. This particular case, this is probably not the end of this. I
feel like the people involved who were the subject of the conversation, they're
probably not going to let this go. And it is unlikely that Capitol Police would let it go
anyway. They're going to want to know exactly what happened. So, this is the beginning of
another big news story that's going to take time. And it's also a sign of things to come. Because
during this election, the 2024 election, you need to be on guard for AI-created material
and people saying things were AI-created and having to wait to find out if that was really
the case.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}