---
title: The Roads to a Jan Q&A
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=jg1ycPOJayg) |
| Published | 2024/01/18 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- January 18, 2024, and a Q&A session is on for The Roads with Beau due to lack of long-format content this week.
- Questions sourced from email, and Beau shares the email address for future submissions.
- Deliberation on voting strategies, including leaving the top ticket blank, supporting a third-party candidate for protest, or sticking with traditional party choices.
- Explanation on engaging in a protest vote to send a message, despite Beau's skepticism about its effectiveness.
- Advocacy for building a political party from the ground up rather than focusing solely on presidential elections.
- Insights into the global perspective on American politics, especially regarding Trump's base and potential impacts of his re-election.
- Clarification on the distinction between tanks and infantry fighting vehicles like Bradleys, and the implications of such distinctions.
- Emphasis on the power of the American people as safeguards against potential authoritarianism in political leadership.
- Addressing the implications of a potential Trump re-election on the US and global economy, foreign policy, and national security.
- Speculation on US government/military actions in securing resources like clean water, including potential ecological agendas and invasions.

### Quotes

- "The safeguard is you."
- "You are the safeguard."
- "The US economy is just now starting to recover from his previous mismanagement."

### Oneliner

Be the safeguard: American people's power, global implications of potential Trump re-election, and the importance of grassroots political engagement.

### Audience

American voters, global citizens

### On-the-ground actions from transcript

- Join Operation Greyhound to adopt or foster retired greyhounds (suggested)
- Contact OperationGreyhound.com for more information on how to help (implied)

### Whats missing in summary

Insights on voting strategies, grassroots political engagement, and global implications of US politics.

### Tags

#VotingStrategies #PoliticalEngagement #GlobalImplications #AmericanPolitics #GrassrootsMovements


## Transcript
Well, howdy there, internet people, it's Beau again,
and welcome to The Roads with Beau.
Today is January 18th, 2024, and this is not,
the road's not taken, this is a Q&A.
This is a Q&A thing,
because we normally put something out on Thursdays,
and I didn't have any long format stuff this week,
so you're getting a Q&A.
This was described as totally random,
And these questions came from the email,
because I think the last time we did this,
they came from Patreon.
If you are interested in sending in a message,
the email is questionforboe at gmail,
and it's the word for.
Okay, so starting off, my question to you is,
should I simply leave the top of the ticket blank,
or is there another candidate that I should vote for?
My daughter supports the socialist candidate,
given that she is almost certain to lose, is there a better way to cast my vote?
A write-in. I managed to hold my nose and vote for Hillary in 2016, despite my contempt over
her vote for war in Iraq, but I can't do it for Biden. Wow, that last sentence is, that is wild to
to me, but, okay, it's wild to me because I personally view Clinton as worse.
Okay, so I don't believe that it is my place to tell people how to vote.
I don't do that.
In fact, if you look, the only candidate I have ever endorsed was Big Bird against Ted
Cruz.
That's the only candidate I have ever endorsed.
It's one of those lines I don't want to cross because I don't want to become a cheerleader
for any candidate.
That being said, I have gotten enough questions about how to do a protest vote, which is what
this is.
There's no way the third party candidate would win, but the idea behind this is to
send a message.
I am very much on the record that I don't really think that this is a super effective
way to do things, but I get messages asking how to do it, which tells me those people
who believe that this is a good idea probably aren't putting out a plan that a whole lot
of people can get behind.
So in the interest of diversity of tactics, because even though I don't think it would
be incredibly effective, I could be wrong, you know?
I will try to put out a video going over a way to try to engage in a protest vote and
send a message without doing more harm than good, because I do believe that both could
actually be done.
I don't necessarily think engaging in a protest vote will bring about calamity if it's done
right.
And for those who are curious, I think that if you're going to try to build a party that
is further to the left than the Democratic Party, you have to build the party.
You have to start at the ground level, at the local level, and work your way up and
actually build the party, not just try to put in a different president.
Because even if that president gets in, well, they have to deal with a Congress that is
not aligned with them.
So they don't really have a lot of power.
Presidents have the power to make things worse, not really make them better on their own.
They need Congress for that.
So I will try to get a video out on that, and you could also try to shift the Democratic
Party from within and realign it by running candidates again at the local level, and then
as time went on they would get sucked up.
Actually there's a video on this channel called The Roads to Realignment that goes through
that process just substitute the drone strikes for elections and you can alter the organizational
structure the same way.
So you could do it without all the violence that's talked about in that video.
Okay, so next one.
Hiya.
I'm one of your Patreons, supporters, and a regular watcher of your channels.
But I'm also not an American. I'm an Australian living in Sydney as someone on the outside looking at your political
situation
It really looks as if half your country is literally insane.
I see footage of interviews with apparently normal Americans saying that only Trump can be trusted on this issue
Or only Trump cares about the little people etc. This seems like absolute lunacy
The rest of the world is watching the US political scene with some trepidation
hoping that sanity will prevail. But in the event, but in the face of half your
population seriously considering re-electing someone who is so
obviously greedy and self-serving, willing to say anything to anyone to
squeeze money out of them, let's just say feeling despair is putting it mildly. Can
you explain for those of us outside the US why so many of you can't seem to see
through Trump. It goes back to the beginning. When he first popped on the
scene as a Republican, keep in mind he was a Democrat before that, and keep in
mind before he ran as a Democrat he was he was a reform candidate as well, he
He launched his platform around playing into nostalgia and giving people who
did not want to progress with the rest of society permission to be their
absolute worst. That's how he built a base. A lot of his base is still with him.
In the U.S., there are a lot of people who will support their political party no matter
what.
And I feel as though what happened in 2020 was the result of Republicans being like,
yeah, I'm not doing it with this guy again.
My gut tells me that's going to occur again in 2024.
But I can't say there's no possibility that he would get elected again.
So we'll, yeah, that's what it boils down to.
At the end of it, it boils down to nostalgia, bad information, people wanting to believe
something because it reinforces their preconceived notions and giving people
permission to to be bad people. It's those things that's that's how he built
his base. With dwindling stockpiles for Ukraine do you think the old Pompkis
bunkers might get used and would M14s be useful on the field. Okay so a little bit
of context. Poncas bunkers were... I believe they've been acknowledged. If not
they're a myth. They're a rumor that won't die. That during the Cold War the
United States in particular, but NATO as a whole, created storage facilities for
weapons and equipment, some of them big enough to house like a lot of tanks, and
that these are not the pre-positioned equipment that everybody knows about, but
there's like secret ones, and that these have been maintained, but a lot of the
equipment in them is older. Maybe that's true. Wood M14s, yeah, I mean it just
because it's not the most modern or best it's still a functional thing. All that
being said I don't I don't think it's gonna get to that point. Could this
happen? Sure, assuming these things actually exist, but I don't think that's going to
be necessary. Big fan, but maybe some happy news along with some education. A Vermont
water rescue just in the past few days. Heroes, we all need them, but also first aid. What
What to do with a person you pulled out of an icy lake?
You've discussed IFAC and First Aid, but what about how to treat potential hypothermia?
This sounds like a suggestion for a video over on the other channel.
So you've got somebody out of an icy lake.
First things first, be gentle.
Don't like rub their skin or anything like that to try to warm them up.
You want to remove wet clothing, insulate them from the ground, then use layers of blankets
to warm them up, obviously dry them off gently.
And then, so there's warm compresses that can be used as well, but I'm kind of reluctant
to talk about that too much because I've read some studies and basically check out the Mayo
clinic on that because the latest stuff I've seen says that if you use it on the
limbs it could be really bad, like fatal. So be gentle, get the clothing off of
them, dry them off, insulate them from the ground assuming they're going to be
outdoors, and use layers of blankets to warm them up. Obviously call 911 and
Key thing to remember, you're not dead until you're warm and dead.
So if you are performing other kind of life-saving stuff, keep doing it until EMS shows up.
Because you're not dead until you're warm and dead.
If you want to know more about that, Google that phrase.
My question, part one.
I found the musical choices you reference to be good and interesting.
What was the last artist you listened to?
The last artist I listened to was Dave's True Story.
There is no part two to this.
That's funny.
Can you explain the Bradley T90 thing and why War Twitter is going crazy over a tank
blowing up a tank?
a Bradley's not a tank. Do you remember those videos? When the US sent Bradleys
over, there was this whole thing about what qualifies as a tank and what isn't.
A Bradley is not a tank. I know it has tracks like a tank and it has a turret
and it looks like a tank, but technically that is an infantry fighting vehicle. The
The T-90 is the top of the line of the Russian side, so the idea that an infantry fighting
vehicle that is totally not a tank took one out has people questioning tanks in the future
and stuff like that.
It's worth remembering that the Bradley is not a normal infantry fighting vehicle.
That auto-canon is something else.
But that's why they're talking about that footage.
Okay, Canada Loves Your Show, is the United States ready for another Trump loss?
I think so, assuming I'm reading that question right.
I think so.
there will be, if it occurs and Trump loses, I think there will be sporadic low-level issues
that arise but probably nothing huge. My question part two. Hopefully your team will separate these
so you can't see this part when you answer the first.
Was your answer a woman with a sexy voice?
If it was, do you know that I just want to bet?
Also, are you aware that your musical choices often lean towards women with sultry voices
and at least some political undertones to their music?
If you are aware, have you examined why that might be?
Okay, y'all are psychoanalyzing me now.
What's your answer?
Dave's Truth Story does, in fact,
have a singer with a sultry voice.
That is true.
No, I hadn't really, I did not know that about myself.
I didn't really think about it,
and so no, I have not examined why that might be.
And you had a bet about that, that's all right.
Okay, moving on.
With a chance of a Trump presidency looming over our heads, there is nonstop talk of doom
and gloom for life in the United States.
With most of his rhetoric and fear-mongering, I tend to believe, such as Project 2025, Day
One Dictator, changing, ignoring the Constitution, and countless others.
My question is, does the U.S. have safeguards in place to prevent him from turning the United
States of America into the United Kingdom of Trump.
That safeguard is you.
That safeguard is you, the American people.
The idea is that somebody like Trump would never be in office.
The idea is that if they got in, their own party would put the country above their party
and impeaching and evict.
You are the safeguard. Make no mistake about it.
There are a bunch of systems in the United States that are incredibly
resilient
to somebody who wants to be a day one dictator.
But there are limits to everything.
It would depend on what happens in the House and the Senate and a whole lot of
other things.
the actual safeguard is the American people rejecting Trumpism.
From an outsider's perspective, it appears to me that the further out of touch the GOP
gets from the general population, the more authoritarian they become.
Is that what is happening or just my flawed perspective?
No, you're right.
You're right.
A lot of their policy is based on the beliefs from the 1950s to the 1980s, and it hasn't
changed.
It is out of touch with the average American.
Because they don't want to change their policy, they have become more authoritarian in trying
to force their ideas and force their way into power so they can control the levers of power.
Yeah, no, you're right.
If you weren't married to Mrs. Bo, which YouTube personality would you like to date?
Which do you find the sexiest?
Not something I'd put a lot of thought into, to be honest.
Even though Biden is less worse than Trump, after the PR and human disaster in dealing
with the recent Palestinian-Israeli situation, VoteBlue, no matter who, seems to be losing
a lot of steam, at least from a non-American perspective.
In case the worst happens, Trump re-election, what would that entail for the rest of the
world?
include South America, I like to think we matter. Okay, so if if Trump is elected
again what would it entail for the rest of the world? The US economy is just now
starting to recover from his previous mismanagement. When he went into office
he was riding Obama's economy. If Trump was re-elected, there would be economic
issues, big ones, and those would definitely cause ripples in South
America. Given the fact that his base does not like most people from South
America, his foreign policy towards South America would be at best
isolationist, you know, tariffs and stuff like that, and at worst outright
confrontational. So it wouldn't be good. It wouldn't be good. I do not believe
that he would like invade a South American country or anything like that.
I think he might like order strikes on different non-state actors and stuff
like that but I don't think there would be like full-on military force. Yeah.
Do you think the U.S. government slash military would enforce some ecological agenda or outright
invade countries to secure clean fresh water?
Absolutely.
No hesitation.
Yes.
If the United States was in a position where access to fresh water became an issue for
a large percentage of the population to the point where it would impact the economy, impact
national security, stuff like that, you can bet that a country with a whole lot of fresh water,
they're going to have some kind of violation, international violation,
you know, they'll use international law, but that country, though, they're going to need some freedom.
The US government would absolutely do that and manufacture a pretext to do so.
Hey Beau, long time watcher from New Zealand. I recently read about the hundreds of unmarked
graves at Hines County. Yeah, okay. So this is about something going on in Jackson, Mississippi.
I don't want to go too far into it because I know somebody who is actively trying to find out
information about that and I am going to wait to see what they find out. This is
not something, there's been a couple of messages about this, this is
not something that I am ignoring or don't think is important. This is
something that I think is important enough to have actively made combat or
contact with somebody who is investigating it. And I'm just going to wait to see what
they find out. And if you have no idea what I'm talking about, in Jackson, Mississippi,
There is a cemetery of sorts where the family members weren't notified, and it appears
to have some real shady things that occurred.
The term terrorist organization, in quotes, to me seems overly political, often being
used by those in power to label an enemy for future theft of power coupons.
I believe labeling acts to be far more accurate.
Acts are definite, whereas organizations can be vast and nuanced.
Do you believe the term terrorist organization has a proper use, and if so, would you define
the term. I'm willing to bet that I said the T word more in this video than I have in my
other ones. Yeah. I believe that that term gets used to label a group as bad guy for
PR purposes. It should be used to identify the strategy that they deploy, which can lead
to a better understanding of workable options and in theory get to peace.
You know that word has become synonymous with bad guy.
The reality is that that is a very specific strategy and just applying that term outside
of that strategy gets complicated and it undermines the understanding of that strategy which means
countering them is even worse, it's even harder.
So to me it should be whether or not the individual or organization is actually deploying that
strategy and I can tell you right now a whole lot of the groups that get that label, they
They are not.
And a bunch of groups that don't get that label absolutely do.
So that is my concern with that term.
For dog-friendly people in, oh, this is cool, okay, so I already saw this one. Short version.
If you live in Southern California and you're interested in a greyhound, a dog, go to OperationGreyhound.com
slash dogs slash dogs dot html.
I'm pretty sure you could probably just go to OperationGreyhound.com.
From what I understand, they have a whole bunch of dogs that have been retired and they
they need to find homes, adoptive foster,
short version, they need help.
And this came in from two different people.
I haven't talked about it yet, because I was actually
going to reach out to them and check it out
and see what they needed exactly.
But two different people have messaged about this.
So I would imagine that they definitely need help.
I just don't have more information than that.
So I would check them out and see if there's anything you could do, especially if you like
big tall super fast dogs.
And that looks like it.
Those are all the questions.
So hopefully that's a little bit more information, a little bit more context, and having the
right information will make all the difference.
You all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}