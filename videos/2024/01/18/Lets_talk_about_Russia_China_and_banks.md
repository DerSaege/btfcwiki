---
title: Let's talk about Russia, China, and banks....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ywymgw42Ves) |
| Published | 2024/01/18 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits
Beau says:

- China and Russia have a special relationship described as a "friendship without limits."
- China's interest in banking interest has led them to limit ties with sanctioned entities in Russia.
- The US authorizing secondary sanctions has influenced China's decision to distance itself from Russia.
- China's economy is stumbling, not collapsing, and it does not need further economic issues with the US.
- China's actions, though potentially helpful to Russia, may not have a huge impact beyond public perception.
- This situation illustrates that even countries with longstanding friendships have limits due to differing interests in foreign policy.

### Quotes
- "Countries don't have friends. They have interests."
- "Even if countries have a relationship of friendship for years and years, there are limits regardless of rhetoric."

### Oneliner
China's distancing from Russia due to US sanctions shows countries prioritize interests over friendships, even in longstanding relationships.

### Audience
Foreign Policy Analysts

### On-the-ground actions from transcript
- Monitor the evolving relationship between China and Russia to understand shifting geopolitical dynamics (implied).
- Stay informed about the impact of US sanctions on international relations (implied).

### What's missing in summary
The full transcript provides more in-depth analysis on the implications of China's actions on their relationship with Russia and the broader geopolitical landscape.


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Russia and China
in friendship and limits and interest and economics
and all kinds of things.
We're gonna do this because China and Russia
have a special relationship.
It's been described as, quote, a friendship without limits.
And that is a phrase that shows up in the comments pretty often,
because I frequently use a different phrase.
Countries don't have friends.
They have interests.
Turns out that one of China's interests is, well, interests,
like banking interest.
The reporting suggests that China has directed its state banks to limit ties with certain
sanctioned entities inside Russia, basically no longer circumvent or help Russia trade,
at least in the banking industry.
The obvious question is, why?
Because the US is authorizing secondary sanctions.
What that means is, let's say there is a company in Russia called Putin Trading Company,
and Putin Trading Company is on the sanction list, and the first bank of Beijing is doing
business with them.
Well, the first bank of Beijing may get sanctioned as well, a secondary sanction.
The reporting suggests that China decided, yeah, that's not worth it.
We're not risking that.
And it makes sense.
It's probably a smart move.
It is in China's interests to not get too sucked in to Russia's current situation.
China's economy, as we've been talking about, it's stumbling.
If you look at a lot of reporting, it's being cast as if it's just collapsing.
It's not, at least not according to the economists that I've talked to.
It's stumbling.
It's having issues, but it's not like it's on the verge of collapse.
issues with the United States when it comes to economics, that is not
something that the Chinese economy needs right now. So they're playing it safe.
Will China engage in extracurricular activities to maybe help Russia avoid
certain sanctions? Maybe, probably, but this is a big step because this is
something that's public and it is definitely not something that is going
to sit well with the Kremlin. In fact, the questions that have been asked about it
have basically gotten the response of this is a very sensitive matter and I
don't think anybody's going to answer those questions, which is code for you
better stop asking about this. We'll see how much impact this has. Based on what
I've seen I don't know that China was really helping Russia that much and if
that's the case then this isn't actually going to have a huge impact other than
the obvious public perception. But one of the reasons I wanted to bring it up
was it's a very clear illustration that even if countries have a relationship of
friendship for years and years that there are limits regardless of rhetoric.
There are always limits because countries do not share the same
interest. Every relationship on the foreign policy scene has limits, and a
whole lot of them are being tested right now. This is a pretty sharp
turnaround. I would imagine that there will be those in the Kremlin who view
this as a betrayal.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}