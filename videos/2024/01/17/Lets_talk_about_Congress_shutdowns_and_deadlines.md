---
title: Let's talk about Congress, shutdowns, and deadlines....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=z-dkiYLegak) |
| Published | 2024/01/17 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Congress is running out of time to pass a budget to keep the government running.
- The Senate is making progress, but the House is facing challenges due to far-right opposition.
- The House Speaker is working to push through a continuing resolution with help from the Democratic Party.
- There's a risk of disruption from rogue Republicans or the right-wing Twitter faction, which could lead to a shutdown.
- The best-case scenario involves the Senate smoothly passing the budget, with support from Democrats and moderate Republicans.
- Dysfunction in the House and among Republicans puts the government at risk of shutdown once again.

### Quotes

- "Government is yet again on the brink of shutting down."
- "The best case scenario as far as keeping the government open is that it glides through the Senate."
- "Once again, due to dysfunction in the House, dysfunction with the Republican party..."

### Oneliner

Congress is racing against time to pass a budget, with the Senate progressing but the House facing challenges from far-right opposition, risking government shutdown. 

### Audience

Citizens, Voters

### On-the-ground actions from transcript

- Contact your representatives to urge them to work towards passing a budget to avoid a government shutdown (suggested).
- Stay informed about the budget process and potential government shutdowns (suggested).

### Whats missing in summary

The full transcript provides a detailed breakdown of the current situation in Congress regarding budget passing, funding, and looming deadlines.

### Tags

#Congress #Budget #GovernmentShutdown #House #Senate #PoliticalAction


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about Congress,
and the budget, and funding, and deadlines,
and how they're kind of running out of time.
And we're going to go over what is occurring
and how long they have to complete the task.
If you don't know, Congress, all of Congress,
the Senate and the House, they have until Friday
to push through something to keep the lights on
when it comes to the government.
The Senate is moving along.
There was a procedural vote.
It was 68 to 13 in favor of moving it along.
And that is to basically set up new deadlines,
kick the can a little bit further down the road,
and the new deadlines are in March.
Odds are things will work out in the Senate. It is a more deliberative body and it doesn't have the
the issues with dysfunction that the House has. The House is a different story. The House is a
different story because you have the speaker there. You have Johnson who is still trying to
fend off the far-right Twitter faction, who for some reason is opposed to even a
continuing resolution to keep the lights on. Now, realistically, he probably will
be able to push this through. The odds are in his favor that he'll be able to
get it through. He will definitely need help from the Democratic Party, but this
is one of those must-pass things where he'll get it. But there is the
possibility that either a rogue Republican senator or the right-wing
Twitter faction in the House, that they may be able to disrupt it, to delay the process.
And not that they'd be able to defeat it, but they could delay it long enough to get
the shutdown started.
And then once that happens, all the math changes.
So the best case scenario as far as keeping the government open is that it glides through
the Senate, and it very well might, and that the Democratic Party and the more moderate
Republicans, they just kind of, again, ignore the far-right Twitter faction and render them
even more irrelevant. That's the best case scenario and it's not out of the question.
But once again, due to dysfunction in the House, dysfunction with the Republican party,
government is yet again on the brink of shutting down. Anyway, it's just a thought. Y'all have a
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}