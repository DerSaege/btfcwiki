---
title: Let's talk about Kansas and the GOP not giving up....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=fVbcnMHrsMA) |
| Published | 2024/01/17 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Kansas voting bill introduced to restrict reproductive rights despite overwhelming public support for access.
- Republicans pushing HB 2492 despite clear public stance on reproductive rights.
- Co-sponsor claims students asked for the bill, but its passage is unlikely.
- If bill passes, governor likely to veto; Republicans may attempt to override veto.
- Courts expected to find the bill unconstitutional if enacted.
- Voting process in Kansas shown as a facade to demonstrate citizens' insignificance.
- Citizens express desire for access to reproductive rights, met with opposition from Republican Party.
- Republicans prioritize their agenda over public opinion, driven by corporate donors.
- Public stance in Kansas contrasts sharply with Republican Party's actions.
- Republicans view themselves as above the people of Kansas, dictating policy.
- Persistent attempt to limit rights despite public opposition.

### Quotes

- "Voting is a show in some ways, and it's to show the people of Kansas that you don't matter."
- "People in Kansas made it very clear where they stand on this issue."
- "The Republican Party is making it very clear where they stand on the people of Kansas, And that's above them, telling them what to do."

### Oneliner

Kansas voting bill introduced to restrict reproductive rights despite overwhelming public support; Republicans prioritize agenda over citizens' voices.

### Audience

Kansas Residents

### On-the-ground actions from transcript

- Organize community forums to raise awareness about the bill and its potential impact (suggested).
- Contact local representatives to express concerns about the restrictive bill (implied).
  
### Whats missing in summary

Full understanding of the context and implications of Kansas voting bill and its impact on reproductive rights advocacy.

### Tags

#Kansas #VotingRights #ReproductiveRights #RepublicanParty #PublicOpinion


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Kansas and voting
and a bill that was introduced, what it means,
what the story behind it is,
what the likelihood of success is,
and then if it does move through all of those steps,
what the courts will probably determine.
Because, y'all might remember, back in 2022, the people of Kansas sent a message.
And that is that they overwhelmingly wanted to keep access to reproductive rights in their
Constitution.
So obviously, since it's in the Constitution and the people recently voted, just overwhelmingly
in support of it, Republicans gave up on trying to take away people's rights.
No, of course not, because they're not your representatives, they're your rulers.
You don't know what you want, you have no idea what you want, you're just one of those
common folk you need to let your bettors decide. HB 2492 was filed on January 10th
and it is a pretty restrictive ban. It's a pretty restrictive ban. It does allow
for some exemptions but not many. So why was this filed? One of the co-sponsors
said that, well, some students asked me to do it. So, you know, we told him it wasn't
going to go anywhere, but, you know, didn't want to discourage him. And, you know, free
speech and all of that. Okay, so what's likely to happen? Odds are it won't pass. Odds are
it won't pass. If it does pass, it goes to the governor. The governor, more than likely,
veto it. I would say almost certainly. Once it's vetoed, odds are that if they do push it through,
they would try to override the veto. Republicans have the numbers to do it.
Then it goes to the courts, where the courts will decide that it can't be enacted. The whole thing
Voting is a show in some ways, and it's to show the people of Kansas that you don't
matter.
They will continue to push what legislation they want.
You will continue to obey because you need to know your place.
The trend that is occurring across the country is the citizens of a state say, hey, you know,
we actually want access to that.
And it's done through ballot initiative or however, but it's made abundantly clear.
And then the Republican Party says, no, this has been our wedge issue and talking point
for too long.
We still have to take away your rights, at least try, because you don't matter.
They don't represent you, they represent their donors.
They will continue to do this as long as they are re-elected.
People in Kansas made it very clear where they stand on this issue.
The Republican Party is making it very clear where they stand on the people of Kansas,
And that's above them, telling them what to do.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}