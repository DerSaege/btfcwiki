---
title: Let's talk about Trump and the current E Jean Carroll case....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=vyTFA3QGmY0) |
| Published | 2024/01/17 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump is involved in a legal case with E. Jean Carroll in New York, where he has already lost and is now determining damages.
- The previous case found that Trump did assault Carroll, and the judge has indicated that victim-blaming won't hold up.
- Trump's legal strategies, including courtroom shenanigans, might not work in this case.
- The judge in this case is described as no-nonsense, and Trump's team's efforts may not be successful.
- E. Jean Carroll's team has an expert witness to calculate the damages to her reputation, which Trump's team opposes.
- The case doesn't look good for Trump, and it's anticipated to focus on the damages Carroll sustained.

### Quotes

- "The judge has said that he can't argue that he didn't do it."
- "One is engaging in courtroom shenanigans, and the other is victim-blaming."
- "I'm not really sure what Trump is hoping to accomplish here."
- "It doesn't look good for Trump, but we'll wait and see how it plays out."
- "Her team wants him to quote pay dearly because of what they are alleging are continued attacks."

### Oneliner

Trump lost in a legal case with E. Jean Carroll in New York, facing damages, with victim-blaming ineffective and courtroom shenanigans unlikely to work.

### Audience

Legal observers and those interested in accountability.

### On-the-ground actions from transcript

- Follow updates on the case to understand the legal proceedings and potential outcomes (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of Trump's legal entanglement with E. Jean Carroll, offering insights into courtroom dynamics and potential outcomes.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump
and New York and shenanigans
and Trump's most recent legal entanglement
and how that is shaping up
because he kind of already lost.
And we're just gonna run through what has occurred so far
and where things will go from here.
So we are going to be talking about
the current E. Jean Carroll case.
And for the sake of clarity,
the one that is occurring right now
we will refer to as the current one.
The one that has already been decided
will be the previous one.
That sounds like common sense,
but if you know how the case has developed,
it's actually not.
Okay.
So in the current case, Trump's already lost.
It's already to the point where it's just,
are determining how much the damages are going to be. The previous case's finding that Trump did
in fact assault her holds. The judge has said that he can't argue that he didn't do it. Also
kind of indicated that they can't attempt to paint her as promiscuous. That's not something
that's going to hold up, and that's interesting because of something that I found out.
One of the people that I know happens to have a decent read on this judge, and they said
that there are two things that you don't want to do in that courtroom.
One is engage in courtroom shenanigans, and the other is victim blame.
If you are familiar with Trump's previous illegal strategies, you probably see how this
might present an issue for him.
As far as courtroom shenanigans, Epstein, who is somebody who is always in Trump's
Okay. He wanted to talk and the judge, are you a member of the bar of this court?
I'm a member of the New York State Bar. All right, then please have a seat. The
The judge is very no-nonsense.
Based on what the instructions are and how things have already been determined, I'm
not really sure what Trump is hoping to accomplish here, to be honest.
I know that the team behind E. Jean Carroll have a witness that is, I guess they're an
expert witness when it comes to what it takes to repair a reputation and all of that stuff
and calculating all of that.
The Trump team really doesn't want that person involved, but it looks like they kind of made
that complaint a little too late, but the judge did say that the questions that they
have are fair game for cross-examination.
Aside from that, I don't know what they're doing.
It doesn't look good for Trump, but we'll wait and see how it plays out.
doubt this is going to be one that there are daily updates on that are really
worth going over because it's going to very much be a rehash and it's going to
talk mainly about, mainly about the damages that E. Jean Carroll has
sustained and what the monetary compensation for that should be. It is
worth noting that her team wants him to quote pay dearly because of what they
are alleging are continued attacks. But yeah this one was kind of over before it
started. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}