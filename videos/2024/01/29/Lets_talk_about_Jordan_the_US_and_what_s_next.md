---
title: Let's talk about Jordan, the US, and what's next....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=kjE6HIExUX0) |
| Published | 2024/01/29 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A strike on an installation in Jordan resulted in the death of three U.S. service members, with speculation on the response.
- Although media reports point to Iran's involvement, it was actually one of the groups they support.
- The U.S. is planning a military response, timed at their choosing, which may not be immediate.
- The risk of expanding the Israeli-Palestinian conflict is subsiding, with other potential hotspots identified.
- Republicans are urging a direct hit on Iran, but Beau criticizes this approach as dangerous and politically motivated.
- Beau argues for a measured response that targets high-value individuals, avoiding civilian casualties and unnecessary escalation.

### Quotes

- "The smart move here is a measured response that is costly, not just loud."
- "You want a measured response. You want to behave like a world power instead of some third rate authoritarian goon."
- "Don't let them stand on the corpses of people far better than themselves to score political points."
- "It's just not smart. And they know that."
- "Our harassment campaign is one thing, but if you actually kill American troops, we are going to remove high-value people from the field in a surgical manner."

### Oneliner

A measured response to Middle East tensions is key, avoiding unnecessary escalation and prioritizing strategic action over political posturing.

### Audience

Decision-makers, policymakers

### On-the-ground actions from transcript

- Plan a measured military response (implied)
- Prioritize strategic actions over political posturing (implied)
- Advocate for policies that avoid civilian casualties and unnecessary escalation (implied)

### Whats missing in summary

Insights on the potential impact of foreign policy decisions on civilian lives and international relations.

### Tags

#MiddleEast #Iran #USResponse #PoliticalAnalysis #ConflictResolution


## Transcript
Well, howdy there internet people, it's Beau again. So today we are going to talk a little bit
about what is going on in the Middle East. We're going to talk about what occurred,
what the response may be, the political situation at home, whether or not this is going to cause
things to widen, so on and so forth. And just kind of run through it all because there's a lot,
there's a lot going on and there's even more speculation that not all of it is great. So
So we're just going to kind of run through it all.
Okay, so if you have no idea what I'm talking about,
you missed the news.
There was a strike on an installation
that had U.S. troops present.
Unlike all of the previous ones,
in this strike, three U.S. service members were killed.
Strike occurred in Jordan.
There is a big difference
between, wow, that's an annoying hit on the installation.
Wow, we had to activate our CRAM, something like that,
and killing three US service members.
There's a big difference.
The media is faithfully reporting that it was Iran.
Was it Iran?
No, not really.
It was one of their groups.
Does that matter in this case?
It was one of the groups they support,
of their non-state actors. Does that matter in this case? No, not really. It mattered before.
That distinction of, oh, that wasn't us, Iran sitting there denying it, as they are in this case,
that mattered before because no U.S. service members were killed. That distinction really
doesn't matter anymore. So, it wasn't really Iran, but Iran will be held
responsible for this. When is that going to happen? At a time and place of the US
is choosing. There's already been some sanctions and stuff like that, but
there's going to be a military response as well. It may not be immediate, and
And that was kind of coded in what the administration said.
They said, at a time and place of our choosing.
My guess that means is that there are certain people that they weren't removed from the
field and that time and place will occur when they know where all of them are at.
Does this run the risk of widening the Israeli-Palestinian conflict?
No, not really.
This is a thin distinction, but it's worth making.
That risk is more or less subsiding.
The risk of that conflict expanding is more or less subsiding.
The real place you have to worry about it is on the northern side of Israel right now.
That's about it.
There are other hot spots and flashpoints, but those would be more of people taking advantage
of the situation and a new conflict emerging rather than an expansion of the Israeli-Palestinian
one.
I know that's a fine line, but it's one that matters.
Is all of those entities and countries that have been telling the Palestinians for decades
that, you know, if you could just get public opinion on your side, we'd back you up, we'd
come help.
They're not.
If they were going to do that, they would have done it by now.
So it's not an expansion of that conflict.
It would be a new one.
Is there a risk of a new conflict emerging because of this?
Yes, but it's relatively small.
Right now there are a bunch of relatively small flash points.
Flash points that carry relatively small risks.
The problem is there's a bunch of them.
So there could still be another conflict emerge.
It wouldn't, again, it wouldn't be a widening of the Israeli-Palestinian one.
Not at this point.
It would be something new that occurred because somebody pressed their luck because they thought
everybody was distracted, that kind of thing.
Politically in the United States, Republicans are basically saying that Biden has to hit
Iran directly and do it now.
Why are they saying that?
I'm tired of sugarcoating stuff like this because they're dumb and they're lying to
their base.
That would be a horrible move.
It would get a bunch of Americans killed.
It's just not smart.
And they know that.
It's getting very tiring, sugarcoating stuff like that.
The idea that the Biden administration left troops hanging out to dry, which is something
that's kind of being pushed right now, that is not what occurred in any way,
shape, or form. Yes, the installation where they were had was less defended
because it was in Jordan, not in Syria, not in Iraq. Yes, it had a different threat
level. The idea that immediate responses is what works and all of that, it's not
true. That's just a lie people learn from action movies. It's not a good idea. The
idea that, you know, it troops are less safe under Biden than they were under
Trump. Yeah, I beg to differ. Why don't you take a look at the actual lists?
You're going to see that one is about three times as long as the other.
There's a bunch of bad talking points coming from the Republican Party because they care
less about American troops, less about civilian lives, less about civilian lives overseas.
They care less about those things than they do their own political seat.
Don't let them stand on the corpses of people far better than themselves to score political
points.
The smart move here is a measured response that is costly, not just loud, not just something
for CNN to show footage of, but something that makes it very clear.
Our harassment campaign is one thing, but if you actually kill American troops, we are
going to remove high-value people from the field in a surgical manner, and we can do
it whenever we want.
That's the right move.
One that doesn't involve hurting civilians.
One that doesn't create more animosity.
that's actually effective instead of playing for the cameras. The playing for
the cameras is what creates all of that footage and while if you are one of
those people who think foreign policy should be dictated by 1980s action
movies sure that footage looks good to you as an American. That footage also
gets played over there. They get the internet in the Middle East too and when
they see cities being destroyed, it tends to anger them. When civilians get hit, it
tends to anger them. You want a measured response. You want to behave like a world power instead
of some third rate authoritarian goon.
So right now, Biden is signaling that he is going to behave as a world power.
We'll have to see if that plays out.
Anyway, it's just a thought.
Go have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}