---
title: Let's talk about the US slowing things down....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Z92omrH3Bqo) |
| Published | 2024/01/29 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains why the US can't just stop providing military aid to Israel and the implications of doing so.
- Raises questions about the effectiveness and ethics of slowing down delivery of offensive systems to Israel.
- Clarifies that slowing down delivery of JDAM guidance systems could lead to more civilian casualties in Palestine.
- Suggests that slowing the delivery of 155 artillery shells could be a more effective way to pressure Israel without causing harm to civilians.
- Points out that the public reporting on the potential slowdown may impact the effectiveness of the strategy.
- Emphasizes the importance of considering the details and potential consequences before implementing such actions.
- Expresses concern over the light reporting on the issue and questions whether it should have been made public.
- Advocates for prioritizing actions that protect lives and aiming for effectiveness in altering Israel's strategy.

### Quotes

- "If Israel doesn't get those, it's not like they're gonna stop bombing. They're just going to be less accurate, less precise, which means they'll probably end up using more of them."
- "This shouldn't have come to the public's attention to be honest."
- "Will it work in the sense of altering Israel's overall strategy? I don't know."
- "So hopefully it will proceed with the 155s, slowing delivery of those. That would have the greatest effect and protect life."
- "It's just a thought."

### Oneliner

Beau questions the effectiveness and ethics of slowing down military aid delivery to Israel, expressing concerns about potential civilian casualties and the impact of public reporting.

### Audience

Policy makers, Activists

### On-the-ground actions from transcript

- Contact policymakers to prioritize actions that protect civilian lives (implied).
- Stay informed about foreign policy decisions and their potential impact on conflict zones (implied).

### Whats missing in summary

Detailed analysis of the historical context and ongoing conflicts between Israel and Palestine.

### Tags

#ForeignPolicy #MilitaryAid #Israel #Palestine #CivilianCasualties #Ethics


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about things slowing down
and whether or not that'll change anything.
We're going to talk about a couple of questions.
We're going to provide some clarity.
We're going to talk about how things that might seem like a
good idea aren't.
And definitely talk about how the devil is in the details.
OK, so the two questions.
First, why can't the US just stop instead of slow?
There'll be a video down below.
There's 1,000 foreign policy reasons
why the US can't just stop providing aid.
If you have no idea what I'm talking about, miss the news.
There is reporting that suggests the United States is
considering slowing delivery of some types of military aid
to Israel, not stopping, slowing,
creating an artificial delay in hopes of encouraging them
to scale back operations.
Okay, the next question was,
will this work, is it a good idea?
Will it work?
Maybe.
Is it a good idea?
Depends on how it's done.
And I have questions based on
what has been publicly released.
So, the information that has been reported
is that the U.S. is considering slowing delivery
of offensive systems.
That makes sense.
That's good.
That tracks.
It appears to be focusing on two specific items.
and you have a whole lot of people who are very excited because the US is
thinking of slowing delivery of JDAM systems. Yeah that's bad actually that's
not good. People associate JDAM with the bombs that are falling on Gaza and yeah
that's part of it. But what the US is talking about and has been delivered,
what's being reported in the discussion anyway, it's not actually the bombs. It's
a little guidance system. It's a package that makes it more accurate, that is
fitted to a bomb. If Israel doesn't get those, it's not like they're gonna stop
bombing. They're just going to be less accurate, less precise, which means they'll
probably end up using more of them. I promise you if the JDAM guidance systems
are not supplied, if they run out, if Israel runs out, there will be an
increase in Palestinian civilian loss, guaranteed. I have no doubts about that.
So, no, I don't think that's a good idea.
That seems like a horrible idea.
The other system they're discussing is slowing the delivery of 155 shells.
These are artillery shells.
Yeah, that makes perfect sense.
That is where the pressure can actually be applied and it not kill a bunch of innocent
people, which I would assume would be the goal here.
So these are exactly what you are thinking when you think artillery shell.
Comes out of a cannon, like a...and lands.
Israel, I don't know the extent to which they are using those to soften areas before they
go in.
But I would imagine it's not being...it isn't being ignored.
That is probably something they're using a lot.
Slowing delivery on that, it is very focused on stopping offensive measures, it is very
focused on something that they really need and they might actually curtail operations
if they don't have, and it doesn't hurt civilians, which again should be the primary
concern here. Will it work in the sense of altering Israel's overall strategy? I
don't know. I don't know. What I do know, the other thing to keep in mind, is that
this is less likely to happen now that it's been reported on. Understand this
isn't new. The fact that this was on the table, this is not new. This is not a new
development. The US position for quite some time has been that Israel needs to
get out of Gaza, needs to scale back. And remember the US deployed advisors trying
to get Israel to not go in to begin with. Slowing, curtailing these deliveries is
is not new. It being reported on is. And if it wasn't reported on, the US would be more
likely to do it, I think. There's a difference between being able to say, oh, well, we just
ran out of those 155s don't know where they're at and openly it being in the
news that you're doing it to disrupt their military operations. So we will
have to wait and see when it comes to something like this the devil is in the
details and some of the details that are coming out they're not they're not well
thought out, but again all of this reporting is very, it's light and it
shouldn't, this shouldn't have come to the public's attention to be honest. So
hopefully it will proceed with the 155s, slowing delivery of those. That would
have the greatest effect and would protect life which again should be the
goal. I would hope that it starts with that whether or not it would be
be effective as anybody's guess.
Anyway, it's just a thought.
you all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}