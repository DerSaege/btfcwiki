---
title: Let's talk about two 14th amendment developments and Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=U8Z782R9r6M) |
| Published | 2024/01/29 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Two developments involving the 14th Amendment and Trump with ballots in Colorado and Illinois.
- 25 historians signed a brief stating the 14th Amendment applies to Trump, and it does not need more process.
- Colorado case is heading to the Supreme Court, but it may not change minds there.
- In Illinois, a hearing officer recommended Trump should not be on the ballot.
- The judge in Illinois believes it needs to go before the courts for a decision.
- State Board of Elections in Illinois likely to follow the hearing officer's recommendation.
- Challenges seem to trend towards disqualifying Trump at this level of the court system.
- The final decision will rest with the Supreme Court, and whether the 14th Amendment is self-executing.
- Supreme Court is expected to hear the case within the next month.

### Quotes

- "25 historians signed a brief stating the 14th Amendment absolutely applies to Trump."
- "Trump should definitely not be on the ballot, engaged in insurrection."
- "The challenges tend to be trending towards Trump is disqualified, X, Y, and Z need to happen."
- "The deciding factor will be what the Supreme Court says."
- "Whether or not it is self-executing will be the larger question."

### Oneliner

Two developments involving the 14th Amendment and Trump's ballot eligibility in Colorado and Illinois trend towards disqualifying him, awaiting Supreme Court decision on whether it's self-executing.

### Audience

Legal analysts, activists

### On-the-ground actions from transcript

- Contact legal organizations for updates on the case (implied)
- Stay informed about the legal proceedings and decisions (implied)

### Whats missing in summary

Detailed legal analysis and implications of the cases discussed by Beau.

### Tags

#14thAmendment #Trump #SupremeCourt #Ballots #LegalAnalysis


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about two developments
involving the 14th Amendment and Trump
when it comes to ballots, whether or not he should be on them
and all of that stuff, because there have been developments
in two different states, Colorado and Illinois.
So Colorado is the one that is headed
before the Supreme Court, so we will do that one first.
Short version on that, 25 historians signed on to a brief, and that brief basically says
the 14th Amendment absolutely applies to Trump, it is self-executing, meaning that there doesn't
need to be more process involved, and it absolutely applies to the presidency, because the presidency
is any civil office, which is also covered.
So that's headed toward the Supreme Court and that will... I don't know how much that's
going to influence the court there because they're probably very well aware of everything
that's in that brief already.
I don't know that that's going to change any minds when it comes to the Supreme Court.
But I'm sure it's nice to have on file.
Okay, in Illinois, there's a challenge there.
And attorneys for both sides went before a hearing officer, which is a retired judge.
And the judge held the hearing and wrote a 27-page recommendation.
And it's, yeah, Trump should definitely not be on the ballot, engaged in insurrection
and all that stuff.
But it's not the state board of elections that should make that determination.
It's the courts.
It is worth noting that the hearing officer, the judge, is a Republican.
a Republican. But again, it's one of those situations where there is a differing view.
Really here, the judge is saying, that's probably not self-executing. Short version is it needs
to go before the judge. Odds are the State Board of Elections is going to see it that
way as well. They will address this and I think vote on it on Tuesday. But they'll probably
just follow the hearing officer's recommendation.
And then from there, we'll see how it plays out.
It'll probably go straight to the courts.
But overall, when you're looking at the challenges and how they're playing out at this level,
they tend to be trending towards Trump is disqualified, X, Y, and Z need to happen.
That's generally the view, once it hits this first level of the court system.
We'll have to see what the Supreme Court says.
That's going to be the deciding factor.
It will matter into how this all plays out.
I think a big question is whether or not it is self-executing.
I think that's going to be the larger question
that they're going to face, more so than anything else.
But we'll hear from the Supreme Court relatively quickly.
I'm gonna say probably within the next month,
it will be heard
and we'll start seeing more movement there.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}