---
title: Let's talk about US-Chinese relations calming....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=C7kS9ebnf4g) |
| Published | 2024/01/09 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The US-China relationship is seeing a cooling trend due to internal issues in China.
- Chinese generals linked to rocket program removed due to corruption, affecting missile readiness.
- New loyal commanders appointed, focusing on force building and auditing.
- China is pushing back timelines for aggressive actions like going after Taiwan.
- US agencies revising their estimates on China's readiness for military action.
- Analysts suggest China was not on an aggressive path as believed by the US.
- More news expected on Chinese military restructuring and potential delays in aggressive actions.

### Quotes

- "China is pushing back timelines for aggressive actions like going after Taiwan."
- "Corruption in Chinese military affecting missile readiness."
- "New loyal commanders appointed, focusing on force building and auditing."

### Oneliner

The US-China relationship cools as internal corruption in China's military delays aggressive actions, impacting missile readiness and strategic timelines.

### Audience

Foreign policy analysts

### On-the-ground actions from transcript

- Monitor developments in US-China relations and Chinese military restructuring (implied)
- Stay informed about potential shifts in global power dynamics (implied)

### Whats missing in summary

Details on the potential impacts of delayed aggressive actions by China on regional and global stability.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about the relationship
between the United States and China
and why the US believes the temperature
is cooling a little bit.
It's worth remembering that for quite some time,
US estimates have suggested that China
was going to become more assertive,
go on the offensive militarily, stuff like that.
Those estimates are being revised,
and it seems as though the collision course
that the U.S. and China were on
isn't going to happen too soon.
So the question obviously is why.
Corruption, corruption mostly.
The reporting suggests that at least nine
Chinese generals or former generals have been removed from circulation.
They're no longer in positions of authority.
There was a purge.
Most of these generals had some connection to the rocket corps, to their military's
rocket program.
This goes along with some reporting that suggests some of their missiles were filled with water
rather than fuel, and that a pretty decent chunk of their missile fields with the silos
in Western China had lids that didn't open right and would prevent the missiles from
firing if you were looking to achieve deterrence, that's going to be an issue.
So this is relatively confined when you're talking about nine key players, not really
a huge deal. However, the expectation is that because this corruption allegedly occurred
inside of their rocket forces with something as sensitive as a silo, it's probably pretty
widespread throughout the rest of the military. So that led to the Chinese leader to remove
a number of generals, and the new commanders are people who appear to be very loyal to
him politically, and they're seen as aggressive commanders.
Not in the sense of ready to go to war, but very much you're going to get the job done
kind of people.
This kind of indicates that they are looking to engage in a little bit of force building
and auditing because China saw the same thing the US did with Russia's invasion of Ukraine
and is very aware of how damaging corruption can be.
So with all of this in mind, the reporting suggests that US agencies are kind of pushing
back the date that they think China might be ready to maybe go after Taiwan or something
like that.
So overall, this is all good news, unless you're one of those nine generals.
So hopefully by then the global temperature as far as conflict will have decreased a lot
and it buys us a little bit of time before we end up with a great power war.
It is worth remembering that there were a bunch of analysts who weren't part of the
US government who were suggesting that the US government's read on it is incorrect,
China really wasn't going down the aggressive path that the US believed
they were, that they were going to use soft power and try to expand more
economically before they really started ramping up a military presence. So that's
where the situation sits right now. There's probably going to be more news
coming out about this. If the reporting that has come out is accurate, they're
probably right. It's probably widespread in other places, which means we're going
to hear more about generals leaving their post, and it's going to
create a delay, because not just do they have to kind of revamp and restructure,
they have to check everything to make sure that it works. Anyway, it's just a
thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}