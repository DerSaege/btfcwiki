---
title: Let's talk about averting a shutdown and the House GOP....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=WDLPVK4e_F8) |
| Published | 2024/01/20 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talking about the budget, continuing resolution, and its passage through the U.S. government.
- The Speaker of the House relied on Democratic votes to pass it, making the far-right faction of the Republican Party more irrelevant.
- Far-right Republicans wanted to obstruct the resolution, but the Speaker avoided a government shutdown.
- Passing bills without needing far-right votes is a strategy to render them powerless.
- Far-right faction now threatens not to support bills that won't succeed in the Senate, but it plays into the Speaker's hands.
- Speaker's goal is to maintain traditional conservative values within the party.
- Far-right faction is learning to adapt to the Speaker's political maneuvering.
- Speaker Johnson is currently winning against the far-right faction.
- The far-right faction may have to fall in line with traditional conservative values or retire.

### Quotes

- "Passing bills without needing far-right votes is a strategy to render them powerless."
- "Far-right faction is learning to adapt to the Speaker's political maneuvering."
- "The Speaker Johnson is currently winning this little fight as of right now."

### Oneliner

Beau explains the passing of the budget and continuing resolution through political maneuvering, rendering the far-right faction of the Republican Party irrelevant.

### Audience

Politically aware individuals

### On-the-ground actions from transcript

- Follow and support political figures who prioritize effective governance and bipartisan cooperation (implied)
- Stay informed about political strategies and maneuvers within party politics (implied)

### Whats missing in summary

Insight into the importance of understanding political strategies and tactics in party dynamics.

### Tags

#USpolitics #RepublicanParty #BipartisanCooperation #PartyDynamics #GovernmentShutdown


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about the budget,
the continuing resolution, it going to Biden,
and what that means for the U.S. House of Representatives
and the Republican party inside of it.
Because the Speaker of the House did in fact,
once again, rely on Democratic votes to get it through,
further rendering that the MAGA faction, the Twitter faction, the far-right faction of
the Republican Party, rendering them more and more irrelevant.
So if you are unaware of what's going on, the U.S. government was up against a deadline
to get a budget or a continuing resolution through.
They went with a continuing resolution.
It made it through the House and the Senate.
has been signed by Biden. The new government shutdown deadline is sometime in March. However,
the far-right faction within the Republican Party, they didn't want to let it go through.
They wanted to obstruct and be disruptive with it. The Republican Speaker of the House,
he didn't want a government shutdown because he has to try to maintain the
majority in the House and if there was a government shutdown there was no way
the Republicans were keeping the majority in 2024. So he relied on
Democratic votes to get it through. This of course upset the far-right faction
and they have started to realize that the Speaker is playing the long game and
and making them irrelevant.
This is something we talked about
when McCarthy was speaker.
If you want to remove the leverage
of the Twitter faction of the Republican Party,
just pass bills that you don't need their votes for.
And that's what he's been doing.
And they've kind of caught on.
So this is something that was said
from a portion of that far-right faction.
Once again, we passed a significant piece of legislation
that keeps in place with predominantly Democrat votes,
policies that were ran against and campaigned against.
And this is where it gets interesting,
because the far-right faction has their new threat, I guess.
This is what they plan on doing to counter the Speaker.
If you don't need our votes for the significant pieces of legislation that impact the country,
that fund the government, and you're going to pass those with Democratic votes on suspension,
then you shouldn't presume that you've got our votes for the meaningless messaging bills
that are dead on arrival in the Senate.
So it appears that the far-right faction is now saying they will not vote in favor of
far-right bills that have no chance of succeeding in the Senate.
They're not going to vote for those anymore.
Okay, the Speaker of the House, I assure you, doesn't want those bills to pass.
The speaker doesn't actually look good if bills go to the Senate and then just die.
That is not effective.
That doesn't make the speaker look good, saying you're not going to vote for those, vote for
those far-right bills that won't get anywhere in the Senate.
That's not a threat.
That's surrender.
That's what he wants.
So they have started to figure out what he's doing, but they still haven't figured out
how to counter it, because by promising not to pass a bunch of far-right stuff that won't
go anywhere, they're actually surrendering.
They're saying, we'll come into line and stick with traditional conservative stuff, which
is what he wants.
That's what the speaker wants.
This is one of those moments where the far-right faction is kind of learning how to play the
game because the political maneuvering that they've been using, that's not traditional
DC politics and it won't hold up for long.
So now they're starting to see that the we demand everything doesn't actually work.
the new speaker. If Johnson stays on this course, eventually he will bring the far-right
faction of the Republican Party, the Twitter faction, bring them into line or they'll retire.
He is definitely winning this little fight as of right now. Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}