---
title: Let's talk about surprising economic vibes....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=vdsKCqk_RAc) |
| Published | 2024/01/20 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Examines the economy and perceptions, revealing surprising insights and questioning polling methods.
- People perceive the economy poorly despite positive economic indicators like GDP growth, low unemployment, and rising consumer spending.
- Axios conducted a survey asking people about their individual economic situation, which showed a stark contrast to perceptions of the national economy.
- 63% of Americans believe their economic situation is good or very good, with 77% happy where they live and confident in finding another job if needed.
- Despite the positive individual outlook, the majority feels the national economy is doing poorly.
- 85% of respondents believe they can improve their financial situation in the current year.
- People's perceptions of their personal economic well-being differ significantly from their views on the national economy.
- The discrepancy between individual and national economic perceptions could lead to polling errors, especially during elections.
- Contrasting views on personal and national economies may impact electoral outcomes influenced by economic sentiments.
- Beau raises concerns about potential polling errors if people's personal economic situations are positive but they express negative views on the national economy.

### Quotes

- "When you ask about the national economy, there's an outlet called Axios and they did this weird thing."
- "National situation bad. Individual situation good for the majority of people."
- "People's individual situation makes up the national situation."
- "It's just a thought."
- "Y'all have a good day."

### Oneliner

Beau questions the disconnect between positive personal economic outlooks and negative national perceptions, hinting at potential polling errors come election time.

### Audience

Economic analysts, pollsters, voters

### On-the-ground actions from transcript

- Analyze your personal economic situation and compare it to your perception of the national economy (suggested).
- Stay informed about economic indicators to better understand the overall economic situation (suggested).

### Whats missing in summary

Insights into the impact of economic perceptions on electoral outcomes and potential strategies to address polling errors.

### Tags

#Economy #Perceptions #Polling #NationalEconomy #PersonalFinance


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about the economy
and perceptions and you're gonna get
some surprising information.
At least it was surprising to me.
And it also sheds some light on why I am
less than trusting of a lot of polling lately
because it all depends on the question you ask.
If you were to ask most Americans right now
how the economy is doing, a whole lot of people
going to tell you that's not doing well, which is weird because that flies in the
face of all of the economic information. The GDP expanding faster than any
developed nation, unemployment down, consumer spending up, inflation
approaching desired levels, even rents are falling. Now I have to admit that last
thing is nationwide. We haven't seen that where I live yet. But even with this
my guess is because of all of the coverage, people don't feel like the
economy is doing well. When you ask about the national economy, there's an outlet
called Axios and they did this weird thing. They had something, I guess they
call it a vibe, Axios vibes or something like that because they're way hipper
than I am. It's a survey, it's a poll. In this little vibe check, rather than
When asking people about the national economy, they ask them about their economic situation
as individuals.
Those answers change so much.
Sixty-three percent of Americans say that their economic situation is good or very good.
A majority.
77% are happy with where they're living, and this includes runners.
And more than half feel that if they lost their job tomorrow, they'd be okay because
their company needs them more than they need the company.
They can go somewhere else and find a job, and they feel very confident in that.
These are all really good signs.
But here's the wild part.
The survey, the vibe check, was conducted at the very end of last year in 2023.
Eighty-five percent feel that they can make their financial situation improve this year.
Eighty-five percent.
What this kind of tells us is that the majority of Americans feel that where they are, the
economy is doing good.
It's just everywhere else that it's bad.
Now this is interesting because this is something we've been talking about, how the economic
indicators say one thing and people's perception of it as something else and sometimes, yeah,
takes time to catch up and all of that stuff. This is interesting for that reason, but it's
also interesting because it shows how important the question being asked is when it comes
to polling. Because I would be willing to bet if you asked these same people, this same
data set, how they felt the national economy was doing, these would not be the answers.
But the individual economy is doing fine.
National situation bad.
Individual situation good for the majority of people.
It's important to remember that people's individual situation makes up the national
situation.
I found it interesting.
This is one of those things that may contribute to other polling errors later, because the
conventional wisdom is that the economic situation has a whole lot to do with electoral outcomes.
If polling continues to guide people towards saying negative, talking about the national
situation, but their personal situation is good, my guess is that we're going to
have more polling errors when it comes to election time. Anyway, it's just a
thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}