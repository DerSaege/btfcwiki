---
title: Let's talk about Trump's total presidential immunity....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=lgzpPqKATIk) |
| Published | 2024/01/20 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing Trump's statement on total presidential immunity.
- Trump claims that without total immunity, every president will be indicted, but this immunity doesn't exist.
- Only three presidents have been criminally investigated: Nixon, Clinton, and Trump himself.
- Trump's own history contradicts his claim about constant indictments without total immunity.
- The attempt to establish total presidential immunity goes against the American principle of government being responsive to the people.
- Creating presidential immunity akin to creating another king, contrary to the checks and balances system.
- Trump's desire for immunity may stem from a possible self-coup attempt to stay in power.
- The notion of a president being above the law is fundamentally un-American.

### Quotes

- "The idea that a president should be totally above the law is the most anti-American sentiment that could possibly exist."
- "Creating presidential immunity is akin to creating another king."
- "His claim, his reason for it having to exist, is disproven by history."

### Oneliner

Analyzing Trump's claim of total presidential immunity and its contradiction with history, revealing the anti-democratic sentiment behind it.

### Audience

Legal scholars, activists

### On-the-ground actions from transcript

- Challenge authoritarian actions in your community (implied).
- Advocate for accountability and checks and balances within government (implied).

### Whats missing in summary

The full transcript provides a detailed breakdown of Trump's unfounded claim of total presidential immunity and its implications for democracy.


## Transcript
Well, howdy there, internet people, let's bow again.
So today, we are going to talk about the idea
of total presidential immunity,
and we're gonna talk about Trump's statement about it,
and just kind of run through it, see if it makes any sense,
because he's made some pretty bold claims
about what would happen if he didn't have
this kind of immunity, okay.
So, running through his statement here,
I'll just read it, a president of the United States must have full immunity
without which it would be impossible for him her to properly function any mistake
even if well intended would be met with almost certain indictment by the
opposing party at term end even events that quote cross the line must fall
under total immunity or it will be years of trauma trying to determine good from
bad. I mean that's a bold claim. Basically he's saying that if presidential
immunity, this total presidential immunity, immunizing a president against
criminal activity, if it doesn't exist then every president is going to be
arrested. That's weird because it doesn't exist. The idea of total immunity,
criminal immunity for presidents, that doesn't exist. It's never been tested in
court. So he says that if it doesn't exist, every president will get indicted.
it, but legally speaking it doesn't exist and he's the only one that's been
indicted. So this idea that without it, it would just be constant indictments, he
made that up. It's just not true. There is nothing to suggest that's the case. In
fact, there's only been three presidents, I think, that have been criminally
investigated. Nixon, Clinton, and him. I mean, it seems weird, right? That this
claim that he's pushing is that without this, all presidents will get indicted.
But he's trying to establish and create this immunity, but he's the first
president to be indicted. His own history shows that his statements are false. The
reason he's having to fight to create this shows that his statement is false.
Maybe it's not a partisan thing. Maybe it really has to do with the whole attempt
to like, you know, kind of do like a self-coup thing and stay in power even
though got voted out. Maybe that's why the indictments happened. The idea that
a president should be totally above the law is the most anti-American sentiment
that could possibly exist. The whole idea is that the government should be
responsive to the people. The whole idea is for there to be checks and balances,
not to have some authoritarian goon up at the top who can do whatever he wants.
His statement, his claim, his reason for it having to exist, is disproven by history.
What he wants to create, that kind of immunity, it's creating another king.
Red hat to red coat, I guess.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}