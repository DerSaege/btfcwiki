---
title: Let's talk about Biden's Senate border deal....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ATjLGiJtSD8) |
| Published | 2024/01/27 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explaining the immigration deal and clarifying that it's a Senate deal, not Biden's.
- The deal is a mixed bag with components catering to different viewpoints.
- Components include more border patrol, asylum officers, and immigration judges.
- There's an expulsion power for a certain number of daily encounters, which Beau strongly opposes.
- Doubts if the deal will pass due to its mixed nature and potential court challenges.

### Quotes

- "The question is whether or not the good outweighs the bad."
- "I don't think that the right is going to have too much of an issue with that, with the exception that they're going to be like, wait, Biden's going to be able to pick those judges."
- "I have a real issue with that, a real issue with that."
- "If that applies to asylum seekers, you can't do it, period."
- "So I don't like it, but I wouldn't be right either because I don't believe the most objectionable part will make it through the courts."

### Oneliner

Beau breaks down the Senate immigration deal, a mixed bag facing uncertainty on passage due to controversial aspects and potential court challenges.

### Audience

Policy Advocates

### On-the-ground actions from transcript

- Contact your representatives to express your views on the immigration deal (suggested).
- Stay informed about the developments around the immigration deal and potential court challenges (suggested).

### Whats missing in summary

Detailed analysis of each component of the immigration deal and its potential impact.

### Tags

#Immigration #PolicyAnalysis #MixedBag #SenateDeal #AsylumSeekers


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about the immigration deal.
What's going on, Biden's deal,
because a bunch of questions came in,
and basically asking what's in it,
whether it's gonna get through what I think about it,
a whole bunch of things.
First thing I wanna point out is that this isn't Biden's deal.
This is a Senate deal that Biden was like,
yeah, I'll get behind it.
That's worth noting.
One of the big questions is answered by another one.
What's in it and will it pass?
Will it pass?
I don't know because of what's in it.
It's a mixed bag.
It is a mixed bag.
If you are somebody who is incredibly supportive of asylum seekers, you're going to find stuff
in here that you absolutely love and you are going to find things in here that would make
you say, no, absolutely not, I can't support this.
But then there's those parts you love.
If you are somebody who is very much wanting to build the wall or whatever, you're going
to find parts in this that you absolutely hate and you will find parts in it you love.
The question is whether or not the good outweighs the bad.
The main components.
This is what you have.
You have 1,300 more border patrol.
That's a wash.
That's going to be completely irrelevant.
You have 1,600 more asylum officers.
That's actually really good.
That's a good thing.
375 immigration judges.
a good thing. Both of those are good. Speeds the process along. I don't think that the
right is going to have too much of an issue with that, with the exception that they're
going to be like, wait, Biden's going to be able to pick those judges. So you have that.
then you get to the expulsion power. Basically, if there are X number of encounters per day,
and I have seen this quoted as 4,000 or 5,000, an expulsion power is created where people
who are coming across, they're not getting processed. They're getting sent back. I have
a real issue with that, a real issue with that. If that applies to asylum seekers,
you can't do it, period. I mean you could pass it all you want, you're not going to
be able to do it once it gets to the courts. They would view that as fouling
twice. Now the other thing that goes along with this expulsion power is
something that says that if that's in effect, those people seeking asylum can go to legal crossings.
That's interesting, but you'd need like signs to let them know.
There's a whole lot to it. It is a mixed bag. Because of that, I have no idea whether or not
it's going to get through. We're going to have to wait and see. Personally, the part that I think is
just totally completely horrendous and very objectionable, I don't think it'll make it
through a court challenge.
So I don't like it, but I wouldn't be I right either because I don't believe the most objectionable
part will make it through the courts.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}