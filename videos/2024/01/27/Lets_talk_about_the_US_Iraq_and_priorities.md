---
title: Let's talk about the US, Iraq, and priorities....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=G5p2wKTXTFs) |
| Published | 2024/01/27 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The U.S. might be shifting its view and leaving the Middle East.
- Talks are ongoing between the U.S. and Iraq to determine the future presence of U.S. troops there.
- The U.S. has around 2,600 troops in Iraq at the request of the Iraqi government.
- Political talking points may emerge from this, with the far right possibly criticizing Biden.
- Recent strikes on U.S. facilities are not necessarily the reason for the potential U.S. withdrawal.
- The Iraqi government was displeased with a U.S. response to one of the strikes, possibly leading to the request for U.S. departure.
- If the U.S. leaves, contractors employed by the Iraqi government may replace official troops.
- This potential withdrawal could be the start of a broader deprioritization of the Middle East by the U.S.
- The U.S. strategy might involve shifting focus to Africa rather than completely withdrawing from global affairs.
- Beau believes this shift is necessary and hopes it will lead to a transition in energy and focus.

### Quotes

- "We don't need to be there. We need to transition our energy."
- "They needed to start this for quite some time."
- "It's a possibility that it's getting underway."

### Oneliner

The U.S. may be leaving the Middle East, signaling a potential shift in foreign policy priorities towards Africa and away from "Masters of the Universe games."

### Audience

Foreign policy observers

### On-the-ground actions from transcript

- Monitor developments in U.S.-Iraq talks to stay informed (implied).
- Stay updated on potential U.S. troop withdrawals in other regions (implied).

### Whats missing in summary

Context on the broader implications of a U.S. withdrawal from the Middle East and the possible consequences for regional stability.

### Tags

#US #ForeignPolicy #MiddleEast #Iraq #Withdrawal #Shift #Africa #GlobalAffairs


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about the United States
and how things may be changing,
how the view may be shifting a little bit
and how the U.S. might be leaving somewhere for real.
Something that has been talked about for quite some time
may be getting underway in earnest.
So today we will talk about the foreign policy concept
of deprioritization when it comes to the Middle East.
We're gonna do this because right now
There are talks underway between the United States and Iraq,
and those talks are to determine the future shape
of the mission, I believe is how they're phrasing it, which
means whether or not the US needs to be there anymore.
The US has about 2,600 troops there, more or less,
and they are there at the request of the Iraqi government.
Therefore, the host nation can, in fact, ask them to leave.
Now, the political talking points that are going to emerge from this, the far right is
obviously going to try to paint this as if it is Biden running away from, I don't know,
the recent strikes on US facilities.
Please remember, just because something is new to you or new to the new cycle doesn't
mean it's actually a new development.
There have probably been more than 100, less than 200 of those strikes over the last six
months.
That's not it.
There is another contender for the reason this is happening now, and that is that in
response to one of those strikes, the US got, well, let's just call it overaggressive in
one of those responses and created some collateral.
The Iraqi government was super unhappy about that, understandably so.
They mentioned at the time that they might ask the US to leave.
So maybe that's it.
But realistically, it's time.
It's just time for this to happen.
And maybe the response, I don't even think it sped things up, it probably just removed
all doubt from the Iraqi side.
So what would happen?
The U.S. leaves, U.S. official troops leave, and maybe some contractors get put there instead,
500 or so, but they're employed by the Iraqi government, not by the U.S.
The Iraqi government may not do that though, because there are also Iranian advisors, wink
wink, in Iraq, and from the Iraqi perspective, they're doing a pretty good job, so they may
not need any help from the U.S. Okay, so why would the U.S. do this other than they're
being asked? It may be the start of a wider deprioritization of the entire Middle East.
That is a long-term U.S. strategy that they've been trying to get underway for quite some
time, and it means exactly what it sounds like. The U.S. would be leaving the Middle
East, being less involved there. Please understand this is not because the U.S. has decided it
It doesn't want to play Masters of the Universe empire building games.
It's that it wants to play them in Africa instead.
Nothing altruistic about it.
With everything going on, it may just seem like the right time to do it.
So it's a possibility that it's getting underway.
I personally have wanted them to start this for quite some time.
They needed to.
I don't really...
I want them to do it so they stop playing Masters of the Universe games there because
we don't need to be there.
We need to transition our energy.
We need to transition our energy.
So we will find out more about this as the information comes out.
My guess is that if the US does leave Iraq, you will see it start to leave in other places,
especially places that had a shared mission pretty quickly as well.
start to see a faster phase out if the U.S. leaves Iraq.
So these are definitely talks to watch.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}