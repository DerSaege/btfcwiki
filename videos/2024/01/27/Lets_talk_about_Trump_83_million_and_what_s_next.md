---
title: Let's talk about Trump, 83 million, and what's next....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Emdarml37xo) |
| Published | 2024/01/27 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump and E.G. Carroll's case comes to a close with a hefty sum of $83.3 million decided.
- During the closing arguments, Trump dramatically stormed out of the proceedings, creating chaos.
- The jury deliberated, and when asked what the "M" stood for in the amount, it was revealed to be million.
- The breakdown of the $83.3 million includes compensatory damages, reputational repair, emotional harm, and punitive damages.
- Trump criticized the verdict, calling it ridiculous and a witch hunt, hinting at a possible appeal.
- Despite the significant financial blow, this may not be the only legal trouble for Trump.
- Beau predicts that this outcome may trigger erratic behavior from the former president in the future.

### Quotes

- "It was a total Trump show. It was a mess."
- "Trump being Trump immediately after this said that it was absolutely ridiculous and it was a witch hunt."
- "A lot of things are starting to kind of close in around the former president."
- "This is going to be the start of incredibly erratic behavior from the former president."
- "Go have a good day."

### Oneliner

Trump and E.G. Carroll's case concludes with $83.3 million decided, sparking Trump's criticism and potential appeal, while more legal troubles loom.

### Audience

Legal observers

### On-the-ground actions from transcript

- Stay informed about the developments in the Trump and E.G. Carroll case (suggested)
- Monitor any potential appeal processes or legal actions taken by Trump (suggested)

### Whats missing in summary

Insights on the potential future legal challenges and how they may impact Trump's behavior.

### Tags

#Trump #LegalCase #EJCarroll #Compensation #PunitiveDamages


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Trump and E.G. Carroll.
And that portion of the entanglements coming to a close,
what was decided in very short order,
and where it goes from here.
Let me talk about all of that.
And we are going to talk about $83.3 million,
because that is that's what was determined. So in the current E. Jean
Carroll case, it went to the jury after the closing arguments. During the closing
arguments Trump literally like stormed out in the middle of the the proceedings.
It was a it was a total Trump show. It was a mess. But it went to the jury the
The jury started deliberating.
They came back and the judge asked when reading it, what does the M stand for?
At that point, everybody knew the number was going to be pretty big, especially when the
jury said, well, that stands for million.
Yeah, so 83.3 million.
18.3 of that is for compensatory damages.
11 million of it is for reputational repair, 7.3 million of it is for emotional harm, and
65 million of it is punitive.
Yeah, I mean that's a pretty big figure there.
The judge then instructed the jury, advised, I guess would be a better term, to never disclose
that they were on that jury.
So Trump being Trump immediately after this said that it was absolutely ridiculous and
it was a witch hunt.
I think it's safe to assume that Trump is going to try to appeal this in some way.
So it's probably not over yet, given the proceedings and how they went, I'm not sure there's a
whole lot of appealable issues for him, but I'm sure he's going to try.
It's worth noting that as high as the 83.3 million is, that may not be the biggest hit
against him this month.
A lot of things are starting to kind of close in around the former president and things
are going to continue to progress in this way this if I had to guess this is
going to be the start of incredibly erratic behavior from the former
president I feel like he's not going to take this well anyway it's just a thought
Go have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}