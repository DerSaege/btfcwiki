---
title: Let's talk about Trump, Haley, and fundraising....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=pgUefnizANk) |
| Published | 2024/01/27 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump expresses concern about Haley's ability to raise money and claims that those who contribute to her will be barred from the MAGA camp.
- Trump's attempt to dissuade donors from supporting Haley backfires, resulting in a significant influx of donations to her.
- The response on social media to Trump's actions was largely mocking, indicating a growing skepticism towards his tactics.
- Despite Trump's efforts to maintain control over donors, substantial amounts of money poured into Haley's campaign.
- Wealthier Republican donors appear to be distancing themselves from Trump, while his core base remains loyal.
- Trump's strategy to bully donors into giving him money does not achieve the intended outcome, leading to a loss of influence among wealthier supporters.

### Quotes

- "Anybody that makes a contribution to Birdbrain from this moment forth will be permanently barred from the MAGA camp."
- "The former president did not get the desired result which was to scare people and bully people."
- "People are starting to see through his antics a little bit more."
- "We don't want to have anything to do with your MAGA camp."
- "The wealthy ones, they will walk."

### Oneliner

Trump's attempt to dissuade donors backfires as wealthier supporters distance themselves, leading to a surge in contributions to Haley's campaign and mocking on social media.

### Audience

Political donors, activists, voters

### On-the-ground actions from transcript

- Support political candidates financially (exemplified)
- Engage in social media activism by sharing donation posts (implied)

### Whats missing in summary

Insights on the impact of shifting donor loyalty and the evolving dynamics within the Republican party.

### Tags

#Trump #Money #PoliticalFundraising #RepublicanParty #Donors


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Trump and money,
finances, but not the big development from today,
the $83.3 million development.
We're going to talk about money that he claims he doesn't want,
which maybe today's events will alter his perception of this.
But we're going to go over something
he said because apparently Trump is a little concerned
about Haley's ability to raise money.
It's making him a little bit nervous.
And he said this, when I ran for office in one,
I noticed that the losing candidates' donors
would immediately come to me and want to help out.
This is standard in politics, but no longer with me.
Anybody that makes a contribution to Birdbrain,
Birdbrain is the name for the person
that he chose to represent the United States at the UN.
Apparently, he doesn't think that the person he chose to do
that's smart.
Anybody that makes a contribution to bird brain
from this moment forth will be permanently
barred from the MAGA camp.
We don't want them and will not accept them
because we put America first and always will.
Okay.
So first, let's be real.
The Democratic party is going to outspend
the Republican party by a whole lot in 2024.
Trump can't turn away money, okay?
Let's also remember that Trump's never turned away money.
That's not a thing.
This is an attempt by him to basically say, look, if you don't start giving me that money
now, we're not going to do you any favors if we get into office.
That's the subtext here.
It didn't land.
It didn't go over well.
The immediate response to this on social media was basically just everybody mocking Trump
because it's very apparent that this is him incredibly nervous about the person he calls
a bird brain raising money and getting those big donors, the ones with deep pockets, the
that have reach and infrastructure. The other thing that happened according to
the reporting is that like after that money just started pouring into Nikki
Ailey. Yeah it it kind of backfired on the former president. He did not get the
desired result which was to scare people and bully people but it just didn't come
across. People are starting to see through his antics a little bit more and
that includes Republican donors because we're talking about hundreds of
thousands of dollars breaking a million pouring in to Haley like right after his
statement and a whole bunch of people coming going on to social media to post
their donation and basically be like yeah we're done with you old man we
don't want to have anything to do with your MAGA camp. I would imagine as Trump
proceeds to go down the road that he's he's on that he will lose a little bit
more sway among those that have deeper pockets. His base, those that don't have those pockets
and those that are easily manipulated, they will stick with him. But the wealthier ones,
going to walk.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}