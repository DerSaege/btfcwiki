---
title: Let's talk about Trump, Haley, Biden, and polling....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=9sAWM6b3L8M) |
| Published | 2024/01/15 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Critiques the current polling's accuracy in predicting election outcomes due to not accounting for unlikely voters.
- Notes Trump's lead in Iowa caucus polling and Nikki Haley's 20% of the vote.
- Mentions an interesting poll revealing that half of Nikki Haley's supporters in the caucus may choose Biden over Trump in the general election.
- Emphasizes that these voters are the most active and partisan in the Republican Party.
- Speculates on the implications of a significant portion of partisan Republicans not supporting Trump.
- Points out the challenge Trump faces in winning over independents, especially if they share similar reluctance towards him.
- Expresses skepticism about Trump appealing to moderates or independents before the election.
- Raises concerns about Trump potentially becoming more inflammatory, which could alienate more voters.
- Acknowledges Trump's strong chance in winning the primary but questions his prospects in the general election due to legal issues and partisan rejection.
- Concludes with uncertainty about how Trump will navigate these challenges in the upcoming election.

### Quotes

- "Roughly half of Nikki Haley's voters would vote for Biden over Trump in the general election."
- "If 10% of the most partisan people in Iowa won't vote for Trump, he's going to have a really hard time."
- "Trump stands a really good chance of winning the primary, but I don't know how he'll fare in a general."
- "It seems unlikely that he develops a softer tone. If anything, he is going to become more inflammatory."
- "I don't know how he's going to overcome that when it comes to the general."

### Oneliner

Beau explains how a significant portion of Nikki Haley's supporters might choose Biden over Trump in the general election, posing a challenge for Trump among partisan Republicans and independents.

### Audience

Political analysts, voters, strategists

### On-the-ground actions from transcript

- Contact local political groups to understand voter sentiment and engagement levels (suggested)
- Organize focus groups to gauge public opinion and potential election outcomes (exemplified)

### Whats missing in summary

Deeper analysis on the potential impact of partisan rejection on Trump's general election prospects.

### Tags

#Politics #Election2020 #Polling #Trump #NikkiHaley


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Trump and Haley
and polling and something that we can pull out of the polling
because we've talked a lot about how the overwhelming
majority of polling right now isn't really worth a lot
because it doesn't take into account unlikely voters.
It hasn't been, well, it hasn't been great
as far as predicting things this far out.
There's just a whole bunch of reason
that most polling right now isn't really useful.
But we have something that's interesting.
Okay, so when it comes to the caucus in Iowa,
most of the polling, it's pretty similar.
Trump has a big lead.
Nikki Haley has about 20% of the vote.
Okay, but there is, there's an interesting poll
There's an interesting poll that says that roughly half of Nikki Haley's
voters would vote for Biden over Trump in the general election. And this is of
likely caucus goers. So these are the most active people in the Republican
Party. These are the most partisan. 10% of them won't vote for Trump and that's
That's the core group.
That's the core of the Republican Party in Iowa, roughly 20%, roughly half.
So about 10% of them won't vote for him, would vote for Biden over him.
These are partisan people.
They're people who are participating in the caucus.
We have talked a lot about how in other races recently you can't win a primary without Trump.
You can't win a general with him.
That might apply to Trump because if 10% of the most partisan people in Iowa, the most
partisan Republicans are like, I'm not voting for Trump no matter what.
I'll vote for Biden over him." The odds are that among rank-and-file Republicans, those who aren't
caucus goers, the number's probably even higher. Don't have anything to back that up, but even at
10%, even if this number holds nationwide, he's going to have a really hard time because he can't
win with 100% of the Republican vote, he has to capture the independence as well, and if the
independents are even half as reluctant, he's going to have real issues. That polling,
it's not far out. That polling is reflective of now, and Trump is
I do not believe that he is going to do things to appeal to moderates or
independents between now and the election. Because of everything that's
going on, because of the likely outcomes when it comes to his presidential
immunity and the New York case and then the other New York case and all of that,
it seems unlikely that he develops a softer tone. If anything, he is going to
become more inflammatory with his rhetoric and push more moderates away.
Trump stands a really good chance of winning the primary, I assuming that his legal issues
don't catch up to him and that's there's a high probability of that but I I don't
know how he'll fare in a general I know we see what the polling says but that
polling it's not it doesn't have a good track record this far out and with the
most partisan of Republicans rejecting Trump, I don't know how he's going to
overcome that when it comes to the general.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}