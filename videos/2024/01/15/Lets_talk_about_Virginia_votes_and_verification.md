---
title: Let's talk about Virginia, votes, and verification....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=xOR6pYtNKdM) |
| Published | 2024/01/15 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Virginia news on election irregularity promised to report on by Beau due to Trump's claims about the 2020 election.
- Major irregularity in Prince William County, Virginia, with thousands of votes affected.
- Biden shorted 1648 votes, while Trump was over-reported 2327 votes, giving him a net of 3975 votes.
- Despite the irregularity benefiting Trump, he still lost the election.
- The error was less than 1% and did not impact the overall outcome of the races.
- No charges are expected to be pursued due to a genuine error related to split precincts.
- The irregularity has been addressed, hopefully putting an end to the claims about the 2020 election.
- No impact on the race outcome as Biden still won in the county.
- Beau promised to report on irregularities over 1000 votes, should any arise.
- Closing message of assurance that any significant updates will be shared.

### Quotes

- "Major irregularity in Prince William County, Virginia, with thousands of votes affected."
- "Despite the irregularity benefiting Trump, he still lost the election."
- "No impact on the race outcome as Biden still won in the county."

### Oneliner

Virginia election irregularity revealed, benefiting Trump but not altering the outcome; Beau fulfills his promise to report on significant discrepancies over 1000 votes.

### Audience

Election observers

### On-the-ground actions from transcript

- Stay informed and vigilant about election irregularities (implied)

### Whats missing in summary

The nuances of the explanation for the irregularity and the potential long-term impact on election monitoring.

### Tags

#Virginia #ElectionIrregularity #Trump #Biden #Report #Promise


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about some news out of Virginia and news that I
feel ethically obligated to, to report on because I promised I would if it ever
happened, um, we are all familiar with Trump's claims about the 2020
election. We all know what he said and repeated and repeated and repeated. When
that was going on, I said that if I ever heard about any major inconsistencies or
irregularities, I would report on it. So, in Prince William County in Virginia there
There was a major irregularity.
We're talking about thousands of votes.
So in that regard, Trump was right, as much as I hate to say that.
So what did they uncover?
Biden was shorted 1,648 votes.
was over-reported 2,327 votes giving Trump a net of 3,975 votes. Yeah, there
was an irregularity all right, but it was in Trump's favor and he still
lost. I would like to note that going through the other races, I don't see any
where this affected the outcome. I think that's important to note and even though
we are talking about I mean almost 4,000 this is this is a less than 1% error.
It looks like they won't be pursuing charges. My guess is that the
explanation that I heard is is accurate which basically it has to do with split
precincts and it was a genuine error so that's that's the news that's that's
what's going on there's there's your irregularity from 2020 finally finally
putting that to bed in 2024, yeah, again, this is actually the biggest that I'm aware of.
Didn't impact the race. Biden won there anyway. And it was actually in Trump's favor. So
So hopefully these claims will eventually start to go to the wayside and we won't hear
any more about them.
But rest assured, because I sillily promised that I would report on it if it happened,
if anything else comes up with more than a thousand votes, I will let you know.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}