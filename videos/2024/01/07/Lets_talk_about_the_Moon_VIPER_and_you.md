---
title: Let's talk about the Moon, VIPER, and you....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=1t-O0B6n56k) |
| Published | 2024/01/07 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- NASA is sending a rover called Viper to the moon in November to search for water ice.
- The purpose of this mission is to pave the way for manned bases on the moon.
- Viper will have to survive for around 100 days in challenging conditions.
- To involve the public, NASA is offering to attach people's names to the rover if submitted by March 15th.
- Submit your name at www.nasa.gov/send-your-name-with-viper by 11:59 p.m. Eastern Time on March 15th.
- This initiative aims to spark interest in space travel, especially among kids.
- Getting your name attached to the rover is a unique way to be part of a space mission.
- NASA provides a digital boarding pass and a QR code for more information when you submit your name.
- This is a great way to kickstart an interest in space and space exploration.
- Beau found this initiative interesting and thought others might enjoy it too.

### Quotes

- "Your name will go into space."
- "This is a great way to kickstart an interest in space and space travel."

### Oneliner

Be part of NASA's Viper mission to the moon by submitting your name for a unique space experience and sparking interest in space travel, especially for kids.

### Audience

Space enthusiasts

### On-the-ground actions from transcript

- Submit your name at www.nasa.gov/send-your-name-with-viper by March 15th (suggested).

### Whats missing in summary

The detailed process of how submitting your name can spark interest in space and encourage involvement in space exploration.

### Tags

#NASA #Viper #MoonMission #SpaceExploration #NASAInitiative


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we're going to talk about, well, the moon,
and NASA, and Viper, and how you can be involved,
in a bizarre way, with one of NASA's projects.
Okay, so, towards the end of the year,
I wanna say November is when it's supposed to be
on the moon, but NASA is sending something up called Viper.
VIPER stands for Volatiles Investigative Polar Exploration Robot, or rover.
And its purpose is to get up there, go around the lunar south pole, and look for water ice.
This is to pave the way for the Artemis missions, which will eventually lead to
manned bases on the moon. So they have to find the water.
And it's going to investigate in an area that is less than ideal.
The rover is going to try to survive for I want to say around 100 days in some pretty
inhospitable climates and lighting situations.
So that's the overall mission and what's going on.
So how can you be involved in it?
if you get them your name by 1159 p.m. Eastern Time on the Ides of March, March
15th, they will take that information and attach it to the rover so your name
will go into space. If you want to do it it's www.nasa.gov
slash send-your-name-with-viper. Yeah, they could have. I'll put the link down below.
Couldn't have made that easy. And the idea behind doing this is obviously to get
people interested in space travel. That's why NASA's doing it. This is something
you might want to involve your kids with. If you're trying to find something to
create a spark, to give them another interest, to get them to look towards the
stars or whatever, this is something that's unique. Your name will go into
space. And as far as NASA's part and their PR part what they do is if you go
this link and put in the information. It's really just asking your name. They
give you a little digital boarding pass and there's a QR code that you can go
and get more information, sign up for more stuff if you want to, but it's
definitely a way to kind of kickstart an interest in space and space travel. So
it's something interesting that I ran across I thought y'all might enjoy. Anyway
It's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}