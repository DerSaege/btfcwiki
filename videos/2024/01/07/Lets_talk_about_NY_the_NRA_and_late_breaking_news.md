---
title: Let's talk about NY, the NRA, and late breaking news....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=w9Y1oFOwoYg) |
| Published | 2024/01/07 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Letitia James is moving forward with another case involving the NRA, accusing top officials of misusing funds.
- The trial, set to begin on Monday, will last six weeks with around 120 witnesses.
- Joshua Powell, one of the accused, admitted wrongdoing and agreed to pay $100,000 before the trial started.
- Powell criticized the NRA as part of the "Grifter Culture of Conservative, Inc."
- The jury will hear accusations of misusing nonprofit status and funds, with potential uncomfortable evidence for donors.
- Expect a lot of paperwork, evidence, and testimonies similar to the Trump case.
- The trial will determine the amount to be paid back to the NRA if the accused are found guilty.

### Quotes

- "The accusation revolves around using the NRA as a personal piggy bank, funding lavish lifestyles."
- "Powell admitted wrongdoing and agreed to pay before the trial started."
- "This trial will involve a lot of uncomfortable evidence, especially for donors."

### Oneliner

Letitia James moves forward with a case against NRA officials accused of misusing funds, with Joshua Powell admitting wrongdoing before trial, potentially uncomfortable for donors.

### Audience

Legal observers, concerned donors

### On-the-ground actions from transcript

- Follow the trial proceedings closely to understand the outcome and implications (implied)
- Support actions taken against misuse of funds in organizations (implied)

### Whats missing in summary

Insights into the potential impacts on NRA's future operations and reputational damage.

### Tags

#NRA #LegalCase #MisuseOfFunds #JoshuaPowell #Trial


## Transcript
Well, howdy there internet people, it's Bo again.
So today we are going to talk about a case
that is moving forward,
an agreement that was made right on the eve of it starting.
We'll talk a little bit about James
and what's going on up in New York.
So if you have missed what's going on
and what's about to start,
Letitia James not satisfied to deal with that minor case
involving former President Trump, another one is starting, involving the top people
of the NRA, or formerly of the NRA.
And it basically revolves around the accusation that they used the NRA as their personal piggy
bank, used it to fund a lavish lifestyle, to include private jets, vacations, no-show
jobs for their friends, all kinds of stuff, to the tune of somewhere north of
60 million dollars. So the trial, I believe it's supposed to start on Monday,
it's gonna last six weeks, have like 120 witnesses. Joshua Powell, one of the
people accused, reached an agreement, admitted wrongdoing, agreed to pay
hundred thousand dollars right before it started. So we're going to see this play
out. Powell is somebody who left the NRA a while back and did not have kind
things to say about it to include categorizing it as part of quote the
Grifter Culture of Conservative, Inc. Now, the jury will hear all of this
information. They will hear the accusations, there will be a defense,
normal thing, take about a month and a half, and then if found guilty, the jury
will recommend the amount that will have to be paid back to the NRA. You know, the
accusations center around misusing nonprofit status and funds. I feel based
on what we have seen in the Trump case, this is going to be a whole lot of
paperwork, a whole lot of evidence, and a lot of testimony that is going to make
a lot of people uncomfortable, particularly if you are somebody who
donated to them. So that begins, you know, it should begin this week and there was
already, before it even starts, that surprise development right before it
occurs and we'll see how it plays out. Anyway, it's just a thought. Y'all have a
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}