---
title: Roads Not Taken EP20
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=3DlxfAqGst0) |
| Published | 2024/01/07 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Providing a weekly overview of underreported news events on January 7, 2024.
- Iraq may ask the US to withdraw troops due to attacks by Iranian-backed militias.
- China's spy balloons over Taiwan causing outrage.
- Concerns about Trump's dog-whistling and Supreme Court case fairness.
- Giuliani's attempts in the Georgia case facing skepticism from the judge.
- Lawsuit over the death of Ms. Babbitt at the Capitol on January 6th.
- Three fugitives arrested for allegations related to the Capitol incident.
- Officer Harry Dunn running for Congress in Maryland.
- Trump's insensitive response to a school incident.
- Secretary of Defense hospitalized with complications from surgery.
- Steamboat Willie entering the public domain with caution about Disney's rights.
- Issues arising from celebrities listed for a fundraiser without their consent.
- Discovery of the East Coast of the US sinking due to satellite observations.
- Russia offering citizenship to foreign fighters.
- Misunderstandings and clarifications on various topics raised in questions.

### Quotes

- "If a similar situation arose under Trump, it [Palestinians' situation] ould be worse."
- "There is no shortage of information that will appeal to your side, whatever your side is."
- "Destroy their ability to engage in war and then get out."

### Oneliner

Beau provides insights on underreported news, foreign policy dynamics, legal cases, and societal misunderstandings, urging viewers to seek clarity amidst complex issues and biases.

### Audience

Viewers

### On-the-ground actions from transcript

- Contact representatives to address concerns about foreign policy decisions regarding Iraq and China (implied).
- Stay informed about legal cases and developments in the Capitol incident lawsuits (implied).
- Advocacy for fair treatment in legal proceedings and understanding of foreign policy dynamics (implied).
- Monitor environmental news and impacts on the East Coast of the US (implied).

### Whats missing in summary

Insights on the importance of understanding foreign policy dynamics, legal nuances, and biases in interpreting news for a well-rounded perspective.

### Tags

#UnderreportedNews #ForeignPolicy #LegalCases #Biases #Community


## Transcript
Well, howdy there, internet people.
Let's Bo again, and welcome to the Roads with Bo.
Today is January 7th, 2024,
and this is episode 20 of The Road's Not Taken,
which is a weekly series
where we go through the previous week's events
and talk about news that was unreported, underreported,
didn't get the coverage I think it should,
or I just find it interesting.
After that, we go through some questions from y'all,
And this week is a little bit different
because a lot of this is news that broke later in the week
and there may be follow-up videos on it
over on the other channel.
OK, so starting off with foreign policy,
Iraq is hinting that it may ask the US to leave,
may ask for a withdrawal of US troops
because the US installations there
getting hit by Iranian-backed militias and the U.S. responded. Iraq has been pretty clear that
there they need to be the people to respond and in a statement they said we have repeatedly
emphasized that in the event of a violation or transgression by any Iraqi party or if Iraqi
laws violated the Iraqi government is the only party that has the right to follow up on the
merits of these violations. This, we don't know if it's actually going to move to a request for the
U.S. to withdraw, but they're mad. They are mad. China's spy balloons are back in the news, this
time over Taiwan, and it is prompting outrage on the island. The Biden administration seems to
to believe that Israel plans on running low-intensity operations through at least the rest of 2024.
Moving on to U.S. news, Trump might once again be engaging in a little bit of dog-whistling
to encourage his base to engage in some bad activity.
When talking about the Supreme Court case, I just hope we get fair treatment, because
Because if we don't, our country's in big, big trouble.
Does everybody understand what I'm saying?
No, Trump, I don't.
Why don't you spell it out for us?
What exactly are you saying?
In the Georgia case involving Trump, the judge seems unimpressed with Giuliani's attempts
to maybe delay because that is how it kind of seems. Giuliani was talking about how the
discovery process was going and basically trying to get more time. The judge said,
while defendant claims to have filed many, and many is in quotes, motions concerning discovery,
none appear on the docket. Not something that a judge likes. There is a lawsuit
moving forward over the death of Ms. Babbitt at the Capitol on January 6th.
It's worth remembering that these kinds of lawsuits hinge on what policy
allows. I feel like the supporters of this lawsuit are going to be in for a
disappointment given the situation at hand and the fact that the VP was there
the policies are they allow for a lot of latitude. Okay three fugitives were
arrested on January 6th, this year, for allegations stemming from the January 6th thing at the
Capitol.
Harry Dunn, one of the cops who protected congressional staff on January 6th, has announced
he's going to be running for Congress in Maryland.
He's running in a really, really blue district where the incumbent is retiring.
Trump has basically stopped even sending thoughts and prayers when discussing an incident at
a school.
He said, it's just horrible, so surprising to see it here, but we have to get over it.
We have to move forward.
The Secretary of Defense was hospitalized after complications from surgery.
The fact that he was undergoing surgery wasn't released and put out the way it normally was.
There are a lot of people who are trying to kind of manufacture outrage or a scandal over
this.
There will definitely be a follow-up video on this one.
Short version, unless you are somebody who is involved with the communications between
the Pentagon and the White House, or you are a reporter who covers the Pentagon, you don't
have any reason to be mad. In cultural news, Steamboat Willie has entered the
public domain. I feel based on some of the things I've already seen people
doing, I feel like it's worth reminding everybody that just because one version
of Mickey has entered the public domain, that does not mean that all versions
have and nobody wants to mess with Disney's lawyers. So be very aware of what you're allowed
to do and what you're not. Kennedy was engaging in a fundraiser and in the advertisement for
it said a number of celebrities were going to be there. Some people took it as like an
implied endorsement. I really don't see that in his statement, in the team's statement about it,
but it was taken that way by some. Either way, the celebrities in question who were listed,
many of them came out to, well, Dionne Warwick said, I don't know anything about this event,
I did not agree to it, and I certainly won't be there. This is absolutely ridiculous. If you're
to lie on my name. At least lie about something cool. Revealed. Dion collaborates with Rihanna
on new album. Martin Sheen said, I wholeheartedly support President Joe Biden and the Democratic
ticket in 2024. My early read on this is that his team talked to the celebrities, like agents,
and invited them. Maybe the agents said yes, but didn't pass it on to their
client, that kind of thing. I don't think that this was actually deceitful, but I do think that
it's going to cause issues for him. In environmental news, satellite observations have led to the
discovery that the East Coast of the United States is sinking. The rate varies in area,
but it varies from about two millimeters to five millimeters per year. Five
millimeters per year is about an inch every five years with things sinking to
include infrastructure designed to stop sea levels from rising. That's going to
complicate things in oddities. Russia is now offering citizenship to foreign
fighters. Let's see. Aliens were claimed to have been seen at a mall in Miami. I
will tell you it was people walking, their shadows mixed with lights from a
a collection of cop cars who were there because it was reported to be an active situation
but it was actually people throwing fireworks at each other.
Caused a whole lot of panic.
A man stripped down in a Bass Pro Shop
and did a cannonball into the aquarium.
Roll tide.
OK, so moving on to the questions here,
we have, of course, we're starting off
with a note from the team.
Say the email address, questionforboe at gmail.com.
And it's question for F-O-R, Bo, B-E-A-U.
Then tell everybody you don't use Telegram,
the scammer is back.
OK, so that's occurring again.
Yeah, I don't use Telegram.
I will never ask you to go to another platform.
And we don't do giveaways in the comments section.
Don't give your personal information.
I will never ask you for your personal information.
just do it that way. Okay, on to the questions. With all the talk about
plagiarism concerning Harvard and then the wife of the person who attacked
Harvard, I was wondering what you think about the people who rip off your
videos. I don't view it as them ripping them off. I view it as them repurposing
them and trying to get them to a different audience. I don't, I don't get
mad about it. I want the information to get out. I know that there was somebody
on TikTok who basically kind of just redid an entire video and I had a whole
bunch of people send it to me honestly it yeah it does look like they used you
know my videos as source material but they repackaged that to get to an
audience that would never watch me so I don't I don't have an issue with it you
if it is
if it's with good intent
trying to help
it doesn't upset me
why do you
the russians keep trying to get through that
in the forcing car key
it's the seventh time and they
keeping destroyed
it's kind of seems like staged ukrainian propaganda
Oh, because they just keep getting demolished.
No, think back to early on, one of the first videos about it.
There are only so many ways to get from point A to point B.
And that's especially true if you're
talking about moving armor.
For those that don't know, there's
a forest near Kharkiv, and the Russians
have been trying to get through it.
there aren't a lot of paths to move something
the size of a tank through.
So of course, the Ukrainians are on those paths.
And the Russians are not having a lot of success
moving through those areas.
And there's a lot of videos and footage coming out about it.
OK, we have another note.
Make sure you read the next two questions together
before answering.
Your coverage of Gaza reveals you to be an Israeli stooge.
Answer for the audience if you're a Jew or not.
And then a word that starts with F
and then another word that rhymes with bike.
OK, the next one, why do you keep saying Israel is making mistakes, you aren't here, we are
surrounded, your support of these inhuman pieces of, is deplorable, Muslim loving, and
then it goes on from there.
I mean, I get why these two questions are paired that way.
Look, when it comes to this topic from, like I said from the beginning, I'm covering the
foreign policy aspects of it, the military strategy aspects of it.
That's what I'm covering.
There is a ton of coverage out there that will appeal to your emotion.
There is a ton of coverage out there that will confirm whatever it is that you want
to believe. That's not what I'm doing here. That's not what I'm going to do. I think
it is incredibly important for people to understand the dynamics. The rhetoric that has been used
for the last 50 years is what perpetuates the cycle going. I think it's incredibly
important for people to understand the actual dynamics of the conflict.
There is no shortage of information that will appeal to your side, whatever your side is.
So, okay, you and then there's two other YouTubers named, have all referenced the Powell Doctrine,
But when I look it up, it doesn't seem to relate.
Am I misunderstanding what you are all saying?
No.
There are two pal doctrines.
The pal doctrine that you are going to find when you look this up is going to be a series
of questions that a nation should ask before it engages in military action.
That is the journalist-created Powell Doctrine.
The Powell Doctrine in slang is a good definition.
A fast operation designed to degrade the opposition's warfighting capability and then a hasty withdrawal.
Desert Storm is the pal doctrine.
Show up, destroy the war fighting capability and leave.
No occupation, no nation building, nothing like that.
Destroy their ability to engage in war and then get out.
How do you feel about Ukraine's draft?
I do not support drafts, that's an across-the-board thing.
Even for causes I support or a country that's in a conflict that I'm sympathetic to, I do
not support drafts, period.
There's no but or however in that.
I've seen a few people on the far left call Biden names
and say flat out that Americans deserve a second Trump term
so they sympathize with Palestinians.
Wouldn't Palestinians be worse off
under a second Trump term?
Well, I mean, it would depend on what happens in that term.
They would be worse off if he was in power right now.
Yeah, so this is going to mostly be
younger people on the far left, and I think it is incredibly important for us to remember
they haven't seen anything like this before. They're not desensitized to it.
It's different.
Seeing something like this in real time and knowing that it's happening right now is
different than seeing it in a history book.
If you're my age, probably weren't aware of Grenada, too young.
But you saw the Famines and were aware of that, saw the footage, Panama, Bosnia, Rwanda.
This is all before you get to Iraq 1.
We were very desensitized to large-scale military operations and the footage that accompanies
it.
They haven't seen it before and they're mad.
So I don't think some of the things they're calling him are necessarily fair.
But maybe we should have been more angry in the past.
I don't believe that Americans would learn their lesson if Trump got a second term.
But at the same time, I can't be upset about the way they're reacting.
They're reacting the way they are because they're angry, and again, it is new.
There is a big difference between knowing it's happening right now and something you
saw on the History Channel or you read in a book.
I think a lot of them believe that if they just get out there and march long enough that
it'll stop or something along those lines.
But direct answer to the question, wouldn't Palestinians be worse off on our second Trump
term?
There is nothing Trump would do to try to restrain or even encourage restraint.
If a similar situation arose under Trump, it would be worse.
Do you feel you have a good sense for how people will react or understand your videos,
especially the produced ones on the roads.
I wonder if simply publishing without having them reviewed
or proofread might end up missing things.
I've been accused of many things.
Being inarticulate isn't one of them.
Now, I like to think that I make my point pretty clear.
If I don't, then I mean, I don't.
How people react to the videos or the information that's
put out, there's not a lot I can do about that.
And as we saw earlier, you can have
people respond in polar opposite ways to the same information
assume that you're trying to do something else. I haven't given this a
whole lot of thought. I haven't... I don't feel like I've been misunderstood a lot.
There have been times when it's happened, but it's relatively... relatively rare.
Let's see those Katie Porter style whiteboards documenting Trump's trial.
please and thank you. No, can't do that. I write a lot of my suspicions down
on those. For those that don't know, I keep track of different events on
whiteboards and I mentioned recently that I have one, a big one, that I
used to keep track of Trump's legal entanglements. There are things written
on them that I can't prove, so I can't show them.
Like it's, who I believe may have already taken a deal, stuff like that.
And I, yeah, I don't want to show any of that until it's confirmed, so, okay.
So that looks like it.
Those are all the questions.
So a little bit more information, a little more context, and having the right information
will make all the difference.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}