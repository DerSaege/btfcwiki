---
title: Let's talk about SECDEF being out of commission....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=7QxzHQ5Dxvk) |
| Published | 2024/01/07 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Secretary of Defense was unable to perform duties after a medical procedure, leading to criticism and questions about the importance of the situation.
- There were concerns about botched communication between the Pentagon and the White House, causing frustration among Pentagon press.
- The Secretary of Defense position is within the Department of Defense, capable of withstanding war with plans in place for continuity.
- General Austin, the Secretary of Defense, is a civilian political appointee, and the Chairman of the Joint Chiefs is the highest ranking military member.
- Beau criticizes the Republican Party for manufacturing scandal around the situation, citing intentional disruptions in military promotions by the same party.
- Beau views the situation as mainly a communication issue rather than a major scandal, with transparency concerns from the press being valid.
- Beau dismisses fear-mongering and pearl-clutching around the situation, advising critics to focus on more substantial issues rather than exaggerating this one.

### Quotes

- "The Secretary of Defense is a civilian, say it again."
- "The fear-mongering and the hand-wringing and the pearl-clutching, it's all being done by people who intentionally disrupted it."
- "Move on to the new outrage of the day."

### Oneliner

Beau breaks down the criticism surrounding the Secretary of Defense's absence, focusing on communication issues and political agendas rather than a significant scandal.

### Audience

Reporters, Pentagon Press

### On-the-ground actions from transcript

- Contact Pentagon press or reporters to understand their perspective and concerns (suggested)
- Advocate for transparency in communication between government agencies (implied)

### Whats missing in summary

Insight into Beau's overall perspective on transparency in government communication and the politicization of minor issues.

### Tags

#SecretaryOfDefense #CommunicationIssues #PoliticalAgendas #Transparency #PentagonPress


## Transcript
Well, howdy there, internet people. Let's bow again. So today we are going to talk about the
Secretary of Defense and the lack of knowledge in a couple of different ways about what happened.
Um, if you missed the news, the Secretary of Defense was basically not performing his duties.
Went, had a procedure done, a medical procedure.
there was a complication, there was an issue, and he was out of the net.
This has turned into a lot of hand-wringing and pearl-clutching and there have been a couple of
questions about whether or not this is really important. There are two legitimate concerns.
One is what looks to be botched communication between the Pentagon and the White House.
That's a concern but not a huge one and we'll get to why. And then the press that
normally cover the Pentagon, they're irked and they kind of have reason to
be. To my knowledge there's no actual requirement for a disclosure like this
but it's kind of always been done so to be caught off guard by that, that's a
legitimate complaint. That's it. So unless you're part of Pentagon Press or you're
somebody responsible for communication between the White House and the Pentagon
all of the other criticisms I have seen are just steeped in just not
understanding the information. Okay so let's go to the communication thing
because that's where a lot of it is stemming from. The idea that our quote
top military officer was out of commission. What would have happened?
Okay this may come as a surprise to people but the Secretary of Defense is
a position within the Department of Defense, an organization that is set up
to withstand war, there are plans. Hicks would have shown up and been fully capable of exercising
the office of the secretary if need be, even if she had to come back from leave to do so.
I say that because she came back from leave. Now, I have an issue with people on the right
turning this into a thing. If this was a real concern and that chain of command being in place
and everything being able to transfer real quickly, I would imagine that you would have
confirmed Baker by now, right? Because that's the next link in the chain and what that's been since
Next July, I have a real hard time taking that criticism seriously.
The other part that I have an issue with is the idea that our top military officer was
out of commission.
The Secretary of Defense is a civilian.
The Secretary of Defense is a civilian, say it again.
General Austin is called General Austin out of courtesy for his former position.
He is a civilian, he is a political appointee.
The highest ranking military member is the Chairman of the Joint Chiefs.
If you don't know that, I have a hard time taking the criticism seriously, especially
when it's coming from a party that I just finished wrapping up months of coverage about
them delaying the promotions of actual military members.
It seems to me that most of the people complaining about this are trying to manufacture some
major scandal when honestly it looks like botched communication and everything was in
place.
So I just, I don't see it.
When it comes to the press, when it comes to the reporters, I get it.
And their argument is, hey, at a time of elevated international tensions, we need transparency.
The public has a right to know.
And I get that.
The counter-argument to that would be, hey, at a time of elevated international tensions,
maybe we don't want everybody knowing that the Secretary of Defense is undergoing an
operation, especially since it's become very clear that people don't actually know how
that chain works.
But to be honest, I don't think that's what happened.
It really looks like bad communication to me, mainly because everything else appears
to have been in place.
It looks like they did not inform the people that they should have.
But the criticisms coming from the Republican Party, I cannot take them seriously.
there was an intentional effort for months to disrupt readiness and the promotions of people
very high up in the chain that were actually in the military. I feel like their criticism of
somebody not being in the office for a week and having somebody kind of take over their role and
and be prepared to take over their role, which is what was going on in the military for months,
I feel like that's not, that's not real.
They don't actually have a concern about that.
And then the concern that most people message about will be, well, what would happen if
the White House needed SecDef and, you know, they didn't know that he was out of the net?
They would have figured it out real quick when Hicks showed up.
The military is a very resilient organization.
The Department of Defense is as well.
I don't think this is as big a deal as people are making it out to be.
The only two people I see, two groups of people that I see that have legitimate concerns are
the reporters who are very accustomed to, it is a very long-standing practice.
You have to understand.
You're talking about a cabinet-level position that was, in essence, incapacitated.
That is something that is normally told to the press. That's normally disclosed. And it wasn't.
I can see them being upset. That part I understand.
The... other than that, it's a communication issue. Like, I don't...
The fear-mongering and the hand-wringing and the pearl-clutching, it's all being
done by people who intentionally disrupted it, disrupted readiness and the
chain far worse, for months, and did it on purpose. I cannot take it seriously. My
advice to them, move on to the new outrage of the day. Go back to talking
Taylor Swift or whatever because you look silly. Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}