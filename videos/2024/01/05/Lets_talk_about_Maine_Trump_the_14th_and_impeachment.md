---
title: Let's talk about Maine, Trump, the 14th, and impeachment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=QovdfVTYCoc) |
| Published | 2024/01/05 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Maine's Secretary of State declared Trump ineligible due to the 14th Amendment, sparking controversy.
- A Republican introduced legislation to impeach the Secretary of State over this decision.
- The Secretary of State made the determination as part of her job, following a case presented by challengers.
- Despite the impeachment proposal, the composition of Maine's legislature makes it highly unlikely to pass.
- The move seems more about signaling support and mobilizing the base than actually impeaching the Secretary of State.
- Making a determination as part of one's job should not be considered impeachable.
- The Secretary of State's position is likely safe despite the controversy.

### Quotes

- "Maine's Secretary of State declared Trump ineligible due to the 14th Amendment."
- "Making a determination as part of one's job should not be considered impeachable."
- "The move seems more about signaling support and mobilizing the base than actually impeaching the Secretary of State."

### Oneliner

Maine's Secretary of State faces impeachment proposals after declaring Trump ineligible due to the 14th Amendment, but the move seems more about signaling support than actual impeachment.

### Audience

Maine residents, political activists

### On-the-ground actions from transcript

- Contact local representatives to express support for Maine's Secretary of State (implied)

### Whats missing in summary

The full transcript offers insights into the political dynamics in Maine and the implications of a controversial decision made by the Secretary of State.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Maine and the 14th amendment and Trump and
meaningless resolutions and legislation being proposed by Republicans because
the never ending clown car of Republicans who are willing to come out and introduce
legislation that has absolutely no chance of passing simply to signal to their base
is just, well, it's never-ending.
Okay, so a quick recap.
The Secretary of State in Maine, Bellows,
well, she decided that Trump was ineligible
to be on the ballot there because of the 14th Amendment.
Okay, this is something that is definitely going
to go through the court system, no doubt.
A Republican who undoubtedly wants a lot of press for it
named, I can't remember his name, anyway, a Republican introduced some legislation
to impeach Bellows over this. Okay, if you were in a Republican state that was
incredibly partisan, maybe this would go somewhere, but the reality is she had to
make a ruling on it, that it's her job, and she stated that the challengers that
brought the case forward, that brought it forward, made it clear that Trump, quote,
used a false narrative of election fraud to inflame his supporters and direct
them to the Capitol to prevent certification of the 2020 election and
the peaceful transfer of power. That's literally just her doing her job.
Now, you may not agree with her determination, but making that determination is her job.
Okay, so let's go to what is the likelihood that she be impeached over this.
In Maine, there are 151 seats in what amounts to their house.
I think they may even call it a house there.
their state legislature there, the lower house. 80 of them are Democrats, 68 are
Republicans, 2 are independents. I mean, I don't know, it doesn't seem like it's
going to get through there, just saying. Let's go to the state Senate. 35, okay, out
of 35, 22 are Democrats, 13 are Republicans.
The likelihood of this going anywhere is zero.
It's something to mobilize the base and signal support.
And that's it.
There's not a chance that this is going to occur.
And I'm not sure that making a determination that you're
required to make is something that really amounts to
something that could be considered impeachable. So, we'll see how this plays
out. I'm fairly certain the Secretary of State's job is pretty safe. Anyway, it's
It's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}