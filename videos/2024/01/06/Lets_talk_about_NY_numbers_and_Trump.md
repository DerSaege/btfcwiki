---
title: Let's talk about NY, numbers, and Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=JxUxIF8tMVM) |
| Published | 2024/01/06 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the increase in numbers in Trump's civil case in New York.
- Attorney General in New York is asking for an increase from the original 250 million to 370 million.
- Details the breakdown of the new number: $2.5 million in bonuses, $60 million in profit from Ferry Point, $139 million in profit from the old post office in D.C., and $168 million in saved interest from four commercial loans.
- State wants lifetime bans on Trump, Weisselberg, and McHoney from participating in the real estate industry or acting as officers or directors of any New York corporation.
- Trump's sons may face five-year bans.
- Closing arguments are on Thursday, after which the judge will make a ruling.
- Expectation is that Trump will have to pay, but the exact amount is uncertain due to changing numbers.
- Anticipated ruling includes some form of ban and a monetary penalty, likely exceeding a hundred million.
- Case is expected to conclude soon, moving on to the appeals process.
- Overall, the case is finally starting to wind down.

### Quotes

- "State wants lifetime bans on Trump, Weisselberg, and McHoney from participating in real estate."
- "Expectation is that Trump will have to pay, but the exact amount is uncertain."
- "Case is expected to conclude soon, moving on to the appeals process."

### Oneliner

Attorney General in New York is seeking an increase in Trump's civil case, with anticipated lifetime bans and monetary penalties, as the case nears its conclusion.

### Audience

Legal observers, political analysts

### On-the-ground actions from transcript

- Stay updated on the developments of the case (suggested)
- Analyze the implications of potential lifetime bans and financial penalties (suggested)

### Whats missing in summary

The full transcript provides a detailed breakdown of the increased numbers in Trump's civil case in New York and the potential consequences for him and others involved.

### Tags

#LegalCase #Trump #NewYork #AttorneyGeneral #CivilCase


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about New York
and an increase in numbers in Trump.
We'll go over what the attorney general up there
in New York is asking for and why,
why the numbers changed,
and just run through everything that is going on.
Now this is in Trump's civil case in New York
that has to do with the paperwork up there. Okay, so if you remember, James was
asking for 250 million. That was the baseline number that they were talking
about throughout the whole thing here. Now, as closing arguments approach, the
new number is 370 million, and that is based off of information obtained via
testimony and they recalculated it. It comes from $2.5 million in bonuses, $60 million in profit
from Ferry Point, $139 million in profit from the old post office in D.C., and $168 million in saved
interest from four commercial loans. On top of this, for Trump, Weisselberg, and McHoney, the state
wants basically lifetime bans on participating in the real estate industry or acting as an officer
or director of any New York corporation or entity. Trump's sons, they, she's asking for five-year bans
there. Now, closing arguments are Thursday. After that point, the judge can make a ruling and we'll
find out what's going on. The expectation at this point is that Trump is definitely
going to be paying. Now, what the numbers are, don't know. There's not a lot of agreement
there, especially now that the numbers have changed. I'm sure that's going to even widen
the debate but the expectation is that the judge is going to come back and
probably agree with some kind of ban for some period and then a dollar amount.
Most people think that it's going to be more than a hundred million but that's
at this point when it comes to that talking to them most of them they'll
admit they're kind of guessing. So we'll just have to wait and see what comes
back. But it shouldn't be too long after Thursday and then this one can finally
come off the whiteboard and move over to the new whiteboard that's just going to
be filled up with all the appeals. So this particular case is finally starting
to wind down and we'll find out soon. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}