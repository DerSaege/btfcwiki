---
title: Let's talk about AI and Bridgeton, New Jersey....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=--P0MW1wJf0) |
| Published | 2024/01/06 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Newsbreak article detailed a fake murder that never occurred in Bridgeton, New Jersey on Christmas Day.
- The article was assisted by AI tools and warned of potential errors.
- AI technology was used to generate not just a story but an entire fake event.
- AI's limitations in creating accurate news stories are evident, as seen in failed attempts to cover high school sports.
- AI scrapes information and cannot generate news content without initial human input.
- News agencies experimenting with AI may produce inaccurate information, akin to playing a game of telephone.
- AI technology is not advanced enough for use in newsrooms, as it may present major story elements inaccurately or fabricate events.

### Quotes

- "AI's limitations in creating accurate news stories are evident."
- "News agencies experimenting with AI may produce inaccurate information."
- "AI technology is not advanced enough for use in newsrooms."

### Oneliner

A cautionary tale on the limitations of AI technology in news reporting, warning against assuming accuracy in AI-assisted content.

### Audience

News consumers

### On-the-ground actions from transcript

- Verify critical information with trusted sources (implied)

### Whats missing in summary

The importance of critically evaluating AI-generated news content and relying on trusted sources for accurate information.

### Tags

#AI #NewsReporting #FakeNews #Bridgeton #NewJersey


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about Bridgestone, New Jersey
and something that happened on Christmas day
that never happened, but caused quite a stir.
And it reminds us of some of the limitations
when it comes to certain new technology, particularly
when it comes to news.
Okay, so what happened, a site called Newsbreak had an article on it that has now been deleted,
but the article detailed the circumstances of a murder that occurred in Bridgeton, New
Jersey on Christmas Day.
This is from the PD there, police department in Bridgeton.
Something even similar to this story occurred on or around Christmas, or even in recent
memory for the area they described.
It goes on, the article does not have an author and states at the bottom, this post includes
content assisted by AI tools.
This content was assisted by AI and may contain errors.
Please verify critical information with trusted sources.
So since it was removed, there's not a lot of investigating I can do to track down like
how all of this came to be, but it certainly appears as though somebody who was using AI
write articles generated not just a story about an event but generated an
entire event. It never occurred. So this is one of those questions that comes up
how is AI going to impact the news? It's not. Not really. When you compare this to
instances where they tried to use AI to like cover high school sports and how
bad it has gone. It shows that AI is not to the level it needs to be at to
function when it comes to news. It's important to remember that AI is not AI
in the way that you might imagine it from science fiction. It scrapes
information. So when it comes to news, it has to be put out there by a person
first. Otherwise, it's not going to be in the data set. And if you were to try to
get it to write an article saying maybe you want something unique about a crime
or something like that. It may just literally make one up. Please remember
this as more and more news agencies experiment with AI. Just assume that the
information is wrong. View it as a good way to get a summary, but
that you're probably playing a game of telephone, and that as it progresses and
it aggregates different outlets and it scrapes that information and puts it
together, that it is going to get major pieces of the story and present them in
well a less than accurate way. Or it may just make up an entire event because the
person asked for a story about something this doesn't belong in newsrooms yet this
technology it's just not there anyway it's just a thought y'all have a good
day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}