---
title: Let's talk about Trump, the Constitution, and charges....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=HrDsRyQVN84) |
| Published | 2024/01/06 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains why Trump wasn't charged with treason in the United States.
- Treason in the U.S. is narrowly defined as levying war against the country or aiding enemies.
- Refers to Article 3, Section 3, Clause 3 of the U.S. Constitution for the specific definition of treason.
- Mentions that treason was narrowly defined because the founders themselves had committed it.
- Talks about how treason charges against Trump wouldn't hold up in court due to other laws governing broader misconduct.
- Criticizes those who misuse the term "treason" for trivial matters like forgiving student debt.
- Points out that invoking treason in situations not involving war is a sign of ignorance about the Constitution.
- Emphasizes that treason, insurrection, and sedition are distinct charges with specific criteria.
- Advises to be cautious of individuals using inflammatory rhetoric about treason without a basis in war.
- Encourages understanding the specific definition of treason to combat attempts to manipulate public opinion.

### Quotes

- "Treason in the United States is incredibly specific."
- "If you understand that treason requires war, when you have people out there yelling that this is treason or that's treason and there's not a war, you know that they're using a rhetorical device to anger you."
- "Treason requires conflict. It is incredibly narrow."

### Oneliner

Beau explains why Trump wasn't charged with treason, clarifies the specific definition in the Constitution, and warns against inflammatory misuse of the term.

### Audience

Constitutional learners and those combating misinformation.

### On-the-ground actions from transcript

- Understand the specific definition of treason in the U.S. and spread accurate information (implied).

### Whats missing in summary

Additional context on the dangers of misusing legal terms like treason and the importance of educating oneself on constitutional matters.

### Tags

#Trump #Constitution #Treason #Misinformation #LegalKnowledge


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Trump and we are going to talk about
the constitution of the United States.
And we're going to answer a question that for whatever reason has come in, I
don't know, 25 times over the last three days, and it's something we've talked
about before, but it's definitely worth going over again, and it all revolves
around why Trump was charged the way he was, and why he wasn't charged with something
more serious.
The core question in all of those messages is, why wasn't Trump charged with treason?
And the answer is simple, because in the United States, he didn't commit treason.
Treason in the United States is incredibly specific.
It can only consist of levying war or helping those who are levying war.
That's it.
And the reason is Article 3, Section 3, Clause 1 of the U.S. Constitution spells it out.
And it says, treason against the United States shall consist only in levying war against
them or in adhering to their enemies, giving them aid and comfort. When this got turned into a
federal law, it became, whoever owing allegiance to the United States levies war against them or
adheres to their enemies, giving them aid and comfort within the United States or elsewhere is
guilty of treason. That's why. It's in the Constitution because the people who wrote the
the Constitution were very sensitive about the word treason. Why? Because they
all committed it. So you could make the argument that because there were arms
there and because he said to fight and because it was to disrupt the peaceful
transfer of power that really it could apply. You can make that argument but
why would you want to in a courtroom? Because the U.S. has a bunch of other laws governing
conduct that isn't as narrow, and that's what he was charged under. So that's why it is the way it is.
The reason I think this is worth going over again is because you see people all the time invoke that
over just silly things. President Biden committed treason because he tried to forgive student debt.
Did he use war to do it?
Because if not, all you're doing is loudly proclaiming, I've never read the Constitution.
That's it.
That's your statement.
You see that rhetoric a lot about treason coming from political commentators and now
coming from politicians.
If somebody suggests something is treason and it doesn't involve war, you can safely
assume that at that point they probably shouldn't be in office because they've never read the
document that they've sworn an oath to support and defend.
Treason is very specific in the United States.
There are other charges, insurrection, sedition, stuff like that that covers like conduct that
doesn't rise to levying war or helping those who are levying war.
It's just one of those things that when, if you understand this, when you're hearing
that rhetoric, you can just safely discount what that person's saying.
You can just safely say, OK, this person isn't actually somebody who cares about the Constitution.
This is somebody who is saying something to make me angry, probably because they benefit
financially if I get angry.
You can use that as kind of a method of turning down the temperature while people are intentionally
trying to inflame it. Treason in the United States is super specific. You have to set
out to try to commit treason. Insurrection, rebellion, occupying some square blocks in
the Pacific Northwest. None of this is treason. Treason requires conflict. It is incredibly
narrow and remembering that can help take away one of the tools that those who like
to inflame the populace, use a lot. If you understand that treason requires war,
when you have people out there yelling that this is treason or that's treason
and there's not a war, you know that they're using a rhetorical device to
anger you. And then your next question should be why. Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}