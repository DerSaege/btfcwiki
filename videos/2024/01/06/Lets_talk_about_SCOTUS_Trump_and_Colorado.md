---
title: Let's talk about SCOTUS, Trump, and Colorado....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=JDDjHLED1qY) |
| Published | 2024/01/06 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The US Supreme Court is reviewing the Colorado Supreme Court's decision to remove Trump from the ballot.
- The expedited process includes Trump's brief on January 18th, Colorado's brief on January 31st, and oral arguments on February 8th.
- Normally, this process takes three times longer, but the Supreme Court has decided to expedite it.
- Beau personally wishes the main case of the Secretary of State's decision to remove Trump had been addressed.
- Depending on the Supreme Court's ruling, there may or may not be more cases afterward.
- Trump and his community accuse the left of a well-funded effort to remove him from the ballot.
- Trump believes his influence over three Supreme Court justices could work in his favor.
- Beau doesn't think this route will work but is surprised it has reached this point.
- Key issues need resolution, and developments are expected in about a month.
- Despite his doubts, Beau acknowledges that unexpected outcomes are possible.

### Quotes

- "The US Supreme Court is reviewing the Colorado Supreme Court's decision to remove Trump from the ballot."
- "I'm not 100% convinced that's going to happen."
- "I never expected it to get this far."

### Oneliner

The US Supreme Court is fast-tracking the case of Trump's removal from the ballot, with key issues expected to be resolved in about a month, amid accusations of a left-funded effort.

### Audience

Legal observers

### On-the-ground actions from transcript

- Stay informed about the developments in the case (suggested)
- Monitor how the Supreme Court's decision unfolds (implied)

### Whats missing in summary

Context on the potential implications of the Supreme Court's decision and the broader impact on future elections. 

### Tags

#USsupremecourt #Trump #Colorado #elections #legal #expeditedprocess


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about
the United States Supreme Court,
the Colorado Supreme Court, and Trump,
because the US Supreme Court has decided
to review the Colorado Supreme Court's decision
to remove Trump from the ballot.
So one of the many things that Trump world
world refers to as a as a witch hunt is finally going to be at a convention of
people in black robes the the thing that I want to stress about this is that they
are doing it fast and I mean fast the let's see I want to say Trump's brief
goes in on January 18th the petitioners from Colorado their brief goes in on
January 31st and oral arguments are on February 8th. That is fast for the Supreme Court to get
to that point that quickly. They have definitely decided to expedite it as much as humanly possible.
I mean it normally takes three times this long. So there is that. Now personally I wish they had
done the main case, the decision of the Secretary of State to remove Trump there, I wish they
had gone that route.
That would have kind of decided it all at once.
If the Supreme Court rules in favor of Trump, this is probably it.
If they rule in favor of Colorado, depending on how they rule, there could still be more
cases afterward because they could be going after different issues of it
depending on what Trump argues here. So that's a brief overview of it. We'll have
an answer relatively quickly on how this is going to play out from here. Trump and
his community of people, of course, are characterizing the attempts to have him
removed from the ballot as a well-funded effort by the left because apparently in
the world of alternate facts a whole bunch of Republicans that filed you know
a whole bunch of these challenges yeah I guess I guess they're left and Trump
himself went out of his way to mention that he fought very hard to get three of
these people appointed to the Supreme Court. I think that he is counting on that to sway
the justices in his favor. I'm not 100% convinced that's going to happen. I don't, I've been
pretty clear I don't actually think that this is a route that will work but at
the same time I never expected it to get this far so if I'm wrong on that I'm
wrong on that but there are some key issues that have to be resolved but
apparently they're gonna be resolved in about a month so we'll have to keep an
eye on this and see how it develops from here anyway it's just a thought you know
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}