---
title: Let's talk about Texas and chains of command....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=eaQVce58PK8) |
| Published | 2024/01/14 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Mexican officials witnessed migrants in distress in the river and contacted Border Patrol for help.
- Border Patrol tried to access the river at Shelby Park but was stopped by the Texas Military Department.
- Texas Military Department sent people with night vision and lights to the river, saw Mexican officials responding, and called off their search.
- A woman and two children drowned in the Shelby Park area after Border Patrol was physically barred by Texas officials from entering.
- Texas engaged in publicity stunts under state control, separate from federal operations handling immigration.
- The lack of coordination between the two chains of command led to tragedy.
- The Biden administration asked the Supreme Court to intervene after Texas took over the area, but there has been no action yet.
- The incident resulted in unnecessary loss of life due to a mix of publicity stunts and poor communication.

### Quotes

- "Chains of command, plural."
- "A woman and two children because of what amounts to a publicity stunt and poor communication."
- "The two chains of command are obviously not functioning properly."

### Oneliner

Mexican officials call Border Patrol for help, but Texas officials' interference leads to tragedy, exposing a lack of coordination between state and federal chains of command.

### Audience

Border Patrol officials

### On-the-ground actions from transcript

- Contact Border Patrol for assistance if witnessing distress in border areas (implied)
- Advocate for improved communication and coordination between state and federal agencies to prevent tragedies (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of the events leading to a tragic loss of life at the border due to a lack of coordination between different chains of command.


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about Texas and Mexico
and border patrol and chains of command.
Chains of command, plural.
It's weird to hear it said like that, right?
Because it's normally chain of command, singular.
chains of command. And we will go over what occurred and what is certain to become a massive
story. Okay, so as near as I can tell, this is how the events unfolded based on the reporting
available at hand. Mexican officials, they witnessed people, migrants in distress in
the river. They contacted Border Patrol. Border Patrol showed up at an area called Shelby Park
to try to get access to the river, where they were met by state officials under the Texas
Military Department, who did not allow them to enter to go help those in distress. It is reported
that the Texas Military Department said that they would take care of it. They
sent people down to the river with NVGs and lights, night vision and lights.
And they detected Mexican officials responding to an incident, so they
They didn't do anything, so they called off their search.
The very likely chain of events here is that Mexican officials witnessed people in distress,
to Border Patrol. Border Patrol wasn't allowed in Texas Military Department who
is operating under a different chain of command because why they're dealing with
the border, we'll find out I'm sure. They went down there but because they weren't
looped in, they very well probably saw the people who called, the Mexican
officials who called Border Patrol. And because they saw them, they called off
their search. The official statement from from the Department of Homeland
Security says, tragically a woman and two children drowned last night in the
Shelby Park area of Eagle Pass which was commandeered by the state of Texas
earlier this week.
In responding to a distress call from the Mexican government, Border Patrol agents were
physically barred by Texas officials from entering the area.
If you missed it, Texas in an attempt to act like they were doing something, they've been
engaging in a bunch of publicity stunts.
These publicity stunts are under state control.
The state operations, they're under state control.
They're not linked in with the federal operations, which is who actually handles the border in
immigration.
The two chains of command are obviously not functioning properly.
This is why, generally with something like this, you have a single chain of command.
You don't have to, because stuff like this happens.
It's worth noting that as soon as this happened with Texas taking over this area, the Biden
administration asked the Supreme Court to step in. There hasn't been any movement
on that. I imagine there will be now. This is completely unnecessary. A woman
and two children because of what amounts to a publicity stunt and poor communication.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}