---
title: Roads Not Taken EP 21
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=xdzhRoSVNzw) |
| Published | 2024/01/14 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Gives an overview of global events from the past week, covering foreign policy, conflicts in Taiwan, Middle East, Ukraine, and Myanmar.
- Mentions the disarray in the U.S., including Hunter Biden's attorneys' promise to comply with new subpoenas and the MAGA faction's anger.
- Raises concerns about the inclement weather affecting the Iowa caucus turnout.
- Shares cultural news about Bill O'Reilly, Taylor Swift conspiracy theories, and environmental updates on climate change.
- Touches on oddities like remains in space, Fox News dropping MyPillowGuy, and major archeological finds.
- Responds to questions on various topics, including the perception of the "woke society" by Normandy veterans, his identity, and global warming.
- Engages in a Q&A session on topics like the NASA Lockheed Martin X-59 Quest and McGurk's plan for Palestine.
- Clarifies misconceptions about his statements regarding Trump's actions in Yemen during his previous term.
- Addresses the debate on whether the U.S. is a Constitutional Republic or a democracy.
- Offers insights on equipping a robot for disaster relief missions.

### Quotes

- "The people who stormed the beaches of Normandy were the people who kept FDR in office."
- "The whole reason they stormed the beaches of Normandy was to fight right-wing authoritarian goons."
- "It's literally because of it. Just saying."

### Oneliner

Beau covers global events, U.S. politics, cultural news, oddities, Q&A sessions, and clarifications while offering insights on equipping robots for disaster relief.

### Audience

Global Citizens

### On-the-ground actions from transcript

- Keep informed about global events impacting foreign policy and conflicts (implied).
- Stay updated on U.S. political developments, including budget issues and MAGA faction reactions (implied).
- Monitor weather conditions affecting political events like the Iowa caucus (implied).
- Support environmental initiatives to combat climate change and its effects (implied).
- Stay engaged with cultural news and remain critical of conspiracy theories (implied).

### Whats missing in summary

Insights on the significance of historical events for shaping current strategies and policy decisions.

### Tags

#GlobalEvents #USPolitics #CulturalNews #EnvironmentalNews #QandA #DisasterRelief #HistoricalSignificance


## Transcript
Well, howdy there, internet people, it's Beau again,
and welcome to The Roads with Beau.
Today is January 14th, 2024,
and this is episode 21 of The Roads Not Taken,
which is a weekly series
where we go through the previous week's events
and talk about news that was unreported, underreported,
just didn't get the context or attention it deserves,
or I just happen to find it interesting.
After that, we will go through some questions from y'all.
Okay, starting off with foreign policy.
Taiwan voted to stay with the party that opposes reunification.
They push back on China.
There's going to be a lot more on this over on the other channel.
Biden has already made it clear that the U.S. does not support independence for Taiwan,
And we'll talk about why over on the other channel.
Taking advantage of the current disarray in the Middle East,
Turkey launched a series of strikes against the Kurds
in Iraq and Syria.
Something like this is one of those things
that could cause the situation to spiral
into a regional conflict.
because it's, it is outside of what's being managed.
Okay, Ukraine is being hammered by Russian strikes
after congressional funding has run out,
while congressional Republicans certainly appear
to be siding with a country that constantly threatens
to use nuclear weapons against the U.S. and its allies.
A ceasefire agreement was reached in Myanmar between the government and the rebel alliance.
That's really what they're called, the rebel factions there.
It was apparently brokered by the Chinese.
This conflict has not been getting a lot of coverage because there's not a built-in audience
for it in the West.
But it was closely followed by military analysts because the rebels, they approached it with
a great deal of ingenuity.
This is going to be studied.
This is going to be one of those conflicts that informs strategy later.
Okay, moving on to the U.S.
Hunter Biden's attorneys have promised that he'll comply if the GOP issues new subpoenas.
Whether intentional or not, Speaker Johnson is currently engaging in some stuff with the
budget that is rendering the MAGA faction irrelevant.
They are super mad about it.
They need capes.
They're that mad.
They are super mad.
I would imagine you're going to see a lot of fireworks within the Republican Party in
the House this week.
The Republican representative from Tennessee, Tim Burchette, indicated that he believed
some of his colleagues in Congress were being blackmailed, but has not elaborated on that
except to say that it isn't necessarily by a foreign power.
I mean, that's interesting.
That'll probably come up again.
Republicans are currently worried
that inclement weather will decrease the turnout
at the Iowa caucus.
Chris Christie dropped out of the GOP presidential primary,
but not before getting caught on a hot mic
mocking other candidates.
It appears that Christie believes Nikki Haley is going to be smoked.
Inflation declined last month.
Moving on to cultural news, Bill O'Reilly has had some books at least temporarily removed
from libraries and schools in Florida under a law that he supported something-something
leopards and, you know, yeah, so there's that.
I would imagine that that's going to turn into a thing because he's probably going
to reach out to government officials that he knows in Florida and then they will exert
pressure, at which point all of this becomes a First Amendment issue in a big way.
I didn't know whether to put this under cultural or oddities, to be honest.
Taylor Swift has been accused by right-wing conspiracy gurus of being a Pentagon psychological
operation, Taylor Swift the Psyop. Can I just say that nothing makes a creator
feel more accomplished than a rumor like this saying that an intelligence agency
is actually like behind everything because the subtext of it, the
underlying concept, is that this person is so good at what they do they have to
have like this massive, secretive organization behind them. There's really not anything more
flattering than that, just so you know. In environmental news, 2023 is now confirmed
to be the hottest year on record. I don't think anybody's really surprised by that. There is now
a push to be less subtle when describing how people need to change their behavior to help
the climate. Rather than just go green, they're looking at putting out more specific talking
points on what the average person can do. As soon as there is some consensus, I will
definitely let y'all know what those are. Okay, moving on to oddities. There's a few
of them. Gene Roddenberry and Star Trek actor James Duhan, better known as Scotty, will
be forever drifting through space along with a bunch of other additional human remains
because technical difficulties made depositing the other remains on the moon impossible.
So there were like two missions.
One was to send the Star Trek people like out into space.
The other one was to put remains on the moon, but now they're all going.
If you're Navajo, well, I mean, you know.
Okay, Fox News has reportedly dumped the MyPillowGuy, and that's, from what I understand in the
reporting, it has to do with him not paying.
It has been a wild, wild week in archaeology with major finds in the Amazon, Saudi Arabia,
and Egypt.
definitely going to be more over on the other channel about this because some of it rewrites
history books.
But short version about the one that I find most interesting, there's like a massive
network of cities in the Amazon.
Okay, recently the world was just captivated by viral images of quote alien mummies that
were found in Peru and they spawned theories about pre-Columbian contact
with civilizations that are literally out of this world. Upon forensic analysis
it was determined that the the mummies were made from human and animal bones
along with paper and modern synthetic glue. So not only were they not aliens
they they weren't even like old. Okay, moving on to the Q&A portion. Note, say the
email address. Question for Bo. Question and then F-O-R Bo-B-E-A-U all one word at
gmail and then this is the same note from last week. Tell everybody that I
I don't use Telegram. I will never ask you to go to Telegram and ask for your personal information.
That is a scammer in the comment section.
Okay, what do you say to people that say those who stormed the beaches of Normandy
wouldn't put up with our woke society? First, I always want to know why it's the
beaches of Normandy.
Like you never hear people say, those who fought at Anzio.
We just talked about Star Trek.
Star Trek was easily one of the most woke TV shows, especially for its time.
A lot of social commentary, a lot of injecting politics into the story type of thing.
Little bit of trivia, you almost never see Scotty's hand.
His right hand, you almost never see it, and when you do see it, it's always kind of obscured.
You never really see it full on, except in a couple of very rare scenes.
It's because he left a large chunk of his hand on the beaches of Normandy.
He was a forward observer.
So I don't know that that's true.
The people who stormed the beaches of Normandy are the people who kept FDR in office.
people who created all of the woke shows or starred in them. And never forget the
whole reason they stormed the beaches of Normandy was to fight right-wing
authoritarian goons. Just saying. Why don't you clarify that you're not Jewish? I
think it might help. Maybe I am. Just like maybe I'm Muslim or maybe I'm gay
or trans or any of the other things that get said. I don't feel like, I feel like if
you deny something like that you're undermining people, if that makes any
sense. If you're denying it, you are in essence saying that's a bad thing if
if you feel like you have to issue a denial about it.
So, maybe I am.
I don't care.
How can you believe in global warming with this weather?
Fun fact,
actually.
The
the arctic blasts that are coming down, the polar waves, whatever term
the weather people are using right now,
the reason that's happening more is because of warming weather.
The winds,
there's like a polar vortex that used to keep
all of the cold air up north and it's become unstable because of the
changing climate.
So it's literally because of it.
Just saying.
The NASA Lockheed Martin X-59 Quest thoughts.
OK, hang on.
The X-59.
OK, so this is the one that looks really weird.
So this is a plane that is being unveiled.
The public-facing story behind it
is that it is to reinvigorate the interest
in supersonic commercial travel, and they will be flying this thing, like once they
get it tested, they'll be flying it over cities and then talking to the people in the
cities to see if they heard it, because it's supposed to be really quiet, it reduces all
of the noise and all of that stuff, and that is certainly for supersonic commercial travel.
There is no possible military application for a very quiet supersonic plane.
What do you think of McGurk's plan?
Little bit of context on that for people who don't know.
A plan for dealing with the period after the fighting when it comes to Palestine, it has
been leaked. At least parts of it have been discussed. My understanding is that this is
not the plan. This is one of multiple plans that are being considered. As far as what
was leaked, I have questions. I definitely have some questions. I mean, it's an interesting
idea and some of the the angling and the the foreign policy intrigue in it. I mean
it's a good way to get people on board but if we want an end to this it ends
with a multinational force standing between the two sides a Palestinian
state or at least a a very viable path to a Palestinian state and then
dump trucks full of cash pouring into Gaza and the West Bank to aid in
reconstruction and rebuilding the economy to a point where people feel
comfortable and then are less likely to suffer from economic instability which
feeds recruitment for extreme groups. Nothing's really gonna get away from
from that. Those ingredients have to be there and they're not all there in McGurk's plan.
Some of the way he's talking about or the way that plan talks about bringing in other
Arab nations to help with the aid and to kind of use that, use one problem to solve another
as far as normalization of the relationship between Israel and Saudi Arabia. All of that
That sounds great, but the key ingredients for actually dealing with the issue I have
questions about.
Okay, cute rant about Trump in Yemen.
I guess we'll have to wait until his next term to see if he does any of that, right?
I love the way you dumb lefties act like you know what he's going to do and act like he's
a war mongerer.
I'd love for you to do a whole video explaining how you know with so much certainty that he
would put boots on the ground in Yemen or veto a resolution.
Come on, you homosexual, show your work, female dog.
Okay, so funny story.
What I said in that video, that's not what I expect him to do in his next term.
It's what he did in his last one.
Those weren't hypotheticals.
vetoed the resolution to end U.S. involvement in Yemen. He did in fact
order boots on the ground arrayed shortly after he took office. Those
weren't hypothetical. That it already happened. That's my work. Go to Google
and type the stuff in. Man. Okay. Constitutional Republic or are we a
democracy? I have a video on this that's pretty in-depth. This is a lot like
saying if somebody asks you do you have a car and you say no because you have a
Jeep. You have a vehicle. I'll put the video in the description and it kind of
goes through it. Just understand the people who say that, most of them don't
even know what these words mean and I'm not saying that to be mean. I'm saying
they literally do not know what what what they're saying. You know, we're not a
democracy, we're a republic. Yeah, okay. I would point out that China is a republic,
the United Kingdom is not. Which one are we more like? Okay, if you were able to go
into a disaster area, but could send a robot into the disaster area, what would
be the top six things you'd equip it with, you can put tools in there and the
robot can serve as a battery generator. Six things, six things to equip a robot
with for disaster relief. Okay, assault. Let's start with that. Assault. I'm assuming
we're sending the robot into a like an impacted area yeah so a saw a radio or
phone so anybody that the robot can reach that people can't can make calls to
whoever they need to and then of course I would want like a video feed an audio
feed from the robot back so the person controlling the robot could talk to the
people. I would put an IFAC, an advanced first aid kit on the robot and add the
ability to purify water and hands like robot hands. I'm wondering if this is the
The tech person who sent out the other question told me to pitch a tech idea in 60 seconds
or something.
Okay, so that looks like it.
This week there's going to be a whole lot of developments.
Probably in the US, I would definitely keep your eyes on the house.
Probably in the Middle East, definitely in Ukraine.
There have already been some interesting developments inside Russia related to Ukraine, and then
we're also throwing Taiwan into the mix now, and we'll go over that as well.
So there's a lot going on.
Okay, but there you go, a little bit more information, a little more context, and having
the right information will make all the difference.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}