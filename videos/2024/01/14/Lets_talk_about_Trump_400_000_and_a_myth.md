---
title: Let's talk about Trump, $400,000, and a myth....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=WH49_mm_SPg) |
| Published | 2024/01/14 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump filed a suit against the New York Times, three reporters, and Mary Trump over an article that challenged one of his core myths about being a self-made billionaire.
- The article alleged that Trump's wealth was due to his father giving him access to hundreds of millions of dollars.
- Trump has been ordered to pay $392,638 in legal fees to the New York Times and the three reporters because their activities were protected under the First Amendment.
- The case involving Mary Trump is still undecided and moving forward, experiencing many delays.
- This incident sheds light on the lengths Trump goes to preserve his image and myths.
- The article in question received a Pulitzer Prize in 2018.
- This development might be overshadowed by other news on Trump's legal issues but is worth paying attention to.

### Quotes

- "It shows the lengths that he'll go to to kind of preserve the image, the myth."
- "The reason Trump really didn't like this article is because it attacked one of those core myths about him."

### Oneliner

Trump sued over a core myth, ordered to pay $392,638 in legal fees, shedding light on his efforts to preserve his image.

### Audience

Legal observers, news followers, Trump critics

### On-the-ground actions from transcript

- Follow updates on the legal case involving Trump, the New York Times, and Mary Trump (suggested)
- Stay informed about instances where public figures sue for defamation to control their image (implied)

### Whats missing in summary

Further details on Mary Trump's involvement and the potential implications of the ongoing legal case.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about myths and Trump,
and Trump having to pay almost $400,000.
And the semi-conclusion to one of Trump's many entanglements
because we have part of one here.
So a while back Trump filed suit against the New York Times, three reporters and Mary Trump.
And it had to do with an article that was put out.
The reason, if I had to guess, the reason Trump really didn't like this article is
because it attacked one of those core myths about him.
And that's that, you know, he was a self-made millionaire, billionaire, gazillionaire, whatever.
The New York Times article alleged that, well, that really wasn't the case and documented
how his dad had given him access to hundreds of millions of dollars.
But that reporting, it undermined one of those core myths, those things that he has to keep
out there for his base to really believe in him because that's part of his brand, his
image.
So when that article went out, he was presumably upset, filed suit.
Now the New York Times and the three reporters, that activity was deemed to be squarely within
the First Amendment.
So Trump now has to pay them, he sued them, but now he has to pay them $392,638 for legal
fees.
Now the portion of the case involving Mary Trump, that is still undecided.
still moving forward. There's been a lot of delays in that one. So we'll continue to follow it,
but this portion of it is over at least. This is probably going to be overshadowed by all of the
other news involving Trump's legal issues, but this is one that's worth kind of paying attention
to because it shows the lengths that he'll go to to kind of preserve the
image, the myth. It is worth noting that the article in question received a
Pulitzer Prize in 2018. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}