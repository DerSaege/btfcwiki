---
title: Let's talk about Trump, NY, and his statement....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=dJWUxq7iP0w) |
| Published | 2024/01/12 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's lengthy New York civil case regarding accounting discrepancies at his company is coming to an end.
- There was a back and forth about whether Trump could give a portion of the closing statement, with the judge allowing it under the condition that he sticks to facts and the law.
- Trump seemed reluctant to stick to the facts, resulting in the judge rescinding permission for him to speak.
- During court proceedings, Trump claimed innocence and portrayed himself as a victim of persecution by someone running for office.
- Trump insulted multiple people in the room, including the judge, and his behavior led the judge to tell him to control himself.
- One of Trump's attorneys mentioned un-rebutted witnesses and urged the judge to consider their testimony, which may not hold legal weight.
- Another attorney suggested the judge should think about his legacy, which is not within the judge's purview.
- After Trump's speech, he stormed out of the courtroom, leaving a lasting impression on the judge who is about to decide on a case involving hundreds of millions of dollars.
- The judge is expected to make a decision on the case by the end of the month, providing temporary closure before inevitable appeals take place.

### Quotes

- "The financial statements were perfect, the banks got back their money and are as happy as can be."
- "You have your own agenda. You can't listen for more than a minute."
- "Some of our witnesses weren't rebutted, so you have to consider their testimony."
- "He needs to control your client."
- "Definitely eventful once Trump was done giving his little speech."

### Oneliner

Trump's New York civil case comes to a dramatic close with insults, claims of persecution, and a stormy exit, leaving the judge to decide amid hundreds of millions at stake.

### Audience

Legal observers, court watchers

### On-the-ground actions from transcript

- Follow updates on the case and the judge's decision by the end of the month (suggested)
- Engage in informed discourse about legal proceedings and accountability (implied)

### Whats missing in summary

Insights into the potential implications of the judge's decision and the impact on future legal proceedings.

### Tags

#Trump #NewYork #legal #civilcase #courtroom drama


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about Trump in New York
and the end of that very, very lengthy process.
And closing statements, the summation,
and how everything went, because there
were some twists and turns.
And it turns out I definitely should have used that popcorn.
Okay, so just a quick recap.
This is the end of the very lengthy New York civil case
involving the discrepancies
in the accounting at Trump's company.
There was a back and forth about whether or not Trump
would be allowed to give a portion of the closing statement.
And the judge basically told them, hey, you know, he can. I'm inclined to let
everybody speak, but he has to stick to the facts and how those facts interact
with the law. Paraphrasing. That didn't seem to be what they wanted to do, so it
went from sure to no. The subject was brought up again today in court, and the
judge was basically asking Trump, hey can you, you know, talk about this without
just delivering a campaign speech and stick to the facts at hand?
Paraphrasing. To which Trump said, well I think your honor that this case goes
outside just the facts. The financial statements were perfect, the banks got
back their money and are as happy as can be. When you say don't go outside of
these things, we have a situation where I am an innocent man, I've been persecuted
by someone running for office, and I think you have to go outside the bounds."
Another relevant part was, this is a fraud on me.
What's happened here, sir, is a fraud on me.
He also said that he never had a problem before.
The judge asked him, wait, haven't you been sued before?
During Trump's statement, he pretty much insulted just about everybody in the room,
to include the judge saying, you have your own agenda.
You can't listen for more than a minute.
This has been a persecution, at which point the judge is finally like, hey, you need to
control your client.
It went on from there.
The only interesting thing that I'm aware of was one of Trump's attorneys basically
saying, you know, some of our witnesses weren't rebutted, you know, so you have to consider
their testimony.
It's an interesting point.
Now, I don't know if it sticks, legally speaking, in New York.
It's interesting, but I think with the judge being the trier of fact up there, I don't
know that that's actually a true statement, but it's worth noting.
Another interesting portion was one of the attorneys, I believe, saying that the judge
needs to consider his legacy, which no, he's actually not supposed to do that.
So it was definitely eventful once Trump was done giving his little speech.
He stormed out of the courtroom because that's always a good thing to, you know, be the last
thing that the judge sees, something like that.
It's worth remembering that the judge in this case is about to decide on a case that's,
I don't know, you're talking about hundreds of millions of dollars on the line.
Probably would have been better to take a different track with this, but I guess we'll
wait and see.
We'll see how it turns out once the judge tells us what the decision is, which we are
supposed to find out by the end of the month. So this is one that is coming off
the board, you know, with the exception of the inevitable appeals. So we will have
some kind of closure, at least temporary closure, on this particular Trump legal
entanglement before the end of the month. Anyway, it's just a thought. Y'all have a
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}