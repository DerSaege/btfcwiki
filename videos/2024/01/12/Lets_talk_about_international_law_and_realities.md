---
title: Let's talk about international law and realities....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=VULvbTs3wyI) |
| Published | 2024/01/12 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the application of international law and the need for a refresher.
- Reads messages urging him to address the ICC case and the South Africa case.
- Differentiates between the ICJ and the ICC in relation to the South Africa case.
- Mentions the limitations of the ICJ in creating desired outcomes.
- Foresees the ICJ case involving South Africa and Israel alleging genocide.
- Points out the lack of enforcement mechanism of the ICJ.
- Notes the cyclical conflict nature and PR aspects of the conflict.
- Indicates possible positive impacts of the ICJ case on the fighting.
- Emphasizes that the ICJ may not stop the fighting as desired.
- Mentions the potential for the ICJ case to inform an actual ICC case, which has enforcement powers.
- Talks about the ICC investigating events related to October 7th and hindrance of aid.
- Mentions that buildings like mosques and hospitals have protected status and hitting them requires justification.
- Expresses that the ICC case might bring about desired outcomes but is at least a year away.
- Compares the slow-moving nature of international law to the special counsel's office.
- Concludes by stating that while the ICJ case will have impacts, it may not meet everyone's hopes and expectations.

### Quotes

- "International law moves slower than the special counsel's office."
- "The ICJ does not have an enforcement mechanism."
- "The ICC is for people."
- "It's not irrelevant. It's going to have impacts."
- "Understand the ICJ case. It's not irrelevant."

### Oneliner

Beau explains the limitations of the ICJ in creating desired outcomes, especially in a cyclical conflict, while hinting at the potential impact of an ICC case in the future.

### Audience

International law enthusiasts

### On-the-ground actions from transcript

- Research and stay updated on the developments of the ICJ and ICC cases (implied).
- Advocate for accountability and justice in conflicts by supporting legal processes (implied).

### Whats missing in summary

Detailed analysis and breakdown of specific events leading to potential ICC involvement.

### Tags

#InternationalLaw #ICC #ICJ #ConflictResolution #EnforcementMechanism


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to once again talk about
the application of international law and how it works.
This is something that we discussed more than a month ago,
but it does seem as though maybe a refresher is in order.
So we're gonna talk about that.
I'm gonna read some messages and questions that I got,
and I'll answer one of them.
But I think it's worth kind of running through this again,
because there are a lot of people who are getting their
hopes up about something.
OK, so starting off with the messages that I received.
At this point, it's clear that blank, blank, blank, and you,
the blanks are two YouTubers and a podcaster, are all owned
by the Mossad.
You need to talk about the ICC case immediately.
Hey, rhymes with bike.
I see you're avoiding the ICC case.
Good job.
Talk about the ICC, Jew.
And then we have this one.
Everybody who talks about foreign policy
seems to be kind of ignoring or barely mentioning the South
Africa case.
Why?
That's a much better question.
That's one that's likely to get a response.
OK.
So if you don't know what the South Africa case is,
South Africa is taking Israel to the ICJ, not the ICC,
two different entities, just so you know.
If it was the ICC, everybody who talks about foreign policy
would be talking about it.
But the ICJ is a little bit different.
International law, a lot of agencies responsible for it,
have a hard time really affecting the outcome that they want to. They have a
hard time creating the outcome that they want to. Let me tell you what's going to
happen with the ICJ case. Odds are South Africa is going to take
Israel there. They are alleging genocide. The ICJ is probably going to issue a
provisional or temporary order saying, hey, stop the fighting. And that
temporary or provisional order is to stop the fighting long enough for the
case to play out. Now the case, we should see that wrap up, I don't know, sometime
around 2027. Now, but they did issue that temporary order saying for the fighting
to stop, right? So the fighting stops, of course. Now, like we talked about in the
first video, okay, you've made your order, now enforce it. The ICJ does not have an
enforcement mechanism. From a foreign policy standpoint, when you're talking
about actually affecting a stop to fighting, the ICJ is on par with a
Twitter poll. That's why it's not getting a lot of coverage because it really
can't do anything. Does that mean that it is completely irrelevant? No, it does
This is a cyclical conflict with unconventional aspects. As we have talked
about numerous times on the channel, it is a PR campaign with violence. Somebody
is about to get really bad PR as this case moves forward. Doesn't matter how
it gets decided.
So there's that.
And it might shift the type of fighting a little bit.
It may have some positive impacts in that way.
But if you are hoping that this is just going to stop the fighting or make everything the
way you want it to be, you're in for a rude awakening.
The ICJ doesn't have a whole lot of power.
this case may do something else. It may inform an actual ICC case. The ICC does
have an enforcement mechanism. They can arrest people. The way you need to look
at the two, ICJ is for countries, ICC is for people. That's who the defendants are.
Now, as far as the ICC is concerned, my understanding is that they are investigating, and they're
looking at three things to my knowledge, two of which are pretty much confirmed.
The third, I am fairly certain that they are looking into, but I don't have a public statement
about it.
The first is what happened on October 7th, because while there might be a right to resistance
or something like that, there are still rules. Odds are those rules were
violated. It does seem that they are looking into the hindrance of aid, there
there's a term for it, but basically blocking aid is a crime and there's been
a kind of semi-public warning about that. And then the other thing that I think
will probably come into play is that Israel is going to have to explain why
every mosque, hospital, piece of civilian infrastructure that was hit, why it lost
its protected status. There are rules and a lot of buildings they're protected
You're not allowed to hit them.
And if you do, you have to show cause.
Generally speaking, a tunnel isn't enough.
You can talk to any American serviceman you know,
service person that you know, that was in Iraq.
That's the one that people are waiting for.
And those people who are looking at the ICJ case and they're like, yes, this is going
to do what we want.
It's not.
The ICC one, when that comes about, that might do what you want.
That's also probably at least a year away.
law is not evenly applied and the wills of international law, they move slower
than the special counsel's office. It's going to take a lot of time. Again, this
is something that we discussed more than a month ago. So understand the ICJ case.
it's not irrelevant. It's going to have impacts, but what I think a lot of people
are hoping is going to occur, it's not in the cards. It's not even
what that court does really. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}