---
title: Let's talk about kids, food, states choosing not to help, and the USDA....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=qy14OV6oQEY) |
| Published | 2024/01/12 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- USDA program to feed 21 million kids in the U.S. over the summer.
- Eligibility: below 185% of the poverty level and living in a specific state.
- Benefits include $40 per month per kid.
- Kids from low-income families struggle to eat during the summer when school meals aren't available.
- Not all states opted into the program; Alabama, Alaska, Florida, Georgia, and more chose not to participate.
- Reasons for not opting in include concerns about big government and paperwork.
- Summer Food Services Program is available in most places for kids to access meals at specific locations.
- States not participating can join in 2025 with a change of heart.
- Some states appear to prioritize bureaucracy over feeding hungry children.
- The program aims to address food insecurity among children during the summer months.

### Quotes

- "If the problem is a lack of money, yeah, it does help."
- "Seems like there's a little bit of a trend there."
- "States not participating can join in 2025 with a change of heart."
- "It's probably just the general trend of wanting to kick down at other people."
- "Now, for those that live in states that don't care about the children of their state..."

### Oneliner

USDA program to feed 21 million kids over summer faces resistance in some states, where bureaucracy seems prioritized over child hunger relief.

### Audience

Advocates for child welfare

### On-the-ground actions from transcript

- Support local summer food programs by volunteering or donating (implied)
- Advocate for expanding food assistance programs in your state (implied)

### Whats missing in summary

The full transcript provides detailed insights into the challenges faced in ensuring food security for vulnerable children, including state-level decision-making impacting access to vital nutrition programs.

### Tags

#ChildHunger #FoodInsecurity #USDAProgram #SummerMeals #StateResponsibility


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about a new program
from the USDA, well, I guess it's not new,
it's permanent now, where it was just kind of
in testing before, that will be available
over the summer and is expected to feed
around 21 million kids in the U.S., help feed them.
So to be eligible, it looks like you have to be
or below 185% of the poverty level and you have to live in a state that it's
available in because not all states have it. There are 35 states that'll have it,
five territories, and four tribes. The benefits are $40 per month per kid over
the summer. The reason this exists is because a lot of kids who come from
families that don't have a lot of money, they can eat at school. Those things
aren't necessarily available during the summer. Now I know the obvious question
is what about those other states? Why isn't it available there? Oh, it
could have been. The state governments there decided to not opt in to it. The
state government chose to let kids go hungry or eat ramen, I guess. Those states
are Alabama, Alaska, Florida, Georgia, Iowa, Louisiana, Mississippi, Nebraska,
Oklahoma, South Carolina, South Dakota, Texas, Vermont, and Wyoming. Man, seems
like there's a little bit of a trend there.
Now, these states, they gave their reasons for not opting in.
Most of them do not hold up to scrutiny.
It's more of the same of, well, we don't think that this will really solve the problem.
No, I mean, if people don't have money, giving them money to buy food.
I mean, that does kind of solve the problem.
Sometimes people, they throw that out there.
They're like, well, throwing money at the problem
doesn't help.
If the problem is a lack of money, yeah, it does help.
But most of them centered on some idea of big government
or there's too much paperwork or something like that.
Those were the public reasons for it.
it's probably just the general trend of wanting to kick down at other people.
Now, for those that live in states that don't care about the children of their
state,
there is something called the Summer Food Services Program
that is available in most places,
where the kids can go to a specific location to eat.
something else and my understanding is that that's available everywhere so there
is that because I mean you know it's like way less cumbersome you know when
you're talking about all the paperwork and everything it's way less cumbersome
to get your kid and drive them somewhere every day than it is to have a card that
allows you to to get more food, right. These states that have not opted in so
far will be able to do so if there is a change of heart in 2025. Anyway, it's just
a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}