---
title: Let's talk about Louisiana maps being approved....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=JjGqFcwRauY) |
| Published | 2024/01/24 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Louisiana's new district map creating a majority-black district was approved by the Senate, House, and Governor without resistance.
- Despite facing a guaranteed loss of a Republican seat in the House, the new district map was signed without controversy.
- Louisiana Republicans chose not to prolong the redistricting battle or create chaos, opting for a smooth process.
- The final step involves the plaintiffs reviewing the map, which is expected to be approved, bringing closure to the issue.
- Concerns loom over maintaining a Republican majority in the House due to the impending loss of a seat.
- Current Speaker of the House and the incumbent in the district, Graves, are unhappy about the changes.
- The process was completed just six days before a potential court intervention, showing timely resolution and adherence to the law.

### Quotes

- "It's so weird to see things like function the way they're supposed to."
- "If your policies guarantee losing a district due to demographic shifts, maybe it's the policies that are the problem."
- "They could have turned this into a giant thing, forced a trial. It could have been a giant mess, and they didn't."
- "The inability to actually do anything with the majority that they have is causing a lot of concern for Republicans."
- "They did it with like six days to spare before the court said they were going to step in."

### Oneliner

Louisiana's smooth approval of a new majority-black district map raises concerns for Republicans facing an imminent loss of a seat.

### Audience

Louisiana residents

### On-the-ground actions from transcript

- Contact the plaintiffs involved in reviewing the new district map to ensure fair representation (implied)

### Whats missing in summary

Insights into the potential impacts on future elections and representation in Louisiana. 

### Tags

#Louisiana #Redistricting #RepublicanParty #House #VotingRights #Demographics


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're going to talk about Louisiana,
that new map, and what happened,
because as we talked about a few days ago,
the Senate advanced the new map,
which would create a new district
that would be majority black.
So it would be the second one,
which means it would roughly reflect
the actual demographics of the state of Louisiana.
Surprise.
Okay, so it made it out of the Senate.
What happened? It went to the House, and the Republican-controlled House actually approved it.
Then it went to the Governor, and the Republican Governor actually just signed it.
It's so weird to see things like function the way they're supposed to, I don't really know what to say.
Because if you've been following the redistricting battles in other states,
Normally, once the courts say, okay, well, you have to do this, it turns into a giant thing with the legislature trying
to find some way to get around what the court said.  In this case, they were just like, okay, you know, that actually
kind of seems okay.  That seems right.
Even though they're pretty much guaranteed to lose a seat in the House, the Republican Party is basically guaranteed to
lose a seat.  it will become a Democratic seat.
Again, I would suggest that if your policies
are so one-sided that a demographic shift,
like if they change in the demographics of a district,
guarantee that you're going to lose that district,
maybe it's the policies that are the problem.
But at this point in time,
I don't want to come down on Louisiana Republicans too hard because, again, they could have drugged
this out.
They could have turned this into a giant thing, forced a trial.
It could have been a giant mess, and they didn't.
The only step left when it comes to getting this map into use, basically, the plaintiffs,
the people who originally were like, hey, this violates the Voting Rights Act.
They have to review it.
looks like they would approve it, then it goes back to the court and then that's it.
This is over.
So the people who are upset about it the most are the current Speaker of the House.
Johnson is not happy because, again, it is pretty much guaranteed that they're going
to lose a seat and that majority is already razor thin and the inability to actually do
anything with the majority that they have is, it's causing a lot of concern for Republicans
in the House because they're worried about maintaining the majority, which yeah, it doesn't
seem likely to be honest.
And then of course, the person who is in the district in Louisiana right now, who will
probably lose their seat. They're not happy about it. Graves. So that's where everything
is right now. That looks to be over. So there, we have conclusion on one of them. We have
a little bit of closure. And they did it, I think, with like six days to spare before
the court said they were going to step in. Anyway, it's just a thought. Y'all have
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}