---
title: Let's talk about the Missouri GOP and a new rule....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=8bqrVG41BjI) |
| Published | 2024/01/26 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Missouri Senate rules change proposed due to confrontations within the Republican Party.
- Proposed rule change involving dueling as a satirical commentary on lack of civility.
- Duel challenge can be issued if a senator's honor is impugned beyond repair.
- Details of the duel process include choice of weapons and terms agreed upon by both senators.
- Beau questions the practicality and tradition of dueling inside the Senate.
- Suggests dueling should occur outside at dawn with an option for the seconds to negotiate a truce.
- Acknowledges that while dueling is dumb and immature, it could potentially shift rhetoric in the Republican Party.
- Points out the historical context of dueling and how it was once a common practice in the US.
- Beau believes the proposed dueling rule is a sharp rhetorical device but does not endorse its actual implementation.
- Expresses concern about the violent rhetoric in politics and how this proposed rule change could potentially alter behaviors.

### Quotes

- "Yeah, the Republican party's doing great."
- "If this was actually the rule, I feel like that rhetoric would change real quick."
- "But I do think pointing out that those people most likely to use that kind of rhetoric would never do it themselves, I think there's some value in that."

### Oneliner

Missouri Senate proposed a satirical dueling rule to address GOP infighting, sparking debate on political civility and rhetoric.

### Audience

Politically engaged citizens

### On-the-ground actions from transcript

- Stay informed about political developments and proposed rule changes in your area (implied)
- Engage in constructive political discourse and advocate for civil interactions (implied)

### Whats missing in summary

The full transcript provides historical context on dueling and raises awareness about the potential impact of aggressive rhetoric in politics.

### Tags

#Missouri #Senate #RepublicanParty #PoliticalCivility #Rhetoric #Dueling


## Transcript
Well, howdy there, Internet people, it's Beau again.
So today we are going to talk about Missouri
and a potential change to the Senate rules there
and something that might reduce the amount
of confrontations that are occurring
within the Republican Party
because there's been a lot of that in Missouri.
There's been a lot of infighting there
in the GOP and somebody proposed a solution.
Now, I would like to think that this is commentary,
that this proposed rule change is in fact commentary
and it is, it's being a little over the top.
It's satirical.
It's meant to draw attention to a lack of civility
lack of civility in the Republican Party. So I'm just going to read the proposed
rule change. It would be rule 103. If a senator's honor is impugned by another
senator to the point that it is beyond repair, and in order for the offended
senator to gain satisfaction, such a senator may rectify the perceived insult
to the senator's honor by challenging the offending senator to a duel.
Yeah, the Republican party's doing great.
The trusted representative, known as the second of the offended senator,
shall send a written challenge to the offending senator.
The two senators shall agree to the terms of the duel, including choice of weapons,
which shall be witnessed and enforced by their representative seconds.
The duel shall take place in the well of the Senate at the hour of high noon on the date agreed to by the
parties to the duel."
Okay, I think we can all agree that duels are dumb and immature, sure.
But, I do have a question, a couple of suggestions when it comes to this, because while I am
fairly certain that this is commentary, that this is a Republican trying to, I would hope,
reduce the temperature when it comes to the rhetoric that is being used, I am kind of
of a stickler for tradition.
And there are a couple of things about this that,
I mean, why do you want to do it inside?
Doing it in the Senate?
Well, that's not a good idea at all.
There's probably a lot more fluid loss
than you're picturing.
You want to do this outside.
And unless you're filming a Western,
this is done at dawn, not at noon.
And you should probably put something in your rule
about the fact that it's the second's job
try to negotiate a truce. I mean remember historically most disputes die and no
one shoots. So yeah I mean I would like to think that this is entirely a joke
but it is Missouri so okay we're doing this. Yeah okay so this is where the
Republican Party is, and I would hope, again, I hope that this is just commentary.
This is a way to try to reduce the temperature.
And I think it's a good one, because the reality is there's a whole bunch of people in the
Republican Party and their rhetoric always tends to trend towards the violence.
But let's be real.
If this was actually the rule, I feel like that rhetoric would change real quick.
People might be more polite.
Obviously, it's not something you would endorse.
You don't endorse dueling, not in this day and age.
As a rhetorical device and as commentary, it's pretty sharp.
It definitely makes a point.
There are a whole lot of people in the Republican Party who are very quick to use that rhetoric.
But I'm willing to bet that if it was them standing on a hill at dawn or in the Senate
well at noon, I feel like the rhetoric might be different.
This country has a long history of people having to answer for their words.
I know for some people watching this, this may be a surprise.
But this actually wasn't uncommon.
Quite a few American figures engaged in this practice.
And again, I don't actually think
it should make a comeback.
But I do think pointing out that those people most likely
to use that kind of rhetoric would never do it themselves, I think there's some value
in that.
Because there are a whole lot of people using that rhetoric for political ends that could
cause real problems and get people hurt for votes.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}