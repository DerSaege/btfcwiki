---
title: Let's talk about Biden, LNG, and next week's news....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=DxNI1CCVkNY) |
| Published | 2024/01/26 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Biden administration announced a pause in permitting for exporting LNG to reestablish criteria for measuring climate change impact.
- This pause is significant for those concerned about the environment and looking to transition away from dirty energy.
- Biden views the climate crisis as an existential threat and criticizes Republicans for denying its urgency.
- The process to determine the impact and gauge projects put on pause will be lengthy.
- Republicans are expected to push for LNG exports, tying it to Putin and national security concerns.
- The White House has allowed exemptions for immediate national security emergencies.
- The administration likely anticipated attacks from Republicans and included provisions for such emergencies.
- The process of pausing LNG exports is not expected to be swift and may not be resolved before the election.
- This issue will likely be a topic of debate in the coming week.

### Quotes

- "This pause is significant for those concerned about the environment and looking to transition away from dirty energy."
- "Biden views the climate crisis as an existential threat and criticizes Republicans for denying its urgency."
- "The White House has allowed exemptions for immediate national security emergencies."

### Oneliner

The Biden administration's pause on LNG exports for climate impact assessment faces Republican criticism tied to national security, with exemptions for emergencies.

### Audience

Policy advocates

### On-the-ground actions from transcript

- Advocate for sustainable energy alternatives (exemplified)
- Stay informed and engaged in the political discourse around climate policies (exemplified)
- Support measures that prioritize environmental impact assessments (exemplified)

### Whats missing in summary

The detailed implications of LNG exports on climate change and the potential consequences of not reevaluating their impact thoroughly.

### Tags

#LNG #BidenAdministration #ClimateChange #Environment #RepublicanCriticism


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Biden pauses,
LNG, that's liquefied natural gas, and exports.
We're gonna talk about what has occurred
and what's likely to occur because of this.
If you missed the news, don't know what's going on.
The Biden administration has announced a pause
when it comes to permitting for exporting LNG.
So the pause is to give the Department of Energy
time to reestablish some criteria
to measure the impact when it comes to climate change, which
that seems kind of important when you're talking
about something like this.
This is a huge win for, well, I mean
for anybody who is even remotely concerned about the environment, honestly, but for those
who are in particular looking to speed transitions away from dirty energy, this is a big deal.
This is a big win for them.
The administration, let's see, Biden said, this pause on new LNG approvals sees the climate
crisis for what it is, the existential threat of our time.
And then he goes on to basically say that the Republicans who are constantly wanting
to reinvest in infrastructure for dirty energy, that they willfully deny the urgency of the
climate crisis.
Kind of a nice statement.
So what happens next?
This is going to be a lengthy process.
It's going to take time for DOE to come up with some kind of metric for determining the
impact and then to actually gauge the impact of the various projects that are going to
be put on pause.
What else is going to happen?
Republicans are going to come out from everywhere, and it's going to be the same Republicans
that have been telling you for months that Putin is not a threat and that what's going
on in Ukraine doesn't matter. It's not really a security issue. They are now
going to come out and say that if we don't let these go through right now, if
we pause these exports, that's going to strengthen Putin and they're going to
make it seem like it's a really big deal because the fossil fuel companies that
have them in their pocket, they're going to push them out there to do that. You
will see people conduct an immediate about-face on this.
Not enough to actually change their opinion, they're just going to lie to their base.
But it does appear that the White House actually kind of anticipated that because buried in
one of these statements says that when it comes to unanticipated and immediate national
security emergencies, well, exemptions can be made.
Meaning that if Putin was to flex his muscles on this, Europe would still be able to get
what they need.
So my guess, because that's kind of buried in there, the White House is going to wait
for Republicans to come out and make those attacks and say that this is going to undermine
security.
remind everybody that this part's there. This is good news all around. It's done
pretty well. It would be great if the process for, if the process of this pause
was a speedy thing. It's not going to be. I would be surprised if this was
resolved before the election. So that's where it's at. This will probably
become an issue, something that's getting discussed next week. Anyway, it's just a
thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}