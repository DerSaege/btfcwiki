---
title: The Roads to Disaster Preparedness
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=MM-Qol7wzv4) |
| Published | 2024/01/11 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the topic of disaster preparedness in the United States after receiving questions about dealing with tornadoes following recent tornadoes in the area.
- Explains the difference between a tornado watch (looking for a tornado) and a warning (one has been seen).
- Advises on seeking shelter in a basement, cellar, shelter, or panic room during a tornado, or a sturdy interior room with no windows on the first floor if in a place like Florida.
- Contradicts previous advice on seeking shelter under an underpass during a tornado, now recommending a low, flat location.
- Emphasizes the importance of the structure of the shelter during a tornado rather than its materials.
- Warns against the dangers of floodwaters post-hurricane, including contamination, bacteria, and unseen hazards.
- Cautions against using a chainsaw after a storm due to the high risk of injury from twisted debris.
- Advises against running a generator indoors due to harmful emissions.
- Provides tips for earthquake preparedness, including securing furniture and knowing how to turn off gas, water, and electricity.
- Recommends finding high ground during floods and having a plan to escape if trapped in an attic.
- Shares advice on staying warm during blizzards by creating a pillow fort in a smaller room with layers of insulation.

### Quotes

- "You need a disaster preparedness kit, you need an emergency kit, food, water, fire, shelter, first aid kit, which includes your meds, and a knife."
- "The first rule of rescue is to not create another victim."
- "Having the right information will make all the difference."

### Oneliner

Beau covers disaster preparedness essentials from tornadoes to blizzards, stressing the importance of proper knowledge and preparation for different calamities.

### Audience

Preparedness Seekers

### On-the-ground actions from transcript

- Contact volunteer firefighters in affected areas for aid group relief efforts (suggested)
- Create a disaster preparedness kit with essentials like food, water, fire supplies, and first aid items (exemplified)

### Whats missing in summary

The full transcript provides detailed guidance on disaster preparedness across various natural disasters, offering practical tips and debunking common misconceptions.

### Tags

#DisasterPreparedness #Tornadoes #Hurricanes #Earthquakes #Blizzards #CommunityAid


## Transcript
Well, howdy there, internet people, it's Bo again,
and welcome to the Roads with Bo.
Today, we are going to be going over the roads
to disaster preparedness, I guess,
because after the tornadoes here,
had a whole bunch of questions that came in
about how to deal with tornadoes.
So what we're gonna do is we're going to go through
the major types of disasters that we see often
in the United States, and just cover the bare minimum of what
you need to know about each type.
And just kind of run through the basics
so people have a better idea of what to do,
mainly because I think a lot of people
were surprised that tornadoes happened in Florida.
Tornadoes can happen literally anywhere. Even if you've never had a tornado in your area, it can occur there.
Starting with tornadoes,
the first thing to know is that a watch is
they're looking for a tornado. A warning means they've seen one. Either visual confirmation or
Or, instrumentation has picked one up.
There seems to be some confusion about that.
A watch, you're looking for one.
A warning, one has been seen.
So warning is worse than a watch.
OK.
So as soon as a watch happens, you
need to start figuring out where you're going to go.
Now, if you're at home, hopefully you already have your plan in place.
But if you're out and about, you have to figure out where you might go if it switches from
warning to watch, or from watch to warning, like immediately.
So what you're looking for, ideally, is a basement, a cellar, a shelter, or like a panic
room.
Now if you live in a place such as Florida where basements aren't really a thing, you
kind of have to do the best you can.
So what you're looking for then is a sturdy building and you're wanting an interior room
that is small with no windows on the first floor and you want to get in there, get low,
your, you know, arms and hands to protect your head and neck just like you learned in
school and wait and just wait it out.
There is, there is debate about brick and wood and all of that.
From the studies that I've looked at, there comes a point where it literally doesn't
matter anymore.
The key part is interior room, a small interior room,
with no windows and no exterior doors, obviously.
OK, if you are caught outside, and this surprised me,
I saw this when I went and looked something up,
because this is the exact opposite of what I was taught
when I was younger.
But according to all the government websites now,
If you are outside, you don't get under an underpass.
You don't get up in that little corner or a bridge or anything like that, which is what
I was taught when I was younger.
But apparently, I guess they have determined now that you are safer in a low, flat location
somewhere, not under the bridge.
Because that was on all of the websites for some reason now.
The other thing to remember is that it's not that the wind is blowing, it is what the
wind is blowing.
Tornadoes can happen anywhere, anytime of year.
Some areas are obviously more common and at certain times of the year they're more common.
But the weather conditions that cause them, they can occur at any time.
on to hurricanes. Step one, leave. Evacuate. Get out, okay? Get your plan together and
get out. And I say this as somebody who sheltered in place through like one of the worst hurricanes
in history, don't do that. Leave. If for whatever reason you can't, when it comes to flooding
and the surge and all of that stuff, you want high ground.
When it comes to the wind,
you want the same thing that you're looking for
for a tornado,
which is
first floor,
interior room.
So there's a conflict there.
And you have to weigh what is the bigger risk.
Now one thing that I want to point out
is that
from my experience with hurricanes, and I have a lot, most people are hurt after the
storm.
It's not during the storm that most people are hurt, they're hurt afterward.
Do not walk, swim, play, surf, drive through floodwaters, even if you've seen some idiot
on YouTube do it, don't do it yourself.
It's important to remember that first, floodwaters are dirty, they are nasty.
It's not just ocean water.
It has picked up a lot of stuff along the way to include sewage.
There's the risk of all kinds of bacteria, some of which can turn into real problems.
you can't actually see what's below you, so there you have the debris issue and
getting cut and getting hurt that way. There might be live wires. There's all
kinds of issues. So that's dangerous. But most people that I have seen that have
gotten hurt did so with a chainsaw. After a storm, people go out and they buy
chainsaws and they try to do a lot of work themselves. If you do not know how
to use a chainsaw after a hurricane is not the time to figure it out. Medical
infrastructure is already strained and it's going to take a while for them to
get to you and it isn't it isn't even people cutting themselves most times.
It's after a storm, a lot of the stuff that needs to be cut and cleared away,
it's twisted together. It's under tension. And when you cut into it and you release
that tension from one side, it's going somewhere. And when that tree starts to
move and that little six-inch nub that used to be a branch on the tree hits you
in the gut or the chest, you better hope that somebody can get you to functioning medical
infrastructure.
Most people I know were hurt cutting something and they weren't actually injured most times
by the blade, by the chain.
And just to prove a point, I have cut down hundreds of trees, probably 50 under tension.
And after Michael, there was one in my yard and it was two trees twisted together.
And I looked at it and looked at it and I couldn't figure out how to cut it without
sending one tree into the shop or sending the other tree into the dining room window.
My wife said I couldn't do that.
The only way I could figure out to drop them both in a place that didn't cause damage was
incredibly dangerous.
So of course, I walked out there with my chainsaw, sat it down, and called somebody who knew
more than I did.
And he came over and within, I don't know, 10 minutes, dropped them both right on top
of each other and did it safely.
So just be aware that that is incredibly dangerous.
The other way that people get hurt after hurricanes, and it is always sad, like just heart wrenching,
your generator goes outdoors.
generator goes outside. Do not run your generator in your house, period. The
the emissions from it, they have caused a number of very bad things to happen.
Okay, so earthquakes. The big advice is to prep your home ahead of time because
during a quake you want to get under some kind of furniture that is sturdy
Now, I saw some studies that kind of indicated that once a quake started, you just needed
to drop to the ground.
And if you couldn't like immediately get to a sturdy piece of furniture to hide under
or something like that, it kind of suggested that you just drop.
It seemed like your risk of injury might be greater while trying to move.
you need to be aware of furniture that can tip and fall or fixtures that can
fall on you, stuff like that. The other thing, and this is true of most
natural disasters, but it's incredibly important when it comes to large
earthquakes, know how to turn off your gas, your water, your electric. Okay
floods obviously same advice as above don't drive swim play drink walk
anything you don't know how deep it is you don't know how fast it's moving you
don't know what's in it you want to get to high ground if you have to go into an
attic for any reason make sure you take something to make a hole to get out do
Do not give first responders nightmares for the rest of their lives.
Make sure you find a way to get out.
If it was me, if the situation was at the point where I had to go into the attic, I
would make the hole before it was even necessary.
Because at that point, you know you're going to have to do repairs, just add it to the
list.
You don't want to be trapped in an attic with rising water.
Blizzards.
This was a big one.
This was one that I actually got a whole bunch of messages about after what happened in Texas.
So the power goes out, the temperature drops, and people are trying to keep warm.
Take a table, a dining room table, move it into a bedroom, a smaller room, and then turn
it into a pillow fort.
Blankets, everything, okay?
And that's where people hang out at, is under that.
It helps trap the body heat.
You put it in a smaller room, you keep that door closed, you're creating layers of insulation
between you and the outside.
And you focus on keeping that one area warm through body
heat, candles, safely, obviously.
Whatever you have, you focus it in that one area.
Don't try to use the whole house, just a very small area.
It's easier to keep a small area warm.
Okay, and I'll go ahead and say the line since we're talking about it.
You need a disaster preparedness kit, you need an emergency kit, food, water, fire,
shelter, first aid kit, which includes your meds, and a knife.
So there's a, I've done a video on this like a dozen times, but just a quick recap.
You need food, meaning something that doesn't require a lot to be prepared or it's all
self-contained. Water, and that could mean bottled water, it could mean the ability
to treat water, could be purifiers, whatever. Fire, fire includes flashlights
and obviously a lighter in case you need to start a fire. Shelter can be anything
from a full-blown tent to one of those little Mylar, you know, bivvy things, a first aid
kit is something I don't think you should skimp on.
That would be the one where you really put the money, because if you need it, you're
going to want to make sure you have everything that you need.
People have asked about that, you can go online and look up IFAK and get a good idea of what
needs to be in one, but keep in mind, you also have to know how to use it.
Make sure that you have your meds, any meds that you absolutely need.
Make sure that you have a week or two supply and then you need a knife.
Why?
Because you always need a knife.
Just trust me on that one.
One of the things that came up was people talking about what if you couldn't, I guess
they were looking for that cup that I had in the other video, I mean they couldn't
find it and they asked about using bleach.
The answer to that is yes.
I'm going to put a link down below that takes you through it because it's not, it isn't
just bleach.
It's got to be a certain kind of bleach, and it also has the charts, you know, how many
quarts to how many drops, you know, and you need to see that for yourself.
And then one of the most important things, make sure you've got a family communication
and evacuation plan, and you have copies of all of your documents and everything like
that, even if it's just on a thumb drive.
And that's just a real quick overview.
This is something that if you want to help, you have to realize that the first rule of
rescue is to not create another victim.
You need to be able to take care of yourself and be self-reliant, and then you can move
on to help.
Now as far as, you know, normally there's questions at the end of this.
only one that came in enough I think to really mention. People asked about, okay
well my group wants to help but we don't know anybody in this area. Talk to the
volunteer firefighters in that area. That's who you talk to. And it's no,
that's not a slight against like the paid firefighters, but generally speaking
from my experience, the volunteer firefighters are better plugged into the
community and they obviously understand people showing up to help. So if you're,
if you have a community network or you have an aid group that wants to do
relief and you don't really know where to go, that's a place to start because
they will always be able to direct you somewhere and from that point you may
find out about other stuff that you can do. So that would that would be my first
stop and that looks like it. So there's a little bit more information a little bit
more context and having the right information will make all the
difference.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}