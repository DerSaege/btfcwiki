---
title: Let's talk about Trump, China, and a report....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=CU5spQQVoF8) |
| Published | 2024/01/11 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Democratic congressional members released a report on Trump accepting money from foreign governments during his time in office, including $5.5 million from China for renting hotels and apartments.
- The report didn't receive much attention, with Trump dismissing the issue as insignificant on Fox News.
- Trump justified the payments from China by claiming he provided services, like lodging, in his hotels.
- Trump's nonchalant admission to accepting payments could spark controversy as it may have required congressional approval.
- Trump's casual attitude towards constitutional limitations on accepting gifts or payments hints at his expectation that his base will defend him.
- The report's lack of fanfare suggests it may escalate into a significant news story as the election campaigns progress.

### Quotes

- "I don't get $8 million for doing nothing."
- "The odds are that this report that kind of came out without a lot of fanfare is about to become something we're going to be hearing about a lot on the news."
- "He's assuming that his base is going to come out and defend him and say, you know, that part of the Constitution doesn't matter."

### Oneliner

Democratic report reveals Trump's acceptance of foreign money, especially $5.5 million from China, sparking potential controversy and overshadowing Hunter Biden.

### Audience

Voters, political analysts

### On-the-ground actions from transcript

- Stay informed on political developments and hold elected officials accountable (implied)

### Whats missing in summary

Further insights on the potential implications of Trump's acceptance of foreign payments and how it could affect his election campaign.

### Tags

#Trump #China #ForeignMoney #PoliticalCorruption #ElectionCampaigns


## Transcript
Well, howdy there, internet people, it's Bo again. So today we are going to talk about Trump and
China and a report and then a surprising development, not something I saw coming.
I've got to be honest, I really hope the rest of the day isn't as strange as the first two videos.
Okay, so if you remember recently congressional members with the Democratic Party,
they put out a report and it was talking about Trump's time in office and how he
accepted a large amount of money from foreign governments while he was in
office. I know that China, the Saudis, I'm sure there were more but I remember
China being like five and a half million dollars just from them and the idea was
that they were renting hotels and apartments and just paying him for that while he was
in office.
That may prove to be an issue and it certainly kind of dims a lot of the spotlight on Hunter
Biden just saying.
So the report came out and not a lot was said or done about it.
more Trump being Trump.
So he was on Fox News and he was kind of asked about it and he said, if I have a hotel and
somebody comes in from China, that's a small amount of money.
But I was doing services for them.
I mean, yeah, I think that's what people are afraid of.
People were staying in these massive hotels, these beautiful hotels, because I have the
best hotels and the best clubs.
I have great stuff, and they stay there, and they pay.
I don't get $8 million for doing nothing."
I mean, okay.
So he just, I mean, certainly seems like he just admitted that, but for the sake of propriety,
I will continue to call them allegations.
So it's worth noting this is not an area that I know a whole lot about because it doesn't
really get violated very often, but he might have needed congressional approval to accept
those payments, to be clear.
Putting it out and just kind of admitting to it is kind of becoming a Trump thing.
He's assuming that his base is going to come out and defend him and say, you know, that
part of the Constitution doesn't matter.
For the record, it's in the Constitution that can't really accept, you know, gifts or payments.
So there's that.
The odds are that this report that kind of came out without a lot of fanfare is about
to become something we're going to be hearing about a lot on the news.
The just open way that he was like, yeah, I was doing services for him.
And I'm sure that he meant, you know, providing lodging.
The phrasing of that was less than ideal.
But this will undoubtedly become yet another story and another thing that Trump will have
to deal with as the campaigns move forward.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}