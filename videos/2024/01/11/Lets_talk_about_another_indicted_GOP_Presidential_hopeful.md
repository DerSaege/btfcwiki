---
title: Let's talk about another indicted GOP Presidential hopeful....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=TijRlGU_NjM) |
| Published | 2024/01/11 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talking about a Republican presidential hopeful who encountered some legal troubles related to false tax returns.
- John Castro, the Republican candidate, made moves challenging Trump's ballot eligibility in multiple states.
- Castro expressed his desire to become the commissioner of the IRS under a different presidential candidate.
- He reportedly believed that Trump and government agencies were conspiring against him.
- Castro demanded Secret Service protection, fearing harm was imminent.
- Indicted on 33 counts linked to assisting in false tax returns preparation.
- Allegations involve an online tax preparation scheme where Castro promised larger refunds by splitting them.
- Amidst this scandal, previous odd behaviors like suspecting his car was bugged seem less surprising.
- Despite his poor show in the presidential race, Castro is one of two indicted Republican presidential candidates.
- Beau remarks on the unusual situation, suggesting this may garner significant news coverage soon.

### Quotes

- "He demanded Secret Service protection because he thought something bad was going to happen to him."
- "So there's that, I mean, I'm pretty sure that's a record, I don't think that's happened before."
- "Anyway, it's just a thought."

### Oneliner

Republican presidential hopeful faces indictment for aiding in false tax returns, while making bizarre demands and conspiracy claims, adding to the party's tumultuous landscape.

### Audience

Political observers

### On-the-ground actions from transcript

- Monitor news updates on this unfolding political scandal (implied).

### Whats missing in summary

Detailed analysis and context surrounding the Republican presidential hopeful's legal troubles and their potential impact on the party.

### Tags

#RepublicanParty #PoliticalScandal #FalseTaxReturns #Indictment #Election2024


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about yet another
Republican presidential hopeful
who has developed some entanglements of his own.
Not too long ago, we talked on the channel
about a Republican presidential hopeful
by the name of John Castro,
and how he was behind a lot of the challenges
to Trump's ballot eligibility in a whole bunch of states.
So, I mean, it was interesting to see a Republican
try to make those moves and, you know,
push Trump out that way.
But then it started to get a little weird.
I think first, he said that he really wasn't expecting to win, but he wanted to get 5 percent
of the vote so he could be the tax man.
He wanted to be, quote, commissioner of the IRS under a different presidential candidate.
And then there was some reporting that suggested he believed that Trump and various government
Agencies were out to get him and working in coordination.
And he demanded Secret Service protection
because he thought something bad was going to happen to him.
So anyway, he's been indicted on 33 counts
related to aiding in the preparation of false tax
returns.
Those are the allegations.
According to the reporting, he had an online tax preparation
thing going on.
And the allegations suggest that he got busted
by somebody undercover who he offered
to get a substantially larger refund for and then split it.
So, there's that, I mean, and after hearing that, the rest of it really doesn't necessarily
sound so weird.
I mean, maybe people were bugging his car.
So that's an interesting turn of developments, and one I have to admit I didn't see coming.
This is a person who really wasn't showing very well in the presidential run.
I think we've only talked about him the one time on the channel.
And yeah, I mean, so now there are two people under indictment running for president in
the Republican Party.
So there's that, I mean, I'm pretty sure that's a record, I don't think that's happened
before.
We'll wait and see how this plays out as more information comes out.
My understanding is that all of this transpired yesterday, so I would imagine this is going
to hit the news pretty soon.
And I'm not sure how much attention it's going to get because of some of the more interesting
statements that Castro has made over the last few months since we last talked about him.
But yeah, the Republican party's doing great.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}