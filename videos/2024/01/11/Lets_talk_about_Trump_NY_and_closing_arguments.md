---
title: Let's talk about Trump, NY, and closing arguments....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=C3Vv97CLVP4) |
| Published | 2024/01/11 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Several outlets are claiming that the judge didn't allow Trump to speak in his defense or provide a closing argument, but that's not accurate.
- The judge was actually going to allow Trump to deliver a closing argument with specific requirements.
- Trump wanted to deliver a campaign speech instead of a proper closing argument, which the judge did not allow.
- The judge required Trump's closing argument to focus on relevant material facts and the application of relevant law, not a rant.
- Trump's reluctance to adhere to the judge's requirements led to him avoiding giving a closing argument, similar to his past actions.
- Beau had his popcorn ready, anticipating Trump incriminating himself if he had given the closing argument.
- Trump's behavior of expecting to be above rules and laws is standard, and people should be accustomed to it.
- The closing arguments in this case won't impact the decision, as it's not a jury trial but based on the law.
- Trump's lack of conciseness and persuasiveness makes him unsuitable for delivering a proper closing argument.

### Quotes

- "The judge totally was going to allow him to deliver a closing argument."
- "It is worth noting that the people who are super mad and outraged about this have no reason to be."
- "Was Trump once again expecting to be above the rules, above the law, being allowed to do whatever he wants."
- "The closing arguments are not going to impact the decision."
- "A closing argument is supposed to be concise and persuasive. Two things that Trump really isn't."

### Oneliner

The judge was going to allow Trump to deliver a closing argument, but his refusal to follow the specified requirements led to him avoiding it, showing his standard behavior of expecting to be above rules and laws.

### Audience

Legal observers

### On-the-ground actions from transcript

- Prepare for legal procedures in a concise and persuasive manner (implied)

### Whats missing in summary

The nuances and detailed analysis of Trump's behavior and the judge's requirements may be better understood by watching the full transcript.


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about Trump in New York
and closing arguments and all of that stuff.
Because there's a bunch of outlets right now
that are running with headlines like,
Judge doesn't allow Trump to speak in his defense.
Judge doesn't allow Trump to provide closing argument.
judge that none of that's what happened. Let's be super clear on something. The
judge totally was going to allow him to deliver a closing argument. The judge
just said it had to be a closing argument. He said that the statements had
to be quote commentary on the relevant material facts that are in evidence and
the application of the relevant law to those facts. You know, it had to be a
closing argument. Said that he couldn't deliver a campaign speech. Quote. And
Trump said, but I don't wanna, because that's not what Trump wanted to do. Trump
wanted to get up there and rant and rave like an old man yelling at clouds. That's
not what a closing argument is. The judge did not deny his request. The judge was
going to allow him to do it. He just had to actually give a closing argument, not
some rant. It is worth noting that the people who are super mad and
outraged about this have no reason to be because the reality is this is just like
Trump the last time when he said he was going to testify again, and then backed out at the
last minute.
This is Trump finding a way to get out of his rhetoric.
Or he really did want to go in and just rant and rave in hopes of catching some headlines
or something.
But those people who are mad about this have no reason to be.
You know who has the right to be mad about it?
Me.
Because I already have my popcorn ordered, and I'm fairly certain that he would have
incriminated himself when he got up there. He would have said something he
shouldn't have. He was not outright denied that. It was more akin to, oh you
want to go to this fancy restaurant? Sure. Put on a tie? No. Well then you can't
come. It's that simple. Was Trump once again expecting to be above the rules,
above the law, being allowed to do whatever he wants. It's a standard thing
with him that people should kind of be accustomed to at this point. The reality
is the closing arguments are going to occur, the closing arguments are not
going to impact the decision. Remember this isn't a jury trial. It's going to
done completely by the law. It's not going to be based on any nice turn of
rhetoric. And let's be honest about one more thing. A closing argument is
supposed to be concise and persuasive. Two things that Trump really isn't.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}