---
title: Let's talk about Pennsylvania, rhetoric, and believing it....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=VkjEg67j9mg) |
| Published | 2024/01/31 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Pennsylvania and rhetoric are discussed, focusing on where the rhetoric comes from and who believes it.
- A man in Pennsylvania was arrested after making a video referencing the Biden regime, an army of immigrants, and a civil war.
- The idea of an invasion is fabricated to scare and manipulate people, as there is no actual invasion occurring.
- The Republican Party is voting against border closures because the fabricated invasion serves as a tool for manipulation.
- Politicians and pundits use inflammatory rhetoric to manipulate and control people, appealing to inflated patriotism or fear.
- Rich individuals historically use inflammatory rhetoric to manipulate and control the masses for their own benefit.
- Right-wing outlets create rhetoric to rile people up and instill fear, ultimately aiming to control and manipulate them.

### Quotes

- "There is no invasion. They made that up."
- "They played you for a fool."
- "They do not have your best interest at heart."
- "They don't believe the same things you do."
- "They don't care about you, and they never did."

### Oneliner

Beau breaks down the fabricated rhetoric surrounding Pennsylvania, exposing manipulative tactics used by politicians to control and scare people.

### Audience

Concerned citizens

### On-the-ground actions from transcript

- Fact-check political rhetoric (implied)
- Challenge fear-mongering narratives in your community (exemplified)

### Whats missing in summary

The full transcript provides deeper insights into the manipulation tactics used by politicians and the consequences of falling for fabricated rhetoric.


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk a little bit
about Pennsylvania and rhetoric.
Where that rhetoric comes from, why it gets used,
who believes it, who doesn't believe it.
We're gonna run through all of that
because it has apparently become incredibly important
to discuss it because there are large segments
of the American population who are apparently
just wholly incapable of understanding when a politician is playing them.
If you have no idea what I'm talking about, a man in Pennsylvania has been arrested
after he was found with his father, a federal employee,
and his father no longer had a head. The man made a video, posted it online,
I'm talking about the Biden regime and the army of immigrants, the invasion that was coming,
how things had gotten too woke, how he was involved in a civil war, a holy war and had
to earn his place in heaven. Man, that all sounds real familiar.
Here's the thing, and this is really important.
There is no invasion.
They made that up.
That's why the Republican Party is currently in the process of voting down the ability
to close the border, because it's not actually an invasion.
It's a tool to scare you and manipulate you and play you.
That's it.
If you are truly concerned and you're scared about what's going on at the border, they
have duped you.
They played you for a fool.
They're blocking the solution.
Their solution, mind you, it's what they wanted to do.
And then when the Democratic Party is like, fine, we'll do it, they're like, oh no, no,
you can't do that because if we do that, then we won't have any way to scare, you know,
the Hee Haw Hezbollah and keep them all riled up.
They played you for a fool.
You never noticed that it's never them.
The politicians and pundits that put out this rhetoric, it's never them.
The people with all that power, all the resources, all that money, it's never them, right?
They don't take action.
It's some random person who was given an excuse to be their worst.
Who was given an excuse to hide the failures in their lives.
who it is. They don't believe it. The people who use this rhetoric, they don't
believe it. It's just a way to control and manipulate you and play you. It's the
same story throughout history. Rich people using inflammatory rhetoric to
appeal to people who are having an inflated sense of patriotism or who are
easily scared and get them riled up to do their bidding and then they pay the
consequences.
We talked about how that border stunt that's being put on down there, all the
right-wing outlets. Oh, he's defying! No, he's not. He's doing exactly what the
Supreme Court allowed him to do and nothing more. The rhetoric is put out
there. It's to scare people. It's to rile them up. He's not being defiant. He's
doing exactly what he's told, like a good little governor. But that rhetoric, it's
It's there to rile you up.
So you're easy to control.
You're easy to manipulate.
They do not have your best interest at heart.
Not economically, not socially, certainly not personally.
They don't believe the same things you do.
They don't care about you, and they never did.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}