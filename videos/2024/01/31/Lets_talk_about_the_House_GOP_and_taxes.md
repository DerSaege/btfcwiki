---
title: Let's talk about the House GOP and taxes....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Ne6yw6l_PU8) |
| Published | 2024/01/31 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Speaker of the House is leading a bipartisan effort on a tax bill that has support from both parties but faces opposition from within the Republican Party.
- The bill includes normal tax provisions, but some Republicans are upset about the lack of an increase in the salt deduction and the eligibility for the child tax credit without a social security number.
- The Speaker is using a procedural trick that will require two-thirds of the House to vote in favor, indicating that he can't pass it with just Republican votes.
- This tactic aims to show Republicans who won't fall in line that they are irrelevant.
- The Speaker's strategy seems to be less about targeting the far-right factions and more about those who won't toe the party line.
- By using this maneuver, the Speaker can render certain Republican caucuses irrelevant and demonstrate that he can push legislation through when needed.
- It's a tactic to make non-compliant Republicans realize their diminished importance within the party.

### Quotes

- "It does appear that the current speaker is less concerned about getting his fellow Republicans to do what they're told than McCarthy was."
- "Republicans who aren't towing the party line, well, they don't matter so much."
- "Showing that this procedure could be used for something like this, it also gives him cover when he is intentionally rendering the MAGA caucus irrelevant."

### Oneliner

The Speaker's bipartisan tax bill effort aims to sideline non-compliant Republicans, rendering them irrelevant in a strategic maneuver.

### Audience

Legislative observers

### On-the-ground actions from transcript

- Contact your representatives to voice support or opposition to the tax bill (suggested)
- Stay informed about the developments in the House of Representatives regarding this bill (suggested)

### Whats missing in summary

Insights on the potential impacts of sidelining non-compliant Republicans within the party dynamics.

### Tags

#TaxBill #RepublicanParty #BipartisanEffort #HouseOfRepresentatives #SpeakerOfTheHouse


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about
the U.S. House of Representatives and the tax man
because the Speaker of the House is once again
engaging in a bipartisan effort
that will render some Republicans,
well, first it'll render them mad,
and then it will render them irrelevant.
Okay, so what's going on?
there's a tax bill in the House.
It is bipartisan and by that means there are large groups
of both parties that support it.
There is also opposition from within the Republican Party.
There is no way that the Speaker can pass this
with just Republican votes.
So using a procedural trick that will require two thirds
of the House to vote in favor of it,
And once again, telling those Republican upstarts that won't fall in line, you don't matter.
Okay, so what's in it and what's the problem?
Basically it's a normal tax bill.
There are two things that Republicans have taken issue with.
Some, particularly Republicans that are in blue states, they're upset that there's not
an increase in the salt deduction, salt deduction being state and local tax.
They wanted that because generally speaking, blue states have higher taxes and they could
take that back and be like, look, we got your tax break.
That's not really in there.
And then the other thing is a lot of Republicans are upset because it seems as though the child
tax credit can be done without a social security number and they're saying that
that would mean that you know undocumented workers could get a child
tax credit which is weird because that almost seems like a Republican
admission that they pay taxes which I've it's because I feel like I've been told
like forever that they don't. Okay, so what happens from here? This is probably
gonna go through. They're upset Republicans, but so far the speaker has
used this maneuver a couple of times. This doesn't actually really seem super
aimed at the far-right, sedition, caucus, MAGA, caucus, freedom, caucus style of
people. This just seems to be people who won't fall in line, really, more than
anything. It does appear that the current speaker is less concerned about getting
his fellow Republicans to do what they're told than McCarthy was.
Showing that this procedure could be used for something like this, it also gives him
cover when he is intentionally rendering the MAGA caucus irrelevant, that far-right caucus.
Because this, he can use this to say, no, I do this anytime we hit a wall.
It's not just to get, you know, to make them look bad on Twitter.
It's not just to make them irrelevant.
I use this technique whenever I need to, whenever we need to get something through
to help the country or whatever, however he's going to frame it. Provides a little
bit of cover, but it's the exact same technique, it's the same strategy, and it's
having the same effect. Republicans who aren't towing the party line, well, they
don't matter so much. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}