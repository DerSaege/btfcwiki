---
title: Let's talk about Taylor Swift, numbers, dads, and the NFL....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=rYuGnwk6m6I) |
| Published | 2024/01/31 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The right wing in the United States has launched a "holy war" against Taylor Swift, despite not being able to articulate a specific reason for their disdain.
- Taylor Swift's presence at a Chiefs game resulted in the highest ratings of the week and the second-highest of the season.
- The game attracted the highest ratings among women aged 12 to 49, with a significant increase in viewership among 12 to 17-year-old girls on October 1st.
- These young girls are likely watching with their fathers, who are influenced to dislike Swift based on online opinions rather than actual reasons.
- Beau criticizes the individuals who allow internet strangers to dictate their opinions and drive a wedge between themselves and their daughters over cultural battles.
- He draws parallels to the Kavanaugh hearings, suggesting that blind opposition to someone based on external influence can damage relationships irreparably.
- Beau urges reconsideration for those who are willing to jeopardize their relationships over trivial matters like a celebrity's political stance.
- He condemns the notion of hating Taylor Swift simply because she encouraged people to register to vote, portraying blind obedience to online hate as damaging and unpatriotic.

### Quotes

- "If you find yourself opposing America's sweetheart because some loser you never met told you to, you might want to rethink things."
- "You have no reason to hate her. They told you to hate her and you obeyed like an obedient lackey."
- "But yeah, you're a real patriot."
- "That might be the single most pathetic thing I have ever heard of."
- "Y'all have a good day."

### Oneliner

The right wing's unwarranted "holy war" against Taylor Swift reveals blind obedience to online hate, damaging relationships and patriotism in the process.

### Audience

Parents, voters, Swift supporters

### On-the-ground actions from transcript

- Have genuine, open dialogues with your children about celebrities and political views (implied).
- Encourage critical thinking and independent opinions within your family (implied).
- Refrain from blindly following online hate and instead foster healthy, respectful communication with your loved ones (implied).

### Whats missing in summary

The full transcript expands on the damaging effects of blind online influence and the importance of critical thinking in familial relationships.

### Tags

#TaylorSwift #OnlineHate #Politics #Parenting #Relationships


## Transcript
Well, howdy there, Internet people, it's Bo again.
So today we are going to once again talk about Taylor Swift.
I feel like that's going to become an important part of this election season, an integral
facet of it.
We'll end up talking about her a lot, but I feel like in the process of that we are
going to miss the single most important part, the part that is actually more important than
the impacts on the election.
it's going to be ignored. So I want to go over that. But if you don't know what we're
talking about, if you missed the news, for whatever reason, the right wing in the United
States has decided to engage in, quote, a holy war against Taylor Swift. Now hopefully
they chose that terminology before what happened in Pennsylvania occurred.
But even though it is unlikely that most of them can articulate why, the specific reason
they are upset with Swift, they are.
I want to go through something and just point to some numbers.
That first game that she showed up at, that Chiefs game, it was the highest rated game
of the week.
Second highest rated game of the season.
I know, you're like, and? Yeah, I mean I get it. Ratings aren't everything. It was
also the highest rated among three different age brackets of women, from 12
to 49. 12 to 49. And that trend has remained. October 1st, the game on
October 1st that drew a whole lot of attention, 53% increase in 12 to 17 year
old women. Girls. Girls. You think they're normal NFL fans? Probably not, right?
They're watching to see Taylor Swift. Do you think they're watching by themselves?
else? Probably not. Willing to bet they're watching with dad. And what's dad doing? Trashing
their hero. Trashing the reason they're watching. Because somebody they never met told them
to. They're hating on somebody they've never met. To impress people on the
internet they'll never meet. Meanwhile they are driving a wedge between them
and their daughter. That is pathetic. That is absolutely pathetic. And they
realistically wouldn't be able to tell you why they dislike her all of a sudden.
other than somebody on the internet told them to. Their daughter is listening to
what they say and I guarantee you they don't like what they're hearing. This is
a lot like the Kavanaugh hearings, forever altering relationships. If you
find yourself opposing America's sweetheart because some loser you never
met told you to, you might want to rethink things. If you are willing to
drive a wedge between you and your daughter because of some culture war
nonsense, you might want to rethink things. Just general advice, if you happen
to organically end up sharing a hobby even if it's just for a short period of
time with one of your kids, you should probably take advantage of that in
every possible way. I certainly wouldn't subvert it and ruin it over a Twitter
talking point. That might be the single most pathetic thing I have ever heard of.
But hey, you know, you got to teach them lady people to stay in their place, right?
That's what this is about because she told people to register to vote.
That's when she became a target.
You have no reason to hate her.
They told you to hate her and you obeyed like an obedient lackey.
And it's damaging the relationship with your daughter.
But yeah, you're a real patriot.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}