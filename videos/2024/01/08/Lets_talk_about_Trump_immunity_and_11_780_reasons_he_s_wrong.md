---
title: Let's talk about Trump, immunity, and 11,780 reasons he's wrong....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=f5BPp8ycg-0) |
| Published | 2024/01/08 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former President Trump is claiming presidential immunity in criminal cases against him.
- Trump is set to attend a hearing in person to present his side of the argument.
- The key demonstration Trump needs to make is that he was operating within the confines of his duties as president.
- Trump's argument of looking for voter fraud as part of his duty doesn't hold up, especially with the "11,780 votes" call.
- Even if Trump was investigating voter fraud, once he started trying to alter the outcome, his argument falls flat.
- Trump's legal strategies are unique but unlikely to succeed, even if he shows up in person for the hearing.
- The appeals court is expected to reject Trump's argument that presidential immunity extends as far as he claims.
- If presidential immunity extended as Trump argues, Biden could cancel elections and rule forever.
- Overall, Trump's claims of immunity are facing significant challenges legally.

### Quotes

- "I got 11,780 reasons why that isn't going to fly."
- "The former president is clinging to some very let's just say unique legal strategies."
- "If it did extend that far, Biden could just cancel the elections and rule forever."
- "One of the things that he is claiming is presidential immunity."
- "He has put out a post that says, I wasn't campaigning."

### Oneliner

Former President Trump's shaky claim of presidential immunity faces legal hurdles, especially with attempts to alter election outcomes.

### Audience

Legal analysts, political observers.

### On-the-ground actions from transcript

- Attend hearings and trials to stay informed and engaged with legal proceedings (exemplified).
- Stay updated on legal developments related to former President Trump's cases (exemplified).

### Whats missing in summary

Insights into the potential implications of Trump's legal battle on future presidential accountability.

### Tags

#Trump #PresidentialImmunity #LegalChallenges #Hearing #ElectionFraud


## Transcript
Well, howdy there, internet people, let's bow again.
So today we are going to talk about former President Trump
and his immunity claims and his announcement.
He is apparently going to attend that hearing in person,
good for him, and we're just gonna kind of run through that
and talk about a new aspect of it
that he is trying to push forward
and go through that just a little bit.
Okay, so on the off chance that you're not aware
of what's going on, when it comes to the criminal cases
against former President Trump,
one of the things that he is claiming
is presidential immunity.
The idea that he can't be charged
because he was president and so on and so forth.
Generally speaking, this is widely viewed
as a losing argument.
But he and his team are going to be there and try
to present their side of it, fine.
So let's just say that we're going
to try to make this argument stick for a second.
The key thing that he would have to demonstrate
is that he was operating within the confines of his duties,
the outer perimeter of them, to use the actual language.
And he's trying to put forth the idea via his little Twitter clone that that's what he was doing.
He has put out a post that says,
I wasn't campaigning.
The election was long over.
I was looking for voter fraud and finding it, which is my obligation to do.
And otherwise, running, running, it says running twice on there for some reason, our country.
Yeah, I got 11,780 reasons why that isn't going to fly.
Y'all remember that, that call?
I just want 11,780 votes.
If you're just investigating, you're not trying to alter the outcome.
investigating, I mean realistically that still wouldn't be within the outer
perimeter, but let's just pretend that it is even if that part was true and that
is what he was doing, the moment he crossed over and started begging for
votes trying to alter the outcome that isn't what he was doing. The argument
isn't gonna fly. I mean it's really that simple. The former president is clinging
to some very let's just say unique legal strategies and they're probably not
going to pay off for him even if he shows up in person. The the appeals court
here I am fairly certain that they are going to say that presidential immunity
does not extend to the level that Trump is arguing that it does. And keep in mind
if it did extend that far, Biden could just cancel the elections and rule
forever because he can't be charged for it. Anyway, it's just a thought. Y'all have
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}