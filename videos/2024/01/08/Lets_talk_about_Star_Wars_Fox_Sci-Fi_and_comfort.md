---
title: Let's talk about Star Wars, Fox, Sci-Fi, and comfort....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=NhKvt-m5_fY) |
| Published | 2024/01/08 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the Star Wars and Fox controversy, focusing on a different aspect that is often overlooked.
- Advocating for the importance of making people uncomfortable through science fiction.
- Explaining how science fiction serves as a platform to address real-world issues and provoke critical thinking.
- Emphasizing that good science fiction challenges societal norms and biases.
- Drawing parallels between iconic sci-fi works like Star Wars, Star Trek, and Avatar in addressing societal issues.
- Asserting that science fiction should not just be about lasers and spacecraft, but about examining unresolved human issues.
- Arguing that discomfort in consuming science fiction is necessary to draw attention to underlying problems in society.
- Stating that creators who make audiences uncomfortable with their work are vital in running science fiction shows.
- Stressing the importance of incorporating moral lessons and impactful moments in science fiction storytelling.
- Concluding with a thought on the essence of science fiction and its purpose beyond commercial success.

### Quotes

- "Science fiction is supposed to make you feel uncomfortable."
- "Good science fiction will make you feel uncomfortable."
- "It's examining not just lasers and spacecraft. It's examining an issue that humanity really hasn't come to terms with yet."
- "You need the actual lesson. You need the moral."
- "You need that moment."

### Oneliner

Beau advocates for discomfort-inducing science fiction to challenge societal norms and provoke critical thinking, stressing the importance of incorporating moral lessons in storytelling.

### Audience

Science fiction enthusiasts

### On-the-ground actions from transcript

- Watch and support science fiction works that challenge societal norms and make you uncomfortable (implied).
- Engage in critical analysis and reflection on the societal themes presented in science fiction media (implied).

### Whats missing in summary

The full transcript provides a nuanced perspective on the purpose of science fiction beyond entertainment, encouraging viewers to embrace discomfort for meaningful reflection on societal issues.

### Tags

#ScienceFiction #SocialCritique #ChallengingNorms #MoralLessons #CriticalThinking


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we're going to talk about Star Wars.
We're going to talk about Star Wars and Fox.
But we're not going to talk about the part
that everybody's talking about, although it is funny
and I will mention it.
I want to talk about something else, because while not
as entertaining, it is probably more important.
It's a more important concept to grasp.
If you have no idea what I'm talking about,
the next Star Wars movie will be directed by a woman.
A woman who has a history of making documentaries
that are woke, for lack of a better word,
feminist documentaries.
And at some point, she said something to the effect of,
I like to make men feel uncomfortable, okay?
Of course, Fox has a problem with this.
Star Wars has gone woke,
and it's gonna be a big flop because of this,
and my kids won't watch it, and whatever.
And towards the end of the segment, one of them,
I am not joking, says, I don't watch Star Wars
because it's woke and everything.
That's why I'm a Trekkie, famously un-woke Star Trek,
the show that does social commentary in pretty much
every episode ever.
And that is definitely the part of this that is getting the most commentary, is that comment.
But I want to talk about making men feel uncomfortable, because I had an issue with that too.
Science fiction is supposed to make you feel uncomfortable.
Good science fiction will make you feel uncomfortable, but one of the great things
about science fiction is that you can pull problems that exist right now, put
them into another context, and talk about them openly and maybe people will get
the point. If you were alive back when the first Star Wars movies came out and
you didn't say, wait a second, are we the Empire? You missed the point. It's
supposed to make you feel uncomfortable. Good science fiction makes you feel
uncomfortable, it allows you to examine the problems that exist in society right
now by putting them in another world, by putting them in this other context where
it's free of all of the biases that exist because you don't have a
nationalistic view of the Empire. You don't have all of the propaganda of
your entire life weighing you down. You get to examine the dynamics free from
the bias, kind of like country red and country blue. It allows you to see it
more clearly and if the person telling the story does their job right, you're
supposed to feel uncomfortable because it should be identifying something that
is wrong in society. It's one of the greatest things about science fiction, and you can
go through pretty much any great piece of science fiction and you will find this to
be true. It's examining not just lasers and, you know, spacecraft. It's examining an issue
that humanity really hasn't come to terms with yet.
Avatar obviously had subtext.
Almost every episode of Star Trek,
the unwoke show with the trans character 30 years ago,
it's all there.
When you look at, even if you take it out of the space
type of science fiction, if you were to look at Fahrenheit 451. There's a lot of
people right now that should read that and feel uncomfortable.
Science fiction is supposed to upset you. It's supposed to draw your attention to something
that you may not recognize it immediately, but you're supposed to be uncomfortable with
some of the dynamics, and then maybe you can draw the parallel to what's happening in the real world.
So somebody who enjoys making people feel uncomfortable with their work, yeah that's who
should be running science fiction shows because that's what they should be doing. Otherwise
it's just another action movie. You need the actual lesson. You need the
moral. You need the moment where the the grizzled warrior grabs their friend and
says, Kurzon, let's Jadzia now. Jadzia, whatever. You need that moment. If you
don't have it, it doesn't do well. I mean, maybe it's commercially successful, but
that's not really what it's supposed to be about, right? In fact, I think there's
science fiction stuff about that too.
Anyway, it's just a thought.
So have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}