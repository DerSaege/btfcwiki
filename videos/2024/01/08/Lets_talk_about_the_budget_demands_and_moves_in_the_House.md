---
title: Let's talk about the budget, demands, and moves in the House....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=fLjdYtuEQl0) |
| Published | 2024/01/08 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Congressional leaders have agreed on top line spending between 1.59 trillion and 1.68 trillion.
- House Republicans are hinting at shutting down the government if they don't get what they want regarding the border.
- House Speaker may ignore the far-right faction and rely on democratic votes to pass spending bills.
- The performative Twitter faction relies on social media engagement and might be weakened if ignored in the House.
- The Speaker may consolidate power by rendering the far-right faction irrelevant.
- Allowing a government shutdown could backfire on Republicans, especially those in competitive districts.
- The dysfunctional House combined with Republican claims of shutting down the government could lead to fallout landing at their feet.
- Giving token concessions to the far-right may be a strategy to avoid a government shutdown.
- Negotiations and back-and-forth are expected in Capitol Hill to work out spending details, border issues, and aid for Ukraine.
- Prioritizing these issues should have been done before going on vacation.

### Quotes

- "Allowing a government shutdown during an election year may not play well, especially for Republicans in competitive districts."
- "The dysfunction in the House combined with them claiming it already and saying we're going to do this, it's going to land at their feet."
- "Maybe it's time for the Democratic Party to play hardball on this one."
- "Negotiations and back-and-forth are expected in Capitol Hill to work out spending details, border issues, and aid for Ukraine."
- "Prioritizing these issues should have been done before going on vacation."

### Oneliner

Congressional leaders agree on spending, but the threat of a government shutdown looms as House Republicans push for border demands.

### Audience

Political activists

### On-the-ground actions from transcript

- Contact your representatives to urge them to prioritize resolving spending details, border issues, and aid for Ukraine (suggested).
- Join advocacy groups pushing for responsible government actions (exemplified).

### Whats missing in summary

The full transcript provides a detailed analysis of the potential government shutdown situation due to disagreements over spending, border issues, and political tactics.


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about funding,
the US government shutdowns, all of that stuff,
and talk about what's going on
and what are some options that might happen in the future.
Okay, so if you've missed the news,
congressional leaders have agreed on top line spending.
This means the total amount that is going to be spent.
Depending on what Matthew used, it is somewhere between 1.59 trillion and 1.68 trillion in
there.
This was agreed upon by the Congressional leaders across the board.
House, Senate, Republican, Democrat, it's there.
Now the issue is going to be getting it through the House because even though congressional
leaders have agreed, you still have the rest of the representatives.
So even though this part is done and it's a big hurdle to overcome, it's not finished
yet.
There are still ways for this to go sideways.
The most likely, and the thing that has kind of already started, is House Republicans that
are part of the performative Twitter faction, the far right, the Freedom Caucus, people
like that.
They're kind of already hinting that, well, we're going to shut down the government if
we don't get what we want when it comes to the border or whatever.
The reality is, they want to talk about the border.
They don't actually want to fix the problem because they don't have any other policy
like at all.
They need that issue.
So I'm not sure how hard they'll really fight for it, fight for what they're saying they
want anyway.
Now you have a couple of options.
One is that the new Speaker of the House continues his trend of making them irrelevant.
done it before, and he might move to do it here as well. It'd be a really good spot to do it.
That is basically just ignoring them, bringing it to the floor, relying mostly on democratic votes
to get it through, and rendering that Twitter faction irrelevant. He has already done this a
couple of times. It's something we talked about on the channel that McCarthy should have done.
I think if McCarthy had done this, he'd still be speaker.
That faction is based on social media engagement.
They're based on performative nonsense.
If they are ignored in the House and rendered irrelevant in the House, well, they don't
get any clicks, they don't get any engagement, and it weakens them overall.
This is a possibility for the overwhelming majority of people watching this channel.
It's worth remembering that the new speaker, he might be mildly opposed to the far-right
MAGA faction, the Twitter faction, all of that.
But he's not your friend.
Keep that in mind.
Sometimes the enemy of your enemy is still your enemy.
Politically, he is not aligned with, I would say, 90% of the people watching this channel.
In this case, it's about consolidating power and restoring the position of the speaker.
That's why he would do it.
The other option is that he doesn't do that, and he just allows it to go ahead.
Maybe this time the Democratic Party should just let them.
Let them shut it down.
I'm sure shutting down the government during an election year, I'm sure that will play
really well, especially for the Republicans in competitive districts.
Because as soon as the Republicans who are running for re-election in those competitive
districts have to deal with the fallout from the economic devastation, they might change
their mind and remember the Republican majority in the House, oh, it's super slim.
It is razor thin.
It might be the time to allow them to do what they constantly say they're going to do.
They've claimed it.
They've said if we don't get X, Y, and Z, we're going to shut down the government.
won't be any confusion about who's responsible. Maybe it's time for the
Democratic Party to play hardball on this one. You want to do it? Go ahead. I do not
believe that most Americans would be happy with the outcome and I'm fairly
certain that the dysfunction in the House that has existed for quite some
time now, combined with them claiming it already and saying we're going to do this, it's going
to land at their feet.
The other option is to give them some token stuff because what they're asking for, what
most of them are asking for, is pretty hard-line stuff when it comes to the border that has
no chance of getting through the Senate. So they may give them some token stuff
to kick the can further down the road and because they're coming up against a
deadline and the Democratic Party doesn't want the government to shut down,
doesn't want economic devastation, but this is one of those cases where the
Republican Party is basically saying do what I say or you know look what you
made me do. I don't think that most Americans are going to look kindly on
that behavior, especially when it starts to impact their pocketbook right when
things are starting to get better. So that's where it's at. There's probably
going to be a lot of negotiating and back and forth this week up on Capitol
Hill. They have to work out the details of the spending because, again, top line
number that's just the total amount. So they have to work out the details of the
spending, probably have to work out something with the border, and then they
have to work out something with the aid for Ukraine. And that's where
they're at coming back. You know, maybe they should have taken care of some of
this before they went on vacation.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}