---
title: Let's talk about what Germany can teach us about China....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=3w9XAghE8Nw) |
| Published | 2024/01/02 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Compares US presidents' speeches on Germany to Chinese leader's speech on Taiwan.
- US presidents spoke of Germany's reunification as a historical inevitability.
- Kennedy, Truman, and Eisenhower all expressed belief in Germany's reunification.
- Kennedy's speech in Berlin in 1963 echoed the sentiment of reunification.
- Lyndon B. Johnson's speech in 1964 emphasized the inevitability of Germany's reunification.
- Germany was reunified in 1990, long after the speeches were made.
- Chinese leader recently stated that China will be reunified, specifically mentioning Taiwan.
- The Chinese leader's statement mirrors the historical inevitability rhetoric used by US presidents.
- Beau clarifies that the Chinese leader's statement is not a threat of war but a statement of future reunification.
- Emphasizes that such political positioning is common among world leaders.
- Beau reassures that the current situation with China is not an indication of imminent conflict.
- States that neither party, China nor the US, desires a war with each other.
- Beau underscores the importance of understanding political posturing in foreign policy.
- Concludes by stating that the Chinese leader's speech is just good public speaking and not a cause for concern.

### Quotes

- "This is not a threat of war."
- "It's just good public speaking."
- "Neither party actually wants to go to war with each other."

### Oneliner

US presidents' historical inevitability speeches on Germany compared to Chinese leader's statement on Taiwan show political posturing without indicating imminent conflict.

### Audience

Foreign policy observers

### On-the-ground actions from transcript

- Understand the nuances of political posturing in foreign policy (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of historical speeches by US presidents on Germany's reunification, comparing them to the recent statement by the Chinese leader on Taiwan, offering insights into political rhetoric and foreign policy dynamics.


## Transcript
Well, howdy there, internet people, it's Bill again.
So today, we are going to talk about US presidents
in Germany and what that can teach us about China
and their leader because something has occurred
and it has led to, let's just say, elevated heart rates.
And I think a little bit of context might help here.
So, I'm going to read a little bit of a speech first.
We go forward within the framework of our unalterable commitment to the defense of Europe
and to the reunification of Germany.
But under the leadership of President Truman and President Eisenhower and our late beloved
President Kennedy, America and Western Europe have achieved the strength and self-confidence
to follow a course based on hope rather than hostility, based on opportunity rather than
fear, and it is also our belief that wise and skillful development of relationships
with the nations of Eastern Europe can speed the day when Germany will be reunited."
All the presidents mentioned, they all spoke in the same way.
that the reunification of Germany was a historical inevitability. It was going to
happen. Can speed the day when Germany will be reunited. It's not a question of
if it's going to occur, but that it will. It's a matter of time, kind of on a long
enough timeline, we win kind of thing. It's a way of speaking that inspires
people. That's what it is. It's worth noting that Kennedy kind of did the same
thing in his famous speech in Berlin talking about people being reunited, not
quite as direct, but it's there and that was in 1963. This speech is Lyndon B.
Johnson, May 23, 1964. And it is worth remembering that Germany was reunified in 1990. A little bit
of time passed, right? Okay, so what has people's anxiety up? The Chinese leader gave a speech and
the official translation of what was said was, China will surely be reunified. Of course talking
about Taiwan. The literal translation, the reunification of the motherland, is a historical
inevitability. This is just, this is just good speaking. This isn't a threat of war.
This is just the Chinese leader saying, on a long enough timeline, this is going to occur.
There's not a threat here.
I understand that it is good for engagement to constantly, you know, dunecast, but that's
not what's here.
That's not what's in this statement here.
This is just the leader of China saying that this is going to occur on a long enough timeline.
That's it.
World leaders do it all the time.
I am certain that sometime in the last couple of years the leader of Cuba has said it.
It's something that occurs.
It is not a threat of war.
We are, again, we're in a near-peer contest with China.
It's pretty low-key as, you know, semi-Cold Wars go.
But it's important to remember that there's going to be a lot of political positioning
when it comes to foreign policy, and that neither party actually wants to go to war
with each other.
This is not something to be concerned about.
This is not a speech that indicates anything is imminent.
It's just stating a well-known and long-held goal of China and stating it as we're going
to win no matter what.
It's just good public speaking.
not it's not a threat of war so anyway it's just a thought y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}