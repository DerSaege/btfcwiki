---
title: Let's talk about gerrymandering, the GOP, and Wisconsin....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=pC6hsWd4rwM) |
| Published | 2024/01/02 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Wisconsin is gerrymandered, with extreme partisan gerrymanders in the United States.
- Justice Protasewicz pointed out the gerrymandering issue in Wisconsin during her campaign.
- The State Supreme Court confirmed that the maps in Wisconsin are gerrymandered.
- Republicans in Wisconsin are taking the gerrymandering case to the US Supreme Court.
- Politicians who support gerrymandering do not support representative democracy.
- Gerrymandering is about picking voters rather than voters choosing representation.
- Gerrymandering dilutes voting power and aims to choose easily manipulated voters.
- Gerrymandering hurts even those in the party it benefits by making their votes irrelevant.
- Gerrymandering creates safe districts where representatives don't have to listen to constituents.
- The Republican Party in Wisconsin fights to maintain gerrymandered maps to ignore voters' needs.

### Quotes

- "Politicians who support gerrymandering do not support representative democracy."
- "Gerrymandering is about picking voters rather than voters choosing representation."
- "Your vote is irrelevant even if you're in their party because they know that they packed enough easily manipulated voters in there to outnumber you."
- "This is how you get a government that is completely unresponsive to your needs because they don't have to be."
- "The obvious answer here is to vote out anybody of any party that supports gerrymandering."

### Oneliner

Wisconsin's extreme partisan gerrymandering undermines representative democracy, rendering voters irrelevant even within their own party.

### Audience

Voters, Activists, Community Members

### On-the-ground actions from transcript

- Vote out any politician, regardless of party, who supports gerrymandering (suggested).
- Stay informed about redistricting issues in your state and take action to ensure fair representation (implied).

### Whats missing in summary

Importance of being vigilant and proactive in addressing gerrymandering issues to safeguard representative democracy.

### Tags

#Wisconsin #Gerrymandering #RepresentativeDemocracy #VotingRights #PoliticalReform


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Wisconsin
and maps and redistricting
and something that everybody needs to know about it.
Even if the redistricting,
even if the maps are in your party's favor,
they're not in your favor.
They don't help you if your party has maps that benefit it.
that benefit it. It actually, it doesn't do well for you either. So we're going to
talk about that but we are going to go over the news first. Quick recap, not too
long ago a Supreme Court Justice in Wisconsin during her campaign, Justice
Protasewicz, she basically was like, hey the state's gerrymandered. Now she
didn't say it that bluntly, but I mean that was the general tone of it. Here's
the thing and the important part to understand about that, Wisconsin is
gerrymandered. If you go to Google and type in is Wisconsin gerrymandered, you
get Wisconsin is home to some of the most extreme partisan gerrymanders in
the United States. If you type in most gerrymandered state in US, you'll get a
blurb about Wisconsin. It's not actually up for debate as to whether or not the
maps in question were gerrymandered. They are, okay? So the Republicans in Wisconsin
were like, we're gonna impeach her if she takes that case, and then they decided
not to. The case went to the Supreme Court. Surprise! The State Supreme Court
decided the maps are gerrymandered because they are. Republicans have
decided that they're going to take it to the federal Supreme Court, to the US
Supreme Court. They're going to try to find federal issues and use those to
defend the gerrymander. Let's just start from the beginning. If a politician
supports gerrymandering. They do not support the Republic. They do not support
representative democracy. They don't want to represent you. They want to rule you.
They want to be in a position where they get to pick their voters rather than
their voters choosing representation. Let's start there. To me, that should be
disqualifying. But let's just process that real quick. There is no way
you can support gerrymandering and support the ideals of the United States.
Those two things don't go together. You don't believe in representation if you
actively try to create maps so people aren't represented, which is what
gerrymandering is. It's where they draw the maps so they can dilute the voting
power of different blocs and choose voters who are, well, honestly, more easily manipulated.
That's really what they're looking for.
Okay, but what happens if the gerrymander is in your favor?
You know, you are a card-carrying member of the Republican Party.
Why should you care?
Have you ever wondered why your Republican representative doesn't actually represent
you?
Because they don't have to.
They've created a district that is so safe that you don't matter.
Your vote as a Republican doesn't matter.
They don't have to listen to you.
Because they know the low-information Republicans, well they're going to vote for them anyway.
It doesn't help you if your party gerrymanders a state in its favor.
If the party that you typically support gerrymanders a state to help them, it hurts you because
they no longer have to listen to you.
They don't have to be responsive to you because your vote doesn't matter.
You have been put into a district with so many other people who are loyal to the party
that they don't have to listen to you.
If they lose voters because they're not representing them, they're ruling them, they know the low
information voters who just show up and vote are no matter what.
They know that they get elected anyway.
hurts you as well. So what we have is a Republican Party in Wisconsin that has
apparently decided tooth and nail they are going to fight for the ability
to preserve obviously gerrymandered maps so they can continue to ignore the
Republicans in Wisconsin, the Republican voters, they can continue to ignore them
and they don't have to worry about the Democratic voters because they don't
have any power. They've been diluted. This is how you get a state legislature that
doesn't do anything for you. This is how you get a government that is completely
unresponsive to your needs because they don't have to be. You can't hold them
accountable. Your vote is irrelevant even if you're in their party because
they know that they packed enough easily manipulated voters in there to
outnumber you and they're gonna fight tooth and nail to keep that. The obvious
This answer here is to vote out anybody of any party that supports gerrymandering.
Anyway it's just a thought.
So have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}