---
title: Let's talk about the USS Georgia and a mystery that isn't....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=THG0vruy5e4) |
| Published | 2024/01/19 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- USS Georgia, Ohio class sub, had the Blue Crew captain relieved of duty for "loss of confidence."
- Sheriff's Department in Camden County revealed the reason as a DUI and improper lane change.
- DUI is a significant issue in high-ranking positions of responsibility.
- Navy likely won't reveal the exact reason immediately, waiting for everything to play out.
- It's probable that the DUI incident is the simple explanation.
- People were expecting a more complex reason, but it seems straightforward.
- Having two crews means the impact on readiness is minimal.
- Losing one person from the crew can easily be managed within a military command structure.
- The incident isn't likely to have a major effect overall.
- The mystery surrounding the captain's relief of duty has been solved with a DUI being the probable cause.

### Quotes

- "DUI is a huge deal. Huge."
- "People were probably looking for something a little bit more Tom Clancy-ish."
- "They are certainly capable of losing one person and somebody else being sucked up into that position."

### Oneliner

The captain of the USS Georgia's Blue Crew was relieved of duty due to a DUI, revealing a straightforward explanation amidst expectations of a more complex mystery.

### Audience

Military personnel, Navy enthusiasts.

### On-the-ground actions from transcript

- Contact organizations supporting responsible drinking (implied).

### Whats missing in summary

More context on the specific impact within the USS Georgia crew and potential consequences for the captain.

### Tags

#USSGeorgia #Military #DUI #Navy #Responsibility


## Transcript
Well howdy there internet people, Led's Bo again. So today we are going to talk about the USS
Georgia and a mystery that wasn't really a mystery, but now it's solved. So we will go
over what happened, why people became interested in this development, and the final outcome of it.
Okay, so if you don't know, the USS Georgia is an Ohio class sub, which means it is a
guided missile sub that theoretically could also be used to insert special operations
forces.
The captain, the commanding officer of the Blue Crew, which subs like this have two crews,
one's at sea, one's at home.
So the blue crew was relieved of duty due to a, quote, loss of confidence.
This is boilerplate language from the military.
When somebody is relieved of command, this is what goes out, always.
But because of recent events and because of the interest due to the fact that it was an
Ohio class sub, people wanted to know why, what the loss of confidence was. And the
the expectation was that it was something just out there among people
who aren't familiar with how common that language is. Okay, it created a mystery
and now the mystery is solved. According to the Sheriff's Department in
in Camden County, which is where the sub hangs out, the reason for the loss of confidence
is most likely a DUI and improper lane change.
That's probably what it was.
Looks like on January 9th, the commander was picked up for that.
That is, when you are talking about positions of responsibility that high up, DUI is a huge
deal.
Huge.
And that is, that's more than likely what occurred.
There's probably no great mystery.
It's probably that simple.
The Navy, the Navy probably won't say for some time exactly why.
they generally let everything play out before they talk about it, but it does
appear that those two things are probably linked. So I know that that's
probably not the explanation that people, you know, were hoping for. They were
probably looking for something a little bit more, I don't know, Tom Clancy-ish or
something but it does appear that it's it's a DUI that that's the reason behind
it. Now again as far as the questions that will always follow something like
this up does it impact readiness no they have two crews they have two crews and
remember it's a military command they are certainly capable of losing one
person and somebody else being sucked up into that position that this is not
going to be a big deal nothing to leave sleep over. Anyway it's just a thought
Alright, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}