---
title: Let's talk about a surprising statement, Blinken, and Biden....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=VT5KD64QJYk) |
| Published | 2024/01/19 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Netanyahu made a statement indicating no Palestinian state, surprising many and prompting questions.
- Blinken, not Biden, was pushing for a pathway to a Palestinian state.
- Netanyahu's rejection of a Palestinian state means a recurring cycle of conflict.
- Genuine security for Israel involves a Palestinian state, acknowledged by many.
- The solution lies in an actual peace deal with financial aid for stability and reconstruction.
- A multinational force and economic support are vital components for lasting peace.
- The tough approach of dismantling Hamas without addressing the root cause leads to repeated conflicts.
- Israeli defense minister hints at the need for a comprehensive plan beyond military actions.
- Without a pathway to a state, conflict will persist with new groups emerging.
- A holistic approach involving a multinational force, a pathway to a state, and financial aid is necessary for lasting peace.

### Quotes

- "You want to throw some other stuff in there as well, that's great, that's fine, but that's the bare minimum that is going to lead to peace."
- "Without those things, all of this happens again."
- "If you don't include that, your dream of peace will turn into a nightmare."

### Oneliner

Netanyahu's rejection of a Palestinian state signals a recurring cycle of conflict without a comprehensive peace plan involving a multinational force, a pathway to statehood, and financial aid.

### Audience

Foreign policy advocates

### On-the-ground actions from transcript

- Advocate for a comprehensive peace plan involving a multinational force, a pathway to statehood, and financial aid (implied)
- Support initiatives that prioritize genuine security through lasting peace solutions (implied)

### Whats missing in summary

Insights on the necessity of addressing root causes and implementing comprehensive peace plans for sustainable peace in the region.

### Tags

#Netanyahu #PalestinianState #PeaceDeal #InternationalRelations #Security #ConflictResolution


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about a statement
that seemingly came out of nowhere,
even though it didn't.
And we're gonna talk about what that statement means,
because as soon as it came out,
a whole bunch of questions were prompted,
because it surprised people,
because it indicated that something was happening
that people didn't really believe was happening.
So we're going to run through that.
OK, if you have no idea what I'm talking about,
Netanyahu, from just out of the blue,
even though it wasn't from out of the blue, he came out
and said, I told our American friends
that there was not going to be a Palestinian state.
I put the brakes on that.
I'm paraphrasing.
That statement indicates that the United States
had been pushing for a Palestinian state.
That surprised a whole lot of people.
A whole bunch of questions came in about it,
and this one seems to sum it up best.
Does the statement from Netanyahu
mean that Biden was pushing for a pathway
to a Palestinian state this whole time,
and what does it mean that Netanyahu said no?
Okay. So the first part of that question, does the statement from Netanyahu mean
that Biden was pushing for a pathway to a Palestinian state this whole time?
No, no, that's not what it means.
That was Blinken.
Blinken was pushing for a pathway to a Palestinian state.
Biden was pushing for a Palestinian state as soon as the shooting stopped.
And that's what I understand to have been happening.
What does it mean that Netanyahu said no?
If the Israeli people co-sign that, and that's actually going to be the position, it means
that this is all going to happen again.
is right in that Israel cannot get quote genuine security without a Palestinian
state. He is right about that. As we have said numerous times on the channel there
is no military solution to this and this ends when it finally ends and not
pauses not a ceasefire but an actual peace deal. It will include a Palestinian
state or a viable pathway to one. It will include a multinational force that will
stand between the two sides for a generation and it will include a ton of
financial aid not just for reconstruction but also to get the economy
up and running to create stability. That's how this ends. If you were paying attention
to the foreign policy moves, the fact that this is what the US was pushing for shouldn't
really be a surprise but there are apparently a lot of people surprised by
it the the reality is if the position is held that a state a pathway to one is
just off the table everything happens again. Everything happens again. The get tough, tough
guy approach never works. It really doesn't. It's in all the manuals. And I do believe
that even the Israeli defense minister at this point is kind of publicly saying
hey you know you got to come up with a plan talking to Netanyahu. The the
statement that Netanyahu keeps making is that you know they're gonna be in there
until they can totally dismantle Hamas and then what? What about the group that
springs up after them because there will be one even if you're successful in that
which is unlikely but even if they're successful in that there's gonna be
another group. They need the pathway or they need the state otherwise this all
happens again and there's nothing that will change that. I know that there are
going to be people like, well, you can't trust them because it'll become a staging area.
That's what the multinational force is for.
If you're going to do this, it can't be one piece.
It can't just be a multinational force with no pathway to a state, no state.
It can't just be a pathway to a state or a state.
It can't just be the aid.
You have to have all three of those pieces.
You want to throw some other stuff in there as well, that's great, that's fine, but that's
the bare minimum that is going to lead to peace.
If you don't include that, your dream of peace will turn into a nightmare.
Without those things, all of this happens again.
light. It's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}