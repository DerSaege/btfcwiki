---
title: Let's talk about the Louisiana senate doing something right....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=nU3Nu_z7Isc) |
| Published | 2024/01/19 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Louisiana Senate approved a new congressional map to address Voting Rights Act violation.
- Previous map diluted black voting power with only one out of six districts being majority black.
- New map includes a second majority black district, better reflecting the state's demographics.
- Speaker of the House, Johnson, objected to the new map, viewing it as surrendering a Republican seat.
- Governor called a special session for House approval before January 30th deadline.
- If House doesn't approve by deadline, trial begins.
- Plaintiffs may object to the new map, potentially leading to court involvement.
- Louisiana appears to be moving in the right direction without constant court battles.
- Governor likely to approve the new map if passed by the House.
- Update to follow once the House decision is made.

### Quotes

- "Louisiana Senate approved a new congressional map to address Voting Rights Act violation."
- "Louisiana appears to be moving in the right direction without constant court battles."

### Oneliner

Louisiana Senate addressed Voting Rights Act violation by approving a new map, reflecting state demographics, despite objections, aiming to avoid trial.

### Audience

Louisiana residents, Voting Rights advocates.

### On-the-ground actions from transcript

- Contact local representatives to express support for the new map (suggested).
- Stay informed and engaged in the legislative process regarding redistricting (implied).

### Whats missing in summary

The full transcript provides in-depth details on Louisiana's redistricting process and the significance of fair representation.

### Tags

#Louisiana #Redistricting #VotingRightsAct #Representation #Governance


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Louisiana
and maps and courts and the Louisiana Senate
doing it right.
Surprise, I mean, I am a little bit, to be honest.
Okay, so if you have no idea what I'm talking about,
this story starts way back in June of 2022
when the current congressional map,
the current voting map in Louisiana was found to be in violation of the Voting
Rights Act. Now there were appeals and pauses and all kinds of things. When it
finally got to a conclusion in the Fifth Circuit, Louisiana was told they have
until January 30th to get a new map together. The Louisiana Senate just
approved a new map. So what was the problem with the old map? In Louisiana, roughly one out of three
people are black. As far as congressional districts, only one out of six was majority black. So it became
very apparent that there was an effort to dilute black voting power. The Louisiana Senate voted 27
to 11 to advance a new map that has a second majority black district, which
would mean one out of three. Not just did they do it, they did it over the
objections of the Speaker of the House, Johnson, the US Speaker of the House, who
is not involved in, you know, local Louisiana politics and state politics
there in theory, but he was really mad about this idea because it was, quote,
the surrender of a Republican seat. If the demographic makeup guarantees
that Republicans would lose the seat, perhaps there should be a policy
adjustment. Just saying, and this is also that reminder, just because Johnson is
doing something to kind of render the Twitter faction of the Republican Party
irrelevant, he's not your friend. Just keep that in mind. Okay, so what happens
from here. It goes to the House. Right now the governor called a special legislative
session to get this map together and it extends until January 23rd. So the idea is that the
House approves this map before then. If they don't have a new map by January 30th, a trial
starts and it turns into a whole thing. Hopefully the people there in the
legislature in Louisiana understand that nobody really wants that. So they approve
a new map and everything is good. The plaintiffs in the original case if they
have objections to the new map and based on what I saw I don't think they will
but if they do then the court will determine whether or not it is in
compliance with the Voting Rights Act. So out of all of the redistricting stuff
and the map battles that had been going on, of all places, Louisiana kind of
seems to be moving in the right direction without fighting the
courts every single step of the way. Which I'm gonna be honest, I was
surprised. I didn't really see that one coming. So we will do an update on this
as soon as the House decides what it is going to do. It seems pretty clear that
the governor intends on signing it, on approving it if it goes through. So once
that happens we'll provide another update but you can be hopeful that this
particular battle ends sometime pretty soon. Anyway, it's just a thought. Y'all
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}