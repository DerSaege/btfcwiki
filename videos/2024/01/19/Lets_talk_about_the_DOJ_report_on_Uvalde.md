---
title: Let's talk about the DOJ report on Uvalde....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=15qfAEAED8w) |
| Published | 2024/01/19 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau focuses on a critical part of a report concerning law enforcement tactics.
- Law enforcement's core objective must be to immediately neutralize the subject, above all else.
- The pressure on the subject must be consistently applied until the threat is eliminated.
- Beau stresses that accountability reports are aimed at preventing similar incidents in the future.
- He mentions the necessity of pressuring the subject even if it means risking lives, including officers'.
- Effective communication and transparency are secondary to the primary objective of neutralizing the threat.
- Beau underlines the importance of law enforcement internalizing and implementing this key recommendation.
- He refers to commentary on incidents like Nashville where proper tactics were not followed but lives were saved due to consistent pressure.
- The report's bluntness about prioritizing neutralization of the subject is a significant shift in perspective.
- Beau urges law enforcement to accept and apply this approach for real change to occur.

### Quotes

- "Your life doesn't matter, including officer safety, is subordinate to that."
- "Nobody cares whether those officers who show up to respond to this go home at the end of their shift."
- "The way of the warrior is the resolute acceptance of your own death."
- "If law enforcement accepts that, internalizes that, and actually applies it, nothing else in the report matters."
- "That's all that matters."

### Oneliner

Beau stresses the critical importance of law enforcement prioritizing immediate subject neutralization above all else, as stated bluntly in a recent report.

### Audience

Law enforcement personnel

### On-the-ground actions from transcript

- Internalize and implement the key recommendation of prioritizing neutralization of the subject immediately (implied).

### Whats missing in summary

The full transcript provides a detailed and thought-provoking analysis of the primary objective in law enforcement actions and the importance of consistent pressure to neutralize threats effectively.

### Tags

#LawEnforcement #AccountabilityReports #SubjectNeutralization #PoliceTactics #CriticalRecommendation


## Transcript
Well, howdy there internet people, it's Bob again
So today we are going to talk a little bit about that report and we're going to go through it
But we are going to really only talk about one part
Because that one part is what matters
There are tons of things in that report that can be discussed
But from my perspective
There's one part and if that is implemented, none of the other stuff matters
It matters for accountability because of what happened, but let's be real. These
reports are not about accountability. They're about stopping it from happening
again. If you have no idea what I'm talking about, the feds, they put out
their report on Yuvaldi. There is one thing that we have talked about on this
channel for literally half a decade when it comes to these types of situations, and that is that
as far as law enforcement is concerned, the only acceptable tactic is to press, to continually put
pressure on the subject, and you continually apply that pressure until it is no longer needed.
If you hit a locked door, you go through a window, you go through a ceiling, you go through a wall,
It doesn't matter. You don't stop. Yes, that means you are going to take fire, but if you are taking fire, that means
they are not looking at the kids.  And yes, that means that you may not make it. That's your job. Welcome to the
profession of arms.  I have said this for half a decade. It is in the report.
I've never seen it put this bluntly before, and this is the key recommendation.
Says that the core objective, quote, must be to immediately neutralize the subject.
Everything else, including officer safety, is subordinate to that objective.
If law enforcement accepts that, internalizes that, and actually applies it, nothing else
in the report matters.
Nothing else matters because it's not going to be questioned.
If that consistent pressure is applied, it saves lives.
People view the actions of law enforcement who arrived as doing the absolute best that
that they can because it is the absolute best that they can.
The communication, the transparency,
all of that stuff that's talked about in the report,
none of it matters.
If everybody who shows up knows that their main objective is
to apply pressure to the subject until they are no longer
a threat, communication issues, yeah, I mean,
they could be better, but they did it, right?
And you can say that that's some hypothetical, but it's not.
Look at all of the commentary about Nashville.
As far as the proper way to do things, yeah,
there were huge errors.
But everybody who had ever applied the proper way
came out to defend them, because it didn't matter
that they didn't do it in a pretty fashion.
They showed up.
They continually applied pressure,
and they saved lives, period.
That's all that matters.
and it's in this report and this is the first time I have ever seen it
stated this bluntly
your life doesn't matter
including officer safety
is subordinate to that
uh...
there are other things and i'm sure that it's going to be discussed
uh... i'm sure that that's
that the communication in the way that
parents were talked to all of that but that's going to be all over the news
this
is what matters.
This is the part that has to be accepted,
has to be applied by law enforcement
if you want the situation to change.
Nobody cares whether those officers who show up
to respond to this go home at the end of their shift.
Officer safety is subordinate to the objective
neutralizing the subject. It's taken half a decade to get to get that into a DOJ
report that bluntly. Now let's just hope that law enforcement understands it. You
want to be warriors, the warrior cop and all of that stuff. Remember that the way
way of the warrior is the resolute acceptance of your own death.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}