---
title: Let's talk about the pitch in Russia from the West....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=tvl4C2LFzt8) |
| Published | 2024/01/25 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Explains the controversy surrounding a video released by the CIA targeting Russian intelligence officers for defection.
- Questions the target audience of the video and suggests it may actually be aimed at wealthy Russian individuals living in the West.
- Posits that the recruitment strategy is geared towards developing agents of influence rather than flipping major Russian figures.
- Suggests that the recruited individuals could subtly advance Western interests and influence Russia from within.
- Describes how disaffected, bored, and disconnected individuals hiding from the war in Russia may be prime targets for recruitment.
- Speculates that the recruitment strategy is likely already successful in progress.

### Quotes

- "If they want to continue to enjoy it, well, I mean maybe they could just switch teams."
- "That's probably who they're going after."
- "It's not new. It's something that happens pretty often."
- "That's the pitch."
- "To me, this is a calling card trying to get out the information to those people who are upset with Daddy or Uncle Putin."

### Oneliner

Beau explains a controversial CIA video targeting Russian intelligence officers, suggesting it may actually be aimed at disaffected wealthy Russians in the West to develop agents of influence.

### Audience

Internet users

### On-the-ground actions from transcript

- Reach out to disaffected individuals in your community who may be vulnerable to recruitment strategies (implied)

### Whats missing in summary

Deep dive into the implications of recruitment strategies and potential impacts on international relations

### Tags

#CIA #Russia #Recruitment #AgentsOfInfluence #InternationalRelations


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about a video,
recruitment, Russia,
and how not everything is as it seems.
It's spy stuff, of course that's true.
We're gonna go over something that happened
because I got a message.
You never seem to miss a chance to make fun of the CIA.
and right now everybody's making fun of that video
to get Russian defectors. Did you miss it because you're sick
or dot dot dot. It's or dot dot dot.
If you don't know what this is about, the CIA put out a video
and sure in theory
it looks like a video that it's designed to
encourage mid-career
mid-level intelligence officers on the Russian side to defect, and they're
taking a lot of heat for it because, well, a lot of the imagery is from back
in the USSR, it's not imagery that would appeal to a mid-grade officer today.
And then a lot of the humor is coming from the fact that they put it on
Twitter. In fact, even the Russians are making fun of them for that. Like, you
know, our people use VK, this other social media platform. Yeah, no, I don't
actually think they messed up. The assumption here that people are
operating under, is that the intelligence agency told the truth about the target audience.
That the target audience was actually mid-level intel guys.
I don't think it was.
I mean, let's be honest.
With the current state of affairs there, mid-level intel people, are they even worth recruiting
anymore?
I mean, look at what they did with Ukraine.
the information they have is false because they made it up themselves to get money to
pay to groups that don't exist.
So how much access do they really have?
Are they really worth anything?
Probably not.
And then you do have the idea that it's geared towards older imagery.
Soviet imagery. And it's in the West. It's weird. It's almost like it's not actually
targeted at Russian intel officers. We have talked about various kinds of agent on this
channel. One that we talk about the most and is often the most undervalued are agents of
influence. And if you were to try to develop agents of influence right now
for Russia, where would you look? Well, I mean, if it was me, I would look for
people that were disconnected, disaffected, and easy to get to. Man, if
If only some of the wealthiest people in the country
sent their children outside of Russia to the West
to hide from Putin's war.
Man, if that had happened, that would be a really good group
to try to recruit.
And if you were to do that, you would probably
use a lot of the imagery that they grew up with as kids.
Which would be the old Soviet imagery.
And you would put it on Twitter because they're in the West using that.
And right now they are cut off from Russian influences.
They are disaffected because they're having to hide from the war.
They see their peers getting in trouble, they don't like the government, and they're bored.
Yeah that's prime recruitment right there.
That's probably who they're going after.
And my guess is that it's already working, and it's working very well, which is why they
made this video.
The people who are recruited for this, they would go back home and they would advance
Western interests quietly.
They run an ad campaign basically.
They would influence things slowly but surely and adjust Russia from within.
It's not new.
It's something that happens pretty often and my guess, that's what that video is for.
They're not actually trying to get some major to flip.
It's not the Cold War anymore.
It would be targeted a little bit differently if that's what they're actually trying to
do.
To me, this is a calling card trying to get out the information to those people who are
upset with Daddy or Uncle Putin for disrupting their good time in their life.
And they're currently in the West enjoying it.
And if they want to continue to enjoy it, well, I mean maybe they could just switch
teams.
That's the pitch.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}