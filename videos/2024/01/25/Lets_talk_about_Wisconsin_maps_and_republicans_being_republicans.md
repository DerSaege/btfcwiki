---
title: Let's talk about Wisconsin maps and republicans being republicans....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=s_BYYNMLBCU) |
| Published | 2024/01/25 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Contrasts Wisconsin's map situation with Louisiana's smooth process, noting Wisconsin's Republicans engaging in underhanded tactics.
- Wisconsin faces a deadline to create new maps, which seems unlikely to be met.
- Republicans in Wisconsin made minimal changes to a proposal, with the governor expected to veto it.
- Republicans seemingly adjusted the map to avoid incumbents facing each other, prioritizing party power over fairness.
- The court will likely have to intervene in Wisconsin's map situation, leading to a map Republicans will oppose.
- The state's long history of gerrymandering means a fair map will upset those accustomed to privilege.
- Democrats are taking a moral stance due to gerrymandering, knowing any court-given map will be better than what they have now.
- Democratic inflexibility appears idealistic but is pragmatic given the likely court outcome.
- Expect a resolution once the court intervenes, as they want a new map in use promptly.

### Quotes

- "When you have become accustomed to privilege for so long, equality seems like tyranny."
- "They want fair maps for our people and we're not going to compromise an inch."
- "A fair map comes out, they are going to be super upset."
- "They're going to come out ahead."
- "Y'all have a good day."

### Oneliner

Contrasting Wisconsin's map turmoil with Louisiana's smooth process, Beau exposes underhanded Republican tactics, impending court intervention, and Democratic pragmatism in the face of gerrymandering.

### Audience

Voters, Activists

### On-the-ground actions from transcript

- Contact local representatives to express support for fair redistricting (implied).
- Stay informed about court decisions regarding Wisconsin's maps (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the political dynamics and strategies behind redistricting in Wisconsin, offering insights into the impact of gerrymandering on fairness and party power struggles.

### Tags

#Wisconsin #Redistricting #Gerrymandering #RepublicanParty #DemocraticParty


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Wisconsin
and what's going on with the maps there,
which is a stark contrast from what happened in Louisiana,
which went so smoothly, it was surprising.
A number of people commented on that.
Yes, I was kind of at a loss for words
because I was truly expecting the Republican Party
engage in some very underhanded Louisiana politics, and when it didn't happen, I had
no idea what to do.
I am more at home in Wisconsin now, because, well, the Republicans there are engaging in
some underhanded.
Anyway, so Wisconsin is in very much the same situation.
They have a deadline they have to meet to have new maps, or the court is going to deal
with it. That deadline does not look like it's going to be made. A number of
proposals were sent up, but the Republicans took a proposal from the
governor there and made some, quote, miniscule changes to it. And advanced
that, the governor is saying that that proposal, that map, is going to be vetoed.
It is worth noting that it certainly appears that Republicans in Wisconsin tried to adjust
the map so incumbent Republicans wouldn't be facing off against each other.
If you believe that they would want to alter the congressional maps to stop two incumbents
from having to run against each other, but they wouldn't alter those maps to preserve
the power of the party as a whole, you might want to take some critical thinking courses.
So the odds here are that this is going to be vetoed and then the court is going to have
to decide.
The court will decide on a map that is one that the Republican party is going to be opposed
to.
They're not going to be happy with it because this is one of those situations where when
you have become accustomed to privilege for so long, equality seems like tyranny, that's
what's about to happen.
The state has been gerrymandered their direction for so long that when a fair map comes out
they are going to be super upset.
That's where things are headed at the moment.
I don't see any of the plans to get like a proposal that everybody agrees on.
I don't see that working because the Republican Party is prioritizing protecting politicians
And the Democratic Party is, I mean, I do want to be honest here, they are very much
kind of grandstanding on being the moral fair ones.
But they're in that position because the maps were so gerrymandered.
They don't have to compromise because anything that the court gives them is going to be way
better than what they have now.
So it allows them the freedom to just kind of be like, no, we want fair maps for our
people and we're not going to compromise an inch because they know they're going to come
out ahead.
So don't believe they got all idealistic or anything.
It's just a moment where they can present themselves in a way that is super idealistic
when it is also incredibly pragmatic because the odds are the court will give them something
better than any compromise proposal.
So I would expect to have this wrapped up pretty quickly.
Once it heads to the courts, the courts made it clear they want this map in use.
So anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}