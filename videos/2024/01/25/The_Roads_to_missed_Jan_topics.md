---
title: The Roads to missed Jan topics....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=6SsWyDwxvfM) |
| Published | 2024/01/25 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Feeling under the weather, Beau does a short Q&A session addressing news events due to inability to do long format content without coughing.
- Reports on a disturbing story about a pauper's grave site in Jackson, Mississippi with hundreds of unmarked graves where next of kin were not notified.
- Civil rights attorney Ben Crump is involved in investigating the pauper's cemetery.
- Teases a future video on Russia's possible claim on Alaska as retaliation for US's stance on Ukraine.
- Addresses misconceptions about Biden's involvement in Ukraine, directing viewers to a video for a timeline breakdown.
- Mentions Iran's vote in favor of a two-state solution at the UN and its significance in foreign policy dynamics.
- Talks about potential defensive moves in Europe and NATO in response to international tensions.
- Explains the interim ruling by the ICJ and clarifies it's not the final ruling in a case.

### Quotes

- "Hundreds of unmarked graves where next of kin were not notified."
- "It's so much worse than it sounds."
- "The reality is what Vice President Biden did in Ukraine reopened an investigation."

### Oneliner

Beau addresses disturbing news about unmarked graves, clarifies misconceptions on Biden and Ukraine, and hints at future videos on international tensions.

### Audience

Viewers interested in current events and political analysis.

### On-the-ground actions from transcript

- Stay informed about international tensions and political developments (implied).

### Whats missing in summary

Exploration of the geopolitical implications and potential consequences of international tensions discussed.

### Tags

#CurrentEvents #BenCrump #Russia #Ukraine #Biden #Iran #ICJ #Beau


## Transcript
Well, howdy there, internet people.
It's Beau again, and welcome to the Roads with Beau.
Today we are going to do another short Q&A,
because I am still feeling under the weather.
And basically, the idea of doing long format content right
now doesn't really sit well, because I
can't go more than a few minutes without coughing.
And nobody really wants to see that.
So they have chosen some questions
that are supposed to be related to actual news events.
And we're just going to run through those
and talk about those real quick.
And it's supposed to be mainly stuff that I missed.
So we'll see how that plays out.
OK.
Greetings from across the pond.
I came across a story which is wildly disturbing in which
There is coverage on, even from one of Fox's channels.
I missed it at the time.
I also can't find any commentary on it from you,
but given your sometimes cryptic video titles,
I may have simply missed it in my search.
The topic is that hundreds, 215 identified.
OK, and Ben Crump is on the case.
OK, so quick overview of this.
Behind a jail in Jackson, Mississippi,
there's a popper's grave that has a couple hundred,
at minimum, a couple hundred graves that are numbered,
but not really marked in any other way.
Their next of kin have not been notified
we're not notified. Ben Crump is a civil rights attorney, very well known one. It
is totally unsurprising that he and his team are there. I did not know that, but
that is not surprising. I have a friend, I have not made a full video about this,
but I've mentioned it in videos on this channel. I have a friend who is actively
investigating this. I'm not going to talk about it until I hear from them.
I will say this, it is totally unsurprising that Ben Crump, a civil rights attorney, is
there looking at a pauper's cemetery where family was not notified because it does appear
that there are some demographic trends.
The other thing I will say is based on what I have been told early on, it's so much
worse than it sounds.
But it's the kind, they're the kind of accusations you don't want to make until you know you're
right.
But it does not look good.
I guess I still can't form a coherent question, but I think more people should be aware of
Putin's vague Alaska reference, because Ukraine is not the end game.
That's definitely going to get a video soon over on the other channel.
Just a quick teaser of the story, Russia is instituting a thing where it is re-examining
old real estate deals to include Alaska and kind of suggesting that they're illegal and
they want that land back.
Basically what's happened is Republicans in Congress have been so weak and have not shown
enough resolve when it comes to Ukraine that now Russia is saber rattling, well, you let
Let us take their stuff, we're going to take yours next kind of thing.
As we talked about, and we will certainly talk about again, Ukraine security is integrally
tied to European security, and European security is tied to American security.
There will be more on that.
I hope you can help me out.
I'm a Democrat and decided to start talking with a Republican.
I know listening is a good skill, but I'm struggling with something he said.
It sounds like a fiction novel.
Said that Joe Biden is corrupt and has taken his corruption to the White House.
What he told me was that Biden has had business meetings with Hunter's associates.
He blocked $1 billion in aid.
And yeah, this just goes through that whole storyline.
I actually can help you with this, because it's grounded in reality in some way, but
it's not in order.
I have a video titled, Let's Talk About Journalism 101 and Ukraine, or and a Ukraine timeline,
something like that.
I'll put it down below.
The video is actually, it wasn't really meant to be a video debunking this theory.
It was meant to be a video showing journalists and kind of reminding journalists that if
you're going to do investigative reporting, you have to put things into a timeline, otherwise
you get wild stories.
Just happen to use this as an example.
I'll put it down below, it will give you all of the information you need to
process what they're saying the story is and the reality, because once you put
just a few pieces of the timeline in place, everything falls apart in their story.
The reality is what Vice President Biden at the time, what he did in Ukraine,
It reopened the investigation into the company his son was on the board of directors of.
It didn't stop an investigation, it started one.
It reopened one.
So I would watch that and then listen to your friend again, listen to the person you've
been talking to again.
I don't remember you talking about how, for the first time on October 27th,
Iran voted in favor of a two-state solution in the UN.
Did you cover it?
Does it matter?
Did I cover it?
No.
Does it matter?
Kind of.
It's not an actual shift yet.
It's not an official shift yet, but it's an opening and a changing of that position.
They're warming to it.
And the reason they're warming to it is because it's going to be very hard to pretend that
they would ever intervene on behalf of the Palestinians because they didn't intervene
with this.
Because of how bad it's been, a whole lot of countries talk.
They've been called on it.
On this channel, there's a video called The Roads to Foreign Policy Dynamics where we
talk about two countries, red and blue.
is definitely not Israel and Red is definitely not the Palestinians but the
dynamics expressed there they kind of line up and Red had a bunch of countries
that were like yeah we'll help you here here's here's the means to fight go
ahead and keep destabilizing this regional power for us and you know if
you do it well enough eventually we'll come and help but they never do they
They never intended on it.
Because of how bad things are in Gaza, it is very hard for those countries to pretend
that they would ever help.
So they're going to have to shift to their position, which means they will warm to a
two-state solution.
Iran will be first, but there will be other countries that come along after that.
Is there a hardball move available that doesn't recklessly escalate tensions?
Like, can he put a significant number of boots on the ground in Poland or
material in Germany to free up resources in their country as a defensive line?
There are other ways to shuffle funds through, shuffle funds to them through NATO.
Biden's hands are kind of tied at this point.
He's done everything he could.
He bought Congress as much time as he could get them, but they still don't have a deal.
And it doesn't look like they're getting one soon, which means the ways in which he could
free-up stuff aren't ways that he would pursue.
OK.
The ICJ said they will release their ruling on the 26th.
I heard they normally take years to release a ruling.
Was that wrong, or is this a special circumstance?
They do take years.
This isn't the real ruling.
This is an interim ruling.
When we talked about it, and I was like,
this is what's going to happen.
I said you'd get a provisional ruling pretty quickly.
It's that one.
This isn't the end of the case.
OK.
And I think that's going to be it.
OK, so a little bit more information,
a little bit more context, and having the right information
will make all the difference.
Y'all have a good day. Thank you.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}