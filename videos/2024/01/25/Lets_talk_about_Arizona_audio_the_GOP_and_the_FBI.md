---
title: Let's talk about Arizona, audio, the GOP, and the FBI....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Ho1ZW34BMLE) |
| Published | 2024/01/25 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Chair of the Republican Party in Arizona resigns suddenly, citing ultimatum to resign or face release of damaging recording.
- Speculation that the recording was edited, with contents suggesting pressure on DeWitt not to run.
- Lake's team denies involvement, but implications are serious federal crimes.
- Federal investigators likely to look into various aspects of the situation.
- Release and editing of the video, potential extortion, and involvement of powerful individuals all under scrutiny.
- Situation has opened a legal can of worms, with no party coming out looking better.
- Anticipates an eventful 2024 political season in Arizona.

### Quotes

- "Nobody is coming out of this looking better than when they started."
- "This is going to be a big deal."
- "A giant mess, a giant can of worms."
- "This is the kickoff to a very interesting 2024 for the Arizona political season."

### Oneliner

Chair of Arizona GOP resigns amid scandal, potential federal crimes, and a messy political situation, kicking off a tumultuous 2024.

### Audience

Arizona residents

### On-the-ground actions from transcript

- Contact federal investigators to ensure a thorough investigation (implied)
- Stay informed about the unfolding events in Arizona politics (implied)

### Whats missing in summary

Full details and context of the scandal and potential federal crimes.

### Tags

#Arizona #RepublicanParty #Scandal #FederalCrimes #Politics #2024


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Arizona, the Republican
Party, and how things are probably
going to progress from here.
OK, so if you have missed the developments,
the chair of the Republican Party in Arizona
has resigned suddenly.
Jeff DeWitt has left that position saying,
this morning, I was determined to fight for my position.
However, a few hours ago, I received an ultimatum
from Lake's team, resigned today,
or faced the release of a new, more damaging recording.
I am truly unsure of its contents,
But considering our numerous past open conversations
as friends, I have decided not to take the risk.
I am resigning as Lake requested in the hope
that she will honor her commitment to cease her attacks,
allowing me to return to the business sector,
a field I find much more logical and prefer over politics."
So a recording went out, and there is speculation
that it was edited.
We'll see.
But the contents of it certainly appeared
to be DeWitt telling, like, hey, we really
don't want you to run.
In fact, so much so we could find you a better job
somewhere for more money.
Is there a number, you know, type of thing?
OK.
It is worth noting that Lake's team has issued a flat denial.
They did not do this.
I mean, that is definitely a denial to be issued,
because this isn't politics.
Like, this is a federal crime.
This is going to be a big deal.
Right now, people are looking at the drama and gossip aspects of it.
There are a number of things that occurred during the course of this, based on the reporting,
that are federal crimes and big ones.
I have a feeling that this was one of those moments where people might have been looking
at things through the lens of, this is what they do on TV.
And maybe didn't really think things through.
But my guess is that there are federal investigators either
already there or on their way there now
to look into multiple aspects of this,
from the release of the video to the editing of the video
to whether or not there was a request for an action
or more information would be released
to maybe who the powerful people back east are.
This opened a giant mess, a giant can of worms.
And I feel like those who were a party
to releasing all of this didn't really understand
what they were doing.
Because nobody is coming out of this
with looking better than when they started.
This is just, it just all looks bad and then there, there are certain legal
questions that are going to be investigated.
Um, so this we can say is the kickoff to a very interesting 2024 for the
Arizona political season.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}