---
title: Let's talk about McConnell, Trump, and deals....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=QXAXyuzIoHA) |
| Published | 2024/01/25 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republican party debating passing legislation promised to constituents for years, realizing it may leave them with nothing to campaign on.
- Effort to tie aid to Ukraine to border security, jeopardizing national security for political gain.
- Republicans considering sabotaging their own legislation to give Trump something to rant about.
- Concern that passing promised legislation will leave them with nothing to scare voters with.
- Republican party prioritizing scaring their base over fulfilling promises and national security.
- Cynical move by Republicans to prioritize Trump's talking points over national security and constituents.
- Mention of McConnell's reputation for cynical political moves.
- Implication that Republican Party's decision on border security bill will be driven by the desire to keep their base scared.
- Disruption of American national security for the sake of maintaining a fear-based campaign strategy.
- Criticism of Republicans for sacrificing national security to give Trump ammunition for campaigning.

### Quotes

- "Republican party is currently deciding whether or not they want to actually pass the legislation that they've been promising their constituents they would pass for years."
- "They are jeopardizing national security for a talking point to make sure that Trump has a talking point during an election."
- "If a border security bill doesn't happen, it will be because the Republican Party chose to not have that bill so they could continue to scare their base."

### Oneliner

Republican party jeopardizes national security and breaks promises to constituents in favor of maintaining fear-based campaign strategy.

### Audience

Voters, political activists

### On-the-ground actions from transcript

- Contact your representatives to express your concerns about prioritizing fear-mongering over national security (implied).
- Join or support organizations advocating for responsible governance and accountability within political parties (implied).

### Whats missing in summary

Detailed analysis of the potential consequences of sacrificing national security for political gain.

### Tags

#RepublicanParty #Trump #McConnell #NationalSecurity #CampaignStrategy


## Transcript
Well, howdy there, internet people.
Let's vote again.
So today we are going to talk about Trump and McConnell, the Republican
party in general, long-term strategy, Ukraine, all kinds of things, because
it's all tied together because the Republican party is currently deciding
whether or not they want to actually pass the legislation that they've been
promising their constituents they would pass for years because they've
realized that if they do that, well, they don't have anything to run on
anymore.
It appears that large parts of the Republican party want to kind of
sabotage their own legislation so Trump can have something to rant about.
If you have no idea what I'm talking about, for a pretty lengthy amount of
time there's been an effort to tie aid to Ukraine to quote border security.
Republicans really wanting that border security so much that they would
jeopardize national security when it comes to the aid to Ukraine. Now they've
kind of got a deal and now they're talking about kind of sabotaging it
because we don't want to undermine the nominee, meaning Trump, something that was
actually said during the discussion. They've realized that if they pass this,
well, then they don't have anything to scare the easily manipulated. They don't
have anything to terrify them with, and that's how you get them to vote. So
So they're probably at this point leaning away from passing the border security bill
because they want to be able to keep people who maybe don't have a good grasp on the situation,
they want to keep them scared.
If it was really as bad as they've been telling you on Fox News, could they put it off for
a year?
importantly, if it is that bad, and they were willing to put it off for a year, is
that really somebody you won't representing you? They've put this out
there and now they're worried that if they actually pass the legislation they
have promised, well they won't have a tool to scare the American people with.
They won't have anybody to blame things on.
They are jeopardizing national security for a talking point to make sure that Trump has
a talking point during an election.
They are going out of their way to not pass something they told their constituents they
would pass, so Trump can continue to scare them.
It is the most cynical move I've seen in politics in a really long time, should be no surprises
coming from McConnell.
Like I said, you may not like him, but don't underestimate him.
So right now, that's the discussion that's being had.
If a border security bill doesn't happen, it will be because the Republican Party chose
to not have that bill so they could continue to scare their base.
Along the way, they will disrupt American national security by disrupting the aid to
Ukraine, all to give what is likely a losing candidate the ability to yell at
clouds and scare people.
Anyway, it's just a thought.
I hope you all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}