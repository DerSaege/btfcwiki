---
title: Let's talk about Boebert and politics being local....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ytzCjkzZFH4) |
| Published | 2024/01/30 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau analyzes Boebert's political situation and its implications for wider politics.
- Boebert, a Colorado representative, seeks to switch districts due to low re-election chances.
- Despite being a high-profile MAGA figure, Boebert is polling fifth in her primary race.
- Local criticisms of Boebert include being labeled a "carpetbagger" and facing challenges on definitions.
- The MAGA movement aims to shift politics nationally, neglecting the importance of local issues.
- MAGA's strategy involves instructing commoners on how to vote, eroding representation.
- Boebert's struggles in her primary race suggest that the strategy may not be as effective as anticipated.
- If more well-known MAGA candidates start losing, it could indicate a shift within the Republican party.
- The culture war approach may have left Republicans less represented and more obedient to party leaders.
- Boebert's future in Congress appears uncertain as she faces challenges in her primary race.

### Quotes

- "It's one of the things that the MAGA movement was trying to do, it has been successful in a number of ways, is getting rid of the idea that all politics is local."
- "This is how you end up being ruled rather than represented."
- "Or the commoners are finally starting to see through it. It's one of the two."
- "It's an early sign. We'll have to see how things play out in the primaries."
- "It seems incredibly unlikely that Boebert will continue her career in Congress."

### Oneliner

Beau dissects Boebert's struggles in a primary race, revealing broader implications for the Republican party's political strategy.

### Audience

Political enthusiasts

### On-the-ground actions from transcript

- Support local political candidates who prioritize community issues (implied)
- Stay informed about local politics and hold representatives accountable (implied)

### Whats missing in summary

Insights on the potential shift in Republican party dynamics and the importance of local representation.

### Tags

#Politics #Boebert #MAGA #RepublicanParty #LocalRepresentation


## Transcript
Well, howdy there, internet people. It's Beau again. So today we are going to talk about
Boebert and what Boebert can teach us about wider politics and how a political shift that
the Republican Party was trying to pull off may not actually work. Okay, so first let's talk about
Boebert. If you don't know, she is a representative from Colorado, currently representing one
district but wants to change and represent a different district in Colorado. The reason
for this is that she really didn't stand a chance of winning re-election in the current district.
So she wanted to move to a redder district. The assumption was that because she's high
profile national figure, MAGA darling, that she'd be a shoe-in to win the primary.
She is currently polling fifth, not doing well at all. In a recent debate, there were
questions about definitions. I have to admit, hearing the word carpet bagger
from Colorado was surprising. The local paper, there was a list of definitions,
a list of words that they assumed she knew the definition to. It's just not
going well all over, but this is kind of telling. It gives us some insight. One of
the things that the MAGA movement was trying to do, it has been successful in
a number of ways, is getting rid of the idea that all politics is local. There's
There's been a rule for a really long time that all politics is local, and when you get
down to it, it's the local issues that are going to matter.
MAGA has been trying to culture war its way out of this and develop a national policy
and make all politics national.
This is why you have people from Illinois super upset about the border, because MAGA
told them what was important to them, rather than them telling their representatives what
was important to the people, right?
This is how you end up being ruled rather than represented.
For this to be an effective strategy, it means that those at the top, the representatives,
the federal government employees that are elected, those people, they tell the commoners
what to believe. And then those commoners have a duty to vote in the way they've been instructed.
Dobert nationally is a well-known MAGA figure. She is a well-known Republican figure.
If this strategy was working, she would be a shoo-in.
she's not, not even on the just the other side of the state.
Because the the people are not obeying their betters.
Their mago rulers are not effective at telling those commoners what to do.
Or the commoners are finally starting to see through it. It's one of the two.
This is a sign that this strategy may not be working.
It's an early sign. We'll have to see how things play out in the primaries, but if you see a bunch
of well-known MAGA candidates start losing, it is a clear indication that the Republican party is
starting to wise up to the fact that they've been played, that the culture war nonsense has left
them less represented. It's left them obeying their rulers instead of getting their interest
represented. Either way, at this point, it seems incredibly unlikely that Boebert
continue her career in Congress because it doesn't look like she's going to make it through the primary.
Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}