---
title: Let's talk about E Jean Carroll spending Trump's money....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=-_9JlRuy83Y) |
| Published | 2024/01/30 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- E. Jean Carroll was awarded over 80 million dollars by a jury in a case against Trump.
- Carroll plans to use some of the money to buy premium dog food as a splurge.
- She also wants to use the money to support causes that Trump dislikes.
- Carroll's goal is to donate the money to things that will cause Trump pain.
- One suggested cause is a fund for women who have been assaulted.
- Despite wanting to make a joke about Trump hating him, Beau found Carroll's ideas for the money to be very worthy.
- Carroll's actions have the potential to create more support for causes due to her profile and the animosity she faced.
- Beau admires Carroll's decision to prioritize doing good with the money over just the bare minimum.
- Carroll's character shines through in wanting to use the money for positive impact.
- Beau sees Carroll's actions as inspiring and hopes to reach that level of giving back.

### Quotes

- "I'm going to donate it and give it away to things that will cause you pain."
- "She can show up to fundraisers and people will show up and support the cause because of that."
- "Carroll's character shines through in wanting to use the money for positive impact."

### Oneliner

E. Jean Carroll plans to use the money awarded by a jury, including buying premium dog food and supporting causes Trump dislikes, showcasing a desire to do good and create positive change.

### Audience

Supporters of social justice causes

### On-the-ground actions from transcript

- Support funds for women who have been assaulted by donating or volunteering (implied)
- Attend fundraisers for causes you believe in to show support (implied)

### Whats missing in summary

The emotional impact of using money awarded from a difficult situation to create positive change.

### Tags

#EJeanCarroll #SocialJustice #PositiveChange #SupportCauses #Activism


## Transcript
Well, howdy there Internet people, it's Beau again. So today we are going to talk about E. Jean Carroll
and what she plans on doing with her money. How she plans on disposing of it because she said
something that's really interesting and I want to acknowledge it because to me it's just absolutely
beautiful. Just in case you missed it or you're unaware, you don't know, the jury
ordered Trump to give her more than 80 million dollars. Now she said that like
initially she couldn't even really process that because it was just
overwhelming and now she has said that she's gonna take some of it and buy
like premium dog food and that's going to be her splurge but she that she wants
to do some good with the money as well which I think is very admirable and she
said that she wants to do good in a way that Trump would hate. She wants to take
the money that he's gonna have to give her and then use that to support causes
that he doesn't like. I love that. I aspire to that level of pettiness. Not just am I
going to get tens of millions of dollars from you, I'm going to donate it and give it away
to things that will cause you pain. One of the things that she suggested was a fund for
women had been assaulted. You know, like, when I first heard about this, I wanted
to do a whole video making a joke about the fact that Trump hates me. So, you
know, hand it over. And then I heard some of the ideas she was tossing around and
they seemed like, you know, super worthy. So I didn't want to make too much of a
joke of it but when you are in her position at her age with the amount of
money that she's going to have coming in and the profile that she has she's in a
position to do a whole lot not just not just be like the ultimate level of petty
that I love, but also create an environment where more people are likely to support those
causes because her actions here, especially because of the animosity she faced in coming
forward and the way everything played out, her profile is now one that can be utilized
to do a lot of good.
If she's wanting to support a fund like that, other people will support it because she does.
She can show up to fundraisers and people will show up and support the cause because
of that. The fact that after that verdict, the thing that appears to be first on her
mind is taking the money and doing something good with it, beyond just the bare minimum,
kind of speaks a lot to her character. And again, I aspire to that level of
Anyway, it's just a thought.
Just a thought.
you all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}