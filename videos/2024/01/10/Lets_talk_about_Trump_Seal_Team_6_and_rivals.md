---
title: Let's talk about Trump, Seal Team 6, and rivals....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=fyIeu-Hl5Jk) |
| Published | 2024/01/10 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President's legal team argued in court that Trump could order SEAL Team 6 to eliminate his political rivals without facing criminal liability unless impeached.
- The argument for presidential immunity was taken to the extreme, implying Trump could act as a dictator with impunity.
- Hypothetical scenario presented: a new President could order removal of political rivals with no consequences unless impeached and convicted.
- Trump's legal team's argument suggests he could eliminate political rivals using the military within the United States without accountability.
- People need to recognize the danger and implications of supporting someone seeking such unchecked power.

### Quotes

- "If you still support this man, you really need to re-evaluate everything in your life."
- "His team argued in court that without being impeached and convicted, he could eliminate political rivals using the military within the United States."

### Oneliner

Trump's legal team argued he could eliminate political rivals with impunity, setting a dangerous precedent for unchecked power.

### Audience

Concerned citizens, political activists

### On-the-ground actions from transcript

- Re-evaluate support for individuals seeking unchecked power (exemplified)
- Pay attention to political implications and seek accountability (exemplified)

### Whats missing in summary

Full understanding of the transcript's implications and the danger of unchecked power.

### Tags

#Trump #PresidentialPower #PoliticalAccountability #Impeachment #Dictatorship


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Trump and SEAL Team Six
and we're gonna follow that to its ultimate conclusion.
I was a little bit out of the loop
when it came to news yesterday
because I was dealing with the whole tornado thing.
But when I got back and started reading up,
I found out that the president's legal team
was in court arguing that he could order SEAL Team 6 to eliminate his political rivals
and he would only face criminal liability for that if the House and the Senate impeached
and convicted him.
If they didn't do that, well, he can't even be charged for that.
You know, when he started claiming presidential immunity, I knew that these scenarios and
the hypotheticals that were going to get posed, I knew his legal team was going to have to
answer yes to do some pretty wild scenarios.
Because to create a precedent where presidential immunity covers the conduct that is being
discussed in his charges, it has to just be all-encompassing. I didn't expect it to
go this far, but let's just follow it through. So we have a new president. His
name is President Smith. President Smith gets made fun of on Twitter by somebody.
So he orders SEAL Team 6 to remove them from circulation permanently because
there a political rival of his? Now, the House and the Senate have to impeach and convict.
Otherwise, President Smith walks. What do you think those conversations in the House
and the Senate are going to be like? Do you really want to become the political rival
of President Smith? Because all he has to do is have you removed permanently, have you
implanted.
The former president of the United States, somebody who is seeking that office again,
the legal team argued in court that he should be allowed to be a dictator.
There's no other way to read that because nobody can oppose him politically.
If they do, well, SEAL Team 6 is going to go take them out.
And he can't be held responsible, can't be held accountable for that, unless he's impeached
and convicted.
But the second they start that process, well, they're his political rivals, right?
So what would he do?
There are a whole lot of people who still do not want to accept that Trump isn't a
normal Republican.
There are a lot of people in the Democratic Party or to the left who don't really want
to accept exactly how bad he is for this country.
His team argued in court that without being impeached and convicted, he could eliminate
political rivals using the military within the United States.
That's what his team argued.
And if you follow that chain of events, anybody who wanted to impeach or convict becomes a
political rival.
They might suffer the same fate.
If you still support this man, you really need to re-evaluate everything in your life
really pay attention to what he is arguing, what he is wanting the courts to give him
the power to do.
Anyway it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}