---
title: Let's talk about Ohio, Idaho, and 2 wins....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=np70HjvpIlQ) |
| Published | 2024/01/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Idaho and Ohio have seen two wins moving into the new year that a lot of people can be happy about.
- In Idaho, a federal judge has blocked enforcement of a ban on gender-affirming care, citing equal protection under the law, due process, and parental rights.
- In Ohio, a Republican governor vetoed a ban on gender-affirming care and school sports for trans kids, which had passed in the legislature with enough votes to override a veto.
- The governor cited protecting human life as the reason for the veto, after speaking with physicians and families who emphasized how gender-affirming care had saved lives.
- The governor's decision to veto a bill passed by his own party indicates a broader understanding of future trends and potential repercussions.
- The opposition to medical care for trans individuals seems to be losing ground despite occasional loud protests.
- While the votes exist to override the veto, the outcome remains uncertain as the fight continues in both Idaho and Ohio.
- These developments suggest a positive trend within the Republican Party towards more thoughtful legislation, particularly regarding issues like reproductive rights.
- Although the rhetoric may not change immediately, legislative actions are showing signs of progress.
- The future outlook appears to be more forward-thinking, with an eye on avoiding past mistakes and implementing more progressive laws.

### Quotes

- "A Republican governor vetoed that ban."
- "It's losing. So the reality is the votes are there to override this veto."
- "The legislation might."

### Oneliner

Idaho and Ohio see wins with a federal judge blocking a ban on gender-affirming care and a Republican governor vetoing a similar ban, hinting at a positive shift in legislative approaches.

### Audience

Legislative advocates

### On-the-ground actions from transcript

- Contact local legislators to express support for gender-affirming care and inclusive policies for trans individuals (implied).
- Advocate for the protection of trans rights within your community and support organizations working towards these goals (implied).

### Whats missing in summary

The full transcript provides additional insights into the nuanced political considerations surrounding gender-affirming care legislation in Idaho and Ohio. Viewing the entire video may offer a deeper understanding of the evolving attitudes within the Republican Party towards such issues.

### Tags

#Legislation #GenderAffirmingCare #TransRights #RepublicanParty #ProgressivePolicies


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Idaho and Ohio
and two wins moving into the new year
that a whole lot of people watching this channel
can be happy about.
And just kind of run through how even the win
And just kind of run through how even the win
that is most likely to be reversed
it may actually be the bigger win.
So we will start in Idaho.
A federal judge has blocked enforcement
of a ban on gender-affirming care
and basically cited equal protection under the law,
the whole due process thing, and parental rights.
There's more to come on that case, no doubt.
In Ohio, the Republican governor vetoed a ban on gender-affirming care and school sports
for trans kids.
Hey, I must say that again.
A Republican governor vetoed that ban.
It's called the SAFE Act, and the governor cited, quote, protecting human life as the
reason for the veto. Basically, he said that he talked to physicians, he talked to family
members, and talked to people who said gender-affirming care saved their life or their kids' life.
And that is the reason he says that he vetoed it. So, I mean, that in and of itself is interesting.
There was also a lean into the idea of parental rights there as well.
That's interesting, but there's two things about this.
One is that this passed in the Ohio legislature with enough votes to override a veto.
The other thing that's worth noting is that a governor doesn't buck their party like
this unless they've run the numbers, unless they have looked at the polling, unless they
understand that this is a decision that they don't want to have to defend later.
They don't want to have to defend signing this.
My guess is that the governor understands more than the people in the legislature about
how things are trending and what the future is going to look like for those people who
opposed what doctors recommend.
It is either that and it's a purely political thing or meeting with the families actually
to a politician, which seems unlikely. And while it may seem disheartening that it's based on
polling, understand that if the polling is enough to make the governor veto a bill passed by his
own party, it's pretty comprehensive. It is something that is, it's not going to be overcome,
which means that as loud as the opposition to medical care of this sort
gets at times, it's losing. So the reality is the votes are there to override this
veto. Now whether or not they're going to do it, I don't know. Maybe the governor
shares the polling information but that fight's not over. Neither one of these
are over but they are both steps in the right direction and you're seeing a
trend start to develop where I feel like the Republican Party doesn't want to
make the same mistake they made with reproductive rights and they're
starting to look a little bit further ahead with what they actually put into
law. Rhetoric may not change anytime soon, but the legislation might. Anyway, it's
It's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}