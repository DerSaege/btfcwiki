---
title: Let's talk about Trump, the 14th, and real impact....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=PJnYoM5P-UI) |
| Published | 2024/01/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the potential impact of Trump being left off the ballot if the Supreme Court allows it on a state-by-state basis.
- Responds to a question from a non-Trump-supporting Republican about the implications.
- Points out that Trump being excluded from the ballot might not affect the presidential race significantly since he wouldn't win those states anyway.
- Mentions the slim chance of the Supreme Court disqualifying Trump from running, which could have a more substantial impact.
- Raises the possibility of Trump supporters not showing up to vote in states where he's not on the ballot, affecting down-ballot races.
- Notes that even in solidly red states like Mississippi, there are non-Republicans, and in blue states, there are Republican districts.
- Suggests that the absence of Trump on the ballot could affect congressional and down-ballot races more than the presidential race.
- Advises against worrying about the situation until the Supreme Court makes a decision.

### Quotes

- "When it comes to Trump being off of the ballot, It seems like that would only happen in hard blue states."
- "If people aren't going to show up because Trump's not on the ballot, that means the advantage that exists in red districts, it's lessened by a lot."
- "I need a straight answer on something and the liberal outlets are screaming and celebrating about how great it is, and the Republican outlets are crying like little female puppies."

### Oneliner

Beau explains the potential impact of Trump being left off the ballot on a state-by-state basis, focusing on down-ballot races more than the presidential race.

### Audience

Politically engaged individuals.

### On-the-ground actions from transcript

- Stay informed about the legal proceedings and decisions regarding Trump's candidacy on a state-by-state basis. (suggested)
- Engage with local politics and down-ballot races to understand the broader impact of Trump's absence on the ballot. (implied)

### Whats missing in summary

Insights into how Trump's absence on the ballot could influence voter turnout in different states and impact down-ballot races. 

### Tags

#Trump #SupremeCourt #ElectionImpact #DownBallotRaces #StateByState #PoliticalAnalysis


## Transcript
Well, howdy there, internet people, let's bow again.
So today we are going to talk about Trump
in the 14th Amendment, and its real impacts
if the Supreme Court decides to let it go forward
on a state-by-state basis and leave Trump off the ballot.
We're gonna do this because we got a question
from a conservative who can't get an answer.
And it's probably worth going over
because I don't know that this has really been discussed
the way it should, but here's the question.
I'm one of the non-Trump-supporting Republicans
that watch you.
I need a straight answer on something
and the liberal outlets are screaming and celebrating
about how great it is, and the Republican outlets
are crying like little female puppies.
When it comes to Trump being off of the ballot,
It seems like that would only happen in hard blue states.
So even if it stands, would it really impact the race?
He's not getting those electoral votes anyway.
Am I missing something?
Kind of, but I mean, it's not something
that's being discussed yet.
So if the Supreme Court decides to let it stand
on a state by state basis, okay,
then yeah, odds are it's only gonna be states
he would lose anyway.
So it really doesn't impact it there.
Now there is the slim chance
that the Supreme Court comes back and says,
no, he can't run, he's totally disqualified.
Doesn't seem likely, but it's not impossible.
That would obviously alter things.
So when you're talking about the presidential race,
probably isn't going to have a whole lot of impact.
At the same time, remember that Mississippi,
which is a super red state, like one out of three people,
aren't Republicans.
And if you go to a lot of blue states,
you will find Republican districts
in those blue states.
If Trump is not on the ballot in those states, odds are the MAGA faithful, they're
not showing up to vote because most of them are very authoritarian and they
really care about voting for president because that's who they think
actually controls everything. So it would probably have pretty big impacts on
congressional races, you know, down-ballot races. If people aren't going to show up
because Trump's not on the ballot, that means the advantage that exists in red
districts, it's lessened by a lot. That would be the real impact if it
occurs on a state-by-state basis. Again, I would not worry too much about this
until it goes to the Supreme Court and they make their decision because we're
not going to know anything until then. Anyway, it's just a thought. Y'all have a
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}