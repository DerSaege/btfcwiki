---
title: Let's talk about the Consulate, Iran, and information....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=YPFWqhAXsq4) |
| Published | 2024/01/16 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the confusion surrounding recent events due to early social media information.
- Describes Iran engaging in long-range missile attacks targeting espionage centers and alleged attack-linked gatherings.
- Clarifies that there was no attack on the consulate as the U.S. claims no facilities were impacted.
- Notes the presence of misleading videos on Twitter, with only one potentially showing a real blast near the consulate.
- Points out the shift in Twitter's credibility from accuracy to sensationalism for engagement.
- Raises concern about manufactured consent for military action against Iran due to false information.
- Emphasizes the dangers of advocating military response based on misinformation about non-existent attacks.
- Mentions the potential implications if the targets were Israeli intelligence facilities and speculates on Israel's response.
- Comments on the record distance the missiles traveled and the need for accuracy verification of the strikes.
- Debunks theories linking Iran's actions to other events or political leaders' responses due to the absence of an attack on the consulate.

### Quotes

"Twitter’s currency is changing from reputation to engagement."
"Manufactured consent for military action is not harmless."
"Advocating military response based on misinformation is dangerous."

### Oneliner

Beau clarifies the absence of an attack on the consulate, warns against advocating military action based on false information, and questions the credibility of social media content amidst recent events.

### Audience

Social media users

### On-the-ground actions from transcript

- Fact-check social media information (implied)

### Whats missing in summary

The impact of misinformation on public perception and the need for critical evaluation of social media content.

### Tags

#SocialMedia #Misinformation #Iran #MilitaryAction #FactChecking


## Transcript
Well, howdy there internet people, it's Bo again.
So today we are going to talk about the consulate
and commentary and videos
and just kind of run through everything.
There is a whole lot of confusion
about what has occurred
because of early information
that went out on social media.
So we are going to start
by going over what we know.
And then we will get to the other stuff.
What we know is that Iran engaged
in some long-range missile attacks.
That's what we know.
They are saying that they were going after,
I think they used the term espionage centers or spy HQs,
something like that, along with gatherings
that they believed were
linked to some attacks in Iran.
That's what they went after.
Right now
somebody is saying, but what about the attack on the consulate?
There wasn't one.
Um...
There wasn't one.
Okay, so the U.S.
is saying that there were multiple strikes
but that quote
No U.S. facilities were impacted.
The U.S. is describing the action as reckless.
I know that some people are going to say that I saw footage on Twitter.
Yeah, you saw one of four videos.
One was from 2022, one was from 2019, one was from 2018.
There is a fourth video that I can't confirm is old and it just shows an
explosion off in the distance. That one might be real. That one it
might be real because one of the strikes was near the consulate, not on the
consulate. And the blast, it looks about right. The older footage, it's also worth
noting that the older footage, all of that is from the embassy, which is in
Baghdad. The consulate is not. There was a time when if you saw
multiple accounts on Twitter posting footage saying something just happened
that it probably did. That time has passed. The reputation of Twitter is
changing because the currency is changing. The currency on old Twitter
was reputation, accuracy. Today the currency on Twitter is currency and
And there is a financial motivation to be sensationalist, it's engagement.
So just remember this, because there are whole streams of commentary about what the US should
do in response to the attack on the consulate, an attack that never occurred.
And a whole bunch of people saw that commentary.
That commentary can be used to manufacture consent for military action against Iran.
It's not harmless.
When this type of information goes out, it changes people's views because they may not
get the fact check.
But at this point in time, from both Iran and the US, they are saying no US installation
was targeted or impacted, no coalition personnel were harmed.
So that doesn't mean that this doesn't matter.
Let's be clear about that.
If these were Israeli intelligence facilities, I imagine Israel is going to respond.
The interesting thing to me is in this reporting, and it's buried, there is the indication
that the missiles traveled 1,200 kilometers, 760-ish miles.
If that's accurate, that's a record for them.
We will find out in the coming days about how accurate the strikes were, if they actually
hit where they planned to hit.
But all of the commentary about Iran did this because of the strikes on the Houthis, no,
they didn't.
Biden's weak and that's why Iran did this, no, because it wasn't at the consulate.
That commentary and the commentary that followed suggesting a strong response, meaning we need
to hit Iran, that's pretty dangerous because you now have a situation where you have people
who think military action is warranted over something that, according to the official
sources from both sides, never happened.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}