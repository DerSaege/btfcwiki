---
title: Let's talk about staying informed and whether you should....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=sSCZf-IpzNY) |
| Published | 2024/01/16 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau tackles the question of why people should stay informed about the news, especially if they believe they can't change outcomes.
- Mention of Neil Postman's perspective that most news headlines don't affect the average person and can leave them feeling powerless.
- People consuming news about events they can't influence end up filled with rage and grief.
- Beau acknowledges the limitations of individuals in altering foreign policy outcomes.
- Despite limited impact, Beau encourages those passionate about issues to continue their efforts as they may still influence outcomes to some extent.
- The importance of applying pressure and making small changes even if the overall outcome can't be drastically altered.
- Staying informed is compared to waiting for a break in traffic to make a move in altering outcomes.
- Beau stresses the significance of being informed to recognize key moments where actions can make a difference.
- Addressing the Israeli-Palestinian conflict, Beau underlines the need to understand the ingredients for peace and be prepared to support moves towards it.
- Beau expresses that informed individuals can play a vital role in supporting initiatives for peace and creating real change.

### Quotes

- "Staying informed is watching constantly, waiting for the break in those cars so you can make your move."
- "That's why you stay informed."
- "Being ready to do it when that golden moment arrives, it's the kind of thing that changes the world."

### Oneliner

Be informed, seize key moments for change, and support initiatives for peace to make a difference in the world.

### Audience

Active Citizens, Change-makers

### On-the-ground actions from transcript

- Support initiatives for peace in conflicts like the Israeli-Palestinian situation (implied).
- Stay informed about key international events and be ready to take action when opportunities arise (exemplified).

### Whats missing in summary

The emotional impact of staying informed and the power of collective action are best understood by watching the full transcript.

### Tags

#StayingInformed #Peacebuilding #ForeignPolicy #Activism #ChangeMaking


## Transcript
Well, howdy there, Internet people, it's Beau again.
So today, we are going to talk about the news,
staying informed, why you should or whether you should,
especially if it feels like you can't change
the things you want to.
You can't alter the outcomes.
You just get to keep score, and if that's the case,
a case, what's the point of even paying attention? We're gonna do this because I
got a question message that came in from somebody. I'm not gonna read the whole
thing because it's it's pretty lengthy but in relevant part it says a few
months ago I read Amusing Ourselves to Death by Neil Postman and he pointed out
that if you're the average person most of the headlines you see in the news are
are things that don't affect you, and that you can do nothing about.
It goes on to talk about the person seeing some of their friends, monitoring everything,
watching all of the horrible things that happen, but they can't do anything about it, and they're
They're just basically filling themselves up with rage and grief.
The person asks why they should stay informed.
They don't want to check out, but they don't understand what they can actually do.
When it comes to foreign policy, and the events that most people probably are imagining, you're
kind of right.
There's not much you can do, not right now.
I want to be clear about this.
That doesn't mean that there's nothing you can do, just that the effects are limited.
And those people who are out there burning themselves up, I hope you continue to do it.
Because while you may not be able to drastically alter the outcome, you might be able to change
it a little bit.
Imagine if there was constant pressure about the US drone program when it was at its height.
You wouldn't have stopped it, but you could have curtailed it.
If you're talking about a major conflict, the foreign policy machinery, it is really
well-oiled, you're not going to be able to stop it, but you might be able to encourage
your strength.
You might be able to shave a few days off of it, and that could be hundreds of lives.
So it's important to remember that when the stakes are as high as they are, when
you're talking about foreign policy, even something that isn't a drastic outcome,
well it really matters to some people. But that doesn't help somebody who
doesn't want to burn themselves up. The reason you stay informed is because when
When you are talking about international events, it's a machine.
Most of the decisions are already made.
The people in power really aren't even making them.
It's just doing the best they can based on the previous hands that were played at that
international poker game where everybody's cheating.
But in this case, we're going to use a slightly different analogy.
If you are your average guy, just some guy to quote the message, your average person,
and you are wanting to alter the outcome of foreign policy, you are standing on the side
of a really busy highway and cars are just zipping by.
You can't do anything to stop those cars.
All you want to do is get to the other side, but they're blocking your way and you really
can't do anything to change that.
Staying informed is watching constantly, waiting for the break in those cars so you can make
your move. Because in most situations you can't drastically alter the outcome, not
while it's ongoing. But there are key moments where you can make a move and it
will matter. But in order to do that you have to know when that time is and you
have to have the best information available when that moment arises. When
Then there's a break in the cars and you can do something.
That's why you stay informed.
You need to know one of the things that you mentioned was of course the Israeli-Palestinian
conflict.
You need to know what the ingredients are for peace.
a ceasefire, not a ceasefire, peace. It's important to remember that everybody who
is lost over the last few months was lost after the ceasefire, the last one. If
you want to alter that, if you want to change it, you have to be there to
support the move for peace when it comes. And given how this has played out, all of
of the horrific things that have occurred, lay the groundwork for this to realistically
be the best chance at peace that we've seen in decades. But you have to be informed. You
have to know what those ingredients are. You have to be able to say, yeah, it's going to take a
multinational force to stand between them. It's going to need a Palestinian state, or at least
viable pathway to one, a real one, and it's going to take dump trucks full of
cash to pump into Palestinian areas not just for reconstruction but to help
build the economy because you need that economic stability. And that
multinational force, a guy who signs up and enlists, goes over that's his first
deployment it would be great if you know that mission ended the year he retired
because that's how long it's gonna take and you have to know that you have to
know that so when a plan surfaces you said you watch both channels like the
one we talked about over on the other channel they got leaked the McGurk plan
plan. You have to know that those ingredients have to be there. So when
some plucky young upstarted State Department loses it and comes out
publicly and is like, no this won't work and this is why, you can be there to
support that move.
Basically, this is the best chance for peace that I have seen in 20 years.
The conditions that were created by all of this.
The problem is that people are very much locked into a cycle and they'll want to continue
that cycle, because humans are creatures of habit, even if the habit is horrible.
There will be people who, in the aftermath, try to forge a real peace. You
have to understand the foreign policy machinery. You have to know what the
ingredients are, and you have to stay informed so you can see that break in the
cars. That's the reason. When it comes to domestic stuff, it's much easier to affect
change. A lot of the stuff that has zero chance of massively altering the outcome in a foreign
policy situation. Oh, it would work domestically. It's one of those things
where staying informed lets you know when you can do something to really
affect change. It's hard, it's hard, but the stakes are pretty high. So,
So I understand it, I get it, but staying informed, learning what you can do, and being
ready to do it when that break in the cars, when that golden moment arrives, it's the
kind of thing that changes the world.
need a lot of people to do it too. I understand what you're saying about your
friends, just they are putting in a lot more work and suffering a moral injury
because they probably think it's worth it.
I would imagine that most of them know that the odds of drastically altering the outcome
are slim.
I would imagine that most of them understand that they're doing that for just a few small
changes.
But those few small changes, when you're talking about situations that are life and death,
matter. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}