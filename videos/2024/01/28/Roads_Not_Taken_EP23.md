---
title: Roads Not Taken EP23
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=YByKEwQ-gj0) |
| Published | 2024/01/28 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces The Roads with Beau, a series where he covers events that were unreported, underreported, lacked context, or did not receive adequate attention.
- Multiple nations, including the US, are pausing funding over allegations that 12 UNRWA employees were involved in the October 7 attacks in Gaza.
- Beau mentions the importance of understanding that the UN organization involved has thousands of employees, making the involvement of 12 individuals questionable.
- The ICJ ruling did not order a ceasefire and lacks enforcement mechanisms, sparking varied opinions and reactions.
- The NSA is buying data on American internet usage, raising concerns about privacy and the need for warrants.
- Peter Navarro was sentenced to four months for contempt, while Texas engages in a political stunt at the border.
- Beau notes the dangerous situation brewing at the border due to right-wing groups confronting the federal government.
- The Trump faction of the GOP is declining to support tough border security legislation, using the issue for manipulation during the 2024 election.
- Wall Street billionaire Clifford Asnes criticizes Trump over blocking Nikki Haley donors from MAGAworld.
- Reporting suggests that about 10% of individuals are refusing to restart student loan payments, prompting proposed measures like layaway for college.

### Quotes

- "It's one of those things where what the story is likely to focus on is kind of undercutting what is a real issue."
- "That's just something they made up to appeal to people who live in Illinois."
- "They can survive pretty much anything. I mean, they're a dinosaur."
- "It started because she told young people that they should vote."
- "It really is that simple."

### Oneliner

Beau covers foreign policy, NSA data purchase, border security politics, student loan payments, and more in this unreported news roundup.

### Audience

Viewers

### On-the-ground actions from transcript

- Contact organizations working on privacy rights to understand and address concerns about NSA data purchases (implied).
- Support initiatives promoting student loan forgiveness and affordable higher education (implied).

### Whats missing in summary

Insight into the potential implications of the US presence in Africa and how it may impact the region's people and relations with China.

### Tags

#ForeignPolicy #NSA #BorderSecurity #StudentLoans #UnreportedNews


## Transcript
Well, howdy there, internet people, it's Beau again,
and welcome to The Roads with Beau.
Today is January 28th, 2024,
and this is episode 23 of The Road's Not Taken,
a weekly series where we go through
the previous week's events and talk about news
that was unreported, underreported,
didn't have context, or just didn't get the attention
I deserve, or I thought it was interesting,
something along those lines.
At the end, we go through a Q&A normally.
I don't actually know if there's one at the end of this.
So we'll find out.
OK, so starting off, as we normally do,
with foreign policy.
All right, multiple nations, including the US,
are pausing funding over allegations
that 12 employees with UNRWA were involved
in the October 7 attacks.
OK, if you don't know what that is,
That is the UN organization that handles relief, humanitarian
efforts, stuff like that, in Gaza and the West Bank.
This will likely exacerbate an already untenable situation
when it comes to humanitarian efforts in Gaza.
It's important to understand that that organization has
thousands of employees.
12, being involved. It's not impossible. I'm sure there are some people who are going to say that didn't happen. Very
well might have.  I don't know that you can link the activities of the employees to the activities of the organization,
though.
That seems like a stretch to me, to be honest. I would need to see some very convincing evidence.
Opinions on the ICJ ruling are flying all over the place.
Two things to understand, the ICJ did not order a ceasefire.
It did not go as far as it could have in a lot of ways.
But as we talked about numerous times over on the other
channel, it also doesn't matter.
The ICJ does not have an enforcement mechanism.
An Israeli official was quoted as saying, hey, shmeg, like it doesn't matter, which,
I mean, that's not an attitude that I think is good, and I'm kind of surprised it was
expressed publicly, but it's also not wrong.
The ICJ does not have a lot of power.
In related news, the UN Security Council will eventually discuss the ICJ ruling and consider
making it actually matter, but refer to those videos on applied international law and how
that is likely to play out.
Okay, moving on to US news.
The NSA, the National Security Agency, is buying data on American internet usage.
This is probably going to turn into a larger story and there's a bit of nuance to this
one because I talked to some privacy experts and this is what I found out.
What this story is about is something called net flow.
As far as the privacy experts I talked to, this doesn't matter.
This is not a big deal at all.
It is totally not something to worry about that the NSA has this, even if it's yours.
But and this is the big but, it is symptomatic of a larger issue both with the intelligence
community and law enforcement when it comes to them buying things that they should really
need a warrant to get.
So it's one of those things where what the story is likely to focus on is kind of undercutting
what is a real issue.
Peter Navarro was sentenced to four months for contempt.
Texas is still engaging in a meaningless political stunt down on the border.
Now eventually the perception and the far-right rhetoric might actually create a dangerous
situation because there are right-wing groups traveling to the area in hopes of confrontation
with the federal government.
Meanwhile, the Trump faction of the GOP is also declining to support the toughest border
security legislation in decades.
Why?
Because they need the issue to scare and manipulate their base during the 2024 election.
That's right folks, Trump is anti-border security now.
Mr. Build the Wall does not want legislation passed that in some instances would literally
shut down the border.
And Trump is actively taking credit for attempts to block this bill, a bill that is realistically
tougher than any immigration legislation he got passed during his entire administration.
My guess is that he feels safe doing this because he knows his base is not going to
research anything.
They're just going to go off of what he says.
For centrists who are not part of the red hat world, this may have actually been an
incredibly smart move for the Democratic Party.
Because without Trump, it probably won't get passed.
But now, it exposed the Republican Party plan when it comes to immigration.
It illustrated very clearly that they don't actually care about the border.
That's just something they made up to appeal to people who live in Illinois.
Okay, Wall Street billionaire Clifford Asnes is mocking Trump and calling him a rhino over
Trump's pledge to bar Nikki Haley donors from MAGAworld.
It is worth noting that Clifford is a Nikki Haley donor and just is like, yeah, we don't
care.
Okay, moving on to cultural news.
Reporting suggests that about 1 in 10 are refusing to restart making student loan payments.
Unsurprisingly, right-wingers are coming out of the woodwork to come up with ways to force
them to.
And there is a novel new approach being proposed, which includes layaway for college.
Okay, environmental news.
The internet found out that alligators stick their noses through ice, like when a pond
or something freezes over, and it just went viral, and now there's a whole bunch of questions
about whether or not that's true in the inbox.
Yeah, alligators do that.
They are, they can survive pretty much anything.
I mean, they're a dinosaur.
Just look at it that way, okay.
There was a cold water coral reef
discovered off of the Eastern United States.
It's huge, by huge, I mean like the size of Vermont.
It's huge.
So there's probably gonna be a lot more
coming out about that.
It's also very deep.
This isn't something you're gonna be
snorkeling to go see but it is it's big and it's apparently a pretty big habitat
as well okay oddities explicit AI images of Taylor Swift spread across Twitter
the incident has led to calls from politicians and other celebrities to
criminalize that kind of image production. It is worth remembering that
the outrage and all of the animosity that is being directed towards Taylor
Swift did not actually start because she started dating a football player. It
started because she told young people that they should vote. It is worth
remembering that. If you want to know why the right wing is going after her,
That's why. It doesn't have anything to do with football. It started before then.
That's just a convenient excuse to whip up the base. Okay, and let's see. Q&A.
There's only a couple. Thanks for answering my fun Jeep question. I just
watched your video of the US leaving the Middle East and you mentioned they
wanted to go to Africa instead, given the state the US is leaving the
Middle East and it's handed to destabilizing the region, I'm concerned about that.
If you have family in Africa, you should be.
My question is, with all the issues and massacres going on in Africa right now that has been
mostly ignored by the media, not to mention China's presence over there already, do you
think the US presence there will help or hurt the people of Africa?
So will China view the U.S. presence in Africa as a challenge in any way?
And they're already there, are we in a near-peer contest?
Yes, near-peer contest, normal Cold War stuff.
Will it help or hurt?
It's going to be a mixed bag.
Some places will be helped, some places will be hurt.
It really is that simple.
Okay, so that looks like it.
I thought that was two questions, it's just one.
All right, so there's a little bit more information, a little bit more context, and having the
right information will make all the difference.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}