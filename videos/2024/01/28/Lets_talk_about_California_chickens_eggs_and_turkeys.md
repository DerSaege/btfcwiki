---
title: Let's talk about California, chickens, eggs, and turkeys....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=4t2bpHZ7Nio) |
| Published | 2024/01/28 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the history of a price increase in eggs and turkeys starting in 2022, peaking in January 2023, and catching media attention later in 2023.
- Attributes the price increase to the avian flu outbreak in 2022, which led to the destruction of 82 million birds.
- Mentions a recent issue in California where over a million birds have been destroyed due to avian flu, raising concerns about a potential repeat of the 2022-2023 situation.
- Warns that there is a high likelihood of prices going back up due to the current avian flu situation.
- Criticizes media outlets for sensationalism and fear-mongering, attributing the price increase to avian flu rather than blaming political figures like Joe Biden.
- Acknowledges that people may be more prepared to handle the situation this time, potentially preventing a massive bird destruction like in 2022.
- Notes that signs of avian flu spreading are already visible and that its impact on prices is a real concern.

### Quotes

- "No, it's Avian Flu's fault."
- "But outlets don't really want to provide information anymore."
- "Hopefully it won't spread to the point where 82 million birds have to be destroyed."
- "It's something to be aware of."
- "You have a good day."

### Oneliner

Beau explains the past and potential future impacts of avian flu on egg and turkey prices, urging awareness to avoid fear-mongering.

### Audience

Consumers, policymakers, farmers

### On-the-ground actions from transcript

- Stay informed about the avian flu situation in your area (implied)
- Support local farmers affected by potential price increases (implied)
- Take necessary precautions to prevent the spread of avian flu in your community (implied)

### Whats missing in summary

The full transcript provides a detailed background on the avian flu's impact on egg and turkey prices, urging proactive awareness and community action to mitigate potential threats.

### Tags

#AvianFlu #EggPrices #TurkeyPrices #ConsumerAwareness #MediaResponsibility


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about the price of eggs
and turkeys too, I guess.
And we're gonna talk about how it's very possible
that the price of eggs is about to go back up.
If you remember back in 2022 is when it started,
the price of eggs just increased and increased and increased
along with Turkey as well, the price of Turkey,
it peaked in January of 2023.
And it caught media attention near the end of 2023 as well,
because it was something that Republicans could point to
and say, look, inflation's out of control.
So what actually caused that price increase?
avian flu. In 2022, 82 million birds had to be destroyed. That's what caused that.
That's what caused the price increase. They started getting it back under control and
things have leveled off. And in California, there's another issue with avian flu. More
than a million birds have had to be destroyed over the last couple of months.
So the concern is that wild birds are migrating and that is infecting commercial birds and
that we may see a replay of what happened in 2022 and 2023.
So there's a high likelihood that prices are going to go back up.
This is why.
I figured I'd go ahead and talk about it now so there isn't a surprise when it occurs and
there's not a bunch of fear-mongering, which again, this is one of those things that rather
than provide context as to what was occurring during Thanksgiving, people were posting images
of turkeys and saying, look how expensive it is.
It's Joe Biden's fault.
No, it's Avian Flu's fault.
But outlets don't really want to provide information anymore.
They want to get clicks.
They want to get revenue.
So there is one upside to it, and that's that this is following so closely on the last time
that happened, that it's likely that everybody knows what to do.
So hopefully it won't spread to the point where 82 million birds have to be destroyed.
So it might be easier to control this time.
But we're already seeing the signs of it spreading.
And the potential for it to impact prices is definitely there.
So it's something to be aware of.
Anyway, it's just a thought.
You have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}