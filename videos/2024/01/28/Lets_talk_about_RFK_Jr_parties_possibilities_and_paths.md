---
title: Let's talk about RFK Jr, parties, possibilities, and paths....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=9W9umuNJRBk) |
| Published | 2024/01/28 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring different possibilities for RFK Junior's political pathways.
- Mentioning RFK Jr.'s controversial views influencing voting decisions.
- RFK Jr. considered primaring Biden as a Democrat and being Trump's VP.
- Mentioning the possibility of RFK Jr. running as a Republican or Libertarian.
- Concern about RFK Jr.'s various party affiliations potentially hurting him more than his controversial views.
- Noting the challenge in the United States with party loyalty and running independently.
- Mentioning that none of the pathways have a real chance of leading to victory.
- Emphasizing that running with Trump might be the only viable path.
- Speculating that RFK Jr.'s consideration of various paths may be damaging overall.
- Warning that indecisiveness or opportunistic behavior might deter supporters.

### Quotes

- "The various pathways when it comes to party affiliation, they may actually end up hurting him more than some of his more controversial views."
- "It's going to lead to issues to where even people who might have been interested, maybe they are supporters of some of his more controversial positions, they might be put off by what is going to be seen as either indecisiveness or opportunistic behavior."

### Oneliner

RFK Jr.'s diverse political pathways may harm him more than his controversial views, potentially leading to issues of indecisiveness or opportunism.

### Audience

Political observers

### On-the-ground actions from transcript

- Analyze the potential consequences of RFK Jr.'s varied political pathways (suggested)
- Stay informed about the evolving political landscape (suggested)
- Monitor RFK Jr.'s decisions and statements regarding his political affiliations (suggested)

### Whats missing in summary

The nuances of RFK Jr.'s controversial views and how they intersect with his political decisions.

### Tags

#RFKJunior #PoliticalPathways #PartyAffiliation #Opportunism #VotingDecisions


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about
a couple of different parties, possibilities,
pathways for RFK Junior,
because a number of events have occurred,
and it's just kind of worth going through all of them
and how it might end up influencing people a little bit.
Okay, so RFK Jr. has a lot of controversial views, which we're just going to kind of skip
over on this one because this is more about something that gets to the meat of how people
are influenced when it comes to voting decisions.
Kennedy started by wanting to primary Biden as a Democrat.
We also have recently found out that early on in the process, Trump's team reached
out to see if RFK would be interested in running as Trump's VP.
And the current thinking on that is to not write that off yet, because that's still
possibility. That would be as a Republican. He has just signaled his openness to also
running as a Libertarian. I know people that have been Republicans, Libertarians, and Democrats.
I don't know any that have been all of those things in a single election cycle.
The various pathways when it comes to party affiliation, they may actually end up hurting
him more than some of his more controversial views.
That's one of those things in the United States, the way party loyalty is.
idea that somebody isn't loyal to a party and is running for office is so
foreign that it may actually cause a much larger issue for for him than
anything else. It is worth noting that aside from being Trump's VP, none of
these paths are something that that have even a remote chance of leading to
victory. Running as the Libertarian candidate, no. An independent, totally
independent is something that has been brought up. There's also something about the
We the People Party and primaring Biden. All of these are at most messaging, at
worst vanity. Going with Trump, maybe there's a path there. You can't write Trump off yet.
But I feel like the, let's just say, eclectic nature of the different paths that Kennedy
seems to have at least considered is going to be damaging overall. It's going to lead
to issues to where even people who might have been interested, maybe they are
supporters of some of his more controversial positions, they might be
put off by what is going to be seen as either indecisiveness or opportunistic
behavior. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}