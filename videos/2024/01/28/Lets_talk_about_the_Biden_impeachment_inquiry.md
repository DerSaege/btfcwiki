---
title: Let's talk about the Biden impeachment inquiry....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=DoKYTT04K7E) |
| Published | 2024/01/28 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Discontent within the U.S. House of Representatives regarding the impeachment inquiry led by Comer.
- Republicans are frustrated with Comer for wanting actual evidence before engaging in impeachment.
- Republicans claimed to have evidence of widespread corruption and direct payments by Biden, but have failed to produce any evidence to support these allegations.
- Despite efforts, Republicans have been unable to find any impeachable offense committed by President Biden.
- The inquiry into Biden is being described by Republicans as a "parade of embarrassments."
- If Republicans lower their standard and resort to innuendo due to lack of evidence, it may backfire on them.
- Republicans might lose energy for discussing the impeachment inquiry and could potentially make it quietly go away or announce something absurd just before the election.

### Quotes

- "Their issue is that he actually wanted evidence for the high crimes and misdemeanors, not just to say it."
- "They told everybody what they were going to tell them and then they didn't have anything to tell them because they didn't have any evidence."
- "When you have the resources that this committee has trying to uncover anything and they can't come up with one impeachable offense, that actually speaks really highly of Biden."

### Oneliner

Discontent within the U.S. House over the impeachment inquiry, Republicans frustrated with lack of evidence against Biden, potential quiet end or absurd announcement looming.

### Audience

Political observers

### On-the-ground actions from transcript

- Contact your representatives to express your thoughts on the impeachment inquiry (implied)

### Whats missing in summary

Insights into the specific political dynamics and implications of the ongoing impeachment inquiry. 

### Tags

#ImpeachmentInquiry #USPolitics #Republicans #PresidentBiden #HouseOfRepresentatives


## Transcript
Well, howdy there, internet people, it's Bob again.
So today we are going to talk about Comer,
the U.S. House of Representatives evidence
and the Biden Impeachment Inquiry
and how things are shaping up or not shaping up
when it comes to that inquiry, okay.
So if you have missed it, there's been a lot of reporting
talking about discontent
within the U.S. House of Representatives.
and this isn't about the budget or the far-right MAGA faction or the Twitter faction or any
of the normal stuff that we've been talking about.
The discontent deals with the impeachment inquiry because remember, this has been going
on since Republicans took over the House.
It's kind of been running in the background the entire time, even before a formal inquiry
was announced.
Comer's been running this whole thing and they don't have anything to show for it and they're running out of time.
They have jumped from topic to topic, they have produced wild claims, but no evidence, and not just the base,
but other Republicans in the House are starting to get irritated with Comer.
That's the reporting. There is an incredibly interesting thing about it
though. If you sift through the reports and you look at the quotes most of them
are anonymous but you can find you can find trends from different outlets that
have reported on it and what you see is yeah there's definitely people talking.
But one of the reasons people are mad at Comer is that he wanted this this crazy
thing, this thing that you just can't imagine somebody would want. Evidence. I'm
not joking. If you sit down and read those quotes, you will realize their
issue is that he set the bar too high. Quote, he wanted payments like evidence
of payments. Quote, their issue is that he actually wanted evidence for the high crimes
and misdemeanors, not just to say it. That's the complaint. I'm going to be honest, that's
wild. I mean, yes, we all know it's a political show, but to say that to an outlet, to imply
that the reason people are angry is because the chairman over the committee that's looking
into it wanted evidence before engaging in impeachment.
That's a bad thing to the Republican Party today, I guess.
So they have gotten themselves into a little bit of a pickle because they told their base
that they had all of this evidence.
They said that there was widespread corruption.
They said that there were direct payments.
He did this in Ukraine.
He did that there.
Influence peddling, all kinds of stuff, and it turns out there's not evidence to support
any of it.
And because they kept blowing themselves out of the water, they really don't have anything
to move forward on.
They don't have something that they can turn into like a January 6th style set of hearings.
They can't do the tell them what you're going to tell them, tell them, tell them what you
told them because they told everybody what they were going to tell them and then they
They didn't have anything to tell them because they didn't have any evidence.
I gotta be honest, as somebody who looked into a lot of these allegations before they
went to the impeachment inquiry and all of this stuff, I knew the Ukraine thing wasn't
real.
That's pretty easy to demonstrate.
And I knew some of the other allegations weren't real, but the fact that the Republican Party
after all of this time has not been able to find one thing that Biden did wrong is amazing
to me.
I assumed that there would be something he did at some point that they could use to kind
to drum up some impeachment charges that obviously weren't going to make it through the Senate,
but something to give their base.
But apparently, they can't find anything.
That's quite the testament right there, to be honest.
When you have the resources that this committee has trying to uncover anything and they can't
come up with one impeachable offense, that actually speaks really highly of Biden.
And if they can't come up with anything, that will be one more thing that gets added to
the, quote, parade of embarrassments, which is how the inquiry is being described by Republicans.
I don't know that there's anything they can do to change this, because now, not just did
you have Comer saying he wanted actual evidence, but now that these criticisms are public,
If they lower the standard, it's going to become very obvious that that's what they
did.
They didn't have anything that they could back up with evidence, so they're going to
go with innuendo.
If they went that route, it would probably play out even worse for them.
My guess is you'll start to see Republicans lose their energy for talking about this.
And then they'll either make it go away real quietly or they'll wait until just before
the election and announce something absurd.
Those are your most likely options because at this point it doesn't look like they're
going to be able to find any evidence of any wrongdoing when it comes to President Biden,
which is amazing because please keep in mind he is a politician who's been up there a long
time.
So I wouldn't expect the impeachment inquiry to yield any real results at this point.
Anyway, it's just a thought y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}