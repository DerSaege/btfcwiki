---
title: Let's talk about changing American foreign policy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=NVxLKfh-8BU) |
| Published | 2024/01/28 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau opens talking about American foreign policy and the desire to change it due to dissatisfaction with its current state.
- He shares a message from someone expressing frustration with the inability to alter American foreign policy outcomes.
- Beau acknowledges the difficulty in changing American foreign policy, especially due to its intractable nature.
- Changing American foreign policy is deemed nearly impossible, requiring a shift within the United States first.
- Beau stresses the importance of initiating change at the local level through community networks for systemic transformation.
- He explains that altering American society is the key to eventually influencing American foreign policy.
- Building power outside of political parties at the local level is emphasized as a way to drive significant change.
- Beau mentions the necessity of not relying on existing political structures to bring about transformation.
- The message underscores that changing American foreign policy starts with domestic changes within the United States.
- Beau likens altering American foreign policy to a game of world domination, requiring networks and bases of power.

### Quotes

- "Power at the local level because that's what's going to give you that deep systemic change."
- "When you actually get to the point where you are really trying to do more than just mitigate the effects of American foreign policy but you're actually trying to change it. You are literally engaging in a game of world domination."
- "It almost always is. The hardest part to really wrap your mind around is that it is going to be the last thing fixed."

### Oneliner

Beau stresses community networks and local power as the key to changing American foreign policy, which must start with altering American society.

### Audience

Activists, Community Organizers

### On-the-ground actions from transcript

- Organize at the local level to build power and create systemic change (suggested)
- Initiate community networks to drive transformation within American society and eventually impact foreign policy (exemplified)
- Avoid relying solely on political parties; establish parallel structures outside of existing systems to effect change (implied)

### What's missing in summary

The full transcript provides detailed insights into the necessity of grassroots organizing and community empowerment to drive systemic change within the United States, leading to eventual shifts in American foreign policy.

### Tags

#AmericanForeignPolicy #CommunityOrganizing #LocalPower #SystemicChange #GrassrootsMovement


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about American foreign policy
and how to change it, how to change it.
That is something that a whole lot of people
are struggling with right now
because they don't like what American foreign policy
is doing, what it is perceived to be doing,
what it's actually doing.
And they want to change it,
But they realize that it's kind of intractable, you know?
And I got a message that I'm going to read relevant parts
of, because it's kind of long, that I think
might shed some light on some things.
I've left a lot of rude comments over the last couple of months.
I hate to admit that you've been right about how things would
play out, but after the disappointment that is the ICJ,
I went back and watched some of your videos again.
It still makes me mad, but in hindsight, it seems obvious
that's how it would play out.
Yes, please keep in mind that I don't actually make these
entities do these things.
I just have a pretty good idea of what they're going to do.
And then there's a long section here that is basically
expressing their frustration with not being able to alter the outcome, that they feel
like that they can't do anything.
I asked somebody who actually knows me pretty well how you think we could change, could
ever change American foreign policy, and she said that you would say community networks.
I'm a leftist, but come on, I don't want to be rude, but how the heck is that supposed to work?
I get it. If you haven't figured it out yet, actually altering American foreign policy is
almost impossible. Altering the foreign policy of most countries is almost impossible. That is
incredibly true when it comes to the United States. It's pretty intractable.
You can mitigate, but you can't really change much. Why? Because it's the last thing that'll change.
American foreign policy is the last thing that will change. Those people that
want deep systemic change, you want to change this country, you want to change
the world. Understand changing the world happens after the change occurs in the
United States. US foreign policy will be what it is right now until that change
occurs within the US. And just in case there's not enough pressure there,
understand it also it has to start here nowhere else that deep systemic change
it can't occur in some other country can't start in France it has to start
in the United States maybe China but there's too many control systems in
place there not gonna happen it has to start here why because let's be real if
a country somewhere engaged in some deep change, developed an egalitarian society
where everybody was going to get a fair shake, at least closer to it, and then
started making moves on the international scene, what would happen?
We'd invade them. That's not a joke. That's actually what would occur. It has
to start here. So, the question isn't how do you change American foreign policy,
it's how do you change American society? And yeah, she's right. Power at the local
level because that's what's going to give you that deep systemic change doesn't
matter what your pet issue is, whether it's labor organizing or you want to be
able to get trans people out of states that have become hostile to them or
anything else, the power rests at organizing at the local level. You want
a third party, you want to change things so people don't have to look between the
Democratic Party and the Republican Party, or you want to fundamentally alter the
Democratic Party, guess what you have to do? Start at the local level and build
that power. And that power has to exist outside of those political parties. You
can't use the party structure to do it, because then it just becomes part of the
party. It has to exist outside. A parallel system, a parallel structure, it is a
power base unto itself. Therefore, it can put people into positions, it can put people into
political positions, it can swing elections. Those people who become representatives, hey,
they can start altering things. When we get to the point where things are changing within the
United States, then you could start looking overseas and how the U.S. interacts with the
the rest of the world.
The part that is amazing to me, this part at the end of the message here, look, I'm
a leftist.
For at least a decade, leftists in the United States have been running around screaming
the U.S. is not a democracy, it's an oligarchy.
And then when the U.S. acts like an oligarchy, there's like surprise.
shouldn't be. When you are talking about altering American foreign policy, it has
to start in the United States. You have to change things domestically first. And
the important thing to remember when you actually get to the point where you are
really trying to do more than just mitigate the effects of American foreign
policy but you're actually trying to change it. You are literally engaging in
a game of world domination. Yeah, you're gonna need help. You're gonna need a
team. You're gonna need a network and that network is gonna have to network
with other networks and you're going to have to build a base of power because
yes, it is a literal game of world domination and you are attempting to go
toe-to-toe with nation-states. It's a little bit more difficult. It's not the
same as trying to sway local politicians on a domestic issue. It's not
about public opinion that might swing an election and therefore the people in
office who perceive a threat to their own power, they adjust themselves so
they're not upsetting you. Now you are talking about entire countries. The
The machinery that keeps that country moving, it does not care about your hashtag.
You have to actually build power, build power for yourself, for your cause.
You can't just borrow it from an elected official.
Yeah, she's right.
It's organizing at the local level.
That's the solution.
It almost always is.
The hardest part to really wrap your mind around is that it is going to be the last
thing fixed.
The domestic stuff is going to come first.
The systemic change will occur within the United States before American foreign policy
ever starts to reflect that.
And it has to start here.
Because if it doesn't,
we will invade them.
Because that's what the machinery is set up to do.
If you want to
influence
the situation in the Middle East,
I am not joking when I say this, you are better off installing solar panels, buying an electric
car, something like that, because it alters the dynamics.
It makes the U.S. less likely to need to be involved over there, and by need to be I mean
that machinery
proceeded that way.
It's very upsetting,
but you're literally
talking about changing the entire world.
Nobody has ever said that was going to be easy.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}