---
title: Let's talk about China, Taiwan, surveys, and media consumption....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=2m-A9n_tZu0) |
| Published | 2024/01/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the perceptions and misinformation surrounding China, the United States, and Taiwan.
- Questioning the idea of China invading Taiwan, debunking mainstream news narratives.
- Addressing the concept of nut-picking and cherry-picking data to create biased narratives.
- Revealing that the majority of experts do not believe China can successfully invade Taiwan.
- Emphasizing the high cost and unlikelihood of China pursuing an invasion.
- Mentioning the bias in media coverage, favoring dramatic narratives over expert opinions.
- Pointing out that sensationalism in news leads to misinformation and inaccurate perceptions.
- Encouraging seeking accurate information rather than sensationalized news for a better understanding.
- Concluding that the idea of a Chinese invasion of Taiwan is not a realistic scenario in the near future.

### Quotes

- "Most people, the overwhelming majority of people who really understand this stuff, they don't think China's invading, not anytime soon."
- "Don't look for things that are just sensationalist because by definition, it won't be good."
- "It's pretty universal thinking that [a Chinese invasion of Taiwan is] not really on the table anytime soon."

### Oneliner

Beau debunks mainstream narratives, revealing the unlikelihood of a Chinese invasion of Taiwan amid sensationalized media coverage.

### Audience

Global citizens

### On-the-ground actions from transcript

- Fact-check news sources for accurate information (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the misinformation and biased narratives surrounding China, the United States, and Taiwan, urging viewers to seek accurate information beyond sensationalized news for a better understanding.

### Tags

#China #Taiwan #MediaConsumption #Misinformation #ExpertOpinions


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about China
and the United States, Taiwan, information consumption,
media consumption, guarding against things
and how people end up with a perception
that may not be entirely accurate.
Okay, so we have this question.
A couple of international affairs people I follow
seem to be saying that China can't really invade Taiwan. And this was before
the news about their corruption issues. I think you're in that group that thinks
they can't. But most news seems to say they can. What do those who hold your
opinion know that everybody else doesn't? Are you guarding against groupthink?
Okay, so we have talked about it before when you're talking about bad intelligence
assessments, a lot of times it is from people talking to each other and then
making their assessments match each other unintentionally because somebody
is very persuasive in their own assessments. Everybody moves theirs
closer to match that one. The person who is most persuasive may not be right.
That's not actually what's going on here. That's a different, there's a different
happening here. When you're talking about looking at data, you have the concept of
cherry-picking data, right? Looking for the information that is most supportive
of what you want it to show. There's also a thing called nut-picking, where you
purposely go find the outliers for whatever reason. If you were to watch
most major news outlets you would definitely believe that most experts
believe that China can absolutely invade Taiwan and win because that's who they
go talk to because that's far more interesting than the reality which is
that the overwhelming majority of experts don't even think they can pull
off the invasion, much less the actual hard part, the occupation afterward, and
it's not even close to be clear. In fact the Center for Strategic and
International Studies did a survey recently of some of their affiliated
experts, 87 of them. 52 were from the US, 35 were from Taiwan. Out of those surveyed,
75%, roughly three quarters, didn't even think China could pull off the amphibious invasion
part. And those who were very confident that they could, like it was like one or two percent,
was not a large group within that survey. I personally, whether or not they can do
it, I don't know. There's a lot of fog of war stuff that would impact that,
whether or not they could get through the invasion part. But what I do know is
that even if they were able to, it would come at such a high cost that they
wouldn't want to and I believe that China knows that which means they're
not likely to pursue that option. And it went through and looked at other things
as well like whether or not people could, whether or not China could quarantine the
island using law enforcement or maybe do a blockade using the military and this
is a good way to see whether or not there was bias at play. You know, you know
Chinese military, they're not any good. No, pretty much everybody thinks they could
do a blockade. So it's not a biased thing. It's not underestimating their military.
It's just that amphibious invasions are like super hard. And what occurs is that
media outlets, they want the drama. They want the high drama. They're not
going to bring in an expert that's going to be like, no, I don't think they're
going to do that. I mean that seems really illogical on that part and you
know they're people, you know, they're not crazy. That's not good TV.
Good TV is, oh that foreign government over there, they're bad. Those people, they
don't think about things logically. Of course they're going to invade. It's not
a matter of the assessments themselves. It's a matter of which ones get
elevated in the media. Most people, the overwhelming majority of people who
really understand this stuff, they don't think China's invading, not anytime soon.
They elevate those who say they do believe that because it's good for
clicks. It's really that simple. I'll try to find that survey and put it down below
so you can look at it. There are a number of situations where stuff like
this comes into play, where the perception of how things would play out
is definitely wrong in the view of the American public because they were
misinformed because outlets intentionally sought out people who held a
contrarian opinion and it wasn't to guard against a group thing. It's just
that the traditional assessment is boring and doesn't get ratings. So maybe
news for profit in that fashion where it's driven by ratings isn't good. Maybe
it leads to people being misinformed. Again, when you're trying to get good
information, don't look for things that are just sensationalist because by
definition, it won't be good. It won't be accurate because they're trying to make
it more interesting. They're trying to make it, you know, groundbreaking, earth
shattering. Don't look for that, just look for the information and you will find
out that when it comes to Taiwan and China, the idea of an invasion, it's
pretty universal thinking that that's not really on the table anytime soon.
Anyway, it's just a thought.
Just a thought.
So have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}