---
title: Let's talk about Trump's own expert and his filing....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=wDnHMrp0mGE) |
| Published | 2024/01/03 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's recent filing lacks substance and fails to address key points raised by Smith and amicus briefs.
- Ken Block, Trump's expert hired to find voter fraud evidence, stated there was none, directly contradicting Trump's claims.
- Block's findings were shared with Mark Meadows, presenting a challenge for Trump.
- Block emphasized that voter fraud claims undermining democracy should be based on evidence, not falsehoods.
- Block advocates for addressing systemic weaknesses like gerrymandering to ensure fair elections.
- The expert suggests creating national infrastructure for elections and ending gerrymandering.
- Trump's team's own expert's suggestions go against the narrative of election fraud.
- Legal channels on YouTube likely to cover the issue extensively.
- The filing in question is 41 pages long but lacks new substantial information.
- Trump's persistence in false claims despite being informed of no fraud by his chosen investigator.

### Quotes

- "The expert that they chose flat out that there was no fraud."
- "Maintaining the lies undermines faith in the foundation of our democracy."
- "Leveling the playing field, ending gerrymandering, and creating national infrastructure for the elections. That's his suggestion."
- "It's wild to me."
- "Trump's statements persist to this day anyway."

### Oneliner

Trump's expert refutes voter fraud claims, undermining his own narrative while advocating for fair elections and national election infrastructure.

### Audience

Legal analysts, political commentators

### On-the-ground actions from transcript

- Contact legal channels on YouTube for in-depth analysis and commentary on the filing (implied).

### Whats missing in summary

Analysis on the potential impact of Trump's persistence in false claims despite evidence against them.

### Tags

#Trump #VoterFraud #ElectionIntegrity #LegalAnalysis #Gerrymandering


## Transcript
Well, howdy there, internet people, it's Bill again.
So today we are going to talk about Trump's own expert
and the statements that have recently come out.
And we will talk about Trump's filing.
We're gonna cover two pieces of Trump news at once.
Let's start with the filing.
The filing has to do with his immunity claims.
He got it in last night.
I mean, I'm not a lawyer,
but there's nothing new in it. There is nothing there designed to refute what Smith said in his
filing. It doesn't even look like they even attempted to address the amicus briefs. I've
never seen anything like it, to be honest. I feel like it's not going to carry a whole lot of weight
with the Abills Court. Okay, so moving on to what may be the more interesting
development. Ken Block. Ken Block is the expert that was hired by the Trump
campaign to find evidence of voter fraud, okay. He said there wasn't any. He said
there wasn't any and more importantly said that those findings were
transmitted to then Chief of Staff Mark Meadows and has since in an interview
indicated that both Smith and Fulton County have access to that. That's going
to present a problem for Trump. The expert that they chose flat out that
there was no fraud. Nothing that would change the election. If voter fraud had
impacted the 2020 election, it would have already been proven. Maintaining the
lies undermines faith in the foundation of our democracy. That's what Block wrote
in an op-ed. Another piece of this that I find incredibly interesting is what he
says is actually the concern. When it comes to preserving our elections, what
we need to do. Address systemic weaknesses in our election systems such
as the distressing lack of national election infrastructure to enforce
election integrity. Destructive practices to our elections such as
gerrymandering and leveling the playing field so that our elections become fairer
and more competitive." Yeah, so leveling the playing field, ending gerrymandering,
and creating national infrastructure for the elections. That's his suggestion.
That's the expert that the Trump team hired. That's what he's suggesting. I
I mean, sounds good to me.
Sounds good to me.
So all of this is obviously going
to cause an issue for the former president.
We will probably hear about the appeal.
We'll probably hear about that first, the filing.
I would imagine that the legal channels on YouTube
are going to be all over this tomorrow.
It's like 41 pages, and there's nothing new in it.
it's wild to me. So I would wait and see what they say. Maybe I miss
something or I don't understand what this filing is supposed to be, but I was
definitely not impressed by it. And then Trump's chosen election investigator
told him way back then that there there was no fraud that what he was alleging
just didn't happen but Trump's statements persist to this day anyway
It's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}