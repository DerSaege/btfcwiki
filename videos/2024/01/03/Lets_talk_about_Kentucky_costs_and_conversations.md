---
title: Let's talk about Kentucky, costs, and conversations....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=GeKWEoYnDX4) |
| Published | 2024/01/03 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about Kentucky, Bill's, a long timeline, and Kim Davis.
- Kim Davis, former county clerk in Kentucky, refused to issue marriage licenses to gay people in 2015.
- Davis was jailed for five days and lost re-election in 2018.
- She was ordered to pay $100,000 to the people she refused, and now faces an additional $260,000 for legal expenses.
- Beau questions the importance of religious beliefs when acting as a government representative.
- Points out the conflict between sincerely held religious beliefs and government duties.
- Emphasizes that as a government agent, one cannot allow religious beliefs to interfere.
- Notes the Constitution prohibits favoring a religion when in a government position.
- Warns against using government power to enforce personal religious beliefs.
- Raises concern about politicians who aim to violate the Constitution and enforce religious beliefs through government power.

### Quotes

- "When somebody is acting as an agent of government, their religious beliefs should not matter."
- "You cannot allow your religion to interfere with that."
- "The government cannot favor a religion."
- "If you take a government position you shouldn't be allowed to use that position to further your religious beliefs."
- "There's a whole lot of people running right now who are basically promising to shred the Constitution as soon as they go in."

### Oneliner

Beau reminds that government agents should not let religious beliefs interfere and warns against politicians who aim to enforce religious beliefs through government power in violation of the Constitution.

### Audience

Politically conscious individuals.

### On-the-ground actions from transcript

- Ensure government representatives prioritize their duties over personal religious beliefs (implied).
- Advocate for the separation of religion and government in political discourse (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the Kim Davis case, touching on the intersection of religious beliefs and government responsibilities, urging caution against politicians who seek to use governmental power to impose personal religious beliefs.

### Tags

#Kentucky #KimDavis #ReligiousBeliefs #Government #Constitution


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Kentucky,
and Bill's, and a really long timeline, and Kim Davis.
That is a name that you may not remember,
or if you do, you may have trouble placing it.
She is the former county clerk in Kentucky
that became just world-renowned because she refused an order that said she had to
issue marriage licenses to gay people. That occurred way back. She was jailed over that in
2015. She was jailed for five days and went on to run for re-election in 2018
and she lost re-election to a Democrat by a wide margin. The proceedings have
been going on for quite some time, obviously. She was ordered to pay a
hundred thousand dollars to the people she refused and now the most recent
development is she has been ordered to pay two hundred and sixty thousand
dollars on top of that for their legal expenses. So we're talking about three
hundred and sixty thousand over this. So I mean there's a couple of obvious
things to point out here especially you know little sound bites we often hear on
the channel about you know it's cheaper to be a good person and all of that
stuff but I think it might be more important to remind everybody that when
somebody is acting as an agent of government, when they're acting as a
representative of government, their religious beliefs should not matter.
That's one thing that always gets lost in this conversation because people talk about,
well, you know, the court told her to do this and this happened and this was the law and
all of that.
At the end of it though, the refusal was based on, which I have no doubt, they are sincerely
held religious beliefs.
At the same time, when you are operating as an agent of government, you cannot favor a
religion.
You cannot allow your religion to interfere with that.
That seems to be how the Constitution was written.
And that's a part of it that gets lost, and it seems really important to remind everybody
because you have a lot of people making religious arguments and saying that
government should do this and this is what they would do based on their
religious interpretation and all of that stuff. The government cannot favor a
religion so if you take a government position you shouldn't be allowed to use
that position to further your religious beliefs.
This is a big part of this conversation and it needs to be remembered because there's
a whole lot of people running right now who are basically promising to shred the Constitution
as soon as they go in and use the power of government to enforce their religious beliefs.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}