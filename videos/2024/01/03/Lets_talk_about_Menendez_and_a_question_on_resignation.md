---
title: Let's talk about Menendez and a question on resignation....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=0ZGBxcWQL4w) |
| Published | 2024/01/03 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Provides an update on Senator Menendez from New Jersey and the allegations against him, shifting from Egypt to Qatar and extending into 2023.
- Mentions that the allegations involve business deals and investments.
- Democrats have been calling for Menendez to resign since the first set of allegations in September, and more calls came with the superseding indictment.
- Explains that despite the calls for resignation, it is ultimately up to Menendez to decide.
- Notes a shift in tradition where politicians facing allegations used to resign once indicted, but that tradition seems to be fading.
- Speculates on Menendez's potential response to the superseding indictment and mentions his intention to take the matter to trial.
- References Menendez's past involvement with gold bars and the evidence supporting the allegations against him.

### Quotes

- "Their calls don't really mean anything."
- "Resignation is voluntary."
- "The tradition seems to be to try to stay in office as long as possible."

### Oneliner

Senator Menendez faces calls for resignation amidst shifting allegations, with Democrats urging him to step down, though the final decision rests with him.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Contact your representatives to voice your opinion on political accountability (implied)
- Stay informed about the developments in political scandals and hold elected officials accountable (exemplified)

### Whats missing in summary

Full details and context surrounding Senator Menendez's legal troubles and the Democratic calls for his resignation.

### Tags

#SenatorMenendez #PoliticalScandal #Democrats #ResignationCalls #Accountability


## Transcript
Well howdy there internet people, let's bow again. So today we are going to talk about Senator
Menendez from New Jersey, provide a little bit of an update on what's going on with that, you know,
just in case you haven't already heard enough about the possibility of political insiders using
their influence to perhaps assist foreign powers. So we're going to provide an update on that and
then we're going to answer a question that has come in a few times about Menendez. Okay,
so quick recap. The senator from New Jersey is already in hot water with the feds and has already
been indicted. Most of the initial allegations dealt with Egypt. There is now a superseding
indictment that kind of focuses on Qatar and it extends the conduct into 2023.
Now what is available right now?
It looks like most of it is stuff related to business deals, investments, stuff like
that.
Those are what the allegations are.
Now one of the questions that has come in about this is why Democrats aren't calling
for him to resign.
And this has come in like a few times.
The simple answer there is they are.
They are.
It started back in September with the first set of allegations.
And by the end of September, a majority of senators who are Democrats had called for
him to resign.
There are already new calls for him to resign based on the superseding indictment.
So the question is, you know, well, why hasn't he?
Because a call for resignation is still a call for resignation.
Resignation is voluntary.
It used to be the standard tradition was if you were being investigated, well, that was
one thing, you could stay, but once you were indicted, you needed to resign for the good
of the party.
And that tradition kind of held up for the most part up until just a few years ago.
And now that's...
It isn't a tradition anymore.
The tradition seems to be to try to stay in office as long as possible.
So it isn't that Democrats aren't calling for him to resign.
They are in large numbers and vocally.
It's just that their calls don't really mean anything.
It's still up to the person who is facing the allegations.
We'll see if the superseding indictment changes anything, but based on the statements coming
from Menendez, it's probably not going to change anything.
So that's what's going on, and if you remember, Menendez...
This is the person who had the gold bars.
There appears to be a decent amount of evidence supporting the allegations against him.
So we'll have to wait and see what he does as far as any potential resignation and then
wait and see how it plays out in court because based on public statements, he's going to
take this to trial.
Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}