---
title: Let's talk about Foreign Agents and another Former Trump advisor....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=cQ21KPgodwM) |
| Published | 2024/01/03 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- GOP lobbyist Barry Bennett is charged with falsifying records related to foreign agent reporting.
- Being a foreign agent does not always involve espionage; sometimes it means representing a foreign power's interests legally.
- The charges against Bennett seem minor, more like driving without a license in the foreign agent world.
- The issue was the lack of disclosure and alleged falsification of records, not conduct damaging to the United States.
- Despite buzzwords like "foreign agent" and "former Trump advisor," this situation seems less severe compared to other allegations surfacing.
- Proper paperwork could have prevented this issue, and it appears to be representing foreign interests without harm to the US.
- Bennett's case may receive significant coverage due to the keywords involved, but it's vital to understand the actual allegations.
- It's emphasized that representing a foreign power doesn't necessarily mean doing so to the detriment of the United States.
- The situation may evolve as more information emerges, but currently, it seems less serious than it may appear.
- Pay attention to the details of the conduct mentioned in the allegations for a clear understanding.

### Quotes

- "Foreign agent doesn't necessarily mean cloak and dagger, cat and mouse espionage stuff."
- "This is pretty minor stuff in comparison to a lot of other allegations that are surfacing right now."

### Oneliner

GOP lobbyist faces charges for falsifying records related to foreign agent reporting, but the issue seems minor compared to the buzz it's generating.

### Audience

Legal analysts, political commentators

### On-the-ground actions from transcript

- Examine the actual allegations and conduct mentioned to get a clear picture (implied).

### Whats missing in summary

Context on the broader implications of foreign agent charges and the importance of proper disclosure and paperwork.

### Tags

#BarryBennett #ForeignAgent #TrumpAdvisor #LegalIssues #USPolitics


## Transcript
Well, howdy there, internet people, it's Bill again.
So today we are once again going to talk about
foreign agents and former Trump advisors
because a situation has once again arisen.
And we'll go through what's there and what's not
because this is one of those
that's probably going to get a lot of coverage
but maybe not a lot of context.
Okay, so if you missed the news and have no idea what I'm talking about, a pretty prominent
GOP lobbyist by the name of Barry Bennett has gotten into some hot water with the Feds.
Bennett, if I remember correctly, he was a campaign manager for Ben Carson, then went
on to become an advisor to Trump of sorts, and then went and did consulting work, like
lobbying stuff. Now the feds have charged him with falsifying records related to
the foreign agent reporting thing. We have talked about it before on the
channel. Foreign agent doesn't necessarily mean cloak and dagger, cat
and mouse espionage stuff. Sometimes it just means representing the interests of
a foreign power. And at times that's legal. You're allowed to do it in certain
situations if it's disclosed and if all of the the t's are crossed and i's dotted.
Based on an early reading of the documentation that's available, and this
could change because more information could come out, this appears to be the
foreign agent version of driving without a license. I don't see anything in the
actual conduct that appears to be damaging to the United States. It's that
it wasn't disclosed and I guess the feds are alleging that records were falsifies,
falsified along the way. But the conduct, it doesn't actually look like horrible.
This has nothing to do with trading secrets or anything along those
lines. The reason I'm saying this is because this is undoubtedly going to get
a whole bunch of coverage because it has those phrases. Foreign agent, former
Trump advisor or whatever, it's going to get a lot of coverage. This is a
situation where one of these things is not like the others. Based on what I've
seen, had the paperwork been done correctly, this wouldn't have been an
issue. But again, it's early and more information could come out later. But
But early on, it's not something to overlook because you can't allow people to not do
this kind of paperwork.
But it isn't what people may initially believe when they see foreign agent.
This is pretty minor stuff in comparison to a lot of other allegations that are surfacing
right now. So it's just something to be aware of because because of the key
words involved in this it's going to get a ton of coverage. I would look through
the the actual allegations of what they're talking about in the conduct to
get a clear picture of what it is. Most of it is just plain and simple
representing the interests of a foreign power, not representing the interests of
a foreign power to the detriment of the United States. Again, it could change
later, but right now that's the read on it. So we'll have to wait and see, but
just be aware that this may get outsized coverage. Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}