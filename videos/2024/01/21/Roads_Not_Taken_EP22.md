---
title: Roads Not Taken EP22
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=vevFTFuTAfo) |
| Published | 2024/01/21 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Covers unreported or underreported stories from the previous week, aiming to provide context and coverage that these stories deserve.
- Beau mentions feeling unwell but proceeds to cover foreign policy, including the Biden administration's stance on Netanyahu and diplomatic efforts against Russia.
- Reports on various incidents in the Middle East, hinting at a potential wider conflict due to escalating tensions.
- Talks about Nikki Haley's criticism of Trump and her endorsement by Asa Hutchinson.
- Mentions the Los Angeles Innocence Project taking up Scott Peterson's case and speculates on the possible outcomes.
- Shares good news about Massachusetts Senate repealing anti-LGBTQ laws and endorsements in politics.
- Comments on Trump's base believing in rigged caucuses, environmental news on weather-related deaths, and oddities like a pastor getting arrested for helping the homeless.
- Updates on NASA regaining contact with its helicopter on Mars.
- Addresses questions about Trump's hands and working while sick, ending with thoughts on Trump's strategy at trials and the public's reception to it.

### Quotes

- "I think it's a little cynical to think the U.S. would invade Canada over water."
- "Those contingency plans, they get drawn up pretty quick. It's not cynical. It's the reality of it."
- "Relatively short episode this week, so a little bit more information, a little bit more context, and having the right information."

### Oneliner

Beau covers unreported stories, comments on Nikki Haley, environmental news, and addresses public perception of Trump's trial strategies.

### Audience

News consumers

### On-the-ground actions from transcript

- Contact local representatives to push for repealing discriminatory laws (suggested)
- Stay informed about political endorsements and implications (suggested)
- Support organizations like the Innocence Project in their efforts for justice (suggested)
- Stay updated on climate-related news and prepare for potential weather-related disasters (suggested)

### Whats missing in summary

Insight into Beau's unique perspective and analysis on current events. 

### Tags

#UnreportedStories #NikkiHaley #EnvironmentalNews #PublicPerception #Justice


## Transcript
Well, howdy there, internet people.
It's Bo again, and welcome to The Roads with Bo.
Today is January 21st, 2024,
and this is episode 22 of The Roads Are Not Taken,
which is a weekly series where we go through
the previous week's events and cover some stories
that were unreported, underreported,
or I just didn't think they would get the coverage
or context that they deserved.
This will probably be a short installment, as many of you have been telling me over the last couple of days.
I don't look good.
I don't feel good either.
Okay.
But we will start with foreign policy.
The Biden administration states that Netanyahu is not opposed to all two
state solutions, just some of them.
But the general tone coming from the White House and State Department right now is not,
It's optimistic.
The US is mounting a diplomatic campaign to convince countries to kind of tighten the
screws on Russia's economy and mentioning the use of secondary sanctions in order to
encourage countries to see the US's way of thinking.
Two British warships collided in a Bahrain port, says there were no injuries but there
was definitely some damage.
There has been some exchanges, some strikes throughout the Middle East in a couple of
different places.
Some of them are likely to cause responses and again there is a steady, a slow and steady
escalation there that is starting to get a little concerning when you are talking about
the possibility of a much wider conflict.
Okay, moving on to US news, Nikki Haley is finally coming out swinging left and right.
She is skewering Trump pretty much every chance she gets now to include stuff like, do you
You know, I had to sit down with him when we were in the administration and tell him
to stop his bromance he had with Putin.
It's dangerous.
You don't do that.
It might be a little bit too late in the game for this to have any effect.
Had she done this right out of the gate, I think we would be looking at a very different
race right now.
But she, I think she might have held back too long.
Okay, in a very surprising turn of events, something that kind of came out of nowhere,
the Los Angeles Innocence Project is taking up the case of Scott Peterson, who was...
This is a case that's from a few decades ago, a couple decades ago.
I would imagine that this is going to turn into just a giant show.
going to turn into a thing, with a lot of questions, a lot of interviews, and rehashing
a whole bunch of stuff.
And then we have to find out why the Innocence Project decided to take it up.
So I would wait to form an opinion on any of that.
Okay, some good news.
The Massachusetts Senate voted to repeal the walking while trans law and some other anti-LGBTQ
laws that were on the books.
We'll see how it plays out as it moves forward.
Expectations are high that that's going to go well.
Asa Hutchinson has endorsed Nikki Haley.
Moving on to cultural news, or maybe oddities, I don't know.
Trump's base believes that the most recent caucus, they believe that was rigged, even
though Trump won, because he didn't win by the margins they think he should have.
They once again have absolutely no evidence to back up this claim, but I mean, it's a
good way for those people who push these rumors to continue to get clicks and
make money off of their followers. It's really what it is at this point. Okay in
environmental news the Arctic blast that hit the US created at least 80 weather
related deaths, there's a high likelihood that we're going to see an almost immediate
shift from the cold to warm weather, along with rain and snow melt.
So once this is over, we need to be on the lookout for flooding, because that will be
next. Okay, in oddities, a pastor at a place called Dad's Place Church in
Williams County, Ohio, started keeping it open 24-7 in March of 2023 because the
local homeless shelter was getting filled up. So, you know, tried to
help. Of course, he's now been arrested for trying to help, you know, in a state
that constantly uses Christianity as an excuse to curtail people's rights and
strip them of their rights and tell them what to do and rule over them. When
somebody actually, you know, embodies a Christian principle, they arrest them for
helping the homeless and getting people off of the street and feeding them
because that's the real concern.
It is interesting to note
that all of those people who over the last few months when it was trying to
curtail people's rights
came out to talk about how important Christianity is and what it means to Ohio,
none of those people can be found right now. They're just gonna let this pastor go
to jail for helping people.
NASA lost and then regained contact with its helicopter on Mars. Currently, at time of filming,
they can communicate. We'll see if that holds up. OK, moving on to the Q&A portion.
If you want to send one in, it is question for Bo at Gmail. OK.
Okay, what's up with Trump's hands is Carville Wright.
If you don't know what that's about, there are some marks that have appeared on Trump's
hands.
They have been attributed to a number of things to include some medical issues.
That I mean, it is what it is.
That will undoubtedly continue to be a story until something comes of it.
I don't have an opinion, to be honest.
Why do you work when you're sick?
You can take a day off.
Yeah, I know that.
I mean, you all have to keep in mind, I don't work in a coal mine or fast food.
I'm not an over-the-road trucker.
I don't have a hard job.
And while I don't have a hard job, people in those jobs I've listed, they listen every
day.
I can drag myself in here for that period of time.
I don't think it's a huge ask to do that.
As long as people like that have to go in sick, I can show up and, you know, sit here.
Again, I don't have a hard job.
And it's not like I'm not at risk of spreading whatever I have to anybody else.
Okay.
Trump seems to have a new strategy, namely to use his presence at his trials as kind
of a mini rally where he gets to speak at the steps of the courthouse without counter
argument. However, the discrepancy between his narrative and what is being said in court,
the truth based on facts, should be more and more clear to the public, especially when the trials
keep piling up. So my question is, do you think that his presence and behavior at the trials will
yield less and less results in the sense that the public won't be paying attention any longer to his
rhetorics, but more to what actually happens inside the courtroom. There is definitely going
to be a point of diminishing returns on this. You're right, it does seem like he's trying to
get a bunch of free publicity and hijack the coverage. I don't think that that's going to
be successful for long, especially once the verdicts start being returned. I feel like those
people who are going to believe the nonsense he says on the courthouse steps, they didn't
need any encouragement.
But there are going to be those people who see what he says and then see what actually
happened and it might undermine him.
He has a base that is very loyal to him and that over time, they still haven't shifted.
But it's worth remembering, he has lost support.
He lost the last election.
His chosen candidates lost the midterms and he's not doing great in the primary for the
position that he's supposed to be in.
So I think over time it's going to hurt him.
You said you think the U.S. would invade a country over fresh water.
I think it's a little cynical to think the U.S. would invade Canada over water.
I know you say countries don't have friends, but jumping from allies to war seems almost
impossible.
It doesn't seem like anything that could actually occur.
Yeah, I get it, because the US and Canada
have been friends for so long, very shared interests.
There is no way that we would even
think about invading Canada, nor Canada
invading the United States.
I'm going to give you two things to Google.
The first is War Plan Red.
That was the U.S. plan to invade Canada.
That was totally a thing.
We were going to invade.
And there are actually some really horrible parts to that plan to include the use of chemicals.
And if you think that that's just a hyper-militant American thing, I would ask you to look up
something called Defense Scheme 1 because that's the Canadian plan for invading the U.S.
That was mainly over politics. Those two plans were developed over politics.
When you're talking about an actual concern over natural resources, those contingency plans,
they get drawn up pretty quick. It's not cynical. It's the reality of it. Now, I don't think that
that's going to occur anytime soon. And I would like to think that before that ever happens,
there are agreements that are put into place. But it's not something that would just be taken
off the table. I can assure you of that. Okay, so and that looks like it. I think
they were very nice to me and did not include a bunch of questions on purpose.
Okay, so that looks like it. Relatively short episode this week, so a little bit
more information, a little bit more context, and having the right information
We'll make all the difference y'all have a y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}