---
title: Let's talk about Trump, Nikki, and Nancy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=iIZQPOu4zrY) |
| Published | 2024/01/21 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump showed confusion at a campaign rally, referencing a rumor about Pelosi, but actually meant Nikki Haley.
- Republicans didn't correct the confusion but painted Trump as a doddering old man instead.
- Nikki Haley clarified the confusion and expressed concerns about someone unfit for the presidency.
- Republicans chose not to address the lies Trump spread, like the false claim about offering 10,000 troops.
- They avoided discussing the real orders that limited the National Guard's actions on January 6.
- Republican reluctance to confront the truth might disqualify them from office.

### Quotes

- "rather than taking that moment to explain the truth of something, they just leaned into, well, he's some doddering old man"
- "when you're dealing with the pressures of a presidency, we can't have someone else that we question whether they're mentally fit to do it"
- "I mean, I don't know, that just sounds like a lie."
- "paint him as losing his mind before they tell the base the truth."
- "Their unwillingness to address that probably should preclude them from being in that office as well."

### Oneliner

Trump's confusion at a rally reveals Republican reluctance to correct lies and address the truth about January 6, painting him as unfit for office.

### Audience

Political activists

### On-the-ground actions from transcript

- Call out political misinformation (suggested)
- Demand accountability from elected officials (exemplified)

### Whats missing in summary

Insights into the Republican Party's handling of Trump's confusion and their avoidance of addressing the truth.

### Tags

#Politics #RepublicanParty #Trump #Misinformation #Accountability


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump and Nikki and Nancy and confusion and how
the Republican party is really kind of showing itself here because something
occurred with Trump and he got a little bit confused.
And rather than taking that moment to explain the truth of something, they
just leaned into, well, he's some doddering old man, which I mean, fine,
whatever, it's an election season.
I get it, but I think there might've been a better use of that moment.
So if you missed it and you don't know what I'm talking about, Trump was
at a campaign rally, and he said, by the way, they never report the crowd on January 6.
You know, Nikki Haley, Nikki Haley, Nikki Haley.
Did you know they destroyed all of the information, all of the evidence, everything, deleted and
destroyed all of it?
All of it, because of lots of things, like Nikki Haley, is in charge of security.
We offered her 10,000 people, soldiers, national guards, whatever they want.
They turned it down.
Now obviously Nikki Haley was not the person that was initially subjected to this rumor.
That was Nancy Pelosi.
And obviously there is some stress that is starting to show when it comes to the former
president and this isn't the first time. But it's the first time you saw a
Republican actually talk about it afterward and point out what they think
is going on. Haley said they're saying he got confused that he was talking about
something else, that he was talking about Nancy Pelosi. He mentioned to me multiple
times in that scenario.
The concern I have, I'm not saying anything derogatory, but when you're dealing with
the pressures of a presidency, we can't have someone else that we question whether they're
mentally fit to do it.
I get it.
It's a good smear for a political campaign.
understand. But why not also point out that he's lying? The 10,000 troops thing?
Nancy Pelosi stopped me. Oh yeah, the Speaker of the House who isn't in the
chain of command stopped the Commander-in-Chief from doing what he
wanted. I mean, I don't know, that just sounds like a lie. Sounds like something
that's made up could have used that moment to set the record straight but
chose not to. Why? I think the answer is pretty obvious because right now the
Republican Party believes that narrative and if the base of the Republican Party
started to question that narrative they would want to look into things and the
The first thing they would find is the orders, right?
The orders that went to the National Guard that were out that day, the ones that denied
them equipment, that said they couldn't do anything, that limited them in every way possible.
The orders that came through Acting Sec Def from the Commander in Chief.
They're not going to bring that up.
paint him as losing his mind before they tell the base the truth. Before they do
anything that might demonstrate what he was actually trying to accomplish that
day. Their unwillingness to address that probably should preclude them from being
in that office as well.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}