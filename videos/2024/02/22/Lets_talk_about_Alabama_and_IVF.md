---
title: Let's talk about Alabama and IVF....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Dm0rSfJASi8) |
| Published | 2024/02/22 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Alabama's Supreme Court declared frozen fertilized embryos as children under the law, impacting in vitro fertilization (IVF) availability in the state.
- The decision is likely to make IVF more costly or completely unavailable in Alabama, hindering modern fertility care.
- Young physicians may avoid training or practicing in Alabama due to this ruling, and existing clinics will face tough choices.
- The state's stance essentially dictates how families can be built, restricting reproductive rights.
- The decision contradicts the notion of freedom often advocated by conservatives, leading to a loss of freedoms for individuals.
- The ruling gives the state control over family-building processes, showcasing a paradox in the idea of small government.
- Rejecting science can have serious consequences, as seen in Alabama's restrictive reproductive laws.

### Quotes

- "Alabama's Supreme Court declared frozen fertilized embryos as children under the law."
- "This is what happens when people reject science."
- "The state of Alabama gets to determine how you build your family."

### Oneliner

Alabama's Supreme Court's ruling on frozen embryos impacts IVF availability, restricting reproductive rights and contradicting conservative ideals of freedom.

### Audience

Advocates for reproductive rights

### On-the-ground actions from transcript

- Advocate for access to modern fertility care in Alabama (exemplified)
- Support existing clinics providing fertility services in the state (exemplified)

### Whats missing in summary

The emotional impact on individuals and families affected by the restrictive reproductive laws in Alabama.

### Tags

#Alabama #ReproductiveRights #IVF #FertilityCare #SmallGovernment


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Alabama,
the great state of Alabama, and science.
Two things that don't often go together,
and they don't go together here either.
We're gonna talk about the Supreme Court of the state,
what it decided, and what that decision is likely to,
well, give birth to.
Okay, so if you missed it, the Supreme Court of Alabama decided that a fertilized embryo
stored in a cryogenic facility, oh, that's a child.
That counts as a child, under the law that permits lawsuits when a child is killed.
Fertilized embryos in a cryogenic facility, frozen, frozen fertilized embryos, like the
kind required for in vitro fertilization, those. Okay, so what does this mean? It
means that realistically IVF is not going to be available in Alabama. It's
going to become even more cost prohibitive or it will just go away. Or as
somebody from the American Society for Reproductive Medicine said, modern
fertility care will be unavailable to the people of Alabama, needlessly
blocking them from building the families they want. Young physicians will choose
not to come to the state for training or to begin their practice. Existing clinics
will be forced to choose between providing suboptimal patient care or
or shutting their doors. Wow!
I mean, who could have seen that coming?
Everybody. Everybody.
Everybody saw it coming.
So, to recap, in the great state
of Alabama, the way it works basically
is, oh, you, woman, you over there, you don't want to have a kid, you have to have yours.
You?
Oh, you want one, right?
No, you don't get to.
You can't have that.
Now, we have all the power, we have all the control.
We own you.
Doesn't seem like this brought about a bunch of freedom, right?
the battle cry. It's what people always say. Why is it that every time a bunch of
conservatives get together, a bunch of rich old white men get together and
start screaming about freedom, people lose their freedoms? Having these count
as children basically makes it impossible because the lawsuits will just
they'll be constant might even lead to criminal violation depending on how it
works out, this is what happens when people reject science. So the state of
Alabama has basically said that they get to determine how you build your family.
That is the small government paradise that they've been trying to build there.
Anyway, it's just a thought.
It's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}