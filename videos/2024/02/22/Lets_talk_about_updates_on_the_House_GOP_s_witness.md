---
title: Let's talk about updates on the House GOP's witness....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=UjlEtAVS4MU) |
| Published | 2024/02/22 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The federal government is appealing the decision to release a witness who was arrested for lying about everything, citing concerns about him being a flight risk due to alleged contacts with foreign intelligence services and having liquid assets in the millions.
- Republican Ken Buck made a surprising statement alleging that two other Republicans, Comer and Jordan, were warned that the information provided by the star witness could not be corroborated.
- Despite calls for resignation, it's unlikely that Comer will step down over this issue.
- Questions arise about whether Republicans were deceived by Russian intelligence or if they knowingly used false information.
- Beau questions whether it's worse if Republicans were aware the information came from Russian intelligence and still used it, or if they didn't know its origin and lied to the public.
- Beau expresses hope that Republicans were simply tricked by Russian intelligence rather than knowingly spreading false information.
- The attempt to discredit Biden by linking him to corruption in Ukraine has backfired, as the information appears to have originated from Russian intelligence and was spread without verification.
- Beau predicts that there will be ongoing questions and scrutiny regarding this issue.

### Quotes

- "Were Republicans duped and tricked by Russian intelligence, or not?"
- "There's going to be questions about whether or not they were tricked or they were playing along with it."
- "This isn't over."

### Oneliner

The GOP House inquiry into Biden's alleged misconduct reveals concerns about deception by Russian intelligence, raising questions about Republican involvement.

### Audience

Political analysts

### On-the-ground actions from transcript

- Contact political representatives to demand transparency and accountability in investigations (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the GOP House inquiry into Biden's actions, shedding light on potential deception by Russian intelligence and the repercussions within the Republican party.

### Tags

#GOP #HouseInquiry #Biden #RussianIntelligence #RepublicanParty


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we are going to talk about a couple
of different stories, a couple of different developments
that all kind of tie back to the GOP House inquiry
and their desire to find something, anything on Biden.
Biden and we're just gonna run through those developments real quick and see
where we're at at the end of it. So the star witness, the very credible human
source who was then arrested for lying about everything, the federal government
is so concerned about Smirnoff not being in custody that they're appealing
that decision. You know he was released on a GPS monitor but the the feds don't
think that's enough. They're going to another judge and basically saying due
to the alleged contacts with foreign intelligence services having reportedly
millions liquid that basically he's a flight risk and they are trying to get
put into custody. We'll see how that plays out. Now, Ken Buck, who is a Republican, made of a kind
of surprising statement and allegation, and that was that Comer and Jordan were warned
and that the information provided by the star witness that has now been arrested, that that
information could not be corroborated, that they were warned at a time.
Here's the thing, that may be true, I mean somebody may have pulled them aside and been
like, hey, you know, this isn't true. That might be true, but there was never the
expectation that that information had been corroborated. It's on an FD1023.
The whole point of the form is that it's unvetted, unverified information. They
specifically sought out that form. You can take from that what you will. Maybe
they just didn't know what that form was. There's a whole bunch of
questions that are going to be asked about this as it moves forward. There are
a lot of people asking for Comer's resignation. Those requests are going to
be ignored. It's incredibly unlikely that he resigns over this. There was a time
when being played, it certainly appears that they were played by Russian
intelligence, there was a time when, yeah, I mean that would be a career ender up
on Capitol Hill, that time's passed.
That time has passed.
Standards have changed.
And that goes to the question that has come in repeatedly.
And the funny thing is, like the way it's being phrased, I mean, does it matter?
The question is, were Republicans duped and tricked by Russian intelligence, or not?
The thing is, I don't know which is worse.
They definitely used the information, okay?
That's obvious.
They were planning on using it.
They were holding it out there, all of that stuff.
they played by Russian intelligence? I mean, I hope so. I hope that's what it was because
if they weren't, that means, I mean, they knew what it was and they ran with it anyway.
To me that's worse. Either they knew it was Russian intelligence and they ran with it
anyway, or they knew it wasn't true, but didn't know it came from Russian
intelligence and just lied to the American people. I would like to think
that they're just not that swift. I would prefer to think that they were tricked.
But, again, there's going to be a lot of questions because there's no way, at this point, based
on it being an FD 1023, the way they very publicly went after that form in particular
when there was contemporary coverage, there's even some on this channel, saying that those
Those forms are for unverified information and the way they ran with it, there's no
way that they're just going to be able to sidestep it.
There's going to be questions about whether or not they were tricked or they were playing
along with it.
And I don't think either one of those options are good.
they will come up with some alternative. You know, a staffer was tricked or
something like that. But what has, what started as an attempt to show that Biden,
I guess, removed a prosecutor who wasn't investigating a company his son was kind
of linked to and replaced that prosecutor with a prosecutor who opened an investigation
into the company his son was linked to.
I guess that's bad, especially if that was done at the request of the Senate-Ukraine
caucus, which was a whole bunch of Republicans.
Because just so you know, that's actually what happened.
So I guess they were trying to find a way to show that as being bad and somehow corrupt
on Biden's part, and it turned into them, knowingly or unknowingly, somehow passing
on bad information
that at this point
appears to have originated with Russian intelligence
and moved through the star witness that they touted on TV
to them through a form
that the smallest bit of research would have told them
contained unverified, unvetted information and then was
pushed out to the public.
There's going to be questions.
This isn't over.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}