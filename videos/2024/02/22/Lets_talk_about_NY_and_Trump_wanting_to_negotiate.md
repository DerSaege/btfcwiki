---
title: Let's talk about NY and Trump wanting to negotiate....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=RceLjgP0OP4) |
| Published | 2024/02/22 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump feels denied the ability to provide a counter-judgment and negotiate the proceedings.
- Defendants usually have some say in how the case moves forward.
- A letter was sent to the judge requesting this ability, which may not cause a significant delay.
- Trump is expected to appeal, arguing that there are no victims in his case.
- Attorneys believe Trump's appeal strategy won't work based on past predictions.
- If Trump's appeal is unsuccessful, he will owe significant pre-judgment and post-judgment interest.
- The interest is accumulating at a rate of a couple of million dollars per month.
- Speculation suggests Trump might struggle to gather the cash to address this issue.
- James, the attorney general, is prepared to take Trump's buildings if he can't pay the judgment.
- James is not sympathetic and will seek judgment enforcement if Trump lacks the funds.
- The attorney general will ask the judge to seize Trump's assets if necessary.
- James seems unwilling to entertain many delays in the process.
- A clearer picture of the proceedings is expected around mid-March.

### Quotes

1. "If he does not have the funds to pay off the judgment, then we will seek judgment enforcement mechanisms in court."
2. "I mean, it's a couple million dollars a month."
3. "James, the attorney general, where's she at? She's ready to take his buildings."
4. "I want to say 600,000 a week is what they finally worked out as a good estimation number."
5. "Trump has stated that he feels he was denied his ability to provide a counter-judgment."

### Oneliner

Trump faces legal challenges in New York, with an impending appeal and potential asset seizure, as James takes a tough stance.

### Audience

Legal observers

### On-the-ground actions from transcript

- Contact legal advocacy organizations for updates on the case (suggested)
- Join local legal aid groups to stay informed about similar cases (suggested)
- Organize events to raise awareness about the importance of legal accountability (implied)

### What's missing in summary

The full transcript provides more in-depth analysis and context around Trump's legal situation in New York, offering a complete picture of the potential outcomes and challenges ahead.

### Tags

#Trump #LegalChallenges #NewYork #JamesAttorneyGeneral #AssetSeizure


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about Trump in New York
and how things are expected to proceed from here,
where James is at and her position
and what Trump's next steps are,
because it's Trump, there's always, you know,
the next move, okay.
So, Trump has stated that he feels he was denied his ability to provide a counter-judgment,
to propose a counter-judgment, basically negotiate how things move forward.
It is worth noting that in a lot of cases, there's a tendency to allow the defendant
to structure, at least have some say in how it moves forward.
So there's that.
We'll see how that plays out.
A letter was sent in to the judge
asking for the ability to do that.
I don't know how that's going to move forward.
But either way, that would not be a large delay.
The other thing is that Trump is expected to appeal.
The general consensus is that Trump will appeal on the grounds that, well, unlike all the
things he gets super mad about, like people walking across a line without a permission
slip and stuff like that, that in his thing, there's no victims.
That's apparently what people believe he's going to argue.
It's also worth noting that most of the attorneys that I have talked to are like, yeah, that's
not going to work.
And these are the attorneys that were correct about how everything would play out thus far.
So there's that.
It's worth noting, if Trump does appeal and is less than successful in those endeavors,
he will owe both pre-judgment and post-judgment interest.
Interest is racking up on this.
I mean, it's a couple million dollars a month.
I want to say 600,000 a week is what they finally worked out as a good estimation number.
So there's that.
There is speculation that Trump may have some issues in getting the actual cash to deal
with this. James, the attorney general, where's she at? She's ready to take his
buildings. She is not, she is not very sympathetic in any way shape or form
right now. She said, if he does not have the funds to pay off the judgment, then
we will seek judgment enforcement mechanisms in court, and we will ask the
judge to seize his assets. Yeah, I feel like she is not in the mood to to
entertain a lot of delays. So that's where everything is sitting right now. My
guess is we'll start to see some of this form up and have a clearer picture about
how things will move forward mid-March. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}