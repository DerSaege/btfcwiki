---
title: The Roads to channel news and questions
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=a7Noq7GuLO0) |
| Published | 2024/02/22 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau is hosting a Q&A video related to channel news and YouTube questions, giving off-the-cuff responses to queries pulled by the team.
- The community networking book Beau has been working on is complete, with the cover approved and ready for release soon as an e-book and in print, followed by an audiobook.
- Beau talks about the streamlined workflow that allows them to produce content efficiently, explaining how they let YouTube choose video thumbnails to save time.
- Beau reveals plans for a series of "MacGyver" videos on the channel, demonstrating practical skills like creating fire and making batteries from household items.
- Interviews were halted due to internet connection issues but may resume more sporadically in the future.
- Beau addresses the time it takes for certain projects to come out, attributing delays to the high production output and the various activities they are involved in.
- Beau explains their approach to mid-roll ads, ensuring they do not disrupt the flow of content and are placed in videos over 10 minutes long.
- Beau hints at a potential summer trip and offers advice to someone looking to start a political YouTube channel, cautioning against high expectations and the mimicry of existing channels.
- Beau mentions a YouTuber who has stopped making videos due to focusing on direct action to help people facing evictions, prioritizing immediate assistance over content creation.
- Beau shares details about their sensitive Sony XLR-VG1 shotgun mic and expresses interest in resuming live streams while discussing the balance between following journalism rules and maximizing views.
- Beau contemplates lessons learned and things they might have done differently when starting out, including organizing videos by topic for easier reference.

### Quotes

1. "The idea is, hopefully, when you learn how to start adapting your environment, you start to look at the world a little bit differently."
2. "Being right is more important than being first or getting the most views."
3. "It's the sitting down, filming it, all of that stuff, and then getting it out and doing it at a time in which it would be useful to people."
4. "She is doing boots on the ground, literally keeping people in their homes kind of work."
5. "A little more information, a little more context, and having the right information will make all the difference."

### Oneliner

Beau navigates questions on book releases, practical skill videos, mic recommendations, and the balance between content production and immediate community action.

### Audience

Content Creators

### On-the-ground actions from transcript

- Share practical skills and DIY knowledge with your community ( suggested )
- Prioritize immediate community action over content creation when needed ( exemplified )

### Whats missing in summary

Beau's engaging personality and detailed insights are best experienced through watching the full video.


## Transcript
Well, howdy there, internet people, it's Beau again,
and welcome to The Roads with Beau.
Today is a Thursday morning video,
and this is a Q&A, from what I understand,
all of these questions are related to,
like, questions about channel news,
or questions about YouTube itself,
which I don't really know what that means yet,
but we'll find out here in a second.
If you haven't seen one of these before,
these questions are pulled by the team.
I haven't seen them yet, and you're getting kind of an off
the cuff answer.
If you want to send a question in,
it's question4bo at Gmail.
And from what I understand, this type of question
was just kind of like they were developing a backlog.
So they wanted to get these out.
OK, and we are starting off with a note that
says, all of the technical questions
started with the person saying, they're
starting a YouTube channel or have one.
I'm sure that'll make sense in a minute.
OK, so the first real question is,
when does the book come out?
For those that don't know, we have been working on a
community networking book.
And it's done.
It is done.
The cover's approved, like everything's done.
So the answer to that question is very soon.
Patreon will get first crack at it.
And it will first be available in print and as an e-book.
And then later, there will be an audio book coming as well.
And right now, the holdup on it is us finding the right time
to be able to put it out, like push it out and get
and kind of be there to get content that runs along with it.
But it should be very, very soon.
But all the work is done, which makes me happy.
OK.
How are you looking for a person to fill in?
Does the person need to live near you?
How are you looking for?
Oh, like word of mouth.
We don't have ads out or anything.
Does the person need to live near me?
I mean, they don't have to live out in the middle of nowhere,
but they would have to be relatively close between Tallahassee
and Pensacola, somewhere in that range.
Let's see.
Your thumbnails are absolutely horrible.
Why don't you follow best practices?
I let YouTube pick them.
When you're going through the process
and you're uploading a video, there's
three suggested thumbnails that just pop up from the video.
That's where they come from.
I've never put anything into selecting certain frames or
anything like that.
One of the things that lets us put out so much content is we
have a very streamlined workflow.
And adding a little bit here and a little bit there,
eventually it adds up.
And putting red text or me looking shocked as a thumbnail
is just, it doesn't fit.
It just doesn't fit, and it adds time.
So we do not follow best practices on that one.
You should do a daily recap like the road's not
taken at the end of each day.
this is not the first time we've gotten this suggestion.
Yeah, that's something that we don't really have time for.
Like, I like the idea, because realistically, the work's
already done for this.
It's the sitting down, filming it, all of that stuff,
and then getting it out and doing it
at a time in which it would be useful to people.
That's more of a scheduling thing, because the work itself, the research for that, it
gets done every day anyway, it just gets wasted.
Those are videos that just turn into nothing, and they get recapped in the weekly thing.
I can see it being useful, it's just not something we have time for in the schedule.
What do you have planned for the channels this year?
Well, I mean the big things, obviously, the book.
And then now I feel comfortable going ahead and saying this part, because we have two
of them already done, like ready to be published.
We are putting together a series of videos on this channel that is, for lack of a better
word MacGyver videos. We always get questions about how certain things are
done. Like when something is mentioned in a video we get asked questions about
well how do you you know make the battery or something like that and we
We are putting together videos that show people how to create fire, how to store energy, literally
make a battery out of household stuff.
The idea, it's not just about learning how to do that.
The idea is, hopefully, when you learn how to start adapting your environment, you start
to look at the world a little bit differently.
You become more solution-oriented, and you see the world as components that can help
you reach your goal.
That sounded like super self-help, you know, that's not where that is.
Anyway, I think people will like them.
We have two of those already made.
There are four more that we're filming, like, already, and we'll probably get all of those
done before we start releasing them, and then there will be more following that, assuming
people like them.
But it's, yeah, half or a third MacGyver, a third Bill Nye, and a third Sears School
is what those videos are.
I think that's one of the bigger things that people didn't know about.
Why don't you do interviews with people anymore?
Those stopped because of internet connection.
We now have that fixed, so they may start back up.
They probably won't be a regular thing like they were before where there was one every
two weeks like clockwork, but they'll probably happen more often now.
Why do some of your projects take so long to come out?
Other channels put out long content and it doesn't take months.
Do you announce them early to build buzz?
No.
They get announced early because y'all ask.
Other channels do put out long content and it doesn't take months, but it is worth remembering
that our production output is really high.
the long content videos are on top of realistically putting out more videos a day than most channels
put out a month.
So they're built in pieces, so it takes longer.
And then we have other stuff going on, like the books and the actual boots on the ground
stuff that we do and the fundraisers and all of that.
That is not a marketing thing.
The extra stuff is just that, it's extra.
So it is a lower priority.
Is there a reason you don't do mid-rolls?
Mid-rolls for those who don't know are the ads in the middle of videos.
It's funny because this question was obviously asked about the other channel and it's being
answered here where we actually do mid-rolls.
Over on the other channel, it just absolutely does not flow.
Sometimes on this channel, it doesn't flow.
But on the other channel, the break in the middle, it just does not work.
Because of the way I generally structure a video, tell them what you're going to tell
them, tell them what you told them type of thing, that ad that would break in comes like
right in the middle of the meat.
And it doesn't go well.
We tried it for like a week, and it was years ago.
But we tried it, and that got vetoed.
We were just not doing that.
So this is what I would assume would be
one of the technical questions.
I would not put them in any video under 10 minutes,
And I would make sure that they don't interrupt the flow.
On these, there's breaks in between topics and stuff like
that, and people may say they're annoying, but they're
not disruptive, or they shouldn't be.
Sometimes there's things that happen.
We still don't have these all worked out over here, but I would be careful with them if
they mess up the actual content.
Are you going to do a summer trip?
I hope so.
I would like to.
It's planned.
We'll see if it happens.
I want to start a political YouTube channel, but see so many people not making it.
It's disheartening, with a question mark.
Yeah.
It's not impossible to start your channel now.
I've seen people talk about that.
You can't be like the other people.
You could be close.
You can have your things.
I have a whole video on this.
I'll try to find it and put it down below, but most people when they start their channel,
they want their channel to be like channels that they like, but a little different.
And what generally happens is they stay too close to the original inspiration channel
and it just, it doesn't work.
The other thing that occurs is people, I think people have an expectation that it's going
to occur quickly because they've seen it happen occasionally.
It normally doesn't.
It normally does not occur overnight or after three or four videos.
It takes time.
And generally what happens is you build up a base of videos,
and a lot of them nobody watched.
And then you have that one video that clicks.
And that one gets you a bunch of subscribers.
And then it builds up.
So it's a process.
And I think that those are the bigger reasons right there,
is expecting it to happen too quickly,
and then sticking too close to the people that
made you want to do your own channel.
I mean, obviously, if you want to do your own channel, you feel like you're bringing
something new, right?
If you were totally happy with the channels that were out there, you wouldn't feel the
need to make your own.
So you need to focus on whatever that difference is and hopefully other people will want that
change as well.
Why doesn't the name of a YouTuber make videos anymore?
She was great and hasn't uploaded in forever.
She yeah, she was great. I'm actually, I'm with you on that one. I think, I think she
should still make videos. I don't get a vote in it. But I can tell you why it might be.
She is currently doing like, she's out there doing the Lord's work. She is stopping evictions
and stuff like that.
She is doing boots on the ground,
literally keeping people in their homes kind of work.
And my guess is she does not have time
to run a YouTube channel.
Now, there may be other reasons.
It's not always everything it's cracked up to be.
And she may not have liked it.
But as much as I liked her videos
and felt it was very unique content that definitely
needed to be out there, what she is doing
is like immediate help.
It's the kind of action that gets the goods.
So I get it.
You said your mic was extremely sensitive.
I need that.
Can you tell me what kind it is?
It is a Sony XMC-VG1, a Sony XLR-VG1.
I'm pretty sure it ends with VG1.
And all I can see from here is that it's a Sony.
It's a Sony Shotgun mic.
And the thing is, as far as mics go,
it wasn't even super expensive, you know?
It's probably mid-range for upper-end mics, I guess.
And it picks up everything.
To the point, this chair now has soundproofing
the little egg crate things because my belt would rub against it and you could
hear this high-pitched sound that would come through. I would I like it because
it works for everything that we do but I would I'd make sure you really need one
that sensitive. When's the next live stream? No clue. No clue. Those I want to
get back to doing those at least once a month. So, maybe soon.
Do you think following journalism rules hurts you? You had that Oklahoma story before anybody,
but you didn't get a lot of views. You could have put in the rumors and just say they were
rumors and been okay? Where's the line on that? I would rather be accurate. I like being
right. There's not a lot of videos of me where I make a factual error.
And I don't like doing it.
I still kick myself over confusing bumblebees and honeybees.
So yeah, maybe it gets less views.
Maybe.
I don't know.
But if it does, I'm OK with that.
I don't think it hurts.
I get what you're saying in the sense that if I had just put everything that I heard
out there that then it probably would have got more views. You are probably
correct about that. But I think overall I think doing that would hurt. That's
that's one of those things that I I try to encourage people not to do. You know
being being right is more important than being first or getting the most views.
in my, to my way of thinking. So, is there anything you'd have done differently when
you started? Oh, lessons learned. Number one, I definitely would have chose a
different naming convention for the videos or at least done some kind of
kind of sheet, you know, put it into Excel or something.
So I could find videos based on topic.
And now I just, I don't feel like I could sit down and watch myself talk for 4,000 videos
or however many there are now and catalog them.
That would be something that I would have done differently right there.
Okay, I guess that's it.
Okay, so yeah, we didn't have any unique or special content for today, so you got the
Q&A.
I don't know if this had more information and more context, but it had a lot of answers.
So anyway, a little more information, a little more context, and having the right information
will make all the difference.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}