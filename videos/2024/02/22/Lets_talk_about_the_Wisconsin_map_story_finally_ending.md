---
title: Let's talk about the Wisconsin map story finally ending....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=cLrTS6Qj48s) |
| Published | 2024/02/22 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The governor of Wisconsin signed new maps into law, breaking the Republican Party's hold over the state through gerrymandering.
- The maps are pretty evenly matched, with competitive districts in both the House and Senate for the upcoming November elections.
- Elections in Wisconsin are now entirely about turnout, especially in competitive toss-up districts.
- Representatives will have to truly represent constituents in these competitive districts as they need their votes.
- The decision to have fair maps instead of playing hardball by the governor has made some traditionally leaning blue districts more competitive.
- Justice Protasewicz is credited as the reason Wisconsin is no longer gerrymandered.

### Quotes

1. "It's weird that when you have districts that aren't gerrymandered and you have competitive districts, it turns into a situation where the representatives kind of have to represent you because they actually need your vote."
2. "These elections are now entirely about turnout."
3. "Y'all have a good day."

### Oneliner

The governor of Wisconsin signing new maps into law breaks Republican gerrymandering hold, making upcoming elections about turnout and competitive districts truly representational.

### Audience

Wisconsin voters

### On-the-ground actions from transcript

- Get informed about the competitive districts in Wisconsin and make sure to vote in the upcoming elections (implied).

### Whats missing in summary

The full transcript provides insights into the impact of fair maps on political representation and the importance of voter turnout in upcoming elections.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Wisconsin maps one more time because it is wrapped up.
That storyline is kind of done for now.
The governor signed the new maps into law.
So what does this mean?
It means that the hold the Republican Party held over the state through gerrymandered
maps is broken.
As far as my idea of this being an opportunity for the Democratic governor to play hardball,
when he signed the maps into law, which are pretty much even, he said something to the
effect of, you know, when I ran I said I wanted fair maps and I meant fair maps,
not maps for my party. And that's what he decided when he neglected to play hard
ball and exercise the power that he had. So what happens now? These maps will be
in effect for the next election, for the elections in November. It is
pretty evenly matched with a number of competitive districts in both the House
and the Senate there. So what does this mean for you? What does it mean for
people in Wisconsin? It means that these elections are now entirely about
turnout. They are about turnout. You have some districts that are, they lean red,
you have some districts that lean blue, but they are much more evenly matched
and then there are a bunch of very very competitive districts that are just
straight toss-ups. In those districts in particular, it's all about turnout. So the
various political machines, the political parties up there, their job is now about
creating a platform that energizes people to show up. That's what it's about.
It's weird that when you, you know, have districts that aren't gerrymandered and
you have competitive districts, it turns into a situation where the representatives,
I mean, they kind of have to represent you because they actually need your vote.
It's not a situation where that district is guaranteed to a certain party.
There are still some of those on the map, but they've become a lot more competitive.
They would be more competitive, by that I mean they'd lean blue, if the governor had
elected to play hardball and just let it go to the courts.
And he decided not to do that and decided to be fair and all of that stuff.
So that's where it's at.
This storyline is kind of wrapping up.
And just for all time's sake, we'll say Justice Protasewicz one more time because she's the
reason this happened.
the reason that Wisconsin is no longer gerrymandered.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}