---
title: Let's talk about Iowa, displays, and predictable events....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=DW1L3uTVcsI) |
| Published | 2024/02/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- In Iowa, a temple put up a religious display at the State Capitol to advocate for the separation of church and state.
- The display was from the Satanic temple, in response to displays from other religions.
- Michael Cassidy from Mississippi, a Republican candidate, allegedly destroyed the display and faced misdemeanor charges.
- Charges against Cassidy have been upgraded to a hate crime, as evidence suggests he targeted the display due to the victim's religion.
- The situation may escalate to a trial, with Cassidy potentially using it to boost his political profile.
- The Satanic temple uses such displays to draw attention to religious double standards in the country.

### Quotes

- "The temple decided they were going to put up a religious display too, kind of illustrate the need for a wall, a hedge, if you will, between church and state."
- "And we'll just catch everybody up on it."
- "Evidence shows the defendant made statements indicating he destroyed the property because of the victim's religion."
- "Cassidy did not win this engagement is exactly what the temple looks for."
- "It's worth noting that the whole reason that the temple does stuff like this is to get publicity and draw attention."

### Oneliner

In Iowa and Mississippi, a religious display leads to hate crime charges, exposing religious double standards and advocating for the separation of church and state.

### Audience

Activists, Advocates, Voters

### On-the-ground actions from transcript

- Support organizations advocating for the separation of church and state (implied)
- Stay informed about legal cases involving hate crimes and religious freedoms (implied)

### Whats missing in summary

The full transcript provides more context on the specific events in Iowa and Mississippi, shedding light on the importance of religious freedom and the implications of hate crimes.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about Iowa
and Mississippi and display
and how things have progressed
and an incredibly predictable chain of events
that of course has happened
and we'll just catch everybody up on it.
Back in December, we talked about a situation
that occurred in Iowa at the State Capitol
where there was a display.
And this display was religious in nature. It was put up by a temple.
It was put up by a temple because other religions
had
displays up.
So this temple decided they were going to put up a religious display too.
And kind of illustrate the
the need
for a wall,
a hedge, if you will, between church and state.
The temple, of course, was the Satanic temple.
While the display was up, in fact, right before it was going to be brought down, a man from
Mississippi named Michael Cassidy, who also happened to be a Republican candidate at one
point in time, somebody active within the Republican Party who was running for office,
Well, Cassidy allegedly destroyed it, and when it happened, he was brought up on criminal
mischief charges, misdemeanor criminal mischief charges.
During the coverage of that, I said, he's incredibly lucky because one might construe
these actions as wanting to interfere with an individual's rights, kind of hinder them.
So the charges have been upgraded.
Evidence shows the defendant made statements to law enforcement and the public indicating
he destroyed the property because of the victim's religion.
And basically the charge, it's turned into a hate crime because it was done in violation
of an individual's rights.
this is a felony now. And odds are that they will pursue it because this wasn't, to my knowledge,
nobody was up there like banging the drums to have this turned into a felony.
It was a lot of public statements that maybe just couldn't be ignored at that point. But
But we will undoubtedly continue to follow this along because my guess is that Cassidy
is not going to just take a deal on this one.
My guess is there's going to be a trial, probably try to elevate the political profile by using
this.
It is worth noting that the whole reason that the temple does stuff like this is, well,
to get publicity and draw attention to, you know, this.
To be clear, Cassidy did not win this engagement is exactly what the temple looks for, because
it highlights the absurdity and how there is a double standard when it comes to the
application of law when it comes to religion in the state in this country.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}