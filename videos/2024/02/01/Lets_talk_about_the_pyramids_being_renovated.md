---
title: Let's talk about the pyramids being renovated...
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=-azNw1f3fHA) |
| Published | 2024/02/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduction to the topic of pyramids and renovations in the world of archaeology.
- Mention of the main pyramids in Egypt at Giza.
- Announcement of the renovation of the smallest main pyramid at Giza, involving granite.
- Acknowledgment that the renovation goes against archaeological conventions.
- Mention of outside backers involved in the renovation project.
- Indication that the renovation began without much prior knowledge.
- Potential intensification of study on the stones around the base due to backlash.
- Lack of a full cataloging of what exists at the pyramid site.
- Noting that the pyramid may not have been completed originally.
- Speculation that the controversy may escalate to legal action due to growing outrage.

### Quotes

- "There's probably going to be more news about this."
- "This is gonna end up in a courtroom because as news of this spreads the outrage seems to be growing."
- "The reason for doing it isn't exactly clear either."

### Oneliner

Beau introduces the controversial renovations of a pyramid in Egypt, sparking outrage and potential legal action due to lack of consultation and unclear motives.

### Audience

Archaeology enthusiasts, historians, activists

### On-the-ground actions from transcript

- Question the renovation project and demand transparency (implied)
- Stay informed about updates regarding the pyramid renovation (implied)

### Whats missing in summary

The detailed nuances and potential developments surrounding the controversial pyramid renovation project. 

### Tags

#Pyramids #Archaeology #Renovations #Controversy #LegalAction


## Transcript
Well, howdy there, internet people. Let's bow again.
So today we are going to talk about pyramids.
We're going to talk about pyramids and
renovations, I guess. I don't know what to call it.
Something is happening in the world of archaeology
and it's
probably going to continue to make news.
Okay, so
there are a whole bunch of pyramids in Egypt. Those best known are at Giza.
The smallest of the main pyramids at Giza is going to be renovated, which is archaeology
straight out of Indiana Jones.
So there is granite that is around the bottom
of the pyramid.
And the Egyptian government, along with some backers
from outside of the country, plan on basically putting
some of that granite on the pyramid.
pyramid, some of it may have actually been on the pyramid before, some of it was not.
This is not really something that most archaeologists would deem acceptable.
It kind of goes against a lot of conventions, but it does appear that it is underway and
nobody really knew about it until it was underway. There has been some walking
back and it seems like they're going to do a more intensive study of some of the
the stones that are around the base of the pyramid before making any final
determination. I don't really think that was the original plan. I think that is
in due in large part to some of the backlash that has occurred. Generally
speaking with something like this you don't introduce modern renovations it's
not it's not necessarily a good idea especially when a full cataloging of
what was there and what wasn't isn't something that really exists. It's worth
remembering that the pyramid, if my memory serves me, this pyramid was not
actually completed. I don't believe the Sun completed the pyramid after
the death of the person. Anyway, there's a lot to this and there's a, there's
probably going to be more news about this. My guess is that at some point this
is gonna end up in a courtroom because as news of this spreads the outrage
seems to be growing and the reason for doing it isn't exactly clear either.
The reason the government is allowing this is not known.
It seems as though it just kind of came out of nowhere and it doesn't appear that there
was a lot of consultation with experts before this got underway. So right now
most of the conversation is coming from those who are opposed to it. I am sure
that the other side to this story will present their side soon and we'll see
where it goes from there.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}