---
title: The Roads to Solar Eclipses and Lost Civilizations....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=iDa_OFS2i4o) |
| Published | 2024/02/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Beau introduces the topics of solar eclipses and lost civilizations that have been generating many questions.
- Details about the upcoming solar eclipse on April 8, visible from the lower 48 states, a rare event not to miss.
- The next observable total eclipse in the lower 48 will happen in August 2044.
- The path of totality for the eclipse includes states like Texas, Illinois, Ohio, and Maine.
- Safety precautions are emphasized, including using solar glasses or indirect viewing methods.
- Beau mentions the rewriting of history books due to the discovery of a previously unknown civilization, the Pono people in Ecuador.
- The civilization existed for about a thousand years, and its discovery will lead to a significant rewriting of historical narratives.
- This discovery challenges the Eurocentric view of history and opens up a new chapter in understanding pre-Columbian civilizations.
- Beau speculates that many legends and myths might be connected to these newly discovered civilizations in the Amazon.
- He encourages viewers to look into these topics as they offer insights into either rare astronomical events or groundbreaking archaeological discoveries.

### Quotes
- "Get the solar glasses, which understand they're not expensive."
- "This discovery challenges the Eurocentric view of history."
- "It's a whole new chapter is going to have to be written."

### Oneliner
Beau introduces rare solar eclipse details and groundbreaking archaeological discoveries challenging historical narratives, urging safety and curiosity.

### Audience
Astronomy enthusiasts, history buffs

### On-the-ground actions from transcript
- Watch the upcoming solar eclipse safely using solar glasses or indirect viewing methods (suggested).
- Research and learn more about the newly discovered Pono civilization and its implications for historical narratives (suggested).

### Whats missing in summary
The full transcript provides additional context and depth on the upcoming solar eclipse and the discovery of the Pono civilization, offering a comprehensive understanding of these intriguing topics.

### Tags
#SolarEclipse #History #Archaeology #Discoveries #Astronomy


## Transcript
Well, howdy there, internet people, it's Beau again,
and welcome to the Roads with Beau.
Today, we are going to talk about the roads to,
I guess, solar eclipses and lost civilizations.
We have two topics that we're getting a lot of questions
about that really don't fit in anywhere else,
so that's what today is.
We will start with the solar eclipse.
So if you don't know, on April 8,
the moon will pass between the Earth and the sun.
And it will create an eclipse, a solar eclipse,
that will be visible from the lower 48.
And it's not something that occurs very often.
If this is something you are interested in seeing,
you definitely want to catch this one,
because the next one that will be observable
from the lower 48 will happen in August of 2044.
So this is kind of the one you want to see.
You will be able to see a total eclipse
if you get near the path of totality, which
will be in Texas, Oklahoma, Arkansas, Missouri, Illinois,
Kentucky, Indiana, Ohio, Pennsylvania, New York, Vermont,
New Hampshire, and Maine.
I will put a link down below that will show you where that is
and what time it is and all of that stuff.
It's also worth remembering, just because you are not
on the path of totality, you can still see something.
I think in Florida, it's going to be like 57% obstructed.
So you will get an eclipse.
It just won't be a total one.
The other thing to remember is, please do this safely.
Get the solar glasses, which understand
they're not expensive.
Or use an indirect method of viewing.
Don't stare into the sun.
That seems like something that you shouldn't need to say,
but here we are.
So again, there's that.
I will put the resources down below
so you can kind of get a better feel for it.
The other thing that we are getting a lot of questions about
is the rewriting of history books,
because it's gonna happen based on what we've seen.
We have kind of briefly mentioned this,
but haven't gone into any depth about it.
Still not gonna get super in depth with it,
but give you a little bit more information
about what's going on.
When I was growing up it was Aztec, Inca, and Maya, right? Those were the
civilizations you learned about. Why? Because those were the ones the
Europeans knew about. That's what made it into history books. So it appears
that there was a civilization, like cities, not, we're not talking about
something that is primitive or anything like that. We are talking about cities,
the Pono people. And the use of lidar, which is light and light detection and
ranging, something like that, has uncovered cities and roads and all of
the stuff in Ecuador right at kind of the kind of the the foothills of the
And there's going to be a rewriting, because this isn't even a short period of time.
It looks like this civilization, these cities, were around for about a thousand years.
So there's going to be a lot of research into this.
It's a whole new phase of getting to understand what was going on in the new world before
it was, quote, discovered, you know, by the people who didn't build the cities that were
already here.
Because that's really what we know about.
That's what we're taught.
this is a a whole new chapter is going to have to be written. It predates just
about everything and it was around for a really long time and we know kind of
next to nothing about it. So a whole new chapter is going to be opened up and my
My guess is that we're going to find out a lot of those lost civilizations in the Amazon
are connected to this by-road.
I feel like we're about to find out that a whole bunch of legends and myths aren't exactly
myths.
They just weren't found.
So there's two stories that probably never would have made it because of the way news
is breaking right now, never would have made it on the main channel, that are definitely
worth kind of looking into if it's something that you're interested in because both of
things. It's either something that occurs once every 20 years or it's a whole
new world that's opening up, a whole new area of research and it's not polluted
by a Eurocentric view because hopefully, you know, we've kind of moved past that
And we're not... maybe we were actually ready to find out about this. Anyway, that's
a little bit more information, a little more context, and having the right
information will make all the difference. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}