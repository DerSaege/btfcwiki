---
title: Let's talk about Michigan, Biden, and the primary....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=i0Bi4tmHZvQ) |
| Published | 2024/02/20 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talking about Biden and the Michigan primary coming up on February 27th.
- Addressing a question about groups trying to influence Biden's messaging during the primary.
- Emphasizing the importance of unity over division in politics.
- Acknowledging that he doesn't tell people how to vote.
- Considering the goals of those advocating against voting for Biden in Michigan.
- Supporting a diversity of tactics in messaging and activism.
- Recognizing the significance of primary elections in shaping platforms.
- Exploring potential outcomes of groups influencing Biden's stance on certain issues.
- Contemplating the risk versus reward in such messaging efforts.
- Suggesting that even if the messaging strategy isn't perfect, it may still have positive impacts.
- Considering the potential positive outcomes of influencing Biden's policies.
- Viewing the primary as an appropriate time for such messaging tactics.
- Stating that uncomfortable actions in politics can lead to positive change.
- Expressing a willingness to accept certain risks for the potential reward of saving lives.

### Quotes

1. "We need unity, not division."
2. "I believe in a diversity of tactics."
3. "Realistically, let's say the Biden strategist become convinced that three to four percent, they're going to lose three or four points over this."
4. "If it's received and acted on, it saves lives."
5. "It's just a thought y'all have a good day."

### Oneliner

Beau addresses groups influencing Biden's messaging in the Michigan primary, advocating for unity and considering potential positive outcomes despite risks.

### Audience

Voters, Activists

### On-the-ground actions from transcript

- Contact relevant groups to understand their messaging strategies and goals (implied).
- Join local political organizations to have a voice in shaping platforms during primaries (implied).
- Organize community dialogues on effective messaging tactics in politics (implied).

### Whats missing in summary

The full transcript provides detailed insights into the dynamics of influencing political messaging during primaries and the importance of diverse tactics in activism.

### Tags

#Biden #Michigan primary #Unity #Activism #Political Messaging


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Biden and Michigan and the primary, which is
coming up on February 27th, I believe.
And we're going to talk about a question that came in and we're going to talk
about a couple of groups up there and what they're trying to do and how they're
trying to message to Biden during the primary, and we're just going to kind of
run through it because it's definitely something that's going to come up and it's worth talking
about.
And the message is, can you please speak out against the people who are saying not to vote
for Biden in the Michigan primary or those saying to vote uncommitted?
We need unity, not division.
Okay.
So first, I don't tell people how to vote.
I don't believe that's my place.
The other thing is, the people who are saying this, what's their goal?
What is their actual goal with this?
Not the strategy of trying to message and all of that, but they want people to stop
getting hurt, right?
They want something done about Palestine, and they want people to stop dying.
I do not see that as a bad goal.
So when it comes to something like that, even if, let's say I do not think that this is
the most effective way to message to Biden, even if I believed that, I wouldn't go against
what they're trying.
Their goal is good.
I believe in a diversity of tactics.
So it's not something I would speak out against, even if I don't think it's the most effective
way to do it.
For me to punch a left on something like this, it's a situation where whatever they're doing
is actively harmful.
If it's not actively harmful, a diversity of tactics normally pays off.
Even if I don't think it's the most effective thing, I do know that I could be wrong.
So the other thing to keep in mind here
is that this is a primary.
This is when they're supposed to message.
This is when they're supposed to do something like this.
The primary process is supposed to help shape platforms
and stuff like that.
This is the right time to do it.
And then you have to look at risk versus reward.
what is likely to come of this.
And just run through the options.
If you look at, okay, so you look at one option,
would be that there's no meaningful change.
Like these movements, these groups,
they don't actually get any traction,
and nothing really changes at the polls.
So what happens?
Nothing.
It's not harmful, right?
What happens if they, best case scenario,
they get a best case scenario,
And not just is the message sent, but Biden receives it.
Now, what they're hoping for is a ceasefire.
Realistically, again, a lot of the people who are supporting these movements, they are
not thinking foreign policy, international poker game, Masters of the Universe type stuff.
They're thinking people are getting hurt and I want it to stop.
So the way they're looking at it, what they want is a ceasefire.
Is that likely?
No, not really.
But that doesn't mean it couldn't send a message that could get real change.
As an example, just off the top of my head, Biden is behind the Senate package that has
than 9 billion in humanitarian aid for Palestinians.
The appeasement package for the House Republicans, it doesn't have that.
Something like this might send a message to the administration that encourages Biden to
say something like, look, we're not even doing this without that and draw a harder line there.
And the result, it saves lives.
Worst case scenario, it saves lives.
What's the worst case scenario?
Like for real?
Not doomsday stuff where it turns into something where Trump immediately takes over.
This is a primary.
Realistically, let's say the Biden strategist become convinced that three to four percent,
they're going to lose three or four points over this.
And there's no way to get them back, which is some of the messaging that's being sent
out.
What happens?
Biden shifts to center.
Biden shifts to the right, shifts a little to center, shifts a little to right on some
topics to make up that percentage points, those percentage points from the center.
That's your worst case scenario.
It's not ideal, but I don't know that when your payoff, your potential reward, is literally
saving lives and your risk is Biden uses more centrist rhetoric, I mean, I don't know that
that's a huge risk.
Again, this isn't about whether or not I think this is a good idea.
Is it harmful?
I don't think it is.
I don't believe it's harmful, and I think that there is a chance that it sends a message.
I don't think it would lead to a ceasefire, but it could lead to other tangible things
that save lives.
I'm cool with that.
Again, this is the primary.
This is when you're supposed to pull stunts like this.
So I know that for a lot of people, it's going to be uncomfortable.
But the people who might benefit from this, they're way more uncomfortable.
So I mean, this is just one of those things that occurs in politics.
And it's messaging, if it's received and acted on, it saves lives.
it's received and they feel like they can't do anything with it, it moves them
to the center a little bit prior to the election and probably doesn't actually
even change any real position, just rhetoric to get the the points from the
middle that were lost on the left. I'm personally I'm okay with that risk anyway
It's just a thought y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}