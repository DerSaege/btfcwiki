---
title: Let's talk about republicans laughing at their base....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=WsfllwX5QlM) |
| Published | 2024/02/20 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republican officials are being called out for laughing at and manipulating their base.
- The Republican Party sabotaged a bipartisan Senate deal to maintain a chaotic border as a campaign issue for Trump.
- Trump needs fear and chaos at the border to manipulate his base.
- Republican officials are running ads on securing the border while actively working against bipartisan deals.
- The party is using fear-mongering tactics and manipulation rather than policies that benefit the American people.
- Beau believes that this time, the Republican base might see through the deception and manipulation.
- If the base doesn't see through it, Beau suggests increasing funding to education to combat ignorance.

### Quotes

1. "Republican officials are up there laughing about how gullible and how easy to manipulate their bases."
2. "They don't have anything that's actually going to help the average American. All they have is fear."
3. "For them to be this cynical, they have to believe that their base is the most ignorant group of people on the planet."
4. "I think their base might actually catch it this time."
5. "If they don't, if the base doesn't see through it, then we have to increase funding to education."

### Oneliner

Republican officials laugh at and manipulate their base with fear-mongering, sabotaging bipartisan deals, but this time, the base might see through their cynical tactics.

### Audience

American voters

### On-the-ground actions from transcript

- Increase funding to education (suggested)

### Whats missing in summary

Beau's analysis and call for increased education funding to combat political manipulation and ignorance.

### Tags

#RepublicanOfficials #Manipulation #FearMongering #BipartisanDeals #EducationFunding


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Republican officials
laughing at their base
and how they might've overplayed their hand
and how the Republican base
might actually see through it this time.
Because this isn't just a case of it being super obvious.
It's also a situation where people called it out
before it happened, and it's a very short time frame
to try to pivot in the way that they're pivoting.
I think for once, while the Republican officials,
Republican politicians, are up there laughing
about how gullible and how easy to manipulate their bases,
I think their base might actually catch it this time.
We have talked at length about how the Republican Party
Republican Party intentionally tanked the bipartisan Senate deal so they could maintain their issue with the border.
You know, they negotiated, they got what they wanted, and then they were like, no, no, no, we don't actually want that.
And the reason is Trump needs a campaign issue.
Trump needs a chaotic border.
He doesn't want anything that the Republican Party says they want to enact.
He doesn't actually want it enacted because if it gets enacted, he doesn't have anything
to scare the easily manipulated with.
He doesn't have anything to trick the gullible with.
He doesn't have that.
So he needs a chaotic border.
And we said that they were going to run on that.
That they were going to take the deal and then run on this issue because they don't
have any policy.
They don't have anything that's actually going to help the average American.
All they have is fear.
So you can't take away that fear.
You know, you can't get rid of that because then they don't have anything to run on.
So here's the thing.
The ads have already started.
The ads have already started.
Mike Johnson.
Now, to my way of thinking, Trump is the person who actually tanked the bipartisan Senate
deal.
But public face, it was Mike Johnson.
This is an ad from Mike Johnson.
It's not even up for debate anymore.
That's how it starts off there.
And then there's this thing and it says, basically says the administration, quote, created the
border crisis through a series of executive actions.
And then help the House, GOP, hold them accountable and secure our border.
They're already running on it.
For them to be this cynical, they have to believe that their base is the most ignorant
group of people on the planet.
They are still actively working against the border deal, the Senate bipartisan package.
They're still actively working against it in the House, in the Senate, up on Capitol
Hill.
They're still fighting for it, fighting against it.
But here they're already running ads saying, hey, we'll fix that border for you.
Be scared.
That's how you'll obey us.
That's how we'll keep you under control.
We'll keep you afraid.
For them to try this in such short order, it means that they believe their base is way
more gullible, way more easily manipulated than I believe.
I think they can see through this.
I think they'll understand that they've been duped, they've been played for the last eight
ears that this whole thing was manufactured to scare them, to manipulate
them, to trick them. And if you can't see that where they're saying, oh well we
want to do this, this is what we promised to do as soon as we get elected, we could
do it right now, this second. There's even a bill to do it, but we don't want to do
it now. We want to wait until we're re-elected to do it. They don't
care about you. They don't care about you. None of their policy benefits you.
Their talking points are just used to scare you and manipulate you. It's
fear-mongering at its finest. We talked about it. It wasn't just this channel.
Everybody called this. They were gonna start campaigning on it. They were gonna
start running ads saying that they had to secure the border and that was gonna
be their promise for 2024 while they're actively tanking their own deal.
They're lying to their base, and I think this time they're doing it in such a quick turnaround,
their base will see through it, at least I'm hopeful.
If they don't, if the base doesn't see through it, then we have to increase funding to education.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}