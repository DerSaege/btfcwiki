---
title: Let's talk about information consumption and an executive order....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=YbPygfGf4Oc) |
| Published | 2024/02/20 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau talks about the trending topic of Executive Order 9066 on social media and the misinformation surrounding it.
- Hundreds of thousands of people were exposed to false information about Executive Order 9066 on Twitter.
- The misinformation claimed that Executive Order 9066 was rewarding "illegals" with $5,000 gift cards, blaming Biden and Democrats for financial struggles.
- Executive Order 9066, signed on February 19th, 1942, led to the internment of Japanese Americans during World War II.
- The spread of misinformation coincided with the anniversary of Executive Order 9066, leading to confusion and false narratives.
- Large social media platforms lacking fact-checking mechanisms contribute to the rapid spread of misinformation.
- Beau advises the audience to fact-check information, especially during the upcoming election season.
- The misinformation surrounding Executive Order 9066 was not new, wasn't signed by Biden, and actually pertained to the internment of American citizens during World War II.
- Beau warns about the prevalence of misinformation and the importance of verifying information found online.
- The need to be vigilant and critical of information shared online, especially during times of heightened political activity.

### Quotes

1. "You're gonna have to fact-check your uncles and fathers and people who believe anything that's on the internet."
2. "Executive Order 9066 is rewarding illegals with $5,000 gift cards."
3. "It's an election season. You're gonna see this more and more and more."
4. "Large social media platforms that do not have fact-checking, that do not really have any cohesive way of avoiding the kind of misinformation."
5. "The executive order that is being referenced, again, it's not new and it certainly wasn't signed by Biden."

### Oneliner

Be ready to fact-check misinformation online, especially during elections, as seen with the false claims about Executive Order 9066.

### Audience

Social media users

### On-the-ground actions from transcript

- Fact-check information before sharing (implied)
- Educate others on the importance of verifying information online (implied)

### Whats missing in summary

Importance of critical information consumption and vigilance against misinformation during sensitive times.

### Tags

#Misinformation #FactChecking #ElectionSeason #ExecutiveOrder9066 #SocialMedia


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about information consumption
and an executive order.
And we're just going to kind of run through something
that everybody should be aware of prior to the election
season really heating up, because you're
going to see a whole lot of this.
And this is a good example.
So today on Twitter, Executive Order 9066 trended on social media, trended on Twitter.
When I clicked on it, a post that was the very first one had been viewed hundreds of
thousands of times, been shared thousands of times, liked by thousands of people.
And it said, Executive Order 9066 is rewarding illegals with $5,000 gift cards.
And it talks about how the person's working really hard, but every month he goes deeper
in debt because of Biden.
And the Democrats have let me know that I am dead last as an American.
I will never support a Democrat.
And it goes on and on.
This is all because of Executive Order 9066.
Hundreds of thousands of people were exposed to this.
Now, it is worth noting, sometime about an hour later,
it looks like a community note finally showed up to say,
hey, none of this is true.
So what is Executive Order 9066?
That was one that came out on February 19th, 1942.
It was the one that led to internment in the United
States during World War II.
And this misinformation that was spread,
it was spread on the anniversary of this being signed,
using this executive order.
My guess is the order was trending
because of the anniversary and because people were talking about how the United States during
World War II interned people based on national heritage.
Not one of the brightest moments in U.S. history, but because it was trending using that executive
order number, well, it got that information, less than accurate information, out to a whole
bunch of people, hundreds of thousands of people. You're gonna have to be ready
for this. It's an election season. You're gonna see this more and more and more.
There are large social media platforms that do not have fact-checking, that do
not really have any cohesive way of avoiding the kind of misinformation that
can spread very, very rapidly.
You're going to have to fact check your uncles and fathers and people who believe anything
that's on the internet.
The executive order that is being referenced, again, it's not new and it certainly wasn't
signed by Biden doesn't have anything to do with gift guards, it has to do with the
internment of American citizens during World War II by their own government.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}