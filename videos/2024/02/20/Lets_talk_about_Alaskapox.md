---
title: Let's talk about Alaskapox...
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=IUMN-dFTgbM) |
| Published | 2024/02/20 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recent developments in Alaska regarding a new illness, Alaska Pox, have sparked curiosity.
- Alaska Pox is similar to smallpox, causing lesions, muscle pain, and swollen lymph nodes.
- Discovered in 2015, there have been seven confirmed infections, with the first fatality recently reported.
- The person who died was elderly and immunocompromised due to cancer treatment, impacting the severity of the illness.
- It is unclear if Alaska Pox can be transmitted from human to human.
- Officials advise staying away from wildlife, as most cases seem to come from animal spillover.
- Washing hands after outdoor activities is recommended to prevent potential transmission.
- The outbreak seems localized around Fairbanks and hasn't spread nationwide.
- Monitoring for new cases is ongoing, despite the relatively low number of reported cases since 2015.

### Quotes

1. "Alaska Pox is similar to smallpox, causing lesions, muscle pain, and swollen lymph nodes."
2. "Stay away from wildlife, wash your hands when coming in from outdoors."
3. "Recent developments in Alaska regarding a new illness, Alaska Pox, have sparked curiosity."

### Oneliner

Recent developments in Alaska have led to the discovery of Alaska Pox, a smallpox-like illness causing concern due to its severity and unknown transmission possibilities.

### Audience

Health-conscious individuals

### On-the-ground actions from transcript

- Stay away from wildlife and wash hands after outdoor activities (implied)
- Monitor for symptoms and seek medical attention if concerned (implied)

### Whats missing in summary

Importance of monitoring symptoms and seeking medical attention for any health concerns related to Alaska Pox.

### Tags

#AlaskaPox #HealthConcerns #Outbreak #Prevention #Monitoring


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to kind of run
through some information.
Some recent developments in Alaska
have led a lot of people to be curious.
So we are going to just kind of run through what
we know about it, how it spreads, what happened,
what the deciding difference may have
been between the recent development
all of those since 2015 and just kind of run through everything. Okay, so if you
haven't figured it out, we are going to talk about Alaska Pox. Alaska Pox. Now
this is in the same family of illness as smallpox, something like that. It's
something that will give you lesions, muscle pain, swollen lymph nodes, along
those lines. Not pleasant. It was discovered in 2015. Since then, there have
been seven confirmed infections. Okay, now officials think there's actually been
more, but there have been seven confirmed infections since 2015. Recently, there was
the first fatality, first person lost to it or at least known to have been lost to it.
So that obviously got a lot of attention because this is something new and now it is causing
loss.
So what do we know?
the person who was lost was elderly and immunocompromised due to cancer treatment.
And there is a widespread belief that those two things in conjunction contributed to the
severity of the situation.
So it's, again, it's one of those things.
It's not saying that there's nothing to worry about, it's saying that this is what happened
in this particular situation that was different, that might have led to a less desirable outcome.
Okay, so the obvious question, can you pass it from human to human?
The answer is they don't know.
They don't know.
There are no confirmed cases of that, and generally speaking when you are talking about
something that causes lesions, touching a lesion, that's not good.
That very well might spread it.
But it does appear at this point that most of this has been spillover from animals, typically
small animals.
So what they're saying is stay away from small, well, stay away from wildlife, I think is
what they're officially saying, like all wildlife, just stay away from it.
And when you come in from outdoors, wash your hands.
That's what they're saying.
Again, this is not something that is nationwide, even though most of the questions were from
other places where it was cold, but not Alaska.
So to my knowledge, it hasn't spread.
It's all actually relatively close to Fairbanks.
But that's the information that is available on it at this point in time.
I will continue to monitor it to see if there's more cases that pop up quickly or if this
is going to be more spread out.
and discovered in 2015, there actually haven't been a lot of cases, but we'll keep watching
it.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}