---
title: Let's talk about Trump's appeals at SCOTUS and NY....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Ah53g0x9n1g) |
| Published | 2024/02/29 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains developments in Trump's legal entanglements with the Supreme Court and the New York case.
- The Supreme Court is taking up Trump's appeal regarding presidential immunity in a criminal case.
- The Supreme Court will address the question of presidential immunity from criminal prosecution.
- Trump's argument for immunity was broad, extending to all acts during his presidency.
- The New York case involves a $454 million judgment, with Trump seeking a stay of enforcement.
- The judge temporarily denied Trump's request for a stay, requiring him to come up with the cash.
- Trump proposed putting up $100 million but the judge did not readily accept.
- The judge allowed Trump to apply for loans in New York.
- More legal developments are expected in separate videos regarding Trump's cases.
- Stay tuned for updates on the New York criminal case for falsification of business records.

### Quotes

1. "The Supreme Court has agreed to hear it the third week in April."
2. "The court decided that it will look at the specific question of whether or not a president has immunity from criminal prosecution."
3. "Trump was asking for a stay of enforcement of that judgment."
4. "Trump offered, like in a counteroffer, I guess, kind of floated the idea of putting up a hundred million dollars."
5. "Anyway, it's just a thought. Y'all have a good day."

### Oneliner

Beau explains developments in Trump's legal battles with the Supreme Court and New York cases, addressing presidential immunity and a $454 million judgment, with more legal updates to come.

### Audience

Legal enthusiasts, Political analysts

### On-the-ground actions from transcript

- Stay updated on legal developments regarding Trump's cases (implied).
- Keep an eye out for further updates on the New York criminal case (implied).

### Whats missing in summary

Details on the potential implications and outcomes of the Supreme Court's decision on Trump's immunity claim and the enforcement of the $454 million judgment in the New York case.

### Tags

#Trump #SupremeCourt #LegalBattles #PresidentialImmunity #NewYorkCase


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Trump
and the Supreme Court and appeals
and where everything heads from here
because there have been some developments
in two separate legal entanglements of Trump's
and we're going to just kind of run through
and talk about where each one is at
and what happens next.
Okay, so the big one, the one that I think most people
probably A, most concerned about and B, paying the most attention to, the Supreme
Court has decided to take up one of Trump's appeals in a narrow way. This is
dealing with the DC case, the federal DC case, the federal criminal DC case, and
this is the one where Trump claimed, you know, presidential immunity and is trying
manufacture this. The Supreme Court has agreed to hear it the third week in
April. Let's go ahead and jump to the the main question right away. Unless the
Supreme Court does something incredibly bizarre and just holds its ruling for a
really long time, you're still getting your trial before the general election,
okay, which is probably what Trump was trying to avoid with all of this. Doesn't
look like it's going to pan out when you sit down and you kind of break it down
month by month, it's still going to happen before the election. The court
decided that it will look at the specific question of whether or not a
president has immunity from criminal prosecution for any and all acts that
they took while they were in office. Please keep in mind what Trump is
arguing was so broad. This is where the whole Trump can use SEAL Team Six to go
after his opponent's thing. This is where that came from. It is utterly
just, it is inconceivable that the Supreme Court actually agree with his
argument in whole. My guess is they want to kind of sketch the outlines of where
this is and that's really what the ruling is about or they just want to
make sure that the decision that the appeals court made is universally
applied. Okay, so that's what's going on there. Now, in the New York case, Trump we're
talking about the New York case with the 454 million dollar thing. It is amazing
that simply the New York case or the DC case or the DC federal case, none of
these are actually enough to narrow down which case we're talking about. But we are talking
about the civil New York case with the $454 million judgment thing. Trump was asking for
a stay of enforcement of that judgment. The judge temporarily denied it, which means Trump
has to come up with the cash. Trump offered, like in a counteroffer, I guess, kind of floated
the idea of putting up a hundred million dollars. And the judge did not readily accept that.
Now the judge did grant Trump a little bit of relief in saying that he could apply for
loans in New York. I'm not sure how much good that's going to do, the former president,
but that's where things are sitting there. Again, this is a temporary decision before
it goes to the five judge panel. So that's where that's at. There are
unsurprisingly more legal developments coming with Trump that will be in a
different video. We have some news coming in about the New York criminal case for
the falsification of business records as well and that'll be in a later video.
Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}