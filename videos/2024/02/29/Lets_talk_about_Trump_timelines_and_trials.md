---
title: Let's talk about Trump, timelines, and trials....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=qBvJlTqGFaM) |
| Published | 2024/02/29 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Speculates on Trump, timelines, and potential events pre-election.
- Consulted 17 attorneys for opinions on the matter.
- Four attorneys say the trial will definitely occur before the election, six say it won't, and seven are unsure.
- Uncertainty surrounds the timing of the court decision and potential sentence outcome.
- Even those with definite answers seemed somewhat unsure.
- Conclusions based on estimations of court decisions and actions of key figures.
- Lots of guesswork involved; not much known for certain.
- Trump's potential indictment and outcomes of various cases are discussed.
- Points out that events deemed impossible have occurred before.
- Raises the issue of immunity extending to Biden if granted to Trump.
- Questions whether Americans will re-elect Trump without knowing trial outcomes.
- Considers the possibility of Trump suppressing the trial if re-elected.
- Acknowledges Trump's weakened position compared to before.
- Urges not to panic and to recall past setbacks and outcomes.

### Quotes

1. "Trump will never be indicted. It's not gonna happen."
2. "Do you believe the American people will re-elect Trump without knowing the outcome to this?"
3. "I understand that this is not what anybody wanted."
4. "There's a lot of high drama with this for obvious reasons."
5. "Anyway, it's just a thought, y'all have a good day."

### Oneliner

Beau speculates on Trump's potential indictment and trial outcomes pre-election, based on consultations with attorneys, pointing out uncertainties and past surprising events.

### Audience

Legal analysts, political commentators

### On-the-ground actions from transcript

- Contact legal professionals for expert opinions on legal proceedings (suggested)
- Stay informed about ongoing developments in legal cases involving public figures (implied)

### Whats missing in summary

Insight into the potential implications of legal outcomes on the upcoming election.

### Tags

#Trump #Election #LegalProceedings #Uncertainty #Immunity


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump and timelines
and whether or not something is going to occur
before the general election
because that's the big question, right?
In the previous video, talked about it,
I had talked to two people
who know what they're talking about.
Both of them were in agreement saying it absolutely would.
Saying it absolutely would.
Since then, a bunch of y'all have had questions and I heard some commentary from some other people,
so I started asking more people. I have talked to or heard the opinions of 17 attorneys, people that
know what they're talking about. This is the breakdown. Four say it definitely will. The trial
definitely will occur prior to the election. Six say it definitely won't. Seven are unsure. Most of
those seem to believe that it would get underway and the thing that would not
happen before the general election would be the outcome of any potential sentence
if there was one. Even those people who provided definite answers, I would say
half of those seemed unsure. They all reached their conclusions by estimating
when the court would return a decision, what Smith would do, what Chutkin would
do, so on and so forth. There's a lot of guesswork. It will be
close no matter what. But what do we actually know based on this? Not much.
I'm sure there are people who are very convinced but when you talk to a whole
bunch of people you get a whole bunch of different answers and there's a... I am
less sure that it will occur prior to the election than I was when I filmed
the last video, but I am not convinced that it won't happen prior to the
election. Okay, now that all that's been said, I want to run through some things
because every time something happens, the sky starts falling. Trump will never be
indicted. It's not gonna happen. There is no way that Trump will be found liable
for the E. Jean Carroll case. Happened too long ago. There is no way New York
will be able to pursue their civil case. There is no way New York will be able
to win their civil case. There is no way they will get a judgment in the hundreds
of millions of dollars. There is no way the New York criminal case will move
forward. If you don't know, that starts in less than 30 days. There have been a
whole lot of things that are that we're seeing as impossible that have occurred.
There's a lot of high drama with this for obvious reasons. I would like to point
out two more things that I think are important to remember.
One, any immunity the court extends to Trump in this also extends to Biden.
Two, do you believe the American people will re-elect Trump without knowing the outcome to this?
Because most people, the reason having the trial prior to the election was so important
was because there was a belief that if Trump won, he'd just squash it all.
I mean, obviously, and he would. I mean, that just makes sense.
But when those positions formed, Trump was not as weak as he is today.
I understand that this is not what anybody wanted and I understand the
displeasure with the Supreme Court for doing it the way they have.
I would not say that the sky is falling yet.
Think back to every setback that has occurred and what the ultimate outcome has been.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}