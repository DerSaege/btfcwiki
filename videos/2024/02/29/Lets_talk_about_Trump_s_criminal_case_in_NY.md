---
title: Let's talk about Trump's criminal case in NY....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=UwI-D38MLQo) |
| Published | 2024/02/29 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's next legal entanglement will begin in less than a month, with the trial starting on March 25th.
- New York has asked for a gag order to prevent Trump from making public and inflammatory remarks that could disrupt the criminal proceeding.
- This is a criminal case, different from the civil cases Trump has faced before, focusing on falsification of business records, known as the "hush money case."
- Trump will be a criminal defendant, the first former president to face criminal charges.
- Trump's team has requested the Access Hollywood tape not be used as evidence and to prevent Cohen from being a witness.
- Expect a flurry of activity leading up to the trial date on March 25th.

### Quotes
1. "They want to prohibit Trump from trumping it up."
2. "Trump will be a criminal defendant."
3. "It is worth keeping in mind as this date closes in on us because it is less than a month away."
4. "There are going to be other motions."
5. "Y'all have a good day."

### Oneliner
Trump's upcoming criminal trial begins March 25th, with New York seeking a gag order to prevent disruptive remarks, marking a significant legal turn for the former president.

### Audience
Legal observers, concerned citizens

### On-the-ground actions from transcript
- Stay informed about the developments leading up to Trump's trial (suggested)
- Follow reliable news sources for updates on the legal proceedings (suggested)

### Whats missing in summary
Analysis of potential implications and consequences of Trump's criminal trial

### Tags
#Trump #LegalEntanglement #CriminalCase #GagOrder #HushMoney #Trial


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about Trump
and his next legal entanglement,
which will begin in less than a month.
And we're going to talk about the opening move,
which is incredibly predictable and very common
at this point.
It's very expected in the former president's
legal entanglements.
And then we're going to remind everybody of the distinction
of this particular situation in comparison
to the ones that have come before.
Okay, so the next case starts March 25th.
That's when the trial starts.
And it is underway now.
Motions are going in now.
One of the first things that was asked for by New York
was a gag order.
They want to prohibit Trump from trumping it up.
It says, Trump quote,
"'has a long history of making public
"'and inflammatory remarks.
"'Those remarks, as well as the inevitable reactions
"'they incite from the defendants,
"'followers, and allies, pose a significant threat
"'to the orderly administration of this criminal proceeding.'"
And it looks like they're trying to have this order apply to known or foreseeable witnesses, court staff, district
attorney staff, as well as their families, and any prospective jurors.
My guess is that the prosecution is going to get at least some of this, some form of this right from the beginning,
and that it might be expanded upon later, depending on how the former president behaves.
The other thing to note is this is a criminal case. All of the other cases
have been civil as far as those that have actually gone to trial and moved
through everything. They were civil cases. This is a criminal case dealing with
the falsification of business records, most commonly referred to as the quote
hush money case. This is a criminal case. Trump will be a criminal defendant. It is
a, it's a different thing. To my knowledge, he is the first former president to face
criminal charges. So it's worth keeping that in mind as this date closes in on
us because it is less than a month away. It is supposed to begin on March 25th.
There are going to be other motions. I believe Trump's team has already asked
for that Access Hollywood tape to be prohibited from being used and I think
they wanted to make sure that like Cohen couldn't be a witness or something like
that. And we'll see how all of this plays out. And there's going to be a lot of...
there's going to be a flurry of activity, and then it'll kind of slow down for a
little bit, and then March 25th the trial is supposed to start. Anyway, it's just a
thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}