---
title: Roads Not Taken EP 27.5
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=VNV4UQzJN2M) |
| Published | 2024/02/29 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Updates on various developing topics, including Trump's disqualification, budget negotiations, and the replacement of George Santos in the House.
- A bill in Arizona proposed by the Republican party that could make it legal to shoot trespassers under certain circumstances.
- Wildfires in Texas and parts of Oklahoma, with one approaching about a million acres in size.
- Beau provides a brief update on his surgery and recovery process.
- Beau's take on the arrests of the Boebert family and his thoughts on the issue.
- Beau explains why he hasn't made a video celebrating the Palestinian Authority resignations, despite predicting it earlier.
- The impact of the Palestinian Authority resignations and steps towards revitalization that Beau had foreseen.
- Beau's opinion on the possibility of uncommitted voters evolving into a viable party.
- Mention of West Virginia SB195 and Beau's intention to cover it in a video.
- Addressing a viewer's request to hear the crickets again during a video.
- Announcement of an upcoming live stream or video to support Project Rebound for formerly incarcerated individuals.

### Quotes

1. "It is exactly what you think it is."
2. "There are some major developments as far as the pieces that have to be there to get a durable peace going."
3. "It sent the message."
4. "But the audio is just the crickets for 25 minutes."
5. "So either we're going to do a live stream or there'll be a video like directing y'all to their website to donate directly or maybe both."

### Oneliner

Beau provides updates on various topics, from Trump's disqualification to wildfires, and teases upcoming content to support Project Rebound.

### Audience

Viewers

### On-the-ground actions from transcript

- Support Project Rebound by participating in the upcoming live stream or visiting their website to donate directly (suggested).
- Stay informed about ongoing issues and developments mentioned in the transcript (implied).

### Whats missing in summary

Insights on Beau's perspective and analysis on current events, ideal for those seeking a concise overview of recent updates.

### Tags

#Updates #Politics #CurrentEvents #CommunitySupport #Policy


## Transcript
Well, howdy there, Internet people, it's Beau again,
and welcome to The Roads with Beau.
Today is February 29th, not a date we say often,
February 29th, 2024, and we're gonna call this
episode 27.5 of The Roads Not Takin'.
This is not a day we normally do this.
However, we've got some news,
and we're kinda getting a backlog of videos
because news just keeps breaking.
So we're going to try to provide a couple of quick updates
on a few developing topics, and then we
will move into a brief Q&A. And that's what we're doing today.
OK.
So starting off, a judge in Illinois
has ruled that Trump is disqualified
from the primary ballot because of the whole insurrection
cause of the US Constitution.
The ruling is out, but at the same time, it's paused.
The Supreme Court is going to end up deciding this.
And it will be decided by the Supreme Court
no matter how many jurisdictions or states make this ruling.
A deal has reportedly been reached to buy a little bit of time for the budget negotiations.
Things do look good, but at time of filming they haven't been resolved yet and they're
pushing for kind of a, I think it only buys like a week.
At least that's the rumored details of the deal.
So we'll see how that goes.
More news out of the House.
Tom Swasey has been sworn in to replace George Santos.
This further shrinks the Republican majority.
It is razor thin now.
There is a bill in Arizona that's been proposed that has been advanced by the Republican
party, making it legal to shoot trespassers in certain circumstances, it's exactly what
you think it is.
It is exactly what you think it is.
And it's being advanced, I do not know if it's going to pass.
If it does, there are certainly going to be legal challenges.
Texas and I believe parts of Oklahoma there are wildfires that are burning and they are
still going at time of filming.
One is approaching about a million acres.
Million acres.
That's about the size of Rhode Island for people in Europe.
It's substantially larger than Luxembourg.
Okay, so it says, there's been a lot of questions, okay so yeah, the surgery went well, I'm healing
up fine, I can move, looks like everything is where it's supposed to be, I still have
a little bit of time before I'm allowed to do anything, but everything seems to have
gone very well.
Okay.
So question and answer section, what do you think of the Boebert family arrests?
Um, yeah, I've, I've seen that, um, they are not running for political office.
Um, I thought it was ridiculous when the Republican party did it with Hunter
Biden. I think this is ridiculous as well. To me, this is outside the scope of what's
real news. I mean, outside of like local news because, I mean, it did appear to be pretty
lengthy. Okay. Why haven't you made a video taking a victory
lap about the PA resignations, the Palestinian Authority resignations.
After all the shade thrown your way, I'd be reminding everybody, I haven't had time.
Believe me, I've wanted to make that video.
For those that don't know, a while back I put out a video talking about the Palestinian
authority and how it was the most likely entity that currently existed that would end up asserting
governing duties over both West Bank and Gaza. And the Biden administration kind of had a plan
hoping to revitalize it. And I said that I thought that many of the people involved might resign
given the stakes and basically clear the way for some new blood because a lot of the figures
in the Palestinian Authority, they are not, they don't have a wide base of support.
So some of them would need to go and I said that I thought that many would do it willingly
simply because of the stakes.
And oh my, the amount of pushback I got from that and messages was just wild.
And I would like it noted that, yeah, this week it happened.
They did resign for those reasons.
And they've made it very clear that they're looking at trying to reestablish things and
revitalize it. And it does have to do with, you know, extending their duties to Gaza.
The current, let's see, the prime minister, the cabinet, they've resigned. They're currently
still involved, but in like a caretaker role. They're not like an interim government thing.
And it basically exactly what I said occurred.
And yes, I actually had a plan to make a video about it because one message I got was incredibly
lengthy and detailed about how ignorant I was.
And it was a person who used their full name and their PhD initials at the end.
And they went to great lengths to explain that I didn't know what I was talking about
and that would never happen and there was no way that the Palestinian Authority would
ever even get their tax payments restarted, which by the way, that's happened too.
And went on to say that I was the worst foreign policy commentator that they had ever heard
off. But you have heard of me. Yeah, so that was one that I definitely planned on doing,
but there's too much real news going on right now for me to make a fun video. So yeah, there
are, that's actually news that should kind of be covered. There are some major developments
as far as the pieces that have to be there to get a durable piece going, there are big
steps being taken.
Some of them they may not sound like, in fact, they may not even make sense because the way
this has been covered in most Western outlets is, hey, the Palestinian Authority resigned.
And there's not a lot of context in the idea that this is to establish something more unifying
that would eventually kind of form the basis of what could lead to a Palestinian state
and that this is something that the Biden administration have been pushing for.
Okay.
Do you think uncommitted voters could evolve into a viable party?
No, not really.
It's a single issue thing, meant to message.
That's what it was.
It sent the message.
We'll see how the Biden administration and Biden campaign responds.
I was wondering if you have seen the West Virginia SB195.
I doubt it will pass, but it's scary to see that such a bill would even be proposed.
I think so.
In fact, I think I have a video on it.
If this is the thing about the librarians, yes, and I have a video, but it's one of
the ones that keeps getting pushed back by breaking news.
So you should see it soon.
And I'm assuming, I mean, it's West Virginia.
I'm assuming that's what this is.
I can't remember the bill number off the top of my head.
The recent questions, comments about the mic you're using
got me thinking.
Any chance you could point that thing outside one evening
so we can hear the crickets again?
I'm half joking, but it would be cool to have a lo-fi bow
to relax slash study to on the roads.
But the audio is just the crickets for 25 minutes.
Yeah, I mean, I could do something like that at some point.
And then there is a note for y'all to be ready.
Because sometime in the next few days,
we are either going to do a live stream or on, I think,
the 6th, maybe the 7th.
early in March, there's a giving day for the California State University, Northridge.
And we'll be doing the support for Project Rebound again, which is a, it's a thing that
helps formerly incarcerated people and helps them with schooling.
And it's a pretty successful program.
we've done stuff for it before and it is that time again. So either we're going to do a live stream
or there'll be a video like directing y'all to their website to donate directly or maybe both.
it just depends on how I continue to feel and how the news over the next week
develops so okay and that looks like it I knew it was going to be short okay so
there you go a little bit more information a little more context and
having the right information will make all the difference y'all have a good
day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}