---
title: Let's talk about Trump, NY, and his day getting worse....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=WhRu_ruieAY) |
| Published | 2024/02/07 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's appeal claiming immunity and the ability to do as he pleases was rejected by the appeals court.
- Weisselberg, involved in the New York Civil case, is reportedly considering a plea agreement for perjury.
- If Weisselberg admits to lying under oath, it could significantly impact the case against Trump.
- The judge in the case expressed a keen interest in Weisselberg's potential change in testimony, which could have serious implications.
- The court may adopt the doctrine of "false in one, false in all," potentially discrediting all of Weisselberg's testimony.
- The judge emphasized the importance of not overlooking any details in a case of such magnitude.
- The outcome of the New York case appears unfavorable for Trump, and Weisselberg's potential confession could worsen it.
- The judge's focus on Weisselberg's credibility could sway the decision to a severe penalty for Trump and his organization.
- The court has set a deadline until Wednesday at 5 p.m. for Trump and his legal team to respond.

### Quotes

1. "Trump's appeal claiming immunity and the ability to do as he pleases was rejected by the appeals court."
2. "The court may adopt the doctrine of 'false in one, false in all,' potentially discrediting all of Weisselberg's testimony."
3. "The outcome of the New York case appears unfavorable for Trump, and Weisselberg's potential confession could worsen it."

### Oneliner

Trump faces a potential setback as Weisselberg contemplates a plea agreement for perjury, impacting the New York case's outcome and possibly leading to severe consequences, with a deadline for response set until Wednesday.

### Audience

Legal experts, political analysts

### On-the-ground actions from transcript

- Respond by Wednesday 5 p.m. (implied)

### Whats missing in summary

Insight into the potential legal implications and the significance of Weisselberg's actions.

### Tags

#Trump #LegalCase #PleaAgreement #JudicialProcess #Weisselberg


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk a little bit about Trump
and how his day isn't getting any better.
So if you missed it, Trump's appeal,
his claim that he's immune to like everything in the world
and can do whatever he wants,
that was not accepted by the appeals court.
But there's this whole other thing
that's been going on that now seems to be kind of coming to a head. In the New
York Civil case, there's been a lot of reporting suggesting that Weisselberg is
looking at taking a deal, looking at taking a plea agreement for perjury.
Obviously, that would alter things. If Weisselberg admits to lying under oath,
that changes things a little bit. Now just in general it's bad news if
somebody who provided testimony on your behalf is then admits you know to
perjury. It becomes worse news when the judge involved in a case that you're
involved in takes a very keen interest in it and sends a message that includes
as the presiding magistrate, the trier of fact, and the judge of credibility, I of
course want to know whether Mr. Weisselberg is now changing his tune and
whether he is admitting he lied under oath in my courtroom at this trial." I
I mean, that's a pretty direct question.
And the judge goes on to kind of indicate that, yes, the article and all the reporting,
it is focused on Weisselberg and claims about the size of Trump's apartment and stuff like
that.
It's relatively minor stuff. The judge is indicating that the court may adopt the
doctrine of, well, false in one, false in all. If you have lied about this thing in this instance
and in this courtroom, we can't take any of your testimony seriously. That's something that would
be incredibly damaging to the former president's case. And the judge says, I do not want to ignore
anything in a case of this magnitude.
So at the end here, what we have is a situation where the outcome of the New York case, it
already doesn't look good for Trump.
Like it already looks like it's going to be bad.
The judge taking an interest in this means that perhaps the judge is on the fence about
it being bad and the absolute worst possible outcome for the former president and that this
might be a deciding factor because there's if if Weisselberg lied under oath and is going to
admit that there's a whole lot of things that the court can kind of pull from that when it comes to
the honesty of, I mean, kind of the entire defense really.
So it may be that thing that sends the decision over the edge into a very extreme penalty
for the former president and the organization that was on the table, but nobody was really
sure if it was gonna be applied. The interest from the court in this might
mean that they were on the fence and this may be the deciding factor. Trump
and the legal team have until Wednesday to to respond. 5 p.m. Wednesday. Anyway,
It's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}