---
title: Let's talk about Netflix, Alexander the Great, and disbelief....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=4QaqcxR_3fI) |
| Published | 2024/02/07 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing a new documentary sparking controversy and concerns.
- Referring to a documentary on Netflix called "Alexander, The Making of a God."
- Comments on the portrayal of Alexander the Great being gay within the first eight minutes of the documentary.
- Expresses surprise over the casting choices and assumptions made in the documentary.
- Asserts that historically, Alexander the Great may have been more accurately depicted as bi rather than gay.
- Points out that during the ancient period in question, individuals weren't labeled as gay because it was a common practice.
- Mentions historians' beliefs regarding Alexander's affection for some of his generals.
- Criticizes modern conservatives for being offended by historical depictions of homosexuality.
- Contemplates the sadness of being unable to watch a documentary without finding something to be upset about.
- Concludes by reflecting on the warped views of individuals concerned about ancient practices and orientations.

### Quotes

1. "Alexander the Great was, well, I mean, he wasn't gay."
2. "I want you to picture how sad it must be and how lonely it must be to be incapable of watching a documentary without finding something to be upset about."
3. "How sad it must be to have your view of the world so warped."
4. "I can't think of many reasons why people would be this concerned about the orientation and practices of people thousands of years ago."

### Oneliner

Beau clarifies historical misconceptions and criticizes modern conservatism's reaction to a documentary on Alexander the Great's sexuality, questioning the obsession with ancient orientations.

### Audience

Viewers, History Enthusiasts

### On-the-ground actions from transcript

- Watch the documentary to form your own opinions and understanding (implied).

### Whats missing in summary

Beau's humorous and insightful commentary on the intersection of historical accuracy, modern perceptions, and societal attitudes.

### Tags

#Documentary #AlexanderTheGreat #HistoricalAccuracy #ModernConservatism #Sexuality


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about a new documentary, I
guess.
It's apparently on Netflix.
I don't know.
I haven't seen it.
But I don't feel like that's going to hinder me providing
commentary and clarifying something for a few people.
because this documentary, it has apparently
sparked some conversations and raised some concerns for people.
And I was sent screenshots of these conversations
all over social media.
And I feel like I can address some of it.
The name of the documentary is apparently
Alexander, The Making of a God.
Netflix loves to inject wokeness into everything.
And it's a picture of what I'm assuming is
Alexander and a man kissing.
Netflix made a new documentary about Alexander the Great.
within the first eight minutes, they turned him gay.
It just goes on and on.
I like this one.
I actually, I'm hoping this one is satire.
I'm honestly surprised they didn't cast a black actor
to play Alexander the Great.
OK.
Yeah, so I feel like this doesn't need to be said,
but apparently it does, because they're super mad.
Alexander the Great was, well, I mean, he wasn't gay.
That's not an accurate depiction.
More bi, I guess, would be how to do that.
Let's just do it this way.
When it comes to this area in this period of the ancient
world, nobody was gay, because everybody was gay.
just let it go. That's not wokeness, that's just what happened. It's pretty well documented.
There's a whole lot of things that lead historians to believe that Alexander really loved some
of his generals. I personally find it hilarious that people who, generally
speaking, view themselves as the protectors of Western civilization, you
know they use those terms, are unaware of some of the more open behaviors that
occurred the founding of Western civilization. Aside from that, I want it
noted that the modern conservative of today is so offended by this, by
homosexuality in general, I guess. They're upset about stuff that happened
thousands of years ago and they don't want it depicted in a documentary. This
isn't even like a movie where they, you know, inserted a love interest that
didn't really need to be there because it's really an action movie about
Alexander's. No, this is a documentary. Yeah, I would imagine they're gonna cover
that. I want you to picture how sad it must be and how lonely it must be to be
incapable of watching a documentary without finding something to be upset
about. And how sad it must be to have your view of the world so warped that
you think the introduction of a male love interest for Alexander the Great is
wokeness. It's sad. I can't think of many reasons why people would be this
concerned about the orientation and practices of people thousands of years
ago.
And I will just leave them to sort those issues out.
Anyway, it's just a thought.
have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}