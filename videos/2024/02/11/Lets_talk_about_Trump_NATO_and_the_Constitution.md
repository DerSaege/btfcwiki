---
title: Let's talk about Trump, NATO, and the Constitution....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Wz9n2KWw214) |
| Published | 2024/02/11 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau dissects Trump's claims about NATO and the US Constitution, focusing on his misleading statements and their implications.
- Trump falsely took credit for NATO payment increases in 2014, claiming he forced other countries to pay more when he wasn't even president then.
- Beau explains the limited nature of treason in the US Constitution and how Trump's statements could be seen as treasonous.
- Trump's narrative of not protecting NATO countries if they didn't pay up goes against the NATO treaty's Article 5, which states an attack on one member is an attack on all.
- Beau criticizes Trump's undermining of NATO, citing it as a cornerstone of US national security and Western security.
- Beau questions the competence of someone who disregards the importance of NATO and its role in global security.
- He challenges individuals who oppose NATO from an ideological standpoint to reconsider their stance in terms of US national security.
- Beau concludes by suggesting that those who advocate for weakening national security might not be fit for the presidency.

### Quotes

1. "Treason in the United States, oh, it's special, it is special."
2. "If you follow it through and ask them if they care about US national security, I'm willing to bet that every one of them that says that NATO shouldn't exist will say no."
3. "That's your position and there's nothing wrong with that inherently."
4. "It's just a thought y'all have a good day."

### Oneliner

Beau dissects Trump's false claims on NATO and the US Constitution, questioning his competence and loyalty to national security, while challenging opponents of NATO to reconsider their stances.

### Audience

Policy Analysts

### On-the-ground actions from transcript

- Educate others on the importance of NATO for national security (implied)
- Advocate for strong alliances with global partners for enhanced security (implied)

### Whats missing in summary

A detailed breakdown of the consequences of undermining NATO and how it impacts global security.

### Tags

#Trump #NATO #USConstitution #NationalSecurity #Treason #PolicyAnalysts


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about Trump and NATO
and the US Constitution.
And we're just going to run through some things
because Trump had a really interesting story that he told.
And I feel like it's worth going over his story
and what he was saying and comparing it
to the real world, because I think it's going to teach us something not just about what
he said, but about the people who cheered it.
Before we get into this, I want to remind everybody that one of my, I don't know, one
of the things that just bothers me is when people misuse the term treason.
Because treason in the United States, oh, it's special, it is special.
is defined in the US Constitution. It's kind of cool. It has a special place in that way.
And it's limited in the United States. Treason is incredibly limited in the United States.
In videos about this, I normally say, in the US, you have to try to commit treason.
Okay, so let's talk about what Trump said. First, he said that, you know, he was meeting
with the NATO people and he forced them to pay more and he did this by, it doesn't matter
because he's lying.
The deal to increase all the payments that happened in 2014, he's taking credit for something
he didn't do.
He wasn't president then.
Now as far as his little story, he was talking about countries who weren't paying what he
felt they should pay.
And he said that, he told him that he wouldn't protect him if Russia attacked him.
And then he told this story about the leader of a big country, didn't say which country,
but I'm sure whoever this was stood up and said, sir, and held him with both hands and
he was a big strong man with tears in his eyes or whatever.
But he said that this person asked if Russia attacked this big country that was a member
of NATO, and they weren't paid up, that Trump really wouldn't protect him.
I said, you didn't pay?
You're delinquent?
No, I would not protect you.
In fact, I would encourage them to do whatever they want.
You gotta pay.
So he's saying that if Russia attacked a NATO ally, that he would encourage Russia.
He would adhere to them.
That's interesting.
Okay, so first we're going to start with this.
It's part of the U.S. Constitution.
I am certain that all of Trump supporters have read the U.S. Constitution.
I know this is just a refresher.
It's not like I believe this is the first time you're ever going to hear this because
never read it. This constitution and the laws of the United States which shall be made in
pursuance thereof and all treaties made or which shall be made under the authority of the United
States shall be the supreme law of the land. It's called the supremacy clause if you want to look
it up for yourself. So, treaties are part of that. You probably didn't know that. Now,
if you want to know what's interesting about the NATO treaty, Article 5 kind of
enshrines the idea that an attack on one is an attack on all. So, if somebody was,
let's say Russia, if they were to attack a NATO country, even a big NATO country,
Well, that would be an attack on all members of NATO.
Article 3, Section 3, Clause 1, treason against the United States shall consist only in levying
war against them or in adhering to their enemies, giving them aid or comfort.
If a nation attacked the United States and somebody cheered them on, that would probably
fall under that, especially if they were in a position to influence policy and use their
office to do so.
doesn't belong anywhere near foreign policy. His claims about everything he accomplished,
most of them cannot survive a basic fact check. His statements about how he would run things.
Either he doesn't understand how anything works or he's just lying. The idea that the
United States would shirk its responsibilities under NATO, that is the
kind of statement that undermines global security. I know that NATO is not well
liked by a whole lot of people and there's good reason for that. You can
debate whether or not NATO is a good thing. You can't debate that NATO in
many ways is the foundation of Western security, of the US national security.
Somebody who would undermine that for some just wildly uneducated take on
foreign policy probably doesn't need to be in the Oval Office. Now, there are going to
be a whole bunch of people who are probably going to comment talking about how bad NATO
is. And a whole lot of what they say, it's going to be true. And their take for a whole
lot of them, is going to be that NATO shouldn't exist and they're going to make that argument
from an ideological perspective, a philosophical one.
If you follow it through and ask them if they care about US national security, I'm willing
to bet that every one of them that says that NATO shouldn't exist will say no.
If that is your opinion that the US should have weakened national security, that's fine.
That's your position and there's nothing wrong with that inherently.
But I would suggest that maybe you're not a good candidate for President of the United
States.
Anyway, it's just a thought y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}