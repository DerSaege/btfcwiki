---
title: Let's talk about different Louisiana maps changing....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=_3XEqh1O_ao) |
| Published | 2024/02/11 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Louisiana is facing changes in state house and senate district maps due to violations of the Voting Rights Act.
- A federal judge ruled that the current maps must be changed as they intentionally diluted black voting power.
- New maps need to be created within a reasonable amount of time, likely not before the next election.
- The federal judge did not specify the exact changes needed in the new maps.
- There is an expectation of around six more majority black districts in the House and three more in the Senate.
- These numbers indicate significant gerrymandering to dilute black voting power in Louisiana.
- The ruling is in favor of the plaintiffs, aiming to replace biased maps violating the Voting Rights Act.
- The resolution of this issue is expected to involve appeals and delays, making it a lengthy process.
- Similar cases, which Beau has followed, have taken years to conclude.
- Beau concludes by acknowledging the time-consuming nature of such legal processes.

### Quotes

1. "The federal judge has ruled in favor of getting rid of the bias maps, the maps that are in violation of Section 2 of the Voting Rights Act."
2. "There was a very concerted attempt to dilute black voting power in the state."
3. "This is not going to be resolved quickly."
4. "There was some heavy, heavy, heavy gerrymandering going on."
5. "It's just a thought, y'all have a good day."

### Oneliner

Louisiana faces map changes violating Voting Rights Act, with significant gerrymandering diluting black voting power, leading to lengthy legal process.

### Audience

Louisiana residents, Voting Rights Advocates

### On-the-ground actions from transcript

- Monitor updates on the creation of new state house and senate district maps (implied).
- Stay informed about the ongoing legal process and potential appeals (implied).

### Whats missing in summary

Importance of staying engaged and advocating for fair district maps to ensure equitable representation.

### Tags

#Louisiana #VotingRightsAct #Gerrymandering #DistrictMaps #LegalProcess #Advocacy


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Louisiana and maps,
but they're not the same maps.
We've had a whole series of videos
about maps being discussed and changed in Louisiana.
This is a different set of maps,
but it's kind of a similar story
because the same thing's about to happen.
Okay, so we have talked about how the maps
the federal elections are going to change in Louisiana.
This is about the state maps, the maps for state, house, and senate districts.
Okay, a federal judge has basically said the current maps, they gotta go.
Why?
They violate section two of the Voting Rights Act.
The plaintiffs in the case alleged both cracking and packing in an effort to dilute black voting
power.
Once again, the maps were found to have been intentionally constructed to take away the
voice of black Americans.
Okay, so that has been found by a federal court.
The new maps have to be created by when, quote, a reasonable amount of time.
There's no date on this one yet, I think because it's kind of at the point where this is not
going to be resolved immediately, probably not before the next election, almost certainly.
So there's not a rush on this one.
So there's no set time period for this to be resolved, but there needs to be new maps.
The next obvious question is how are the maps going to change?
In this case, the federal judge did not provide a set amount.
In a lot of the cases that we've talked about where we've been talking about gerrymandering,
we have heard judges say, okay, you need to create at least two more districts that have
a majority black voting base or something like that.
There's nothing like that in this ruling.
The expectation is that it will be around six more districts in the House that are majority
black and three more in the Senate.
Those are big numbers.
If you're not familiar with this stuff, those are big numbers.
I have not personally looked at the state maps, but for them to be even remotely in
this range, there was some heavy, heavy, heavy gerrymandering going on.
There was a very concerted attempt to dilute black voting power in the state.
So the federal judge has ruled in favor of the plaintiffs, in favor of getting rid of
the bias maps, the maps that are in violation of Section 2 of the Voting Rights Act.
This is not going to be resolved quickly.
Go ahead and imagine a bunch of appeals and a bunch of delays.
the cases that we have been following up on and being here for the
conclusion of, many of them started like years ago. This is a lengthy process.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}