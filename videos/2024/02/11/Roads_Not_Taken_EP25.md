---
title: Roads Not Taken EP25
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=dekz21rEi_E) |
| Published | 2024/02/11 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the episode as episode 25 of "The Roads Not Taken," where he covers unreported or underreported news and answers viewer questions.
- Mentions the date, February 11th, 2024, and that it's Super Bowl Sunday.
- Talks about a mock-up of Taylor Swift on the cover of Madden, sparking reactions, and gives his opinion on it.
- Touches on foreign policy updates, including Israel's move into Ra'afah, Germany preparing for war with Russia, and Finland increasing ammo production.
- Notes Mexico overtaking China as the top exporter to the US and a Russian anti-war candidate being barred from running against Putin.
- Shifts focus to US news, mentioning House GOP's attempt to impeach the DHS Director, Texas GOP's decision on anti-semites, and Wisconsin GOP rep Mike Gallagher not seeking re-election.
- Mentions calls for McConnell to be ousted from Senate leadership.
- Talks about cultural news like Fauci's upcoming book, backlash over a black woman captain in Pirates of the Caribbean, and a reward for information on three gray wolves' deaths in Oregon.
- Mentions odd news like thieves stealing a 200-foot radio tower and insights from the former head of Pentagon's UFO-hunting office on conspiracy theories.
- Shares a Q&A segment where relationship advice and book recommendations on Trump are discussed.
- Addresses audience questions on US arms sales, manuals for understanding military and police actions, press biases, US foreign policy's impact on Latin America, and the potential horrors of a civil war in the US.
- Ends with a personal anecdote about his new German Shepherd puppy.

### Quotes

1. "Just asking them out. I'm not talking about going over the top or saying gross things in the process, but just asking them out."
2. "If the answer is no, it's no."
3. "The US foreign policy messed up all of those countries."
4. "It [a civil war in the U.S.] would be horrific."
5. "Y'all have a good day."

### Oneliner

Beau covers unreported news, foreign policy updates, US political developments, cultural news, and answers audience questions, ending with a personal anecdote about his German Shepherd puppy.

### Audience

Viewers

### On-the-ground actions from transcript

- Contact questionforboe@gmail.com for questions to be answered (implied)
- Seek information on US arms sales and their implications (implied)
- Look up field manuals on archive.org for insights into military and police actions (implied)
- Read "Taking Down Trump" by Tristan Snell for a different perspective on Trump (implied)
- Watch Beau's videos on US foreign policy's impact on Latin America for more detailed information (implied)

### Whats missing in summary

Insightful commentary on various global and domestic issues, along with practical relationship advice and book recommendations on Trump.

### Tags

#UnreportedNews #USForeignPolicy #GOP #CulturalNews #RelationshipAdvice #Trump #CivilWar #BiasReporting #LatinAmerica #GermanShepherd


## Transcript
Well, howdy there, internet people, it's Bo again,
and welcome to The Roads with Bo.
Today is February 11th, 2024,
and this is episode 25 of The Roads Not Taken,
which is a weekly series where we go through
the previous week's events and talk about some news
that was unreported, underreported,
just didn't get the coverage I thought it should,
or just something I found interesting.
And then at the end, we go through a little Q&A
with questions from y'all.
If y'all would like a question to,
if you have a question you want answered,
you can send it to questionforboe at Gmail.
That's F-O-R-B-E-A-U.
Please remember, there are a couple hundred questions a week,
so they're not all getting answered.
Okay, and today is Super Bowl Sunday,
the big sports ball game between the San Francisco Taylors
and the Kansas City Swifts.
I'm just saying that because I got a whole bunch of messages
from people who are very unhappy that the next Madden will
apparently have Taylor Swift on the cover of the game, which
is totally not true.
But somebody did a mock-up and put it on social media,
and people actually got mad about it.
I personally think that if Madden wanted to expand its base,
putting Taylor Swift in their game
would probably be a smart business move.
Okay, so starting off with foreign policy.
The big news is that Israel seems very committed
to its move into Ra'afah, which is, it's not a good idea.
a video out on the other channel about this. It's not a good idea, but Israel
seems very committed to this and that course of action. We'll have to wait and
see how it plays out, but I feel like that's going to be a mistake. A German
general caused waves by suggesting that Europe needed to prepare for war with
Russia sometime in the next five years, anticipating that Russia might stage an
attack on a NATO nation. Finland is increasing their ammo production by a
a massive amount, maybe up to 500%, and the obvious assumption is that a lot of this is
going to go to a Ukraine.
Mexico overtook China as the top exporter to the United States.
A Russian anti-war candidate was barred from running against Putin because, you know, yeah,
anyway.
So, moving on to U.S. news, the House GOP is set for a second try at impeaching the
DHS Director, the Homeland Security Director.
They failed in their first attempt to get that impeachment.
offhandedly admitted that January 6th was an insurrection, which was surprising.
The Texas GOP decided that they would ban, like, open anti-semites, which was... I mean,
because, you know, that wasn't actually policy until now. But that's, that's
happening that's a move in the right direction I guess. Wisconsin GOP rep
Mike Gallagher won't seek re-election which is surprising kind of an up-and-
comer in the Republican Party but has decided not to seek re-election. He is
actually one of the reps who voted to not impeach the DHS chief that may have
something to do with it. Now over in the Senate there are a number of people
calling for McConnell to get the McCarthy treatment and be ousted from
the Senate leadership. We'll see how that plays out and that might be one of those
be careful what you wish for things. Okay moving on to cultural news. Fauci has a
book coming out called On Call. I cannot wait to see the reaction to that. The
right-wing in the United States is apparently mad that a future Pirates of
the Caribbean movie will have a black woman as a captain. At least this is the
rumor going around on social media. They are mad because apparently it would be
unrealistic to have a black woman as a captain during this time period. In a
movie franchise with ghosts and barnacle people, you know, the realism would be
messed up by having a black woman captain. In environmental news, there is a
There's a $50,000 reward for information regarding the deaths of three gray wolves in Oregon.
They were all from the same pack.
I think two of them were even collared.
There's not a lot of information available right now, not even about like what happened
to them, but the obvious implication from the reward
is that there was human involvement.
In odd news, apparently thieves stole a 200-foot radio tower
from a small community radio station,
like the giant tower out anyway.
It looks like it was dropped and then
cut up and carted off. But that's just where it's at right now. I cannot wait to hear the
conclusion of that story. The former head of the Pentagon's All Domain Anomaly Resolution Office,
this is the office that hunted UFOs, it's like the X-Files, the former head of that office,
Sean Kirkpatrick basically said that it was the government's fault that conspiracy theories
existed because the secrecy creates an information vacuum and said our efforts were ultimately
overwhelmed by sensational but unsupported claims that ignored contradictory evidence
yet captured the attention of policy makers and the public.
In other news, a conspiracy theory believer is alleged to have killed his father, and some of
the allegations suggest the son was enraged because he learned his dad had gotten the vaccine.
So there's that.
OK, moving on to the Q&A section, it says, OK, we have a note, say the email address.
I already did.
And we have a bunch of questions about the AMOC.
Maybe link your previous video on it.
There must be new reporting on it.
Oh, the current.
The current.
So yeah, there's probably a bunch of new reporting.
OK, so yeah, I have done a video on this.
I will get it in the description.
If you're not aware, there is a real concern
about a specific ocean current collapsing
and the fallout from that.
My guess is that there's maybe signs
that the collapse is coming.
We reported on this about six months ago,
but I feel like there's probably been a new development
that I'm not aware of.
Hello, Bo.
Valentine's Day is coming up, and I
would like to ask you for relationship advice.
I feel like I can trust you, because unlike many people who
give dating advice, you are actually a feminist.
And you recognize that women's issues are real.
I know a lot of women get a lot more attention
than they would like and are often scared of men
who show interest because the men might turn violent
if they are rejected.
I don't want to add to these problems
or ruin some poor woman's day,
but I don't know how I would show romantic interest
without being a bother.
Do you have any advice?
Yeah, I mean, okay, so you have identified the issue,
one of them.
I'll let you know another as we move along.
Here's the thing.
If the situation is something where it, where she might easily believe that things could
go bad, that's not the right time to ask.
Don't I'm going to take this to an extreme, but you know, don't ask in a dark hallway
where it's just the two of you, you know, maybe ask her out in a place where
there's more people around, where you're not, where there's an avenue of escape.
If these are, if this is your concern, you can easily address that and just take
away the scenario that, that would make her feel uncomfortable.
Um, just add into this because it's not here.
You probably know this based on how the message is phrased, but, um, I would also suggest
not asking her while she's at work because it's the same.
She can't leave.
She can't leave and she's at work, you know, she has to smile and be nice and there's
a, uh, it's just not an ideal time to do that.
That can also create discomfort.
So here's the thing, you know, ruin some poor woman's day.
I don't know a woman that has ever really been upset, had their day ruined by a guy
asking them out, just asking them out.
I'm not talking about going over the top or saying gross things in the process, but
just asking them out.
The issue from my friends who have talked about it, it's not being asked out.
It's not taking no for an answer.
If the answer is no, it's no.
And I know that there's a school of thought when it comes to this that you should pursue
and all of that.
Guys don't even chase your whiskey, okay?
Just don't do that.
Not just does it generally not work.
If it does, it's probably not going to lead anywhere successful.
I would suggest being kind of direct in making sure that whatever you're asking is something
that is clearly a date.
Like it can't be mistaken for two friends going out, so there's no mixed messages there.
And ask, but be aware of the situation so you're not making her uncomfortable.
do it when she's at work and if she says no accept that answer. I would be I would
be leery of asking people of a different age group or of a different subculture
for advice because it those things change like as far as approaches as an
One example, my generation, when I was younger, if you were to ask a woman out via text or
over the phone, the answer would have been no.
Even if she wanted to, she would have said no.
That's really common now.
I would be very careful getting advice on approaches from people that aren't in your
same subculture and of the relatively the same age, because things are different.
So I would be direct, not pushy, except no for an answer, and to have a plan.
You know, when are you going to let me take you out?
And then, and again, see, that would be common in for my age to phrase it that way.
That would probably come off like super pushy today.
That may not be appropriate because it's assuming the answer is going to be yes.
But however you ask, make sure you actually have a plan.
If she says yes, know what you want to do.
Know what you want to do.
Don't leave it up to, okay, well, we'll figure something out.
No, have a plan right then.
Okay.
My 91-year-old dad is an old school Republican who thought Trump did a lot of good things,
even though when I asked, he couldn't give me an example.
I got him to watch the Jan 6th hearings, and this really changed his opinion.
Unfortunately, that was two and a half years ago.
Now he feels the country has gone down the tubes and we need a strong leader like Trump
to fix it.
He has no access to the internet or social media and is a devoted Fox News watcher.
He's an avid reader of nonfiction, so I want to give him a book on Trump.
Can you recommend a book by a reputable author that will show Trump as a corrupt, incompetent,
dangerous man who did not add any value to this country and never will.
Um, I'll tell you what, um, and before I do this, I need to say, I was given this book, um,
like for free, it was sent to me, uh, but I wasn't like paid for an endorsement or anything.
I just disclosed that I got the book for free.
I'm reading a book now by Tristan Snell, it's called Taking Down Trump, and it's about,
so he was one of the attorneys behind Taking Down Trump with the whole Trump University
thing.
That might be a good one.
It's an easy read and the way it's structured is pretty informative and it kind of gives
not just, it's not a book that is just like, this is why Trump is horrible.
It's a book about, kind of framed about like how to prosecute Trump, like a template to
do that.
But in the process, there's a lot of cool little insights.
And I think it would be interesting to read if...
I think it would be interesting for a Fox News viewer to read, because it's an entirely
different perspective on something.
So maybe try that.
In a recent Q&A, you mentioned your mic being sensitive.
I want you to know that I can't hear your stomach,
but I can hear you putting your hands
in your pockets for some reason.
And for some reason, I get a kick out of it.
Yeah, I do that when I put my hands in my pockets
when I'm standing up.
Or there was one video where I put my hand in my pocket
and I had change in it, and I didn't realize
I was moving my fingers in my pocket,
and you can actually hear like this jingle bell sound
throughout the whole thing.
It is amazing to me at times,
especially in this format here where I'm sitting down.
Like, you probably just heard me swallow.
The mic is amazingly sensitive
and it has turned all the way down.
Okay.
I read often about how the U.S. is selling arms
to another country.
Who makes the money?
Is the government a middleman or the actual seller?
Is there a stockpile?
Yes.
The answer to that is yes.
It depends on the situation.
In most cases, generally speaking,
the government is approving the sale.
The government isn't actually selling the weapons.
They're approving the sale.
And it has to do with exporting articles of war.
In those cases, the manufacturers make the money.
But at the same time, a lot of people responsible for
approving the sale can sometimes make money, too,
especially if it goes to their Congress, because the money
then goes to lobbyists, too, then, and it circles around.
In some cases, there is a stockpile.
A lot of times, that's really given more as aid, though.
And then there are other deals that
get made for weapons that are more covert at times.
I'm sure people can think of high profile examples of that.
So all of your options are possible,
but most times when the US is selling arms,
the US is approving the sale.
the government is approving the sale.
Is there an actual manual?
Is there an easy to understand version of it so that lay people can understand why military
and police act?
Oh no, yes, that is not a plot device.
There is an actual manual.
Go to archive.org and type in field manual counterinsurgency.
There are a bunch of different terms you can look for.
Archive.org has hundreds of field manuals, military field manuals online.
Now a lot of them are a few years old, especially when you get into the more sensitive stuff,
their older declassified versions.
It's also worth kind of noting that if you want to know how to do anything, the military
has a manual on it and they're written at a very basic level so soldiers can pick it
up and learn it.
And I mean anything, carpentry, plumbing, any blue collar skill, there's a field manual
for it or a technical manual.
That is just a grossly overlooked resource because it's free, the government paid for
it and you can access them for free in most cases.
But yes, when I talk about it's in the manual, that's never a plot device.
There's actual manuals on this stuff and the U.S. government spent millions developing
this stuff and it never gets applied. It never gets applied. Now that's more
specific to the military police manuals. That varies by jurisdiction. They are
harder to get at times. If you're really interested, what you can sometimes do is
go to where the police academy is because most times the police academy is
not a police academy where everybody goes. It's actually ran through like a
local community college or something like that and you can just buy the books.
Okay, thank you. A whole bunch of nice things about me and my ability to deconstruct current
events. I was wondering what you think of how the press, including the New York Times,
ran with Biden's verbal slip when responding to the special counsel's report, clearing him of
wrongdoing in his own documents case, but throwing him under the bus saying that he was a well-meaning
an elderly man with a poor memory. I did a video on this. The Frame Lab, a site founded
by a linguist, commented that this is how Republicans destroy Democrats, by framing
topics to their advantage and choosing to debate topics. And we aid them by arguing
back on their terms rather than starting from our own premises. The press could have just
This is well written, Trump loses again as Biden is cleared.
Democrats could then jump on TV and insist that this is an excellent time for Trump to
return any classified documents that he still has at his fraudulent business as well.
And end his betrayal of American national security.
What do you think is the best way to fight back against bias reporting and commentary?
I would love for the Democratic Party to go on the offensive when it comes to messaging.
If you look at my big critiques of the Democratic Party as a whole, it has to do with messaging
and that's probably my biggest complaint.
The only thing that even comes close to it is not running more progressive candidates
in primaries, but that has to do more with people on the bottom, not the democratic strategists.
It would be nice if the democratic party would stop playing softball.
And yeah, what you're describing here is a way to do it.
I've often heard you make mention of how US foreign policy has created a hostile environment
in Latin American countries, and as a result, people flee those countries for their own
safety.
Can you talk more specifically and give some examples?
I actually have two videos, I think, yes, I have two videos on this in detail.
One is about El Salvador specifically, I think.
And then I think somebody sent me a message, a very snarky one, basically saying, OK, well,
you did it with this one country.
What about everybody else?
And so I went through all of them.
I have a very in-depth video on this detailing what occurred in various countries going all
the way back to the Banana Wars.
Yeah, I'll find those and put them down there.
They are useful.
Those are older videos.
Those may be three or four years old.
I think those, yeah, I think those were made under Trump's time in office.
But when you talk about Latin America, Central America in particular, with the exception
of like Belize, we messed up all of those countries.
US foreign policy messed up all of those countries.
And you can start going way back, way back to, you know, Butler's time, to General Butler's
time and follow it in even as recently as the 2000s.
U.S. foreign policy has not been good and has created a lot of the issues that people
are fleeing there.
And yeah, I have these videos already made.
I will definitely dig them up.
They could probably use an update because I'm sure in the time that has passed since
those videos went out, the US did something to mess up another one of those countries
down there or to make the situation worse.
So yeah, look in the description for that.
What would a civil war in the U.S. look like?
It would look like a chaotic version of the breakup of Yugoslavia, as if that wasn't
chaotic enough.
It would be even more chaotic than that.
People who talk about a national divorce or a civil war in the U.S., they have no idea
what they're asking for. Whatever they're picturing, it's not that. It would
be horrible and countries around the world would keep it going. Arms would
flood in. It would be a disaster. There would be no glorious battles or
anything like that. It would be a disaster. It would destroy the country
and culture and everything that they proclaim that they love.
People who think that's a good idea have no clue what they're talking about.
It would be horrific.
And that's the question they chose to end on?
Really?
They had been doing so good with like ending on a light note.
can't think of any off the top of my head right now. Can't even think of any
funny stories or anything from the... oh, well I have one. So story from the ranch
here. We mentioned, I think I mentioned, that we got a new German Shepherd, a
puppy. She is still a puppy only now she's... you know, German Shepherds are
puppies like mentally, even once they're like 100 pounds. So she is now a hundred
pound puppy and she runs up and down the fence with Marilyn and the dog and the
horse have become, they like antagonize each other through the fence. It's the
weirdest thing but they have become friends so and it's it is unique okay so
there we go there's a little more information a little more context and
having the right information will make all the difference.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}