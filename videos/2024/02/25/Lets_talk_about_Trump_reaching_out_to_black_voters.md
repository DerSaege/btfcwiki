---
title: Let's talk about Trump reaching out to black voters....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=tEcTYIynMws) |
| Published | 2024/02/25 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces the topic of Trump's perception in reaching out to black voters.
- Trump claimed that his mugshot was number one, indicating his multiple indictments could endear him to black voters.
- A commentator on Fox suggested that black voters lean towards Trump because they are into sneakers.
- It was emphasized twice that Trump was connecting with black America because they love sneakers.
- The belief in Trump World is that to get the black vote, one needs to be arrested and have gold sneakers.
- Beau expresses skepticism about this portrayal resonating with black America due to its inaccuracies and stereotypes.
- Beau encourages those who ordered Trump's shoes to read the fine print on the website, especially regarding taxes.

### Quotes

- "To get the black vote in the United States, Trump world apparently believes you need to be arrested and have gold sneakers."
- "I'm not gonna provide a whole lot of commentary on this one. I'm gonna let the comment section do it for me."
- "There are certainly people in the comment section who can provide some more direct commentary who won't be having to guess how other people feel."
- "Y'all have a good day."

### Oneliner

Beau introduces Trump's perception on reaching black voters, including claims about mugshots, indictments, and sneakers, sparking skepticism and encouraging scrutiny of Trump's shoes' fine print.

### Audience

Commentators

### On-the-ground actions from transcript

- Read the fine print on Trump's shoes website to understand all disclaimers and tax information (suggested)
- Provide direct commentary on the topic in comment sections (implied)

### Whats missing in summary

Deeper insights into the potential impact of Trump's portrayal on black voters and the broader implications for political outreach strategies.

### Tags

#Trump #BlackVoters #Stereotypes #Perception #PoliticalOutreach


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about Trump
and Trump World's perception
and how they believe Trump is successfully reaching out
to black voters in the United States
and just kind of run through it.
I'm not gonna provide a whole lot of commentary on this one.
I'm gonna let the comment section do it for me.
Um, because it's, uh, it's quite a take to be honest, it's, yeah.
Okay.
So starting off, Trump said that, you know, his mugshot was number one, you
know, greatest mugshot un unbeatable, greatest words, greatest mugshots.
Um, he did really did say that he said his mugshot was number one.
and indicated that he believed
his multiple indictments would endear him
to black voters. The black voters would look at him
and see the same discrimination that they face
and that they would relate to him on that level.
The white billionaire who
has had some run-ins with DOJ
about race, they're going to connect on some level with that is Trump's apparent
belief. Not to be outdone, Fox, a commentator on Fox, said that black
voters were really going to lean towards Trump because, quote, they're into
sneakers. This is a big deal, certainly in the inner city. Yeah, so there's that.
And again, the same commentator said that he was, that Trump was connecting, quote,
connecting with black America because they love sneakers. So it's not even like
something that was said once is a throwaway line.
It was said twice.
So there you have it.
To get the black vote in the United States, Trump world apparently believes
you need to be arrested and have gold sneakers.
I feel like a whole lot of black America is not going to be happy with this
portrayal, the inaccuracies and stereotypes in it are not exactly things that I feel
like are going to resonate, particularly the idea about the discrimination aspect
of it. I feel like that's gonna fall flat but there are certainly people in the
comment section who can provide some more direct commentary who won't be
having to guess how other people feel. They can just say it for themselves.
However, if you are one of the people who ordered Trump's shoes, you might want
to go read the fine print because you'll get them eventually. Also taxes. There's
some things in the disclaimers and the fine print on the website that you should probably
review.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}