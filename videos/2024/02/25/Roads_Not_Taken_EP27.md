---
title: Roads Not Taken EP27
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=jHYZmeqGKM0) |
| Published | 2024/02/25 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Summarizes the past week's underreported or interesting news, including strikes against the Houthis, Russia's operation into Ukraine, Israeli-Palestinian ceasefire progress, and U.S. sanctions against Russia.
- Mentions Blinken's statement on Israeli settlements, U.S. launching sanctions against Russia, Alabama Senate's attempt to ban lab-grown meat, and House GOP members leaving positions.
- Talks about McCarthy's revenge operation in the House, a presidential ranking by scholars, Olivia Rodrigo's reproductive rights initiative, and Odysseus spacecraft's moon landing mishap.
- References a National Health Service Trust letter comparing milk from trans women to cis women, President Lincoln pardoning President Biden's ancestor, and a Q&A session about government branches overriding MAGA stunts and New York trucker boycott.

### Quotes

1. "Silence about Gaza is complicity."
2. "There is no protection in the Constitution against that type of authoritarianism because the founders never believed that those people [MAGA] could be voted in."
3. "What kind of injury, surgery did you have, and how did you have surgery without messing uploads? Planning, of course, planning."
4. "One of them [Taylor Swift] cares about you, the other one [Trump] doesn't."
5. "Having the right information will make all the difference."

### Oneliner

Beau covers underreported news, MAGA stunts, celebrity initiatives, and more, stressing the importance of informed actions.

### Audience

News consumers

### On-the-ground actions from transcript

- Direct concerns about Gaza to effective platforms (implied)
- Plan effectively for surgeries to avoid disruptions (implied)
- Make informed decisions about supporting billionaires (implied)

### Whats missing in summary

Beau's engaging delivery and nuanced insights are best experienced by watching the full transcript.

### Tags

#UnderreportedNews #MAGAStunts #Celebrities #InformedActions #Community


## Transcript
Well, howdy there internet people, it's Beau again and welcome to the roads with Beau today is February 25th
2024 and this is episode 27 of the roads not taken which is a weekly series where we go through the previous week's
events and
Talk about news that was under reported unreported didn't get the coverage it deserved or I just find it interesting
Let's see then at the end we go through a Q&A from y'all and I'm gonna be honest
I don't know if there's questions at the end of this one.
OK, so we're starting off in foreign policy.
OK, another round of strikes against the Houthis
have taken place.
They're unlikely to be the last in the seemingly never ending
back and forth.
They are also unlikely to be incredibly effective.
OK, the world is watching as Russia's three-day operation
into Ukraine, advances into its third year.
Let's see.
Progress is reportedly being made
on an Israeli-Palestinian ceasefire agreement backed
by the United States, but it has not been finalized yet.
At least not at time of filming, but it
does look promising for once.
Let's see.
Blinken has stated that Israeli settlements in the West Bank are, quote, inconsistent with international law.
This reverses the Trump decision
that was
done under the Pompeo Doctrine.
For a long time the U.S. position was that the settlements were, quote, inconsistent with international law. And then
Trump
said they were, basically,
under a different doctrine.
doctrine, so it is back to that.
The U.S. looks like they're launching a new wave of sanctions against Russia.
Moving on to U.S. news.
Not content with limiting your reproductive rights, the Alabama Senate is also trying
to ban lab-grown meat, so they can also control what you eat, freedom, because there's freedom
there.
That's what Republicans in Alabama stand for, freedom, just not for you.
Okay, lots of House GOP members are leaving their positions and not running for reelection,
with one Florida representative saying, I thought that some of our members would be
smarter.
The loss of a lot of the rising stars and some of the committee chairs within the party
has made a lot of people nervous.
concerned about losing the real political skill. And to be honest, yeah, it's a legitimate concern.
McCarthy appears to be engaged in a revenge operation against some of his opponents in the
house. Some of that is no longer really being subtle. It seems pretty out in the open.
But I would imagine that's going to heat up this week, maybe then the following week.
Okay, 154 scholars ranked the presidents.
Lincoln, FDR, Washington, and Teddy Roosevelt were the top four, Biden was 14th, and Trump
was unsurprisingly dead last.
In cultural news, Olivia Rodrigo launched a reproductive rights initiative.
I'm sure there's going to be a lot of commentary about that coming out of right-wing circles.
Okay, in scientific news, Odysseus, a private spacecraft, managed to make a soft landing
on the moon, however it then tipped onto its side after one of its feet got stuck.
It is very surprisingly, very surprising that a craft named Odysseus would end up taking
a detour.
A National Health Service Trust letter in the United Kingdom basically kind of indicated
that milk from trans women would be comparable to milk from cis women, so get ready for that to be
a massive talking point in the United States used by people who really, really want to be angry
about something. In oddities, it was revealed that President Lincoln pardoned President Biden's
great-great-grandfather for what appears to be his part in a knife fight back in the 1860s.
Okay, moving on to the Q&A, and there are some. Good. Okay, email address is Bo, or I'm sorry,
the email address is questionforbo at gmail. Okay, I looked recently through the U.S. Constitution
in search of something that would allow two of the three branches of government to override
a stunt like the MAGA in the house are pulling.
There is nothing in good faith they are doing and are blatantly taking orders from a non-U.S.
state actor."
Wow.
I couldn't find anything.
There must be some sort of provision for this, no?
No, there's not.
The founders of this country never pictured the American people being, being manipulated
to the point that they would elect MAGA. It's that simple. They never expected that form
of authoritarian to be elected. There is no protection in the Constitution against that
type of authoritarianism because the founders never believed that those people would be
voted in. It's really that simple. There's a person on Twitter who tags you almost every
day telling you silence about Gaza is complicity. I find it sad because you're not on Twitter
anymore. You do talk about it and because they don't tag their representatives. Just
you. What can you say to somebody like that? Yeah, well, let's start with that. For those
Those who don't know, I actually don't check my Twitter notifications anymore, so I wouldn't
have seen that.
Yes, I do cover Gaza, and they tag me, but not their representatives.
There are a lot of people who feel that commentators are better at representing them, and that
That may be true in the sense of representing their ideas, but we don't actually have the
power to force change.
But realistically, most of the representatives don't here either.
When it comes to stuff like this, when people are genuinely concerned about saving lives,
that's their goal, and they're operating in good faith, I would just try to direct them
somewhere more effective.
When it comes to stuff like this, I give people a huge pass on doing stuff that just doesn't
make any sense, if it's coming from a good place anyway.
What kind of injury, surgery did you have, and how did you have a surgery without messing
uploads?
I had an injury like 20 years ago and you know back when I was 220 pounds of twisted
steel and sex appeal it didn't really matter but as I have gotten older there needed to
be some reinforcement done when it came to my abdominal wall.
And how did you have a surgery without missing uploads?
planning, of course, planning.
What do you think of the New York trucker boycott?
You know, my main thought on that is once again, Trump the billionaire that is supposed
to be the guy standing up for the working class, he's asking the working class to
miss out on money to help him because he doesn't care about the working class.
And it's worth remembering that the other billionaire, the one they villainize all the
time, Taylor Swift, oh she's known for giving $100,000 tips to truckers.
One of them cares about you, the other one doesn't.
Just remember that when you're deciding whether or not to torch your career over some billionaire
you've never met.
And that looks like it.
That looks like all the questions.
So there you go, a little more information, a little more context, and having the right
information will make all the difference.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}