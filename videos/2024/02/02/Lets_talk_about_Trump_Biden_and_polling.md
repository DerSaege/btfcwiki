---
title: Let's talk about Trump, Biden, and polling....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=JoS4IycL3Os) |
| Published | 2024/02/02 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the latest polling data showing Biden leading Trump by six points nationally.
- Stresses that early polling is irrelevant and not predictive of the final outcome.
- Points out that the demographic makeup of supporters is more significant than the overall polling numbers.
- Mentions that Biden is significantly ahead with women and independents, which is not surprising considering certain policies.
- Emphasizes that preserving democracy was the top issue for independents in polling.
- Notes that Trump struggles with independents due to focusing on culture wars rather than policy.
- Comments on the pattern of far-right candidates doing well in primaries but failing in general elections.
- Suggests that the MAGA faction is viewed as a threat to democracy by independents.
- Concludes that while Trump is necessary to win a primary for the Republican Party, he may hinder winning a general election.
- Leaves viewers with a final thought on the current political landscape.

### Quotes

- "Preserving democracy. That's the number one issue among independents."
- "Independents don't want their culture war nonsense. They want policy."
- "You can't win a primary without Trump. You can't win a general with him."

### Oneliner

Beau breaks down early polling, demographics, and the importance of preserving democracy for independents, suggesting Trump's struggle with policy may impact election outcomes.

### Audience

Voters, Independents

### On-the-ground actions from transcript

- Contact local independent voters to understand their concerns and views (implied)
- Organize community events focusing on policy issues rather than divisive cultural topics (implied)
- Engage with others to raise awareness about the importance of preserving democracy in elections (implied)

### Whats missing in summary

The full transcript provides additional insights into the challenges faced by far-right candidates and the potential impact on election outcomes.

### Tags

#Polling #Biden #Trump #PreservingDemocracy #Independents


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about polling
and Biden and Trump and all of that stuff.
And we are going to reiterate something,
go over something again,
because it is incredibly important to understand it.
And this is the ideal time to do it.
Okay, so new polling is out.
And what does that new polling show?
It shows that Biden is up 50 to 44.
Biden is up six points over Trump, national polling.
Okay, so here's the point to remember.
Polling this early out is pointless.
It is not predictive, it is not indicative of anything.
It doesn't matter.
That's not just true when Trump is leading.
This polling is irrelevant when it comes to determining
the final outcome.
There are a couple of little interesting bits in it,
but it has nothing to do with who is going to win.
If you want to look at the interesting parts,
look at the demographic makeup of who supports who.
That's more than likely going to carry
through to other demographics.
Because remember, all of this polling is self-selected,
and it includes the people who take polls.
And the reason, I believe, the reason a lot of polling
has been less than predictive recently
is because of unlikely voters and because they're not really
getting an accurate sample.
But what they do get in terms of demographics might carry through.
What does it tell us? Exactly what you would expect.
Biden, way ahead when it comes to women. Shocker!
Mr. I'm the guy who made roe go away and claimed it.
Yeah, women didn't like that. Okay, that tracks.
Biden is way ahead with independence.
Yeah, that tracks too. All of that makes sense. Here is the one thing that I do
think is actually really worth paying attention to. When they were polling
people and they talked to independents, they asked them what they thought the
most important issue was. And you got a bunch of different answers, you know, the
economy, immigration, so on and so forth, a whole bunch of them. But do you know
what the number one issue was? Preserving democracy. That's the number one issue
among independents. Now, despite all errors in polling when it comes to
determining who's ahead and who's gonna win and all of that stuff, that question,
that doesn't have anything to do with that. That has to do with you are an
independent, how do you feel about this? That's probably a lot more accurate.
Trump is not doing well with independents.
This is something we have talked about over and over again on the channel.
It's the truth behind most MAGA candidates, most far-right candidates.
They do well in the primary, and then they fail when it comes to the general.
Because independents don't want their culture war nonsense.
They want policy. They don't want to be told who to be afraid of and who to hate.
And apparently they see the MAGA faction as a threat to democracy. I can't imagine
why January 6th. I don't anticipate that part changing between now and the
election. When it comes to the Republican Party, you can't win a primary without
Trump. You can't win a general with him. That includes Trump. Anyway, it's just a
thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}