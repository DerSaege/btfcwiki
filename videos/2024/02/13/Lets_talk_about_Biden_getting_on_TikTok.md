---
title: Let's talk about Biden getting on TikTok....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=cIzmm06l3ms) |
| Published | 2024/02/13 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau dives into the topic of Biden joining TikTok to reach out to younger voters.
- The idea of Biden getting on TikTok may not be as straightforward as it seems.
- TikTok's demographic is primarily people under 30, which may pose challenges for Biden's campaign.
- There are associated risks with Biden producing content for TikTok, including potential backlash and criticism.
- Beau suggests that Biden's campaign may need to involve surrogates who are younger to effectively reach TikTok users.
- The success of Biden's presence on TikTok depends on how authentic and genuine the campaign is in its approach.
- Beau mentions the importance of allowing Biden to be himself rather than trying too hard to fit in with TikTok trends.
- Surrogates may play a significant role in carrying forward Biden's message on TikTok.
- Beau points out the idealistic nature of a wide portion of TikTok's user base, which may scrutinize and question posts from the president.
- The success of Biden's TikTok venture hinges on how well the campaign navigates the platform's dynamics and engages with its audience.

### Quotes

1. "Hello fellow young people, how do you do today?"
2. "It's not the message, it's the messenger."
3. "If they allow Biden to be Biden and be, you know, kind of cool grandpa, it might do all right."
4. "There's a lot there that could go wrong."
5. "It all depends on how the campaign plays it."

### Oneliner

Beau analyzes the potential risks and challenges of Biden joining TikTok to reach younger voters, stressing the importance of authenticity and surrogate involvement.

### Audience

Campaign Strategists, Social Media Managers

### On-the-ground actions from transcript

- Involve younger surrogates in carrying forward the message on TikTok (implied)
- Ensure authenticity and genuineness in content production for TikTok (implied)
- Navigate TikTok's dynamics and audience engagement effectively (implied)

### Whats missing in summary

Insight into the impact of Biden joining TikTok on his outreach to younger voters and the potential consequences of not effectively engaging with TikTok's user base.

### Tags

#Biden #TikTok #YouthVote #SocialMediaMarketing #CampaignStrategy


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Biden and TikTok
because apparently Biden is getting on TikTok.
As soon as that news broke,
I don't know, probably 25 messages coming out.
People of different age brackets,
all asking the same thing.
Is this a good idea?
The fact that people from all age brackets are asking that means it's either a good idea
or it's a horrible one.
There's really no middle ground on that.
So the idea is that the Biden campaign is going to get on TikTok and try to reach out
to younger voters.
And I mean, when you say it like that, I mean, sure, that sounds okay.
That doesn't sound like something that could go wrong.
Hello fellow young people, how do you do today?
It might.
This might be something that is better left to surrogates.
The popularity of TikTok is people under 30.
That's where that base is.
Biden getting on TikTok and producing like the normal stuff, the normal stuff that his
social media teams produce, not really going to be super effective.
And if he gets on there and puts out stuff that would be relevant to TikTok, A, there's
going to be a lot of pushback because there are some things that the TikTok demographic
is very upset with Biden about.
But there's also the risk of, you know, Biden posting cringe on Maine and upsetting people.
There's a risk associated with this.
Now, if they were to put out normal content to show that they're on TikTok and just lightly
delve into some of the stuff that occurs on TikToks, maybe some of the trends and stuff
like that.
I mean, yeah, that could work, but it's also not going to be super effective.
I think what you'll probably see is the Biden campaign doing normal Biden stuff, maybe stuff
with him eating ice cream and cracking jokes like dad jokes.
Maybe some dark Brandon stuff if they go, you know, really kind of lean into it.
But I think you'll also see surrogates, people who are aligned with Biden that are younger
pushing that message out.
And it's, there's definitely associated risks.
are associated risks with this because there is a wide, there's a wide portion of the base of TikTok
that is entirely idealistic. They are ruled by their morals and that's not necessarily criticism.
And they're not going to allow posts from the president to go unquestioned.
There's a lot there that could go wrong.
If it's done right, yeah, it could help.
Maybe a point or two.
But there's also an associated risk.
At the same time, it's not like this isn't something they could pull the plug on at any
time, and it's not like they risk losing that wide portion that is going to respond,
that is just going to be constant backlash for the Biden campaign.
It's not like they risk losing those voters anyway, because they're not voting for Biden.
It's unique.
It all depends on how the campaign plays it.
If they try to lean too hard into it, it will come off as very inauthentic and it just won't
work and might even have a negative impact.
If they allow Biden to be Biden and be, you know, kind of cool grandpa, which is, you
know, that was how it was described.
know, the last election was described as an election between the kind of cool
grandpa and the wannabe fascist. That's how it was described by a European to
me, who only consumed American politics via memes. So if they lean into that, it
It might do all right, but they have to let him be him and not try to push it.
They have to use the surrogates, other people, to carry that message forward.
Sometimes it's not the message, it's the messenger.
And Biden is very unlike most people on TikTok.
Anyway, it's just a thought y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}