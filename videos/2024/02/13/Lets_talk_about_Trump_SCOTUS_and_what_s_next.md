---
title: Let's talk about Trump, SCOTUS, and what's next....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ExhK9yOfhLo) |
| Published | 2024/02/13 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains Trump appealing to the Supreme Court regarding a DC federal case where he claims presidential immunity.
- Trump argues that he can do whatever he wants because the Constitution doesn't matter.
- The appeals court rejected Trump's argument that the President can violate laws meant to limit his power.
- Trump's last-minute appeal to the Supreme Court raises questions about possible outcomes.
- Supreme Court could agree to hear the case beyond the election, fast track it, or deny the petition.
- If Supreme Court denies Trump's argument, the appeals court ruling will stand, and the trial will restart.
- There is a possibility that the Supreme Court may not even hear Trump's argument due to its lack of constitutional basis.
- Beau predicts that the Supreme Court may grant a brief stay and then deny Trump's appeal.
- He believes that Trump's argument lacks legitimacy and is unlikely to succeed.
- Beau questions the relevance of the Supreme Court if Trump's argument is accepted, as it renders the court powerless.
- Concludes by expressing doubt that the Supreme Court will support Trump's argument and underscores the potential consequences.

### Quotes

1. "I'm president, therefore I can do whatever I want. I'm the absolute monarch."
2. "If this argument is accepted, the Supreme Court is irrelevant. They don't matter at all."
3. "There's no reason to even have a court."
4. "Nothing's illegal because official acts from the president wouldn't be able to be questioned."
5. "Even with this court, I find it unlikely that they're not going to see the downstream effects of what Trump is asking them to agree to."

### Oneliner

Beau breaks down Trump's appeal to the Supreme Court, questioning its legitimacy and potential consequences.

### Audience

Legal scholars

### On-the-ground actions from transcript

- Analyze and stay informed about legal proceedings related to presidential immunity (suggested)
- Advocate for upholding constitutional checks and balances (implied)

### Whats missing in summary

Insights on the potential impact of the Supreme Court's decision and the broader implications for American democracy.

### Tags

#Trump #SupremeCourt #PresidentialImmunity #Constitution #ChecksAndBalances


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump
appealing to the Supreme Court and what the options are.
I think that is probably the thing
that's being the least covered.
You have a lot of commentary
about what everybody thinks will happen,
but there's not a lot showing the different outcomes
that could occur, so I think we'll go through that.
If you have no idea what I am talking about,
in the DC federal case under Judge Chutkin,
this is Trump claiming presidential immunity.
He's allowed to do whatever he wants
because the Constitution doesn't matter, short version.
He took it to the appeals court, and the appeals court
was like, yeah, no, can't really co-sign an argument that
says the executive branch, the President of the United States, is allowed to violate all
of the laws that are meant to be checks on his power.
So they didn't side with him.
Trump, of course, waited until the last moment to appeal to the Supreme Court.
Now what happens from here?
Okay, so the first option that the Supreme Court could exercise, they could agree to
hear it and throw it on the schedule.
If they do that, this trial, this case, it is beyond the election now.
That simple.
Another option would be for them to agree to hear it and fast track it, in which case
they'd hear it in weeks.
They would hear it within weeks, and not to presume the outcome, but odds are the trial
would start shortly thereafter.
Now, another option would be for them to deny the petition after a brief stay, in which
case the appeals court ruling would stand.
The trial would restart.
And then the other option is they could just deny it from the start.
There is no obligation for the Supreme Court to actually hear Trump's argument.
It is so far outside the bounds of what could even remotely be considered a real constitutional
argument.
There are some people who are hopeful that they're just going to be like, yeah, we don't
need to hear this.
The appeals court ruling stands.
And that's a possibility.
Those are your four outcomes.
Which one?
I'm going to say that they will grant a brief stay and then deny it.
But any of these are possible.
I think your least likely is that they agree to hear it without expediting it, then your
next least likely would be for them to agree to hear it on an expedited schedule, and
then probably denying it flat out, and then granting a brief stay so they can appear that
they considered it, and then denying it.
Realistically, when you really look into this argument, it doesn't have legs.
It is incredibly unlikely that this goes anywhere.
If it does, the Supreme Court doesn't matter.
If it does, Biden doesn't have to hold another election.
It is an argument that really doesn't seem to have a basis in the Constitution.
It seems to be, I'm president, therefore I can do whatever I want.
I'm the absolute monarch.
I am the dictator.
I get to decide everything.
The Constitution, laws, none of that applies to me.
That in essence is the argument.
I feel like the Supreme Court won't go for that.
Even though a lot of them are supportive of Trump, I don't think that they'd be willing
to give up all of their power.
Because understand, if this argument is accepted, the Supreme Court is irrelevant.
They don't matter at all.
They become nothing.
They are irrelevant because they can't, they won't be able to look at what the executive
branch does.
So why do they even need to exist?
There's no reason to even have a court.
The president could disband them or take away their ability to have a schedule in here cases.
could stop them because nothing's illegal, because official acts from the president wouldn't
be able to be questioned.
So even with this court, I find it unlikely that they're not going to see the downstream
effects of what Trump is asking them to agree to.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}