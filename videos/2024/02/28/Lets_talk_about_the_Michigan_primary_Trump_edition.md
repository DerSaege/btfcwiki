---
title: 'Let''s talk about the Michigan primary: Trump edition....'
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Ye7X9itJXKQ) |
| Published | 2024/02/28 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing the Michigan primary results for Trump after discussing Biden's loss in the previous night.
- Biden lost around 13-15% of the vote to a protest vote, causing concerns for the general election.
- Trump also lost about a third of the vote, significantly more than Biden, with limited media coverage on his losses.
- People casting votes for Haley as a protest, expressing their displeasure, despite not expecting her to win.
- The media focuses on uncommitted voters and Biden, downplaying Trump's significant losses.
- The media has a vested interest in prolonging the Trump narrative, despite doubts about his strength as a general election candidate.
- Trump's struggle to capture centrist voters due to defections within his own party raises doubts about his appeal in the general election.
- Observes a double standard in media coverage when discussing Biden's losses versus Trump's losses.

### Quotes

1. "Biden lost around 13-15% of the vote to a protest vote."
2. "Trump once again lost about a third of the vote, with limited media coverage on his losses."
3. "People casting votes for Haley as a protest, expressing their displeasure."
4. "The media focuses on uncommitted voters and Biden, downplaying Trump's significant losses."
5. "This is a moment where you can see the double standard that gets applied when you're talking about Biden and Trump."

### Oneliner

Analyzing Michigan primary results reveals significant losses for both Biden and Trump, with media bias evident in coverage.

### Audience

Political analysts

### On-the-ground actions from transcript

- Examine media coverage biases (implied)
- Stay informed on political developments (implied)

### Whats missing in summary

Insights on the impact of media bias and public perception on election outcomes.

### Tags

#MichiganPrimary #Trump #Biden #MediaBias #ElectionAnalysis


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about the Michigan primary,
Trump edition.
We did Biden last night.
So today it's Trump, but it's also about the media
because you're gonna kind of realize some things,
at least I hope so.
So, Biden last night, he took a pretty big hit.
a pretty big hit, I think at the time I recorded mine was 15%, 15% of the vote was lost to
a protest vote.
And I think now it's right around 13%, because they're still counting, it's fluctuating
a little bit.
And I talked about how that's enough.
Biden is going to want to pick up the phone.
They're going to want to talk to these people.
This is a large enough amount to be concerned about when it comes to the general, 13-15%.
And you see stories like that all over the media right now, already, that I filmed this
last night.
They're already out talking about how Biden's in danger with 13-15%.
So let's talk about how Trump did.
He once again lost about a third of the vote.
Odds are you're not going to see those articles about Trump.
Trump is routinely losing more than 13 to 15 percent, and at this point I think most
people know that barring something knocking Trump out of the race, Haley's
not really the candidate. She doesn't really have a clear path to actually
win. She's staying in and hanging her hopes on the idea that something else
makes Trump less interested in being president. She is a protest vote. People
don't expect her to win, but they're casting their vote that way for the same reason, expressing
their displeasure.
You will undoubtedly see a flood of articles about the uncommitted voters and Biden.
How many do you think you're going to see about Trump losing twice as much?
Not many.
There is a vested interest on the part of the media to keep the Trump show running for
as long as possible.
He is not shaping up to be a strong candidate for the general.
He's doing well in the Republican primary in the sense that he's winning, but he's
not doing great in the Republican primary. He's going to have to capture
centrist in the general. And if he's having the defections that he's having
from within his own party, it's kind of unlikely that he captures that centrist
vote, right? It's something to keep in mind. This isn't really a takeaway
away strictly from Michigan because we've seen this before.
But this is a moment where you can see the double standard that gets applied when you're
talking about Biden and Trump.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}