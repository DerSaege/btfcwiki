---
title: Let's talk about Mexico, water, and learning....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=0HHBKV5wIxg) |
| Published | 2024/02/28 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Mexico City faces water supply issues, with people resorting to reusing water due to scarcity.
- Concerns arise about a potential "day zero" water crisis in Mexico City.
- Some question the accuracy of officials' statements and warn of a possible day zero within months.
- The hope that Mexico's water crisis will wake up the American right to climate change dangers is discussed.
- Beau explains that those who deny climate change often believe in American exceptionalism.
- He points out that American right figures will spin the crisis to reinforce biases rather than address climate change.
- Beau stresses the need for climate infrastructure improvement and resilience.
- The American right is unlikely to have a wake-up moment until climate change directly impacts Americans.
- Thought leaders in the right-wing community benefit from maintaining the status quo and fear-mongering.
- Beau underscores that the majority of Americans are aware of climate change but action from the government is lacking.
- He calls for faster action and transitions to combat climate change.
- Beau asserts that those resistant to change and content with fear-mongering won't be swayed by awareness campaigns.
- It's time to shift focus from raising awareness to demanding more concrete action on climate change.

### Quotes

1. "We need to move towards resiliency."
2. "Those people who enjoy listening to those who fear monger, they're not going to be reached in the way that you hope."
3. "It's time to move from raising awareness to trying to get more action."

### Oneliner

Mexico City's water crisis won't awaken the American right to climate change; action, not awareness, is needed.

### Audience

Climate activists, advocates

### On-the-ground actions from transcript

- Advocate for government action on climate change (implied)
- Push for faster transitions to combat climate change (implied)

### Whats missing in summary

The full transcript provides additional context on Mexico's water crisis and the challenges in addressing climate change denial within certain political circles.

### Tags

#ClimateChange #MexicoCity #WaterCrisis #Awareness #GovernmentAction


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we're going to talk about a potential teachable
moment and why it's likely to be missed.
We're going to talk about Mexico.
We're going to talk about water.
And we're going to talk about information ecosystems.
OK, so a question has come in a few times.
And it's whether or not something
that's occurring in Mexico will end up being that moment where portions of the American
right wake up to the dangers of climate change.
And it's a nice thought.
Let's go over what's occurring first.
If you don't know, Mexico City's in trouble when it comes to their water supply.
having real issues, getting enough water out to everybody, it's just the supply
isn't there. You already have instances where people are using water twice,
meaning they're bathing with it and then using the runoff from the bath to flush
their toilets, that kind of thing. And the officials are saying, you know, day zero
isn't on the horizon, don't worry about it, all of that. There are those who are
questioning whether or not the officials are accurate in those statements. Some
suggesting that a day zero could occur within months. Okay, the question is, is
this going to help the American right understand that we have to address
climate change? I applaud the idealism. I really do, but you need to think about
the demographic you're trying to reach. Those people who do not believe that
climate change is a threat or they don't believe that humanity has anything to do
with it, generally speaking they are people who truly buy into American
exceptionalism. They're not going to view it as a wake-up moment. It will reinforce
their own biases because those people in their information ecosystem that are the
thought leaders will intentionally do that. They will spin it so the point
isn't made. It won't be. Look, we need to do something about our climate
infrastructure. We need to move towards resiliency. No, we need to do something
about border security because that city's got 20 million people in it and
they're going to come here. That's how it's going to be played. You cannot count
on the American right to have a wake-up moment until it is impacting Americans
because they don't care. Their biases prevent them from learning. It's
That's what it is.
The people who are major figures within that community, the thought leaders there, they
benefit from the status quo more than most.
They want to maintain the status quo.
This will be used to fear monger, not provide a solution.
Remember they need the fear.
They need the fear.
how they stay in control. A solution in being proactive, it doesn't actually help
them. They need the fear. Now, something that is important to remember is that
most people, the overwhelming majority of Americans, understand that climate change
is an issue. It's not
about awareness anymore. It's not about raising awareness. People are aware.
It's about getting action from government. It's about getting
that transition moving even faster.
There's been big strides, but we need more.
It's not enough. That's where we're at.
Those people who haven't been, who haven't caught on yet,
they're not going to. The drive for awareness, you're beyond that now. Now
it's time to get that transition underway and get it underway at a much
faster rate than it is occurring. You can't wait for people to wake up who
have no intention of it and have no desire to to better the world. Those
people who are happy being afraid, those people who enjoy listening to those who
fear monger, they're not going to be reached in the way that you hope. It's
time to move from raising awareness to trying to get more action. Anyway, it's
It's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}