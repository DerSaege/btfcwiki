---
title: 'Let''s talk about the Michigan Primary: Biden edition....'
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=FMab5Yge014) |
| Published | 2024/02/28 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau provides an overview of the Michigan primary, focusing on the Democratic side.
- Biden emerges as the clear winner with about 80% of the vote.
- A significant portion, about 15%, of voters chose to remain uncommitted to send a message.
- The uncommitted movement aimed to express displeasure with the administration's handling of Gaza.
- Despite initially aiming for 10,000 votes, the movement achieved over 30,000 votes.
- Beau suggests that achieving political capital comes with the responsibility of wielding power effectively.
- While slogans can unite and motivate, achieving the specific goal of a ceasefire may not be realistic for the Biden administration.
- The movement's success lies in making the administration acknowledge their viewpoint and take some action, even if the ultimate goal is not fully realized.
- Beau points out the intersection of ideology and political reality that decision-makers now face.
- The movement's impact goes beyond just achieving their slogan; it has garnered political capital that can be utilized for broader benefits.

### Quotes

1. "They put up more than they needed to get the campaign to pick up the phone."
2. "It achieved political capital."
3. "Even if it doesn't seem like it later."
4. "Ceasefire now, that's not something the Biden administration can provide, not realistically."
5. "If the goals that were outlined by this movement aren't met, that doesn't mean that it failed."

### Oneliner

Beau breaks down the Michigan primary, showcasing the power of grassroots movements in shaping political narratives and wielding political capital effectively.

### Audience

Michigan voters, grassroots activists

### On-the-ground actions from transcript

- Mobilize community efforts to address pressing issues (implied)

### Whats missing in summary

Context on the significance of grassroots movements in influencing political decisions.

### Tags

#Michigan #Grassroots #PoliticalCapital #Movement #Messaging


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Michigan.
We're gonna talk about the Michigan primary,
Biden edition, we'll do Trump later,
because to me, the implications from the Democratic primary
far more interesting, far more interesting.
Okay, so what happened?
Biden won, big surprise.
That was never in doubt.
Got about 80% of the vote.
Given the situation, good numbers, good enough.
Out of the remainder, about 5% was split
between Williamson and Phillips.
It's important to note that not everything
is done being counted, and these numbers
can fluctuate a little bit.
The remaining 15%, uncommitted.
Uncommitted.
The uncommitted voting block, the overwhelming
majority of those voters voted that way to message to the administration, to
message to Biden and express their displeasure with how the administration
is handling Gaza. That's what it's about, 15%. The people behind this movement, they
set a goal of 10,000 votes and I did a video and I was like 10,000 votes isn't
gonna cut it. They're not gonna pick up the phone for 10,000 votes. That is not
going to matter. You need twice that. So, they blew past 10,000 votes, then they blew
past 20,000 votes. They're north of 30,000 votes, and they're not done counting. They won.
The uncommitted movement was successful. Biden, the Biden campaign, the Biden administration, will pick
up the phone. And I feel like there may be some people who are kind of key
figures in that movement, they're going to wish they didn't win. Because now
they're going through the looking glass. They have achieved political capital.
They have achieved power. Now how are you going to wield it? That's the question.
numbers that were put up, that will get the administration and it will get
the campaign to pick up the phone. But that doesn't mean that they're going to
get what they asked for. Slogans, they get used to motivate people for a
movement like this. They are useful because they motivate people and they
set a goal, they set an expectation, and they unify people around something.
The reality is that the unifying goal, ceasefire now, that's not something the Biden administration
can provide, not realistically.
they still have that political capital that can be used in other ways to
mitigate, to help a whole lot of people and there will be people that will have
to make those decisions. I do not envy them because they are about to be at
that intersection of ideology and political reality and it is a dangerous
intersection. There's a reason that saying the most radical revolutionary
becomes a conservative the day after the rebellion. There's a reason that saying
exists. And the movement behind this, they won. They put up the numbers. They put up
more than they needed to get the campaign to pick up the phone. But now
they have to deal with the realities of being in the big game. So the reason I'm
saying this, and the reason I think it's important to note this ahead of time, is
is that if the goals that were outlined by this movement
aren't met, that doesn't mean that it failed.
It achieved political capital.
It definitely got them to pick up the phone.
That political capital is probably
going to be used to help a whole lot of people.
But the odds of it getting the slogan that motivated everybody, it's pretty slim.
Pretty slim.
And I would not want to be, I wouldn't want to be one of the people that's making the
call because they are in a very, very tough position.
It's important to remember that this was about messaging.
It was about getting them to acknowledge and hearing out that viewpoint and getting some
kind of action.
All that's going to be achieved.
The ultimate goal?
Probably not.
So if you were somebody who put a lot of time and energy into this, you won.
Even if it doesn't seem like it later.
Anyway, it's just a thought y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}