---
title: Let's talk about Trump, appeals, and immunity....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=QKBHWAF9L-8) |
| Published | 2024/02/06 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains how Trump's dreams of presidential immunity were crushed by the appeals court decision.
- Trump argued for complete immunity to avoid indictment after leaving office, a claim not supported by history as past presidents have understood themselves to be subject to impeachment and criminal liability.
- The appeals court emphasized that there is no all-encompassing presidential immunity protecting Trump from criminal prosecution.
- Speculates on the next steps, suggesting Trump might take the case to the Supreme Court.
- There is uncertainty whether the Supreme Court will take up the case, with opinions divided on the possibility.
- Some believe the Supreme Court could clarify where immunity ends and liability begins for civil actions.
- Concludes by criticizing the notion of total presidential immunity, implying it is a desire for dictatorial power and undermining the Constitution.

### Quotes

1. "There is no all-encompassing presidential immunity that protects him from criminal prosecution. He made that up. He lied, again, it's not a thing."
2. "Your argument is bad and you should feel bad."
3. "They hate the Constitution and they hate the United States."
4. "President Biden's next step would obviously be to lock all of them up. Because there's nothing anybody can do about it, right?"
5. "If this was to be the case and presidents are totally immune from everything, President Biden's next step..."

### Oneliner

Trump's dreams of presidential immunity were crushed by the appeals court, denying his claim of complete immunity and raising concerns about dictatorial desires.

### Audience

Legal scholars, political analysts, activists

### On-the-ground actions from transcript

- Stay informed about legal proceedings and decisions related to presidential immunity (suggested)
- Advocate for accountability and transparency in governance (implied)
- Support efforts to uphold the rule of law and prevent abuses of power (implied)

### Whats missing in summary

The full transcript provides detailed insights into the legal arguments surrounding presidential immunity and the potential implications for accountability and governance.

### Tags

#PresidentialImmunity #LegalSystem #Trump #SupremeCourt #Accountability


## Transcript
Well, howdy there, internet people, let's bow again.
So today we are going to talk about Trump
and how his dreams of presidential immunity
were crushed by the appeals court.
The decision came back today,
so what we're gonna do is we're gonna go over it quickly,
hit the high notes in what was decided,
and then we'll go over where it's likely to go from here.
Okay, so let's start with this.
It would be a striking paradox if the president, who alone is vested with the constitutional
duty to take care that the laws be faithfully executed, were the sole officer capable of
defying those laws with impunity.
Already from there, it's just not going well.
One of Trump's arguments was that he had to be totally immune from anything he did,
otherwise all presidents would be indicted as soon as they got out of office.
Keep in mind, there's been no immunity like this and all presidents have in fact not been
indicted when they got out of office.
The appeals court decided moreover, past presidents have understood themselves to be subject to
impeachment and criminal liability, at least under certain circumstances.
So the possibility of chilling executive action is already in effect.
And then it goes on, the risks of chilling presidential action or permitting meritless
harassing prosecutions are unlikely, unsupported by history, and too remote and shadowy to
shape the course of justice.
So short version, there is no all-encompassing presidential immunity that protects him from
criminal prosecution.
He made that up.
He lied, again, it's not a thing.
Okay, so what happens from here?
Odds are Trump is going to try to take it to the Supreme Court.
I have talked to a number of people who really understand this stuff.
of them, meaning a majority, not a huge majority, but a majority believe that the Supreme Court
isn't even going to take it up.
They're going to let the appeals court stand and just say, no, this isn't worth our time.
Your argument is bad and you should feel bad.
The rest believe that they're absolutely going to take it up, but they're going to take it
up with the express purpose of denying his argument but clarifying everything and putting
it out there where immunity ends and liability begins as far as the immunity that does exist
for civil actions and stuff like that.
That's what they believe will be decided if the Supreme Court decides to hear it.
I have not talked to any person who actually understands this stuff that believes it's
going to go to the Supreme Court and the Supreme Court is going to decide that presidents are
totally immune and that Trump's argument is valid, not one.
It just seems like something that Trump world has latched onto because they want a dictator.
They hate the Constitution and they hate the United States.
So they want somebody to be able to undermine all of that and use an unlimited amount of
executive power.
I would like to remind everybody that if this was to be the case and presidents are totally
immune from everything, President Biden's next step would obviously be to lock all of
them up.
Because there's nothing anybody can do about it, right?
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}