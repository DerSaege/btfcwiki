---
title: Let's talk about Cat 6 hurricanes....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=RIHiUdLJBgw) |
| Published | 2024/02/06 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the potential development of category 6 hurricanes to differentiate and showcase the impact of stronger hurricanes in a warming world.
- Expresses concern that creating a category 6 hurricane will result in more people staying during category 5 hurricanes, leading to more deaths.
- Shares a personal experience of someone not evacuating during a category 4 hurricane and suffering devastating consequences.
- Believes that creating an additional level above category 5 will make category 5 hurricanes seem less powerful and undermine the urgency to evacuate.
- Stresses the importance of understanding that category 5 means immediate evacuation unless incredibly well prepared in a secure location.
- Concludes by expressing strong disapproval of the idea of introducing a category 6 hurricane and warns of the potential horrible consequences.

### Quotes

- "If you alter the current system, people will say, oh, it's only a cat 5, the way people do with a cat 4."
- "People should understand that cap five means get out. No matter what, get out."
- "I think it's wrong. I think it's bad and I think it's going to have horrible consequences if they go forward with it."

### Oneliner

Beau expresses strong opposition to the development of category 6 hurricanes, warning that it may lead to more fatalities during category 5 hurricanes by undermining the urgency to evacuate.

### Audience

Climate activists, policymakers, emergency response teams

### On-the-ground actions from transcript

- Prepare your evacuation plan (implied)
- Ensure you have a secure location to evacuate to during hurricanes (implied)

### Whats missing in summary

Importance of maintaining the current hurricane category system to ensure clear understanding and urgency for evacuation.

### Tags

#Hurricanes #Category6 #Evacuation #ClimateChange #DisasterPreparedness


## Transcript
Well, howdy there, internet people.
Let's bowl again.
So today, we are going to talk a little bit about hurricanes.
We're going to talk about cat 6 hurricanes, something
that may be coming soon, category 6.
If you don't know, the current scale goes up to 5.
Anything above 158 miles per hour
is a category 5 hurricane.
Some researchers have indicated a desire
to create a new category, 192 and up, I believe,
that they will classify as cat six, category six.
It seems like the reason for wanting to do this
is to kind of differentiate and showcase
What can happen in a warming world?
Because hurricanes will become stronger,
and the idea of 192 mile per hour winds
isn't out of the realm of possibility.
And I want to kind of show that.
Here's the thing.
As somebody who is very familiar with the psychology of people
who ride out hurricanes, who don't evacuate,
I promise you if you create a category six, more people will stay during a category five
and more people will die.
It's a horrible idea.
I understand the desire, I get it, and I understand the logic, and I understand the value in driving
home the point that hurricanes are going to become much stronger and more dangerous.
If you alter the current system, people will say, oh, it's only a cat 5, the way people
do with a cat 4.
I know somebody prior to Michael, and I was talking to him, and I don't remember what
the exact wins were, but I want to say it was like 147 or something like that.
I was like, yeah, you might want to get out of here, and they're like, well, wake me up
when it's a cat 5.
because that 11 mile an hour difference or whatever it was, that's gonna really matter.
Their home was destroyed, by the way.
I feel as though creating an additional level will, it won't achieve the desired effect.
It will make a category five seem less powerful rather than draw attention to
something that could be even worse. It's, it's, I think it's a really bad
idea. I think that you would be undermining the system as it is. People
should understand that cap five means get out. No matter what, get out. Unless
you are incredibly well prepared in a very secure location, leave. If you alter
that, if you create a category above that, it loses some of the urgency because
it isn't the most powerful it could be. I understand the motivation behind it. I
think it's wrong. I think it's bad and I think it's going to have horrible
consequences if they go forward with it. Anyway, it's just a thought. Y'all have a
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}