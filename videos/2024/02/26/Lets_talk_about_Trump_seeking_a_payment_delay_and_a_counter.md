---
title: Let's talk about Trump seeking a payment delay and a counter....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=mEQyJtYTynU) |
| Published | 2024/02/26 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump and his team sought to delay the process of making payments to E. Jean Carroll after a jury verdict.
- The judge refused to grant any delay without giving Carroll a chance to be heard.
- Trump's argument for the delay is that the payment is too much.
- Carroll's team has until Thursday at 5 p.m. to respond.
- The judge seems unwilling to grant any delay without Trump posting the required money.
- The case involves a payment of approximately 83.3 million dollars.
- The judge's response was not surprising, as it was expected that delay tactics wouldn't work.
- The judge's order or response came on a Sunday, giving Carroll's team until Thursday to respond.
- There is a website tracking the interest on the judgment against Trump, which is around $464 million.
- The website is TrumpDebtCounter.com, showing the interest accruing on the judgment.

### Quotes

- "Trump and his team sought to delay the process of making payments."
- "The judge refused to grant any delay without giving Carroll a chance to be heard."

### Oneliner

Trump seeks to delay payments to Carroll after a jury verdict, but the judge refuses without Carroll's input, setting a deadline for response.

### Audience

Legal observers

### On-the-ground actions from transcript

- Follow updates on the legal proceedings regarding Trump's payments to Carroll (implied)

### Whats missing in summary

Details on the specific arguments made by Carroll's team in response to Trump's request for a delay.

### Tags

#Trump #LegalCase #DelayTactics #EJeanCarroll


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Trump's request
to delay him paying something.
In a completely predictable move,
Trump and his team sought to kind of drag out the process
in which he would be required to make payments
to E. Jean Carroll.
The judge had this to say, 25 days after the jury verdict in this case and only shortly
before the expiration of Rule 62's automatic stay of enforcement of the judgment, Mr. Trump
has moved for an administrative stay of enforcement pending the filing and disposition of any
post-trial motions he may file.
He seeks that relief without posting any security money.
The court declines to grant any stay, much less an unsecured stay, without first having
afforded plaintiff a meaningful opportunity to be heard."
Now Trump's argument here is basically it's too much.
That's the argument.
I mean, obviously I'm summarizing it, but that seems to be the basis of it.
Now, Carroll's team has until 5 p.m. Thursday to respond.
The judge does not seem inclined to grant any kind of delay whatsoever, much less one
that doesn't involve Trump putting up the money.
Keep in mind, this case is about, what, 83.3 million, I think is this one.
This move was widely anticipated and the response was kind of predicted by most people.
The judge in this case seems to be done with any delay tactics and it just needs to move
on from here, seems to be the position of the court.
So none of this is really surprising.
The only real surprise is that it played out over the weekend.
The order from the judge or the response from the judge came in on Sunday.
So they have till Thursday to make their response.
My guess is they're going to put in a response and the judge is going to agree with them.
There's a possibility of a different outcome, but that certainly seems most likely given
the phrasing of this.
It's also worth noting that as far as the other large sum, somebody has launched a website
called TrumpDebtCounter.com and it tracks the interest.
You can see the interest accrue on what is now listed as a $464 million judgment on that
counter.
It was pretty close to moving into 465 last time I looked at it, but we'll wait until
Thursday to hear more developments about this.
Carol's team may get their response in a little bit earlier, but we'll follow up when there's
more news.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}