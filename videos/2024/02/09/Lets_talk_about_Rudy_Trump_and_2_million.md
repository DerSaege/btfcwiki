---
title: Let's talk about Rudy, Trump, and $2 million....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=9b9Dh2bm6A8) |
| Published | 2024/02/09 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Disclosing information about Rudy Giuliani and Trump's financial situation.
- Giuliani considered bankruptcy after learning about a lawsuit in Georgia.
- Giuliani believed he was supposed to be paid by Trump's campaign for legal work.
- Trump allegedly did not pay Giuliani's legal fees, only expenses.
- Giuliani claims Trump owes him around $2 million for legal work.
- Giuliani took a major financial hit after losing his license post-January 6.
- Giuliani's financial struggles are evident during bankruptcy proceedings.
- Uncertainty remains about the implications of Giuliani's financial issues for him and Trump.
- Giuliani confirms the widely believed notion that Trump stiffed him.
- Giuliani admits to his financial troubles and struggles to pay attention due to them.

### Quotes

1. "Giuliani can barely pay attention."
2. "Trump did in fact stiff Giuliani."
3. "Giuliani confirms the widely believed notion."
4. "He lost his license after Jan 6."
5. "Giuliani believed he was owed around $2 million."

### Oneliner

Beau discloses Giuliani's financial struggles, confirming Trump stiffed him, leaving Giuliani in dire financial straits post-bankruptcy proceedings.

### Audience

Political commentators, Trump critics.

### On-the-ground actions from transcript

- Support organizations advocating for fair compensation for legal work (implied).
- Stay informed about financial transparency in political campaigns (implied).
  
### Whats missing in summary

Details on the potential consequences for Giuliani and Trump from these financial revelations.

### Tags

#RudyGiuliani #Trump #FinancialStruggles #LegalFees #Bankruptcy


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk a little bit
about Rudy Giuliani and Trump and a couple million dollars
and fees and financial situations and all of that stuff
because something was disclosed
and I think it's something that...
Well, I think it's something that most of us
probably suspected, believed, maybe even joked about,
But I don't know that we ever had it confirmed yet.
But now that has occurred.
OK, so as most of you probably already know,
shortly after Giuliani received the word of what was going on
in Georgia when it comes to the $148 million suit,
Giuliani started looking at bankruptcy.
Now, the filing, I believe it's Chapter 11 bankruptcy protection is what's going on.
He met with creditors and people from an office overseeing now, all of that and that kind
of stuff, and they asked him about his financial state.
He said, talking about working for the former president on all of those claims about the
election and all of that stuff, he said, once I took over, it was my understanding
that I would be paid by the campaign for my legal work and my expenses to be paid.
When we submitted the invoice for payment, they just paid the expenses, not
all, but most, they never paid the legal fees.
So it appears that Trump did in fact stiff Giuliani.
I know that's something that most people believed happened, but I don't know that we ever had
anybody admit to it.
Giuliani says that he didn't go through and calculate it all out, tabulate it, and how
much exactly was owed, but he believed it was around $2 million, $2 million that he
is saying that is owed to him for those legal endeavors. Now on top of this there was also
a discussion about his current financial situation and it talked about how he had taken a pretty
big financial hit. I believe it was described as a major financial hit because he lost his
license after Jan 6. So Giuliani is not in a good financial situation and seems
to be doing a pretty good job of demonstrating that during the
bankruptcy proceedings here. What this means for all of the entanglements for
Giuliani and for Trump, that kind of remains to be seen, but it's worth
acknowledging that the one thing that everybody kind of expected was apparently
at this point Giuliani is saying that it's true and that because of that
Giuliani can barely pay attention. Anyway, it's just thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}