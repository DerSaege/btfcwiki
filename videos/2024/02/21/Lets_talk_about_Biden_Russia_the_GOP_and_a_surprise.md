---
title: Let's talk about Biden, Russia, the GOP, and a surprise....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=EJ5e-E92qUk) |
| Published | 2024/02/21 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau dives into the Republican party's impeachment inquiry involving allegations from a confidential source named Smirnoff.
- Smirnoff was arrested for lying and is actively spreading new lies post-meeting with Russian intelligence officials in November.
- The request to keep Smirnoff detained includes allegations of wiring a hotel to gather compromising material on US officials.
- Questions arise on the credibility of information circulating and the potential ties between the Republican Party's source and Russian intelligence.
- Beau suggests that if Smirnoff is a Russian asset, there may be American hostages taken for future exchanges.
- The judge did not grant detention ahead of trial for Smirnoff but imposed additional release requirements like GPS monitoring.
- Beau notes the ongoing attempt by Russian intelligence to influence the 2024 election.

### Quotes

1. "The Republican Party has a choice. They can either accept this as the new development or they can say, well, he was telling the truth back then but he's lying now."
2. "If this person is a Russian asset of some kind, what can you expect? Russia's gonna snatch up some Americans so later they can make a trade."
3. "It's worth noting that according to his statements, there is once again an active attempt by Russian intelligence to influence the 2024 election."

### Oneliner

Beau delves into the Republican impeachment inquiry, exposing ties to Russian intelligence and potential impacts on US elections.

### Audience

Political analysts, concerned citizens

### On-the-ground actions from transcript

- Monitor news updates on the political developments discussed (implied)
- Stay informed about potential foreign interference in elections (implied)

### Whats missing in summary

In-depth analysis and additional context can be gained from watching the full video.

### Tags

#Politics #ImpeachmentInquiry #RussianIntelligence #ElectionInterference #USOfficials


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk a little bit about Biden,
both Bidens, Russia, some of the things
that were said about them
and where that information came from.
And we're going to talk about a surprising turn
that isn't really that surprising,
but I feel like some people are gonna be very surprised,
or at least they'll pretend to be.
And we're just gonna kind of run through
some information because what a twist.
Okay, so we've talked about it before
on the channel repeatedly.
The Republican party is pushing an impeachment inquiry.
A lot of the allegations that are at the heart
of this inquiry came through a confidential human source
run by the FBI, a person named Smirnoff.
Now, this person was arrested for lying, okay,
this stuff. This is where it gets interesting. The prosecution wanted to
keep him detained, didn't want to let him out prior to trial. Why? He is actively
peddling new lies that can impact the US elections after meeting with Russian
intelligence officials in November. That's key to it right there. He
apparently has, quote, extensive and extremely recent contact with Russian
intelligence officials and has admitted this. There is a lot that is in the
request to keep him detained. Everything from an allegation about a hotel being
wired to get compromising material on US political officials or business people.
Just all kinds of stuff that is an admission of sorts. So this leaves a lot
of questions about where some of this stuff came from and how it circulated
through. The indications are that the person the Republican Party was
relying on to provide them information, this person might have been in the
employee of Russian intelligence. Surprise! Okay, so the Republican Party has a
choice. They can either accept this as the new development or they can say, well
he was telling the truth back then but he's lying now. It's hard to say
which way they would go. The smart money would of course just walk away from the
whole thing, but the Republican Party definitely has a history lately of
doubling down when they shouldn't. So we don't really know. Now if you have
questions, there's an easy way to tell. Generally speaking, because this is old
school Cold War stuff right here. If this person is a Russian asset of some kind,
what can you expect? Russia's gonna snatch up some Americans so later they
can make a trade. That's how it's done traditionally. So if that occurs, I mean
And that's a pretty good indication that he is, in fact, a Russian asset of some kind.
That's what I would be on the lookout for.
Now, realistically, this could have already occurred, and we don't have the news of it
yet.
But normally, when one counterintelligence service of one country picks up an asset of
another intelligence service, the counterintelligence people in that country, well, they snatch
an asset of the other country.
The whole gentleman's agreement thing.
We'll wait and see if that happens.
It's interesting that all of this led back to Russian intelligence, though.
I mean, I don't know that a whole lot of people
were suspecting that, right?
I mean, that's a shock to everybody.
Nobody saw that coming at all.
So we'll wait and see how it plays out.
It is worth noting that the judge apparently
did not grant the request for detention ahead of trial,
but placed just a whole bunch of extra requirements
for being released ahead of trial, GPS monitoring,
stuff like that.
We'll see how that plays out as well.
So there's definitely a new turn in this story, a new twist.
I don't know that it's super surprising,
but it should lead to some interesting developments.
It's also worth noting that according to this
and according to his statements,
there is once again an active attempt
by Russian intelligence to influence the 2024 election.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}