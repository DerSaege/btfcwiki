---
title: Let's talk about Oklahoma vaguely....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=DevaZeafwmQ) |
| Published | 2024/02/21 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses questions on why major outlets are not covering a certain incident in Oklahoma involving a student's death.
- Major outlets refrain from covering the incident due to lack of official documents or statements connecting events.
- Alleged individuals involved in the incident are minors, making it a sensitive topic.
- Law enforcement is conducting interviews, but the necessary information is not publicly available.
- Journalists are cautious due to legal questions and the sensitive nature of the incident.
- Major outlets are expected to cover the incident once official statements are available.
- Journalists are working on various aspects, including the appropriate names to use in their coverage.
- The delay in coverage is not due to ignorance but because critical information is missing.
- Beau advises waiting for larger outlets to cover the incident once the necessary criteria are met.
- The situation is complex, involving legal considerations and potential consequences for inaccurate reporting.

### Quotes

- "Major outlets, when you're talking about something like this, you have to look at what is rumored in all of the blogs."
- "This isn't a case of it not being covered because it's being ignored."
- "Nobody wants to be wrong."

### Oneliner

Why major outlets are not covering a sensitive incident in Oklahoma involving a student's death and the legal and ethical considerations behind delayed coverage.

### Audience

Journalists, News Outlets

### On-the-ground actions from transcript

- Wait for larger outlets to cover the incident once official statements are available (suggested).

### Whats missing in summary

The nuances of legal and ethical considerations in journalistic coverage and the importance of waiting for official statements before reporting.

### Tags

#Oklahoma #Journalism #SensitiveTopic #MediaCoverage #LegalQuestions


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about Oklahoma vaguely.
And we're going to do this early on because I got, I
don't know, maybe 50 questions about this.
And at the heart, the questions are all the same.
I'm hoping that by the time this video actually
publishes, it's obsolete, but it may not be.
At the heart of the questions, it's all the same.
Why aren't major outlets covering this?
If you want to know what's being discussed, you want a
little bit more context, because this video is just
going to be almost void of it.
Go to Google and type in, Oklahoma student dies.
And then click News.
You're going to see a bunch of blogs.
You'll see small outlets.
You will see outlets based outside of the United States.
Outlets in the US, major outlets in the US,
they're not covering it yet.
Why?
I talked to somebody at a larger outlet.
And they're like, I actually started
to write an article about this and then stopped.
And I'm like, why?
They said two words, passive voice.
If you are concerned about this getting coverage,
you don't want to write it yet.
Major outlets, when you're talking about something
like this, you have to look at what
is rumored in all of the blogs.
Almost everybody involved or alleged to be involved
is a minor.
there's a lot of sensitive topics at play. It is worth noting that for major
outlets to connect to events they have to have an official document or
statement connecting those two events. If somebody was to fall down at a roller
skating rink, and then a day later die, no outlet is going to connect those two
events without an autopsy report. That report is not publicly available.
There's a lot of stuff like that at play. That's why it's not being covered by
major outlets yet. I would imagine, regardless of what
information comes out, eventually, this is going to get
coverage. I can also say from making a couple of phone calls,
that law enforcement in that area is conducting interviews about this, but
that's it. So this isn't a case of it not being covered because it's being
ignored. This is a case of the information that is needed to write the
article isn't publicly available, and there's a lot of legal questions with this one.
So basically the major outlets are going to wait until there is official statements that
can be used.
And there's a whole lot of other stuff that needs to be worked out by the journalist covering
it as well.
everything from the appropriate name to use. There's just a lot in the mix on
this one and because of the sensitive nature of it, nobody wants to be wrong.
That's what's going on. Again, this shouldn't take long for these issues to
resolve themselves, but due to the sheer volume of questions coming in, I
I figured I should get this out.
So I would just hang out and wait for the larger outlets to
check the boxes they need to cover a topic like this.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}