---
title: Let's talk about Biden, messages, and Michigan....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=rdW_T0Sja0o) |
| Published | 2024/02/27 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Michigan is organizing an effort to send a message to the Biden administration regarding Gaza policies during the primary.
- The goal is to encourage people to vote uncommitted rather than for Biden to influence change.
- The governor of Michigan is supporting Biden but expects a significant number of uncommitted votes.
- An organization aims for 10,000 uncommitted votes, but Beau believes this won't be enough to impact political strategists.
- In 2020, there were 19,000 uncommitted votes, showing that 10,000 votes may not be sufficient.
- Beau suggests that the public goal of 10,000 votes may be lower than the actual private goal.
- Beau thinks protest votes during the primary are vital and preferable to other forms of protest.
- The numbers during the primary will determine the impact; Beau believes 20,000 votes could make a difference.
- Beau encourages an open, public protest vote during the primary to ensure being heard.
- He ends by urging action during the primary to send a powerful message.

### Quotes

1. "Michigan is organizing an effort to send a message to the Biden administration regarding Gaza policies during the primary."
2. "This is the time to do it, this is the way to do it, during the primary and doing it in a very open public fashion through a protest vote."
3. "If you get the numbers, you'll be heard."

### Oneliner

Michigan's effort to influence Biden's Gaza policies through protest votes during the primary may need more than 10,000 votes to make a significant impact.

### Audience

Michigan residents

### On-the-ground actions from transcript

- Organize and encourage people to participate in protest votes during the primary (suggested)
- Ensure a high turnout of uncommitted votes to send a powerful message (implied)

### Whats missing in summary

The full transcript provides additional context and nuances around the importance of protest votes during the primary and the potential impact of different vote counts.

### Tags

#Michigan #ProtestVotes #BidenAdministration #GazaPolicies #PrimaryElection


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Biden in Michigan
and the governor and numbers and goals
and the upcoming primary
and how all of this is going to tie together
and whether or not a message that is being sent
is gonna be received.
Okay, so in Michigan,
There is an organized effort underway to send a message to the Biden administration
when it comes to the administration's policies concerning Gaza.
The effort centers on the idea of getting a whole bunch of people during the primary
to vote uncommitted rather than vote for Biden.
And they're hoping that that will send a message to the administration
maybe affect some change. The governor of Michigan is currently out there trying
to shore up support for Biden and is also acknowledging that she expects a
sizable, quote, amount of votes for uncommitted. One of the organizations
that is kind of behind this effort, they have set the goal of getting 10,000
uncommitted votes. That is not enough. That won't send a message. It's one of those things
where the number that is being proposed, it's not going to be enough to influence the political
strategists with the Biden campaign. In 2020, there were 19,000 uncommitted votes.
10,000 isn't going to cut it. There are certain situations where it might start
to raise eyebrows, such as if there's really low turnout and 10,000 uncommitted
votes. That might do it, but if everything is normal and there's 10,000
uncommitted votes, it's not going to send the message that they're hoping to send.
They're gonna have to break 20,000. It's also important to note that 10,000 is
the public goal. The private goal could be much higher and that's something that
we have talked about on the channel before. When you're setting
something up like this, you want to make sure that your goal is attained. So they
may have set one that they know they're going to get and they're shooting for
something much higher. But based on the publicly available information, 10,000
isn't going to cut it. That will not send the message. It will not affect change. I
I know that there are people who have issue with this.
This is the primary.
This is when you're supposed to message.
This is when it's supposed to happen.
And given some of the other protests that have occurred recently, I feel like limiting
effectiveness of a protest vote is a bad idea. I think we would all prefer to see protest votes
during a primary than some of the other things that we've seen recently.
So, when the primary rolls around, this is definitely going to be something that matters
in Michigan. The numbers are going to tell the story. 10,000, I don't think it's
going to be enough to make an impact. 20,000, there you go. But I would also
temper expectations about how much change is going to be affected. So it's
It's one of those things where this is the time to do it, this is the way to do it, during
the primary and doing it in a very open public fashion through a protest vote.
If you get the numbers, you'll be heard.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}