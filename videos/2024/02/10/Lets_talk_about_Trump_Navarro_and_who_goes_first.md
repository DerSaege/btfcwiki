---
title: Let's talk about Trump, Navarro, and who goes first....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=s3wxb3K_5WI) |
| Published | 2024/02/10 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Peter Navarro might be the first to go behind bars for his entanglements with Trump's crew.
- Navarro, a Trump advisor, faced a subpoena during the January 6th hearing but was uncooperative with the committee, not providing documents or testimony.
- Navarro was found guilty of contempt, sentenced to four months in confinement, and has been appealing this decision.
- Despite his appeal, the judge ordered Navarro to report to the designated facility as per the Bureau of Prisons' instructions.
- Navarro claims the situation is a political witch hunt, but the judge dismissed this argument, stating there was no proof.
- It appears unlikely that further appeals will keep Navarro out of confinement, and he may have exhausted his options.
- The most probable outcome now is that Navarro will serve his four-month sentence, though there is a slim chance of interruption.
- The exact reporting date to the facility is not readily available.

### Quotes

1. "Navarro might be the first to go behind bars for his entanglements with Trump's crew."
2. "There is still a slim possibility that something could interrupt that."

### Oneliner

Peter Navarro may be the first to face confinement for his involvement with Trump's crew, despite his appeals, as the judge orders him to report to the facility, likely leading to him serving his four-month sentence.

### Audience

Legal analysts, political enthusiasts

### On-the-ground actions from transcript

- Stay informed about the legal proceedings involving public figures. (implied)

### Whats missing in summary

Insight into the potential impact of Navarro's confinement on future political and legal developments.

### Tags

#PeterNavarro #Trump #LegalProceedings #Confinement #PoliticalWitchHunt


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about Trump's crew
and who goes first, because it looks like we may finally
have that answer.
Throughout all of the entanglements,
one of the big questions that has popped up
is who is actually going to end up going behind bars first.
And it appears that Peter Navarro might be that person.
So to refresh everybody's memory,
Navarro, Trump advisor, January 6th hearing going on, subpoena.
He's like, yeah, whatever, not exactly cooperative
when it came to the committee, the hearings,
and all of that stuff, didn't provide documents and testimony.
Contempt, then it went to court, found guilty.
Sentenced to four months.
Has to go do four months in confinement.
Been appealing it.
And was basically asking to stay out of confinement
while the appeal process played out.
The judge said no.
The judge has said that Navarro needs to report to the designated facility at the time the
Bureau of Prisons says he needs to report there.
When that is, I actually can't find out.
That information wasn't easy to come by.
Now Navarro is saying, of course, that this whole thing is a giant political witch hunt,
political bias, so on and so forth.
The judge didn't really go for that
and said that there was, quote, no actual proof
that it occurred, and went on to say
that the, quote, defendant's cynical,
self-serving claim of political bias
poses no question at all, let alone a substantial one.
So that argument was not going anywhere.
Okay, so what happens here?
Navarro can go do his four months,
or there's still one more possible appeal.
It seems unlikely that that appeal would be successful
when it comes to keeping him out
while further appeals play out.
It does look like Navarro has exhausted the options.
So, although we don't know when, currently it seems like the most likely outcome from this point
is that Navarro goes and does his four months.
There is still a slim possibility that something could interrupt that,
but based on everything we've seen so far it looks like he's first. Anyway, it's
It's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}