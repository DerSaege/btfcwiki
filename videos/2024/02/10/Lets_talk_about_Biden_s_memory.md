---
title: Let's talk about Biden's memory....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=bEdQWITGIIE) |
| Published | 2024/02/10 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses the Biden and memory report, expressing his usual avoidance of certain topics that generate engagement but are essentially noise.
- He points out the lack of evidence in the report about Biden's willful retention of documents, urging people to read the report for themselves.
- Beau questions the assumption of guilt in the report, pointing out the shortage of evidence and innocent explanations for the documents found.
- He challenges the idea that Biden's memory issues invalidate the case, contrasting it with the evidence-based case against Trump.
- Beau criticizes the media's focus on sensationalized aspects rather than the actual content of the report.
- He suggests that accusations distract from the real issues and express his skepticism about politicians' honesty.
- Beau concludes by encouraging people to focus on the actual content of the report rather than the media's spin, pointing out the lack of evidence to support the accusations.

### Quotes

1. "If your entire case rests on the memory of the accused, you don't have a case."
2. "This type of stuff, the accusations, they draw away from the actual points."
3. "I do not believe that politicians are all honest."
4. "It's almost always there to pull away from the actual point."
5. "If you actually read the report, instead of allowing your chosen news outlet to force-feed you the quotes, you're going to come away with the conclusion."

### Oneliner

Beau challenges assumptions about Biden's memory and criminal intent, urging people to focus on the lack of evidence in the report rather than media sensationalism.

### Audience

Voters, Critical Thinkers

### On-the-ground actions from transcript

- Read the full report on Biden and memory to form your own opinion (suggested)
- Focus on evidence rather than media headlines (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the Biden memory report, focusing on evidence and challenging assumptions rather than sensationalism.

### Tags

#Biden #MemoryReport #Evidence #MediaSensationalism #Politics


## Transcript
Well, howdy there, there are no people that's bowing down.
So today we're gonna talk about Biden and memory
and that report and all of that stuff.
Now, I wanna point out, this is generally the type of thing
that I don't talk about.
There are a whole bunch of topics
that I just kind of let pass by.
They're easy topics to cover, and they get a bunch of engagement on social media.
But most times they're just noise.
You won't find me talking about Trump's sons or any of the issues that they might have.
You won't hear me talk about the whole moms for liberty thing.
You won't hear me talk about the situation with the DA in Fulton County, even though
I know that there are attorneys on the defense side that are engaged in relationships with
other attorneys on the case, because it's not news.
That might become news, but generally speaking, all of this is just noise.
It's good for views, it's good to give people their two minutes of hate, but it's just noise.
Every once in a while on a topic I'll have enough questions come in to kind of feel like
I have to talk about it.
This is one of those.
Because I've got a whole bunch of people saying, well, how do you feel about the fact that
Biden didn't get arrested because he's some doddering old man and that he did willfully
retain and that they only did it because he's got a bad memory.
How do I feel about that?
I feel like you should read the report instead of letting people who go for those two minutes
of hate tell you what's in it.
Because if you read the report, you might find out that there's a shortage of evidence.
That's a quote.
If you read the report, you might find out that while it is natural to assume that Mr.
Biden put the Afghanistan documents in the box on purpose and that he knew they were
there, first, is it natural to assume that?
I kind of thought we were supposed to assume the opposite.
I thought we were supposed to presume innocence, but I mean, whatever, let's just assume that.
Let's assume that it is natural to draw the conclusion that Biden committed the crime
because that's what it is.
That would be the willful retention part.
Okay, so we naturally assume that, but it says there is in fact a shortage of evidence
on these points, meaning the points related to the crime.
We do not know why, how, or by whom the documents were placed in the box.
We do not know whether or when Mr. Biden carefully reviewed the box's contents.
We do not know why only some of Mr. Biden's classified Afghanistan memos to President
Obama from the fall of 2009 were found in the box, but several other memos he wrote
during that time period were not.
And then you have this part, different part mind you.
In addition to this shortage of evidence, there are other innocent explanations for
the documents that we cannot refute.
So to be clear, they cannot make their case and they cannot refute the case that Biden
presented.
That doesn't sound like, oh, we were going to indict him, but he's got a bad memory.
That sounds like they didn't have the evidence.
Now I know somebody's going to say, well, they didn't have the evidence because Biden
had a bad memory and didn't tell him anything.
If your entire case rests on the memory of the accused, you don't have a case.
It should be remembered that Trump didn't provide this information and they still built
a case there, right?
Because there was evidence.
Weird how that happens.
You have pretty clear statements in here about what they know and what they don't.
The thing is, it's not scandalous.
aren't the quotes that you're seeing in the headlines. You're seeing, well-meaning, elderly
old man or whatever. And I mean, okay, maybe he is that. I mean, the alternative to them
not having the case and them not having the evidence, the alternative to this is that
Biden is some kind of criminal mastermind and knew exactly what to, I just don't remember,
enough to derail the entire case.
That kind of flies in the face of the whole idea that he's suffering from cognitive decline,
right?
He's actually like some criminal genius where they just don't have the evidence.
It's one of the two.
And I know, because of that, somebody's going to say, but he mistook everything when he
was talking.
Right after defending that he had a good memory, he showed that he didn't and didn't know what
he was talking about.
He didn't even know what country he was talking about.
Okay, let's read that.
As you know, initially, the president of Mexico, Sisi, did not want to open up the gate to
allow humanitarian material to get in.
I talked to him.
I convinced him to open the gate.
Okay.
Yeah, I mean, Mexico is not Egypt.
That's true.
So the obvious assumption here is that Biden is suffering from cognitive decline, not that
But there was a slip of the tongue talking about two countries that he had been dealing
with all week, both dealing with border issues, both dealing with refugees on those borders,
and both dealing with them crossing over, right?
That context wasn't in any of your reporting that you saw, probably.
Sounds like a slip of the tongue to me, it's what it looks like.
I mean, he got the president name right about the country he was talking about.
He said Mexico instead of Egypt.
I mean, okay, I mean, I know I've never mistaken a word before, like honeybee and bumblebee.
Long time viewers, you'll get that anyway.
If this is a signal of cognitive decline, I would have to ask what it is when you mistake
a photo of the woman that you get found liable for sexually assaulting for a photo of your
wife during a deposition.
Not in some offhand comment, but during a deposition.
And that your attorney has to correct you.
What is that?
If you don't know what I'm talking about, maybe type that in, maybe the news that you're
consuming isn't giving you a clear picture of things.
This type of stuff, the accusations, they draw away from the actual points.
Now when I look at this, my assumption is not that Biden is some doddering old man.
My assumption is that Biden actually knows how to cover his tracks, honestly.
Keep in mind, I am not a Democrat.
I am somebody who has publicly said that I don't think Biden's the strongest candidate
and that he's not my man.
I do not believe that politicians are all honest.
I believe most of them are not.
So I don't have the assumption that he's a doddering old man.
I have the assumption that he saw what happened.
Maybe it's an accident.
Maybe he did it on purpose.
But either way, he knew what not to say.
That would be my assumption.
But I can't prove that, right?
So it's not really an accusation you can make because there's a, what's the term, a shortage
of evidence. When you're looking at stuff like this, just remember it's almost
always there to pull away from the actual point. And that goes to all of
this. All of this that we discussed in here. None of this is actually relevant
to the issues at hand for each topic. It's just something extra. Generally
speaking on this channel, I try to clear that stuff away. It's not that I'm afraid
to talk about it. And for those who, you know, begged me to talk about this, just
remember, I mean, you literally asked for this repeatedly. If you actually read the
report, instead of allowing your chosen news outlet to force-feed you the quotes,
you're going to come away with the conclusion that, yes, this special counsel,
who is not a psychologist, believes that Biden is super forgetful, but you're also
going to come away with the conclusion that they didn't have the evidence to
to make the case no matter what, because that's
what's actually in the report.
I would suggest that people stop focusing on the parts of it
that don't matter.
If you believe that he's suffering
from cognitive decline, OK, then you probably
shouldn't vote for him.
But that doesn't, the statements in the report
don't actually warrant the response that has come out.
This response is really a very well-engineered response
to cover up for the fact that there
is no evidence of the crime.
There is a shortage of evidence to use
term in the report and that looks really bad for Trump. I'll tell you this, the
same thing is going to happen with Pence. The false comparison that was made by
the right-wing media between Trump, Pence, and Biden, these reports undermine that.
It undermines their viewers' belief in what they're told. So they have to change
the subject because they can't dwell on the fact that that comparison that they
made was garbage and it was garbage from the beginning from anybody who actually
understood this stuff. Go back and watch the videos about Trump and about why we
knew right from the beginning that this was going to be a big deal and then
compare them to the report. It's not the same. Anyway, it's just a thought. Y'all
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}