---
title: Let's talk about Trump, Montana, and the GOP....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ryglZPZ4Mkk) |
| Published | 2024/02/10 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republicans are angling to secure Tester's Senate seat in Montana.
- Rumors suggest Rosendell, a Trump supporter, wants to run against Tester.
- There was speculation about a deal for Rosendell to get an endorsement in exchange for a vote.
- The Senate already had a candidate named Sheehee to run against Tester.
- Rosendell officially announces his candidacy, setting up a primary that could drain Republican resources.
- Trump endorses Sheehee, the Senate's candidate, causing tension and hurt feelings.
- Despite endorsing Sheehee, Trump acknowledges Rosendell and leaves room for future support.
- The Republican party is divided between establishment candidate Sheehee and upstart Rosendell.
- The ongoing drama could impact the Republican Party's chances in the election.
- Democrats should pay attention to the situation and support Tester.

### Quotes

1. "Republicans are angling to secure Tester's Senate seat in Montana."
2. "Trump endorses Sheehee, causing tension and hurt feelings."
3. "The Republican party is divided between establishment candidate Sheehee and upstart Rosendell."

### Oneliner

Republicans in Montana are divided as rumors swirl, primary elections loom, and Trump's endorsement causes tension in the quest for Tester's Senate seat.

### Audience

Political observers, activists

### On-the-ground actions from transcript

- Support Tester's campaign (implied)
- Stay informed on the election developments (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the political dynamics and strategies in the race for Tester's Senate seat in Montana.


## Transcript
Well, howdy there, I don't know people, let's bow again.
So today we are going to talk about Montana
and the Republican quest to get testers seat,
testers Senate seat.
They really want that seat.
And there's been a lot of angling to try to get that seat.
So we're going to talk about how things are shaping up there
and how they might progress.
Now, recently we talked about a rumor
And that rumor said that a person named Rosendell, who is currently in the House, who is like
a Trump supporter all the way, MAGA hardliner, that he wanted that seat, definitely wanted
Tester's seat.
And we talked about that rumor that he was going to run and how that there might have
been like a deal made to where he was going to get an endorsement from Johnson, the current
speaker of the House in exchange for a vote. Again, just a rumor. None of this is confirmed
or anything. But according to the rumor, as soon as news of that broke, the Senate got
really upset because they had already chosen their candidate to run against Tester and
that's a person named Sheehee. Okay, so that was the rumor that was unconfirmed.
Rosendell has announced his candidacy to go after Tester's seat.
Now it is worth remembering the Senate is already behind Chihi and Rosendell is maybe
not the best candidate to go against Tester.
I mean, objectively looking at it, I believe Rosendell actually lost to Tester in 2018
before all of this other stuff.
But Rosendell has officially announced that he's going to run, which does set up a situation
where there's going to be a primary, and that primary is going to drain Republicans of resources
that they could use in the general. It's going to drain both Rosendell and Chihi
of resources. At this point, I mean the obvious thing for them to do is to start
getting those endorsements, maybe not one from the Speaker of the House. Again,
Rosendell is a massive supporter of former President Trump, big-time
supporter. It is worth noting that hours after Rosendell announced that he was
gonna run, Trump issued his endorsement of she. Rosendell just can't catch a
break here. Yes, Trump has decided to endorse the establishment candidate, the
the one chosen by the Senate and it is it's definitely something that is
shaping up to be a fight and there are going to be hurt feelings over this so
much so that even Trump who is not known for being sensitive to people's feelings
when it comes to stuff like this, said, I also respect Matt Rosendell and was very happy
to endorse him in the past and will endorse him again in the future should he decide to
change course and run for his congressional seat.
But in this instance, Tim is the candidate who is currently best positioned to defeat
lazy John Tester, I guess that's the very, very original inventive nickname that the
former president came up with.
And Tim is Sheehy.
So you now, once again, you have a situation where the Republican party is divided.
The establishment is falling in behind Sheehy.
upstarts are kind of leaning towards Rosendell. Either way this is bad for the
Republican Party because it's unlikely that they're going to that either one of
them is gonna drop out. Rosendell might now that Trump has said hey you know
you're not gonna get my endorsement kind of thing that might sway him to drop
out but we don't know yet. Either way, the Republican Party and the amount of
drama that has already occurred for Montana, they have set their
sights on this seat. John Tester, Senator Tester, they're going for that
seat with everything that they have. If you're a democratic strategist, you
probably want to pay attention to that. Tester's probably going to need help.
Anyway, it's just a thought y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}