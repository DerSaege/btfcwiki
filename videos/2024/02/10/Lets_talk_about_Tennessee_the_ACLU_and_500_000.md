---
title: Let's talk about Tennessee, the ACLU, and $500,000....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=YgNF79FL8GU) |
| Published | 2024/02/10 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about Murfreesboro, Tennessee, as an example of a common theme.
- Mentions the manipulation by politicians to rally support by targeting and demonizing certain groups.
- Recalls a past instance where politicians used fear-mongering tactics to go after drag shows.
- Notes the involvement of the Tennessee Equality Project and the imposition of discriminatory rules to restrict free speech.
- Acknowledges the ACLU's intervention and a federal judge blocking the discriminatory rules.
- Points out the typical scenario where politicians refuse to admit wrongdoing and end up costing the community money in legal battles.
- Contrasts Murfreesboro's response by settling for half a million dollars, acknowledging the unconstitutional nature of the laws.
- Emphasizes the importance of voting for individuals who understand basic American principles and civics.
- Encourages voters to support candidates who embody the spirit of the country rather than those who simply claim to love America.
- Concludes by suggesting that ultimately, it's cheaper to be a good person.

### Quotes

1. "They made something up to vilify your neighbors, to scare you, to manipulate you, to play you, and you fell for it."
2. "Maybe next time you vote, maybe vote for people who understand the basic principles of this country a little bit more."
3. "It's cheaper to be a good person."

### Oneliner

Beau talks about the manipulation of politicians in Murfreesboro, Tennessee, urging voters to support those who understand basic civics and American principles.

### Audience

Voters

### On-the-ground actions from transcript

- Vote for candidates who understand basic American principles and civics (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the manipulation of politicians, the importance of understanding civic principles, and the consequences of falling for fear-mongering tactics.

### Tags

#Murfreesboro #Tennessee #Politics #Voting #Civics


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about
Murfreesboro, Tennessee, as an example,
because this is gonna happen in other places,
but it happened there and it's made some headlines.
So we can talk about that,
and we can talk about a long-running theme on this channel,
and something that just continually ends up
being proven true over and over and over again.
But a lot of people can't seem to get the message.
message. So we will talk about Murfreesboro, Tennessee, we will talk about
equality, we will talk about civics, and we will talk about the ACLU. See, it
wasn't that long ago when a group of politicians, Republican politicians,
decided that a good way to rally their base and get people good and energized
is to find a group of people and other them. Fear monger about them. Make stuff
up about them. They do it all the time. They're doing it right now with a
different group. But back then it was going after drag shows. Do y'all remember
that? And you had all of those politicians who didn't understand the
Constitution, basic American principles, or basic civics, promising to stop them
from exercising their constitutionally protected rights. I mean, they didn't say
it that way. They didn't use those words. They said, they're coming for your kids,
or something like that. They weren't. The politicians made that up. They lied to
you. And they got everybody scared. They created that moral panic, energized that
base, manipulated, those that are easily manipulated, and they move things along.
In Murfreesboro, something similar to that might have occurred, and it might have kind
of centered on something called the Tennessee Equality Project.
And new rules might have been put in place that were discriminatory, that aimed to stop
people from expressing themselves, to curtail speech.
The ACLU got involved, eventually a federal judge got involved, blocked the new rules
from being enforced.
And as is typically the case in situations like this, it plays out and at the end of
it somebody's getting paid.
Now normally the politicians and those people behind them, they don't admit that they're
wrong.
They take that to court and they fight it tooth and nail and then the people in that
community are on the hook for a whole lot of money.
Murfreesboro is different.
Murfreesboro is different. They settled. Half a million dollars. All of those ordinances,
all of those laws that were put on the books, that when they happened, we talked about and
we said, these are unconstitutional. These will not stand. They're making their way through
the court system now. And lo and behold, what's occurring is a lot of settlements.
It's something that the voters should remember. They made something up to vilify your neighbors,
to scare you, to manipulate you, to play you, to use you, and you fell for it. And now you've
got to pay for it too. Maybe next time you vote, maybe vote for people
who understand the basic principles of this country a little bit more. Don't
vote for the person who puts a flag behind them and says I love America. Vote
for the person that actually embodies the the spirit, that actually understands
basic civics. The long-running theme is that at the end of the day, it's cheaper
to be a good person.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}