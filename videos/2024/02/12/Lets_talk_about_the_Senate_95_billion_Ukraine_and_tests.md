---
title: Let's talk about the Senate, $95 billion, Ukraine, and tests....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=sphkbvw1XSc) |
| Published | 2024/02/12 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Senate advanced a $95 billion aid package on Super Bowl Sunday, including $60 billion for Ukraine, $14 billion for Israel, $9 billion for Gaza, and $4.2 billion for the Indo-Pacific region.
- Republicans negotiated a border deal, but Trump interfered, preventing them from addressing the border issue to use it for fear-mongering.
- Some Republicans now want to vote against the aid package to retain leverage for the border deal they were ordered not to pursue.
- McConnell's move in the Senate (67 to 27) may be challenging his critics to oust him or calling their bluff.
- Despite likely passage in the Senate, uncertainty looms in the House where Republicans, more influenced by Trump, may vote against the aid package due to fear tactics and lack of policy.
- Republicans in vulnerable positions may push the aid package through if polling shows they understand Trump's manipulation regarding the border security deal.

### Quotes

1. "Republicans turned on their own deal, on their own provisions."
2. "They don't actually know what they're doing, most of them. They are also, many of them, don't have policy. They need fear."
3. "They may vote against this because they know that at some point they may need to do something about the border and they need this foreign aid as leverage."
4. "It's a mess because a whole lot of Republicans are taking orders from somebody who I would like to remind you lost their last election."
5. "It's a mess, isn't it?"

### Oneliner

Senate advances a $95 billion aid package, exposing political turmoil as Republicans navigate Trump's influence and conflicting priorities around foreign aid and border security.

### Audience

Political activists and concerned citizens

### On-the-ground actions from transcript

- Contact your representatives to express your views on foreign aid and border security negotiations (suggested)
- Stay informed and engaged with political developments to hold elected officials accountable (suggested)

### Whats missing in summary

The full transcript provides a detailed analysis of the complex interplay between political decisions, leverage, fear tactics, and Republican dynamics under Trump's influence.

### Tags

#Senate #PoliticalAidPackage #TrumpInfluence #BorderSecurity #RepublicanPolitics


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about the Senate
and McConnell and Ukraine and $95 billion
and a surprise move that is a whole bunch
of political tests coming together at once.
So, if you missed the news,
the Senate advanced an aid package,
A $95 billion aid package on Super Bowl Sunday.
What's in it?
$60 billion for Ukraine, $14 billion for Israel, $9 billion for Gaza, $4.2 billion for the
Indo-Pacific region.
That's what's in it.
And I know people are like, man, that sounds familiar.
It's almost like that's what was in that border deal that they had worked out.
It is.
They just ripped the border portion out of it.
The Republican Party negotiated a border deal.
When they got what they wanted, they were going to advance it.
But then Trump came in and was like, no, no, don't do that.
You can't actually address the border.
I need that to scare people with.
I can't run on policy.
I don't have any.
I need fear.
You can't address the border.
So then Republicans turned on their own deal, on their own provisions.
They went against it.
And now they've decided they don't want to do anything about the border.
But at the same time, they know that the only reason they were able to get what they wanted
until Trump ordered them to not want it anymore was the leverage from the foreign aid package.
So now you have some people who want to vote against this because they need to retain that
leverage to get the border deal that they've been ordered not to want even though they
told their constituents they would get it.
Man that's a mess, isn't it?
So this past 67 to 27, I think, I feel like that's McConnell's way of saying, hey, you
want to oust me?
Do it.
your shot. I feel like that's in calling their bluff. Okay, so what happens from
here? Odds are it's gonna make it through the Senate. It'll pass the Senate. That's
what it looks like right now. Then it's gonna go to the House. What happens there?
Whew, I don't know. The Republican Party has a majority, but it's slim. The
Republicans in the House are more likely to take orders from Trump because they
They don't actually know what they're doing, most of them.
They are also, many of them, don't have policy.
They need fear.
So they know they can't address the border.
They just have to tell people to be scared of the border while actively stopping anything
that might alleviate that fear.
So they may vote against this because they know that at some point they may need to do
something about the border and they need this foreign aid as leverage.
All that being said, they don't have a big majority and there are a lot of Republicans
who are in vulnerable positions when it comes to the next election.
And there's probably polling going on right now determining whether or not Republicans
We're smart enough to see through Trump intentionally torpedoing the border
security deal so he can continue to scare people on it.
If that polling comes back and says that Republicans understand what the party
did, they may go ahead and push this through to try to change the subject, or
they may attach the border provision that they wanted and then they said they
didn't want because Trump told them they didn't want it but now they really do
want it again because they already told their constituents that even though they
voted against it and back and forth it may go back on to it at which point it
would probably go back to the Senate and it would the border deal would go back
on in the Senate version as well. It's a mess and it's a mess because a whole lot
of Republicans are taking orders from somebody who I would like to remind you
lost their last election anyway it's just a thought y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}