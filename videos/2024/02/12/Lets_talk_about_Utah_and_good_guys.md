---
title: Let's talk about Utah and good guys....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=w76-EnRS2HU) |
| Published | 2024/02/12 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Natalie Klein from the Utah State School Board put out a social media post with an image of a 16-year-old girl, leading to hate and threats towards the girl who had to go into hiding with police protection.
- Klein, supposed to advocate for children's safety, well-being, and privacy, triggered a situation where the girl was believed to be trans due to the post.
- The governor and lieutenant governor mentioned holding Klein accountable, but unless the legislature impeaches her, she must resign or lose at the polls.
- The moral panic surrounding the incident has led people to attack and bully children under the guise of protecting girls in sports.
- Bullying a child and making assumptions about her body is not just about hate but also about causing lasting harm to the child's mental well-being.
- If your actions lead to bullying children online, you are not on the right side of the story.
- The consequences of online bullying can be severe, as seen with the 16-year-old girl needing police protection because of a post aiming to score political points.
- The situation raises questions about the morality and actions of those involved in cyberbullying children.
- The post and comments made assumptions about the girl's identity and body, leaving a lasting impact on her.
- Bullying any child, regardless of their background, is never acceptable and should be condemned.

### Quotes

1. "If your moral panic has you bullying children on the internet, you're not the good guys."
2. "Bullying a child and making assumptions about her body is not just about hate."
3. "A child, a teen girl, wound up needing police protection because of one of these posts."
4. "You thought it was okay to bully a child."
5. "The whole moral panic convinced people to attack and bully children."

### Oneliner

Natalie Klein's social media post led to cyberbullying a 16-year-old girl, showing how moral panic can harm children online.

### Audience

Social media users

### On-the-ground actions from transcript

- Support organizations combatting cyberbullying by raising awareness and educating communities (implied).
- Advocate for policies that protect children from online harassment and bullying (implied).

### Whats missing in summary

The emotional toll on the 16-year-old girl and the need for stronger measures against cyberbullying.

### Tags

#Cyberbullying #ChildSafety #MoralPanic #OnlineHarassment #Utah


## Transcript
Well, howdy there, internet people.
Led Zebo again.
So today we're going to talk about, I guess, Utah and girls
basketball and something that occurred
and what the remedies are and whether or not
they're going to be employed and how everything is shaping up.
So if you have no idea what I'm talking about,
don't know what happened, and you miss this story,
A person with the Utah State School Board named Natalie Klein put out a social media
post with an image of a 16-year-old girl.
And that image, well, I'll just read what the girl's father said, here's a person that
supposed to be in a position of leadership that advocates for our children's safety,
well-being, their privacy, and she's the one who has instigated this post that has led to all this
hate. The girl had to go into hiding with police protection because the post led people to believe
that she was trans.
She's not. She's not.
Now, the governor and lieutenant governor, they have said that Klein's going to be held
accountable.
I mean, sure, that sounds good, but unless the legislature is going to impeach her,
she has to resign or lose at the polls.
So I don't know how that's going to happen really.
But here's the thing.
This whole moral panic, the way they have convinced people to other, and attack children,
and bully children, is by saying that, oh, well, we're protecting the girls.
a trans girl being on a sports team ever led to a cis girl needing police
protection going into hiding? Because if the answer to that is no, you're not the
good guys in this story. The comments that were on the post, it's not just all
of the hate. It was the whole idea of taking this child and making assumptions
about her body. Comments that I promise will end up being in her mind for years.
Why? Because you thought it was okay to bully a child.
She wasn't the type of child it's okay to bully and that's why it's a problem.
them. If your moral panic has you bullying children on the internet, you're
not the good guys. You might want to re-examine a whole lot of things about
your life.
A child, a teen girl, wound up needing police protection because of one of these posts.
So somebody could score some cheap points for re-election.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}