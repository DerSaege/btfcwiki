---
title: Let's talk about Michigan and precedent....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=LK2ga6JM-JE) |
| Published | 2024/02/08 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Michigan is at the forefront of a potential trend that could mitigate incidents at schools by holding parents accountable for their child accessing firearms.
- After the Parkland shooting, there was a push to raise the age to purchase a rifle from 18 to 21 to ensure parents manage firearm access, but it lost traction.
- A mother in Michigan was found guilty of involuntary manslaughter for allowing access to a firearm used in a school shooting, setting a possible trend.
- Comparisons are drawn to holding adults responsible for providing alcohol to minors who then drive, suggesting this trend is not unprecedented.
- Unsecured firearms granted access by parents could lead to legal consequences, making it vital to secure firearms regardless of individual beliefs about their children.
- There is a legal precedent being set in Michigan that firearms must be secured, with Beau stressing that this should have been standard practice already.
- Beau predicts that this trend in Michigan is likely to spread to other locations, underscoring the importance of securing firearms.
- The importance of securing firearms is emphasized as a general safety measure beyond legal implications and potential consequences.
- Beau concludes by suggesting that this trend of holding parents accountable for firearm access is something to watch as it unfolds and becomes more widespread.

### Quotes

1. "Michigan is at the forefront of a potential trend that could mitigate incidents at schools by holding parents accountable for their child accessing firearms."
2. "This is probably going to be a trend that is going to spread across the country."
3. "Those firearms need to be secured."
4. "There is a legal precedent that is being set that will spread."
5. "It should be something that was already occurring."

### Oneliner

Michigan leads trend in holding parents accountable for child's firearm access, setting legal precedent for firearm security spreading nationwide.

### Audience

Parents, community members

### On-the-ground actions from transcript

- Ensure firearms are securely stored and inaccessible to children (implied)
- Advocate for responsible firearm storage in your community (implied)

### Whats missing in summary

The emotional impact and urgency of addressing the issue of firearm access and accountability in light of school incidents. 

### Tags

#Michigan #FirearmSafety #ParentalAccountability #SchoolSafety #LegalPrecedent


## Transcript
Well howdy there internet people, Ledzebo again.
So today we are going to talk about Michigan
and something that occurred there
that is probably going to be the start of a trend
and it might help mitigate some things.
And it's something that has been talked about
for quite some time but it never really got,
it never really got a full push the way it should.
People talked about it and said, hey this might help
But, for the most part, it got ignored.
And we're going to talk about how that might be changing.
One of the things that has been suggested as a method of mitigating incidents at schools,
mass incidents at schools, is making sure that parents are more responsible when it
comes to a child accessing a firearm. The first time I remember this really
getting any significant push was after Parkland when there was an idea to bump
the age to purchase a rifle from 18 to 21. The subtext of that is if you raise
the age then you know it'll be the parents who are responsible for managing
access to a firearm. That fell out of favor the way it always does because
people reverted to their normal talking points rather than talk talking about it.
So again nothing happened. A jury in Michigan is holding a mother responsible
for basically allowing access to a firearm
that was used in a mass incident at a school, used in a mass shooting.
She was found guilty of
four counts of involuntary manslaughter. That's probably going to become a trend.
And people are going to say that that's not right.
But when you compare it to anything else, it's the way it's always been.
If you were to provide access to alcohol, to minors, and then they got on the road,
what would happen?
It's the way it's always been with other stuff.
This is probably going to be a trend that is going to spread across the country.
The firearm, if it is not locked up, if it is not secured, and it was a parent who granted
access to that firearm and it gets used, I'm going to suggest there's going to be a high
probability that that parent is now going to be on the case. They're gonna be on the
case with the kid. It is something for people to note because there are a lot
of people who have the idea that, well, not my kid. Okay, maybe that's true, but
you know what? You should still secure the firearm. Just saying that that'd
That would probably be a good idea.
And now there is a legal precedent that is being set that will spread.
Those firearms need to be secured.
And I would remind everybody that should happen anyway.
That should be something that was already occurring.
So we can watch this, but I'm fairly certain you're going to see it in other locations.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}