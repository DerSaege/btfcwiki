---
title: Let's talk about NY, kicking back, and DOJ....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=lvqyrxPVwwQ) |
| Published | 2024/02/08 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Federal government targets New York City Housing Authority in a bribery takedown.
- Between 65 and 70 people have been arrested in connection with the investigation.
- Allegations suggest a pay-to-play, kickback scheme within the Housing Authority.
- Scheme involves exploiting a loophole in no-bid contracts under $10,000.
- Potential charges include bribery and extortion.
- More revelations expected as cases progress.
- DOJ's actions likely a message against illegal practices.
- Emphasis on the illegality of pay-to-play and kickbacks.
- Possibility of further investigations and charges.
- Department of Justice intends to remind city employees of the legal consequences.

### Quotes

1. "DOJ has engaged in what is being referred to as the largest bribery takedown in their history."
2. "It appears that the way this functioned was basically it was a pay-to-play scheme."
3. "When something is as widespread as the feds are alleging here, there's probably more."
4. "Understand it's illegal and doing it with this many people was a way of generating headlines."
5. "This is probably going to expand into a much larger thing."

### Oneliner

Federal government targets New York City Housing Authority in a massive bribery takedown, exposing a pay-to-play scheme and sending a strong message against illegal practices.

### Audience

City residents, government employees.

### On-the-ground actions from transcript

- Report any suspicions of corruption to relevant authorities (implied).
- Stay informed about legal practices and regulations within your community (implied).

### Whats missing in summary

The full context and detailed implications of the corruption scandal. 

### Tags

#NewYork #Corruption #PayToPlay #Bribery #CommunityPolicing


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about New York,
New York City Housing Authority and the way things work
in New York at times and paying to play
and all of that stuff and how it appears
that the federal government is very unhappy
with the way things sometimes work in New York.
Okay, so DOJ has engaged in what is being referred to
as the largest bribery takedown in their history.
They have apparently arrested somewhere
between 65 and 70 people.
It looks like it stems from an investigation
of the New York City Housing Authority
and a pay-to-play, a kickback scheme.
It looks, at first glance, like it's going to be bribery, extortion, and stuff like that will be
the charges. I'm going to suggest that this isn't the end of this, that as these cases move forward
and people start talking about it, there's going to be more. It appears that the way this functioned
was basically it was a pay-to-play scheme in that centered on the no-bid
contracts that are under $10,000 as a way of mitigating this practice which if
you don't know what it what it means it's let's say you are a contractor you
fix sinks, okay, or you install toilets. And the Housing Authority has 10 new
toilets that need to be put in, okay, at $900 a piece. So it's $9,000. The person
who makes that decision is given $500 bucks and they give it to the contractor
who provided them with that. Because the total amount was under $10,000, there's
There's not a bidding process that is designed to, A, save the city money, and B, mitigate
this.
It appears that they were exploiting that loophole, and that's where it was occurring.
When something is as widespread as the feds are alleging here, there's probably more,
more substantial ways and using other loopholes rather than just the under $10,000 thing.
I do not think that we've heard the end of this in any way shape or form. This is probably going
to expand into a much larger thing. This is also something that is probably the Department of
justice kind of sending a message to the city employees in large cities saying
hey maybe this has become an accepted practice but understand it's illegal and
doing it with this many people was a way of generating headlines to remind
people that pay-to-play and kickbacks are in fact against the law. Anyway it's
If it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}