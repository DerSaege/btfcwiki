---
title: Let's talk about Biden and Trump being treated differently....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=YSE5ALz-naM) |
| Published | 2024/02/08 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Comparing Biden and Trump on handling documents.
- Impending release of a report on Biden's handling of documents.
- Anticipating different outcomes between Biden and Trump.
- Mentioning the absence of charges for Biden.
- Predicting bad faith arguments about different treatment.
- Speculating on Biden's likely course of action with discovered documents.
- Contrasting Biden's conduct with Trump's alleged willful retention.
- Explaining the basis for potential charges against Trump.
- Hinting at no charges for Pence due to lack of willful retention.
- Criticizing commentators for misleading comparisons.
- Emphasizing the importance of reading the report for clarity.
- Stating the core difference in outcomes is based on conduct and not preferential treatment.

### Quotes

1. "The conduct is different, therefore the outcomes are different."
2. "It's not that one got preferential treatment."
3. "They're gonna lie to you."
4. "It's just a thought, y'all have a good day."

### Oneliner

Beau compares Biden and Trump on document handling, predicting different outcomes based on conduct, not preferential treatment.

### Audience

Political observers

### On-the-ground actions from transcript

- Read the report for clarity (suggested)

### Whats missing in summary

The full transcript provides a detailed comparison between Biden and Trump's handling of documents, focusing on potential legal outcomes based on conduct rather than preferential treatment.

### Tags

#Biden #Trump #DocumentHandling #LegalOutcomes #Comparison


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Biden and Trump
and differences and similarities,
and we are going to run through
what is likely to be the difference
because the report when it comes to
Biden's handling of documents is due out.
The public is going to get to see large portions of it
from what I understand,
but that whole thing is wrapping up.
It looks like the special counsel
is wrapping up that investigation.
And obviously, there's going to be different outcomes.
We know this because, you know,
there aren't charges for Biden.
This is going to lead people to operate in bad faith.
They're gonna say, look, they're being treated differently.
Trump got charged for the same thing, and they're going to lie and say that there's
a difference in treatment rather than a difference in conduct, which is going to be your answer.
We don't know yet because the report isn't public yet, but since there aren't charges,
I'm going to take a wild guess here.
I'm going to say that when Biden or his team discovered that those documents were out in the
wild and in a location that was his or in his possession or whatever, that they contacted
proper authorities and returned them. It is very hard to charge somebody with willful retention
when they did not retain the documents upon discovery. Difference in conduct.
Trump, on the other hand, is alleged to have willfully retained the documents.
Therefore, he would be charged with willful retention. The allegations say
that Trump knew the documents were there, improperly stored them, lied to the
federal government about possessing them, had attorneys say that they were all
returned, so on and so forth. All of this is what constitutes the crime. It's not a
difference in treatment, it's a difference in conduct. That's why the
outcomes are different. I'll go ahead and tell you based on what I have heard
about what Pence did, there will be no charges there either. Why? Because he
didn't willfully retain them. It's really that simple. But it doesn't matter
because we live in a world now where commentators and people who like to pass
themselves off as journalists or reporters are going to try to say that
But it's the same.
They're going to lie to you.
They're going to try to manipulate you.
The report will come out.
You can read it.
If you have any doubts as to why the Trump-appointed investigator decided not to charge, I mean,
you could read that rather than listen to somebody who has an agenda, a reason to tell
you something other than the truth? The answer is very simple. The conduct is different,
therefore the outcomes are different. It's not that one got preferential treatment. It's
The one clearly followed the law and the other, well, the allegations are that he didn't.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}