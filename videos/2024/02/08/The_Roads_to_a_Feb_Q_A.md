---
title: The Roads to a Feb Q&A
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=auOEuF521lw) |
| Published | 2024/02/08 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau hosts a Q&A session where he answers questions sent in by his viewers without any prior preparation.
- Questions are chosen by the team based on interest and frequency of similar inquiries.
- Beau explains why some questions are shortened for brevity and to prevent revealing personal information.
- He clarifies that being a veteran with certain beliefs does not make him a minority in the larger veteran community.
- Beau addresses the U.S. support for Israel, citing regional power dynamics and the balance of power in the Middle East.
- He dismisses claims of scandal involving a DA in Fulton County, stating it's not worth his time due to lack of substantial impact.
- Beau talks about Republicans perceiving the economy negatively and attributes it to the economic conditions in predominantly Republican states.
- He refutes the idea of bias in his coverage of Trump, stating that Trump has nothing beneficial to offer the U.S.
- Beau warns about the dangers posed by individuals who seek to enforce their religious beliefs through state power.
- He explains the importance of U.S. aid to Ukraine for European security and U.S. dominance, particularly in countering Russia's ambitions.
- Beau suggests using local elections, especially school boards, to attract younger voters disenchanted with the political system.

### Quotes

1. "Trump and authoritarians like him are like lead in your drinking water. There is no acceptable safe level."
2. "The U.S. wants to keep Europe allied to it; it helps the U.S. maintain dominance."
3. "Republican economic policies don't do well. They sound good, but that's not how the real world works."
4. "Getting them involved in electoral politics is going to be hard."
5. "A little bit more information, a little more context and having the right information will make all the difference."

### Oneliner

Beau addresses viewer questions on various topics, from U.S. support for Israel to Republican economic policies, providing insightful explanations and perspectives.

### Audience

Viewers, Voters, Younger Folks

### On-the-ground actions from transcript

- Contact school boards to get involved in local elections (suggested)
- Stay informed about political issues impacting your community (implied)

### Whats missing in summary

Insights on the Q&A session dynamics and Beau's candid responses to viewer inquiries.

### Tags

#Q&A #U.S.ForeignPolicy #RepublicanEconomics #LocalElections #YouthEngagement


## Transcript
Well, howdy there, internet people.
It's Bo again.
And welcome to the Roads with Bo.
And today is a Q&A.
These are messages from y'all that the team picked out.
And we're going to go through them.
And I will try to give you as good an answer
as I can provide with absolutely no prep time,
because I haven't seen these yet.
OK.
So what are we starting off with here?
Oh, I'm supposed to start by telling you
that if you want to get a message in here,
you need to send it to question4bo.
That's question, F-O-R-B-E-A-U, at Gmail.
OK.
How do you choose questions for the Q&As?
And not that I care, because I appreciate the answer.
But why did my last question get rephrased?
And then in parentheses, it says her last question
did not get rephrased.
Somebody else asked a similar one.
but maybe explain that sometimes we cut for length.
OK.
So the questions are picked out by the team.
And there are hundreds that come in each week.
So they don't all get answered.
And one of the things that I know that they do,
aside from pick things that they find interesting,
is they pick things that come in a lot.
So if a whole bunch of people ask the same question
or similar questions, it is more likely to get picked.
And when it is a long message, sometimes they
will cut it down to just give me the necessary parts, I guess.
Sometimes I have noticed that they
do remove a little bit of context that
might have changed an answer.
But they also make sure that I don't accidentally
reveal personal information as well.
because, again, I could just be reading it and mess it up.
OK, so I think that answers everything.
All right, I'm really starting to feel alone here,
like I'm the last American veteran who still
holds the core values of the oath he swore
when he joined the service in the first place.
Am I really the minority here?
Speaking of ones that I feel like probably have more context,
I'm willing to bet there's more to this message
that I didn't get.
But my guess is this is somebody who is
probably in a very red area.
And they feel like all vets lean one way.
They don't, it's not true.
I mean, keep in mind that most of the people on this channel, many of whom are further
to the left than I am, that they have military experience.
It's a matter of finding who you're looking for.
I have searched for your video on why the U.S. continues to support Israel as part of
US foreign policy, but I haven't been able to find it. Please help me find that
video as I cannot begin to understand how this administration continues to
support Israel at this point. I understand, not completely, your point that
foreign policy is about power. What power does Israel have that isn't
explicitly given by the US? If we remove our support, Israel has no power. Okay, no.
Alright, so there are multiple videos where I have talked about this, and this is one
of those things.
It's not that people can't find it, it's that they hear it in the videos, but they
don't want to believe it's really that simple.
People want a much more complicated answer than what it is.
In the Middle East there are three regional powers, Iran, Saudi Arabia, and Israel.
Saudi Arabia and Israel currently lean towards the United States.
That gives the United States the edge when it comes to the balance of power.
Therefore Saudi Arabia and Israel can get away with a whole lot.
Things that the U.S. wouldn't be so lenient about when it comes to other countries.
Because they're regional powers, they get away with more because the US doesn't want
to disrupt the balance of power and risk them going to the side that has Iran.
See, as Americans, we look at the Middle East as a dynamic where Iran and Saudi Arabia,
well, they would never be friends, and Iran and Israel, they would never be friends.
Please remember the reason Iran is the way it is, is because of the United States.
US intervention helped shape it.
It can go the other way and there are situations where Saudi Arabia and Iran might find themselves
on the same side if they had an economic or military reason to do so because foreign policy
is not about all of the things that governments tell people it is.
It's about power and nothing else.
That's what it's about.
That's why the U.S. will ignore pretty much whatever because they're looking at it through
the lens, U.S. foreign policy, and this isn't this administration.
This is any administration.
This is what foreign policy is.
It's about power.
Pretending it's about something else is what makes it confusing.
Now, as far as Israel having power that isn't given to it by the US, that's like a common
line of thinking in the US, and it's not true.
Israel is a nuclear power.
They are the only nuclear power in the region.
have a whole lot of power. If the United States was to stop giving it power, which is where this
train of thought normally heads to, you know, just completely cut off financial and military aid and
all of that, Israel is strong enough to be an inviting opportunity for China or Russia.
And they would get it from there, which would shift the balance of power in the Middle East,
which is why the US doesn't do it. People want a more complicated answer than
it's all about power. It's not. It's really that simple. People want a
complicated answer because the the effects of this are so pronounced. They
want there to be a more important reason. There's not. There's not. Okay, I keep
emailing you to please make a video on what is happening with Willis. So the DA
in Fulton County, is that not worth your time? Just wondering, I would really like
like to hear your take on this.
Yeah, it's not.
OK, so maybe I should do something just explaining
why I'm not talking about it.
If you don't know, the DA there, there
are a lot of allegations, let's call them
allegations at this point, that involve
her having a personal relationship
with another prosecutor.
This is, it is scandalous.
You know, all of the insinuations, it's good tabloid news, it makes for good headlines.
And sure, the Trump team is trying to say that this is going to be good for them, and it's enough to get rid of the
whole case and everything.  Yeah, these are the same people that were like indicating that he had the power to call in
SEAL Team 6 on his political rivals.
Based on what I have seen, and I have actually talked to attorneys about this and attorneys
from Georgia about this, the general tone, my personal opinion, is that there's really nothing
here. Most of them are saying there's something here but it's kind of minor. That's probably half
of the people that I've talked to. A quarter of them are like there's nothing there and then
there's a quarter that's like no this is a huge deal. So you have people that have a wide degree
of opinion on whether or not her behavior when it comes to this other prosecutor constituted a
conflict of interest or some ethical issue and it just runs the entire spectrum of belief from
it doesn't matter at all to it's a really big deal. The one thing that all of these people agree
on is that it doesn't impact the actual case against Trump at all. It's not going to throw
it out. So it isn't something that I have taken seriously. This is one of those things that
But to me, this has to do with somebody's personal life, and that's the only reason
it's news, and it's not something I generally cover.
There's plenty of coverage about it.
When it gets to the actual motions and the legal arguments that may eventually arise,
I'll probably cover it then.
But covering who went on vacation with who, I literally don't care.
I don't think that it matters.
And the attorneys that I've talked to, they have all said that this is not something that's
going to derail the case.
That the worst that would happen would be somebody else take over the case.
But it wouldn't lead to a dismissal.
Why do you think Republicans are more likely to say the economy is bad than Democrats?
The obvious answer is that they're in polling, they're saying what they want the polling
to say.
So they're saying the economy is bad because they want to blame Biden.
I think the more likely answer is that to them it is worse.
Most Republicans will live in Republican states.
Out of the top ten most poverty-stricken states in the United States, nine of them are red
states.
I think it's really that simple.
They have a dimmer view of it because they have a state economy that doesn't do well
because they have policies that are specifically designed to keep them down.
And then they're surprised, but they'll continue to vote for it, so it is what it is.
You seem to be posting a lot about Trump.
The more little media I consume, it seems he might be America's answer.
Do you think your prejudice is clouding your vision?
No.
Trump and authoritarians like him are like lead in your drinking water.
There is no acceptable safe level.
The other tactic, which I have seen floated, by the way, on the message boards for Republicans
that are active online and what y'all are supposed to say, I know that it's out there
now that you are supposed to say, hey, you're talking about Trump all the time.
You're talking about Trump all the time.
Yeah, he's a presidential candidate.
He's news.
The reason y'all have that talking point now, and that's the way you're supposed
to behave, is to get people to stop talking about him.
The reason is because he's a failure.
They don't want people highlighting all of the ways he's failing.
That's what it's about.
No, there is nothing that Trump can bring to the United States in a second term that
That is beneficial.
It's that simple.
That's not bias, that's statement of fact.
As it becomes more and more likely Trump will face consequences, either in loss of wealth,
jail time, and disgrace of his name and brand, it seems to me he will do anything to avoid
And there are many people in the USA who believe everything that he says and will be violent
on his orders.
We have seen it before on Jan 6th.
In the next months, as these cases start and drag on, he will become more scared and aggressive.
And he will tell them to stand by, not stand down again.
I mean, I get the subtext here and the question, which there's not actually a question in this,
at least not what got included.
Is that a risk?
I mean, yeah, it is.
It is, but that is, this is one of those things that a lot of people don't understand.
In the U.S. you have a lot of people saying this as a way to dissuade him being held accountable.
But the reality is the levers of power, the mechanisms that be, when they hear that it
just becomes more important to hold him accountable.
So yes, I've said on the other channel, he's likely to become more erratic and say more
erratic things.
And some of that very well might cause real problems.
But I mean, there's nothing at this point that is going to stop that chain of events.
So we have to ride it out.
Many Republicans have made and elevated anti-Semitic statements and conspiracies over the years,
yet they claim to support Israel.
What am I missing?
So when you are talking about the right wing that is super pro-Israel, it's mostly people
who believe Israel has to exist as a country and it has to be involved in a war.
It has to do with religious beliefs in the United States and bringing about revelations
in the end times.
That's where a lot of it comes from.
At the same time, it's worth remembering when you were talking about the normal conservatives,
quote, Israel's a pretty conservative country.
So on some level it's just a shared belief system, but when you are talking about those
that are just over the top in their support, most times you will dig into it and you will
find out that they are evangelicals who believe that not just that Israel needs to exist,
that Israel needs to exist in a perpetual state of war so that the final, you know,
Armageddon can occur and all of that stuff.
There's a deep, very weird religious thing to it.
Okay.
You say that USAID to Ukraine is critical for European security, hence US security,
Why is the most economically prosperous part of the world outside of the US need US aid?
Why does the most economically prosperous part of the world need US to maintain their
security?
Which aid are the Europeans giving to Ukraine compared to what the U.S. has sent, want to
send?
I don't want to sound like a MAGA America First person, but I'm genuinely curious about
why Europe can't provide sufficient aid to their next door neighbor and needs our help.
Okay, so first, Europe is providing a lot of assistance.
Like right now, the US is trying to get through a $60 billion aid package.
The EU got through a $50 billion aid package.
I do not have a running total of what Europe has provided and what the US has provided.
But the European powers are not like, they're not slacking.
I mean, I would imagine if you're in Ukraine, you feel like they are.
They are not expecting the United States to pull all of the weight.
They are putting a lot into it.
At the same time, why does Europe need the US for its own security?
That goes both ways.
First, think of it like this.
I have a friend, and this friend works on cars,
like really knows how to work on cars.
And they have all the tools, like any possible tool
you would need to work on a car.
And I'm talking about like specialized stuff.
They have it all.
I don't need to buy it.
I can use theirs.
There's a little bit of that at play with Europe.
But at the same time, meaning that the US has this massive military, Europe doesn't
need to buy one as well.
At the same time, European security is American security, because if European security fails
and a hostile power, an adversarial nation, was to gain control over what you described
as the most economically prosperous part of the world outside of the United States.
What does that mean for the US?
It's one of those things when it comes to Europe, because of the amount of power coupons
they have, being economically prosperous and foreign policy being about power, the United
States wants to keep Europe allied to it.
it maintains, it helps the U.S. maintain dominance in that way.
If another country that perceives itself to be a near-peer gets a bigger foothold in
Europe and starts to whittle it away, it chips away at U.S. dominance as well, and that's
not something the U.S. would want.
Now it's worth noting that Russia is not a near-peer.
They have demonstrated that very clearly.
At the same time, they have demonstrated it to the world, but they don't realize it themselves
yet, which makes them pretty dangerous because they very well might pick another fight that
they can't win.
The fact that this is going on as long as it has demonstrates that Russia is not in
the same league as the US or China.
But Russia doesn't know that.
There aren't a lot of things more dangerous than somebody who doesn't understand their
own limitations because they could do something completely unpredictable because they believe
they can win.
I mean, please keep in mind the amount of time that has elapsed since the beginning
of this three-day operation or two-week operation or whatever it was supposed to be.
The fact that Russia, a country that wants to be seen as a world power, has not been
able to take Ukraine is an utter humiliation.
Again, they still haven't got to the hard part.
They weren't able to actually take the territory.
And that shouldn't have occurred.
But Russia still wants to cast the image that they're the Soviet Union, that they are as
capable as the Soviet Union was.
They're not.
And that means that they very well might hit another country.
They might try to move into another country.
More importantly, they might try to hit a country that is NATO-aligned.
it's it's a real risk okay I'm just wondering what your thoughts on the
threat level of Dominionists okay okay so for those who don't know there is a
and this really gets into it too this is kind of cool but it's also way too long
to read. Okay, so there is a subset of the religious right in the United States who believe
that their political views and their religious views, that they should use the power of the
state to enforce their religious views. And I know there's a whole bunch of people right
now going, that's all Republicans. No, I mean, these people are creepy about it. Like even
more so than your run-of-the-mill Republicans who are trying to use the state to enforce
their religious views.
They go to a whole new level with it.
And the real question here is, are they a threat?
And can they be a threat?
The answer is yeah, yes.
because the views being espoused are not politically tenable. They can't really
hold those politically. It means they have to use force to achieve them. So, yes,
there is a pretty big risk. I don't know if it's dominionist per se. I don't know that that's the
right term. There are a lot of far-right religious groups that want to use the
violence of the state to enforce their religious beliefs on others. And yeah,
there's a big danger there because the positions that they hold, they're
not popular, therefore they can't win elections, therefore they need to use a
of violence to take control of the levers of power of the government.
That's where the risk comes in.
That's probably something that will end up turning into a video when it comes up.
We're probably a few months away from that becoming a major issue that's going to be
talked about.
I think we're going to have to get closer to the election for that one.
What do you think the correlation would be if pollsters asked, do you think you're worse
off financially than you were x number of years ago?
And how much money have you donated to a Republican campaign?
And do you think if it is a positive correlation, as I suspect, that would convince people that
a decent part of their economic hardship might be self-inflicted?
Either because they keep giving money away, or because the people they support win and
it hurts them.
This goes to out of the top 10 most poverty-stricken states in the United States, nine are Republican
states.
And that's pretty much always like that.
Republican economic policies don't do well.
They sound good, but that's not how the real world works.
So I mean, would it convince them?
No, it wouldn't because you already have that information.
Like you can already point to a map and do an overlay of here are the Republican states,
here are the poverty-stricken states.
Yes, it looks like it's the same map.
And that's not enough to convince them.
So yes, I believe that correlation would exist.
No, I don't think it would alter opinions, though.
There have been times in my past when voting for president seemed a foregone conclusion,
especially since my state electorate, Washington, will always lane Democrat.
I have almost always voted anyway, as there is often a local election issue that is important
to me.
It seems to me that this get involved, vote locally idea could be used in an effort to
draw in younger folks who might not otherwise vote.
How could this message be put out in a way they might hear and give AF about?
Use school boards.
If you were going to go this route with it, use school boards.
One of the reasons young people who are very politically inclined today, younger people
People today are more politically aware than people want to give them credit for.
They just don't buy into the electoral process.
Why?
Because it's completely failed them at every turn.
A whole lot of young people today, people who are recently graduates of high school,
they probably think that the government should do something about the fact that they had
to learn how to run, hide, fight, but the government hasn't.
The government's only sent thoughts and prayers, and it doesn't matter.
Even if you're somebody who's a huge supporter of the second, I feel like even you would
suggest there's got to be something that could be done, right?
But the government does nothing.
That can't be something that is motivating for people coming straight out of high school.
This is something they have to deal with, and the government does not care, does not
accomplish anything.
So if you wanted to get them involved, you might want to use that message and talk about
how the school board and getting new people in the school board might be able to impact
that or how it might, if they went and voted in school board elections, that it might be
able to stop their friends from being harassed, that kind of thing.
That's how I would try to go about it.
But you're in an uphill fight because younger people today have watched older generations
do absolutely nothing to help them.
Not listen to their concerns and certainly not address them.
So getting them involved in electoral politics is going to be hard.
So that looks like all of the questions.
Some of these will probably end up being turned into videos if we have a slow Newsweek because
these are definitely good jumping off points.
So I guess that's it.
A little bit more information, a little more context and having the right information will
make all the difference. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}