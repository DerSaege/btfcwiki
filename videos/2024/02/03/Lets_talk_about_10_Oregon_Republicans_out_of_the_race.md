---
title: Let's talk about 10 Oregon Republicans out of the race....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=57Oh-de6BM0) |
| Published | 2024/02/03 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Oregon's Republican Party staged boycotts, preventing lawmaking, which angered voters.
- Measure 113 was passed, disqualifying legislators with ten unexcused absences from running in the next election.
- Republicans changed the interpretation of the measure to avoid disqualification until a subsequent election.
- A six-week boycott by 10 Republican lawmakers led to their disqualification from running.
- The state Supreme Court upheld Measure 113, making the 10 lawmakers ineligible for the next election.
- Incumbents typically have an advantage in elections, but these Republican lawmakers lost their edge.
- The vacant seats will lead to primaries and political chaos due to the Republican Party's refusal to abide by voter expectations.
- Despite voter approval of Measure 113, lawmakers attempted to exempt themselves from its consequences.
- The state Supreme Court's decision ensured that legislators couldn't skip work and keep their jobs.
- Beau criticizes the lawmakers for their hypocrisy in disregarding the working class while expecting special treatment.

### Quotes

- "I cannot show up for work and keep my job. I'm not like you commoners."
- "What the voters intended is what's going to happen."
- "They did not show up for work, unexcused, as they often try to kick down at the working class of this country."
- "Incumbents have an edge, always."
- "I get to do whatever I want."

### Oneliner

Oregon Republicans face consequences for boycotting legislation, losing their edge in upcoming elections due to the state Supreme Court upholding Measure 113.

### Audience

Voters, Activists, Politicians

### On-the-ground actions from transcript

- Contact local representatives to ensure they uphold voter-approved measures (implied).
- Stay informed about local politics and hold elected officials accountable for their actions (implied).
- Participate in upcoming primaries to support candidates who prioritize representing constituents over ruling (implied).

### Whats missing in summary

The full transcript provides a detailed breakdown of how Oregon's Republican Party faced repercussions for attempting to circumvent voter-approved measures and the importance of accountability in politics.

### Tags

#Oregon #RepublicanParty #Measure113 #StateSupremeCourt #Voters #Accountability #Primaries


## Transcript
Well, howdy there, Internet people.
Let's bow again.
So today, we're going to talk about Oregon and the Republican
Party up there, the Supreme Court of that state, what they
determined and how it's kind of a big deal.
And just run through what occurred and why the next
election up there, there's going to be a lot of open seats.
Okay, so a little bit of background information.
One of the things that was going on in Oregon
was basically legislators would,
they just wouldn't show up for work.
They'd boycott the session
and stop lawmaking from occurring.
The voters did not like this.
They passed something called Measure 113
that basically said if you had ten unexcused absences when it came to the floor, well,
you didn't get to run in the next election.
That was a pretty simple reading of what the measure did.
As time went on, the Republican Party didn't like that, so they changed what it meant.
They started saying, no, it didn't mean the next election, it meant the one after that.
That's the one you can't run for, because that makes sense.
They staged one of their little boycotts, and there were 10 lawmakers, 10 Republican
lawmakers that didn't show up for definitely more than 10 days.
The boycott, the walkout was six weeks long, which is a record, I think.
So they were precluded from running.
They took it up to the state Supreme Court and the state Supreme Court said, no, read
it.
You don't get to run.
So 10 Republican lawmakers are now ineligible to run in the next election.
One of the things that becomes pretty apparent very, very quickly when you first start paying
attention to politics is that incumbents have an edge, always.
Unless they are really, really horrible and they've been so bad at their job that there's
negative voter turnout, then they have an edge.
Ten Republican seats lost that edge.
This is going to matter and now because they're going to be vacant seats, there's probably
going to be primaries involved, it's going to be a giant mess because the Republican
Party out there, they tried to rule rather than represent.
The voters passed measure 113.
They passed that and they made it very clear what they expected.
The representatives were like, no, that doesn't apply to me.
I cannot show up for work and keep my job.
I'm not like you commoners.
I get to do whatever I want.
The state Supreme Court decided no.
What the voters intended is what's going to happen.
You in fact do not get to keep your job.
I would like it kind of remembered that that's how it went down.
They did not show up for work, unexcused, as they often try to kick down at the working
class of this country and talk about how lazy they are.
Can any of them name a job where you could just walk out for six weeks with no excuse
and come back and expect to keep your job?
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}