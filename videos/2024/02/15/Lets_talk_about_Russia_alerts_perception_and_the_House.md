---
title: Let's talk about Russia, alerts, perception, and the House....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=0NdfiIFfwwA) |
| Published | 2024/02/15 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing perception vs. reality, discussing Russia, Star Wars, and GOP House.
- Republican House Intel Committee chair raised national security alarm on declassifying info.
- Despite alarm, Republicans decided to go on a two-week vacation instead.
- Leaks suggest a space-based nuclear program from Russia, potentially involving satellites.
- Intelligence community has long been aware of such programs; not groundbreaking news.
- Disagreement on the need to declassify information for a national conversation.
- Average person likely unable to contribute meaningfully to this specialized topic.
- Risk of declassifying information includes exposing intelligence sources and methods.
- Russia, behind the potential program, is currently engaged in a war and facing military challenges.
- Funding requests in the US to support Ukraine against Russia's actions.
- Republicans seem more interested in using issues for fear-mongering than solving them.
- Choice between addressing funding for Ukraine to counter Russian advancements or going on recess.
- Beau concludes with a call for action and reflection on the situation.

### Quotes

1. "It's about fear."
2. "They don't want to solve an issue because they need to scare people."
3. "The Speaker of the House chose to go to recess instead."
4. "This could be addressed."
5. "It's just a thought, y'all have a good day."

### Oneliner

Beau addresses Republican actions on national security, Russia's nuclear program, and the choice between addressing critical issues or going on vacation.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Advocate for responsible decision-making within political leadership (implied)
- Support funding for Ukraine to counter Russian advancements (implied)
- Stay informed and engaged with national security and foreign policy issues (implied)

### Whats missing in summary

Beau's passionate delivery and emphasis on the importance of addressing critical issues rather than resorting to fear-mongering tactics.

### Tags

#Politics #NationalSecurity #RepublicanParty #Russia #Ukraine


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk a little bit
about perception and reality.
And we're going to talk about Russia and Star Wars
and the GOP House, Republicans in the House,
and how they're perceived
and how they can't really be surprised
by the way they're perceived,
because we have a very simple example
that shows why the public views them the way they do. If you missed it and you
probably didn't because the beginning to this was like super sensationalized, but
a the chair of the House Intel Committee who is a Republican basically raised the
alarm and was like there's a national security issue we need to declassify
stuff. We need a national conversation because it's important and destabilizing
and blah blah blah. So this is from a high-ranking Republican House Intel
committee chair or so on and so forth. What did Speaker Johnson do in response?
Like what's the next thing that happened? We're gonna go on vacation for two
weeks. I mean, I don't get it. Are we supposed to be getting gas masks or sunglasses? Going
to the beach? What's going on? There's mixed messaging. Okay. So, let's talk about that
part first. What is it? The general consensus based on leaks, and please keep in mind this
This is early reporting based on information that has trickled out.
This is not actually confirmed yet.
The idea is that it has to do with a space-based program from Russia that is nuclear.
It is important to note that this could mean a nuclear-powered space-based initiative of
some kind. It may not necessarily mean a nuclear weapon, and it could have
something to do, even if it was a nuclear weapon, it could also have
something to do with taking out satellites and not actually dropping it.
So there's that as well. So that's the information. That's what
it appears they're talking about. Is this something to panic about? No, it's
It's actually not, it isn't.
These programs have been known about by the intelligence community for quite some time.
Maybe there is new information talking about how far along they are or when they plan to
test it, something like that.
But this isn't actually like earth shattering news.
So Johnson is correct, right?
No, not really.
No.
I mean, this is one of those things where, no, you really didn't need to raise an alarm
about this.
And I don't understand the idea of needing to declassify information to have a national
conversation about it either.
I'm fairly certain that the average person is not going to have an informed opinion on
spread of nuclear weapons into space or space-based warfare, satellite-based warfare, any of that.
I don't think this isn't something that, you know, the average person is going to be able to contribute
a lot to the conversation. Not when declassifying information might risk the sources and methods,
methods, means and methods that were used to obtain the information.
That seems like a bad idea.
But be that as it may, Johnson isn't right either.
Because yeah, sure, here is this topic being blown out of proportion.
Fine, we're going to go on vacation.
But also, no, not fine, because the country behind this that, you know, apparently wants
to hang nukes over our heads in some way, they are currently fighting a war.
And there's a country that is just hollowing out their military.
And the funding that Russia has to commit to that war slows their groundbreaking technology
or whatever.
And there's the whole funding thing from the US where Congress is being asked to fund
American companies to produce stuff for the war in Ukraine and supply that aid.
But I guess while Republicans are going to pretend it's a huge issue, like many things,
they don't want to do anything about it because what they really want is the issue.
They don't want to solve an issue because they need to scare people.
They need to be able to issue that national alert and say something is scary because that's
what it's about.
It's about fear.
This could be addressed.
The Speaker of the House chose to go to recess instead.
The funding for Ukraine would help reduce the likelihood that whatever this technology
is ever comes online, because that funding requires Russia to match that funding, to
continue to commit resources to a conflict that has been nothing but a national embarrassment
for them.
Or I guess, you know, they could go on recess instead.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}