---
title: Let's talk about a chance for Dems to play hardball in Wisconsin....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ZGVQaMn_QhM) |
| Published | 2024/02/15 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the situation in Wisconsin with the Republican Party giving up and the Democratic party having a chance to play hardball.
- Talks about how the Republican Party has used gerrymandering to maintain an unfair advantage in Wisconsin.
- Mentions that the courts have ordered new maps to be created due to the unfair advantage created by the Republican Party.
- Points out that consultants deemed the map supported by the Republican Party as unworthy of consideration.
- Notes that the Republican Party has seemingly caved and decided to vote in favor of a map proposed by the Democratic governor.
- Suggests that the Republican Party's acceptance of the new maps is because they are still favorable to them compared to what the courts might decide.
- Expresses a personal opinion of allowing the courts to choose the map rather than accepting the Republican-approved one.
- Raises the possibility of the Democratic Party vetoing the map to allow the courts to select a more favorable option for them.
- Emphasizes that the Democratic Party could potentially gain even better maps through court decisions.
- Encourages a more strategic and assertive approach from the Democratic Party in this situation.

### Quotes

1. "The Republican Party has used gerrymandering to maintain an unfair advantage."
2. "I personally, if it was me, I would allow the courts to decide."
3. "If the Democratic Party wanted to play hardball for once, they could veto it."
4. "The odds are that the courts would choose a map that is even more favorable to the Democratic Party."
5. "It's just a thought, y'all have a good day."

### Oneliner

Beau suggests letting courts decide on new maps in Wisconsin, offering the Democratic Party a chance to play hardball and gain more favorable outcomes.

### Audience

Politically active citizens

### On-the-ground actions from transcript

- Contact Democratic representatives to advocate for a strategic approach to map decisions (suggested)
- Join local political organizations to stay informed and engaged in similar situations (exemplified)

### Whats missing in summary

The full transcript provides a detailed analysis of the political dynamics in Wisconsin, offering insights into the implications of map decisions on party advantages and urging for a strategic approach.

### Tags

#Wisconsin #Gerrymandering #DemocraticParty #RepublicanParty #CourtDecision


## Transcript
Well, howdy there, Interno people, it's Bo again.
So today we are going to talk about Wisconsin, we're going to talk about
Wisconsin and we're going to talk about the Republican party giving up,
admitting defeat, and we're going to talk about an opportunity for the
Democratic party to play hardball for once.
We have been talking about the maps up there in Wisconsin for quite some time,
talking about how the Republican Party has used those maps to maintain an
unfair advantage. They've been engaged in gerrymandering. The courts believe that
too. Now the order was to have new maps put together and people submitted new
maps, different groups. The Republican Party, well they got behind a map that
was so bad when consultants looked at it, they said that it does not deserve
further consideration. Other maps? Well, they made it. The Republican Party has
caved, seemingly, and decided to vote in favor of a map proposed by the governor
who is a Democrat. They're doing this because it's still kind of favorable to
them and if they wait then they may get a worse map for them. The governor can
veto it. The Republican Legislature, the gerrymandered Republican Legislature, pushed these maps
through and said, fine, Governor, we'll give you your maps. The Governor could veto it.
And let the courts decide. If that occurs, the Democratic Party will probably get better
maps than the ones that the Republican Party just approved. I understand the
spirit of goodwill and all of that stuff, but the reality here is the reason
they're okay with these maps is because they're the best for them. This is a
party that gerrymandered this state for a decade. I would personally, if it was
me, I would allow the courts to decide. I would allow the courts to choose the
map. We don't know what the governor is going to do. The governor has options and
can pursue it in various ways, but if the Democratic Party wanted to play hard
ball for once, they could veto it because the odds are that the courts would choose
a map that is even more favorable to the Democratic Party and is even less
partisan in nature. Anyway,
It's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}