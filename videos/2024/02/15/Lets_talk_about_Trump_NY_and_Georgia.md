---
title: Let's talk about Trump, NY, and Georgia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=WphXMwFVRf0) |
| Published | 2024/02/15 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Overview of legal proceedings involving Trump in New York and Georgia.
- Trump's case in New York is primarily about alleged falsification of business records, not hush money.
- Trump is facing more than 30 felony counts in the New York case.
- Trump's team objects to the trial affecting his presidential campaign.
- Georgia case involves allegations of a DA having a personal relationship and lying about it in records.
- Outlets are drawing parallels between the cases, implying that if one is true, the other should be considered.
- The comparison aims to show the different outcomes in how people view the events.
- Despite potential removal of the DA in Georgia, the case is expected to proceed.
- The new trial date for Trump in New York is March 25th.
- The rescheduling may impact other legal cases involving Trump.

### Quotes

1. "This is not a hush money case. That is horrible framing for this."
2. "If Trump was willing to lie in records about this relationship to cover it up, well, he's disqualified because that's what's being said in Georgia."
3. "March 25th is the new date."
4. "I think outlets are trying to draw that comparison without just coming out and saying, if it's true for one, it's true for the other."
5. "It is worth noting that by every lawyer that I've talked to who's familiar with Georgia, Georgia, even if the DA is removed, the case proceeds."

### Oneliner

Trump's legal proceedings in New York and Georgia reveal parallels in falsification of records and personal relationships, prompting comparisons and implications about accountability and justice.

### Audience

Legal analysts, activists

### On-the-ground actions from transcript

- Reach out to legal experts or organizations for insights on the legal implications discussed (suggested).
- Stay informed about ongoing legal proceedings and their potential impact on accountability (implied).

### Whats missing in summary

Insights on the potential consequences of the legal cases for accountability and transparency in political leadership.

### Tags

#Trump #LegalProceedings #FalsificationOfRecords #Accountability #Justice


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about Trump in New York
and framing, and we're gonna talk a little bit about Georgia
and parallels that it seems people want to make
and comparisons that people want to make,
but they're not making clearly.
And just kind of run through where everything is
and where it goes from here.
Okay, so in New York, the proceedings went forward today, and it was determined that
Trump's, quote, hush money case will proceed to trial on March 25th.
It is expected to last about six weeks.
Let's start there.
This is not a hush money case.
That is horrible framing for this.
That is not what this case is.
The core of this case is not the payment of hush money for a relationship.
The core of this case is the alleged falsification of business records.
And Trump is facing, I don't remember the exact number, more than 30 felony counts over
and I want to say 34.
Now, Trump's team says we strenuously object to what is happening in this courtroom.
The fact that we are now going to spend, President Trump is now going to spend the next two months
working on this trial instead of out on the campaign trail running for president is something
that should not happen in this country.
I agree he should suspend his campaign.
He obviously can't focus on it.
idea that the entire legal system across the country should grind to a halt
because somebody has declared candidacy doesn't really track. It kind of
institutes the idea of a two-tier justice system where those that have the
means to run for president, well all they have to do is keep running for president
and then they never have to face any accountability. See how weird that
argument is when you really kind of follow it down. Okay, so that's what's
going on. This case, to me, is the least impressive of all of the criminal cases
against the former president. Doesn't mean that it won't result in a
conviction. It just means that it's the least impressive. This is one that kind
of wasn't getting a lot of coverage because it was expected to come at the
end. The other cases were, the anticipation was that the other cases would go first, and by
the time this came around, nobody would care. At its heart, this is about the falsification of
records, not hush money. Hush money is just what the records were falsified about, if that makes
sense. Okay, so I've got a question that says, you know,
most outlets are covering this development and Georgia at the
same time. Why? And if you don't know what that's about, in
Georgia, the DA in that case, there's proceedings about that
and the and the relationship she I guess at this point, it has
been admitted has occurred. And they're just kind of running
through that in an attempt to disqualify her. Why are so many outlets running them
at the same time? What Republicans are accusing her of and what they are saying
is a disqualifying act for a DA's position is she had a personal
relationship and then lied about it in records. That's the summation of the
Republican case on this is that because she did this, she's unfit for office. She
can't be on this case because she did this. The New York case is that Trump
Trump had this relationship and then lied about it in records.
That's the allegation.
It's the same thing.
I think outlets are trying to draw that comparison without just coming out and saying, if it's
true for one, it's true for the other.
If Trump, in his quest for the presidency, was willing to lie in records about this relationship
to cover it up, well, he's disqualified because that's what's being said in Georgia.
That's why outlets are covering them together, I think.
I've read a bunch of those stories where they're putting the two events together.
I haven't seen anybody openly make the comparison, but I believe that's why they're being run
that way, is to kind of try to highlight the similarities in the events and then the very
different outcomes when it comes to how people view them and what they think should happen.
It is worth noting that by every lawyer that I've talked to who's familiar with Georgia,
Georgia, even if the DA is removed, the case proceeds.
Maybe the defense team is hoping to get a prosecutor who isn't as good, or a prosecutor
who maybe is more favorable to Trump.
But this is not something that would end the prosecution by literally every Georgia attorney
I've talked to, says that's not how it works.
just she wouldn't be on the case. So that's what's going on. I think that's why the comparison's
being made. March 25th is the new date. It is worth noting that this may mean that the
election interference case, the federal one, even if the Supreme Court was to decide like
today, that one may go in May now. We'll have to wait and see how this being bumped to the
front of the line impacts the other cases.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}