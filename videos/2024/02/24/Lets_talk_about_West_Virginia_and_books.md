---
title: Let's talk about West Virginia and books....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=n-zwjCJKEVI) |
| Published | 2024/02/24 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- West Virginia proposed legislation criminalizing providing obscene material through libraries.
- Legislation framed as protecting children, but standards of obscenity are subjective.
- Librarians could face up to five years in prison and a $25,000 fine.
- Real goal is to chill the spread of information and keep people ignorant.
- Aim is to make it easier to control the population by limiting access to information.
- Libraries might remove controversial material to avoid legal repercussions.
- Implication that librarians may leave the state if the legislation passes.
- Attack on education raises questions about the rulers' intentions.
- Pushback against the legislation is necessary to protect access to information.
- Legislation threatens academic freedom and intellectual growth.

### Quotes

- "If your political party wants to put librarians in prison for five years, you need a new political party."
- "The real goal is to put a chill on the spread of information. The real goal is to put a chill on libraries."
- "They want you uneducated because that makes you easier to control."
- "What West Virginia needs is more academics leaving."
- "When your rulers, your betters are consistently attacking education, you need to ask yourself why they want your children ignorant."

### Oneliner

West Virginia's proposed legislation aims to criminalize providing obscene material through libraries, ultimately threatening intellectual freedom and education while raising questions about controlling access to information.

### Audience

Community members, activists

### On-the-ground actions from transcript

- Rally support to oppose the legislation (suggested)
- Contact local representatives to voice concerns (implied)

### Whats missing in summary

The full transcript provides detailed insights into the potential consequences of the proposed legislation and the importance of defending intellectual freedom in West Virginia.

### Tags

#WestVirginia #Libraries #Education #Censorship #CommunityAction


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about West Virginia
and books, yeah, you already know where this one's going.
There's obviously something happening in West Virginia
that is against books, right?
We're gonna talk about a piece of proposed legislation
that has already made it through the House
and is on its way to the Senate.
And what does it do?
Well, it goes after librarians, criminally.
So basically, the piece of legislation
would criminalize providing obscene material
through the library.
And of course, it's being framed around, well,
we're going to protect the children.
Here's the thing, the standard of obscene.
It includes phrases when you look at the definition.
It includes phrases like applying contemporary community
standards or lack serious literary or artistic value,
things that are completely subjective, things
that the librarian would just have to guess at.
And then when they're before, going before a jury
is when they get to find out
whether or not their guess is right.
That sounds like really hyperbolic, right?
I mean, how serious could the penalties be?
Five years in prison?
If your political party wants to put librarians
in prison for five years, you need a new political party.
that simple. Yeah, it's a $25,000 fine and up to five years in prison. I feel like
West Virginia might have things a little messed up. So what's the real goal here?
The real goal is to put a chill on the spread of information. The real goal is
is to put a chill on libraries. Why? Because they want you ignorant. They want you uneducated
because that makes you easier to control. They want your children dumb. It's really that simple.
The framing of it is what's going to be used to push it, but the reality is the way this would
work is it would completely chill the libraries. Anything even remotely
controversial would be taken out for fear that a minor would get their hands
on it, and they're not going, librarians are not going to risk going to prison
for five years. My guess is that most of them, if they want to stay in that
profession, they'd leave the state. They'd be silly not to. They'd go
somewhere else because you know that's what West Virginia needs is more
academics leaving. When your rulers, your betters are consistently attacking
education, you need to ask yourself why they want your children ignorant. Anyway
It's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}