---
title: Let's talk about a weird development in satellites....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=4wmFE1BKshY) |
| Published | 2024/02/24 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Japan is leading a surprising development in satellite design by considering wood as a material due to the issue of space junk.
- Kyoto University tested Magnolia, Cherry, and Birch for satellite construction, with Magnolia emerging as the best option.
- The satellite, named Lingosat, has been designed and is set to be launched soon to test the performance of wood in space.
- Wooden satellites have the potential to address risks associated with traditional metal satellites during reentry and burning up.
- This innovative idea challenges the conventional metal satellite design portrayed in science fiction movies.
- The Japanese Space Agency is collaborating on this project, and the satellite is expected to launch on a US rocket.
- Wooden satellites may offer unique solutions to long-standing issues related to space debris and satellite disposal.
- The concept of using wood for satellites has roots going back years, with persistent ideas on countering space junk problems.
- Lingosat, the wooden satellite prototype, represents a significant step towards testing and potentially implementing wood satellites.
- The first wooden satellite, similar in size to a coffee cup, could revolutionize satellite design and space sustainability efforts.

### Quotes

1. "Wood satellites could greatly reduce risks from reentry to space junk burning up."
2. "Lingosat, the wooden satellite prototype, will test Magnolia's performance in space."
3. "A surprising development in satellite design - Japan leading with wood satellites."
4. "Challenging the norm of metal satellites with an innovative wooden design."
5. "The idea of wooden satellites challenges traditional metal designs portrayed in sci-fi."

### Oneliner

Japan leads an innovative shift in satellite design by testing wooden satellites, aiming to address space junk issues and revolutionize space sustainability efforts.

### Audience

Space enthusiasts, Environmental advocates

### On-the-ground actions from transcript

- Support research and development of sustainable satellite design by staying informed and spreading awareness (implied)
- Follow updates on the Lingosat mission and advancements in wooden satellite technology (implied)

### Whats missing in summary

Exploration of the potential environmental impact and long-term benefits of wooden satellites compared to traditional metal ones.

### Tags

#SatelliteDesign #SpaceInnovation #WoodenSatellites #SpaceSustainability #JapanSpaceAgency


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about a surprising development
when it comes to the design of satellites.
And it's been talked about for a while,
but it looks like Japan is going to be the country
to put it into action.
So we will talk about Japan, space, and wood, wood.
One of the problems with satellites is that there's a whole bunch of them, and it's space junk floating around up there.
When they hit our atmosphere and burn up, current design leaves a bunch of aluminum particles up there, and that's just
not good.  So Kyoto University decided, hey, what if we made it out of wood?
sounds ridiculous, right? I mean, it sounds really silly because every science fiction
movie we have ever seen showed metal satellites. What if we make them out of wood? So they
tested Magnolia, Cherry, and Birch. Turns out Magnolia is best for this. So they sent
up to the International Space Station. They tested it there. They found out that it worked
really well. So, a satellite has been designed. It's called Lingosat. And this satellite is
slated to go up relatively soon, and it will test and kind of measure how well wood, magnolia,
performs. And if it holds up, then we may start to see wood satellites. Satellites that, in theory,
could greatly reduce a whole lot of bad things from the risk of reentry to what
happens when they burn up. It's unique, it's kind of a cool idea, definitely
not something I ever even remotely imagined, but when I started looking into
it. You can find traces for this idea going back years because people realized
there was an issue they just didn't really know how to counter it and this
idea kept popping up and now it looks like it's going forward. The Japanese
Space Agency got involved and it looks like it'll go up on a US rocket
relatively soon. So we will see whether or not wooden satellites, I guess the
first one's about the size of a coffee cup, will be in our future. Anyway, it's
It's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}