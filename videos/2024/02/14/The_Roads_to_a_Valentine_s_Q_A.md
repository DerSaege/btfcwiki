---
title: The Roads to a Valentine's Q&A....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=EpLxHWg_-hk) |
| Published | 2024/02/14 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau and Miss Beau host a Q&A special for Valentine's Day on their show, engaging with questions from fans and discussing various topics ranging from federal employees to Valentine's Day celebrations.
- Beau shares his thoughts on the rumors of federal employees being forced back to offices, expressing uncertainty about the situation and extending the concern to contract employees as well.
- Miss Beau and Beau reveal they do not celebrate Valentine's Day, finding the idea of a specific day to demonstrate love somewhat silly and unnecessary.
- The couple reminisces about sweet moments in their relationship, reflecting on gestures of care and consideration that solidified their bond.
- Beau expresses his disapproval of Alabama's use of nitrogen gas for executions, citing cruelty and advocating for the practice to be reserved for extreme cases.
- A viewer expresses disdain for Valentine's Day and shares a regret about a past interaction with their future son-in-law, seeking advice on how to address it now.
- Beau and Miss Beau touch on the lack of hopeful messaging about love in politics and the rise of division and violence in the absence of such messaging.
- Miss Beau and Beau underscore the importance of maintaining hope and healing in a divisive political climate, pointing out the lack of emphasis on love in political discourse.
- A viewer requests a video on Susan B. Anthony's advocacy for women's rights, prompting a response hinting at upcoming content exploring various topics.
- Miss Beau humorously rejects the idea of Beau voluntarily cleaning up after a horse as a Valentine's Day gift, shedding light on their dynamic regarding ranch responsibilities.

### Quotes

1. "A calendar to dictate when you demonstrate your love for someone is kind of silly."
2. "The smallest amount of observation and consideration can go a really long way."
3. "Be prepared, not scared."
4. "Don't do anything that disrupts that for you."
5. "Little things make big differences."

### Oneliner

Beau and Miss Beau navigate questions on federal employees, Valentine's Day, and societal issues while sharing anecdotes from their relationship, advocating for hope and healing in a divisive political climate.

### Audience
Individuals navigating relationships and seeking advice on maintaining hope amidst societal challenges.

### On-the-ground actions from transcript
- Find a local wildlife rehab phone number and keep it handy to assist injured wild animals (suggested).
- Start a YouTube channel or any passion project now instead of waiting for the perfect time (suggested).
- Set limits on news consumption to regulate information intake and avoid fear-mongering sources (suggested).

### Whats missing in summary
Insightful exchanges on relationships, societal issues, and personal anecdotes provide a holistic view of navigating challenges and maintaining hope in a divisive political landscape. The transcript captures a range of perspectives from Beau and Miss Beau, offering practical advice and heartfelt moments. Viewing the full transcript can provide a deeper understanding of their dynamic and valuable insights. 

### Tags

#Relationships #ValentinesDay #Hope #Community #Advice #Society #PoliticalClimate #WildlifeRehab #ContentCreation #NewsConsumption


## Transcript
Well, howdy there, internet people,
and welcome to the Roads with Bo.
Today is our Valentine's Day special,
which is gonna be a Q&A,
and we'll run through a whole bunch of them.
We like to put out longer content around the holidays.
This may take the place of the video
that typically goes out on Thursday morning.
So it'll be in part a normal Q&A,
and impart questions for a special guest
we have with us today.
Miss Bo is just off camera,
so she will be answering some of these
that are directed to her.
Okay, so starting off, as a federal employee,
I'm hearing rumblings about everyone being forced back
to the offices.
Any truth to that, or is it just the DC people?
I am hearing the same rumblings and rumors.
I do not know.
Eventually, this will happen.
I don't know if what is being talked about now is real,
or just people talking about it.
It does extend.
The rumors extend to people who are contract employees as well.
OK.
Hi, Bo, and hopefully Miss Bo.
Do you celebrate Valentine's Day?
If so, what would you say was your all-time favorite
Valentine's Day moment with each other?
If not, why not?
We do not celebrate Valentine's Day,
and the moment that we discussed that and agreed on that
would be like the best day.
It's it's we both kind of came from the same place. It's a it's a silly day
Calendar to dictate when you demonstrate your love for someone is kind of silly. I
Think what she told me initially was that if I
If I needed a special day to do something nice then
Probably didn't need to stay together too long something along those lines
Okay
What was the what was the moment when you knew this was the right person for you?
I think I'm the person they're talking about
I
Mean that I would hope anyway the jury still might be out on that. I mean you're mr. All right
No, oh
One of the sweetest things when we first started dating we hadn't been dating long
I wanted to go out but I was too tired to go out and then a few hours later
someone you know Bo called and said hey we're going out so I got ready I left
and I was digging in my purse in the car and I was getting a little frustrated
obviously and he's like what's the matter and I said I I'm looking for my
lip gloss and he asked if we needed to stop and get some and it was like the
sweetest moment ever because at no point do I think I ever dated somebody that
paid attention or asked if they could do something for me like that so it was a
small little thing that went a really long way and I knew then it was
definitely definitely in for the long haul. Note guys the smallest amount of
of observation and consideration can go a really long way.
Okay, I was wondering what your thoughts on Alabama
using nitrogen gas to execute Kenneth Smith were.
Do you feel the cruelty is the point
for the powers that be there?
I think the apathy is the point.
I am not a fan of this practice to begin with.
In general, I think it should be reserved
the absolute most extreme situations.
And I feel like eventually this is going to go by the wayside.
Not just the nitrogen, but in general.
There's a lot of information about this and it really, it does not achieve the desired
effect contrary to all of the rhetoric.
as is typically the case, the rhetoric doesn't actually pan out.
Okay, I don't have a question, I just wanted to take this opportunity to express my general
hatred of the entire concept of Valentine's Day.
When my future son-in-law asked for my daughter's hand, I told him I raised my baby already
and she's a woman now so if she says yes you have my blessings. That was over 10
years ago and I love my son-in-law but sometimes I wish I had something more to
say back then. Is there more a dad can say 11 years later without crossing the
line? I mean it depending on the relationship I feel like it's always
kind of appropriate to provide advice. I don't I don't know how you'd recapture
that moment though. What do you think? Agreed. I think but if it's
something negative you're kind of at the forever hold your peace thing. If it's
something positive there's never a bad time to say something positive and
encouraging the family. So what do you think hopeful messaging about the power
of love has in our current political environment. Major politicians seem to avoid that word except
in very certain contexts. I feel like no one's really advocating for learning to love one another,
which is weird because that seems to me like an inspirational message that could get people
motivated. And the alternative is rising division, dehumanization, and violence.
But the people who most rely on that hopeful message, hopeful language, about love are
fringe figures who don't even run in elections with the goal of winning them because of how
fringe they are.
What am I missing here?
I mean I definitely think that messaging, it's always viable and it's always been a part
of it.
Fringe figures, that's true, and generally speaking, historically speaking, you need
to look at what the powers that be, the authorities, the establishment has done to people who carry
forth a message of love.
But I would point out that it's not as fringe as it seems at first.
I mean, sure, you look at a candidate like Marianne Williamson, who made that kind of
messaging very central. I mean, yeah, she's not winning, but she's getting points, not
fractions of a point. She's getting points in a primary that wasn't really meant to
be fought, you know? What do you think?
I think we have a lot of healing to do. There's never really a lot of love in politics, but
I think healing's first.
You can't love again without healing.
OK.
Bo, it would be nice to see a Thomas Paine type
video on Susan B. Anthony and how she helped
advocate for women's rights.
We have this type of stuff coming on this channel.
There's more.
There's definitely more in between what's
happening right now, and more biographical stuff,
there's going to be a whole lot more stuff coming out.
But stuff like this is still in the pipeline.
We're running through various formats
and seeing what works and what doesn't.
And then the additional question is for Miss Bo,
will your hubby voluntarily scoop Marilyn's poop?
That's a horse, by the way, on Valentine's Day
as a surprise gift for you.
Okay, I'm laughing at the poop part,
not so much the cushion itself, but that was funny
that it is a horse.
Absolutely not.
That's not even a question.
He will gladly take her to another pasture.
He will gladly take her and play with her,
talk to her, entertain her, whatever,
but he's not gonna like clean up after her
or take care of her.
That's not his role.
That's not what he does, so no.
That's not a thing.
Right, I've talked about it before.
I like playing with the horses, some of the horses.
I like some of the horses.
I get along with them, I love them.
I like to play with them.
To me, taking care of them, stuff like that,
that's a chore, and she actually enjoys it, so.
It's like a workout, I don't mind it.
Very different view of that kind of stuff.
What's your take on getting your wife slash girlfriend a gift well before Valentine's
Day and that being called, quote, the gayest thing?
I figure it'll be a funny one.
Yeah, I've heard something about that.
There's a whole genre of commentary and conversation right now that has turned into, hey, this
incredibly heterosexual thing, yeah that's actually not. And I just, I mean
when it comes to this whole this whole line of reasoning and and commentary
though to me if every interaction you're having with a woman as a man is really
based on how it's going to be perceived by other men, I question who you really
want to spend your time with.
So OK, I'm missing your posts on various social media,
as I shared them a lot.
There's a second account on Facebook, though.
Looks like you, but I think it's a copycat.
It's definitely not me.
Maybe that's why you got messages from Facebook saying
you weren't posting original content.
Yes, for those that don't know, the reason
we don't have a running Facebook
is because Facebook did not believe that we were us.
I even put out a video specific to Facebook
talking to the person who was going to review it,
and apparently it didn't matter.
Let's see, I've sent a couple messages before
asking if it's really you, but didn't see a reply.
So what social media accounts do you actually still have?
Getting closer to elections,
I think a bigger presence on the socials
by someone like you would really help
to get to fact-based, reasonable, logical, informative,
and sane responses to a lot of the stuff that is being
and will be slung over the next several months.
Right now, I'm not really active on any.
I really liked Twitter, but I can't use it with what it's
become and how the platform itself is now running.
I'm waiting to figure out where everybody's going to go,
and then I will re-establish a presence there.
Bo, you work too much.
If you're anything to go by, so does Miss Bo.
What country is your guy's dream vacation destination?
Me and my missus are going to Japan later this year
and are extremely excited about it.
Hers is a Cruz in the Med, right?
Yes, with something in Greece.
Mine would be Ireland, so.
You guys have quite the plethora of animals already.
What is a species you guys don't have yet?
have yet but really want or which one does Miss Bo really want but Bo still needs to be convinced
of? Oh you've got that backwards. When it comes to like species of animal that you wouldn't think
would be on a ranch it's not it's not her that it's me like like I want peacocks or tapirs and
And she won't let me have a taper or two.
I mean, fish and wildlife won't let me have a taper either.
But when it comes to things that are a little bit more
unique or out there, it's in me that wants them.
But I mean, there's probably normal ranch animals that you?
Oh, yeah, I want quite a few things.
But I definitely would love goats and chickens.
But I'm afraid that I can't contain the goats
and that it would be like safety issues and concerns for them.
And then the chickens,
I'm afraid that the other wildlife we have around here would hurt them.
And I don't know that I could, that would hurt my heart. So I just,
um, I really am sticking with the larger,
the horses and the big dogs we have. And, and I think, and the barn cats,
and I think we're good for now. That's, that's plenty to keep alive.
I go by how many heartbeats I have to keep going around here.
and that's quite a bit, so adding more is a bit much.
What is your idea of a perfect date night with Miss Bo?
Something not here at the ranch,
where there's a whole bunch of kids and animals
and other things that tend to interrupt
anything that's going on.
I know that you are really rooted where you live, and I understand that building a local
network is a vital strategy for lasting change.
If it weren't for your home and not wanting to disrupt your family's life, however, would
you have decided at some point over the past few years to just cut and run local network
or no?
I know that despite being deeply committed to the practice of teaching and being a faculty
member at a state university in the upper midwest. I'm counting the months to when I can retire and
leave this deeply and smugly authoritarian state for a far more sane and welcoming majority minority
one. So the question here is would I be doing the same thing if I didn't have a family? No, no. I
I would I would be definitely more active on the road doing I would be engaged in helping
people build networks and doing force multiplication stuff in that way.
Okay Miss Bo or Captain Miss Bo my youngest four still cannot sleep through the night.
have any suggestions. We tried a way to blanket, melatonin, sound machine, and lots of snuggles.
Every parent is so different. Giving advice on kids is difficult, but kind of the only thing I
didn't hear on that list was full bellies. I mean, I did it from the time my kids were about six
weeks old on with that little extra cereal in their bottles, just to kind of thicken it up.
firm believer in the in the full belly baby will sleep well little one will
sleep good so there's that. What else did we try? A snack before bedtime. Active, active like not
right before bed because then they're all wired but like get them real active
running around doing something and then kind of tone down the night and
eventually you'd end up like they fall asleep and they have to be teleported to
bed that might work that's all I've got okay if you lived in a different country
where would y'all live or have y'all you thought much about it and when do the
gardening and homesteading videos come out and are you considering a
more hurricane-proof option like small small walled greenhouse for next year's
gardening projects. So we put together greenhouse with acrylic panels and
everything and luckily we got footage of it because the hurricane or not the
hurricane the torn-eyed out demolished it so like all that's left is the metal
frame now but and the gardening and homesteading videos they're coming just
I feel like they've been built up so much at this point that they're going to be disappointing.
Okay, if you lived in a different country, where would y'all live?
Ireland.
Yeah.
Ireland or some country where the central government is so weak that if need be, it
could be realigned easily.
Okay.
What is the kindest experience you've had on Valentine's Day?
I don't know.
I've personally had something that I remember.
But what I do love about Valentine's Day is like with my little kids, with my kids.
And they go to class and everybody shares valentines and everybody they make their little
boxes or their bags and they decorate them and they exchange valentines.
It's their little ice cream socials.
I always think that's so fun and so cool and it's kind and everybody's involved and they're
kids so they're innocent and they just enjoy it.
Someone thinks I'm over the moon, you know, it's great.
Our family is same sex, mixed religion with an adopted child.
The political climate has been causing increasing fear and anxiety within our family and my
My spouse has begun looking at international job listings as well as domestic.
Our families think we are drastically overreacting, but we are very concerned about our future
should Trump be elected again.
At the very least, we are trying to get out of the South.
Do you have any thoughts or advice?
You're not overreacting.
When you're talking about your safety, when there's doubt, there's no doubt.
When people hear this, they look at it in one way, is that really necessary?
The other way to look at this is you're doubting whether or not your family will be safe.
There's no doubt over that.
is a feeling that you have. Planning to go somewhere else to alleviate that feeling,
there is nothing wrong with that. I would never advocate or say that somebody is overreacting
when it comes to getting their family somewhere where they would feel safer. There's even
just the feeling alone is detrimental. Like just the feeling that you might need to go
somewhere else is detrimental. So if you have the resources and you have that feeling, yeah, I mean
do it. That would be my my advice or at least come up with a really good plan to do it.
What is your favorite comfort food? That green bean thing, right? Mine, the green bean casserole, yeah.
Yeah.
Yours is Oreos.
Yes.
Oreos.
And use a fork.
If you don't know, stab them with a fork
to put them in the milk.
Anyway.
Stab them and drop them.
That's right.
Stab them and hold them under the liquid
until the bubbles stop coming up.
And when you say it like that, it sounds like,
doesn't sound like you're talking about cookies.
OK.
Like many others, I am trying to balance my increasing anxiety at the state of our country
with day-to-day living.
I am finding it more difficult to stay focused on creating positive change, doing my day-to-day
living when it seems like we have bigger issues to worry about, and wondering if I should
be preparing for worse-case outcomes.
I'm finding it more difficult to continue to reach across the aisle, or across the street
in my case, to people who vote for candidates that seem bent on destruction and hate, even
when those people seem to be otherwise good intentioned.
I know I'm not doing anyone, including myself, any good if I've become too chaotic and angry,
but I am definitely finding it harder to maintain this position.
How are you balancing these issues yourself, and what is your perspective on realistic
levels of fear, anger, and worry.
Okay, again, wondering if I should be preparing for worst case outcomes.
This goes back to the previous, or I guess two questions back.
Be prepared, not scared.
If you have a plan, you're more comfortable.
Just the feeling alone is a detriment.
All of this same stuff applies.
As far as continuing to try to reach across the aisle,
reach across the street, there's a famous quote,
never argue with somebody that blank would have blanked.
Just remember, there's a point of diminishing returns.
That saying isn't really about, it's not the way
that it comes off, which is like hyper militant.
It's really more don't waste your time.
There is a point where continuing a conversation with people who are bent on destruction and hate, it only wears you
down.
So while I, as somebody who is just more stubborn and very dedicated, more so than they could ever be, it doesn't bother
me.
If it starts to weigh on you, yeah, you don't have to do it.
You have to remember that in order to build a better world,
you have to be in a better place, too.
So don't do anything that disrupts that for you.
I am currently taking a course on environmental sustainability
for my environmental science degree
because of having that subject matter on my brain lately.
And I know that you have previously
expressed a lot of concern for the environment.
My thoughts have drifted to how you might manage your ranch
to keep its operation sustainable.
Do you have any details on this that you
are comfortable sharing?
I personally think the biggest thing we do
is something we don't do, actually.
We don't utilize all of the land.
The amount of forest that is maintained,
it's carbon negative.
The ranch is carbon negative.
We could add tractors running 24-7,
and the ranch would be carbon negative.
When new stuff goes in, to me, that's the biggest thing,
is maintaining large sections of undeveloped dirt.
That's a big thing.
On top of that, when new stuff goes in, it's solar.
And eventually we'll get to where we have everything
retrofitted and the whole thing is renewable.
And then as far as the horses themselves,
what do you think?
They maintain the cleared area as well in the pastures.
And so that cuts down on a lot of other sources being used.
We also, we get our hay locally from people around us, so we don't need a lot of that
because we have grass like nine months out of the year.
We don't, our grass doesn't go dormant for very long, so again, it sustains itself, the
small things like that.
Okay, yeah, I would say, yeah, and you're right.
Not having to run stuff to maintain and just letting the horses eat it.
That's another thing.
I never really thought of it that way.
You mean the little things.
Little things make big differences.
That does.
Okay, let's see.
Because, okay, let's see, there's not a chance I'm going to let pass by Ms. Bo.
First I missed the little Twitter conversations you two had, especially on the day he managed
to sneak in Maryland and he had no idea, or pretended he had no idea, I don't care, it
was funny.
I know what you're talking about and I was not aware of, anyway.
Second, what is Bo's most annoying habit you've accepted over the years but still
have trouble with from time to time? Things that make you go, ah, why do you
assume I have an annoying habit? I am obviously perfect in every way.
Practically perfect. I'm like Mary Poppins. No, you're not. What's funny to me is what have you
accepted? When you hear my tone change, when I'm explaining this to you, you're
You're going to understand I haven't truly accepted it, but I do live with it.
My biggest pet peeve with Bo, like something he does that just irritates me so much is,
yeah, grab the coffee cup, grab the coffee cup.
See he even knows, if he will leave his coffee cups, a water bottle, any beverage that he
has had, it will always have two sips in it.
That's it.
ever drink it. But not only will he never fully drink it, he will leave it wherever
he left it. It will never make it to the trash can or the sink. Two sips of
something everywhere. All the time. And obviously for me the most annoying habit
that she has that I have had to grow to accept over the years is she always picks
up my coffee cup before I'm done with it and moves in. It sits there for two days. It does
not sit there for two days. I would never leave it there two days, but it sits there
long enough you're done. Okay, and then with... Can I also just, it was Charlotte that I snuck
in. Marilyn, I did the stocking thing with. Yeah, yeah. For those that don't know, my
wife takes rescue horses or horses. They're not even rescued sometimes. Horses, they need
a home. And I'm not always informed of what's going on when they arrive. And one day I'm
out doing something on the property and there's just an extra horse.
And I sent her a message over Twitter because I knew she was on Twitter and she responded
by, like, I don't know what you're talking about.
That's obviously not really a horse.
The heat's just gotten to you or whatever because, you know, that's how we roll.
Okay, what would be a way to set expectations in finding a committed partner when that hasn't
really been modeled for you during your youth in a way which inspires, even if it's hard?
What do you think?
It's hard even if you have had the example set, but I think the most important thing
to do as an individual is know yourself very well as to what you cannot tolerate or have
in your life and what you can't live without.
And keep that in mind as you're dating somebody and you're going through life.
Because you have to remember, marriage and dating in a future together with a significant
It's a marathon. It's it's not a sprint
So there's going to be things that go up and down and that you don't always like or that
And people are going to change it but that those hard lines that you can live with that, you know
You know are acceptable and the hard lines that are absolutely not acceptable to you
The other thing that I would
Kind of acknowledge is especially if you haven't seen
committed relationships over time just remember that they're not always they're
not always roses you know things aren't always perfect don't set the expectation
that everything is going to be fine and that your your partner is going to be
perfect and that over time they will totally learn to move that coffee cup
because they won't. They won't. And just learn to accept those things and know
what, know where those lines are. Like she said, know what you want and what you
you absolutely don't want and find that middle ground but accept that within
that range there's going to be ups and downs.
It's a wide range.
Do you think Trump sends a Valentine's cards and gifts to himself?
I have no idea.
See, you know, it's one of those things like when somebody asks a question like this, it
sounds like an odd question, but you don't know because when you really think about that
guy, he has a lot of very strange behavior.
Like beyond the political stuff that I talk about, you know, all the stuff that I just
tend to gloss over because it's not really news, but if you really just look at his behavior
it overall, he has some odd behavior and we'll just leave it at that.
Why do some right wingers think the military would help Trump with a coup?
Most military detest Trump.
That's a product of a vocal minority.
So there are a lot of the right wing, they look at veterans and they equate veterans
with active duty military because that's, you know, where they came from.
And a lot of veterans are in support of Trump and that's how it appears, but
what it really is, a lot of vocal veterans, people who have, not every
veteran makes being a veteran their identity, those who are very open and
upfront about being a veteran, they tend to lean more towards
the nostalgia, which then means that they lean more to Trump. So
it's one of those things where what they see, and what they
associate, isn't really representative of the whole. So
there's a, there is a misappraisal of how much support
And a lot of them who hold this opinion, I would point out even most veterans who support Trump, they don't actually
want to coup.
And most of them don't believe that the military would participate in that because they understand how the military
works.
They understand the systems that are in place to stop that, the safeguards.
And it was demonstrated, those systems are resilient.
They got a bounce check, and it worked.
So OK, how does Miss Bo find time
to take care of kids, animals, work, and you,
and still be somewhat calm?
What makes you think she's somewhat calm?
What video?
I'm going to need a citation on that.
What video that I put out led you to believe
that she was calm?
No, I'm joking.
What?
Extremely calm.
Okay, so, well, how do you do it?
Well, for one, you're a full-grown adult person.
You take a lot of care of yourself.
I mean, I don't have to do that much.
Just pick up your coffee cups every now and then,
things like that.
Work, well, I have stepped back from full-time nursing,
So I do have more time but I do dedicate quite a bit of time behind the scenes to TFC and
with the crew a lot.
And then yes, the kids, the farm, all of it, the ranch, all of it works together.
So yeah, long busy days but it all balances out.
Okay.
How does a family marriage relationship not just survive but thrive in this world with
with so many horrors pulling our attention outward.
How do we speak about hope for the future,
near and distant?
How do we talk to children about this weird now?
I personally would think that a family marriage relationship
would provide motivation and support.
You need support and sometimes you need motivation
to continue to stay in the fight.
So that would help maintain a positive outlook
about the future.
How do we speak about hope for the future?
You have to maintain it.
You have to have that hope yourself.
How do we turn down the volume without being clueless?
So much fear.
That's information consumption.
I mean, there's little things you can do.
regulate, set your news consumption
to where you do it in the morning,
do all of it in the morning or all of it in the evening.
But I mean, the big thing you can do is avoid outlets,
avoid sources that intentionally sensationalize,
that play to your emotions, that fear monger.
Avoid all of that, and you'll be,
you can stay informed, you won't be clueless,
and you won't have your emotions being poked the entire time.
That'd be my suggestion.
Yeah, definitely dial back on some of the social media
and the outrage and things like that.
And find a calm, soothing voice that explains
things and reason and logic.
I personally, I watch this guy, Bo of the Fifth Column
on YouTube.
That's my best advice for you right there.
Very rational, doesn't tell me what to think,
kinda gives me things to think about,
and covers the topics that I can go and research myself.
So it helps a lot.
That was smooth.
You're welcome.
Okay.
Little bit of a request instead of a question,
but to celebrate the day of love,
would you be able to make a little PSA
on behalf of wildlife rehabbers?
I'm an experienced rehabber that focuses on possums,
and I know the season is coming.
I would love it if people could look up
a local wildlife rehab phone number
and just keep that info handy,
as we get a lot of calls from people
who don't know where to call,
who don't know where to call
when they find a sick or injured wild animal,
and we get the animal too late.
Just a quick search, find a local number and keep it handy.
Rehabbers network, so an owl person probably knows a fox person or an or a possum person
That's a funny
Possum person that's fun to say
So calling any rehabber may help thank you and all my love and respect for this wonderful channel
Yeah, so there you go the information went out if you want to clip this use it Okay
Ok, Mr. Bo occasionally talks about letting Mrs. Bo have her way with strange animals
coming to the ranch.
I'd be curious to hear from Mrs. Bo what Mr. Bo isn't telling us about what he gets
away with.
Well, again, as noted with the horses, I would like to point out that, you know, she gets
away with the animals coming because it's like she does all the work so you
don't get a whole lot of say in something that you don't you know
actually do anything with except play with so okay so what do I get away with
yeah on that trees trees trees trees fruit trees trees I have lots of trees
Yeah. How is that getting away with anything?
I have a new load of trees.
How many are out there?
So right now there are trees out there that haven't been put in the ground.
There's one, two, there's ten.
There's ten actual fruit trees out there that haven't been put into the ground yet.
the ground yet, but they will. It's a matter of time. I didn't know that that was something
I got away with. Before they bear fruit. Cute. Does Miss Bo help with the channel? Does she
have her own? She does help with the channel and she does not have her own. How did Mr.
and Ms. Bo meet.
So I was working and she came by and was getting some photos and stuff and I was interested
in she basically there was an order that was for her and it had stuff in it that was needed
to be framed and stuff like that.
And I had the number to call.
So I called and I wasn't too forward or anything like that, but I definitely had, you know,
the voice of, you know, how are you doing?
And the woman on the other end was incredibly receptive.
And we sat there and started flirting for a little bit, at which point, after I don't
know, maybe five, six minutes of this, she was like, I'm pretty sure that you're actually
trying to reach my daughter and then gave me her phone number so I flirted
with her mom for a while first that's there's that and so I called her number
and she was actually sick and she answered the phone with something like
I'm on quarters and she just didn't want to talk to anybody at that moment but I
I told her that the stuff was in, but I set it aside.
I didn't frame it or anything like that,
because I figured I would get a chance
to talk to her when she came by to pick it up.
And when she showed up to pick it up, I started framing it.
And she was like, you couldn't have already had this done?
I was like, wow, this is not going according to plan.
And I actually said, well, I was hoping
to have a minute to talk to you.
And she's like, yeah, how's that working out for you?
OK, I practiced what I preach.
I accepted the answer no.
So that was the end of it.
And then as she was walking off, she looks over her shoulder
and she's like, well, you have my number.
And I was like, well, OK.
So for whatever reason, I called her again.
And yeah, I called her and it took a while because we were both pretty busy and eventually
I think our first real date was you bringing me coffee at work.
And yeah, it just it went from there.
In my defense, though, I did feel bad and I realized I was being a very nice person.
So that is why I told him he could call me.
I never expected to hear from him, never.
I've been happily married for many years, but my spouse has less progressive views and
is downright conservative on some issues compared to my leftist beliefs.
Over the years, we've found common ground, but some topics are verboten since I know they'll
spark an argument.
Any suggestion on navigating hot button issues when you understand your spouse's reasoning,
completely disagree with their conclusions. I don't know, that's hard. I mean, this is
when you've been happily married for many years. Yeah, I'm gonna be honest. If it's
not broke, don't fix it. You know, when you're talking about stuff like that, it's a little
bit over time, slowly but surely addressing things and talking about it, I wouldn't look
for something to create a drastic change.
I would slowly try to do it if you're going to.
Here's a morbid one.
Do you have a replacement or someone to cover for videos?
Did you get sick, or need to take a break, or does the column collapse?
We are actively looking for somebody to kind of get into that role.
Somebody who wouldn't mind being in front of the camera.
To get even more morbid than that, if I was to, you know, no longer be here tomorrow,
y'all would actually still get videos periodically.
Obviously not as often as you get them now, but over time, every few months, something
would pop up.
Part of that planning thing.
What is the most romantic thing I've done for you?
Definitely the day you gave me the moon rock necklace.
was incredibly sweet and very romantic and had a great opportunity in time in
my life when I needed a little something. And then you also gave me a confusing
Russian novel. That's right, a confusing Russian novel. You're not gonna explain
that at all. No. Refusing to elaborate further. Okay, all right, moving on. As
As someone with a bunch of kids, how do you think, what do you think about the decision
to not have children given the state of the world, climate change, political upheaval,
etc. etc.
Like I hear this most when you're talking about climate change or something like that
and the idea is that it's a species ending event.
I would say that not having kids is also a species ending event if you're talking about
from a societal level. What do you think? I agree but you know that the choice to
have children is every individual's choice. If you want to have children, have
children. If you don't, don't. If you don't, please don't. Right, right. That's
that's it. That's one of those other things. If there's doubt, there's no doubt.
Okay, if you don't want it, if you don't want kids, don't have kids. Okay, dear
Mrs. Bow, what attracted you to Mr. Bow? What keeps you in the partnership?
Well, I mean, you just heard the awesome story about how we met.
I mean, he's very patient and charming and kind.
That's how he is all the time, even when I'm mad about coffee cups.
And that's what keeps you in it.
he's supportive and again you have to remember what I said earlier about marriage is a marathon.
It is not a sprint.
There are going to be hard times.
There are times when I am the one who is patient and kind and supportive and charming I'm sure.
And there are times that it's his role and it's what keeps us going and together I think.
What is a show or film you've been able to enjoy together recently?
Me and my partner are going back to Watch Hilda if you need a recommendation.
We just rewatched The Sopranos and that movie, The Many Saints of Newark, which I had not
seen before.
Prelude to Sopranos.
What do you plan to do with your Valentine's Day this year?
This?
It's Wednesday.
It's Wednesday.
It's Wednesday.
It's really not.
We're a Halloween family, not a Valentine's Day family.
Bo, do you love your dogs as much as they love you?
If so, what are you getting them for Valentine's Day?
They get in steak.
So that's really more for Baroness.
Baroness is the eldest German shepherd.
She is way beyond her expectancy by years.
So at this point, she gets human food every once in a while, gets to lead her best life
at this point.
It doesn't get asked to do a whole lot.
I want to reiterate that the Community Network videos are my favorites and, in my opinion,
the best ones you publish because they give me hope.
Is there an interesting space story you haven't had time to talk about?
No, actually, not really.
Not one that's just coming to mind right now.
I'm sure as soon as I stop filming this I'll think of something.
So right now, no, this may be a silly question, but where would you and Ms. Bowe go on vacation
if you had the time?
So that sounds more like with the family.
Oh yeah, I'm with you though, I still want to do like a Route 66 drive and look at all
the historic sites, the well-known things, go camping, do basically education on the
take all the kids and yeah that's yeah I want to do that I want to do route 66
like with or without the kids to be honest like that that's I want to do
that and do that sometime soon okay Bo I keep hearing you say European
security is American security can you explain this in a little more depth yes
because I'm getting a whole bunch of questions about this there's gonna be a
a whole video on it, probably on this channel.
It'll get really in-depth.
But for a shorter answer that we can go over now,
the basis of American national security is NATO.
A whole lot of NATO is in Europe.
If there is instability in Europe,
countries in Europe get drawn into it.
Some of them will be members of NATO.
Therefore, the United States will get drawn into it.
Hope that made sense.
It's a chain.
Europe and the United States are tied.
And there's no easy way, even if you wanted to,
to untie that knot.
Okay.
Getting married to my partner of over eight years
this upcoming summer.
Any tips on maintaining a strong and healthy relationship
as we move into this next phase?
Eight years?
you're probably doing fine. I mean, I wouldn't worry about it too much. That's one of those
things that, I mean, eight years. The only advice I would have is whatever small coffee cup type
issues that you have, don't expect marriage to fix them. That would be like my only advice. If
you have been together happily for eight years, I mean, even happily most of the time for eight
years and you're now getting married I you're you're good just maintain the
communication and and the activities that that have kept you together for eight
years don't let it slip you know what do you think yeah I think a mile marker in
the marathon the wedding is a just another step you're doing great
Valentine's Day is my birthday, no question, just wanted to thank everyone for taking each
other out for dinner, giving cards and flowers and chocolates, etc. all in honor of my big day.
Sounds good, happy birthday. Yeah, okay. Miss Bo, did you grow up on a farm or ranch? I'm a
lifelong townie and a mom who'd worked full-time and I can't fathom how you manage a husband,
lots of kids, animals, and nursing. We had cats and dogs and other little critters but
nothing big and no ranch. I'm still missing you on Twitter.
Manage me. Okay, so did you grow up on a farm or ranch? No, I absolutely did not.
traveled a lot and the only animals I had growing up was a schnauzer and a
chow and a hunting dog and like one at a time. So no I just just series of life
choices and paths and here's where I am it's okay Bo and Miss Bo what if any
special things, public friendly, do you do for Valentine's? Do you take time for
just the two of you and if so are there special favorite activities that you do
together? It's the big thing we do for Valentine's. Well we really don't do
anything just you and I, we do things for the kids. Yeah. She puts it puts together
like Easter baskets but they're valentines for the kids yeah yeah and so
they get those in the morning okay I'm a happily married I'm happily married and
met my wife through a traditional method friend of a friend at a friend's
barbecue. I am out of touch with the dating scene since, you know, I'm married
but I do read a lot of lonely on Reddit because one day in the future when I have
time I'll do my own YouTube channel to hopefully help out. Instead of a question
I wanted to get your thoughts on the current dating landscape and finding
someone who has similar values. My opinion on the aspects of the current
dating scene, and then gamified dates through dating apps, more isolation because of technology
has made it easier to accomplish daily tasks without social interactions, loss of identity
for masculine men overall, inflation and cost to date, overcoming issues relating to mental
health and the aversion to interact with non-green flags because of quick judgment that they
or on the other side.
Those were loaded topics, but I like that you offer a more balanced view so I'm not
drinking my own Kool-Aid.
So I mean all of these things are definitely topics for a channel and some of them are
definitely issues.
I don't know about the aversion to interacting with non-green flags.
I mean red flags are red flags for a reason.
I think there might be a tendency of people
to accept other people's red flags as their own.
But that's one of those things that I kind of agree with.
When people are like, this is a behavior that I just cannot
endorse, I don't endorse it.
So there's that.
But I think, for me, the one of these,
The topic that I would find most interesting
is the way that technology has made
it easier to engage in those daily tasks,
so there's more isolation.
We have instant communication and absolutely no connection.
So I think that would, if it was me doing it,
that's where I would focus.
But the whole message overall, the thing
that actually jumps out at me, has nothing
to do with your question.
I do read a lot of Lonely on Reddit
Because one day in the future, when I have time,
I'll do my own YouTube channel.
No, you won't.
Do it now.
There, you'll never have time to do it the way you want.
Start.
Just start.
10 minutes a day, you know, whatever.
Just start, or you won't.
That's the part that jumps out to me.
OK.
Miss Bo, please tell us about your horses.
I miss seeing your sassy exchanges with Bo on Twitter
because I don't really look at the platform anymore.
Well, you're not missing much.
We're not on Twitter very much either.
So, all right, sassy exchanges.
Tell them about the horses.
What are the horses doing?
Tell them about the ranch.
Well, the horses have been really good lately.
No shenanigans that I'm aware of.
We did put a cross fence in
so that Marilyn can have somebody with her,
but not with her because she's not a nice,
nice young lady to other horses.
She wants to be in charge and I have two older mares
that are not gonna accept that kind of behavior
so she has to have her own area.
But she does seem to have a budding friendship
with the younger German Shepherd Zonya.
She runs the fence with her and kind of bucks
but wants to look down and see her.
She doesn't seem frustrated.
She seems like she's interested in her.
So we'll keep an eye on it and see how that goes.
But they're all healthy.
They're doing well.
Even my older gal is doing great.
So even the weather's getting better for them.
So we'll be moving around doing more.
Okay.
So yeah, I guess that's it.
And for those who may not have got that,
If you don't know, horses are 1,200 pounds on these little tiny teeth.
They try to hurt themselves at times.
They are goofy.
Their personalities, a lot of them are kind of goofy, and they can injure themselves.
So that's what that's about.
Okay, but that's it.
Those are all of the questions that we have here.
So I hope that helped.
Y'all hang out and everything.
So there's a little more information, a little more
context, and having the right information
will make all the difference.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}