---
title: Let's talk about Mr. Rogers, commercials, and themes....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=zvskx9pds_o) |
| Published | 2024/02/14 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Reacting to a commercial aired during the Super Bowl that featured imagery of feet being washed, with a Christian message portraying Jesus washing feet.
- Surprised by the pushback from right-wing Christians who were upset by the commercial, particularly for showing a Christian in a subservient role.
- Explains the significance of the foot-washing imagery in the Bible, particularly during the Last Supper, where Jesus washed his disciples' feet.
- Emphasizes that the act of washing feet symbolizes humility, servanthood, forgiveness, and love, which are core teachings of Jesus based on love.
- Criticizes Christians who fail to understand the message of love, humility, and forgiveness that Jesus preached.
- Draws parallels to a similar symbolic act of foot washing by Mr. Rogers in a scene related to desegregation, showcasing service and love.
- Encourages Christians to focus on the actual teachings of Jesus rather than following potentially misguided interpretations from pastors.
- Challenges the notion of portraying Jesus as a capitalist and urges individuals to reexamine the Bible's teachings on wealth and service.
- Concludes by suggesting further exploration of Jesus' teachings and the importance of understanding his message of love and humility.

### Quotes

1. "Jesus was not a person who was advocating for subjugation of people and forcing them to your will."
2. "It's an act of humbling and being a servant and forgiveness and that one element that's always missing, love."
3. "The only message being sent is that there is too much hate and if you're really a Christian, you would humble yourself, forgive, love."
4. "It's just a thaw, y'all have a good day."

### Oneliner

Beau addresses the controversy surrounding a Super Bowl commercial featuring foot-washing imagery, urging a focus on Jesus' teachings of love and humility rather than misinterpretations.

### Audience

Christians, Activists

### On-the-ground actions from transcript

- Read the Bible by yourself, focusing on the actual teachings of Jesus rather than relying solely on a pastor (suggested)
- Reexamine Luke 18:25 and other passages related to wealth and service in the Bible (suggested)

### Whats missing in summary

The full transcript provides a detailed analysis of the controversy surrounding a Super Bowl commercial, delving into the symbolic significance of foot-washing imagery and urging a deeper understanding of Jesus' teachings on love and humility.

### Tags

#SuperBowl #Controversy #Christianity #JesusTeachings #LoveAndHumility


## Transcript
Well howdy there internet people, it's Bo again. So today
we are going to talk about a commercial. We're gonna talk about a commercial that
came on during the Super Bowl, the reaction to it, and the lesson in
it. Because it took a surprising turn.
When I first heard about it and saw it, I assumed that I would be talking
about this, but I assumed I would be talking about it for very different reasons.
I did not anticipate the pushback that arose.
Okay, so if you have no idea what I'm talking about, missed it.
During the Super Bowl, a commercial was aired, and that commercial had a lot of imagery of
feet being washed. It said that Jesus didn't preach hate, Jesus washed feet.
This is obviously a Christian message and from the imagery in it, it to me seemed
pretty obvious that it came from a right-leaning organization. So I was
pretty surprised to find out that right-wing Christians were upset by it.
I'm guessing they did not understand the message or the theme. So first to get
into this we have to talk about some of the some of the reasons people were
upset and overall it's because it shows the the Christian in the imagery in a
subservient role, you know, washing feet. And there were questions about why it's
expected like that because Jesus said that he was setting an example like it's
in the book. For those that don't know, for those who aren't Christian, for those
who have never heard the stories, never read the Bible, never saw the Da Vinci
code. It's important to understand that this is not some obscure lesson from the
Bible. This happened at the Last Supper. It's a big part of the
teachings. And basically during this time, walking along the roads, sandals,
your feet get dirty. It was common to have a servant wash your feet. Basin of
water and everybody's feet would be washed, Jesus takes the role of the servant and washes
the other's feet.
He does this, you know, it's an act of humbling and being a servant and forgiveness and that
one element that's always missing, love.
And he says that he's setting an example.
It's important to remember that the actual teachings of Jesus are, they are based on
that missing element.
They're based on love.
That's what's there.
And Jesus was not a person who was advocating for subjugation of people and forcing them
to your will.
The idea was to set the example and draw people that way.
This is apparently a lesson that has not been learned by a lot of Christians today.
This is not a new thing as far as social progress and as far as setting a tone for the country.
The only message being sent is that there is too much hate and if you're really a Christian,
you would humble yourself, forgive, love, use that missing element type of thing.
The last time feet washing entered the political discourse in this way, it was because of Mr.
Rogers, the scene on the shirt. See, segregation was over, at least on paper,
wasn't really in real life. Mr. Rogers, there was a scene where he and the cop,
they put their feet in a kiddie pool, and a lot of the tension was about pools
being desegregated, and that was there, but it's also worth noting that Mr.
Rogers tried his feet, washed his feet. That was the last time it entered. Again,
an act of service, an act of love, it would be great if those people who
pronounce themselves as Christians read a little bit more, followed a little bit
more closely the teachings of Jesus rather than maybe the teachings of a pastor who may
not necessarily have, who may not necessarily be teaching the right thing.
It is worth noting that Jesus did this, washed everybody's feet, and said that He was setting
an example showing that service right before He announced who was going to betray and abandon
Him.
Now as far as some of the questions that came in, I got one in particular that was basically
hey, you know, I know you're kind of a lefty but portraying Jesus as a leftist in this
way, doesn't this bother you?
I mean a leftist is somebody who is opposed to capitalism.
That's really what it boils down to.
That's the defining line there.
Do you believe that Jesus was a capitalist?
I would start with that question.
And if you do, maybe look into the whole parts with the money changing and maybe look at
Luke 18.25 and see what's said about camels and needles and rich men in heaven.
If you are under the impression that Jesus was a capitalist, I'm going to suggest that
you need to read the Bible by yourself, without your pastor.
And really kind of zoom in on the actual teachings of Jesus, because I've got a feeling you're
about to be really surprised.
Anyway, it's just a thaw, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}