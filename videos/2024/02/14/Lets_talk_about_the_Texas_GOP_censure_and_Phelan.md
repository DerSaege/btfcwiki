---
title: Let's talk about the Texas GOP, censure, and Phelan....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=CMUclshqKb4) |
| Published | 2024/02/14 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- GOP executive committee in Texas voted to censure Dade Phelan, the Speaker of the House, 55 to 4, based on party rules.
- Censure is speculated to be related to Phelan's involvement in the impeachment of Paxton in Texas.
- Speaker's spokesperson accuses Texas state GOP of associating with neo-nazis and losing moral authority.
- In-fighting in the Texas Republican Party is between normal conservatives and ardent authoritarians.
- Republican Party's cohesion has disintegrated into multiple factions with varying ideologies.
- The split within the Republican Party is between conservatives and far-right authoritarians, especially pronounced in red states.
- Politicians in red states have adopted extreme positions, moving away from traditional Republican values.

### Quotes

1. "The executive committee has lost its moral authority and is no longer representative of the views of the party as a whole."
2. "In-fighting within the Texas Republican Party is based on whether or not you're a normal conservative or you're an authoritarian."
3. "The Republican Party of today is not all Republicans. Some of them are far-right authoritarians."
4. "Politicians adopted positions further to the right, and now they're no longer even really Republicans."
5. "There's gonna be more of this in various states."

### Oneliner

GOP in-fighting in Texas reveals a split between conservatives and authoritarians, with red states seeing the most pronounced shifts to the right.

### Audience

Texas Republicans

### On-the-ground actions from transcript

- Contact local Republican groups to understand their stance and influence change (suggested)
- Stay informed about the political climate and shifts within the Republican Party (exemplified)

### Whats missing in summary

The full transcript provides a deeper insight into the internal turmoil within the Texas Republican Party and hints at potential future divisions within other states' GOP.

### Tags

#Texas #RepublicanParty #GOP #In-fighting #Authoritarianism


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Texas and the speaker of the
house out there in Texas, the GOP executive committee, and the just
never ending downhill slide of the clown show that is Republican
state party politics.
Okay, so the GOP executive committee out there, they voted 55 to 4 to censure Dade Phelan,
who is the Texas Speaker of the House member of the Republican Party.
They did this in theory because there's a portion of their rules that says that if he
violates the fidelity or the priorities, I guess, of the Republican Party of Texas
three times that they can do this. Why are they really doing it? It is widely
believed because, you know, that whole impeachment of Paxton out there in
Texas. That's what they think it has to do with. So they put together their
event, they cited five instances of him breaking with the party, they have censured him.
The spokesperson, Kate Whitman, now this is the spokesperson for the speaker, it doesn't
appear that they're going to take this just passively, it seems like there might be a
a little bit of a show to come because the spokesperson came out and said some
very interesting things about how the Texas state GOP like rolled out the red
carpet for a group of neo-nazis quote and there's more there's actually more
it goes in depth into a whole bunch of things that I feel like are going to
become common talking points and goes on to say that the executive
committee has lost its moral authority and is no longer representative of the
views of the party as a whole. So what's happening in fighting within the Texas
Republican Party? What is it based on? It's based on whether or not you're a
normal conservative or you're an authoritarian. An ardent authoritarian. I
I know most conservatives are authoritarians to some degree, but that's really the basis
of this.
It is a divergence of ideologies.
For a long time, the Republican Party was very cohesive.
There are multiple factions now, and for a lot of people, they don't really know that.
are a lot of Republicans. They don't really understand that the Republican
Party of today is not all Republicans. They're not all conservatives anymore.
Some of them are far-right authoritarians. This is that split showing
and becoming very, very clear. There's gonna be more of this. There's going to
be more of this in various states. Most of the time this is going to occur in
red states because that's where it's going to be most pronounced. In swing
states or in blue states, the Republican Party that exists there, it tends to just
still be mostly just conservatives. In the red states, in order to differentiate
themselves, a lot of politicians adopted positions that are further and further
to the right, and now they're no longer even really Republicans. That's what's at
play. We will see this again. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}