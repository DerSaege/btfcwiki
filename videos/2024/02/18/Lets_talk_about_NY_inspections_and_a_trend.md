---
title: Let's talk about NY inspections and a trend....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=RA6VD56-cto) |
| Published | 2024/02/18 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Mentioning a chain of events possibly connected to developments in New York involving the fire department.
- The Federal Bureau of Investigation and the city's Department of Investigation are looking into the fire department, specifically focusing on a couple of chiefs.
- Allegations suggest that individuals in the fire department expedited inspections rather than passing failed ones.
- The focus is on the timing of inspections being expedited and not on passing inspections that shouldn't have passed.
- Public integrity standpoint is concerned about potential corruption leading to other illegal activities if money is taken for one thing.
- Allegations indicate a significant sum of hundreds of thousands of dollars being involved.
- No individuals have been accused of any wrongdoing at this point, and investigations are ongoing.
- Recent arrests related to a kickback scheme within the housing authority last week are also part of the larger picture of uncovering corruption.
- Anticipation of more arrests and uncovering of low-level corruption in the upcoming months.
- Expectation of further developments and investigations into perceived corruption in New York.

### Quotes

- "If you're gonna take money for one thing you might take money for something else."
- "As far as the FBI is concerned, that's not going to matter."
- "I don't anticipate this being the end."
- "Generally speaking this stuff kind of comes in waves to send a message."
- "There are probably more developments dealing with perceived corruption, alleged corruption in New York."

### Oneliner

The FBI and city investigators probe alleged expedited inspections in the New York fire department amidst wider corruption concerns.

### Audience

Investigators, concerned citizens

### On-the-ground actions from transcript

- Stay informed about the ongoing investigations and developments in New York (suggested).
- Report any suspicions of corruption or illegal activities to relevant authorities (implied).

### Whats missing in summary

Full context and detailed analysis of the chain of events leading to investigations in the New York fire department.

### Tags

#Corruption #NewYork #FBI #Investigations #FireDepartment


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about New York
and the fire department and a chain of events
that may or may not have started a week,
a little more than a week ago now, that we talked about.
They may be connected, they may not be,
but it is a, it's a development
in what is probably going to be a chain of developments.
OK, so let's start with the current news.
The Federal Bureau of Investigation,
along with the city's Department of Investigation,
they're looking into the fire department.
Specifically, it looks like they're looking
into a couple of chiefs.
And it looks like the allegations
are that there were people who were with the fire departments
that were expediting inspections that the fire department had to do before buildings got cleared
and projects got moved along, that kind of thing. I do think given what happened over the last,
you know, 48 hours or so during this period when this news broke, I feel like it's important to
note, all of the allegations I have seen suggest that they expedited inspections,
not that they passed failed ones. That is, as far as the FBI is concerned, that's
not going to matter, but I think that's an important distinction to make given
the fact that like a dozen people were just injured in a fire. All of the
allegations, it seems to be more about timing than things getting passed that
shouldn't. From a public integrity standpoint, when you're talking about the
reason the feds and the other investigators are interested in this is
because one thing can lead to the next. If you're gonna take money for one thing
you might take money for something else. That's why they're interested in it. The
The allegations suggest we are talking about hundreds of thousands of dollars.
We'll have to wait and see how it plays out.
This is early.
They're still doing searches.
At this point, nobody has actually been accused of any wrongdoing.
So what does this have to do with last week?
There were just a bunch of arrests dealing with the housing authority and a kickback
scheme there. I would expect that there's probably going to be more arrests and more
types of low-level stuff like this that is uncovered in the coming months. I don't anticipate
this being the end, as we said in that video, generally speaking this stuff kind of comes in
waves to send a message. So there are probably more developments, not just with this, not just
with the housing authority, but there are more developments dealing with perceived corruption,
alleged corruption in New York. On the way. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}