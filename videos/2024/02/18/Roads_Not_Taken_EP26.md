---
title: Roads Not Taken EP26
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Bs2hVw2v15k) |
| Published | 2024/02/18 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau starts the episode by providing the date and episode number, setting the stage for the weekly series where he covers news that was under-reported or lacked context.
- NATO allies express concern about Trump potentially being re-elected and how it could impact global security negatively.
- The House representative who raised an alert about a space-based threat from Russia faces pressure to resign due to the handling and fallout of the situation.
- A new aid package, excluding humanitarian aid for Gaza, is being proposed to appease Republicans in the House.
- Hungary might ratify Sweden's NATO advance by the end of February.
- Ukraine loses support from House Republicans, causing them to withdraw from a contested area as Russia loses a warship to Ukraine.
- Egypt is constructing a containment area for potential refugees due to a proposed ground incursion into Ra'afah, which has faced widespread condemnation.
- The Biden impeachment inquiry fell apart after a key witness was arrested for lying to the feds.
- Beau mentions companies like Amazon, SpaceX, and Trader Joe's arguing to have the NLRB declared unconstitutional.
- Beau talks about a federal judge sentencing McGonigal to a total of six years and six months in prison.
- George Santos plans to sue Jimmy Kimmel for alleged deception over the Cameo platform.
- Beau shares predictions that the Amazon ecosystem could collapse, turning into a savanna within the next 30 years.
- Beau mentions an oddity where a female stingray named Charlotte is pregnant without being around a male for eight years.
- Beau addresses questions about Ukraine's situation, Biden's handling of documents, the ICJ's role, and the maturity of senators versus house counterparts.
- Beau provides updates on two individuals he previously discussed in romance-related videos.

### Quotes

1. "If Europe doesn't step up, and Congress doesn't act, it is likely to change the type of fighting, and it is likely to be much longer and at a much higher cost for both sides in people, not in dollars."
2. "Nationalism is politics for basic people. It creates a team mentality rather than one that is looking for progress."
3. "Having the right information will make all the difference."
4. "Life finds a way."
5. "Patriotism in its sense of wanting to protect your neighbors, I get."

### Oneliner

NATO allies express concerns about Trump's potential re-election as Beau covers under-reported events globally, from aid packages to environmental predictions, offering insights on patriotism and providing updates on romance-related stories.

### Audience

Information seekers, global citizens

### On-the-ground actions from transcript

- Contact your representatives to push for humanitarian aid for Gaza in proposed aid packages (suggested).
- Stay informed about global events and encourage others to seek accurate information (implied).
- Support environmental initiatives and awareness campaigns to prevent ecosystem collapse (exemplified).

### Whats missing in summary

Beau's engaging storytelling style and depth of insight on various global issues can best be appreciated by watching the full transcript.

### Tags

#GlobalNews #SecurityConcerns #AidPackages #EnvironmentalPredictions #Patriotism #RomanceUpdates


## Transcript
Well, howdy there, internet people, it's Beau again.
And welcome to the Roads with Beau.
Today is February 18th, 2024,
and this is episode 26 of the Roads Not Taken,
a weekly series where we go through
the previous week's events and talk about news
that was unreported, under-reported,
didn't get the coverage it deserved, lacked context,
or I just found it interesting.
And then at the end, we go through some questions
from y'all, if you want to send in a question,
you can send it to questionforboe at Gmail.
Okay, starting off with foreign policy.
NATO allies are expressing concern
that Trump might be elected again
and undermine global security for cheap talking points.
The leader of Estonia made some comments
that definitely got a little bit of coverage
And I would expect to see more along those lines,
basically saying that a United States under Trump
would make them worry.
And in short, the United States would
lose its position as a global leader
because of the erratic behavior of Trump.
OK, the representative in the House
issued the alert about the space-based threat from Russia is under pressure to resign.
The way it was handled, the way the information came out, the almost panic it caused, it gave
a lot of people pause.
In an attempt to appease Republicans in the House, a new aid package has been put together.
The humanitarian aid for Gaza has been removed.
The package that came out of the Senate that has Biden's support has more than $9 billion
in aid for Gaza and the West Bank.
The package that is being put forth to try to get Republicans to act does not have that.
Hungary might ratify Sweden's NATO advance as soon as, I want to say it was February
26th, the end of the month.
Russia lost another warship to Ukraine.
Meanwhile, Ukraine is withdrawing from a heavily contested area, and this is occurring after
House Republicans decided to go on vacation and failed to provide aid.
It's worth remembering that as we have talked about this over the years, it is very clear
that Ukraine will win if given support.
If support from the West was to dry up or be interrupted, Ukraine might still win.
But it will be much longer.
It will cause more economic devastation to the West, and it will cause more loss, not
in dollars, but in people.
But I guess the House had to get that vacation in, that seemed more important than helping
U.S. allies, I guess.
Egypt appears to be building a containment area for potential refugees from any proposed
ground incursion into Ra'afah.
That operation has been just widely condemned and panned across the board.
From everybody from humanitarian groups to Biden to counterinsurgency experts, you cannot
find a lot of people that think that this is a good idea.
We're still waiting to find out what's going to occur with that.
Moving on to U.S. news, the Biden impeachment inquiry fell apart when a witness at the very
heart of Republican claims was arrested for lying to the feds about those claims.
Trump was booed at a sneaker convention, sneaker-like shoes, when he launched his new line.
The ones I saw were exactly what you would expect, they're gold.
And I think they're running $399, almost $400, something like that.
The representative who issued the alert about Russia's space-based threat is now under pressure
to resign due to the way it was handled, the way information might have come out, and just
the general panic it almost caused.
A number of companies, including Amazon, SpaceX, and Trader Joe's are arguing to have the NLRB
declared unconstitutional.
NLRB, that is the National Labor Relations Board, it protects the rights of workers against
just horrible companies.
It is not a new entity.
It has been around for decades.
A judge sentenced McGonigal to two years and four months in prison.
This is a separate sentence.
This is in addition to a 50-month sentence for a separate case up in New York.
So you're talking about a total of six years and six months because in a very unusual move,
a federal judge ordered those sentences to run consecutively, which is incredibly, that
doesn't happen very often.
sentences normally run concurrently in the federal
system. And for those who may not remember who that is, that
is the the former FBI counterintelligence chief. Moving
into cultural news, George Santos is apparently going to
sue Jimmy Kimmel for three quarters of a million dollars.
Because I guess the allegations are that Kimmel tricked
Santos over the Cameo platform, and that has somehow bothered Santos.
In environmental news, the Amazon ecosystem could collapse sometime in the next 30 years.
One of the predictions suggests it might turn into a savanna.
In oddities, a female stingray named Charlotte is pregnant.
That's not the weird part.
The weird part is that she hasn't been around a male in
like eight years.
But life finds a way.
This time, it looks like it's using a form of asexual
reproduction, where all of the genetic
material comes from mom.
OK, moving on to the Q&A section.
I don't always agree with your naively rosy idealistic outlook
on things, but the information you provide
is usually incredibly accurate.
There's a lot of terms for me.
I don't know that naive is one of them.
Okay, my question is, why did you stop doing updates on Ukraine?
And I'm not talking about the U.S. perspective, how Congress can't agree on an aid package,
but more about how the war is going over there.
Is it really as bad as everyone seems to think?
Between the people actively rooting for the invader and the doom and gloom people, who
are even more cynical than me, I get the impression it's really bad, Russians advancing faster
in the blitzkrieg. Capital is about to fall. Ukraine to capitulate within days. Is there
any truth to that? No. No. So, I mean, it's not going well right now for Ukraine. They
have an ammunition shortage. They're running low on a lot of equipment. But the idea that
that this is going to end with a Russian victory within days? No, that's not...
That seems incredibly unlikely. Remember that a lot of the people who support Russia, they
tend to wish-cast, they tend to broadcast what they want to happen rather than what's
likely. There was a relatively large setback for Ukraine with the loss of a town, but it's
a lot like Solidar or Bakhmut. The price paid for it, it goes back to the whole Bunker Hill
thing.
The question at play is whether or not Congress is going to act.
That's why I'm focusing on that because that's what really matters.
And if Congress is not going to act, whether or not Europe is going to step up.
That's what matters.
If Europe doesn't set up, step up, and Congress doesn't act, it is likely to change the type
of fighting, and it is likely to be much, much longer and at a much higher cost for
both sides in people, not in dollars.
I think there's just been a lot of other stuff going on.
Right now, I would say that more than half the messages I'm getting are about why I'm
not covering a particular topic and it's you know spread over two dozen topics you know I put out
four videos a day plus the stuff on this channel and I still can't get it all there's a lot of
news breaking right now okay okay so Biden didn't commit a crime with the documents but is there
There's still valid criticism of him over the handling of the documents.
Yeah.
Okay.
To be clear, well, so we don't actually know that.
I would presume that Biden mishandled documents, okay?
Not to a criminal level, but definitely mishandled them.
I can't prove that, but given the context of everything around, yeah, there's an issue.
We've known there's an issue with how classified material is being handled by politicians.
Ever since this first came to light, when it first came out with Trump, that seemed
to be a very intentional thing at first glance, then you get news about Pence and Biden.
None of that seemed intentional, but the reality is it still shouldn't happen.
It shouldn't happen by accident either.
There's still criticism to be made there.
And I don't know that it's necessarily of, it shouldn't be limited to Biden.
Biden undoubtedly had a part in it because this is, it is clear that this is not something
that was isolated to Pence.
There's more to this.
There's a widespread issue at the White House that hopefully has been addressed by now.
Why isn't the ICJ doing anything to stop RAFA?
A very negative term for Biden.
There's a lot of colorful language surrounding it, but let's just say Joe issued a stronger
condemnation, but that's not like in context, that's more like even this guy did.
Why isn't the ICGA doing anything to stop RAFA?
the ICJ doesn't have an enforcement mechanism?
This is something that we've gone over a couple of times,
and the answer isn't going to change.
The ICJ does not have an enforcement mechanism.
In many ways, and this is something
that we'll go over at a later date,
Because going over it now, it would just make it worse.
But in many ways, the ICJ did not help.
It doesn't have an enforcement mechanism.
So the only thing it can do is apply pressure.
It can get countries to do things
that are symbolic in nature.
say, not delivering parts that really aren't needed for another four years, and say it's
for this reason.
It can encourage countries to act, however, if the ICJ gives a timeframe and says, hey,
we need to know what's going on by this date, those countries that are applying pressure,
have been applying pressure, they lose a little bit of their leverage because Israel can say
we're following what the ICJ said.
We just have to make sure that we do this and make sure that we're doing everything
we can to prevent this problem.
A lot of times, again, it's foreign policy,
international poker game, and everybody's cheating.
If you go in trying to play by the rules
when everybody else is cheating, you normally don't win.
And again, this is one of those things
where we can talk about it afterward
and go through where things happened.
Don't hope that the ICJ is actually
going to solve this issue.
Don't hang your hopes on that.
They can apply pressure, and it gives some moral cover, but there are as many downsides
as upsides.
from a Canadian. Why do senators appear more mature and level-headed than their house counterparts?
Given how senators have a much longer term than congresspeople, you'd think they'd be
the ones playing fast and loose, since they could bury past bad behavior when it comes
time to be re-elected." Okay, so this is way easier than it gets made out to be. It's
It's not like senators, you know, they go through the House and they become more mature
or more level-headed.
It's that a senator is generally a statewide race, whereas representatives in the House,
it's from a smaller district.
So if you have an incredibly conservative district that is incredibly extreme in their
belief, well, you can get an incredibly extreme Republican
representative. However, that same representative would never
win statewide, because the the extreme progressives, they would
balance it out. So what you end up with in the Senate, most
times are either centrist or people like Bernie Sanders as an example, okay?
Somebody who has progressive views but is also very pragmatic.
Somebody that knows how to make a deal.
So that's why that occurs.
It's the way the elections actually are conducted and somebody who wants to win as a senator,
they can't be extreme because the turnout from the non-extreme side will make sure they
don't win.
So it's much harder to get that in there.
It's not impossible, I should point out.
It's just harder.
Do you consider yourself a patriot?
What do you think about patriotism more generally?
A lot of folks who watch you have very dismal reactions to that word, but because of how
I grew up, patriotism rings deep chords with me.
It is a thing I aspire to.
my country right or wrong, if right to be kept right, if wrong to be corrected.
I want more leftists to embrace patriotism and invite the world to join us."
Patriotism in the sense of love of one's country, countryside, neighbors, sure.
Patriotism in the way that term normally comes out, which is nationalism.
me. I don't think that's a good idea. I don't like it as a motivating tool. Patriotism in
its sense of wanting to protect your neighbors, I get. And that's one of the things that,
one of the reasons the left has the reaction to it, in particular the anti-authoritarian
left is because a lot of people who identify themselves as patriots, they are totally not
patriots.
They're nationalists, and they're not even like the better kind of nationalists.
Those two terms have been conflated in the U.S. so much that there's a real issue there.
I understand what you're saying and from a perspective of who it might reach, liberals,
not necessarily leftists, but liberals are probably more open to the idea of patriotism
in the way that you're probably thinking here.
So again, to me, nationalism is politics for basic people.
It's politics that creates a team mentality rather than one that is looking for progress.
It creates it more competitive than cooperative.
My personal ideology is more about cooperation.
I have mango trees in my backyard, and I'm wondering if the trees you mentioned were
also mango trees.
Oh, in the recent thing with my wife.
No, the trees that are out there.
Let's see, there are, the ones that are not planted yet are, there's four apple trees,
I think, maybe Dorset and Gala, maybe, then two peach trees, a Florida King and Florida
Queen, two plum trees, I don't know the varieties, and two pear trees.
What happened? I went to go and actually buy citrus trees. None of those are citrus
as you might imagine, but when I showed up they had those and they were all
they were all types that helped the other pollinate. The varieties went
together and you it is harder than you might imagine around here to get pears
that that complement each other in that way. So when I saw them I went ahead and
got them and I haven't had time to put them in the ground yet. So that is that's
that's what my wife was talking about. I do not have any mango treats though. We
have we have tons of different kinds of fruit trees. Lemon, grapefruit, tangerine,
Tangelo, just all kinds of stuff. Kumquat, loquat. I do not have any mangoes though.
Okay. Do you ever hear back from the people who asked you romance-related topics that you
eventually made videos about? I'm specifically thinking about two videos from a while back.
The framer, who was insecure about his relationship with the doctor, the surgeon, and the young
man with the southern suite co-worker.
For the record, the twinkle in your eye when you said to that young man that you like her
was adorable.
And I always hoped for a follow-up video on this one letting us know how things turned
out with them.
Okay, so the framer and the surgeon, he asked her to marry him, and she of course said yes.
For those who have no idea what this is about, the framer makes what a framer makes. The
surgeon makes what a surgeon makes. And he said that everything was perfect, but this was something
that he was really worried about, because she made so much more than him.
And I want to say, initially, I put the message on Twitter with all personal
information removed, but that doesn't help if your significant other is the
person who introduced you to the channel. And she saw it and was like, hey, I'm a
a surgeon with a framer boyfriend, why don't you tell them to put a ring on it?"
And I just posted it.
And yes, my understanding is that they've gotten married, I think.
I think that's already happened.
I'm not sure, though.
I know they're engaged.
How about that?
And then the young man with the Southern Suite co-worker.
He got an update from him and basically the last I heard they were dating and he took
her home to meet his family.
His brother was not cool, not very accepting of the whole thing.
His dad was like, you know, you're weird but you've always been weird, you know, so this
This is weird, but whatever, as long as you're happy, I don't care.
And mom treated her like any other woman that came to date her son.
And everything seemed to be going well, at last I heard.
And I just realized there's no explanation for any of that in here.
The co-worker was a trans woman and yeah, but last I heard all of that is going very
very well too.
And that's it.
Those are the end of the questions.
So there you go, a little bit more information, a little more context and having the right
information will make all the difference.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}