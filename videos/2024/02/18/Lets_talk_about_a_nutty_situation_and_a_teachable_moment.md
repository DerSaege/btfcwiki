---
title: Let's talk about a nutty situation and a teachable moment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=zrfL3d08pFY) |
| Published | 2024/02/18 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing a law enforcement situation where an acorn prompted two deputies to fire into a patrol car at a handcuffed, unarmed suspect.
- The deputies fired every round in their magazines in quick succession, known as mag dumping.
- Despite firing multiple rounds, the suspect was not hit, but the use of force was deemed inappropriate.
- One deputy resigned, and the other was cleared because the practice of firing if another officer does is common in law enforcement training.
- Beau questions the flawed training that led to firing without visualizing the threat, stressing the potential dangers of such practices.
- He underscores the importance of changing this training policy to prevent unnecessary harm and legal consequences.
- Beau advocates for ensuring officers have a clear visual on a target before using lethal force to prevent tragic outcomes.
- He warns of the risks involved in blindly following the actions of other officers without assessing the situation independently.
- Changing the practice to require officers to see the threat before firing is emphasized as a critical step to saving lives and avoiding legal repercussions.
- Beau urges law enforcement officers to challenge current training methods and advocate for safer, more effective approaches to handling threats.

### Quotes

1. "An acorn hit the deputy and that prompted two deputies to mag dump into a patrol car at a handcuffed unarmed suspect."
2. "If you can't see the threat, you probably shouldn't be firing at it."
3. "It is not smart for a whole bunch of reasons."
4. "If this practice is changed, and you actually have to put eyes on the target, it will save lives."
5. "If it doesn't change, you can end up in jail."

### Oneliner

Law enforcement must rethink training practices to ensure officers visually confirm threats before using lethal force, preventing potential tragedies and legal repercussions.

### Audience

Law enforcement officers

### On-the-ground actions from transcript

- Challenge current training practices and advocate for safer approaches (suggested)
- Initiate dialogues with training officers about the need for visual confirmation of threats before firing (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of a concerning law enforcement incident involving mistaken gunfire and underscores the importance of revising training practices to prioritize visual confirmation of threats.

### Tags

#LawEnforcement #TrainingPractices #UseOfForce #Safety #LegalRepercussions


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about
best practices, and training,
and a teachable moment that should be analyzed
and should be used to alter
what is an incredibly common practice.
It shows why it's not a good idea,
despite it being conventional wisdom
and an incredibly common practice for a really long time.
And we are going to talk about a situation
that involves, well,
it involves law enforcement
and the situation getting a little nutty.
Okay, so this is what occurred
according to best reporting available.
The deputy has made contact with a suspect, detains the suspect, searches the suspect,
cuffs the suspect, puts the suspect in the back of their patrol car, then goes to get
some more information or get a form, something like that.
Coming back, the deputy hears a noise and feels an impact, their chest, pulls the radio,
on hit, drops to the ground, pulls their weapon, and mag dumps into the patrol
car. Another deputy, seeing what's going on, also mag dumps into the patrol car. A
Magdump is firing every round in the magazine in quick succession.
Okay, so what happened?
Well the deputy was not hit.
The current theory is that an acorn hit the deputy.
An acorn, a nut fell from a tree and hit the deputy and that prompted two deputies to mag
dump into a patrol car at a handcuffed unarmed suspect.
Now the good news is that mag dumps are notoriously inaccurate.
Despite two magazines being emptied into the vehicle, the suspect was not hit.
Now that doesn't actually make it okay.
I am sure the suspect is going to sue and win.
That was probably a very terrifying experience, even though they weren't physically injured.
OK, so what did the review determine?
The review determined that this was inappropriate force,
obviously, that it wasn't an objectively good thing
for the first deputy, the deputy who first opened fire,
and that deputy resigned.
The other one, well, that was OK.
Why?
because it is incredibly common practice, cops are trained that if they roll up and
they see an officer firing that they should fire at the same thing.
This should be an indication that this is bad training.
If you can't see the threat, you probably shouldn't be firing at it.
There are obvious exceptions if you are laying down suppressive fire or you have seen the
threat but the threat has dropped behind concealment or something like that.
But if you never saw the threat, you shouldn't fire simply because somebody else does.
And in an investigation where nobody was hit, yeah, I mean, you'd probably be cleared.
But my question is this, and this is the question that any cop watching this should probably
ask themselves and then go talk to the training officer about.
If this situation had gone differently and the suspect had been hit and the suspect did
not have a good outcome at the hospital. When the feds showed up to investigate
the person who was handcuffed, unarmed, and in the back of a patrol car who was
killed, do you think they would just indict the first cop? This is really bad
training. This practice, I know it's been around for a long time. It's not smart. It
is not smart for a whole bunch of reasons. This is an extreme situation
that proves the point, but understand if you're simply firing in the direction
that the other cop is firing in, you don't know what else is over there. That
cop could have a good bead on something like you know maybe maybe they can shoot
past the trash can that you try to shoot through that the toddler is hiding
behind. It is a horrible practice. This should be a teaching a teaching tool to
demonstrate that. It's one of those things that absolutely it will save
lives. There's no doubt if this policy is changed, if this practice is changed, and
you actually have to put eyes on the target, which seems like I mean it
should go without saying in most situations, it will save lives. I'm just
saying that out of self-interest, because most times people are motivated by self-interest,
if it doesn't change, you can end up in jail.
Anyway, it's just a thought.
Ya'll have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}