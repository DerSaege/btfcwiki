---
title: Let's talk about Trump, Taylor, and a tale of two fundraisers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=7zDhsbo4tKg) |
| Published | 2024/02/18 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Comparing Trump and Taylor Swift fundraisers.
- Taylor Swift targeted for encouraging young people to vote.
- Trump supporters raised about $100,000 a day for him.
- Taylor Swift donated $100,000 to the family of a woman killed at the Kansas City Super Bowl Parade.
- Beau questions attacking good people to maintain group identity.
- Encourages finding new peers if needed.

### Quotes

1. "I refuse to believe that this many people in this country are innately bad."
2. "Your group's bad."
3. "I will totally attack somebody for telling people to do their civic duty."
4. "If you find yourself constantly having to hate and attack good people to stay in the good graces of your peers, you need new peers."
5. "It's just a thought, y'all have a good day."

### Oneliner

Beau compares fundraisers, questions attacking good people to fit in groups, and urges finding new peers if needed.

### Audience

Observers of group behavior.

### On-the-ground actions from transcript

- Find new peers (implied).

### Whats missing in summary

The full transcript provides additional context on the comparison between Trump and Taylor Swift fundraisers and the impact of attacking individuals for positive actions.

### Tags

#Trump #TaylorSwift #Fundraisers #GroupBehavior #PeerInfluence


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump and Taylor
and a tale of two fundraisers.
We're going to do this because over the last month there's
been a chain of events.
And then over the last weekend, there was another.
And there's been a lot of comparisons made.
There's been a lot of things said.
And somebody came under scrutiny and came under unfair, unjust,
and unwarranted attacks from the right.
Taylor Swift just got trashed for the last month
for the apparently unforgivable crime of dating a football
player. The real reason the right went after her, it wasn't because she dated a football
player and the cameras got turned on her. That was just the excuse that they gave their
base. That was just how they manipulated them. The reason they went after her is because
she encouraged young people to vote. Didn't even tell them how. Just told them to register
and vote, you know? That was it. But see, that is a threat to authoritarian goons
everywhere. So she needed to be under fire to generate some reason to dislike
her. Now, Trump, he had his event in court and got that big judgment against him. His
supporters, they put together a fundraiser for him, The Billionaire.
It looks like they're raising about $100,000 a day, which is, I mean, that's interesting.
That's, I mean, if they were able to keep up that intensity and just sustain it forever,
it would take care of the interest because that judgment is accruing.
And it's about $100,000 a day.
Taylor Swift is also a billionaire.
Even though Trump actually is allegedly more of a billionaire, has a net worth twice what
she does, Taylor Swift was also involved in a fundraiser over the weekend.
So about $100,000, well with her it was exactly $100,000, two $50,000 donations to the family
of the woman who was killed at the Kansas City Super Bowl Parade.
A billionaire doesn't need your money.
Here's the thing, and it's something that the last weekend, the last month, you figure
it would have taught people this, but if you constantly find yourself having to attack
hate, people who do objectively good things just to stay in the in group, to
stay in the in crowd. Your group's bad. If you are expected to attack people for
doing good things or for just living their life, you're not part of a good
group and that group has overcome your identity. I refuse to believe that this
many people in this country are innately bad. You're following the crowd. You're
following your peers. You are othering, attacking, and hating people simply
because you're told to by your group. It's not behavior you would tolerate from
your children, given the age brackets involved. My guess is you would say
something about if all of your friends jumped off a bridge would you do it too?
It appears that for a whole lot of people, the answer is yeah. Yeah. I will
totally attack somebody for telling people to, you know, do their civic duty.
I'll attack somebody because a cameraman showed them having a good time.
I'll attack somebody who donates to the family of somebody who's been involved in a tragedy.
I will call them names and I will support the opposite.
If you find yourself constantly having to hate and attack good people to stay in the
good graces of your peers, you need new peers.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}