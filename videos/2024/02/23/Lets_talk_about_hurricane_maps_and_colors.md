---
title: Let's talk about hurricane maps and colors....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=mErfOd_P7CQ) |
| Published | 2024/02/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- National Hurricane Center conducting an experiment out of season to make a change and get people ready.
- The Cone of Uncertainty (or Cone of Doom) map used during hurricanes is familiar, but lacking.
- The National Hurricane Center has tons of information but doesn't provide it all to the public to avoid decision paralysis.
- New maps will include additional color-coded information about hurricane and tropical storm conditions further inland.
- Changes in weather patterns mean traditional safety expectations are no longer reliable.
- Providing easily digestible information to the public to assist in decision-making during hurricanes.
- People may resist the change due to the addition of more colors in the maps.
- Importance of embracing useful information to aid decision-making during emergencies.

### Quotes

1. "The National Hurricane Center has tons of information, but they don't provide it because the public can't handle it."
2. "People may resist the change because, you know, there's more colors. I'm sure the National Hurricane Center has gone woke."
3. "Changes in weather patterns mean traditional safety expectations are no longer reliable."

### Oneliner

The National Hurricane Center is introducing new color-coded maps with additional info to help the public navigate changing weather patterns and make informed decisions during hurricanes.

### Audience

Emergency Preparedness Planners

### On-the-ground actions from transcript

- Study and familiarize yourself with the new color-coded maps from the National Hurricane Center (suggested).
- Stay updated on changing weather patterns and safety recommendations in your area (suggested).

### Whats missing in summary

Additional context on the importance of adapting to changing weather patterns and utilizing available resources for enhanced decision-making during emergencies.

### Tags

#NationalHurricaneCenter #Hurricanes #EmergencyPreparedness #WeatherPatterns #DecisionMaking


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about hurricanes,
and cones, and maps, and a whole bunch of things
that seem really out of season right now,
because the Atlantic hurricane season
doesn't start till June.
It is out of season, but it's because
the National Hurricane Center
is conducting a little experiment.
They're going to be making a change this year,
And just trying to get people ready for it.
OK, so what are they doing?
Normally, you have probably seen it before.
The Cone of Uncertainty, I guess is what they call it.
I think most Floridians call it the Cone of Doom.
It's that map you see plastered all over the internet
anytime there's a hurricane.
And it has the little cone that comes out, and then a
circle at the end, and then some years there's like a sharpie that comes along
and moves it, that thing. That's the map that's put out by the National Hurricane
Center. It does a pretty good job of providing information, but it's lacking
and the National Hurricane Center knows this. The thing is, they have tons of
information, just tons and tons and tons of information, and they could provide a
bunch to the public. They don't because realistically the public can't handle it.
The public can't handle that much information. If you give people in a
stressful situation too much information it leads to decision paralysis and they
can't decide what to do which means they may not leave when they need to. So the
The National Hurricane Center has tried for years to strike a balance between providing
the information that is helpful and not overloading people.
They have determined that the American people are capable of handling a little bit more
information as long as it's color-coded.
So the new maps, the ones that you will start seeing this year as part of this experiment,
they will include additional information about whether or not you will experience hurricane
conditions or tropical storm conditions further inland.
Because right now you just have like the color coding along the coast and then the cone.
You will get color coding moving further inland.
And hopefully that will help make people, it'll help people make decisions better.
Because things are changing, traditions are changing, normal patterns are not the same.
There was a time when if you were north of Highway 90 in the Panhandle, you didn't have
to worry about anything unless it was a huge hurricane.
And even if it was a huge hurricane, you could expect it to drop a category or two during
that time period.
That's not the case anymore.
So this is a way to provide better information in an easily digestible fashion to the public.
Undoubtedly people are going to complain about it because it's a change and humans are reluctant
to change.
And I'm sure there will be complaints because, you know, there's more colors.
I'm sure the National Hurricane Center has gone woke.
But from where I'm standing, it's useful information that might help people overcome that indecision
that occurs when it's do we need to leave or not.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}