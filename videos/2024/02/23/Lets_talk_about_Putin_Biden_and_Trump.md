---
title: Let's talk about Putin, Biden, and Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=7EDaIIuMQNo) |
| Published | 2024/02/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains Putin's statement preferring a second Biden term to Trump returning.
- Points out that Putin's preference is a tactic to help Trump by providing him with a talking point.
- Contrasts Trump's passive approach towards Russia with Biden's efforts to counter Russian aggression.
- Emphasizes that Putin's goal is to manipulate Trump's gullible base.
- Stresses the importance of recognizing Russia as an adversarial nation.
- Notes that Putin's intentions are not truthful but aimed at advancing his interests by weakening the US.
- Urges viewers to understand the manipulation behind Putin's statement and its implications for Trump's base.

### Quotes

1. "It's Putin trying to help Trump."
2. "Putin said this to give Trump a talking point to use on the most gullible, least intelligent Americans."
3. "He knows that Americans, most Americans, understand that he is on the other side, that he's adversarial."
4. "It's because people aren't thinking back to when Trump was in office."
5. "You cannot look at the foreign policy position of Russia under Putin and under Trump and honestly believe that Putin would prefer four more years of Biden keeping him in check instead of four more years of Trump doing whatever he's told."

### Oneliner

Beau explains Putin’s tactic of preferring a Biden term to aid Trump, manipulating his gullible base against Biden’s strong stance on Russia.

### Audience

Viewers, political observers

### On-the-ground actions from transcript

- Educate others on the nuances of international relations and the tactics used in political manipulation (implied).
- Stay informed about geopolitical events and understand the context behind political statements (implied).

### Whats missing in summary

In-depth analysis of the implications of foreign policy decisions on international relations.

### Tags

#Putin #Trump #Biden #Russia #ForeignPolicy


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about what Putin said.
We're going to talk about what Putin said about Biden and Trump and just kind of go
through it because there's been a lot of questions about it and I think it's
important to go over what Putin said, why he said it, and why he believed it was
important to say it.
Okay, so first, let's talk about what he said.
Putin said that he would prefer a second Biden term to Trump coming back.
Now, obviously, especially with the relationship that Biden has with Russia right now, you would have to wonder why he
would say that.  And it's because people aren't thinking back to when Trump was in office.
Do you remember the way Trump just held Putin in line when it came to the Open Skies Treaty?
Or after the Kurt Strait incident, the way Trump just cornered Putin?
No right?
Because Trump never, never made any move against Russia.
In fact, to the contrary, Trump actively sought to weaken NATO, whereas Biden has led the
charge to make sure Ukraine has the aid it needs to grind Putin's military into dust
and has led the charge to make sure that Russia is sanctioned and their economy is tumbling
because of the aggression in Ukraine.
So why would he want a second Biden term?
He doesn't.
He was trying to give a talking point to Trump so he could get the weak candidate back.
Trump was very weak on foreign policy.
He wasn't good at it.
He never asserted anything when it came to the United States.
He caved at every opportunity, especially when it came to Russia.
So why would Putin say this to give Trump the talking point?
Why does he believe it would work?
Because the Russian intelligence services pay close attention to the information ecosystem.
And they know that a lot of Trump's base are gullible.
They have fallen for a lot of obvious ploys in the past.
So it stands to reason that they'd fall for it again.
Putin said this to give Trump a talking point to use on the most gullible, least intelligent
Americans.
And Trump is going to be happy to do that.
That's what happened.
You cannot look at the foreign policy position of Russia under Putin and under Trump and
honestly believe that Putin would prefer four more years of Biden keeping him in check instead
of four more years of Trump doing whatever he's told.
Because that's what he did.
It's important to remember that Russia is an adversarial nation.
What interest, what motivation would Putin have to tell the truth?
He doesn't have one, right?
His goal is to get the weakest candidate.
And he knows that Americans, most Americans, understand that he is on the other side, that
he's adversarial.
But he also knows that Trump's supporters are gullible.
And they will take this talking point and they'll run with it.
He knows that by saying he would want four more years of the guy who has just destroyed
his military and destroyed his economy.
He knows that by saying that, Trump's base will run out there and say, look, Biden's
weak on Russia, even though it was Trump that caved at every opportunity.
It's Putin trying to help Trump.
That should be obvious.
If you didn't see that and you're a long time viewer of the channel, I have failed you.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}