---
title: Let's talk about a student debt relief question....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=DQfy2ILFl4c) |
| Published | 2024/02/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing student debt relief and providing context.
- Biden's latest student debt forgiveness viewed as a re-election stunt by some.
- $1 billion forgiven for 150,000 people, seen as inadequate by critics.
- Larger programs underway, with $138 billion forgiven for 3.9 million people.
- Misconceptions that Biden is not doing anything about student debt relief.
- Only a fraction of eligible people enrolled in the SAVE program.
- Habit of repeating talking points without updating information.
- Some critics claim Biden did nothing regarding student debt relief.
- Department of Education reaching out to eligible SAVE program non-enrollees.
- Continuous development and changes in student debt relief programs.
- Importance of keeping up to date with evolving programs.
- Programs available for debt relief despite misconceptions.
- Need for individuals waiting for debt relief to stay informed.
- Constant changes and updates in student debt relief policies.
- Legal challenges expected for new programs.

### Quotes

1. "He didn't do anything. He didn't do anything. And it has stuck."
2. "That's huge. That's huge."
3. "It's changing constantly."
4. "Keep up on this without pundits."
5. "When you start adding it all up it's just like this stuff."

### Oneliner

Beau breaks down Biden's student debt relief efforts, dispels misconceptions, and urges staying informed amid evolving policies.

### Audience

Students, debt holders

### On-the-ground actions from transcript

- Stay informed on evolving student debt relief programs and policies (implied).
- Enroll in eligible debt relief programs (implied).

### Whats missing in summary

The full transcript provides a detailed breakdown of Biden's student debt relief efforts and the need to stay informed despite misconceptions.

### Tags

#StudentDebtRelief #BidenAdministration #Misconceptions #StayInformed #DebtForgiveness


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to answer a question
about student debt relief and we're going to answer it
and provide a little bit more information
and context and stuff like that.
Here's the thing, if you are somebody who believes
that Biden isn't doing anything about student debt relief
and you have debt you won't forgiven,
You need to watch this video.
Okay, so here's the question.
I've seen a lot of people saying
that Biden's latest student debt forgiveness
is just a re-election stunt,
and that $1 billion and 150,000 people
is too little, too late.
I have to admit, I feel the same way,
but I also kind of have a memory of you saying
he was doing things behind the scenes.
I first started listening to you on a long road trip
and listened to 50 episodes at once,
and it was then, I'll never find it again.
Is there more to this?
Yeah, yeah.
Okay, first, it's not behind the scenes.
Okay, let's just start there.
But if you don't know what's going on,
the latest project from the administration has come out.
It's 150,000 people, a billion dollars
is gonna be forgiven.
Yeah, I mean, if that was all he did,
that would be a re-election stumped.
The problem is that the real numbers are 138 billion and 3.9 million people.
That's the reality.
That's what has been forgiven so far.
There are larger programs that are part of the rule rewrite that are still underway.
This doesn't include them.
The thing is, you have a whole lot of people that continually repeat that.
He's not doing anything.
He hasn't done enough.
Not just is it bad because it's obviously leaving people with the wrong impression.
It's bad because if you are waiting for student debt relief, you might not be keeping up on
it because people are telling you he's not doing anything about it, that the administration
isn't doing anything, so you're not paying attention.
The latest one that went out, the 150,000 people, a billion dollars, a little more than.
To get that, you have to be enrolled in the save plan.
There are 30 million people that are eligible for that.
You know how many are enrolled?
Less than a third.
Seven point five million.
There is a habit among people to latch on to a talking point and continually repeat
it even if it's wrong.
Even if the situation changes.
When it comes to he's not going to do anything about student debt relief, there were a bunch
people who said that right from the beginning, before he even took office.
And then when the Supreme Court shot down the plan that he put forward that would have,
I don't know, I want to say that was 400 billion, I think, that would have forgiven that.
That was the end of it.
They just started repeating that over and over again.
He didn't do anything.
He didn't do anything.
And it has stuck.
billion dollars from 3.9 million people. And it's not behind the scenes, it's out
in the open. As far as I know, I think the Department of Education is
actually calling people now or trying to contact people who are eligible for the
SAVE program who aren't enrolled. It's not behind the scenes, it's that
people got their talking point they don't want to change it. They don't want
to keep up with the recent developments. The 138 billion that it came in in small
amounts. Four billion here, six billion there, over and over again and all of it
you know, it's small amounts and all of it can be blown off. But when you add it
all up, that's huge. That's huge. And again, it's not a matter of giving credit
where credit is due. It's politics. Nobody expects that. But there are
programs that are available, that people who are under the impression that he's
not doing anything, they might be eligible for them. They might be sitting
there wanting their debt forgiven. They might be eligible for the program, but
they're not enrolling in it because people are telling them nothing's
happening. I would definitely, if you are somebody who is waiting for a little bit
of breathing room, I would try to keep up on this without pundits, you
as might go between because this is changing constantly. The programs that are coming out,
they're being added constantly. The rule changes are huge. If those can make it through the
inevitable legal challenges that will certainly come forward from Republicans, those will be
huge those they might put it put it up to that 400 billion dollar mark I've
seen a lot of people talking about it right now there's no firm numbers but
when you start adding it all up it's just like this stuff it's you know a
billion here 2 billion there pretty soon you're talking about real money anyway
It's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}