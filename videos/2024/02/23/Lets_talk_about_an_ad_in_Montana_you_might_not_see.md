---
title: Let's talk about an ad in Montana you might not see....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=CQx1CPrvZ9s) |
| Published | 2024/02/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Ads from the Montana National Guard showed up in Montana, aiming to recruit based on family inspiration.
- The main ad featured a command sergeant major holding a photo of his grandfather, a US military veteran, with a background photo of troops marching away.
- The troops in the background were wearing helmets associated with the World War II German Army, not the US military.
- The Montana National Guard acknowledged the error and took down the poster, stating it doesn't represent their history or values.
- The poster's removal from distribution was confirmed, but the mistake in including the German Army troops was significant.
- The ad campaign, themed "more than college, spirit of tradition," faced criticism for the oversight.
- The Montana National Guard leadership issued an apology for the inaccurate poster.
- The presence of German Army troops in the background contradicted the intended message of family tradition and military service.
- The mistake likely originated from a lack of understanding or research by those creating the ad.
- The incident serves as a cautionary tale about the importance of accuracy and sensitivity in military advertising.

### Quotes

1. "This poster does not represent our history or values."
2. "It's a spirit of tradition, having those soldiers in the background that was just all bad."
3. "Somewhere in Montana, a sergeant major's boot is being removed from the lower intestine of somebody in the public affairs section."

### Oneliner

Ads by Montana National Guard in Montana featured a major oversight, showing German Army troops in an ad promoting family military tradition.

### Audience

Recruitment officers, marketing teams

### On-the-ground actions from transcript

- Contact Montana National Guard for further clarification on their recruitment strategies (implied)
- Monitor military advertising campaigns for accuracy and cultural sensitivity (generated)

### Whats missing in summary

The full transcript provides a detailed account of an advertising mishap by the Montana National Guard, showcasing the importance of historical accuracy and cultural sensitivity in military recruitment efforts.

### Tags

#Montana #MilitaryRecruitment #AdvertisingMistake #GermanArmy #Accuracy


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Montana
and advertising in recruitment and mistakes.
We're gonna talk about mistakes.
Um, okay, so in case you missed it,
the, uh, some ads showed up in Montana
and they are from the Montana National Guard.
Now, the Montana National Guard, they were like, hey, you know what, a lot of times people
who joined the military, they did so because a family member kind of inspired them.
Their granddad, their uncle, their dad, they were in the military, so it inspired them
to join.
Because of this, they decided this was an untapped advertising avenue.
And they launched their own advertising campaign at the state level for the Montana National
Guard. I believe it was Montana Army National Guard that did this and the
main advertisement is it says more than college it's spirit of tradition and
it's got a photo of what appears to be a command sergeant major and that command
sergeant major he's holding a photo of what is probably his grandfather who is
a veteran of the US military and see that sounds like something that you go
without saying right the US military okay so that's what's on the ad with the
exception of the background and the background is just like a black and
white photo of some troops marching away they are not the US military now they
are marching away so it is one of those things where maybe the person who did it
didn't really understand what they were doing. But if you are familiar with the
equipment of the era, during World War II American troops had a distinctive
helmet and there were some helmets you would rather not see. Those were the ones
that were being worn by the troops in the photo on the Montana National Guard
recruiting poster, the photos, yeah, it's the helmet you would rather not see.
That's who they had. Okay, so the Montana National Guard is aware of this,
and they have released a statement. Montana National Guard leadership is
aware of a Montana Army National Guard recruiting poster that appears to show
members of the World War II German Army in the background. I love that it says
appears to show. No, I mean, that's what it shows. This poster does not represent
our history or values, and has been removed from further distribution.
I think that's a good move, and they do apologize for it, but the idea that they removed that from
further distribution will come back to that. So it does appear that this was a mistake,
the way that it went out, especially with it's a spirit of tradition, and having
those soldiers in the background that was just all bad but according to the
release it is being removed from distribution and I would imagine that
somewhere in Montana a sergeant major's boot is being removed from the lower
intestine of somebody in the public affairs section. Anyway it's just a
thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}