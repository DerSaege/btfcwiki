---
title: Let's talk about Putin, Navalny, and rhetoric....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=gqrLglNZEx8) |
| Published | 2024/02/16 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing Putin and Alexei Navalny's situation, focusing on what is known and unknown.
- Navalny was a prominent opposition figure against Putin in Russia.
- Speculation surrounds Navalny's death in custody, with suspicions pointing to Putin, though unproven.
- Noteworthy is the lack of concrete evidence proving Putin's involvement in Navalny's death.
- There were previous attempts on Navalny's life with similarities to Putin's tactics.
- Various reactions in the United States include accusations towards Putin, but evidence is lacking.
- Suggestions range from Putin orchestrating Navalny's death to inadequate medical care in custody.
- Beau urges clarity on what is confirmed and what remains conjecture regarding Navalny's demise.
- Criticism is directed at political figures in the U.S., with contrasting stances between political parties.
- Regardless of who is to blame, the focus remains on the lack of accountability due to presidential immunity in Russia.

### Quotes

- "You do everything you can to support him. Let's be clear on that."
- "You know, the end of the United States, of the Constitution."
- "What you're seeing there is what will be here."

### Oneliner

Beau analyzes Putin and Navalny's case, stressing the need for clarity amidst speculation and contrasting reactions.

### Audience

Political observers

### On-the-ground actions from transcript

- Support organizations advocating for political accountability (suggested)
- Advocate for transparency and justice in political systems (suggested)

### Whats missing in summary

The emotional weight and urgency conveyed by Beau's analysis and the potential repercussions of unchecked authoritarianism.

### Tags

#Putin #Navalny #PoliticalAccountability #Authoritarianism #PresidentialImmunity


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we are going to talk about Putin and Alexei Navalny.
And we're going to go through what we know, what we don't, and we're going to
talk about the responses here in the United States and something that everybody
kind of needs to acknowledge about this, no matter how it plays out.
Okay.
So the first thing to note is Navalny was the opposition, he was the face of the opposition
within Russia against Putin.
The immediate headlines and immediate reaction and the rhetoric coming from the West is that
Putin killed him while he was in custody.
We don't know that, can't prove that.
I think it's important to acknowledge that we don't actually know that.
It's widely suspected, it's widely believed, and it's not an unfounded theory.
But you can't prove that.
Not yet.
Now while you can't prove that, you can't prove that there had been numerous attempts.
And those attempts carried all of the calling cards of Putin's goons.
That is easily demonstrable.
Okay, so the reaction in the United States.
You have a whole lot of people saying Putin did it, political figures saying that.
And again, it's not an unfounded theory.
That's not coming out of nowhere, but we don't have the evidence to say that yet.
I would say that it's highly likely, but it's also possible that in custody he got sick
and that medical treatment wasn't good.
That's also a possibility.
Now you can say Putin killed him because he had him unjustly jailed and all of that stuff.
That's fair game as well.
But we have to be specific about what we know and what we don't on this.
This is obviously going to ratchet up a lot of rhetoric.
Now most of this is coming from the centrist and the liberal side of things.
The right wing has come out, Pence has said, you know, there's no room in the Republican
party for, you know, Putin apologists and so on and so forth, something to that
effect. Say what you mean. You're talking about Trump. Say it. You have Speaker
Johnson out there basically throwing out his words of support. Yeah, you don't get
to do that. Not after going on recess while Ukraine is waiting for the aid. You
don't get to do that. There's a whole lot of people talking out of both sides of
their mouths right now when it comes to, oh we oppose Putin. No you don't. No you
do not. You do everything you can to support him. Let's be clear on that.
Now, the other thing to note is regardless of how it occurred, regardless of how the
death occurred and who is responsible for it directly or indirectly, the one thing to
note is that he's in jail, he was in jail, he was in custody, he was persecuted the way
he was and ultimately died in custody, because the ruler of that country has presidential immunity.
The executive of that country can do whatever he wants without any accountability.
You know, the thing that is being argued for in the United States.
thing that people who call themselves patriots are backing. Those who say they
love the Constitution are backing. You know, the end of the United States, of
the Constitution. If Trump and the authoritarian goons that support him get
their way. What you're seeing there is what will be here. Anyway, it's just a
thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}