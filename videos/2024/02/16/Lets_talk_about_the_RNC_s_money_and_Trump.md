---
title: Let's talk about the RNC's money and Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=8U_Fjf2Ybio) |
| Published | 2024/02/16 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Republican National Committee (RNC) is facing potential leadership changes, with Trump providing suggestions for key spots, including Laura Trump.
- Many Republicans are supportive of Trump's suggestions, but some have expressed concerns that the RNC may become a "piggy bank" for Trump, paying his legal bills.
- There are worries that focusing solely on electing Donald J. Trump as president may overshadow other responsibilities of the RNC.
- Laura Trump's quote about every penny going towards electing Trump is causing concern among some Republicans about the potential misuse of funds.
- Despite concerns, there may not be enough opposition to derail Trump's picks for leadership positions within the RNC.
- Financial issues, similar to those faced at the state level within the Republican Party, may arise as the RNC potentially heads towards a similar direction.

### Quotes

1. "Every single penny will go to the number one and the only job of the RNC, that is electing Donald J. Trump as president of the United States."
2. "They're worried that Trump's chosen leadership team might just turn the RNC into a piggy bank for Trump."
3. "Y'all have a good day."

### Oneliner

The RNC faces potential leadership changes with concerns over misuse of funds to solely support Trump's presidency.

### Audience

Republican Party members

### On-the-ground actions from transcript

- Monitor and hold accountable the use of funds within the RNC (suggested).
- Advocate for a balanced approach to RNC responsibilities beyond supporting a single candidate (implied).
- Support transparency and financial responsibility within the party apparatus (implied).

### Whats missing in summary

Insights into the potential long-term implications of the RNC's financial decisions and leadership choices.

### Tags

#RNC #RepublicanParty #LeadershipChanges #Trump #MisuseOfFunds


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk a little bit more
about the RNC, the leadership changes that
might be going on there.
And we're going to talk about some of the things
that Republicans have brought up that they may
be a little worried about.
And we're going to talk about a quote that is certainly not
going to ease those particular concerns in any way, shape, or form.
Okay, so if you have missed it, don't know what's going on.
The RNC, that's the Republican National Committee, this is the national level Republican party
apparatus.
It looks like there's going to be some leadership changes, let's just call it.
McDaniel looks like is out, and Trump has provided suggestions as to who should take
over key spots, one of which is Laura Trump.
Now generally speaking, it looks like a lot of Republicans are relatively supportive of
a lot of Trump's suggestions on this one.
At the same time, a number have expressed concerns.
Some publicly it's in reporting and then some privately that has also made it into reporting
that they're worried that Trump's chosen leadership team might just turn the RNC into
a piggy bank for Trump and have the Republican National Committee paying Trump's legal bills,
stuff like that.
They don't think that that's a good idea.
They feel like that might be an inappropriate use of the funds.
The RNC does actually have way more to do than just support a presidential candidate.
So the quote that is sure to come up over and over again in the future, at least until
the leadership situation is resolved and then it will probably come up again after that
because this is going to be one of those moments where it's not like you knew I was a scorpion
when you picked me up.
This is a quote from Laura Trump, every single penny will go to the number one and the only
job of the RNC, that is electing Donald J. Trump as president of the United States.
I mean, I'm going to suggest that, yeah, I mean, anything that Trump needs, the RNC
may end up spending that money on.
Maybe that includes legal bills.
I feel like the Republican party should see this coming, and there are some that apparently
do and they're talking about it, but I don't know that it's in enough numbers to derail
Trump's picks from getting those spots.
And then later, there's going to be an issue.
There's going to be some confusion as the RNC runs low on funds.
We have seen at the state level how a lot of the Republican Party apparatus at the state
level has just not done well with finances when pursuing certain objectives that are
MAGA related.
the RNC may be headed the same direction.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}