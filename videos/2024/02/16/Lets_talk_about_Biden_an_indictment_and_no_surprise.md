---
title: Let's talk about Biden, an indictment, and no surprise....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=E1vu7z7BeWc) |
| Published | 2024/02/16 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the recent developments involving the special counsel appointed by Trump who indicted a person for making false statements due to bias against Biden.
- Recalls discussing the FD-1023 form months ago, which contained unverified information about bribery, and how the Republican Party wanted to use it to mislead their base.
- Shares skepticism about the FD-1023 and expresses hope that the FBI, tired of political manipulation, will release determination memos proving the information false.
- Questions why the FBI had not released the determination memos yet and later discovers they were using them to build a criminal case.
- Points out how the media was aware that the FD-1023 allegations lacked credibility but still treated them as real, leading to a narrative now called into question.
- References an old video detailing a Ukraine-Biden timeline and journalism principles, stressing that despite new allegations, nothing has changed from when the video was first created.
- Emphasizes the need for evidence to change the narrative instead of relying on speculation or rumors.

### Quotes

1. "If you want to change that story, you need evidence, not any window."
2. "Years ago, I did a video, Journalism 101 and a Ukraine-Biden timeline or something like that."
3. "So without this information without these allegations that are now definitely under question, where does the storyline rest?"
4. "They treated it as if it was real."
5. "Y'all have a good day."

### Oneliner

Beau explains recent developments involving biased allegations against Biden, the misleading use of the FD-1023 form, and the importance of evidence to alter the narrative.

### Audience

Journalists, Fact-Checkers

### On-the-ground actions from transcript

- Examine and fact-check information before sharing or reporting (implied).
- Seek evidence-based narratives rather than speculation (implied).

### Whats missing in summary

In-depth analysis and context on the political manipulation of information and the impact on public perception.

### Tags

#Biden #SpecialCounsel #FD1023 #Media #Journalism


## Transcript
Well, howdy there, internet people.
It's Bob again.
So today we are going to talk about some news.
We're gonna talk about Biden.
And we're gonna talk about things
that have been said in the past.
We're gonna talk about forms.
We're gonna talk about the FD 1023.
Do y'all remember that?
Super important a few months ago.
We're gonna talk about that.
And we will talk about some old videos
and an even older video.
And we'll just kind of run through everything because of the newest developments.
Now, if you have missed it and you don't know what's going on, the special counsel, the
Trump appointee special counsel, the one that has previously filed charges against Hunter
Biden, that one, indicted the person who made all of the allegations about the bribery scheme
involving Burisma and all of that stuff that involved Hunter Biden and his dad,
you know, the president. That person was indicted for making false statements
and according to the allegations did so because they have a bias against Biden.
Yeah, so who could have seen that coming? Anybody who's paying attention.
Six to eight months ago, did a series of videos talking about the FD-1023s. I'll put a couple of them down below.
The FD-1023 was a form the Republican Party wanted really, really bad. They wanted it, they needed it, they needed to
get it and put it out there.  because it had allegations of bribery. And we went over how the FD-1023 is a
form used to record unverified, unvetted information. There's nothing saying that
anything on an FD-1023 is true, but that's what the Republican Party wanted
because they wanted to be able to hold it up there with the DOJ logo on it and
trick their base. Went through it at the time, expressing my skepticism. Also, at
one point, said that I was hoping the FBI was just tired of being used
politically like this, and that they were waiting for the Republican Party to go
all in and make all of these claims, and then they were going to release the
determination memos. Because after an FD-1023 is created, sometimes the the
feds will go through and try and verify and vet that information and they have
other memos. And I was hoping that the FBI, tired of being politicized the way
the Republican Party is intent on doing, they were gonna let them use that FD-1023
1023 and then released the determination memos that determined the
information to be false. Because at the time when they were talking about it, it
was pretty clear that the information was false. I have to be honest though,
about a month ago, maybe two, I started thinking about it for whatever reason
across my mind again and I was like, man, I wonder why they haven't released those
memos yet. And I started to second-guess myself. Well now we know they didn't
release those memos because they were busy using them to build a criminal
case. So those videos will be down below. You can go through them and look. It also
details how the media knew what an FD-1023 was. They knew these allegations
didn't stand up to scrutiny. They knew that just as a journalist there was no
way they would be able to publish a story based on the information contained
in that FD-1023, but they treated it as if it was real. So without this
information without these allegations that are now definitely under question, where does the
storyline rest? Where does everything go now that this allegation is gone? It goes back to that first
video. Years ago, I did a video, Journalism 101 and a Ukraine-Biden timeline or something like that.
I'll put it down below. That video is years old, despite all of the allegations, all of the promises
of new evidence that the Republican Party has made. Nothing has changed. Everything is exactly
the same as it was when that video was made.
If you want to change that story, you need evidence, not any window.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}