---
title: Let's talk about a Genie in Washington....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=_09Huoeno6I) |
| Published | 2024/02/04 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Washington State and Ohio involved in a bizarre news development involving a rocket-like object.
- A person in Washington contacted the National Museum of the Air Force in Ohio about a military-grade rocket found in an estate.
- The rocket turns out to be an Air 2 Genie, a 1950s unguided rocket with a 1.5 kiloton nuclear weapon designed to shoot down planes.
- The situation led to the local sheriff's department sending out bomb technicians to inspect the rocket.
- Despite the initial unnerving discovery, it was clarified that there was no warhead attached to the rocket, just a piece of historical military equipment.
- The EOD team questioned the necessity of sending a press release about the rocket since it posed no actual danger.
- The Air 2 Genie is a rare historical item, with only a few examples existing in museums due to its unique design for carrying a nuclear weapon.
- The situation was resolved without any risk of a lost nuclear weapon, only the rocket itself.
- The historical context of the Air 2 Genie showcases the 1950s era's approach of putting nuclear weapons on various military systems.
- The incident served as an interesting, albeit potentially unsettling, event involving Cold War-era military technology.

### Quotes

- "You know, at the end of the day here, we have a hunk of rusting metal."
- "That situation has been resolved. There was no lost nuclear weapon, just the rocket that would carry one."

### Oneliner

Washington State and Ohio involved in a bizarre incident with a 1950s military rocket carrying a nuclear weapon, posing no actual danger.

### Audience

History enthusiasts

### On-the-ground actions from transcript

- Contact local museums to learn more about historical military artifacts (suggested)
- Support efforts to preserve and display rare historical items like the Air 2 Genie (implied)

### Whats missing in summary

The detailed historical context and significance of the Air 2 Genie in military technology during the Cold War era.

### Tags

#History #MilitaryTechnology #ColdWar #NuclearWeapons #MuseumCollections


## Transcript
Well, howdy there internet people, it's Beau again. So today we are going to talk about Washington, Washington State,
and
Ohio, and a genie, and a wish,
and a bizarre
development in the news. Something that I'm almost considered just holding to put it in the oddities section over on the
roads not taken.  But, I figure this will give everybody a good laugh and maybe a scare at the same time.
Okay, so the story starts in Washington.
A person in Washington called the National Museum of the Air Force in Ohio was like,
hey, you know, there's this person recently departed in their estate, there's this thing
it looks like a rocket, some kind of military-grade rocket, maybe y'all want it.
So the museum calls back to Washington, and the local sheriff's department or law enforcement
agency there, they send out their EOD texts, their bomb texts, and their bomb texts find
an Air 2 Genie, which was probably a little unnerving at first.
Air 2 was made by Douglas. It shows you how old it is. Made by Douglas. That's
before Douglas merged and became McDonnell Douglas. So this is from the
50s. And the Genie is a unique item. Not a lot of, not a lot of weapon systems
were made in this style. See, during this period there was a a large concern
about Soviet bombers flying over the United States, you know. So they wanted
something simple that would be able to knock them out of the sky and that was
the Genie. It was an unguided rocket that had a 1.5 kiloton nuclear weapon on the
end of it. An air-to-air nuke. A nuclear weapon to shoot down planes. Yeah so
obviously I guess they were a little unnerved at first. There was no warhead
attached to the rocket. It was just the rocket. So at the end there was no real
danger, and the EOD team was like, why are you even sending a press release out about this?
You know, at the end of the day here, we have a hunk of rusting metal. Which, I mean, yeah,
that's one way to look at it. The other thing is these things are so rare that if you were to
go to Wikipedia you would find that they actually list out the museums that have
an example of this because this was obviously a unique idea putting a nuke
on an air-to-air system you know it's the 50s put a nuke on everything
anything, artillery, whatever, didn't matter. But, so, that has, that situation has been
resolved. There was no lost nuclear weapon, just the rocket that would carry one.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}