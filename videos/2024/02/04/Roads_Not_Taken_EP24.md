---
title: Roads Not Taken EP24
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=qh_mIGHVtV4) |
| Published | 2024/02/04 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Provides a weekly overview of global events, offering insights on unreported or underreported news.
- Mentions the US approving a $4 billion drone sale to India for maritime security.
- Notes the US and UK targeting Houthi targets in Yemen, separate from strikes in Iraq and Syria.
- Emphasizes the importance of understanding distinct conflicts and tailoring strategies accordingly.
- Reports on journalists being arrested at an anti-war event in Moscow.
- Comments on aid for Ukraine and Republican disarray halting aid from the US.
- Mentions a disturbing incident where a man fled after allegedly killing his father and attempting to rally the National Guard against the government.
- Talks about House Republicans pushing for more military aid to Israel.
- Reports on Connecticut canceling medical debt and an earthquake in Oklahoma.
- Covers cultural news like Nikki Haley's cameo on Saturday Night Live.
- Mentions Greta Thunberg's climate demonstration charges being dismissed and a Space Force officer heading to the International Space Station.
- Addresses conspiracy theories involving Taylor Swift and Alina Habba.
- Talks about canceling a plan to renovate one of the pyramids due to international outcry.
- Responds to questions regarding the use of long-range bombers, ideological divides between young men and women, and pronunciation of Iran and Iraq.
- Mentions a suggestion for movie commentary videos.

### Quotes

- "It's worth noting that these are really, these are different conflicts."
- "These politicians are putting out incredibly dangerous rhetoric."
- "It's just something they say because they're paranoid and angry and don't have a lot of common sense."
- "A little bit more information, a little bit more context, and having the information will make all the difference."
- "Y'all have a good day."

### Oneliner

Beau gives insights on global events, conflicts, aid, dangerous rhetoric, conspiracy theories, and movie commentary suggestions, advocating for context to make a difference.

### Audience

Global citizens

### On-the-ground actions from transcript

- Contact local representatives to advocate for nuanced responses to conflicts (implied)
- Support anti-war events and advocate for peace (exemplified)
- Stay informed about global aid efforts and political decisions (exemplified)

### Whats missing in summary

Beau's engaging delivery and detailed analysis make understanding global events more accessible and impactful.

### Tags

#GlobalEvents #ConflictAnalysis #AidEfforts #ConspiracyTheories #CommunityEngagement


## Transcript
Well, howdy there, internet people, it's Beau again,
and welcome to The Roads with Beau.
Today is February 4th, 2024,
and this is episode 24 of The Road's Not Taken,
a weekly series where we go through
the previous week's events and talk about news
that was unreported, underreported,
didn't get the context it deserves,
or I just found it interesting.
Afterward, we will go through some questions from y'all.
Okay, and starting off with foreign policy, we, what do we have here, okay, so the U.S.
approved a $4 billion drone sale to India, and they are armed drones, and it looks like
they will be used for maritime security.
The United States and the United Kingdom hit a slate of 30 Houthi targets in Yemen the
day after hitting a bunch of targets in Iraq and Syria.
It's worth noting that these are really, these are different conflicts.
They are different conflicts, regardless of how much the media is going to try to blend
them together for the sake of an easy narrative, these are actually different things.
And it gets even more confusing when you think about the fact that they're both backed by
the same state actor, but these are different non-state actors that have different goals.
And it's important to keep that in mind because the strategies used to deal with each conflict
should be different and they're not.
The responses in Iraq and Syria from a foreign policy standpoint, those actually make sense.
The same strategy of the strikes being used against the Houthis kind of don't make sense
at this point.
They're not incredibly effective.
They're not sending the message that they should.
It's important to understand the dynamics because the responses should be different.
and journalists were arrested in Moscow at a demonstration near the Kremlin.
The demonstration was an anti-war event put on by the wives of soldiers who are opposed
to the mobilization.
The EU is coming across with about $54 billion in aid for Ukraine.
Meanwhile the United States is still not moving on aid because of Republican disarray in the
House.
Moving on to U.S. news.
The man who allegedly killed his father and took his head and posted the video to YouTube,
when he fled, he fled to a National Guard installation, climbed the fence, and he was
taken into custody without incident there, but it's worth acknowledging the reason he
went there.
He went there, according to the allegations, to try to rally the National Guard to take
up arms and fight against the feds.
Where have we heard that rhetoric lately?
These politicians are putting out incredibly dangerous rhetoric.
It really needs to stop.
The House Republicans are pushing forward with a bill for more military aid to Israel.
Connecticut will cancel roughly $650 million in medical debt for an estimated quarter of
a million people, and that's supposed to happen this year.
The Oklahoma had a decent sized earthquake, don't have a whole lot of information on that
so far.
In cultural news, Nikki Haley did a cameo on Saturday Night Live where she was asking
questions of a fake Trump candidate.
She did manage to provide an updated answer to the question about what the Civil War was
about and did acknowledge that it had something to do with slavery, which I mean there's that
I guess.
In environmental and science news, Greta Thunberg's climate demonstration charges have been tossed
by a judge in the UK, and then a Space Force colonel will be the first Space Force officer
to go into space when hitches a ride with NASA on a NASA mission up to the International
Space Station.
In oddities, Taylor Swift has been accused of being a psychological operation for the
Pentagon by prominent right-wingers again, yes, again, but now it's the Pentagon, not
the CIA, or maybe there's a Pentagon last time in the CIA now.
Either way, that has slightly evolved and is continuing on.
Obviously there is no logic or even explanation behind it.
It's just something they say because they're paranoid and angry and don't have a lot of
common sense. In related news, Alina Habba has been called a deep state plant by right-wing
commenters, because of her performance in the former president's legal entanglements.
The plan to renovate one of the pyramids has been rolled back.
the outcry, it looks like that's coming to a stop. If you don't know what I'm talking about,
one of the, I guess it's the smallest of the main pyramids at Giza,
there was a plan to kind of renovate the outside of it and kind of re-coat it
it, with some rocks that were nearby.
A lot of archaeologists had questions about how it was going to be done.
The whole project seemed to have been rolled back at the moment.
There will be more details coming, but the international outcry definitely appears to
have had at least some impact.
OK, moving on to the Q&A. And the email for this
is questionforboe at Gmail.
OK.
Why did they use long-range bombers
when the carriers are there?
There's a whole bunch of reasons for that.
But the one that is probably most pressing
and the one that wouldn't change based on scenario
is, generally, you don't want to use the stuff on the carriers.
You want to bring stuff in, the stuff on the carriers
is in case there's a response.
So you don't want to use those resources if you don't have to.
Oh, that's a command, not a question.
Do a dating slash relationship Q&A for Valentine's Day.
Those are your best videos.
The dating and relationship Q&As are my best videos.
I really hope that's not true.
OK.
I'm assuming that you've seen those charts from a poll
showing an increasing ideological divide between women
and men aged 18 to 29.
OK, and do you notice anything about these polls that allows
you to not be too concerned?
Yes, I think people are misreading them.
OK, so short version, if you haven't heard about this,
there is some polling that suggests, I guess,
one way to read it is that young men are
becoming more conservative than young women.
That is a way to read it.
I don't know that it's the correct way to read it.
That is definitely the way to write it
if you want really good headlines.
But if you get into it and you really look at what was said,
that's kind of not what's there.
The biggest group describe themselves as moderates,
not as conservatives. And when they were asked, the men didn't have any particular
issues that they really felt strongly about, more of an identity. It seems as though young
men are latching onto, I'm a conservative, because they don't have an identity, they
They don't have anything else, and that is something that they can claim to be rather
than have any defined principles.
When you look to the women, what people are seeing is that there's, I don't remember
the exact numbers, but let's say it's 40% of women identified as liberal, of young
women identified as liberal, and it's only 23% of young men.
I mean yeah, that kind of makes sense though, doesn't it?
If the largest group of young men is moderates, and that's making up almost 50% of it, then
Yeah, there should be a split on each side of that,
some conservative, some liberal.
And it would make sense that more women,
more young women view themselves as liberal
because they've been pushed there
because the government's trying to take away their rights
and is successfully doing so in a number of states.
So they have more reason to become politically involved
because they themselves are the targets.
I mean, that's kind of like being surprised
that people that are LGBTQ are politically active.
It's one of those things, like, once you actually
become a target of the legislation,
you tend to be more involved.
So I don't know that there's actually any real surprises
there other than, yes, the gap is larger.
The trend of women being more liberal,
that's kind of always been there.
It's just a bigger gap this time.
But I mean, that tracks.
That makes sense.
So I don't really see a whole lot in that to be concerned
about.
Why do you say Iran and Iraq like a British person instead of an American person?
So yeah, I say Iran the way I do because I dated a woman from Tehran when I was younger
and she did not like Iran.
That's why.
Yeah, so, I didn't know that was a British pronunciation.
Okay, I think it would be neat if you reviewed movies that have applicable social commentary
and point out the things you pick up on, maybe a Discord, Patreon exclusive, if that makes
you more comfortable.
This is way outside of your normal content.
But I think maybe one of these every so often
would be a good idea for reference watching
RoboCop 1987 right now.
I mean, that sounds fun.
That's one of those things that I would file under, like,
good idea, not enough time.
We have a lot going on that we're just
trying to wrap up at the moment but I mean doing something like this I mean I
wouldn't I wouldn't say no in fact we did something like this with at the
beginning of the the pandemic we watched outbreak we did like a live stream and
watched Outbreak. I don't know if y'all remember that. Okay, so that looks like it.
That looks like all the questions. Okay, so alright, so that's it. So there you
have it. A little bit more information, a little bit more context, and having the
information will make all the difference. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}