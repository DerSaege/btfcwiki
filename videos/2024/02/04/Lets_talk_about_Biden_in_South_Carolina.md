---
title: Let's talk about Biden in South Carolina...
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=XmBooKocyew) |
| Published | 2024/02/04 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Explains the surprising outcome of the South Carolina Democratic primary, where Biden won by a landslide.
- Contrasts the commentary with the actual results, pointing out that Biden's success was unexpected based on commentary about key demographics.
- Clarifies that leftist commentary criticizing Biden did not impact the primary results significantly.
- Notes that South Carolina is not a stronghold of leftism and leftists generally aren't heavily involved in primaries.
- Points out that Arab Americans, another demographic upset with Biden, did not have a significant impact on the primary due to low turnout.
- Emphasizes that liberals, not leftists, showed strong support for Biden in the primary.
- Predicts that future Democratic primaries in more left-leaning states may have different outcomes.
- Advises not to be misled by overrepresentation of leftist commentary compared to their actual percentage within the Democratic coalition.
- Stresses the importance of Biden retaining support from leftists and Arab Americans for the general election against Republican nominees like Haley or Trump.

### Quotes

- "Why did Biden do so well? Because it was a primary of liberals."
- "Liberals are united behind Biden."
- "The demographics that have problems with Biden, that have policy differences, or in some cases moral differences, they're not really represented in South Carolina."
- "Don't get caught up in the fact that leftist commentary is overrepresented."
- "Biden is going to need the two or three points from the leftists and the two or three points from the Arab Americans."

### Oneliner

Beau explains the surprising success of Biden in the South Carolina Democratic primary, attributing it to liberal support rather than leftist commentary.

### Audience

Democratic voters

### On-the-ground actions from transcript

- Organize efforts to involve leftists in primary elections (exemplified)
- Ensure support for Democratic candidates from diverse demographics for general elections (exemplified)

### Whats missing in summary

Insight into the potential impact of different demographics on future Democratic primaries.

### Tags

#SouthCarolina #DemocraticPrimary #Biden #LiberalSupport #LeftistCommentary


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about South Carolina and Biden and the
primary and the results, what happened, because there are a lot of
surprised people right now.
So we're going to talk about the commentary and why it didn't exactly
line up with what occurred, because as soon as those results started coming
out, questions started pouring in.
It was all pretty much the same question, how did Biden do
so well because all of the commentary was saying that he was going to do poorly because he was
missing out on a couple of key demographics and that certainly did not occur. Okay, so if you
missed the news, don't know what happened. South Carolina, the Democratic primary went ahead and
Biden won in a landslide.
We're talking like 96% of the vote.
The next closest contestant was Marianne Williamson, with right about 2%.
Okay.
So how did that happen?
Because if you've been paying attention to commentary, Biden is losing key segments.
There are demographics that are very upset with him and they're not going to support
him.
That's not bad commentary.
That's true.
It just doesn't have anything to do with the primary.
Think about who those demographics are.
Let's start with who was making the commentary.
Leftists, right?
or progressives, some of which are... they're really leftists that don't want to admit it,
so they call themselves progressives. That's where that commentary came from.
They are upset with Biden. Yes, that's true. You have two things at play and why it didn't matter.
First, South Carolina is not actually known as a bastion of leftism. There's not a whole lot of
them there. There are some, but they're around the universities and there's not many. The
other thing is that generally speaking, leftists don't get involved in the primaries. There
are some organizations that are trying to change that, and I personally think they're
definitely on the right track. One of the reasons leftists don't have candidates they
like during the general is because they're not involved in the primary.
So even though there's an attempt to alter that, as it stands right now, generally speaking,
leftists are involved in the general election.
And it boils down to you either get the vote or you don't.
are, they're ideologues. They're not exactly pragmatic. So there's a, there is very much a
we're with you or against you thing. But that doesn't show up in the primary. So it didn't
impact to those numbers at all. The other demographic, Arab Americans. Now, there's
actually more in South Carolina than you might imagine, but still not enough to
make a huge difference. The other thing to keep in mind is that they may not
have shown up either. They may not have decided to show up and vote for different
candidate. They just didn't show up. So you just didn't have the demographics that
are upset. They didn't have an impact in this primary. What it showed is that
liberals, not leftists, liberals are firmly united behind Biden. That's what
it shows. More so than the Republican Party is behind Trump. Trump would
love to be able to pull 90% in a primary. He can't, because he's not that popular.
Now, does that mean that the rest of the Democratic primaries are going to look the same way?
No, I don't think so, because I think when you get to states that are that are a little bit more left,
California, as an example, I think that the numbers will show there. Probably show
around the Great Lakes as well, but at the same time I imagine it'll still be
in the 90s, just not high 90s. The other thing to keep in mind is that the people
People who make commentary about this, they're left.
Or they say they're progressive, but that's just to hide the fact that they're left.
Don't get caught up in the fact that leftist commentary is overrepresented.
you are talking about the percentage of people within the Democratic coalition
that are leftist, it's not much. It's like maybe 5%. But when you get to the
commentators, it's probably 50%. So it seems like it's a bigger chunk
of the Democratic Party than it is. It's worth remembering that Biden can't lose
that 5% though. He's going to need that when it comes to the general. If Haley
is the Republican nominee. If Trump is the Republican nominee, I don't think
that there's much that... I don't think there's much Biden could do that would
create a situation where he loses. Not at this point. If it's Haley or anybody
else really, Biden is going to need the two or three points from the leftists
and the two or three points from the Arab Americans. Gonna need that in the
general. But we don't know how that's going to shape up yet. The answer to the
question, why did Biden do so well? Because it was a primary of liberals.
Liberals are united behind Biden. It's leftists. It's other demographics that
are more progressive than liberals that have an issue. And that's where it is. The demographics
that have problems with Biden, that have policy differences, or in some cases moral differences,
they're not really represented in South Carolina, so it didn't show up there. When it does show
up. I don't think it's going to be dramatic though. I think that's been overstated in
the commentary.
So anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}