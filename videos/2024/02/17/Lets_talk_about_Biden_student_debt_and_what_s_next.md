---
title: Let's talk about Biden, student debt, and what's next....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=MHOV5qOwJe8) |
| Published | 2024/02/17 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden administration's attempt at student debt relief, after previous failure.
- Over $125 billion forgiven for 3.5 million people, but falls short of promises.
- New plan includes three classes of debt relief.
- First class involves resetting interest rates to original balance.
- Second class forgives old loans after a certain period of payment.
- The most impactful class focuses on forgiving loans for those likely to default in the next two years.
- Uncertainty about the number of people impacted by the new plan.
- More classes of debt relief rumored to be coming.
- Anticipated legal challenges to the new plan.
- Efforts to make the plan expansive while staying within legal boundaries.

### Quotes

1. "Over $125 billion forgiven for 3.5 million people, but falls short of promises."
2. "New plan includes three classes of debt relief."
3. "The most impactful class focuses on forgiving loans for those likely to default in the next two years."

### Oneliner

Biden administration's attempt at student debt relief falls short of promises, introducing three classes of debt relief, with focus on future loan defaulters.

### Audience

Students, debt holders

### On-the-ground actions from transcript

- Stay informed about updates regarding student debt relief plans (implied)
- Advocate for comprehensive and impactful student debt relief measures (implied)

### Whats missing in summary

Details on the specific eligibility criteria for each class of student debt relief.

### Tags

#StudentDebtRelief #BidenAdministration #DebtForgiveness #LegalChallenges #FinancialAid


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about the Biden administration
and student debt relief, student debt forgiveness.
And what appears to be,
looks like maybe Biden's attempt
for a second bite at the apple, trying again.
So if you remember,
Biden had a large student debt forgiveness thing,
went to the Supreme Court, got shot down.
Now, there's been a bunch of small stuff done
over the course of the administration that has added up.
Thus far, the Biden administration has forgiven
more than $125 billion in student debt relief
for three and a half million people.
Those are huge numbers, but it's not what he promised.
There was a lot more on the table is what he was going for.
That was a goal and attempted to get,
but didn't succeed.
So what's the new plan?
Three different classes that I have seen so far,
there's probably more based on what I've heard.
The first is something that amounts
to just resetting the interest.
So it could be $10,000, $20,000.
The idea is to get people who have been paying
high-interest loans, you know, those people, you hear the stories, they've
been paying on it for five years and they owe more than they borrowed.
Basically to reset the interest so their original balance is what they're paying
on. That's one of them. Another one is for old loans. Let's say you've been
paying on it for 20 years. Well, it just goes away. And then the big one, the one
that I think is probably going to be most effective, although when pressed
about it, the White House couldn't give a number on how many people this would
actually impact. When you look through the criteria and how it could be applied,
even just taking a middle of the road, I feel like this is going to be a big one.
is hardship. And it's basically forgiving the loans of those people who
are highly likely to default in the next two years. There's a bunch of different
criteria and a bunch of different ways that could be figured out. Now again
there's supposed to be more. I just don't know what they are yet. There have been
different rumors about different things and it is starting to leak out. My guess
is that by the time this video goes out with the way news keeps you breaking,
there's gonna be more information, there'll be more classes that people are
talking about, but it's shaping up to be another attempt. Now please understand,
this is gonna get announced, it'll get signed, it'll get pushed out there as
as some kind of executive order, some kind of rule change, and then there's going to be a legal
challenge. It looks like they tried to tailor this to be as expansive as possible while still
being within the previous ruling. So we'll have to wait and see how it plays out, but this is not
something that was just forgotten about. It's still being worked on. Anyway, it's
It's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}