---
title: Let's talk about a gofundme for Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=wSGNVliR4yA) |
| Published | 2024/02/17 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former President of the United States faced news from New York, seeking $355 million from him.
- A GoFundMe has been created to assist the former president in covering this amount.
- The GoFundMe had raised nearly $28,000 at the time of filming.
- Beau questions our ability to grasp the significance of large numbers like $355 million.
- To contextualize, he breaks down how long it'd take an average worker earning $15/hour to raise $355 million.
- If that average worker started in 678 BC, working 24/7 without breaks, they still wouldn't have raised the full amount.
- Trump's ability to fundraise over the years despite the staggering amount of $355 million is remarked upon.
- The mentioned $355 million is just a fraction of Trump's total worth, according to allegations.
- Beau expresses his curiosity about the outcome of the GoFundMe and leaves with a closing thought.

### Quotes

1. "We don't know what those numbers mean."
2. "When they get that big, average people, we have no way to visualize or have a concept of what that means, $355 million."
3. "If that person wanted to raise the $355 million dollars, How long would it take them? 23,666,666 hours and change."
4. "678 BC. It's strange to me how somebody who has the resources that Trump does was able to spend eight years or so fundraising the way he has been."
5. "I'm very curious to see how the GoFundMe plays out."

### Oneliner

Former President faces $355 million judgment in NY. GoFundMe set up, but can average people grasp such numbers? Beau breaks it down, leaving us curious about the outcome.

### Audience

Social Media Users

### On-the-ground actions from transcript

- Donate to the GoFundMe for the former president (suggested).

### Whats missing in summary

The full transcript provides a detailed breakdown of the challenges in comprehending large numbers and the context around a GoFundMe for the former president, offering insights into financial scale and fundraising efforts.

### Tags

#Numbers #GoFundMe #Trump #Finance #Curiosity


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about something
that was a joke, at least it started as a joke.
People used to talk about it.
And when they talked about it before, it was a joke.
But now that it's happening, we're
going to talk about it, because the internet is a very, very
wonderful and strange place.
But before we get into what's happening,
we have to catch anybody up that missed the news. The former president of the
United States got some news from New York, was not good news for him. The
judgment came down and New York wants around $355 million from him. Now it's
there there's more math that goes into it but we're just gonna stick with the
$355 million because that's what the GoFundMe is for. Not a joke.
Not a joke, a GoFundMe has been set up to benefit the former president of the United States
to help offset this cause, I guess.
And the cost associated with this
for $355 million. At time of filming, it was closing in on $28,000 raised.
And there is tons of commentary that can be made about this.
I mean just piles and piles, and I am sure that people that are far funnier than I am are going to just
have content for weeks based on this. But for me, this has confirmed something that I have believed for a really really
long time.  And that's that
we don't know what those numbers mean.
When they get that big,
average people, we have no way to visualize
or have a concept of what that means, $355 million. We don't have a clue
because I, no doubt, the people behind us, they're looking at that and they're like
$28,000. We are off to a great start. So what we're going to do to put this
into normal people terms. We're gonna take that average, you know, Trump
supporter that you see in the commercials. The one that's like, we don't need to
raise the minimum wage, those people deserve to make that much. That person.
He's making about $15 an hour. If that person wanted to raise the $355
million dollars. How long would it take them? 23,666,666 hours
and change. That's what it would take. But see the thing is, that number is so big
we have no way to visualize it, to have a concept of it. So let's do it a
different way. If the person wanted to raise that money they would have had to
start working in the year 678 BC that's when they would have had to start
working at $15 an hour and it's worth noting that when I did the math on this
I just assumed that this person was super dedicated to Trump and that they
were gonna work 24 hours a day 7 days a week they weren't even gonna take off
Christmas. 678 BC. It's strange to me how somebody who has the resources that
Trump does was able to spend eight years or so fundraising the way he has been.
$6.78 BC, that's a lot of time at $15 an hour.
It's worth noting that according to what has been alleged, that $355 million, that's just
a fraction of what Trump's worth.
I'm very curious to see how the GoFundMe plays out.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}