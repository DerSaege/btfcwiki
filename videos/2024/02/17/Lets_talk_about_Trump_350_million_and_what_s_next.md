---
title: Let's talk about Trump, $350 million, and what's next....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=33NKNRXYmT4) |
| Published | 2024/02/17 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the judgment against Trump in New York, including a $350 million penalty and a three-year ban on holding certain positions.
- Points out that the judge emphasized that Trump and his team did not commit extreme crimes like murder or robbery, but their refusal to admit error led to increased penalties.
- Notes that Trump cannot appeal the judgment, which may impact his ability to do business in New York.
- Predicts that the financial losses and legal troubles will further stress Trump, leading to erratic behavior and blaming others for his problems.
- Suggests that Trump's downfall may involve turning against his closest allies, including Republicans and his own supporters.

### Quotes

1. "Defendants did not commit murder or arson. They did not rob a bank at gunpoint."
2. "The penalties were substantially increased because of [Trump's] refusal to admit error."
3. "This is the beginning of his downfall."
4. "He is racking up loss after loss after loss and he knows it."
5. "Eventually I feel like he's going to turn on his base."

### Oneliner

Trump's legal troubles and financial losses may trigger erratic behavior and blame-shifting, potentially leading to his downfall and turning against his supporters.

### Audience

Political observers

### On-the-ground actions from transcript

- Monitor and support individuals affected by Trump's erratic behavior and potential blame-shifting (implied)
- Stay informed about the legal developments and financial losses affecting Trump (implied)

### Whats missing in summary

Analysis of potential impacts on Trump's future business ventures and political career.

### Tags

#Trump #LegalTroubles #FinancialLosses #Downfall #ErraticBehavior #BlameShifting


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump and that judgment and where
everything goes from here.
We're going to talk about what the judgment is, where it goes from here,
what it means for Trump's future campaign, and I'm going to read
something from the judge that I think is incredibly important, especially
if you are a supporter of Trump's.
If you support Trump, you're going to get one of those fundraising messages.
I think you need to hear this.
Okay, so where does it start?
The judgment itself, right, for those people who don't know, $350 million more than.
That's the judgment.
A three-year ban on being an officer or director in any legal entity in New York, and the finances
will fall under the purview of an independent monitor.
Almost everything that the attorney general asked for.
Not quite everything, but almost.
This is something that the judge said that I think is incredibly important.
Defendants did not commit murder or arson.
They did not rob a bank at gunpoint.
Donald Trump is not Bernard Madoff, yet defendants are incapable of admitting the error of their
ways.
Defendants' refusal to admit error, indeed to continue it according to the independent
monitor constrains this court to conclude that they will engage in it going forward
unless judicially restrained.
Now when I read it, I read what I just said aloud, but I hear something else.
I hear the judge saying, if they had just walked in here and said, our bad judge, we
know. We'll stop. It probably would have been a 10 million dollar fine and
probably would have come under an independent moniker. That's what it seems
like. It seems like Trump going out and saying, I did nothing wrong, we did
nothing wrong, it was perfect, everything was fine. My way of reading this, and I
could be wrong, this is my personal impression, the penalties were
substantially increased because of that. So I think that's something worth
acknowledging as you read a fundraising email that will no doubt attack the
judge and say that he did nothing wrong. Okay, so where's it go from here? Trump
can't appeal. There's going to be issues with that because of the inability to do
business in New York. Of course, the Trump legal team is acting very defiant.
They're acting like they won in a weird way, kind of. They're acknowledging the
loss, but still saying that they know they're going to prevail in the end.
Okay, so, what does it mean for Trump's campaign?
This on top of another 80 plus million dollar loss, so we're talking about more
than $400 million at that point, I think it's probably going to start to shake
Trump even more. We've talked about it before. He is becoming more and more
erratic as time goes on. The stress and the pressure is getting to him. He's
starting to nicky-nancy his way through a whole lot of things. He is not capable
of putting on the performances that he once was. He's mumbling. He's not
coherent and that's it's the pressure. Now I would imagine that after the E. Jean
Carroll case and now this one, no matter what he puts to the public, no matter his
public face, he has to start worrying about those criminal cases because the
same people that told him, oh, you're gonna be fine with these that he lost.
They're probably telling them the same thing about those criminal cases.
The pressure is going to build and honestly, I don't think Trump's up to it.
I think he is going to become more and more erratic to the point where he
starts blaming those people closest to him. Starts pointing the finger at
anybody but himself. And I know that that's been common practice for a
really long time, but I'm talking about to a much more pronounced
degree. He might even start, you know, ordering people to take care of things
that he fired in the past. Picture him screaming, room full of people, blaming
people who aren't there anymore. This is the beginning of his downfall.
fall. I feel like if he continues to be in the public the way he has been, it's
going to come through. Now the alternative to this is that he becomes
more private. I don't think that's going to be the case, but it's something that
could happen and it's also possible that I'm totally wrong and he being the
brilliant business tycoon that he is. He's going to be able to handle this
pressure with no issues, but I feel like this is going to start to weigh pretty
heavily on him and he's probably going to start to show it and those
people who will bear the brunt of his attacks, oh they're not going to be the
Democratic Party. It's going to be Republicans who he feels let him down,
who he feels didn't use all of their power to save him. And eventually I feel
like he's going to turn on his base. Those people who didn't give him enough
money, they will eventually be to blame too. Again, it's all my opinion, but
But that's why I see this heading.
He is racking up loss after loss after loss and he knows it and he knows that it's all
very public.
Anyway it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}