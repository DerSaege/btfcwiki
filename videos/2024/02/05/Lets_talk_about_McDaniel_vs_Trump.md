---
title: Let's talk about McDaniel vs Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=eSGQa2y4tkE) |
| Published | 2024/02/05 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump and McDaniel have been friendly for quite some time, but now there is a shift in their dynamic.
- McDaniel, the chair of the RNC since 2017, followed Trump's lead which led to his protection of her.
- The base is starting to blame McDaniel for fundraising issues and election results since 2017.
- Trump is now indicating that changes may be needed, signaling a change in his relationship with McDaniel.
- Despite issues, Trump still holds influence as his followers continue to obey him.
- The base is critical of McDaniel for fundraising problems, but Trump's separate fundraising efforts undermined the party's funding.
- Trump's control over talking points and rhetoric contributed to election failures, shifting blame from McDaniel to him.
- Trump is seeking someone to blame for party failures, but these issues began when he took over the Republican Party.
- Every criticism against McDaniel can be traced back to Trump, who seems unwilling to take responsibility.
- Ultimately, Trump will scapegoat McDaniel for party problems, avoiding accountability for his actions.

### Quotes

- "Every single thing being leveled as a criticism about McDaniel can be directly traced to Trump."
- "It was Trump, not McDaniel."
- "He will push her under the bus and walk away scot-free."

### Oneliner

Trump shifts blame onto McDaniel for party failures, but every criticism can be traced back to him, yet he avoids accountability.

### Audience

Republican Party members

### On-the-ground actions from transcript

- Hold Trump accountable for his actions and their impact on the Republican Party (implied)
- Support leaders based on merit rather than blind loyalty (implied)
- Advocate for transparency and honesty within the party (implied)

### Whats missing in summary

Analysis of the potential long-term consequences of Trump's leadership on the Republican Party.

### Tags

#RepublicanParty #Trump #McDaniel #PartyLeadership #Accountability


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Trump and McDaniel,
the chair of the RNC, and how things are shaping up,
because they've been pretty friendly for quite some time.
They've really been on the same page,
but now a bus is pulling into view.
Okay, so for those who don't know,
the chair of the RNC, McDaniel,
she's been chair since 2017.
Well, she's been on good terms with Trump because she's done everything that he said.
She followed his lead.
And because of that, well, he protected her.
But now tides are turning because the base is starting to blame McDaniel for all of the
errors that have occurred since, you know, she took over in 2017, mainly the
inability to fundraise, you know, the funding issues that the RNC has, and
there are less than ideal performances in all of those elections, and that's
her fault because she was doing whatever Trump told her to. And now Trump says
there need to be some changes, or there'll probably be some changes, I believe is what
he said.
Interesting that he knows that whatever he says is what's going to happen because they
still obey him like lackeys.
But the weird part about it is the two reasons they have issue with her.
The fundraising, yeah, I mean the fundraising hasn't been great, but why?
Trump launched his own separate fundraising machine and started that
rhino rhetoric so people didn't donate to the Republican party.
He undermined the fundraising efforts.
It's his fault, not hers.
And what about the election results?
Can't win a general with him, right?
He determined the talking points. He determined the rhetoric. They needed his approval to get
through the primary and then they couldn't win the general election. It's his fault.
As is typically the case, the former president is looking for somebody to take blame for his
failures. This is his failure. The issues that the Republican Party is having, they started not in
2017 when you know there was a new chair but when he took over the Republican
Party that's when the failures began. It's him. It's him. Every single thing that
there that is being leveled as a criticism about McDaniel it can be
directly traced to Trump, but, I mean, it doesn't matter.
Let's be real, nobody in the Republican party is going to put
McDaniel above Trump because I don't know, it's probably some song about it
doing well, personalities or something like that, but he, uh, he is to blame
for this. He will shoulder none of it because if people start to acknowledge
all of his failures and all of the ways he damaged the Republican Party, they'll
have to acknowledge that he duped them, he played them, he tricked them. It was
Trump, not McDaniel. And incidentally, even if you were to say it was McDaniel,
Don't keep in mind, Trump kept McDaniel in office, protected her.
No matter how you cut it, it's Trump's fault.
But he will push her under the bus and walk away scot-free, and people will continue to
allow his failed leadership to destroy the Republican Party.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}