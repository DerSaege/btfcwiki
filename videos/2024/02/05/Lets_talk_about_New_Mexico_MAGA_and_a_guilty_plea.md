---
title: Let's talk about New Mexico, MAGA, and a guilty plea....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=h_iKSSEZmH4) |
| Published | 2024/02/05 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Updates on developments in New Mexico related to incidents involving the election.
- Incidents involved shootings at homes of people involved in the election.
- Allegations point to a GOP candidate named Solomon Pina, also known as the MAGA king.
- Pina allegedly contracted others to carry out the acts, even participating in some.
- Rounds from the shootings entered the bedroom of a child, fortunately without injuries.
- A person paid to help with the incidents has entered a guilty plea.
- The guilty plea may impact Pina's charges and trial scheduled for June.
- Possibility of Pina reaching an agreement with the prosecution due to the guilty plea.
- Acts of violence stemmed from false claims about the election being rigged.
- Same source of rhetoric inciting real-world violence.

### Quotes

- "Acts of violence that were committed by the allegations because of lies made up about the election."
- "It's the same rhetoric. It is coming from the same sources. It is causing real-world violence."

### Oneliner

Updates on New Mexico incidents involving election-related shootings and GOP candidate Solomon Pina, with a guilty plea impacting charges and trial, stemming from false election rigging claims and real-world violence.

### Audience

Community members in New Mexico.

### On-the-ground actions from transcript

- Contact local authorities to ensure safety measures are in place (implied).
- Stay informed about the case developments and trial dates (implied).

### Whats missing in summary

Full context and detailed analysis of the incidents and their implications.

### Tags

#NewMexico #ElectionViolence #GOPCandidate #FalseClaims #CommunitySafety


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk a little bit about New Mexico
and some developments there, how things have proceeded
since the last time we talked about this,
which is, it's been a minute.
But there have been some major developments
that will certainly shape events to come.
Okay, so a while back, we talked about an incident,
well, a series of incidents, really,
where the homes of people involved in the election there in New Mexico, they
got shot up. And there was some ballistics evidence that kind of pointed
to it being a candidate, a GOP candidate who described himself as the MAGA king,
guy named Solomon Pina. Now as the story developed, it turned into a general
allegation that Pina had contracted with other people to engage in these acts,
even though he's alleged to have participated in some of them directly.
It's worth noting as we kind of gloss over this that in one of these instances,
the rounds went into the home, into the bedroom of a child. It is worth noting
that nobody was injured in any of this, but that certainly appears to be luck
at this point. Okay, so the person that was paid to help, that person has entered
guilty plea. Admitted their role in it and entered a guilty plea. This is not
going to be something that is taken lightly by the courts, I can assure you,
but this guilty plea is certainly going to impact Peña's charges and how things
play out there, which I believe he is scheduled for trial in June, I think.
I would fact check that if you really care about it, but it's currently scheduled in
June.
Given this guilty plea, it seems unlikely that that will remain the case.
speaking, once these kinds of plea deals start being made, there is a chance that
Pena may decide to come to an agreement with the prosecution. We don't know, of
course, they're all allegations, so maybe he's truly innocent and he's going to
take it to court and demonstrate that. We do have to keep that as an open
possibility. But the guilty plea of the person who is alleged to have been paid
by him and participated in acts with him related to these shootings seems to
greatly undercut a defense. Again, it's worth remembering these are acts of
violence that were committed by the allegations because of lies made up
about the election. Made up about the election being rigged. It's the same
rhetoric. It is coming from the same sources. It is causing real-world
violence. We're going to have to acknowledge that at some point. Anyway,
It's just about, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}