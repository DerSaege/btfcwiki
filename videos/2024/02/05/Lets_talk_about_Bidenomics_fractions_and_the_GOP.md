---
title: Let's talk about Bidenomics, fractions, and the GOP....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=IZ0jP9kJA6A) |
| Published | 2024/02/05 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains House Republicans' talking point on Bidenomics not working because six in 10 live paycheck to paycheck.
- Breaks down the fraction of six in 10 to three-fifths or 60% for easier understanding.
- Compares the current situation under Bidenomics to previous years, like February 2022 (seven-tenths or 70%) and beginning of 2019 (8 out of 10 or 80%).
- Criticizes Trump's mismanagement of the economy, contrasting it with the current economic situation.
- Suggests that under Bidenomics, 20% of the U.S. population has seen an improvement in their economic situation.
- Advocates for electing leaders who understand fractions to govern the economy effectively.

### Quotes

- "Bidenomics apparently has made twenty percent of the U.S. population have a better economic situation."
- "Maybe we should stop electing people to govern the economy who don't understand fractions."
- "In this case, the smaller number is better."
- "The alligator is going to eat the worst number."
- "Yeah, Bidenomics is working."

### Oneliner

Beau breaks down House Republicans' claim on Bidenomics, explains fractions, and criticizes Trump's economic management while advocating for leaders who understand math.

### Audience

Citizens, Voters

### On-the-ground actions from transcript

- Elect leaders who understand fractions to govern the economy effectively (implied).
- Share information about economic policies and their impact on the population (implied).

### Whats missing in summary

Analysis on how understanding fractions can provide better insights into economic policies and their effects on the population.

### Tags

#HouseRepublicans #Bidenomics #Economy #Fractions #Leadership


## Transcript
Well, howdy there, Internet people. Let's bow again.
So today we are going to talk about House Republicans and
Bidenomics and math and fractions.
Fractions are very, very hard and we're going to go over it and provide a little
bit of a lesson when it comes to fractions today.
OK, so the House Republicans have put out their new talking point.
hit Twitter. Six in 10 live paycheck to paycheck. Bidenomics is not working. Hashtag Bidenomics,
not in all caps, is not working. Bidenomics is not working. Six in 10 live paycheck to paycheck.
Okay. Six in 10, that would be a fraction. If you wanted to reduce it to smaller numbers to make it
more manageable for the House GOP it would be three-fifths. Three is a much smaller number than
six. Be easier to understand. Another way to say it would be 60 percent. Okay, to indicate
that Bidenomics was not working you would need other like things to compare it to from previous
years. Perhaps we could go back to February of 2022. What was the number then? With seven-tenths.
See, that one you can't reduce, but it would be 70%. You can't reduce the fraction, but you
could turn it into a percentage. 70%. 70% is bigger than 60%. Like the little alligator mouth.
Do y'all remember that exercise from last year?
So that means that it's getting better.
Bidenomics would be working.
But see, that's all under Bidenomics.
If you wanted a real comparison, you would have to go back to Trump.
Now we don't want like one from the last year because, you know, he did that whole horrible
mismanagement with the pandemic and, you know, crash the economy thing.
But we could go back to 2019, the beginning of 2019, and see what it was then.
That would be 8 out of 10.
8 tenths.
Reduce it to 4 fifths.
I know what you're saying.
Well, maybe that was really close to, maybe people already knew something bad was going
to happen.
It was something else.
That wasn't Trump's total mismanagement and failure of an economy while he just lied
to his base.
Let's go back further.
August of 2017, what was it then?
You guessed it, eight tenths.
Eight out of ten, eighty percent.
Bidenomics apparently has made twenty percent of the U.S. population have a better economic
situation.
Maybe we should stop electing people to govern the economy who don't understand fractions.
Maybe we should start there.
Six tenths, six out of ten living paycheck to paycheck, that is better than eight out
of ten.
In this case, the smaller number is better.
The alligator is going to eat the worst number.
Yeah, binomics is working.
If this is the metric you're going to use, it's a resounding success.
The House Republicans just don't understand math, I guess.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}