---
title: Let's talk about McConnell's recent statements....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=vDsSyXJst0Y) |
| Published | 2024/04/27 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Providing insight into Mitch McConnell's recent statements and their implications.
- McConnell rejects Trump's argument for presidential immunity.
- Blames Trump for hindering Republicans' border security packages.
- Describes the Ukrainian aid package as one of the most significant legislative victories of his career.
- Credits someone named Johnson for neutralizing Trump on the aid issue.
- Raises questions about Johnson possibly being groomed to be the next McConnell.
- Speculates on the dynamics behind McConnell's involvement in key legislative victories.
- Suggests that McConnell sees teaching Johnson as a significant accomplishment.
- Implies that McConnell may view Johnson as his successor.
- The transcript ends with a reflective musing on the evolving political landscape.

### Quotes

1. "It’s not really about the aid package. It’s about teaching somebody, well, how to be him."
2. "When he said that he believed that the Ukrainian aid package was one of the most significant legislative victories of his career."
3. "You got to wonder what happened."
4. "He says that Johnson, quote, neutralized the former president on this issue."
5. "You're starting to see some coverage of it, some articles, like, what if he's really good at this?"

### Oneliner

Beau delves into Mitch McConnell's recent statements, hinting at a potential successor and the nuances behind key legislative victories.

### Audience

Political enthusiasts

### On-the-ground actions from transcript

- Speculate on the evolving political landscape and foster critical thinking (implied)

### Whats missing in summary

Insights into the potential grooming of a successor and the behind-the-scenes dynamics shaping legislative victories.

### Tags

#MitchMcConnell #PoliticalAnalysis #Succession #KeyLegislation #Johnson #CriticalThinking


## Transcript
Well, howdy there, internet people.
Let's battle again.
So today, we're going to talk about Mitch McConnell.
We're going to check in on old McConnell
and see how he is doing, because he
has said some pretty interesting things
over the last few days.
And they're worth going over, and they
might provide us a little bit of insight
into some recent events and how they transpired.
And in the process, it might actually confirm a theory
they kept showing up down in the comments section
that couldn't be confirmed at the time,
couldn't be proven but this might lend a little bit
more credibility to that.
Okay, so what are the things that he said?
First, he came out and he says that he is definitely
not in favor of presidential immunity
in the way that Trump is arguing for it.
He put Tucker Carlson out there over his opposition to Ukraine, to that aid.
He said that Trump being quiet made all the difference when it came to the Ukraine aid
package.
He put Trump on blast for being the reason Republicans didn't get the border security
packages that they wanted, saying our nominee for president did not seem to want us to do
anything at all. So if you are one of those who is wondering whether or not that's just rhetoric,
McConnell, you know, lifelong Republican, certainly seems to believe that Trump was
responsible for tanking the border security bill. And all of this is interesting. It's interesting
stuff, it gives us insight into a few things, but I think the most interesting part was
when he said that he believed that the Ukrainian aid package was one of the most important
legislative victories of his career.
That's a big statement.
I mean, you need to think about how long he's been up there.
That is a big statement.
the most important and that he'd been working on it for most of the year. Doing what? He's not
Senate Majority Leader right now. What, throwing some support behind Schumer, trying to wrangle
his own party? This is minor stuff for McConnell. That's just Tuesday. What could he have been doing
to Phil so connected to it that it was one of the most important legislative victories of his career.
And that he'd been working on it that much.
Strange.
But again, Trump being quiet is what sealed the deal.
What's interesting is that McConnell gives Johnson credit for that.
He says that Johnson, quote, neutralized the former president on this issue.
I mean, that's a statement right there.
Makes it sound like Johnson got the better of Trump, which is certainly how it appears.
You know, when we were talking about all of this, when, you know,
it was that slow motion event we could see coming like 72 hours in advance.
One of the common comments was that maybe he looks like McConnell in the making because
McConnell is tutoring him, helping him.
He worked on it most of the year, huh?
Better part of the year?
I mean, at least that would explain that statement because publicly he didn't do a whole lot.
But if he was there helping Johnson, he probably had a lot of work to do.
And that also might explain why he sees it as such an important legislative victory.
It's not really about the aid package.
It's about teaching somebody, well, how to be him.
You know, there are a lot of people who don't want to acknowledge that Johnson is McConnell
in the making.
You're starting to see some coverage of it, some articles, like, what if he's really good
at this?
And, you know, stuff like that.
I think if you're wondering whether or not somebody could be the next McConnell, somebody
to ask would be McConnell.
Based on the statements, it certainly seems like he might believe that Johnson is McConnell
in the making.
It's interesting.
It's an interesting turn of events, especially that phrasing neutralized.
You got to wonder when he went down there, held hands, sang kumbaya, had that press conference.
You got to wonder what happened.
Anyway it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}