---
title: Let's talk about Trump, Biden, and RFK....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Tugdv1O7Oj8) |
| Published | 2024/04/27 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Historically, American presidential choices were limited to candidates like Jack Johnson and John Jackson, offering minimal variation.
- Trump's presidency marked a departure from the norm, showcasing a different style of leadership.
- In 2024, some seem to have forgotten the lessons of 2020 about the dangers of authoritarianism and extreme right-wing ideologies.
- There is a segment of the population seeking alternative leadership options, considering RFK Jr. as a potentially different choice.
- However, the concern arises that new candidates may not truly expand the range of options but instead add more choices within the existing spectrum.
- Trump's criticism of RFK Jr. as a Democrat plant aiming to benefit Biden adds a layer of intrigue to the dynamics.
- The possibility that RFK Jr. may draw more votes from Trump than Biden raises questions about the overall impact on the political landscape.
- Building a wider range of options requires grassroots efforts to create a new political party with a distinct platform.
- Simply running third-party candidates without a comprehensive strategy for long-term change may not lead to the desired outcome.
- To effect real change and introduce new options, individuals must take action and shape the political landscape they desire, rather than waiting for top-down solutions.

### Quotes

- "You have to build the party you want with the platform you want because anybody who is going to try to come in up at the top and run as a third party and actually try to win well they're going to be within that same range."
- "You get more choice but you don't get more options. It's the same range. It's just packaged differently."
- "If you want your platform, if you want more options, if you just want to settle for the options that are going to be presented to you, sure, once every four years will do it."
- "You have to build it. You can't wait for a leader. You have to become one."
- "There is no power structure to support that. You have to build it."

### Oneliner

Building a wider range of political options requires grassroots efforts to create new parties with distinct platforms, rather than settling for existing choices.

### Audience

Political activists and reformers

### On-the-ground actions from transcript

- Build a new political party with a distinct platform (suggested)
- Start grassroots efforts to shape the political landscape (implied)

### Whats missing in summary

The full transcript dives deeper into the necessity of active citizen participation beyond election cycles to effect real change and create a political system that truly represents diverse viewpoints.

### Tags

#PoliticalOptions #GrassrootsEfforts #ThirdPartyCandidates #ActiveCitizenship #PoliticalChange


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Biden and Trump and RFK
Jr and choices, options, and the dynamics at play and having a
wider range of options and how you actually get to that point.
You know, for a long time in this country, the choices for
president, your candidates, were Jack Johnson and John Jackson.
My opponent's a good man.
He's a fair man, but I disagree on this one wedge issue vote for me, maybe two
wedge issues, if you're lucky, but that's what it was for a really, really long
time. And then along came Trump and Trump was different. He really was. And people
saw that difference is good in 2016. And then by 2020 they realized no, being more
authoritarian and further right-wing is actually not good. But now in 2024 it
It appears that some people have forgotten that.
And then you have a group of people who is unhappy with the current dynamic, unhappy
with the leadership choices, and they are looking at Jr., they're looking at RFK Jr.
and assuming that he's different, that it will lead to more options, meaning a wider
range of options, not just more choices within the same range.
But sometimes in those situations you end up with Jack Johnson, John Jackson, and Jimmy
Jack John.
I know, everybody thinks that he's very different.
But let me read you something.
This is from Trump.
R.F.K.
Jr. is a Democrat plant, a radical left liberal who's been put in place in order to help crooked
Joe Biden.
It goes on and on.
A vote for Jr. would essentially be a wasted protest vote.
Wow, that's weird coming from Trump.
What happened?
They found out that it appears that Junior might be taking more votes from Trump than
from Biden.
It's close though.
It's close.
That's weird, right?
The candidates cast themselves as very, very different.
And somebody who is supposed to be very different from them is splitting the difference?
Does that really give you a wider range of options or does it just give you more choice
within the same range?
More paths to get to roughly the same point.
Now to be clear, I am of the opinion that there is a substantial difference between
Trump and the other two.
those people looking at junior they probably don't think that way. So there's
another choice but it doesn't really add to your range of options. So how do you
get a wider range. You know, that's one of those things that people talk about.
They want additional parties and most times they want to run somebody for
president and as we've talked about before even if they win it doesn't
actually do anything but the question is how do you get a wider range of options?
Get beyond the current range of political discussion.
You build it.
That party, you have to build it.
You have to build it from the ground up.
You can't wait for a leader.
You have to become one.
have to build the party you want with the platform you want because anybody
who is going to try to come in up at the top and run as a third party and
actually try to win well they're going to be within that same range. They're
going to pull a little of column A a little of column B and try to pull
together a coalition that can win. Now you have third party candidates or
independent candidates who run, who are outside of that range.
But they really don't plan on winning.
It's more about messaging.
And there's definitely a place for that, to be clear.
But if you actually want a wider range of options, you have to build it.
And it takes time.
It takes time.
If you were to start now, you would not be ready by 2024.
You would not be ready by 2026.
You might by 2028 be able to get a couple of House seats.
Still not ready for the presidency though, because you have to build the support.
You have to actually build the party.
Nobody who is going to come in at that top level is going to have the positions you want.
The reason you want a wider range of options, the reason you are unhappy with the current
choices is because they don't reflect what you want, right?
You want something outside of that.
who is going to come in that has a chance of winning is going to try to build a coalition
within the range of options that you have already rejected.
You have to build it.
You have to build the support.
You have to make the case for your position, for your platform, for the world you want
that is outside of that range.
You're not going to get it
by a candidate who has to get elected by existing within that range.
You get more choice
but you don't get more options.
It's the same range.
It's just packaged differently.
One of the things that you have to acknowledge and you have to remember when you are talking
about the United States and the way our system currently is, it is advanced citizenship.
It is something you have to participate in if you want it to function.
If you believe in electoral politics, you have to be involved way more than once every
four years.
If you want your platform, if you want more options, if you just want to settle for the
options that are going to be presented to you, sure, once every four years will do it.
But if you want something else, you have to build it, especially because those people
who want something else, by definition, they want something outside of the status quo.
You want something beyond what currently exists.
There is no power structure to support that.
You have to build it.
You can't wait for a leader.
You have to become one.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}