---
title: Let's talk about Trump and economic influence....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=_Fg65jkA194) |
| Published | 2024/04/27 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains a reported plan in a potential second Trump term affecting the economy.
- Plan involves making Trump an acting central bank board member with power to fire the Federal Reserve chair.
- Mentions potential implications if the plan is put into action.
- Stresses the importance of Federal Reserve independence from political influence.
- Suggests that if the Fed chair were to answer to the president, it may lead to a significant market reaction.
- Expresses concern over Trump's track record in long-term financial decisions.
- Points out past economic indicators prior to the current public health crisis.
- Expresses doubt on the plan passing through Congress but warns against assumptions.
- Emphasizes the significant impact the plan could have on the U.S. economy.
- Raises concerns about individuals benefiting from betting on Trump's ventures failing.

### Quotes

1. "Dramatic reaction does not cut it. Volatile does not cut it."
2. "It is a huge deal. It impacts everybody."
3. "Not somebody I would want to have sway over the Federal Reserve."
4. "We can't really make those kinds of assumptions anymore."
5. "Y'all have a good day."

### Oneliner

Beau explains a reported plan in a potential second Trump term affecting the economy, stressing the importance of Federal Reserve independence and warning about potential market reactions, while expressing doubts on the plan passing through Congress and raising concerns about Trump's financial decisions.

### Audience

Concerned citizens, economic analysts.

### On-the-ground actions from transcript

- Stay informed about economic policies and their potential impacts (suggested).
- Support efforts to maintain the independence of institutions like the Federal Reserve (suggested).

### Whats missing in summary

The full transcript provides more context on Trump's potential influence on economic decisions and the risks associated with the reported plan.

### Tags

#Trump #Economy #FederalReserve #MarketReaction #FinancialDecisions


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about Trump
and a plan that has now been reported
on that would affect the economy in a potential second Trump
term.
And we're just going to kind of run through what was said
briefly and talk about the potential reactions to it if that plan was put into place.
The short version of it is that it would make Trump an acting central bank board member
with the power to fire the chair of the Federal Reserve.
That's quite a statement for those that don't know the Federal Reserve is supposed to be
independent, should not answer to the whims of the presidency for political reasons.
I believe it would also give Trump some control or they would have to consult with him over
interest rates as well.
If it becomes clear that there's going to be a Fed chair that answers to the president,
I would expect a dramatic reaction in the markets.
always joke about how I'm very understated. No, this is an understatement.
Dramatic reaction does not cut it. Volatile does not cut it. Especially, and
this would be true of any president to be clear, but this would be especially
true of Trump, somebody who does not necessarily have the greatest record
when it comes to long-term financial decisions. It is important to remember
that prior to the public health issue that arose during his administration, we
saw the yield curve invert, we saw signs that the U.S. was headed into a
recession, and in many ways the pandemic kind of provided cover for it.
This is something that I would like to assume would never make it through Congress.
But we can't really make those kinds of assumptions anymore.
This would be a huge deal.
It would impact everybody.
It would impact the U.S. economy from top to bottom.
And again, a dramatic reaction is that's an understated assessment of the situation.
In totally unrelated news, it does appear that Trump's latest venture, there are people
who have made tens of millions of dollars with it by, in essence, betting on it failing.
Not somebody I would want to have sway over the Federal Reserve, just saying.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}