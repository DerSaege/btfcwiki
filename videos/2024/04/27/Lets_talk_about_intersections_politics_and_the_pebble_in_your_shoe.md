---
title: Let's talk about intersections, politics, and the pebble in your shoe....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=KkE3ksRg3Zo) |
| Published | 2024/04/27 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains why he hasn't talked about how Biden's handling of Gaza will impact the election, stating that people generally don't vote based on foreign policy.
- Mentions that despite possible impacts, it's unlikely for individuals to switch their vote from Biden to Trump solely based on foreign policy.
- Talks about the influence of the "lesser of two evils" argument for progressives and leftists unhappy with Biden's handling of Gaza.
- Emphasizes that the deciding factor for most voters is usually the issue that directly impacts them personally, likening it to a "pebble in their shoe."
- Suggests that even though foreign policy is significant, it often doesn't sway people's votes, noting that enthusiasm for Biden may decrease in certain areas due to his handling.
- Speculates on potential impacts on voter turnout in swing states due to less enthusiasm for Biden's foreign policy decisions.

### Quotes

1. "People don't vote based on foreign policy."
2. "The deciding factor for most voters is usually the issue that directly impacts them personally, like a 'pebble in their shoe.'"
3. "Enthusiasm for Biden may decrease in certain areas due to his handling."
4. "A lot changes in seven months."
5. "For the majority of people, this isn't going to be a deciding factor."

### Oneliner

Beau explains why foreign policy isn't a major voting issue and predicts potential impacts on voter turnout due to Biden's handling of Gaza, focusing on personal impacts.

### Audience

Voters, Political Analysts

### On-the-ground actions from transcript

- Analyze the candidates' stances on key issues beyond foreign policy (implied)
- Encourage voter education and engagement on all pertinent topics (implied)

### Whats missing in summary

Insights into how lesser-known issues can still influence voter behavior.

### Tags

#Biden #Election #ForeignPolicy #VoterTurnout #Politics


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about an intersection
of two topics that I haven't talked about.
We're going to talk about Biden, foreign policy,
and the election.
We're gonna do this because I got a question.
And the question was,
Hey, you are very much a foreign policy nerd and a politics nerd, why haven't you
talked about how Biden's handling of Gaza is going to impact the election?
Long experience, long experience.
I know that this is really hard to believe right now at this moment with
everything going on. People don't vote based on foreign policy. They really
don't. It's not a major issue for most people. You will have some when I say
people just understand that's a generalization, but most times people
vote based on the pebble in their shoe, the thing that actually impacts them
directly. So that's one reason, is that generally speaking, it's not a deciding
factor for most people. Now, that doesn't mean there won't be any impacts, but it
means you're not going to have people switch their vote from Biden to Trump.
It's just not going to be a deciding factor. It may be a deciding factor in
whether or not they go vote for maybe a larger demographic than the
Biden administration thinks, but it's probably not going to change
somebody's vote. But again, most people they don't vote based on foreign
policy, and this is a long-running thing in the U.S.
The other reason is the lesser of two evils argument definitely is going to come into play here.
When you are talking about people who are unhappy with Biden's handling of Gaza, they are progressive, maybe actual
leftists, a few liberals.  liberals, even if they are unhappy, and I mean completely unhappy, they will acknowledge
the difference between the guy who sanctioned Israeli entities and individuals and the guy
who said Israel needs to finish the problem, you're not going to get a
change in vote over that, because you may have some people who really
believe that they can push Biden further left and there's room there.
Realistically, objectively, there's room there, maybe not as much as people think
because foreign policy is constrained in some ways, but there's been a lot of movement and
some people may give him credit for that when it when it comes down to it. I'm not talking about
what they say on social media, I'm talking about what happens when they're in that booth if they
show up. It's a whole lot like all of the incredibly conservative women in the
red states who voted to keep reproductive rights. They may never admit
that that's how they voted. They may never admit that they voted, but that'll
come into play. And then the other thing is the actual pebble in the shoe. What
actually is the pebble in the shoe? Again, the people who are upset by this, they
are people who are progressive, leftists, a few liberals. I would imagine that most
Most of them have a trans friend.
That is closer to home, which is what people tend to vote on.
I would imagine that most of them know a woman.
Some of them may actually be a woman themselves.
Some of them may be a trans woman.
The pebble in the shoe.
As a foreign policy nerd who actually believes that a lot of things would be better off in
the United States if our foreign policy was different, I wish more people would consider
foreign policy when they vote.
But long experience tells me they don't.
The most it does is influence whether or not they vote.
I do believe that Biden will have less enthusiasm, there will be some
areas where turnout will be lower because of his handling, but I don't
think anybody's going to vote for Trump over Biden on this issue.
And because of all of the other high stakes issues, the pebble in the
shoes probably going to win out in most cases. And then you also have, and to be
clear I did not talk about all the pebbles, you also have the Supreme Court,
expected vacancies, stuff like that. And then you have the other thing. It's still
seven months away, and I know people are like, it's not going to change in seven
months. A lot changes in seven months. I don't think it is, I don't think you can
make the determination yet other than he will probably have lower voter turnout.
To me at this point that's the only real, the only real connection that exists at
that intersection right now. It may change, but at this moment in time, its seven
months from now, people are going to care about the pebble in their shoe. They
almost always do. And then they will lean towards the things that are most
directly impacting them, so for the majority of people this isn't going to
be a deciding factor. Again, there could be less enthusiasm that leads to less
voter turnout and in some cases that's probably going to be in a swing state
And that may, that may provide a real impact, but we don't know yet.
Anyway, it's just a thought y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}