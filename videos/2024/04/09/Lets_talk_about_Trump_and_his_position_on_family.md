---
title: Let's talk about Trump and his position on family....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=kSjHwCY4jfw) |
| Published | 2024/04/09 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump announced his position on family planning and reproductive rights, leaving it up to the states.
- Many in Trump's party wanted a national ban on reproductive rights, but he said it's a state matter.
- Trump claimed to be responsible for ending Roe v. Wade but emphasized that states will have varying laws.
- This decision by Trump is viewed as a betrayal by the pro-life voting block.
- Individuals supporting bodily autonomy and reproductive rights are also unhappy with Trump's stance.
- Politically, this move is seen as the worst Trump could have taken, as it does not satisfy either block.
- Republicans previously used the "up to the states" stance as a tactic, not a genuine position.
- Lindsey Graham and Mike Pence, along with national organizations, have expressed disagreement with Trump's stance.
- This decision may alienate a significant voting bloc that has a long memory.
- Overall, Trump's announcement is perceived as a poor political move that exposes the Republican Party's strategies.

### Quotes

- "He said many states will be different. Many will have a different number of weeks and some will have more conservative than others."
- "There's not a worse position because you don't get either of the blocks."
- "Now you've made us look silly."
- "That was his big announcement that he teased."
- "I don't think it went the way he hoped."

### Oneliner

Trump's stance on family planning and reproductive rights, leaving it up to the states, angers both pro-life and pro-choice groups, exposing political tactics and risking alienation of key voting blocs.

### Audience

Politically active individuals

### On-the-ground actions from transcript

- Contact national organizations advocating for reproductive rights and bodily autonomy (exemplified)
- Join or support local grassroots movements promoting reproductive rights (exemplified)

### Whats missing in summary

The full transcript provides a detailed analysis of Trump's decision on family planning and reproductive rights, including its potential impact on different voter blocs.

### Tags

#Trump #ReproductiveRights #PoliticalAnalysis #ProLife #ProChoice


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump.
We're gonna talk about Trump and him announcing
his position on a topic that a whole lot of people
were curious about, didn't really know
how he was going to handle it.
I think there was a wide expectation
that he was going to do what he normally does,
which is double down on something,
especially when it is a contentious topic.
That's not what he did in this case.
Of course, if you don't know what we're talking about, Trump announced his position when it
comes to family planning, reproductive rights.
He basically said, well, it's up to the states.
It's up to the states.
There's a whole lot of people pushing for a national ban within his party.
He said it was up to the states.
He said that he was proudly the person responsible, quote, for ending Roe.
But it's up to the states and it would vary.
He said many states will be different.
Many will have a different number of weeks and some will have more conservative than
others.
And that's what they will be.
At the end of the day, this is all about the will of the people, sure, when it comes to
one of the most unpopular decisions. Anyway, here's the thing, from a
political standpoint, because that's, that is probably how he's looking at it.
From a political standpoint, this is literally the worst position he could
have taken. Because here's the thing, I know after Roe was shot down, I know
that that's what Republicans said. Oh well, see now it's up to the states, and
and they'll get to decide. That was just something that they were using to trick
that easily manipulated within their own party. That wasn't actually their
position. That was never their position. Their position was to do it
at the national level and to go after family planning, reproductive rights in
general. That's why you have talk about, you know, getting rid of no-fault divorce.
you have talk about contraceptive, IVF, all of this stuff, it all came out of
that. It was never supposed to be something that they were going to leave
up to the states. It was a move against reproductive rights. So those
people who are in favor of restricting rights, they're not going to be happy
with this. That's what is often defined as the pro-life voting block. They're not
going to be happy with this at all. You have Lindsey Graham and Mike Pence, both
of them already coming out against Trump's position, as well as like well
know national organizations. Lindsey Graham said something like that he
respectfully disagrees or something you know very Lindsey Grammish. Pence said
something like it's a slap in the face. I mean honestly it seemed like he had more
of a reaction to this than he did the sixth. They're not going to be happy with
it. This is not going to help him with that voting block. They will see it as a
a betrayal. And then you have the people who are in favor of, you know, generally
having bodily autonomy, reproductive rights. They're obviously not going to
be happy with it because it's not a reversal and, quote, I was proudly the
person responsible. When it comes to politics coalition building, there's not
a worse position because you don't get either of the blocks and you end up
exposing the Republican Party's rhetorical trick. Oh, it's up to the
states. We never meant that. That was just something we were telling those
people who would believe whatever we said. Now you've made us look silly." This was
not a good political move. He does have a habit of being able to kind of just push through
stuff like this, but this is a big one. The voting bloc that he is going to have angered
with this. They tend to have pretty long memories. That was his big announcement that he teased.
I don't think it went the way he hoped.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}