---
title: Let's talk about the hurricane season forecast....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=o1SQCMumAkY) |
| Published | 2024/04/09 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Providing an overview of the upcoming hurricane season forecast from Colorado State University.
- The forecast predicts an above-average season with 23 named storms, 11 hurricanes, and five major hurricanes.
- The factors contributing to this forecast include heat in the Atlantic Ocean and the presence of La Niña.
- La Niña can intensify hurricanes rapidly, from category one to three or three to five.
- Beau advises early preparation due to the advanced heat content in the Atlantic, suggesting starting preparations even before June.
- He stresses the importance of emergency preparedness, including food, water, fire, shelter, a knife, first aid kit with medications, and ensuring the safety of documents.
- Beau recommends keeping digital copies of vital documents to prevent their loss during emergencies.
- He encourages those in the Atlantic hurricane zone to pay attention and not underestimate the potential impact.
- Beau concludes by urging viewers to take the upcoming hurricane season seriously and prepare adequately.

### Quotes

1. "Start getting ready now. Right now."
2. "Make sure you have your documents."
3. "Don't blow it off."

### Oneliner

Beau advises early and thorough preparation for an above-average hurricane season, citing factors like La Niña and increased Atlantic Ocean heat content.

### Audience

Residents in hurricane-prone areas

### On-the-ground actions from transcript

- Start emergency preparations now (implied)
- Ensure you have necessary supplies like food, water, fire, shelter, first aid kit, and documents (implied)
- Create digital copies of vital documents (implied)
- Stay informed and prepared for the upcoming hurricane season (implied)

### Whats missing in summary

Importance of staying informed and following official guidance during hurricane season.

### Tags

#HurricaneSeason #Preparedness #EmergencyPreparation #LaNiña #AtlanticOcean #Forecast


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about hurricane season
and the forecast, how everything is shaping up,
and just run through the information.
Okay, when it comes to the forecast
for how active a hurricane season is going to be,
you have two decent sources.
You have NOAA and Colorado State University.
NOAA has not put out theirs yet.
the university has. Their April forecast for this year's hurricane season which
runs from June 1st to November 30th under normal circumstances includes
23 named storms. If that sounds high to you it's because it is. On average there
are 14. They are saying that there will be 11 hurricanes. On average there are
seven, and they are saying there will be five major hurricanes, which means
categories three, four, and five. Under normal circumstances, the average is
three. Okay, so why, right? What is causing this forecast? Heat in the Atlantic Ocean
and the switch to La Niña. One of the things to remember about La Niña is
is that it also tends to mean that a hurricane can go from, you know, one to three, or three to
five pretty quickly. So when you are making your plans for this year's hurricane season,
keep that part in mind. If it was me, I would start getting ready now. Right now. You know,
know it's it's April and it doesn't really start till June but we are ahead
when it comes to heat content in the Atlantic we're a little bit ahead. So I
mean this is not my area but the conditions to cause hurricanes appear
to be coming earlier which means maybe one shows up before June. Again, not my
area but that just seems kind of common sense. I would start putting your stuff
together. Get your plans together. You know there's a bunch of videos about
this in the emergency preparedness playlist. I'll put it down there. But
short version, food, water, fire, shelter, knife, first aid kit to include your meds
and since you're talking about a hurricane, your documents. Make sure you
have your documents. That's something we deal with every year. Every time we go to
help, somebody has lost all of their paperwork about everything. A good way to
do this, at bare minimum, if you can't go ahead and get copies, is digital photos,
put them on a thumb drive, stuff like that. Get something to take care of all
of the important paperwork.
If you're in the Atlantic hurricane zone where this is something that's going to impact
you, affect you, I would pay attention this year.
Don't blow it off.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}