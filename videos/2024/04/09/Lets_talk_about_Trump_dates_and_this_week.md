---
title: Let's talk about Trump, dates, and this week....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=qCz81Vc6KJs) |
| Published | 2024/04/09 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Providing updates on Trump's legal entanglements.
- Appeals court denies Trump's request for a change of venue in the hush money case.
- Trump to appeal the gag order.
- Details on potential juror questionnaire for New York trial revealed.
- New York trial scheduled to start on April 15th.
- Questions arise about the bond in the New York civil case.
- Supreme Court to hear arguments about Trump's presidential immunity.
- Former military officials urge the court to reject Trump's claims of immunity.
- Oral arguments for the Supreme Court case set for April 25th.
- Anticipating developments this week in Trump's legal battles.

### Quotes

1. "Former military brass signed on to a brief urging the court to reject Trump's claim."
2. "April 15th and April 25th are your dates to watch if you are following all of Trump's entanglements."
3. "There are a couple of small surprises, but nothing major."
4. "That, I think the oral arguments are supposed to happen on April 25th."
5. "Anyway, it's just a thought."

### Oneliner

Beau provides updates on Trump's legal battles, from denied venue changes to upcoming oral arguments, urging attention on April 15th and 25th.

### Audience

Legal observers, political analysts.

### On-the-ground actions from transcript

- Stay informed about the developments in Trump's legal cases (implied).
- Monitor the scheduled dates for key events (implied).

### Whats missing in summary

Insights on the potential implications of the legal outcomes.

### Tags

#Trump #LegalBattles #SupremeCourt #Immunity #Updates


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump
and we're gonna cover a couple of topics.
So we'll talk about Trump and dates
and his various entanglements
and just provide little updates
as some of them draw a little bit closer.
Most of this is exactly what was expected.
There are a couple of small surprises, but nothing major.
Okay, so we will start off with New York.
We'll start off with that one,
the one being called the hush money case.
Okay, so the appeals court has denied Trump's request
for a change of venue.
They heard the argument and were like no.
Trump was arguing that he wasn't gonna be able
to get a fair trial there,
That they wouldn't be able to find an impartial jury.
That was not how the court saw it.
Trump is also going to appeal the gag order.
That has not been argued yet, so we're still waiting for information on that.
A little bit of information came out about the questionnaire that the potential jurors
would receive. And it is more or less exactly what you would expect. Not a
whole lot of surprises there. I think the the two outlier questions were asking
where people got their news and if they listened to any podcasts. That certainly
seemed to be screening for people who might be sympathetic to, I don't know,
hypothetically speaking, Cohen. And then another question asked about various
groups or movements that might be sympathetic to the former president. But
overall, that was pretty much what was expected. Okay, so currently that one is
running on schedule. That's running on schedule. That is supposed to start
April 15th, so just a few days from now. There were some additional questions
that surfaced about the bond in the New York civil case. There's not enough to
really get into that too deeply yet, but I would be surprised if that didn't come
up again, in more than one way, to be honest.
Okay, the Supreme Court will be hearing the arguments about Trump's presidential immunity.
Smith, of course, urged the court to reject Trump's arguments and claims to just, I can
do whatever I want, which is kind of what the argument is.
The surprise in that one was that former military brass just across the board, generals, admirals,
former secretaries of the Army, Navy, Air Force, tons, a very wide cross section, signed
on to a brief basically urging the court to reject this claim because of what his team
previously argued about being able to send the military after political
rivals. They basically said it would inject chaos into the military and
they're right. They're right, it would. That, I think the oral arguments are supposed to
happen on April 25th. So April 15th and April 25th are your dates to watch if
you are following all of Trump's entanglements.
I would imagine there's going to be some developments this week that we might have to come back
and revisit, but I don't see any huge surprises except maybe with the bond that was put up.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}