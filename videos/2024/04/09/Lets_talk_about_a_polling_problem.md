---
title: Let's talk about a polling problem....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=4krGIceLaM4) |
| Published | 2024/04/09 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces the topic of polling and his history with discussing polling accuracy.
- He mentions how polling was incredibly accurate before 2020, even when the other person won within the margin of error.
- In 2020, Beau started discussing unlikely voters and how they were affecting polling accuracy, especially due to younger people becoming more politically engaged.
- There was also a public health issue affecting polling demographics in favor of Biden.
- By 2022, the issue with unlikely voters became broader, indicating that polling was not capturing the right demographics.
- Beau notes that recent polling seems off, with strange results like Biden leading among seniors and Trump leading among Gen Z, which seems unlikely.
- He expresses skepticism about the accuracy of current polling and mentions that reporting on polling may not be worth the time.
- Beau hopes that pollsters can fix the errors causing inaccuracies but admits he has no clue about the root cause this time.
- He warns about potential bad actors manipulating polling to influence voting turnout.
- Ultimately, Beau concludes that the only poll that truly matters is the one in November.

### Quotes

1. "The only real answer is a non-response bias, but we've talked about that on the channel before too. That's not a good answer."
2. "The only poll that is going to matter is the one you go to in November."
3. "So maybe they'll be able to fix it."
4. "I don't think it's a good idea to do that."
5. "Y'all have a good day."

### Oneliner

Beau breaks down the evolution of polling accuracy, casting doubt on current polls and pointing to the importance of the final election day poll.

### Audience

Political analysts, voters

### On-the-ground actions from transcript

- Be cautious of potential manipulation in polling data by bad actors (implied)
- The most impactful poll is the one you participate in during November (implied)

### Whats missing in summary

Insight into the potential consequences of inaccurate polling on voter behavior and election outcomes.

### Tags

#Polling #Accuracy #Election #Politics #Voters


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about polling.
We're going to talk about a problem with polling.
Before we get into this, because I'm going to say
something that is a little unusual, I want to kind of go
through my history when it comes to polling for people
that are newer to the channel.
2019 and before, maybe even into 2020, you will see me saying stuff like, that's not
what the polling says, because the polling was incredibly accurate.
Very accurate.
Even when it was wrong, in the sense of the other person won, it was normally within the
margin of error.
Very, very accurate.
Starting in 2020, you would hear me say stuff about unlikely voters, which at the time what
I meant was I could tell in my analytics that younger people were becoming more and more
engaged politically.
Some of them were going to show up and it was going to throw off some of the polling.
We also talked about a public health issue that was occurring and how that might make
older demographics skew more towards Biden when it came to the actual vote versus the
polling.
Why?
Because there was, you know, a pandemic and older Republicans didn't really protect themselves.
And the data about that showed that, yeah, that was true.
When it came to 2022, same thing with unlikely voters, only then it was a little bit more
broad.
It didn't seem like they were polling the right people.
They were viewing the likely voter that they were polling more narrow than they should.
So people weren't being polled who were going to show up.
There wasn't going to be a red wave, and then there wasn't.
You might have noticed that we stopped talking about polling recently, except to point out
where it was really wrong, like where Trump is underperforming in primary polling.
The polling says he's going to get 70% and he's only getting 60%, stuff like that.
And then we just kind of stopped talking about them because I don't view them as accurate
now.
Before they were off, now I'm not sure they're accurate.
I don't feel bad saying this now because now there's reporting about it.
There's a couple of different places that have started talking about this.
When you look at current polling, you see some really strange things, like Biden being
ahead with seniors, which some of that is going to be because of the whole pandemic
thing and still the effects of that.
Some of it shows Trump leading among Gen Z, in some cases by double digits.
That seems super unlikely, and the more you look into it, the weirder it gets.
Some of it you can come up with some kind of explanation for, and if you torture numbers
long enough, they'll tell you whatever you want to hear.
I don't think it's a good idea to do that.
So as it stands, I'm not sure that reporting on the polling is really worth the time because
it appears that it may be less accurate than before.
This is being talked about in a few different places and hopefully that means that the pollster
maybe they'll be able to fix whatever is causing the error. In this case,
I don't even have a clue. Like in 2022 and 2020, I could be like, this is what it is.
I have no idea what's causing all of this. The only real answer is a non-response bias,
but we've talked about that on the channel before too. That's not a good answer, even if it seems
like it's the answer. Generally speaking, that's not it. So maybe they'll be able
to fix it. The flip side to this is now that it's getting coverage, you will
probably find bad actors who might push out polling that is designed to increase
or depressed, voting turnout. So you need to be kind of on guard for that. But more
and more, unless they can figure out what's going on, the only poll that is
going to matter is the one you go to in November. Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}