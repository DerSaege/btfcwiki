---
title: Let's talk about jargon, intensity, and definitions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=iLPqFnBxCzQ) |
| Published | 2024/04/12 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the confusion around the term "low intensity" used in reporting about Israel.
- Differentiates between the normal definition of "lower intensity, slower tempo" and the military jargon meaning of a "low intensity conflict."
- Defines a low intensity conflict as a confrontation below conventional war but above routine peaceful competition, involving competing principles and ideology.
- Mentions that low intensity conflicts can range from subversion to the use of armed forces and are often localized in the third world.
- Provides an example of a low-intensity operation with the movie "Black Hawk Down."
- States that the term "low intensity" in reporting mostly refers to a slower tempo, not necessarily a strategy shift.
- Speculates on the reasons why people are interested in the potential shift to low intensity operations by Israel.
- Notes that low intensity operations are considered safer for civilians.
- Acknowledges that while the slower tempo could indicate a shift to low intensity, there is no concrete evidence of a strategy change.
- Anticipates that clarity on whether there's a move to low intensity operations by Israel may come within a week.

### Quotes

1. "Low-intensity conflict is below conventional war but above routine peaceful competition."
2. "Low intensity operations are much safer for civilians."
3. "The slower tempo could just be them regrouping."

### Oneliner

Beau explains the confusion around the term "low intensity" in reporting on Israel, clarifying its military jargon meaning and the potential implications, noting that while a slower tempo has been observed, there is no definitive evidence of a strategy shift yet.

### Audience

Military analysts, journalists, policymakers.

### On-the-ground actions from transcript

- Monitor updates on Israel's military operations (implied).

### Whats missing in summary

Beau's engaging delivery and additional context can be best understood by watching the full transcript.

### Tags

#Military #Conflict #Israel #LowIntensity #Reporting


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about jargon, and definitions,
and intensity, and how terms get used,
and how confusion occurs.
And we're going to do this because I've
gotten a few messages about a term I have used on the channel
a number of times.
And that term, that phrase, anyway,
starting to show up in reporting. And the questions are like, hey there's
reporting that says Israel has moved to a lower intensity. I'm surprised you're
not covering it. Okay, so when it comes to the term low intensity, there are two
definitions. One is the normal definition of the term, you know, of that phrase.
Lower intensity, slower tempo. The other is military jargon and it's... it is a
military operation other than war. Now keep in mind, war is full-scale. To go
straight to the manual on this one. The definition of a low intensity conflict
is a quote confrontation between contending states or groups below
conventional war and above the routine peaceful competition among states. It
frequently involves protracted struggles of competing principles and ideology.
Low intensity conflict ranges from subversion to the use of armed forces. It
It is waged by a combination of means employing political, economic, informational, and military
instruments.
Low intensity conflicts are often localized, generally in the third world, manual needs
an update, but contain regional and global security implications.
That's a mouthful that doesn't really tell you a whole lot, right?
To translate that, in the military imagination, conventional war, normal scale, World War
II, low intensity is below that.
We'll give you another example in a second.
But generally speaking, when people think of war, you think of artillery, a whole bunch
of tanks, you think of Ukraine.
Low-intensity conflict is not that, but it doesn't mean that it's peaceful and it doesn't
mean that it's not intense.
An example of a low-intensity operation that I think most people would be familiar with,
the movie Black Hawk Down depicts a low-intensity operation.
It may not mean what people think it means.
doesn't mean peace. Now, in all the reporting that I have seen that has used
that term, they mean slower tempo. That is what they mean. They don't mean that
there's been an indication of a strategy shift to low-intensity operations by
Israel. That being said, the obvious question there is, does the slower
tempo mean that they're changing strategies? It could, in generally
speaking, when countries switch from something that is full scale to low
intensity, there is a period of a slower tempo, but I haven't seen any evidence
of that.
I haven't seen any evidence that they have a strategy shift.
We know the Biden administration pushed for it, but I mean, the Biden
administration has pushed for a lot of things that have not occurred.
So, the slower tempo could mean a shift to low intensity operations, but there's no guarantee
of that.
Why do people care?
Because generally speaking, low intensity operations are much, much safer for civilians.
That's why people care.
isn't, I haven't seen any real evidence that that's their plan. Could it be? Sure,
but I haven't seen that yet. The slower tempo could just be them regrouping. It
could be them pausing to reform up to launch that offensive that they say they
have a date for now when it comes to RAFA. It could be a lot of other things.
So, the reporting that's saying it's a lower intensity, it's not wrong because it is at
a slower tempo, but they have not moved to low intensity operations as of time of filming.
That hasn't happened.
So that's what's going on.
And I would guess we would know within a week.
If they are going to move to low intensity operations, the slower tempo, it probably
wouldn't last longer than a week without real signs, a week longer, without real signs
that they have pivoted their strategy, but we have to wait to see that.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}