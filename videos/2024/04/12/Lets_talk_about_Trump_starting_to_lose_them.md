---
title: Let's talk about Trump starting to lose them....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=GDssHYcLvmg) |
| Published | 2024/04/12 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump is losing some of his core base of voters due to a specific issue that Republicans are finding difficult to navigate.
- Republicans caught in a tough spot due to differing views on a 16-week ban issue.
- Single-issue voters are key players in this dilemma.
- The issue revolves around reproductive rights and restrictions.
- The statistics and implications of enacting bans at different weeks are discussed.
- Moderates embracing more moderate ideas risk losing support from the core base.
- Republicans are struggling to find a balance between appealing to independents and retaining their base support.
- The Republican Party's historical fusion with certain causes is now causing turmoil.
- The dilemma of losing support from either independents or the core base is emphasized.
- The issue of single-issue voting blocs and their impact on elections is significant.

### Quotes

1. "This is what happens when you're the dog that caught the car."
2. "In the eyes of that group, they're not doing anything because what's being proposed, it doesn't accomplish what they want."
3. "This issue is going to play more in 2024 than I think people are giving it credit for."
4. "If they go after the independence they will certainly lose some of their base."
5. "So the thing to keep in mind is that, well, the person we know, no, I definitely believe him when he says he's not voting for him now."

### Oneliner

Trump's loss of core voters over a 16-week ban dilemma reveals Republican struggles between base support and appealing to independents, setting the stage for significant impacts in future elections.

### Audience

Political analysts, Republican strategists

### On-the-ground actions from transcript

- Analyze the implications of single issues on voter behavior (suggested)
- Understand the challenges faced by Republicans in navigating conflicting views on key issues (suggested)
- Engage in productive discourse on reproductive rights and related restrictions (suggested)

### Whats missing in summary

Deeper insights into the intricacies of Republican Party dynamics and the potential repercussions of failing to address key issues effectively.

### Tags

#RepublicanParty #VoterBehavior #ReproductiveRights #SingleIssueVoters #Elections


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about how Trump is losing some of them, his core
group of voters, that core base, and why a certain issue is so
difficult for Republicans to navigate.
We're going to talk about statistics and it's an interesting one because it's
going to require the majority of people who watch this channel to step outside of their
own beliefs and look at something from a different point of view.
So we are definitely talking about how Republicans caught the car.
This topic has come in in a bunch of different ways, but we're going to use an example that
happened to a friend of mine. He was talking to this person who we know, and
this person we know, they're Republican, super Republican, like Trump stickers,
Trump flags. I think, I think he's an evangelical. He says that he's not voting
for Trump now because he's a single issue voter and he views Trump as a quote
sellout because Trump is, according to him, in favor of a 15-week ban. Now I went
back and looked. There's reporting in February that says that he privately said
that he was in favor of a 16-week ban so we're going to use that because that's
what the reporting says. You, most people watching this channel, you're in favor of
reproductive rights, family planning, all of that stuff. You hear a 16-week ban
and you're like no, no way, absolutely not. Here's the thing, those people who
are in favor of restrictions and they truly believe in this cause and this is
Is there a single issue?
They're also saying, no, no way, why?
If you're a single issue voter, if you have a cause that is near and dear to your heart,
you know a lot about it.
There's a whole bunch of people watching this channel who are very unhappy with how
Biden has handled the Middle East.
I would be willing to bet that most of you would be able to give me an estimate as to
the number of lost, that's pretty close. They're the same way. They know the numbers.
So, using numbers from 2021 from the CDC. Why 2021? Because it's before DOBS and that's going to
become important in a minute. Using those numbers, if you enacted a 16-week ban, what percentage of
procedures, would you stop? Take a guess. I can't tell you exactly because the
CDC numbers, their milestone is 13 weeks. So if it was at 13 weeks sooner, it would
stop about a little more than 6%.
It's not good enough for them.
That's why they're mad.
Now back in March, Carrie Lake out in Arizona, she said she believed that the issue would
settle somewhere between 15 and 24 weeks.
take 20 weeks because that's kind of in the middle of that and that's one of the CDC milestones.
If it was a 20-week ban, what percentage would it stop?
About 1.
About 1%.
And this part is anecdotal.
This isn't part of the CDC numbers.
But from what I understand, most of that 1%, those would be exemptions, like moms at risk.
So a 20-week ban is basically throwing away daubs.
It's going back to that.
And for this group of people, that was their big win.
And Republicans who are embracing the more moderate ideas, they're throwing that win
away.
the eyes of that core single-issue voter group.
At 24 weeks, I have no idea what it would be.
So this is why this issue is so difficult for Republicans.
The moderate position, it's as if daubs never happened.
Sure, they might appeal to some independence that way,
but they lose that core base, that activated base
that has supported them for so long
because they're unhappy with it.
If they go the other way
and they go to those really early ones,
well, they lose the independence.
They need both to win.
This is what happens when you're the dog that caught the car.
For a very long time the Republican Party fused with this cause.
They made it their thing.
Now they have the power to do something about it and they're not doing anything.
In the eyes of that group, they're not doing anything because what's being proposed,
it doesn't accomplish what they want it.
I'm sure there are Democratic Party members who can relate to this.
So the thing to keep in mind is that, well, the person we know, no, I definitely believe
him when he says he's not voting for him now.
But I would imagine that most singly-issued voters like this, they would probably still
vote Republican.
it might cost a point or two and if you're a candidate who, hypothetically
speaking, already lost to the person you're running against once, you need
every point you can get. If you're in a competitive district, you need every
point you can get. This has put them in a position where they have no clue how to
navigate it because if they go after the independence they will certainly lose
some of their base. If they appease their base they will certainly lose
independence. They're in a tough spot and to understand why you have to
understand these numbers because that single issue voting bloc they know
these. They absolutely know these. So that's what's going on. That's why there
is so much turmoil in the Republican Party because they made so many promises
for so long and now they're finding out they're really hard to keep and if they
don't keep them they know that some of their base is gonna look at them like
they sold out. And in this case, it's something that they view as moral. In some cases, it's
an edict from God. They're in a tough spot. This is going to play more in this election.
This issue is going to play more in 2024 than I think people are giving it credit for. Because
Because right now most people are focused on the independents.
Understand if the Republican party appeases them, they've got a whole new issue on their
hands.
Anyway it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}