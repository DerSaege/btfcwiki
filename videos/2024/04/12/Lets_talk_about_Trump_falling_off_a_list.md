---
title: Let's talk about Trump falling off a list....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=tGGM1rV2znc) |
| Published | 2024/04/12 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's name has been removed from the Bloomberg 500 list of top billionaires in the world, causing a significant blow to his ego as he values his perceived wealth being talked about.
- Trump's fall from the top 500 billionaires is attributed to his media company, Trump Media, which saw its shares plummet in value from $78 to about $2.
- Being no longer on the top billionaires' list is a major blow to Trump, who heavily relies on his brand and name for his image.
- In the past, setbacks like this have caused erratic behavior from Trump, with him likely to compensate by boasting more about his wealth and degrading his perceived enemies like Bloomberg.
- This incident could exacerbate Trump's current issues, potentially leading to a string of late-night tweets displaying further erratic behavior.

### Quotes

1. "No longer being one of the top 500 billionaires in the world, but still being in the top 1,000, you know. That's not something that would upset most people."
2. "He often makes other political mistakes at the same time."
3. "I could see one complicating the other."
4. "I expect a whole lot of Trump talking about his wealth, talking about how rich he is."
5. "This is the type of thing that in the past we've seen it kind of get in his head and cause erratic behavior."

### Oneliner

Trump's removal from the top billionaire list could trigger erratic behavior as he heavily relies on his wealth and brand for validation and may resort to boasting and attacking perceived enemies.

### Audience

Political observers, media analysts

### On-the-ground actions from transcript

- Monitor Trump's public statements and behavior for signs of erratic behavior and potential political mistakes (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of Trump's reaction to being removed from the Bloomberg 500 list and how it may impact his behavior and rhetoric.

### Tags

#Trump #Bloomberg #Wealth #Brand #ErraticBehavior


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about Trump
and how he might feel and react to the news
that his name has been removed from a certain list.
It is no secret that the former president
a big fan of people talking about his perceived wealth. There's a list called
the Bloomberg 500, top 500 billionaires in the world or whatever, and recently he
was on it in, I want to say in the top 300, and he fell off. He's no longer on
it anymore, at all. He's not in the top 500. Now, the generally accepted wisdom on
why has to do with Trump media. Those, the shares, I want to say they started
trading at $78. Last time I looked, it was about half that. There is reporting
that some experts have looked at the numbers and basically by going through
like a fundamental approach to it have determined that the real value or worth
is about two bucks a share. So while certainly this is going to be meme-worthy
and people are going to definitely talk about this, the thing is when it comes to
Trump, this is a big deal for him, you know. No longer being one of the top 500 billionaires
in the world, but still being in, I don't know where he is, but let's say he's still
one of the top 1,000, you know. That's not something that would upset most people. Trump,
going to bother him because right now he is staking a lot on his brand and his
brand is his name, which is no longer on that list. This is the type of thing that
in the past we've seen it kind of get in his head and cause erratic behavior.
I would expect a whole lot of Trump talking about his wealth, talking about
how rich he is, and probably going the other way and talking about how little
other people or other companies, anybody that's a perceived enemy, I would imagine
Bloomberg is in for it on this one, how they're not worth as much. And when he
gets into these phases that he goes through, he often makes other political
mistakes at the same time. This coming at the same time as his issues with his
family planning stance, I could see one complicating the other. I would be ready
for, you know, a string of like late-night tweets or whatever they're
called over there, in which he displays more erratic behavior.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}