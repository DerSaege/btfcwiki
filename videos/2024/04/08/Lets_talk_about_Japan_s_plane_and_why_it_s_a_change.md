---
title: Let's talk about Japan's plane and why it's a change....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=zIEX0NS_5UM) |
| Published | 2024/04/08 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Japan has decided to authorize the export of a fighter plane that they are co-developing with Italy and the UK.
- This decision marks a significant departure from Japan's pacifist stance since 1945.
- China has expressed serious concerns about Japan's move to export arms, urging Japan to respect security concerns and its history of aggression.
- The Japanese government has reportedly decided not to export the fighter plane to Taiwan.
- The futuristic fighter plane is expected to be in service by 2035, with real deployment likely by 2037.
- This shift in Japan's export policy is a major change, as Japan has traditionally refrained from exporting arms.
- Despite China's concerns, other neighboring countries have not vocalized opposition to Japan's decision.
- Japan's move to export arms is a gradual change from their historical stance on military exports.
- The Global Combat Air Program involving Japan, Italy, and the UK is a significant collaboration in developing advanced military technology.
- The export of this fighter plane is not an immediate security concern, given its expected deployment timeline.

### Quotes

- "Japan has decided to authorize the export of a fighter plane that they are co-developing with Italy and the UK."
- "China has expressed serious concerns about Japan's move to export arms, urging Japan to respect security concerns and its history of aggression."
- "The futuristic fighter plane is expected to be in service by 2035, with real deployment likely by 2037."

### Oneliner

Japan's decision to export a fighter plane marks a major departure from its pacifist history, raising concerns from China and signaling a significant shift in its military export policy.

### Audience

Policy analysts, international relations experts

### On-the-ground actions from transcript

- Contact policymakers to advocate for transparency and accountability in Japan's military export decisions (exemplified)
- Stay informed about developments in global military collaborations and their implications (suggested)

### Whats missing in summary

The full transcript provides additional context on Japan's historical pacifist stance and the reactions from neighboring countries, offering a comprehensive understanding of the significance of this shift in military export policy.

### Tags

#Japan #MilitaryExport #GlobalRelations #China #Pacifism


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Japan, and planes,
and exports, and questions, and provide a little bit of context
to what's going on.
Because while this is being reported on,
a lot of the reporting seems to be leaving out some key pieces
as to why it's a big shift, it's a big departure from the way
things have have been for quite some time. Okay, so what's the headline? Short version,
Japan has decided that it will authorize the export of a fighter plane that they are co-developing
with Italy and the UK as part of the Global Combat Air Program. When it's done, they'll export it.
The question that came in the most is why does this matter? Why is this a big
deal? Because if you're an American exporting arms is normal. Japan doesn't
do that. Something occurred around 1945 and ever since then they have been very
pacifist and they do not do this. This is a major departure from a very long
tradition at this point, and it has raised eyebrows. Particularly China, and
their foreign ministry said, these moves trigger serious concerns among Japan's
neighboring countries and the international community. We urge Japan
to earnestly respect the security concerns of neighboring countries, deeply
reflect on its history of aggression, commit itself to the path of peaceful
development and earn the trust of its Asian neighbors and the international
community through concrete actions." It is worth noting that China seems to be
the only country that was kind of like, hey, what are you doing? But at the same
time in that region, I mean, the Chinese opinion matters. Now, it's worth
noting that according to the rumor mill, the Japanese government has already
decided that when they export, Taiwan will not be considered as a country to
export it to. If that rumor becomes a public commitment, I feel like Chinese
concerns about this would probably go away pretty quickly. The plane itself is
supposed to be very futuristic. It's next generation stuff. So next generation
that the expected service date is 2035. If you go by normal timelines, that means
in real life you're probably looking at 2037. So this is not an immediate
security concern for anybody. But the big shift here is Japan being okay with
exporting stuff. Slowly but surely they've done little things that have
altered that stance. But this is a big step and that's why it raised
eyebrows. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}