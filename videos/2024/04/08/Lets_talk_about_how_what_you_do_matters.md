---
title: Let's talk about how what you do matters....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=zSoPHEX0l1Y) |
| Published | 2024/04/08 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Received a message from someone in the community about a story that needed to be shared.
- Story of a man with a severe alcohol addiction who turned his life around with the help of the community.
- The man went to rehab after a heartfelt question about his pain, not his addiction.
- After 20 years of drinking, he got sober, went to therapy, and rebuilt his life.
- He went back to college to become a food scientist after being inspired by Beau's channel.
- Diagnosed with stage 4 cancer in 2023, he fought with courage and spirit.
- Shared a deep bond with the person narrating the story.
- Eventually, he had to move to hospice care and passed away.
- His story is shared to inspire others to find courage, listen, and be inspired by the community.
- A testament to the impact of community support and the power of personal transformation.

### Quotes

- "Don't ask why the addiction, ask why the pain."
- "This is a reminder that everything we do matters."
- "He's an inspiration for finding the courage to change, listen, and be inspired by others."

### Oneliner

Beau shares a powerful story of a man's journey from addiction to inspiration, showcasing the impact of community support and personal transformation.

### Audience

Community members

### On-the-ground actions from transcript

- Support individuals struggling with addiction through open and honest communication (suggested)
- Show compassion and patience to those in need of help (exemplified)
- Be a source of inspiration and courage for others in difficult times (implied)

### Whats missing in summary

The emotional depth and impact of witnessing one man's journey from addiction to inspiration through community support.

### Tags

#CommunitySupport #AddictionRecovery #PersonalTransformation #Inspiration #Courage


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we are going to talk about community, and what we
do, and how it matters, even if you don't know it.
We're going to do this because I got a message.
And it's from the, well, it's in the story.
I'm going to read almost all of it.
The person who sent it said, I'm putting down
his story because I feel someone in the community should know.
If you've been around a while, you probably
want to watch this.
He wasn't always a good guy.
Charming, polite, affable is all.
get out, but had a pretty nasty alcohol addiction. He hid it from everyone, including me, effectively
and for a long time, until he couldn't, which was soon after we'd married.
I divorced him a few months later because I'd seen that story before and wasn't going
to love it. He went to live with friends, then family, and finally burned enough bridges
to end up out on the street. I was scared of him at the time. He was around our downtown
area, which is where she worked, and just kept showing up and looked so horrible, angry
and demanding attention. He wasn't threatening, but it felt like it wasn't far away. I'd
started following the channel on a YouTube recommendation and kept following. One day,
I decided to ask him a question another Bo Peep had put forth in a series of comments
on addiction.
Don't ask why the addiction, ask why the pain, which changed our lives.
We started talking honestly for the first time why he was so pained after a few weeks
of being softer, kinder, and more patient than I knew either of us could be, he went
to rehab, all on his own.
After 20 years of solid drinking, because he started young, he put it down and never
slipped.
He did a lot of work, went to therapy, got into a halfway house, got a job, and suddenly
was off the streets.
He was also following Bo in the channel by then.
We hadn't been romantic at all, just friends through the first years of sobriety, when
the channel's topics and the folks commenting began discussing masculinity, that it is self-defined
for the most part, and set him to looking at himself in a different way.
He asked if we could go to therapy to repair and deepen our friendship, because I had been
his best friend and he missed me.
Then COVID happened.
He moved back in to keep us all safe, eventually quit his job because it wasn't serving him
at all anymore, and went back to college.
I have photos of his first day at school, where he's sitting there on the porch in
front of a laptop. He decided that after being a cook, he would take the chance
slowing down, had given him, and learn about something he'd been curious about
but never knew, just like an astronaut someone talked about. He decided to
become a food scientist with an environmental science minor. He was
cautiously optimistic that I was over the moon. He made the Dean's List his
first semester back and everyone after. He cried when hearing about the letter
from the trailer park New Year's guy and said that's the same as me. I hope he's
is lucky. Through the years, the channel has inspired, comforted, and sparked all kinds
of discussion. It, Bo, the team, and everyone involved has changed our lives. It changed
our lives again when in July of 2023 he was diagnosed with stage 4 cancer. As a
tremendous collector of t-shirts, I added the Bo's boxing club shirt, which he
often wore to chemo, which made for great conversations. When he lost his ability
to speak, he would bring up a relevant point on his phone. When he lost his
hearing in his left ear, he happily charged many right ear buds so he could
be distracted three times a day and have people he felt he knew he could
talk to, even as his world got smaller and smaller. I got him out of the
hospital on March 25th after a long stay in the hospital over a
misinstalled peg tube that left him too weak, skinny, and atrophied to really
recover. He believed he would all the way, ever hopeful, ever fighting. His oxygen
went too low on the 31st of March and he went into sepsis. I got him to the
hospital in time, but not in time. He deteriorated during the week he was
there. His illness had just pushed me to push him to contact his family last
January, and they came down. Spending that week in fixing all the hurt and
estrangement they had over the last 12 years, not perfect, but good enough. Three
Three days ago, he and I agreed to hospice at home.
Two days ago, it became obvious to me that I wouldn't be able to safely care for him,
and we would have to go to a facility.
It's one of the hardest things I've had to do, watch the light drain out of him.
He figured if he couldn't live, he would die with me in our bed.
Today he was moved to a very nice hospice house, and tonight I'm sitting up while
he sleeps, waiting.
He's 44 years old.
I'm telling this story in its entirety because he's an inspiration.
Not for getting sick, fighting, and too soon dying, but for finding the courage to change,
listen and be inspired by others, for making a life he wanted to fight for after not caring
at all, for allowing strangers on the internet to give him courage and share enough to figure
out what he wanted in his life at all and how to be his own man.
This is a good everyone did as a community without even knowing it.
This is a reminder that everything we do matters.
Anyway it's just a thought.
have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}