---
title: Let's talk about Good, Johnson, McCarthy, and the House GOP....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=sAYIyryntlU) |
| Published | 2024/04/08 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the internal dynamics within the Republican Party in the US House of Representatives involving Good, Johnson, and McCarthy.
- Allies of McCarthy are working to prevent Good from being reelected through a primary challenge.
- Despite past tensions, Good is seeking support from Johnson, the current speaker, to combat the primary challenge.
- Johnson's role is to secure reelection for all Republicans to maintain the majority and his speakership.
- McCarthy, who previously supported Good's election, may now be backing efforts to primary him, potentially due to past grievances.
- Good has not disclosed whether he will support Marjorie Taylor Greene in a motion against Johnson, complicating the dynamics further.
- Johnson is facing significant infighting within the Republican Party, being labeled as a "House babysitter" due to the intense internal politics.
- Uncertainty surrounds Johnson's response to the situation, considering the delicate balance between various factions within the party.

### Quotes

1. "McCarthy, when he was speaker, he helped Goode get elected in 2020 from what I understand, like used millions to do it."
2. "The internal politics of the Republican Party could absolutely be a soap opera at this point."

### Oneliner

Beau delves into the intricate power play between Good, Johnson, and McCarthy within the Republican Party, showcasing how loyalty and ambitions intertwine, turning internal politics into a captivating drama.

### Audience

Political observers

### On-the-ground actions from transcript

- Contact your representatives to express your views on internal party dynamics (implied).
- Stay informed about local political developments and how they can impact your community (implied).

### Whats missing in summary

Insights on the potential consequences of these power struggles on policy-making and governance within the Republican Party.

### Tags

#USPolitics #RepublicanParty #InternalDynamics #PowerStruggles #PartyPolitics


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk a little bit
about the US House of Representatives
and some of the unique internal dynamics
going on within the Republican Party there,
specifically among Good, Johnson, and McCarthy.
Y'all remember him?
He was the Speaker of the House for a bit.
So it certainly does appear that allies of McCarthy want to make sure that good
doesn't get reelected, and they're using a primary challenge to do this.
Now, keep in mind, these are all Republicans, everybody we're going to
be talking about is a Republican.
And this primary challenge does appear to have Good, I mean, a little bit shook.
Looks like it might, you know, it stands a decent chance of being successful.
So Good has reached out to Johnson, the current speaker, and kind of said, you know, like some help, endorsements,
something along those lines.  lines. Which, I mean, that in and of itself is funny because Good and the House Freedom
Caucus that he leads has kind of been a giant, you know, pain in the neck for Johnson. But
it is Johnson's job to try to get all of the Republicans reelected. That's how they maintain
the majority and that's how he maintains his speakership. I believe he refers to it
something like protecting the flock or something like that. But there's something else that Johnson
has to consider. McCarthy, when he was speaker, he helped Goode get elected in 2020 from what I
understand, like used millions to do it. And then not too long afterward, well McCarthy got pushed
under the bus, lost his speakership. Maybe it was something that upset him to the point where he
might have encouraged his allies to primary good. So if you are aware that the former speaker was
pushed under the bus by a certain Republican representative.
I mean, if you were the current speaker,
how much would you really help? I mean, it's your job, sure,
to get everybody re-elected. But,
I mean, if hypothetically it's a super red district and whoever wins that
primary is likely to win that seat,
I mean, that really doesn't hurt your majority, does it?
It's also worth remembering that so far, Good has not announced whether or not he would
support Marjorie Taylor Greene in that whole motion to vacate against Johnson, the person
that he would like to endorse him.
I mean, that's something else. That is something else. Johnson, with all of the
infighting at this point, he's not the House speaker. He's the House
babysitter. That's what it's devolved to in the Republican Party. The infighting
has gotten that extreme. What Johnson's going to do? I don't know. I doubt he's
decided yet, but it does appear that McCarthy's assumed attempt, again we
don't have confirmation that McCarthy is behind all of this stuff that is
happening to people who he might have indicated that he wanted to kind of pay
back. There's no way for us to confirm that, but I imagine it's going to be a
fun couple weeks. I don't think that's over yet. I don't know what Johnson's
going to do about all of this, but he might not want to upset the more moderate
Republicans, the less right-wing Republicans maybe, by interfering in this.
And, let's be honest, the House Freedom Caucus has not been very helpful to Johnson.
The internal politics of the Republican Party could absolutely be a soap opera at this point.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}