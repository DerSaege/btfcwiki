---
title: Let's talk about aid, amendments, and theories....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ommG1KuIU18) |
| Published | 2024/04/18 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains his previous reference to Marjorie Taylor Greene as the "space laser lady" and why he started using her name.
- Discovers an image of an amendment introduced by Marjorie Taylor Greene to divert funding for space lasers in the Israeli aid package.
- Mentions other amendments introduced by her, including diverting funding for recovery from a fire in Hawaii and a conspiracy theory about laboratories.
- Reads a bizarre amendment requiring members of Congress to conscript in the Ukrainian military if they vote in favor.
- Comments on the shocking nature of these amendments being part of the congressional record.
- Speculates on the social media attention Marjorie Taylor Greene may be seeking through these amendments.

### Quotes

1. "Is this real?"
2. "Marjorie Taylor Greene introduced an amendment to the Israeli aid package to divert funding as needed to develop space lasers."
3. "But wait, there's more."
4. "These are now part of the congressional record."
5. "It is both totally unsurprising and completely shocking at the same time."

### Oneliner

Beau reveals shocking amendments by Marjorie Taylor Greene, including diverting funds for space lasers and a bizarre conscription requirement, now part of the congressional record, questioning the social media attention sought through these actions.

### Audience

Those following political developments.

### On-the-ground actions from transcript

- Contact your representatives to express concerns about these concerning amendments (suggested).
- Stay informed about legislative actions and hold elected officials accountable (implied).

### Whats missing in summary

The full transcript provides detailed insights into the concerning amendments introduced by Marjorie Taylor Greene and the potential implications on legislative processes.

### Tags

#MarjorieTaylorGreene #Amendments #Congress #Legislation #PoliticalAnalysis


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Marjorie Taylor Greene and amendments and something
y'all aren't going to believe.
You know, for a long time on this channel, and I mean a long time, you would never hear
those words, Marjorie Taylor Greene.
You would never hear that phrase because that's not what I referred to her as.
And I started using her name in the last month or two.
And a number of you commented on it and asked, you know, why don't you call her the space
laser lady anymore?
And I kind of just thought, how long can you hold a statement like that over somebody's
head?
Thought it had been long enough.
I was wrong.
So I get this text.
Is this real?
And it's an image that certainly appears to be an amendment introduced to legislation
in the House.
So I hopped on the internet and went to the House website and looked.
Marjorie Taylor Greene introduced an amendment to the Israeli aid package to divert funding
as needed to develop space lasers in the southwest on the Israeli aid package, mind you.
If y'all remember where that quote came from, yeah.
So just out of my own bizarre curiosity, I started looking around, seeing what else was
there. On the Ukrainian aid package, she introduced something to divert funding
and basically help with recovery for the fire in Hawaii. Remember that
devastating fire? And a lot of you may not know this because you probably don't
frequent those parts of the internet, but there was a conspiracy theory about how
that fire started. That is something else. But wait, there's more. There's
another one referencing a conspiracy theory about laboratories, and there's...
I want to make sure I read this to get this right.
Any member of Congress who votes in favor of this act shall be required to
conscript in the Ukrainian military? To conscript what exactly? Yeah, so that's
That occurred. These are now part of the congressional record. It is both totally
unsurprising and completely shocking at the same time. But I'm sure that, you know,
the people of Georgia are going to be real happy over the social media clicks she gets
from this.
Anyway it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}