---
title: Let's talk about the dismissed impeachment and what it means....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=6RrlKY5K1Tg) |
| Published | 2024/04/18 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- House Republican efforts to impeach the Secretary of Homeland Security fizzled in the Senate.
- Senate dismissed the articles of impeachment and deemed them unconstitutional.
- Despite anticipation, the efforts to impeach didn't lead to any tangible outcome.
- Republicans lacked the votes for conviction, not just for dismissal.
- The whole process seemed like a show, with Republicans not meeting the required bar for conviction.
- Impeachment proceedings against Biden also lack senatorial support and are more about engagement than substance.
- Lack of clarity on what Biden did wrong indicates a weak case for impeachment.
- Social media hype around impeachment contrasts sharply with the actual outcome.
- Republicans should take this as a wake-up call regarding the evidence required for impeachment.
- The entire ordeal serves as a cautionary tale for political engagement and expectations.

### Quotes

1. "They barely had the votes to dismiss, let alone convict."
2. "It's all a show to keep people engaged. Nothing more."
3. "They don't have the votes to impeach a president without significant evidence."

### Oneliner

House Republican efforts to impeach fizzled in the Senate, lacking votes for conviction and revealing a lack of substance beyond political showmanship.

### Audience

Political observers, Republican base

### On-the-ground actions from transcript

- Pay attention to the evidence and substance behind political actions (suggested)
- Stay informed about the political processes and avoid getting swept up in hype (suggested)

### Whats missing in summary

Detailed breakdown of the Senate proceedings and implications for future impeachment efforts.

### Tags

#Politics #Impeachment #Senate #RepublicanBase #PoliticalEngagement


## Transcript
Well, howdy there, Internet people, it's Beau again.
So today, we are going to talk about what happened
in the Senate and how House Republican efforts
have fizzled yet again, and we'll just kind of run through
what occurred very quickly and what that means
for other endeavors, okay.
So quick recap for those who missed the lead up to this.
Last week, the Republicans in the House, they made the move to impeach the Secretary of
Homeland Security, said at the time that if they took the articles of impeachment over
to the Senate right away, that it would be dismissed right away.
If they waited a week, there would still probably be a vote to dismiss the articles right away,
that they might be able to put on a little bit more of a show and get a sound bite on
Fox News.
They waited a week, it went over, and it was dismissed right away.
The Senate dismissed the articles of impeachment and determined one to be unconstitutional.
That's over.
All of those hearings, all of that lead up, all of the social media posts, all of that
engagement and nothing happened. Yet again. Seems to be a trend, just saying.
Now, I know that people are saying, but they barely had the votes to dismiss.
Yeah, I mean that's true. They barely had the votes to dismiss, but that means that
Republicans were nowhere near having the votes to convict because you need more
votes to convict. The whole thing was a show and it got dismissed out of hand. So
that's it. That's the end of it. Very anticlimactic for people who believed
that this was going somewhere. It certainly didn't help that you had a
number of prominent Republicans basically saying, yeah I mean I'm probably
going to vote to dismiss as well, but we should at least have a debate or
something like that. When you have Republicans saying that, they didn't
meet the bar. They didn't meet the bar. And because you had Republicans saying
that, the Democratic Party felt very comfortable just dismissing it out of
hand, because they knew there was zero chance of the votes being there for
conviction. It just, it didn't meet the bar. So, how can this further inform our decision-making?
If you are somebody who believes that the impeachment inquiry into Biden is going somewhere,
please understand the House committees are going to need senatorial approval and they don't have
the votes. It's not going anywhere either. It's all a show to keep people engaged.
Nothing more. I mean, please keep in mind that after all of this time, they still
haven't actually even been able to articulate what it is they think he did
wrong. Even at this point, they can't specify what the high crime or
misdemeanor is. That should be a clear indication that they don't have it. They
don't have it. It will probably, if they were to pursue it, it would probably be
something else that is determined to be unconstitutional. So as you see the
Social media posts about that move forward, remember all of the social media
posts about this impeachment and how certain they were. How they framed
everything for their base and then look at what happened. They didn't have the
votes to impeach a cabinet member. They don't have the votes to impeach a
president without significant evidence, which we still haven't seen. This should
be a wake-up call for a lot of the Republican base. I doubt that it will be,
but it should be. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}