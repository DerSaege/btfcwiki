---
title: Let's talk about Johnson, Ukraine, and what's next....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=4ik4v-n6IHk) |
| Published | 2024/04/18 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing Johnson's statement on aid packages and its implications.
- Challenging conventional wisdom that Johnson didn't support aid for Ukraine because of his association with Trump.
- Johnson publicly supporting the aid for Ukraine and giving an impassioned speech in its favor.
- Implications of Johnson's support for aid for Ukraine on his political positioning and the Democratic Party.
- Johnson's strategic moves to position himself as the de facto leader of the Republican Party.
- Warning against underestimating Johnson's political acumen and recognizing his tactics.
- The need to respect political opposition and recognize successful strategies.

### Quotes

1. "He's trying to get it all through. He is trying to get it all through."
2. "If he is successful at this and retains his seat, the takeaway is that the Democratic party had better recognize what's going on."
3. "He simultaneously outplayed the Twitter faction, Trump and Biden at the same time, cannot underestimate him, cannot underestimate him."
4. "It's one of those situations where you need to respect your opposition because if you don't respect them, they'll continually outplay you."
5. "It looks like you actually did have a politician play 4D chess and mean to do it."

### Oneliner

Beau analyzes Johnson's surprising support for aid to Ukraine and warns against underestimating his strategic political moves.

### Audience

Political analysts, Democratic Party members

### On-the-ground actions from transcript

- Recognize and analyze political strategies employed by leaders (implied)
- Respect and acknowledge the strength of political opponents (implied)

### Whats missing in summary

Insights into the nuances of Johnson's political maneuvers and the potential impact on party dynamics.

### Tags

#PoliticalAnalysis #AidPackages #RepublicanParty #DemocraticParty #StrategicMoves


## Transcript
Well, howdy there, internet people, let's bow again.
So today, we are going to talk about
the various aid packages.
We are going to talk about Johnson and his statement
and what it means,
because there are some pretty big implications to it.
Three, maybe four days ago I put out a video,
and I'll put it down below,
where I questioned conventional wisdom
something. Because when it comes to the aid package, Johnson splitting it up the
way he did, the conventional wisdom on that was that he wanted everything to go
through except for the aid for Ukraine. The reason people believed that was
because Johnson went down there, hung out with Trump, held hands, sang kumbaya, all
of that stuff, which of course means that Johnson is Trump's errand boy. And we know
that Trump doesn't want to do anything that would upset Russia so the Ukrainian
aid not going to happen. That was conventional wisdom. I suggested that
that something else might be the case and that Johnson is trying to get it all
through because it positions him very well if all of it goes through in this
method.
Tonight Johnson came out and publicly supported the aid for Ukraine.
And for Johnson, he gave a pretty impassioned speech in favor of the aid for Ukraine.
He said he believed the intelligence, that this wasn't a game, that history was going
to judge us, so on and so forth.
He said that if Putin won in Ukraine, he'd be going to the Balkans or knocking on Poland's
door next. He has a son at the Naval Academy and to him, you know, this is a big deal because
he would rather send American bullets to Ukraine than American boys. He's trying to get it
all through. He is trying to get it all through. So, what does this mean? If he is successful
at getting it all through and retaining his seat afterward.
He undermined the Twitter faction.
He put daylight between him and Trump, set himself up as the de facto leader of the Republican
Party, and put Biden in a position where he kind of had to eat his words by giving Biden
exactly what he wanted, but doing it in a way that Biden said he wouldn't accept and
in a way that makes it look like the Republican Party isn't as dysfunctional as it is.
It makes him look good all the way around.
If he is successful at this and retains his seat, the takeaway is that the Democratic
party had better recognize what's going on, they had better see it, and they had
better start treating him as McConnell in the making, because that's who he is.
If he succeeds at this, he simultaneously outplayed the Twitter faction, Trump and
and Biden at the same time, cannot underestimate him, cannot underestimate him.
Because while in this case, I think a whole lot of people are going to be happy about
this, because he is, at least it certainly appears, he's trying to get the aid for Ukraine
through.
But understand, this guy, his domestic policies are not, they're not something that even
Even most conservatives who watch this channel would be behind.
It's one of those situations where you need to respect your opposition because if you
don't respect them, they'll continually outplay you.
If he's successful at this, the Democratic Party has to recognize what happened.
still sitting there playing the, you know, I'm the embattled speaker and all of
that stuff. That's not what it looks like. It looks like you actually did have a
politician play 4D chess and mean to do it because while this is a surprise,
I mean I didn't see it till three days ago, he had to have had this planned for
while. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}