---
title: Roads Not Taken in Ukraine EP 34.5
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=zimtIqgy2m4) |
| Published | 2024/04/18 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Today's episode is focused on Ukraine and recent developments that are critical for understanding the context.
- Western estimates suggest Russia has 8 to 18 months of staying power in Ukraine.
- A Chinese professor wrote that Russia is "doomed in Ukraine" due to deficits in the Russian military, particularly with intelligence and micromanagement by Putin.
- Russia is slowly making gains in Ukraine but at a high cost, which could continue unless US aid arrives.
- Ukraine has made significant strides in drone warfare, potentially developing a drone battleship.
- Both Russia and Ukraine are mobilizing heavily, relying on conscripts, which is not ideal for either side.
- Ukraine's success with drones has led Russia to deploy a new jammer for drones, sparking intense operations to obtain it.
- Russia is telling Iran to avoid further provocations to protect Iran's production capabilities that Russia relies on.
- China may provide drone and missile tech to assist Russia, despite limits in their friendship.
- Norway is providing Ukraine with F-16s, and China has proposed a vague peace framework that Western nations criticize.
- Reports of widespread desertions in Hursan on the Russian side raise questions about potential partisan activity.
- The US aid for Ukraine is critical, as European support, while steady, is insufficient.

### Quotes

- "Russia is slowly making gains."
- "Ukraine has made huge advances when it comes to drone warfare."
- "Countries don't have friends, they have interests."
- "Trump's peace plan, after careful analyzation, is basically just surrender."
- "Having the right information will make all the difference."

### Oneliner

Beau provides critical insights into the evolving situation in Ukraine, from estimates of Russia's staying power to developments in drone warfare, urging the importance of US aid for Ukraine's war effort.

### Audience

Global citizens

### On-the-ground actions from transcript
- Send aid to support Ukraine (exemplified)
- Stay informed about the situation in Ukraine and advocate for increased aid (suggested)
- Monitor developments in the conflict and raise awareness in your community (implied)

### Whats missing in summary

Insights into the potential consequences of the ongoing conflict in Ukraine and the importance of international support for Ukraine's defense efforts.

### Tags

#Ukraine #Russia #USaid #DroneWarfare #InternationalRelations


## Transcript
Well, howdy there, internet people, it's Bo again,
and welcome to The Roads with Bo.
Today is April 18th, 2024,
and we're going to do a Roads Not Take an episode
that is topic specific.
So we will call this episode 34.5.
Today, we are going to talk about Ukraine
and stuff dealing with Ukraine
because there's been a lot of developments
that haven't been groundbreaking, but they're important
and they will become more important
as far as context goes in the coming months.
And none of it was able to make it to the other channel
because of, well, everything that's going on.
Okay, so starting off, one of the big questions
everybody wants to know, as far as Western estimates go,
They suggest that Russia has eight to 18 months left of staying power in Ukraine.
They've been pretty good at evading and getting around sanctions, however, they have issues
with mobilization, and those are likely to persist.
Some of their ability to get around the sanctions may also become limited due to other events
in the world.
Now, from the other side, a Chinese professor wrote a piece saying that Russia was, quote,
doomed in Ukraine.
It pointed to a number of deficits that the Russian military has.
I think the thing that was the most important really
had to do with their lack of intelligence
and how even if they had valid real-time intelligence,
it wasn't getting to the decision-makers
because Putin is doing a lot of micromanaging.
Now, on the actual field, Russia is slowly making gains.
They're paying a just ridiculously high cost for them,
but they're making gains.
That trend will likely continue unless US aid arrives.
It's important to remember that if Russia were to succeed
in capturing all of the territory it seeks,
they would finally have gotten to the hard part.
That's something to just bear in mind,
is that even if Russia advances enough
to claim a conventional victory as far as the territory goes, they've just gotten to
the difficult part, which is the more unconventional phase, and their troops are not up to that.
Any troop that they had that was even remotely trained well enough to handle that, they're
long since gone.
Ukraine has made huge advances when it comes to drone warfare and not just using them in
conjunction and in the air and the stuff that's widely talked about, but in developing new
things.
There is widespread speculation that they have developed what people are calling a drone
battleship.
And it appears that they had a bunch of, I think they were sticks, if you're familiar
with this stuff, I think they were sticks, rockets.
And they had a bunch of surplus Soviet-era stuff, and they took the spicy part off and
put it on a large boat.
That is going to make quite the impact, literally and figuratively.
Both sides are gearing up or have started larger mobilizations, and those mobilizations
are heavily reliant on conscripts.
That's not a good thing for either side.
Russia appeared to have deployed a new jammer for drones.
Because of Ukrainian success with drones, they were incredibly interested in this.
There was a high interest because the Ukrainians are winning the drone war and it is critical
to their success.
So when it appeared that a new system showed up on the field, they were like, well, let's
go still one.
And they launched what I understand to be an operation that turned out to be incredibly
intense to obtain a physical version of this new jammer.
It succeeded and they discovered that it was improvised.
It was not a new system.
It was just something that had kind of been cobbled together, and it didn't look that
effective.
Now for those people who participated, that was probably a huge letdown.
From the intelligence side in Ukraine, that's a huge win, because now they have confirmation
that the new system isn't actually a new system and they don't need to alter any
of their their behavior. So you have that as well. Russia is telling Iran to chill
out. Why? Countries don't have friends, they have interests. If things flare and
And there is a regional conflict in the Middle East.
Iran's war production is likely to be hit.
Even if it isn't hit and destroyed,
Iran will need it for, well, Iran.
Right now, Russia is incredibly reliant on Iran's capabilities
as far as their production capabilities.
So Russia is kind of indicating that it
be really cool if you didn't respond to any further provocations, because they don't want to see
Iranian production capability wiped out and therefore them lose as well.
Now on the other side of this, China might be getting ready to help Russia a little bit with
some drone and missile tech to keep it in the fight. Why? Same thing, despite claims of
friendship without limits or whatever, the limits are there and we've seen them a number
of times.
Russia and China are friendly-ish.
They're not friends.
And if you have a Chinese professor saying that Russia is doomed, you probably have a
a similar feeling within the Chinese military, within their intelligence.
So if they're helping, it's probably to prolong the damage that is being caused because it
helps knock Russia further out of the near-peer game, gets them away from the big table at
that international poker game where everybody is cheating.
Russia probably knows this but doesn't care because they desperately need the equipment.
Norway is setting up Ukraine with some F-16s and I think the training is actually already
occurring in Romania, but fact check that part if it's important to you.
Now China has also put forth a peace framework.
It's being widely criticized as being super vague.
Yeah, that's just how they do things.
When it comes to the Chinese side, they tend to outline rough principles for peace.
Not any specifics.
You're not going to hear, well, this much territory or anything like that.
It's just guiding principles for the negotiations, and then they get down to details at the table.
This has happened a number of times, and it seems like every time it occurs, Western nations
are like, this is too vague to even matter.
That's just how they do it.
They do it that way.
I don't know why they always expect something else from the Chinese side.
That's just how they handle it.
Trump's peace plan, after careful analyzation, is basically just surrender.
And then a bunch of questions came in about what is being reported as widespread desertions
in Hursan on the Russian side.
troops just leaving, disappearing in the night, all of that stuff.
And there have been a number of questions about, you know, is there any way that, you
know, that could be replicated somewhere else?
Or is it that bad in this region?
Why are they leaving?
It is important to remember that in the very early days of this, you had a whole bunch
of people who had specializations in unconventional warfare that suggested this exact region would
be a hotspot of partisan activity.
If you were a partisan group that was in some way helping Russian troops disappear, you
You might want to cast some of those disappearances as desertions.
It's worth noting that the information about widespread desertions in that area came from
a Ukrainian partisan group.
What is known is that some troops are no longer present.
Whether or not they just had an attack of conscience or a change of heart and decided
to leave the field, or perhaps somebody helped them leave the field, that's not exactly
known.
I would not be surprised if later on we found out that there was a lot of Ukrainian assistance
in helping them no longer be present.
So I have a lot of questions about those claims.
Just to tell you this, if I was a Russian soldier in this area,
I would not be alone at any point in time.
It seems like that might be one of the things that
sparks people to desert is if they are alone away from other Russian troops now
as far as them saying well we're not going to engage in combat operations
because there's that happening as well that's probably real that's probably
actually occurring exactly as is being reported and that is already occurring
in other places.
So this is kind of a rough overview of how things are shaping up.
Now you have some movement in the US on aid for Ukraine.
Understand it's critical.
European nations are, they're still doing a lot of piecemeal stuff, but it's relatively
steady.
But it's not enough.
It's important to remember that for a long time, the United States was viewed as the
arsenal of democracy or whatever term you want to use.
The production capability is here.
So it's going to be...
The Ukrainian war effort desperately needs the aid package that is at time of filming
still in the house, but there's been movement. We'll see what happens this week.
And that looks like it. So there you go. A little more information, a little more
context and having the right information will make all the
difference. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}