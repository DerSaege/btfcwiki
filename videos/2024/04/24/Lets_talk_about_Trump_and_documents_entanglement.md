---
title: Let's talk about Trump and documents entanglement....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ss_gkDyAyFs) |
| Published | 2024/04/24 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Updates on the slow-moving documents entanglement involving Trump and a co-defendant.
- New information suggests Trump's co-defendant was assured of a pardon by the former president in 2024.
- Trump was reportedly advised to give everything back to avoid indictment, to which he responded nonchalantly.
- The case against Trump seems open and shut based on publicly available evidence.
- Procedural delays, not the strength of the case, are causing the slow progress.
- If the case goes to trial, it could be extremely damaging for Trump.
- The outcome of the election may impact whether the case even goes to trial.

### Quotes

1. "Whatever you have, give everything back. Let them come here and get everything. Don't give them a noble reason to indict you, because they will."
2. "Okay, so that is something that if this case ever was to actually make it to trial, that's going to be incredibly damaging."
3. "This is pretty open and shut, especially given the nature of the charges."

### Oneliner

Updates on Trump's legal entanglement reveal potential damaging revelations if the case goes to trial, with an open and shut nature suggesting significant exposure for Trump.

### Audience

Legal analysts, political commentators

### On-the-ground actions from transcript

- Stay informed about updates in the legal proceedings involving Trump and his co-defendant (suggested).
- Analyze the potential implications of the new information on Trump's legal situation (implied).

### Whats missing in summary

Analysis of the potential consequences for Trump if the legal case proceeds to trial.

### Tags

#Trump #LegalEntanglement #Pardon #Indictment #Election


## Transcript
Well, howdy there, you don't know people, let's bow again.
So today, we are going to talk about Trump,
documents, and some new information.
We have not been updating a lot
on the documents entanglement,
because, well, let's just say it is slow moving.
It's about the nicest way I can put that.
Because of that, and because of all
the procedural delays, there hasn't been a lot of new information, there's not a
lot of updates going on. But as part of one of those wider arguments that's
occurring, some new information came out. Apparently, one of Trump's co-defendants
in that case, quote, was also told that even if he gets charged with lying to the FBI,
F. POTUS will pardon him in 2024.
Okay, so that is something that if this case ever was to actually make it to trial,
that's going to be incredibly damaging. That's going to be a huge deal because
it indicates a foreknowledge of the alleged lies. Another little tidbit that
came out was that Trump, according to this, was reportedly told, quote,
whatever you have, give everything back. Let them come here and get everything.
Don't give them a noble reason to indict you, because they will. And according to
this Trump responded with a quote weird you're the man type of response that's
what he reportedly said in response to being told to give everything back that
is also incredibly damaging. When you're talking about the willful aspect of
willful retention, if there are conversations about him still having it
and him being told to give it back, him retaining it after that definitely
further demonstrates the idea that it would be willful. One of the things about
this case that is different than a lot of other ones is that based on the
evidence that has been made public, this is kind of an open and shut case. You
know, Trump may put up some kind of defense, there may be evidence that we're
not aware of, there are allegations, but based on what is publicly available, this
is pretty open and shut, especially given the nature of the charges. The delays
are procedural in nature. It doesn't have anything to do with the strength of the
the case or the evidence in question so this is one that if it makes it's a
trial this is probably the biggest exposure for Trump it is still slowly
moving ahead. Again, this one will probably be after the election though. So
the election will determine whether or not this case is likely to even be heard.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}