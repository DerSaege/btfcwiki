---
title: Let's talk about Biden, privacy, and limitations....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=-kM5PVxHCEM) |
| Published | 2024/04/24 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Biden administration introduced a new federal regulation through HIPAA providing some shield for individuals seeking reproductive health care in another state.
- The rule aims to prevent medical records from being used against individuals seeking lawful reproductive health care.
- However, there are limitations to the rule, such as not covering individuals receiving mailed reproductive health care from another state.
- The rule may not provide a total shield as it could still be accessible to investigators with additional steps.
- About 20 attorneys general from Republican states opposed the rule when it was proposed and may challenge it in court.
- Beau advises not to rely solely on headlines about the rule and recommends consulting an attorney for accurate information.
- The rule's coverage may not be as all-encompassing as portrayed in the media.
- Beau suggests being cautious about assuming complete protection under the new rule, as its level of shielding could change over time, especially if challenged by the Republican Party.
- It is prudent to understand the limitations of the rule and be aware that its provisions could be subject to alteration through legal challenges.
- Beau ends by reminding viewers to stay informed and exercise caution regarding state laws and potential changes to the rule.

### Quotes

1. "No one should have their medical records used against them, their doctor or their loved one, just because they sought or received lawful reproductive health care."
2. "I wouldn't rely on this without talking to an attorney."
3. "Be cautious before you go and stick your tongue out at the local state laws."
4. "There are limitations to this and the level of shielding might change as time goes on."
5. "It's just a thought. Y'all have a good day."

### Oneliner

Beau warns about limitations of the Biden administration's new rule on reproductive health care, advising consultation with an attorney due to potential challenges and changes.

### Audience

Policy advocates

### On-the-ground actions from transcript

- Consult an attorney for accurate information on the Biden administration's new rule (suggested).
- Stay informed about the limitations and potential changes to the rule (suggested).

### Whats missing in summary

The full transcript provides detailed insights into the limitations and potential challenges surrounding the Biden administration's new rule on reproductive health care. Viewing the entire video can offer a comprehensive understanding of the nuances not captured in this summary.

### Tags

#BidenAdministration #ReproductiveHealthCare #HIPAA #LegalChallenges #ConsultAttorney


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about the Biden administration,
a new rule, privacy, and limitations.
And this is important because I feel like the limitations
of this may not be properly discussed.
So if you have missed the news, the Biden administration
has put out a new federal regulation, this new rule.
And it's through HIPAA.
And basically what it does is it provides some shield
if somebody was to leave one state
and go to another state to get reproductive health care.
So if you lived in a state where something was illegal
and you went to another state where it was legal,
it would provide some shield.
The some is important here.
The White House statement on it,
no one should have their medical records used against them,
their doctor or their loved one, just because they sought
or received lawful reproductive health care.
OK.
So as it's written, as it exists,
there are limitations.
Like it would, it appears that it
would cover somebody who left a state
and went to another state to get reproductive health care
performed.
It would not cover that person if, say,
they had something mailed to them from a different state. It wouldn't even apply
that. It also doesn't appear to be a total shield. It looks like it would still
be accessible to anybody who is investigating. They just have to go
through some more steps to get there. Now, one of the things that is worth
noting is that when this rule was proposed, I want to say it was almost 20
attorneys general from Republican states basically urged against adopting this
rule. My guess is that a large number of them are going to challenge this and try
to take it to court, try to get it thrown out, try to get it limited. The short
version here is that when you see the headlines on this, I would not rely on
this without talking to an attorney. This does not seem as all-encompassing as
some of the coverage may make it out to be. Talk to an attorney about this if
this is something that matters to you and understand that as it is written
that may change because the Republican Party for whatever reason they're still
chasing the car they caught, and they very well may challenge this. And if they
do, we don't know how it would play out in court. So I would just be very
cautious before you go and, you know, stick your tongue out at the local
state laws, I would I would be aware that there are limitations to this and the
level of shielding might change as time goes on. Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}