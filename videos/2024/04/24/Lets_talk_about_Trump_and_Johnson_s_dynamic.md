---
title: Let's talk about Trump and Johnson's dynamic....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=jv1UI1j9-nM) |
| Published | 2024/04/24 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Johnson, the Speaker of the House, pushed through an aid package, sidelining Trump's biggest supporters in the House.
- Trump wasn't super in favor of aid to Ukraine and dislikes anything that hurts Russia.
- Johnson refused to get rid of FISA as ordered by Trump over Twitter.
- Trump's normal move when someone disobeys him is to come out saying mean things, but in this case, he called Johnson a good person.
- Republicans in Congress don't actually like Trump; even those who support him now disliked him back in 2016.
- Republicans in Congress fed Trump's ego to tap into his voting base, despite not liking him.
- They played into Trump's ego and worst instincts to secure their positions.
- In 2020, Republicans supported Trump's claims of fake news and other behaviors to stay in his good graces for re-election.
- Trump's relationships were transactional; loyalty was not genuine.
- Johnson outplayed Trump in a public fashion, with limited outcry from the House.
- Trump seems powerless to act against Johnson, with one of his biggest supporters leading the charge to oust Johnson.

### Quotes

- "It seems like a strange dynamic but it's not."
- "They played into Trump's ego and worst instincts to secure their positions."
- "You don't have outcry, not widespread, about what Johnson did. He repeatedly defied or undermined Trump."

### Oneliner

Johnson strategically outplayed Trump, revealing the transactional nature of Republican support and the lack of genuine loyalty.

### Audience

Political strategists

### On-the-ground actions from transcript

- Analyze political dynamics (implied)
- Monitor political strategies (implied)

### Whats missing in summary

Full context and depth of insights from Beau's analysis.

### Tags

#Trump #Republicans #PoliticalStrategy #loyalty #transactionalRelationships


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about another way
to look at the Johnson-Trump dynamic because there's still
questions coming in about it.
And this isn't exactly the perfect metaphor,
but I think it might help.
First, let's run through what occurred, recent developments.
So Johnson, the Speaker of the House, pushed through that package, that aid package, and
in doing so, he sidelined the Twitter faction.
That's what we call them on the channel, the Twitter faction.
What's another way to describe them?
Trump's biggest supporters in the House of Representatives.
His most vocal supporters got sidelined.
And Johnson did this after going down there and talking to Trump.
And let's be clear, it's not like Trump was super in favor of aid to Ukraine to begin
with.
Trump's never in favor of anything that hurts Russia.
And Johnson did that after refusing to get rid of FISA, which Trump ordered over Twitter.
What did Trump say after all of this happened?
What is Trump's normal move when somebody disobeys him?
He comes out after him, right?
Saying very, very mean things about him.
Calling him a rhino.
Because they were failing to obey him.
Because he's the one that should be respected and saluted.
But what did he say?
Well, look, we have a majority of one, okay?
It's not like he can go and do whatever he wants to do.
I think he's a very good person, that's a departure, right?
That's a deviation from the norm.
It seems like Trump just can't really get a handle on him.
Somebody would be forgiven for thinking that Johnson outplayed Trump in a
pretty public fashion. But you don't have a lot of outcry from the House of
Representatives as a whole. Just certain elements. Mostly that faction that got
sidelined. It seems like a strange dynamic but it's not. You have to go back
to 2016. Remember most Republicans up in Congress they don't actually like
Trump. Think about what they say about him in private when it leaks out. Have you
ever heard something good leak out? They don't like him. They never liked him.
Even people who offer support for him today, back in 2016, they did not have
kind words for the man. They didn't see him coming back then. But as thick as
As they were, they paid attention.
They watched him.
And they realized that Trump was all about his ego.
If you fed his ego, even just words that fed his ego, the words were something that boosted
his pride.
And you ran.
watched how Trump took those people who had vacant expressions, maybe the lights
weren't on upstairs, and he had them eating out of his hand. And all they had
to do to tap into that was just feed his ego. They're politicians, that's a ready
voting base. So they did it. That doesn't mean they liked him, it just means they
went along with him. And in the process, they went along with his worst instincts.
This isn't a defense of them, to be clear. Then 2020 started to roll around. They
were not going to be caught unaware. It wasn't like 2016. They knew that for
whatever reason he had that captive audience. So they played into it. They
played into it. They put up with the claims of fake news. They put up with all
of his behavior and they just tried to stay on his good side because that
MAGA era was tiptoeing nearer. It was good for them. They didn't really have to
do much, all they had to do was stay in his good graces, and they would be able
to use that base. They just had to listen to teacher, obey, and they'd get
re-elected. They'd get his support. They'd be connected to the leader who was all
time adored. And just in true, true Trump fashion, everything was transactional. So
yeah, they had to take certain duties on board. But those people in Congress, they
looked at Trump's family and they saw them making out just tons of money. If you
were in Trump's circle, the future was littered with prizes. All he had to do
was stay in his good graces, and he never failed to emphasize that, well, you
wouldn't get anything without him, right? You had to stay in his good graces. Or he
would come out and trash you on Twitter. Something he's not doing to Johnson. So
So, they obeyed, they listened, they got prepared for the coup of the century.
The murkiest scam.
Not really the greatest planning, and it failed.
What happened to Scar at the end?
The hyenas turned on him.
I'm not saying that this is the beginning of the hyenas turning on Trump, but it definitely
appears that at least one of the hyenas is smarter than him, way smarter, and is positioning
himself to be the leader if Trump was to take a fall.
It's important to remember that most Republican Congresspeople do not actually like him.
They're not going to be upset, not most of them.
They supported him because it was good for them.
And those who recognized what he was and supported him anyway, I mean to me that's kind of even
worse.
Again, this isn't a defense, it's an explanation of what they did.
It was politics.
And it set the scene for a lot of bad things.
don't confuse that political expediency for actual loyalty. It's not there. They
don't actually support him, not most of them. You don't have outcry, not
widespread, about what Johnson did. Repeatedly. He repeatedly defied or
or undermine Trump.
And Trump realistically seems powerless to do anything about it.
In fact, when you think about it, one of his biggest supporters is leading the charge to
oust Johnson, meanwhile Trump's out there making excuses for him.
I think he's a very good man.
I think he's trying very hard.
You have two real choices.
One is to believe that Trump, at his age, has suddenly changed his behavior.
The behavior we've seen for a very, very long time.
And maybe Trump's trying to play the long game or something like that.
Or Trump realizes that maybe his star is fading.
You don't have to believe that Johnson is a genius.
You don't have to believe that he is McConnell in the making.
But if you are a democratic strategist, don't underestimate him.
Don't do what you did with Trump, where nobody would really follow him.
It didn't work out well last time.
Either Johnson got incredibly lucky repeatedly and Trump has just become incredibly understanding
or Johnson outplayed him.
And for whatever reason, Trump doesn't think he can do anything about it.
That to me, if I was a democratic strategist, might be the more concerning part.
You know, the big worry about Trump wasn't that he would actually be successful when
it comes to something like the sixth.
The worry is that his rhetoric would pave the way for somebody who was a bit more polished,
a bit more subtle, but had the exact same goals, don't underestimate this man.
Maybe things change.
Maybe the political winds shift again and Trump gets a chance to get even, or maybe
Trump doesn't even realize that he got bested right now.
may not even realize it because the Trump of today is not the Trump of 2016.
I think that's becoming more and more clear the more we hear about his behavior in court.
But if he does realize it, sure, maybe Johnson star starts to fade.
But either way, the Democratic Party cannot underestimate this man.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}