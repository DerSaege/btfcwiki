---
title: Let's talk about AZ, Pluto, and freed tamales....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Go5jiKOE9d0) |
| Published | 2024/04/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Governor Hobbs signed into law Pluto as Arizona's official planet, despite the International Astronomical Union's declaration that Pluto is a dwarf planet.
- The decision to make Pluto the state's official planet was to recognize a significant discovery made at Lowell Observatory in Flagstaff, Arizona.
- Arizona has a lot of official symbols, including a state dinosaur.
- Arizona has updated regulations to allow home cooks to offer tamales requiring refrigeration to the public.
- This change benefits those who provide food at job sites, a common practice in the state.
- The state agency in charge of enforcing regulations was not pleased with the change but won't likely intervene.
- The adjustment prevents grandmothers and aunts from facing hefty fines for sharing food, a cultural tradition at many job sites.

### Quotes

- "Governor Hobbs signed into law Pluto as Arizona's official planet."
- "Arizona has updated regulations to allow home cooks to offer tamales requiring refrigeration to the public."
- "This change prevents grandmothers and aunts from facing fines for sharing food."

### Oneliner

Governor Hobbs makes Pluto Arizona's official planet while Arizona updates regulations to allow home cooks to share tamales, preventing fines for a cherished cultural practice.

### Audience

Arizona residents

### On-the-ground actions from transcript

- Support local home cooks by purchasing their tamales and other food items (implied).
- Advocate for cultural practices like sharing food at job sites by engaging with local officials (implied).

### Whats missing in summary

The full transcript provides more context on the unique official symbols of Arizona and the regulatory changes affecting home cooks and their traditions.

### Tags

#Arizona #Pluto #StateSymbols #Regulations #HomeCooks


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about Arizona
and Pluto and Tamales,
and just kind of run through some developments
that occurred in Arizona.
First, Governor Hobbs signed into law the idea
the idea that Pluto was the state's
official planet. This isn't an April Fool's thing.
For those who don't know, the International Astronomical Union
said that Pluto wasn't a planet
not too long ago, that instead it was a dwarf planet.
Arizona doesn't care. It is the state's
official planet. Now, obviously
The question is, why would Arizona do this, right?
And there's a legitimate reason behind it.
There really is.
The real discovery behind it was made at Lowell Observatory
in Flagstaff, Arizona.
So it's a recognition of that event
and adding another item to the state's many official symbols.
because they have a lot, most states do,
but they seem to have a lot,
and even things that may seem a little weird,
like they have an official state dinosaur, I think.
Okay, in other news from Arizona,
they have decided to free the tamales,
free the tamales.
That actually isn't something that is as weird as it sounds.
This applies to home cooks and their ability to offer those products to the
public. This is a big deal. If you like work at a job site, you know the person
that comes around and offers you food. In a lot of states they can't do that if it
requires refrigeration, which was the case in Arizona. They have changed it to
allow that to happen. That's what occurred. So that's another one that when
you first hear it, it sounds just incredibly bizarre, but I mean there's
a legit reason behind it. It is worth noting that the state agency there
that is in charge of enforcing that kind of stuff, they were not happy
about this change. But it's a common practice, particularly out there, and I
mean it's not like they're gonna stop them anyway. So this is just making
sure that, you know, grandmothers and aunts that are out doing this aren't
getting hit with these massive fines for just doing something that is that is
incredibly common and in many ways almost part of the culture of a lot of
these job sites. I mean it's it's something that you know when they show
everybody's happy. So anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}