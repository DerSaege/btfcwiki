---
title: Let's talk about the Speaker, votes, and aid for Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=yMCZAo95ur0) |
| Published | 2024/04/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Overview of the US House of Representatives and the aid package for Ukraine.
- Representative Bacon announced a commitment from Speaker Johnson and the Foreign Affairs Committee chair to bring Ukraine's aid up for a vote post-recess.
- The potential for a floor vote on Ukraine's aid package and the dynamics of left-leaning Democrats and far-right Republicans voting against it.
- Despite potential opposition, there are enough votes to pass the aid package.
- Concerns about Speaker Johnson potentially losing his speakership due to internal Republican party dynamics.
- Democrats indicating they will follow Jeffries' lead on the vote, creating a situation where Johnson needs to collaborate across the aisle.
- Importance of trust-building between Johnson and Jeffries for the future dynamics in the House of Representatives.

### Quotes

- "There is going to have to be a lot of trust that develops very quickly between Johnson and Jeffries."
- "If that commitment exists, there's going to be a floor vote."
- "Democrats, all of the ones that I've seen, have kind of indicated that they're just going to vote however Jeffries tells them to."
- "The odds are that there's going to be a floor vote."
- "Johnson will probably need Democratic assistance to do that."

### Oneliner

Beau from the internet talks about the US House, Ukraine aid package vote dynamics, and the precarious position of Speaker Johnson needing Democratic support.

### Audience

Political observers

### On-the-ground actions from transcript

- Reach across the aisle and collaborate with those from different political views to work towards common goals (implied).

### Whats missing in summary

Insights on the potential consequences of internal party dynamics on future legislative decisions.

### Tags

#USHouse #UkraineAidPackage #SpeakerJohnson #Bipartisanship #Trustbuilding


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about
the US House of Representatives,
how things are shaping up there.
We're going to talk about that big aid package
that includes the money for Ukraine
and how that is maybe moving ahead
and how we are likely to see
yet another big fight in the House.
Okay, so what's going on?
A representative, Bacon is his name, he came out and basically said that there was a commitment
from the speaker, from Speaker Johnson, and from the chair of the Foreign Affairs Committee
that they'd bring Ukraine's aid up for a vote after the recess.
If that's true, the odds are that there's going to be a floor vote.
If that commitment exists, there's going to be a floor vote.
Now, by my math, the votes are there for that to pass, and it's going to pass in a weird
way.
You would see people who are more left-ish in the Democratic Party vote against it because
it's not just aid for Ukraine, there's also aid for Taiwan and Israel and other
things. So you'd see some Democrats vote against it. You would also see the far
right in the Republican Party vote against it. But by my math, they have the
votes to get it through. It's there. There won't be enough defections to
actually stop it from passing. But that's only part of the story because, quote,
it's possible that Johnson lose his speakership, meaning that one of the
Twitter faction of the Republican Party decides to do a motion to vacate, and
then they're back in that massive amount of dysfunction. The interesting part
that has come up is that the Democrats who have spoken publicly about this
possibility, all of the ones that I've seen, have kind of indicated that they're
just going to vote however Jeffries tells them to. If Jeffries says hey,
protect Johnson, well they will. If he says no, well then they don't. So this
This creates a situation where it is in Johnson's best interest to reach across the aisle, talk
to Jeffries, and work out something that makes everybody happy.
Because when it comes to protecting his speakership, Johnson will probably need Democratic assistance
to do that.
So that's how that is shaping up.
We'll see how it plays out once, you know, Congress comes back from recess.
The end result is that there is going to have to be a lot of trust that develops very quickly
between Johnson and Jeffries.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}