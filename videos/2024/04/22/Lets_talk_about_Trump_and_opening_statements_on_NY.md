---
title: Let's talk about Trump and opening statements on NY....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=0OxEEeQTScU) |
| Published | 2024/04/22 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the focus on Trump's New York entanglements and the expected proceedings.
- Mentions the importance of opening statements over opening arguments in the legal doubleheader.
- Details the role of a person named Pecker who is familiar with the alleged process used to protect Trump from unflattering information.
- Describes the prosecution's strategy of structuring their presentation through Pecker to provide context and storytelling.
- Notes the ongoing movements regarding the bond in the civil case, with conflicting stances from the Attorney General's office and Trump's legal team.
- Reports Trump's discontent with the coverage from the previous week, citing bias concerns and dissatisfaction with courtroom sketches.
- Anticipates Trump's grievances impacting his public statements and potential responses to the coverage.
- Foresees potential actions related to a judge examining alleged violations of the gag order in the upcoming days.

### Quotes

- "Almost every attorney in the world was in my inbox this weekend."
- "Tell them what you're gonna tell them, tell them, tell them what you told them."
- "He feels that those people are out to get him."
- "When Trump gets something like this in his head, generally speaking he doesn't let it go."
- "That's how things are shaping up."

### Oneliner

Beau gives insights on Trump's New York entanglements, the legal proceedings, Pecker's role, conflicting stances on the bond, and Trump's discontent with coverage, shaping up the week's developments.

### Audience

Legal analysts, political commentators

### On-the-ground actions from transcript

- Contact legal experts to understand the implications (suggested)
- Stay informed about the developments in Trump's legal cases (exemplified)

### Whats missing in summary

Insights on the potential impacts of Trump's grievances on his public statements and legal strategy.

### Tags

#Trump #LegalProceedings #NewYork #Pecker #GagOrder


## Transcript
Well, howdy there Internet people, it's Beau again.
So today we are going to talk about Trump's New York entanglements, plural, today, and
how things are expected to proceed and move along.
We will talk about what it appears the strategy is, and then we will talk about how Trump
Trump is reportedly viewing some of the coverage from last week and just kind of run through
everything.
So the expectation is that today opening statements, not arguments, will start in the main event
of his legal doubleheader.
Yeah, apparently that is a very important distinction.
is opening statements, not opening arguments. Almost every attorney in the
world was in my inbox this weekend. I felt like I had said gas instead of fuel
to somebody who drives a diesel. So opening statements are to proceed. Then
after that, it's being suggested that the prosecution is going to bring up a
person named Pecker. Pecker, according to reporting, is very familiar with the
alleged process, the methodology used to lessen the exposure of potentially
unflattering information about the former president. So what's the
prosecution doing? They're gonna tell them what they're gonna tell them. If you
around for the January 6th hearings. We talked about how they structured things
very well. They used a good presentation format. Tell them what you're gonna tell
them, tell them, tell them what you told them. Pecker seems to be somebody who
is going to lay context and kind of put things into a story and then the
prosecution will fill in the details and then bring somebody on to remind them
of the story. That appears to be the strategy and that's kind of what the expectation is at this
point. There is also, in addition to the stuff dealing with the quote hush money case, there is
there's movement when it comes to the bond in the civil case and there will be
talks about that today. The Attorney General's office is basically saying
don't accept that bond and Trump's legal team is saying to accept it. We'll see
how that plays out. As far as Trump goes, reporting says that he is incredibly
unhappy about the coverage from last week. Basically, it seems like he thinks
it smells of bias. He's unhappy about the coverage of him allegedly falling
asleep, and he is reportedly very unhappy with the sketches that were made in the
courtroom. According to the information that has come out, he feels that those
people are out to get him. This will add to Trump's list of grievances. This is
something that might influence what he has to say coming in or going out of the
courtroom if he chooses to make any statements. When Trump gets something
like this in his head, generally speaking he doesn't let it go and it influences a
lot of what he says in public. That leads us to the other thing that is likely to
occur in the coming days, which is movement when it comes to the judge
looking at any potential or alleged violation of the gag order. So that's
what's going on. That's how things are shaping up. That is your opening
statement for the week when it comes to Trump's entanglements. I'm sure there
will be more developments as the week goes on. Anyway, it's just a thought. Y'all
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}