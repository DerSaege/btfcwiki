---
title: Let's talk about the US turning up the pressure....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Cd4Ed3RFaU4) |
| Published | 2024/04/22 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talking about the US considering or readying sanctions against a specific unit in the Israeli military.
- Sanctions are based on rules regarding units committing crimes and facing consequences.
- If a unit has credible accusations but no actions are taken, it becomes ineligible for U.S. aid.
- Netanyahu has vowed to fight against these potential sanctions.
- Remediation or disbanding of the unit are possible outcomes, with disbanding possibly leading to wider sanctions.
- The mere public discourse about potential sanctions carries significant pressure.
- Uncertainty surrounds whether the sanctions will be implemented.
- This action is rare for the United States, especially against an ally.
- The situation may escalate to a suspension of offensive military aid.
- Beau concludes with a call to keep an eye on the developments.

### Quotes

1. "This is not something the United States does often, period."
2. "It is a surprising turn of events because it is a very big deal."
3. "Netanyahu has vowed to fight it."
4. "The problem under the rules is that if that isn't happening, if nobody ends up in cuffs."
5. "Anyway, it's just a thought, y'all have a good day."

### Oneliner

Beau talks about the US potentially sanctioning an Israeli military unit, facing rare consequences for alleged crimes, and the uncertainty surrounding the situation's outcome.

### Audience

Global citizens

### On-the-ground actions from transcript

- Watch the situation closely and stay informed about the developments (implied)

### Whats missing in summary

Insights on the potential implications and repercussions of the US considering sanctions against an Israeli military unit.

### Tags

#US #Sanctions #IsraeliMilitary #Netanyahu #GlobalPolitics


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about something
we discussed briefly this weekend
because there were little rumors about it.
Now there is a widespread reporting
and some of it is from BBC.
So we're gonna go ahead and talk about
what is apparently being either considered or readied
we'll talk about what the difference means. Okay, so the reporting suggests
that the United States is either considering, meaning they're looking at
it, or readying, which means they're putting the paperwork together. They're
either considering or readying sanctions against a specific unit within the
Israeli military. Now, the US and the EU both recently sanctioned people or
entities. This is a whole different thing and I cannot stress what a big deal
even talking about it really is. Okay, so as I understand it, the the portion of
the rules that this would fall under has to do with specific units that commit
crimes. Now I want you to picture a situation in which a unit is known or
credibly accused of committing a bunch of crimes to the point where the
battalion commander ends up in cuffs and you have like just all kinds of people
going to prison, all of this stuff.
That's actually OK.
I know that doesn't make any sense.
But under the rules, that's not actually a big deal.
The problem under the rules is that if that isn't happening,
if nobody ends up in cuffs.
There's a specific term for it.
I think it's called un-remediated,
or maybe unremitting, something like that, where you have a unit that has credible accusations
against it, but nothing's being done.
Under these rules, that unit is no longer eligible for U.S. aid.
End of sentence.
Not offensive aid, just aid, period.
Supplies.
Anything.
Because of how intertwined Israeli and US defense production is, it causes a major issue
for that unit being able to get anything because a lot of things are jointly produced.
Now according to the reporting Netanyahu has vowed to fight it.
I don't even know what that means.
If this is prepared and issued, realistically the only thing I can think of is either them
engaging in a bunch of remediation or disbanding the unit.
And given the fact that Netanyahu is vowing to fight it, it seems unlikely that they're
going to remedy the situation.
It doesn't seem like something they're considering.
Now, disbanding the unit, if they were to disband the unit
and take the people and put them into other units,
that wouldn't necessarily solve the problem.
It might expand the sanction to other units.
It is a big deal.
The fact that this is being now discussed, I mean, kind of openly, that is realistically
more pressure than the sanctions that were actually issued, just talking about it.
Again, at this point, we don't know if it's going to happen.
We don't know if it's being considered or readied.
The situation at time of filming is that it is being discussed publicly, it is being widely
reported and there is reporting about Israeli officials commenting on it.
It is a surprising turn of events because it is a very big deal.
This is not something the United States does often, period.
As far as doing it to an ally, I mean I don't even know.
This is definitely something to watch because this may be the warning that occurs before
a suspension of offensive military aid.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}