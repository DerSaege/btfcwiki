---
title: Let's talk about the 2024 list from the Library Association....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=bqPiCRegqzw) |
| Published | 2024/04/11 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Every year, the American Library Association releases a list of the 10 most challenged or banned books.
- In the previous year, there were 1,247 attempts to ban or challenge books, targeting 4,240 titles.
- The fact that there were more titles than attempts to ban them suggests organized efforts rather than random occurrences.
- Beau questions what books should be banned if we were a functioning society, suggesting they should focus on serious topics like war and famine.
- However, the actual most challenged books mainly revolve around LGBTQ+ themes.
- Despite the framing, the issue isn't about shielding children but rather about suppressing LGBTQ+ representation.
- Beau criticizes the consistent theme of targeting LGBTQ+ representation in these lists.
- He points out that books depicting "man's inhumanity to man" never make the most challenged list.
- Beau sees this targeting as a means to marginalize a group and prevent their representation.
- The motivation behind this censorship seems to be about maintaining power dynamics and having someone to look down upon.

### Quotes

- "It's always about going after people who are just finally starting to get some real representation and trying to stop that."
- "If your entire basis for saying that you're better is based on the idea that you are intentionally keeping somebody else down, maybe you're not."

### Oneliner

Every year, organized efforts target LGBTQ+ representation in the most challenged book list, revealing deeper motives than shielding children.

### Audience

Readers, activists, educators

### On-the-ground actions from transcript

- Support LGBTQ+ representation in literature by recommending and sharing books that provide authentic and diverse perspectives (implied).

### Whats missing in summary

The full transcript provides a deeper understanding of the systemic challenges faced by LGBTQ+ representation in literature.

### Tags

#Censorship #LGBTQ+ #BookBanning #Representation #Activism


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about what
could be a reading list.
I've used it that way in the past.
And we're going to talk about 10 books that made a list.
And we're just going to kind of run through some of the things
that we can pull out from this information.
If you don't know, every year, the American Library
Association, they put out a list.
and that list includes the 10 most challenged or banned books.
Normally, you can find some pretty good stuff
on these lists, stuff to read.
But we'll go through some of the information about it first.
So for the previous year, there were 1,247 attempts
to ban or challenge books, going after 4,240 titles.
Do the math real quick, more titles than attempts.
What does that tell you?
That this is not a case of a kid walking into a library,
picking up a book that mom didn't know about mom finding it
and then going up to the library and raising a stink.
because there's more titles than attempts. It's organized.
It's organized.
Now, as far as the
the ten most challenged books,
what do you think they are?
What would you imagine they would be?
What should they be if we were a functioning society?
If you're trying to shield children, which is the general tone of these, I mean, I would
suggest that they should be books that are about war, famine, man's inhumanity to man,
all of that stuff.
That's probably what we should try to shield our children from until they're a little bit
older.
What are they actually about?
Love, love.
about love. Okay, without further ado, here's the list. Number 10. Sold. Number 9. Let's
talk about it. Number 7 and 8 are a tie with Me and Earl and the Dying Girl and a book
called Tricks. Number 6. The bluest eye. Number 5. Flamer. Number 4. The perks of being a
wallflower yet again. That makes it a lot. Number three, this book is gay. Number two,
all boys aren't blue. And number one, genderqueer. It's almost like there's a theme there. You
can walk into most libraries and you can find books about, let's just say, growing up activities,
A whole bunch, a whole bunch of them.
What this list shows is that despite the framing
that they try to use, it's not actually
about a depiction of a grown-up activity.
It's about a depiction of a grown-up activity
where the author or the characters are LGBTQ+.
That's what it's about.
This has been a relatively consistent theme
for a bit now.
There was a time when I did, I used these as reading lists because you got all kinds
of cool, cool new subjects introduced.
That's rain, by the way, if you can hear it.
But now it's getting kind of repetitive because it's organized.
Because it is specifically targeting a group of people that they want marginalized, that
They don't want to have representation.
I don't believe it's about shielding children.
Go walk into a library and find books about, again, term it loosely, man's inhumanity
to man.
Those books, they never make this list.
It's always about kicking down.
It's always about going after people who are just finally starting to get some real representation
and trying to stop that.
Because let's be real.
It's about keeping other people down so those people who aren't doing so well, they have
somebody to look down at.
You're better than them.
If your entire basis for saying that you're better is based on the idea that you are intentionally
keeping somebody else down, maybe you're not.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}