---
title: Let's talk about Trump trying to let go and not sign....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=pZQuV28g2Uw) |
| Published | 2024/04/11 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump was asked if he'd sign a national abortion ban on the tarmac in Atlanta and replied with a simple "no."
- Despite claiming to be proudly responsible for ending Roe, Trump now claims he won't sign a federal ban, citing a newfound belief in bodily autonomy.
- Trump's attempt to distance himself from a national abortion ban may stem from political strategizing rather than personal beliefs.
- He may try to justify his stance by invoking states' rights, but his base, eager for a national ban, may not let him off the hook.
- Trump's inconsistency on the abortion issue suggests that his recent statement is more about polling and retaining support than genuine conviction.
- Polling indicates that pushing for a national abortion ban is a losing issue for Trump.
- The vocal minority who staunchly support Trump may demand a national ban, potentially causing rifts if he doesn't deliver.
- Trump's statement on abortion may cause waves, but it's unlikely to result in significant change.
- Anticipating pushback from his base, Trump might reverse his position on abortion yet again.

### Quotes

1. "He saw the light. He believes in bodily autonomy now."
2. "He knows the Republican party caught the car, he's trying to let go."
3. "His activated base, that incredibly vocal minority that supports Trump just no matter."
4. "So Trump said something, it's going to make waves, but at the end of the day, it's probably not going to change much."
5. "He might even change his opinion yet again."

### Oneliner

Trump's back-and-forth on a national abortion ban reveals strategic moves rather than genuine beliefs, facing potential backlash from his base.

### Audience

Political observers

### On-the-ground actions from transcript

- Mobilize for reproductive rights advocacy (implied)
- Stay informed on political stances and actions (suggested)

### Whats missing in summary

Insights into the potential impact of Trump's wavering stance on abortion rights.

### Tags

#Trump #AbortionRights #PoliticalStrategy #RepublicanParty #Polling


## Transcript
Well, howdy there, internet people, let's bow again.
So today, we are going to talk about Trump.
We're gonna talk about Trump and something he said
and how he is trying so hard to let go of that car
because he said he wouldn't sign something.
And it's prompted a lot of questions,
so we'll go through it.
We'll talk about what was said,
we'll talk about the political implications of what was said and just
kind of run through everything. If you don't know what happened, he was in
Atlanta, I think, and he's on the tarmac and a reporter asks a question and he
had the one-word answer. He was asked, would you sign a national ban? Would you
sign a national abortion ban and he said no. The man who referred to himself as
proudly responsible for ending Roe said no. Okay, I believe that people can change
their position so let's run through it. He saw the light. He believes in bodily
autonomy now. So he won't sign a federal ban. Easy enough. And I'll believe that as
soon as he states his opposition to the state bans. Until then, it's all talk. It's
Trump being Trump, right? Now more than likely he'll try to frame this as some
philosophical nonsense. He'll say, you know, I have always supported people's bodily
autonomy and their right to plan and them having all the choices that
they want, but I believe in states' rights. Yeah, we've heard that nonsense before.
right to do what? Exactly. He's trying so hard. He realizes that the Republican
Party is the dog who caught the car. He is trying to let go, but his base isn't
going to let him. That activated base, they are not going to back down on this.
This is entrenched 50 years in the Republican Party.
They want that national ban and they're going to do anything they need to to get it.
Those people who are in favor of restrictions, they're going to be mad or they're going
to think it's 4-D chess, one of the two.
people who are opposed to restrictions, they're not going to believe this. They
are not going to believe this. Now when Trump comes out inevitably at some point
I am almost sure that he will come out and say, you know I've long
supported bodily autonomy and planning and choice and so on and so forth. He'll
probably say something like that at some point in time and that's true. It is.
because before he was against it, he was for it, and actually before that he was
against it, and before that he was for it. He's not real consistent on this
issue, but I don't think this has anything to do with his personal
beliefs or philosophy. This has to do with polling. He knows the Republican
party caught the car, he's trying to let go.
Polling, any polling, any half-decent polling, old polling, new polling, it doesn't matter,
all of it, it shows that this is a losing issue.
His activated base, that incredibly vocal minority that supports Trump just no matter
But they want that national ban.
If he pushes this too hard, he might actually lose some of them.
And he's not going to gain any moderates or independents on this because nobody's going
to buy it.
He's proudly responsible for ending Roe.
So Trump said something, it's going to make waves, but at the end of the day, it's probably
not going to change much.
And my guess is that he is going to get a pretty big pushback from his base on this,
and he might even change his opinion yet again.
So as far as all of this is concerned, and what he said, whatever.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}