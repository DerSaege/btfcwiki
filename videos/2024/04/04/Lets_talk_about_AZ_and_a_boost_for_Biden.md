---
title: Let's talk about AZ and a boost for Biden....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=rSeWCwTPiFw) |
| Published | 2024/04/04 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A petition in Arizona with over 100,000 extra signatures aims to put reproductive rights on the ballot in November.
- Collecting more signatures than required acts as a buffer against challenges to ensure the issue makes it to the ballot.
- Increased voter turnout for reproductive rights can indirectly benefit Biden in the 2024 elections.
- Voter enthusiasm for protecting reproductive rights might draw in those who were previously unlikely to vote.
- States like Florida, Montana, and others are also pushing for ballot access on reproductive rights, potentially affecting election dynamics.
- Organizers of these movements aim to appeal to moderate Republicans and independents, steering away from partisan politics.
- Concerns exist that associating the issue with benefiting Biden could hinder bipartisan support.
- Beau doubts that voters will change their stance on reproductive rights just because it could help Biden secure a boost in the polls.

### Quotes

1. "People who may not show up for Biden because they're not enthusiastic about him, well, they very well may show up for this."
2. "It's likely to help Biden a little bit."
3. "Organizers of the ballot access movements, most of them are like, please don't politicize this."
4. "I'm not sure that's how it will play out."
5. "If you have somebody that is already willing to vote against what is viewed as the Republican way, I don't believe that their opinion on that is going to change simply because the extra turn out would help."

### Oneliner

A movement in Arizona for reproductive rights through a petition with surplus signatures may indirectly boost voter turnout for Biden in 2024.

### Audience

Voters, Activists, Community Members

### On-the-ground actions from transcript

- Support the petition for reproductive rights in Arizona by signing and encouraging others to sign (suggested).
- Educate moderate Republicans and independents on the importance of reproductive rights regardless of political affiliation (suggested).

### Whats missing in summary

The full transcript provides a nuanced perspective on how grassroots movements can impact voter turnout and election dynamics.

### Tags

#Arizona #ReproductiveRights #VoterTurnout #Biden #Petition #Election2024


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Arizona
and signatures, petitions.
We're gonna talk about Biden
and how events in Arizona and elsewhere within the country
are likely to give Biden a boost come 2024.
Okay, so the first question is obviously what's going on.
a petition being circulated in Arizona and by the statements made by the
organizers it appears that they have more than 100,000 extra signatures
already. They have that many more than is needed to gain ballot access and the
goal is to put reproductive rights on the ballot in November. They have so
many more that has prompted the question of why are they continuing to collect
signatures through early July? The answer to that is once the petition gets
submitted other groups come in and challenge signatures. So this way by
gathering you know tens of thousands or maybe in this case hundreds of thousands
of extra signatures even if they are successful in challenging a bunch of the
signatures, they still have the number required to put it on
the ballot. That's what's going on there. So, the likelihood
here is that in Arizona come November, the people there will
be voting on whether or not they want reproductive rights enshrined
in their constitution. How does this help Biden? Because stuff
Just like this increases voter turnout.
People who may not show up for Biden because they're not enthusiastic about him, well,
they very well may show up for this.
And when you are looking at the results, particularly when it comes to this issue, you have a lot
of people show up to protect this right that they're unlikely voters.
So it would give Biden a boost.
Now in Arizona, I want to say Biden only won the state by like a third of a percent.
So a boost there would be pretty helpful.
It's worth noting that there are, I want to say eight, I could be off one or two there.
But there are a number of states that are doing something similar and they're moving
to get ballot access when it comes to protecting reproductive rights.
Any state where that happens, it's likely to help Biden a little bit.
We talked about this going forward in Florida recently, and there were a lot of questions.
Basically, you know, Florida's red.
There's no way that Biden's going to win.
In 2020, Trump only won Florida by less than three and a half points.
than three and a half points. A big boost from a vote on reproductive rights it
might tip the scales and there are again I think Montana's won that there's a
bunch of states where this is going to be on the ballot and it's probably going
to alter dynamics or they're trying to get it on the ballot and it's it's going
to alter dynamics. Now one of the interesting things is that the organizers
of the ballot access movements, most of them are like, please don't politicize
this. They're trying to make sure that it's not viewed as Republican or
Democrat. They want moderate Republicans and independents to vote in favor of
their proposal, obviously, and there are a few that have expressed concern that people
talking about how it will give Biden a boost will undercut that.
I understand the concern, I'm not sure that that's how it would play out.
I get it, I understand what they're saying because of the very partisan environment and
how politicized everything is. It makes sense. At the same time, I don't think people are
going to alter their vote on this topic because, well, it might help Biden. I don't see that
happening. If you have somebody that is already willing to vote against what is viewed as
the Republican way, which is taking everybody's rights away, I don't believe that their opinion
on that is going to change simply because the extra turn out would help.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}