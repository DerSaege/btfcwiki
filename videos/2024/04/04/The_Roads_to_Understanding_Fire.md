---
title: The Roads to Understanding Fire
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=CW9bOlaRDWI) |
| Published | 2024/04/04 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the importance of fire and its historical significance in aiding civilization, keeping predators away, and building community.
- Details the three things needed to create fire: heat, fuel, and oxygen.
- Describes different methods to create fire, such as using a lens to focus the sun's rays, striking a match head, or utilizing an exothermic chemical reaction.
- Talks about materials like quadruple zero steel wool and hand warmers for creating fire.
- Mentions the importance of tender materials like dryer lint, wood shavings, and dry pine straw for starting a fire.
- Gives examples of common campfire lay setups like the Dakota Firehole and the log cabin.
- Emphasizes the importance of campfire safety rules and preparedness.
- Advises on processing firewood into different sizes for efficient burning.
- Demonstrates setting up kindling in a V shape to shield from the wind and ensure a sustainable fire.
- Shows how to use a ferro rod and striker to ignite tinder bundles effectively.

### Quotes

1. "Fire is one of those things historically, it aided in civilization, it kept predators away, it built community."
2. "Fuel, oxygen, and heat—these are the keys to a fire."
3. "Campfire safety rules are paramount."
4. "You need to add wood consistently for a sustainable fire."
5. "Fire loves chaos."

### Oneliner

Beau explains the historical significance of fire, the essentials for creating it, common campfire lay setups, and the importance of campfire safety and preparedness.

### Audience

Survival enthusiasts

### On-the-ground actions from transcript

- Collect and process firewood into different sizes for efficient burning (exemplified)
- Set up kindling in a V shape to shield from the wind and ensure a sustainable fire (exemplified)
- Follow campfire safety rules and ensure preparedness (exemplified)

### Whats missing in summary

The full transcript provides detailed instructions and demonstrations on creating fire, common campfire lay setups, and the importance of safety and preparedness. Viewing the full transcript offers a comprehensive guide to mastering fire-making skills.

### Tags

#FireMaking #CampfireSafety #Preparedness #SurvivalSkills #HistoricalSignificance


## Transcript
Well, howdy there, internet people, it's Beau again,
and welcome to The Roads with Beau.
Today, we will be continuing on that path
and learning about adapting our environment.
One of the things we talk about often
are the things that you have to have in order to make it.
Food, water, fire, shelter, first aid,
to include your meds, and a knife.
Those are things you have to have.
So today, we will focus on fire, we'll focus on fire.
Fire is one of those things historically, it aided in civilization, it kept predators
away, it built community, a place to hang around and talk.
So we will talk about how to make fire and as promised, you'll learn about that one.
At its base level, you need three things to create fire, heat, fuel, and oxygen.
The heat used is always from a transfer of energy.
It could be the focusing of the sun's rays with a lens or even a magnifying glass, or
it could be the friction that is required to strike a match head, or it could be an
exothermic chemical reaction, such as when you use a pair of hand warmers when it's
cold out.
These hand warmers contain lead, active carbon, water, salt, and vermiculite.
Exposing the warmers to air and then shaking them to mix the contents causes an exothermic
reaction.
Then, in about 15 minutes, they're nice and toasty.
The oxygen is in the air all around us.
Fuel for starting a fire can be nearly anything, but should generally be fine material like
dryer lint, wood shavings, dry pine straw, or even quadruple zero steel wool.
This material is known as tender.
If you continue to build the fire, you would next add material that is twig to pencil
size, then thumb to finger size, and finally wrist size pieces or larger.
Focusing the sun's rays on this mixture of fine wood chips and charcoal dust starts to
heat up, and in no time at all, it'll give you an ember that you can use to start a fire.
quadruple zero steel wool is so fine that when it heats up from the electrical resistance,
the metal actually burns.
This is why you can do the same thing with almost any fresh battery in aluminum-backed
paper.
Okay, we also have the famous battery in foil.
You can use this with foil that has a paper backing in any battery.
You're just connecting the positive and the negative, and poof, you get that.
You can use that to light up just about any normal source of tinder, and a battery is
normally good for a couple of drives.
A ferro rod, also known as a metal match, are generally seen here on YouTube on bushcraft
channels.
They're extremely handy even if you're not trying to rough it out in the woods.
Remember, you may not always have your trusty BIC lighter available, or even if it is, you
is available, remember that the moment you need it, that's when it's going to fail.
Fuel is in the bar's mixture, known as ferrocerium.
When you put friction against it with a hard piece of steel, the rod releases a shower
of sparks that reach temperatures as high as 6,000 degrees Fahrenheit.
Here are a few examples of the most common campfire lays.
The Dakota Firehole is a setup that's designed to help remain a little bit more concealed.
Start by digging a hole about 6 to 8 inches in diameter and at most 10 inches deep.
Then 12 inches away, start digging a new hole at an angle that meets at the bottom of your
first hole.
This will allow for air and fuel to feed the fire.
Start with your tender and then carefully feed in some small kindling until you have
a sustained fire.
After this, you can gradually feed in larger pieces through the angled hole you dug.
up is just filling in the hole. Adding some debris after filling it could help
conceal the fact that you are even there. The log cabin, much like the classic
Lincoln log toys, is built from the bottom up. However, in this case you'll
start with larger pieces on the bottom and smaller kindling as you go up. The
tender bundle is placed in the center and ignited. Add small kindling as you
wait for the fire to grow, occasionally blowing air into the lay to help sustain
the fire. Make sure you follow all campfire safety rules. Keep anything that
could catch fire 10 feet away with extra firewood upwind. Keep a shovel nearby to
use dirt to put out the fire. Never leave it unattended and so on. We'll put a list
down below from smoke.
One important thing to note about all the videos in this series is that just off-camera,
there will be fire extinguishers, there's first aid kits, everything is done in a very
controlled environment, even if it doesn't look like it.
Preparation is key to any task, so have your firewood collected and processed.
Fires require fuel, so until you have reached a big enough size and temperature, you won't
be able to burn large logs.
Just remember, you will need to add wood pretty consistently, and dry split wood burns better
than large unprocessed limbs or branches.
Remember oxygen and fuel are key to any fire, so you may have to blow slowly into the base
of the fire to help it flare up.
extra kindling ready so you can build a sustainable fire with enough heat to burn the larger pieces
that make up the fire away. This is especially true if you decided to build one that's kind of
like normal size instead of one that's the size of an example for a YouTube video. This one is built
similar to the classic TP. Larger pieces are stood up on end and lean into one another up at the top,
then thinner pieces are leaned against them. Leave an opening opposite from the direction
the wind is blowing and place your tinder bundle inside and ignite when ready.
There we go, a little more information, a little more context and having
the right information will make all the difference. This series is going to continue and eventually
you'll learn how to make and use one of these. We talked about how this is fire today. Food,
water, fire, shelter, knife, first aid kit. This is for one of those.
Keith has been gathering firewood this whole time, so we're gonna see what he's got and
how we're doing.
Alright guys, when you're processing your firewood, you want to start by breaking it
down into small pieces and separating it into piles.
Your first pile you want to be pencil lead and pencil size pieces.
Your next pile you'd want to be finger and thumb size pieces, and then after that you'd
want like wrist size or larger pieces in a pile, and if they're any bigger than that,
really you want to split them so that they'll burn well.
So this is kind of the configuration
that you want to set your kindling up into.
Again, you're very fine kindling to start
before you add your bird's nest,
before you start trying to set fire to your tinder.
You want to set it up this way,
and I'll show you how to do it in a minute,
but the reason is is you want to point this like a V
into the wind so that this acts as a shield.
If the wind's blowing towards you,
then you would have a bundle on top to roll over
once you get your tinder started
so that it will have fuel to start.
Keys to a fire are three things.
You need fuel, oxygen, and heat.
So to build it, you're going to take your extra fine kindling
here, and you're going to get it into three bundles, kind of
Formula V here, so you have a place
to get your tinder started out of the elements.
And your third, again, will go up here
the front so that whenever it starts you were able again you would be over on
this side and you would pull this over on top once it got going to keep the
fire going then you would add more fine tender then you would add your finger
and thumb size and keep adding to between those two until it gets hot
enough that you could add the thicker wood to keep your fire going.
Here in Northwest Florida we're kind of lucky about year-round there's this stuff
called deer moss that just grows everywhere on the ground and it's pretty much free tender.
So I'll show you how easy it is to get you a fire going. I say that and my match blows up.
And when you burn this stuff, I mean, I would be careful.
People have been known to accidentally set areas of woods on fire because this stuff
goes up pretty fast, as you can see, and again, I just picked that up off the ground a moment
I just picked that up off the ground a moment ago.
Alright guys, so we're going to show you how to use a ferrocium rod and striker.
You get these things, they have this black coating on it.
You gotta use the striker, the steel striker, to get that off first before you can really
get that spark going.
Like I said, in the back end it comes with two of these little tufts of tinder, I'm going
get this down here. Dear moss, I'll try to get this going real quick. And usually you
want to hold the striker in place and pull your Pharisee right away so that it showers
the sparks instead of trying to shove them off. See how quick that took? Get your moss
going. Go ahead and roll your twigs and sticks up over. Man you got that on one try and I
forgot to hit record. Did you? No I messed up. Again you can sit here and just keep feeding
it until we get bigger and hotter. Get you a little more of a cold bed down in here and
then you would start adding these. But again, that's going to be another few minutes away
before we could get hot enough to do that. Throw those on and you ain't got to do it
all fancy. People talk about doing a lay for a fire. Other than that first set up, they
say fire loves chaos. So, just throw that stuff on there.
Who says that?
I don't know.
I've heard that more than one time in the world scale.
Sounds good though.
If our
college
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}