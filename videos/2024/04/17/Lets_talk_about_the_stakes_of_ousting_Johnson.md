---
title: Let's talk about the stakes of ousting Johnson....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=xvcuLsHkbGc) |
| Published | 2024/04/17 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the move by the Twitter faction of Republicans to remove the Republican Speaker of the House, Johnson.
- Mentions the dynamics at play and the potential downstream effects of this move.
- Describes how Johnson wasn't the choice of the Twitter faction, leading to unhappiness and attempts to oust him.
- Notes Marjorie Taylor Greene's motion to vacate and the gathering support to remove Johnson.
- Points out that public support seems to be in favor of removing Johnson, but public and private support may differ.
- Speculates on the potential outcomes if the Twitter faction successfully ousts Johnson.
- Emphasizes the high stakes for the Twitter faction and the risks involved in attempting to remove Johnson.
- Suggests that the move to remove Johnson might not be the smartest decision for the Twitter faction.
- Comments on the internal discord within the Republican Party and its implications for future elections.

### Quotes

1. "If they don't succeed, they're in a worse position than when they started and if they fail miserably they very well may be ending their political careers."
2. "It's a big gamble for Twitter faction Republicans."
3. "There is a lot of discord within the Republican Party that does not bode well for them come the next election."

### Oneliner

The Twitter faction Republicans risk their political futures by attempting to oust the Republican Speaker of the House, facing high stakes and potential irrelevance within the party.

### Audience

Political observers

### On-the-ground actions from transcript

- Rally support within your community to address political issues (implied)
- Stay informed and engaged in political developments (implied)
- Advocate for unity and coherence within political parties (implied)

### Whats missing in summary

Insights into the specific strategies and tactics being employed by the Twitter faction Republicans in their attempt to remove the Speaker of the House. 

### Tags

#Politics #RepublicanParty #SpeakeroftheHouse #TwitterFaction #HighStakes


## Transcript
Well, howdy there Internet people, it's Beau again.
So today we are going to talk about the U.S. House of Representatives and Johnson, and
the move by the Twitter faction to get rid of him, Republicans trying to get rid of the
Republican Speaker of the House.
We have talked about the dynamics at play here a couple of times, you know, how if he
does X, Y, and Z, well then maybe the Democratic Party would protect him, whether or not the
votes are there, all of that stuff. What we haven't talked about are the downstream effects
of this move and what happens. So a quick recap, when Johnson became speaker a whole lot of people
thought he was going to be the Twitter factions guy. He was going to be their speaker. Turns out
he's not. A whole lot of them are very unhappy. Some of them are downright combative. Marjorie
Taylor Greene put forth that motion to vacate, but didn't do it in a way that forced it
right then.
It was a warning.
She has been gathering support to try to oust him from that position.
There is some public support now to do it.
Based on what's public, the votes aren't there.
However, what's public and what's private aren't always the same thing.
So we'll run through it.
If the Twitter faction under the leadership of Marjorie Taylor Greene moves forward with
this and they achieve a decisive victory, meaning they oust Johnson and get somebody
who is aligned with them in the Speaker's spot, well, they're back.
Right now, they don't have a lot of influence, not outside of social media.
Johnson has done a very good job of bringing them to heel.
If they can oust him and get somebody of their mind in, they're back.
They have their influence back.
If they can oust him and the new person is not aligned with them, they have failed again
and they lose even more influence.
become even more irrelevant. And then the worst case scenario for them is they
make the move and he retains his seat. He retains that position. He stays speaker.
If that occurs, a lot of them will probably make it through this election
and keep their seat. I don't think that would be true for the
election after that, and even those that are heavily entrenched would probably be
gone by the following election. This is high stakes for them. It's not a move I
would make if I was in their position because it's all or nothing. It is make
or break. If they don't succeed, they're in a worse position than when they
started and if they fell miserably they very well may be ending their political
careers. Not immediately because it's already it's already too far into the
election cycle but in two years four years at the outside I don't expect you
would see a whole lot of them. So that's what's going on. Now obviously that can
be influenced by whether or not Trump gets reelected and you know he ends up
back in the White House. There's a lot of little things that could alter that
outcome, but based on what is most likely, if Johnson retains his seat, they're done.
done. This is a, it's a big gamble for Twitter faction Republicans. I don't know that it
makes a whole lot of sense for them to do this. It seems like it would be smarter to
just bide your time and see what happens in 2024. That would seem like it would be the
smart move but I mean that doesn't play as well on social media so at this point
in time we don't even know that they're going to do it but it does appear that
they are trying to build that support and they're starting to get public
statements saying that they support the move to get rid of Johnson that will
complicate things for the Republican Party through the election and
realistically continues to show the dysfunction that exists in the US House
of Representatives when it comes to the Republican Party. They want to paint the
image of everybody being united behind Trump. They're not even united
amongst themselves in the House.
There is a lot of discord within the Republican Party that does not bode well for them come
the next election.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}