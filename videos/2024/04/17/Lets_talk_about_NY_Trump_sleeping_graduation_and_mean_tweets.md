---
title: Let's talk about NY, Trump sleeping, graduation, and mean tweets....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=zvSd7m_M6uI) |
| Published | 2024/04/17 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recap of events in New York involving the former president's entanglement and recent developments.
- Former president believed the judge wouldn't allow him to attend a family function, sparking outrage.
- Judge did not rule against former president attending, but mentioned trial time as a factor.
- Actual proceedings have seven jurors, aiming for 18, not halfway there yet.
- Possibility of trial starting on Monday is hopeful but ambitious.
- Former president reportedly fell asleep in court for the second time, causing upset.
- Hearing on gag order violation fines set for the 23rd or 24th.
- Bragg seeking $1,000 per violation as a fine, with potential for jail time up to 30 days.
- Proceedings adjourned till Thursday, to resume juror selection process.
- Former president likely to generate future outrage based on perceived events without full context.

### Quotes

1. "Two minutes of hate designed to keep you easy to manipulate, easy to control."
2. "It might be a good idea to find out what was actually said before just getting very, very angry."
3. "Old Dozing Don reportedly fell asleep for the second time in two days in court."
4. "He seems to be very upset about that and very bothered by the reporting of that."
5. "All of that is based on accepting that what Trump believed was true."

### Oneliner

Former president's perceived court restrictions spark outrage, but details show a different story; trial proceedings and potential penalties unfold amid ongoing drama.

### Audience

Courtroom spectators

### On-the-ground actions from transcript

- Attend court proceedings to observe and stay informed (exemplified)
- Stay updated on trial developments and outcomes (implied)

### Whats missing in summary

Details on the potential impact of the ongoing trial proceedings and the significance of staying informed on the actual events.

### Tags

#NewYork #FormerPresident #CourtProceedings #GagOrder #JurorSelection


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about a recap
of events up in New York
dealing with the former president
and his entanglement up there.
There are a number of little developments
that are worth noting.
We'll also do a quick explanation
of something that was widely discussed
on social media today
when it probably didn't need to be discussed at all.
And we'll just run through how everything is developing.
Okay, so you might have heard
that the former president was very upset
because he believed the judge was not going to allow him
to go to a family function later on.
This belief of Trump's turned into the outrage of the day.
The two minutes of hate designed to keep you easy to manipulate, easy to control.
It was a firestorm.
Talking about how mean the judge was and all of this stuff.
And then you had people from the other side of things saying, well, did Trump go to any
of the other ones?
Here's the thing.
All of that is based on accepting that what Trump believed was true.
What did the judge actually say?
According to reporting, he said, it really depends on how we're doing on time and where
we are in the trial.
The judge didn't rule that he couldn't go.
The AP is saying that the judge indicated that if the trial proceeds as planned, that
he'd be willing to adjourn for the day.
Wow, that's totally different than all of the conversations
that occur, right?
Weird.
Okay, so how did the actual proceedings go?
At this point, they have seven jurors.
People are saying they are halfway there.
They are not.
They need 18, so they're going for 18 jurors.
They have seven.
There are some who are expressing hope
that they could actually start the trial on Monday.
That seems ambitious to me, but we'll see.
Old Dozing Don reportedly fell asleep for the second time
in two days in court.
He seems to be very upset about that
and very bothered by the reporting of that,
probably because of all that Sleepy Joe stuff that he said, but the reporting is that, yes, he dozed off
two of two days so far. In part of the proceedings that some people found mildly entertaining,
Trump had to sit quietly while potential jurors had their social media looked at
and had to sit there quietly while people read mean tweets about him, basically.
The gag order thing. I think the hearing for that is set on the 23rd or 24th. I
know it started on the 24th but I think they moved it to the 23rd and then I saw
reporting that said they moved it back. I'm not sure. Currently it looks like
Bragg is seeking $1,000 per violation as a fine. Also, warned that for future
violations, there might be other penalties. Keep in mind, the judge does
have the ability, in this case, to put Trump in jail for up to 30 days. I don't
know if they will actually seek that, but the warning was there. Now, the
proceedings will not go on tomorrow. They are adjourned till Thursday, and then
everything will come back and they'll start the process all over again looking
for more jurors and go from there. So I would imagine given the amount of
mileage he got out of coming out and saying hey the judge said this and it's
super mean to me and how much outrage was generated he's probably going to try
to do that again in the future.
It might be a good idea to find out what was actually said before just getting very, very
angry about how Trump perceived the events when, I mean, for all we know, he wasn't
even awake when they occurred.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}