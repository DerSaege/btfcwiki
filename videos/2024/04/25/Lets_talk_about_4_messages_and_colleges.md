---
title: Let's talk about 4 messages and colleges....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=r5JMdXOGWV4) |
| Published | 2024/04/25 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Received messages from college students questioning the effectiveness of protests and demonstrations.
- Students express concerns about the lack of immediate impact and motivation to continue participating.
- Beau addresses the importance of remaining hopeful and staying in the fight to influence foreign policy.
- Talks about the significance of performative actions in carrying ideas forward, despite some being superficial.
- Emphasizes that influencing foreign policy is a long, tedious process with high stakes and often involves losing most of the time.
- Warns about the consequences, both intended and unintended, of attempting to influence foreign policy.
- Urges individuals to continue striving for change, even though it is challenging and may result in frequent losses.
- Notes the demoralizing effect of constant messages that nothing will change, and the detrimental impact of adopting a cynical attitude.
- Advises against using rhetoric that undermines motivation and reinforces a belief that change is impossible.
- Concludes by stressing the importance of persisting in efforts to influence foreign policy for the greater good.

### Quotes

1. "Don't expect immediate effects. How do you stay hopeful? Well, I mean, the easiest way is to remember that you don't have a choice."
   
2. "All of these messages, the goal is to save lives. The goal is good."

3. "It's worth doing. If your goal is the preservation of life and that's really what it's about for you."

### Oneliner

College students express doubts about the impact of protests, while Beau encourages persistence in influencing foreign policy for positive change despite frequent setbacks.

### Audience

College students, activists

### On-the-ground actions from transcript

- Reach out to representatives and actively participate in efforts to influence foreign policy (implied).
- Stay informed about global issues and understand the long-term nature of effecting change in foreign policy (implied).
- Persist in advocating for causes that aim to save lives, despite facing challenges and setbacks (implied).

### Whats missing in summary

Beau's insightful commentary on the importance of maintaining hope and continuing efforts to influence foreign policy in the face of adversity.

### Tags

#Activism #Influence #ForeignPolicy #Change #Hope


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we're going to talk about some messages I received
from people at college.
And there's four of them that we're going to read.
There's actually more, and they've all come in within the
last 36 hours or so.
But we'll go through them, and then I will answer the
specific questions, and then provide some general
commentary. All of them are linked by a theme.
Okay, so, Beau, I go to Name of School. I've been asked to participate in
demonstrations by my friend, who has spent the last three months
telling me that Biden will never change course and help the Palestinians.
Then, WTF, good is the protest.
I want to help if I can, but I don't
want to take a bunch of professional risk
for no reason.
The next one.
I went to the demonstration at name of school.
The next day, some of my friends who stayed out overnight
were talking about how I should have stayed.
And when I said that the media had already left,
They said it wasn't for the media because nothing was going to change anyway.
I didn't go back and I feel bad, but it just took my motivation away.
Are they right?
Is it all pointless?
There was a really long lead-in to this one that we're not going to read, but how do
you remain positive when keeping track of all of this?
Everybody else I watch is basically throwing their hands up.
do you stay hopeful? It's been six months of death every day. I just can't anymore.
And then the long one. I watched a video of yours a long time ago about
performative actions. I know you said you liked them, but I can't remember the
reason and I need to hear it right now. I was recently at a demonstration and
afterward a bunch of us were all hyped at this restaurant. I brought up an app
that lets you contact your reps. They laughed. They literally... something laughed.
When I go out and ten minutes into it, the guy is dropping red flags. I always
think, did I shave my legs for this? I blurted out, did I just get... a word... pepper
sprayed for this. They said that it was to send a message to... who? The people they
laughed about contacting. That's who. Performative, self-righteous, there's a
whole bunch of descriptive words here, people. Give me the reason they help
again. They don't give a care about any of this. They certainly don't give a care
about the Palestinians. I got pepper sprayed and pushed down by some random
organ and they think that we're winning because they got to feel militant. I need
something on diversity of tactics or how they're really helping right now
because I feel so defeated their cynical bull hurt more and was so much more
demoralizing than actually getting attacked. Yeah country's doing great. Okay
so are they right? Is it pointless? Of course not, but don't expect immediate
effects, and we'll go over that in a little bit more detail in a minute. It's
not pointless. All of these messages, as near as I can tell, the goal is to save
lives. The goal is good. There will probably be another video coming as well
from people who were at demonstrations who did not have necessarily a good goal,
but if your goal is to save lives, it's worth doing. Just don't expect immediate
effects. How do you stay hopeful? Well, I mean, the easiest way is to remember that
you don't have a choice. You don't have a choice. Not if you want to stay in the
fight you don't have a choice. I mean that's that is something that you have
to understand like you cannot give up on hope and even remotely believe that
you're gonna be effective because you won't be. You won't be. You will end
up being part of the uniting theme here, one thing I would say is that being aware of what's
going on doesn't mean that you have to expose yourself to that quote death every day.
You don't have to expose yourself to the imagery to be aware.
I mean, I do, but I have to.
Most people don't.
I know that's a big thing right now.
Don't look away.
Yeah, as somebody who has looked at it for years on end,
it's OK to look away.
Totally all right.
If you don't, you'll either become defeated or be
desensitized.
And neither of those is actually a good thing.
It's okay to take a break, and you don't have to expose yourself.
You can know what happened without seeing it.
And then the long one.
The name of that video is something like, let's talk about spears and social movements
or social change or something like that.
I'll put it down below.
But you're right.
I love the performative. Love it. Absolutely love it. Because the people who engage in
performative actions, most times they are the people who actually carry the idea beyond
those who actually care. And when you're talking about trying to create influence, the idea
to get that idea, whatever that idea is, into the main political discussion. Those
people who are performative, they're critical for that. Those people who show
up and take a selfie and leave, you're right, they probably don't actually care.
Those people who went so they could get a cool story, but they have no intention
of contacting their rep, yeah, probably performative, but they're going to tell that story.
They're going to talk about how they got knocked down.
It's going to carry the idea further.
I will put that video down below.
Now, as far as being demoralized and all of that, reading this, you can probably be somebody
who tries to influence foreign policy.
Most people can't, not for any extended period of time, because it's hard.
It is hard.
It's different than other kinds of ways that people influence society.
Foreign policy is a different game.
And there are things that you have to know going into it.
First is that it's not going to be quick.
Don't expect immediate results.
talking about changing the direction of a nation, it does not occur quickly.
There is no instant gratification here.
It is a long, tedious process that absolutely has to include contacting your reps.
There's no other way to do it.
That has to be part of it.
It won't be easy.
There is no simple solution.
The simple solutions that have been promised, none of them have worked, right?
The ICJ, the UN.
Those international bodies, they're not actually good at stopping things once they start.
They're good at normalizing the agreements that occur at the end.
That's what they're good at.
at taking the changes that happened because of the foreign policy event and turning it
into the status quo.
That's what they're useful for.
The simple solution isn't.
The easy path is always mine.
The other thing that you have to know is that you're going to lose most of the time.
If you are going to make foreign policy your thing, trying to influence foreign policy,
that's going to be what you set about to do.
You have to understand that you are going to lose most of the time, because you have
to put it in context of what you're actually trying to do.
You as an individual or a small group of people, you are entering a game of literal world domination
and you as an individual or a small group of people are trying to go up
against the interests of nation-states, you're gonna lose most of the time. It
won't even be close to a winning record. Does that mean that you shouldn't do it?
No. I mean I'm a big supporter of the idea, you know, you choose fights that are
big enough to matter and small enough to win but when it comes to
foreign policy, it's important to remember
the stakes and it's important to remember that there are people
counting on somebody
to try to influence it for the better, try to save lives.
And then the hardest part, and the part that you have to be aware of
even though I hope you never experience it, is the consequences.
The consequences of winning or losing, because it is life and death.
The unintended consequences of winning, sometimes, are...
they're not good.
you may feel guilt because of it.
I'll give you an example.
A two-state solution, right?
That is something that is widely supported
by a whole lot of people.
A possible outcome of that, that it's not like a tiny
percentage either, is that it could trigger a civil war
Gaza. So if you're fighting for that, you're trying to influence foreign
policy and achieve that goal and then that happens. You have to be aware of
those possibilities. There's a reason why a lot of times when people push for
something most times because it's seen as a simple solution and it just so
happens to align with the nation's interest and it occurs. A lot of times
they don't talk about the after effects because a lot of times they aren't
pretty. Trying to influence foreign policy is a very different game than
trying to influence local, state, or federal law, or, you know, or trying to achieve civil
rights or something like that.
It is very, very different.
It is long, it is tedious, it is hard, the stakes are incredibly high, and you are going
to lose the overwhelming majority of the time.
But somebody has to do it.
But somebody has to do it.
And if you are one of those people who, after getting pepper sprayed and knocked down, your
goal while everybody is there and hyped, is not to just recount your glory of being, you
know, a super tough activist, but is to try to get people to reach out to their reps,
Yeah, you probably got it.
You probably have the clarity and the determination to make that your thing.
Just know that it's going to be hard.
Now general commentary, all of these, all of these, all of the ones where they were
people who certainly appeared that their actual goal was the preservation of life, that that's
really what was motivating them.
All of these and some that I didn't read.
The uniting theme was that they felt demoralized.
Not because of the situation, but because they were constantly told that nothing would
change.
Doomers, doomers.
The people who end up adopting those positions and that rhetoric, they get there a number
of ways and it's really two main groups with a subgroup but the first is people who have
burned out because they didn't take a break and they just, they don't care anymore.
They can't see that things could be better.
The other are people who don't care.
Sometimes it's a defense mechanism.
They burned out, they suffered from it, and they're back, but they're not going through
that again.
So that very cynical attitude, it's a shield, it protects them.
They know what's right, but they're not going to allow themselves to fill it.
And then sometimes they don't care because they don't care, they never did.
It's an image that they're pursuing.
That rhetoric, if you find yourself starting to use that rhetoric or be in that position,
understand that it's not without cost.
people who are around you may be influenced by it and it may undermine their motivation.
You know, there's, you see this a lot when it comes to the climate.
You see it a lot when people are just like, well, it doesn't matter anyway.
And a lot of times it comes off as if they're trying to express the urgency.
And maybe it's an attempt to guilt trip people into changing.
That's not how it works.
If you tell people there's no point in changing, they're not going to.
If you tell people there's no point in doing this because the powers that be are never
going to change, they're not going to try. That rhetoric is not helpful. So I'll put
the video down below, the one about Spears. If your goal is the preservation
life and that's really what it's about for you. It's worth doing and I know, I know it's me, this is
foreign policy, morality's got nothing to do with it. Yeah, that applies to nation-states, not
individuals getting involved. If you're an individual and you're going to make foreign policy your thing
and you're going to try to influence it, morality should be a pretty big part of it. Anyway, it's
It's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}