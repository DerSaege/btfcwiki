---
title: Let's talk about Trump, Harris, and the Secret Service....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=nCjziUCu9ag) |
| Published | 2024/04/25 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's Secret Service detail is planning for a scenario where he is found in contempt and ordered to go to jail.
- The contingency planning started after the ADA mentioned not seeking jail time yet.
- The main concern for the Secret Service is figuring out how to deal with movement in such a situation.
- They will need to ensure Trump's medical needs are taken care of in case he needs to be moved.
- Moving on to Vice President Harris' detail, there was a medical incident involving a uniformed Secret Service officer behaving erratically.
- The incident led to a physical altercation with the lead agent and the officer being cuffed.
- After an evaluation, the officer was taken to the hospital.
- Reports differ on whether the officer was armed during the altercation.
- Beau speculates on potential personnel like New York court staff or US Marshals being brought in to assist in moving Trump if needed.
- He humorously mentions that the Secret Service may long for simpler days of just hiding girlfriends.

### Quotes

1. "The main concern they're going to have is not really securing a particular area. It would be figuring out how exactly to deal with movement."
2. "It does appear to be a medical thing."
3. "So yeah that's what the Secret Service has been up to."
4. "I'm sure they miss the days of you know just like having to hide girlfriends or whatever."
5. "Y'all have a good day."

### Oneliner

Beau shares insights on Trump and Harris' Secret Service details, from jail contingency plans to a medical incident, portraying a humorously contrasting scenario for the service.

### Audience

Internet users

### On-the-ground actions from transcript

- Contact local organizations to understand how community policing can be supported (implied).
- Attend community meetings to learn about local policing practices and challenges (implied).

### Whats missing in summary

Insights on the implications of these incidents on Secret Service operations and public perception.

### Tags

#Trump #VicePresidentHarris #SecretService #ContingencyPlanning #MedicalIncident


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump, Vice President Harris, and the
United States Secret Service, because there were developments involving both
details, we will start with the Trump stuff first, so his Secret Service
detail is reportedly figuring out contingency plans to deal with a situation in which he
is maybe found in contempt and ordered to go to jail.
The planning for this reportedly started sometime after the ADA up there said that they were
quote, not yet seeking gel time. So that's as far as the contingency planning has gone
in that regard. Realistically, this isn't going to be very hard. The main concern they're
going to have is not really securing a particular area. It would be figuring out how exactly
to deal with movement.
They're not going to be incredibly comfortable with not being able to move him, especially
if people know where he is.
So I'm not sure if they will bring somebody on from the New York court staff or maybe
bring in the US Marshals, but they will have to come up with some way to move him in the
event of an emergency. And then from there they'll also make sure his medical
needs and all of that stuff are taken care of if this eventuality was to
occur. Now moving on to the Harris detail, apparently they had what at this point
sounds like a medical incident. One of the, as I understand it, one of the
uniformed Secret Service officers was behaving erratically. I think somebody
said that they were they were speaking gibberish, I think is the quote. It led to
an altercation with the lead agent on that detail, a physical altercation. After
that the uniformed officer was cuffed and after an evaluation they wound up in the
hospital. So it does appear to be a medical thing. Some of the reporting
is mixed saying that the uniformed officer was armed and then some are
saying that they were unarmed. My understanding was that the uniformed
officer was armed, meaning they had their weapon, but the altercation did not
involve a weapon. And I think that's where the confusion is coming in when it
comes to the reporting. So yeah that's that's what the Secret Service has been
up to. I'm sure they miss the days of you know just like having to hide
girlfriends or whatever. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}