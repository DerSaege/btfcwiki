---
title: Let's talk about Trump, more entanglements, AZ, and MI....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=fJqbPO7JdBw) |
| Published | 2024/04/25 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Arizona has indicted 18 people, including Giuliani and Meadows, along with 11 others described as fake electors.
- Trump is described as the unindicted co-conspirator in both Arizona and Michigan cases.
- Meadows, Ellis, Giuliani, and Trump are all unindicted co-conspirators in the Michigan case.
- The Arizona case may be more troubling for Trump due to his strong support there.
- More information on the indictments is expected to be released soon.

### Quotes

1. "Arizona has indicted 18 people, including Giuliani and Meadows."
2. "Trump is described as the unindicted co-conspirator in both Arizona and Michigan cases."
3. "The Arizona case may be more troubling for Trump due to his strong support there."

### Oneliner

Arizona and Michigan indictments reveal Trump's entanglements, with him as the unindicted co-conspirator in both cases, posing potential trouble in Arizona due to his strong support there.

### Audience

Political observers, news consumers

### On-the-ground actions from transcript

- Stay informed on the developments of the indictments (suggested)
- Follow reputable news sources for updates on the cases (suggested)

### Whats missing in summary

The full transcript provides detailed insights into the legal entanglements surrounding Arizona and Michigan indictments, offering a comprehensive understanding of the situation.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about Arizona and Michigan
and Trump world and more entanglements
because there is some news and some developments that
seem like they might be worth going over.
We'll start with Arizona because that seems to be
more pressing of the two and according to reporting it appears that Arizona has
indicted 18 people. We don't have all of the names yet and they'll all be
released as the people are served but it does appear according to reporting that
both Giuliani and Meadows are being indicted, along with 11 people who are
described, you know, by fake electors. I think that includes two state lawmakers
and the former chair of the Arizona Republican Party. So that leaves a
decent number still unidentified at this point. Now as far as Trump, which is
obviously going to be the big question there. The general consensus is that he
is described as the unindicted co-conspirator. I think unindicted
co-conspirator one. So there's that and it's likely that as more of the
names come out, they will be recognizable names as well. Moving on to Michigan, the
reporting says that Meadows, Ellis, Giuliani, and Trump are unindicted
co-conspirators there as well. This information came out during a pretrial
hearing in the case of one of the alleged fake electors there. Some of the
rumor mill about who is and isn't charged is pretty interesting. So this is yet
another complication for the former president even if he is not indicted in
either of these cases.
This is still a core group to him, and the Arizona one is probably going to be the most
troubling given the support that Trump and his community enjoy there and the fact that
it went before the grand jury there.
So that's, that is the latest news on this particular topic, I would imagine that tomorrow
there will be more information about who, who is being indicted for certain, and we'll
see, we'll see how it plays out then.
Anyway, it's just a thought, y'all have a good night.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}