---
title: Let's talk about Tiktok, root causes, and concerns....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=2gcWcpHpBMA) |
| Published | 2024/04/25 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the goal behind the TikTok legislation, clarifying that it's not a ban but rather to force the sale of the U.S. component to an American company due to perceived national security concerns.
- Raises the concern of data collection and questions whether it should worry the average person, given Chinese intelligence's bulk data buys.
- Addresses the possibility of TikTok being used for Chinese information operations, particularly regarding elections, and its potential impact.
- Mentions a less discussed concern where TikTok's notification to users about Congress led to more support for the legislation, possibly due to demonstrating the app's influence.
- Points out the questionable effectiveness of the legislation in addressing root causes and suggests that it may not deter Chinese intelligence activities.
- Talks about timelines and the likelihood of legal challenges delaying any immediate actions against TikTok, allowing creators time to prepare for potential platform changes.
- Speculates on the completion of a sale of TikTok and how it might not significantly alter the app due to its current success.

### Quotes

1. "The concerns, they're real. Don't know if this is the best way to address them."
2. "It's not a ban and that's pretty much it."
3. "It is about national security concerns."
4. "Y'all have a good day."
5. "It sounds good, but it's probably not going to be that effective."

### Oneliner

Beau explains the TikTok legislation, addresses concerns about data collection and information operations, and questions its effectiveness in countering national security threats.

### Audience

Creators, TikTok users

### On-the-ground actions from transcript

- Stay informed about the developments regarding TikTok's situation (implied)
- Prepare for potential platform changes by exploring other social media platforms (implied)

### Whats missing in summary

Exploration of potential long-term implications and effects on social media landscape.

### Tags

#TikTok #NationalSecurity #DataCollection #InformationOperations #Legislation


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about TikTok.
We are going to talk about what's going on,
the timeline behind it, the concerns that prompted it,
the root causes, and whether or not
those are actually being addressed by this,
and whether or not the concerns are founded
in a whole lot of ways.
Okay, so you have probably heard about the, quote, TikTok ban, we'll start there, it's
not a ban.
The goal of the legislation is not to ban TikTok.
The goal is to force the sell, it's forced capitalism, to force the U.S. component of
TikTok to be sold to an American company.
what it's about. Why? Perceived national security concerns, because there is a belief among those
in national security circles that TikTok is closely aligned or could be influenced by Chinese
intelligence. When it goes to the actual concerns, there's a whole bunch of them that get talked about.
One is data collection. Is that concern something that the average person, by
average person, I mean you don't work for the military, you don't work for a
government agency, you're not a journalist, you don't deal in trade
secrets, so on and so forth. Is that something that the average person would
need to be concerned about? If you have other social media apps on your phone,
Not really, because if the concern is Chinese intelligence, it's worth
remembering that Chinese intelligence does bulk data buys. So they probably
already have the information that could be harvested in that way, at least most
of it. Congress is concerned about the exceptions to that. That's what the
government is worried about. They're worried about military, government agencies,
these journalists trade secrets, so on and so forth.
I mean, yes, it's a concern.
Is it a huge one?
I mean, I guess that depends on whether or not your job depends on countering those risks.
Then another one of the concerns that keeps being brought up is the possibility of it
being used for Chinese information operations.
I mean, that's not how they say it.
What they say is China could meddle in the elections.
Is that real?
Definitely real.
Definitely real.
But they can also use just about any social media for that.
Might it be more effective?
Maybe.
I can't say for sure one way or the other on that.
But they would absolutely use TikTok or any other social media platform to run information
operations. If it was owned by an American, if it was an American company,
then there's more leeway for the US to counter it. That's part of it. But we talk
about information operations all the time on the channel. They occur. Every
country does them. Almost every country. Certainly every world player. And I mean
with media literacy being the way it is in the United States? Yeah, it's a concern.
We have seen information operations take hold in certain segments of society. So,
yes, is this the best way to counter it? Does it address the root cause of the
fact that people will very easily fall for this stuff? Not so much. But
information operations are a major part of the modern battlefield now. So it's a
concern from the national security standpoint. Then you have the one that
isn't really getting talked about that much and in this case TikTok may have
made a mistake. A lot of people found out about this when TikTok itself sent out a
notification, saying Congress is trying to shut us down, and then a whole bunch
of people called Congress. And it was weird because a whole bunch of people
called Congress saying, don't ban TikTok, and it seemed like that made Congress
want to move forward with this legislation even faster. It was a bizarre
turn of events, unless you think of things from the standpoint of a national
security briefer who talks to Congress and things that would probably be brought up such
as the ability to send a notification to millions of Americans at once and influence their behavior.
That would probably be brought up in a briefing by saying, imagine you're sitting at home
watching TV and you're seeing news coverage about elevated tensions between the United
States and China and then all of a sudden your phone gets a notification
and you look down it's an app you use every day and it says that there is a
nuclear strike imminent where you live. Imagine the panic and the economic costs
that would ensue. If you don't know national security briefers that they
like always present like the most dramatic worst-case scenario imaginable.
So, Congress was probably told about this, and then they got a real-life demonstration
of it.
It probably did not help TikTok.
At the same time, while it did show that TikTok, like many apps, has that ability, to my way
of thinking, it kind of suggests that maybe they aren't as closely aligned with Chinese
intelligence as a lot of people may believe because I do not think that Chinese intelligence
was sitting there and thinking, you know, the Americans are really concerned about this
capability.
Let's show them that we can do it.
That just doesn't really track to me.
So there may be a wider gap than some people think.
But I mean, is it a real risk?
Would it motivate people?
Would it alter Americans behavior if it was used in that way?
I mean, obviously, yes.
But does this address the root cause?
Not really, okay?
So you have these apps.
All that is really going to be accomplished is basically the Chinese intelligence will
be able to do the exact same thing with a couple of extra steps.
It doesn't really address the root causes of any of the issues, I mean, not even the
data collection because they could, you know, ban bulk data cells.
That would be a move.
So the concerns are real.
I don't know that this is the way to go about countering them though.
In some ways, it might have been wiser to allow TikTok to remain as is, as kind of a
lightning rod.
The US efforts to counter those kinds of operations could be pretty focused on an app that they
knew about.
Now it's not like this is going to stop Chinese intelligence from pursuing those capabilities.
They're just going to do it in a sneakier way now.
Okay so let's talk about timelines because this is kind of what prompted this.
If you are a creator on TikTok, this is something that you're probably concerned about.
No time soon.
into the law is, I want to say it's nine months with a three month extension.
So that's a year, off top.
But beyond that, as many, many people have brought up, there are legitimate First Amendment
concerns that have merit.
My guess is that this is going to go to court.
In fact, I think the company has already kind of indicated that.
will probably be a stay in place.
So basically the clock doesn't start to count down until the case is resolved or until a
stay is lifted, so on and so forth.
How long does that take?
Think about how long the Trump cases have been going on.
And with TikTok, my guess would be that their actual goal would be to delay, delay, delay,
delay delay because it's a social media company and eventually they you know
decline anyway. So it would seem like their strategy would be to delay it as
long as possible in in the belief that it won't be trendy anymore in a few
years. So while there's no immediate you need to get your stuff together and get
this platform by next week, it's probably still wise if you're a creator on TikTok to start
looking at other platforms and figuring out where you're going to go. You just have more time to do it.
And then there, I mean there is the chance that TikTok actually succeeds in some ways.
I don't know, but at some point I would imagine that a cell would be completed.
It would be the end result. When that cell is completed, odds are not much is going to change,
because from what I understand TikTok is a relatively successful model, so they're
probably not going to tinker with it too much immediately. So that's what's going on.
The concerns, they're real. Don't know if this is the best way to address them. It
is certainly not addressing the root causes that are causing the national
security concerns. There's a pretty lengthy time to make changes and
decisions. It's not a ban and that's pretty much it. I've seen people assign a
a bunch of different motives to why the government is doing this. Most of them
are not true. There's one that says, you know, it's so they can control the
narrative. I mean there's an element of truth to that because of the information
operation angle, but I don't know that it is, I don't know that it's the way it's
It's being framed in a lot of the videos that I've seen related to that angle.
It is about national security concerns.
It is the perfect congressional solution to something like that, which means, I mean,
it sounds good, but it's probably not going to be that effective.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}