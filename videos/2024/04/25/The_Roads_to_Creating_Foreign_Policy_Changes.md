---
title: The Roads to Creating Foreign Policy Changes
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=_-yPz2YBywI) |
| Published | 2024/04/25 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the term "manufacturing consent" in the context of foreign policy, shifting public opinion to gain support for actions like wars or military bases.
- Illustrates how government agencies create narratives through bulletins and press releases to influence public perception.
- Describes the process of manufacturing consent for war, starting from portraying a country's people sympathetically to eventually justifying military intervention.
- Mentions that the same process can be used for non-military purposes, like establishing a military base or influencing infrastructure investments.
- Analyzes the US's selective vigilance towards allies' behavior in conflict zones, pointing out the lack of accountability in certain situations.
- Walks through a hypothetical scenario involving the Kingdom of Danovia to demonstrate how consent can be manufactured by leveraging media coverage and public opinion.
- Suggests that foreign policy decisions are primarily driven by power dynamics rather than moral or ethical considerations.
- Outlines a potential chain of events where the US pressures the Kingdom of Danovia to change course through public opinion and diplomatic maneuvering.
- Concludes with the idea that regardless of individual motives, foreign policy decisions revolve around power dynamics.

### Quotes

1. "It's about power, not about any of that other stuff."
2. "Manufacturing consent for a major foreign policy change."
3. "A deviation from the norm is always worth paying attention to."
4. "Regardless of what the individuals involved may care about, it's really about power."
5. "Anyway, it's just a thought, y'all have a good day."

### Oneliner

Beau explains how "manufacturing consent" influences foreign policy decisions, revealing the power-driven dynamics behind public perception shifts and policy changes in a hypothetical scenario involving the Kingdom of Danovia.

### Audience

Policy analysts, activists, citizens

### On-the-ground actions from transcript

- Monitor and critically analyze media narratives and government communications to identify potential instances of manufactured consent (suggested).
- Advocate for transparency and accountability in foreign policy decision-making processes (exemplified).

### Whats missing in summary

The full transcript provides an in-depth analysis of how public opinion can be manipulated to support foreign policy decisions driven by power dynamics, urging viewers to critically question narratives and decisions presented by governments. 

### Tags

#ManufacturingConsent #ForeignPolicy #PowerDynamics #PublicOpinion #GovernmentNarratives


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about a term
that most people are familiar with.
You've probably heard it at least once.
But most people are also only familiar with it
in one particular context.
And the same process gets used in a whole lot
of other contexts in different ways to kind of shift things
and impact the way Americans look
at different situations in foreign policy.
So we're going to talk about this term
in the way it's normally used,
and then the other ways it gets used,
in hopes of maybe shedding some light
on some recent surprising events
when it comes to foreign policy.
Manufacturing consent.
Most people, when they hear this term, they think of manufacturing consent for a war.
Because that's normally when people use the term.
If you were alive in the late 80s, early 90s, you saw this.
You saw it firsthand.
This is how we wound up with trading cards.
They were like baseball cards, but they had weapon systems and generals on them in gas
stations.
yellow ribbons everywhere. So we're going to talk about how it normally gets used,
how that term normally gets used, and that process first. Okay, so the United
States realizes that it needs something that the kingdom of Danovia has, okay, but
But they can't just come out and say, hey, foreign policy is all about power, so we need
to invade or realign the Kingdom of Danovia.
They can't do it that way.
They need the consent of the people at home.
But they have made the determination that the Kingdom of Danovia, well, they need some
democracy and some extraction contracts.
So what happens?
Government agencies put together bulletins, press releases, all the time.
They go out all the time.
And all of a sudden, there's a bunch of them dealing with the Kingdom of Danovia.
And well, they're super specific.
These aren't normal little bulletins or press releases.
They have everything that a news team needs to put together a segment.
Sometimes they even include media.
So those news teams that are often overworked, well, they throw that segment together.
And when there's a slow news day, well, that segment goes out and the American people are
told about the good, hardworking, industrious people of Danovia.
Over time, people become more interested.
So we find a little bit about the king out, the king of Danovia.
He's not a good guy.
He's done some really, really bad things.
And this is the situation that the good, hard-working, industrious people of Danovia find themselves
in.
Public opinion shifts again.
Not just are they now interested, well, now those poor people.
along the line as this process continues it switches from those poor people to we
have to help. At this point you have senators and politicians come out. I
have always supported the good hard-working industrious people of
Danovia. We need to help them. In fact, I have a plan to help them to stick with
me. And that is when civilian advisors show up in Danovia meeting with the
Danovian Liberation Organization and those guys are like, hey I believe in
your cause, whatever it is, here's some money, get yourself some weapons, some
berets, ray-bans, whatever it is you need. And that's where it starts. And then
eventually special forces, then special operations, then there's an inciting
incident and then democracy arrives via aircraft carrier in B-52. Manufacturing
consent for a war. That's how it happens. And that's how the term gets used all the
time. But the same thing can occur in other ways. The same thing can occur for
things that aren't war. Let's say the United States decides it would be really
great to have a military base in, I don't know, on the coast of East Africa, okay.
There's a famine there. We need to go there and help. We need to help these
people. So the same thing occurs. The bulletins go out, the story goes out
about the famine, everybody gets interested, we need to help. All of a
sudden the military is showing up to help. Hey, you know what? There's warlords
interfering with us distributing the food, we need to put a military base here.
Poof. Consent is manufactured. And it can be used for simpler things, things that
have nothing to do with the military as well. It can be used anytime there needs
to be a shift in public opinion when it comes to foreign policy, even if it's
just like providing aid for infrastructure investment because the
US needs a port in this region. It can be that simple. So we're familiar with this
term. Most people have heard it. A lot of people have seen it. You now understand
it can be used in other ways. The next little piece of this puzzle. Does
anybody watching this believe that the United States is incredibly vigilant
when it comes to the behavior of our allies in conflict zones, when it comes
to, you know, making sure they follow the laws of armed conflict or, you know, they
don't get rights violations or anything like that. I mean, everybody knows the US
is very much on guard for this, right? This behavior is not accepted in our
allies. This pause is for everybody to stop laughing, nobody really believes
that because it's not true. The most advanced all-encompassing intelligence
network the world has ever known, all of a sudden when it comes to our allies
engaging in less than ideal behavior in a conflict zone, they turn into me in a
grocery store when I see a mom stick baby formula into her bag. I got
something in my eye. They don't pay attention to it. And that's the norm.
That's the status quo. A deviation from the norm is always worth paying attention
to. So, hypothetically speaking, if the United States is allied with the Kingdom
of Danovia and all of a sudden they come out and say, hey, one of their units, the
the Danovian Home Guard, they've been involved in some really, really bad things, and we
truly care about this.
It's something to watch.
It's a big deal.
It's a deviation from the norm.
You can't assign motive yet.
You don't know why, but you know that it's worth paying attention to.
If you want to know why, you have to look at all of the other pieces, all of the other
things that have occurred.
Let's say, hypothetically speaking, the Kingdom of Danovia had US advisors show up, I don't
know, like six months ago and say, hey, King, buddy, pal, I believe in your cause, whatever
it is, but you need to listen to your court.
You need to listen to your court of advisors because they're telling you not to launch
a ground offensive into this province that you have issues with.
They're telling you not to do that.
should follow their advice and the king ignores the US advice as well. Then it
causes a humanitarian issue. Then there's a lot of coverage of this and the coverage
is being linked to the United States. That doesn't look good. Now the US, they
They don't want to sever the relationship with the Kingdom of Danovia.
They've been friends a long time.
And by friends, countries don't have friends, they have interests.
See the Kingdom of Danovia, they're a regional power.
There are three regional powers.
There's the Kingdom of Danovia, Bloody Oblabia, and Iran.
Iran is adversarial.
The other two are aligned with the US.
The US does not want to upset that balance of power.
Therefore, it doesn't actually want to sever the relationship with the kingdom of Danovia.
So it starts to apply pressure to the king.
Talk to him about different things and say, hey, look, things aren't going so well.
You got to alter course here.
But the king's just intractable.
He is set on his course and he decides not just did he do that first big offensive, he
wants to go into another city, let's call it Afar, and the U.S. is really opposed
to that because it's going to increase the humanitarian situation. It's
going to make that problem worse. And the U.S. has tried a bunch of ways to apply
pressure. Meanwhile you have U.S. senators back home and they're saying we
need to cut offensive military aid to the kingdom of Danovia. Look at what
they're doing. Now foreign policy people, they don't care. It's about power,
not about any of that other stuff. You know, those moral arguments, those ethical
arguments, I mean, that's all fine and good, and that's how the world should
work, but we're playing Masters of the Universe games right now. So they don't
want to cut offensive military aid because they want to keep that
relationship intact. At the same time it's an election year. Something's got to
be done and the situation is getting out of hand in a whole bunch of different
ways which might expand the conflict beyond the kingdom of Danovia and draw
the United States in and the US doesn't want to do that. Why? Because it's not in
interest of power. So, what might they do? The Secretary of State may come out and
be like, hey, the Danovian Home Guard, they did these really, really, really bad
things. We're not going to allow our aid to go to them. And just like all of the
other forms of manufacturing consent, the bulletins go out. All of the incidents
that are linked to that one specific unit because all of a sudden the US
intelligence community got that thing whatever it was out of their eye but they
can only see this one unit so they pull it all out they have all of that
information ready and it's packaged for the press all of the incidents or maybe
it's just a list and they hope the media does it on their own but either way you
You have the unit and you have the list.
Now if the king was to come out and be like, you're not doing that, I support the Danovian
Home Guard, okay.
What happens?
The media with all of those segments about all of those different incidents, they have
the ability to talk about the incident and then play the soundbite of the king saying,
hey, I support the Danovian Home Guard.
And the American people hear about whatever the horrible thing was, and then the king
saying they support it.
Public opinion starts to shift, manufacturing consent for a major foreign policy change.
What would it be?
Realistically, in the case of the Kingdom of Danovia, they don't want to sever the
relationship but it might be super helpful to have polling shift and have
a whole bunch of people in favor of those senators that are saying to cut
offensive military aid completely because then the Secretary of State can
show up and be like king buddy pal believe in your cause whatever it is you
know that you know the routine you need to look at this we you got to change
course. People are very very unhappy. We got to turn the temperature down. Maybe
maybe maybe you don't go into afar. Maybe you don't go into that city. You
remember six months ago your advisors were telling you to engage in a strategy
of political realignment and shaping. Maybe you should try that and just let
things blow over. Meanwhile let's get the aid rolling. I don't know if it'll work
but that's the play for the Kingdom of Danovia. So that is the likely chain of
events that State Department is counting on. Now they would like the King of
denovia to just be like, okay fine, I get it, we'll alter course. That doesn't seem
very likely though. So if I had to look at all of the pieces, all of the actions
and try to determine intent, you have the major deviation from the status quo
that signals something big. You have all of the other pieces at play
suggesting that the United States wants to alter course there.
Now, you may want to assign a motive
but you don't need to because
regardless of what the individuals involved
may care about, it's really about power.
So, that process will probably start to play out and hopefully it won't take long and maybe
the King of Denovia will alter course.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}