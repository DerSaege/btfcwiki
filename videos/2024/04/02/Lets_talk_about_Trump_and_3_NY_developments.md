---
title: Let's talk about Trump and 3 NY developments....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=yc2_gVycu80) |
| Published | 2024/04/02 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump posted a $175 million bond in New York.
- A judge imposed an expanded gag order in the hush money case involving Trump.
- The judge mentioned concerns about the Fair Administration of Justice and the rule of law.
- The expanded gag order prevents Trump from going after the family members of court staff and the judge.
- Hope Hicks, a former advisor to Trump, is set to testify in the New York case.
- Hicks' testimony could be damaging as she may confirm details from calls relevant to the case.
- Hicks' involvement in the campaign side is significant in the state's case framing.
- These developments will likely bother Trump and have implications as the case progresses.

### Quotes

- "Such concerns will undoubtedly interfere with the Fair Administration of Justice and constitutes a direct attack on the rule of law itself."
- "Hope Hicks, do y'all remember her? She was an advisor to Trump."
- "That extra person being in the loop, being able to confirm what was said, that would be pretty damaging."
- "I think Cohen has said that she was actually on some of the calls."
- "There will be more developments, I'm sure, when it comes to both the gag order and Hicks."

### Oneliner

Trump posted bond, faced an expanded gag order in the hush money case, and anticipates damaging testimony from Hope Hicks in the New York legal proceedings.

### Audience

Legal observers, political analysts, news followers

### On-the-ground actions from transcript

- Monitor legal proceedings and developments in the case involving Trump and New York (implied)
- Stay informed about the roles and testimonies of key individuals like Hope Hicks (implied)

### Whats missing in summary

Contextual details and implications of the legal developments in the New York case involving Trump and the significance of Hope Hicks' testimony.

### Tags

#LegalProceedings #Trump #NewYorkCase #HushMoney #HopeHicks


## Transcript
Well, howdy there Internet people, it's Beau again.
So today we are going to talk about Trump, New York, and three developments up there
that are worth noting as we slowly move towards the date where everything is going to get
started.
Okay, so the first piece of news is that $175 million bond, Trump posted it.
posted it. Now the other thing that occurred was the New York case that is
widely being referred to as the the hush money case. Trump was hit with an
expanded gag order in that case. The judge said that the average observer
after hearing the defendant. Basically, they should worry not only for
themselves but for their loved ones as well. Such concerns will undoubtedly
interfere with the Fair Administration of Justice and constitutes a direct
attack on the rule of law itself.
So, we'll see where it goes from here.
The expansion means that Trump is not allowed to go after the family members of court staff,
the judge, stuff like that.
We'll see how the former president handles that.
And then in what isn't really surprising news, but definitely worth noting and probably the
piece of news that is going to bother Trump the most, Hope Hicks, do y'all remember her?
She was an advisor to Trump.
She testified before the grand jury that chose to indict in the New York case.
is now being indicated that she would testify at the trial.
Not exactly a huge surprise but probably something that's going to bother Trump a lot because
I think Cohen has said that she was actually on some of the calls.
That extra person being in the loop, being able to confirm what was said, that would
be pretty damaging, especially given the fact that she's on
the campaign side, which that's a major component to the way
the state is framing their case.
So those are three items that are definitely going to show
up again.
So we'll wait.
There will be more developments, I'm sure, when it
come sue both the gag order and Hicks because I feel like, I feel like that's really going
to get under Trump's skin.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}