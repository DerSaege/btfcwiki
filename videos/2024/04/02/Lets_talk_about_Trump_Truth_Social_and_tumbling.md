---
title: Let's talk about Trump, Truth Social, and tumbling....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=AJjoVB_Sj-8) |
| Published | 2024/04/02 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's social media platform, Truth Social, experienced a decline in stock value due to significant losses.
- An SEC filing revealed that the company had a revenue of $4.1 million but losses of $58 million.
- Last week, there were reports of Trump's net worth increasing by billions, but it has now declined by $2.5 billion to around $3.8 billion.
- Without intervention, Trump may not be able to convert the paper money from the stock into actual money for months.
- The drop in stock value has implications for not only Trump but also his supporters who invested based on faith in him.
- The decline in stock value could potentially impact how Trump's political supporters view him as a candidate.

### Quotes

1. "Trump's social media platform, Truth Social, experienced a decline in stock value due to significant losses."
2. "Without intervention, Trump may not be able to convert the paper money from the stock into actual money for months."
3. "The drop in stock value has implications for not only Trump but also his supporters who invested based on faith in him."

### Oneliner

Trump's Truth Social faces stock value decline and financial challenges, impacting both him and his supporters who invested based on faith.

### Audience

Investors, Trump supporters.

### On-the-ground actions from transcript

- Monitor developments in Truth Social and Trump's financial situation (implied).
- Stay informed about the implications of stock value declines on investments (implied).

### Whats missing in summary

Insights on the potential broader effects of Truth Social's stock decline on the online platform landscape and political dynamics.

### Tags

#Trump #TruthSocial #StockMarket #Investors #FinancialImpact #PoliticalSupporters


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Trump and truth social
and how all of that is going
because there have been some developments
and we'll just run through everything.
It's worth remembering
because a bunch of questions have come in about this.
I don't give investment advice.
That's not something I do.
But we will go through what has transpired. If you missed the news, we
talked about it briefly on Sunday how there were a lot of people who believed
that the stock value of Truth Social was, well, it was unexpectedly high and that
it was kind of riding a bubble and so on and so forth. And I said that when that
moment came, well it would definitely be a moment. It is less bubbly today and at
least one of the people that I talked to about this, they seem to believe that
this actually isn't the moment where it's getting bad that that's still to
come, but again it's the stock market so take that with a grain of salt. What do
we know has occurred.
An SEC filing indicated that the company had a revenue of 4.1
million.
Not bad, 4.1 million.
I mean, that's a lot of money.
And losses of 58 million.
That led to a wee bit of a decline
when it comes to the value of the stock.
If you remember last week, there were a bunch of articles talking about Trump's windfall
and how many billions of dollars he made and how his net worth went up.
It was $6.3 billion last week.
That has declined by about $2.5 billion, $1 billion of that today, leaving him with around
$3.8 billion at time of filming. It's worth remembering that without
intervention of some kind, he won't really be able to turn the stock value,
that paper money, into actual money for months. So if this trend was to continue,
it is unlikely that this is the windfall that is going to fix his problems. So
that's what's going on. You know there were a lot of people saying that the
stock was overvalued just based on revenue and and users and a whole bunch
of technical analysis and stuff like that. It appears the SEC filing put some
of that into focus for some people. It's worth noting that if somebody with a
bunch of money really had faith in Trump's brand and put a bunch of money
into the stock, well the price would go back up and Trump would regain some of
that value. The other thing worth remembering, and this may be the more
consequential part, is that part of the reason the value of the stock was so
high, the price was, was because a lot of people trusted Trump and they put their
money into it. They are suffering the same fate that Trump is as far as when
When the price of that stock went down, they lost money too.
That might have wider implications because at least some of the people that were invested
were people who, well, they put a lot of faith in Trump as a whole.
They might also be his political supporters, and it's unclear how these developments might
impact their view of him as a candidate.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}