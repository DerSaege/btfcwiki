---
title: Let's talk about Missouri and a totally unrelated message....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=SVVKCc4qFPM) |
| Published | 2024/04/26 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing a message received about his use of the term "reproductive health care" and Republicans
- Message criticizes Beau's terminology, accusing him of misrepresenting Republicans' stance on reproductive rights
- Beau reads the message without responding and transitions to news from Missouri
- Missouri's state legislature is sending a bill to defund Planned Parenthood to the governor's desk
- Beau questions the motive behind defunding Planned Parenthood, given that abortion is already banned in Missouri
- Explains that Planned Parenthood offers various services under the umbrella term of reproductive health care
- Republican Party's actions are seen as restricting women's rights beyond reproductive health care
- Beau believes the motive is about control and infringing on people's rights, not just targeting one procedure
- Despite abortion being banned, Republicans are still going after Planned Parenthood in Missouri
- Beau criticizes the harm caused by restricting access to reproductive health care, indicating the broader impact on people in Missouri

### Quotes

1. "Stop spinning this."
2. "It's about control."
3. "They're so caught up in their own rhetoric."
4. "But that's already banned there."
5. "Y'all have a good day."

### Oneliner

Beau addresses criticism of his use of "reproductive health care," exposing Republican actions targeting women's rights beyond abortion, particularly evident in Missouri.

### Audience

Social media users

### On-the-ground actions from transcript

- Contact local representatives to advocate for access to reproductive health care in Missouri (implied)
- Support organizations providing reproductive health care services in Missouri (implied)

### Whats missing in summary

Nuances of Beau's tone and delivery

### Tags

#ReproductiveHealthCare #RepublicanParty #PlannedParenthood #Missouri #WomenRights


## Transcript
Well, howdy there internet people, it's Beau again.
So today, we are going to talk about a message I received
and then just some totally unrelated news from Missouri.
The message itself is about a term that I use,
I guess a lot, and the person doesn't think
it's the right term.
And I think that if you feel this way
and you feel strongly about it,
it might be good to hear this said.
So I'm just going to read this, not even going to respond to it, and then we'll just move
on to the news out of Missouri.
Okay, I liked your channel.
I'm a Republican but have watched you for a while.
You generally have a no-spin approach, and even though I can tell what you believe, you
don't try to cram it down other people's throats.
But I see you constantly calling it reproductive health care or reproductive rights.
It's not that.
Every time you talk about Republicans, you say they're attacking reproductive health
care.
And your viewers believe you.
You call Trump's base easily manipulated, but your viewers are too.
They believe Republicans are trying to infringe on their rights and access to reproductive
health care.
Stop spinning this.
You'd have to be a not-smart person to believe Republicans were trying to stop anything
other than abortion.
It's not Republicans who are easily manipulated here.
The IVF fiasco was an accident.
Stop.
Just stop.
Okay.
There you go.
Now in totally unrelated news from Missouri, the state legislature there, they're sending
something up to the governor's desk.
And what it would do in essence is defund Planned Parenthood.
I mean obviously with this message fresh in my mind, I will say that this is a
Republican move to stop a... wait a second, wait, that doesn't make any sense. It
can't be, it can't be about that because in Missouri that's been banned since
2022. So, like, you know, Planned Parenthood, they're not doing those in Missouri because
it, you can't. I mean, so it must be something else. Let's think about the other things that
Planned Parenthood does. They do, let's see, they provide contraceptives, they do screenings,
they do general health care. Oh, you know what? There's an umbrella term for all of this stuff.
It's called reproductive health care. To be clear, this message came in today. The movement
on this being sent up was two days ago. Point is, I didn't have to dig for it because it's
pretty constant. The Republican Party is not just going after one thing. They are
restricting women's rights in a whole bunch of different ways. And it's not
even just reproductive health care. There's a reason you have a lot of
figures on the right talking about no-fault divorce as well. It's about
control. It's about, well, infringing on people. The reason I use that term is
because it fits. The reason I say that's what they're going after is because they
are. That's what it's about. It's about control. It's not just one procedure.
That's just the rhetoric that they used to sell it to people. Again, for clarity
sake, they're going after Planned Parenthood in a state where that's already banned.
So it can't be about that.
That's just how they cloak it.
That's just what they say.
But that's already banned there.
So the only thing that you can really assume is that it's about the other things.
they're so caught up in their own rhetoric and they bought into their own
propaganda so much they don't realize that by doing this they are going to
harm a whole bunch of people in Missouri who are going to lose, well, access to
reproductive health care. Just saying. Anyway, it's just a thought. Y'all have a
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}