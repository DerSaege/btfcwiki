---
title: Let's talk about Biden, Johnson, and the National Guard....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=3fsnI7C4U3I) |
| Published | 2024/04/26 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Speaker Johnson and other Republicans want Biden to send National Guard to colleges.
- Mentions May 4th, 1970, Ohio event to caution against the idea.
- Governors, not presidents, should request National Guard deployment.
- White House defers to governors for such requests.
- Signing off on a deployment request is usually a formality.
- Congress's request goes against states' rights principles.
- Military force at demonstrations can escalate unrest instead of quelling it.
- Using force often leads to demonstrations spreading.
- References Trump's mistakes in the Pacific Northwest.
- Caution against wishing for forceful suppression of demonstrations due to potential harm.

### Quotes

- "Governors, not presidents, should request National Guard deployment."
- "Using force often leads to demonstrations spreading."
- "Caution against wishing for forceful suppression of demonstrations."

### Oneliner

Speaker Johnson and Republicans call for National Guard at colleges, but history warns against it; force at demonstrations spreads unrest, not quells it.

### Audience

Activists, protestors, policymakers.

### On-the-ground actions from transcript

- Contact local representatives to voice opposition to deploying National Guard at demonstrations (suggested).
- Educate others on the risks of using military force against demonstrations (implied).
- Support nonviolent methods of protest and conflict resolution in your community (generated).

### Whats missing in summary

The full transcript provides a detailed analysis of the potential consequences of deploying the National Guard at demonstrations, drawing on historical events and expert opinions.

### Tags

#NationalGuard #Protest #Demonstrations #StatesRights #CommunityPolicing


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about President Biden
and Speaker Johnson and a request, the National Guard,
and then a question that is kind of entertaining
but can be used to be pretty informing as well.
OK, so if you've missed the news,
don't know what's going on. Speaker Johnson and a collection of other Republicans in Congress have
called upon President Biden to send the National Guard into the colleges where the demonstrations
are. If only there was something in American history that might be able to inform our
opinion of this suggestion may be something that occurred sometime between 1969 and 1971,
and it's not 1969 or 1971, it's 1970, May 4th, 1970 in Ohio.
If there was such an event, it might suggest that this is a mistake, that that would be
a really bad idea.
Now that that's out of the way, let's move on to the next part.
If that mistake is to be made, it's to come at the request of the governor, not the president.
It's the governor's responsibility to make that request.
Speaker.
Now the White House was asked about this, and they said, that's the governor's responsibility.
In a weird way, they also did not confirm whether or not they would sign off on it,
which is odd.
Signing off on a request like that is very much a rubber stamp, just historically speaking.
It was a weird statement.
It may not mean anything at all.
It was just odd.
Okay, so the short recap here is people in Congress are requesting the president to send
troops into colleges, and that's a bad idea, but more importantly, that is supposed to
be at the request of the governor, which you would think the Republicans who constantly
talk about states' rights would understand that part, but whatever.
Now the entertaining question, and it was basically, hey, I have a friend who's very
much in favor of these demonstrations, but they're outposting saying that the National
Guard should be sent in, why?
My guess is that your friend has read the manuals.
Generally speaking, if you have a demonstration and you use military force to put it down,
it spreads.
It spreads.
It's in all of the manuals.
Regardless of what you actually think about the demonstrations, whether you are for or
against them, understand the manuals that were developed over decades and millions of
dollars worth of research and all of that stuff, say that that's the wrong
move. If you would like to see it play out in recent memory, think about Trump's
mistakes up in the Pacific Northwest. That kind of response causes the
demonstration, the event, to spread. It doesn't put it down. Your friend is an
accelerationist and is basically hoping that they do it so the demonstration
spreads. I would remind everybody that generally speaking when that kind of
force is used to put down a demonstration people get hurt. It's
It's probably not something to wish for.
But yeah, the country's doing great.
Anyway, it's just a thought.
Y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}