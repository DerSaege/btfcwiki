---
title: Let's talk about this week in Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Kp8oAPTrv3I) |
| Published | 2024/04/30 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing the phases in the ongoing story of Trump's New York entanglement
- Transition from "tell them what you're going to tell them" to "tell them" phase
- Detailed explanation phase expected to continue for the next few weeks
- Anticipating dry testimony in the upcoming period
- Mention of the possibility of introducing more interesting elements if the audience loses interest
- Describing a structured approach to disseminating information
- Reflection on how Beau's videos could follow a similar storytelling strategy

### Quotes

1. "Tell them what you're gonna tell them, tell them why you told them."
2. "This is all details."
3. "Assuming they more or less stick to this, the next couple of weeks are probably going to be pretty dry testimony."
4. "If I was to use that strategy for providing information, I would probably start off each one of my videos with something like, 'well howdy there internet people, it's Beau again.'"
5. "Y'all have a good day."

### Oneliner

Beau analyzes the phases in Trump's New York entanglement story, expecting detailed testimony ahead, with a structured approach to disseminating information.

### Audience

Political observers

### On-the-ground actions from transcript

- Analyze and stay informed about ongoing political developments (implied)

### Whats missing in summary

Insight into the storytelling approach and potential shifts in narrative focus.

### Tags

#Trump #NewYork #Testimony #Storytelling #InformationDissemination #PoliticalAnalysis


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about what you can expect
when it comes to the next week,
and realistically even longer than that
in Trump's New York entanglement.
Because it does appear that they are
employing that strategy.
Tell them what you're gonna tell them,
tell them why you told them.
Up until now
it has been the tell them what you're going to tell them phase,
so they're setting out the story,
they're providing the information and saying
this is what you're going to hear
and this is
the baseline of the story
and it looks like that phase has ended.
So now, they're not in the
tell them what you're going to tell them phase,
they're in the tell them phase.
This is all details.
This is all details.
So it will probably be even more tiresome for the former
president, because there's not going to be a lot of
excitement here, I don't think.
Once this phase is over and the details are there, that
brings up the tell them what you told them phase.
And this is when you'll start to get more interesting testimony again.
And that's the closing part where they bring everything into focus.
So assuming they more or less stick to this, the next couple of weeks are probably going
to be pretty dry testimony.
Not a lot of exciting stuff there.
Now, sometimes this breaks and changes if the audience starts to get bored, then something
else that's more interesting gets brought back in.
But as long as the audience is paying attention, this is where all the details are.
And they will probably stick to that until just before the end of this, which is still
like a month away. So that's what's, that's what's going on. Now the last time
I mentioned this strategy of, you know, disseminating information, I got a
question that came in and it has been just bouncing around in my head ever
cents, it asked for an example of how that gets used. And when I say this I
there's going to be that moment for some people. If I was to use that strategy
for providing information, I would probably start off each one of my videos
with something like, well howdy there internet people, it's Bo again. Today we're
gonna talk about X, Y, and Z. Tell them what you're gonna tell them. Then provide
the bulk of the information, the details, and then then that's the tell them phase
and then tell them what you told them and use something interesting to bring
it into context and bring it all into focus and then you know that that would
that would be how my videos were structured if I ever decided to use that
strategy. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}