---
title: Let's talk about speculation, the ICC, and questions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=FgMM8bmeCSs) |
| Published | 2024/04/30 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Speculation and questions arise from recent information about potential ICC warrants against people in Netanyahu's administration.
- The effectiveness of international bodies like the ICJ and the UN in stopping conflicts is questioned.
- The ICC is seen as having more teeth compared to other international bodies.
- Speculation surrounds the possible issuance of ICC warrants, particularly against Israelis in Netanyahu's administration.
- The potential charges on the ICC warrants might involve interfering with humanitarian aid and a failure to exercise due care.
- The jurisdictional issues regarding Israel and the ICC are discussed.
- Hamas may also face warrants due to violations on a specific date.
- Concerns within Netanyahu's administration about the ICC warrants have led them to reach out to other countries.
- The limitations and role of international bodies like the ICC are explained as instruments to maintain the status quo rather than implement change.
- Beau urges to let the ICC do their job and stresses the importance of understanding the limitations of international bodies.

### Quotes

- "The ICC can absolutely do what you have wanted the other bodies to do."
- "These international bodies, they are not vehicles for change. They are instruments to maintain the status quo."
- "They know best how to use those teeth."
- "You are finally at one that has some real teeth."
- "Let them do their jobs."

### Oneliner

Beau explains the speculation surrounding potential ICC warrants, the limitations of international bodies, and the importance of letting the ICC do its job.

### Audience

Advocates for international justice

### On-the-ground actions from transcript

- Support organizations advocating for international justice (suggested)
- Stay informed about developments related to international bodies and their actions (implied)

### Whats missing in summary

Insights on the potential impact of speculated ICC warrants on peace negotiations and international dynamics.

### Tags

#ICC #InternationalJustice #Speculation #Netanyahu #Hamas


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about questions,
speculation, wishes, and warrants.
Whole bunch of information has came out.
That information has led to speculation
and that speculation has led to questions.
So we are going to go through
just a brief overview of the speculation.
Then we will talk about the questions.
We are going to go over some that I have received
And I'm going to try to anticipate some questions that
are going to come in so we don't have a repeat of what
occurred when we were talking about the ICJ and the UN
and all of that stuff where the questions just kept coming in.
OK, so if you missed it and haven't
heard what's going on or the speculation
about what's going on, there is widespread reporting
that includes the idea that the ICC is close to issuing warrants against people in Netanyahu's
administration. That has prompted a whole bunch of questions. We're going to start off with three
real quick questions from the same person. If you are this person, understand I am not picking on
you, you reply to your own email so these were all very easy to find. A whole bunch of people
do this and a whole bunch of people... I understand that my inbox has turned into a place for people
to vent and that's totally okay. Yours were just easy to find. Okay, a few months ago,
stop telling people the ICJ won't matter. They can issue an order to compel the fighting to stop.
I think at this point, most people would agree that the ICJ is in fact on par
with a Twitter poll as far as its effectiveness at stopping conflict. Okay,
after that there was, don't you dare say the UN doesn't have teeth, here's a small
list of things UN troops have done over the years in case you don't know Iraq
who has a list. Okay, show and tell time. What's this? What is this? It is a UN
helmet cover. It's a UN helmet cover. Why does it exist? It does exactly what it
sounds like it does. It covers a helmet and identifies the wearer as a
participant in a UN operation. Why does it exist? Why does it need to exist?
because the UN doesn't have troops. The UN doesn't have troops. Your list, I
didn't go through the whole thing and check line by line, but another way of
saying that would be things that the United States and partner countries have
done while wearing these. Well, I mean not this one in particular, I'm pretty sure
This is a British helmet cover.
The UN, if you want to use the teeth analogy, the UN doesn't have teeth.
It has dentures that it borrows from other countries.
Other countries were not going to be willing to commit their troops to enforce a ceasefire
that neither side at the time had agreed to.
You're having a hard time finding countries to commit their troops to a peace treaty that
they might agree to.
It just wasn't in the cards because the UN doesn't have the teeth without the backing
of the countries willing to commit the troops.
Okay, you've said the ICC has teeth and if you're wondering where this was going, if
If you are one of the people who has had to hear people like me say, yeah, this is not
the international body you're looking for for the last six months, you're finally at
the right spot.
The ICC can absolutely do what you have wanted the other bodies to do.
You said the ICC has teeth.
Why aren't you talking about the ICC warrants?
You have a moral obligation to use your platform to advocate not only for them, but to ensure
U.S. officials are on the warrants too. Okay, so I have talked about the
possibility of ICC warrants before. That's where you got the the phrase the
ICC has teeth. Haven't talked about this speculation before because again it is
speculation. The warrants as far as we know at time of filming do not actually
exist. Not yet. And they may not be issued. It's speculation right now.
Obligation to use your platform. I think you greatly overestimate my influence.
Okay, now this is another question from a different person now. Why are the warrants only
against Israelis? Why not Americans? Okay, again, speculation. Warrants don't exist yet.
yet. But odds are when they come out they will only be against people in Netanyahu's
administration. There will not be any Americans. I would be really surprised if
there were Americans. The answer to this question, there's a bunch of different
answers. I'm gonna start with the simple snarky one because the ICC knows better
how to prosecute an ICC case than Twitter. That's your short answer and
that would answer most of the other questions. A longer answer is that they
want it to stick. The ICC has teeth. Going up against the United States, well
that's something different. What country would enforce it? What country is going
to enforce that warrant against a nation that proposed something that was known
as the Invade the Hague Act. If Americans are on the warrants, you might as well
just throw them away. All of them. It would end up nullifying all of it. They
wouldn't be worth the paper they're printed on. So they are probably not
going to include Americans. And then you have the you have the last reason and
this is going to be the one that bothers people. People are asking you know are
Americans going to be on the warrants or they're saying Americans should be on
the warrants or why aren't they all of that. All of that is based on an
assumption that is probably not true. You are assuming you know what the charge
is going to be because the people who are very excited about this, they are
expecting one specific charge and the US officials, they're complicit in that.
Odds are that is not going to be the charge. That's not what's going to be on
the warrants, if they are issued, I would be absolutely shocked if that's what was
the charge. The charges would most likely be interfering with international
humanitarian aid and a failure to exercise due care. That's hitting
protected buildings, not taking care around civilians. That's what it would be
about. If charges come forward, that's likely what the charges will be. And the
answer for that is that they, again, it's what they can prove. That's what
it's about. You are, if you're one of the people who has been desperately hoping
for an international body to get involved, you're finally at the right
spot. Let them do their jobs. You don't want to turn this into one of those
foreign policy lessons on unintended consequences where a bunch of pressure
gets applied, something gets done, and it just it doesn't go the way people
imagine. Okay now we're going to go on to some questions that I anticipate. Israel
Israel isn't a signatory, no jurisdiction.
Well, like I said, I didn't phrase them as questions.
Okay, so there is definitely going to be talk about the fact that Israel didn't sign in
to be under the jurisdiction of the ICC, so they have no jurisdiction.
I mean, sure, that argument can be made.
It has been made in the past.
It won't work.
In 2015, I believe, the court recognized the state of Israel, or the state of Palestine.
And then in 2021, it determined, if this is important to you, check the years.
In 2021, they determined that they did in fact have jurisdiction over that.
So the jurisdictional issues aren't really going to matter because the ICC will in fact
rule in favor of the ICC and say that they have it.
Okay, I'll turn this one into a question. Why was Hamas charged? Right now people
are hearing warrants and because a lot of this speculation came from within
Netanyahu's own government, that's what people are focused on. Odds are there
will be warrants dealing with Hamas as well. Why? The answer is that while there
might be a right to resist or something like that there are still rules. Those
rules were most likely violated on the 7th and realistically you don't have to
say most likely here they admitted that they violated them. Why did they admit it?
because it doesn't matter. What's the ICC going to do? Sanction them, not let them leave the place where they're at,
sanction an invasion. There's nothing the ICC can do, so there wasn't any reason for them to be like,
oh no, we didn't do that. So I would be very, very surprised if there wasn't also a set of warrants
that dealt with Hamas unless of course those that would have been named are no
longer with us. Okay and I would imagine that this has already started. So Netanyahu's
administration got a little bit concerned about this. They have certainly reached
out to different countries to include the United States asking them to you
know, step in and help. The US doesn't have a lot of sway in what the ICC
determines, okay? They can try but there's not much there. And then because of that
there's going to be talking points. The one that I can think of that is almost
certainly going to be used is this is going to disrupt the peace negotiations.
You got to stop. There were moments when stuff was going on at the UN where that
was a legitimate concern. Do I believe that's a legitimate concern with this?
No, not at all. Not in any way shape or form. Sure, it might be, it might give the
Palestinian side a little bit of a morale boost, but if anything I think it
would be an encouraging sign for a group that recently suggested they might be willing to
lay down their arms to see the ICC operating equally.
I mean, yeah, they're not going to be happy about, you know, warrants being issued for
their members, but if it's applied equally, I think it might actually encourage them to
participate more more openly. I do not see this realistically impacting peace
negotiations. I mean sure from the the side of the Netanyahu government, I mean
it might impact it in the way that they may be distracted by other things that
are going on but there are other people that can fill their spots. If you're
actually talking about the dynamics of peace negotiations, I don't see it on this one.
And this is coming from somebody who in the past has been like, no, they really couldn't
do this at the UN because that would mess this up, and that's not really a play here.
Okay, so those are the questions that came in, and those that I could anticipate there
may be more.
And I know that some of those answers, for those people that have been waiting for this,
waiting for the ICC, waiting for some kind of international body to jump in and actually
do something that matters, you're going to say that this isn't fair, it's not justice,
something along those lines.
I mean, yeah, yeah, I get what you're saying, but it's foreign policy.
at everything that's been happening and tell me that you have been given some kind of sign
that you believe any of it's fair.
It's just not what it is.
One of the things that people, especially Americans, have an issue with when it comes
to these international bodies is that they believe the propaganda about those international
bodies.
These international bodies, they are not vehicles for change.
They are instruments to maintain the status quo.
They're not good at stopping conflict, but they can normalize the agreement afterward.
The new status quo that is reached, they can do that.
A lot of people look to the international bodies to implement change.
That's not what they do.
That's not really what they're about.
They're about maintaining a status quo.
They're about maintaining an order.
Their goal is to stop the war, stop a conflict.
That's why most of them occurred, like as far as the UN, the idea was to stop it before
it starts, not stop it once it does.
That's not really, it's not really good at that.
It doesn't have a strong history of being able to do that.
So these are the best answers I can give right now.
Because again, all of this is based on speculation.
You don't know that any of this is true yet.
You know that Netanyahu's administration got concerned, and that's really it.
ICC, they're a lot like Smith, the special counsel's office. They don't talk
a whole lot. Basically they issue one warning and then they investigate and
that's it. So we'll wait and see what happens, but the questions that came in
those are the answers, those are some additional questions that I was able to
anticipate. Just remember for all of the people that for months have been looking
at different international bodies and getting mad at me because they don't do
what you thought they did and hoping that they would be able to influence the
outcome, you are finally at one that has some real teeth. But there are
limitations, let them do their job. They know best how to use those teeth. Anyway,
It's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}