---
title: Let's talk about proxies, deniability, and foreign policy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=JUlUZ2IxNoQ) |
| Published | 2024/04/14 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing a question about the difference in standards between two countries in foreign policy.
- Explaining the significance of Iran launching missiles and drones from its own territory.
- Noting the surprise among experts at Iran's decision to launch from inside its own territory.
- Comparing Iran's lack of deniability due to technology with Israel's use of deniability.
- Emphasizing the importance of maintaining deniability in international conflicts.
- Mentioning Iran's efforts to prevent the situation from escalating into a regional conflict.
- Pointing out the message behind Iran's decision to launch from its own territory.
- Stating the importance of deniability in international conflicts, even when actions are known.
- Using a poker game analogy to illustrate the concept of maintaining deniability.
- Concluding that the key factor is maintaining the illusion of civility in conflicts.

### Quotes

1. "It's all about that poker table and maintaining the illusion of civility as stuff falls out of the sky."
2. "Everybody knows what happened. But the deniability is important and Iran didn't maintain it here."
3. "They were both expected to maintain deniability, to maintain the charade."
4. "We know it was Israel. Israel knows that we know it was them. But they have deniability."
5. "That very much seems like, okay, I've had enough type of thing."

### Oneliner

Beau addresses the foreign policy question, explaining the significance of Iran launching from its territory while discussing maintaining deniability in international conflicts like a poker game.

### Audience

Policy analysts, international relations experts

### On-the-ground actions from transcript

- Pay attention to international conflicts and the tactics used by different countries (implied)

### Whats missing in summary

In-depth analysis of the current geopolitical situation and its implications.

### Tags

#ForeignPolicy #Deniability #InternationalRelations #Iran #Israel


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about a question that came in
at least 50 times last night.
Some variation, all asking the same thing, looking at
something that seems as though two countries are being held
to very different standards when it comes to something.
So we will talk about foreign policy.
we will talk about secrecy and deniability.
I'm going to use this question because it kind of lays the scene and tells everything.
Hey Bo, can you explain why Iran launching missiles and drones from its own territory
is worse for this situation than if it had used its proxies? I have now heard several
experts say the same thing, but I don't understand why. Didn't Iran have to respond
to Israel's strike on Iran's diplomatic-adjacent building in Syria. Don't they have to do a showy
response of some kind, lest they anger their populist military allies and proxies into further
uncoordinated responses? I know this may seem obvious, but I can't see how it makes any
difference in this particular situation from which territory Iran attacks. If you don't know,
So all experts were really surprised that Iran launched from inside its own territory.
That was a surprising move and there was a lot of commentary about it.
Another comment under that video said something that even now I don't know if it was a next-level
joke by an F-35 pilot or it was somebody who just didn't understand the significance of what they
said. And it said Iran is expected to maintain deniability, meanwhile Israel used an F-35.
Something along those lines. Yeah, I mean that's probably what happened. That's a decent read on
what occurred. Iran doesn't have F-35s, doesn't have that technology. Israel, up
to time of filming, has not admitted that they were responsible. They haven't
confirmed it, they haven't denied it. The closest you get is them saying that
wasn't a diplomatic building, it was a military facility, but they never admitted
that they did it. They have deniability. Iran doesn't have that technology. They
don't have the ability to just say, oh, that wasn't us, and use their planes. So
they use their proxies. But it works the same way. I mean, I don't even think the
media is saying a suspected Israeli strike at this point. I think they're
just saying Israeli strike. We know it was Israel. Israel knows that we know it
was them. But they have deniability. If Iran had used its proxies, everybody
would have known it was Iran. And Iran would have known that everybody knew. But
they would have had deniability. As it stands now, because they launched from
inside their own territory and it got picked up on radar super quick. Both countries are quote
known to have committed an act of war against each other, but only one can be proven. That's why it's
a surprise. So it's not actually that they're being held to different standards. Both have to
maintain deniability or are expected to.
When it comes to Iran, they have done their part to stop this from turning into a regional
conflict in a number of ways.
My best read on this is this is them kind of saying, we're kind of done with this.
We aren't without, you know, our own abilities here.
It's probably the fact that they launched from inside their own territory is probably
something that every country in the Middle East and the West should pay attention to.
That very much seems like, okay, I've had enough type of thing.
But they're not actually held to different standards.
They were both expected to maintain deniability, to maintain the charade.
Everybody knows what happened.
But at that international poker game where everybody's cheating, you have to pretend
like it wasn't you.
You have to sit there and be like, oh, we're all looking for the guy who did this.
Nobody knows what actually happened though.
But the deniability is important and Iran didn't maintain it here.
And I think that's, in some ways, that's a bigger signal than the drones themselves.
And it's one that should probably be paid attention to.
So yeah, that's what it boils down to.
It's all about that poker table and maintaining the illusion of civility as stuff falls out
of the sky.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}