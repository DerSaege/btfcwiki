---
title: Roads Not Taken EP 34
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=FCFCakkM8Y8) |
| Published | 2024/04/14 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Overview of recent global events, including a former U.S. Ambassador's sentencing for espionage and Russian advisors in Niger.
- Updates on Iran's response to Israel, U.S. transferring weapons to Ukraine, and internal Israeli politics.
- U.S. news covers Trump's upcoming trial, Biden's student loan forgiveness, and Forbes' prediction on Trump media stock.
- Concerns about potential Moscow-style incidents in the U.S., election fraud cases, and progress in the restoration of Notre Dame Cathedral.
- Science news includes AI-piloted military aircraft and national limits set for harmful chemicals in drinking water.
- Oddities like Ukraine using caltrops via drones, and conflicts within political campaigns.
- Space Force members attending Ranger School, blending military and space training.
- Beau responds to viewer questions about his work schedule, political strategies, and previous predictions on climate change framing.
- Clarifications regarding Trump's status as commander in chief and civilian control of the military.

### Quotes

- "Biden has launched another batch of student loan forgiveness, impacting about a quarter million people and forgiving billions."
- "Sell Trump media stock now, implosion likely."
- "If your goal is to do something about climate change, framing it as infrastructure and jobs is key."
- "Space Force and went to Ranger School. He's a space ranger."
- "Trump was commander in chief, so how is it that none of his trials are court marshals?"

### Oneliner

Beau covers global events, U.S. news, cultural updates, and viewer questions, including insights on political strategies and climate change framing.

### Audience

Global citizens

### On-the-ground actions from transcript

- Contact local representatives to advocate for stricter regulations on harmful chemicals in drinking water (implied).
- Join organizations promoting transparency in political campaigns and election processes (implied).
- Organize community events to raise awareness about election integrity and combat misinformation (implied).

### Whats missing in summary

Insights on the dynamics of global politics, U.S. news, and the intersection of military and civilian control.

### Tags

#GlobalEvents #USNews #PoliticalStrategies #ClimateChange #ElectionIntegrity


## Transcript
Well, howdy there, internet people.
It's Beau again, and welcome to the Roads with Beau.
Today is April 14th, 2024,
and this is episode 34 of The Road's Not Taken,
which is a weekly series
where we go through the previous week's events
and talk about news that was unreported, underreported,
didn't get the coverage it deserved,
or I just found it interesting.
And then we normally go through a few questions from y'all.
Okay, starting off in foreign policy, the former U.S. Ambassador Rocha was sentenced
to 15 years for his part in what is described as the longest-running infiltration of the
United States government, working for Cuba.
Russian advisors have arrived in Niger.
US appears to have lost a key partner in the region. Iran launched its response to Israel's
strike on a diplomatic facility last night. If you missed the news, there's a video about it over on
the main channel, and there's more coming out today. RumorMill says that Australia is seriously
considering the recognition of a Palestinian state.
The U.S. is transferring tons, literal tons, of weapons and ammo seized from Iran to Ukraine.
The former boss of Shin Bet has called for Netanyahu to step down, saying that he was
leading the country to, quote, doom.
On to U.S. news.
Without a major development today, tomorrow will begin the first criminal trial of Trump,
and jury selection is up first.
Biden has launched another batch of student loan forgiveness, and it's impacting about
a quarter million people and forgives billions.
I want to say around seven, but fact-check that.
This is separate from the Plan B,
which is the attempt to do a very widespread thing.
This part is the, you know, a few billion here
and a few billion there.
Forbes ran a headline saying,
sell Trump media stock now, implosion likely.
I'm sure that's not going to go over well with Trump.
The boss of the FBI is concerned about a Moscow-style incident in the U.S.
The use of the word coordinated raised eyebrows when he was talking about it, because that
term in this context generally means with foreign direction, and it's not a term that
it's dropped very often.
So that's something we'll be following.
In cultural news, a search of the right-wing Heritage Foundation database, it's on confirmed
fraud cases when it comes to elections.
It lists less than 100 examples of non-citizens voting in 2022—oh, I'm sorry, that's
less than 100 examples of non-citizens voting from 2002 to 2022.
Trump's lying to you again.
This is not a widespread issue.
The Notre Dame Cathedral is nearing completion when it comes to the restoration that was
undertaken after the fire.
OJ died in science news.
The Secretary of the Air Force has indicated they're going to fly on an AI-piloted F-16
basically to just demonstrate faith in the system.
The Biden administration set national limits for the first time on forever chemicals in
drinking water.
Looks like Norfolk Southern is going to end up paying around $600 million in a settlement
over the train derailment in Ohio.
In oddities, Ukraine is apparently deploying caltrops from drones to disable Russian wheel
vehicles trying to think of something too in the movie heat at the very
beginning the stuff they drug across the road those they have been used for a
very long time in in conflicts but we haven't seen them recently RFK is RFK
Jr. is having some major issues with his campaign after word leaked that some of his supporters
do not actually want him to win, but really just kind of want to drain votes from Biden.
That story, there's a lot of speculation and he said she said stuff in it, but it's
not a good look, regardless of what eventually gets confirmed.
So there are articles being run in newspapers that serve the military.
It appears that there is a push to get people who are in Space Force in the United States
to attend Ranger School because one of the people in Space Force actually went through
and did this back in October and graduated.
Space Force and went to Ranger School.
He's a space ranger.
If y'all are calling him Buzz Lightyear, I don't know.
Okay, moving on.
Now we are to question and answer.
My wife and I have been watching you for years and started talking about how you put out
a video normally multiple every single day without fail.
I know you record some ahead of time, but when was the last time you actually had a
day off where you didn't have a video published?
It was February or March of 2020.
I got really sick, like really sick.
In fact, it might have been what was going around
at the time, I'm not sure.
Okay, do you think you could be underestimating Dems
in Arizona when it comes to throwing their weight
behind repealing the 1864 law?
Do you think it's possible they knew the GOP
were going to shoot it down and pushed it
to let them show people in Arizona
how much they hate freedom and women.
Is it possible?
Yeah.
Yeah, it's possible.
That's not something I would say because, A, I can't demonstrate that.
They would have to know how the Republican Party was going to vote ahead of time and
that seemed pretty chaotic.
The other reason is this is assuming that the Democratic party is playing 4D chess.
It's not out of the realm of possibility because if they did know how the vote was
going to go, they might have made that decision.
But I don't like to assume that politicians know what they're doing, that they are playing
4D chess.
That's not, that's normally not a safe assumption.
Is it possible though, yeah, yeah.
I found a Bostrodamus video that doesn't have to do with war.
It's called, let's talk about how we talk about climate change.
Okay now, for those who don't know, the whole Bostrodamus thing, it's a joke.
If you go back through pretty much any video where it appears like I predicted the future,
it just happens to be about a topic I know a whole lot about.
It's not magic.
Okay, it's called, let's talk about how we talk about climate change.
I vaguely remember that one.
Four or five years ago, you said exactly how climate transition would have to be framed
to be successful, and it's what Biden is doing, just wondering what your thoughts are.
If I remember, that was right before the Trump-induced recession was going to hit, and for those
Because I don't know, we were headed into a recession prior to the pandemic and then
the pandemic happened and it kind of, it covered up a lot of Trump's economic mistakes.
If I remember right, this video went out about that time and I was talking about how it was
a, when we came out of the recession, it would be a good point in time to start climate transition,
but you wouldn't frame it as climate transition, you would frame it as infrastructure, jobs,
you know, stuff like that.
And now that's what Biden is doing.
I'm just going to say if your goal is to do something about climate change, it's a conclusion
you would come to.
It's like the fact that it's also a national security issue.
So framing a lot of it around national security is going to be helpful and it's definitely
something that's going to occur.
And I don't really see that as something that would qualify under that.
Trump was commander in chief, so how is it that none of his trial are court marshals?
So commander in chief is not a rank.
It's one of the big things in the military and one of the things that Trump actually
Anyway, civilian control of the military.
As President, Trump is a civilian.
He is a civilian.
It's a big thing in the US to have civilian control of the military.
So that's why he wouldn't come under that.
He wouldn't come under UCMJ or anything like that.
that looks like it. Okay, so there you go, a little bit more
information, a little more context and having the right
information will make all the difference. Y'all have a good die.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}