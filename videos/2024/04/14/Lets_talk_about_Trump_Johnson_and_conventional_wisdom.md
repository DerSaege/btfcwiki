---
title: Let's talk about Trump, Johnson, and conventional wisdom....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=NyFHPCCBXvY) |
| Published | 2024/04/14 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Current Speaker of the House, Johnson, met with Trump in a perfect PR move, showing unity and support.
- Conventional wisdom suggests Johnson did this to protect his speakership and gain Trump's support.
- Johnson already had assurances from the Democratic Party to protect his speakership, making Trump's support unnecessary.
- Initially seen as the Twitter faction speaker, Johnson has now shifted to a more independent and leadership role.
- Johnson may be emulating McConnell and positioning himself as the de facto leader of the Republican Party if Trump's influence wanes.
- Being opposed to Trump in some ways doesn't automatically make Johnson an ally to the majority.
- There may be more to Johnson's actions than just safeguarding his speakership, hinting at a broader political strategy.
- Johnson's actions coincided with the House's defiance towards Trump's requests, indicating strategic political moves.
- While protecting his speakership may be the primary reason, there could be deeper motives behind Johnson's actions.
- Johnson's handling of the Twitter faction suggests a calculated and politically savvy approach.

### Quotes

1. "Conventional wisdom on this is that Johnson did this to protect his speakership."
2. "He didn't need Trump."
3. "He's the one holding the leash."
4. "He might be trying to become McConnell."
5. "Just because something is most likely doesn't mean it's the only option."

### Oneliner

Current Speaker of the House, Johnson's strategic moves with Trump hint at deeper political ambitions beyond safeguarding his speakership.

### Audience

Political observers, Republican Party members.

### On-the-ground actions from transcript

- Analyze and stay informed about political maneuvers within the Republican Party (suggested).
- Engage in constructive political discourse to understand the motivations behind politicians' actions (implied).

### Whats missing in summary

Deeper insights into the potential long-term implications of Johnson's political strategy.

### Tags

#Politics #RepublicanParty #SpeakerJohnson #Trump #McConnell


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about Trump
and Johnson and conventional wisdom
about their little get-together and then we're going
to remind everybody about some conventional wisdom
of the past and maybe see if there's some parallels.
Okay, so if you don't know what happened,
the current Speaker of the House, Johnson,
met with Trump and they did the perfect PR thing, you know, they held hands, sang kumbaya,
we support each other, we love each other, Johnson's a good speaker, all of this stuff.
The conventional wisdom on this is that Johnson did this to protect his speakership, to get
Trump's support to make sure that he wouldn't run afoul of a motion to vacate.
There's nothing wrong with that read.
That very well could be what's happening.
But he didn't need Trump.
He already had assurances from the Democratic Party that as long as he did the right thing,
that his speakership was protected.
He'd be all right.
So he didn't actually need to do this.
Remember when he became speaker?
Conventional wisdom was that he was going to be the Twitter faction speaker because
they owned him.
Because they vacated McCarthy.
Well Johnson, he was their guy.
And he did all the right things.
He stood with them for a little bit, held their hand, sang kumbaya, but at this point
in time, it doesn't really seem like he is the Twitter faction's attack dog.
It seems like he's the one holding the leash.
He's brought them to Hill, not the other way around.
He didn't need Trump.
He didn't need to go hold hands and sing kumbaya.
I think it might be wise to view Johnson as somebody who really studied McConnell and
might be trying to become McConnell.
There is a non-zero chance that the read on this is very, very wrong, and that Johnson
is positioning himself to be the de facto leader of the Republican Party if, I don't
know, somehow Trump lost his influence.
It is worth remembering that just like McConnell, just because he's opposed to Trump in some
ways, doesn't make him your friend.
Not for the majority of people watching this channel.
But he very well may have his own agenda.
I would not view him as just somebody who is going to do whatever Trump says.
I think there might be another motive at play, and I think he might be a little bit more
politically savvy than anybody thinks he is, and he's probably using that to his advantage
right now.
know, won't take too long, but it's interesting that this occurred right
around the time that the House was like, oh you really want us to do this Trump?
No. Conventional wisdom is generally seen as the thing that's most likely, and
And yeah, maybe the most likely option here is that Johnson wanted to hedge his bets
to keep his speakership.
It could be that simple.
But just because something is most likely doesn't mean it's the only option.
And with the way that he's handled the Twitter faction, I just, I think there might be more
to this.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}