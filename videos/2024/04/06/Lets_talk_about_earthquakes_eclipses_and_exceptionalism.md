---
title: Let's talk about earthquakes, eclipses, and exceptionalism....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=C065eD4jstc) |
| Published | 2024/04/06 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the recent earthquake in New York leading to various right-wing figures suggesting it as a sign from God.
- Points out the common occurrence of earthquakes before eclipses and the fallacy in connecting them as divine signs.
- Mentions the high frequency of earthquakes globally, around 20,000 per year with an average of 55 per day.
- Notes that even moderate earthquakes, starting at 5.0, happen frequently, around 1000 per year.
- Attributes the phenomenon of connecting natural events to political beliefs to American exceptionalism.
- Criticizes the use of people's religious beliefs to manipulate them for political gain.
- Advocates for individuals to not allow politicians to use their beliefs to manipulate or scare them.
- Emphasizes the importance of distinguishing between personal beliefs and attempts to manipulate those beliefs for political purposes.

### Quotes

- "American exceptionalism leads people to the belief that everything has to do with the United States."
- "Don't allow politicians to use those beliefs to manipulate you, to scare you."
- "Science doesn't back that up."
- "The USA is a country, it's a geographic area on the planet."
- "Not everything that occurs in the world is related to the United States."

### Oneliner

Beau deconstructs the fallacy of connecting natural events to political beliefs rooted in American exceptionalism, cautioning against manipulation through religious beliefs.

### Audience

Critical Thinkers

### On-the-ground actions from transcript

- Question the narratives pushed by politicians and media, especially when they attempt to link natural events to political agendas (implied).
- Educate others about the fallacy of connecting unrelated events as divine signs for political gain (implied).

### Whats missing in summary

Full understanding of Beau's insights and analysis on the manipulation of beliefs for political purposes.

### Tags

#Earthquakes #Eclipses #Politics #AmericanExceptionalism #Manipulation


## Transcript
Well howdy there internet people, it's Bo again.
So today we are going to talk about earthquakes and eclipses
and beliefs and politics
and science and averages.
We're just going to run through some things because
something that occurs in the United States, a whole lot, is occurring and it's
worth just kind of going over.
If you missed the news,
there was an earthquake
it impacted New York. That led to people on the right, Republicans, politicians,
commentators, putting out a whole bunch of commentary like this. God is sending
America strong signs to tell us to repent. Earthquakes and eclipses and many
more things to come. I pray that our country listens. That is apparently from
Marjorie Taylor Greene. You also have another version of this that is
basically it's coming from a whole bunch of different places and it's phrased
differently each time but the general tone is hey in Revelations there was an
earthquake before an eclipse. So obviously trying to draw that connection so here's
the thing there's pretty much always an earthquake before an eclipse like always
there's normally an earthquake while the eclipse is you know on the day of the
eclipse. Why? Because there's about 20,000 earthquakes per year, which the average
I want to say is 55 per day. So, yeah, that stands to reason. Now, even if you
want to say, well, no, that's not what we're talking about. A lot of those
earthquakes are very small. We're talking about, you know, the bigger ones. Okay,
let's just go to moderate. Let's go to moderate earthquakes and just count
those. The thing is the earthquake that they're pointing to doesn't qualify
because it's a 4.8, moderate starts at 5.0, but let's just give it to them.
There's still about a thousand a year. So with the idea that this is
incredibly common, that this is very normal, why is this taking hold? Why are
are some people, I'm sure, are earnest in their belief,
and there are probably some people
who are using other people's beliefs to manipulate them.
Why is it easy to do this?
American exceptionalism.
That's what it's about.
Contrary to popular belief, scientific evidence
does not prove that the world revolves
around the United States.
This is why it is easy for politicians
convince people that every event in the world is somehow related to the
ridiculous partisan divide in the United States. American exceptionalism leads
people to the belief that everything has to do with the United States and this is
ingrained in people from the time they're little. They're told that you
know USA number one all of that stuff. The USA is a country, it's a geographic area
on the planet. Not everything that occurs in the world is related to the
United States and certainly not by design to influence American politics
and give your chosen party an edge. But because American exceptionalism is so
strong, bad actors can absolutely use people's earnest religious beliefs to
manipulate them. To convince them that they're on the right side and that God
is on their side. That's the reality. People have their beliefs. It's important
to remember that whatever your beliefs are, they're your beliefs. Don't allow
politicians to use those beliefs to manipulate you, to scare you, to take
something that is totally mundane, that occurs all the time, and turn it into
something that leads you to believe literally that the world is going to
end. Science doesn't back that up. Anyway, it's just a thought. Y'all have a good
day.
Thanks for watching, I hope you enjoyed the video.
If you did hit that thumbs up button, it helps me to make good content for you, other then
you
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}