---
title: Let's talk about Trump's Truth Social trouble....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=AL6ZRXd83lg) |
| Published | 2024/04/06 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Two brothers associated with Trump's media company pleaded guilty to insider trading.
- They used confidential information about a merger to make illegal trades.
- The brothers admitted to profiting more than $20 million through these trades.
- This adds to the challenges facing Truth Social, Trump's media company.
- There is no evidence linking Trump to the insider trading incident.
- Despite ongoing issues, some investors may still be drawn to the Trump brand.
- The stock performance of Truth Social has been rocky, but it could potentially improve.
- Future news stories may further impact the company.
- The reliance on the "Trump brand" could influence investor decisions.
- The stock market is unpredictable, and situations can change rapidly.

### Quotes

1. "Two brothers associated with Trump's media company pleaded guilty to insider trading."
2. "The stock performance of Truth Social has been rocky, but it could potentially improve."
3. "Despite ongoing issues, some investors may still be drawn to the Trump brand."

### Oneliner

Two brothers plead guilty to insider trading in relation to Trump's media company, adding challenges to Truth Social, with no direct evidence implicating Trump.

### Audience

Investors, Media Consumers

### On-the-ground actions from transcript

- Monitor news related to Trump's media company and Truth Social (implied).
- Stay informed about developments in the stock market that may impact investments (implied).

### Whats missing in summary

Analysis of potential implications for investors and the future of Trump's media company.

### Tags

#Trump #MediaCompany #InsiderTrading #TruthSocial #Investors


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about some news coming out of New York, and we will go inside
Trump's media company a little bit, talk about all of that, and then answer the real big
question that has come in the most since this news started to come out.
And it's a surprising answer for a lot of people, I guess.
So we'll talk about that and two brothers.
Okay, so if you have no idea what's going on because this news has definitely been overshadowed
by some other things, two brothers who kind of helped fund this whole endeavor when it
comes to Trump's media company and all of that, they have entered guilty pleas when
it comes to what amounts to insider trading.
I'm sure there's a specific charge, if you care, look it up.
And what the statement says is that the brothers, quote, admitted in court that they received
confidential inside information about an upcoming merger between DEWACC and Trump Media and
used that information to make profitable but illegal open market trades.
So there's that.
My understanding is that this goes to the tune of more than $20 million or so, and it's
definitely one more thing when it comes to the bumpy start of truth social as far as
it going public.
The big question that has repeatedly come in since this news came out is what did Trump
have to do with it. There is no evidence that has been talked about, that I've
seen, anything that says Trump was actually involved in this or even aware
of it. And I mean that is the big question that most people have had, you
know, is this something linked to Trump? Based on everything that's public, no.
This is an entirely separate thing. It doesn't look like he was involved at all
at this point in time. As far as truth social goes, this is probably going to
continue causing issues for it. There are other things that have started to
surface, dealing with alone and just all kinds of other stuff, that may mean that
But the incredibly bumpy start that that now public company has had is going to continue.
You don't know.
It's the stock market.
Things change pretty quickly.
But there are still other news stories out there that may surface in the future.
And some of them may end up causing more issues for the company.
But again, with the company relying on the quote Trump brand, whether you believe that's
valuable or not, there are people who believe that.
So they might continue to invest in it.
at some point in time, I mean, it's possible that the current slide of the stock reverses.
It has not done well lately.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}