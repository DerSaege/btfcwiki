---
title: The Roads to Viewing an Eclipse
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=oFIYz5zdf_o) |
| Published | 2024/04/06 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Solar eclipses have mesmerized humans for ages, evident from ancient structures made for observing the sky by our ancestors.
- These celestial events were significant historically, symbolizing favor with rulers or the end of dynasties.
- Eclipses are not mystical; they are simply one astronomical body passing in front of another.
- A solar eclipse occurs when the moon blocks the sun, while a lunar eclipse happens when the Earth obstructs the moon's light.
- About five solar eclipses are visible annually on Earth, with most being unnoticed due to the Earth's water-covered surface.
- Total solar eclipses occur roughly every 18 months, but they are rare and reoccur in the same place only about every 400 years.
- Looking directly at the sun can cause permanent eye damage, so it's vital to observe safety precautions.
- To safely view a solar eclipse, inexpensive eclipse viewing glasses are recommended, but a DIY pinhole projector can be made using a shoebox.
- Materials needed for the pinhole projector include a shoebox, white paper, aluminum foil, tape, a paper clip, a needle or tack, knife, and scissors.
- Steps for creating the pinhole projector involve cutting holes in the shoebox, attaching aluminum foil, and positioning white paper for viewing the eclipse.
- The projector allows you to view the eclipse safely by standing with your back to the sun and looking through the viewing hole with your right eye.
- Beau advises caution when purchasing eclipse viewing glasses to avoid counterfeit products.
- He stresses the importance of accurate information and preparation for observing celestial events like solar eclipses.

### Quotes

1. "Solar eclipses have captivated humans probably since before there was a spoken language."
2. "Observing a solar eclipse is easy to do when you're prepared."
3. "Please remember that if you're going to get these glasses, make sure that they're not fake."
4. "A little bit more information, a little more context and having the right information will make all the difference."
5. "Enjoy the show."

### Oneliner

Beau introduces the history of solar eclipses, safety precautions, DIY projector creation, and the significance of accurate information in observing celestial events.

### Audience

Astronomy enthusiasts

### On-the-ground actions from transcript

- Make a DIY pinhole projector to safely view solar eclipses (suggested).
- Purchase reliable eclipse viewing glasses for safe observation (suggested).

### Whats missing in summary

The detailed steps and materials required to create a DIY pinhole projector are best understood by watching the full video tutorial.

### Tags

#SolarEclipse #Astronomy #DIYProjector #SafetyPrecautions #AncientStructures


## Transcript
Well, howdy there, Internet people, and welcome to the
Roads with Bo. Today, we're going to talk about the roads to
observing the solar eclipse. You know, solar eclipses have
captivated humans probably since before there was a spoken
language. We are steadily finding ancient structures
dedicated to tracking the sun and the moon built by our
ancestors. From the remains of our earliest civilizations, we
can see the importance placed on being able to watch the sky. It could gain you great
favor with a ruler or signify the end of a dynasty, but there is nothing magical about
them. An eclipse is just one astronomical body passing in front of another. A solar
eclipse is the moon passing in front of the sun, and a lunar eclipse is the earth blocking
the moon from the sun's light. There are, on average, five solar eclipses visible yearly
on Earth. Most of them go unnoticed as the majority of the Earth's surface is water and
there's nobody there to observe them. The majority are partials with a total solar eclipse
being observable about every 18 months or so, making total solar eclipses rare in comparison.
They become even more rare when you realize that they only reoccur in the same place about
every 400 years. You should never look directly into the sun. I feel like that should go without
saying, but if you do, it could cause permanent damage to your vision. However, observing
a solar eclipse is easy to do when you're prepared. You can find eclipse viewing glasses
online and they are fairly inexpensive. But if you don't have the extra money or shipping
will take too long, here's an easy project. You likely did this in grade school with a
cereal box. Today, we're going to make a pinhole projector out of a shoebox to view the upcoming
solar eclipse.
OK, so here's what you'll need.
You're going to need a shoe box, a sheet of white paper,
aluminum foil, tape, a paper clip, a needle or a tack,
knife, and scissors.
OK, so let's go over the steps.
First, remove the lid from the shoe box
and cut a small square hole in the bottom right-hand corner
of one end of the box.
Next, cut a piece of aluminum foil larger than the hole
and tape it into place, covering the hole.
Then poke a tiny hole in the aluminum foil
directly center of the hole it's covering
using the needle or the tack.
Fourth, cut your white paper down
to roughly the size of an index card
and secure it into place using paper clips
opposite of the hole in the foil.
Now you're going to cut a small square hole in the bottom left corner on the same end
of the box as you made the first hole.
This will be how you view the eclipse.
Okay and then you're going to replace the lid on the shoebox and tape it on to secure
it and keep out any extra light.
Okay and then to see if it works, you're going to stand with your back to the sun, look through
the bottom left viewing hole with your right eye, and align the sun's rays through the
pinhole in the foil. So finally, on the day of the eclipse, as the moon passes in front of the sun,
you'll be able to use this to safely view the eclipse.
Okay, so that's it. Quick, simple, easy. Please remember that if you're going to get these
glasses, make sure that they're not fake, because those are floating around.
So that's the roads to taking a look at the sun without hurting yourself.
A little bit more information, a little more context and having the right information will make all the difference.
You all have a good day, and enjoy the show.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}