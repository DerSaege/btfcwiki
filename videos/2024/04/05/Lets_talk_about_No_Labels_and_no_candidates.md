---
title: Let's talk about No Labels and no candidates....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=wCvmby1NDyo) |
| Published | 2024/04/05 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- No Labels, a centrist group, planned to run a Unity ticket for president and vice president, but ultimately decided not to run a candidate.
- The group was seen as center-right and approached candidates like Haley, Hogan, Manchin, and Chris Christie, who did not seem interested.
- Senator Lieberman's death had a big impact on the group's plans.
- No Labels had made moves to be on the ballot in around 20 states.
- The group realized they couldn't put together a viable ticket for the White House or secure a significant share of the vote.
- They will not be running a candidate but might remain active as commentators on other candidates.
- There are plans for post-election activities after 2024, but the strategy might need a change for success.
- Beau believes starting a third party at the highest level without a strong base of power won't work in the US system.
- Without building from the bottom up, any future attempts by No Labels might face the same fate.
- RFK Jr. may face pressure to drop out due to the two-party system's stronghold and concerns about splitting votes.

### Quotes

1. "They said they were going to run a centrist Unity ticket for president, vice president."
2. "They probably realized that they can't get a ticket together that had a viable pathway to getting a decent share of the vote."
3. "If you're not starting at the highest level, it's not going to work."
4. "My guess is there's enough concern from both parties that he will take their share of the vote."
5. "It's just a thought."

### Oneliner

No Labels' failed attempt at a centrist Unity ticket sheds light on the challenges of third parties in the entrenched US political system, leading to potential pressure on RFK Jr. to drop out.

### Audience

Political analysts

### On-the-ground actions from transcript

- Analyze and understand the challenges faced by third parties in the US political system (implied).

### Whats missing in summary

Insights on the potential influence of failed third-party attempts on future political strategies.

### Tags

#NoLabels #CentristPolitics #ThirdParties #USPolitics #RFKJr #PoliticalChallenges


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about no labels.
And apparently, no candidates either.
OK.
So if you aren't familiar with no labels
and what it was going to do this election season,
just catch up real quick.
They said that they were going to run a centrist Unity
ticket for president, vice president. They're going to try to take the White
House. They said they were centrist. They certainly appeared to be center
right to me. Some of the candidates that we believe they approached and in
some cases no, like Haley, Hogan, Manchin, Chris Christie, people like that. They did
not seem interested. Now, the group itself, it kind of took a big hit when Senator Lieberman
died. Very integral to what was going on. They had made some moves. I want to say they
got access in, I don't know, 20 states or so. They'd be on the ballot. But they have
now decided they will not run a candidate because they probably realized
that they can't get a ticket together that I mean forget about a viable
pathway to the White House. They probably couldn't get a ticket together
that had a viable pathway to getting a a decent share of the vote. So they've
basically called it they're not gonna run a ticket. If the reporting is
correct they're not going to do anything as far as running anybody. There is an indication that
they're still going to be active, but it almost sounds like they're going to be commentators
on what other candidates say. There are plans to continue post-election after 2024.
My guess is that if they pursue the same strategy and try to get a
ticket for the White House, it's not going to go anywhere. When it comes to
third parties, I'm pretty open about this, if you're not starting in the house at
the highest level, it's not going to work. You have to build the base of power
to get anywhere, even if you win, you have a Congress that is not aligned with
you. The US system is very entrenched. If you want a third party, you have to build
it from the bottom up. So unless their post-election plans include that, my
guess is the next election will work out the same way. So what does this mean
politically? It means, my guess is that RFK Jr., he's probably about to fill the
pressure from everywhere because it is a two-party system and it is very
entrenched, my guess is there's enough concern from both parties that he will
take their share of the vote or whatever that he's about to see a lot of
pressure for him to drop out. Other than that, there's probably not a lot of big
implications because they didn't get far enough in the process to really shake
anything loose. So I think that's going to be your biggest impact. Anyway, it's
It's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}