---
title: Let's talk about developments from Biden's call....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=pCa1pVQ8zbY) |
| Published | 2024/04/05 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden administration's increased anxiety and sharper rhetoric was noticeable around the time of the State of the Union.
- Requests for aid and a UN resolution recognizing a Palestinian state gave the Biden administration leverage.
- A video conference about RAFA was productive but required a follow-up meeting.
- Biden's call with Netanyahu was hoped to bring about change, especially with added pressure from the situation and World Central Kitchen.
- National Security Council spokesperson expressed confidence in seeing announcements of changes soon.
- Netanyahu and his cabinet agreed to open a gate in the north for aid access and give Jordan access to a port for food distribution.
- The US response emphasized the need for rapid implementation of the aid agreements.
- Biden's focus on RAFA offensive raises concerns about potential humanitarian crises.
- Delaying delivery of military aid aims to avoid a record of supporting actions deemed unwise.
- Biden's reluctance to push for an immediate permanent ceasefire is tied to aiming for a long-term peace solution.

### Quotes

- "The US response to this was basically great, but, well, it needs to, quote, must now be fully and rapidly implemented."
- "An offensive into RAFA, send hundreds of thousands of people fleeing into areas with no infrastructure."
- "Nothing about this is fair."
- "They're angling for a long-term solution, not just a permanent ceasefire that just reinforces the status quo."
- "If it was a permanent ceasefire it would be a peace with very very few exceptions."

### Oneliner

Biden's foreign policy navigation in the Middle East aims for long-term peace through strategic pressure and aid agreements.

### Audience

Foreign policy analysts

### On-the-ground actions from transcript

- Monitor the implementation of agreed aid access and food distribution in impacted areas (implied).
- Stay informed about developments in the Middle East to understand the implications of foreign policy decisions (implied).

### Whats missing in summary

Deeper insights into the nuances of foreign policy strategy and the potential impact on peace negotiations.

### Tags

#Biden #ForeignPolicy #MiddleEast #PeaceNegotiations #AidAgreements


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we're going to talk about Biden, that call,
and the things that have transpired since that call
because we already need to do an update on it
because there have been some developments
and we're going to talk about foreign policy.
So we're gonna start off with an incredibly quick recap
of the last three or four weeks.
We noticed around the time of the State of the Union
that the Biden administration was getting
a little bit more, well, let's just call it anxious
when it comes to the situation.
The rhetoric was getting a little bit sharper
and there was an indication that they were ready
to exert a little bit more pressure.
But at that time, we didn't really see it.
you saw the Biden administration get a little bit more leverage that it could actually use
without having to redesign the entire US presence in the Middle East.
Some of that came through requests for aid, and some of that came through the French throwing
up that resolution at the UN that included the recognition of a Palestinian state.
gave the Biden administration leverage, but didn't know if they were going to use it.
We saw more calls about RAFA, then you had the call, the video conference about RAFA.
The information came out, it was productive, constructive, but not so constructive and
productive that they didn't need a follow-up meeting.
We knew Biden's call, a call between Biden and Netanyahu, was going to occur.
And there was the hope that that was the moment that Biden was really going to say, this has
to change because of the added pressure on top of the famine, the added pressure
when it comes to World Central Kitchen. The call occurred, the first statement
from the White House came out. It mainly focused on what the U.S. said, not a lot
on how it was received by Netanyahu. You can't trust the first statements ever.
There's more information that is going to come out and you can
tell a little bit more as things move along. It wasn't long after that that
National Security Council spokesperson Kirby came out and he said that they
quote, hope to see announcements of changes in the coming hours or days. I mean,
that's confidence. That's confidence. That suggests that Biden might have used a
lot of that leverage. I would imagine the part that the public never gets to
here would be Biden saying something like, what makes you think we're going to veto that
French resolution?
Because that would definitely get Netanyahu's attention.
Something like that.
It would be something big to instill this level of confidence unless Netanyahu's team
had already called and said, okay, we're going to do some stuff.
Shortly thereafter, the news started really coming out.
Netanyahu and the cabinet over there have agreed to open the gate in the north, the
gate in the north to allow aid in.
This would allow direct access to the most heavily impacted areas, the areas where the
US says that famine is probably present.
And they agreed to give Jordan access to a port to basically flood the area with food.
Realistically, this is good news, but it's just an announcement.
The US response to this was basically great, but, well, it needs to, quote, must now be
fully and rapidly implemented.
The announcement isn't enough.
And it's worth noting, I can guarantee you, this is not all they asked for.
This is not all that was spoken about.
I would guess there's stuff about Rafa in there as well.
Now as long as no games are played and that gate gets opened, they don't talk about security
concerns or whatever, this stands a really good chance of stopping the situation in the
north from spiraling out of control to the worst-case scenarios and to be clear
on that the numbers that we've talked about on the channel that are in the
tens of thousands those are not the worst-case scenarios. The worst-case
scenarios are they're really bad. So that's where it sits right now. There's
been movement. How effective it's going to be, we have to wait and see. But the
conversation certainly appeared to have an impact. How big and how long that
impact lasts is yet to be determined. There are three real quick questions
about this that we're going to go through right now. One is that, you know,
Biden keeps talking about an offensive into RAFA. They're already doing stuff
in RAFA. An operation and an offensive are not the same thing. What's going on
now, the raids going in and stuff like that, those are operations. My guess is
that, well, I know the Biden administration is okay with those. My guess is that
they're actually encouraging them because those are lower footprint when
you are talking about civilian loss. An offensive is a major offensive,
something that would realistically, in RAFA, send hundreds of thousands of
people fleeing into areas with no infrastructure. If that occurs, the
humanitarian situation would be almost impossible to deal with, if not
impossible. If they launch an offensive into Rafa, there's not going to be any
mistaking it. So it's too, it's use of military terms that that may be
getting lost in in the coverage. Another one is why isn't Biden pointing out the
delivery dates when he takes criticism about the recent arms transfers. We
talked about this on the channel more than once. The whole point of slow
walking delivery of military aid if you believe your ally is about to do
something that's just a really really bad idea. The whole point of slow-walking
it is so there's not a record of you doing it. When we talked about it I think
I said something like Biden could address it via priorities. So you know
we've got Ukraine, we've got Taiwan, we have our own needs, and you, your, the
political winds just aren't with you right now. Don't worry we'll get you
your stuff, it's just gonna take a little bit of time. The whole point of doing it
that way is to not put it out there that when your ally requests something
you're not giving it to them. So for that to be effective, he just has to take the
criticism. It's, I mean, it's not fair I guess, but it's foreign policy.
Nothing about this is fair.
That's the reason he's not pointing it out.
It's also worth noting that some of that, that's
just the way it is.
When it comes to the planes that won't be there for years,
that's the normal time frame.
There's some other stuff that's been requested and approved
that's taking months to get there, that, I mean, sure, maybe that's a legitimate shortfall,
but in most cases, a lot of the stuff that's taking months to get there could be there
in less than a day.
I think it is possible that some of it is him slow-walking it, but we'll never know.
I mean, not until long afterward.
It's possible, but we don't know that.
And then the last one is why isn't Biden pushing for an immediate permanent ceasefire?
Because when he's saying ceasefire, if you don't know, he's talking about a temporary
one.
This is something we've talked about since early on.
and that foreign policy team, they are not going for a ceasefire. All of this
started during a ceasefire. They're angling for a peace. All of their moves
are angling for a peace and one that includes a Palestinian state. That's
where they're going. That's what all of their foreign policy moves indicate.
The concern, I think, from the Biden team is that if they get a permanent ceasefire,
then there's less urgency to get a peace deal.
That's my guess.
I don't know that, and that is not something that has been said, but if I had to guess
what the reluctance was to support a permanent ceasefire, it's that.
Because they're angling for a peace.
They're angling for a long-term solution, not just a permanent ceasefire that just reinforces
the status quo before the 7th and honestly basically guarantees another giant cycle.
I personally believe that you could have a ceasefire now and work out the peace.
I believe that's possible.
That doesn't look like what the administration believes.
But if I had to guess, the reluctance is because they're not angling to just return to the
status quo.
They're going for a peace.
They're going for a peace plan.
what all of the moves with the Palestinian Authority and all of the
different talks about regional security forces. It's not for a short-term
ceasefire because understand a permanent ceasefire it's an oxymoron that's not a
thing. If it was a permanent ceasefire it would it would be a piece with very
very few exceptions. So that's it. This is a good start. The events, that's a good start,
and as long as no games are played at that gate, there's a really good chance of avoiding
some really bad stuff, but the White House response of seeing it fully and rapidly implemented,
we're going to have to wait to see it before we know that.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}