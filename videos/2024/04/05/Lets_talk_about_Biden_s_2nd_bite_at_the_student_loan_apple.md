---
title: Let's talk about Biden's 2nd bite at the student loan apple....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=lSrbvYx2Oe4) |
| Published | 2024/04/05 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden's second attempt at student loan forgiveness is being tailored within the Supreme Court's ruling that struck down the first proposal.
- Two different tracks were worked on by the Biden administration after the first proposal was rejected by the Supreme Court.
- One track involves smaller, piecemeal forgiveness amounts totaling around $144 billion for four million people.
- The other track focuses on a new law for a large student loan forgiveness program tailored within the Supreme Court's ruling.
- The new proposal is set to address categories such as financial hardship, those who didn't apply for existing programs, and those with high-cost, low-income programs.
- Eligibility may include individuals with certifications or degrees from programs with high default rates or balances larger than the original loan amount.
- The aim is to alleviate current issues with student debt but also raise awareness about systemic problems.
- Questions remain about the effectiveness of this new approach and whether it will work within the confines of the Supreme Court ruling.
- The Biden administration is expected to announce details of the new proposal soon, possibly within the next week or two.
- The proposed student loan forgiveness plan may become active before November, with the administration aiming to fulfill campaign promises.

### Quotes

1. "Addressing student debt can alleviate current issues, but it doesn't address the root causes."
2. "The Biden administration wants this out before November because it's about a third of what was promised during the campaign."
3. "This is something they're actively working on and pursuing for a second term."

### Oneliner

Biden's new student loan forgiveness plan tailors within Supreme Court's ruling, addressing various categories but raising questions on effectiveness and timeline.

### Audience

Students, Debtors

### On-the-ground actions from transcript

- Stay informed about the details of the new student loan forgiveness proposal (implied).
- Provide feedback during the public comment period for the rulemaking process (implied).
- Advocate for comprehensive solutions to address systemic issues with student debt (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of Biden's new approach to student loan forgiveness, which may better inform individuals on the potential impact and limitations of the proposed plan.

### Tags

#StudentLoans #BidenAdministration #SupremeCourtRuling #DebtForgiveness #CampaignPromise


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Biden's second bite
at the Apple.
We're going to talk about the next attempt
when it comes to student loan forgiveness
and how that is being received immediately
and how it's likely to play out as it moves forward.
Okay, so quick recap.
Biden attempted to address student debt early on.
The Supreme Court shot the first proposal down, the big one.
Once that happened, almost immediately, the Biden administration started work on two different tracks.
One, the piecemeal stuff, the stuff that we've talked about every once in a while on the channel.
You know, $7 billion of forgiveness here, $3 billion there.
pretty soon it adds up. And so far it looks like around four million people and 144 billion
dollars have been either, you know, had their debt forgiven, and that's the amount that's been forgiven.
The other track that he was working on was using a different law that would be tailored within the
Supreme Court's ruling that struck down the first law to do a large student loan forgiveness
program.
So contrary to the right-wing talking heads that have come out already about this saying
that Biden is defying the Supreme Court, no, they're taking the ruling from the Supreme
Court and tailoring the new rules under a different law to fall within the
Supreme Court's ruling. That's not defying the Supreme Court, that's
literally how it's supposed to work. It does make it very clear that the one
class that a lot of these pundits do not have debt for is basic civics. Okay, so we
don't have the details of the new proposal. I think they're supposed to be
announced next week, but the new rules reportedly will address a number of
categories. One, people experiencing financial hardship. I know people are
going to ask, no clue on how that gets defined. That information really isn't
out there yet. There's theories and hints, but nothing worth reporting on, nothing
solid enough to report on yet. Those eligible for other programs that didn't
apply for them. Those that didn't apply for the programs that already exist, they
would be covered by this as well. Those people who had high-cost, low-income
programs that they went through in school, they would be eligible. I'm not
sure how the math on that would work out. Those people who sought
certifications, degrees in programs
that have a high default rate.
Again, it's hard to know how that would be determined
at this point in time.
Those that have balances bigger than the original loan amount.
I mean, the fact that that is a large enough group of people
to be its own category, it is kind of like a clear
demonstration that there's something broken with the system and that while
addressing student debt can certainly alleviate the people currently
experiencing the issues, it doesn't address the root causes. Hopefully this,
once it goes through, would raise awareness about that and maybe
we get some movement that actually leads to a real solution rather than the
band-aids. And then the last category is those people who have been paying on
their loans for 25 years or more because that is another group of people. Okay, so
the obvious questions that are going to come in, is this going to work? Don't know.
The whole point is they spent almost a year going through the Supreme Court
ruling and tailoring this proposal to be within the confines of what the Supreme
Court said. So theoretically, yes, but this Supreme Court is, well, it's
different. It may contradict itself in some way. The other thing to note is when
this went on the first time, there was a bunch of commentary about how the Biden
administration was doing it wrong and how that they should have used both laws to
push through the first time. Understand if that had happened, he wouldn't get
the second bite of the apple. There was another group of commentary saying that
he should have used the law they're using now. You might have been right. You
might have been right. But that being said, the Supreme Court might have struck
it down anyway and then he would have had to use the the law he used the first
time to tailor the proposals within the Supreme Court ruling. My understanding
is that this is going to be announced sometime relatively soon, next week, maybe
the week after. The details will come out. I think this is using a process that
will have to have public comment because it's using like a rulemaking process, and
And that would take 30 days, something like that.
There is, by normal timelines, there's a chance that this becomes active before November.
There's also a chance that it doesn't.
But my guess is the Biden administration wants this out before November because while 4 million
people 144 billion forgiven. While those are good numbers, they're not the campaign promise.
This is about a third, around a third of what was promised during the campaign. My guess
is that he wants it clear that this is something they're actively working on and would pursue
in a second term.
So anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}