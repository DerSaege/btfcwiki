---
title: Let's talk about a message, a memo, a video, and a kingdom....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=0aI5z2N7wlU) |
| Published | 2024/04/29 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces the topics of a message, a memo, a video, and a kingdom, aiming to connect recent information.
- A leaked memo from the State Department raises concerns about assurances given by Netanyahu's government, with some components within the department doubting their credibility.
- The memo includes a list of potential violations of international humanitarian law, which has not received much coverage.
- Beau questions the potential connection between the leaked memo and his previous video discussing the fictional kingdom of Danovia, suggesting it might not be a coincidence.
- The leak of information from the memo is unusual and serves the purpose of signaling that the information is out there, potentially applying pressure without severing relationships.
- There is speculation about a potential timeframe for action before a report on the issue goes to Congress on May 8th.
- Beau hints at a pattern of how information is strategically released to the press to prepare the public for foreign policy changes.
- The ultimate goal seems to be applying pressure to prompt specific actions or responses.

### Quotes

1. "I mean honestly I don't know I can't prove that but that would be a really weird coincidence if it wasn't right?"
2. "It's how they do it. It's how they do it."
3. "This is how they are trying to apply pressure because they don't actually want to sever the relationship, but they have this card that they can play."
4. "Anyway, it's just a thought."
5. "Y'all have a good day."

### Oneliner

Beau connects a leaked memo raising doubts about international humanitarian law violations to his video on a fictional kingdom, hinting at strategic information leaks to apply pressure without severing relationships.

### Audience

Policymakers, activists, analysts

### On-the-ground actions from transcript

- Contact policymakers to inquire about actions being taken regarding the potential violations of international humanitarian law (suggested)
- Stay informed about updates related to the leaked memo and its implications (implied)

### Whats missing in summary

The detailed analysis and context provided by Beau in the full transcript may offer a deeper understanding of how strategic leaks can influence foreign policy decisions.

### Tags

#ForeignPolicy #StateDepartment #Leaks #InternationalRelations #PressureTactics


## Transcript
Well, howdy there internet people, it's Bo again.
So today we are going to talk about a message,
a memo, a video, and a kingdom.
And we're just gonna go through some information
that has come out and see if anything is connected.
A few days ago, I put out a video
that talked about how countries in general,
with the United States specifically in the video, get ready to engage in a foreign policy
shift in the steps that they take.
This video might be worth watching before you watch this.
If you haven't already seen it, it's the one about the fictional kingdom of Danovia.
Okay, so that's the video.
Now let's move on to the memo.
A memo was, quote, leaked from State Department, wound up in the hands of the press.
Now right now, all of the coverage is about the fact that some components within State
Department, they are less than believing of assurances given by Netanyahu's government.
And they say that they don't find them, quote, credible or reliable.
That's what most of the coverage is on right now.
Now it's worth noting that that is some components within State Department.
Some are willing to accept the assurances.
Some don't take a position at all.
But that's what the coverage is on.
The thing is that memo is not just like four lines long.
It also contains something else.
a list of, I think, eight potential violations of international humanitarian law, and 11
instances of potential violations.
But that's not really getting a lot of coverage yet.
But it's in the memo that is now in the hands of the press.
Okay so that's the memo. Now let's go to the message which is pretty short and
sweet. Dude, WTF is the leaked State Department memo the list about the
Kingdom of Danovia. You mean is the list detailing the exact kind of
information that I said was going to come out coming from the exact agency I
I said it was going to come from while a senator
was making the same type of statements
I said were going to be made. All happening within
what 48 hours of that video going out. Is that connected?
I mean honestly I don't know I can't prove that
but that would be a really weird coincidence if it wasn't right?
It's
it's how they do it. It's how they do it. Leaking the information, that's new, that's new. I don't
think I've seen that before. Normally it comes out in like press release fashion.
But that serves another purpose in this case because if it leaks, not just does the press get
the list of incidents that they might need to talk about at a later point in time, but
it also sends a very public signal that that information is out.
So that's new, assuming that this was intentional, again can't prove that, but I mean that would
be a really, really weird coincidence, especially having Senator Sanders make
the statements that he made around the same time, just like is described in the
video. It's a pattern. It's what they do. So what does this mean? It
means that, like I said in that video, I don't know if it'll work, but this is the move.
This is what they're doing.
This is how they are trying to apply pressure because they don't actually want to sever
the relationship, but they have this card that they can play.
The obvious question is, well, what's the time frame?
I don't know if I had to guess May 8th, and that's not just a random number, on May 8th
the report about this type of thing is supposed to go to Congress.
They would want some kind of movement on it before that report goes because whether or
not there is movement might, it might influence how specific that report is.
Or it may include, hey, these things happened but they have since changed their ways.
So yeah, I mean that, it certainly seems like it.
Again, I can't prove that, but it fits, it tracks.
For those that haven't seen the video, understand it's a long one, I didn't want to recap the
whole thing.
But the short version is that oftentimes the US government will put out information and
get it into the hands of the press so they can then put together stories that go out
to the public and kind of ready the public for any potential changes that might need
to be made when it comes to foreign policy.
out.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}