---
title: Let's talk about a Biden impeachment inquiry surprise....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=v9Itn8-S0bA) |
| Published | 2024/04/29 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau expresses surprise at the lack of findings in the Biden impeachment inquiry.
- Comer, a key figure in the inquiry, expressed being "done" with it and hoping for divine intervention to end it.
- Despite 15 months of investigation, nothing incriminating has been uncovered against Comer.
- Beau had expected some fabricated allegations to be blown out of proportion for political gain.
- Impeachment inquiries may continue for social media engagement, but their outcome seems doubtful.
- Even if something minor was exaggerated, passing it through the House and Senate remains uncertain.
- The lack of substantial findings is unexpected to Beau, hinting that the inquiry might fizzle out without major developments.

### Quotes

1. "They haven't found anything at all. That's surprising."
2. "I honestly figured they would come up with something and try to just repeat it and blow it out of proportion."
3. "It kind of seems like this is over."
4. "So obviously that means that the impeachment inquiries will stop, right?"
5. "Y'all have a good day."

### Oneliner

Beau expresses surprise at the lack of incriminating evidence in the Biden impeachment inquiry, hinting at its potential fizzle-out without major developments.

### Audience

Political observers

### On-the-ground actions from transcript

- Stay informed on political developments (implied)
- Engage in political discourse with others (implied)

### Whats missing in summary

The full transcript offers Beau's detailed analysis and commentary on the lack of substantial findings in the Biden impeachment inquiry. Watching it provides a deeper understanding of his perspective on the situation.

### Tags

#Impeachment #PoliticalAnalysis #SurprisingFindings #Beau #Biden #Inquiry


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about
an impeachment inquiry surprise.
Surprising to me anyway.
May not be surprising to anybody else,
but it is surprising to me.
So we're going to go through some of the recent developments
and the reporting and just talk about where that's at
and just kind of run through everything.
Okay, so Comer is one of the key people
in the Biden impeachment inquiry.
And reportedly, he recently told a colleague
that he was quote, done with the impeachment inquiry.
And another lawmaker said that Comer is hoping
Jesus comes so he can get out.
Why?
Because they haven't found anything.
They haven't found anything at all.
And that is surprising to me.
And I'm not talking about the Ukraine influence stuff.
I've got videos going back years debunking that stuff.
That was never something I expected to pan out.
But they've been looking into him for 15 months.
This guy's been a politician for, carry the one, like 612
years, and they couldn't find anything.
Not even something to, you know, a molehill to turn into a mountain.
Nothing.
That's surprising.
I honestly figured they would come up with something and try to just
repeat it and blow it out of proportion until
they got some kind of
traction with their own bass at least.
But here we are
and it doesn't look like they've
anything at all. So obviously that means that the impeachment inquiries will stop,
right? I mean, probably not. They'll keep them open so they can continue to
generate social media clicks and stuff like that. But at this point, it seems
incredibly unlikely that this is really gonna go anywhere. I mean, again, you've
You have had Democratic Party members basically daring them to go ahead and take the impeachment
vote.
And it is important to keep in mind that even if they were to take a molehill and turn it
into a mountain, it's not even certain it would get out of the House, much less through
the Senate.
But the fact that they really couldn't find anything to latch onto, to just do the normal
Republican thing and just repeat it over and over again until people believed it, that's
surprising to me.
So I wouldn't expect a whole lot of major developments with this.
You may have rhetorical developments, but it kind of seems like this is over.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}