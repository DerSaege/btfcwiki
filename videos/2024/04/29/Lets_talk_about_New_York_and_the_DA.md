---
title: Let's talk about New York and the DA....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=or7iiLaE5xE) |
| Published | 2024/04/29 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Story from Monroe County, New York involving a vehicle speeding.
- Driver did not stop when officer attempted to pull them over.
- Intense confrontation captured on body cam inside driver's garage.
- Driver revealed to be the district attorney.
- Driver showed defiance and arrogance during the interaction.
- Despite resistance, driver eventually accepted the ticket.
- Local officials and state lawmakers called for an investigation and resignation.
- Governor launched a state investigation into the district attorney's conduct.
- Public outrage over the district attorney's behavior.
- District attorney deleted her social media presence.
- Likely investigation by the Attorney General due to the seriousness of the situation.
- Anticipation of further developments in the story.

### Quotes

1. "I know more about the law than you."
2. "You're supposed to do what they say, nobody is above the law."
3. "I'm sure that this story is not over and that there will be more developments."

### Oneliner

A story unfolds in Monroe County, New York as the district attorney's defiant behavior leads to calls for investigation and resignation, sparking public outrage and media attention.

### Audience

Community members, activists

### On-the-ground actions from transcript

- Contact local officials to demand transparency and accountability in the investigation (suggested).
- Join community efforts advocating for justice and fair treatment under the law (implied).

### Whats missing in summary

The detailed nuances and emotions of the intense confrontation between the district attorney and the officer, as well as the potential long-term implications of the investigation.

### Tags

#MonroeCounty #NewYork #DistrictAttorney #Investigation #PublicOutrage #Accountability


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about a story out of New York,
out of Monroe County, New York.
And we're just going to run through the events,
talk about some of the highlights
when it comes to what was said, and then
talk about the effects that are certainly on their way
And the fact that this story is definitely not over yet.
It starts off pretty simply.
A vehicle was exceeding the speed limit,
according to the officers, doing 55 in a 35.
Officer tried to pull the vehicle over.
Vehicle did not stop.
Vehicle just drove home.
The officers followed.
And they got into a conversation that
was captured on body cam inside the driver's garage.
The driver, at one point during the conversation,
indicated that they worked with the district attorney's office.
And the cop was like, oh, you're a DA.
And she's like, no, I'm the DA, the DA.
It gets better, just wait. Highlights include I was going 55 coming home from work. The officer
says 55 and a 35. She says I don't really care. When asked why she didn't stop, because I didn't
like stopping on Phillips Road at 530. And other highlights include stuff like I know more about
the law than you, so on and so forth. The district attorney has gotten into a little bit of hot water
over this conversation that occurred. Now, at the end of it, I want to say it's 25 minutes or more,
she does accept the ticket eventually. So what has happened? Well it started with
local officials calling for a probe or investigation then it moved to state
lawmakers. There were calls for her resignation and now it appears that the
governor has launched a state investigation suggesting that the DA
undermined her ability to serve. The public is not happy about this. There's
a lot of outcry as far as, you know, you're supposed to do what they say,
nobody is above the law, these standard stuff. My guess is that a lot of the
public reached out over social media because the DA has, it certainly appears
that she has deleted her social and her social media. Yeah so this is going to
end up more than likely end up being an investigation ran by by James by the
Attorney General up there. Given her habits so far when it comes to people
in office and her, well, let's just say reluctance to let things slide, my guess
is that the investigation will be followed up on. So we'll see where
it goes from here, but I'm fairly certain that this story is not over and that
There will be more developments.
And I'm sure that given the people involved, the attorney
general and the DA, and now the governor's gotten involved,
there's going to be a lot of media coverage about this.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}