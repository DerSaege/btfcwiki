---
title: Let's talk about Florida being in play for 2024....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=RCsUwvp2E3M) |
| Published | 2024/04/03 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Florida's recent rulings affect the timing and dynamics of the upcoming presidential election.
- Two rulings were made in Florida: one implementing a six-week ban on family planning and abortion, and another putting reproductive rights on the ballot in November.
- Floridians will vote on a statement in November regarding abortion laws, requiring a 60% majority to pass.
- The impact is that a six-week ban will be in effect for 30 days before the November vote.
- The outcome in Florida could influence the entire presidential election, making it a key state for both parties.
- The Biden campaign stands to benefit from investing resources in Florida due to these changes.

### Quotes

1. "Florida is in play for the presidential election."
2. "If the Democratic Party invests resources in Florida, it very well may pay off for them."
3. "This has changed that dynamic."
4. "It very well might alter the outcome of the entire presidential election."
5. "Anyway, it's just a thought."

### Oneliner

Florida's rulings put the state in play for the presidential election, impacting abortion laws and potentially altering the election outcome.

### Audience

Florida voters

### On-the-ground actions from transcript

- Mobilize to vote and advocate for reproductive rights in Florida (exemplified)
- Support organizations working to protect reproductive rights in the state (implied)

### Whats missing in summary

Analysis of the potential implications of these rulings on reproductive rights beyond the presidential election.

### Tags

#Florida #PresidentialElection #AbortionRights #ReproductiveRights #PoliticalImpact


## Transcript
Well, howdy there Internet people, it's Beau again. So, today we are going to
talk about timing and
November, Florida, and how the
math, as far as who's going to become president,
just changed a little bit because of a ruling
in Florida. So, what happened?
There are really two rulings. One, basically said,
Okay, 30 days from now, a six-week ban can go into effect when it comes to family planning,
when it comes to abortion.
The other, in a 4-3 ruling, said that, well, reproductive rights, it'll be on the ballot
in November.
the people in Florida that will be asked to say yes or no and vote on a statement that
is going to say, no law shall prohibit, penalize, delay, or restrict abortion before viability
or when necessary to protect the patient's health.
I don't know, 22, 24 weeks somewhere in there is when that normally gets decided.
In Florida, you need 60% to do something on the ballot like that for it to pass, 60%.
So what does this mean?
It's going to go from in 30 days, six week ban is going to go into effect.
Floridians are going to see that.
They're going to see the impacts of it.
And then they're going to have the opportunity to get rid of it in November at the same time
as the presidential election.
The groups that are in favor of a limitation of viability, they're going to be pushing
to get 60% of the vote.
The short version here is that Florida is in play for the presidential election.
The electoral votes are up for grabs.
This has changed that dynamic.
So if the Democratic Party invests resources in Florida, it very well may pay off for them.
This is a situation where the Biden campaign stands a decent chance of getting a state
that Trump won in 2016 and 2020 because they're going to get a big boost from this.
If they devote the resources to it, it very well might alter the outcome of the entire
presidential election. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}