---
title: Let's talk about 11% of your paycheck disappearing....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=OWbFY-dP1KY) |
| Published | 2024/04/21 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the year 2049 and the surprising proximity of it compared to past expectations of flying cars and jet packs.
- Mentions a study from the Potsdam Institute revealing a projected 11% reduction in median income in the United States over the next 25 years due to climate change.
- Emphasizes that this income reduction will affect almost all countries globally, not just highly developed ones.
- Compares the projected income reduction to a scenario where Congress raises taxes by 11%, indicating the impending economic impact.
- Urges viewers to understand that this study's projections are based on current conditions, not worsening future scenarios.
- Stresses that climate change consequences are transitioning from abstract issues to personal financial impacts on individuals' paychecks.
- Encourages a shift to cleaner energy sources to mitigate the economic effects outlined in the study.

### Quotes

1. "Imagine if your tax rate went up 11%. You probably wouldn't be happy."
2. "This study is based on now. Locked in is the term. It is locked in 11%."
3. "From now on, when you hear a politician say they don't want to do anything about climate change, what you need to hear is they want to raise your taxes by 11%."
4. "There's finally a study that's like, hey, here's how it's going to impact your pocketbook."
5. "Every time you hear a politician push back on doing what needs to be done, they want to raise your taxes by 11 percent."

### Oneliner

Beau warns of a projected 11% median income reduction due to climate change by 2049, urging action towards cleaner energy to avoid personal financial impacts.

### Audience

Climate activists, environmental advocates

### On-the-ground actions from transcript

- Transition to cleaner energy sources to mitigate the economic impacts of climate change (implied).
- Stay informed and advocate for policies that support a sustainable future (implied).

### Whats missing in summary

The full transcript provides in-depth insights into the economic implications of climate change, urging individuals to take proactive steps towards cleaner energy solutions.

### Tags

#ClimateChange #IncomeReduction #CleanEnergy #EnvironmentalAdvocacy #PolicyAdvocacy


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we're going to talk about 2049.
2049, I can remember when that was really far away.
We'd have flying cars and jet packs by then.
But it's just 25 years from now.
So we will talk about 2049, $38 trillion, and 11%
of your paycheck.
Why?
Because a pretty interesting study came out
from the Potsdam Institute.
And what it says is that over the next 25 years,
there's going to be a 19% reduction in global income,
11% in the United States.
The median income reduction, 11%.
Our analysis shows that climate change will cause massive economic damages within the
next 25 years in almost all countries around the world, also in highly developed ones such
as Germany and the United States, with a projected median income reduction of 11% each, and France
with 13%.
version here is that unless you live in a country that like borders the Arctic,
this hurts you. You have a reduction coming and you're gonna make less money.
I want you to imagine how you would react if Congress decided to raise your
taxes by 11% to increase subsidies to dirty energy. Because I mean through
there in action. I mean that's kind of what's happening, right? 11%. Imagine if
your tax rate went up 11%. You probably wouldn't be happy. You would want
something done about it. Here's the thing that people really need to
acknowledge. This study is based on now. Locked in is the term. That's the term
that was used. It is locked in 11%. That's not if things get worse. If things
things get worse....it's more!
This is something we talked about for years on the channel.
We have talked about the health consequences, we have talked about food security consequences,
we have talked about national security consequences.
We have talked about economic consequences.
Now it's not a national economic issue, it's your paycheck.
income reduction. The changes that need to be made, they need to be made. It's not
a political thing. It's something that has to occur. You know, in the United
It states basically every issue now has become polarized and politicized.
It shouldn't happen with this.
From now on, when you hear a politician say that they don't want to do anything about
climate change, they don't want to mitigate or they don't believe or whatever, what you
need to hear is they want to raise your taxes by 11% because I mean that's the
effect right? That's how it ends up playing out. And again for a lot of
people 2049 that's just such a date that's so far away. It's 25 years and
And this is going to happen within that period.
And it's based on now, not if things get worse.
There's a whole bunch of reasons to start a transition
to cleaner energy.
This one, there's finally a study that's like, hey, here's how it's going to impact
your pocketbook, your personal financial well-being.
Because before it was all abstract.
Every time you hear a politician push back on doing what, I mean, it's going to have
to be done, they want to raise your taxes by 11 percent.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}