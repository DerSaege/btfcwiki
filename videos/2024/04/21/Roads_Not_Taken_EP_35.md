---
title: Roads Not Taken EP 35
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=QlFyfkECBP8) |
| Published | 2024/04/21 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recap of events from the previous week, focusing on unreported or underreported news.
- US providing aid to Ukraine while pulling troops out of Niger, Russian advisors taking their place.
- No signed RAFA op from the US despite earlier reports.
- Israel possibly facing sanctions from the US and EU, including a specific unit within the IDF.
- In US news, Republican Party facing internal fractures and infighting.
- Fox News criticizes Marjorie Taylor Greene, hinting at a shift in attitude towards the GOP.
- Trump's rally canceled due to weather, impacting his campaigning ability.
- 9-1-1 outages affecting multiple states, prompting awareness of emergency contact alternatives.
- Cultural news involving actor Alan Richson clashing with the Fraternal Order of Police on social media.
- Settlement reached between Smartmatic and OANN in a lawsuit over election claims.
- Bird flu spreading among humans and cows, Boston Dynamics showcasing humanoid robot.
- Piece of metal falls through a Florida home from the International Space Station.
- Q&A session includes topics on abortion rights, Democratic strategies, and political dynamics.

### Quotes

- "Marjorie Taylor Greene is an idiot. She is trying to wreck the GOP." 
- "It was about internal politics, and I definitely think that he did a really good job."
- "Look at the actions and then try to figure out what they're doing."

### Oneliner

Beau recaps unreported news, internal GOP turmoil, potential Israeli sanctions, and Democratic strategies in a recent episode of "Roads with Bo."

### Audience

Political enthusiasts, news followers.

### On-the-ground actions from transcript

- Contact local representatives to express support for aid to Ukraine and awareness of potential Israeli sanctions (suggested).
- Stay informed on political developments and internal party dynamics (implied).
- Prepare for emergency situations by knowing alternative ways to contact emergency services during outages (implied).

### Whats missing in summary

Insights on the impact of unreported news, internal party conflicts, and geopolitical strategies discussed by Beau in the full transcript.

### Tags

#UnreportedNews #GOP #DemocraticStrategies #PoliticalAnalysis #EmergencyPreparedness


## Transcript
Well, howdy there, internet people, it's Bo again and welcome to the Roads with Bo.
Today is April 21st, 2024, and this is episode 35 of the Roads Not Taken, which is a weekly
series where we go through the previous week's events and talk about news that was unreported,
underreported, didn't get the coverage it deserved, or I just found it interesting.
And then we go through some questions from y'all at the end that are picked by the team.
Okay, so starting off in foreign policy, the big news that I would imagine most people
are already aware of, Ukraine will be getting a pretty big influx of U.S. aid.
The U.S. is pulling its troops out of Niger, Russian advisors are already starting to take
their place.
Despite reporting from earlier in the week, the U.S. has not, I repeat, not signed off
on a RAFA op of any kind.
It is still advising against any large-scale offensive.
Reports say that Netanyahu's office held emergency meetings on the subject of how to deal with
potential ICC warrants. The U.S. and the EU are issuing sanctions on some Israeli organizations
and individuals. Additionally, reports suggest that the U.S. might be readying sanctions
against a specific unit within the IDF. Understand, if that occurs, there will probably be a full video
on it over on the other channel, but that's huge. That is a huge deal. A short version,
if that happens, that unit cannot get U.S. military aid. If it occurs from the Israeli
Realistically they probably have to disband the unit. It's a big deal.
Okay, moving on to US news. The Republican Party is dealing with massive amounts of
infighting that is demonstrating that the party has become less and less cohesive and more and
more a coalition party. It is unusual for conservative parties to become coalition
parties in this manner, to break up like this. Because to be conservative, all you
have to do is, you know, fight progress, fight change. Having differences of
opinion to the point where things factionalize, that's a big deal and is
normally really bad news for the party. Okay, so, quote, Marjorie Taylor Greene
is an idiot. She is trying to wreck the GOP, end quote. That's just a headline
from Fox News that I found incredibly interesting. It does appear that Fox
starting to turn against the Twitter faction at least to some degree. The
Arizona GOP is reportedly planning to attempt to confuse voters with their own
ballot initiative about abortion. I feel like that's not gonna go according to
plan. Let's see, Trump's rally was canceled due to inclement weather. His entanglements
are cutting into his campaigning ability pretty sharply. Every other delay is going to be
a major upset for him. So it is important to watch things like that and see how that
impacts his behavior. There were 9-1-1 outages in multiple states that impacted millions of
people last week. It might be wise to be aware of the direct line to fire EMS police just in case
of outages like that.
In cultural news, Alan Richson of the show Reacher reportedly got into it with the Fraternal
Order of Police on social media.
The short version is that the actor mocked Trump.
Then the FOP got mad and started attacking him.
He questioned whether bullies should have the protection of an untouchable union.
It is unlikely that that's over.
That's probably going to turn into a giant thing.
Let's see.
Smartmatic and OANN have reached a settlement in a lawsuit over election claims.
From what I understand, it is governed by a confidentiality agreement and we don't
get to know the terms.
In science news, another bird flu case has popped up in a human and it continues to spread
among cows.
Boston Dynamics showed off its humanoid robot and basically terrified the internet.
I have to admit, every time I see one of their creations, I am simultaneously just amazed
at how far humanity has come, and I'm trying to figure out how to destroy it when the inevitable
robot uprising takes place.
Remember any robot named Bender Rodriguez has to go first.
Okay, in oddities, a piece of metal fell through a Florida home.
It was garbage jettisoned from the International Space Station.
Moving on to the Q&A.
Long time listener, first time caller, this past week Colorado abortion rights organizers
submitted nearly double the required signatures to get a constitutional amendment on the ballot
that would enshrine rights in the state constitution.
The major thing this would do other than making future restrictions more difficult is allow
state funds in the form of health insurance to go to abortion, repealing a 1984 prohibition.
My question, do you think this will boost turnout and help Democrats?
Yes.
It goes on to say Biden doesn't need it here, but CO3, Boebert's Old District and CO8 are
both shaping up to be close races.
could be swayed significantly by a small boost.
Yes, when it comes to anything related to family planning
being on the ballot, it is probably
going to help the Democratic Party.
And that's regardless of state.
That is regardless of state.
It may not be enough to sway an election in all cases,
but it will lean towards helping them.
Why do you still call them the Twitter faction?
It's called X now.
You want me to call them the X faction?
That sounds like comic book heroes.
No.
Absolutely not.
What is the weirdest thing you do as far
as getting ready to record?
I was recently at a TV studio and saw the rituals people do
perform from trimming nose hair, to eating certain foods,
to adding padding to the chair so their belt rubbing
on the chair doesn't make it sound like they trumped on air.
The weirdest thing I do is the soundproofing,
like the egg crate stuff, that foam padding
you see on the walls in a lot of studios. If I haven't eaten breakfast, I will often
tuck that under my shirt so it muffles the sound of my stomach. I think that's probably
the strangest thing I do.
Do you think that the Dems would actually declare him an insurrectionist? Wouldn't
letting Trump run on the Republican ticket be a fairly easy win for them?
If they did, what would happen?
If this happened after Trump was declared the nominee, could they nominate someone else
instead?
If not, what then?
Oh, it would be a mess.
And if they can, who do you think they would nominate?
Nikki?
Yes.
I mean, if they were smart, that's what they would do.
I don't know that they would do that.
Do I think the Democratic Party would actually do this?
Maybe.
Because in some ways, the bases about Trump, without Trump on the ticket, a lot of them
won't show.
So they might.
I don't know.
I don't have an answer to that.
That's a long one.
Okay.
Can we please stop talking about Speaker Johnson like he has a significant amount of McConnell's
political capabilities?
It's an insult to McConnell's vileness.
Passing the foreign aid packages through the House as separate bills is not a big accomplishment
if McConnell blocks one or more.
Even if there are more GOP resignations in the House, about five would need to occur.
And if those all happen, giving us Speaker Jeffreys, there is 0% chance that Congress
passes a bill to get Trump off the ballots.
Again, McConnell would stop that.
We know this because he already had his chance to get Trump off the ballot, and he did not
do it in February of 2021.
Thanks, I look forward to a video telling me why I'm wrong.
Okay, so there's a lot in this one.
He's not McConnell.
Not yet.
He's McConnell in the making.
Passing foreign aid packages through the House of Separate Bills is not a big accomplishment.
Don't assign motive.
You're assigning intent.
And I don't want to get too far into that because on the main channel we used to do
arcs where multiple videos were connected by a theme.
I'm trying to bring that back. And this arc about not assigning intent or motive has already started.
Nobody knows why Marcellus threw Tony out of that window, except for Marcellus and Tony.
I will say that Johnson's goal was not the aid packages.
That was the tool to cement himself in the Republican Party.
I don't think that he suddenly really cared about that.
That was a mechanism he used to do something else.
Don't assign intent.
The big accomplishment was once again rendering the Twitter faction kind of, well, rendering
them more irrelevant, making Biden accept a standalone package on Israel.
And maybe, it depends, we don't know this part yet, but maybe positioning himself as
the de facto leader of the Republican Party now that McConnell is fading, and maybe Trump's
The star is also declining.
Don't think it was actually about the aid.
It was about internal politics, and I definitely think that he did a really good job.
Now, even if there are more GOP resignations in the House, about five would need to occur.
There is zero percent chance that Congress passes a bill to get Trump off the ballot.
Again, McConnell would stop that. I don't know that to be true.
Just because a politician did something at one point in time doesn't mean they would do it later, see the aid packages.
Especially if perhaps McConnell and Johnson have an understanding.
Again, it's not about the aid package, it's about positioning within the Republican Party.
The other thing along this line that I've seen is, you know, well, there's no way they'd
get that through the Senate.
And there's no way that they would go for that because it would open them up.
That's exactly how they would get the votes.
If we use the 14th Amendment on, you know, the president, I mean, there's no reason to
do it to people in the Senate, especially if we have their vote.
There's going to be a lot of discussion about this over the next week as far as assigning
intent and motive when that's not really something that's known.
Look at the actions and then try to figure out what they're doing.
Not the other way around because then you end up reading into actions rather than the
actions in forming your opinion.
I've watched your videos for a while now and have come to enjoy the way you are able
to make me look at something from a different perspective.
I've been an avid Warhammer 40k fan for a while now.
Games Workshop, the company that owns and runs the IP, added a female custode to the
lore.
As far as retcons go, this should have been a huge nothing burger as it fundamentally
doesn't change anything.
Everyone in my local game store doesn't seem to be bothered by it at all, and most, if
not all of them have been engaging in the setting longer than I have.
Why is the internet blowing up over this?
It feels like several people online from vastly different sides of this argument are treating
this like some kind of major loss or victory.
I can't help but believe that this has gotten so much attention because 40K has become a
recognizable brand.
I do not know a whole lot about this.
Not about the game and not about this current,
oh no, they've gone woke thing.
But what I can tell you, and I'll
try to find the video and put it down below, a while back,
there was something similar occurred with the GI Joe
brand over a character being Asian.
I know.
It doesn't make any sense trying to explain it later,
and that may be why this is hard to grasp.
Online, in spaces that are devoted to culture war,
or at least talk about it a lot, it was this huge thing.
It was going to end GI Joe and all of this stuff.
G.I. Joe's fan base never cared, like that was not, that was not a thing, because it
kind of, it was a nothing-burger, because of the dynamics of G.I. Joe for a long time.
I'll find the video, I'll put it down there.
There's probably a lot of overlap.
There are a lot of culture war topics that don't really represent the actual fandoms
being talked about.
It happens way more than you would imagine.
So it's not surprising that something has come up and it's being made a bigger deal
than the people who routinely use that game view it.
It happens a lot.
But I would take that.
If there's a lesson in this, it's
the whole idea of the silent majority,
or the vocal minority is really what it boils down to.
A lot of times, you have people that
are very vocal about a topic that
get a lot of attention that aren't
representative of the whole.
And they're certainly not representative
of the demographic that is using that brand.
So there you go.
And that looks like it.
So those are all the questions.
OK, so there you go.
A little more information, a little more context,
and having the right information will make all the difference.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}