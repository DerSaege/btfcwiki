---
title: Let's talk about 5% for the sleepy guy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=KOqrQN5Jkoc) |
| Published | 2024/04/21 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump is making it harder for down-ballot candidates in his party to run successful campaigns by taking over fundraising and demanding a 5% cut from Republicans who use his name and likeness.
- Small donors are directed to Trump, and there's pressure on Republicans to send a portion of their funds to him.
- Trump has installed loyalists at the RNC, making it clear that electing Trump is the priority.
- Republican candidates need Trump's support, but Trump needs them more, despite the current political dynamic.
- The hierarchical mentality within the Republican Party plays a role in Trump's control over fundraising and candidate loyalty.
- If Republican House members stood up against Trump's demands, they could significantly impact his influence.
- Despite Democrats out-raising Republicans in fundraising, Trump's demands for tribute will further disadvantage Republican candidates.
- This move comes at a time when the Republican Party is already struggling with fundraising, especially with key figures like McCarthy gone from the House.

### Quotes

1. "Trump is making it even harder for down-ballot candidates in his own party to gather the resources they need."
2. "He wants a 5% cut from other Republicans if they use his name and likeness in fundraising."
3. "Trump needs Republican candidates behind him. They don't actually need him."
4. "Dear leader demands tribute."
5. "In a time when the Republican Party is already having severe fundraising issues, this is its insult to insult on top of the original injury."

### Oneliner

Trump’s control over fundraising and demands for tribute are further disadvantaging down-ballot Republican candidates, worsening existing severe fundraising issues.

### Audience

Political activists and fundraisers

### On-the-ground actions from transcript

- Contact local Republican representatives to express concerns about Trump's fundraising demands (implied)
- Organize fundraising efforts for down-ballot Republican candidates independent of Trump (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of how Trump's fundraising control affects down-ballot Republican candidates and the inherent power dynamics within the party.

### Tags

#Trump #Fundraising #RepublicanParty #DownBallotCandidates #PoliticalInfluence


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we're going to talk about Trump making it even
harder for down-ballot candidates to run.
Making it even more difficult for candidates in his own
party to gather the resources they need to have a
successful campaign.
Just to recap, Trump has, in many ways, just taken over when it comes to fundraising.
These small donors, they go to him.
And then there's, in addition to the donations, there's Bibles and shoes and stock and everything
that they should buy to be true Trump supporters and all of that stuff.
He has also installed Trump loyalists at the RNC, who have made it clear that the RNC's
goal is to elect Trump, that that's what matters.
And I believe they said something along the lines of every single penny would go to that.
Now on top of all of this, Trump has issued an unusual, a unique request.
He wants a 5% cut from other Republicans if they use his name and likeness in fundraising.
He wants money.
He wants them to send a minimum of 5% of what they raise to him, which is quite a departure.
It gets even more interesting when you look at the memo because the memo, any split that
is higher than 5% will be seen favorably by the RNC and President Trump's campaign and
is routinely reported to the highest levels of leadership within both organizations.
In other words, 5% means I don't send you a cease and desist letter, but if you actually
want to be able to come kiss the ring, you better kick up more than that.
This is going to damage his down-ballot candidates.
Now, the funny part of this, to me, is that the reason this works is the mindset of conservatives,
the mindset of the Republican Party, the mindset of wanting to be ruled.
That hierarchical mentality.
Let's be clear about something.
Trump needs Republican candidates behind him.
They don't actually need him.
In real life, that's how it works.
Now I understand they've created a political dynamic where everybody defers and bends the
need to Trump.
But they don't have to do that.
And if all of the members, the Republican members of the House of Representatives walked
out more like, hey, this guy has led us to loss after loss after loss and now he's ensuring
that we can't win on our own because he wants money.
It would have an impact.
He would lose his influence very, very quickly.
because the conservative mindset is we need a strongman leader, we need somebody
to tell us what to do and obey and all of that stuff, they won't ever consider
that. So my guess is that despite the reporting that we did recently talking
about how the Democratic candidates are already drastically out-raising the Republican candidates
in many, many cases, and especially in those that really matter when it comes to control
of the Senate, they're now going to be in an even worse position because dear leader
demands tribute.
In a time when the Republican Party is already having severe fundraising issues, especially
with McCarthy gone in the House, this is its insult to insult on top of the original injury.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}