---
title: Let's talk about Ukraine, Johnson, and deterrence....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=hNTOONmcDmM) |
| Published | 2024/04/21 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The US House of Representatives passed the aid packages, debunking the conventional wisdom surrounding the breakup.
- There is a rumor circulating on Capitol Hill that some Republicans may resign if there is a motion to vacate, potentially shifting the majority to the Democratic Party.
- The rumor suggests that with a Democratic majority, Trump could be declared an insurrectionist in line with a Supreme Court ruling and be removed from the ballot.
- This rumor could explain Trump's supportive attitude towards Speaker Johnson.
- While Johnson may find this scenario excessive, it's plausible given the circumstances.
- Republicans on the Hill are discussing this rumor, indicating some belief in its validity.
- The idea of Johnson potentially being underestimated is raised, warning against viewing him solely as a Christian figure.
- There is uncertainty about Johnson retaining his speakership, with the rumored deterrent complicating the situation.
- If the Democratic Party gains control in both the House and Senate, it could lead to significant consequences for the Republican Party in 2024.
- The speaker references a scene from Pulp Fiction, possibly alluding to the unpredictability and intrigue of the current political landscape.

### Quotes

- "If you are a member of the Democratic Party, you have got to stop thinking of Johnson as just some weird Christian dude."
- "He is much smarter people are giving him credit for."
- "It is an incredibly strong deterrent."
- "We'll have to wait and see if it pays off."
- "Y'all have a good day."

### Oneliner

The US House passed aid packages, sparking rumors of potential political upheaval and strategic moves that could impact Speaker Johnson's position and Trump's candidacy.

### Audience

Political observers

### On-the-ground actions from transcript

- Monitor political developments closely to understand potential shifts in power (implied)
- Stay informed about rumors and speculations circulating in political circles (implied)
- Engage in critical thinking and analysis regarding political strategies and motives (implied)

### Whats missing in summary

Insights into the specific aid packages passed and their implications on various sectors.

### Tags

#USHouse #SpeakerJohnson #Rumors #PoliticalStrategy #DemocraticParty


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we're going to talk about the US House
of Representatives, Speaker Johnson, the votes,
what occurred, and something I heard.
OK, so if you have missed the news, the aid packages,
they went through.
It is clear at this point that conventional
wisdom about why they were broken up was wrong.
So, now the next question is, how does Johnson keep his seat?
And I heard a rumor.
I heard a rumor, and what we know about this rumor is that
it is being circulated up on Capitol Hill.
In fact, you have seen some Republicans reference it in public, and that rumor is that if there
is a motion to vacate, well, some Republicans will immediately resign.
That in and of itself isn't a huge deal, except it's enough to throw the majority to the
Democratic Party.
Now, is that a fact?
No, it's not a fact.
It's just something I heard from they and they talk a lot, but that is quite the deterrent.
That is quite the deterrent because it isn't just throwing the majority to the Democratic
Party.
It could also be throwing Trump off the ballot.
If you are the Democratic Party and you get the majority in the House right now at this
exact moment in time when you also have control of the Senate, what might you entertain?
Declaring Trump an insurrectionist in line with the Supreme Court ruling.
And he's done.
He's off the ballot.
That might explain Trump's attitude towards Johnson as far as he's such a good guy, he's
doing a good job, because maybe that possibility was discussed when Johnson and him had their
little kumbaya meeting.
Now from Johnson's point of view, does that seem reasonable?
No, it seems excessive, but that doesn't mean it didn't happen.
It's a unique set of circumstances.
We have no idea whether or not this rumor is true.
I don't, anyway.
But what we do know is that Republicans up on the Hill are talking about it, so at least
some of them believe it's true.
And when those little scamps get together they're worse than a sewing circle.
That might be a deterrent to a motion to vacate.
And as far as the conversation, nobody knows why Marcellus threw Tony out of that four
story window, except for Marcellus and Tony. We'll probably never know unless
there is a motion to vacate and it's successful and that happens. But my guess
would be that this rumor circulating at this exact time, when Johnson desperately
needs a deterrent, I don't think that's an accident. That sounds really sneaky. It sounds
like something McConnell would do. If you are a member of the Democratic Party, you
have got to stop thinking of Johnson as just some weird Christian dude. He is much smarter
people are giving him credit for. If he is underestimated you're going to end up
with somebody as politically savvy as McConnell that has the rhetoric of Newt
Gingrich and I don't think anybody wants that. I don't even think most Republicans
want that. We don't know what is going to happen as far as him retaining his
speakership. But there is a deterrent out there. At least some Republicans are
entertaining it as a real idea. And it is a huge deterrent because it's way
bigger than just throwing the majority to the Democratic Party. It's also way
bigger than just potentially throwing Trump off the ballot. Because if they get
the majority in the House, and they have it in the Senate, right before the election,
after an extended period of Republican dysfunction, and the Democratic party just racks up legislative
win after legislative win, it is really going to go bad in 2024 for the Republican party.
Now again, it's a rumor. It's just something I heard. It's been talked about, it's been
referenced publicly, but it is just a rumor. As far as I can find, there is absolutely
no confirmation of this, but I'm not that plugged in. So, other people may have confirmation.
don't know, but it is an incredibly strong deterrent. We'll have to wait and
see if it pays off. Now if you're wondering about some of the weird lines
in this and you can't figure out where it came from, it's Pulp Fiction, the
the scene with the five dollar milkshakes. Anyway it's just a thought.
Y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}