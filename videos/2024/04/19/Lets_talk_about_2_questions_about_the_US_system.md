---
title: Let's talk about 2 questions about the US system....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Qyk3lcYTPqw) |
| Published | 2024/04/19 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the connection between fundraising and winning elections, focusing on the importance of name recognition through ads.
- Points out that a significant percentage of Americans cannot even name their current representative.
- Describes how attack ads are used to inform voters about candidates' positions, often in a negative light.
- Details the different types of ads used by Republican and Democratic candidates to signal to their party base.
- Emphasizes that fundraising is primarily used to gain name recognition, win primaries, and then secure general election victories.
- Addresses the perception of the House of Representatives as immature compared to the Senate, attributing it to the primary process and district dynamics.
- Illustrates how winning a Republican primary has shifted towards extreme rhetoric to appeal to the base, leading to unique candidates in the House.
- Mentions the influence of Trump's style on current House members and the less pronounced effect on the Democratic Party.
- Contrasts the Senate's statewide races with the House's district-based elections, resulting in more deliberative candidates in the former.
- Explains the role of the filibuster in promoting bipartisanship in the Senate and preventing extreme legislation from passing.

### Quotes

- "People vote for the R behind their name."
- "That's how people end up being ruled rather than represented."
- "It's just hidden."
- "But they get wrapped up in the culture war issue because the party platform told them what to believe."
- "That's what caused this."

### Oneliner

Beau explains the influence of fundraising on elections and the differences between House and Senate dynamics, revealing why certain candidates prevail and how representation can turn into rule by party platforms.

### Audience

Voters, Political Observers

### On-the-ground actions from transcript

- Understand the importance of informed voting and research candidates beyond party affiliations (suggested).
- Get involved in local politics to ensure a better understanding of candidates and their policies (exemplified).

### Whats missing in summary

The full transcript provides a detailed analysis of how fundraising shapes election outcomes, why certain candidates prevail in different chambers, and the impact of party platforms on representation versus ruling by ideology.

### Tags

#Elections #Fundraising #Government #HouseOfRepresentatives #Senate


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about two questions that came
in a lot over the last week, two weeks.
And the questions primarily came from people who were
from outside of the United States.
And they identified themselves that way in most cases.
and the questions are about how our government functions.
So we're just gonna go through those
because I imagine there are some people in the United States
that also have the same questions.
The first one is,
what does fundraising have to do with winning an election?
If that's your question, you don't have to specify
that you don't live in the United States.
Yeah, okay, so this is the piece of information
you need to know to start with
for everything else to make sense.
The percentage varies from year to year in study to study.
But if you look,
somewhere between thirty-seven percent
and sixty percent
can identify the name
of their current representative.
Keep in mind
These are the people currently in office,
not just running for office.
These are the people who won last time
and who are currently in office.
Understand the 60% number, that is definitely the outlier.
It's normally not more than half.
So there's a lot of fundraising that goes on to create ads.
What are the ads for?
get name recognition to win. People recognize the name, and they vote for it.
I talked about this a lot when it came to a justice in Wisconsin because her
campaign was brilliant. She had, she has a name that is, it's hard to pronounce, has
difficult spelling. Her ads were teaching people to say her name. And if you have
ever watched any video about Wisconsin and wonder why I pronounce Justice
Protasewicz her name that way, it's because that's what her ads were. It was
brilliant. She's in office. She won. There was more to her winning than that, but
that didn't hurt, I assure you. So they need name recognition. Most times this
money is spent during the primary, and we'll get to why here in a little bit.
The other thing that a lot of it gets used for is attack ads. If roughly less
than half, most times, of Americans can even name their representative. Do you
think they know their policy positions? They don't. So ads get run to inform the
voters of what that person has done or said. Most times it's negative. And then
each candidate runs their standard ad to signal to their party base. Like if
you're a Republican, normally the ad is the candidate in like a rural area that
is probably not their house and they're standing there with their rifle and you
They're very rural looking, even though they probably left recording that ad and climbed
into a Mercedes.
And they're just like, you know, my name's Jerry Mander and I'm running for office in
your area.
I don't actually live there.
I live in another state.
But you want to vote for me because look, I'm just like you.
For the Democratic Party, you all aren't getting off the hook.
It's a different weird ad.
Normally it's something with them, like if it's a guy, they're in a suit, but it's very
relaxed and they're in a coffee shop or a diner and they're shaking hands with an impossibly
diverse customer base in that coffee shop or diner.
Or they have the weird one with the hard hat too.
But it's to signal to the base.
they talk about what they're going to do, and they talk about the party platform.
All in hopes of getting name recognition, winning the primary, and then moving on and
winning the general election.
That's what it gets used for.
I mean, it gets used for other stuff as you see from the headlines, but that's what it's
supposed to be used for.
Yeah.
Okay.
The other question is, why does your House of Representatives seem like it's full of
children and the Senate seems so much more mature?
Yeah, that tracks.
That's not even a thing that is seen only by people from outside the United States.
The Senate is referred to as the more deliberative body.
Okay, so how does that happen?
Why do you have so many very unique people in the House of Representatives?
It has to do with the primary process and the districts.
See, the House of Representatives, running for that you are running in a district.
Oftentimes that district is, well, let's just say that most of the lines aren't straight.
They are constructed in a way most times to give a particular party an edge.
So what matters is winning the primary.
If you're in a very, very red area, winning red meaning a Republican area.
If you're in a very red area, you have to win the primary and then you'll just kind
of coast through the general election.
How do you win a Republican primary?
It didn't always require this, but something happened around 2016 or so that basically
turned a Republican primary into people saying the most right-wing, regressive thing that
they can think of to appeal to the base.
And over time, a number of things have happened.
One, that rhetoric has moved further and further to the right.
And then two, you actually have people who now believe that rhetoric rather than it just
being a politician lying to you.
They win the primary because people don't know who their representatives are.
They're just voting for the party.
People vote for the R behind their name.
That sends them to the U.S. House of Representatives when really their main qualification was putting
out that weird ad and saying the weirdest thing in the room.
Winning that primary, getting the nomination, then in the general election the other Republicans
just vote for the R. That's how you end up with a lot of the candidates you find in the
U.S. House of Representatives.
Now to be clear, this happens with the Democratic Party as well, but it is nowhere near as pronounced.
This occurred because of Trump.
That's what caused this.
People emulating his style created what you see up there now.
So that's how you end up with the more childish, to use the term, people in the House of Representatives.
The Senate, on the other hand, they don't run that way.
It's a statewide race.
So saying the, you know, just wildest thing you can think of doesn't help you because
even if it's a red state, there's a whole bunch of areas in that state that are
Democratic strongholds. So you have to be more moderate. Therefore, the candidate
has to actually think about what they're going to say because it has to appeal to
both the rural voter and the city voter. It has to appeal to the Democrat and the
Republican and they have to build a wider a wider base to get their office.
Building that base requires more thought so you end up with more deliberative
people in the Senate. That's why that happens. But that's not just, again, that's
not something that's just observable from outside. The Senate is, and has
been for a hundred years at least, the more deliberative body. It has been the
the place where all of the just not all of the but most of the just really
ridiculous legislation
goes to die. The other thing is that
the Senate has a thing called the filibuster which
basically in the House for most things
you need a simple majority. In the Senate
for most things you need a little bit more than that.
That also helps
keep the more deliberative nature because what goes through the Senate
it almost always has to be at least somewhat
bipartisan
so you don't get
you don't get the extremes there
that's not exactly how
you know the House of Representatives and the Senate were designed
The idea of the House of Representatives being done by districts was really so the representative
could represent the people of that district and carry their concerns to DC.
Somewhere along the line that changed and it turned into the representative telling
the people in the district what the party platform is and telling them what's important
to them.
That was one of the places where the United States and the current government definitely
went awry.
You have people in districts that are incredibly rural with absolutely like a very limited
amount of exposure to anything LGBTQ.
There's just not a lot there.
It's very hidden still.
I guess I shouldn't say it's not there.
It's just hidden.
But that becomes one of the main talking points when in reality they should probably be more
concerned with infrastructure, rural development, you know, agriculture bills, stuff like that.
But they get wrapped up in the culture war issue because the party platform told them
what to believe.
That's how people end up being ruled rather than represented.
Anyway, it's just a thought y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}