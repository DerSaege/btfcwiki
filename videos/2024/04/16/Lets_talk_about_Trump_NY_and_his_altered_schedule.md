---
title: Let's talk about Trump, NY, and his altered schedule....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=cttJh1m8i_g) |
| Published | 2024/04/16 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's best-laid plans to delay legal proceedings have failed, forcing him to appear in court during the campaign season.
- Trump's strategy of delaying cases after the election backfired, leading to all proceedings occurring in the middle of his campaign.
- A judge's order for Trump to show up for court or face arrest will significantly impact his campaigning.
- Trump's displeasure at being referred to as "Mr." resulted in a contempt hearing being scheduled for April 23rd.
- Trump's request to have the case passed to another judge was denied, and his defense has a limited time to submit exhibits.
- Despite attempts to delay, nothing is going in Trump's favor in court.
- The current situation was predicted from the beginning due to Trump's strategy of delaying legal proceedings.
- Even after this case is resolved, Trump may face similar situations due to his unsuccessful delaying tactics.

### Quotes

1. "Trump's best-laid plans to delay legal proceedings have failed, forcing him to appear in court during the campaign season."
2. "From the very beginning, people were talking about the strategy of delaying, causing this exact situation, and now it happened."

### Oneliner

Trump's failed delay tactics land him in court during campaign season, as predicted.

### Audience

Political observers

### On-the-ground actions from transcript

- Attend or follow updates on the contempt hearing scheduled for April 23rd (suggested)
- Support legal accountability for all individuals, regardless of position (implied)

### Whats missing in summary

Insights into the potential impact of these legal proceedings on the political landscape.

### Tags

#Trump #LegalProceedings #CampaignSeason #DelayTactics #Accountability


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about delays,
and Trump, and strategies, and best-laid plans,
and Trump, and strategies, and best-laid plans,
and how those sometimes turn out.
And we are going to talk about something
that we've talked about before, repeatedly.
And now that it is very apparent
that that's going to happen,
we're gonna talk about the perception of it.
We're going to do this because I got a question and it boils down to how is Trump supposed
to campaign given what the judge said today?
Yeah.
If you are unaware of what the judge said, he said that Trump had better show up for
court and that if he didn't, I believe the exact quote was, there will be an arrest.
So, it's going to influence the way he campaigns, yes.
But as we have talked about repeatedly, Trump's strategy was to delay.
He was trying to get all of the cases after the election, and we talked about how that
was incredibly unlikely, but that was his strategy.
that strategy failed, now all of the proceedings are going to be in the middle of campaign
season. That's not something that was some planned event by the system. It's because
he continually tried to delay the various cases and this period opened up. Yes, it is
definitely going to alter the way he campaigns. Two, two and a half months he's
going to be in court now. But it's because of his best-laid plans. Okay, other
things that happened in court. Apparently Trump was very unhappy that he was
referred to as Mr. That apparently bothered him.
There is now a contempt hearing scheduled for April 23rd, starting off there.
Trump filed, I think, what?
There was another request for the judge to pass on the case, give it to somebody else
that was denied.
The defense has, I want to say 24 hours, I would double check that if the timing is very
important to you, to get exhibits in.
It was normal stuff, but it did not go in Trump's favor.
But the big thing for Trump is that he has to be there.
And that's not a surprise.
None of this is a surprise.
From the very beginning, people were talking about the strategy of delaying, causing this
exact situation, and now it happened.
And keep in mind, it is not impossible that once this is resolved, he has another one.
strategy of trying to delay the way he did. It is not working out. Anyway, it's
It's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}