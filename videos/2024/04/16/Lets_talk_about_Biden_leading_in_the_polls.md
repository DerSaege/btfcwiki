---
title: Let's talk about Biden leading in the polls....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=2a5I52HWIkk) |
| Published | 2024/04/16 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- There has been a shift in the polls with Biden now leading in many of them.
- Beau had previously expressed concerns about the polling methodology.
- He questions whether there is a significant demographic shift or if there is a fundamental issue with polling accuracy.
- Beau does not trust the current polling methods, citing past inaccuracies in primary polling.
- He believes that even though Biden is leading now, it doesn't change his lack of faith in polling this far from the election.
- Beau suggests that predicting the direction a candidate is heading in based on polling requires careful consideration of methodology consistency.
- He points out that potential changes in the likely voter demographics for 2024 could further impact polling accuracy.
- Beau expresses skepticism about the polling results and the possibility of significant changes in how polling is conducted in the future.
- He concludes by stating his lack of faith in polling at this stage and hints at the need for improvements in polling methodologies.
- Beau signs off with a thoughtful message, leaving room for possible changes and updates on polling analysis.

### Quotes

1. "There has been a shift in the polls with Biden now leading in many of them."
2. "I just don't have a lot of faith in the polling, especially this far out."
3. "It's just sus to me."
4. "If I see anything like that I'll let you know but until the polling lines up with the results or we have an explanation for a giant shift..."
5. "Have a good day."

### Oneliner

Beau questions the reliability of current polling methods amidst Biden's lead and potential demographic shifts, expressing skepticism about predicting future outcomes based on polls.

### Audience

Voters, Pollsters, Political Analysts

### On-the-ground actions from transcript

- Analyze and question polling methodology for political insights (implied)
- Stay informed about potential changes or updates in polling methodologies (implied)

### Whats missing in summary

Insights on the potential impact of demographic shifts on future polling accuracy.

### Tags

#Polling #PoliticalAnalysis #Biden #Trump #DemographicShifts


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about polling.
We're gonna talk about polling.
And we'll talk about Biden's numbers and Trump's numbers
and run through everything,
because if you don't know, there's been a shift.
And Biden, in a whole bunch of polls now, is in the lead.
And that led to a bunch of questions.
My opinion hasn't changed.
My opinion on this has not changed.
If you don't know what that's a reference to, I don't know, a week ago, two weeks ago,
I talked about the polling and I indicated that I think there might be a fundamental
issue with the polling.
I'll put the video down below if you want to watch it, but the short version here is
either, we are witnessing one of the largest demographic shifts in decades when it comes
see how different demographics are voting, or there's a fundamental issue with the polling.
Since it's a rematch, I don't really believe that there's a giant shift going on. So for me,
there's only one option. When you take that and combine it with some of the things that occurred
in 2020 and 2022, that kind of reinforces it. Almost like it's, almost like the methods they're
using are degrading over time and then you take that and you look at the
primary polling that in some cases was off by double digits. There's just not a
lot of faith that I have in the current polling. That doesn't change just because
Biden is now in the lead. It doesn't mean much to me. You know this far
far out, generally I don't really put a whole lot of stock in polling anyway.
For us to be this far out and for me to suspect there might be some issues, I just don't
put a lot of stock in them.
One of the questions that came in that I did find interesting was whether or not you could
at least kind of extrapolate the the direction that a candidate was headed
from the polling. Maybe, but you'd have to be real careful because what you would
have to do is find a poll from a specific pollster and then you would have
to make sure that they used the exact same methodology because again the issue
with the polling there's been some reporting on it so I would imagine that
of the pollsters are trying to maybe adjust who they determine is a likely voter or the methods
that they're collecting the information or whatever. If everything is done the same in the
polling and you have a candidate that is headed up or down, you can safely assume that they are
headed up or down with the demographic that's actually sampled. But I would not
trust the percentage points. I wouldn't trust that. And then you take all of this
when it comes to the polling and combine it with the fact that there's a
high likelihood that the likely voter in 2024 is going to be different than
previous years, that is also something that that might throw off the polls.
Hypothetically speaking, maybe something occurred that would drive more women to
vote than normal that would throw off the polls or encourage a higher
percentage of young people to turn out. There's stuff like that that is
definitely going to be at play, especially when you might actually have
a third party. There's a lot in flux here. I just don't have a lot of
faith in the polling, especially this far out. Maybe there will be changes to how
the polling is done. If I see anything like that I'll let you know but until
the polling lines up with the results or we have an explanation for a giant
shift it's just it's just sus to me. Anyway it's just a thought y'all have a
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}