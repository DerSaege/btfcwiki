---
title: Let's talk about Biden declining the impeachment interview....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=IBxnz_8dLAI) |
| Published | 2024/04/16 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republicans in the House invited President Biden to talk for their impeachment inquiry, but he declined.
- The committee's impeachment inquiry into Biden has failed to find any evidence of wrongdoing.
- Despite ample evidence supporting Biden's innocence, the committee continues to push false allegations.
- The impeachment inquiry seems to be lacking substance, with no significant findings against Biden.
- Rumors suggest that the inquiry will be referred to the Department of Justice (DOJ), but it's unlikely to lead to any action.
- Referring the inquiry to the DOJ won't change their stance since they likely already have the information.
- The committee may use the lack of action by the DOJ to claim they were thwarted by the "deep state."
- The anticipated outcome is that the inquiry will continue to focus on baseless allegations and conspiracy theories.
- Biden's refusal to participate in the inquiry may lead to further accusations from the committee.
- Ultimately, the committee's actions may be viewed as a political tactic rather than a genuine pursuit of justice.

### Quotes

1. "The facts do not matter to you."
2. "There's no there there."
3. "They will frame it in a very conspiratorial way."
4. "He doesn't want to talk in public or whatever."
5. "It's just a thought."

### Oneliner

Republicans invite Biden to impeachment inquiry, fail to find evidence, likely shift focus to DOJ with little impact, resorting to conspiracy theories in the end.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Stay informed about political developments and misinformation (implied)

### Whats missing in summary

Insight on the potential implications of baseless political tactics and misinformation campaigns.

### Tags

#Impeachment #Politics #DOJ #ConspiracyTheories #Biden


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Biden.
We're going to talk about President Biden
and an invitation he received
and him declining that invitation
and where it'll likely go from here
and how it'll end up playing out.
Okay, so if you don't know, if you haven't heard,
Republicans in the House have been running
what they are calling an inquiry
Um, and they were looking for some reason, I guess, to impeach president
Biden, one of those committees has invited him to come talk, he had said no.
Uh, but the response was a little bit more, uh, a little bit, uh, more
descriptive than that your committee's purported impeachment inquiry has
succeeded only in turning up abundant evidence that in fact, the president
has done nothing wrong. Yet rather than acknowledge this reality, your March 28, 2024 letter contains
the same litany of false allegations that have been repeatedly debunked and refuted
by the very witnesses you have called before your committee, and the many documents you
have obtained. Your insistence on peddling these false and unsupported allegations despite
ample evidence to the contrary makes one thing abundantly clear. The facts do not matter
to you. Yeah, I mean, that makes sense. If you haven't been following the impeachment
inquiry, there's no there there. There's really not. And they pursued multiple paths trying
to find something, anything, that was really untoward. I am actually surprised they didn't
come up with something but it doesn't look like they have. So what's going to
happen next? There have been rumors and statements indicating that they are
going to refer it to DOJ and that's their big move. Yeah I mean that tracks
that's a good political decision it really is but it won't do anything most
likely because most of this stuff is stuff that DOJ already had. So referring
it to them isn't going to change their opinion on it. They're just getting the
same stuff back with, honestly, I think there's probably more evidence in favor
of Biden now than when it started. So then what happens? DOJ doesn't do
anything about it because there's no change in what they have. So then those
responsible for the inquiry get to come out and say look we did our part but the
deep state they stopped us. That's probably gonna be their move and honestly
with their base, those kinds of shenanigans will probably work and it
will probably be believed among some of their base. But my guess is that's how
this is going to play out. Biden declined the interview. They'll rant and rave and
say, you know, look he doesn't want to talk in public or whatever. Then they'll
make their referrals, and then nothing will happen, and after a set amount of time they
will come out and demand that DOJ does something, and then they will blame it on somebody.
They will frame it in a very conspiratorial way.
That would be my guess.
It seems like the move they're going to make.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}