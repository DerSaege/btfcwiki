---
title: Let's talk about Trump failing with FISA, fading, and the future....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=-KKT1ioXni0) |
| Published | 2024/04/13 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump wanted to get rid of the FISA program, used for US intelligence gathering, due to personal grievances.
- Most Republicans in Congress ignored Trump's order to eliminate FISA, pushing it through in the House.
- If FISA had been eliminated, it would have significantly benefited foreign intelligence agencies like Russian and Chinese intelligence.
- Despite media portrayal, Trump's political influence within the Republican party seems to be fading, as many Republicans rejected his push to kill FISA.
- FISA was eventually reauthorized for two years, with a potential for reform due to Trump's failure in this instance.
- Trump's failure may have unintentionally helped civil liberties advocates by bringing attention to the need for reform in the FISA program.
- The program is deemed critical for US intelligence but has long-standing issues that require reform.
- Trump's unsuccessful attempt to eliminate FISA could lead to a coalition forming for real reform in the coming years.

### Quotes

1. "Trump wanted FISA gone."
2. "Despite media coverage, Trump is not the political powerhouse that they make him out to be."
3. "Trump failing the way he did here. He might have helped the civil liberties left."
4. "Trump really might have actually succeeded at something here by failing in the way that he did."
5. "Trump's influence within Republicans on Capitol Hill, it's fading in a big way because this was public."

### Oneliner

Trump's failed attempt to eliminate the FISA program may inadvertently lead to much-needed reform in US intelligence, showcasing his diminishing influence within the Republican party.

### Audience

Civil Liberties Advocates

### On-the-ground actions from transcript

- Advocate for real reform in the FISA program (implied)
- Join or support organizations working towards FISA program reform (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of Trump's failed attempt to eliminate the FISA program and its potential implications for US intelligence and civil liberties advocacy.

### Tags

#Trump #FISA #USCongress #Intelligence #Reform


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Trump
and the US Congress as a whole.
And we're going to talk about Trump failing with FISA
so hard that it demonstrates some fading
and it's going to change things in the future.
Okay, so what happened?
Trump wanted FISA gone. If you don't know what FISA is, it is a program that
assists US intelligence gathering capabilities. Trump has personal
grievances dealing with this program and he sent out his order to Republicans in
Congress to get rid of it. It's coming up for reauthorization and he wanted it to
go away. I believe his exact words were, quote, kill FISA. That didn't happen. But let's talk
about what would have happened if it did. The first thing that would have occurred if FISA
went away is every legal attache at every Russian embassy in the world would have popped
a bottle of champagne. It would have been a huge win for Russian intelligence. Massive.
Chinese intelligence would have been really happy about it as well. The next
thing that would have happened would have been a very, a very quick slide when
it comes to US intelligence and counterintelligence capabilities. Okay. It
is in fact a critical program and I know there's people wanting to say something
we'll get there, don't worry. So he wanted this thing gone. Basically most
Republicans in Congress were kind of like, hey, grown folks are talking, sit down,
and they ignored it. They pushed it through in the House. From what I
understand, there were a couple of motions that might delay it moving over
to the Senate, but once it gets there, by my count, it'll go through there as well.
There was even a little move to have them add a warrant to a specific portion.
That also would have greatly assisted foreign intelligence agencies.
So, it didn't happen.
Trump very publicly gave his order to his party, and they were like,
No. No. We're not doing that. Despite media coverage, Trump is not the political
powerhouse that they make him out to be. A whole bunch of Republicans in Congress
just told him to sit down, that this is not something that he should be involved
in. My understanding is that Republicans in the Senate are hot, like they're mad.
So, FISA was reauthorized for two years.
And in a very, very bizarre twist of fate, Trump failing the way he did here.
He might have helped the civil liberties left more than any Democrat ever could.
FISA is both a critical part of U.S. intelligence and counterintelligence capabilities and a
program that has some issues.
Understand Trump's framing of it, it was garbage.
But he's not entirely wrong.
The program needs reform.
And there has been a push for reform for years and years and years.
The problem is that most times it was pretty partisan.
Because of this, and because Trump is undoubtedly going to be unhappy about Republicans that
told him to sit down, it's going to add to his list of grievances.
This is something that is going to continue to be talked about, not just by him, but by
those people who are still firmly in his camp. They are going to talk about how
FISA needs reform and in two years when it comes up for reauthorization the
Civil Liberties Left is going to show up with a real reform package and my guess
is that it goes through in a very bizarre fashion. Trump really might have
actually succeeded at something here by failing in the way that he did. The, again,
the program itself, what it's designed to do, it is critical to US intelligence.
However, there are a lot of issues with oversight, with how information gets swept up that the
civil liberties left has been working on for years and they really haven't had any way
of getting it pushed through.
That might have just changed.
fix this program, it wouldn't be hard. It wouldn't be hard. And I think there's now
a real chance that that occurs, but solely because he wasn't successful. Understand
what he was pushing for would not have fixed the program. So you have a very unique situation
that is definitely going to come up again in two years.
You will hear about this program again.
You will hear about reforms.
And my guess is that you are going
to see a really weird coalition develop to reform this program
and leave it to where it can still function,
but also fix the issues that are apparent and have been
apparent for quite some time.
So you have two takeaways.
One, Trump's influence within Republicans on Capitol Hill,
it's fading in a big way because this was public.
This wasn't something behind closed doors.
Like, he tweeted out about, you know, getting rid of FISA.
He did this publicly, and they publicly were like,
No, that's a big deal, it's worth noting.
The other thing is because of how it happened, and because there are going to be right wingers
who are upset that Trump didn't get his way in two years when a real plan gets presented,
I think it'll go through.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}