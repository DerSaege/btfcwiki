---
title: Let's talk about language and reaching out....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=LuJPAT1uXHE) |
| Published | 2024/04/13 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the importance of words, definitions, and connotations in communication.
- Mentions using practical tidbits of how to reach out to people using specific words.
- Recaps receiving a message discussing avoiding polarizing terms to keep people engaged during a video.
- Shares an example of avoiding the term "pandemic" to prevent shutdowns and cognitive dissonance in viewers.
- Explains the concept of avoiding certain words to reach a broader audience effectively.
- References Jonathan Swift's quote about reasoning people out of positions they didn't reason themselves into.
- Emphasizes the importance of using non-polarizing terms in critical topics like foreign policy and climate for effective communication.
- Mentions the significance of reaching the 10 to 15 percent of people who can be influenced by avoiding polarizing terms.
- Encourages using terms untainted by typical political discourse to have rational, productive conversations.
- Concludes by discussing the possibility of changing opinions through rational dialogues and the importance of careful communication in difficult situations.

### Quotes

1. "Reasoning will never make a man correct an ill opinion, which by reasoning he never acquired."
2. "Ten to 15 percent of people that are up for grabs."
3. "You can't reason somebody out of a position they didn't reason themselves into."
4. "Every hostage negotiator, everybody who has ever dealt with a DV situation that turned into a barricade situation, they know that's wrong."
5. "It's just a thought. Y'all have a good day."

### Oneliner

Beau shares insights on using non-polarizing terms to effectively communicate and potentially change opinions through rational dialogues, especially in critical topics like foreign policy and climate.

### Audience

Communicators, influencers, activists.

### On-the-ground actions from transcript

- Choose non-polarizing terms in your daily communications (suggested).
- Engage in rational dialogues with others using carefully selected words (implied).

### Whats missing in summary

In-depth examples and personal anecdotes shared by Beau.

### Tags

#Communication #Words #ReachOut #Opinions #Change #Perspective


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about words,
definitions, connotations.
We're going to talk about language and how to use it.
Practical tidbits on how to use it to reach out.
And we'll talk about a quote, and we'll do this because I
got a message.
And it allows us to talk about something that I think is
to be really important for the rest of the year. So I'll go through, read the message, and then
come back and talk about different parts of it. I saw you in the comments talking about why you
didn't say the word, and then there's parentheses. Is it after 30 seconds, if not pause? Abortion,
often. The video you linked is fascinating because it's related to my major. You say you
avoid certain words to keep people engaged to hopefully reach them and
bypass their visceral reaction to certain terms. It all makes sense, but
people who have that reaction aren't rational, right? So what do you make of
the idea that you can't reason somebody out of a position they didn't reason
themselves into? Jonathan Swift, right? Okay, so the first part of that message
it's about a video. I will put it down below, but a very quick recap is there is a language,
there are words that are very polarized, and if you use them you lose the person you're talking
to if you're trying to reach out to them. Longtime viewers of this channel know that we went through
an entire public health issue, and it was incredibly rare for me to say the word pandemic
because the people who needed the information the most, when they heard
that word, they shut down. They weren't listening anymore. Cognitive dissonance
kicked in and they tuned out. This list of words is always changing and y'all
unintentionally tell me what the words are. If you want to reach people, you have
avoid those terms. You've probably experienced this in your personal life.
You've been talking to somebody about politics and then all of a sudden they
just got mad or they just shut down, didn't want to talk about it anymore,
something like that. And if you had access to the same graphs that are
talked about in that video, you could probably go back and find the word you
used. Strong rhetoric, sharp rhetoric. It's good to energize people who already think
the way you do. But if you're trying to convince people, not so much. Okay. But people who
have that reaction aren't rational, right? So what do you make of the idea that you can't
reason somebody out of a position they didn't reason themselves into. Jonathan
Swift, that's who that is often attributed to. That's not exactly what he
said 300 years ago. The quote has kind of adjusted throughout time, but it's really
close to something he said. The thing is what he said, the more accurate quote, is
also a more accurate statement. Reasoning will never make a man correct an ill
opinion, which by reasoning he never acquired. Correct an ill opinion, meaning
you're trying to get that person to think the way you do. You're trying to
correct the opinion that they hold, not just reason somebody out of a position.
They're close, but they're not the same thing.
If you go back and watch that video, that last minute of it, the danger of using the
softer language and just trying to get the information out there, let them decide for
themselves, all of that stuff, the danger is that you don't necessarily get somebody
who agrees with you, but you do get them out of that position.
You don't correct their ill opinion, but you can get them out of that position.
This is going to be important, not just because of the election, but because of topics involving
foreign policy and climate and a whole bunch of other things.
When you look at the polling that has explored this topic where they ask somebody, a lot
of times you see it as like a funny gotcha, you know, what do you think of Obamacare?
Oh, I hate it.
What do you think of the Affordable Care Act?
It's great.
I have that.
The polarized term.
When you look at the polling on it, what you find is that there are about 10 to 15 percent
of people that are up for grabs.
They can be reached by avoiding those polarizing terms.
Ten to 15 percent of people, that's huge.
It's definitely worth trying.
And it's going to be important.
It's gonna matter.
It's just something to keep in mind when you're having your conversations, when you are talking
to people in real life, in comment sections, whatever.
Try to use terms that haven't already been tainted by the normal political discussions.
you can find a way around those, a lot of times you can get somebody to reason. Now
they may still not agree with you, but once you've got them out of that
position, well then it's just a normal rational conversation. And even if they
didn't arrive at your position initially, you can probably get them there later.
Or the thing that nobody likes to consider about this is that they view it
more rationally than you and maybe you have to change your opinion. You have to
take the correction, so to speak. The other thing about this quote that I don't
like, you can't reason somebody out of a position they didn't reason themselves
into. Every hostage negotiator, everybody who has ever dealt with a DV situation
that turned into a barricade situation, they know that's wrong. They know it's wrong.
It's hard. You have to be careful and you can't get excited and you have to avoid
certain terms, but it can be done. And most times it's worth the effort. Anyway,
It's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}