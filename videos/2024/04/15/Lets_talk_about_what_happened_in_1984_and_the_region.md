---
title: Let's talk about what happened in 1984 and the region....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=OZzOjDvuHvU) |
| Published | 2024/04/15 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau Young says:

- Talks about how George Orwell's "1984" can inform our foreign policy views today based on what happened in the book.
- Mentions the importance of the commentary on control systems and world building in "1984."
- Explains how the constant war and power dynamics in "1984" parallel real-world foreign policy situations.
- Raises questions about the motivations behind countries like Saudi Arabia and Jordan defending Israel.
- Analyzes the concept of power and control systems in shaping foreign policy decisions.
- Compares the perpetual war in "1984" to real-life scenarios where conflicts are prolonged to maintain power.
- Touches on the idea of smaller attacks being permitted to uphold a siege mentality and domestic power.
- Suggests rereading "1984" to focus on its foreign policy aspects rather than just the control systems.
- Points out that destabilization caused by perpetual conflict can help maintain power for certain entities.
- Summarizes that the core motive behind foreign policy decisions remains the pursuit and preservation of power.

### Quotes

1. "You can't rebel until you become conscious, and you can't become conscious until you rebel."
2. "If you are bored and you want to take a look at something through a new lens, maybe reread '1984' and focus on not the control systems, not the underlying story, none of that stuff, but focus on the foreign policy."
3. "It's about power. If you're not picking on the Saudis, there's a nation that people say is predominantly Christian, and focuses on Christian values, but do the elite of that nation tell you to love your neighbor or do they give you two minutes of hate?"
4. "It always is. Every decision, when it comes down to the end, the decision was made based on preserving or gaining or protecting a nation's power."
5. "Because the end motive of foreign policy hasn't changed. It's power."

### Oneliner

Understanding foreign policy through the lens of "1984" reveals the enduring pursuit of power as the driving force behind decisions, echoing real-world dynamics.

### Audience

Foreign policy analysts

### On-the-ground actions from transcript

- Analyze current foreign policy decisions through the prism of power dynamics (suggested)
- Educate others on the historical context of "1984" and its relevance to contemporary global politics (suggested)

### Whats missing in summary

Exploration of how perpetual conflict for power impacts regions like North Africa, the Middle East, and parts of India.

### Tags

#ForeignPolicy #PowerDynamics #1984 #ControlSystems #PerpetualConflict


## Transcript
Well, howdy there, internet people.
It's Bo Young.
So today, we are going to talk about the region in 1984,
and how what happened in 1984 can inform our foreign policy
views of the region today.
And I know the history people right now,
you're sitting there thinking, what happened
that was so important in 1984?
No, I don't mean what happened in the year 1984.
I mean in the book, 1984, George Orwell's dystopian
masterpiece, widely regarded as the crowning achievement
of that genre.
Why?
Why is 1984 different from the hundreds, if not thousands,
of other books providing commentary on the same topic?
People are going to say, well, it's
because he really gets into the control systems.
You know, he talks about Big Brother.
He talks about the telescreen.
Yeah, if Orwell only knew we'd be carrying our telescreen
in our pocket.
He talks about the two minutes of hate.
And if you are one of those people who is constantly just
preoccupied with the outrage of the day that the media gives
you, I've got some bad news for you.
You are one of the people in 1984 who, well,
You can't rebel until you become conscious, and you can't become conscious until you rebel.
As long as you are stuck thinking about the outrage of the day, well, they've got you.
And that's what people point to, but here's the thing, all of those other dystopian novels,
they talk about the control systems too.
That's not it.
The next answer you probably get is world building,
because 1984 is immersive.
I mean, you have national anthems,
you have maps showing the geography,
you have a glossary of terms.
It is immersive.
But here's the thing, the maps, the world building
that occurs, the reason it's seen as better
because it resonates. Why does it resonate? Because it's true. Because you
can see it in the real world. The foreign policy that occurs in 1984, well, I mean
that's commentary too and it is just as good as the commentary on the control
systems that keep a population in line. It just gets overlooked because it's
kind of an afterthought. What is the the what's the foreign policy situation? You
have three super states, right? Oceania, East Asia, and Eurasia, and they're
constantly at war, ever-shifting alliances. I mean it would not be out of place for
East Asia to drop a nuke on Eurasia and then just a little while later be
outlied with it. Looking at Japan and the United States right now. It resonates
because it's real. And the rules that kind of govern their behavior, it's
commentary on real foreign policy rules. The ever-shifting alliances, stuff like
that. And that brings us to the questions that prompted this video. There are two.
One, is people became aware that the Saudis and the Jordanians helped defend Israel, and
that prompted a whole lot of questions, normally about the Saudis.
Why would they do that?
Here's the thing.
The three super states in 1984, they're constantly at war, right?
But the war doesn't end.
It's a literal forever war.
In fact, some suspect that the war isn't meant to be won.
It's meant to do what?
Maintain power.
Sure, each one of those three nations, they rise and decline as things move along, but
they're always one of the top three, right?
In the Middle East, you have three regional powers, Israel, Saudi Arabia, and Iran.
If Iran was able to hit Israel effectively while Saudi Arabia was normalizing relations,
might cut into their power.
So, well, we've always been at war with East Asia, yeah, we'll help.
I know some people are going to want to say that, well, it's also the religious thing.
Control systems.
It's control systems.
I'm not saying people in Saudi Arabia are not devout.
I'm saying that those people at the top, those policy makers,
I mean, tell me you've never partied with rich Saudis without telling me.
I don't even think some of them know what the word haram means.
That's another control system.
That's not really what it's about.
It's about power. If you're not picking on the Saudis,
there's a nation that people would like to say is predominantly Christian,
and focuses on Christian values, but
Do the elite of that nation, do they tell you to love your neighbor or do they give
you two minutes of hate?
It's a control system.
That is how it is used by those in power.
So that's what it's about, maintaining the regional superpower status of those three
nations.
That's why it happened.
don't have to overthink it, it's foreign policy. You know what it was about. Power.
It always is. Every decision, when it comes down to the end, the decision was
made based on preserving or gaining or protecting a nation's power. It's always
what it's about. The other question that came in had to do with an Israeli
apparently saying the quiet part aloud, said something to the effect that they
can't accept an equation where they hit something in Syria and Iran hits them
directly. Wow, you can't say that. I mean, not in public. Go back to 1984. You have
the three super states and they're always fighting, right? They're always at war,
But, are they attacking each other?
Not really, I mean, there's the little, you know, tiny stuff.
Some in that world believe that those smaller attacks are even allowed.
They're permitted.
They're allowed to happen to maintain like a siege mentality, keep the power domestically.
those super states, they don't fight in their own territory. I mean if you're a
superpower you don't allow that to happen. Where do they fight? North
Africa, the Middle East, a little bit of the area around India. That's where they
fight. That's where the war takes place. Because if you are powerful you don't
want it to occur on your soil. That's why they said that. Because that's true
commentary as well. That also probably had to do with the Saudi decision. I mean
think about it. If a regional conflict popped off, I mean there's a
pretty high likelihood that it would impact them in their territory and they
don't want that. So the questions again, when you get to the end of it, it's about
power. If you are bored and you want to take a look at something through a new
lens, maybe reread 1984 and focus on not the control systems, not the underlying
story, none of that stuff, but focus on the foreign policy and how it's viewed because
the inter-party members, they know that war is not going to be won. They don't really
want it to be either because that destabilization that occurs, well, it helps maintain power.
I mean, the people in those areas, North Africa, the Middle East, parts of India.
Yeah, I mean, it's bad for them and there are valuable resources there,
but as long as they're destabilizing the other super states more than it hurts you,
well, it's a win.
Remember what we said about Iran's view of the Palestinians.
That is why 1984 resonates, because all of the world building, it's also commentary,
and it resonates on a subconscious level.
You see it, because just like the commentary about the control systems, it's decent commentary,
even after all this time.
Why is it still good?
Because the end motive of foreign policy hasn't changed.
It's power.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}