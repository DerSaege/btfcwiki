---
title: Let's talk about Trump and Biden debating or not....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=vMxMtAkccLc) |
| Published | 2024/04/15 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- News agencies sent a letter urging commitment to presidential debates between Trump and Biden due to the benefits for ratings and the bottom line.
- The RNC banned its candidates from participating in debates organized by the non-partisan commission on presidential debates in 2022, raising doubts about the debates' occurrence.
- Trump initially expresses eagerness to debate Biden but may eventually avoid it by citing the RNC's ban, despite having control over the organization.
- Trump may resist debating due to concerns about his public image differing from 2016, especially when going off-script.
- Despite claiming Biden is afraid to debate, Trump might be the one with objections when pressured, potentially leading to the debates not happening.

### Quotes

- "He named the time and place and I will be there because I'm a stable genius and I've got this."
- "My guess is that he doesn't want to because, I mean let's be real, he is not who he was in 2016."
- "He will definitely be like, 'oh, Biden's afraid to debate me,' but when push comes to shove, it'll be him with the objections."

### Oneliner

News agencies push for Trump-Biden debates, but RNC ban raises doubts; Trump's eagerness may fade due to image concerns, potentially leading to no debates.

### Audience

Political Observers

### On-the-ground actions from transcript

- Pressure politicians to commit to participating in debates (implied)
- Stay informed on developments regarding the debates (implied)

### Whats missing in summary

Insight into potential impacts of the debates on the election cycle.

### Tags

#PresidentialDebates #Trump #Biden #RNC #PoliticalAnalysis


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about debates,
presidential debates, Trump and Biden.
And what's going on with that?
Because what occurred was a bunch of news agencies
basically sent out a letter saying,
hey, y'all need to commit to having these
presidential debates.
Let us know what's going on.
And the reason the news outlets did that is obvious. The ratings, it's good for
it's good for the bottom line to have those debates. That has something to do
with it. The other thing is that it's kind of in question whether or not
they're going to occur. Why? Because in 2022 the RNC banned its candidates from
participating in debates that were organized by the the non-partisan commission on presidential
debates. The organization that has handled presidential debates for decades, but in 2022
the RNC said, no, we're not going to do that anymore.
Okay, so what's going to happen?
Initially, Trump is going to do what I think he's already doing,
being like, I want to debate Biden so, so bad.
I want that.
He named the time and place and I will be there because I'm a stable genius and I've got this.
And then it'll probably turn into the Republican primaries.
where he ran from the debates because he knew if he got on stage with Haley or
DeSantis, well we'd probably have a different presumptive nominee. And then
after he, you know, basically calls Biden out, he'll say, you know, I'd love to
debate but not with that organization because, you know, the RNC said I couldn't.
I mean that sounds good and all except Trump has control of the RNC. Trump has
control of the RNC. Remember he installed his people up at the top of it? He could
change that rule if he actually wanted to debate. My guess is that he doesn't want
to. Now he may eventually get pressured into it but my guess is that he doesn't
want to because, I mean let's be real, he is not who he was in 2016. We've all seen
him go off script. When that is broadcast to people other than his base, it's not
going to go over well and he knows it. So my guess is that he's going to put up a
lot of resistance to this. He'll say otherwise, you know. He will definitely
be like, oh, Biden's afraid to debate me, but when push comes to shove, it'll be him with the
objections. Hopefully, the pressure that mounts will overcome that, but that's what's going on.
That's why there's a question about it. We'll have to wait and see how it plays out, though.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}