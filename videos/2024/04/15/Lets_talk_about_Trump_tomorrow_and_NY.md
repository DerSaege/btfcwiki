---
title: Let's talk about Trump, tomorrow, and NY....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=bHQwe88X6lA) |
| Published | 2024/04/15 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the upcoming trial regarding Trump in New York, clarifying misconceptions.
- Emphasizes that the case is not solely about hush money but falsified financial records.
- Mentions the importance of understanding the motive behind falsifying records.
- Points out that the case involves potential felony counts for Trump, not the exaggerated reports of over a hundred years in prison.
- Expects the trial to last six to eight weeks, predicting intense media coverage due to its criminal nature against a former president.
- Plans to provide condensed summaries on the channel due to the extensive coverage and speculations.
- Advises viewers to be cautious of speculations and wait for actual events to unfold.
- Mentions Trump's claims and uncertainties about how they will impact the case.
- Assures that despite covering the trial, the channel won't turn into "Trump TV" for the next two months.

### Quotes

1. "This is not a hush money case."
2. "The question at hand really deals with whether or not business financial records were falsified."
3. "This is a criminal case against a former president of the United States."
4. "Just wait and see what occurs."
5. "We are not turning this into Trump TV for the next two months."

### Oneliner

Beau explains the upcoming criminal trial against Trump in New York, focusing on falsified financial records, potential felony counts, and intense media coverage without turning it into "Trump TV."

### Audience

Legal analysts

### On-the-ground actions from transcript

- Wait for actual events to unfold (implied)

### Whats missing in summary

Context on the potential implications of the trial beyond the media coverage.

### Tags

#Trump #NewYork #CriminalTrial #FalsifiedRecords #MediaCoverage


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we're gonna talk about tomorrow.
Today, we are going to talk about tomorrow.
Trump, New York, run through some basics
because in the run-up to this,
there's been a lot of coverage that has gotten away
from what the key elements actually are.
So we'll just kind of run through
what this is actually about.
We will talk about the potential downstream effects,
how long it's going to last, all of that stuff.
OK.
So of course, we are talking about the, quote,
hush money case.
This is not a hush money case.
That's not what this is about.
That's just kind of a way that it's been talked about.
But that's not what the jury is going to be looking at.
The question at hand really deals with whether or not
business financial records were falsified.
And if they were, if the jury determines that they were, why?
And the why becomes a big part of it.
That's really what this is about.
Now, obviously, as I'm sure you know, there are a lot of other things that are connected
to this case that are far more interesting than financial records.
And there's going to be a lot of focus on that stuff, but that's not really what the
case is about.
You know, actress Stormy Daniels, obviously there's going to be a lot of focus on any
conduct that occurred with her, but that's not really what the case is about.
Now as far as downstream effects, Trump is looking at, I want to say 34 felony counts.
I have seen reporting saying that he's looking at a hundred and thirty something years or
something like that. That's not how this works. I've talked to somebody who is
very familiar with sentencing up in New York and basically he's looking at
probation to three years. That's really what this is, what a likely sentence
from this would be. The trial itself, looking at six to eight weeks, it is
going to be an absolute media circus. Because unlike some of the other
entanglements that have gone before, this is not civil. This is a criminal case
against a former president of the United States. For the next six to eight weeks,
weeks, expect a whole bunch of coverage when it comes to this. As far as here on
the channel, we will probably try to condense it all into something that
provides summaries rather than covering every single event because there's going
to be a lot. And the other thing is there's going to be a lot of speculation.
Remember that the speculation you see on your screen doesn't necessarily depict
what's happening. So just wait and see what occurs. There have been claims by
Trump. I don't know how that's going to play out as things move forward.
but we'll watch it just to understand we are not turning this into Trump TV for
next two months. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}