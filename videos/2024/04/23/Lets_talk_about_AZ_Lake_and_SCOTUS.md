---
title: Let's talk about AZ, Lake, and SCOTUS....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=3aVOcsr-F3c) |
| Published | 2024/04/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Arizona, Kerry Lake, and the Supreme Court are part of a long-running story coming to a close.
- Carrie Lake and another candidate alleged issues with voting machines before the 2022 midterms.
- The case went to court in Arizona, then the Ninth Circuit, and finally to the Supreme Court.
- The Supreme Court, despite being conservative, declined to hear the case.
- The Ninth Circuit stated that the allegations did not support a plausible inference of future election votes being affected.
- This decision by the Supreme Court should mark the end of this storyline.
- Lake's campaign, like many Republicans, has made claims about the election.
- The outcome of this case could negatively impact Lake's campaign in terms of fundraising and enthusiasm.
- The case ending abruptly may not be beneficial for energizing her base.
- While this is a bump in the road, it might not derail Lake's campaign completely.

### Quotes

1. "The incredibly conservative Supreme Court has declined to hear it."
2. "This should be the end of this storyline."
3. "Probably going to reduce the amount of publicity that she gets."
4. "This is definitely a bump in the road for the Lake campaign."
5. "It's probably going to impact fundraising and enthusiasm for the Lake campaign."

### Oneliner

Arizona's long-running story with Kerry Lake and the Supreme Court concludes, potentially impacting Lake's campaign's fundraising and enthusiasm.

### Audience

Arizona Voters

### On-the-ground actions from transcript

- Stay informed about the developments in Arizona politics and elections (implied)
- Support campaigns and candidates that prioritize transparency and integrity in elections (implied)

### Whats missing in summary

Insights on the potential implications of recent developments in Arizona on upcoming elections.

### Tags

#Arizona #KerryLake #SupremeCourt #ElectionClaims #CampaignImpact


## Transcript
Well, howdy there internet people, it's Beau again.
So today, we are going to talk about Arizona
and Kerry Lake and the Supreme Court
and a very, very, very long running story
and a very, very, very long running story
that appears to finally be coming to a close.
So we will talk about that
and the implications for Lake's campaign.
Okay, so back prior to the 2022 midterms, Carrie Lake and another candidate were involved
in a suit, and basically it was alleging something was wrong with the voting machines.
Went to court in Arizona, and they did not get the result they were looking for.
Took it to the Ninth Circuit.
not get the result they were looking for. Ticket to the Supreme Court, and the Supreme Court has
declined to hear it. This Supreme Court has declined to hear it. The incredibly conservative
Supreme Court has declined to hear it. At the Ninth Circuit, let's see, it says, in the end,
none of plaintiffs' allegations supports a plausible inference that their individual
votes in future elections will be adversely affected by the use of electronic tabulation,
particularly given the robust safeguards in Arizona law,
the use of paper ballots, and the post-tabulation retention of those ballots.
With the Supreme Court deciding not to take it up, this should be the end of this storyline.
Lake, like many Republicans, has made a number of claims about the election.
And in some ways, this is probably going to hurt the campaign a little bit.
Her personal view of this case, I don't know.
I don't know what she thought was going to happen.
Personally, I don't think that she believed she was going to win this at the Supreme Court.
It seemed more like an energizing thing for the campaign, something to get the base fired up about.
This ending now, instead of being prolonged, is probably not good.
It's probably going to impact fundraising and enthusiasm for the late campaign.
pain. We don't know that yet, but that would be my assumption. There were a lot
of people who were very excited about these sorts of claims. It not being heard,
it's probably a pretty big letdown for those people who were supporting her for
that reason. So this is definitely a bump in the road for the late campaign. I
don't think it's gonna send them off the road though. This is probably not as much
of an issue as some of the other recent developments in Arizona. Those are
They're probably going to weigh way more heavily on voters' minds when they actually go to
the polls, but this is probably going to reduce the amount of publicity that she gets and
the enthusiasm among that core fired-up base.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}