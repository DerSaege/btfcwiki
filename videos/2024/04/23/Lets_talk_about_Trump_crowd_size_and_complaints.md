---
title: Let's talk about Trump, crowd size, and complaints....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=5DB26QnRVHk) |
| Published | 2024/04/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump was expecting a large turnout of supporters for certain proceedings, but the actual turnout has been dismal, with only a small number of supporters showing up.
- There are more people showing up to mock Trump than there are actual supporters present.
- Trump seemed to suggest that law enforcement might be keeping his supporters away, leading to the low turnout.
- The lack of support at recent events may be due to past instances where supporters were told it was a trap or infiltrated, leading to skepticism and reluctance to attend.
- The highly energized base of Trump supporters is not as large as it was in previous elections, and this lack of enthusiastic support is evident in the low turnout.
- Trump is known to care deeply about crowd sizes, and the smaller-than-expected turnout is likely bothering him.
- Despite some supporters being present, the numbers are significantly lower than anticipated, likely disappointing the former president.

### Quotes

1. "The highly energized base of Trump supporters is not as large as it once was."
2. "Trump is known to care deeply about crowd size."
3. "There are more people showing up to mock him than his actual supporters."

### Oneliner

Trump's anticipated large turnout of supporters has turned into a dismal reality, with more people mocking him than supporting him, reflecting the waning enthusiasm among his base.

### Audience

Political analysts

### On-the-ground actions from transcript

- Attend political events to show support or opposition (implied)
- Encourage others to participate in political activities (implied)

### Whats missing in summary

Insight into the potential impact of dwindling support on Trump's future political endeavors.

### Tags

#Trump #Supporters #CrowdSize #PoliticalAnalysis #Enthusiasm


## Transcript
Well, howdy there, you want to know people?
Let's bow again.
So today we're going to talk about Trump and crowd sizes and what he was hoping
for and what isn't occurring and how that is leading to some interesting
developments.
Trump apparently was envisioning a whole lot of his supporters showing up for
these proceedings. If you don't know
the turnout of his supporters, I mean
dismal would be the only word that I could think of.
It could be counted in dozens.
Not huge crowds. From what I've seen
those people showing up
And to mock him are outnumbering his supporters that are showing up.
Now apparently he kind of indicated that maybe he believed that NYPD or something was keeping
his supporters away.
And that's why there weren't big crowds of supporters there for him.
A news crew went outside and filmed the roads.
It wasn't shut down, all of that stuff.
And there have been a couple of questions that came in today asking why his supporters
aren't showing up.
Would you?
I mean, really think about it, and don't think about it from your perspective.
Think about it from the perspective of somebody who is super energized by Trump,
one of the real big supporters.
Would you show up?
What happened last time?
And again, don't look at it from the perspective of you.
Look at it from the perspective of one of his big supporters, somebody who
believes all of the information in the information silo that exists.
The last time there was a giant gathering in support of Trump along these lines, they
were told that it wasn't really them, that it was infiltrated, that it was a trap.
Why would they show up?
It's one of those cases where the rhetoric that Magelworld used to excuse the events
of the 6th are probably coming back to bite them.
My guess is that those who are most energized by Trump don't want to be around a bunch
of quote deep state people.
So they're they're not showing up.
And then the other thing is the part that he doesn't want to admit.
His highly energized base is not as big as it once was.
It's not 2016.
It's not 2020.
There aren't as many of them, and in that area, there are fewer.
He might have a bigger turnout in a place that is more reliably in favor of him, but
that's kind of not true either, because he didn't have a big turnout with any of the
other cases.
It's just the support, that kind of support isn't there.
That energized base that's willing to show up and stand there while he's inside a building,
it just doesn't exist the way that it used to.
And then I would imagine that those who are still that fired up about him and those who
are still that energized, they don't want to go there because they believe what they
They were told by supporters of Trump that it's obviously a trap, that it's the deep
state, that it will be just like what happened before and they don't want any part of it.
Again, this is one of those things that is a lot like falling off that list.
Trump really cares about crowd size.
It's a big thing to him.
It has been for years and years and years.
This is one of those things that is going to kind of, it's going to gnaw at him.
going to bother him. You know the first part of it during selection and that was
probably excusable in his mind but now that opening statements have occurred
he probably pictured a lot more of his you know red hat waving fans to be out
there. And they're not. Not in any big numbers. Don't get me wrong, there are a few. But I
doubt it was more than a hundred. Probably going to bother the former president. I would
imagine more tweets encouraging people to show up. And the more he tweets, the more
he might be disappointed by the outcome.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}