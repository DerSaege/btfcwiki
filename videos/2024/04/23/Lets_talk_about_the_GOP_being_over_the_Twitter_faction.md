---
title: Let's talk about the GOP being over the Twitter faction....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=s8UFL3aALhU) |
| Published | 2024/04/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Republican Party in the US House of Representatives is facing turmoil due to some colleagues' antics.
- There are calls for Mike Johnson to resign as Speaker of the House.
- Johnson's faction is strategically positioning him to potentially receive Democratic Party support to maintain his speakership.
- Messages are being crafted to portray bipartisanship positively and blame the Twitter faction for hindering legislative progress.
- The base is being prepared for the idea of Johnson needing Democratic votes to retain his position.
- Democrats are keeping their plans close to the chest, potentially to make Johnson uneasy.
- The Democratic Party may benefit from Marjorie Taylor Greene winning, causing further chaos in the Republican Party.
- Speculation arises about whether Johnson was promised Democratic votes in exchange for aid, showcasing missed opportunities for legislative priorities.
- Beau suggests that leveraging could have led to achieving more legislative goals instead of focusing on social media traction.

### Quotes

1. "Don't talk about it, be about it."
2. "The only way that we can move forward is through bipartisanship."
3. "If it weren't for the Twitter faction, the Republican Party probably could have got a lot in exchange for the aid package."
4. "If he retains the speakership, he retains it. If he doesn't, he doesn't."
5. "But I mean, I guess Twitter likes are good too."

### Oneliner

Republicans navigate internal strife, considering bipartisan approaches and potential Democratic support for Speaker Johnson.

### Audience

Political observers

### On-the-ground actions from transcript

- Support bipartisan efforts to move legislative priorities forward (implied)
- Stay informed about political developments and potential shifts in leadership (suggested)

### Whats missing in summary

Insights on the potential consequences of the Republican Party's internal struggles and the impact on legislative outcomes.

### Tags

#RepublicanParty #SpeakerJohnson #Bipartisanship #LegislativePriorities #DemocraticSupport


## Transcript
Well, howdy there, Internet people. Let's bow again.
So today we are going to talk about how the Republican Party
in the US House of Representatives is just totally over
the antics of some of their colleagues and how
some Republicans are starting to message
to set the scene in case
Johnson needs Democratic Party votes
retain his speakership. Okay, so what's going on? First, you have the space laser
lady out there saying stuff like, Mike Johnson's speakership is over. He needs
to do the right thing to resign and allow us to move forward in a controlled
process. If he doesn't do so, he will be vacated. Don't talk about it, be about it.
it. Do it. You had the chance. You haven't done it yet. You could have done that before the recess,
but that didn't happen. But it doesn't look like Johnson's faction is willing to
leave anything to chance. They are setting the stage for him to be able to take
support from the Democratic Party to retain his speakership without backlash
from other Republicans. How are they doing that? Basically explaining exactly
what the Twitter faction has been doing. Okay, so what do we have? We have a
number of comments. One, the members who screamed the loudest about border
security were actively and knowingly preventing us from getting it done.
That's definitely about the Twitter faction.
Another member of Congress, they're making us the most bipartisan Congress ever because
they are unwilling to compromise just a little bit in a divided government.
They force us to make bigger concessions and deals with the Dems.
So you have Johnson's faction of the party now getting out there and messaging, saying,
being bipartisan is actually good.
Had we done that, we might have been able to get that border security thing in when
we did the aid package.
But because of the hardliners, because of the Twitter faction, because of the need to
create social media clicks rather than advance policy, we weren't able to get that.
We could have got something and we got nothing, and it's their fault.
The only way that we can move forward is through bipartisanship.
I would expect this type of messaging and rhetoric to increase over the next week.
And this is all to get the base ready to accept the idea of maybe Johnson needing democratic
support to retain his speakership.
Keep in mind, at this point in time, we don't even know that that's something that would
happen.
There's been signals, but we don't really know yet.
It could be Johnson just preparing in case that happens, or he could know
something that we don't. We don't know yet. Only look at the actions, don't try
to guess the motive. But it certainly appears that they are setting the stage
and getting the base used to the idea of bipartisan is good. And the Twitter
faction is actually the reason the Republican Party has failed when it comes to advancing
its legislative priorities.
Now, outside observer, is that true?
I mean, yeah, it is.
It's a true statement, it really is.
If it weren't for the Twitter faction, the Republican Party probably could have got a
lot in exchange for the aid package, particularly the aid package for Ukraine.
They could have leveraged that.
But because certain elements within the Republican Party were very, let's just call it intractable,
they didn't have any leverage.
So what they're saying is true.
The reason they're saying it is probably to ready the base for something either they
hope will happen or they know will happen.
How long this goes on is another question.
If the base accepts the idea that Johnson needs a few votes from Democrats to keep his
speakership or whatever, they may just let it go, or they may move in and
really hammer it home in an attempt to
further remove the influence of the
Twitter faction. Now, if you're on the
Democratic side of the aisle, what's the rumors there? There aren't any. They're
being very tight-lipped about what they're going to do. It seems like
they're trying to make Johnson sweat. If I was Jeffries, I would say vote your
conscience and just let people decide. If he retains the speakership, he retains
it. If he doesn't, he doesn't. For the Democratic Party, it would be best if
Marjorie Taylor Greene won. It would further throw the Republican Party into
disarray. But that requires the Democratic Party playing hardball and
they don't tend to do that. The other thing is we don't know if Johnson was
promised the votes to retain his speakership to get that aid, which he very well may have
been, which kind of goes to prove the point that the Republican Party could have got stuff
in exchange for that aid.
They could have legislated some of their priorities if they had used that leverage.
But I mean, I guess Twitter likes are good too.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}