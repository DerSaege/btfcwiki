---
title: Roads Not Taken EP 36
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=B70R0aNwuR8) |
| Published | 2024/04/28 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recap of unreported or underreported news events from April 28, 2024.
- Updates on foreign policy, including China-US meetings, Gaza relief ships, and ceasefire negotiations.
- Tensions within the United States over the potential suspension of military aid.
- Governor Kristi Noem's controversial act of putting down a puppy.
- Trump's push to eliminate the filibuster facing resistance from Republican senators.
- LAPD issues a citywide alert over demonstrations at USC.
- News on net neutrality, non-compete agreements, and a viral sermon by Pastor Livingston.
- World Health Organization study shows vaccines saving millions of lives.
- Oddities involving Trump presenting a key to the White House and US intelligence on Putin.
- Q&A session covering topics like political activism, two-state solution for Israel-Gaza, and media coverage bias.

### Quotes

1. "Okay, going to foreign policy."
2. "How do you channel the rage that comes from being a leftist in a deep red area?"
3. "It's not a magic pill that changes everything immediately."
4. "Attention, right? People have to be aware that something is happening."
5. "Definitely desensitized."

### Oneliner

Beau provides updates on foreign policy, US news, controversy, and oddities, addressing audience questions and shedding light on media biases and desensitization.

### Audience

Policymakers, activists, news consumers

### On-the-ground actions from transcript
- Support Ryan Hall's fundraising for tornado victims at yallsquad.org (suggested)
- Stay informed about political developments and global issues to advocate for change (implied)

### Whats missing in summary

Exploration of media bias and desensitization's impact could be better understood through the full transcript.

### Tags

#ForeignPolicy #USNews #MediaBias #Activism #Desensitization #GlobalIssues


## Transcript
Well, howdy there, internet people, it's Bo again,
and welcome to The Roads with Bo.
Today is April 28th, 2024,
and this is episode 36 of The Roads Not Taken,
which was a weekly series
where we go through the previous week's events
and talk about news that was unreported, underreported,
didn't get the coverage I thought it deserved,
or I just found it interesting.
Then we go through some questions
from y'all picked by the team.
And we're going to start off by saying hi to Nolan.
Good luck.
Okay, going to foreign policy.
China and the United States have had some meetings.
There's a lot of posturing and positioning
when it comes to economic concerns.
A lot of big talk, not a lot of big developments,
at least none that have been released at time of filming.
There may be more news about that coming later.
Ships planning to leave Turkey to head to Gaza with relief supplies have been delayed after an inspection.
And they had their flags yanked.
Without intervention,
that's going to be a lengthy delay if there's not pressure from somewhere to figure out exactly what went on.
At time of filming, there is a lot of
conflicting reports as to what exactly occurred.
word, none of it seems normal by any real definition.
At time of filming, ceasefire negotiations are still underway.
Blinken is on his way to Saudi Arabia.
Israel has for the first time indicated a willingness to discuss a sustainable calm,
is the term they're using for it.
everybody else, that's a long-term ceasefire. We'll see how that plays
out. The United States and Israel are reportedly reviewing remediation plans
about the unit that was considered for a suspension of military aid. When we
talked about it the first time, we talked about how if those acts were
suspected or they occurred and things were done to prevent it happening in the
future, well, it doesn't impact US policy. Remediation plans means that the US
brought the issue to their attention and they're willing to engage
in some kind of changes. What those are? Don't know. The reviewing of the plan
means that this is still in the early phases of it. Now my understanding is
that the atmosphere at State is, let's just call it tense, and that there are
multiple schools of thought about how the United States should proceed and it
is, there's a lot of discussion going on right now. And the, my guess is that we
We will find out a little bit more about which direction the U.S. is going to head this week.
Moving on to U.S. news, Governor Kristi Noem, who is a potential VP pick for Trump, put
down a puppy.
Normally, saying something like that would be some kind of euphemism for a bad political
act or something along those lines. In this case, no, that's quite literal. The
act first came to light when she disclosed it in what appears to be an
attempt to show that she would be willing to get her hands dirty. The
reporting from the Guardian indicates that they got an advanced copy of her
autobiography, and it is in that. And oddly enough, it doesn't end there. The
puppy was, I guess, according to the story, wasn't well-trained. It was not
yet a well-trained dog. Then there was a goat as well, and according to the
reporting, at least part of that reason was that the goat smelled bad because goats are
you know known for smelling good.
Yeah, the motivation behind this and the response, it does not seem to be having the effect that
she I guess thought it would.
So I would imagine there's going to be a lot more discussion about that one as well.
Trump apparently wants the filibuster gone in the Senate.
It appears that Republican senators are like marshaling forces and locking arms to say
no.
And they are building a coalition to basically just tell him that's not on the table.
Now Republican members of Congress have been very supportive of the right-wing media going
after the space laser lady, cheering it on, sharing it on social media.
It does not appear that her move against Johnson is going very well at the moment.
LAPD issued a citywide tactical alert over the demonstrations at USC.
Donald Payne of New Jersey's 10th district, I think, died this week.
Net neutrality has been restored by the FCC.
The FTC banned most non-compete agreements.
If you are under one, you might want to look into that coverage.
In cultural news, Pastor Livingston just ripped Trump and that Bible apart in a now viral
sermon about the place of politics in religion and vice versa.
That was quite a speech.
In science news, a World Health Organization study indicates that in the last 50 years,
vaccines have saved about 154 million people.
101 million of those would have been infants.
about China's version of the stealth bomber came out. It was not good
information and it has led defense analysts to dub it what happens when you
order your B2 from Wish. It does not meet the standards or capabilities that
people thought it was going to meet. A study indicates those treated by female
doctors live longer. I honestly cannot wait to see how the right wing responds
to that in the United States. Something, something, feminism or DEI or something.
I would imagine that's going to be a a culture war nonsense topic for next week.
In oddities, Trump was mocked repeatedly after engaging in an incredibly bizarre ceremony
in which the former president, again, the person who is not president and has not been
for years, presented a key to the White House as a gift.
It was unique, and in another incredibly weird development, US intelligence indicated that
they didn't believe that Putin was behind the extrajudicial death of a political opponent.
Peskov, who is a Putin ally and spokesperson at the Kremlin, said, I would not say this
This is high quality material that deserves any attention to some very empty reasoning.
So to be clear, the Kremlin is saying that the U.S. intelligence report clearing Putin
is apparently based on bad reasoning.
I've got to be honest, I did not see that one coming.
OK, moving on to the Q&A. OK, why did it not pass until now, after five years?
This is obviously to alter the flow of information on Gaza, even if they were trying before,
they expedited it now.
But they didn't.
It's not expedited, it's actually longer.
This doesn't go into effect, it's a year.
It's a year.
The previous attempts actually had a shorter time frame.
It's, yeah, okay, I'm writing again because I'm still wondering as to why you never talk
about the influence of religion on the situation in Israel, Gaza.
same reason I never talk about the flags.
It's an easy defining line, but it's like many things on the foreign policy scene, it's
not actually the motivation.
It's a tool to either motivate one populace or, you know, other another.
It's not...
I'll try to do a video about it, but generally speaking, you do have some exceptions to this.
But most times when you are talking about religion and its purpose and conflict, it
It's a tool used by those in power.
It's not really the cause.
It's a method of marshaling support.
How do you channel the rage that comes from being a leftist in a deep red area?
How do you push that hate into a blender and chop it up into a smoothie of love?
Much like the question the other day over on the other channel, you've got to remember
sometimes you don't have a choice.
You can be angry all the time, but it doesn't do any good.
what I do, I have to focus on pulling out the teachable moments and stuff like that.
Being angry in many cases is a wasted emotion.
You can use it to motivate you, but beyond that, if you let it get to you, it burns you
up.
So you have to either use it as a motivation, I guess channel it into the blender, or find
a way to let it go.
How do I do it?
I don't have a choice.
I mean, if I came and ranted every time something in my immediate area bothered me, it would
be a very different channel.
Ever thought about telling these people the truth?
Leave activism and go straight into politics.
the middle and focus your younger years on building a political career to make
the actual changes you need. I believe in a diversity of tactics. No, that's
not electoral politics again to me least effective form of civic engagement. Not
ineffective, but least effective. There are, without the support structure that
exists outside of it, the campaign doesn't go anywhere. People have to be out there
getting the message out in different ways for any political campaign to work.
So I don't believe this is an either-or, it's more of a yes-and. Yes, if you want
to have a progressive platform, you need progressive politicians to carry it
forward, but they're not going to get through their primary if they don't have
activists out there really pushing those issues. It's... you need both. Just wanted
to share this with you, Ryan Hall is fundraising for victims of the tornado outbreak this weekend
at, they all squad, oh, it's Ryan, the y'all squad, the y'all squad dot org.
They all squad, okay, yeah, okay, well, I mean, I guess that's it, that's the information
that needed to get out right there.
Ah, I had already pulled this to do a full video on it.
So let's say that everything goes perfectly and the two-state solution happens.
Let's even include that some of Hamas gets representation in the new Palestinian state
and turns over a new leaf.
won't residual Hamas and other individuals who can't let go of the ancient grudge against
Israel still form groups within the Palestinian state, get funding from Iraq, and continue
to lob drones at Israel, causing the whole situation again.
I don't understand how the two-state solution solves anything at all.
I don't see how we can break the cycle.
Okay, so the first thing is that it's not going to stop immediately.
It is not a magic pill that changes everything immediately.
There will certainly still be elements that won't let go.
However, from the point of it actually being a functioning state, it should, theoretically,
be handled internally among Palestinians.
That's how that would play out, as it has in other places that have gone down the route
of the Omar moment where you have that moment where peace is possible and they seize on
it, the reason those moments occur is because people have grown tired of the violence.
And generally speaking, something really bad happened.
And so it's not going to end immediately, but it would be handled very, very differently.
And over time, the value of those groups would diminish to the Iranian policymaker.
Therefore they would get less funding and it would be a downhill decline instead of
a cycle that continues to feed, one action feeds the next, in theory it would be a situation
where an event occurs but it's handled jointly and over time you end up with a situation
where economic ties develop and there's more movement.
This is especially true, one of the critical things about this and this is why the aid,
quote, dump trucks full of cash are so important, is that areas within that
state would have to be allowed to flourish. Trade would have to go in and
out. The economic stability is incredibly important because it
reduces the appeal of certain groups.
So it's not a thing where, OK, they've signed a deal.
Now there's never another shot fired in anger
or anything like that.
It turns into a very gradual decline.
and you end up with a situation like in Ireland,
where it declines, it declines, it declines,
and then eventually you have that moment
where there's a shift to where it isn't the norm.
It's important to remember that Irish Republican groups still exist.
They just don't have as much broad support and it tones things down and generally over
time the two sides, they realize that they're not that different and that what they were
we're fighting for, it may not be worth everything that it's caused.
So yeah, it's not a situation where it's an immediate end, but it's the step that
has to be taken to get to any, I guess the terminology right now is sustainable calm,
world peace.
Food insecurity and famine are concerns for many people across the globe.
What sets the visibility of the plight facing Gaza above places like Sudan and other places
in Africa?
A built-in audience in the West?
It's probably not the answer people want to hear, but that is the answer.
Okay, so what generates outcry?
Attention, right?
People have to be aware that something is happening.
To be aware of it, it has to make it onto the news.
It has to be covered, or a movement has to pick it up
and make it their cause, you know,
that they're gonna push it out there.
If it's not getting coverage,
the reason is because it isn't economically viable.
There's not a built-in audience for that cause.
So, it doesn't get coverage, it continues to not get coverage, and generally speaking,
when you really think back, a lot of times those situations go unresolved until it starts
to impact a built-in audience of some kind, and that's why the situation in Gaza gets
so much attention because there is a built-in audience that also has a highly polarized
topic which means people are going to continue to watch it.
It's really good for ratings.
And it's incredibly sad because that is the driving factor behind it because people
know that they can profit from it, and so it gets more coverage.
They know that if they plug the word Gaza into their title on YouTube even, that it's
going to get a lot of views, which yeah, and the same is true for major outlets.
They know that this topic, because of its highly polarized nature, because of the built-in
audience, it makes some money.
And therefore, people are more aware of what occurs, therefore they have stronger opinions
about it, and it just kind of continues in that way.
When you talk about the places that get overlooked, generally speaking, there isn't a built-in
audience in the West, and that really is the... that determines where the spotlight gets
pointed.
So are you defeated or desensitized?
Desensitized, definitely.
In a recent video, I talked about how viewing certain kinds of imagery over time, it's
going to do one of two things to you, and that you don't have to do that, and then
I made the comment that I have to do it because of what I do.
No, definitely desensitized.
Definitely desensitized.
the next question, how did you come back from being desensitized? I haven't. I haven't.
I mean without a doubt, low estimate over the last two years, a little bit more than,
I have probably watched 20,000 people go, and most of them watched it more than once.
I have not come back from being desensitized and I probably won't until I retire.
That is part of doing this job.
being desensitized, it leads you to have to be careful when you are talking to people
who aren't.
There are times when I'm sure that you all have picked up that I'm definitely trying
to choose my words carefully, and that has a lot to do with it.
There are still things that bother me when you're talking about the misapplication of
violence and stuff like that.
But even me, the reason this is coming up is because I was saying you don't have to
do that.
Yeah, I have to do it.
But even with what I do, I haven't done a lot of police use of force stuff lately because
it was not good for me to continue watching it.
So that's just not something I'm taking a break from covering that right now.
There are still things you can do to mitigate, but as far as coming back from it, I'm definitely
not.
That's what they chose to end on?
You've got to be kidding me, okay, I don't even have any filler, we've been really good
about ending these on a light note lately, but I guess not, or maybe they thought I was
going to make a joke out of that.
Yeah, okay, so yeah, this is us heading into next week and a lot of what's occurred over
the last five or six days seems to be building up to developments that might actually start
to alter some of the long-running stories.
Which way it goes, I don't know yet.
Anyway, so there's a little more information,
a little more context, and having the right information
will make all the difference.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}