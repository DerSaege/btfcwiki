---
title: Let's talk about Trump and RFK's dynamic....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=U9a7Q0umIIc) |
| Published | 2024/04/28 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former President Trump targets RFK Jr. on social media after suspecting Junior was taking more votes from him than Biden.
- RFK Jr. responds to Trump's attack, calling it a barrage of wild and inaccurate claims.
- RFK Jr. challenges Trump to a debate to defend his record.
- Junior's ability to hit Trump from the right due to his anti-establishment framing poses a threat to Trump.
- Trump is predicted to avoid debating RFK Jr., as he tends to run away from confrontations.
- The potential debate between RFK Jr. and Trump could have significant implications for Trump's base and his anti-establishment image.

### Quotes

- "When frightened men take to social media, they risk descending into vitriol, which makes them sound unhinged."
- "Let's hear President Trump defend his record to me, mano a mano, by respectful, congenial debate."
- "Junior being a newer face can point out Trump's record to his base in an incredibly effective way."

### Oneliner

Former President Trump targets RFK Jr. on social media, sparking a potential debate that could threaten Trump's anti-establishment image.

### Audience

Political observers, voters

### On-the-ground actions from transcript

- Contact political organizations to stay updated on developments in the potential debate (suggested)
- Engage in respectful political debates with others to understand different perspectives (exemplified)

### Whats missing in summary

Insights on the potential impact of the debate on Trump's base and anti-establishment image.

### Tags

#Trump #RFKJr #Debate #PoliticalStrategy #AntiEstablishment


## Transcript
Well, howdy there, internet people, it's Bill again.
So today we are going to talk about an incredibly interesting dynamic that is
beginning to emerge between former president Trump and RFK Jr.
We talked about it, but in case you missed it, it wasn't too long ago that,
Well, Trump, for whatever reason, the appearance is it seems like he got news that Junior was
taking more votes from him than from Biden, but he decided to go after Kennedy on social
media.
He went after Junior on social media.
R.F.K.
Jr. responded,
When frightened men take to social media, they risk descending into vitriol, which makes
them sound unhinged.
President Trump's rant against me is a barely coherent barrage of wild and inaccurate claims
that should best be resolved in the American tradition of presidential debate.
wants to get on the debate stage and Trump is probably not going to want that
because, I mean, let's be real, Junior kind of has Trump's number. So it goes on talking
about Trump, he said, he promised to end the Ukraine war and then colluded with
Speaker Johnson and President Biden to fund it.
He let big pharma and his corrupt bureaucrats run rough shot over him as president.
He promised to cut the deficit and ran up the biggest debt in history.
He promised to run the government like a business and then close down our businesses.
He promised to drain the swamp and then filled his administration with swamp creatures.
He promised to protect our rights and then torpedoed the Constitution.
Instead of lobbing poisonous bombs from the safety of his bunker,
let's hear President Trump defend his record to me, mano a mano, by respectful, congenial debate."
Trump is going to run from this dude.
He is going to run from him.
One of the things that the Democratic Party has not done well is actually highlighting
Trump's record.
See Junior doesn't have a problem doing that.
Because of the way he has positioned himself, he can hit Trump from the right.
And Trump, his whole framing is that, well, he's anti-establishment.
Your former President Trump, no you're not.
You're a billionaire, no you're not.
You are the establishment.
Junior being a newer face can point out Trump's record to his base in an incredibly
effective way. Trump is going to run from this guy. It is going to be very interesting
because if RFK can get him running, it probably won't stop. If Trump starts to lean into
to know I'm scared to debate him, I believe in this message I think Junior called him
panicked about debating him. If he starts going down that road, it'll be very hard
for him to turn around because he doesn't like to adjust course. But his first instinct
is going to be to do what he did during the primaries, which is run away from everybody.
He's not going to want to meet on the debate stage regardless of what he says.
This could be an incredibly interesting turn of events because a lot of Trump's base,
they believed his anti-establishment rhetoric.
If somebody successfully points out that that's literally not what he did and it resonates
with that base, Trump very well may lose them.
This little dynamic, if they get into a debate over social media, because again, I don't
think Trump's going to debate him in person, but if they get into a back and forth on social
media, it will echo in Trump's base, they'll see it.
This is a risk to Trump's campaign that he might think he can ignore, but he probably
shouldn't.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}