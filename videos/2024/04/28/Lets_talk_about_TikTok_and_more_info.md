---
title: Let's talk about TikTok and more info....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=kSyAzBa9tpU) |
| Published | 2024/04/28 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses TikTok and the questions that have arisen since the last video.
- Emphasizes the importance of gathering all information before assigning intent to something.
- Responds to a message questioning the justifications for the TikTok ban, mentioning information operations and Israel's control of the narrative.
- Mentions a video discussing Republicans pushing legislation regarding information operations and the potential impact on TikTok.
- Talks about an executive order from the President of the United States regarding TikTok and its connection to national security.
- Points out that events surrounding TikTok are not new and have been in progress for half a decade.
- Suggests researching countries that have banned or restricted TikTok for more context.
- Brings up concerns about data harvesting, information operations, and countries adversarial to China in relation to TikTok.
- Mentions a past experiment with TikTok due to data harvesting concerns, leading to the nickname "bat phone."
- Addresses the issue of correlation not equating to causation and the broader context of national security concerns surrounding TikTok.
- Explains that other countries took a more tailored approach compared to the U.S. due to legislation writing capabilities and capitalist motivations.
- Mentions that restrictions on TikTok are about influencing citizens rather than data harvesting concerns.
- Notes the difference in approaches taken by the U.S. and other countries regarding TikTok restrictions.

### Quotes

1. "It's spy versus spy stuff. That's what this is about."
2. "There's literally half a decade of available evidence saying that this is about national security issues."
3. "It's about the ability to influence the citizens of the country that has restricted it."
4. "It's an added benefit for the politicians, not the reason it was brought up."
5. "When you are trying to assign intent or motive to something, it's important to get all of the information."

### Oneliner

Beau addresses TikTok questions, stresses the importance of gathering all information before assigning intent, and explains the broader context of national security concerns surrounding the platform.

### Audience

Advocates for Digital Literacy

### On-the-ground actions from transcript

- Research countries that have banned or restricted TikTok for more context (suggested).
- Take a tailored approach in addressing technology concerns rather than broad bans (implied).

### Whats missing in summary

Further insights on national security implications and the long-standing concerns related to TikTok can be gained from watching the full transcript.

### Tags

#TikTok #InformationOperations #NationalSecurity #DataHarvesting #DigitalPrivacy


## Transcript
Well, howdy there, internet people, it's Bob again.
So today we are going to talk about TikTok once again.
We're gonna talk about TikTok, answer some questions,
provide some more context, and just go through some things
because a whole bunch of questions have come in
since that last video about it.
And I've realized that it's probably worth going over.
Now, the message that I'm gonna choose to read,
I'm reading this one because it has all of the questions in it,
not because of how it's phrased.
The theme here for this week has very much
been about making sure you get all of the information
before you try to assign intent to something.
And this fits very, very neatly into that.
OK, so here's the message.
You are so full of it.
Your after-the-fact justifications
the TikTok ban are total bull. That's a different word. I like you, but the first time I ever
heard the phrase information operations was on your channel and I've heard it again and
again here more than I have anywhere else combined. If you thought it was an information
operation risk, why don't you have videos talking about it before now? You just don't
want to admit this is really the Israelis controlling the narrative. This is about Gaza.
Okay, so down below there will be some videos.
The first one will be a video where I talk about how once the Republicans have their
majority shored up and everything, they will probably push through this piece of legislation.
If it gets through the Senate, then it's very likely that Biden will sign it, and it has
to do with information operations.
But the real goal of it isn't really going to be to ban TikTok, it will probably be to
spend TikTok off into its own company in the United States. And there's more to it about how
the Republican Party will lean into it, because it's very easy for them to see, you know, China bad and
all of that stuff. It's all there. People have viewed this video over the last week. I don't
think that any of them have realized that it's from 2022 because it's exactly what happened.
It's from 2022 and there's more to it.
It's that this is not a new thing. It's not a new idea to deal with TikTok in this exact manner
And again, this is coming from somebody who thinks that they're being a little,
they're going a little overboard in how they're dealing with it, but it's definitely not new.
Now, to take it a step further, a little bit more information, you know, I'm going to read the order
here, and then we'll just kind of go from there with it. I, blah, blah, blah, President of the
United States, find that additional steps must be taken to deal with the national emergency with
respect to the information and communications technology and services supply chain declared
an executive order blah blah blah blah blah specifically the spread in the united states of
mobile applications developed and owned by companies in the people's republic of china
continues to threaten the national security foreign policy blah blah blah blah at this time
action must be taken to address the threat posed by one mobile application in particular tiktok
Okay, and there's way more to this, but reference to what's happening right now, it's not in there.
People are taking correlation of two events occurring in close proximity and creating causation.
And I know somebody right now is saying, well, it's not like they'd let Biden write the real reason that they were
banning it.  banning it. Well, Biden would have a hard time dealing with that anyway, because
this is from Trump in 2020. What people believe because of the close proximity
of events is connected. This is really a process that is half a decade in the
making. It's not new. They're not connected in the way people think they
are. If you want a little bit more context on it, type in list of countries
that have banned or restricted TikTok and just take a look. Most of these lists
will also list the reason why. What's the reason? Going back over the years, the
reasons I gave in the most recent video as far as the data harvesting from
certain people, you know, military, government employees, journalists, trade
secrets, that kind of stuff, and information operations. All of these
countries, they're not necessarily allied, but they're all adversarial to China.
It's spy versus spy stuff. That's what this is about. It's about information
operations and to a lesser degree data harvesting, which we've talked about.
There are way better ways to deal with it. And then if you want a little bit
more evidence from me.
I haven't gone through and found this video
and I probably won't, to be honest.
But for those people who have been around the channel
a while, remember we experimented with TikTok.
And there was a big joke at the time about the red phone,
the bat phone.
Because when we did it, because even though I do commentary,
I still get a lot of off the record comments from people.
And there was no way I would have TikTok on my phone.
So we got a prepaid phone and it wound up, the only case they had was like this bright
red one.
So it became known as the bat phone or the red phone.
Because the data harvesting issues, they're real.
They're real.
And again, that's years ago.
It's spy versus spy stuff.
That's what it's about.
happening in close proximity to something else, but correlation does not equal causation.
You have half a decade of evidence showing that this is what it's really about, and
this comes from countries all over the world that have the same concern.
Now there was one sentence in this, is Raleigh's controlling the narrative?
In 2022 I put out a video saying exactly how this was going to play out.
I'm telling you now that I imagine there will be people in the comments section explaining
why that sentence is really going to bother people.
And it might be a good idea to listen to them about it.
That may be, I would imagine that it's unintentional, but that's going to hit a whole lot of people
wrong.
So I mean unless it's meant in the way of, you know, information operations, but it can
easily be taken to mean something else. So understand there's
literally half a decade of available evidence saying that this is about
national security issues. Now they frame it around the data harvesting which is
very much what they used and they do that because that impacts everybody
because everybody thinks they're immune to information operations so they frame
it around the data harvesting as we've talked about unless you're in one of
those classes you know government employee military blah blah blah I mean
yeah they're gonna get your information but that they're getting the same
information from a thousand other sources.
It's a concern, but it's not the concern.
Let's be clear about that.
It's about the ability to influence the citizens of the country that has restricted it.
Now, the other question is, why did the other countries use a much more tailored approach
than the U.S. has?
Two reasons.
they're better at writing legislation, obviously. Two, the U.S. is a hyper-capitalist entity,
and they see this as an opportunity to get their hands on a platform that's worth billions. That's
why. That's why it's done in such a broad fashion here. If you're looking for something that is
beyond the obvious, the very open, very public national security concerns, I mean, that's
something that could definitely be discussed because if you look most
other countries did not try to force TikTok into selling. They just
restricted it in most cases pretty narrowly. Now there are exceptions to
that where they did do a ban but in most cases it was like if you fit into these
classes of people, you can't have it. So again, I understand what it looks like. I
understand the correlation and as far as it being used, this is, you know, kicking
down at young people. That's also talked about in that first video that will be
down below because there there's an element of truth to that but it's an
added benefit, not the reason. It's an added benefit for the politicians, not the
reason it was brought up. So when you are trying to assign intent or motive to
something, it's important to get all of the information, everything you can first.
because a lot of times the chain of events extends much further than you
think it does.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}