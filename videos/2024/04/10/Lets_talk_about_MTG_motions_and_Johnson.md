---
title: Let's talk about MTG, motions, and Johnson....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=NR3R67j3Usk) |
| Published | 2024/04/10 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republicans in the House are back and need to address the issue of Marjorie Taylor Greene's motion to vacate.
- Greene appears to be building support to remove the current Speaker of the House, citing reasons related to alignment with the Democratic agenda.
- There are concerns that Greene's main points of contention are aid for Ukraine and support for warrantless surveillance.
- The current Speaker, Johnson, is trying to downplay the situation and avoid further division within the party.
- Johnson has been successful in managing the Twitter faction of the Republican Party.
- Greene may try to generate outrage among the Republican base, potentially putting Johnson at risk.
- If Johnson faces trouble, he might seek Democratic support to protect his position.
- Greene's motion to vacate could ironically lead to a deal between Johnson and the Democratic Party.
- The situation seems chaotic and could result in the House being without a speaker again.
- The dysfunction within the House could impact the Republicans' chances in 2024.

### Quotes

1. "This has been a complete and total surrender to, if not complete and total lockstep with the Democrats' agenda."
2. "Tying your speakership to saving the Republic."
3. "It might be Marjorie Taylor Greene's motion to vacate that forces Johnson into a situation where he ends up making a deal with the Democratic Party."
4. "This whole thing doesn't seem very well thought out to me."
5. "Piling on more dysfunction on top of the giant pile of dysfunction that has existed since Republicans took over the House."

### Oneliner

Republicans in the House face turmoil over Marjorie Taylor Greene's motion to vacate, potentially leading to unexpected alliances and further party division.

### Audience

House Republicans

### On-the-ground actions from transcript

- Contact your representatives to express your views on the situation (implied)
- Stay informed about the developments within the House of Representatives (implied)

### Whats missing in summary

Insights into the potential long-term impact on the Republican Party and the functioning of the House of Representatives.

### Tags

#Republicans #HouseofRepresentatives #MarjorieTaylorGreene #SpeakerOfTheHouse #PartyDivision


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about Republicans
in the House, because they're back.
And now that they're back, they have
to deal with the elephant in the room.
They have to deal with that item that was just kind of left
hanging out there when they went on break, Marjorie Taylor
Green, that motion to vacate.
Now, at this point, it's not set in a way that is going to force a vote.
It's not set to bring it to the floor yet, but that could change.
And reporting indicates that she is circulating a letter, and that letter lays out what appears
to be the reasons for getting rid of the current Speaker of the House.
again.
This has been a complete and total surrender to, if not complete and total lockstep with
the Democrats' agenda that has angered our Republican base so much and given them very
little reason to vote for a Republican House majority.
I mean, it's unique because when you think about it, it wasn't actually Johnson that
was responsible for all of the obstruction, right?
And it wasn't Johnson that was responsible for the Republican Party not really doing
anything this whole time.
That wasn't him saying.
But it appears that Marjorie Taylor Greene is trying to build that support, and it seems
as though her two main points of contention are the idea that Johnson might bring forward
an aid package for Ukraine, or that he might be in support of a warrantless surveillance
program that allows warrantless surveillance of foreign nationals.
appear to be the two things that she is really concerned about. Now as far as
Johnson, he's trying to downplay it for the most part, saying stuff like a
shutdown would not serve our party or assist us in our mission of saving the
Republic by growing our majority, nor will another motion to vacate. Tying your
speakership to saving the Republic. I guess that's one way to do it. Now as far
as the Republicans in the House overall, I think most have realized that so far
Johnson has been pretty successful at managing the Twitter faction of the
Republican Party and bringing them to heel. He's done pretty well with that.
he's been more aggressive than McCarthy at removing their base of power.
At the same time, that bit about the Republican base, it might indicate that Greene is hinting
that she might try to gin up outrage.
And if she does that and she's capable of generating that outrage and that outrage ends
up creating calls to House Republicans, well Johnson might be in trouble.
If he ends up in trouble, what's he going to do?
Probably go to the Democratic Party to get the votes to protect himself.
In a very funny twist of fate, it might be Marjorie Taylor Greene's motion to vacate
that forces Johnson into a situation where he ends up making a deal with the Democratic
Party, which in theory is what she's trying to avoid.
This whole thing doesn't seem very well thought out to me.
But it does appear that we might be headed into another situation where the U.S. House
of Representatives is without a speaker again.
I am certain that piling on more dysfunction on top of the giant pile of dysfunction that
has existed since Republicans took over the House will certainly help their chances come
2024. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}