---
title: Let's talk about AL leaving Biden off the ballot and how it isn't OH....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=GaIYXgYMcAQ) |
| Published | 2024/04/10 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republicans in Alabama are trying to create a scenario similar to Ohio by suggesting Biden may not be on the ballot in 2024.
- Alabama is not a swing state like Ohio, and it typically votes red by a significant margin.
- The worst-case scenario for the Democratic Party in Alabama if Biden is left off the ballot is maintaining the status quo – they weren't likely to win those electoral votes anyway.
- Democrats in Alabama see this move as an attempt to silence their voice, which could increase enthusiasm within the party.
- Republicans' best-case scenario is no change – maintaining the status quo with lower voter turnout.
- If Trump's base doesn't show up to vote for down-ballot races where Biden is not on the ballot, it could impact the outcomes significantly.
- State politicians, especially in the Republican Party, often rely on the coattails of bigger candidates like Trump for voter turnout.
- The move to potentially exclude Biden from the ballot could have differing impacts on voter turnout for Democrats and Republicans in Alabama.

### Quotes

1. "Democrats have nothing to lose and everything to gain."
2. "Republicans have everything to lose and nothing to gain."
3. "Those down-ballot races without that Trump loyalist base, I don't know, I feel like that might swing an election or two."

### Oneliner

Republicans in Alabama are trying to replicate Ohio's scenario by suggesting Biden may not be on the 2024 ballot, impacting voter turnout dynamics and potential outcomes for down-ballot races.

### Audience

Alabama voters

### On-the-ground actions from transcript

- Contact local Democratic and Republican party offices to stay informed and engaged with the voting process (suggested).
- Organize voter registration drives in Alabama communities to increase voter turnout (implied).
- Encourage community members to stay informed about local elections and candidates to make informed voting decisions (suggested).

### Whats missing in summary

Implications of voter turnout strategies on local election results.

### Tags

#Alabama #Biden #VoterTurnout #ElectionStrategy #DemocraticParty #RepublicanParty


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we're going to talk about Alabama and Biden
and how Alabama apparently wants to create a scenario
like Ohio, but Alabama's not Ohio, not in any way.
Okay, so Republicans in Alabama have indicated that Biden,
Well, he may not be on the ballot come 2024.
And it's a duplicate scenario to Ohio.
It has to do with dates and when the official announcement gets made
and all of that stuff.
In the past, the state has been accommodating two parties
when they missed the deadline,
but apparently that's not going to happen this time.
It's what Republicans in Alabama are saying
because they want that scenario like Ohio.
But see, Ohio is a swing state.
Does anybody believe that Alabama's in play?
No, right?
Not really.
It's going to go red in 2020, what, 62% of the vote?
Something like that.
Real close.
Pretty similar in 2016.
It's unlikely that changes.
So what's the worst case scenario for the Democratic Party in Alabama if Biden is left
off the ballot?
Nothing.
Status quo.
They're not going to get those nine, I think, electoral votes, but they weren't going to
get them anyway.
That's the worst case scenario.
What's the best case?
Democrats in Alabama believe the state is trying to silence their voice and it increases
enthusiasm a little bit.
Not enough for Biden to win, okay?
Like he's not going to mount some write-in candidate drive that in some way gets him
the state.
That is super unlikely.
But there is a high likelihood of there being increased enthusiasm because of the perceived
shenanigans, right? So, from the Democratic Party side, they have nothing to lose and
everything to gain this way. What about the Republican side? What's their best
case scenario? Nothing changes. The status quo. That's their best case
scenario. What's their worst case? Real quick, pull up a mental image of your
representative. A higher percentage of people who watch this channel will be
able to do it than most. I don't believe that most people show up to vote for
down-ballot races, especially not in the Republican Party of today. They're loyal
to Trump and if Trump doesn't need them to show up, Biden's not even on the
ballot. There's no way he's gonna win. Well, they may not show up, right? Let's be
real. Most state politicians, you're riding the coattails of the bigger
candidates. And this is true of both parties. I'm not picking on Republicans
here. It's more pronounced in the Republican Party because it has become
very centered on Trump. And if he doesn't need his base to show up, well, I don't
think they're gonna show up to vote for somebody whose name they can't even
remember. If you're a lawmaker in Alabama, an elected official, I want you to
think about the last time you went out. Not to some event, but to the grocery
store. Did people that you don't know personally recognize you?
Probably not, right?
So, for the Republican party, you may have lower voter turnout this way.
While the Democratic party, in their best case, well, they may have more.
They may have higher turnout than normal.
And you might have lower.
Worst case scenario, as far as nationally, Biden's not on the ballot, nothing changes.
If I'm the Democratic Party and Republicans in Alabama wanted to pull this, bet, bet.
Democrats have nothing to lose and everything to gain.
Republicans have everything to lose and nothing to gain.
Those down-ballot races without that Trump loyalist base, I don't know, I feel like
that might swing an election or two.
You're not Ohio.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}