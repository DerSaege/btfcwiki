---
title: Let's talk about AZ, 1864, and Biden....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=0Im6tr28g0k) |
| Published | 2024/04/10 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former President Trump's actions led to the overturning of Roe v. Wade, setting off a chain of events in Arizona.
- The Supreme Court in Arizona upheld a law from 1864 that prohibits abortion, despite updates in 1901 and 1913.
- The current Attorney General in Arizona expressed reluctance to enforce the law, stating it's a low priority.
- Biden won Arizona in 2020, and it's unlikely that a policy shift could turn Biden voters into Trump supporters.
- Beau predicts that this decision on the 160-year-old law will boost support for Biden in Arizona significantly.
- He argues that politicians often create false divisions on issues that most Americans agree upon, like bodily autonomy.
- The impact of this decision could be detrimental to the Republican Party in Arizona, potentially causing irreparable damage.
- Beau advises individuals affected by the law to seek legal counsel and make their views known through voting.
- He warns that while the current Attorney General may not prosecute under the law, others might not follow suit.
- Beau underscores the importance of being cautious and informed about the implications of the state Supreme Court's ruling.

### Quotes

- "Politicians please stop saying it like that. You don't have the resources to do this."
- "The overwhelming majority of Americans shares one opinion on this, and that is that bodily autonomy is a thing."
- "Not all will."
- "I think there's a vote about it."
- "This just gave Biden a huge boost in Arizona."

### Oneliner

Former President Trump's actions led to a chain of events in Arizona as its Supreme Court upholds a 160-year-old law, potentially boosting support for Biden.

### Audience

Voters in Arizona

### On-the-ground actions from transcript

- Make your views known through voting in November (implied)

### Whats missing in summary

The full transcript provides additional context on the historical and political implications of the Arizona Supreme Court's decision and the potential consequences for women's rights and the political landscape in the state.

### Tags

#Arizona #SupremeCourt #AbortionLaw #Biden #Trump


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about Arizona,
something that occurred there,
and what it means for events 160 years later,
and how it is likely to influence things in the future.
Okay, so it wasn't too long ago that former President Trump said that he was proudly the
person responsible for taking down Roe.
event, Roe going away, it set a chain of events into motion and we've talked
about it numerous times the Republican Party became the dog that caught the car
it was chasing. In Arizona that car drove the Republican Party back to 1864. The
The Supreme Court there in the state said that a law from 1864 that was, I want to say
it was codified and updated in 1901 and 1913 as well, that it was okay, that was all right.
So from a period before Arizona was even a state, and even the updates occurred before
women could vote. But that's going to be on the books. Now, the AG, the attorney general
out there, has indicated that there's not a high likelihood of it really being enforced.
The exact wording was, let me be clear, as long as I am attorney general, no woman or
doctor will be prosecuted under this draconian law in this state. Now I
understand that sentiment. Politicians please stop saying it like that. You
don't have the resources to do this. It will be a low priority. Don't just say
you're not going to enforce a law. It doesn't doesn't always play well. In this
case, I don't think it's going to be an issue. So here are some facts. In 2020, in Arizona,
they had a pretty decent turnout, almost 80%. Biden got 1,672,000 votes and change. Trump
got 1,661,000 votes and change. Biden won. Now, unless you can think of some major policy issue
that is going to turn Biden voters into Trump voters, especially when you consider all of the
the good the Biden administration has done for Arizona when it comes to jobs
and a whole bunch of other things. And whatever that policy issue is, it has to
be more than what is going to be shifted by this decision. It is going to have to
be more than what is created as far as support for Biden when it comes to this
160 year old law now governing the lives of women in that state.
Unless you can think of something I cannot, how do they say it?
I've seen enough, I'm going to go ahead and call Arizona for Biden.
You caught the car, gentlemen.
times the dog is smart enough to let go. This is a big deal. This is a state that Biden
needs and this is going to probably help him get it in a big way. At some point in this
country, we are going to have to realize that politicians put out a lot of
statements, they have a lot of talking points, they have a lot of issues that
they pretend are really things with two sides, that they really are things that
the country is divided about. If you look at the results of any of the votes
that have taken place, you're going to find out that this isn't an issue with
two sides. The overwhelming majority of Americans shares one opinion on this,
and that is that bodily autonomy is a thing, and that the government should
respect that.
That seems to be the core element that's being debated.
So this, barring something just really bizarre, really out there, this just gave Biden a huge
boost in Arizona.
I do not know that it is something the Republican Party can recover from, to be honest.
Now to those people who are impacted by the 160-year-old law, the Attorney General seems
to be on your side.
I would be incredibly careful.
I would be incredibly careful.
I would, before you undertake any action, I would talk to an attorney, I would, I would
be very aware of it and just be conscious of what the state supreme court has said.
That attorney general may protect you.
Not all will.
So it is probably in your interest, if this is something that concerns you, to make your
views known come November.
I think there's a vote about it.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}