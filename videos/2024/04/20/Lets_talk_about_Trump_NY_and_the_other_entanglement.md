---
title: Let's talk about Trump, NY, and the other entanglement....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=jTbtxyqzjOw) |
| Published | 2024/04/20 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains about the New York entanglement for Trump involving a $454 million issue with a $175 million bond.
- The New York Attorney General's Office is moving to void the bond, claiming lack of identifiable collateral and a policyholder surplus of $138 million.
- Points out that the phrase "trustworthiness and competence" in the motion is related to regulations, not an indictment of the company's management.
- Anticipates more legal troubles for the former president due to the developments in the case.
- Mentions that a hearing is scheduled for Monday regarding the $454 million issue, coinciding with the start of opening arguments in another New York entanglement.
- Emphasizes that amidst the focus on the New York case, other significant developments are also taking place, hinting at overlapping issues in the future.

### Quotes

- "It's going to be a busy week when it comes to this kind of news about the former president."
- "As time goes on, more and more of them are going to start to overlap, and there's going to be a lot of stuff happening at once."

### Oneliner

New York Attorney General's Office moves to void a $175 million bond in Trump's $454 million entanglement, hinting at more legal troubles ahead for the former president amidst overlapping case developments.

### Audience

Legal analysts, political commentators

### On-the-ground actions from transcript

- Prepare for upcoming legal developments (implied)
- Stay informed about the evolving situation (implied)

### Whats missing in summary

Details on the potential consequences and implications of these legal challenges for Trump.

### Tags

#Trump #LegalIssues #NewYork #Entanglement #AttorneyGeneral


## Transcript
Well, howdy there internet people, it's Beau again.
So today we're going to talk about New York, Trump, and entanglements and
Monday, but we're going to talk about the other New York entanglement for
Trump on Monday, because there have been some developments as far as that goes.
This is the one that has the four hundred and fifty four million dollar
thing, that one.
Okay, so if you remember, that was secured by a one hundred and seventy-five
million dollar bond.
The New York Attorney General's Office has basically moved to, I mean, kind of void
it for lack of a better term. They are saying that the company behind it does
not have enough identifiable collateral and that the policyholder surplus is
just 138 million I think maybe 134. So they're asking for that to not be used.
Okay, now one question that has come in a number of times and I think that it's
important to kind of point this out. The phrase trustworthiness and competence
has come up a lot in questions. Don't read into that. That's language related
to the regulations really. That doesn't mean that the state is saying that the
The company is managed by people who aren't competent or anything like that.
I wouldn't read into those two words, which are kind of near the end of the motion.
So this would certainly cause more issues for the former president.
The judge, overseeing the $454 million thing, has scheduled a hearing about this on Monday,
the same day that opening arguments are supposed to begin in the other New York entanglement.
It's going to be a busy week when it comes to this kind of news about the former president.
I would expect a whole bunch.
It's important to remember that while everybody is currently focused on the New York case,
the so-called hush money case, that there are still other developments occurring.
As time goes on, more and more of them are going to start to overlap, and there's going
going to be a lot of stuff happening at once.
I would go ahead and get ready for that.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}