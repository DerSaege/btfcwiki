---
title: Let's talk about Biden, oil, and Alaska....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=SVGwp5AWWP8) |
| Published | 2024/04/20 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Biden approved something called Willow, causing uproar due to potential environmental issues.
- Around the same time, the idea of protecting massive amounts of area in Alaska was promised.
- A years-long fight finally resulted in the protection of 13 million acres, including 40% of the National Petroleum Reserve.
- Republicans are expected to sue in response.
- Native groups have differing opinions on the protection.
- The decision will lead to legal battles for years to come.
- Politicians in Alaska are upset due to potential revenue loss.
- Environmental costs of extracting and burning oil are high.
- The protection could speed up the transition towards cleaner energy.
- Other news on environmental and economic costs may reduce opposition by about 11%.

### Quotes

1. "This is going to set up legal fights for years to come, no doubt."
2. "The environmental costs of what happens once it comes out of the ground and gets burned, that's a high cost to everybody else."
3. "It's a balancing act that maybe people should reconsider."
4. "This is a move that should speed transition."
5. "Anyway, it's just a thought. Y'all have a good day."

### Oneliner

President Biden's decision to protect 13 million acres in Alaska, including 40% of the National Petroleum Reserve, sparks legal battles and differing opinions among Native groups, setting the stage for a transition towards cleaner energy.

### Audience

Alaskan Residents, Environmentalists, Native Groups

### On-the-ground actions from transcript

- Support Native groups in their differing opinions on the protection (implied)
- Stay informed and engaged in the legal battles that will follow (implied)
- Advocate for a faster transition towards cleaner energy (implied)

### Whats missing in summary

The full transcript provides a detailed look at the ongoing environmental and legal implications of President Biden's decision to protect significant areas in Alaska, urging a reconsideration of the costs and benefits involved.

### Tags

#PresidentBiden #Alaska #Oil #EnvironmentalProtection #NativeGroups #TransitionTowardsCleanEnergy


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about Biden and Alaska and oil and something that
that Biden promised that not a whole lot of people thought there was going to be
follow through on.
If you remember last year, I think it was last year, President Biden approved
something called Willow and it caused a whole lot of uproar because of the
potential environmental issues going along with it. When he did that around
the same time the idea of protecting massive amounts of area in Alaska was
kind of promised. So hey we're gonna do this almost like a consolation prize and
And this has been a years-long fight to get this done, and Biden's like, well, we're going to do it.
Not a whole lot of people put a lot of stock in that.
It's apparently finalized. 13 million acres are going to be protected, and that includes about 40% of the NPR, the
National Petroleum Reserve.  So that's done.
Now what happens next?
Republicans sue.
That's probably what's going to occur next.
They're already saying there are going to be lawsuits and they will certainly make the
attempt.
Generally speaking, they're not really successful when it comes to this sort of thing.
So there's a very good chance that all of this stands.
There is, and that was a question that came in, why are some Native groups in favor of
this and some opposed?
Because Native groups aren't unified, it's not one group, and they have different ways
of looking at things.
But there's going to be a lot of discussion about this.
I'm surprised this announcement didn't get timed to go along with Earth Day.
But this is a big deal.
This is going to set up legal fights for years to come, no doubt.
But in the meantime, it will be protected.
Obviously, politicians in Alaska are upset because it's a big source of revenue for
them. The environmental costs of what happens once it comes out of the ground
and gets burned, that's a high cost to everybody else. So it's a
balancing act that maybe people should reconsider. Take a deeper look at as
far as what the actual benefits are. This is a move that should speed
transition. It'll take time because, of course, there's going to be the legal
battles. But there's some other news that will be coming out about the environment
and the economic costs that might that might reduce the opposition by about 11
percent or so. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}