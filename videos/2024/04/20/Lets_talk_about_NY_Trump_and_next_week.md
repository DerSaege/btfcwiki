---
title: Let's talk about NY, Trump, and next week....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=_5QvmA1sA5s) |
| Published | 2024/04/20 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- New York and Trump trial developments discussed.
- Opening arguments set to begin on Monday.
- Jury consisting of 12 jurors and 6 alternates.
- Case widely referred to as the hush money case, actually about falsification of business records.
- Focus on why records were altered.
- Media circus expected due to historic nature of the trial.
- Speculation and media presence anticipated.
- Old Dozing Don allegedly fell asleep in court.
- Other behavior in court not to be covered.
- Coverage to focus on jury and trial developments.
- Expect additional hearings related to former president's behavior.
- Warning about false claims and misinformation.
- Caution against basing beliefs on former president's portrayal of events.
- Environment expected to have a lot of misinformation.

### Quotes

1. "Please remember that it is speculation."
2. "Y'all have a good day."

### Oneliner

New York and Trump trial updates, media circus expected with caution against misinformation.

### Audience

News consumers

### On-the-ground actions from transcript

- Stay informed on reliable news sources (implied)
- Verify information before sharing (implied)

### Whats missing in summary

Insights into the specifics of the trial proceedings and potential implications.

### Tags

#Trump #NewYork #Trial #MediaCircus #Speculation


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about New York and Trump.
The developments, what you can expect, and just how everything is set to move along.
We talked briefly about how I thought it was ambitious for them to suggest that they would
starting on Monday. Apparently they are ambitious people because from what I
understand opening arguments begin on Monday and that is what most people
associate with the beginning of a trial. So because of that obviously they were
able to find a jury. They have 12 jurors, six alternates that will be taking a
look at everything and making the decision. This, for clarity's sake, because there are
so many, this is the one that is widely referred to as the hush money case. Again, it's not
actually about hush money really. It's about the falsification of business records. That's
the allegation. And then a critical part of the discussion will be why those records were
altered if they were. That's what's going to matter. There's going to be all kinds
of other things that are going to be discussed, but that's the real question that's being
decided. It is important to remember that this is going to be a media circus. It's
a historic event. First criminal trial of a former president and all that stuff. The
media is going to be there constantly and reliably. That means you will have other people
tried to get their message out by taking advantage of the media presence.
You will probably see that happen.
Beyond that, there's going to be a lot of ups and downs and twists and turns, no doubt.
I am aware of, at least from what I have been told, Old Dozing Don did fall asleep again,
which is an interesting trend.
I am aware of the other behavior that he allegedly allowed to pass in the courtroom.
I'm just going to go ahead and tell you now, that's not going to be part of the coverage
here.
That's not something we're going to be talking about.
But it is starting.
There's going to be coverage, there's going to be speculation.
Please remember that it is speculation.
What matters is the jury.
of the other stuff, it's people's best guess. But it is going to be constant, and in many
ways, even though this is a historic event, it will probably overshadow things that are
more important. We will continue doing what we've been doing as far as condensing the
developments into one video and in doing it that way and just and just try to
provide the information in that manner because it's going to be everywhere.
There are also undoubtedly going to be additional hearings. My guess is that at
least some will be related to the former president's behavior inside or outside
of the courtroom in addition to the one that's already scheduled. And there are
going to be a lot of false claims about what occurred. I would imagine that the
former president himself will say things and portray events inside the courtroom
in a certain light. I would not base any debate or discussion on that. We've
already seen how that plays out with the whole thing about the graduation. Just be
very aware that this is an environment that is going to have a whole lot of
of bad information put out.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}