---
title: Roads Not Taken EP 33
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=3il1UKhlEHw) |
| Published | 2024/04/07 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Overview of underreported news events from April 7, 2024, in episode 33 of The Road's Not Taken.
- Ukraine's use of drones against Russian warplanes in the air war is successful.
- Israel Defense Forces (IDF) firing military officers over World Central Kitchen Strike.
- Zimbabwe launching a gold-backed currency.
- Bernie Sanders' office in Vermont caught fire; arson suspected with no motive disclosed.
- No Labels giving up on running a presidential candidate for 2024.
- Trump's failed attempts to dismiss legal entanglements.
- CIA debunking whistleblower claims in Biden's impeachment probe.
- RFK Jr. making damaging statements about January 6th.
- U.S. employers adding 303,000 jobs in March, indicating a strong job market.
- Speculation about Ted Cruz potentially losing his seat due to fundraising issues.
- Right-wing upset over a new depiction of Romeo and Juliet with a black Juliet.
- Engineer discovering a potential cyber attack backdoor by accident.
- Forecast of an active Atlantic hurricane season.
- Allegations of Lauren Boebert being over-served and seeking selfies from Trump.
- Class action lawsuit by January 6th defendants against Capitol police alleging excessive force.
- $30 million cash stolen from a security company.
- Addressing questions about Lavender, Mia Khalifa's opinions, media focus on aid workers, and systemic racism.
- Advice on being a first-time parent and debunking religious fear-mongering beliefs.

### Quotes

- "Why systemic racism? That's the answer."
- "Just trust me on that one. You'll thank me later."
- "People are free to believe whatever you want. I'm just saying maybe you should base them on your beliefs and your religious text."

### Oneliner

Beau covers underreported news events, from successful drone use in Ukraine to Zimbabwe's gold-backed currency, debunking religious fear-mongering beliefs.

### Audience

News consumers

### On-the-ground actions from transcript

- Contact IDF to support World Central Kitchen (suggested)
- Stay informed and prepared for the active Atlantic hurricane season (implied)
- Support Capitol police reforms for excessive force allegations (implied)

### Whats missing in summary

Insights on Beau's unique perspectives and analysis are best gained from watching the full transcript. 

### Tags

#UnderreportedNews #Ukraine #Russia #BernieSanders #Trump #SystemicRacism #Parenting #ReligiousBeliefs


## Transcript
Well, howdy there internet people, it's Beau again, and welcome to the Roads with Beau.
Today is April 7th, 2024, and this is episode 33 of The Road's Not Taken, which is a weekly
series where we go through the previous week's events and talk about news that was unreported,
underreported, didn't get the coverage it deserved, or I just found it interesting.
Normally, we start off with a whole bunch of foreign policy news.
This week we covered most of it, so there's not a whole lot of that.
Okay, so starting off with foreign policy.
Ukraine has been using drones to go after Russian warplanes and kind of level the playing
field as far as the air war goes.
They have been pretty successful with this.
I feel like eventually Russia will find some way to mitigate what's currently going on,
but until that occurs, they seem to have developed a tactic that will work until Russia figures
something out.
Today is the day of remembrance for Rwanda.
The IDF has fired some military officers over the World Central Kitchen Strike, saying that
some things weren't done correctly.
Zimbabwe is launching a gold-backed currency.
Moving on to US News.
The office of Bernie Sanders in Vermont caught fire.
Arson is suspected.
At time of filming though, they don't have or haven't released any potential motive.
No labels is giving up on the idea of running a presidential candidate in 2024.
We talked about that.
The national director has indicated that he is riding with Biden and will be voting for
Biden.
Trump launched multiple attempts to get some of his legal entanglements dismissed.
To keep this short, they all failed.
The CIA has said the whistleblower in Biden's impeachment probe and the allegations that
came about there said that those allegations are, quote, false.
I mean, okay.
Right-as-political parties are set to put RFK Jr. at the top of their list when it comes
to outlier things that they have to deal with prior to the election.
He made a series of statements about January 6th that basically just handed the Democratic
Party everything they needed as far as talking points from now until for as long as he's
a viable candidate.
Trump wanted Nebraska to change the way they do their electoral college system and basically
make it winner-take-all, and Nebraska was like, yeah, no, we're not going to do that.
I would imagine some angry tweets about that in the future.
U.S. employers added 303,000 jobs in March, which is another signal of an incredibly strong
job market.
There is some speculation that Ted Cruz might lose his seat this term amid, well, let's
just call it fundraising issues. In cultural news, as there's almost one of
these every week, the right is very upset about a new depiction of Romeo and
Juliet in which Juliet is black. Many believe it to be a new quote woke movie
it's worth noting it's live theater but if it was a movie I would suggest that
the addition of cars in the last remake was probably a bigger change. For those
who are going to pretend that they knew that it was theater, I would suggest that
Juliet being played by a woman is a bigger change to, you know, how
Shakespeare would have done it. Okay, in science news, an engineer doing routine
maintenance stuff found a potential back door in a system that could have been
used for a massive cyber attack and created a lot of disruptions.
And it was found entirely by accident.
That's going to go down in the stuff of internet legend right there.
If you're in the part of the world impacted
by the Atlantic hurricane season, be ready.
Researchers are forecasting an incredibly active year.
And oddities reporting alleges that Lauren Boebert was, quote,
over-served at a recent event.
And security had to stop her because she wouldn't
stop asking Trump for selfies.
Some January 6th defendants have filed a class action lawsuit
against 21 Capitol police cops alleging excessive force.
I feel like they're going to become incredibly disappointed
when they find out the amount of force that would fall within policy because the vice
president was present.
And then in last bit of odd news, $30 million in cash that had already been circulated was
reportedly stolen from a security company.
Moving on to some questions from y'all, if you haven't seen this before, these are put
here by the team.
I haven't seen them yet.
What do you think of Lavender?
That's not what you're talking about.
When it comes to that, for those that don't know, there are a number of things that are
going to need to be talked about in depth afterward when it comes to what's going on
in Gaza.
The term lavender is probably going to come up a lot.
Short version for right now, until we can really get into it, it's not good.
I think it's kind of cool you don't hide your friendships with people like her, but
But can you explain why I should give a something about Mia Khalifa's opinion on the Middle
East?
Why does she get media coverage?
Um, okay.
Well, I mean, I would imagine that with her having a high profile and the fact that she's
Lebanese, that's probably why she gets coverage, I'm gonna guess.
That's what it has to do with.
Why do seven aid workers matter more to the media than all the Palestinians?
You know why.
Don't pretend like you don't.
You know exactly why these seven aid workers, and it's really not all seven, right?
Remember when Ukraine popped off and you had a bunch of reporters saying things that were
just kind of shocking to hear?
They were talking about how there's people running around with RPGs and AKs and this
isn't the Middle East?
Why does it matter more?
Why does the media care more?
Because it plays better in the United States.
Why systemic racism?
That's the answer.
Because of appearance, it is more impactful.
The other part about it is the hero aspect that we talked about on the main channel.
So you have the idea of these people are like us.
part, which plays into it a whole lot, and then you have the fact that they went there
to help.
And they were getting media coverage before that about how, you know, they beat the US
military to get the food in there via a naval route.
They were the hero.
That organization was the hero of the war.
And they took a big hit.
two things together. And like I said when it happened, we are not done feeling the repercussions
from that. That's not over.
I'm about to become a first time father. I'm super excited but I'm also extremely nervous.
advice you or Ms. Bo could share about being a first-time parent and not in the philosophical
sense.
You've got enough videos on that.
I mean, like, here's something that's going to be more or less of a problem than you think.
We're having a daughter if it matters.
I mean, we've answered this question enough for friends.
I can actually tell you what she'd say.
number one thing would absolutely be to make sure that when you're there in the early stages,
to be in the moment, yeah, you might be tired, you might be exhausted, and might be as you
will be, but enjoy it because it doesn't last.
also say to work out the schedules. You know, I am very much a night owl. You know, she's
more of a morning person. So figure out some kind of way, if you have a relationship that's
at play here that allows some kind of division to take place, do it early.
As far as me, however much you think it's going to cost, it's going to be more.
And go ahead right now, go ahead and make the decision, pay the money, and get the diaper
genie thing, right by the changing table.
Just go ahead and do that.
And I know it seems just wild to spend, I don't know what they are, 60, 70 bucks for
what amounts to a small trash can, but just trust me on that one.
You'll thank me later.
I like your takes, but don't like the way you mocked people's religious beliefs about
the Second Coming and the Eclipse.
I do not mock people's religious beliefs.
I don't like people manipulating others' religious beliefs.
I have read most religious texts, and I am one of those people that is cursed with a
lot of retention when it comes to stuff that I've read.
As far as those people who come out and predict the second coming, predict the end times,
yeah.
I mean, you're more than welcome to do that, but of that day and hour knoweth
that no man, no, not even the angels will know the time, right? But of that day and
hour, knoweth no man. No, not the angels of heaven, but my Father only, right,
depending on which version you want to go through. A plain reading of that part,
it's in Matthew, indicates that if you believe this, if this is your set of
religious beliefs. A plain reading of that indicates that if there is a man, if
there is a person who is saying that I know that this is going to be what
happens and this is when it's coming, it won't. The habit of saying that the end
an error. It's fear-inducing. It's what it's about. It's about bringing about that
fear and using that fear to either reinforce a belief that may not be based
in Scripture or to reinforce obedience. And I'm going to say that if somebody is
claiming to know the day or hour that maybe they're not the person you should
be getting guidance from. I'm not mocking your beliefs. People are free to believe
whatever they choose. I just think maybe you should read your book. Maybe read
book and look for people, if you need or want assistance interpreting it, look for
people who are actually pushing the message of the text. And this
goes for any religion, because there does seem to be this bizarre overlap between those
people who predict that the end is near and those people who forget that maybe a semi-important
rule is to love your neighbors yourself.
free to believe whatever you want. I'm just saying maybe you should base them
on your beliefs and your religious text rather than the fear-mongering of some
politician. Okay, so that looks like all the questions. So a little more
information, a little more context, and having the right information will make
Make all the difference.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}