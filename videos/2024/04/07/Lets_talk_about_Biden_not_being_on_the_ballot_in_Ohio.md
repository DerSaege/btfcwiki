---
title: Let's talk about Biden not being on the ballot in Ohio....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=E9QfLv4GmNk) |
| Published | 2024/04/07 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Ohio state law requires knowing the candidate by August 7th, but the Democratic National Convention is on August 19th, creating an issue.
- In 2020, both parties missed the deadline, and Ohio granted a special exemption. Now, Ohio has informed the Democratic Party and they are figuring out their next steps.
- Options include moving the convention earlier, the state legislature granting another exemption, or fixing the problematic law.
- Ohio is a swing state, so the Biden administration is unlikely to give up. There's a belief that not being on the ballot could actually drive more people to show up, though Beau is skeptical.
- The situation may become a long-running story if the state legislature doesn't act quickly. Only the Democratic Party is behind this deadline issue this year.
- Some in Ohio may be exercising power for power's sake, but how voters will react remains uncertain.
- Despite uncertainties, the Democratic Party's stance is that Biden will be on the ballot. Beau has questions about how this will unfold.
- It's advised for those in Ohio to keep an eye on this developing situation.

### Quotes

1. "Ohio state law requires knowing the candidate by August 7th, but the Democratic National Convention is on August 19th."
2. "Ohio is a swing state, so the Biden administration is unlikely to give up."
3. "There's a belief that not being on the ballot could actually drive more people to show up, though I'm skeptical."

### Oneliner

Ohio's deadline issue for candidates could impact Biden's presence on the ballot, urging attention and potential action from voters and officials.

### Audience

Ohio Voters

### On-the-ground actions from transcript

- Keep a close watch on the situation and be prepared to act accordingly (implied).

### Whats missing in summary

Details on potential consequences and implications of Biden not being on the Ohio ballot in the upcoming election.

### Tags

#Ohio #Biden #Election #DemocraticParty #Deadline


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Biden and Ohio
and dates and timelines and how Biden,
well, he may not be on the ballot in Ohio.
Okay, so what's going on?
According to Ohio state law,
basically they have to know
the candidate is going to be by August 7th. The Democratic National Convention is August 19th.
That obviously presents an issue. But it's not a new issue because in 2020 both parties were
after the deadline. Then they granted a special exemption. The state legislature did a one-time
change to the law, to the statute that requires this. So basically Ohio has
told the Democratic Party about this and now they're trying to figure out what to
do. The obvious answers would be the convention could move up but that's kind
of unlikely because those things take a lot of planning. The legislature, the
state legislature could again grant an exemption or perhaps maybe they should
think about fixing a law that is obviously problematic. That would be the
sensible route. The sensible route would be for the state legislature to fix it.
Okay. Do we know that they're going to do that? Of course not. What are the other
options? There's hundreds to include, you know, legal battles and all kinds of
other things. So one of the things to remember is that this isn't something that the Biden
administration is going to give up on. Ohio is seen as a swing state. So it's unlikely
that they're just going to be like, okay, fine, we'll be a ride in, as he did during
a primary. There is some unusual math that suggests that might actually help
him. I'm not sure if I buy that math and that belief that by creating a
situation where he's not on the ballot it actually drives more people to show up.
I don't know that I buy that, but this is going to be a thing. This is the
start of another news item that is going to continue. Unless the state
legislature steps up, does the sensible thing, and does it quickly, this is going
to become a long-running story because this year it isn't both parties.
It's just the Democratic Party that is behind this date.
There will probably be some in Ohio who are looking at exercising power for power's sake
here.
the voters of Ohio feel about that, that's an entirely different question because you
don't know.
But as it stands right now, the general tone from the Democratic Party is Biden's going
to be on the ballot.
Don't worry about it.
I mean, I have questions.
I have questions about how it's going to play out.
So again, something to put on your radar to watch, especially if you're in Ohio.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}