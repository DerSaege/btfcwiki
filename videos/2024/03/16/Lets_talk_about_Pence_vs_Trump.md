---
title: Let's talk about Pence vs Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=bsGuzlg_i1I) |
| Published | 2024/03/16 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Pence, the former vice president, refused to endorse Trump, listing reasons why.
- Pence announced on Fox that he couldn't endorse Trump due to differing conservative values.
- Trump is seen as straying from Republican and conservative ideals by Pence.
- Pence believes Trump has shifted on issues like national debt, sanctity of human life, and China.
- This is not just a simple refusal to endorse; Pence is actively discouraging support for Trump.
- Pence's announcement signifies a significant departure from supporting Trump as the presumptive nominee.
- The move is more impactful than a mere lack of endorsement from someone like McConnell.
- Pence is trying to dissuade voters from supporting Trump, citing a shift towards populism over conservative values.
- The announcement is critical for principled conservatives to take note of.
- Pence's position is a notable departure from his past alignment with Trump.

### Quotes

1. "Pence went on Fox to announce that he couldn't endorse him."
2. "This isn't just Pence saying, 'I'm not endorsing him.'"
3. "Pence is trying to sway people away from voting for Trump."
4. "Pence believes that Trump has abandoned conservative values, Republican ideals."
5. "It's probably worth noting, especially if you are somebody who considers yourself a principled conservative."

### Oneliner

Former VP Pence publicly refuses to endorse Trump, citing a departure from conservative values, urging voters to reconsider their support.

### Audience

Principled conservatives

### On-the-ground actions from transcript

- Reassess your support for political figures based on their alignment with your principles (suggested)
- Stay informed about political shifts and changes within party dynamics (exemplified)

### Whats missing in summary

Context on how this move by Pence may impact the upcoming election and the dynamics within the Republican Party.

### Tags

#Pence #Trump #ConservativeValues #RepublicanParty #ElectionImpact


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about Pence and Trump
and the move that Pence just made
and kind of put it into context because the headlines,
they're kind of leaving out an important piece of this.
If you haven't heard yet,
Pence, the former vice president,
has refused to endorse Trump,
his former running mate.
There are a lot of people who are not coming out
to endorse Trump. And that's what it sounds like. It sounds like Pence
just isn't going to endorse him. That's not what happened.
Pence went on Fox to announce that he couldn't endorse him.
And
He said, Donald Trump is pursuing and articulating an agenda that is at odds with the conservative
agenda that we governed on during our four years.
And he goes on to list the different ways that he sees Trump as, well, being a rhino,
for lack of a better word, and giving up on Republican ideas, on conservative ideas.
He says, as I have watched his candidacy unfold,
I've seen him walking away from our commitment
to confronting the national debt.
I've seen him starting to shy away from a commitment
to the sanctity of human life.
And last week, his reversal on getting tough on China.
This isn't just Pence saying,
I'm not endorsing him.
This is coming out and listing the reasons why.
This is coming out and providing a counter endorsement in a way, saying if you actually
believe these things, you can't vote for Trump.
That is worth noting.
He said he was not going to vote for Biden.
But coming out like that and announcing that he's not going to endorse him, that's a little
different than simply refusing to endorse him.
It's an open statement that he no longer believes that the presumptive nominee of the Republican
Party is really fit to be there.
It's not a little thing.
It's not as simple as, say, if McConnell had withheld an endorsement.
This is a change in tone and it was announced.
Pence is trying to sway people away from voting for Trump.
That's what's occurring because apparently Pence believes that Trump has abandoned conservative
values, Republican ideals, and is engaging in that populism that he warned about when
he was running.
It's probably worth noting, especially if you are somebody who considers yourself a
principled conservative.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}