---
title: Let's talk about Project Rebound....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=pVll9aJwqfY) |
| Published | 2024/03/11 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the topic of Project Rebound at California State University, Northridge, and the purpose of the video.
- Shares the successful outcome of the fundraiser, with the goal of matching last year's $23,000 achievement, but raising $90,990 instead.
- Explains that the funds raised go towards supporting formerly incarcerated individuals with essentials like food, books, school supplies, and childcare to help them navigate school.
- Emphasizes the importance of Project Rebound as a support system for individuals who lack belief and support from others, helping them through education.
- Points out the significant impact beyond monetary value – 1,185 individuals now have the support and belief of others, a priceless recognition.
- Mentions that the program is pleased with the results and expresses optimism for its future success.
- Plans to share two videos related to messages received, hinting at the potential impact Project Rebound could have had on those individuals.
- Encourages viewers to have a good day, ending on a positive note.

### Quotes

1. "Those students now know that one thousand one hundred and eighty five people that they may never meet believe in them."
2. "It goes to anything that helps get them through. It could be food, books, school supplies, child care."
3. "And that it probably would have benefited them."
4. "One of the questions that came in is what does this money actually, you know, go to?"
5. "Y'all have a good day."

### Oneliner

Beau shares the success and impact of Project Rebound, providing support and belief to formerly incarcerated individuals pursuing education.

### Audience

Supporters of education equity

### On-the-ground actions from transcript

- Support Project Rebound financially or through volunteering to help provide essentials and support for formerly incarcerated individuals (suggested).

### Whats missing in summary

The full video provides more context on the significance of Project Rebound and the potential impact it could have had on individuals who reached out to Beau.

### Tags

#ProjectRebound #CaliforniaStateUniversity #Fundraiser #Support #Belief


## Transcript
Well, howdy there internet people, let's bow again.
So today, we are going to talk about Project Rebound
at California State University, Northridge.
We're doing it this way because quite a few of you said,
you don't have a video that says,
let's talk about Project Rebound,
so I have no idea what this is about.
So this will serve that role next year.
And it will also provide the final update
on what happened.
So they had their giving day,
their fundraiser day, and our goal was to send people over to match what we were
able to accomplish last year, which was $23,000, which that was huge, I understand,
that was huge. Y'all helped get that number up this year to $90,990, way
beyond what anybody expected, hoped for, even imagined, and it's massive success.
One of the questions that came in is what does this money actually, you know, go
to? How's it spent? It goes to anything that helps get them through. It could be
food, books, school supplies, child care. Project Rebound acts as a support
structure for those people who may not have one, and it helps formerly
incarcerated people get through school. That's what it does.
One thing that I think is important to note is that it's people who don't
necessarily have a support structure.
People who
may not have people who believe in them,
people who may have been told
that they'll never amount to anything.
Beyond
the monetary value,
which is huge,
those students now know that one thousand one hundred and eighty five
people that they may never meet
believe in them.
That's worth a lot all by itself, without the money, recognizing that value.
The program is obviously very happy with the results.
I'm sure they are thrilled.
Down below, I'm going to put two videos.
They're videos that were prompted by messages I have
received over the years.
One was relatively recently.
Neither one of those people had anything to do with
Project Rebound.
They were not students or anything like that that went
through the program.
But I definitely believe that they could have been.
That they could have been.
And that it probably would have benefited them.
And I may not have got those messages had that program been
available to them.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}