---
title: Let's talk about Biden and positioning....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=DAzs0J2dzrc) |
| Published | 2024/03/11 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau dives into the recent political positioning, focusing on Biden and Netanyahu.
- Biden engaged in signaling, including a potential bluff about offensive military action.
- High-ranking Democratic senators discussed conditioning or ending aid to Israel.
- Netanyahu seemed to call Biden's bluff by announcing an offensive into Rafa.
- The Palestinian Authority seeks control over Gaza from Hamas, causing opposition.
- Ceasefire talks show little progress, with potential complications over captives.
- Ramadan has begun amidst ongoing tensions and power struggles.
- The international geopolitical scene resembles a poker game with unknown hands.
- Beau predicts developments to unfold within 14 days due to conflicting positions.
- Urgency is emphasized regarding the need for quick action on humanitarian aid.

### Quotes

1. "In that international poker game where everybody is cheating, everybody's got their cards."
2. "You don't know what's in their hand, but the hand's been dealt."
3. "All of this stuff that we just went over, you're going to see it again."
4. "They don't have 60 days to get that food and water in there."
5. "The situation is at the point now where you're going to start seeing developments."

### Oneliner

Beau predicts imminent geopolitical developments within 14 days as conflicting positions intensify, urging urgent action on humanitarian aid.

### Audience

Political analysts, activists, policymakers.

### On-the-ground actions from transcript

- Contact local representatives to advocate for immediate humanitarian aid (suggested).
- Support organizations providing assistance to affected regions (exemplified).
- Stay informed and engaged with developments in the conflict (implied).

### Whats missing in summary

Insights on the potential consequences of the ongoing power struggles and their impact on regional stability.

### Tags

#Geopolitics #Biden #Netanyahu #Palestine #Israel #HumanitarianAid


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about positions,
because a whole bunch of positioning occurred last week.
And we're going to go through and talk
about where everybody is sitting at time of filming,
which is late Sunday night.
I will try to limit my commentary during this
and just go through the information,
Because this is information you are going to see again.
Okay, we're going to start with Biden.
Last week, Biden engaged in a whole bunch of signaling.
Now, what you include in that is up to you.
It could be the hot mic moment.
It could be things that he said openly during the interview.
It could be the way he said things in the interview.
But there was a bunch of it that occurred to include things
that he said during the State of the Union.
You also had high-ranking Democratic senators
talk about conditioning or maybe even totally ending aid. The way it was phrased, it could
mean all aid or it could mean offensive military aid. My gut tells me that it's offensive.
We got the timeline on the pier in the Seaborn operation, 60 days, two months. I do not believe
have that amount of time. I don't think they have 60 days. Biden established his, quote,
red lines, one of which was an offensive into Rafa, an Israeli offensive into Rafa.
Okay, Netanyahu shortly thereafter was basically like, Rafa, yeah, we're doing that. In essence,
calling what he believes to be a bluff by Biden. Why does he believe it's a
bluff? All of the reasons that we've talked about, all of those normal
foreign policy constraints, the sphere of influence, masters of the universe,
American dominance, balance of power reasons that the US can't normally just
drop Israel, right? Under normal circumstances it would be pretty safe to
call that bluff. Famine does indeed hit different. It changes things on the
foreign policy scene, particularly if the United States has made a giant spectacle
out of saying we're not going to let this happen to include constructing or
or ordering a giant pier to be constructed.
That is also about power.
Now, one of the other things that Netanyahu said was that an offensive in Taraffa would
not take more than two months or 60 days.
Maybe that's a coincidence.
So both Netanyahu and Biden have talked very publicly and put themselves in positions that
are in direct contradiction to each other.
They're oppositional on this.
Okay.
The Palestinian Authority, elements within the Palestinian Authority have basically called
for Hamas to hand over Gaza to them, to give them the reins of power in Gaza. Hamas is not
likely to be receptive to this. There is a rumor at time of filming, it is unconfirmed, but I feel
like it's going to be confirmed, that Hamas wants to put their own person up for the top position in
the unity government. The Palestinian authority is unlikely to be very
receptive to that. The ceasefire talks are, I don't want to say not progressing,
they are not showing progress if that makes sense. They're still talking but
they're not, it's pretty static. Something I want to add to this because
questions have come in from all over the place asking about the the captives
taken by either side and why nobody's covering it. When people are talking
about the ceasefire they're covering it. Any exchange is going to occur in that.
So it's people just not differentiating but the any exchange and a ceasefire at
At this point, they certainly seem tied together in those talks.
The other development is that Ramadan has started.
Okay, so what does all of this mean?
In that international poker game where everybody is cheating, everybody's got their cards.
Everybody has their cards.
What you see here is the public-facing stuff.
This is literally the player's face.
You don't know what's in their hand, but the hand's been dealt.
The stage is set for developments to occur.
All of this stuff that we just went over, you're going to see it again.
I don't know that you will see the developments right away, but my guess is within 14 days
you'll start to see a lot of this stuff get addressed because these positions are interfering
with other things, so there's going to be a lot of pressure to resolve them.
And they don't have 60 days to get that food and water in there.
It doesn't exist.
And we will probably start seeing some more, maybe, counter signaling, like within the
next two days over this.
Now whether or not Biden is bluffing, I don't know.
I don't know.
But there's a...
The situation is at the point now where you're going to start seeing developments that are
going to shape the rest of it.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}