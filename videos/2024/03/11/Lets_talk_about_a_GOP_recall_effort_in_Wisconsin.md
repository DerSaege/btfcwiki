---
title: Let's talk about a GOP recall effort in Wisconsin....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=QYfiuXGb8ko) |
| Published | 2024/03/11 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Assembly Speaker, Robin Voss, faces a recall attempt in Wisconsin by Republicans who are unhappy with his lack of support for Trump.
- The recall petition needs around 7,000 signatures and has reportedly gathered 10,000.
- Reasons for the recall include Voss's opposition to decertifying the election in 2020 and his reluctance to impeach an election official.
- The involvement of the "pillow guy" in the recall attempt is noted.
- Uncertainty looms over the success of ousting Voss and which electoral map should be used due to recent redistricting.
- If successful, Voss's ousting could have significant implications for the Republican Party in a pivotal battleground state like Wisconsin.

### Quotes

1. "Because he's not Trumpy enough."
2. "There's still more to go along with this."
3. "If they were successful in ousting him, it would be huge news."
4. "This is a battleground state."
5. "Anyway, it's just a thought."

### Oneliner

Assembly Speaker Robin Voss faces a recall attempt in Wisconsin due to lack of support for Trump, with significant implications for the Republican Party in a battleground state.

### Audience

Wisconsin residents

### On-the-ground actions from transcript

- Join the signature collection for the recall petition (exemplified)
- Stay informed about the progress of the recall attempt (exemplified)

### Whats missing in summary

The full transcript provides more context on the recall attempt against Assembly Speaker Robin Voss in Wisconsin and the involvement of key figures like the "pillow guy," along with uncertainties surrounding potential success and implications for the Republican Party.

### Tags

#Wisconsin #GOP #RecallAttempt #RobinVoss #TrumpSupport #BattlegroundState


## Transcript
Well, howdy there, internet people, let's bow again.
So today, we are going to talk about the GOP.
We're gonna talk about Wisconsin.
We are going to talk about Assembly Speaker, Robin Voss,
and a recall attempt.
Okay, so what's going on?
The Assembly Speaker, Robin Voss,
is one of the most powerful Republicans in Wisconsin.
And a recall petition has been circulating amongst Republicans.
They need a little less than 7,000 signatures.
Reporting says they have 10,000.
They have until this afternoon to produce them.
And then they'll go through, verify their signatures, all of that stuff.
And it might be enough to initiate a recall.
Why would Republicans want to go after one of the most powerful Republicans in the state?
Because he's not Trumpy enough.
That's really what it boils down to.
They are upset about Voss's opposition to decertifying back in 2020, as far as the election
goes.
Voss was not somebody who wanted to go along with that.
They're mad that he didn't push forward the impeachment of an election official.
And he's not super supportive of Trump.
Those are really the reasons.
Now the crew behind this, the pillow guy, had something to do with it.
But they apparently have the signatures.
Now there are still unanswered questions, because if you remember, the state just had
all of their maps done, they just had all their maps redone.
So it is unclear which map would need to be used for this process.
That may be something that comes up.
it's also unclear that they would be successful in ousting Voss. Again, this is a petition for
the recall. There's still more to go along with this, and Robin Voss is one of the most powerful
Republicans in the state. If they were successful in ousting him, it would be huge news and it would
send shockwaves through the Republican Party because this is a battleground state. This is a
state that's going to matter a lot in 2024, in the 2024 election. So that's what's going on. We'll
We'll find out a little bit more about it this week
and how it's going to progress.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}