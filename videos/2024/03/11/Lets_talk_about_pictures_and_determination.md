---
title: Let's talk about pictures and determination....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=D8TjP5NN8D8) |
| Published | 2024/03/11 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explaining how different events impact various age groups differently.
- Receiving a message critiquing his reasoning in a recent video.
- Describing the concept of "trauma memes" related to pictures or videos.
- Using the vulture picture as an example of a trauma meme.
- Connecting the vulture picture to the Ethiopian famine in the 80s.
- Mentioning Kevin Carter as the photographer of the vulture picture.
- Hinting at the concept of moral injury and suggesting looking up Kevin Carter.
- Disagreeing with part of the definition of moral injury.
- Ending with a positive message and wishing everyone a good day.

### Quotes

- "Are these pictures or videos trauma memes?"
- "Trauma memes."
- "That photo is symbolic of that entire period."
- "And if you want to talk about moral injury, you should probably look him up."
- "Nah, F that. We're stopping this."

### Oneliner

Beau talks about the impact of events on different age groups, trauma memes, the vulture picture, Kevin Carter, moral injury, and ends with a positive message.

### Audience

Content Creators

### On-the-ground actions from transcript

- Research trauma memes and how they impact perceptions (suggested)
- Look up Kevin Carter to understand his work and the context behind the vulture picture (suggested)

### Whats missing in summary

The emotional depth and detailed exploration of trauma memes and their impact on perception and memory.


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about pictures, determination,
and a message I received about that recent video where we
were talking about how different things hit different
generations in a different way, even though it's not
really generations.
It's age groups.
And we're going to go through the message.
And then, well, we'll go through the first part,
a little commentary, and then the second part.
And if you missed that video,
it was talking a lot about how,
about how people who were around
and witnessed Ethiopia in the 80s saw that coverage,
how that is influencing
how they respond to current events today.
Here's the message.
Hello, BoomerBow, you're over 30, you're a boomer to us.
I thought your reasoning was not good.
But then I read the comments under that video,
and all of your fellow boomers were
talking about the different things they
remember from back then.
I made the mistake of looking up a few.
And then I got to the vulture picture
everybody was talking about, and I get it now.
Are these pictures or videos trauma memes?
Things that everybody just knows?
Trauma memes.
I never would have used that term, but it's a good one.
Yeah, that's exactly what they are.
They're images that evoke an idea or an emotion.
And if you were exposed to it, you recognize it,
you're aware of its meaning. That's a really good term and a very
unique observation. And just like memes of today, get ready for a twist, they
often lose their origin. If you're not familiar with the photo, it's a photo of
a vulture looking at a child. It's a pretty powerful photo. It's not from Ethiopia. It's
from years later in Sudan. But because Ethiopia was so defining, all famine, all of that imagery
gets evoked in the same way.
It brings up the same things, the tune of We Are the World,
all of that.
That photo is symbolic of that entire period.
And it didn't originate where most people think it did.
And the only reason I know this is because I
looked into the photographer.
And if you want to talk about moral injury,
you should probably look him up.
His name's Kevin Carter.
The rest of the message.
I also looked up moral injury.
And if I understand the way you're using it,
disagree again. Part of the definition is fails to prevent. Nah, F that. We're stopping this.
good anyway it's just a thought y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}