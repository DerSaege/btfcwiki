---
title: Let's talk about Trump, NY, and a difficult task....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=m5l5iQL2USA) |
| Published | 2024/03/27 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explaining a partial gag order issued against the former president in the New York hush money case, restricting him from criticizing certain individuals.
- The order aims to prevent inflammatory statements that could disrupt the court's proceedings.
- Speculating on the potential challenges the former president may face, especially concerning witnesses like Cohen and Miss Daniels.
- Anticipating difficulty for the former president when speaking off-script and addressing personal issues.
- The situation unfolds from April 15th onwards, with uncertainties about how it will progress.

### Quotes

1. "Given the nature and impact of the statements made against this court and family members thereof, the district attorney and witnesses in this case..."
2. "Such inflammatory extrajudicial statements undoubtedly risk impeding the orderly administration of this court."
3. "This seems like something that'll probably come up."
4. "It definitely appears like it will be difficult for him."
5. "Y'all have a good day."

### Oneliner

Beau breaks down a partial gag order in the New York hush money case against the former president, predicting challenges ahead, especially with witnesses like Cohen and Miss Daniels.

### Audience

Legal observers, political analysts

### On-the-ground actions from transcript

- Stay informed about the developments in legal cases involving public figures (implied)

### Whats missing in summary

Insights into the potential legal ramifications and implications of the partial gag order on the former president.

### Tags

#Trump #NewYork #HushMoneyCase #GagOrder #LegalProceedings


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about Trump and New York.
What's going on up there, the order that was entered,
what it covers, what it doesn't,
and how part of it is probably a really bad sign
for the former president,
because it seems like something
that's gonna be incredibly difficult for him,
and just kind of run through everything. So if you don't know what's going on the
judge in the New York entanglement commonly being referred to as a hush
money case which is really a falsification of business records case
has issued a partial gag order against the former president. Under it he is not
allowed to criticize court staff, he's not allowed to go after them, family
members, witnesses, but he is allowed to make statements about the judge and the
DA. Now the order itself it starts off by saying you know in the beginning we
didn't issue this order and then it says given the nature and impact of the
statements made against this court and a family member
thereof, the district attorney and an assistant district
attorney, the witnesses in this case,
as well as the nature and impact of the extrajudicial statements
made by the defendant in the D.C. Circuit case, which
resulted in the D.C. Circuit issuing an order restricting
his speech.
And given that the eve of trial is upon us, it's time, basically.
It goes on to say, such inflammatory
extrajudicial statements undoubtedly
risk impeding the orderly administration of this court. Okay, so this isn't a
surprise if you've been paying attention to the other entanglements. Where this
one might get difficult for the former president is the witness aspect. This
would presumably apply to Cohen and Miss Daniels. It seems unlikely that the
former president will refrain from making statements about them. Cohen in
particular, because of their, let's just call it adversarial relationship, it
It seems likely that the former president is going to want to talk about it.
It's something we're going to have to wait and see play out, but I feel like this is
going to be incredibly difficult for the former president, especially in those moments where
he stops looking at a teleprompter and kind of goes off script and just starts airing
his issues, this seems like something that'll probably come up.
Could be wrong, but it definitely appears like it will be difficult for him.
So this starts on April 15th, and we'll see how it plays out from there.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}