---
title: Let's talk about infrastructure and Baltimore updates....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Ypy-75iNW7s) |
| Published | 2024/03/27 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Updates on the incident in Baltimore involving a ship striking a bridge are provided, with new information suggesting it was an accident.
- The ship lost power, issued a mayday, leading to traffic being blocked, potentially saving lives.
- Emergency responders are still in search and rescue mode, not just recovery, with some workers unaccounted for.
- At least six workers are missing, with two rescued from the water and one in a trauma center.
- Sonar has identified vehicles that fell into the water, including three passenger vehicles, a cement truck, and an unidentified vehicle.
- Pollution mitigation efforts have begun, with the pollution deemed not significant, although the Coast Guard has closed the waterway into the harbor.
- The incident has sparked a broader national debate on infrastructure, beyond just this specific event in Baltimore.
- Beau stresses that the US is behind in infrastructure, with aging and outdated technologies that need a more forward-thinking approach.
- Strengthening existing infrastructure is necessary, but Beau suggests the US needs to look beyond immediate issues to catch up.
- Beau proposes building out unused capacity at other ports to prevent future supply chain interruptions.

### Quotes

- "It's not just about fixing problems. It should be about stopping the problems from occurring in the future."
- "What we have is not enough. What we have is aging. What we have is outdated technologies."
- "It's thinking beyond the immediate that is going to get somewhere."

### Oneliner

Beau provides updates on the Baltimore incident, stressing the need for a more forward-thinking approach to US infrastructure beyond addressing immediate issues.

### Audience

Policy Makers, Infrastructure Planners

### On-the-ground actions from transcript

- Enhance infrastructure planning to prevent future incidents (implied)
- Advocate for forward-thinking infrastructure development (implied)

### Whats missing in summary

The full transcript provides a comprehensive analysis of the Baltimore incident and prompts a reevaluation of national infrastructure planning and development.

### Tags

#Infrastructure #Baltimore #ForwardThinking #Policy #EmergencyResponse


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about infrastructure
and how we talk about infrastructure
and the way we think about it.
But before we get into that, we'll
go through the newer information when
it comes to what occurred up in Baltimore
and just provide a little bit of an update
what's occurring up there. Okay, so the most recent information indicates that
it was an accident. The ship lost power shortly before striking the bridge. The
ship issued a mayday. That mayday was received and it led to traffic being
blocked. A move that undoubtedly saved lives. There are a lot of theories
floating around. The fact that the ship itself issued the mayday that led to
traffic being blocked, it casts a little doubt on some of those theories. The
Emergency responders up there are saying that it is still search and rescue and not just recovery, which is good news.
An unknown number of workers who were performing maintenance on the bridge are unaccounted for.
There are at least six still missing. Two have been pulled from the water successfully.
successfully. One is apparently fine. The other is at a trauma center. Sonar has identified
a number of vehicles that fell into the water. Three are passenger. One is a cement truck
and another is unidentified. There seems to be an expectation that Sonar will
identify more. That wasn't said, but it seemed to be the subtext of what was
being said. As far as pollution, there is some. They have already started
mitigation efforts and the pollution was deemed quote not significant. I do not
know what that means. Coast Guard it appears has closed the waterway into the
harbor. There will be supply chain issues because of this. Okay so that's the most
recent update that I have. That's the most recent information. Now because of
this, you have a lot of people, obviously, talking about infrastructure, but when
they're talking about it, they are talking about this specific incident.
They're talking about this. You know, this is, these are things that we should do to
bridges, you know, to mitigate this and make sure it doesn't happen again, and
you know, which is the natural response. It makes sense. You don't want this to
happen again, you want to address that immediate issue. Yes, and, yes, and, yeah,
I mean obviously you want to strengthen existing infrastructure, but the US is
way behind when it comes to infrastructure. It's not a thing where we
can just safeguard what we have. What we have is not enough. What we have is aging.
What we have is outdated technologies. It needs to be more forward-thinking and
not just dealing with the immediate issue. This immediate issue of the
collapse leads to supply chain issues. Why? Because there's also an
infrastructure issue there. It might be wise to build out unused capacity at
other ports. So if a supply chain interruption does occur in the future,
it's more easily mitigated. An incredibly far-thinking country might
consider building out an additional port, east coast, west coast. One that
isn't really used. It's thinking like that beyond the immediate that is
going to get somewhere, that is going to allow the United States to catch up. It's
not just about mitigation, because making sure what we have functions, yeah, it's
important undoubtedly, but that's not a solution to the overall problem. The
overworked infrastructure leads to other issues. So when the conversation
arises, yes, and we need to do more. It's not just about fixing problems. It should
be about stopping the problems from occurring in the future, seeing what's
coming down the road and altering the outcome. Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}