---
title: Let's talk about State and a resignation....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=zYaB5EXpKSY) |
| Published | 2024/03/27 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A junior at the State Department has publicly resigned due to US support of Israel and its impact on international law.
- The resignation is tied to the call for increased US aid to prevent moves into RAFA and potentially cut off offensive military aid.
- The State Department has not issued a finding on Israel's compliance with international law despite media reports suggesting otherwise.
- Netanyahu's actions, like pulling a delegation about RAFA, reinforce concerns about humanitarian issues not being taken seriously by the US.
- Despite talks being rescheduled, the pressure seems insufficient to change the opinion of the resigned State Department employee.
- The official finding from the State Department is still weeks away.

### Quotes

1. "The resignation is tied to the call for increased US aid to prevent moves into RAFA."
2. "The State Department has not issued a finding on Israel's compliance with international law."
3. "Netanyahu's actions reinforce concerns about humanitarian issues not being taken seriously."
4. "The pressure seems insufficient to change the opinion of the resigned State Department employee."
5. "The official finding from the State Department is still weeks away."

### Oneliner

A State Department junior resigns over US support of Israel, calling for increased aid while confusion arises over compliance with international law.

### Audience

State Department staff

### On-the-ground actions from transcript

- Contact organizations supporting Palestinian rights (suggested)
- Monitor and advocate for increased US aid to prevent violations in RAFA (exemplified)

### Whats missing in summary

The full transcript provides detailed insights into a State Department resignation linked to US support of Israel, adding clarity to international law compliance concerns. 

### Tags

#StateDepartment #Resignation #USsupport #Israel #InternationalLaw


## Transcript
Well howdy there internet people, it's Beau again. So today we are going to talk about
a very, very public
resignation and
we'll talk about the reasoning for it a little bit and then we will talk about
some additional information related to that
and just kind of go through everything.
a person at State Department has resigned
and they have publicly put their reasoning
out there. I'm not going to
you know read the whole thing because it is lengthy but the link will be down there.
Okay, so let's run through
what's happening. The person who resigned
she is a junior
at State Department. I think she's on with a
two-year contract or was on with a two-year contract as a foreign affairs
officer and her resignation is directly tied to US support of Israel and how it
and how it plays out as far as international law. One of the big things
about this is that she was a short-term staff member there. That's going to be
one of the things. It's worth noting that in her explanation she talks about how
her colleagues asked her to speak out because she is resigning and some of
them aren't. What are the reasons? Exactly what you would imagine. The different
findings by the international bodies. The US kind of not really taking those
seriously but the big ask, the big ask is that the US do more for aid to stop a
move into RAFA and if necessary cut off offensive military aid. Now when I say
very public, what do I mean? I mean CNN, she wrote something for CNN and it has
gone out. Now there are some developments and some things that need to be
clarified because this is a question that came in a whole bunch and it's not
understanding exactly what was said and it's actually kind of mentioned in the
statement of reasons. You might have seen a bunch of headlines saying that the US
has determined that Israel is in compliance with international law. That
is not what happened. This is the actual statement that those headlines are based
on. We have not found Israel to be in violation of international humanitarian
law either when it comes to the conduct of the war or when it comes to the
provision of humanitarian assistance. And then it goes on to say, you know, this
view is informed by our ongoing assessments. We have not found them to
be in violation. That is not the same thing as we have found them to be in
compliance. The reason that neither of those statements are correct is because
State Department hasn't issued a finding. They haven't found them to be in
violation because they haven't issued a finding. They haven't found them to be in
compliance because they haven't issued a finding. That statement went out the day
after the statements, the assurances, and all of that stuff came in. My
My understanding is that the official finding is weeks away.
That's why that statement was made.
There's a lot of confusion over what that meant.
The other thing worth noting is after the U.S. did not exercise its veto at the Security
counsel. Netanyahu's big move was to pull that delegation about Rafa, not have
that conversation, and I said that's just reinforcing the opinions of those people
at State Department who are saying they're not taking the humanitarian
issues seriously, and that as soon as he did that, they won the argument. It is
worth noting that while he is still saying that they're going into RAFA, the
delegation, those talks, they're being rescheduled. It does appear that the
pressure is having some effect. What effect? I don't know. It is certainly not
enough to change the opinion of the State Department employee who resigned.
Again, it's an interesting read and it will be down below.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}