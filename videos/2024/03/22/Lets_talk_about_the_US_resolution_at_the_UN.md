---
title: Let's talk about the US resolution at the UN....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=hBy8Uzh7PG0) |
| Published | 2024/03/22 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- The US-backed UN ceasefire proposal is moving forward for a vote soon.
- The proposal calls for an immediate ceasefire tied to the release of people.
- The UN has more leverage compared to the ICJ to enforce a ceasefire, although military action is unlikely.
- Pressure from the proposal is aimed at Israel, Hamas, Hamas' backers, and the US.
- The main goal is to get aid in and protect civilians, not necessarily halting Israel's pursuit of Hamas.
- Netanyahu is trying to leverage his relationship with the GOP in the US to alleviate pressure.
- The resolution opens up new ways to apply pressure and sets a tone for countries' priorities.
- The focus is on pressuring negotiators to reach a lasting peace deal rather than an immediate ceasefire.
- The civilian loss and humanitarian crisis are the driving forces behind the action.

### Quotes

1. "Does this stop the fighting?"
2. "The odds of getting an enforced ceasefire by the UN are super unlikely."
3. "It isn't a guarantee that the fighting will stop but it puts a whole lot more pressure."
4. "There's a big difference between it not mattering and it leading to the immediate ceasefire."
5. "The resolution won't do it by itself, but it's a tool to apply pressure to make it happen."

### Oneliner

Beau explains the US-backed UN ceasefire proposal, focusing on pressure, leverage, and the hope for lasting peace through negotiations.

### Audience
Negotiators, peace advocates

### On-the-ground actions from transcript

- Pressure negotiators for a lasting peace deal (suggested)
- Advocate for aid distribution and civilian protection (implied)

### Whats missing in summary
Details on the ongoing negotiations and potential outcomes are missing from the summary.

### Tags
#UN #CeasefireProposal #USBacking #Pressure #Negotiations


## Transcript
Well, howdy there internet people, it's Bo again. So today we are going to
talk about the UN and the US backed version
finally moving forward. As I understand it,
it looks like it will come up for a vote tomorrow.
And we're gonna go through what that means
because a number of questions came in and we're just gonna kinda go through it.
And this is one of the questions.
I remember you saying the US had its own UN ceasefire proposal.
They're submitting it.
The people who are acting like this is the greatest thing since sliced bread are the
same people who said the ICJ would matter.
You said it wouldn't and you were right.
I know you're sick, but also know you'll still make videos.
So can you give us a no BS learning to play the poker game summation?
Does this stop the fighting?
Okay, so this proposal that does call and put support behind an immediate ceasefire
tied to the release of people, because I know people are going to ask, those two things
go together.
There's a whole lot of other stuff in it, but that's the key part.
Does it stop the fighting?
No, but, and it's a big but.
It's not like the ICJ.
The ICJ, it was a piece of paper.
It doesn't stop fighting.
This is different.
The UN has a whole lot more levers to pull.
The odds of getting an enforced ceasefire, where the UN actually authorizes military
action to enforce a ceasefire, that is super unlikely.
Israel is a nuclear power.
is incredibly unlikely, but there are a bunch of other levers to pull. It is
important to remember that if this goes through, the pressure that is going to be
exerted, it's not just aimed at Israel. It'll be aimed at Israel, it'll be aimed
at Hamas, it'll be aimed at Hamas' backers, it'll be aimed at the US. The US is
obviously prepared for that. So it isn't a guarantee that the fighting will stop
but it puts a whole lot more pressure, a lot more. It's not pointless. The
other thing to remember is that there are ongoing negotiations for a ceasefire
and they're saying they're getting close. We'll see what that means. It's also
important to remember what the real position of a whole lot of the countries
backing this is because that that's kind of getting lost in this. The real
position is not Israel stop going after Hamas. The real position is you've got to
get the aid in and you've got to protect civilians. They don't... many of the
countries wouldn't care if Israel continued to pursue a campaign against
Hamas as long as civilians were protected and the aid was going in. It's
important to keep that in mind as well.
Imbia protects civilians specifically about Rafa which is actually mentioned
in the resolution. Now Netanyahu appears to be trying to leverage his
relationship with the GOP in the US with Republicans in the US to to kind of
alleviate some of that pressure. If it goes through I don't think it's going to
matter much. There's going to be a lot of pressure. So it is not like the ICJ but
it is also not this passes and everything stops and they stop fighting.
It opens up a whole bunch of new toolboxes to get that pressure and it
also sets a tone as to what various countries care about. So we'll have to
wait and see how it plays out because there's a bunch of different ways this
could move from it meaning very, very little to it turning into an economic
thing to there's just it's a list of different ways that it could play out.
I feel like the reason they're pushing it forward now is if what's being said
about the actual negotiations is true, this is their way of putting pressure
on them to close that final gap and get get the details worked out because
whatever deal they have worked out together, meaning Israel and Hamas, it's
probably better for both sides than what the UN would push for. So it isn't a
guarantee of the end of the fighting, but it's also not pointless. It's not
something that just doesn't matter. There's a big difference
between it not mattering and it leading to the immediate ceasefire that it's
calling for. There's a whole bunch of different options in the middle of that.
it's important to remember the real line that was crossed was the civilian
loss and the humanitarian situation. That's what's prompting the action.
So, we'll have to wait and see. If you want to ceasefire, hope that this move
puts the pressure on the negotiators to close that final gap and come up
with a deal that actually leads to a durable, a lasting peace. That's what you
need to be hoping for right now. This resolution won't do it by itself, but
it's a tool to apply pressure to make it happen. Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}