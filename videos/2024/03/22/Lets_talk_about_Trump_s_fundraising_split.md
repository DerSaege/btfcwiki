---
title: Let's talk about Trump's fundraising split....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=mplDhk4SWyQ) |
| Published | 2024/03/22 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump has a big fundraiser scheduled for April 6th, with incredibly wealthy guests attending.
- Money raised at the fundraiser will be divided among different entities, starting with the Trump 47 committee.
- The maximum allowable donations by law will go to the Trump general and primary campaign.
- The Save America pack will receive funds, known for paying millions in Trump's legal bills.
- The media will likely focus on the Save America pack's involvement in the fundraiser.
- Despite media attention, the actual impact of this specific fundraiser may not be significant due to donation limits.
- The key concern should be whether this fundraising division pattern will apply to all joint activities between Trump and the RNC.
- Donors may be deterred if a significant portion of their contribution goes to the Save America pack.
- The fundraiser, in terms of financial impact, may not be as significant as the broader implications for future fundraising efforts.
- Beau suggests that the focus should be on the potential implications for future fundraising activities rather than this specific event.

### Quotes

1. "The question isn't really about this fundraiser, it shouldn't be."
2. "Y'all have a good day."

### Oneliner

Beau explains the financial breakdown of Trump's upcoming fundraiser, suggesting that the focus should be on future joint fundraising activities rather than this specific event.

### Audience

Political donors

### On-the-ground actions from transcript

- Attend or support fundraisers for political candidates (exemplified)
- Stay informed about how political fundraising is conducted (exemplified)

### Whats missing in summary

Insights on the potential impact of fundraising practices on political contributions.

### Tags

#Trump #Fundraiser #SaveAmericaPack #PoliticalDonors #JointFundraising


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about Trump
and that big fundraiser he has coming up
with all the billionaires
and how that money is going to get divided up
because that information is out now.
And I think it's important to put it into context
because I'm sure a lot is going to be said about this.
So if you've missed the news, on April 6th there's going to be a big fundraiser
for Trump and it's a joint fundraising endeavor and the guests are going to be
incredibly wealthy. Okay so it looks like the money is going to Trump 47
committee and then from there the maximum donation allowable by law will
go to the Trump general and primary campaign. After that the next group to
get money would be the Save America pack and then they get the max donation
allowable by law. After that then it goes to the RNC and the state level parties
if there's money left over. Okay? Undoubtedly this is going to get a lot
of attention because the Save America pack, while it does a lot of other
things, one of the things it is known for right now is paying a whole bunch of
Trump's legal bills to the tune of millions. Like a lot of millions, not even
just a few millions. So here's the thing. The media is going to talk about this a
lot because of that fact. At this particular fundraiser, it really doesn't
matter. Not really. Because of the dollar amounts involved and the maximum amount
that you're allowed to donate to the first two categories, it's really not
that much. The question isn't really about this fundraiser, it shouldn't be.
The question should be whether or not this type of split is how all of the
joint fundraising activities between Trump and the RNC are going to play out
because that might matter. If somebody is not a donor who's going to donate, you
know a quarter million dollars but a big donor but not a huge one like let's say
10,000 they may be less likely to help Trump out and donate through this way
because they know half of it would end up going to the Save America pack or
close to half of it so that's really where this would come into play at this
This dinner, I'm sure it's going to get a bunch of coverage, but realistically it doesn't
really matter.
You're talking about small percentages in this one.
The question is, is this what's going to apply in the future?
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}