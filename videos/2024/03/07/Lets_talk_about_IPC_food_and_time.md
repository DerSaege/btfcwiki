---
title: Let's talk about IPC, food, and time....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=1QLbhC_Qbyk) |
| Published | 2024/03/07 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the Integrated Food Security Phase Classification (IPC) scale from one to five, with one being good and five representing famine.
- Describes the criteria for something to be considered a famine, including extreme lack of food, acute malnutrition in children, and high mortality rates.
- Mentions that Gaza is currently at a four on the scale, indicating extreme food insecurity.
- Notes that the committee to conduct a famine review has been convened, signaling a critical situation.
- Emphasizes that time is running out for action, as indicated by the urgency of the situation.
- Addresses the need for foreign policy constraints to be set aside in light of the impending crisis.
- Points out that the United States has its own ceasefire proposal at the UN but is delayed due to debates over wording.
- Stresses that immediate action is necessary, as delays in aid and ceasefire agreements could worsen the situation.
- Raises concerns about reports of looting of aid trucks in an area on the brink of famine.
- Urges for prioritizing actions to address the pressing food insecurity and potential famine.

### Quotes

1. "Means they're out of time."
2. "You're out of time."
3. "Everything else has to come second."
4. "Hunger, food insecurity, that's kind of common in the world that we live in, sadly. Famine is rare."
5. "They're out of time."

### Oneliner

Beau explains the urgency of Gaza's food insecurity crisis, stressing the critical need for immediate action as the region faces the risk of famine and time runs out for effective intervention.

### Audience

Global policymakers, humanitarian organizations

### On-the-ground actions from transcript

- Push for immediate implementation of aid and ceasefire agreements to address the escalating food insecurity crisis (suggested).
- Support initiatives aimed at providing sustainable food assistance and preventing further deterioration of the situation (implied).

### Whats missing in summary

The full transcript provides detailed insights into the urgency of addressing Gaza's food insecurity crisis and the need for swift action to prevent a potential famine.

### Tags

#FoodInsecurity #HumanitarianCrisis #Ceasefire #ForeignPolicy #Urgency


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about a scale
and how the clock is ticking.
The clock is ticking because there's been a development
that alters a lot of normal foreign policy concerns
and it should change the math on a whole lot of things.
There is an IPC committee review going on.
What is that, right?
Integrated Food Security Phase Classification, that's a mouthful, IPC.
What is it?
It's a chart, it's a scale from one to five.
One is good, five is famine.
For something to be a famine means 20% of households have extreme lack of food, 30%
of children have acute malnutrition, 2 in 10,000 people are dying of starvation or malnutrition
per day.
How do they measure malnutrition?
They literally measure the circumference of the arm.
A committee to conduct a famine review is convened.
Gaza is currently at four on the scale.
Five is famine.
What does this mean?
Means they're out of time.
Who?
Everybody.
Israel, the United States, everybody's out of time.
the normal foreign policy constraints, they have to go away. They have to go
away. Now there is no guarantee that the committee will move it from four to five.
I don't have that information. I don't know if the criteria are met, but the
fact that the committee has been convened says you're out of time. The
the Masters of the Universe stuff, all the normal foreign policy constraints, they have
to go away.
The obvious question, what can be done?
I think most people watching this channel know that the US has used its veto at the
UN a few times on a ceasefire.
But most people probably don't know, because I've only mentioned it in passing, because,
well, the U.S. has its own ceasefire proposal.
It's been up there for weeks as they argue over verbiage.
The right words to put in the right places.
You're out of time.
You are out of time.
Sure, it appears that the airdrops are sustained.
But they're not scaled.
You're out of time.
The trucks going in, there's a report that says that they've increased.
I don't know.
I can't confirm that.
There's also reporting that the trucks are being looted.
Well I mean that's kind of understandable when you're talking about an area on the
brink of famine.
You're out of time.
out of time. Everything else has to come second. The U.S. proposal, they've been
arguing over terminology when it comes to how the ceasefire should be phrased.
I'm not joking. That's really what it's kind of boiled down to. The most recent
version of it that I'm aware of is immediate ceasefire for roughly six
weeks or something like that.
Yeah, that's good enough for right now because what matters is the immediate
part out of time. When I was saying last week, it could get bad. It could get really bad.
And I had a whole bunch of people saying, it already is really bad. This is really bad.
This is really bad. Hunger, food insecurity, that's kind of common in the world that we
live in, sadly. Famine is rare. It's different and it's much worse. They're out of time.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}