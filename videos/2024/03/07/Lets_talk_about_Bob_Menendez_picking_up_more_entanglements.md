---
title: Let's talk about Bob Menendez picking up more entanglements....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=8xEhlr2Apbc) |
| Published | 2024/03/07 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Senator Bob Menendez from New Jersey faced new charges related to obstruction and a cover-up.
- The allegations involve what the feds call bribes, which Menendez claims were actually loans.
- Menendez's co-defendant on previous allegations entered a guilty plea and began cooperating.
- The federal government does not believe Menendez's explanation regarding the alleged bribes.
- Menendez's re-election prospects are affected as he is currently polling in single digits.
- All Democratic senators have called for Menendez's resignation, with talks of possible expulsion.
- Despite the ongoing legal issues, Menendez is still running for re-election.
- Republicans who claim this is election interference are not making the same argument in this case.
- The timing of legal actions cannot be adjusted to impact elections for partisan purposes.
- Election interference claims are unfounded in this context.

### Quotes

1. "The allegations involve what the feds call bribes, which Menendez claims were actually loans."
2. "The federal government does not believe Menendez's explanation regarding the alleged bribes."
3. "All Democratic senators have called for Menendez's resignation, with talks of possible expulsion."
4. "Republicans who claim this is election interference are not making the same argument in this case."
5. "Election interference claims are unfounded in this context."

### Oneliner

Senator Bob Menendez faces new charges of obstruction and a cover-up, claiming alleged bribes were loans, impacting his re-election prospects amid calls for resignation.

### Audience

Political observers

### On-the-ground actions from transcript

- Contact your representatives to express your opinion on Senator Menendez's situation (suggested).
- Stay informed about the developments and decisions regarding Senator Menendez's legal issues (implied).

### Whats missing in summary

Context on the legal process and potential implications for Senator Menendez's political career.

### Tags

#BobMenendez #NewJersey #DemocraticParty #ElectionInterference #LegalCharges


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are once again going to talk about
Senator Bob Menendez out of New Jersey,
the Democratic Senator out of New Jersey,
and talk about the developments there,
and just kind of run through everything
and point out something that is noticeably
absent from a lot of the commentary about this.
Okay, so if you don't know what's going on, miss the news.
Senator Menendez has picked up yet another set of charges.
I don't know how many sets this is,
but I hope it's the last
because I'm running out of Janis Joplin shirts.
The newest allegations come days after his co-defendant
on previous allegations entered a guilty plea
and began cooperating. The new conduct that is alleged could be best summarized as obstruction
and a cover-up. It basically focuses on the idea that is being presented by Menendez that
what the feds are referring to as bribes were actually loans and that he was totally going
to make amends for that Mercedes-Benz. For those that don't know, the allegations include
that he took what the feds are calling bribes in the form of payments on a Mercedes and I think
even a mortgage at one point. But Menendez is saying that those were not, in fact, bribes,
that they were loans and that he was going to repay those, or did repay them, I think. At least,
I think they were checks that were shown. The federal government does not believe that explanation.
Okay, so obviously this is going to continue to cause issues for Menendez when it comes
to re-election.
He's currently polling in the single digits, I believe.
The Democratic Party, I actually think all Democratic senators at this point have called
his resignation and there is light talk of actually moving to expelling him. But
it is interesting that he's running for re-election isn't it? And with all this
going on and it keeps happening and it keeps progressing you know that they
continue to do it, even new charges. It's weird, right? Because I've been told by a
lot of Republicans that they can't do this, that this is election interference.
Doesn't seem like it is. It's weird that, you know, the Republican commentators
aren't making that claim here, right? It's just just for the one person. The
The reality is this is how it's done.
They can't adjust the timing to impact an election for the purposes of affecting an
election, or providing an advantage or disadvantage to any candidate, so they're just supposed
to move on forward with it once it's rolling.
It's not election interference, and those who keep telling you that it is, they know
It's not, and they don't make that claim at any other time.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}