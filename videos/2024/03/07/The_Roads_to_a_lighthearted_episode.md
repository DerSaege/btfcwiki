---
title: The Roads to a lighthearted episode
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=oonE_eLmKZ8) |
| Published | 2024/03/07 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Providing a lighthearted Q&A session for viewers, filming ahead of time without knowing the news.
- Updates on his recent surgery, feeling well with no pain, and upcoming medical checks.
- Supporting the University of Southern California, Northridge, and Project Rebound, aiding formerly incarcerated individuals in education.
- Humorous flashback about not planting fruit trees yet due to surgery.
- Sharing the last book he read, "Taking Down Trump," by attorney Tristan Snell.
- Explaining a coffee cup Easter egg in the background of a video.
- Teasing upcoming content on Trump underperforming in fundraising and polls.
- Addressing a glitch with an unseen white MRE in a video.
- Humorous exchange about buying a new shirt to avoid talking about Trump.
- Mentioning the pause in interviewing people due to internet issues but considering resuming it.
- Playful banter with his wife about annoying traits.
- Clarifying his involvement with the horses on the property.
- Responding to a question about Route 66 and his beard changing colors due to lighting.
- Sharing thoughts on predicting the next "woke" content.
- Revealing having numerous hat patches attached with Velcro, unsure of the exact count.
- Explaining how he selects causes to support on the channel, often based on viewer suggestions.

### Quotes

1. "Sometimes a cigar is just a cigar, and sometimes a coffee cup is just a coffee cup, not an Easter egg."
2. "If you have suggestions, send them in, and we'll see what we can do."

### Oneliner

Beau provides light-hearted updates, supports education for the formerly incarcerated, and hints at upcoming content on Trump underperforming and hat patches. Sometimes a coffee cup is just a coffee cup.

### Audience

Viewers

### On-the-ground actions from transcript

- Support the University of Southern California, Northridge, and Project Rebound (suggested).
- Send suggestions for causes to support (exemplified).

### Whats missing in summary

Insights into Beau's personality and lighthearted approach may best be understood by watching the full transcript.

### Tags

#Q&A #SupportEducation #CommunityEngagement #Humor #ChannelUpdates


## Transcript
Well, howdy there, internet people.
It's Beau again, and welcome to the Roads with Beau.
So today is going to be just a lighthearted Q&A
with questions from y'all.
Haven't seen them yet, normal routine.
And we're filming this ahead of time.
I don't know what the news is going to be.
So this is something because I know
y'all have come to expect something on Thursday mornings
here.
OK, how was your surgery?
It went well.
I feel fine.
I've got a checkup.
And I'll go in and check it out here soon.
But everything seems to be good.
Even the pain is gone at this point.
So OK, and just in case, so I don't forget,
This should go out on Thursday.
During this period, we will be
helping the University of Southern California, Northridge,
and something called Project Rebound,
which is, it is a program that
it assists formerly incarcerated people get through school.
Like it gives them a support structure.
It helps with childcare if they need it, stuff like that.
And last year we helped them out by like sending people to their giving day in their site and they brought in like 20
grand.
So we'll be doing that during this period, the day this goes out, so hopefully there'll be a link down below so y'all
can get there.  But it has a really good track record as far as, you know, cutting down recidivism
and helping people get where they're going, good GPAs, all of that stuff.
And it's one of those things, we'll probably end up doing this every year now.
Are the trees in the ground yet?
No, they're not.
I had a surgery.
Flashback humor.
Okay, for those that don't know, my wife was picking on me because apparently I get
away with buying fruit trees, getting fruit trees, getting fruit trees from other people.
Anyway, I end up with a bunch of fruit trees, and it takes forever for me to put them in
the ground.
And yeah, there are still 10 out there that are not in the ground.
But soon, hopefully, as soon as I get an all clear, I'll get them in.
What's the last book you read?
Tristan Snell, and I think it's called Taking Down Trump or something like that.
He's an attorney who went up against Trump and won, and it's interesting because it's
kind of written like a template.
If you are interested in legal stuff, it's worth checking out.
What was that?
Coffee cup Easter egg in the background.
It's white and had writing on it, but I couldn't read it.
Yeah, that was my coffee cup.
That was just my coffee cup.
I set it down back there and forgot about it.
There was no hidden meaning or anything with that one.
I think it said something like, anything that can be imagined can be achieved, or everything
that was achieved was imagined first, something along those lines.
And it kind of fit with the video.
I don't really remember what the video was, but I remember doing that.
Sometimes a cigar is just a cigar, and sometimes a coffee cup is just a coffee cup, not an
Easter egg.
Can you explain what you mean by Trump underperforming?
I have a video coming out on this so I don't want to say.
Short version, he is underperforming when it comes to a couple of different things.
Fundraising is one.
But people are looking at the polls, and he is underperforming in key places that matter.
And there's a bunch of different ways, but that's the key one I think needs to be addressed.
I saw a video of yours that wasn't public fact-checking pictures about expired MREs.
I couldn't comment.
did you ever find out where the white MRE came from?
Yeah, that is such a weird glitch.
Every once in a while, you'll catch a video
that isn't, that y'all aren't supposed to see yet.
Yes, actually, the white MRE is a UN, it's a UN food bar.
It wasn't part of the airdrop to my knowledge.
It was not, it is certainly not US, that much I know.
But I don't... I have more information, but I think the important thing is not where it came from, but when it showed
up.
Okay, note you got two of these from different people and they're almost identical.
Wash your car and it rains. Buy a new detrumpification shirt so you won't keep wearing one that's so worn we can't even
read it anymore and maybe you won't have to wear it so often.
Think if I buy a new shirt, I won't have to talk about Trump anymore. I'll give it a shot.
Why don't you interview people anymore? We stopped because of the internet
connection actually not being able to sustain it. We think we have that fixed
it will come back but it's the other content comes first you know I've been
toying with going back to three videos a day to make more time to do stuff like
that, because I do. I'm very conscious of the fact that people have a schedule and like
to stay on it. And I know that for a lot of you, these videos and being in the comments,
it's become a part of your day. So I try not to miss anything, which means a lot of other
Other stuff gets pushed back.
When you and your wife were arguing about the coffee cup, she didn't really say what
you thought was her most annoying trait because I'm smart.
What is it we won't tell?
She doesn't have one.
She has no annoying traits.
She over explains things.
She over explains things at times.
It's not a constant thing, but it's just a normal communication thing.
I'm normally pretty concise, and she talks more.
Do you really not help with the horses at all, in all caps?
No, it's not that I don't help at all.
It's that the horses are her thing.
in her, and for those that don't know, we have horses here that are, they're broke
or they were hurt or they're retired or they didn't fit in where they were at and you
know, she kind of runs a horse foster system, I guess, even though they rarely leave.
For her, it's fun.
She enjoys doing it.
For me, it's work.
I don't enjoy going out and cleaning out a stall.
She puts her headphones on, it goes out there, and enjoys it.
Me, not so much.
But it's not that I don't do it at all, it's that it is definitely not my thing.
What's the white guy fascination with Route 66?
I didn't know that was a white guy thing.
It's just a,
it's a road trip, it's an adventure, Chicago to LA.
Like, it's something, I guess it resonates
with a lot of people.
And it's, I don't know.
The draw of the open road, to be cliche, I guess.
Why does your beard change colors in videos?
It's the lighting, because I'm in different spots.
People talk about that every once in a while,
where I get like white spots in my beard.
It's from the lighting.
Predict the next show movie book comic
that has always been woke,
that people will suddenly call woke.
I can't predict that. It could be anything.
There's a new season of Resident Alien.
Of a show called Resident Alien.
I
I would imagine that maybe somebody's gonna watch, you know, because there's a
new season they'll be advertising and somebody will watch it for the first time.
I don't know, claim that it went woke.
I have no idea.
That is a, that's a fun show though, by the way.
Very entertaining.
How many hat patches do you have?
And are those really on there with the Velcro?
Yes, they're really on there with the Velcro.
How many do I have?
I have no idea.
A lot, a lot.
50 maybe?
I'm guessing, I don't know.
How do you pick the causes you want to support on the channel?
Normally, y'all tell me.
Normally, y'all are like, hey, this organization needs help,
or they're in trouble, or something like that.
Something like that comes through,
and if we have the ability, we do it.
That's where it comes from.
There are the normal ones that we do every year,
or if there's a strike or something like that.
But normally, the ones that kind of come out of nowhere,
where it's like, OK, so we're going to do a live stream
to help a wolf preserve.
That's when that happened.
One of y'all was like, hey, they need help.
So, yeah, if you have suggestions, send them in, and we'll see what we can do.
We can't always do it for everyone, but if we can, we will.
And that looks like it.
So yeah, I knew it was going to be short, but yeah.
that's it. We'll be back to the normal, regularly scheduled stuff on this channel soon. Probably
Sunday, I would hope. Okay. Anyway, so a little more information, a little more context and
and having the right information will make all the difference.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}