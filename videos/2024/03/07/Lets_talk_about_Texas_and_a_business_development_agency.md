---
title: Let's talk about Texas and a business development agency....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=xKRMyqQjV08) |
| Published | 2024/03/07 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Providing an update on helping Project Rebound at California State University Northridge, a fundraiser aiding formerly incarcerated individuals with college access.
- The fundraiser has raised nearly $40,000 from almost 500 people at the time of filming.
- Last year's fundraiser raised around $23,000, showing significant growth.
- Moving on to news about a federal judge in Texas ruling that the Minority Business Development Agency must offer services to white individuals.
- The judge cited the 14th Amendment for equal protection under the law, demanding the agency not use race or ethnicity in applicant considerations.
- The ruling alters the operations of the agency established in 1969 under Nixon.
- Under normal circumstances, this ruling may not stand due to potential implications and legal challenges it opens up.
- With a Trump-appointed judge overseeing the case, the outcome becomes unpredictable given previous controversial rulings.
- These agencies address systemic issues that still persist, and the future standing of this ruling remains uncertain.
- Hope remains that the Supreme Court will not allow this ruling to stay due to its far-reaching consequences.

### Quotes

1. "Understand these entities came into being and exist because of systemic issues that have not been addressed."
2. "It's not like the problem is solved."
3. "I am hopeful that the Supreme Court won't allow this to stand."
4. "It totally altered everything about the Minority Business Development Agency."
5. "Okay, on to the news."

### Oneliner

Beau provides an update on supporting Project Rebound's fundraiser for formerly incarcerated individuals while discussing a controversial ruling by a Trump-appointed judge requiring the Minority Business Development Agency to offer services to white individuals, challenging previous practices and raising concerns about systemic issues.

### Audience

Community members, activists

### On-the-ground actions from transcript

- Support Project Rebound's fundraiser by donating or sharing the link (suggested)
- Stay informed about legal developments and cases affecting marginalized communities (implied)

### Whats missing in summary

The full transcript provides more context on the specific details of the ruling and implications for ongoing systemic issues.

### Tags

#ProjectRebound #LegalRuling #SystemicIssues #Fundraiser #CommunitySupport


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about some news out of Texas
dealing with a development agency and a ruling
and boy, is it a doozy but before we get into that,
we're gonna provide a little update
on what we've been doing and just tell everybody
where we're at at time of filming.
guys if you don't know we have been helping Project Rebound out at
California State University Northridge and sending people their way because they
are doing a fundraiser. This is an organization that helps establish a
support structure and helps get people who were formerly incarcerated through
college and it does really good work and we've been sending people their way
Now, at time of filming, it is creeping up on $40,000, $40,000 by closing it on 500 people.
The link will be down below.
There is still time.
For context and comparison, last year when we did this, it was around $23,000, and that
was, I mean, that was huge.
So this is, this is really good stuff.
Okay, on to the news.
And a federal judge out in Texas decided that the Minority Business Development Agency needed
to offer services to white people.
Said that they had to provide their services and assistance to all individuals regardless
of race.
This is a Trump-appointed judge who is responsible for some other, let's just call them controversial
rulings, but in relevant part, the order said that they couldn't, the organization was
barred from, quote, considering or using an applicant's race or ethnicity.
The rationale the judge used was the 14th Amendment, equal protection under the law.
This is an entity that has been around since Nixon put it together in 1969.
It was under the Department of Commerce at first.
It got reorganized, reestablished in, I don't know, 2021, 2020?
check that. And yeah, so that happened. It totally altered the, well, everything
about the Minority Business Development Agency. Under normal circumstances with a
normal Supreme Court, I would say there's no chance of this standing because it
opens a whole bunch of doors. It just becomes a giant can of worms because then they have
to determine when the federal government is operating, I don't know, let's say the
VA. I mean that's a service, you know, it was from the government and there will be
arguments that are like that. It's going to open a lot of other, a lot of other doors
to a lot of other bizarre suits.
Under normal circumstances with a normal Supreme Court, I would say there's no way this stands.
However, Trump-appointed judge, there's a lot of Trump-appointed justices.
I don't even want to guess at how this is going to play out, but it is definitely unique.
understand these entities came into being and exist because of systemic
issues that have not been addressed. It's not like the problem is
solved. So I am hopeful that the Supreme Court won't allow this to stand, but
that's hope. That's not belief. I have no idea what they're gonna do. Anyway, it's
It's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}