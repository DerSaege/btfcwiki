---
title: Let's talk about the budget bill and what's next....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=NsoZFciKvvU) |
| Published | 2024/03/07 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Provides an update on Project Rebound, a program at California State University Northridge helping formerly incarcerated individuals reintegrate and pursue higher education.
- Mentions that Project Rebound's giving day is ongoing, aiming to raise funds for the program.
- Shares that last year's fundraising efforts raised $23,000, with the current total exceeding that by $10,000.
- Expresses gratitude for the support received and mentions individuals who have benefitted from the program.
- Shifts focus to the House passing a package containing six spending bills, preventing a government shutdown.
- Notes the package's funding of government operations through September and its upcoming journey to the Senate for approval.
- Talks about the Twitter faction of the Republican Party in the House being upset about the package passing.
- Emphasizes that there are still six more bills to be addressed by March 22nd to avoid a shutdown.
- Acknowledges the uncertainty of completely avoiding a shutdown but expresses optimism.
- Encourages continued support for Project Rebound and suggests that donations can make a significant impact.

### Quotes

1. "It does amazing things for the recidivism rates."
2. "But given the way this year is going, we'll have to wait and see."
3. "Y'all have a good day."

### Oneliner

Beau provides updates on Project Rebound fundraising and the House passing a budget package, navigating through potential government shutdowns with cautious optimism.

### Audience

Community members, supporters.

### On-the-ground actions from transcript

- Donate to Project Rebound to support their initiatives (exemplified).
- Stay informed about the progress of the budget package in the Senate and subsequent bills to prevent a government shutdown (implied).

### Whats missing in summary

Details on specific ways individuals can contribute to the success of Project Rebound and stay engaged with ongoing developments in government funding.

### Tags

#ProjectRebound #GovernmentBudget #Fundraising #Support #CommunityActions


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about the budget,
the package that made it through the house.
So we'll talk about that.
We'll talk about whether or not
we're actually out of the woods yet,
where it goes from here, all of that stuff.
But before we get into that,
we are going to talk briefly
about our own little monetary endeavor today
and provide an update on how that's going.
So we will talk for a second about Project Rebound.
If you missed the earlier videos,
have no idea what I'm talking about,
there is a program out at California State University
Northridge that helps formerly incarcerated people
reintegrate and get through college.
And it provides a support structure and all of that stuff.
It does amazing things for the recidivism rates.
It's just an all-around good program.
And their giving day is today.
And we've been kind of directing people that way.
Now, last year when we did this, it helped them raise 23,000.
Right now, at time of filming, we're at about $10,000 more
than that.
It's going really well.
It is going really well.
And it will go through late evening tomorrow.
I'm not sure of the exact time, but there's still
more time to go.
I truly appreciate it, especially
those who did something to make it send me
a message, which was cool.
I like seeing those come in.
And I am certain that the people there appreciate it.
And something I haven't really mentioned,
But there are people in the comments who have benefited from this program.
That's how I found out about it.
So that's where that's at.
It's going really well.
Okay, so moving on to the news.
The House passed a package that has six different spending bills in it.
And that avoids the partial government shutdown that was set to occur on Friday.
This package is like more than a thousand pages, I have not read the whole thing yet.
But it funds large portions of the government through September.
So from here it goes to the Senate.
Now the Twitter faction of the Republican Party in the House, of course they're upset
about it they're really mad they really wanted that government shut down but
this part made it through does that mean that we're out of the woods no not
really because there's still six more bills that deal with different
departments that generally speaking are the more difficult to negotiate and
And those have to be done by, I want to say March 22nd,
to avoid a shutdown there.
So it's still not a guarantee that we'll avoid a shutdown.
But we're on the way.
There's not a high expectation of a lot
of resistance in the Senate.
But given the way this year is going,
We'll have to wait and see.
But the expectation is that it will get through there.
So that's what's going on.
And again, you can still donate to Project Rebound.
And I am certain that it does a lot of good.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}