---
title: Let's talk about the Public Service Loan Forgiveness....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=TB0DnuD0AfU) |
| Published | 2024/03/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the Public Service Loan Forgiveness Program and its recent developments, with another $5.8 billion impacting 78,000 people.
- Mentions that the program is geared towards teachers, firefighters, and nurses, with average forgiveness around $75,000.
- Notes that the total forgiven debt under this program is now $143 billion for about 4 million individuals.
- Talks about how the original goal was $400 billion before the Supreme Court struck it down, leading to piecemeal forgiveness efforts.
- Addresses the confusion around attributing credit to Biden for the program, clarifying that it was established in 2007.
- Points out that during Biden's administration, around 850,000 people have utilized the program, a significant increase from previous years.

### Quotes

1. "This isn't his program. The stuff that's being used is stuff that has been in existence."
2. "If you are one of the firefighters who is going to benefit from it, yeah, this is huge."
3. "The program is still trying to find something that is a little bit wider in scope for other people."

### Oneliner

Beau explains the Public Service Loan Forgiveness Program, recent developments, and clarifies why credit is attributed to Biden, despite the program existing before his term.

### Audience

Public servants, borrowers

### On-the-ground actions from transcript

- Contact your local representatives to advocate for broader loan forgiveness programs (implied).

### Whats missing in summary

More details on the impact of the loan forgiveness program on various public service professionals.

### Tags

#PublicService #LoanForgiveness #Biden #DebtForgiveness #GovernmentPrograms


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about
the Public Service Loan Forgiveness Program,
Biden, and another $6 billion or so.
And we're gonna answer a question
about the Public Service Loan Forgiveness Program
that has come in that is,
it's worth kind of noting for a second.
Okay, so what happened?
Another little batch of that $4 billion here,
five billion there. This is five point eight billion is the number that I saw
impacting 78,000 people. It's loan forgiveness. This would apply to teachers,
firefighters, nurses. That's who this is geared towards. This bumps his numbers on
on this up to roughly 143 billion and around 4 million people still because
there's not a whole lot of this particular batch. I guess the average
when you average it out I heard it was somewhere around $75,000 so these are
people in public service positions that have a whole bunch of debt and it is
going to be it's going to be forgiven according to this plan. So the original
goal was to get to 400 billion. That was the original plan before the Supreme
Court struck it down. Since then it's piecemeal. The administration's been
going through, finding programs, widening them out, and working towards getting more
and more debt forgiven.
They're at $143 billion so far.
And that brings us to the question that came in.
People are talking about the public service loan forgiveness program like it's Biden,
but it's not Biden, it's something that came about in 2007.
Why is he getting credit?
the question. All of that's right. 2007, I think it officially came into being in
2006 maybe, but it didn't start doing anything till 2007, something like that.
And that's a completely accurate statement. Why is he getting credit?
Because from 2007 to the start of his administration, 7,000 people. 7,000
were able to take advantage of it in all of that time. My understanding, I mean you
can just look at this latest batch, which is 78,000, but my understanding is that
this program is, I want to say 850,000 people during his administration, this
was used. That's why. This isn't his program. The stuff that's being used is
stuff that has been in existence and they're basically applying it and trying
to find ways to make sure that it benefits the maximum amount of people.
That's what's going on. So, this again, if you are one of the firefighters who is
going to benefit from it, yeah, this is huge. This is a big deal, but it is a
narrow category of people. My understanding is that the administration
is still trying to find something that is a little bit wider in scope for
other people that have that. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}