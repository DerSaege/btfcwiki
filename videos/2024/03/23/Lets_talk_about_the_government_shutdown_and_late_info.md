---
title: Let's talk about the government shutdown and late info....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=DdyQZe4NVQ4) |
| Published | 2024/03/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Republican dysfunction in the House led to a delayed government shutdown package.
- Senate political posturing caused the package not to be passed in time.
- A deal has been reached to prevent the effects of the government shutdown.
- A vote is scheduled for Saturday to push through the budget.
- If the vote fails, Sunday could see a filibuster vote.
- This could extend the shutdown into Monday.
- The shutdown is not expected to last long.
- Senators might use the 30 hours given for posturing.
- The eyes of the country are on them to resolve the issue.
- The hope is for a last-minute deal to make the situation irrelevant.

### Quotes

1. "Republican dysfunction in the House led to a delayed government shutdown package."
2. "The eyes of the country are on them, and they have a little bit of trouble when it comes to getting to the point."
3. "Hopefully the literally last-minute deal that the Senate reached will make all of that irrelevant."
4. "It won't be a long-lasting thing."
5. "Anyway, it's just a thought, y'all have a good day."

### Oneliner

Republican dysfunction delays government shutdown package, Senate posturing causes delay in passing, last-minute deal reached to avoid long-lasting shutdown.

### Audience

Legislative observers

### On-the-ground actions from transcript

- Monitor news for updates on the government shutdown (implied).

### Whats missing in summary

The detailed breakdown of potential consequences if the Senate fails to pass the budget and the government shutdown prolongs.

### Tags

#GovernmentShutdown #Senate #RepublicanDysfunction #BudgetVote #LastMinuteDeal


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about the government shutdown.
Where it is, what might happen based on late breaking information,
and what could happen if that doesn't work out.
Okay, so in theory, we're kinda in a government shutdown.
What occurred was that Republican dysfunction in the House
led to the package being delayed, delayed, delayed, delayed, delayed.
By the time it got to the Senate,
there's still political posturing and stuff like that that goes on there.
It didn't get passed in time.
It didn't get passed by midnight.
So the news is that they have a deal
and
that some of the actions that they've taken
are going to stop any effects of the government shutdown from really
showing
and that Saturday morning,
let's just go ahead and call it Saturday afternoon because it is the Senate,
they will vote again
and push through
the budget.
You know, the absolute base thing that
you know they're supposed to do.
They'll take care of that.
So in theory
This is kind of over before it starts.
If that doesn't occur, this is what happens.
Sunday, there would be a vote to break the filibuster on it.
It would take 60 votes.
By my count, they have the votes to break it.
If that occurs, then there is up to 30 hours that allow senators to get up there and uselessly
posture and get sound bites.
They don't have to use all 30 hours, but they could.
That would push the government shutdown into Monday.
You would really start seeing it then.
Most federal employees are off on the weekends.
But the expectation is that even under that scenario, it would be resolved Sunday night
if they don't use all of the 30 hours or Monday.
So this shouldn't be long.
They may use the 30 hours if it goes that route,
because once they're in this position,
they know the eyes of the country are on them,
and they have a little bit of trouble
when it comes to getting to the point,
because they know the cameras are there,
and they know they can get sound bites.
It doesn't matter if it causes you discomfort.
But hopefully the literally last minute deal that the Senate reached will make all of that
irrelevant and there will be a solution that comes out tomorrow morning.
We'll see how it plays out and pick it up from there.
It appears that the shutdown, well, it technically happened, but it won't be a long-lasting thing.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}