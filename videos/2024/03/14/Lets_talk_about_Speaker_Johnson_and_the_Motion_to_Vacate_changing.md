---
title: Let's talk about Speaker Johnson and the Motion to Vacate changing....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=hwM8YeL7b4U) |
| Published | 2024/03/14 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Speaker Johnson's remarks on the motion to vacate in the U.S. House of Representatives shed light on internal Republican dynamics.
- The motion to vacate allows for the removal of the Speaker of the House, with recent changes making it easier for one member to trigger a floor vote.
- Johnson acknowledged the frequent discourse on the motion to vacate among members, hinting at potential future changes.
- Despite mentioning the topic, Johnson clarified he has not actively pushed for any alterations, indicating his current stance.
- His public statements suggest a lack of concern towards the Twitter faction within the Republican Party, contrasting with McCarthy's approach.
- Johnson aims to diminish the influence of the obstructionist Twitter faction and restore norms for legislative processes.
- By expressing a desire for more collaborative and thoughtful debates across party lines, Johnson signals a shift away from obstructionist tactics.
- Johnson's strategy involves gradually neutralizing the Twitter faction's power and asserting control over the House's operations.
- His actions and statements are perceived as a victory against the hard-right faction, hinting at a secure position in potential floor votes.
- Johnson's focus on leading rather than accommodating obstructionists underscores his approach to governance within the House.

### Quotes

1. "Johnson is slowly but surely bringing the Twitter faction to heel."
2. "This is about as close to a victory speech as you're going to get."
3. "I've got my votes to stay."
4. "It was a message."
5. "Y'all have a good day."

### Oneliner

Speaker Johnson's strategic handling of the motion to vacate in the U.S. House of Representatives reveals a shift towards leadership over obstruction, challenging the influence of the Twitter faction within the Republican Party.

### Audience

Political observers, House representatives

### On-the-ground actions from transcript

- Contact your House representative to express your views on leadership and collaboration within the legislative process (suggested)
- Stay informed about internal dynamics within political parties to better understand decision-making processes (implied)

### Whats missing in summary

Insights on the potential impact of Johnson's approach on future legislative decisions and the dynamics within the Republican Party.

### Tags

#USPolitics #HouseOfRepresentatives #RepublicanParty #Leadership #LegislativeProcess


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Speaker Johnson
over in the U.S. House of Representatives,
the motion to vacate, and how something that he said
can give us some insight into something that is happening
within the Republican Party.
Um, now, if you don't remember what's going on,
the motion to vacate is the method
by which the Speaker of the House is removed from their position.
It was changed shortly before McCarthy was shown the door to where one member can force
a floor vote to house the Speaker.
Johnson said, the motion to vacate is something that comes up a lot amongst members in discussion,
And I expect there will probably be a change to that as well.
But just so you know, I've never advocated for that.
I'm not one who's making it an issue because I don't think it is one for now.
So changing that back, if they change it back, then it would take more members to do it.
The thing is, him openly saying this, what does that mean about the Twitter faction?
because it was that faction of the Republican Party that made McCarthy change it to the
one-person threshold.
It certainly appears because this statement is public, it certainly appears that Johnson
isn't that concerned about them.
Now McCarthy and Johnson handled that faction very differently.
Johnson has done everything he can to reduce their influence.
It also goes on to say, I think that there's a desire on the behalf of the vast majority
of the members of the House to reestablish some of the norms with regard to rules on
the floor and how legislation is handled.
Many of us, you know, do long for thoughtful and deliberative conversation even across
the aisle.
That statement is basically telling the Twitter faction, the hard right faction that is very
obstructionist, that he's won.
These are public statements.
Keep in mind, under the current rules, they can still bring up a motion to vacate.
He's not talking about changing them until the next Congress.
If they bring up a motion to vacate, it certainly appears that he seems very secure in the fact
that he will prevail on the floor.
That again reduces their influence.
Johnson is slowly but surely bringing the Twitter faction to heel.
The speaker's job is to actually lead in the House, not to run everybody's errands.
And Johnson has moved repeatedly to take that faction that is very obstructionist and basically
bring them in line.
This is about as close to a victory speech as you're going to get.
This was public.
This is information that is out.
It is everywhere.
And he's not only talking about taking away their leverage, he's talking about reaching
across the aisle.
Now do I really believe that he longs for deliberative conversation with Democrats?
No, of course not.
But saying that would certainly get under the Twitter faction's skin.
Please keep in mind as we talk about this, Johnson is not somebody that the overwhelming
majority of people watching this channel agree with in, I mean, any way.
But his statement that they plan on removing that leverage and that they want to work across
the aisle is, I can't see it in any other way than putting the Twitter faction on notice.
I've got my votes to stay.
That might alter things moving forward, especially with the harder part of the budget negotiations
coming up.
And remember, they're on a clock for that.
So this statement may have needed to be made to just illustrate that point so they can
get somewhere with the budget negotiations, because keep in mind, the next Congress is
it's a while from now.
There was no reason to tip your hand early if you didn't need to.
It was a message.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}