---
title: The Roads to a March Q&A
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=IIkK8WpOOB0) |
| Published | 2024/03/14 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses questions about the House passing a bill potentially banning TikTok and its impact on younger voters.
- Explains the concept of Gonzo journalism and its defining characteristics, using a personal story involving a bird and police brutality.
- Responds to inquiries about the potential post-war profiteering by construction companies and the sourcing of statistics in his content.
- Shares thoughts on waiting for further information before forming opinions on certain cases, such as the Benedict, Oklahoma incident.
- Provides insights into the relief of two US submarine commanders in 2024 and dismisses notions of a Navy effort to remove Trump loyalists.
- Expresses intention to revisit the topic of Gonzo journalism due to its relevance and increasing prevalence.

### Quotes

- "It's about drawing out a deeper social commentary, a deeper social critique."
- "The reason you didn't get scared is because you're not that animal's prey."
- "Having the right information will make all the difference."

### Oneliner

Beau answers questions on TikTok bans, explains Gonzo journalism, and shares insights on post-war profiteering and naval command changes.

### Audience

Content Creators, Aspiring Journalists

### On-the-ground actions from transcript

- Research and understand Gonzo journalism (suggested)
- Stay informed about developing stories like the Benedict, Oklahoma incident (implied)

### Whats missing in summary

In-depth examples and further elaboration on the topics discussed by Beau in this Q&A session.

### Tags

#TikTok #GonzoJournalism #SocialCommentary #StatisticalAnalysis #InformationSourcing


## Transcript
Well, howdy there, internet people, it's Beau again,
and welcome to the Roads with Beau.
Today is March 14th, 2024,
and we're doing a Q&A for you this Thursday morning.
And these are your questions.
So we will run through them and let's get started.
Okay, so first time writer,
long-time listener. I'm curious what your thoughts are on the House recently passing a bill that could
ban TikTok and how that could play out in the general election with younger voters.
I could see this being spun as a Biden-TikTok ban.
I think as more reporting comes out about this, I don't think that this is going to
to be the thing that alienates younger voters, because really, this bill isn't about banning
TikTok.
To my way of thinking, that's not what this is about.
It's about getting TikTok sold to an American.
That's what it seems like to me.
That's what it seems like the real purpose of this is.
So there's that, and we don't know how it's going to do in the Senate.
I haven't seen an accurate vote count, but I think deep down, I think that this is not
going to be a deciding factor for a lot of young people.
I think there are other things that are going to weigh far more heavily on the vote.
I was talking to my friend about what I thought were Biden shirts that you wear.
He said they weren't Biden, but Hunter S. Thompson.
I assumed because of the aviator glasses, they were Biden.
It started a conversation about gonzo journalism and he said it's because you do a lot of
gonzo reporting.
I've done some reading and don't understand exactly what it is or why it isn't just fiction
or maybe based on a true story.
He said, one of your videos about a bird teaching you about police brutality was probably gonzo
but couldn't find it to show me.
A bird teaching me about police brutality?
Um, oh, the hawk.
The hawk.
I'll put it down below, oddly enough, no, that is exactly how that happened.
I will put that down below, but that is definitely what, that is what Gonzo sounds like.
Okay, so, I wish I got this question for a whole video.
What is Gonzo journalism?
Okay, so, most journalism is about objective fact, who, what, when, where.
Maybe, why, but they're going to use an official source of some kind for that, and how, in there as well.
Gonzo journalism is not really concerned with the objective facts, it's not about that.
It's about drawing out a deeper social commentary, a deeper social critique.
So, there is poetic license that is taken.
Hunter S. Thompson is like the grandfather of this kind of journalism, and way over the
top.
Let's see.
Okay, so what are some defining characteristics?
Generally in the first person, and so there's self-inserting.
I would imagine that a lot of the wild parties that are depicted in Hunter S. Thompson's
stuff he may not have been physically present for. He got the interviews, he got a little
bit of the experience when he was there for a few minutes, but didn't see everything,
and then put it all together, told in the first person to create a... to create kind
of a tighter narrative, a better storyline, and Gonzo is told via story rather than reporting.
to make it more consumable for the reader and to pull out that social commentary, that social critique.
So, I mean, based on a true story, that's, I mean, that's not wrong.
There's a lot of self-satire, as well. I'm fairly certain that Hunter S. Thompson
and didn't have a Z carved into his forehead, that probably didn't occur, but it was the
over-the-top nature of it.
There's a lot of self-satire, self-inserting, there's a lot of exaggeration and embellishment.
And that is, that's all part of gonzo journalism, and again, the goal isn't who, what, when,
where.
It's the social critique, the social commentary.
That's why the story about the bird, that's why he thinks that's gonzo.
Okay, so I'll put it down below, but a short version of it.
Two of my friends, they go up to Alabama, and one of them's white, one of them's black.
The black guy is driving, and they get pulled over.
He gets nervous, and he, you know, it's yes sir, no sir, the whole thing.
Later on, the white guy, he's kind of making fun of him for it.
And at the time, I just, I did not have it in me to have one of those conversations.
But I don't know, a few days later, maybe a month later, I don't remember, I was walking
in the pasture, and I'm standing there by Marilyn, the horse.
And there's squirrels, bunnies running around, I'm just sitting there watching it, and a
hawk flies over.
The horse and I, we're not scared.
The smaller animals run off.
And the point, well you'll get the point in the video, but if you were to gonzo that
story up, I would be in the car, even though I wasn't, okay?
I'd be in the car, I would witness the whole thing.
everybody's actions would be exaggerated. Like, this would be the most chill cop in the world,
okay? Start to finish. And he would be way more nervous than he really was. And the passenger,
my white friend, he would be completely oblivious to an even greater degree than he was. Then when
When we got to the destination, we'd all get out in the car, and me and the white guy,
we would walk through the pasture together, and that's when the hawk flies over.
There's this moment where he sees it, and the social commentary and critique comes out.
The reason you didn't get scared is because you're not that animal's prey.
That's what gonzo is.
That's how it works.
There's not as much importance on the objective facts and the chronology.
It's the lesson, for lack of a better word, that can be pulled out of it.
And that's what gonzo is, but no, that's actually how that occurred.
So I mean, based on a true story, that's not a bad way to look at it, but it's based
on a true story with a lot of self-satire, a lot of exaggeration that takes it over the
The top and the end point isn't about the events that occurred.
It's about what can be pulled out of it.
I hope that makes sense.
We'll probably come back to this because there are a lot of people who are doing this today
and they may not even realize they're doing it themselves.
So we'll probably come back to this topic.
Do you think the peer will be used to get construction companies in to make a buck after
the war?
It's the United States.
Yeah, there are going to be companies that get in to profit off of that.
Absolutely.
I would be absolutely shocked if that didn't occur.
or not they use the peer, they probably will. They probably will. The large companies that
are going to be needed for reconstruction, yeah, that seems incredibly likely. I was
just curious about where it is you get your statistics when you quote surveys.
I do a number of surveys as a side gig and some of the information you give
sounds like the questionnaires that I have taken part in. As a side gig? Oh wow,
you just gave me a rabbit hole to go down. Okay, so when I'm talking about
surveys and polling and stuff like that. What I normally do is when polling comes
out there's an article, normally a series of articles from different outlets that
write about that polling, most times you can go to it and it will tell you who
the pollster was. Sometimes they'll even be nice enough to have a link directly
to it. You can follow the link or you go to the pollsters website and you can
find their methodology, and you can find all of the questions
that they ask.
Oftentimes, the question that is in the article,
the percentage that they're reporting on or whatever,
it may not be the most important piece.
And that's where a lot of that comes from.
But now, I have to look into whether or not
pollsters are using a service to collect surveys for them
in which people are paid as part of a side gig.
Because that would certainly influence the polling data.
I never even considered that as a possibility.
But most times, you can track it back and get
the all of the information.
and they generally ask way more questions
than what gets reported on.
And there are a lot of little nuggets in there
that are beneficial for understanding the bigger picture.
Do you plan on waiting for confirmation
of next Benedict's ME report from an independent source?
I'm one of those who gave you a, let's just say gave you a hard time for defending the
journalist that waited.
Sorry.
No reason to be sorry.
Yes, I absolutely plan on waiting for more information now.
So if you're not familiar with this case that happened in Oklahoma, there's a lot of reporting
on it.
you can use your favorite search engine and find it,
I would want to know a little bit more than just the MEs
report now, because there's a lot of information that I would
not be comfortable just taking that right off the bat.
It's one of those things where it's a complex situation,
and it's not something I don't want to be wrong about it.
I do not want to be wrong about it.
But I would suggest everybody Google Benedict, Oklahoma,
and take a look at the story because it is developing, but I personally believe it is
still developing.
I don't think that that story is over yet at all.
I have recently read news reports of two US submarine commanders being relieved of command
so far in 2024. Both were captains of one of the two rotating crews of two different Ohio-class
boats. These are Alder Ballistic Missile submarines that have been converted to land attack operations
with their ICBM load replaced by long-range cruise missiles. One was the US Ohio, USS Ohio,
the other was the USS Georgia. In both cases, the captains were relieved because their bosses,
quote, lost confidence in their ability to command. Forgive my paranoia, but since Trump
apparently intends to invoke the Insurrection Act if elected and involve the military in domestic
peacekeeping. Do you think it's possible the Navy is trying to remove Trump loyalists?"
Okay, so I did a video on...
I think it was the Georgia. I think it was the Georgia. You know, the fact that
it's Ohio class and Ohio is one of the subs, it gets a little confusing, but I
believe it was the Georgia. The captain, from what I remember, got a DUI, got
pulled over DUI, something like that. I'll find it and put it down below. Lost
confidence and their ability to command is boilerplate. What that means from
the Navy is something happened and it it caused us to question and we're not
going into detail. The reason I did a video on it is there was a question
about it at the time as to why it happened. I think it was the Military
Times that actually found out what occurred but no I don't believe there is
an active effort to get rid of Trump loyalists.
OK, so I guess that's it.
That looks like all the questions.
Yeah, I would imagine we're going
to come back to the topic of Gonzo,
because I think that's something that people need to be
able to recognize more readily, because it's
occurring more and more.
And I think sometimes the people doing it
don't even realize what they're doing.
Anyway, a little more information, a little more context, and having the right information
will make all the difference.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}