---
title: Let's talk about RNC changes and updates....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=0pCnZTfYnO0) |
| Published | 2024/03/14 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the news from the RNC, focusing on Trump's leadership team and their actions.
- Trump handpicked the top people at the RNC responsible for coordinating election strategies.
- The new leadership team is making deep cuts to staff and appears to be solely focused on getting Trump re-elected.
- Reports suggest the Republican Party may be cutting minority outreach programs.
- Programs like bank your vote, the Republican Party's mail-in voting drive, are being scrapped, despite previous support from the RNC's former head.
- They plan to replace bank your vote with Grow Your Vote, aiming to reach unlikely voters who lean towards Trump.
- The focus will shift towards legal challenges and claims about the election, mirroring the strategies of 2020.
- Despite the previous strategy not working, the Republicans seem set on duplicating it.
- Beau suggests the Democratic Party should prepare to defend voting rights in response to the upcoming challenges.

### Quotes

1. "The new leadership team is going to focus solely on getting Trump back in the White House and nothing else."
2. "They plan to duplicate what didn't work last time."
3. "The same rhetoric, everything."
4. "Y'all have a good day."

### Oneliner

Beau explains the RNC's focus on Trump's re-election, cutting programs, and planning legal challenges, mirroring past failed strategies.

### Audience

Voters, Democrats.

### On-the-ground actions from transcript

- Prepare to defend voting rights against legal challenges (implied).
- Stay informed and engaged with election developments (implied).

### Whats missing in summary

The detailed analysis and context provided by Beau in the full video. 

### Tags

#RNC #Trump #ElectionStrategy #VotingRights #DemocraticParty


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about the news
coming out of the RNC.
So we will go over the reporting,
talk about what that reporting means,
where it likely goes from here,
and just kind of run through everything
now that Trump's leadership team is firmly in place.
Okay, if you have missed this story thus far, Trump handpicked the top people at the RNC.
The RNC is the Republican National Committee.
They are responsible for kind of coordinating everything and developing a national level
strategy for the elections.
Just as soon as his leadership team came in, there were deep cuts to staff.
The general belief is that the new leadership team is going to focus solely on getting Trump
back in the White House and nothing else.
Generally speaking, they have a much wider job, but it seems to be narrowed.
Okay, so what does the new reporting say?
It starts with the idea that the Republican Party may be cutting minority outreach.
That's the term being used.
The Republican Party had a number of programs that were designed to go after different demographics.
Apparently those are going away.
That's what the reporting says.
The other thing that is going away is something called bank your vote.
According to the reporting, that is also being scrapped.
What is that?
That is the Republican Party's mail-in voting drive.
The previous head over there at the RNC, McDaniel, was a big supporter of this.
Trump doesn't like it for whatever reason.
Which is kind of odd, to be honest, because when you look at the research on it, it kind
of indicates that there's no partisan divide as to who Mellon voting really helps.
And the few that show that there is an advantage, it's a slight advantage to Republicans.
But it was part of Trump's rhetoric, so they're apparently getting rid of that.
That will be replaced or maybe supplemented, complemented by something called Grow Your
Vote, which is their attempt to reach people who lean towards Trump's ideas but may not
show up to vote for him.
They're trying to recruit unlikely voters.
There is also reporting that says that they will prioritize legal challenges and that
they're going to put a lot of energy into claims about the election.
So what does that mean?
What can you expect?
You can expect a repeat of 2020.
The same rhetoric, everything.
the same rhetoric, the same challenges, the same statements, all of the stuff that we've
heard before.
Which seems odd to me because it didn't work last time.
Maybe they're hoping that this time will be different, more people will be more believing
of them, I guess.
But based on these moves, it looks like their plan is to duplicate what didn't work last
time.
At the same time, the claims about the elections, we saw what they did.
So while it seems like it wouldn't be productive to go this route, it's something that perhaps
the Democratic Party should start preparing for and getting their lawyers together to
be ready to defend voting rights.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}