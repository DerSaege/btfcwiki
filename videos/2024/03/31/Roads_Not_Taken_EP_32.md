---
title: Roads Not Taken EP 32
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=9kOPgvAsJe0) |
| Published | 2024/03/31 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Provides an overview of the topics discussed in episode 32 of "The Roads Not Taken" on March 31st, 2024.
- Addresses foreign policy news including Russia's veto at the UN and China's economic talks with U.S.
- Talks about the situation in Ukraine, hinting at the lack of aid and potential military actions.
- Mentions a second aid shipment by boat to Gaza, the Palestinian Authority's new cabinet, and the presence of famine in northern Gaza.
- Shares U.S. news such as a judge recommending John Eastman lose his law license, controversial statements by Republican Tim Walberg, and legal issues within the GOP.
- Notes cultural news like the rise of Truth Social stock and the overlapping of Easter and Trans Day of Visibility.
- Touches on science news, oddities like a man changing his name to "literally anybody else," and then answers viewer questions.
- Responds to questions about a controversial Green Beret patch, explaining its history and symbolism.
- Addresses the possibility of an "Omar moment" in Gaza and the urgency surrounding the current situation for peace efforts.

### Quotes

1. "Influence and aid tend to go hand in hand."
2. "Satire is dead."
3. "It's not an image that should be on a U.S. uniform."
4. "I do believe there's a chance."
5. "So I don't know how you could tell people to chill out when it comes to this."

### Oneliner

Beau provides insights on foreign policy, Ukraine, aid to Gaza, U.S. news, cultural updates, scientific developments, oddities, and answers viewer questions about controversial symbols and peace prospects in Gaza.

### Audience

News enthusiasts, activists.

### On-the-ground actions from transcript

- Support aid efforts to regions in need (suggested).
- Stay informed about global policies and events (implied).
- Advocate for peace and humanitarian initiatives (implied).

### Whats missing in summary

Detailed explanations and additional context on the topics discussed in the transcript.

### Tags

#ForeignPolicy #Ukraine #Aid #USNews #CulturalNews #PeaceEfforts #Symbols #CurrentEvents #HumanitarianActions #Activism


## Transcript
Well, howdy there, internet people, it's Bo again,
and welcome to The Roads with Bo.
Today is March 31st, 2024,
and this is episode 32 of The Roads Not Taken,
a weekly series where we go through
the previous week's events and talk about news
that is unreported, under-reported,
didn't get the attention I thought it deserved,
or simply lacked context, or I just find it interesting.
Um, and then at the end,
we go through some questions from y'all,
picked by the team.
OK, by the way, happy Easter to those who celebrate.
I know people are waiting for something.
Don't worry, that's coming.
OK, foreign policy news, starting off there.
Russia used its veto to protect North Korea at the UN.
The resolution that was vetoed, it
would have investigated suspected sanction violations.
China's leader met with U.S. business leaders to talk about the economy.
It certainly seemed like the goal was to present the Chinese economy as something to invest
in.
And as tensions slowly fall, there's probably going to be more of that.
News from Ukraine.
Ukraine is saying they may have to retreat from certain areas without aid.
that aid is currently being held up by the GOP.
Ukraine is also getting ready to push out its higher end artillery and put that back
into action because they have some ammo on the way.
The US, while denying aid, is asking Ukraine to not hit inside Russia, particularly when
it comes to oil infrastructure.
requests are probably going to be ignored because the U.S. lost its influence when it
stopped providing aid.
Influence and aid tend to go hand in hand.
A second shipment by boat from World Central Kitchen and Open Arms is headed to Gaza from
Cyprus.
That second shipment is underway.
I want to say it has 300 tons on it, has food and aid.
The Palestinian Authority has announced a new cabinet.
A number of them are from Gaza, so it does appear that they're trying to do that.
The reception from Palestinians was lukewarm, but to be honest, anything short of outright
That hostility is kind of a win.
The Palestinian Authority does not have a lot of warm fuzzy feelings from most Palestinians.
That new cabinet, they have their work cut out for them because they have to find a way
to garner support and they have to demonstrate that the new boss is not the same as the old
boss.
They have a lot of work to do.
The U.S. says that famine is likely present in northern Gaza now.
It is.
It is present.
Moving on to U.S. news.
A judge has recommended that pro-Trump attorney John Eastman lose his law license.
There are, I think there's still one more step.
still one more step in that process. Republican Tim Walberg appeared to suggest using nuclear
weapons in Gaza. USNS Harvey Milk completed its maiden voyage. A judge has found a ranking
member of the Georgia GOP who spread a bunch of claims about the 2020 elections, the judge
found that he voted illegally.
Satire is dead.
Cranes are working to clear the debris, open up the harbor where the bridge fell.
supporters in Wisconsin appear to be gearing up for another attempt to try to
recall the Republican assembly speaker up there who happens to be one of the
most powerful Republicans in the state. We'll see how that plays out. A Texas
appeals court has ruled against the state and their desire to launch probes
into parents who are allowing their children to get gender-affirming care.
So the appeals court ruled against the state on that. Moving on to cultural news,
truth social stock is riding a bubble that is defying all expectations. When
When that happens, it is going to be a moment.
That's going to be a moment.
Republicans are super mad today because today is both Easter and the Trans Day of Visibility.
They appear to be angry because they are unsure of how calendars work.
For those who don't know, March 31st is always Trans Day of Visibility.
Easter is the first Sunday after the first full moon that follows the spring equinox.
Sometimes that is also on March 31st.
In science news, Puerto Rico has declared a health emergency over dengue cases.
A report says that more than half the water from the Colorado River is being used for
the agricultural industry. And highly pathogenic avian flu, bird flu, is appearing in unpasteurized
samples from cows in the U.S. We will definitely be following that. In oddities, a Texas man
changed his name to, quote, literally anybody else. This is a legal name. And is
running for president. Vote for literally anybody else. Okay, moving on to the
questions. Only looks like a few here. How do you know or determine when we'll see
this material again, it's knowing what the building blocks are.
This is something that, it used to be common, this stuff used to be covered, and it's not
journalists, it's news outlets.
They don't provide the context, they don't provide the lead up to events the way that
I think they should.
If you see the building blocks being put into place, things are less shocking and surprising
when they happen.
The thing is, the current news industry is based off the shocking and surprising.
So they don't do it.
Give you an example.
We just talked about the Palestinian Authority and the new cabinet.
The plan got floated to use a quote revitalized Palestinian authority to administer both the
West Bank and Gaza.
That was put out there and we mentioned it because it was floated in a couple of different
ways which certainly seemed to indicate they were testing the waters.
Once the old cabinet was kind of shown the door, headed for the door, however that played
out. At that moment, it was very clear that they were going to try to enact that because
they talked about it, they floated the idea, it got an okay reception, and then once the
old cabinet members resigned, we knew that they were going to try to bring in a new cabinet,
that would hopefully be acceptable to all Palestinians.
And that's what they're trying to do.
So from the second the old cabinet resigned, you were going to see that material again.
It's just knowing what the building blocks are and talking about them as they occur rather
than just waiting for something that's going to be a good headline.
There are two questions about this, and you said that this was something you'd probably
make a video about.
That's obviously from the team.
I talked to my dad on the phone and told him about that Green Beret patch that's all over
Insta.
He was a Green Beret and said it was BS, and that skulls have always been a part of Green
Beret symbols.
WTF is my dad overlooking fasc symbols.
Okay so a little bit of background on this.
I believe it was 20th SF put a photo on Instagram.
One of the people in that photo was wearing a patch.
patch had a skull on it. That skull is closely associated with things you would
rather not see. The Germans from World War II and like the the bad ones.
Obviously when people saw that skull it prompted a lot of backlash and looking
at the other question, we'll get to the resolution there. But it's not a symbol you would expect
to see on a US uniform. It's not appropriate. Okay. So as far as your dad goes, no, he's
right. He's right. See what you're saying. I talked to my dad on the phone and told him
about that Green Beret patch that's all over Insta. He was a Green Beret and said
it was BS and that skulls have always been a part of Green Beret symbols.
That's 100% true. Just not that skull. I would strongly suggest sending him the
image. Text it to him. I'm willing to bet his attitude is going to change because
Because if I had to guess, type in, mess with the best, die like the rest, green beret skull.
That's the skull he's thinking of, which sometimes is even paired with palms.
That is not the skull that was on that patch.
It's again, it's inappropriate.
So my guess is that he is picturing the skull images that have been a part of Green Beret
symbols going back at least to the beginning.
Very popular from the early days of Vietnam on, but it's not the same skull.
So I would make sure that he knows which skull it is.
The next question.
Is there any way to give the 20th group guy the benefit of the doubt on that patch?
Every SF guy I've ever met was pretty cool.
Depends on what you mean by benefit of the doubt.
didn't know at some point in time the racist connotations that go with that
skull. Maybe. Maybe. But this... there's a little bit of a backstory here that
that should probably be told. Right now 20th Group is catching all of the static
for this because they posted the photo. The thing is, from my understanding, that
That is not a 20th group logo.
That was a logo that was used as kind of like an unofficial logo by a subgroup of third
group.
And third group, the command over there, found out about it years ago and banned it.
So the chain of events required to give him the benefit of the doubt as far as him wearing
something that he knew he shouldn't, it would take him leaving in like 2021, completely
separating and going back into the National Guard, ending up in 20th group.
It would be a chain of events that would be really specific.
Even with that, his command, even if they give him a pass on, okay, you didn't know
what this goal was and you weren't intentionally trying to be racist with it.
Even if they give him that, there's still the judgment issue when it comes to wearing
something that was banned by another command and then you have to think about the fact
that it's SF and their job, they should be able to ingratiate themselves into other cultures.
I feel like his command is going to be like, yeah, you should have known the subtext of
this.
I get what you're saying.
Every SF guy I've ever met was pretty cool, meaning you don't see them as the type of
people who would wear those kinds of symbols.
For whatever reason, that particular skull is starting to show up in a lot of places.
I don't know that everybody knows what it means, and I don't know that they're viewing
it the way that they should.
It's not an image that should be on a U.S. uniform.
So I do not feel like his command is going to give him the benefit of the doubt, unless
it is a very specific thing where he left before it was banned, so he didn't know,
and then maybe it was on an old piece of equipment that he was using, you know, that he had in
his kit after he came back to the National Guard.
Like, the chain of events required is, it's a reach.
I don't think that this guy is, I feel like he's going to get in trouble.
The other thing to be aware of, since this has come up, and even though currently it's
still in a pretty small group of people talking about it, eventually somebody's going to pick
up on this and it'll become a wider news story.
If you're in armor, I would imagine those images of Rommel and the burning tanks and
all of that stuff, they're probably going to have to go to.
Because of this, I feel like this is going to become a blanket thing, DOD-wide.
For those who don't know, American tankers often had, I don't know if they still do this,
but there used to be images of Rommel, because they went up against him, in a tank on fire,
but it has the same images.
are probably going to be, if they're still up, I imagine the command is going to say
take them down.
So okay, do you believe there could be, do you believe that there could be an Omar moment
with the current situation in Gaza?
If you do, why don't you tell everybody to chill out?
Okay, an Omar moment is a period, it's a moment in cyclical violence where things get
so bad that peace becomes a real possibility.
Do I believe that that's the case here?
Yes.
I do believe that that window is going to open.
And if not, imagine how bad it would have to be.
If this doesn't create one, I don't know what will.
So I do believe that that window is going to open.
The thing is, that window's not open long.
So if you do believe that, why don't you tell everybody to chill out?
Number one, because people are dying.
Number two, all of the pressure that is building, like across the board, when that window opens,
hopefully that pressure is going to push through it.
As bad as it is, and as bad as it is likely to get, I do believe there's a chance.
But if that chance occurs, the window of time in which to get something done, it's short.
So all of the pressure, it's creating that urgency.
So when that window opens, hopefully our betters will see fit to push through it.
And anybody who is reluctant to go through it, they get drug along with it even if they
don't want to.
Because I do, I think this is probably the best chance in a generation.
So I don't know how you could tell people to chill out when it comes to this.
So those are the questions, and that's it.
So there's a little more information, a little more context, and having the right information
will make all the difference.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}