---
title: Let's talk about Trump's order in New York....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=88A9qqXMUKE) |
| Published | 2024/03/31 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recent order in New York against Trump prompts letters to judge.
- Gag order issued in the Hush Money case against former President.
- Former President quickly makes comments about judge's daughter.
- Prosecutors send letter to judge seeking clarification on order's scope.
- Trump accused judge's daughter of social media posts she didn't make.
- Trump's team likely to argue for his right to free speech.
- Proceedings could lead to hearings, arguments, and appeals.
- Concerns about litigation delaying jury selection in two weeks.
- Prosecution eager to move forward with the case.
- Possibility of prosecution seeking to expand or clarify concerns.

### Quotes

1. "Recent order in New York against Trump prompts letters to judge."
2. "Trump accused judge's daughter of social media posts she didn't make."
3. "Prosecutors seek clarification on order's scope."
4. "Concerns about litigation delaying jury selection in two weeks."
5. "Prosecution eager to move forward with the case."

### Oneliner

Recent order against Trump in New York prompts letters to judge, sparking concerns about delays in litigation as prosecution seeks to move forward with the case.

### Audience

Legal observers, concerned citizens

### On-the-ground actions from transcript

- Contact legal experts for insights on the potential implications of Trump's behavior towards the judge and her family (suggested)
- Stay informed about the developments in the case and be prepared to support the rule of law (implied)

### Whats missing in summary

Full details and nuances of the legal proceedings and potential impacts of Trump's actions can be better understood by watching the full transcript.

### Tags

#Trump #NewYork #LegalProceedings #Prosecution #FreeSpeech


## Transcript
Well, howdy there internet people, let's go again.
So today we are going to talk about Trump in New York
and the recent order that was put in
and how it is already having letters written
about it to the judge.
That didn't take long.
Okay, so if you remember, recently the judge in the New York case that is being referred
to as the Hush Money case put in a gag order against the former president, basically telling
him he couldn't talk about X, Y, and Z, so on and so forth.
And it didn't take long at all before the former president was making comments about
the judge's daughter.
On Friday, prosecutors sent a letter to the judge basically asking the judge to clarify
or confirm the scope of that order and what it really means and to instruct Trump to immediately
desist from attacks on family members.
Some of what has been said, according to the reporting, Trump reportedly wrongly accused
the judge's daughter of making certain social media posts.
According to the reporting, that's not even her account.
So this is probably something that is going to have to be addressed.
The real issue here that is going to come up is that Trump's team is not just going
to say, yeah, it's okay.
View it through this lens that the prosecution wants you to view it through.
They're going to argue that the order allowed or continues to allow the speech that he's
engaged in, and they're going to say that it's core political speech.
This is something that, if pursued, would end up being hearings.
They would talk about it.
They would have to argue over it before the judge made a decision.
And of course, that decision might be appealed.
All of this is normal when you're talking about Trump because that's how things play
out, right?
The thing is, and the real concern that a lot of people have, is that this case, they're
picking the jury in, what, a little more than two weeks?
So any litigation that goes on regarding this, it stands a chance of delaying these proceedings.
And I don't think that the prosecution really wants that.
I think they want to go ahead and get this underway.
So we'll have to wait and see next week how this plays out, but I feel like there's a
possibility that the prosecution lodges their concerns, asks for it to be expanded.
it or clarified but I'm not sure how far they're gonna push it because I don't
think they want to push that date back anyway it's just a thought y'all have a
a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}