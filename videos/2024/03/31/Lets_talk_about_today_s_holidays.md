---
title: Let's talk about today's holidays....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=fTwb0yTuy1E) |
| Published | 2024/03/31 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the significance of March 31st being Trans Day of Visibility, Cesar Chavez Day, Bunsenburner Day, National Tater Day, Eiffel Tower Day, Crayon Day, National Farm Workers Day, Anesthesia Tech Day, and World Backup Day.
- Mentions the confusion caused when Easter coincides with Trans Day of Visibility.
- Criticizes political figures or religious leaders who try to capitalize on the coincidence for division and hatred.
- Suggests that questioning the intent of those who exploit such coincidences is more vital than getting upset about calendar overlap.
- Wishes everyone a happy Easter, Trans Day of Visibility, and other holidays, ending with a positive message.

### Quotes

1. "Happy Easter, happy Trans Day of Visibility. All those other ones to."
2. "If your favored politician or your religious leader, in an effort to capitalize on division and hatred, chose to tell you to be angry about these two days coinciding, I think you have more important questions to ask yourself than figuring out how calendars work."
3. "Y'all have a good day."

### Oneliner

Beau explains the significance of various holidays on March 31st, criticizes those who exploit coinciding celebrations for division, and encourages questioning intent over getting upset about calendar overlap.

### Audience

Social media users

### On-the-ground actions from transcript

- Wish others a happy Easter and Trans Day of Visibility (exemplified)
- Spread positivity and understanding about different holidays on the same day (exemplified)

### Whats missing in summary

The full transcript provides a deeper reflection on the importance of questioning intent and spreading positivity amidst holiday overlaps.

### Tags

#Holidays #TransDayofVisibility #Easter #Unity #QuestionIntent #SpreadPositivity


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about holidays
and confusion and calendars.
And we're going to run through some things
and just kind of talk about them briefly
because it is a topic that came up really yesterday,
but it's becoming a wider issue.
So we're just gonna kind of go through it
and address a few things.
OK, so today is March 31st.
It is Trans Day of Visibility.
It is also Cesar Chavez Day.
That's not what you thought I was going to say, is it?
It's also Bunsenburner Day.
It's also National Tater Day, Eiffel Tower Day, Crayon Day,
National Farm Workers Day, Anesthesia Tech Day.
And let me tell you something, after my surgery,
I feel like celebrating that because y'all are great, and it is World Backup Day.
All of these things and more occur on March 31st.
Easter occurs on the first Sunday after the first full moon after the spring equinox.
Sometimes that is also March 31st.
You have political figures asking for an apology because Trans Day of Visibility and Easter
fell on the same day.
Trans Day of Visibility is always March 31st.
Always.
Easter isn't. But here's the thing, if your favored politician or your religious
leader, in an effort to capitalize on division and hatred, chose to tell you to
be angry about these two days coinciding. I think you have more important
questions to ask yourself than figuring out how calendars work. I feel like
preaching hate on Easter is probably not, you know, in line with everything. If in
an attempt to rally people. That's what they did. Like I said, if it was me, I
would have questions. I would have questions about what their intent was
behind that. So happy Easter, happy Trans Day of Visibility. All those other ones
to. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}