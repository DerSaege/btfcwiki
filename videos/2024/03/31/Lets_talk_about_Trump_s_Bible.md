---
title: Let's talk about Trump's Bible....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=_XAronoUCMc) |
| Published | 2024/03/31 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump is promoting a Bible, sparking various reactions and debates, particularly about its $59 price tag and where the money is going.
- Backlash from religious scholars includes concerns about nationalism, additions to the Bible, and Trump's appropriateness.
- Some view the Bible as just another product with Trump's name on it, akin to shoes or NFTs.
- The Bible includes the Constitution, prompting reflection on the importance of its ideas and the separation of religion and government.
- Beau questions the blending of religion and nationalism, citing historical examples where this mix led to horrific outcomes.
- He recalls a common saying about fascism in America being wrapped in a flag and carrying a cross, alluding to the symbolism of the Bible cover.

### Quotes

1. "When fascism comes to America, it will be wrapped in a flag and carrying a cross."
2. "There aren't a lot of good examples of when religion and nationalism got blended together."
3. "You'd think that if people viewed the Constitution with such reverence, they'd want it presented in this fashion."
4. "It's just another product, like the shoes or the NFTs, just something he's putting his name on."
5. "Normally, it's pretty horrific."

### Oneliner

Trump's promotion of a Bible prompts debates on nationalism, religion, and the Constitution, reflecting on the dangers of blending religion and government.

### Audience

Activists, Religious Scholars

### On-the-ground actions from transcript

- Question the blending of religion and nationalism (implied)
- Contemplate the importance of separating religion and government (implied)

### Whats missing in summary

Deeper insights into the implications of intertwining religion and nationalism, and the potential risks associated with such actions.

### Tags

#Trump #Religion #Nationalism #Constitution #Fascism


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Trump's latest product and the
conversation surrounding it, and we'll kind of just run over some of the
discussion because it's prompted a lot of conversation, and if you don't know
what I'm talking about. There is a Bible that Trump is pushing. And the
conversations about it, they talk about the price. It's like $59. Where
that money is going to go is a question that's being asked. There's a lot of
backlash from religious scholars and that backlash just ranges all over the
place from the idea of the the nationalists presentation to adding to
the Bible because it includes other things to whether or not Trump is an
appropriate person. All of that stuff. And then you have people talking about how
it is. It's just another product. Like the shoes or the NFTs. Just something he's
putting his name off. Those conversations are probably left to other people be
better off that way. But there's one thing about it because it includes the
Constitution. You would think that if people viewed the Constitution with
such reverence that they would want it presented to them in this fashion, that
That they might think that the ideas are important.
You would think that maybe they would understand that the government shouldn't have a hand
in the establishment or the free exercise of a religion.
That that shouldn't be a part of it.
And when you think about it, there aren't a lot of good examples of when religion and
nationalism got blended together and the outcome was good.
Normally, it's pretty horrific.
It's bad.
To be honest, I can't think of a single example of when it was good, but I'm sure there's
an exception somewhere. If you think of one, put it down below. In the United
States, there's a very common saying. There's a phrase that gets used a lot,
and it was definitely talked about during Trump's administration. It's
something that most people would attribute to Sinclair Lewis, but I don't
actually think that's correct. But the saying is that when fascism comes to
America, it will be wrapped in a flag and carrying a cross. Maybe remember that as
as you look at a bible
that is
literally
wrapped in a flag
that's the cover
it's just a thought
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}