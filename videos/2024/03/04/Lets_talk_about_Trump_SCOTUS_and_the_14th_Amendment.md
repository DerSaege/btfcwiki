---
title: Let's talk about Trump, SCOTUS, and the 14th Amendment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=SsIQ1VdM6Ec) |
| Published | 2024/03/04 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the Supreme Court's decision on Colorado trying to use the 14th Amendment to keep Trump off the ballot, which was viewed as a long shot.
- Mentions that the Supreme Court ruled against this attempt, stating that it's Congress's responsibility, not the states', to enforce Section 3 of the 14th Amendment.
- Points out that the power to enforce this belongs to Congress, thereby ending similar cases in Maine and Illinois.
- Raises concerns about determining what constitutes insurrection and the lack of due process in such cases.
- Notes that the decision wasn't surprising, given the historical context and previous discourse.
- Concludes by underlining that the Constitution is not a magic fix and only works when individuals prioritize the country's best interests over personal gains.

### Quotes

1. "The Constitution is not a magic document."
2. "It only works if people have the best of intentions."
3. "It's not a magic document."

### Oneliner

Beau explains the Supreme Court's ruling on Colorado's 14th Amendment challenge against Trump, stressing the importance of good intentions for the Constitution to function effectively.

### Audience

Legal activists

### On-the-ground actions from transcript

- Organize to get out the vote (suggested)

### Whats missing in summary

The nuances of the Supreme Court's decision and its broader implications for enforcing the 14th Amendment.

### Tags

#SupremeCourt #14thAmendment #Constitution #Trump #Enforcement


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump
and the Supreme Court of the United States
and the 14th Amendment to the US Constitution
and the decision that came down dealing with Colorado
and what its impacts are.
Okay, so for those who need a little bit of a refresher,
because again, there are a lot of legal entanglements
involving the former president of the United States,
This one deals with states, in this case, Colorado, trying to use the 14th Amendment to the U.S.
Constitution to prohibit Trump from being on the ballot because the 14th Amendment, a section of
it, basically says that if you engage in insurrection, you don't get to hold office again, short version.
The Supreme Court said, no, that's not how this works, in a way.
Not really a surprise.
We've talked about this for like a year.
This was always viewed as a long shot.
Now interestingly enough, the reason the Supreme Court gave was pretty simple.
Because the Constitution makes Congress, rather than the states, responsible for enforcing
Section 3 against all federal officeholders and candidates, we reverse.
Short version, states can't do this.
This power belongs to Congress.
So Congress could do it, theoretically.
So the similar cases in Maine and Illinois, they're gone too.
This is the end of this.
Now during the arguments, there were a lot of other questions, and even beyond this answer,
there are other questions about whether or not the way they were going about it was constitutional.
One thing that the justices definitely seemed concerned about was a back and forth.
So Colorado says that Trump engaged in insurrection on the 6th, while Texas says that Biden engaged
in insurrection because he didn't do what they wanted at the border.
Because there's no set way of determining what insurrection is, there's kind of a lack
of due process as well.
a number of issues and they've been present. So this decision wasn't really a
surprise. I know that there were a lot of people hoping that this was going to
pan out, but it was never really likely. So where does it go from here? Nowhere.
This is it. As far as the 14th Amendment stuff goes, this is pretty much the end
of it. Sure, theoretically, somebody could try something in Congress. It seems unlikely.
I think that Americans need to remember that the Constitution is not a magic document.
The U.S. has a very long history of things not going well and a lot of bad things happening.
It's important to remember that the U.S. Constitution either explicitly authorized those
things or was powerless to prevent it.
The Constitution only works if people have the best interest of the country at heart.
It only works if you have advanced citizenship, people who are interested in informing themselves.
People who succumb to a partisan mindset, it doesn't blend well to that.
And when you have people seeking positions of power to feather their own nests, it doesn't
really help there either.
It's one of those things that it works if people have the best of intentions.
If they don't, well, it's just an obstacle for them to get around.
It's not a magic document.
So this is putting a lot more pressure on those who are organizing to get out the vote.
Anyway it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}