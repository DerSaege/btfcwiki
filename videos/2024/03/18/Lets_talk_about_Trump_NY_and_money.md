---
title: Let's talk about Trump, NY, and money....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=FcmCYmr1cSA) |
| Published | 2024/03/18 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau provides an overview of the latest developments regarding Trump and money, specifically focusing on the New York entanglement.
- Trump's team has disclosed that they are unable to secure a bond worth $464 million due to difficulties in finding companies willing to accept real estate as collateral.
- Various brokers and companies have declined to provide the bond, preferring cash or cash equivalents instead.
- The total amount now needed is over $550 million, which Trump seems unable to put up.
- Trump's attorneys mentioned that while the company is functioning fine for day-to-day expenses, they cannot afford to put up such a significant amount.
- Trump is requesting the appeals court to hold off enforcing the $464 million case until all appeals have been exhausted.
- New York authorities have not yet responded to this request.
- The uncertainty lies in whether the court will require the bond or grant Trump's request, with Beau admitting he doesn't have a clue about the outcome.
- The appeals court has a deadline within the next eight days to make a decision before enforcement begins on March 25th.
- Beau acknowledges that the situation will likely be extensively covered in the media, especially Trump's claim of lacking the necessary funds.

### Quotes
1. "Trump saying that he doesn't have the money is definitely something they're going to push out repeatedly."
2. "The big question is what's the court going to do? And I don't have a clue."
3. "I had no idea this was going to happen today."
4. "They can't get a bond for it."
5. "Trump's team is saying that they went to various brokers and companies."

### Oneliner
Beau provides insights on Trump's financial challenges in securing a $464 million bond in the New York entanglement, leaving uncertainty about the court's decision with a looming deadline in eight days.

### Audience
Financial analysts, legal experts

### On-the-ground actions from transcript
- Stay updated on the developments in the legal proceedings related to Trump's financial challenges (implied).

### Whats missing in summary
Insights on the potential implications of the court's decision on Trump's financial and legal standing.

### Tags
#Trump #FinancialChallenges #NewYork #LegalProceedings #Deadline


## Transcript
Well, howdy there, internet people.
It's Bob again.
So today, we are once again going to talk about Trump and money.
When that first video went out this morning,
I had no idea this was going to happen today.
There have been some developments when
it comes to the New York entanglement up there.
And Trump's team has put forth some information
that they are asking to be considered.
Okay, so what's going on?
That $464 million
case,
Trump's team is saying that they went to various
brokers and companies
trying to
get a bond for it
and they can't.
They can't get a bond for it.
and they have a bunch of different reasons as to why, but short version, some
of them didn't want to take real estate as collateral, they wanted cash or cash
equivalents, when you figure in the percentages now you're talking about
five hundred and fifty plus million dollars, and that basically this isn't
something that Trump can do, can't put it up, can't put the money up and the
attorneys for Trump kind of said that you know the company's doing fine but
they need day-to-day expenses and doing the day-to-day expenses and the putting
up the money it's just they don't have it they can't pull that off they can't
the bond for it. Okay, so that is more or less what's going on, obviously
paraphrasing. So what's Trump asking for? Asking the appeals court to not enforce
the 464 million dollar case until after the appeals have played out. That's
his move. That's what he's asking for here. Now at time of filming, New York,
they haven't really commented on this. Nothing from the office, anything like
that. The big question that has come in is, you know, what's the court going to do?
And I don't have a clue. Generally speaking, New York wants that bond up. At
same time, the argument being presented, it's not exactly unbelievable. So I don't
have a clue. The good news is we're not going to have to wait long because if
the appeals court is going to do something, they're going to need to do it
in the next eight days because I believe enforcement begins on March 25th. So
there's not a lot of information other than what was put out which is certainly
going to be all over the media because it's Trump saying that you know he
doesn't have the money which is definitely something that they're going
to you know push out repeatedly but as far as what's going to happen we're
going to have to wait but we won't have to wait long anyway it's just a thought
thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}