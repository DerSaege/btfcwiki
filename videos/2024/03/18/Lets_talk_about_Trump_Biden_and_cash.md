---
title: Let's talk about Trump, Biden, and cash....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=8Fz0Dpb4C8c) |
| Published | 2024/03/18 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the financial landscape of the Biden and Trump campaigns.
- Biden currently has $155 million cash on hand, the most ever for a Democratic candidate at this point in an election cycle.
- Biden received about $53 million in donations last month, with 97% being small donations of less than $200.
- Trump, on the other hand, has $36.6 million cash on hand in his two major committees, significantly less than Biden.
- Trump's fundraising numbers are not required to be disclosed until April, but it is expected that they will reveal his financial situation.
- There is a possibility for Trump to close the fundraising gap through a successful blitz, although Biden is currently out-raising him.
- Last month, Trump seemed to spend more than he brought in, indicating a concerning trend early on.
- The momentum of fundraising, particularly through small donor donations, will play a key role in gauging popularity and support.
- Small donor donations will be indicative of who holds more appeal among the electorate.
- These baseline numbers provide a starting point to track fundraising trends moving forward.

### Quotes

1. "Biden has about $155 million cash on hand, more than any other Democratic candidate ever at this point."
2. "97% of that are small donations of less than $200. That is healthy fundraising."
3. "The momentum of fundraising is going to be important."
4. "It's those small donor donations that are going to provide a pretty good indication of who is more popular."
5. "These baseline numbers provide a starting point to start with."

### Oneliner

Beau breaks down the financial status of the Biden and Trump campaigns, revealing Biden's significant lead in fundraising through small donations.

### Audience

Campaign strategists

### On-the-ground actions from transcript

- Support political campaigns through small donations (implied)
- Stay informed about campaign finance updates and trends (implied)

### Whats missing in summary

Analysis on potential implications of fundraising differences on campaign strategies and outreach efforts.

### Tags

#Biden #Trump #CampaignFinance #Donations #Fundraising


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Biden and Trump and cash.
We're gonna talk about the money
when it comes to the campaign,
because while it shouldn't be this way,
the amount of money spent on a campaign
can greatly influence the outcome of the election.
So we're gonna talk about the money situation
as it sits right now.
Okay, so starting off with Biden.
Biden has about $155 million cash on hand.
That is more than any other Democratic candidate for president at this point in time in the
election cycle ever.
Last month, he reportedly took in about $53 million.
What is notable about it is that according to Biden, 97% of that are small donations
of less than $200.
That is healthy fundraising.
That's really good for this point in time.
So where's Trump at?
His February numbers show that he had $36.6 million cash on hand in his two biggest committees.
His stuff is a little bit more spread out, so realistically, it's a little bit more
than that, but it's a fraction of what Biden has.
Now, the other thing to keep in mind is that Trump really doesn't have to show the real
numbers until April, but the expectation is that when those numbers come out, it's going
to become very apparent why Trump was incredibly interested in getting some
sway over the RNC. Remember every single penny to get Trump re-elected. Now that
is at this point. Things could change. It is possible that Trump launches a very
successful fundraising blitz, and he makes up a whole lot of ground. But at this point, Biden is
far out-raising him. The other thing to note is that last month, it did appear that Trump spent
more than he brought in. So the number is going the wrong direction. But again, it's incredibly
early to really start measuring these, but it's going to give us a good
baseline to see where things go from here. The momentum of fundraising is
going to be important, not necessarily in dollar amount, but it's those small
donor donations that are going to provide a pretty good indication of who
is more popular among those people who are going to do something about it. So we
could use that as a gauge as time goes on. And now we have some baseline numbers
to start with.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}