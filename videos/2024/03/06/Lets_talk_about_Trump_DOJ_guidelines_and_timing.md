---
title: Let's talk about Trump, DOJ guidelines, and timing....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=H70tALvH-p4) |
| Published | 2024/03/06 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump misinterpreting DOJ guidelines on election interference.
- Quoting guidelines to stop prosecution against him.
- Federal prosecutors cannot time actions to affect elections.
- Trump's misunderstanding of the guidelines.
- Former president not comprehending the quote.
- Guidelines prohibit adjusting timing to impact elections.
- Prosecution cannot be stopped to give an advantage.
- Trump's repeated claims of election interference are unfounded.
- Trump wanting DOJ to halt prosecution against him.
- Former president's failure to grasp the concept.

### Quotes

1. "They cannot adjust the timing to impact the election."
2. "It doesn't matter how many times he screams election interference."
3. "The former president apparently doesn't understand that quote."
4. "The timing of any action, meaning, hypothetically speaking..."
5. "Y'all have a good day."

### Oneliner

Trump misunderstands DOJ guidelines on election interference, wanting them to stop his prosecution to gain an advantage, which goes against the regulations.

### Audience

Legal scholars, political analysts.

### On-the-ground actions from transcript

- Understand and advocate for the proper application of DOJ guidelines on election interference (suggested).
- Support initiatives that uphold the integrity of legal processes and prevent political interference in prosecutions (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of Trump's misinterpretation of DOJ guidelines on election interference and his desire to stop his prosecution to gain an advantage.

### Tags

#DOJ #ElectionInterference #Trump #Prosecution #Guidelines


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump
and DOJ guidelines and Trump quoting DOJ guidelines
and what those guidelines actually mean
as opposed to what Trump apparently thinks they mean.
Okay, so over on his little social media site,
Trump, Trump says, we're in the middle of an election.
We have Super Tuesday coming up very shortly.
According to Justice Department guidelines, they shouldn't be prosecuting me.
Quotation marks.
Federal prosecutors and agents may never select the timing of any action, including investigative
steps, criminal charges, or statements for the purpose of effecting any election or for
the purpose of giving advantage or disadvantage to any candidate or political party."
End quotations.
And then in all caps, this is election interference and should be stopped by the courts.
Okay, so run through this again.
Federal prosecutors and agents may never select the timing of any action.
Now, we're going to remove the examples it gives here to just go ahead and complete the
sentence because apparently those examples threw him off.
Federal prosecutors and agents may never select the timing of any action for the purpose of
affecting any election or for the purpose of giving an advantage or disadvantage to
any candidate or political party.
The timing of any action, meaning, hypothetically speaking, if a candidate for, I don't know,
say president, was indicted and the case was moving along, they could not adjust
the timing to affect the election, meaning they couldn't stop the
proceedings until after the election because that would give an advantage to
that candidate. Trump wants DOJ to stop the prosecution and he's quoting
guidelines that prohibit them from doing what he wants them to do. It is very
interesting to me and I think it is incredibly telling that the former
president of the United States apparently doesn't understand that quote, doesn't
understand what he cited. It's the timing. They cannot adjust the timing to impact
the election. So if they've indicted you and they're going ahead with it, they
can't stop it to help you. That would be giving Trump an advantage. It doesn't
matter how many times he screams election interference. It doesn't matter
how many times he walks out into the yard and screams at clouds. That's not
what this means. The former president of the United States, I feel like he should
should understand that.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}