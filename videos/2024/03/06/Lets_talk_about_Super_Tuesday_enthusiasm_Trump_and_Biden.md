---
title: Let's talk about Super Tuesday enthusiasm, Trump, and Biden....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=qJeSZw0Ym8Q) |
| Published | 2024/03/06 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau provides an overview of Super Tuesday and the recent primary elections in the United States.
- He mentions that Trump and Biden were the winners, with Biden winning all his primaries and Trump losing one.
- Beau talks about how the percentages by which candidates win can indicate the enthusiasm within the party for that candidate.
- Trump won in various percentage brackets, while Biden had higher percentages showing more enthusiasm from voters.
- Beau notes that Biden seems to have more enthusiasm behind his campaign compared to Trump based on primary results.
- He mentions a program called Project Rebound at California State University Northridge, which supports formerly incarcerated individuals and encourages support for their fundraising day.

### Quotes

1. "Biden has more enthusiasm behind his voters."
2. "Trump underperformed again, as he normally does."
3. "The recidivism rate for that four-year period was zero."
4. "More people within the party were okay, at bare minimum, with voting for Biden."

### Oneliner

Beau analyzes Super Tuesday primaries, pointing out higher enthusiasm for Biden over Trump based on percentage wins and encourages support for Project Rebound.

### Audience

Voters, Supporters, Donors

### On-the-ground actions from transcript

- Support Project Rebound's giving day fundraiser at California State University Northridge by waiting for the official giving day to start and contributing to the program (suggested).
- Research and learn more about Project Rebound to understand its impact on formerly incarcerated individuals (implied).

### Whats missing in summary

Full understanding of the primary election breakdowns and the effectiveness of supporting programs like Project Rebound.

### Tags

#SuperTuesday #PrimaryElections #Biden #Trump #ProjectRebound


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Super Tuesday.
We're gonna talk about the primaries that happened yesterday.
For those that don't know,
for those overseas in particular,
yesterday was a big day for primaries in the United States.
Primaries are where the parties choose their candidate.
And we're gonna be looking at the presidential primaries
that occurred yesterday
and seeing what information we can pull out.
Not a whole lot of surprises, you know, Trump and Biden won, well, let me rephrase that,
Biden won all of his, Trump actually lost one, but overall both of them did as expected
when it comes to winning the races.
Okay, so that really doesn't tell us much, but you know what can tell us something?
percentages. The percentages by which they won is a pretty good indicator of
how enthusiastic people within the party are about that candidate. As an example,
if somebody, you know, got 51%, sure they won, but if the other candidate got like
90%, people are more enthusiastic about that candidate. So what we're going to do
is we're going to run through the percentages, the number of races that
fell in each little percentage bracket. We'll do it by tens, you know, 40 to 50%,
50 to 60%, so on and so forth. And we'll just see where everything kind of shakes
out. Okay, so starting in the 40s, a candidate that got in the 40s, you know,
percent of the vote of their own party. So Trump got one of those. That's the
one he lost. Biden didn't get any that low. Okay, in the 50s, Trump
got one of those too. Biden did not. He didn't get any that low. In the 60s,
Trump got four of those, four of those actually, but Biden didn't get any that low.
In the 70s, Trump got six. Biden got two. Biden did get two in the 70s. It is worth
noting that at least one of those was due to uncommitted voters. So there's
that that needs to be factored in. Okay, in the 80s, Trump got two. In the 80s,
Biden got nine in the 90s, scoring 90% or better.
Trump didn't get any that high, Biden got four.
Trump won, but these numbers do not indicate a lot of enthusiasm behind him in his own
party.
It's worth acknowledging that because enthusiasm in the primaries, a lot of times that indicates
turnout in the general.
It certainly appears, now this is not super scientific, but this certainly appears that
Biden has a lot more enthusiasm behind his voters.
And there are people that are going to say, well, you know, Trump was actually running
against somebody. Biden was running against people too. It just it just didn't seem like it.
Williamson actually put up some decent numbers in some states but it's just people for the most part
within the Democratic Party, especially among primary voters, they are in fact riding with Biden.
So that's that's what you have. Now the one thing to note about this is that I'm
doing this a little after midnight so not all of the the votes are tallied yet.
So this could change a little bit but not much. In fact at time of filming I
don't think any of Alaska's numbers are in so that one wasn't even included but
But overall, the big takeaway here is that Biden has more enthusiasm.
More people within the party were okay, at bare minimum, with voting for Biden than they
were for Trump, in Trump's own party.
He will undoubtedly come out and talk about what a great night that he had and all that
stuff.
He'll probably talk bad about Vermont, I think that's the one he lost.
But overall, he didn't perform well.
He underperformed again, as he normally does.
This is something to keep in mind when all of those terrifying polls of people who, you
know, pick up their phone to an unknown number emerge.
So that's what's going on now.
One other thing that's going on today is there will be a link down below.
Last year we put some support behind a program out at California State University Northridge
called Project Rebound and it's a program that helps formerly incarcerated people.
There'll be a link down below.
It's their giving day to raise funds for that program.
So if you have the ability, do that, but wait a couple hours from time of publication.
So their official giving day starts so if there are any bonuses that the university
or big donors give, I know last year if they got a certain number of donors, it got matched
or something like that. So wait until the actual giving day starts. There'll be a reminder
about this in a later video. But if you're not familiar with the program, look into it.
Just the shortest thing I can tell you is that I looked last year in the most recent
numbers I could find about something because I like to look at the effectiveness of stuff.
It covered a four-year period, and the recidivism rate for that four-year period was zero, if
you know anything about those rates.
The number was so low, I checked it like three times because I didn't believe it.
It's definitely effective as far as from a criminal justice perspective, but it also
skip people back on their feet.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}