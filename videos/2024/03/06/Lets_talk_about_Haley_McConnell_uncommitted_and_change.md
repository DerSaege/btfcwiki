---
title: Let's talk about Haley, McConnell, uncommitted, and change....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=m_nwGaZpn4Y) |
| Published | 2024/03/06 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Haley dropped out from pursuing the Republican nomination, signaling the end of her campaign due to the lack of a clear path to victory.
- The American voter in the general election serves as the last line of defense against underperforming candidates like Trump.
- McConnell’s endorsement of Trump surprised many, potentially to avoid a challenge for Senate leadership by withholding his support from Trump.
- McConnell’s endorsement of Trump may tarnish his legacy within the Republican Party, as Trump and McConnell do not necessarily share the same values.
- Biden lost in America's Samoa, where 51 out of 91 voters chose Jason Palmer, affecting the delegate count.
- Uncommitted voters risk overplaying their hand and pivoting Biden towards disaffected Republicans, potentially affecting turnout in the general election.
- Organizers behind the uncommitted movement likely have plans to address risks and exert leverage in the political landscape.
- Concern arises if Biden’s peace team succeeds in a peace deal, as it may necessitate immediate support from uncommitted voters.
- Beau encourages supporting the California State University Northridge Project Rebound, aiding formerly incarcerated individuals in education and reducing recidivism.

### Quotes

- "The American voter in the general election is the last line of defense."
- "I don't like McConnell, he was the heart of the American conservative movement for a very long time."
- "It's not like they're brand new. They have some kind of plan for this."
- "This will go through tomorrow night and there will be periodic reminders."
- "Y'all have a good day."

### Oneliner

Beau explains Haley's exit, McConnell's surprising move, Biden's loss, risks for uncommitted voters, and supporting Project Rebound.

### Audience

Political enthusiasts, voters

### On-the-ground actions from transcript

- Donate to the California State University Northridge Project Rebound to aid formerly incarcerated individuals in education and reduce recidivism (suggested)

### Whats missing in summary

Further insights into the potential impacts of Biden's loss in America's Samoa and the dynamics of uncommitted voters.

### Tags

#PoliticalLandscape #Haley #McConnell #Biden #UncommittedVoters #Support #Education #Recidivism #ProjectRebound


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about
the changing political landscape in the United States.
We're gonna talk about how yesterday, how the primaries
shifted things and changed some dynamics.
So we will talk about Haley, we will talk about McConnell,
and then we will also answer a question
that deals with Biden and voters
who are uncommitted.
Okay, so starting with Haley.
Haley has dropped out, no longer pursuing the Republican nomination for president.
What does that mean?
Well, it means that she didn't have a clear path to get there anymore.
It was over.
What does it mean for you?
It means that despite the fact that Trump is underperforming, when it comes to the general
election, barring something outside of the norm happening, the American voter in the
general election is the last line of defense.
The tool that is left is the ballot box.
That's what it means.
Okay.
What about McConnell?
McConnell endorsed Trump. A whole bunch of people were surprised by that.
Why? Because he set the stage and he made a move so he didn't have to.
At least that is what is widely believed. Odds are that when McConnell
announced that he was leaving Senate leadership, he was probably setting the
stage so he could withhold his endorsement from Trump and give it to
Haley. That's what was going on more than likely because Trump was begging McConnell
for that endorsement and Trump's real leverage would be to basically say, hey
Hey, you saw what we did to McCarthy, right?
Well have you challenged for leadership, too?
If McConnell is just like, okay, I'm leaving at the end of the year.
What happens?
Nobody wants to challenge for Senate leadership in an election year against McConnell when
he's already said that he's leaving in a few months, and it removed that leverage.
So it allowed McConnell to delay giving Trump the endorsement.
Once Haley dropped out, there was no reason to continue that play.
To me, it's surprising that McConnell gave it to him because I don't like McConnell,
he was the heart of American, of the American conservative movement for a
very long time. And rather than being remembered as that, he's going to be
remembered as the guy who endorsed the person who is trying to undo all of the
conservative wins that McConnell was able to accomplish. Because please keep
in mind, Trump isn't a conservative. Trump and McConnell, Trump and the
Republican Party don't actually share values. So, I mean, it is surprising to me that McConnell
endorsed. It really is. Not because it's out of character. As far as McConnell is concerned,
it's all about the party. But because it destroys the legacy he would have had within the Republican
party. He will not be remembered for all of his conservative moves. He will be remembered for
endorsing Trump. Okay, so a quick update on something. This information was not available
last night when I made the previous video. Biden lost in America's Samoa.
So, 91 people voted, 91 people voted, 51 of them voted for a person named Jason Palmer.
Jason Palmer got three of the delegates.
I think there's six up for grabs and I'm pretty sure that Palmer gets three and Biden gets
three.
So yeah, there's that.
And the only reason I'm really bringing this up is because I said that Biden won all of
his last night and then this happened.
So just wanted to put that information out there.
It's not like the three delegates are going to derail Biden's path to victory and getting
the nomination.
But a correction.
Okay, now on to the uncommitted voter and that question.
voted uncommitted and then got home and just had like a panic attack and started wondering
what the risks of the uncommitted movement really are.
Okay, so really there's three risks.
One is that they overplay their hand.
They have made their point.
They have applied the pressure.
Now if they become too intractable, Biden is looking at them and saying, okay, right
now he's looking at them and saying, okay, I've got to get these votes.
I need to get these people on board.
So he will slowly move in their direction.
And he has his peace team actually pushing in that direction too.
The risk is that they overplay their hand, they become too intractable, and Biden is
like, okay, well, I'm never going to be able to satisfy them.
I can't get their votes, and he pivots right to pick up the disaffected Republicans.
That's a concern.
Another one is that it depresses turnout for Biden in the general.
This is not something that they would ever say because it removes their leverage.
But they don't actually want another Trump presidency.
It's not like the people behind this are going to be like, oh, yeah, let's bring back the
administration that was like, oh, the settlements, those are totally cool.
They don't want that.
But those are two normal political risks and the important thing to remember is that the
people behind this, the organizers, at least the ones that I know that are a part of this,
it's not like they're brand new.
They have some kind of plan for this.
I would be really surprised if they didn't.
not new to politics, that they know how to exert leverage. So I would imagine they have an escape
hatch for those two risks. The only one that I'm actually like concerned about would be if Biden's
peace team actually is successful at producing a peace deal, not a ceasefire, a peace deal,
like a peace process.
I'm concerned about that because I don't think that they've planned for that eventuality
because I think that they honestly, that they do not believe that Biden's team is even pursuing
that.
So I don't think that they necessarily would have planned for that to be successful because
they don't even think it's happening, at least that's my read on it.
That's a risk because if they are successful, that peace plan, that deal, it's going to
have components that have to go through Congress.
And this block of people, they have to immediately switch gears and get on board with the peace
deal.
adversarial to Biden still and in fact if it was me that's kind of how I would
plan to do it. I would be like you know Biden did the bare minimum by getting
this you have to support it kind of thing but that's the only one I don't
that's the only one I'm not sure that they haven't accounted for and have a
plan for yet and that's just because those that I know that are actively
involved in this, they don't believe that Biden's team is even working on it, despite like lengthy
conversations about it. So that's the only one that I think is a real risk. And realistically,
because this isn't a top-down movement, because it's not hierarchical with the people barking orders,
I think most people who care about this, who are uncommitted, if a peace deal
comes up, I don't think they're gonna need to be told to switch gears. At least
I hope not. I'd have to question a lot of things if they did. So those are
the only real risks. They overplay their hand. I don't think they're gonna do that.
I think they understand that risk. I would imagine they have an escape hatch
to deal with the possibility of Trump coming back and then not being
ready for a situation in which Biden's team actually is successful. Those are
risks. So okay reminder down below there's going to be a link to a to a
page to donate to the California State University Northridge Project Rebound.
It is a program that helps the formerly incarcerated people get through school,
provides them with support structures, it has an amazing impact on recidivism, so
and so forth. We helped them last year with this and I think they raised just
over 20 grand. Right now, last time I looked it was at like eight, so off to a
good start. This will go through tomorrow night and there will be periodic
reminders and the link will be down there. So yeah, just check it out if it's
something that you're interested in and to answer the one question that came in
I dug around on their site and it says yes it is tax deductible and apparently
if you use if you go through that link and donate through that they will email
you a receipt. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}