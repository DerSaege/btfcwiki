---
title: Let's talk about Johnson considering creating a tough vote....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=G9ojtp6gYrg) |
| Published | 2024/03/15 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Speaker Johnson's sudden interest in moving aid packages in the US House of Representatives, aiming to force a tough vote on the Democratic Party.
- Johnson plans to split the aid packages for Ukraine and Israel, trying to create a challenging decision for the Democratic Party.
- The move is to push Democratic politicians into a vote that may not resonate with their constituents during an election year.
- Democratic Party leadership may advise representatives to vote according to their districts' preferences rather than a party line vote.
- Uncertainty surrounds the final vote outcome, with no clear data available for predictions.
- Democratic Party leadership signaling about conditioning aid may not influence the vote if passed in this manner, as per Sanders' letter to Biden.
- Johnson's strategy aims to have the Republican Party engaged in real politics instead of internal conflicts.
- This political maneuver resembles pre-former president tactics, aiming to put the opposition in a tough spot.
- Johnson's move is not finalized, and other Republican leaders like McConnell may have input on the strategy.
- The tactic seeks to create a challenging situation for the Democratic Party without genuine care for the aid recipients.

### Quotes

- "Forcing a hard vote for your opposition is normal."
- "It's the kind of politics we saw prior to the former president."
- "He doesn't suddenly care about any of this."

### Oneliner

Speaker Johnson stirs political maneuvers by pushing for aid package votes to challenge the Democratic Party, prioritizing strategy over genuine concern for aid recipients.

### Audience

Politically active individuals

### On-the-ground actions from transcript

- Contact your representatives to voice your opinions on aid packages and how they should vote (suggested).
- Stay informed about the political strategies and moves happening in your government (suggested).

### Whats missing in summary

Context on Speaker Johnson's political background and previous actions could provide a deeper understanding of his current strategy.

### Tags

#USPolitics #AidPackages #DemocraticParty #RepublicanParty #PoliticalStrategy


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about Speaker Johnson
in the US House of Representatives and aid and his sudden
interest in moving that along, even though it's been sitting
there this whole time, all of those aid packages.
And now, all of a sudden, he seems really interested to get
it to the floor.
Is this because he suddenly cares about any of these countries?
No, of course not.
He's indicated that he plans on, I guess, is considering splitting the aid packages
up, so there would be a vote for Ukraine, there would be a vote for Israel, and do it
that way.
Why?
he believes that it's going to force the Democratic Party into a tough vote.
Ukraine would probably go through.
The Democratic Party mostly supports it.
There's probably enough Republicans to cross over, and that would go through.
That's not the one he's talking about.
He's talking about Israel because you've had major signaling from the top Democratic
Party leadership.
So he's hoping to bring it to the floor and force the Democratic Party to make a
decision on what they're going to do.
My guess is that Jeffries is going to tell the Democratic Party representatives to vote
as you see fit.
They're not going to try to do a party line vote on it.
Does vote as you see fit mean vote as you believe?
No.
Check your polling data and your demographic information in your home district and vote the
way they want you to, which is, you know, kind of how the whole thing is supposed to work all the
time, but rarely does. Johnson is hoping that he can force Democratic politicians, representatives,
into a vote that their constituents don't agree with. And that way, during an election year,
it can be thrown up and say, hey, you voted this way. My guess is the Democratic Party
is going to see that coming, and they're just going to tell everybody to vote the way their
home district wants them to. Now, the obvious big questions about this, one would be, what
would the final vote be? Don't have a clue. Couldn't even guess what it would be. I have
not seen any data on that broken down to that level before.
And then the other one is with so much of the Democratic Party leadership signaling
about conditioning aid, does that matter?
Does that impact that if they pass it through this way?
Probably not, because the letter that Sanders sent to Biden, Sanders and the other senators,
It cited a law that's already on the books and they would have to carve out some kind
of exemption to that for that rationale to not be something that Biden could use if he
wanted to go that route.
So it probably wouldn't impact that.
It does produce a moment for the Republican Party to actually play like real politics
instead of fight amongst themselves.
I don't know how successful it's going to be,
so that's what it's about.
Now, none of this is set in stone yet.
Johnson has said he is considering doing that.
McConnell and other Republican leadership
may have suggestions about that.
I don't know.
But it's the kind of politics we saw prior to, you know, the former president.
Don't see a lot of this anymore because, well, things are different now.
But forcing a hard vote for your opposition is normal.
And that's what he's trying to do.
He doesn't suddenly care about any of this.
it's putting the Democratic Party in a difficult position,
or at least he thinks so.
I think they'll navigate it relatively easily.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}