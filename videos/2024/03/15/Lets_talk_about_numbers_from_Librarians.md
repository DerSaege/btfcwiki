---
title: Let's talk about numbers from Librarians....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=XkRaKO2sK7I) |
| Published | 2024/03/15 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The American Library Association tracks challenges to books in libraries, with 2023 being the highest year on record with 4,240 books challenged.
- The tracking is based on reports from librarians and news coverage, suggesting the actual numbers could be higher.
- Many challenges were organized, targeting certain types of books rather than individual disapproval.
- Some challenges included multiple books, especially focusing on books featuring people of color and the LGBTQ+ community.
- The ALA releases a list of the top 10 challenged books each year, providing unique perspectives and insights.
- Despite successful efforts to make challenged books available, this news story faded from coverage.
- The release of the top 10 challenged books for the year is expected on April 8th, often serving as a recommended reading list.
- Challenged books offer diverse viewpoints and ways of seeing the world, making them enlightening reads.

### Quotes

1. "2023 is the highest year on record with 4,240 books challenged."
2. "The type of book that was being challenged the most were books that put a spotlight on people of color, on the LGBTQ plus community."
3. "The books that end up on these lists they're normally pretty enlightening."

### Oneliner

Beau talks about the increase in challenges to books in libraries, focusing on organized efforts against diverse voices and the upcoming release of the top 10 challenged books list.

### Audience

Library advocates, book lovers

### On-the-ground actions from transcript

- Support libraries by promoting diverse literature and advocating against book challenges (implied)
- Stay informed about the top challenged books list and encourage others to read them (implied)

### Whats missing in summary

Insights on specific actions individuals can take to support libraries and combat book challenges.

### Tags

#BookChallenges #AmericanLibraryAssociation #DiverseLiterature #ReadingList #LibraryAdvocacy


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk a little bit
about the American Library Association
and some numbers they released.
And we're just gonna kind of browse through them
and take a look at where things are.
So for more than 30 years,
the American Library Association
has had a program that goes through
and it kind of catalogs and keeps track
challenges to books in libraries. Challenges to books in libraries. Another
way to say that would be attempts to ban books in libraries. And they have been
collecting this information for more than 30 years. 2023 is the highest year
on record. It is reportedly the highest year on record with 4,240 books challenged. The previous
year 2022 was 2,571. That's a pretty big increase. It is worth noting that the way they track this
information is by reports from librarians and news coverage. Short version, it means
that they don't have them all. The numbers are likely higher than what gets reported
in this. They're also saying that they discovered that a lot of the attempts to challenge were
organized. It wasn't a case of a parent having a child who read a book that they didn't
approve of, but an active attempt to go after certain types of books. And they
said that sometimes they would find out that a challenge included multiple books,
sometimes 10 books, 20 books, and more. The type of book that was being
challenged the most were books that put a spotlight on people of color, on the
LGBTQ plus community. It was those voices and those stories that seemed to get
challenged the most. This is something that you know we talk about every year.
The main reason is because they release the top 10 books that get challenged
every year. This year they will release that on April 8th and generally I use it
as a reading list. You know there are some books that make it every year but
there's normally some new stuff. The books that end up on these lists they're
normally pretty enlightening. They give you a different point of view, a
different way of looking at the world. So it's definitely something we'll talk
about again. The other thing to note about this is that this is a a news
story that kind of fell out of coverage because there were some pretty
successful attempts to to find ways to make sure that challenged books were
available through a whole bunch of different means. So it was one of those
things that kind of fell out of the news cycle. The numbers say maybe it shouldn't
have. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}