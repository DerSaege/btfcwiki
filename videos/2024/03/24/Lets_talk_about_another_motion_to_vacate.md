---
title: Let's talk about another motion to vacate....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=9KdK-D0vsDo) |
| Published | 2024/03/24 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Marjorie Taylor Greene filed a motion to vacate in the US House of Representatives aimed at the current speaker, Johnson.
- The motion is in response to the recently passed budget that has left both the far right and left dissatisfied.
- The constant process of replacing the speaker is causing disarray in the House, with even Newt Gingrich commenting on its ridiculousness.
- Gingrich pointed out that this constant shuffling is contributing to people leaving and the infighting within the Republican Party.
- Despite talk of unity, the Republican Party in the House remains in disarray.
- The motion filed is not under a procedure that requires an immediate vote, with Greene calling it a warning.
- It is uncertain whether the motion will be addressed soon or left to linger, potentially serving as a gesture for Greene's base on social media.

### Quotes

- "This is what happened to McCarthy."
- "It's a compromised budget."
- "He hasn't changed much."
- "Regardless of how a lot of Republicans in the House are talking about how they're starting to get that unity back? No, they're not."
- "It's just a thought."

### Oneliner

Marjorie Taylor Greene files a motion to vacate in the House over the dissatisfactory budget, contributing to ongoing disarray within the Republican Party.

### Audience

Politically engaged individuals.

### On-the-ground actions from transcript

- Contact your representatives to express your concerns about the constant shuffling of speakers in the House (suggested).
- Stay informed about the budgetary decisions and their impact on various political factions (implied).
- Engage in constructive political discourse to address disarray within parties (generated).

### Whats missing in summary

Insights on the potential implications of constant speaker shuffling and budget dissatisfaction on the functioning of the US House of Representatives.

### Tags

#USHouse #MarjorieTaylorGreene #Budget #Disarray #NewtGingrich


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, once again, we are going
to talk about a motion to vacate in the US
House of Representatives, because another one of those
has been filed, this time by Marjorie Taylor Greene.
If you don't remember, this motion
is what is used to replace the Speaker of the House.
This is what happened to McCarthy.
This motion is aimed at Johnson, the current speaker, and the obvious question is, why?
What is Marjorie Taylor Greene so mad about?
The answer is the budget, the budget that just went through.
There is a list of things in it that the far right is incredibly unhappy about.
It's a compromised budget.
was going to be happy. I would imagine that people on the left probably have
as much to complain about. Nobody was going to be happy with this budget. It's
a compromised budget. With the margins in Congress the way they are,
there was no way anybody was getting what they wanted. But that reality
apparently didn't matter. So the motion has been filed.
This process of constantly
replacing the speaker is something that is
leading to a lot of the disarray up there. It's so much that
even Newt Gingrich came out
to talk about how ridiculous it is. He didn't say ridiculous.
I believe he said that McCarthy's replacing McCarthy, it, quote, unleashed the demons,
which is just a totally newt Gingrich thing to say.
He hasn't changed much.
For those who don't know who he is, he was a Speaker of the House back in the late 1900s.
everybody who does remember him, you probably feel really old right now, but
even he came out to say that this is an issue, that this is part of the reason
people are leaving and part of the reason there's so much infighting and
jockeying and why the Republican Party is in the disarray that it's in. The
interesting thing to note about it is that the motion is just a normal motion.
It's not, it is not under the procedure that would require it to be voted on
anytime soon. Green herself referred to it as a warning. So does that mean it's
going to come up after the break or something like that? Maybe, it might, or it
it will just languish, and this was all about a motion to show her base on social media
that she is super upset about the budget.
It could be that.
But what this does show is that regardless of how a lot of Republicans in the House are
talking about how they're starting to get that unity back? No, they're not. It's
still in as much disarray as it was under McCarthy. It's just a little bit
more or low-key now.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}