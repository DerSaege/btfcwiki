---
title: Roads Not Taken EP 31
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Yi3XlPiX4Po) |
| Published | 2024/03/24 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- March 24th, 2024, episode 31 of The Road's Not Taken, where Beau covers unreported or underreported news events.
- French president considering sending troops to Ukraine; U.S. urging Ukraine to stop hitting Russia's refineries.
- Biden administration exploring aid delivery methods without American boots on the ground.
- Concern about black market for aid; flooding the area with supplies suggested as a solution.
- Palestinian clans reportedly securing aid convoys; Niger-U.S. relationship strained.
- U.S. facing questions about military presence in other countries; respecting host nation's decision is vital.
- Social Security retirement age increase proposed; Ken Buck hinting at future political moves.
- Montana Supreme Court to vote on family planning measure; Bernie advocating for a four-day workweek.
- Biden signed a $1.2 trillion funding package; compromise bill likely contains elements displeasing to many.
- Workers at a Volkswagen factory in Chattanooga seeking to join the UAW.
- Twitter daily users decreasing; measles spreading in the U.S., vaccination recommended.
- Double comet and eclipse on April 8th; warning against bogus eclipse glasses.
- Oklahoma National Guard team deployed for Eclipse influx; focus on logistics expertise.

### Quotes

- "They get to make that decision. If they want the U.S. out, the U.S. has to leave."
- "Be ready for parts of it that you don't like."
- "It's going to be yes and yes, there's going to be trains and there's going to be EVs."
- "Don't look for a single thing because it's not going to be one thing."
- "Y'all have a good day."

### Oneliner

Beau covers global events, aid strategies, political updates, and cultural shifts, reminding that choices have consequences.

### Audience

Global citizens

### On-the-ground actions from transcript

- Stay informed about global events and seek out diverse news sources (implied).
- Support aid organizations working in conflict zones (implied).
- Advocate for respectful foreign relations and military presence decisions (implied).

### Whats missing in summary

Insights into Beau's unique analysis and commentary on the news events discussed.

### Tags

#GlobalEvents #AidDelivery #PoliticalUpdates #CulturalShifts #CommunityAdvocacy


## Transcript
Well, howdy there, internet people, it's Beau again,
and welcome to The Roads with Beau.
Today is March 24th, 2024,
and this is episode 31 of The Road's Not Taken,
which is a weekly series
where we go through the previous week's events
and talk about news that went unreported, underreported,
lacked context, or I just found it interesting.
I am still doing the best I can
to preserve my voice and take it easy.
So we'll kind of make this a light one today.
Starting off in foreign policy, the French president has again refused to take the idea
of sending troops to Ukraine off the table.
The United States is reportedly urging Ukraine to stop hitting Russia's refineries.
The Biden administration is considering a whole bunch of different methods to move aid once
it comes off that pier.
Some of the methods that are being discussed, they cross that threshold about American boots
on the ground.
There seems to be a reluctance to use other partners.
A concern is the worry of a black market developing for aid.
The solution to that is simple, and it should be obvious in an incredibly capitalist nation,
but it's supply and demand.
If you flood the area with food, water, and aid, and just inundate the entire area, it
doesn't have a monetary value because it's everywhere. That's the solution to
that issue.
Something that we talked about last week, briefly, was the Palestinian network
structure there. They're called clans. It's just not a word that goes over well
in the U.S., they have reportedly started securing some of the aid
convoys. The reporting on how often that's occurring is spotty, but it does
look like it's happening. The relationship between Niger and the U.S.
is on the rocks and may lead to the U.S. leaving the country. There's still a lot
up in the air with that, and there have been a lot of questions about it, particularly
from people who are asking, you know, what the U.S. can do to stay.
To hear the decision makers there talk about it, be less condescending.
That seemed to be a big part of it.
But the important thing to remember here, this isn't a trade agreement.
This is a military presence in their country.
They get to make that decision.
If they want the U.S. out, the U.S. has to leave.
That's not really something you can debate.
The presence, a presence like that is at the request of the host nation.
They can't stay.
If they're told to leave, they have to leave.
And I know there's a whole bunch of people right now going, well, that's not always
the case.
Yes, the United States does at times have a history of ignoring that.
But as far as the way it should work, it's a military presence.
It's not a trade thing.
They have full jurisdiction over that choice.
Okay, moving on to U.S. news.
proposed raising Social Security retirement age in their wish list. Ken
Buck is teasing his next steps in politics. Like I said I didn't think he
was he was just gonna go away. The the stuff that's been said so far has been
been kind of vague. It seems like he might be doing some stuff in the media, but also
maybe running some kind of political groups. He's definitely keeping it close to his vast.
The Montana Supreme Court has agreed to voting on a family planning measure during the 2024
Bernie continues to push a plan to establish a four-day workweek with no loss of pay.
Now, it's unlikely to pass at this time, but it's pushed this conversation into the mainstream,
and it's moving that overton window.
Biden signed the $1.2 trillion funding package.
It was a compromise bill, so be ready for parts of it that you don't like.
There's definitely going to be some, and it doesn't matter where you sit on the political spectrum.
There's going to be stuff in this that you don't like.
There's going to be stuff that you really oppose.
Workers in Chattanooga at a Volkswagen factory have filed for an election to join the UAW.
In cultural news, reporting says that two research firms show that daily users of Twitter
have fallen over the last year, down 23% in the U.S. and 15% worldwide.
In science news, measles appears to be spreading in the U.S. still, and experts are suggesting
vaccination.
The double comet is anticipated to be visible on April 8th, the same day as the eclipse.
In oddities, another little bit about the eclipse, this is a bit of a public service
announcement, the American Astronomical Society is warning about bogus or ineffective protective
glasses for watching the Eclipse that are being sold.
They have a suggested vendor list, and that's at eclipse.aas.org slash I dash, I'll put
the link down below, wow.
Okay, so also kind of dealing with the Eclipse, but this is more getting out in front of what
is inevitably going to be a conspiracy theory.
An Oklahoma National Guard CBRN qualified team will be deployed, CBRN, chemical, biological,
radiological, and nuclear.
They will be deployed to help with the projected influx of people.
They're being sent because they're really good at logistics.
I wouldn't read too much into that.
Moving on to the questions.
Why do you call it family planning instead of abortion?
Because they're not just going after abortion, they're going after family planning.
Contraceptives, they're going after a lot of stuff, and it's not just one thing.
I would imagine if that's one of those questions, like asking if I'm avoiding it the way I
avoided the word pandemic. There is a little bit of a drop off with that word, but that's not the
reason I use it. Is not undoing US foreign policy towards Cuba by Biden an endorsement of it by his
foreign policy team. I don't know that I would call it an endorsement. I would say
that it's a low priority. That may change though, given some of the events. So if
you don't know, U.S. foreign policy towards Cuba, it's vintage. It's dated. It
It doesn't make any sense in the modern world, but it hasn't changed.
Now some of that is probably political at home, because there are a lot of voting blocks
in the United States that want that pressure kept up.
At the same time, U.S. foreign policy towards Cuba is not part of the modern era.
In many ways, it just doesn't make sense.
But yeah, I don't know that that would be an endorsement of the current foreign policy.
More of, ever since the Biden administration took over, they've been putting out foreign
policy fires everywhere, and some of them are really big and ongoing.
This would be something that would be addressed when nothing else is happening.
Why not mention trains instead of EVs?
There were a lot of questions about that under that video.
It's not an either or proposition.
It's not a thing where the transition is going to be all trains or all EV.
We have to completely shift our entire infrastructure and it's going to be yes and yes, there's
going to be trains and there's going to be EVs.
Most of these questions came from people who live in cities.
Please understand, there's a decent percentage of the population of the United States that
would have to drive like 200 miles to get to anywhere where there would be a drain.
But at the same time, when you're talking about public transit and even long distances
within the United States, trains could replace planes.
There's a lot that has to be done.
Don't look for a single thing because it's not going to be one thing.
It's going to be a lot of things.
Yes and.
Anyway, so that looks like it.
So a little more information, a little more context and having the right information will
make all the difference.
Y'all have a good day.
a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}