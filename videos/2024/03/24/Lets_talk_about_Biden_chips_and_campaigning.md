---
title: Let's talk about Biden, chips, and campaigning....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=_PdVtKNkd6E) |
| Published | 2024/03/24 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden was in Arizona showcasing the Chips Act and its benefits in creating jobs.
- Intel is set to receive either 8.5 billion in grants or 19.5 billion in grants and loans to invest in factories.
- The investment aims to induce Intel to spend 100 billion in four states to build or expand factories and create thousands of jobs.
- The states benefiting from this investment are Ohio, Arizona, New Mexico, and Oregon.
- Biden's focus is on bringing back manufacturing jobs to the U.S., which also has national security implications.
- Producing chips domestically aids in securing the supply chain.
- Intel aims to regain its position as a world leader in chip production, especially in AI chips by 2025.
- This initiative is viewed positively as it benefits politics, the average person, long-term economic growth, and national security.
- The Biden administration will continue to champion this initiative.
- The investment is expected to have far-reaching positive impacts.

### Quotes

- "Making the chips in the United States is good for supply chain stuff."
- "What's good politics is good for the average person, is good for long-term economic growth, and it's good for national security."

### Oneliner

Biden's initiative with Intel in Arizona to boost domestic chip production is a win-win for politics, the economy, and national security.

### Audience

Tech enthusiasts, policymakers, activists.

### On-the-ground actions from transcript

- Support local initiatives for job creation and economic growth (suggested).
- Stay informed and advocate for policies that enhance national security and economic stability (implied).

### Whats missing in summary

Insight into the specific details of Intel's plans and how they will impact the communities and industries involved.

### Tags

#Biden #Arizona #ChipsAct #Intel #Manufacturing #Jobs #Economy #NationalSecurity #CommunityImpact


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about Biden in Arizona
and Intel and chips and what's going on
because it's a big deal, okay.
So earlier this week, Biden was in Arizona
and kind of showing off how the Chips Act was doing
and how it was gonna help bring jobs and all of this stuff.
Now, there's details now and one of the questions that came in because there's two different
numbers.
It looks like Intel is going to get 8.5 billion in grants or the other number you're seeing
is 19.5 billion, but that is grants and loans.
that money is going to induce them to spend 100 billion dollars in four
different states to build or expand factories, creating thousands of jobs. I
know Arizona, I want to say it's 3,000 there and I don't even think that's the
big one. I think the big one is Ohio, the four states are Ohio, Arizona, New
Mexico and Oregon. Those are the ones that are going to benefit from it. Now
obviously you have the political aspect of this. Biden said I'm bringing back
jobs, manufacturing jobs, okay? And yeah, it does that. There's also the national
security aspect of it. Making the chips in the United States is good for supply
chain stuff and then you have the long-term economic stuff. As Intel ramps
up its production on all of this stuff it can once again become a world leader
in that rather than it you know happening somewhere else and with some
of the AI chips that they're talking about which I to be honest don't really
understand, but it appears that they want to take the lead in that by 2025 and this puts them on
track to do it. So this is one of those rare moments where what's good politics is good for
the average person, is good for long-term economic growth, and it's good for national security.
Hear more and more about this because the Biden administration is going to push this
out there because they look great right now. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}