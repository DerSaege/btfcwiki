---
title: Let's talk about former chair of the RNC, NBC, and Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=VF27KISnv7U) |
| Published | 2024/03/28 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former RNC chair McDaniel was set to become an NBC contributor.
- NBC staff and on-air talent expressed their discontent, leading to NBC retracting the offer.
- Trump criticized McDaniel's firing by NBC, leaving her in an awkward position.
- McDaniel may seek opportunities with Fox News or similar outlets.
- The public fallout sheds light on dynamics within Trump World.
- Speculation arises about the repercussions of McDaniel's dismissal on Trump supporters.
- Overall, a publicized chain of events involving McDaniel, NBC, and Trump.

### Quotes

1. "Fired by fake news NBC. She only lasted two days."
2. "It's called Never Neverland, and it's not a place you want to be."
3. "These radical left lunatics are crazy."
4. "And it's just an all caps, all man yelling at clouds rant."
5. "It's an interesting development for Trump World to see this so publicly."

### Oneliner

Former RNC chair's NBC contributor offer retracted after staff backlash, drawing public criticism from Trump, possibly leading to a shift to Fox News or similar outlets.

### Audience

Media Observers

### On-the-ground actions from transcript

- Analyze the dynamics of media and political relationships (suggested)
- Stay informed about developments in media and political spheres (suggested)

### Whats missing in summary

Insights into the possible implications of the publicized events for McDaniel and Trump supporters.

### Tags

#Media #Politics #NBC #Trump #FoxNews


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about
the former chair of the RNC.
We're gonna talk about NBC,
and we're going to talk about Trump's reaction to this,
and just kind of run through the chain of events.
If you missed it, the former chair of the RNC, McDaniel,
Daniel, she was slated to become an NBC contributor.
The news came out, and that's what was going to happen.
When the news came out, staff and on-air talent, they were not happy about that decision for
a variety of reasons, and they expressed their position very publicly.
Eventually, the top people at NBC, they decided that the staff was right, and it appears that
McDaniel will not be a contributor at NBC.
So what would the obvious next move be if you're McDaniel?
you you go back to the Republican Party and you find something to do. Trump, well,
he said this about somebody who was pretty loyal to him. Wow, McDaniel got
fired by fake news NBC. She only lasted two days. I think she actually lasted
And this, after McDaniel went out of her way to say what they wanted to hear, it leaves
her in a very strange place.
It's called Never Neverland, all caps, and it's not a place you want to be.
These radical left lunatics are crazy, all caps, and the top people at NBC are weak,
all caps.
They were broken, all caps, and then it just keeps going.
And it's just an all caps, all man yelling at clouds rant.
So it does not seem as though McDaniel would be welcomed back.
So if I had to guess, she will probably
find her way to Fox or some other similar outlet.
It seems unlikely that a center outlet is going to bring her on.
It's an interesting development for Trump World to see this so publicly.
You would think that yet another person going under the bus, it may not sit well with everybody.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}