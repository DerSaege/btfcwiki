---
title: Let's talk about Michigan, Biden, and a nuclear power plant....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=lBe7uGOdKZ8) |
| Published | 2024/03/28 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recently, the Biden administration announced providing a $1.5 billion loan to bring an 800-megawatt nuclear facility in Michigan back online.
- The facility, built in 1971 and shut down in 2022, is now aimed to be operational by 2025 and run until 2051.
- Two electric co-ops in rural areas have already agreed to purchase electricity from the plant long-term.
- Critics of the plan have requested a hearing with the Nuclear Regulatory Commission (NRC).
- The NRC, responsible for safety, will likely take these concerns seriously.
- This sudden shift indicates a strong interest from the Biden administration in expanding nuclear power in the US.
- Nuclear power is more costly than other green energies like solar and wind, but it offers reliability in areas where solar energy might be challenging to maintain.
- The move towards nuclear power seems to go beyond a mere formality and signals active pursuit.
- The future developments in this area are anticipated to continue beyond this announcement.
- Beau concludes by saying this shift towards nuclear power is just the beginning, hinting at more updates to come.

### Quotes

1. "It's worth noting nuclear power is more expensive than solar or wind or a lot of the other greener energies that are being pursued."
2. "The Biden administration is incredibly interested in building out the nuclear power capability of the US."
3. "We'll wait and see how it plays out."
4. "This is not the end of this."
5. "Y'all have a good day."

### Oneliner

The Biden administration is investing in bringing back a nuclear facility in Michigan, signaling a strong interest in expanding nuclear power despite critics' concerns, with more developments expected.

### Audience

Climate activists, energy policymakers.

### On-the-ground actions from transcript

- Contact local representatives to voice support or concerns about the revival of the nuclear facility in Michigan (suggested).
- Attend public hearings or meetings related to the Nuclear Regulatory Commission's decision on the facility (implied).

### Whats missing in summary

The full transcript provides a detailed insight into the recent development of investing in nuclear power in Michigan and the potential implications for the energy landscape in the US.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about Michigan
and nuclear power.
We're gonna talk about nuclear power again
because there's been a development recently
within the last few days, we were talking
about a pledge that the United States had signed
and said, hey, we're gonna devote more attention
to building out nuclear power within the United States.
At the time, we were like, hey,
I mean, they signed the pledge, but we don't know if they're actually going to do anything
with it.
Okay.
So the Biden administration is going to provide a $1.5 billion loan that according to reporting
will be paid back with interest to a company to bring an 800 megawatt facility back online.
It's in Michigan, I believe it was initially built in 1971, and was shut down in 2022.
I believe the plan was to actually dismantle it, but now they want it back up and running
by 2025, and it appears that they're going to try to keep the license and try to keep
it running through 2051.
reporting says that there are already two electric co-ops up there for rural
areas that have agreed to buy electricity from the plant over the long
term. So that's the news. It certainly does appear that they're going to put
some effort into that pledge. Now, obviously, this plan has its critics,
and they have requested a hearing with the Nuclear Regulatory Commission. My
guess is that the NRC is going to take their concerns very seriously because
Because it's the NRC that will be responsible for the safety and bringing it back online.
I don't foresee them just blowing off any concerns.
So we'll wait and see how that plays out.
Now this is a unique shift and it is apparently occurring very quickly where it appears the
Biden administration is incredibly interested in building out the nuclear power capability
of the US.
The pledge may have been a formality.
It may have been something that was discussed privately between a whole bunch of countries
and now they're just going to actively pursue it.
It's worth noting nuclear power is more expensive than solar or wind or a lot of the other greener
energies that are being pursued.
areas up north, well, solar gets hard to maintain at times.
So we'll wait and see how it plays out.
My guess is that this is not the end of this.
There's going to be more news on this front.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}