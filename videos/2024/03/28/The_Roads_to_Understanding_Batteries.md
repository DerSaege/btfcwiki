---
title: The Roads to Understanding Batteries....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Z5-uGGXku5M) |
| Published | 2024/03/28 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces the topic of adapting your surroundings and the importance of improvisation during tough situations.
- He talks about how altering your surroundings can make achieving desired outcomes easier.
- The focus is on creating power sources using simple materials like lemons, potatoes, and mud.
- Beau demonstrates creating a lemon battery using zinc and copper plates connected to a watch to generate electricity.
- The electrolytes in the water interact with the metals to create a chemical reaction that powers small devices like LED lights or digital watches.
- He explains the process step by step, making it accessible for anyone to try at home.
- Beau mentions building an electrolyte solution using table salt and water for another project involving cans, pencils, and wires.
- By introducing graphite from pencils to the solution, a chemical reaction occurs, generating an electric current to power a calculator.
- Another battery creation involves pennies, vinegar, table salt, and cardboard to build a functional flashlight.
- Beau provides detailed instructions on creating the battery using vinegar-soaked cardboard and copper-zinc coins.

### Quotes

1. "Your situation is a little bit easier to change in other ways."
2. "As you move forward, things will get more interesting."
3. "Having the right information will make all the difference."

### Oneliner

Beau introduces adapting surroundings through improvisation, demonstrating creating power sources from simple materials like lemons and pennies.

### Audience

DIY enthusiasts

### On-the-ground actions from transcript

- Build a lemon battery using zinc and copper plates (suggested)
- Create an electrolyte solution using table salt and water for a power project (suggested)
- Build a battery using pennies, vinegar, table salt, and cardboard to power a flashlight (suggested)

### Whats missing in summary

Practical demonstration of creating alternative power sources using everyday items.

### Tags

#DIY #Energy #Empowerment #Innovation #CreativeSolutions


## Transcript
Well, howdy there, internet people, it's Beau again.
And welcome to the Roads with Beau.
Today is going to be a little bit different.
We are going to delve into how to adapt your surroundings.
We talk about preparedness a lot on the channel.
And one of the things that we talk about
is how to improvise, how to alter your surroundings.
This is one of those things that's
it's incredibly valuable as a skill
during a bad situation, but once you start to look at the world through those eyes, you
start to see that your situation is a little bit easier to change in other ways.
It helps you view the world as components to an outcome you want to achieve, rather
than everything being an obstacle.
So today, we are going to talk about power and how to achieve it.
We're going to talk about batteries.
We're going to, of course, talk about the ones you learned about in school, a lemon,
a potato.
We'll even make one out of mud, but we'll also talk about some that are a little bit
more advanced, and these will be things that you can put together with the stuff
around you with the components that are available to you and then from there we
will continue a series on and learn about how to apply some of it. So to start
with we have an off-the-shelf science project kit here and we're going to use
this to form the basis of what we're doing and it functions by creating an
Enviro battery that uses zinc and copper plates in a
watery electrolyte solution.
The first version we're going to do here is one using a
lemon.
So you start with your lemon, and then you take it and you
cut it in half.
Now you're going to add the components from the kit.
You're going to take one zinc and one copper plate and
stick it into each half of the lemon.
The zinc acts as your negative electrode, and the
copper acts as your positive.
Connect one zinc and one copper plate from each half
with the white wire.
Then connect the red and black wires from the watch to the
proper electrodes.
Black is universally recognized as negative,
making the red the positive.
And here you can see the watch is now powered by your
lemon battery.
The electrolytes in the water interact with the metals
creating a chemical reaction.
This reaction breaks down the metals, causing them to
release electrons.
reaction happens quicker with the zinc plate causing a flow of electrons from
the zinc to the copper plate. This flow of electrons creates a small electric
current. Although the current is relatively weak, it is still able to power
a tiny LED light bulb, a small digital wristwatch, or a small circuit like a
sound chip. Now your medium for this could be nearly anything from around the
house or the yard. You can use a potato, you could use a piece of fruit, you could
you could use a couple of potted plants.
Really doesn't matter as long as you're completing the circuit.
All right, let me jump in and kind of interrupt for a second.
I know right now some of y'all are looking at this like, hey,
I did this in middle school or I did this in high school,
maybe even elementary school.
And for a lot of the beginning of this series, that's gonna be true.
But this is one of those things where everybody needs to be on the same page,
have the same foundational elements.
As we move forward, things will get more interesting.
bit more complex, a little bit more obscure as far as how some of this stuff works, but it all
builds on each other. I promise you, you will see this material again. So this next one is going
to require you to create an electrolyte solution by dissolving some table salt into water. Here's
everything you'll need for the next project. Take a quick look. Start by sanding the rims of your
cans to remove the coating and reveal the aluminum beneath. Attach your screw
connectors to the outside of the can. Now it's time to sharpen the pencils and
when you do that you're going to have to sharpen both ends of the pencils. Add
your saltwater solution to the cans filling them about three-quarters of the
way. Now you're going to connect your pencil holders. Then you're going to
place the pencils into the solution and begin connecting your wires. The positive
red wire connects to one of the pencil tips above the rim of the can, while the
negative, that's the black wire, connects to the screw on the adjacent can. Finish
by connecting the white wire from the other pencil tip to the screw on the
adjacent can. This completes the circuit. By introducing the graphite within the
pencils to the solution, a chemical reaction occurs. This reaction causes
some of the tiny water molecules to split into ion particles. Hydroxide ions from this split form
another reaction with the aluminum inside the can, creating a substance known as aluminum
hydroxide. This releases electrons that create the electric current used to power this calculator.
This is also sometimes called an aluminum air battery. Another battery that we can make up
real quick is one that uses pennies, vinegar, table salt and cardboard. Here are the contents
of the off-the-shelf kit that we bought. All you need from home are a few paper towels and some
distilled white vinegar and table salt. Start by punching out 17 coin-sized cardboard pieces then
count out 17 copper and 17 zinc coins. These would be the pennies if you were doing this not using a
kit. If you're using pennies, use post 1982 pennies because they are copper over zinc.
Here is a cool little casing that comes with this kit that we're going to use to build
the battery. Go ahead and mix up your solution. It's going to be one quarter cup vinegar and
one tablespoon of table salt. Soak the cardboard pieces for one to two minutes. Pull the cardboard
out of the solution and blot dry with a paper towel. You want the cardboard damp, not dripping
wet. Excess liquid will short out the current. Start your first stack of coins. In order,
it's going to be copper, zinc, cardboard. Start with the copper at the positive end
of the battery case. Repeat this stack 16 more times in the same order until you reach
the last stack. The final stack will end on the zinc coin, not the cardboard. Then you can close
up the battery casing. All that's left is to grab the flashlight. Then you're going to insert the
battery and replace the end cap. Test it out to make sure that it powers on. Then test, test,
test. Looks like you have a working flashlight. Okay, so that's a little bit more information,
a little more context and having the right information will make all the
difference. This series will be around, there will
be more coming, and as we do it you will
uh learn how to do some other stuff and alter your
your environment in other ways.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}