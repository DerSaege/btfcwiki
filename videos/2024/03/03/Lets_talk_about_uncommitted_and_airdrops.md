---
title: Let's talk about uncommitted and airdrops....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=KrGMTz9AAGc) |
| Published | 2024/03/03 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing uncommitted voters in Michigan and the impact of their stance on foreign policy decisions.
- Uncommitted voters in Michigan indirectly influencing the decision to conduct air drops as a good foreign policy move.
- Acknowledging the role of uncommitted voters in showcasing real support for the decision.
- Emphasizing that the decision for air drops might not have been as easy without the presence of uncommitted voters.
- Stating that the influence of uncommitted voters was significant in making a difficult decision easier.
- Expressing that uncommitted voters can rightfully claim credit for their role in influencing the decision.
- Pointing out the tangible impact of uncommitted voters' support on foreign policy decisions.
- Differentiating between causing a decision and influencing it, showcasing the importance of support in decision-making.
- Stressing that the support demonstrated by uncommitted voters was not merely symbolic but had real implications.
- Concluding that the influence exerted by uncommitted voters can have life-saving consequences.

### Quotes

1. "Uncommitted voters can claim credit for their role in influencing the decision."
2. "Support demonstrated by uncommitted voters had real implications, not just symbolic."
3. "Influence exerted by uncommitted voters can have life-saving consequences."

### Oneliner

Uncommitted voters in Michigan had a significant impact on foreign policy decisions, showcasing real support that influenced life-saving choices.

### Audience

Michigan voters

### On-the-ground actions from transcript

- Claim credit for influencing decisions (implied)
- Showcase real support for causes (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of how uncommitted voters in Michigan influenced foreign policy decisions through their support, showcasing the tangible impact of their stance.

### Tags

#Michigan #UncommittedVoters #ForeignPolicy #Influence #Support


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk a little bit
about air drops and decisions and how they're made
and uncommitted people in Michigan.
I'm gonna do this because I got this question
and I think it's worth going over.
Okay, so it says, I'm one of Michigan's uncommitted voters.
Your video the night of the primary made me mad.
They didn't say made me mad here, it's a little bit more colorful than that.
I couldn't understand what you could possibly mean by we wouldn't get what we were asking
for but we might get something else.
Now I've seen a few people say the airdrops were caused by us.
Is that true?
Caused by you?
I don't know if I would go that far, influenced, probably.
It's a good foreign policy decision.
The airdrops are a good foreign policy decision across the board.
But Biden and his team, they may not have known whether or not there would be support for it.
the uncommitted votes, that campaign, it showed that there was support.
So it's not like this was put on the table because of the uncommitted voters in Michigan.
But it was on the table and the fact that the uncommitted voters existed
made what might have been a difficult decision really easy. It's a decision
that might have gone the other way. So can uncommitted voters claim this as a
victory? Yeah, yeah, that's fair. I wouldn't, I don't like the term cost
because if this was a bad foreign policy move or something that wasn't
feasible it regardless of the uncommitted voters it wouldn't have
happened but because it was a good one and it was feasible the uncommitted
voters showed that there was support for it real support and that probably made
that decision very easy and it probably contributed it it contributed to it
being more than just a token we now have quotes from people on Biden's staff
making it very clear this is not going to be a one-and-done it's going to be
sustained, still don't know the scale yet, but it's not going to be just a token response.
All of these things were influenced by the uncommitted voter.
I don't know that I would go as far as to say caused.
I hope that makes sense.
And I think for a lot of people that's a distinction without a difference.
if you were part of that movement, when I was talking about you having political
capital to get stuff that could mitigate and help, this is exactly what I was
talking about. And I wouldn't, you can't say that this wouldn't have happened
without the uncommitted voter, but I would have no problem saying it's a
whole lot less likely. That movement showed that there was support, that it
wasn't just a bunch of idealistic kids dreaming at online polls, that it
was real. That makes it much easier to make a decision to do something tangible
when it comes to policy as long as it fit the other criteria, meaning it
actually was good for US foreign policy and it was feasible. There's tons of
things right now that are good for US foreign policy and they're feasible that
we're not doing because there isn't pronounced support for it. What happened
in Michigan showed pronounced support, so it made what might have been a difficult
decision easy. I totally think you can claim credit for this and say we had
something to do with this. I don't know I would use the word caused because
there's more to it than just the primary results. So I hope that
answers the question. It shows how the influence can be exerted and this
is it. And make no mistake about it, this is literally going to save lives. So I
wouldn't take that as a small win either. Anyway, it's just a thought. Y'all have a
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}