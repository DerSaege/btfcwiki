---
title: Roads Not Taken EP 28
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=V7CNz0X34b8) |
| Published | 2024/03/03 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Provides a weekly overview of events in "The Road's Not Taken" series.
- Mentions a British ship being sunk by the Houthis in the Red Sea.
- Talks about a deal for a six-week ceasefire in Gaza.
- Comments on Sweden joining NATO after 200 years of non-alignment.
- Mentions the US vice president meeting an Israeli minister without Netanyahu's approval.
- Updates on Trump's New York judgment and poll showing him leading by four points.
- Reports on the Department of Education launching an investigation into a school district.
- Mentions Wisconsin Supreme Court rejecting an appeal on new congressional maps.
- Talks about Trump's attorneys disputing E. Jean Carroll's claims on his financial state.
- Reports on a petition by Christians demanding Justice Thomas to recuse himself from ruling on Trump's case.
- Mentions Oregon possibly recriminalizing some substances.
- Updates on rising ocean temperatures and a massive blizzard in the Sierra Nevada.
- Reports on ongoing Texas wildfires and their impact on electrical lines and cattle markets.
- Mentions the discovery of an 1,100-pound bomb from World War II in the UK.
- Talks about progress in using pigs to grow organs for human transplant.
- Responds to viewer questions about food drops in Gaza, foreign policy, and Trump's impact on Palestinians.
- Mentions a Girl Scout troop called Troop 6000 serving Scouts in the shelter system.
- Explains his support for a particular foreign policy plan as a stepping stone for peace.

### Quotes

1. "I support that plan because it's the only thing that would work."
2. "There is no downside."
3. "It's a win across the board."
4. "Information needs to go out."
5. "Having the right information will make all the difference."

### Oneliner

Beau provides a weekly overview, touches on foreign policy events, US news, cultural and science updates, responds to viewer questions, and supports a foreign policy plan as a step towards peace.

### Audience

Viewers interested in global events and foreign policy.

### On-the-ground actions from transcript

- Support Troop 6000 serving Scouts in the shelter system by ordering Girl Scout cookies (suggested).
- Research the Pompeo Doctrine to understand potential impacts on Palestinians (suggested).

### Whats missing in summary

Insights into Beau's detailed explanations and unique perspectives on various global events beyond the brief overview. 

### Tags

#GlobalEvents #ForeignPolicy #USNews #ScienceUpdates #CommunitySupport #Troop6000 #PompeoDoctrine


## Transcript
Well, howdy there, internet people, it's Beau again,
and welcome to the Roads with Beau.
Today is March 3rd, 2024,
and this is episode 28 of The Road's Not Taken,
a weekly series where we go through
the previous week's events
and talk about news that was unreported, underreported,
didn't get the context it deserves,
or I just found it interesting,
and then we go through some questions from you.
If you want to send in a question,
It is question for Bo at Gmail.
And it is question, not questions.
OK, starting off, foreign policy.
A British ship was sunk by the Houthis in the Red Sea,
marks the first vessel that was actually lost during that.
There is reportedly a deal on the table for a six-week ceasefire
Gaza and right now it looks like people are waiting for the sides to provide
their commentary and whether or not they're gonna accept or dismiss it. The
ceremonial aspects of Sweden joining NATO are expected soon. Sweden will be
ending like a 200 year run of being non-aligned and since this is happening
after you know the the new year this puts Putin on the fast track to being
NATO's best recruiter for the third year running. Okay moving on to US news the
vice president will be meeting with an Israeli minister who apparently does not
have Netanyahu's approval. I would be prepared for some developments. I have a
hunch but I don't want to say yet. Okay Trump's New York judgment has already
accrued another million dollars in interest. A poll is causing a bunch of
waves because it shows Trump leading by four points. I would not draw too many
conclusions from that rather than just looking at the headline. I would look at
all of the questions that were asked and pay particular attention to the
percentages of those people who responded when it comes to which primary
they voted in. That might provide a little bit of insight. Okay, so as much
Much of the country is still waiting for further explanation.
The Department of Education has launched an investigation into Next Benedict's school
district.
This story is far from its conclusion.
The Wisconsin Supreme Court rejected an appeal to review the new congressional maps that
were adopted by the state last month.
Trump's attorneys took issue with E. Jean Carroll's objection to Trump wanting to delay
posting the bond in her case, saying that her, quote, current position that President
Trump's ability to satisfy a judgment of 83.3 million is in doubt is clearly inconsistent
with her position barely one month ago that President Trump has 14 billion in assets and
can thus easily satisfy an enormous punitive award.
I would suggest that goes both ways, and that maybe there have been some other developments
when it comes to his financial state, but we'll see what the judge decides on that.
In cultural news, a petition created by America Faithful has been signed by more than 15,000
Christians, and it demands that Justice Thomas recuse himself and not rule on the Trump case.
Oregon appears to be on the verge of recriminalizing some substances.
It's not a complete overhaul of the decriminalization efforts, but it's enough to have advocates
really concerned.
Moving on to science news, ocean temperatures continue to rise at a pace that is alarming
scientists because the rate is exceeding the modeling.
There's some real concern and they don't really seem to know why.
massive blizzard in the Sierra Nevada. Y'all be careful, stay warm. The Texas
wildfires are still raging. The larger fire is only 15% contained at time of
filming. In addition to the obvious issues, more than a hundred miles of
electrical lines have been destroyed, cutting power to a lot of areas. There is
also an expectation that there's going to be a disruption in the cattle markets.
So be prepared for images on social media saying, you know, oh look at the
the price of steak, you know, it's Biden inflation or whatever. I would be ready
for that. In oddities, a person in the United Kingdom stumbled upon an 1,100
pound bomb from World War II while doing some home renovations. It prompted the
evacuation of 10,000 residents while British EOD decided how to deal with the
device. And then in a weird blurb that just kind of caught my eye and haven't
done a lot of research on yet, progress is apparently being made in the quest to
use pigs to grow organs to transplant into humans. Okay, moving on to the Q&A.
Okay, there's a note here and it says, quote, there's like a hundred versions of this,
and they aren't all like the one you answered in the video.
Okay, I know you just did two videos on the air drops on Gaza, but I'm curious if the
food will be halal.
Okay, yeah, I have a rather snarky response to that coming up in a video, but that was
asked by somebody who did not just ask the question. They had a lot of additional commentary.
Okay, so there's two things at play here. First, when it comes to actual MREs, military
MREs, yes, a lot of that is pork. However, it is important to know that in certain circumstances,
It is not forbidden if you are facing starvation.
The other thing to know is that there's another product that is typically used for humanitarian
efforts that is in compliance with the various religious diets.
My guess is that in the beginning it's going to be a mix and then it will pretty much all
be the humanitarian mill going out.
And just so you know, the person who sent the other message, had they just asked the
question, they would have got a simple answer like that.
Okay.
You make very convincing arguments to change people's ideas.
For example, if the Palestinian forces steal food drops, food from airdrops and keep it
themselves and might turn Palestinians against them. However, sometimes after making a logical
and convincing argument about a subject, you say that the people who believed in that subject are
a bunch of dummies. Some people may take offense to that and discard the very opinion of which you
just convinced them. So yeah, in the video that has already gone out where I'm answering a question
about this. I have two tones. Like if it's not your job and you have this
opinion, well that's one thing. If you are a policymaker and you say something
like this, it is going to bother me. And the response will include
something like, well since you don't know anything about this. There's an
expectation for me, and I don't know why I hold this expectation, but there's an
expectation that I have of policymakers that says that they should inform
themselves before making public statements. And the statement about the
food being seized came from a policymaker, so I was a little bit more
Snarky in my response there. Just so y'all know, y'all haven't even seen the mean one yet.
There is another video coming that I almost didn't release because after watching it, I was like,
wow, it's really obvious that I am irked. But the information needs to go out.
And it's going over all of the objections that people have come up with to this.
And I'm just kind of running through them.
And in that, yes, I am, it's pretty snarky.
I am not the normal, polite person
that I am in some of those answers.
On a related note, something else that's worth mentioning,
because somebody sent a message about it.
I often seem more irritated when I am covering news about the government in Tennessee.
I grew up in Tennessee, so I am more bothered by what occurs there.
Okay. Oh, this is going to be a good one. I can tell from the first three words. I'm
a psychologist. I truly appreciate the no-frills focus on foreign policy in dealing with Gaza,
but I can tell it's eating you up to not openly express your opinion. It's easy to see you
support the three ingredients you keep mentioning. It might be helpful for you to publicly state
that you support that plan. Just a thought. You know, the funny thing is, I support that
plan as a stepping stone. It's not a, this is what I want. Like if in my ideal world,
if I could wish for anything and I wasn't trying to engage in the art of the possible,
That wouldn't be my plan.
But as far as what would work and what would build a durable peace and what would stop
people from getting hurt, I support that because it's the only thing that would work.
Not really a choice, more of a lack of options.
You supported these kids before, I thought I'd pass on this year's info.
Oh, okay.
So there's a Girl Scout troop, it's called Troop 6000, and you can find it online by
Googling it, and you can order it.
You can get your Girl Scout cookies through the link.
That particular troop serves Scouts that are currently in the shelter system and they could
be there for a number of reasons.
So it funds a lot of the activities.
So it's a worthy way of getting your thin mints or whatever and that is troop 6000.
You can find it online pretty easily.
Why didn't you mention that Jordan had done air drops in the first video about it?
Because that wasn't the question.
Normally, when I'm responding to a direct question, I try to limit my answer to that
question.
Um, I think I had actually mentioned that Jordan did the, uh, probably not all of their
drops.
I probably haven't mentioned all of them, but I'm fairly certain that I talked about
their drop at the hospital once because that was super cool.
Um, but yeah, it's just a matter of keeping the video concise and on topic.
Is there any truth to the idea that Trump would be worse for Palestinians than Biden?
Google Pompeo doctrine.
Just start there.
My personal opinion is that yes, he would be.
But that's something that you would have to research.
But there's plenty of ways to do that.
I would start by Googling something called the Pompeo Doctrine.
Why do I feel like if Biden only does one drop, he'll lose you?
Well, first, he doesn't have me.
I'm not somebody that's like riding with Biden.
I personally believe that the other candidate is literally a fascist.
So I mean, it would take a lot, like, I mean, I would never come out and be like, oh, vote
for Trump.
Again, I don't endorse candidates.
But I give people the benefit of the doubt until they don't deserve it.
I have been very understanding when it comes to some of the foreign policy constraints.
And I, there is a part of me that wish I hadn't started this coverage by only talking about
the foreign policy, but here we are.
When it comes to this drop, there are no foreign policy constraints.
This isn't like a lot of the other things where there are downstream effects that people
don't readily see.
You're providing food.
There's no negative outcome that can occur that he would need to consider.
The test has been done and it was successful.
If this turns out to be a publicity stunt and there's not follow on drops or they stop
the drops before they get other means worked out, it is definitely going to impact the
amount of the benefit of the doubt that I give him.
This is not like other proposals and other foreign policy things that have occurred in
relation to this.
This is very, very different.
I always talk about how foreign policy is not about morality.
It's not about anything except for power.
This is something that demonstrates U.S. power, that builds, I don't even want to say builds
support, how about reduces opposition.
And it assists in the long-term goal of a durable peace.
And there's no downside.
There is no downside.
The bad thing that could happen, it's just the normal stuff that goes along with a humanitarian
operation because there's always risk.
There's always some.
But it's very, very small, and it has the potential to have a huge impact.
And in pursuit of foreign policy, in pursuit of morality, in pursuit of American interest,
in pursuit of peace, in pursuit of everything, it's a win across the board.
And there's no downside.
It would definitely alter my perception if Biden only does one drop, absolutely.
Can you explain what purple hand medals are?
I'm looking it up and it shows me purple hearts.
Okay, yeah, so at the end of one of the videos about the drops, I mentioned how some people
are going to get a pretty purple medal with a hand.
Google Humanitarian Service Award, Humanitarian Service Medal, one of the two, and the whole
comment was comparing that to the medals that you would get for deployments in combat.
I personally would prefer to see the US service members of the future, their racks have the
purple metal with the stars, meaning you went on more than one deployment like that than
the combat ones.
That's what I was saying.
Humanitarian service to me is, I think that our country would be better served if we had
more humanitarian deployments.
Again, big supporter of the idea of the world's EMT rather than the world's policeman.
And that's it.
Those are all the questions and almost all of those were about the drops.
So we will find out more, hopefully, about the potential ceasefire because six weeks
is a long time.
That might be enough to work out a permanent one.
But they're being very, very close-holed when it comes to what the details are.
I feel like there might be something in this that one side or the other views as a non-starter.
There's something about this because the details aren't really public yet, but we should find
that out soon.
Okay, so I guess that's it.
Little bit more information, a little more context
and having the right information
will make all the difference.
Y'all out. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}