---
title: Let's talk about objections to the airdrops....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=uEK8li63I1A) |
| Published | 2024/03/03 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits
Beau says:

- The drops have started, testing was successful, and more drops are planned with planes being loaded and scheduled.
- Questions arise about feeding people during conflict, with comparisons to World War II.
- Feeding civilian populace is key in conflict to prevent desperation and combatant recruitment.
- Biden's actions are criticized as a ploy to win an election, but representatives should prioritize people's needs.
- Concerns about religious dietary restrictions and the use of pork MREs in aid drops are addressed.
- Air Force training with food drops is defended as a necessary and historical practice.
- Logistics and challenges of dropping aid by air versus sea are discussed.
- Scaling up aid drops to reach a large population like Gaza is feasible but may face pilot fatigue issues.
- The humanitarian aid efforts should be universally supported regardless of political affiliations.
- Rumors suggest continued and larger aid drops are planned, aiming for sustained support until no longer needed.

### Quotes
1. "Feeding civilian populace is key in conflict to prevent desperation and combatant recruitment."
2. "There are no negative foreign policy implications to it. This is one of those things that's just good all the way across the board."
3. "Air dropping supplies is a thing."
4. "It should continue and shouldn't just be a one-time thing."
5. "There is no reason to oppose this."

### Oneliner
Beau addresses questions on feeding people during conflict, defends aid drops, and calls for universal support, dispelling criticism of political motives.

### Audience
Humanitarian supporters

### On-the-ground actions from transcript
- Support ongoing and sustained humanitarian aid efforts (implied)
- Stay informed on the progress of aid drops and advocate for continued support (implied)
- Educate others on the importance of feeding civilian populations during conflict (implied)

### Whats missing in summary
The full transcript provides a detailed exploration of the rationale behind humanitarian aid drops and addresses common criticisms and questions, offering insights into the logistical challenges and importance of sustained support.

### Tags
#HumanitarianAid #ConflictResponse #Logistics #PoliticalCriticism #AidEfforts


## Transcript
Well, howdy there, internet people, let's bow again.
So today we are going to run through some more questions
related to the drops because y'all ask a lot of questions.
If you missed the news, the drops have started.
A test was conducted, it worked, it went well.
More drops are planned, planes are being loaded
and scheduled, everything seems to be going
the right direction, which makes a lot
these questions, let's just say even more annoying to me personally because most
of them are just a lot of gymnastics trying to justify not feeding people. But
I do think they're worth going through so we will. Just forgive me if I get a
little snarky with some of these. Okay so starting off, why are we doing this? We
didn't drop food into Berlin. I mean, are you sure about that? I get it though. I
get it. You're not talking about the Berlin air left. You are talking about,
you're actually talking about during World War II, you know, that conflict in the
1940s that was an entirely different kind of conflict in which the US was an
active participant and combatant in during a period in time where the U.S.
engaged in a whole bunch of things that today would be crimes. Yeah, things
were done differently back then in the completely different kind of conflict
in a completely different time. Yeah. Okay, here's the thing. This type of
conflict, if you do not think it is a good idea to feed the civilian populace,
none of your other opinions about it matter. Not to put too fine a point on it.
That is 101 level stuff right there. So it's, yes, to answer your question, you
know, why are we doing this? Because it's not World War II and because in this
type of conflict one of the key things is to eliminate desperation in the
civilian populace so they don't become combatants.
Also people are starving, and today we view it as a good thing to feed people.
Okay, Biden is only doing this to win an election in Michigan, an obvious reference to the uncommitted
voters.
This is a talking point coming out of the right.
You have even seen Republican politicians say this publicly.
Yes. I mean, no, it's not only happening because of that. I would say it's influenced
by it. I would say that it's influenced by it. But the fact that this is a criticism
is pretty telling. Quick reminder, the United States is a republic that is a representative
democracy. Republican politicians are not actually supposed to be your rulers. They're
supposed to represent. If a whole bunch of people, let's say a hundred thousand, decided to make a
statement, it should alter policy. The politician should represent and try to do that. That's
their job. They're not actually supposed to be your rulers. You're not supposed to do whatever
they're told or whatever you're told by them. That's how it's supposed to work.
work. This was directed to me personally. Did you not give any thought to the sadistic
joke the US is playing? Most of those meals, meaning MREs, are pork. Muslims can't eat
them. Those boxes will sit in the corner while they starve. It's a sick joke. And then it
goes into a lengthy thing about how I need to learn more about other cultures because
I am an ignorant redneck.
They can eat them.
The pork, MRE, meals, they can eat them.
And that's not me being apathetic to their religious beliefs.
It's that their religious beliefs include kind of exceptions for this exact kind of
situation.
A lot of things that are prohibited, they aren't in certain situations.
If you are starving, you can totally eat pork.
That's okay.
I mean, I don't know, I'm just ignorant hillbilly and all that stuff, but I'm fairly certain
that's how it works.
Maybe you could read a book about another culture and find out for me.
The other thing to note is my guess is in the early drops there's going to be a lot
of mix with a lot of MREs in it, but eventually they'll be getting something called an HDR,
which is like a humanitarian daily ration.
Maybe that's what it is.
That would be a good acronym for it if that's not actually what the acronym means.
it's like an MRE but is, I actually think they're vegetarian or either way they are
in line with the various religious beliefs. So again, it will be okay. Okay, this is normally
tied with something about money and how much it's going to cost and they need to train
dropping bombs, not food, they being the U.S. Air Force.
I disagree, I happen to like food, not bombs, instead.
But here we are, they need to train dropping bombs.
Yeah, this is gonna get expensive, it will.
But the idea that this isn't what the Air Force
is supposed to do, that's wrong.
It's just flat wrong.
I know that most of y'all love World War II,
so it's been a thing since then,
and it was used recently
during a rock
to get supplies into a city.
Air dropping supplies is a thing. Most of the right wing, you're convinced we're
going to war with China any day now. How exactly
do you think supplies are going to get to that island quickly?
So they can totally use this as training.
The UN said it was impossible to do this by air. No they didn't.
They didn't say it was impossible, they said it was inefficient, and they're right.
Especially when you're talking about an aid group.
Inefficient meaning cost prohibitive?
Yeah, it's incredibly inefficient.
Not for the US Air Force, though, because it's not cost prohibitive for them.
Let them use that bloated budget for something good.
It's not impossible, it's just expensive.
There are better ways to do it, like truck or by sea, but it can be done this way.
For those who asked questions about doing it by sea, yeah, it is complicated.
It's not simple.
For those that don't know, it's not like the boat can just pull up and offload.
There's more steps to it.
It would go from big boat to little boat to shore.
There's a whole lot to it, but it could do more faster that way.
Okay, how are they going to do this for 2.3 million people?
They're not, they're not.
I mean, they could, but that's not what's going to happen.
Okay, 2.3 million is the entire population of Gaza, according to the UN, one quarter
of which are facing starvation.
600,000. The test dropped 40,000 mils. It can totally be done. My guess is that
there's going to be a combination of things and I feel as though some
pressure might be mounting as far as humanitarian aid on the ground as
well. Make no mistake about it, if it was 2.3 million it could actually
still be done. It would be much harder. It would be a huge undertaking. The
British would definitely have to be getting involved and to be clear there
are other countries involved in this. The real issue that they're going to run
into, it's going to be pilot fatigue after a while.
That's going to be the real concern.
So other countries may have to chip in with pilots.
So those are the questions.
It can be scaled to get to the size it needs to be.
It can be done.
It's not an evil trick to give them MREs.
Those are probably just the closest.
Those were the easiest to get their hands on, ready to go.
There are other mills that would eventually be used,
or maybe they're being used now.
There's a whole bunch of benefits
to the Air Force doing this.
Yes, I am sure that uncommitted influenced this decision.
And yeah, that's about it.
The other thing is, is this a token?
Is it face-saving?
Is it, you know,
is it something that isn't gonna matter?
We don't know yet.
It doesn't look like it.
Based on the rumor mill, it doesn't look like it.
But we're not gonna know that yet.
We have to see the scale.
For those who don't get the question,
there is a concern from a lot of people
that this is gonna be like two drops or whatever
and then it's just gonna stop.
And it's just a way to kind of put a happy face on things.
Based on the rumor mill, I don't think that's what this is.
That being said, I will hold that judgment until I see how many drops are going to be happening.
This is something that should be universally supported.
It doesn't matter which side you're on.
It doesn't matter what political party you belong to.
doesn't matter what your ideology is, it doesn't matter what your religion is,
there is no reason to oppose this. There are no negative foreign policy
implications to it. This is one of those things that's just good all the way
across the board. I hope that that's understood and I hope that it
continues and it is at a sustained level. But the concern is that it's just face
saving, that it's just a it's a one-time stunt or something like that. From what I
understand it's not. The rumor mill says that it's not, that they are underway
already planning more and bigger and that it's going to continue until it's
no longer needed. If that's the case, it's good. If it's just a stunt, I will be the
first one to very publicly take issue with it. There are a lot of things that, due to
the foreign policy constraints I am I understand it I don't like it but I
understand it there is no foreign policy constraint here there's there is no
downside to this this is good all the way through so it should continue it
shouldn't just be a one-time thing and it doesn't look like it is but I'm also
going to wait and see. If it does turn into that, yeah, it's going to bother me a lot.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}