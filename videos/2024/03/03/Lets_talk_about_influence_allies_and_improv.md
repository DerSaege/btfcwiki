---
title: Let's talk about influence, allies, and improv....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=pM1LgtQPSVM) |
| Published | 2024/03/03 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of "yes and" in the rules of improv, focusing on agreement and building on ideas.
- Uses an example of feeding the homeless in a city to illustrate how to influence people positively.
- Emphasizes the importance of maintaining a positive and constructive tone to influence others effectively.
- Talks about addressing root causes rather than just providing temporary solutions.
- Encourages moving people further along the same line of thinking rather than opposing them.
- Acknowledges the feelings of hopelessness and frustration in some individuals but offers a way to shift perspectives positively.
- Stresses the need to understand the intentions behind comments that may seem discouraging.
- Urges to continue caring and taking action instead of giving up, especially in progressive causes.
- Reminds that small actions, even if not the ultimate solution, can still make a significant impact.
- Advises using the "yes and" approach in interactions to build on shared beliefs and move the narrative forward positively.

### Quotes

- "Yes, and is really important to this person and the others who may be feeling this way."
- "This can be done, it can be scaled up, and it can do a whole lot of good."
- "If you are seeing those comments and you feel like it's hopeless, I promise you none of those people want you to wash your hands of this and not care anymore."

### Oneliner

Influencing positively through agreement and constructive engagement, even in challenging situations, can shift perspectives and drive collective action towards meaningful change.

### Audience

Progressive activists

### On-the-ground actions from transcript

- Approach interactions with a "yes and" mindset to build on shared beliefs and ideas (suggested).
- Continue caring and taking action, even in the face of discouragement (implied).

### Whats missing in summary

The emotional depth and nuanced approach to influencing others positively through constructive engagement and maintaining hope amidst challenges.

### Tags

#Influence #CommunityEngagement #ProgressiveActivism #PositiveChange #ConstructiveDialogues


## Transcript
Well, howdy there internet people, it's Bo again.
So today we are going to do things backwards.
We're gonna do the video
and then I'll read the message that prompted it.
The message I'm gonna read,
I got six or seven of them with a very similar tone.
And that is leading us to talk about
How to influence people in the rules of improv.
I've done a video on this before,
going over all of the rules.
But tonight, we're going to kind of focus in
on one of the rules.
But if you have any questions, the video
that goes over everything will be down below.
Tonight, we will focus on yes and, the rule, yes and.
When I say improv, I'm talking like whose line is it anyway?
If you're ever watching something like that,
the one person, they may say, oh, your legs broke.
The other person immediately is limping.
Yes and.
They start limping and they move the story further.
They take that story to a new position,
But they agree with what was said to begin with.
This is important if you are trying
to influence people who are more or less in agreement with you
but just aren't as far down the road as you are.
Yes and.
Give you an example of how this could work.
Let's say there's a person who is in a major city,
and it's a city where it's not illegal to feed the homeless.
And they made 100 sandwiches, and they're handing them out.
Fed 100 people.
And right at the end, you walk up and you say,
you know how many homeless people there are in this city?
You didn't even put a dent in it.
Man, this is pointless.
Do you think that person is going
to listen to anything else you have to say?
Do you think you're going to be able to move them
to a different position based on that?
As far as the common good, the best case scenario
is that they just ignore you and continue
doing what they were doing.
The worst case scenario is that your statement
impacts their morale, and they lose hope.
They give up.
They stop, but let's do it a different way.
Let's say, instead of walking up and saying,
pfft, great, 100 sandwiches, big deal.
You walk up and you say, this is good.
That's great.
That's great.
This should be done on a much wider level.
In fact, it shouldn't be needed to be done at all.
We should address root causes
So, this doesn't happen, but you've probably made an ally that way, and you might even
convince them, somebody who may have believed that the most that could be done was handing
out sandwiches.
You may be able to move them to a different position in the story.
You might be able to move them a little bit further on down the line.
Yes, and. Not, no, it's a different tone and it helps shift people. This is incredibly
effective if the person is somewhat aligned with you, if they agree with you in some way.
Doesn't really work if, you know, they're opposed to you and whatever they're advocating
as something actively harmful, but if they're somewhat aligned, this is
incredibly effective because you can introduce new ideas that are in line
with what they already believe. You can move them further down the road and you
don't risk undermining them. The worst case scenario is they believe the thing
that's good is good. Maybe they don't go further, but you don't undermine them,
they don't lose hope, and they don't end up being opposition. Or just lose hope
them become apathetic. Yes and. Again there are other rules that are really
helpful here and again that video will be down below. So here's the message that
prompted this and again there were a few of these. I will admit I chose the
absolute saddest one but this person is not alone. Bo, I'm so down right now. I was
so excited that we were finally doing something, anything to help. I wanted to
do more. I voted uncommitted and I saw this as a win but now I'm not sure. I was
happy then I was on Twitter and saw people saying it didn't matter, that it
wasn't going to put a dent in it. Then, in your comments, it was a lot of the
same. We can't really feed 2 million people this way, can we? I really need you
here. It's just so hopeless. Is there even a reason to care? Maybe we should just
wash our hands of this or push Egypt to take them in. At least they'd be alive.
This is somebody who voted uncommitted. They're probably coming from a pretty
progressive place, but the rhetoric they saw convinced them that nothing could be
done.
Yes, and is really important to this person and the others who may be feeling this way.
This is not pointless.
This is not pointless.
You're right.
You can't feed 2.3 million people this way.
I mean, you can, but logistically that would be literally drops 24 hours a day, seven days
a week.
The thing is you don't actually have to do 2.3 million people this way.
The number I saw was 25%, which is 600,000.
The test drop was almost 10% of that.
It can totally be done.
It can help.
It can do a whole lot of good.
The people who, and this may be hard to believe, but the people who made you feel like it didn't
matter, like nothing could be done, that the best thing to do was just give up, that is
not what they were saying. I promise. I see the comments. That's not what they were saying.
They are saying, this needs to be done on a wide scale level. In fact, this shouldn't
even need to be done. We need to address root causes. That's what they're saying. They're
further down the road in the discussion and that they're not doing well at reaching back
for you. I guarantee you, not a single person who was saying, this isn't going to matter
or it's not going to put a dent in it or anything like that. If they were expressing
concern for, if they were expressing concern for people in Gaza, they weren't saying,
feed them. They want more done. A lot of people have suffered a moral injury and
they are angry. They're not doing a good job of explaining other things that
might be on the table, that they feel are on the table. But I can assure you that
that they don't want people to not get food.
This can be done, it can be scaled up, and it can do a whole lot of good.
It can save a lot of lives.
Those people who made you feel like this, on some level they're right.
This isn't a solution, but it's buying time.
It's doing something.
It's not a solution though.
At best, it allows people a little bit of breathing room, but there's more.
So just bear that in mind.
If you are seeing those comments and you feel like it's hopeless, I promise you none of
those people want you to wash your hands of this and not care anymore.
It's them being angry and not necessarily being good at moving the conversation along.
Yes and is a very good way.
If you are in contact with somebody, if you are talking to somebody and you are more progressive
than they are, or you are on a further,
if you're further down the road, than they are.
Don't, don't say no, say yes and, yes.
And there's more we can do too.
If you see somebody that's happy about something,
that is in line, moving in the direction
that you want to go, yes and.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}