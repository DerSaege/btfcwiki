---
title: Let's talk about measles in 2024....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=CNKZ1NY_iOA) |
| Published | 2024/03/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the unexpected topic of measles in 2024, after its elimination in 2000 due to successful vaccination programs.
- Notes the resurgence of measles in 2024 due to people doing their own research instead of relying on vaccines.
- Provides statistics on the effectiveness of the measles vaccine versus the chances of getting infected without it.
- Mentions the CDC guidance for children exposed to measles, requiring them to stay away from school for 21 days.
- Advises on the importance of isolating oneself from measles and the possibility of indirect spread.
- Alerts about the spread of norovirus and the need to maintain basic hygiene practices.
- Encourages checking on children's vaccination status and following preventive measures.

### Quotes

1. "We're gonna talk about measles in 2024, which is really weird."
2. "Just as a quick reminder, there's a vaccine for this."
3. "It's worth remembering it can spread even without direct contact."
4. "Wash your hands, don't touch your face, you know all the basic hygiene stuff."
5. "It's probably a good idea to see if your kids are up to date."

### Oneliner

Beau reminds us in 2024 about measles resurgence due to vaccine hesitancy, stressing the importance of vaccination, isolation, and hygiene practices.

### Audience

Parents, caregivers, community members

### On-the-ground actions from transcript

- Ensure your children are up to date with their measles vaccinations (implied).
- Follow CDC guidance on keeping children away from school for 21 days after exposure to measles (implied).
- Practice good hygiene by washing hands regularly and avoiding touching your face (implied).

### Whats missing in summary

Importance of community education on the effectiveness and necessity of vaccinations to prevent disease outbreaks.

### Tags

#Measles #Vaccination #PublicHealth #Hygiene #CommunitySafety


## Transcript
Well, howdy there, internet people, it's Bill again.
So today we're going to talk about something
that we really shouldn't have to talk about.
We're gonna talk about measles.
Yeah, we're gonna talk about measles in 2024,
which is really weird that we're talking about it in 2024,
because fun fact about measles
is that in 2000, it was declared eliminated in the United States because we had this like super successful, incredibly
effective vaccination program that did wonders, wonders, so much so that it was declared eliminated in the year 2000.
but here we are in 2024 with everybody doing their own research and we have a measles outbreak.
Just as a quick reminder, there's a vaccine for this.
A full course, if you're totally up to date, it is 98% effective. Conversely, if you are unvaccinated
and you come into contact with it, you have a 90% chance of getting it.
Incidentally, if you get it and you're a child, you have a one in five chance of
ending up in the hospital. CDC guidance says that your kids should stay away
from schools for 21 days after their most recent exposure. That's what it says.
Just so everybody knows. About measles. I'm reminding everybody because you know
we haven't had to deal with it in a while because again we had an incredibly
effective vaccination program. So I imagine some people have forgot how to
deal with it and how you need to isolate yourself from it. It is worth
remembering it can spread even without direct contact. So yeah. It's also worth
noting that norovirus is spreading up north so we're back to the back to that
old sign off you know wash your hands don't touch your face you know all the
basic hygiene stuff and it's probably a good idea to check to see if your kids
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}