---
title: Let's talk about Tennessee trying to remind people to stay in their place....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=e9yUJF2d8AQ) |
| Published | 2024/03/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Tennessee legislature expelled two members with darker skin tones while allowing the white member to stay, under the pretext of decorum and rules.
- The expelled members were supported by their communities for representing them authentically, as opposed to trying to rule over them.
- The Tennessee legislature is advancing a bill to prevent expelled members from being reappointed, seen as a way to keep certain individuals in their perceived place.
- Questions about the constitutionality of the bill arise, especially concerning the Tennessee State Constitution.
- There is a likelihood that the bill will pass in the state house but face challenges in the Senate due to constitutional concerns.
- The overarching message seems to be an attempt by those in power to remind the people of Tennessee that they do not decide who represents them; the authorities do.
- The tone of control and superiority from the Nashville leadership extends beyond this specific incident, reflecting a broader attitude towards all residents of Tennessee.
- The authorities are perceived as wanting to rule and dictate to the people rather than truly represent them.
- The message is clear: the people are expected to obey rather than have a say in their governance.
- Beau leaves with a reminder of the power dynamics at play in Tennessee and beyond, urging viewers to contemplate the situation.

### Quotes

1. "It's the same tone that has existed for so long."
2. "Your betters up there in Nashville, they have to find some way to remind you that you're wrong and that you don't really get to decide who your representatives are."
3. "They're not there to be their boss. They're not there to represent you. They're there to rule you and tell you what to do."
4. "None of you matter."
5. "You're supposed to obey."

### Oneliner

The Tennessee legislature's actions hint at a broader attitude of control and superiority, aiming to remind the people that they do not determine their representatives but are expected to obey authority unquestioningly.

### Audience

Tennessee residents

### On-the-ground actions from transcript

- Organize community meetings to raise awareness about the proposed bill and its potential impact (suggested)
- Reach out to local representatives to express concerns about the bill's implications for representation (implied)

### Whats missing in summary

The full transcript provides a deeper understanding of the power struggles within the Tennessee legislature and the implications of bills targeting reappointment post-expulsion.

### Tags

#Tennessee #Legislature #Representation #PowerDynamics #CommunityPolicing


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Tennessee.
And we're going to talk about Tennessee being Tennessee
in the most Tennessee way possible.
So some of you might remember a while back
in the Tennessee legislature, there were these three people.
And two of them got expelled.
You know, the two that have darker skin tones got expelled,
and the white one got to stay.
because it totally wasn't like this bigoted thing at all.
It was just about decorum, or proper rules,
and etiquette, or whatever.
Anyway, so the Tennessee legislature,
they went on this power trip
because they did not like two young black men.
Personal opinion, that's what I think it was.
They expelled them.
You know, they wound up back
because their communities support them
because they actually represent their communities
rather than try to rule over them
like, you know, authoritarian guns, just saying.
The fact that they wound up back in the Tennessee legislature,
that really bothered a lot of the people
in the House in particular.
So they have introduced a proposal.
They're advancing a bill that would
stop people who were expelled from being reappointed. Again, it is nothing more
than you uppity folk better stay in your place. That's what it is. It's the same
tone that has existed for so long. It's being advanced. There are questions about
about it, as far as just even the basic constitutionality when it comes to the Tennessee State Constitution
as to whether or not they can do this.
If I had to guess what's going to occur is it's going to pass in the state house.
The Senate, on the other hand, maybe not.
They may take a different view of it because there is a constitutional question there.
But at the end of the day, it's probably important to remind the people of Tennessee that even
once the people were appointed back, they win their elections, everything.
Your betters up there in Nashville, they have to find some way to remind you that you're
wrong and that you don't really get to decide who your representatives are.
They do.
They're in control and you don't matter.
In fact, we're going to change the rules so you have even less of a voice.
And if you think that this attitude just applies to certain people in Tennessee, no.
Please understand.
The people in Nashville that run that state, they think that way about all of you.
None of you matter.
They're not there to be their boss.
They're not there to represent you.
They're there to rule you and tell you what to do.
You're supposed to obey.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}