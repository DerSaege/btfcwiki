---
title: Let's talk about Trump underperforming....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=EOrWaQlkbBY) |
| Published | 2024/03/13 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing Trump's underperformance and the need for a better explanation.
- Providing examples of Trump's underperformance in polls.
- Explaining reasons behind the discrepancies in polling results.
- Mentioning the importance of looking at fundraising alongside poll numbers.
- Pointing out vested interests in portraying Trump as unstoppable despite underperforming.
- Emphasizing the reality of Trump being a former president and already proven to be a losing candidate.
- Encouraging vigilance and critical thinking in analyzing political narratives.

### Quotes

1. "Trump is underperforming."
2. "He is, in fact, already proven to be a losing."
3. "It's just a thought, y'all have a good day."

### Oneliner

Beau explains Trump's underperformance in polls, attributes it to electorate composition and non-response bias, and warns against complacency and biased narratives.

### Audience

Political analysts, voters

### On-the-ground actions from transcript

- Analyze polling data and fundraising trends to understand political performance (exemplified)
- Stay vigilant against biased political narratives (implied)

### Whats missing in summary

Full understanding of the impact of biased narratives and vested interests in political commentary.

### Tags

#Trump #Polls #Underperformance #ElectionAnalysis #PoliticalBias


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about Trump underperforming
a little bit.
We're gonna go through that.
I realize it's something I have said a few times,
and thanks to an incredibly eloquent message
that I received, I realized that maybe I haven't gone
into it in any way.
I've just said he's underperforming.
perhaps a better explanation is needed. So we're going to just kind of run
through that. Let's see what our very inspiring poet had to say. Trump's
underperforming? Question mark all caps. You clown? All caps. List one way, just one
way, in which Trump is underperforming. You female dog. He's winning, person who
engages and acts with other men. Check the polls, you female dog. He's leading that senile ice
cream sniffer. You losing. I'm assuming that means loser. Okay, so here's the thing. When I say he's
underperforming, I mean in a whole bunch of different ways from him not being able to unite
the party behind him to fundraising. But rather than me give you a whole bunch of, you know,
different categories, let's just do what you said. Check the polls. Obviously you
believe that he is performing well by the polls, so let's look at that. We could
start in New Hampshire where he was supposed to beat Haley by 18 points, but
he only beat her by 11. So that's a seven point difference. He's under
performing. I know you're you're you're saying well that's just one okay South
Carolina where he was supposed to beat her by 28 points and only beat her by
20. That's an 8-point difference under performing the polls. We have checked
them now and to be clear those aren't the only times that's happened. So what's
occurring is you're saying he's performing well because of the polls. The
polls that show him at most what three or four points up doesn't really help
if on the day of he's down seven or eight points, right? The polling is not
actually great. Why? There's a whole bunch of different reasons of being floated
it around. There's three that I think are worth addressing. One is the one that's
being pushed out the most by the people responsible for the polling, which is
that it's undecided voters. Yeah, sure. All of the undecided voters, they decided
to break one way for one candidate. Nobody's buying that. That's not it.
That's definitely not it. The other is electorate composition and that's
like a super technical way to say unlikely voters. What they think is
happening is that the people conducting the polls, they're getting their
measurements right, you know. They're actually able to transfer everything
correctly but they're asking the wrong people. Yeah, I mean I think that has
something to do with it right there. That's probably it. Again, something
that has factored into like the last couple of elections. Yeah, I mean that
that may have something to do with it. That would track. The other thing is a
non-response bias. That means some people are not participating in the survey and
and they make up a chunk of one group, meaning those people more likely to pick
up the phone if the call is from an unknown number are more likely to
support Trump. Therefore, there's a there's a bias in his favor in the
polling. Generally speaking, non-response bias isn't really a thing in most cases.
In this case, yeah, I think that has something to do with it. I think it's a
combination of the composition of the electorate and a non-response bias
that's creating this. Trump is underperforming. I'm sorry. Look at the
fundraising as well. There's that going on too. It's important not to get
complacent, but it's also important to remember that a whole lot of people
who provide commentary on this have a vested interest in it being close, and a
whole lot of people who provide commentary on this have a vested
interest in portraying Trump as if he is unstoppable.
If he was unstoppable, he wouldn't be the former president.
He is, in fact, already proven to be a losing.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}