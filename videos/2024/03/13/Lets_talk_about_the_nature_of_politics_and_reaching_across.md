---
title: Let's talk about the nature of politics and reaching across....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=1qfFbA6LRuo) |
| Published | 2024/03/13 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the nature of politics and what it should be in theory.
- Addressing the topic of reaching out to Trump supporters and whether it's effective.
- Mention of some Trump supporters changing their minds, leading to Trump being the former president.
- Politics is meant to make things better and evolve society to a slightly better place.
- Laws are tools for change, but shifts in thought in individuals must occur first.
- Belief in changing the world hinges on people changing their minds.
- Acknowledging issues like potential cuts to entitlements isn't a waste of time.
- Encouraging people to change their minds is vital for creating a better society.
- The goal of politics is to create a better world through mindset shifts.
- Emphasizing the importance of encouraging change for a better future.

### Quotes

1. "Politics is meant to make things better and evolve society to a slightly better place."
2. "Belief in changing the world hinges on people changing their minds."
3. "Encouraging people to change their minds is vital for creating a better society."
4. "Acknowledging issues like potential cuts to entitlements isn't a waste of time."
5. "The goal of politics is to create a better world through mindset shifts."

### Oneliner

Beau delves into the essence of politics, the significance of changing minds for societal progress, and the necessity of reaching out to all individuals, not just Trump supporters, to strive for a better world.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Reach out to individuals with differing political views to encourage understanding and open-mindedness (implied).
- Acknowledge and address concerns such as potential cuts to entitlements in political discourse (implied).
- Advocate for a society where mindset shifts lead to positive change and progress (implied).

### Whats missing in summary

The full transcript provides a nuanced exploration of the role of politics in society and the importance of fostering mindset shifts for a better future.

### Tags

#Politics #PoliticalEngagement #MindsetShifts #SocietalProgress #ReachingOut


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about politics.
What politics should be, at least in theory.
The nature of politics, I guess.
And we're going to talk about Trump supporters
and reaching out to them and whether or not it matters
or it's effective.
We're going to do this because I got a message.
Why do you keep trying to reach out to Trump supporters,
expecting them to change their minds.
That level of optimism is too much.
And it's not just Trump supporters,
but in the last couple of videos,
you've specifically talked about or to them.
They aren't going to change their minds
over Social Security or the RNC.
You're wasting your time.
I mean, but some did, though, right?
Some did change their minds.
That's why he's the former president. The United States collectively
changed its mind.
And then you have some public figures
that changed
their mind about it.
And it gets to the nature of politics.
What is politics?
We're talking about electoral politics.
What is it?
What's it supposed to be?
at least in theory, it's supposed to be to make things better, right?
To evolve a little bit, get to a slightly better place.
That's what's hoped for in the current system.
And sometimes it gets grandiose when people talk about it, we're going to change the world.
When they say that, they don't really mean change the world as in change the earth, right?
They mean change society.
And we've talked about that.
Laws are a tool to help that, but most times the shift in thought occurs before the laws
change.
So that shift in thought occurs in people, individual people.
Because if people didn't change their mind about something, the laws wouldn't change.
even when people change their mind about something the laws don't change. But that's what it's
supposed to be. You can't really believe that you can change the world if you don't
believe that people can change their minds. Those two things go together. Now, as far
as wasting my time being too optimistic, maybe I might be too optimistic at times, I'll cop
to that, but if somebody is watching one of those videos and that is something that resonates
with them, that Trump is looking to cut their quote entitlement that they paid into their
entire life. I don't think an acknowledgement of that, I don't think
those 15 seconds, I don't think that's wasted. I think it could help and if it
It doesn't, it's a few seconds.
I don't think it's a huge task to include that.
The goal of politics should be to create that better world, through a better society.
To get there, people have to change their minds.
They have to take that step.
Not encouraging that, I don't think it's a waste of the five seconds because it might
help.
It might help them change their mind and it's not, I mean you said it, but it's not just
Trump supporters. There's a lot of encouragement that needs to happen if you want to get to
that better world. That world that is better than the most we can hope for is a slight
change or evolution. There's a lot of minds that need to be changed. Now sure, there are
There are some people that won't, but there are some people that will.
Anyway, it's just a thought.
You all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}