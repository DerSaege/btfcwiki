---
title: Let's talk about Trump's delay attempt and confusion....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=0HPp8y_Tj6Y) |
| Published | 2024/03/08 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Providing an update on Trump's requested delays in the E. Jean Carroll case involving an $83 million judgment.
- Two requests for a delay - one for three days and the other for a longer pause until post-trial motions are resolved.
- The judge denied the three-day delay but hasn't ruled on the longer pause yet.
- Attorneys believe the judgment amount may be reduced or eliminated, seeking to delay enforcement until then.
- There's talk about a partial bond as well, and E. Jean Carroll can start trying to collect on Monday if no further delay is granted.
- Speculation on whether Trump can meet the financial obligations, given potential judgments totaling over $500 million.
- Confusion arose regarding the denial of a delay, with most assuming it was related to the post-trial motion which is still pending.
- Uncertain if the judge will issue an order over the weekend if no decision is made today.

### Quotes

1. "Attorneys believe the judgment amount may be reduced or eliminated, seeking to delay enforcement until then."
2. "E. Jean Carroll can start trying to collect on Monday if no further delay is granted."
3. "Given the fact that you're talking about what more than 500 million dollars, I mean I imagine it's not going to be easy to pull it all together."
4. "There was a lot of confusion because they said that the delay was denied and I think most people thought that dealt with the post-trial motion one."
5. "It could just be normal Trump stuff."

### Oneliner

Beau provides an update on Trump's requested delays in the E. Jean Carroll case and the potential implications of the pending decisions on the $83 million judgment.

### Audience

Legal observers

### On-the-ground actions from transcript

- Stay updated on the developments in the E. Jean Carroll case (implied).
- Follow legal commentary on the potential outcomes of the judgment (implied).

### What's missing in summary

Analysis of the impact on Trump's financial situation and reputation.

### Tags

#Trump #LegalCase #EJeanCarroll #DelayRequests #Judgment


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Trump
and the requested delays that he has made,
a decision on one of those,
and that's where some of the confusion is coming in,
and just kind of run through what's going on
with all of that stuff.
A quick update for those who didn't see the recap last night, the fundraiser that has
been going on for Project Rebound, that has finished with $90,000 raised, so y'all did
really good.
There will be more about that, I'll put together like a full video about that later.
Okay, back to Trump.
There are two requests for a delay.
This is where it's gotten confusing.
There's two.
And this is in the E. Jean Carroll case with the 83.3 million, I think three.
One of them was for a three-day delay.
The other is for a longer pause until all of the post-trial motions are figured out.
And they're both seeking to delay enforcement of, well, the $83 million judgment.
The judge denied the three-day delay.
The judge hasn't ruled on the longer pause, which would go until all the motions are done.
Most attorneys, they're arguing that basically there's a high expectation or they think it's
very probable that the judgment will be reduced, that the amount of money that he's going to
have to pay is going to be reduced or eliminated.
That's what they're saying.
And they want to delay it until then.
And then there's also talk about a partial bond as well.
there's still things that are open, but my understanding is that E. Jean Carroll
can begin trying to collect on Monday. So, I mean, my expectation would be that if
you don't hear something today about the longer delay, the one until after the
post-trial motions that I mean come Monday he's gonna have to pay or put up
a bond and if he does a bond I want to say instead of 83 it would be 91
something so or I guess he could do a deposit with the court put up collateral
there's a couple different things but things have to start to go into motion
in on Monday, and the judge has said that he'll rule on it as soon as reasonably possible,
but I mean, unless we hear something today, I don't really know what that means.
I don't know that the judge is going to issue an order over the weekend or decision over
the weekend.
There are commentators who are suggesting that this means Trump doesn't have the 83,
you know, can't pull it together, I don't know that it means that, to be honest.
Trump has a tendency to delay regardless of the situation. So I've seen that
commentary. I don't know that that's what it means. It could just be normal Trump
stuff. But given the fact that when you start adding all of this stuff up, these
different judgments, you're talking about what more than 500 million dollars.
I mean I imagine it's not going to be easy to pull it all together but I know that when
the reporting went out there was a lot of confusion because they said that the delay
was denied and I think most people thought that dealt with the post-trial motion one
and that one still hasn't been ruled on.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}