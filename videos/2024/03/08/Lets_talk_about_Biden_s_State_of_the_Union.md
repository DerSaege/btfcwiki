---
title: Let's talk about Biden's State of the Union....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=QULXij0_Sug) |
| Published | 2024/03/08 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The State of the Union was different this time, with Biden being more energetic than usual and making jabs at Trump without mentioning his name.
- Biden used the phrase "the greatest story never told" when talking about the economy, possibly taking a swipe at negative media coverage and the Democratic Party's messaging.
- He emphasized the lies surrounding the election as the greatest threat to democracy and spoke extensively on foreign policy, including Ukraine, NATO countries, and Gaza.
- Beau encourages watching the State of the Union and suggests that calling representatives, especially if they are Republican, could help bring change regarding Gaza.
- He stresses the urgency of taking action now, mentioning suggestions like using Chinooks to deliver aid to those in need.

### Quotes

1. "This is the moment where it's not rhetoric. It's, you got to pull out all the stops."
2. "Whatever it takes. Whatever it takes."
3. "It has to be now."
4. "Our policies are at least better than the Republicans but that doesn't happen."
5. "If calling your representatives, especially if they are Republican, could help."

### Oneliner

Beau breaks down the unique aspects of Biden's energetic State of the Union, addressing jabs at Trump, the economy, foreign policy, and urgent calls for action on Gaza.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Contact your Republican representatives in the House to push for support on Gaza (suggested)
- Advocate for urgent action on delivering aid to those in need, using any means necessary (exemplified)

### Whats missing in summary

Insights on how to navigate the changing political landscape and take tangible actions to support urgent causes.

### Tags

#StateOfTheUnion #Biden #Gaza #ForeignPolicy #PoliticalAction


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about the State of the Union
and how this was a little different.
And we'll run through some unique things
that I think are worth mentioning out of it.
There's going to be tons of here's the top eight takeaways
or whatever to get more information from.
Before we get into that, quick recap,
because I'm sure, at this point, people are tired of the interruptions.
But the fundraiser for Project Rebound has closed $90,000, $90,000.
Please keep in mind, the hope was that we were going to be able to push enough that way
to match what happened the last year which was 23,000. Just really, really good
stuff. But y'all don't want to hear about that, y'all want to hear about the State
of the Union. Okay, if you have seen a bunch of State of the Union addresses in
the past, I would watch this just for comparison sake because it was
different. Normally, a State of the Union speech is, it has sound bites for the
media, but for the most part it is low energy and boring. Very policy-driven,
driven, very, just very low speed.
Biden was energetic, energetic.
I would say almost too much at some points, coming off a bit
yelly at times, not to the level of like a Howard Dean scream, but he might
should have dialed it back a little bit at moments.
but it came across okay. Throughout the speech, he made jabs at Trump, but I don't think he ever
used his name. I don't think he ever used his name, and I didn't re-watch the whole thing,
um, but I feel like that was intentional. Like when he was talking about Roe, you know, he's like,
my predecessor's bragging about it. He's bragging about it. You know, he did this and da-da-da-da-da-da.
Never used his name, I don't think. If he did, it wasn't many times.
When he was talking about the economy, he used the phrase, the greatest story never told. Most people are
reading that as kind of a swipe at the media for what the administration perceives as a bunch of
negative coverage which is I mean there's some there's some truth in that
but I think it's also I think that was also an acknowledgement that the
Democratic Party has bad messaging has bad messaging you know when a headline
goes out that says you know six and ten blah blah blah and that's objectively
better than what it was they should be responding to that and they're they're
not you know our economy has a lot of income inequality in it there's a lot
of inequality built into the system so a lot of stats are always going to to have
that if it's better than it was before it should be something that the
Democratic Party could use to say, our policies are at least better than the Republicans but that
doesn't happen. I think his statement was talking about both. He talked about the lies that went out
about the election, said it was the greatest threat to democracy, going back to what worked
for him in 2020. And given the fact that a lot of those less than accurate
statements are still being repeated, it might still carry a lot of weight. I
would think it would, but we'll wait and see. When it comes to foreign policy,
there was quite a bit about that. He talked about Ukraine a bit and again
took a swipe at his predecessor when talking about what Trump said and saying,
well, I would tell Putin to do whatever he wanted.
Talking about NATO countries, allied countries, if they hadn't met their two
percent goal or whatever. And then he talked about Gaza. I am a little
disheartened when it comes to that topic right now. So I would watch the
State of the Union. I would watch the State of the Union. People have asked
about what they can do when it comes to Gaza and in particular asked if calling
their representatives would help. Yeah, yeah. That's not always something that is...
That's not always something that would move the needle. In this case it will.
especially if you live in a red district, if your representative is a Republican,
and I'm specifically talking about in the House, in the Senate they're a bit
more deliberate, so not as important, but if your representative is a Republican
in the House, it might matter because that's where the objection is going to
come from. You know, the Democratic Party is going to support this and hopefully
push further. But the Republican Party, they are, they have a tendency to be very
obstructionist right now in the House, so calling them might really help. And if it
was me, I don't care if you're an ardent atheist, I would be, I would be like, it's
the Christian thing to do. I do believe that that would, that would help. It
move the needle. Because some of what is going to occur, hopefully, is going to need to go through
Congress. It's not something Biden can do on his own, particularly if they do get a real peace deal,
because there's going to be funding involved with that, and it's going to have to get through the
house. So, yeah, I would do that. Overall, I think most people would be satisfied with
Biden's performance. Obviously, there's going to be issues with the various policy
aspects and stuff like that, but I think overall people are going to respond receptively to
it but there's still a whole lot to do when it comes to a situation that is
very pressing and one of the things that came up was you know it's going to take
time to get this doc that the peer you know what about and that there were a
whole bunch of suggestions yeah all of it in the meantime all of it that there
isn't time. There isn't time. It has to be now. I mean, everything, you know,
somebody suggested using Chinooks. Yeah, whatever it takes. Whatever it takes.
This is the moment where it's not rhetoric. It's, you got to pull out all
the stops. You have to pull out all of the stops to get the food and medicine
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}