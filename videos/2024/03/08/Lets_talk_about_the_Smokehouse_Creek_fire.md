---
title: Let's talk about the Smokehouse Creek fire....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=WiJRARrmQJE) |
| Published | 2024/03/08 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the Smokehouse Creek fire in Texas, the largest wildfire in the state's history, currently 74% contained.
- Texas A&M Forestry Service investigated and found both the Smokehouse Creek and Wendy Deuce fires were caused by power lines.
- XL Energies is allegedly tied to the Smokehouse Creek fire, with a lawsuit claiming an improperly maintained pole contributed to the fire.
- XL Energy states that some of their equipment appeared to be involved in an ignition, but they deny negligence in maintaining their infrastructure.
- Encouraging those impacted by the Smokehouse Creek fire to submit claims for property damage or livestock loss, despite lack of knowledge on the claims process.
- Noting a trend where wildfires are increasingly originating from poles, suggesting a need for the U.S. to address this issue due to the scale of damage caused.

### Quotes

1. "XL Energy disputes claims that it acted negligently."
2. "One of the big questions that always arises after a fire starts is how did it start, right?"
3. "At some point, given the amount of damage that's being caused, checking into this This might be something that the U.S. wants to put on its to-do list."
4. "Encourage people impacted by the fire to submit a claim."
5. "Have a good day."

### Oneliner

Beau addresses the Smokehouse Creek fire in Texas, investigations linking power lines to the fires, and the need for potential U.S. action due to increasing wildfire origins from poles.

### Audience

Texans, wildfire victims, environmental advocates.

### On-the-ground actions from transcript

- Submit a claim for property damage or livestock loss caused by the Smokehouse Creek fire (suggested).
- Advocate for stricter regulations on power line maintenance to prevent future wildfires (implied).

### Whats missing in summary

Detailed information on XL Energies' response and actions taken to prevent future incidents.

### Tags

#Texas #Wildfire #PowerLines #XLenergies #FirePrevention


## Transcript
Well, howdy there, internet people, let's bail again.
So today we are going to talk about
the Smokehouse Creek fire.
That's a fire in Texas,
if you haven't been following the story.
It is the largest wildfire in the history of Texas.
Currently, at time of filming, it is 74% contained.
Now, one of the big questions that always arises
after a fire starts is how did it start, right?
That's what people wanna know.
Texas A&M Forestry Service, they did an investigation.
They looked into Smokehouse Creek and Wendy Deuce,
which is another fire that hasn't gotten the attention
that Smokehouse Creek has.
They determined that both of them were caused by power lines.
Now there's no word on the company involved with the
Wendy Deuce fire, but it appears that the Smokehouse
Creek fire had something to do with XL Energies.
There's a lawsuit that alleges that an improperly
maintained pole fell, and that that had
something to do with it.
XL Energy, they say that some of their equipment, quote,
appear to have been involved in an ignition.
They go on to say, Xcel Energy disputes claims
that it acted negligently in maintaining and operating
its infrastructure.
However, we encourage people who had property destroyed
by or livestock lost in the Smokehouse Creek Fire
to submit a claim.
Now, I know absolutely nothing about their claims process.
I don't know how it works, so I would check into it
before signing up for it type of thing, but given the fact that I know that we have people
watching this that are impacted by it, I figured that was information that we should get out.
When we're kind of talking about this, remember this is big. We're talking about
livestock in the thousands. I want to say 500 or more structures. This is huge.
And one of the things that I've noticed and I don't know if this is just me
seeing it more often or there is some trend. I haven't really looked into it
that much, but it seems as though the source of wildfires is becoming poles
more often. And again, that may just be me observing it that way and that not be
backed up by fact. And it's not just these. This has occurred over and over
again in different states not necessarily linked to this company. But
at some point, given the amount of damage that's being caused, checking into this
This might be something that the U.S. wants to put on its to-do list.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}