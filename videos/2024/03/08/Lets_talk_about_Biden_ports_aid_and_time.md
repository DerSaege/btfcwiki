---
title: Let's talk about Biden, ports, aid, and time....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=AJO6ilk8rNw) |
| Published | 2024/03/08 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Updates on Project Rebound at California State University, Northridge, closing and raising $70,000 to help formerly incarcerated individuals with reintegration.
- Biden expected to announce seaborne operations to get aid into Gaza quickly.
- Complications in using big ships due to Gaza lacking a port for them.
- Constructing a pier to avoid the complication of transferring aid from big ships to little ships, allowing more aid to get in.
- Concerns raised about the time it will take to construct the pier.
- Urgency in delivering food to Gaza due to the immediate need.
- Biden administration opting to avoid putting American boots on the ground in Gaza.
- Possibility of airdrops as an alternative method, albeit not the most efficient.
- Issues with ground routes due to interruptions by Palestinian forces.
- Likelihood of uncomfortable questions arising for Israeli politicians if hunger situation worsens.
- Comparisons drawn to the Mission Accomplished banner incident and the effectiveness of current operations.
- Urgent call for action to prevent famine and address the immediate crisis.

### Quotes

1. "Something has to be done now."
2. "Out of time."
3. "It's time that doesn't exist."
4. "The Seaborne operation, it is a great idea."
5. "Everything else has to come second to that."

### Oneliner

Beau provides updates on Project Rebound and urges urgent action to deliver aid to Gaza, raising concerns about complications and time constraints.

### Audience

Community leaders, activists.

### On-the-ground actions from transcript

- Contact California State University, Northridge to support Project Rebound (suggested).
- Advocate for swift and efficient aid delivery methods to Gaza (suggested).
- Organize efforts to raise awareness about the urgent need for food and aid in Gaza (implied).

### What's missing in summary

The detailed context and emotional urgency conveyed by Beau in the full transcript.

### Tags

#ProjectRebound #SeaborneOperations #AidDelivery #GazaCrisis #Urgency


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about ports
and seaborne deliveries and piers and all of that stuff.
And we're going to run through it and talk about timing
and talk about questions that are going to arise
and just kind of go through things.
But before we get into that, a quick update
on what has been going on
as we enter the final few hours of it,
assuming this goes out when I think it will,
it looks like Project Rebound,
which is a project out at California State University,
Northridge, is closing and on raising $70,000.
This is a project that helps formerly incarcerated people
navigate reintegration, and has a really good success rate.
Last year, when we did this and sent people to it, it helped them raise about 23,000.
So this is huge. The link will be down below, hopefully.
Okay, so Biden is expected to announce that the United States is going to conduct those seaborne operations that we were
talking about,  getting stuff in from the sea. When we talked about it I said you know it's
super complicated but they can do a whole lot more faster if they go this
route and I said that the main objection would be American boots on the ground. So
what what's happening? It looks like they want to do it and avoid the
complications by constructing a pier because Gaza doesn't have a port that
can handle the big ships. Remember we were talking about how that it would go
from big ship to little ship and they're trying to avoid that complication
which would allow way more, way more aid to get in. I mean we have massive amounts
enough, right, enough to actually matter. There are problems. They're saying, well
this will take weeks. When they say weeks, it sounds like two, you know, maybe
three. Unless I am grossly underestimating their capabilities here, it's way longer
than that. It's longer than that and it's time they don't have. Now if this was
ordered way before it was announced that's one thing but we don't know that
and they don't have time they don't have time they do not have those weeks this
should still be done I'm all in favor of this because this will actually solve
the problem but in the meantime something has to be done something has
to be done to get food into Gaza. As far as the objection, American boots on the
ground, yeah, there won't be. That objection apparently occurred and the
Biden administration does not have the appetite to put American boots on the
ground, which is probably a good idea. Okay, so they can scallop the airdrops.
That's one thing. Not the most efficient way of doing it, but that's an option.
They're saying that they can't do this stuff on the ground, the ground routes,
because the Palestinian forces are interrupting that. When you are talking
about hunger. Once it hits that tipping point, it gets worse and worse and worse
real quick. When that footage starts to come out, the idea that it was
Palestinian forces disrupting it, that's going to prompt very uncomfortable
questions for the Israeli politicians and not the uncomfortable questions that
have been coming from the international community, it's going to prompt uncomfortable questions
from people who are in support of this operation.
Remember when we were talking about the Mission Accomplished banner?
They hung up a Mission Accomplished banner in the north, and I compared it to the U.S.
doing that.
That's where this is happening.
the, if it is out of the realm of possibility for convoys to be secured going into these
areas, how accomplished was the mission?
That line of reasoning is going to, it's going to cause questions from people who thought
this was a good idea because remember you had the Israeli IDF experts that
told them you know you've got to do something because we're going to have to
go back into these areas and they were ignored. It certainly seems like they
were ignored. I do not believe that the line of oh it's the Palestinian
forces, I don't believe that that's going to hold with the Israeli population as a reason.
Because there shouldn't be, according to what they've been saying, there shouldn't be
enough Palestinian forces there to do this, to disrupt anything.
area is where it's mission accomplished. For the politicians involved across the
board, you're out of time. There's nothing, there's no spin, there is no method
Instead of deflecting this, when this tips into famine and that footage starts coming
out, the outcry is going to be deafening.
Something has to be done now.
The Seaborne operation, it is a great idea.
I thought it was a great idea last week or the week before when we were talking about
it, but it takes time to do that and it's time that doesn't exist. The food, the
aid, the medicine, it's got to start getting in and getting in in bigger
numbers. Everything else has to come second to that. Out of time. Anyway, it's
It's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}