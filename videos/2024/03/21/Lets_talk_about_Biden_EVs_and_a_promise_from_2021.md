---
title: Let's talk about Biden, EVs, and a promise from 2021....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Lku4KhyBpK8) |
| Published | 2024/03/21 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden's promise in 2021 to have a majority of new cars in the US be electric by 2030.
- Ongoing talks between the Biden administration and the automotive industry on emissions regulations, not a ban on any cars.
- Projections suggest 56% of new cars will be electric between 2030 and 2032, reducing seven billion metric tons of carbon dioxide emissions.
- The new rules will impact various vehicle categories, but no mention of semi trucks.
- Legal challenges are expected, but the rules focus on increasing emission standards, which may help them withstand challenges.
- Automotive industry pushback wanting more time to comply, with the requirement to start by 2027.
- Beau hopeful that Biden will fulfill this promise, even though he was initially skeptical.
- Transition to majority EVs could pave the way for further progress.
- The US's approach is less stringent than European nations, but there is more room for legal challenges.
- Overall, Beau sees this as a positive step in the right direction.

### Quotes

1. "If the projections are right, he will fulfill a promise that I was hopeful for."
2. "My understanding is that they have to start putting it together by 2027."
3. "It's not as strict as a lot of European nations, but there's more ability to take the administration to court here."

### Oneliner

Biden's plan for a majority of electric cars by 2030 faces industry pushback but promises positive environmental impact and legal resilience.

### Audience

Policy advocates, environmentalists

### On-the-ground actions from transcript

- Advocate for stricter emission standards in your local area (implied)
- Stay informed about the progress and challenges of transitioning to electric vehicles (implied)

### Whats missing in summary

The transcript provides insights into Biden's ambitious plan for electric cars by 2030, industry reactions, legal challenges, and the potential environmental benefits.

### Tags

#Biden #ElectricCars #Emissions #EnvironmentalImpact #PolicyChange


## Transcript
Well, howdy there Internet people, it's Beau again.
So today, we are going to talk about a promise
that Biden made back in 2021.
Something he said he wanted to do
that I was super supportive of,
but also kind of skeptical that he'd be able to do it.
And it deals with his statement that by 2030,
majority of new cars in the United States would be electric for I think more
than a year. There have been ongoing discussions between the Biden
administration and the automotive industry. New rules are coming out about
emissions. So it's not a ban on any type of car or anything like that. It is
strengthening emission requirements.
The projections say that a majority of new cars
will be electric in 2030, 56% between 2030 and 2032.
It will reduce by the projections
seven billion metric tons of carbon dioxide emissions.
Hey, look, we used metric for something
over the next 30 years.
It looks like it impacts basically every category of vehicle,
car, SUV, all of that stuff.
The only thing I didn't see was like semi trucks.
So these are the new rules.
There are certain to be legal challenges
from a whole bunch of places.
But the way it's been written,
I feel like they'll withstand the legal challenges
because it's not a ban.
It's not anything like that.
It's increasing emission standards.
The way they're going about it
kind of shields them a little bit
when it comes to legal challenges.
Now, obviously there is some pushback
from the automotive industry itself.
They wanted a, basically a longer time
to start getting this together.
My understanding is that they have to start
putting it together by 2027, and keep in mind,
these aren't Star Wars dates anymore.
These are right around the corner.
Um, so, the new rules will come out.
There will be legal challenges.
They look pretty well positioned to defeat those challenges.
If the projections are right, he will fulfill a promise that I was hopeful for.
That's one of those things where I wanted him to succeed.
I just didn't really expect him to.
Once this occurs, once a majority of the EVs, or a majority of cars are EVs, it's going
to make it easier to push it even further.
So I mean, this is getting on the right track.
It's not as strict as a lot of European nations, but there's more ability to take the administration
to court here.
So that's where it stands.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}