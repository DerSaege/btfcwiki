---
title: The Roads to the Rule of 3s....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=-hyEHeRTb3A) |
| Published | 2024/03/21 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Providing a few quick questions for Thursday, acknowledging voice strain and hinting at potentially not releasing all four videos due to it.
- Addressing a viewer who questioned the severity of the situation in Gaza, particularly regarding food aid, and the imperative need for assistance.
- Explaining the critical situation in Gaza where more than 210,000 people are in catastrophic food conditions, underscoring the urgency for immediate aid access.
- Emphasizing the importance of changing the aid situation in Gaza to prevent a worsening crisis, with the potential for a situation far worse than what is currently projected.
- Clarifying the role of clans in Gaza, mentioning their focus on aiding their own networks and potential involvement in facilitating aid distribution.
- Detailing the dynamics between Gaza clans, their interactions with Hamas, and their stance on being a substitute government.
- Positing that the Gaza clans are likely to assist with aid efforts but are not inclined to confront Hamas or act as a replacement government.
- Addressing concerns about the Associated Press's future after some publishers opted to discontinue using their services, expressing confidence in AP's resilience and adaptability.
- Asserting that AP has diversified its revenue streams and suggesting that the recent developments could be part of negotiation tactics with publishers.

### Quotes

- "If the aid situation does not change in the next month, it's going to be bad. Way worse than a few hundred."
- "Their primary function is the preservation of their own."
- "AP will be fine; it'll be around. They're a global organization, they have a lot of information."

### Oneliner

Beau provides insights on the critical food aid situation in Gaza and the potential role of local clans, while also expressing confidence in the resilience of the Associated Press amidst recent changes in partnerships.

### Audience

Global citizens

### On-the-ground actions from transcript

- Contact NGOs to support aid efforts in Gaza (implied)
- Stay informed about the situation in Gaza and potential aid initiatives (suggested)
- Access news from AP directly through apnews.com.org (implied)

### Whats missing in summary

Insights on the nuances of clan dynamics in Gaza and potential implications for aid distribution and governance.

### Tags

#Gaza #AidEfforts #AP #AssociatedPress #ClanDynamics #HumanitarianCrisis


## Transcript
Well, howdy there, internet people, it's Beau again,
and welcome to The Roads with Beau.
Today, we're just gonna do a few real quick questions
for Thursday, because I know y'all like to have something
at this point in time, and I'm gonna try to save my voice,
at least try to make it to the weekend.
I'll go ahead and tell you now,
you may not get all four videos this weekend,
because my voice is strained probably a little bit more
than y'all might be able to tell.
OK.
So we just have three questions, but they all
seemed like questions that probably
need a little bit of context.
One of them in particular, I think
you may need before Monday.
Okay, the first one, I've watched you for four or five years and I can only remember
you doing calls to action to appeal to representatives two or three times.
When you said there were links to contact our reps below, I knew you thought it was
serious.
I have a question though.
I've seen some people saying that it won't be that bad.
What exactly about the report makes you certain it will be more than a few hundred?
A few hundred is bad.
I'm just wondering what your thought process is.
Now, this is in reference the food situation, the aid
situation, in Gaza.
I've seen some people saying it won't be that bad.
They're wrong.
My guess is that it's already as bad as they think
it's going to get.
They just haven't seen it yet.
What exactly about the report makes you certain it will be more than a few hundred?
Again, I believe it's already a few hundred.
But what you have in that report is the idea that more than 200,000, I think 210,000, are
already in catastrophic conditions as far as food.
So that information combined with the rule of threes.
Three minutes without air, three days without water,
three weeks without food.
And remember, the three weeks without food
assumes that you have the water.
And it does not appear that they have enough of that.
If the aid situation does not change
and they can't really start getting aid
in to that particular area, it is going to spiral.
There's no way it can't.
They have to get the aid in.
The fastest way to do that is to open it up
and get the trucks in, just like the person from the World Food Program said.
They need full and immediate and unfettered or whatever, whatever the terminology was,
access to the north.
That's what it's going to take.
And I don't see another way around that.
World Central Kitchen, they're doing what they can. The airdrops are doing what
they can, but that's not enough. Not when the situation has progressed to what
that report says. I don't have any reason to doubt that report. I don't have any
reason to second-guess those numbers. So if the AIDS situation does not change in
In the next month, it's going to be bad.
Way worse than a few hundred.
And yeah, you're right, I don't do calls to action.
I put out information and I believe that people should lead themselves and do what they feel
is right with that information. If it's something where I think it can move the
needle, then yeah. And in this case, I do believe that it might help. It might help
get things open. It might put a little bit more pressure. Getting that food in
their matters.
Can you explain whether the clans of Gaza will confront Hamas, help with aid, be a substitute
government?
This is the information I think you're probably going to need before this weekend because
there's already been there's already been some movement here. If you don't
know inside Gaza there's, see that term has such a negative connotation in the
United States, can, like in Appalachia, that there's networks of, family networks
for lack of a better term, it's not exactly that, but close enough, and they're
big and they have power. Will they confront Hamas? There are skirmishes,
there are intense situations that develop with these various networks and
Hamas, that not just is it going to happen in the future, it's already
happened in the past. In fact, there was a dust-up
sometime in the last week or so, maybe two weeks.
That occurs. Are they going to confront Hamas
in an attempt to remove them? No.
It's not what they're for. Will they help with aid?
Yeah, probably. Probably. As long as that aid
does not require them to cooperate
directly with Israel.
That's kind of a line they won't...
Well, in the north they might because of the situation.
But generally speaking,
that is not a line they cross.
Will they be a substitute government? No, and they have no interest in that. That's
not what they are.
They are more about
taking care of their own,
They're networks. They're people.
They're not
about
being administrators. They have their own kind of,
I don't know if you want to call it a law, but their own culture.
Even with how they interact with the other networks.
You know, because sometimes they go at each other
occasionally as well.
They are not something that would be used as a substitute for a government, and I would
hope that the West understands that if they were to attempt to do that, it would be perceived
as an incredibly cynical method of dividing them, and I believe it would provoke a backlash
That is not what they are, it's not what they want to do, it would be
crossing lines that they don't cross. Even those that are not in favor of Hamas,
and there are some, the lines extend beyond that. It's not because they're not,
I mean they are political, but not in that way.
Their primary function is the preservation of their own.
Would they help with aid?
Yeah, in fact I think they are.
I'm pretty sure that's already occurring to some degree and they may even do it more.
It would not surprise me if some of the NGOs, the non-government organizations, actually
used them because they have their own arms if they use them to secure stuff while it's
being moved into certain areas.
And that might be a decent move there as long as it's not with Western entities, as long
as it's not something that could be perceived as collaborating with Israel or collaborating
with the United States.
They would probably be very open to that, especially if it's them moving it into their
area. And I don't foresee a confrontation with Hamas. This is something that's probably
going to come into play over the next five or six days in a big way as they try to really
start figuring out alternatives for getting food in. Yeah, I can see them helping with
aid, not a substitute, not going to confront. But again, not confront is they're not going
to confront on behalf of the West. They may confront on their own terms, but not for anybody
else. Any attempt to use them in that way, I think it would backfire in a big way. That's
not what they are. And then the third question, I know you like AP and I've
grown to like them too. I was wondering if you think they'll make it after USA
Today stopped using them. Okay, so AP, the Associated Press, a couple of publishers
that have a whole lot of outlets have decided to stop using their service and
And this has prompted a whole lot of
conversation and rumors and stuff like that. Will AP make it?
Yeah, yeah. The Associated Press,
they're a, it is a renowned organization.
Like they get a Pulitzer almost every year. They got an Academy Award this year
for a documentary.
They've been around a really really long time. They have access to a lot of good
information
and they use it. They saw the trends early when it came to newspapers and
they adjusted. There was a time when if the AP lost a whole bunch of outlets at
once, yeah, it would be a big financial hit. From what I understand, if they lost
every single newspaper in the United States, it would account for less than
15 percent of their revenue. So it's that's not where they make their money
anymore. They have other services that they provide and if you're worried about
still being able to access their content you can get it from apnews.com.org
AP News. And it's just an advertiser based news outlet. So yeah, I
definitely think they'll make it. There's also a part of me that believes this is
actually part of a negotiation thing because the contract with these
publishers is it's not expired it's coming up on being expired and I think
this may be a way for those publishers to position to pay less because some of
them are not doing well economically some of the outlets aren't so it may be
that or what they said is true and they want to invest in their own newsrooms
which I mean that can't be bad you know if they're going to actually start and
bring back the old school newsroom that would probably be very helpful but AP
will be fine it'll be around that's that's not a worry yeah they are they're
They're a global organization, they have a lot of information, so yeah, so there we
go.
A little more information, a little more context, and having the right information will make
all the difference.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}