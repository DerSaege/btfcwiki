---
title: Let's talk about water infrastructure and security....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=T2m5tFLTr7I) |
| Published | 2024/03/21 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Biden administration and the EPA are urging governors to protect water infrastructure against cyber attacks.
- Basic cybersecurity measures like resetting default passwords and updating software are critical.
- The U.S. has around 150,000 water systems that need protection, making it a challenging task.
- Iran allegedly accessed a system to display anti-Israel messages, while China is reportedly finding vulnerabilities in systems for potential disruption.
- Despite the alarming nature of the warnings, there have been no disruptions to the water supply as a result of these cyber activities.
- The messaging to governors is proactive rather than reactive, aiming to prevent potential cyber attacks on water systems.
- Cyber warfare is increasingly recognized as a significant form of response short of war.
- The current proactive security measures are similar to strategies employed during the Cold War, which were never utilized.
- While attacks today may have lower impacts, there is a higher likelihood of them being used compared to the Cold War era.
- The messaging to governors serves as a precautionary measure for potential cyber threats in critical infrastructure.

### Quotes

1. "Anybody who's familiar with this type of security understands it's difficult at best."
2. "This is simply good preventative security presented in a direct way."
3. "Cyber warfare is becoming a very accepted form of response short of war."
4. "It's worth noting that during the Cold War, that messaging, that training, that proactive security stuff went on for decades and was never used."
5. "This messaging that went out to the governors is proactive, not reactive."

### Oneliner

The Biden administration and EPA are proactively warning governors about cyber threats to water infrastructure, advocating for basic cybersecurity measures to prevent potential attacks.

### Audience

Governors, Water Authorities

### On-the-ground actions from transcript

- Ensure cybersecurity measures like resetting passwords and updating software (suggested)
- Allocate necessary funding for cybersecurity upgrades (implied)

### Whats missing in summary

The full transcript provides a detailed insight into the proactive approach of the Biden administration and EPA towards safeguarding water infrastructure from cyber attacks, aiming to prevent potential disruptions.

### Tags

#BidenAdministration #EPA #CyberSecurity #WaterInfrastructure #Governors


## Transcript
Well, howdy there, internet people.
Let's go again.
So today we are going to talk about
the Biden administration, the EPA,
the national security advisor,
and their message to the various governors
when it comes to your water,
and what's going on,
and how that information is going out.
Okay, so the EPA and the national security advisor
have reached out to the various governors and they've basically said hey
y'all kind of need to get on the ball when it comes to protecting water
infrastructure against cyber attacks and this includes both drinking water
wastewater just across the board and part of the conversation included this
Even basic cybersecurity precautions, such as resetting default passwords or updating software
to address known vulnerabilities, are not in place and can mean the difference between
business as usual and a disruptive cyber attack. Okay, so first things first. You're talking about
150,000 or so different systems because of the way the United States is distributed in
In this regard, it's not one system, it's a bunch, and they all have to be protected.
Anybody who's familiar with this type of security understands it's difficult at best.
Now because of the way this went out, you can expect a whole bunch of very alarmist
stuff, especially because they talked about something that Iran did and
something that China may be doing and so on and so forth. There's gonna be a lot
of fear-mongering about this. So let's start with this. Iran was allegedly
successful. I believe they're saying they didn't do it, although I mean it
certainly appears to have been wrong. Back in November, they gained access to a
system and used the equipment to display messages that were quote anti-Israel.
China, the allegations there are that China is pre-positioning. They're
finding vulnerabilities in different systems in case they ever needed to disrupt US water
supplies during a conflict.
It is important to note as all of the coverage starts to ramp up of this, that at no point
in time have any of these operations actually disrupted or hurt the water supply.
This is a preventative thing.
The tone of it is very direct.
Hey, you need to get on the ball about this.
This is important.
Make sure you allocate the funding.
Make sure that the stuff gets updated.
Change the passwords, you know, so on and so forth.
It's very direct in that regard because it could be serious, not because there has been
a serious impact.
This is simply good preventative security presented in a direct way.
It's not indicative of anything happening tomorrow.
I think that's important to stress, that this is a proactive thing not reacting to something
that is damaging or disrupting the systems.
Okay, so that's what's going on with that.
I'll go ahead and say, you can expect very similar messages
about other forms of critical infrastructure.
Cyber warfare is becoming a very, it isn't becoming,
it is a very accepted form of response short of war.
So it's something that's going to have
be addressed more often and it's going to be in the headlines more often. Again,
it's not indicative of something happening tomorrow. This is one of those
things a lot like during the Cold War where you know you had all of the all of
the announcements to be ready for this and this is how you build this and this
This is what happens if there's this kind of strike and so on and so forth.
It's the same kind of stuff.
It's worth noting that during the Cold War, that messaging, that training, that proactive
security stuff went on for decades and was never used.
attacks are much lower impact so there's probably a higher chance that at some
point it will be used. But this messaging that went out to the governors is
proactive, not reactive. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}