---
title: Let's talk about a move on Biden's impeachment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=jq55shisAq8) |
| Published | 2024/03/21 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Representative Moskowitz wore a Putin mask to a House hearing, sarcastically suggesting impeaching President Biden.
- Moskowitz, a Democrat, challenged the ongoing impeachment inquiry, questioning the lack of evidence.
- Moskowitz proposed to immediately impeach Biden during the hearing, pointing out the lack of grounds for impeachment.
- He emphasized that the Democrats do not have enough votes in the House or Senate for impeachment.
- Moskowitz criticized the Democrats for misleading their base by discussing impeachment without solid evidence.
- He hinted at the Republicans doubling down on baseless claims in response to his actions.
- Moskowitz anticipated a week of intense debates and conflicts in the House following his bold move.

### Quotes

1. "Let's just do the impeachment."
2. "I just think we should do it today."
3. "It's all fake."
4. "They're lying to their base."
5. "It's a show."

### Oneliner

Representative Moskowitz sarcastically pushes for Biden's impeachment, exposing the lack of evidence and political theatrics in the House proceedings.

### Audience

House Representatives

### On-the-ground actions from transcript

- Challenge misinformation within political parties (implied)
- Stay informed and engaged with political proceedings (exemplified)

### Whats missing in summary

The full transcript provides a detailed account of a House hearing where Representative Moskowitz challenges the impeachment inquiry against President Biden, shedding light on the lack of evidence and political posturing.

### Tags

#HouseHearing #ImpeachmentInquiry #PoliticalTheatrics #LackOfEvidence #PartyPolitics


## Transcript
Well, howdy there, internet people, it's Bob again.
So today we are going to talk about
the US House of Representatives,
in particular, Representative Moskowitz,
and something that occurred during one of the hearings today,
looking into the possibility of impeaching President Biden,
and an incredibly, incredibly public lesson in sarcasm
them and calling a bluff. Okay, so Moskowitz apparently woke up on the
incredibly funny and sarcastic side of the bed today. Walking to the hearing, he
decided to wear a Putin mask, like a Halloween mask of Putin going to the
hearing. Then during the hearing, he basically called to just go ahead and impeach Biden.
It is worth noting that Moskowitz is part of the Democratic Party. He said, let's just
do the impeachment. Why continue to waste millions of dollars of the taxpayers' money
if we're going to impeach because you believe you've shown he's committed a high crime
or misdemeanor. What are you waiting on? He goes on. They haven't proven he committed
a high crime or misdemeanor. Otherwise, we would call for an impeachment. And then it
gets really odd. I just think we should do it today. Let's just call for it. I'll make
Make the motion, Mr. Chairman, I want to help you out.
You can second it, right?
Like make the motion to impeach President Biden, go ahead.
There was no follow up to that.
And then he says they're never going to impeach Joe Biden.
It's never going to happen because they don't have the evidence.
This is a show.
It's all fake.
I mean, having looked into what they have as evidence, I would agree that is certainly
one way to get the point across.
It's worth noting that from here he went on to say that they're lying to their base.
That they're just stringing them along, saying they're going to impeach them, but they never
intend to because they don't have the evidence.
I mean, that is certainly the appearance of things.
That is certainly how it appears.
It's also worth remembering, and this is something that was pointed out in this moment of very
blunt honesty about how he feels, is that they don't have the votes.
Even if they were to move forward with an impeachment, they don't have the votes on
the floor.
need a massive amount of evidence that we haven't seen so they don't have the
votes in the House and they really don't have the votes in the Senate either. So
I mean he's right it's a show and this was certainly a way of illustrating
that to the public, I would imagine that this will induce Republicans to double down and
make more claims, and odds are those claims will be, let's just say, not entirely backed
up by evidence that anybody's seen.
Again, the claims that they're looking into primarily are the claims that just happen
to be used in an exercise about journalism years ago on the channel.
And to this day, nothing has changed.
There is no, it's not there.
So I would imagine that there's going to be a lot of commentary, it's probably already
started by the time you all watch this, a lot of commentary and then a lot of fireworks
over the next week in the House, because their Republican Party is going to have to try to
recoup from this, because it's going to go out pretty widespread, and it's going to
be devastating to their position.
Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}