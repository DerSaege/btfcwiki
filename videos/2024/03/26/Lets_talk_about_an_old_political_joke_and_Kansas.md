---
title: Let's talk about an old political joke and Kansas....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=NJEy4Mro3B8) |
| Published | 2024/03/26 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains an old joke about American politics involving lawmakers wearing jackets with logos like NASCAR drivers to show their sponsors.
- Mentions the enduring joke's origin in the 90s, reflecting the reality of money in American politics.
- Talks about a new requirement in the Kansas House where bills will now include the name of who asked for the bill, whether a lobbyist, lawmaker, or private citizen.
- Considers this requirement as the closest thing to the NASCAR jacket idea becoming a reality.
- Expresses curiosity about how long this requirement will last and if it will spread to the Senate and other states.
- Notes that if the House in Kansas is implementing this requirement, there’s no reason for the Senate not to do the same.
- Speculates on the potential positive impact of this transparency on American people understanding who is behind proposed bills.

### Quotes

1. "There are a lot of jokes about American politics because, I mean, sometimes you have to laugh or cry, right?"
2. "It is an interesting idea. I'm very curious how long it will last, and whether or not it will spread."
3. "Anyway, it's just a thought, y'all have a good day."

### Oneliner

Beau explains the enduring joke about lawmakers wearing sponsor-filled jackets in American politics and how a new bill requirement in the Kansas House may bring transparency to legislation.

### Audience

Politically engaged citizens

### On-the-ground actions from transcript

- Contact legislators to advocate for similar transparency requirements in your state (implied).

### Whats missing in summary

The full transcript provides a detailed exploration of the enduring joke in American politics and the potential impact of increased transparency in legislation.

### Tags

#AmericanPolitics #Transparency #Legislation #GovernmentReform #NASCARJackets


## Transcript
Well, howdy there, Internet people, it's Beau again.
So today, we're going to talk about a joke, an old joke,
about politics and Kansas and an idea that might catch on.
There are a lot of jokes about American politics
because, I mean, sometimes you have to laugh or cry, right?
There's one that is enduring.
It has been around a really, really long time.
I actually tried to track it back to where it started.
I know it goes back to the 90s.
Odds are there was some version of it before then.
And it's the idea that lawmakers, people in Congress,
they should wear jackets.
And those jackets should have logos on them, like NASCAR.
They're sponsors.
the people who gave them money, lobbyists, all that stuff.
So, the American people, well, they get to know
who really wants the legislation,
where the votes really come from, and it does.
It strikes at the heart of the dysfunction
that occurs in American politics
because there's so much money in it.
And it's a joke. It's always been a joke. There have been
like attempts to do something similar, proposed, but it's always been a joke.
Apparently not in the house in Kansas, not in the Senate, but in the house.
They don't have to wear a jacket with logos on it,
but there's a new requirement for bills. The bill will have the bill number, you
know, the way they all do, and it will have the legislative sponsor the way they
all do, and in the House in Kansas it will have who asked for the bill, who
asked for that piece of legislation, whether it be a lobbyist asking for a
client or another lawmaker, maybe a private citizen, whoever, the name goes on the bill.
Realistically, that's probably as close as we're ever going to get to the NASCAR jackets.
It is an interesting idea. I'm very curious how long it will last, and whether or not it will
spread. The Senate has no reason not to do it at this point because generally
speaking you have a bill that gets introduced and then there's one in the
Senate, something like that, so if they're going to put it on there in the House
they might as well do it in the Senate. And if the whole state of Kansas is
doing it, well maybe it catches on. It is definitely a strange development.
But it might do a whole lot of good if that spread and the American people got
to see who was actually behind some of the bills they get proposed up in DC.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}