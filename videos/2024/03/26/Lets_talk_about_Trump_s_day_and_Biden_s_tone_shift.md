---
title: Let's talk about Trump's day and Biden's tone shift....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=sSMBJUKE5Ik) |
| Published | 2024/03/26 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's bond was successfully argued to be reduced to 175 million in the New York civil entanglement, with 10 days to post it.
- Another New York entanglement, known as the hush money case, is set to begin on April 15th, with Trump planning to appeal the decision.
- Trump held a press conference that was described as unusual and possibly unhinged, making several errors during it.
- Trump mentioned not having elections in the middle of a political season and wanting to bring crime back to law and order.
- The day for Trump started with some good news but progressively worsened.
- Biden-Harris HQ released a statement criticizing Trump's actions and character, stating America deserves better.
- The statement marked a change in tone for the Biden campaign, possibly aiming to win over moderates and build a coalition.

### Quotes

- "Donald Trump is weak and desperate, both as a man and as a candidate for president."
- "America deserves better than a feeble, confused, and tired Donald Trump."
- "He is uninterested in campaigning outside his country club."
- "Trump is, to use the word, unprecedented."
- "It's quite the statement from Brandon there."

### Oneliner

Trump's eventful day included reduced bond, appeal plans, and an unusual press conference, while Biden's camp slammed him for being weak and desperate, marking a shift in tone.

### Audience

Political observers

### On-the-ground actions from transcript

- Share and amplify the Biden-Harris HQ statement criticizing Trump's actions and character (implied)

### Whats missing in summary

Full context and nuances of the events and statements discussed in the transcript.

### Tags

#Trump #Biden #Campaign #Politics #Elections


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we are going to talk about Trump's day,
how it went.
And we're going to talk about a new tone
we're seeing from the Biden campaign,
something we haven't seen before.
So we'll just run through what occurred.
Okay, so starting off in the New York civil entanglement,
Trump was successful in arguing that his bond should be reduced. It was reduced to
175 million and he has 10 days to post that. In the other New York entanglement,
what's being referred to as the hush money one, that is now slated to begin
on April 15th. Trump promised to appeal that decision. I'm not exactly sure how
he's going to do that. That would take some unique maneuvering. So after that he
He gave a press conference, and it was unusual.
People have referred to it as unhinged and a whole bunch of different things.
I don't know all about that, but it was definitely Trump.
In it, he made a number of mistakes, one of which was him saying that you can't have an
election in the middle of a political season.
Being generous, you can assume that he meant trial and didn't say that correctly.
Then he also said that he wanted to bring crime back to law and order.
Being generous, maybe he meant bring law and order back to crime-ridden areas.
So he didn't have a good day overall.
Started off with some good news and it just kind of went downhill from there.
And then Biden-Harris HQ, which is slightly social media rapid response part of the campaign
from what I understand.
They released a statement from the campaign.
Donald Trump is weak and desperate, both as a man and as a candidate for president.
He spent the weekend golfing, the morning comparing himself to Jesus, and the afternoon
lying about having money he definitely doesn't have.
His campaign can't raise money.
He is uninterested in campaigning outside his country club, and every time he opens
his mouth, he pushes moderate and suburban voters away with his dangerous agenda.
America deserves better than a feeble, confused, and tired Donald Trump."
And it's got the Biden-Harris logo at the bottom.
Yeah that's a change in tone right there a little bit.
What is unique coming from the Biden campaign?
I don't know if we're going to see more of that type of statement.
It probably depends on how it resonates with people.
Again, for the Biden administration, they're trying to build that coalition.
This might win over some moderates.
And I'm not sure, I'm going to be honest.
It's not the type of campaign that Biden is typically associated with.
But Trump is, to use the word, unprecedented.
So maybe the campaign is looking at a shift in tactics, but that's quite the statement
from Brandon there.
Anyway, it's just a thought.
Y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}