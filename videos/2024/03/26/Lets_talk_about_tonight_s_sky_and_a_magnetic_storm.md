---
title: Let's talk about tonight's sky and a magnetic storm....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=CnuAKWTt_kc) |
| Published | 2024/03/26 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces the topic of a geomagnetic storm, specifically a severe one hitting Earth from a coronal mass ejection.
- Despite the severe storm, the public is reassured that no adverse impacts are anticipated, and no action is necessary.
- The disturbance in the Earth's magnetic field may cause minor disruptions, such as spotty GPS signals, but nothing catastrophic.
- Beau mentions that individuals in large sections of the United States, particularly in the Midwest, may have the rare chance to see the aurora, or northern lights, as a result of this event.
- The aurora may also be visible from places like Alabama, Northern California, the UK (especially Scotland), and some parts of Australia.
- This topic often attracts fearmongers due to its portrayal in disaster movies, but experts suggest that it will likely just result in pretty lights in the sky.
- Beau encourages people to go outside and witness this phenomenon if they are interested.

### Quotes

1. "The public should not anticipate adverse impacts and no action is necessary."
2. "According to the reports it seems certain that places in the Midwest will be able to see it."
3. "According to the experts you might see pretty lights in the sky."
4. "Y'all have a good day."

### Oneliner

Beau gives an informative PSA on a geomagnetic storm, reassuring the public of no adverse impacts while hinting at the rare chance to witness the aurora in various locations.

### Audience

Science enthusiasts, stargazers

### On-the-ground actions from transcript

- Go outside to potentially witness the aurora in areas like the Midwest, Alabama, Northern California, the UK (Scotland), and some parts of Australia (suggested).

### Whats missing in summary

Beau's calming and informative demeanor in discussing the geomagnetic storm and the potential to witness the aurora firsthand.

### Tags

#GeomagneticStorm #Aurora #PublicServiceAnnouncement #NorthernLights #SpaceWeather


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about
public service announcement.
We're gonna do a little PSA about something
that is occurring tonight, something that you might see.
And we will talk about the sky and the earth's magnetic field
and just kind of run through the information.
Because this topic has been used in a lot of movies that depict, well, I mean, kind
of the end of the earth, I want to start off with this.
The public should not anticipate adverse impacts and no action is necessary.
But it might be good to stay informed.
Okay, so what's going on?
A geomagnetic storm, a severe one, which is the second highest level, is hitting Earth.
It's from a CME, a coronal mass ejection, we've talked about them on the channel before.
It is causing a, quote, major disturbance in the Earth's magnetic field.
So they say, should not anticipate adverse impacts.
I mean, that's true when you're comparing it to all the disaster movies.
Your GPS may be a little spotty.
There are minor things that are probably going to happen according to the experts on it,
but the world's not going to stop.
Okay, so if you don't need to take any action, why are we even talking about it?
you might want to walk outside. Large sections of the United States and other
places and we'll go through them may be able to see the aurora, the northern
lights from where you're at, which that doesn't happen very often, you know. So
according to the reports it seems certain that places in the Midwest will
be able to see it. Some of the forecasts say parts of Alabama, so as far south as
Alabama, Northern California, outside of the United States it appears that it
will be visible from the UK, I guess particularly Scotland, and then on the
other side of things down south it should be visible from some parts of
Australia as well. So this is a topic that often gets people who like to
fear monger, it gets them going because there's movies about it. According to
the experts you might see pretty lights in the sky. It doesn't appear like it's
going to be a big deal, but it's worth mentioning,
especially if you want to go outside and look at it.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}