---
title: Let's talk about half the budget bills being signed....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=CbtVzDsNkLI) |
| Published | 2024/03/10 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau provides an overview of the six funding bills that were signed into law, totaling around $460 billion to carry parts of the government through the end of the 2024 budget.
- There are six more bills pending, with a deadline of March 22nd for negotiation, considered to be more challenging.
- In the House, the bills passed with 339 in favor and 85 opposed, with mixed party support.
- The Senate passed the bills with a vote of 75 to 22, with less drama compared to the House.
- Both Democrats and Republicans are claiming victory, with Democrats focusing on increased funding for social programs and Republicans on spending cuts.
- Despite the bills being signed, there are concerns about potential hang-ups and disagreements in the House.
- The Office of Management and Budget is resuming normal operations in anticipation of the signed bills.
- The bills' passage was anticipated, with little surprise once they made it through the House.
- Beau points out that the upcoming negotiations for the other six bills may face more significant challenges.
- The lack of a coherent vision among House Republicans could lead to further conflicts and impede larger policy initiatives.

### Quotes

1. "Both sides, both parties."
2. "They're super mad about what has happened."
3. "They made it through this one, albeit at the absolute last minute."

### Oneliner

Beau gives a detailed rundown of the signed funding bills, hinting at potential challenges ahead in negotiations for the remaining bills.

### Audience

Legislative observers

### On-the-ground actions from transcript

- Contact your representatives for updates on the pending bills (implied).
- Stay informed about the budget negotiations and their potential impact on community programs (implied).

### Whats missing in summary

Insights on the specific areas where spending cuts or increased funding will be directed in the signed bills.

### Tags

#FundingBills #BudgetNegotiations #Legislation #GovernmentFunding #PoliticalAnalysis


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about the budget,
the six funding bills that made it through and were signed.
If you've been following this coverage,
the short version of this video is exactly
what we expected to happen is what happened.
But in the sense of rounding out the coverage,
we'll just run through everything.
So today, as expected,
Biden signed these six funding bills.
These bills carry parts of the government through the end of the normal 2024 budget.
I want to say the total price tag is $460 billion and change, somewhere in there.
Keep in mind, there are still six more bills that have not gone through yet, and they have
until March 22nd for those. Generally speaking, those are viewed as the bills
that are harder to negotiate. So we're we're not actually out of the woods on
this one yet. Okay, so how did they get through? In the House, it was 339 in
favor, 85 opposed, with 83 Republicans voting against and two Democrats voting
against. In the Senate, 75 to 22. Not a lot of theatrics or drama in the Senate, a
little bit at the end, but nothing major. Again, more deliberative body. Okay, so
who won? Well, if you listen to them, both sides, both parties. The Democratic Party
is out there touting the increased funding for certain social programs.
The Republican Party is out there talking about spending cuts in other areas.
Generally speaking, politically speaking, every faction except for the Twitter faction in the
House is claiming victory. They're super mad about what has happened. They didn't
want this. They referred to it as surrendering on every Republican
priority. Again, showing that that influence is kind of decreasing.
decreasing. Okay, so this was expected late yesterday. The
office management and budget went ahead and said that they
were going to be resuming normal operations. Now keep in mind,
Biden didn't actually sign this until today. But this is one of
those things that was incredibly expected. There weren't a lot of
surprises once it made it through the house. Now again, six more that they have
to work out the details on by March 22nd and that's expected to be more
difficult and that's where that's where the real hang-ups might occur and the
the fighting might get a little more a little more pronounced even outside of
the House. But again, your main hang-up is going to be in the House. Republicans in
the House are not, they do not have a coherent picture on what they want. So
there's a lot of, there's a lot of fighting there that derails larger
policy initiatives. But they made it through this one, albeit at the absolute
last minute. So we'll see what happens with the other six. Anyway, it's just a
With that, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}