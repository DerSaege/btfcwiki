---
title: Let's talk about numbers, timelines, and plans....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=IcsqUUl2Ey4) |
| Published | 2024/03/10 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Providing an update on U.S. endeavors involving building a pier, seaport, and causeway.
- No American boots on the ground; aid reaches the causeway and is handed over to vetted partners.
- The timeline for aid delivery is 60 days, with a warning that if the situation worsens, they might not have that time.
- Urgent options include increasing airdrops, opening land routes, and finding quicker ways to deliver aid.
- A malfunctioning airdrop resulted in the death of five individuals, raising concerns about safety and effectiveness.
- Emphasizing the need for immediate action due to the dire situation and time sensitivity.
- Mentioning a joint effort by World Central Kitchen and Open Arms to deliver food via ship without the need for the pier.
- Acknowledging that while this effort is positive, more action is required as time is running out for those in need.
- Stressing the importance of exploring and implementing alternative options if the 60-day deadline is not viable.

### Quotes

1. "The situation there is dire. It needs to be addressed now."
2. "Even if the chute opens, you don't want one of these things landing on you."
3. "You're out of time, you're out of time."
4. "The 60 days, I don't believe it's there, I don't believe it's there."
5. "There has to be other options."

### Oneliner

Beau provides updates on U.S. efforts to deliver aid, stressing the dire situation and the need for immediate action as the 60-day deadline may not be feasible.

### Audience

Humanitarian organizations

### On-the-ground actions from transcript

- Support organizations like World Central Kitchen and Open Arms in their efforts to deliver aid (exemplified).
- Get involved in local initiatives to contribute to aid efforts (suggested).
- Advocate for faster and more efficient aid delivery methods (implied).

### Whats missing in summary

The full transcript provides detailed insights into the challenges and urgency of delivering aid in a dire situation, urging immediate action to address critical needs.

### Tags

#UrgentAction #AidDelivery #HumanitarianEfforts #CommunitySupport #DisasterResponse


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about some numbers,
we are going to talk about some timelines,
US endeavors, provide an update on that,
and just kind of run through that information
and then talk about other options
and then answer a question.
Okay, so starting off,
because the last time we talked about this,
A lot of this was unconfirmed and there was a lot of speculation going on.
As far as the U.S. Peer Plan, orders are cut and people are underway to go build it.
The general plan is to build a pier, seaport, it's being termed in different ways, and
causeway.
No American boots on the ground, the aid comes off, hits the causeway.
the time it hits the causeway on, it is in the hands of vetted partners, quote,
whatever that means.
I don't know.
Um, all of this is all fine and good.
The timeline is 60 days.
Biden recently said that another 30,000 lost was a red line.
If the reporting coming out about the food and water situation is accurate, I don't think they have 60 days.
It is getting, it is not getting, it is dire. It is dire.
dire. So what are the options? One, increase the airdrops tenfold. Two, get the land
routes open. Three, figure out a faster way to get it from sea to shore. It's
something to run in the meantime because I don't believe the 60 days exists for a
whole lot of people. And those three options, that is either or and all of the
above. The situation there is dire. It needs to be addressed now. I don't
believe the 60 days exist. Okay, so one thing that occurred, an airdrop
malfunctioned. The chute didn't open and it landed and killed five people. The
number one question about this that came in was, was it the US? My understanding
is no, but the next one could be. There is risk associated with this. When you go
back to that video, let's talk about airdrop questions. Talk about why,
before the drops ever happened, it was gonna be on the coast, it was gonna be in
fields, they might use a cemetery because it is safer for the living. It's a risk.
Malfunctions are rare. Malfunctions landing on people, it's even more rare but
it happens. It can happen. This is why in that video it was so important to
inundate that first day. That first day just tons literally tons and tons and
tons of food. I do not know if this played a part in the in what happened
with the five people but you have people running towards them because there's not
enough food. They have to get the food in. There are going to be more issues if
they don't. And to be clear, even if the chute opens, you don't want one of these
things landing on you.
There needs to be a lot more done and it needs to happen a lot faster.
Now ending on some good news, one question that came in is anything actually working
in getting to where it's supposed to be?
There is a joint effort by two non-profits, World Central Kitchen and Open Arms.
They have a couple hundred tons of food that is trying to make its way there right now
via ship.
My understanding is that this does not need the pier.
And I also, from what I understand, once they're done with that, they have like more loads
to bring.
It will help, it will help, but it's not enough.
You're out of time, you're out of time.
The 60 days, I don't believe it's there, I don't believe it's there.
So the other routes, the other options have to be explored.
If there is no temporary structure that's going to be thrown up in a matter of days
to get the food to shore, there has to be other options.
They have to be implemented because the reporting coming out, the 60 days isn't there.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}