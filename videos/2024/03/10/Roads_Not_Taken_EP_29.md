---
title: Roads Not Taken EP 29
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=LMDO3tt8enM) |
| Published | 2024/03/10 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Beau provides a weekly overview of events on March 10th, 2024, in episode 29 of The Road's Not Taken.
- Turkey offers to host peace talks between Ukraine and Russia, with mounting pressure on Ukraine to seek peace.
- Multiple countries are restarting funding for UNRWA in Palestinian areas.
- In Nigeria, gunmen take 15 children, eliciting concerns and potential Western responses.
- Sweden officially joins NATO, while the Middle East lacks a ceasefire deal.
- US news includes Biden defending uncommitted voters and controversies surrounding Trump and the GOP in Missouri.
- Taylor Swift's call to vote stirs predictable responses, and the US faces measles outbreaks and health concerns.
- Odd news features women taking a man's body to a bank drive-thru and the Pentagon debunking UFO sightings.
- Beau addresses questions on MRE fact-checking, Biden's efforts, personal growth, aiding Haiti, Republican endorsements, and US foreign policy.
- He shares insights on shifting US foreign policy and the usefulness of hobbies in emergencies.

### Quotes

- "Change is not an event, it's a process, it takes time."
- "You have to show them, and honestly, telling them doesn't do any good."
- "I want to see US foreign policy actually make morality something that actually factors into it on a deeper level."
- "The list of hobbies that are necessary in an emergency is just never-ending."
- "A little more information, a little more context, and having the right information will make all the difference."

### Oneliner

Beau provides insights on global events, US news controversies, personal growth, and emergency preparedness through useful hobbies. Change takes time, and informed action matters.

### Audience

Global citizens

### On-the-ground actions from transcript

- Contribute funds to aid Haiti (implied)
- Engage in hobbies that can be useful in emergencies, like radio operating (implied)

### Whats missing in summary

Beau's engaging delivery and nuanced perspectives on current events are best experienced in the full transcript.

### Tags

#CurrentEvents #GlobalNews #USPolitics #PersonalGrowth #EmergencyPreparedness


## Transcript
Well, howdy there, internet people, it's Bo again,
and welcome to the Roads with Bo.
Today is March 10th, 2024,
and this is episode 29 of The Road's Not Taken,
which is a weekly series
where we go through the previous week's events
and talk about news that was unreported, under-reported,
didn't get the context it deserves,
or I just found it interesting.
And then once we go through that,
we answer some questions from y'all
that were picked out by the team.
Okay, and starting off with, I hope you changed your clocks.
Okay, foreign policy.
Turkey is offering to host peace talks
between Ukraine and Russia.
There is mounting pressure on Ukraine to seek a peace.
Ukraine does not seem entirely receptive to this idea,
unless, of course, the peace involves Russia withdrawing.
Multiple countries are restarting their funding
of UNRWA, which is an organization that
is in Palestinian areas and provides a lot of aid
and infrastructure, stuff like that.
In Nigeria, and this is kind of still developing,
but 15 children were taken by gunmen.
This has happened in the past, and sometimes it has elicited Western response.
Sweden has officially joined NATO.
There is still no, I don't want to say no, progress.
There is no ceasefire deal in the Middle East right now.
And right now things seem pretty entrenched.
There are still conversations occurring, but we do not seem closer than we did a few days
ago.
Moving on to U.S. news.
Before we move on, something that isn't on here because I expected to have a video out
about it by now. There's a whole bunch of news involving US efforts in in Gaza
from the pier to the airdrops just a whole bunch of stuff that I have more
information on since the last video went out and I just haven't been able to get
it out yet but that should come out soon okay in US news Biden defended
uncommitted voters saying quote it's understandable why they feel that and
that's why I'm doing everything I can to stop it. Trump hosted Orban the
Hungarian autocrat at his club and that drew some pretty widespread controversy.
controversy. The GOP in Missouri is trying to remove a Republican candidate for governor
who is reportedly an honorary member of the Klan.
Sinema leaving has people once again wondering whether or not the Senate filibuster is going
to remain intact, whether or not it's going to be removed.
In cultural news, Taylor Swift told people to vote, or maybe people just believed she
told people to vote.
I didn't actually check to see if she told people to vote on Super Tuesday, but it doesn't
matter because the very predictable response came from the right, angry about somebody
telling telling people to vote. Again, now that I think about it, I put this note in
here and I never checked to see if she actually did it or somebody just created a meme and
the right believed it. In science news, measles outbreaks, plural, may mean that the US has
to revise its status as eliminated.
There are currently 15 or 16 states that have cases.
So there's that in 2024.
A new study says that microplastics in clogged arteries increase the chance of heart disease.
In odd news, two women in Ohio reportedly took a man's body to the bank drive-thru
to withdraw his money before dropping him off at a hospital.
The Pentagon says a spike in UFO sightings in the 1950s and 60s was the result of people's
seeing spy planes, basically, and advanced tech
that they were researching.
It also states, for the record, that it
has no record of any reverse engineered alien tech.
So there's that.
OK, so these are on to the questions.
Where's the MRE fact check video?
It's still up.
I haven't published it yet, honestly, with Gaza on the brink of famine, it does not seem
appropriate to fact check that right now.
there's enough food flowing in, it seems inappropriate to go through and say, no, this actually is
in date or something like that.
So hopefully, I will release that soon because hopefully, there will be food flowing in soon.
You seem to think Biden is actually trying.
Why don't you push back on those who say he's not?
I'm assuming this is, I'm assuming that's about, that that's about the Middle East.
The pressure can't hurt.
The pressure cannot hurt.
Whether or not I believe he's trying or not, the pressure doesn't hurt.
There is no way that that pressure ends up causing a negative outcome.
It's not like it can derail what he's doing.
It can only provide incentive to do it better or faster.
also in many ways helps what he's been doing because it gives him political cover when
talking internationally.
Because he can speak frankly and say, look, I have all this pressure at home.
You've got to do something.
In some ways, despite me believing that he is actually trying, I kind of wish there was
a little bit more.
To be honest, I don't see how it can hurt.
I'm a semi-reformed kind of guy.
I used to have some very, very bad opinions regarding race, gender, you name it.
And I worked through those, but I still fill them in me.
When I see someone of a different color, I unconsciously stereotype them.
And I still say some pretty sexist things without realizing until after I've said them.
It's a serious problem and it's cost me a friendship that I treasured dearly.
What do I do now?
How do I beat this?
How do you beat it?
You keep working at it.
Change is not an event, it's a process, it takes time.
You continue to work at it.
continue to read, obviously watch YouTube, watch YouTube videos that address wherever
it is, whatever it is you still need to work on, and learn about it.
The what do I do now, that sounds like it's about the recent friendship.
If you lost it because of these issues, you're not going to get it back until you change.
It's one of those things, you can't tell people you've changed.
You have to show them, and honestly, telling them doesn't do any good.
You have to show them.
working on it and then maybe you can rekindle that friendship but if it was
lost because of that reason you have to show them that that you've changed that
you're still working. Any thoughts on the Haiti thing? Anything Americans can do
to help it not turn super bad.
As far as the U.S. in Haiti, maybe contribute funds.
Two countries that should just not appear in Haiti unless there is no other alternative,
France, and the United States.
The situation there is not going well, and they may need help, but I don't know that
the U.S. is the country to help, to actually show up.
There's a history there and it just probably won't go well.
Do you think any Republicans will endorse Biden for president?
I'm sure some will.
I'm sure some will.
I don't know how many.
I actually saw another question like this, but apparently that's not the one that got
picked, pointing out a few times that it had happened and wondering if Republicans would
would basically endorse Biden to say, you know, I want the party of Reagan back, you
know.
I want the old GOP back, not the ultra-authoritarian version that exists today.
It's possible.
The question is how big the names are and whether or not it's actually going to sway
anybody.
Are our moves in Israel a step towards becoming the world's EMT instead of the world's
policeman?
I'd like to think so, but I don't.
That's what I would like to see US foreign policy become.
I would like to see US foreign policy actually make morality something that actually factors
into it on a deeper level and I would like it like to see it shift to the
world's EMT but I this seems more like a stopgap than anything else. I don't know
that this signals a significant shift in US foreign policy and as we move
into that near-peer world it will probably take it will probably take a
lot to make that occur. My town has a drone club that recently used their
quadcopters for search and rescue finding some kids that wandered off in a
wooded area. Are there other hobbies you can think of which can be useful in an
emergency. Tons. I mean, everything from using ATVs to off-roading in four-wheel drives to
rafting to canoeing to... I mean, this list just goes on and on. Rock climbing. It's
It's how it's applied.
The list of hobbies that are necessary in an emergency is just never ending, useful
in an emergency.
I could probably sit here and list off stuff for 15 minutes.
radio operators come in critical. It is a never-ending list. I think it would be easier
to list out your hobbies and then cross off the few that wouldn't be useful. When you're
talking about an emergency, especially just widely defined, it takes skills that oftentimes
oddly enough, are hobbies in the United States because the U.S. does have a relatively stable
environment for most people most of the time.
The hobbies tend to be things that go outside of that, and that's where your emergencies
are.
on the edges, and they're where the status quo comfort breaks
down.
So those hobbies are incredibly useful.
OK, and those are all the questions that they picked.
OK, so a little more information, a little more
context, and having the right information
will make all the difference.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}