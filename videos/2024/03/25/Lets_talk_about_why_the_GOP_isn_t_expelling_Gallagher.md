---
title: Let's talk about why the GOP isn't expelling Gallagher....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=6zX4l6-5xw4) |
| Published | 2024/03/25 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains why the GOP is unlikely to impeach Gallagher, a Republican representative from Wisconsin who is leaving on April 19th.
- Mentions the possibility of a special election to replace Gallagher if he leaves before April 8th.
- Clarifies that the Republican Party does not have the votes to expel Gallagher before he leaves.
- Notes that the House is on break until April 9th, making it logistically impossible to expel Gallagher before then.
- Stresses that a two-thirds majority is needed to expel Gallagher, which the GOP does not have.
- Emphasizes that even if the House were to reconvene, they still wouldn't have the votes to expel Gallagher.
- Points out that the Constitution specifies the requirement for a two-thirds majority to expel a member.
- Concludes by stating that Gallagher is likely to leave on April 19th, leaving the seat vacant.

### Quotes

1. "It's being pushed out as a piece of rhetoric."
2. "All signs point to him leaving on April 19th."
3. "Y'all have a good day."

### Oneliner

Beau explains why the GOP is unlikely to impeach Representative Gallagher, as they lack the necessary votes, leaving the seat vacant when he departs on April 19th.

### Audience

Political analysts

### On-the-ground actions from transcript

- None mentioned in the transcript.

### Whats missing in summary

Details on the potential implications of Gallagher's departure for the Republican Party.

### Tags

#GOP #Impeachment #Republican #Constitution #House


## Transcript
Well, howdy there internet people, it's Bob again.
So today we are going to talk about something
that a whole lot of y'all have been curious about.
So we're going to run through it.
We are going to talk about why the GOP
is not going to impeach Gallagher.
Because it's something that has come up,
prompted a whole lot of questions,
and it is just incredibly unlikely that that occurs.
Okay, so if you have no idea what's going on,
a representative in the House,
Gallagher,
he's leaving.
He's a Republican from Wisconsin,
he's leaving, he's leaving on April 19th.
This puts the Republican Party in a little bit of a bind.
If he was to leave before the second Tuesday in April,
which would be Monday April 8th or before, well, they would be able to have a special
election and replace him.
If he leaves after that, well, the seat stays vacant, it hurts their majority.
Some Republicans, to include Marjorie Taylor Greene, have floated the idea of, well, we're
just going to expel him before then.
Okay. That's not happening. So they would have to expel him before the second
Tuesday in April. The second Tuesday in April is April 9th. The house is
currently on break from now until April 9th. It's not gonna happen. They would
need to get rid of him on April 8th or before. So the obvious question from
there is, well what if they bring everybody back? Yeah, they don't have the
votes. They do not have the votes. I don't think that they would even be able
to muster a simple majority here because it really wouldn't take much to cross
over and vote with the Democratic Party to stop him from being expelled. The
thing is they don't need a simple majority. They need two-thirds. They don't
have it. They do not have the votes for this. So it's being pushed out as a piece
of rhetoric. It's being pushed out as, oh I'm really mad about this and this is
what I would do, but they're on break till after the point they would need to
do it. Even if they called people back they wouldn't be able to get the votes
to do it because again they need two-thirds concurrence on this. It's
in the Constitution. I believe it's Article 1, Section 5, Clause 2, if you
want to look it up or around that. So that is not something I would be
particularly concerned about when it comes to any potential move from the
house. It seems incredibly unlikely that they would even pursue it, and even if
they did, I have no idea how they would even remotely come close to the number
of votes needed to actually expel Gallagher. All signs point to him
leaving on April 19th, and that seat remaining vacant. Anyway, it's just a
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}