---
title: Let's talk about generations and a shift....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=1M1Ds2qo7tY) |
| Published | 2024/03/09 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring generational differences in responses to crises, specifically focusing on hunger and famine.
- Recounts a viewer's observation of his emotional response in a video about starvation, noting the lack of desperation seen before.
- Describes a collective shift in response to hunger, particularly among older generations like Gen X and older millennials.
- Mentions the impact of past experiences, like watching coverage of famine in Ethiopia as children, on current reactions to crises.
- Suggests that the prolonged media coverage of past famines has shaped people's psyches and responses.
- Connects the lack of current aid efforts to the absence of continuous media coverage and celebrity involvement.
- Points out how different generations have varying levels of desensitization towards war and famine.
- Talks about moral injury and how different generations perceive and respond to global crises based on their upbringing.
- Hopes that the younger generation's aversion to military operations in urban areas stems from their exposure to famine-related issues.
- Concludes by mentioning the potential collective shift in attitude due to heightened awareness of famine and its impacts.

### Quotes

1. "You're not talking about boomers, you're talking about some Gen X and older millennials."
2. "This is your moral injury."
3. "Famine will tip scales. It will change things."
4. "It will be a collective shift. And yeah, there are people who might have been complacent."
5. "And it is because it does indeed hit different."

### Oneliner

Beau delves into generational responses to crises, citing past experiences like the Ethiopia famine coverage, to explain why hunger impacts different age groups distinctively.

### Audience

Generations reflecting on responses.

### On-the-ground actions from transcript

- Support famine relief efforts through donations or volunteering (implied).
- Advocate for increased media coverage and celebrity involvement in famine crises (implied).
- Encourage intergenerational dialogues on crisis responses and sensitization (implied).

### Whats missing in summary

Deeper insights on the emotional and psychological impacts of past media coverage on current responses to crises.

### Tags

#GenerationalResponses #Famine #MediaImpact #CrisisAwareness #CollectiveShift


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about differences
in generations and how things hit different
for different generations and why.
Why there are different responses to different things.
And we're going to go through it because I got a message.
And I'm going to read the relevant part of the message
removing personally identifying information and stuff.
You sounded very different in your videos about people
starving, and it was scary, but I didn't know why.
Somebody in the comments said that we've seen you angry,
and we've seen you amused, but we've never
seen you desperate before.
That's what it was.
Then other people were down there
talking about using helicopters in different boats
planes. It just seemed different, like a collective shift. When I was watching
your video, my mom stopped when she was walking by and listened. She doesn't care
about the news at all. Later, I heard her on the phone talking to her friend about
how, quote, they had to feed those people. Her friend was agreeing. Later, I talked
to her about it and she was visibly upset. This woman does not care about
about any of this normally at all.
No offense, but WTF.
Why is the response to hunger so much different
from old people, comparing to what's been happening?
Why does this hit boomers different?
I'm glad, but seriously, WTF.
First, because I have to say it, I am not a boomer.
So, it's not boomers, it's not boomers.
You know, a long time ago I saw a video and it was teaching you to view people as kind
of increments of time.
People are influenced by things that occurred throughout their life, sometimes without them
ever realizing it.
You're not talking about boomers, you're talking about some Gen X and older millennials.
That's who you're talking about, really.
Because things were different when we were growing up.
It was not uncommon for families to sit together and watch the evening news.
It wasn't uncommon to watch the evening news while eating dinner.
eating those microwave dinners and you're waiting for that for the
permission to eat that horrible brownie that for some reason we thought was so
delicious watching children in Ethiopia starve. Wondering why your parents couldn't
anything about it. Same way you are now. It was 83 to 85, but the coverage went on
for years. The coverage went on for years and that footage was up for years. To
this day there is still no accurate accounting of how many were lost. The
The low end of the estimates is 300,000.
The high end was 1.2 million.
I would imagine that that's what it has to do with.
Even if people don't realize, that's why it hits different.
It will be a shift.
will be a shift. There is something about it that is certainly burned
into people's psyches. When it comes to famine, that's not something that that
block of people, those who had that experience, who were influenced in that
way, that they're going to be willing to accept. I would imagine that if
If the media had footage of what's happening in Sudan right now, up on the screens all
the time, you would have people clamoring to provide aid.
I guess there's not enough celebrities who want to get together and sing We Are the World.
That's what it is.
This is your moral injury.
This is your moment when you truly begin to understand how horrible and brutal the world
can be.
For you, yeah, when you were growing up, Iraq, Afghanistan, that was the status quo the whole
time you were growing up.
It wasn't anything new.
So you probably tuned it out.
becomes aware of the world at a different point in time and in different ways.
This is probably your moral injury.
Hopefully that means that the thing your block, your age block, will not accept is military
operations in urban areas.
That would be a good lesson for the world, that would be a good thing to hold on to.
reality of famine hitting. And those images coming out, it will shift things, it
will be a collective shift. And yeah, there are people who were, who might have
been complacent, like your mom, about everything else going on because they're,
That same block, very desensitized to war, to conflict, to civilian casualties.
Very desensitized to it.
But hunger is something else.
It manifests in different ways.
You know, your generation, I've heard the joke so many times and it still, to this day,
I always kind of, it gives me pause when y'all joke about making friends with the quiet kid
in class just in case.
It's unnerving the same way that the generation that is going to be swayed by famine, jokes
about nuclear war and anytime there's even mention of it, my inbox fills up with questions
from your generation.
Famine will tip scales.
It will change things.
Collective shift might be the right term.
And it is because it does indeed hit different.
Anyway, it's just a thought.
have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}