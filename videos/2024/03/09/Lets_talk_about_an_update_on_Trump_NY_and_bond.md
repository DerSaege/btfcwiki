---
title: Let's talk about an update on Trump, NY, and bond....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=HlvS5AG5KNo) |
| Published | 2024/03/09 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump posted a $91.63 million bond related to the E. Jean Carroll defamation case.
- The bond only provides security through the appeal of the $83.3 million judgment.
- It is underwritten by Chubb, a company with the current CEO being a former Trump appointee.
- E. Jean Carroll indicated readiness to enforce the judgment if Trump didn't put up a bond.
- Carroll can oppose the bond proposal and needs to do so by Monday.
- Trump will face a $454 million judgment in another case on March 25th.
- There's no news on what collateral was put up for the bond or if Trump had to pay anything.
- Carroll mentioned something about ripping out gold toilets.
- It's likely that Trump's team will eventually share more details on the bond.
- The judge will have arguments about any objections to the bond on Monday.

### Quotes

1. "Trump posted a 91.63 million dollar bond."
2. "E. Jean Carroll seemed okay with the development, but also indicated she was ready to begin enforcement of the judgment."
3. "She said something about ripping out gold toilets."
4. "Trump will have to go through the same thing with the $454 million judgment in another case."
5. "Y'all have a good day."

### Oneliner

Trump posted a $91.63 million bond related to a defamation case, E. Jean Carroll indicated readiness to enforce judgment, and more legal battles loom.

### Audience

Legal observers, news followers.

### On-the-ground actions from transcript

- Object to the bond proposal by Monday (suggested).
- Stay updated on the developments in the legal cases (implied).

### Whats missing in summary

Details on potential future updates and outcomes of the legal proceedings.

### Tags

#Trump #LegalCase #Defamation #EJeanCarroll #Bond #Judgment


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump in New York
and how things are progressing up there
because there have been some developments.
You know, we just did that video saying
that things were kinda coming to a close
and something was gonna have to happen by Monday.
Shortly after that video went out,
well, something happened.
Trump posted a 91.63 million dollar bond.
That bond is underwritten by Chubb,
and the current CEO of which is a former Trump appointee
to like a trade gig.
This of course is in relation
to the E. Jean Carroll defamation case.
One of the interesting things about the bond
is that it only provides security through the appeal of the $83.3 million judgment.
Any future appeals would need a different bond is the way it seems.
There is no news, no indication of what might have been put up as collateral or if Trump
had to pay anything or anything along those lines.
There's no news on that yet.
I would imagine that eventually some of that would come out, but it will probably be Trump's
team that actually eventually says something about it.
As far as E. Jean Carroll is concerned, she seemed okay with the development, but also
indicated that she was definitely ready to begin enforcement of the judgment on Monday
if Trump did not put up a bond or something like that.
She said something about ripping out gold toilets.
So there's that.
Now she actually has the ability to oppose this bond proposal.
She can object to this.
She needs to do that by Monday and I believe if she does it they will have their arguments
about it on Monday, Monday afternoon.
Now keep in mind all of this is occurring and on March 25th Trump has to basically go
through the same thing with the $454 million judgment in the other case, in the obviously
larger case.
So things are moving along there.
We'll wait and see how it plays out.
We'll see if Carol objects to this, and if so, what the judge decides on Monday, and
and we'll see where it goes from there.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}