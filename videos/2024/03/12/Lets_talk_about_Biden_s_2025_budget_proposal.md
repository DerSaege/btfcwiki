---
title: Let's talk about Biden's 2025 budget proposal....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=iB-jC4f1drs) |
| Published | 2024/03/12 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces Biden's proposed budget for 2025 and notes the slow pace in the house in finalizing the 2024 budget.
- The budget is based on non-recessionary conditions with low unemployment rates.
- Noteworthy proposals include optional free preschool for four-year-olds and affordable childcare for those making less than $200,000 a year.
- The budget focuses on taxes, aiming to reduce the deficit by 3 trillion over 10 years and increase revenue by 4.9 trillion through new taxes.
- Middle and low-income individuals receive tax breaks, while higher-income earners face higher tax bills.
- Significant changes include expanding the child tax credit from $2,000 to $3,600 and extending the Earned Income Tax Credit to more demographics.
- The budget also includes a $10,000 tax credit for first-time homebuyers.
- Funding for these proposals comes from increasing the corporate tax rate to 28%, implementing a minimum tax rate for billionaires, and raising the top tax bracket for individuals earning over $400,000 and couples over $450,000.
- Beau acknowledges that not all proposed measures may make it into the final budget as negotiations begin with this initial proposal.
- He anticipates opposition from Republicans and expects a process of negotiation to determine the final budget.

### Quotes

1. "Optional free preschool for four-year-olds."
2. "If you make less than 200,000 a year, it guarantees affordable childcare."
3. "The deficit goes down 3 trillion over 10 years."
4. "What I just said is incredibly unlikely to all make it in."
5. "This is a starting negotiation position."

### Oneliner

Beau introduces Biden's proposed budget for 2025, focusing on tax changes, childcare provisions, and potential negotiation challenges with Republicans.

### Audience

Budget analysts, policymakers

### On-the-ground actions from transcript

- Contact your representatives to express your support or concerns regarding the proposed budget (suggested)
- Join advocacy groups working on budget-related issues to stay informed and engaged (exemplified)

### Whats missing in summary

Detailed breakdown of specific tax changes and their potential impacts

### Tags

#Budget #Biden #TaxChanges #ChildcareProvisions #Negotiation #Republicans


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about
Biden's proposed budget for 2025.
It is worth noting due to,
well, let's just call it a slow pace in the house,
that we still don't actually have
all of the 2024 budget yet,
but this is his proposal.
Okay, so what's it based on?
All of these proposals exist based on a set of conditions.
The conditions here are, it's non-recessionary.
So no recession and unemployment stays low.
Reasonable enough.
What's in it?
I'm not gonna go over everything.
Things that popped out to me
were optional free preschool for four-year-olds.
And if you make less than 200,000 a year,
it basically guarantees affordable childcare
from birth to kindergarten.
That seemed noteworthy.
From there, everything else is about taxes.
So big picture stuff, deficit goes down 3 trillion over 10 years, revenue from new taxes
goes up 4.9 trillion over the same period.
If you are middle or low income, you're getting breaks.
If you are not, you're going to have a higher tax bill.
What are some of the changes?
One of the big ones to me, the child tax credit.
This was tried out during the pandemic, was incredibly successful.
I think it's being expanded from 2,000 to 3,600.
And this is what occurred, I want to say, in 2021, and it lifted millions of kids out
of poverty. That one works, like we've seen it. So that's in there. The Earned Income
Tax Credit, which is normally geared towards workers with children, it looks like it would
be expanded to include more people who don't have children. And I want to say there's
There's $800 for about 19 million people, something like that, or it might be 19 million
individuals and couples, so the number would be even bigger.
And that would include workers over 65 and under 25 as well.
There's also a $10,000 tax credit for first-time home buyers.
And those seem to be the bigger cuts.
Now there's more stuff in it.
These things are always relatively complicated.
But those were the things that stood out to me.
How are you paying for all of this, right?
Who is the tax man going to see?
First, the corporate tax rate goes up to 28%, I think, from 21.
Billionaires would have a minimum tax rate of 25% on their income.
And then if you are making more than $400,000, let's not have that confusion again, more
than $400,000 as an individual or $450,000 as a couple, your top tax bracket would go
up to 39.6%.
Keep in mind that's the top part of it, not the whole thing.
So, those are the things that struck me.
So what does this mean?
This is the starting point.
What I just said, it is incredibly unlikely that all of that makes it in, because this
is where the negotiations start.
I imagine that this is going to be described as just pie in the sky, a whole bunch of stuff
that he's never going to get, and they're going to be really mad about it.
But this is a starting negotiation position.
The Republicans are not going to like a whole lot of this, and they'll have to hammer out
the differences, and then we'll see what's left.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}