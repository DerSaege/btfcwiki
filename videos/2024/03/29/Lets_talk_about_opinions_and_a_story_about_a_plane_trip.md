---
title: Let's talk about opinions and a story about a plane trip....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=5oUntRVjIaM) |
| Published | 2024/03/29 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces a story about a guy who loves space and encounters an astronaut on a plane.
- The guy sitting next to the astronaut tries to strike up a conversation and ends up embarrassing himself.
- Despite the guy's enthusiasm, the astronaut remains focused on a book about Portuguese art.
- Beau uses this story to explain why he hasn't expressed a firm opinion on nuclear power.
- He acknowledges his lack of expertise on the subject and the importance of being well-informed before forming opinions.
- Beau points out the danger of jumping to conclusions without sufficient knowledge or information.
- He stresses the importance of being open to changing opinions based on new information.
- The story serves as a reminder to avoid forming strong opinions without adequate knowledge.

### Quotes

- "If you don't know a whole lot about something, your opinion should be subject to change."
- "Your opinion should be subject to new information."
- "People have a very firm opinion about that, and they're wrong."

### Oneliner

Beau shares a story to illustrate the importance of being informed before forming firm opinions and the need for openness to change based on new information.

### Audience

Social media users

### On-the-ground actions from transcript

- Keep an open mind to changing opinions based on new information (implied)

### Whats missing in summary

The full transcript provides a valuable lesson on the importance of being well-informed before forming strong opinions and remaining open to changing perspectives based on new information.

### Tags

#Opinions #Knowledge #Informed #OpenMind #Change


## Transcript
Well, howdy there, you don't know people,
it's Billy Gunn.
So today we're going to talk about a question
that came in from y'all.
Opinions and a story.
And it's a story that I've told on the channel before,
but I wanna say it was like five years ago.
And it's definitely worth repeating.
It's a story I heard as a kid that,
well, I mean, it stuck with me.
So, the question that has come in quite a few times over the last week is that, hey,
you've talked about nuclear power, but you haven't expressed your opinion on it.
So this story, it's about this guy, and he loves space, everything about space.
It's his hobby.
It's his thing.
reads about it, watches documentaries about it. One day he's on a plane
and he's sitting there in his seat and the stewardess
well she's talking to this guy up at the front of the plane and
eventually walks him back, sits him right by this guy, the guy
looks over, and the guy that's sitting next to
him is an astronaut, and I can't remember which one but
it was one of the names you'd recognize you know somebody who's like walked on
the moon or something like that. So this guy, he's sitting next to his hero so
he's trying to spark up a conversation. Because he's sitting next to his hero
he's also making a fool of himself throughout the whole thing. And at one
point he looks out the window and he kind of nudges the astronaut and he's
like, look at how small everything looks down there on the ground. And the
astronaut looks out the window and tells him roughly how many thousands of feet
they are in the air, because the structures are still visible on the Earth.
And the guy is like, I'm calling it the ground, he's calling it the Earth. That's
why he's an astronaut. The guy keeps trying to spark up a conversation.
Eventually the astronaut pulls out a book, probably trying to get away from
the guy. The book's on Portuguese art. The guy, not taking the hint, continued to
try to have a conversation with him. And he's like, you're an astronaut, why are you
reading a book on Portuguese art. And the guy says, because I don't know anything
about it. That is why he's an astronaut. Why have I not expressed a firm opinion
on nuclear power? Because I don't know enough about it to have one. You know, I
I know the criticisms, I know the concerns, I've also read about the new reactors thanks
to the comments section, which that's cool, but I do not believe I'm well versed enough
on the subject to provide a firm opinion and say I'm definitely right about this.
So I haven't.
You might want to normalize that because over the last week thousands of people have watched
a video saying that what happened to that bridge, well that wasn't because of the boat,
it was because of something else and things going boom.
That is not what happened.
This person has a very firm opinion about that.
people have a very firm opinion about that, and they're wrong. It's probably something
that we should strive to make a good thing. If you don't know a whole lot about something,
your opinion should be subject to change, subject to new information.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}