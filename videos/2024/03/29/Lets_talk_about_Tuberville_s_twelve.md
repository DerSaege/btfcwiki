---
title: Let's talk about Tuberville's twelve....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=jJREpDGMWXI) |
| Published | 2024/03/29 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Discussed Senator Tuberville's year-long hold on military promotions, disrupting numerous lives in a political stunt.
- Tuberville held up promotions for 10 months over a program allowing service members to receive leave and travel reimbursement for family planning.
- Despite his strong stance, only 12 service members utilized the program, leading to disrupted promotions and commands.
- Beau questions the logic behind Tuberville's actions, considering the minimal impact of the program.
- Points out that the military has always had workarounds for such situations, making the political grandstanding unnecessary.
- Notes the excessive time and effort spent on 12 travel reimbursements, questioning the value of such political stunts.

### Quotes

1. "Tuberville in many ways kind of undermining his own political career with this stance that just didn't make any sense."
2. "At the end of this, when you're gauging political stunts, you will have politicians take incredibly firm stances and all kind of theatrics over 12 travel reimbursements."
3. "That doesn't seem like a good use of time to me."

### Oneliner

Senator Tuberville's year-long disruption of military promotions over a program used by only 12 service members raises questions about the value of political stunts over minimal impact issues.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Contact your representatives to prioritize meaningful issues over political theatrics (suggested)
- Support policies that benefit service members and their families (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of Senator Tuberville's disruptive actions and questions the effectiveness of political stunts over minimal impact issues.

### Tags

#SenatorTuberville #MilitaryPromotions #PoliticalStunts #ServiceMembers #GovernmentActions


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Senator Tuberville
and my curiosity and how all of that played out.
You know, recently we did that video talking
about what went on in Huntsville.
And it got me thinking about all of the other stuff
that was going on when those older videos were made.
And I started thinking about Senator Tuberville,
and his almost a year-long hold on promotions in the military.
It was 10 months.
10 months he had him on hold.
Disrupted just countless lives in this political stunt
that he engaged in.
And if you don't remember, it was all about this program
DOD had that they had started up that allowed people who needed to leave the state for family
planning to get leave and travel reimbursement.
That's what it was.
And he was convinced that this was just a really bad idea and if he held up the promotions
long enough, I don't know what he thought was going to happen.
So I was very curious about how all that had played out.
So I started looking, do you know how many people use that program?
This whole thing, all of these lives disrupted, all of the political grandstanding, everything
that occurred, Tuberville in many ways kind of undermining his own political career with
this stance that just didn't make any sense and it wasn't going to go anywhere,
that program was used 12 times, 12 service members used that program, 12, one, two, 12.
I feel like maybe getting an idea of the scope of the program would have been a good idea before
engaging in a 10-month battle, you know, freezing a bunch of promotions and disrupting people
moving around and taking their stations and commands and all of that stuff.
It seemed like that might be a good idea, but I guess not.
And I started thinking about it, and this makes complete sense, because there have been
workarounds for years, and we talked about it at the time, the military has always had
a way of making sure that people could get to where they needed to go.
I'm willing to bet using this program to get the travel reimbursement and all of that stuff,
it's probably a whole lot of paperwork and the other ways are a whole lot easier.
So at the end of this, when you're gauging political stunts, remember that a lot of times
you will have politicians take incredibly firm stances and engage in all kind of theatrics
over 12 travel reimbursements.
That doesn't seem like a good use of time to me.
if you're somebody who agreed with him.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}