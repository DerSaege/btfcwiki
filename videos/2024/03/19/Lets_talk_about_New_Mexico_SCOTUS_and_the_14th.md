---
title: Let's talk about New Mexico, SCOTUS, and the 14th....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=EMD1cTL-X3I) |
| Published | 2024/03/19 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the situation in New Mexico involving a person named Griffin who was disqualified under the 14th Amendment, Section 3, for their involvement in the January 6th events.
- Mentions the Supreme Court's rejection of Griffin's appeal, resulting in his disqualification remaining in place.
- Clarifies the supposed inconsistency people see between Griffin's case and a previous Colorado case involving Trump.
- Points out that the Supreme Court's decision regarding Griffin is consistent with the Colorado case ruling.
- Raises the possibility of a state being able to disqualify individuals from federal offices, particularly the presidency, based on the phrasing of the Colorado case decision.
- Notes Griffin's request to become Trump's vice president and speculates on Trump's potential response.
- Emphasizes that while many may not agree with the initial ruling, the decision on Griffin remains consistent with it.

### Quotes

1. "People obviously see an inconsistency here, but it's not."
2. "It's inconsistent with the first ruling."
3. "Now, obviously that [...] it's Trump so we don't know."
4. "The key thing here is that the two cases they don't contradict each other."
5. "I know a whole lot of people don't agree with that first ruling, but it was the ruling and this is consistent with it."

### Oneliner

Beau explains the disqualification of Griffin under the 14th Amendment, showing consistency with a previous case involving Trump despite perceived contradictions.

### Audience

Legal enthusiasts, political analysts.

### On-the-ground actions from transcript

- Analyze legal implications of state disqualifications regarding federal offices (suggested).
- Stay informed about constitutional interpretations and court rulings (implied).

### Whats missing in summary

Detailed analysis of legal nuances in the disqualifications and court decisions.

### Tags

#NewMexico #SupremeCourt #14thAmendment #StateDisqualifications #LegalInterpretations


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about New Mexico
and the Supreme Court and the 14th Amendment.
And we're gonna go over something
and add a little bit of context
because while it isn't widespread just yet,
I've already seen it kind of start to spread out
a little bit and people believe there was a contradiction
when there really wasn't.
So we're just gonna kind of go through it
to add a little bit of context. Okay, so in New Mexico there's a person last
name Griffin. This person was like cowboy for Trump, Jan six participant, so on and
so forth. They were removed from their position, they were disqualified under
the 14th Amendment, Section 3. The same section that the Supreme Court just said
Colorado couldn't remove Trump under. Griffin appealed his case to the Supreme Court and
they rejected it, so he remains disqualified. People obviously see an inconsistency here,
but it's not. Back in March in the Colorado case, this is what was said. We conclude that
states may disqualify persons holding or attempting to hold state office, but states have no power
under the Constitution to enforce section three with respect to federal offices, especially
the presidency."
So them rejecting this is actually completely consistent with the Colorado case decision.
It's not a surprise that this is how it went.
The interesting thing about the phrasing in what I just read is it, to me, it almost
leaves the door open for maybe somebody in Congress because it's a state, it's representing
the state and it's the state's election even though it's a federal office.
That part, especially the presidency, kind of caught my eye.
Now, obviously that would have to be very well argued, but it's an interesting comment
and maybe on that one I'm reading too much into it, but it was weird that that was left
open that way.
So Griffin remains disqualified and since then, he has asked Trump to allow Griffin
to be his vice president. I am not sure how Trump will respond to that to be
honest. I mean that seems like something that would immediately kind of be a no,
but it it's Trump so we don't know. The key thing here is that the the two cases
they don't contradict each other. It's not inconsistent. It's inconsistent with
first ruling. Now I know a whole lot of people don't agree with that first
ruling, but it was the ruling and this is consistent with it. So anyway, it's just a
thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}