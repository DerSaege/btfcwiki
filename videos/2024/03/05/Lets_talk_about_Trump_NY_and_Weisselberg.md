---
title: Let's talk about Trump, NY, and Weisselberg....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=0YKN3foIw1U) |
| Published | 2024/03/05 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump Organization's CFO reportedly pleaded guilty for perjury at 76 years old in Trump's civil case.
- The New York Times previously reported negotiations for the guilty plea back in February.
- The judge in the civil case may not be pleased with these recent developments.
- The documentation from Weisselberg could play a significant role in Trump's upcoming criminal case in New York.
- The case, often called the hush money case, revolves around falsification of records.
- The terminology "hush money case" may set a negative tone for the situation.
- Despite the seemingly minor impact on Trump directly, Weisselberg's guilty plea might have downstream effects.
- This event could potentially affect Trump more negatively than expected.
- Updates on sentencing and potential cooperation from Weisselberg will be shared as they become public.
- The situation is worth following as it unfolds.

### Quotes

1. "Weisselberg pleading guilty is probably going to have downstream effects for Trump."
2. "Calling it the hush money case is not the best terminology for it."
3. "It's just a thought y'all, have a good day."

### Oneliner

Trump Organization's CFO's guilty plea for perjury may have significant downstream effects on Trump's upcoming criminal case in New York, despite its seemingly minor impact initially.

### Audience

Political observers, legal analysts

### On-the-ground actions from transcript

- Stay informed on updates regarding Weisselberg's case and its potential implications (suggested)
- Follow reputable sources for accurate information on this legal matter (suggested)

### Whats missing in summary

Analyzing the full transcript provides more context on how this legal development could shape future events and Trump's standing.

### Tags

#Trump #NewYork #Weisselberg #CriminalCase #LegalMatters


## Transcript
Well, howdy there, internet people, let's bow again.
So today we are going to talk about New York
and Trump and Weisselberg and today's events
and how they may impact things in the future.
Okay, so if you have missed the news, aren't aware,
Trump Organization's chief financial officer
has reportedly entered a guilty plea for perjury.
Allen Weisselberg, who I want to say is 76 years old.
The allegations are that he perjured himself,
provided false information during Trump's civil case.
Now, it's important to remember that the New York Times broke a story back in February
suggesting negotiations were underway for this plea agreement, for a guilty plea.
And the judge in the civil case asked about it and I am not certain he's going to be
happy with these developments based on what it seems he was told at the time.
Okay, so what happens to Weisselberg?
We don't know yet.
But what is going to matter is that Weisselberg's documentation?
That's going to come into play in Trump's next criminal case in New York, what's being
called the hush money case, which is really about the falsification of records.
Calling it the hush money case is not the best terminology for it.
It's going to set a tone right away that there are problems when it comes to stuff like this.
So even though this event is relatively small and doesn't really impact Trump directly,
the odds are that this development, Weisselberg pleading guilty, it's probably going to have
downstream effects for Trump that are going to impact him more negatively than he might
imagine, I think.
So we'll follow it, and as sentencing becomes public or any potential agreement, whether
or not Weisselberg's going to cooperate, that's not expected.
But anything like that that comes up, we'll keep you updated on.
Anyway, it's just a thought y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}