---
title: Let's talk about food and the winds changing....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=HiNlJf00gN8) |
| Published | 2024/03/05 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Samantha Power, the boss over USAID, emphasized the need for Israel to do more to provide aid to those in desperate need.
- White House National Security Council spokesperson John Kirby's statement signals a shift in tone, urging Israel to increase efforts to provide food aid.
- Kamala Harris' focus on building support for a real peace process contrasts with the logistical and humanitarian concerns raised by Power and Kirby.
- The IDF is pressuring Israeli politicians to provide aid, proposing plans like routing aid through Palestinian businesses, which raises concerns about potential issues like price gouging.
- Feeding the civilian populace is strategically vital in conflicts, as a hungry civilian population can lead to desperation and instability.
- The IDF's push for aid to civilians indicates a growing concern about the situation potentially worsening.
- Countries like Jordan, Egypt, France, and the United States are reportedly involved in taking steps to address the food aid situation.
- There is a consensus among various organizations and countries that immediate action is needed to address the food crisis.
- Different entities within governments may start to clash as they focus on different aspects of the crisis, but this divergence may not occur until the food aid situation is resolved.
- Overall, there is optimism about progress being made towards addressing the food crisis, with a growing willingness from various institutions to take action.

### Quotes

1. "Feeding the civilian populace is 101 level stuff."
2. "There is a consensus that this needs to be addressed now."
3. "Y'all have a good day."

### Oneliner

Samantha Power and John Kirby push for increased food aid in Israel, while the IDF's pressure on politicians raises concerns about potential challenges and the growing need for immediate action.

### Audience

Global citizens

### On-the-ground actions from transcript

- Contact local organizations to support food aid efforts (implied)
- Join humanitarian initiatives working to address the crisis (exemplified)

### Whats missing in summary

The full transcript provides more context on the dynamics between different entities involved in addressing the food crisis and the potential challenges that may arise in the future.

### Tags

#FoodAid #Israel #HumanitarianCrisis #InternationalRelations #ConflictResolution #GlobalCommunity


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about
how the winds may be changing when it comes to food.
We're going to talk about some statements that were made,
what those statements mean.
We're gonna talk about some questions
that were prompted by those statements
and a little bit of nuance
and just kind of run through what has occurred.
So the first statement comes from Samantha Power.
That is the boss over USAID.
If you don't know what USAID is, there'll
be a video down below.
If you would describe USAID as a front for central intelligence,
you should watch it too, not because you're
wrong in sentiment, but there's a nuance there
that's going to be important to understand later.
So what did she say?
She said there's a, quote, need for Israel to do much more to get aid to those who desperately
need it.
U.S. aid, that's a statement that makes sense.
We also have this one.
This is from White House National Security Council spokesperson John Kirby.
There have been some incidents where they have not been either able or willing, or maybe
both, to keep the trucks going at an increased level.
bears a responsibility here to do more. The U.S. aid statement makes sense. The
one from Kirby, well that's a shift in tone. There's more willingness in the
United States government to basically tell Israel, hey you got to get these
people food. And then, well before we get into the next one, we'll answer the
question that goes with this. Did Harris sway the entire US government with that
speech? No, no, don't be silly. Of course not. That's definitely not what happened.
Two different things. Two different things. Harris, in her statement, was
about building support for a real peace process. Power and Kirby, that was
It's about logistics on the ground right now, dealing with a potential ceasefire and just
general humanitarian stuff.
Logistics, long-term planning.
To go back to the example from the other night, Kirby and Power handing out sandwiches.
Harris, the vice president, more, rather than just handing out sandwiches to homeless people,
This is more about wanting more shelters, more infrastructure, solving the problem at
root issues.
Two different things.
Right now, the goals of both, the goals of the people who are focusing on each issue,
they're in line.
It's important to start to distinguish between the two though because later on, there may
be moments where those seeking a peace process and those seeking immediate relief and dealing
with immediate needs and logistics, they're probably going to butt heads later.
So go ahead and start dividing the two sides out.
Now obviously there's going to be some people who are less than happy about this statement.
feel like it's the US pushing Israel too hard or whatever. There's another
statement, there's another thing going on that should address that because there's
another entity that is pushing Israel, the politicians in Israel, to do more.
And that's the IDF. The Israeli military, the boots on the ground, are telling the
politicians, you got to feed these people. They even came with their own plans, one
of which I think would be an abject disaster, that is their plan to route the
aid through Palestinian businesses. Personally, that's just begging for
price gouging and all kinds of bad things. The other is a seaborne route to
get more aid in. That, of course, prompted questions and these came from both sides.
left, right, both sides of the conflict, all over the place. The short question is
why would the IDF do this? But they were phrased in is this a trap and why would
they want to feed their enemy? The civilians are not the enemy. Start there.
start there. The civilians are not the enemy. From a strategic standpoint when
you are talking about this kind of conflict, feeding the civilian populace
is 101 level stuff. A hungry civilian populace is a desperate civilian
populace. That's why. That's why. So from a strategic point of view, if you are somebody
who supports Israel, understand the boots on the ground, the IDF are now telling the
officials, the politicians, you got to fix this. It's going to cause problems.
So what does this mean? It means that there is a growing concern that it's
going to get really bad. That's the real read. There is also a
willingness to set the conditions to get to a real peace process. Right now, all of
this is in line. Later on, believe me, there's going to be a divergence. Okay, so
those are the statements and those are the questions about that. Right now I'm
filming this real early and there's breaking news. I haven't been able to
totally confirm everything yet, but it looks like there was another drop and it
looks like it was Jordan, of course, Egypt, France, and the United States
states involved in it.
So this next bit may be a little extra.
It may not be necessary, but when
I was thinking about what I was going to say,
I didn't have this piece of information yet.
But just in case this turns out to be wrong,
to my own government, there have been load masters
and former load masters down in the comments sections
under these videos working out the math on exactly how many
C-17s, it would take to feed the entire population of Gaza.
Do not tell us this can't be done."
So all of this is relatively good news.
It's moving in the right direction.
There's a growing willingness on the part of the establishment
of various organizations, countries, just institutions
that are intractably involved in this to get the food there.
Some of it is self-serving.
Some of it is humanitarian.
There's a whole bunch of different reasons for it.
But there's a growing consensus that this
needs to be addressed now.
There is reportedly more steps being taken immediately.
But we'll wait and get further confirmation on that
as the day moves along.
So that's where we're at.
Again, you have two different processes occurring now.
So when you start to see different, let's say,
organizations within governments start to argue and start
to maybe even undercut each other at times,
it's because they're working on different issues.
So I would expect to see that start happening, but it probably won't occur until the AIDS
situation is addressed.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}