---
title: Let's talk about the Bronx and tuition....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=zebwzY-pcGk) |
| Published | 2024/03/02 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- An elderly woman's unexpected billion-dollar gift paid off the tuition for all students at Albert Einstein College of Medicine.
- Ruth Gottesman, a former professor, received a portfolio from her late husband containing stock worth a billion dollars in Berkshire Hathaway.
- Her husband's only instruction was to do what she thought was right, leading her to donate the entire amount to the medical school.
- This donation is the largest ever made to a medical school.
- The annual tuition per student at the school is around $60,000.
- With this generous gift, every student at the medical school will have their tuition paid for perpetuity.
- The heartwarming story goes beyond just the current students; it impacts all future students at the school.
- The act of kindness by Ruth Gottesman demonstrates how one person's decision can make a significant difference in the lives of many.
- This unexpected donation ensures that students at Albert Einstein College of Medicine will have their tuition covered indefinitely.
- The footage circulating on social media captures the moment when students were informed about their tuition being fully paid.

### Quotes

1. "A billion dollars worth. Billion with a B."
2. "It's everybody at the school forever."
3. "All because of an unknown portfolio and stock and a woman deciding to make the world a little bit better."

### Oneliner

An elderly woman's billion-dollar gift ensures free tuition for all students at Albert Einstein College of Medicine, impacting generations to come.

### Audience

Donors and philanthropists

### On-the-ground actions from transcript

- Donate to educational institutions (exemplified)
- Support students with tuition fees (exemplified)

### Whats missing in summary

The emotional impact of the unexpected and generous donation on the students and the community.

### Tags

#Education #Philanthropy #Generosity #MedicalSchool #TuitionAssistance


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we're going to talk about the Bronx
and medical school,
tuition,
and how some footage came to be.
So, you might have seen this already.
It has been floating around on social media,
but there's a bunch of students in there in an auditorium,
And there's this elderly woman making this announcement that they don't have
to pay for their tuition and everybody cheers. So the question is how did that
happen, right? What occurred? A former professor by the name of Ruth Gottesman
who is ninety-something years old.
Well, her husband passed, and her husband left her a portfolio that she was unaware of.
And this portfolio contained some stock in a company called Berkshire Hathaway.
About a billion dollars worth.
Billion with a B.
So, the only instructions that her husband left were, do what you think is right.
She decided to structure it and provide it in full to Albert Einstein College of Medicine.
It is the largest gift to a medical school ever.
And the interesting thing, even more interesting than, you know, all of those students getting
their tuition paid, the interesting thing is that when you look into it, you know, it's
a billion dollars, a billion dollars.
So the students will get their tuition paid forever, all the students, in perpetuity.
at that school is about $60,000 a year.
But it is structured now, and it should
pay for tuition for every student
at the medical school forever.
It's kind of a nice little story, right?
I just realized, this is Einstein on the shirt.
You can't see that.
But yeah, so that's what that footage is.
It is not just these students in the auditorium being told that they're going to, you know,
get free tuition.
It's everybody at the school forever.
All because of a unknown portfolio and stock and a woman deciding to make the world a little
bit better.
I know that this is not world news or national news even, but every once in a while it is
nice to talk about some good news. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}