---
title: Let's talk about the Army restructuring....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Ts3mDTbqupo) |
| Published | 2024/03/02 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The US Army is restructuring, cutting thousands of positions, but it's spaces, not faces.
- They are eliminating slots for different jobs, not personnel.
- The restructuring is to prepare for large-scale combat engagements in a near-peer contest.
- They are shifting away from counterinsurgency operations towards major warfare.
- The cuts are mainly in special operations and counterinsurgency roles.
- Beau believes deprioritizing the Middle East in US foreign policy is a reason for the shift.
- He mentions the need for counterinsurgency personnel in Africa after the Middle East.
- Beau expresses skepticism about the restructuring, especially regarding potential future needs.
- The focus is on retooling for conflicts similar to those seen in Ukraine rather than Afghanistan.
- Beau anticipates fear-mongering or conspiracy theories around the restructuring.

### Quotes

1. "They are cutting thousands of positions, but it's spaces, not faces."
2. "The US Army is restructuring to prepare for large-scale combat engagements."
3. "The rest of it makes sense, retooling to go to Ukraine style conflicts rather than Afghanistan style conflicts."
4. "This isn't indicative of them finding something out or anything like that."
5. "Y'all have a good day."

### Oneliner

Beau explains the US Army's restructuring, focusing on preparing for larger conflicts and shifting away from counterinsurgency operations, anticipating potential challenges and changes in foreign policy priorities.

### Audience

Military analysts, policymakers

### On-the-ground actions from transcript

- Monitor and advocate for the impact of the US Army's restructuring on national security (implied)
- Stay informed about shifts in military strategy and foreign policy (implied)

### Whats missing in summary

Context on how the US Army's restructuring may impact global security and future military engagements.

### Tags

#USArmy #Restructuring #MilitaryStrategy #Counterinsurgency #ForeignPolicy


## Transcript
Well, howdy there, internet people, let's bow again.
So today we are going to talk about
the US Army restructuring
and the loss of thousands of positions
and what it means because that news went out
and a whole bunch of questions came in asking,
you know, what, did peace break out?
Is there not a concern?
What's happening?
Okay, so the U.S. Army is restructuring.
They are cutting thousands of positions, but it's spaces, not faces.
They're getting rid of slots for different jobs.
They're not actually getting rid of personnel.
They're changing things.
They're not going to be refilling certain positions, and they're basically retooling
army. Why? Because, as we talked about for years, we are entering a new phase, a
near-peer contest. So they are restructuring the military to fight
large-scale combat engagements. That's what they're doing. What are they
getting rid of so they can retool? Counterinsurgency stuff. They're getting
right at counterinsurgency stuff mainly. They're shifting away from a lot of special
operation stuff and going back to major warfare stuff. They are including new
positions for like smaller drone operators, you know, applying the lessons
learned from Ukraine. But the the biggest cuts are going to be in special
operations, counterinsurgency, stuff like that.
Personally, I think they're probably making a mistake.
It's good that they're not trying to win the next war by
preparing for the last.
However, the goal of US foreign policy is to
deprioritize the Middle East.
So of course, we would need less
counterinsurgency people.
The thing is, immediately following that deprioritization would be prioritizing Africa.
You're going to need the counterinsurgency people.
I think they're making an error there.
The rest of it makes sense, retooling to go to Ukraine style conflicts rather than Afghanistan
style conflicts.
So that's what's happening.
It's not that peace is breaking out.
This isn't indicative of them finding something out or anything like that.
I'm sure some kind of fear mongering or conspiracy theory is going to develop.
This is something we've talked about on the channel for years.
They're just finally doing it instead of talking about it.
I don't really see anything major that's wrong with it.
I think that they're probably going to need to bring some of those positions back, but
I would prefer I'm wrong on that.
Maybe they are planning on not realigning countries all over Africa, and they feel like
they won't need them, and that would be fantastic, you know, I hope that's the case.
But I am skeptical.
Anyway it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}