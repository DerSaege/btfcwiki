---
title: Let's talk about airdrop questions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Yqa6kDyu1ok) |
| Published | 2024/03/02 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The US is gearing up for air drops of relief supplies into Gaza, possibly in coordination with other countries.
- Supplies are loaded onto planes, flown over designated areas, and released with parachutes to land on the ground.
- The operation is likely to use C-130s and C-17s, with the west coast being a probable location for drops.
- The goal is to distribute Meals Ready to Eat (MREs) initially, as they are easy to distribute and contain all necessary components for a meal.
- An emphasis is placed on clarifying that the dates on MRE cases are not expiration dates but inspection dates.
- There are considerations for a waterborne operation to expedite the delivery of supplies, despite potential risks.
- Concerns about food falling into the hands of Palestinian combatants are addressed, with a focus on the broader impact on support and governance.
- Beau stresses the moral obligation for the US to provide relief aid, noting minimal foreign policy downsides to the operation.
- The sustainability and scale of the relief efforts remain uncertain, but Beau hopes for continued support until it's no longer necessary.
- Beau expresses surprise at the possibility of a waterborne relief operation, which could significantly increase efficiency.

### Quotes

1. "The US is gearing up for air drops of relief supplies into Gaza."
2. "MREs are perfect for this. They contain everything you need for a meal."
3. "There is no reason to oppose this. Any attempt to manufacture one is not going to hold up to scrutiny."
4. "The US should be obligated to do this on a moral level."
5. "Way more can be done way faster that route."

### Oneliner

The US prepares for relief air drops into Gaza, focusing on MRE distribution and addressing concerns about aid distribution amidst conflict.

### Audience

Humanitarian organizations

### On-the-ground actions from transcript

- Coordinate with local humanitarian organizations to support relief efforts (implied).
- Correct misinformation about the dates on MRE cases to ensure people are informed about food safety (implied).
- Advocate for sustained US support for relief aid in conflict zones (implied).

### Whats missing in summary

Details about the potential impact of relief aid on the ground in Gaza and the broader geopolitical implications.

### Tags

#US #Gaza #ReliefAid #MREs #HumanitarianAid #ConflictZones #InternationalAid #Geopolitics


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about air drops,
only today we're gonna talk about them in a different way
because it's no longer hypothetical.
It does appear that the US is going to begin doing that.
So we are going to go through questions
that have already come in,
and then I'm going to anticipate a few questions
that are likely to arrive
and put out some information that I am certain is going to cause an issue at
some point because it always does.
Okay, so first in case you missed yesterday's video, what's happening? The
United States is going to begin airdropping in relief supplies into
Gaza. Looks like they're gonna do it in coordination with Jordan and probably a
other countries, or alternatively depending on who you talk to, other
countries are going to run their own operations. Doesn't matter, the more the
merrier on this. The number one question that has come in since
this information broke was, how does this actually happen? Wile E. Coyote style, okay,
The U.S. Air Force has a number of different planes that are well suited to
this. The supplies are put on pallets in the back of the planes. The planes fly
over. The pallets are locked in place during flight. Then they're unlocked and
they kind of get scooted out the back and they have parachutes on them and they
land. It's really that simple to describe. It's harder to actually do
it and get it to land where you want it to. So that's what's going to happen. My
understanding is I guess they're gonna use C-130s and C-17s to do this. That's
the C-17s, that's a whole lot of food. That's a whole lot of stuff that is
distributed with each one. Okay, where? My guess is along the west coast, along
the western side. Mainly because that area seems to have the best place to
drop it and reduce the risk of something bad happening on the ground, meaning the
pallet doesn't land in somebody's living room, or it doesn't land on rubble that
and make it unsafe to get to, stuff like that.
To me, that seems like the most likely place.
Other places that it could be done
are not always.
Think about places that have open fields.
Sometimes a good place to do a drop like this
often cemeteries as well. Not ideal for other reasons but it's it's safer for
the living. Okay, so from there what what other things might they consider?
Hopefully the first day they do it they inundate the area, let's say it's the
coast. They just inundate the coast. So there are so many pallets, there aren't a
bunch of people trying to get stuff off of one pallet. That would reduce a lot
of risk and reduce the level of anxiety overall. What are they going to be
dropping? Initially, MREs, meals ready to eat, US military rations. I know some
people are already groaning, but let's be honest, they're perfect for this. They're
easy to distribute either individually or by the case. The case is the right
size for one person to be able to move it. They contain everything you need for
that meal to include the means to cook it. You need water. Something that always
comes up when MREs are distributed outside of people who are familiar with
with them. The date on the side of the case is not an expiration date. It is an
inspection date to very different things. If you see it being described as an
expiration date in future social media posts, correct that information. The
worst thing in the world would be for there to be food there and people be
afraid to eat it. Those are inspection dates. I have personally eaten many a
meal well beyond the date on that box. If an MRE is bad, generally speaking,
you're gonna know when you open it. And there will be no doubt that it is not
something you want to eat. So that's information that may need to be
countered as it goes out. Okay, what else? We talked in the comments section, a few
of us talked about a waterborne operation and bringing in supplies that
way it could actually be done much faster and much more efficiently that
way. We were talking about how there were there were downsides to it in the way
people would complain about boots on the ground so on and so forth. Didn't really think it was likely.
Apparently Biden is considering doing that as well and according to reporting the Israeli
government is quote very receptive to that plan which is surprising to me. There are risks with
that as well, but a whole lot more could be done faster that way even though
there is a slightly elevated risk to the the people providing the relief. Okay, the
obvious thing that has come in a lot, well, what if Palestinian combatants
take it. Yeah, that's a risk when it comes to any food distribution thing in a
conflict zone. That's a risk. The fact that somebody else might do
something bad. They take that food and not distribute it. That does not mean
that you shouldn't do something good, okay. There are a number of people who
are anti Hamas, US policymakers who have said this, who have said well it we have
to find out how to stop it from falling into Hamas's hands. Okay, if you are truly anti-Hamas,
and that's your stance, and you understood anything about this type of conflict, you
would want that to happen. You would want it to happen. You wouldn't view that as a
bad thing because if they took it and they didn't distribute it to the people
who needed it, it would erode their support. It would make it more likely for, I
don't know, hypothetically speaking, a revitalized Palestinian authority to be
able to assert governance. But since you don't know anything about this type of
of conflict, perhaps you should just be quiet. There is no reason to oppose this.
There is no reason to not do this. Any attempt to manufacture one is not
going to hold up to scrutiny. There is a risk that Hamas is going to get some of
I imagine they will. I wouldn't even say it's a risk. It's gonna happen to some
degree. If they do what people are suggesting they'll do, it won't actually
help them. It will hurt them. I don't foresee that being something that
occurs on any widespread level. They may take it and then distribute it to
themselves to to get the the PR points for it because they do understand this
type of conflict. But I do not think that they're going to take it and not
distribute it. That seems incredibly unlikely and it would be a really bad
move. And again the risk of somebody else doing something bad does not negate the
U.S.'s moral responsibility here. This is something the U.S. should be obligated to
do on a moral level. I understand. I'm the guy who always says its foreign policy
morality has nothing to do with it. From a foreign policy standpoint, there is
zero downside to the United States doing this. There is no real objection to this.
this. Any objection to this will be manufactured. So, that's where it's at. We don't know how,
we don't know the scale of it yet. We don't have that rumor mill up and running yet. What
we do know is that it is not going to be a one-off, it's not going to be a token, but
But we don't know how widespread it's going to be and whether or not the US has the will
to sustain it until it's not needed.
I would hope that the waterborne stuff actually does kick off.
That was not something I thought would be on the table because I honestly I didn't
think the Israelis would would be okay with that and I didn't think US
policymakers would want to accept the the elevated risk but way more can be
done way faster that route. That would be great if that happened. So it does look
like a whole bunch of US service members are gonna get that pretty purple metal
with the hand on it and I personally cannot wait for the day when the racks of service
members have those with stars on them instead of other ribbons.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}