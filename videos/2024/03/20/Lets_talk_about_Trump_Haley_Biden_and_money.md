---
title: Let's talk about Trump, Haley, Biden and money....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=WUMkBzO7D44) |
| Published | 2024/03/20 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits
Beau says:

- Joe Biden is leading in fundraising, with recent reports confirming his significant lead.
- A Trump fundraiser is planned for April, with tickets priced between $250,000 and $814,600, targeting Republican billionaires.
- Despite Trump's efforts, it seems he is not focusing on small donor donations.
- Some of Nikki Haley's donors are now fundraising for Biden after Trump's statement that they are not welcome in the MAGA camp.
- There are Republican super PACs dedicated to defeating Trump, showing stronger fundraising performance than him.
- Biden claimed that 97% of his donors were small donors, but this may only be accurate for a specific period, not overall.

### Quotes
1. "Biden has a huge lead on fundraising."
2. "It's an event for Republican billionaires."
3. "Trump's attempt to push people into leaving Haley for him appears to have backfired."
4. "There are Republican-oriented super PACs committed to defeating Trump."
5. "Biden said 97% of the donors were small donor donations."

### Oneliner
Biden leads in fundraising, Trump targets billionaires, and some of Haley's donors shift to Biden, while Republican super PACs aim to defeat Trump.

### Audience
Political activists

### On-the-ground actions from transcript
- Reach out to local organizations to get involved in fundraising efforts for political candidates (implied)

### Whats missing in summary
Analysis on the impact of fundraising strategies on the upcoming election.

### Tags
#Politics #Fundraising #Trump #Biden #SuperPACs


## Transcript
Well, howdy there, internet people.
Let's go again.
So today we are going to talk about Trump and Haley
and money and fundraising and Biden.
And just go over some of the more recent news
since the last time we talked about this.
If you missed it or don't remember
the last time we talked about this,
Biden has a huge lead.
It appears he has a huge lead on fundraising.
That reporting went out shortly thereafter.
You've got a lot of coverage about a Trump fundraiser.
Some of this is being read as he's doing this because he's so far behind and the coverage
came out and all of that stuff.
I think that this was planned before then and they're just putting out coverage of it.
that he threw this together overnight. So, what's the fundraiser? It's in April. Do
not expect to be able to attend. My understanding is that the price per
person to attend is somewhere between $250,000 and $814,600. I have no
idea where that higher number comes from or how it became such a strange number.
It is, it's an event for Republican billionaires basically. There are a few
of them and they're very open about their support of Trump. That appears to
be who that's targeted to. Again, I don't think this was thrown together because
of the recent coverage. I think they're pushing out the information because of
the recent coverage.
You have that.
It certainly does not appear that Trump is going to, at least not with this event, is
trying to court the small donor donations.
In other news, back in January, Trump said, anybody that makes a contribution to Haley,
From this moment forth, we'll be permanently barred from the MAGA camp.
We don't want them and will not accept them."
It appears that some of Haley's donors are taking Trump at his word.
So they are now raising funds for Biden.
They are reaching out to their business associates.
Most of these people are business people, and they're reaching out to their associates
And basically, the pitch is Trump would be horrible for the economy, that from my understanding,
that's what they're doing, and they're going to be raising money for Biden.
And it's worth remembering, there are a number of Republican-oriented super PACs that are
committed to defeating Trump, that appear to be doing better at fundraising than Trump,
based on the early information, based on that baseline information.
And just so I don't have to do a separate video about it because it's a very closely
related topic, Biden said 97% of the donors were small donor donations.
My guess is from context he's talking about that last month.
It's definitely not true for the whole period of fundraising.
That appears to be closer to 50-50 than somewhere in the 90% range.
As far as whether or not that claim of 97% is accurate, we have to wait until the next
round of reporting on fundraising comes out.
be real easy to tell from that. So that's that's where things sit at the
moment. Trump's attempt to push people into leaving Haley for him appears to
have backfired to some degree and he is putting out information about a
fundraiser for the incredibly wealthy that's going to occur in April. Again
And my guess is that's been planned.
They're just pushing the information out to offset the bad news about those baseline numbers.
Anyway, it's just a thought.
have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}