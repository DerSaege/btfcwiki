---
title: Let's talk about Biden, students, and the taxman....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=3G2X5u-8ntY) |
| Published | 2024/03/20 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden's initial plan for student debt forgiveness was rejected by the Supreme Court, prompting a regrouping and a new strategy of allocating billions of dollars towards forgiving student debt.
- Currently, under the American Rescue Plan of 2021, student loan forgiveness is tax-free until the end of 2025, whereas previously forgiven student debt was subject to taxation.
- Biden is advocating to make student loan forgiveness tax-free permanently, indicating a long-term commitment to the practice.
- The decision to make student loan forgiveness tax-free indefinitely might suggest Biden's intention to continue this initiative through his potential second term.
- Despite potential pushback from both Republican and Democratic parties, making student loan forgiveness tax-free could be a strategic move to maximize the impact of allocated funds.
- The move to ensure student loan forgiveness remains tax-free is seen as unnecessary and potentially indicating a long-term commitment to the program.
- Biden's decision to push for tax-free student loan forgiveness could be perceived as a way to benefit working-class individuals, yet it may face criticism from certain Democratic factions.
- The ultimate goal or motive behind making student loan forgiveness tax-free permanently remains uncertain, and its impact on different political groups is yet to unfold.

### Quotes

- "He's making a move and it might be an indication of things to come."
- "Either Biden is playing 4D chess in trying to trick voters or he intends on continuing this practice through his second term."
- "It's an interesting move because realistically it's a fight he doesn't have to have."
- "We'll have to wait and see how it plays out."
- "Anyway, it's just a thought. Y'all have a good day."

### Oneliner

Biden's push for tax-free student debt forgiveness hints at a long-term commitment, potentially impacting future policies and elections.

### Audience

Voters, policymakers

### On-the-ground actions from transcript

- Contact policymakers to advocate for permanent tax-free student loan forgiveness (suggested)
- Stay informed on developments regarding student loan forgiveness and tax laws (suggested)

### Whats missing in summary

Detailed analysis of potential implications and reactions from different political factions

### Tags

#StudentDebt #Biden #TaxFree #Policy #FutureImplications


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about
Biden student debt relief and the tax man.
And see if there's anything that we can kind of pull out
of something that he's doing because he's making a move
and it might be an indication of things to come.
So if you don't remember one of Biden's big things
was he wanted to do student debt forgiveness,
got shot down at the Supreme Court.
Once that happened, he regrouped,
the administration regrouped,
and they started doing five billion here,
four billion there.
Pretty soon, you're talking about real money,
and I wanna say now it's $140 billion
spread over like almost four million people,
somewhere in that range.
One of the big questions about this
was, is this something he's going to continue to do for the rest of
this term, and would he continue to do it if he was re-elected?
One of the big talking points
was that he was just doing this for votes.
So, if that was the case,
then you would expect it to stop after his re-election.
Okay, so right now, under the American Rescue Plan of 2021,
student loan forgiveness is tax-free,
and it will stay tax-free through the end of 2025.
Prior to this, if you had student debt forgiven,
it could be taxed,
normally taxed at the at your normal income tax rate. So it kind of offset
things a little bit, you know, and it defeated the purpose. If you're having
$30,000 forgiven in student loan debt but then the IRS turns around and hits
you with an $8,000 tax bill, sure it helped but it wasn't exactly the same
And that's on top of any state income tax that might be applied.
So Biden is pushing to make it tax-free forever, make that permanent.
You have two choices. Either Biden is playing 4D chess in trying to trick voters or he intends
on continuing this practice through his second term. Because the current way it's being done,
it goes through the end of 2025. There's no reason to pick this fight. He's going to get
bunch of pushback not just from the Republican Party but probably from
within his own party on this, making it tax-free. It's probably an indication
that he intends on at the bare minimum continuing the four billion here, five
billion hair route and is trying to get the most effect out of that as he can by
making sure that that four or five billion doesn't end up being taxed. So
it's an interesting move because realistically it's a fight he doesn't
have to have. It's a fight he doesn't have to have and he doesn't need to have
now. The only reason I can think of is that he intends on continuing it if
he's re-elected or he's assuming that the average person who is waiting for
where student debt relief really pays attention to the tax laws and they're,
and this is gonna somehow trick them. I don't really think that's what he's
banking on. We'll see how the Republican Party responds to it because you know
this is a tax cut for you know people who generally speaking are working
people so we don't know if they'll actually support it and then you may have
have people on the Democratic side of the aisle point to degrees that make a
whole bunch of money and say it's a tax cut for the rich, which it doesn't really
add up with the way things are being done as far as who's getting their
student loan forgiven. It's just an interesting thing to note. We'll have to
wait and see how it plays out. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}