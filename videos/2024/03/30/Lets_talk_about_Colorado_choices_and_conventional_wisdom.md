---
title: Let's talk about Colorado, choices, and conventional wisdom....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=EaQPeBQsDno) |
| Published | 2024/03/30 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau provides an overview of the situation in Colorado regarding the vacant seat left by Ken Buck's departure and the upcoming special election and primary.
- Ken Buck, a Republican, left Congress creating the need for a special election alongside the primary.
- Lauren Boebert, also from Congress, cannot run in the special election due to being in office already.
- Boebert expressed concerns about voters being confused by having to split their vote between the special election and the primary.
- The Republican panel chose a former mayor, Greg Lopez, for the general election, not from the primary, thwarting Boebert's expectations.
- Some see this decision as favoring Boebert as voters won't have to split their vote, while others question if it will benefit her image.
- Boebert's actions and statements may have already created discontent among some constituents in Colorado.
- The outcome of these events and voter perceptions remain uncertain until the votes are cast.

### Quotes

1. "The establishment concocted a swampy backroom deal to try to rig an election."
2. "It just never materialized."
3. "I have questions about the conventional wisdom on this one."

### Oneliner

Beau provides insights into the Colorado situation, questioning the impact on Boebert's image and voter perceptions ahead of the election.

### Audience

Coloradans, voters

### On-the-ground actions from transcript

- Wait for the upcoming votes to understand the actual impact of these developments (implied).

### Whats missing in summary

Insights into the potential consequences of these political maneuvers for Boebert's future in office.

### Tags

#Colorado #SpecialElection #KenBuck #LaurenBoebert #Republican #GregLopez


## Transcript
Well howdy there internet people, it's Bo again.
So today we are once again going to
talk a little bit about Colorado and how things are playing out there
when it comes to the seat left vacant
by the departure of Ken Buck and how everything is playing out because
there have been some developments and there's
a couple of different ways that those developments could be perceived by the
people in the district.
Okay, so a real quick recap for people who aren't familiar with this storyline.
Ken Buck is a Republican leaving Congress.
He left in a way that created a situation in which there would be a special election
at the same time as the vote for the primary.
The special election would be there to determine who filled his seat until the next election,
and the primary is the primary.
This is the same district that Lauren Boebert is leaving her current district to go to.
Because she's currently in Congress, she can't actually run for the special election.
That put her in a situation where she said that, quote, the establishment concocted a
swampy backroom deal to try to rig an election.
She was worried that voters would not be able to figure out making two choices at once.
They would be confused by the ballot because it in essence would ask them to split their
vote.
They would vote for somebody for the special and then vote for her in the primary.
Okay, so after all was said and done and all of the expectations and anticipation about
The thing is, the Republican panel that made this choice, they didn't select anybody running in the
primary. They chose a former mayor, Greg Lopez, to run as the candidate in the general, which means
all of that stuff that Boebert was talking about, well, it just never materialized. So because Lopez
is not running in the primary. He's just going to be there in the special. So nobody gets an advantage
this way. Now there are a lot of people who are seeing this as a benefit to Lauren Boebert
because, now, she doesn't have to ask her voters to split the way they're voting. They're gonna
have to split no matter what. I don't see how it makes it less confusing, though. The thing is,
I don't know that that's how it's gonna be perceived. Boebert is already in a situation
where there are people who are unhappy about the way she is leaving one
Colorado district and going to another. Now, she basically came in and was like
everything is corrupt and it's all to stop me. And then it didn't
happen. I don't know that that is going to be perceived in a way that is
beneficial to Boebert. It very well may not. People may look at that as a sign of
of things to come and they may not want to be involved in it. We don't know. We'll
have to wait until the votes to find out, but I have questions about the
conventional wisdom on this one.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}