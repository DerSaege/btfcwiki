---
title: Let's talk about Biden poking at Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=m4YfCg25eaY) |
| Published | 2024/03/30 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden campaign's change in tone is discussed.
- The change is believed to showcase policy differences and character disparities.
- Biden's decision to go after Trump in this manner is attributed to him, not younger staffers.
- The strategy aims to energize the base and sway independents.
- Supportive Biden followers appreciated the approach.
- Impact on independents remains uncertain.
- The approach appears to unsettle Trump.
- Biden is likely to continue with this strategy as it resonated.
- Expect more of the same from the campaign.
- The shift is not temporary.

### Quotes

1. "Biden believes that going after him in this way is important, that it showcases not just the policy differences that every candidate tries to demonstrate..."
   
2. "So if you liked this, expect to see more of it because it's not some staffer, it's Biden."

### Oneliner

Biden's campaign's strategic shift in tone, spearheaded by Biden himself, aims to showcase policy differences and character disparities, energize the base, sway independents, and unsettle Trump, with expectations of continuity.

### Audience

Campaign strategists

### On-the-ground actions from transcript

- Support Biden's campaign efforts by amplifying the messaging and engaging with it online (suggested)
- Stay informed about campaign updates and be prepared to support future initiatives (implied)

### Whats missing in summary

Insights on the potential long-term effects of Biden's strategic shift and its impact on the upcoming election

### Tags

#Biden #CampaignStrategy #Trump #Election #PolicyDifferences


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about a change in tone
coming from the Biden campaign.
And we're going to talk about why that change is occurring.
And the surprising part is where that change originated.
Now, we talked about it before
when we first noticed it coming from Biden-Harris HQ,
which is the campaigns like social media response team,
they made fun of Trump openly.
They poked at him.
And we talked about it at the time.
He's probably trying out new messaging,
see if it resonates.
If it does, you'll see more of it, and all of this stuff.
the wide assumption by most commentators, myself included, was that this was the work of
younger staffers going to Biden and being like, look, embrace Dark Brandon, go after him in this
way, you know, poke at him, pick on him, you know, just get under his skin. Apparently, according to
According to the reporting, that's not the case.
According to the reporting, this is Biden.
Biden believes that going after him in this way is important, that it showcases not just
the policy differences that every candidate tries to demonstrate in an attempt to differentiate
themselves from their opposition.
Also, Biden believes that it shows a difference in character, and that that difference in
character, not just will it energize the base, but it will also help sway independents who
maybe forgot about what it was like to have Trump in office and how things really played
out.
The reality is, as far as it energizing the base, check the comments under the first video
where we talked about it.
Those people who are supportive of Biden, oh, they liked it.
So that part is correct.
As far as whether or not it sways independence, well, we don't know.
And there's no real way to gauge that yet.
But it does certainly appear to get under Trump's skin.
And there are a couple of things that the Biden campaign has done in the way they're
framing press releases and stuff like that, that certainly seem to be geared to push Trump's
buttons.
So if you liked this, expect to see more of it because it's not some staffer, it's Biden.
So it will probably become a bigger part of the campaign moving forward, because it did
resonate so they know at least part of the theory is sound.
So we'll see how it plays out, but I wouldn't expect it to change anytime soon.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}