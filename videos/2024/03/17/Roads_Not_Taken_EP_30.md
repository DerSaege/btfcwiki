---
title: Roads Not Taken EP 30
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=keLvvOAQH50) |
| Published | 2024/03/17 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The United States announced $300 million in aid for Ukraine, including ATTACMs.
- A US-backed ceasefire resolution at the UN has been finalized after weeks of argument over wording.
- The White House commented on Schumer's speech about Netanyahu, respecting Israeli elections.
- USAID to Haiti is delayed due to Republican dysfunction in the House.
- Russia's election is ongoing, with Putin expected to win.
- A pro-Trump group in Wisconsin may have fallen short on recall election signatures.
- Charges against Trump in the Georgia case were dismissed.
- Trump's first criminal case in New York is postponed until mid-April.
- Massachusetts governor announced tens of thousands of pardons for cannabis possession.
- Biden issued a statement supporting LGBTQ+ rights in schools.
- Bernie Sanders is advocating for a 32-hour work week with no pay loss.
- Lady Gaga faced backlash for a social media post, accused of being "woke."
- A Japanese commercial rocket bound for space exploded shortly after launch.
- Reports suggest a gene-edited cow may be producing human insulin in its milk.
- West Point faced accusations of changing its motto, sparking reactions.
- Beau addresses questions from viewers about terminology usage and potential book segments.
- Concerns are raised about the logistics and challenges of evacuating civilians in RAFA.
- The Biden administration has requested details on the evacuation plan by March 24th.
- Senators are being cautious in legislating the TikTok ban, aiming to avoid rushing into decisions.

### Quotes

1. "Every young person deserves the fundamental right and freedom to be who they are."
2. "Lady Gaga has gone woke, maybe she was just born this way."
3. "It seems as though the Governor maybe did a couple of ads, and it has caused a bit of controversy."
4. "The Senate is interested in having the ability to do what the TikTok ban is supposed to do."
5. "A little more information, a little more context, and having the right information will make all the difference."

### Oneliner

The United States announces aid for Ukraine, concerns rise over RAFA logistics, and senators approach the TikTok ban cautiously.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Contact local representatives to express support for aid to Ukraine (suggested)
- Join community efforts to raise awareness about the challenges of evacuating civilians in conflict zones (implied)
- Organize educational events on LGBTQ+ rights and support for youth in schools (exemplified)
- Stay informed about legislative developments regarding tech and social media regulations (implied)

### Whats missing in summary

Insight into Beau's perspectives and analysis on the various news events mentioned

### Tags

#USaid #Ukraine #CeasefireResolution #TikTokBan #RAFA #LGBTQ+ #Legislation #EvacuationChallenges #PoliticalAnalysis #CommunityEngagement


## Transcript
Well, howdy there, internet people, it's Bo again,
and welcome to The Roads with Bo.
Today is March 17th, 2024,
and this is episode 30 of The Road's Not Taken,
a weekly series where we'll go through
the previous week's events and talk about news
that was unreported, underreported,
didn't get the coverage it deserved,
or I just found it interesting.
Okay, so starting off this week,
we will go to foreign policy.
and looks like the United States has announced $300 million in aid for Ukraine.
The package is expected to include ATTACMs, and this is different than the aid package
that is currently still in Congress.
The US-backed ceasefire resolution at the UN is reportedly finalized, finally.
This is the one we've talked about before where they've been arguing over wording for
weeks.
It's apparently done.
It is still unclear when it would be put forward for a vote, if it will.
The White House has commented on Schumer's speech about Netanyahu and said it was a good
speech but has also said that elections were a matter for the Israeli people.
USAID to Haiti appears to also be tied up to some degree by Republican dysfunction in
the House.
Russia is currently having their election.
I am sure that Putin won.
Moving on to US news, the pro-Trump group that targeted the GOP assembly speaker up
in Wisconsin for a recall election may have come up short on signatures, reviews are still
ongoing and there are also allegations of fake signatures.
Please keep in mind, one of the reasons for the recall was that they believed that the
speaker did not keep the election process secure.
Some of Trump's charges in the Georgia case were dismissed.
The Trump World Move to Disqualify the DA failed, but it did provide them with a delay,
which is probably part of the goal.
In like news, Trump's first criminal case in New York has been postponed until mid-April.
The Massachusetts governor announced tens of thousands of pardons for simple possession
of cannabis.
Biden issued a statement on next Benedict.
In relevant part, says that every young person deserves to have the fundamental right and
freedom to be who they are and feel safe and supported at school and in their communities.
Bernie is currently pushing for a 32-hour work week with no loss of pay.
In cultural news, to answer one of the questions from last week, what's going to be the next
thing that goes woke that always was?
Lady Gaga received anti-trans backlash after a social media post.
Yes, Lady Gaga has gone woke.
Maybe she was just born this way.
In science news, a Japanese commercial rocket bound for space exploded shortly after takeoff,
after launch.
There were no injuries, but it is a setback.
This is something I haven't totally checked out yet, but it looks at first glance from
the reporting, like a gene-edited cow is producing human insulin in its milk. If
this is something that, you know, once checked out turns out to be accurate and
and can be scaled, this is big news for diabetics. In oddities and something
else that was accused of going woke West Point yes West Point was accused of
going woke after being accused of changing its motto from duty honor and
country the motto of course was not changed it was simply dropped from a
a mission statement, and the reactions poured in.
Moving on to a few questions from you, let's see what it says here, I've heard you say
Tisha in previous videos, why just call him the Prime Minister in the most recent?
Because the last time I got a whole bunch of questions from Americans asking what that
so I switched it to Prime Minister and this time I've got a whole bunch of questions from angry Irish people.
I guess next time I will use both.
Okay, idea for The Roads.
Bo takes a few minutes to review a banned book he's recently read or just address the challenge book list when it comes
out.
I'd love to read more and I struggle to find good stuff.
No question there, but implied question, will I do that?
Yeah, deal. Yeah, I can do that.
Explain why even pro-Israel commenters seem worried about RAFA.
I think context and scale might be important to understand here.
stand here. Part of the plan, the at least presumed plan to go into Rafa, involves the
evacuation of the civilians. To put this in scale for Americans, Dallas, you're evacuating
Dallas. However, you're doing it without any infrastructure because it's gone, so
you're doing it on foot. And in theory, somehow separating out the
opposition from the civilians, that's their plan. That's the goal that
they're trying to accomplish. There are people who are pro-Israel who are
concerned about it because it's not an impossible task but it is an
incredibly incredibly difficult one that requires a ridiculous amount of
of precision that is providing a lot of concern for a lot of people.
And then, I mean, so you're assuming that you have an opposition element in Dallas,
because that's the population size we're talking about.
out the civilians, moving them on foot when they don't trust you either.
This is, there's a reason why basically every commenter, every commentator, everybody who's
talking about it is expressing concern about the logistics of doing it, regardless of where
they sit on the conflict itself. It is again it's not impossible to do
something like this but it's it does not seem likely that it is going to go well
And it is provoking a lot of worry from people everywhere.
It is worth noting that from what I understand, the Biden administration has basically said,
we want to see your homework on this one.
I think they have until, they set a deadline of March 24th, maybe, to see it.
They want details on how this is going to be done.
There's a lot of concern across the board, and the reason is scale.
This is a massive undertaking that I don't know anybody who isn't worried about it.
What is my governor doing?
I'm from South Carolina or South Dakota, the governor of South Dakota.
Oh, the ads, like you're going to answer me through the camera.
ads, I'm assuming the governor up there has, there was like a dental office in Texas, maybe.
And then something else, I think, I have no idea what that's about.
From what I understand, somebody's filing suit.
It's a weird story that I haven't looked into yet because it's kind of down the priority
list.
But yeah, it is strange.
It seems as though the governor maybe did a couple of ads.
And it has caused a bit of controversy.
Why does the reporting say senators are slow-walking the TikTok ban?
I'm glad, but why?
Yeah, that's not unexpected.
Again, the Senate, more deliberate body.
I think Ron Wyden said it best.
And he said something to the effect of
when the Senate rushes to legislate on tech
and social media, they mess it up.
There's a concern.
It seems that the Senate is interested in having the ability
to do what the TikTok ban is supposed to do,
which keep in mind,
the goal of that is not actually to ban TikTok.
But they don't want to,
they don't wanna go too far with it.
That they have a lot of concerns
and there's a bunch of them
on a whole bunch of different levels.
I personally do not actually think
that this bill will result in a ban of TikTok,
but there are a lot of other issues
with the legislation as it is. My guess is that there's going to be some pretty
major changes to it in the Senate.
We'll have to wait and see.
And that looks like it.
Okay, so
a little more information, a little more context, and having the right information
will make all the difference.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}