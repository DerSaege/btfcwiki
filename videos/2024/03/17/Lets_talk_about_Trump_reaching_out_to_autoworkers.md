---
title: Let's talk about Trump reaching out to autoworkers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=4R_gkdKb-8k) |
| Published | 2024/03/17 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump reached out to autoworkers in Ohio, campaigning for himself and supporting his primary candidates.
- Trump's promises about bringing auto manufacturing jobs back to the US and imposing tariffs on cars from other countries were reiterated.
- The challenge for Trump lies in his previous term as president, where his promises to auto workers did not materialize, such as the GM walkout.
- Auto workers likely recall the lack of delivery on promised jobs and the impact of court decisions made by Trump's appointees on their union.
- Biden, who already has the UAW endorsement, presents a stark contrast to Trump in terms of support for the auto industry.
- Union leaders have referred to the prospect of Trump's second term as a "disaster," contrasting it with Biden's initiatives.
- The Biden administration has shown tangible support for auto workers, as seen with the first lady inviting a UAW member to the State of the Union.
- Trump's belief in the effectiveness of his rhetoric from 2016 may face challenges as people are unlikely to overlook his past actions while in office.
- The overall sentiment suggests that auto workers will likely recall the actual outcomes of Trump's presidency and compare them to his promises.
- The Labor Relations Board's situation and lingering effects from the Trump administration contribute to the skepticism surrounding Trump's outreach efforts.

### Quotes

1. "Trump's promises about bringing auto manufacturing jobs here and a hundred percent tariff on cars from other places."
2. "United Auto Workers already endorsed him."
3. "Union leaders were talking about a second term referring to Trump coming back as, quote, disaster."
4. "A lot of support that has come from the Biden administration that didn't materialize under a Trump administration."
5. "It seems like that they're going to remember what actually happened and compare it to the rhetoric."

### Oneliner

Trump reaches out to autoworkers with promises, but his past actions may hinder his credibility compared to Biden's support for the auto industry.

### Audience

Autoworkers, voters

### On-the-ground actions from transcript

- Support UAW-endorsed initiatives (implied)
- Stay informed about political candidates' stances on auto industry support (implied)
- Advocate for policies benefiting auto workers (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of Trump's outreach to autoworkers and the contrasting support offered by Biden, offering valuable insights for voters and individuals interested in labor relations and the auto industry.

### Tags

#Trump #Biden #Autoworkers #UAW #LaborRelations


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're going to talk about Trump
reaching out to autoworkers
and how that is likely to play out.
If you missed it, Trump was in Ohio
and he's basically up there,
he was up there kind of campaigning for himself,
but also trying to give his primary candidates a boost
and just the normal stuff.
And he launched into his talking points about auto workers and the auto industry in the United States.
And it's the normal stuff from Trump, you know, he's going to bring auto manufacturing jobs here and he's going to slap
a hundred percent tariff on cars from other places and so on and so forth, extending the hand.
The problem that Trump is going to face here is that he's already been in office once.
This rhetoric, yeah sure, it worked in 2016, but he's already been the president
and I would imagine that most most auto workers remember the GM walkout under
him and what he did or didn't do. I think they remember that those auto jobs he
promised, they never really showed up and they certainly remember the court
decisions from his appointees and how they impacted their union.
This is going to be a hard sell, especially in contrast to Biden, who already has the
UAW endorsement.
United Auto Workers already endorsed him.
And when you compare the Biden administration to Trump's, when it comes to the auto industry,
there's not really a comparison.
I think most people in that industry would remember back in September of 23, when union
leaders were talking about a second term referring to Trump coming back as, quote, disaster.
And then you compare it not just with the pushes that Biden has engaged in openly, but
the little things that are really going to matter.
The first lady, one of her guests at the State of the Union was a woman named Dawn, a UAW
member from Belvedere.
a lot of support that has come from the Biden administration that didn't materialize under
a Trump administration.
And I feel like this is going to be one of those things where Trump believes that that
rhetoric is going to work again because he believes people are going to forget what actually
happened when he was in the White House last time.
I don't think that's the case.
I don't think that's how that's going to play out at all.
It seems like a very tough sell, especially with the hits that the Labor Relations Board
has been taking and everything that is still fallout from a Trump administration.
It just seems like, it seems like that they're going to remember what actually happened and
compare it to the rhetoric.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}