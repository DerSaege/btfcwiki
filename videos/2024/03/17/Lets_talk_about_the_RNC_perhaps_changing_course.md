---
title: Let's talk about the RNC perhaps changing course....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=R_f28adc-pk) |
| Published | 2024/03/17 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Updates and reports on the Republican National Committee (RNC) are discussed.
- Trump's hand-picked leadership team had plans to cut programs and focus solely on getting Trump back into the White House.
- Reports surfaced about 60 people being fired and programs being discontinued.
- Programs like the minority outreach and mail-in voting initiatives were initially set to be scrapped but now are said to remain.
- Laura Trump is looking to bring Scott Pressler on board to lead a legal ballot harvesting initiative, which may face resistance within the Republican Party.
- Questions arise about whether the RNC's change of heart regarding programs is genuine or if they will be underfunded.
- Uncertainty lingers about the resources that will be allocated to these programs moving forward.

### Quotes

1. "Their sole purpose was to get Trump back into the White House."
2. "I don't believe that the leadership team has suddenly decided to focus a lot of energy on other things."
3. "This is undoubtedly going to be an ongoing story."
4. "Y'all have a good day."

### Oneliner

Beau provides updates on the RNC's shifting strategies and questionable program cuts, leaving uncertainty about their true intentions and resource allocation moving forward.

### Audience

Political analysts, Republican Party members

### On-the-ground actions from transcript

- Stay informed on the developments within the Republican National Committee (suggested).
- Monitor how resources are allocated to critical programs (implied).
  
### Whats missing in summary

Insights into the potential implications on upcoming elections and the broader political landscape.

### Tags

#RNC #RepublicanParty #Leadership #ProgramCuts #PoliticalStrategy


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about some new developments
and new reporting, once again, about the RNC.
And we'll just go through those changes
to the changes, perhaps,
because it appears they may now be running
in the other direction.
And we'll start off with a quick recap
for those who haven't really been following this.
Trump had his own hand-picked team for the leadership of the RNC. That team
made it in. They're the leadership. Shortly thereafter there was reporting
about 60 people being shown the door and that there were gonna be a lot of cuts
and that a whole bunch of programs were also going to be discontinued. The
leadership team that Trump wanted made it very clear from the beginning that
their sole purpose was to get Trump back into the White House.
Everything else was secondary, if a consideration at all.
And this obviously rubbed candidates down ballot the wrong way.
Recently those programs that were being cut, the RNC is acting like that was never the
case and that a lot of those programs will still be around. The two in question
are the quote minority outreach program which the rumor mill says candidates
down ballot we're like no we absolutely need the Asian vote and the Mel in
voting initiative. Those two things are now apparently going to stay. Again the
R&C is making it seem like they were never going to go. The reporting suggests
that perhaps they had a change of heart and they were going to scrap them but
now they have decided not to. The other thing that is occurring that is likely
to cause a little bit of a stir is that Laura Trump, she apparently wants to
bring on Scott Pressler to head up their, what do they call it, legal ballot
harvesting initiative. Legal ballot harvesting quote. That is probably going
to get some pushback from a lot of people within the Republican Party, but
honestly I feel like they're gonna push through that. I feel as though even if
If that pushback occurs, that he'll probably still end up with that job or another one.
Because despite how some people may feel about him, there are a lot of people now that are
very closely connected to the leadership team that are supportive of him.
If you're not familiar with him, he is somebody who gets described in the media as liking
queue and an election denier, it's that kind of stuff.
Now one of the questions that the candidates down ballot have to ask themselves is whether
or not this is actually a change of heart from the leadership team or whether or not
that initial reporting was correct and how much resources are going to be devoted to
these programs now that they are apparently still going to be around. I
don't believe that the leadership team has suddenly decided to focus a lot of
energy on other things. This may be one of those things where the program is
back, but maybe it's underfunded or under-resourced. So this is undoubtedly
going to be an ongoing story and we'll follow up on it with the next
developments.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}