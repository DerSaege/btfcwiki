---
title: Let's talk about why Biden can't fix Trump's documents case....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=cbwlb6EtBWQ) |
| Published | 2024/07/19 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about
why Biden can't do something.
In a recent video, we were talking about
Trump's document case.
And I said that basically at this point,
it wasn't going to happen before the election.
It was gonna have to go through the appeals process,
then eventually make it to the Supreme Court,
if it goes that route, so on and so forth,
it was gonna be a lengthy thing.
And I think I said that if it was for it to happen
before the election, it would take a miracle,
I think is the quote.
And I got this, why would it take a miracle?
Why can't it just take a presidential act?
Why can't a sitting president remove the judge
from her position and have a new judge assigned?
Okay, so that can be interpreted two ways. Well, I guess with everybody reading into things, it could be interpreted
three ways.  I'm going to assume you're not talking about Biden exercising his new immunity.
This could mean, realistically, it could mean removing the judge from the case
case or removing the judge from her position as a judge through a presidential act?
Okay, removing the judge from the case is a different branch of government.
The president doesn't have any authority to do that, like at all.
The federal judge is part of the judicial branch.
The president is the executive branch.
There's a wall there.
Does that mean there's no way for a judge to be removed?
No.
But it's a process, and it's not the president.
The short answer to why the president can't just literally fire the judge is the U.S.
Constitution.
Article II, Section 4, I believe Clause 2, let's just call it Article II, Section 4,
that way I'm not wrong.
the part that talks about impeachment. It says President, Vice President, and all
civil officers. Now, oddly enough, all civil officers is not actually defined
in the Constitution, and when you look at the debate, the constitutional debates
that were occurring at the conventions, you don't really find a whole lot there
to expand on what they were talking about. However, in the Federalist Papers,
which if you don't know, that's a really good guide for how to interpret the
Constitution as it was written at the time, said that impeachment was a check
against the judicial branch. So that's where the interpretation comes from. It's
very sound, and the historical use of it has applied to federal judges. So that's
the answer. We don't actually have a chief executive with unlimited power,
and we shouldn't want one. So the the answer to the question, why can't he
remove her regardless of what you mean as far as just take the case away from
her or fire her. He doesn't have the power to do that. The president, contrary
to a lot of people's opinions right now, doesn't have limitless power. It's not
supposed to. The president is really, is really supposed to be a, I don't want to
say a figurehead because there are powers, but the president is not supposed
to have unlimited authority over the other branches. Presidents pushing the
bounds of their authority is what has led to that impression. This is something that wouldn't occur
if Biden tried it. I am certain that the current Supreme Court, one of whom kind of indicated
she should make the ruling that she did, they would strike it down. That's not something he
can do. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}