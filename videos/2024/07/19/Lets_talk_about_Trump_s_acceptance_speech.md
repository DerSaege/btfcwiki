---
title: Let's talk about Trump's acceptance speech....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Pnbqq-G9nUE) |
| Published | 2024/07/19 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about Trump
and his acceptance speech
at the Republican National Convention,
where he formally became the Republican nominee
for president, we will talk about the speech.
We'll talk about whether or not
it matched with the messaging, how it might play.
I promise this video will be shorter than the speech.
In fact, all the videos today will be added up,
will be shorter than the speech.
So, the messaging that went out.
Trump is changed.
He's gonna preach unity.
He is going to be a statesman.
You are going to see the best that Trump can be
the convention. Okay, that was the messaging that went out. They telegraphed
that for, I don't know, four days or so. So the speech started about 10 minutes
into it. I'm sitting there like, hey, he must have listened to his handlers, to
as political advisors. He's doing pretty well. About 20 minutes into it, it started
to kind of wobble. By, I don't know, 40 minutes into it, fact-checkers around the
world were calling in relief, and it just, it didn't go over the way he wanted
until the final 15 minutes or so and he pulled it back together. My guess is that
the beginning and the end were the work of his political advisors and he filled
in the gap. It was an hour and a half long. My theory on that is twofold. One,
it's Trump so he wanted the biggest and the best and so on and so forth. It is the
longest acceptance speech in history. It also was probably part of the plan to
showcase that you know Trump can stand up there and talk for an hour and a half
and compare it to Biden. If the entire speech was like the first 15 minutes in
the last 15 minutes, that probably would have worked.
I don't know that it will given how it went. If this is the best that Trump has
to offer, there's not much of a change here. You know, there was that promised
new tone. The speech really didn't live up to it. I don't think that this won
over many moderates. As far as undecided voters, I don't think that this is going
to be something that sways them. A lot of the talking points within it, they're the
same Republican talking points that have just been hammered home over and over
and over again. Nothing new, nothing about policy, nothing about real
solutions to it, just the same the same grievances. I don't see this as being
something that is going to be incredibly powerful when it comes to swaying
people. There were points when even the attendees kind of looked, not bored
because they never look bored, more anxious, kind of a when is this going to
end thing, but not not bored. When you are talking about a Republican crowd, Trump
does keep them engaged, but they did look like they were waiting for it to end. So
that's what occurred. That's the speech. It did not live up to the hype as far as
unity. It did not live up to the hype as far as a new tone. It did not live up to
the hype as far as showcasing him as like a real statesman if unless the
media goes and just pulls clips from the beginning and the end and that's what
they put on TV I don't see this as having really any effect on on the
election on undecided voters anyway it's just a thought y'all have a good day
Thank you.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}