---
title: Let's talk about maintaining hope....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=vcWBtsAQ0ww) |
| Published | 2024/07/19 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about hope
and maintaining it and why you have to,
not just for you, but for other people.
I feel like this needs to go out now.
We'll get back to our normal schedule
and news and all of that stuff later.
We're doing this because I got a message from somebody
And it's a message from somebody who reached out, I guess, more than a year ago.
I'm going to remove some parts of the message for various reasons.
I feel like there are a number of people who might need to hear the core message.
So this is it.
They reached out I guess a year ago and it says that since then I have lost my only two
friends I had to suicide.
And then it gives the reasons, it gives the reasons.
I've tried helping out with local groups,
but they have all lost hope and dissolved
and or stopped doing work.
My online friend group has been collapsing under pressure,
either suicides or them just going away, I can't say.
But I have, in all this, found myself completely
isolated from anyone, which is what made me reach out again
after so long.
things have gotten so much worse, and I've tried to think in the ways you do
on a long enough timeline, etc., or that you hate doomerism,
or always expecting the worst.
And I've been
not trying to expect it,
to unlearn that,
but in the past year
it has basically been
the worst case over
and over and over.
And then there's another section
just talking about the political situation and this is a person who the
outcome of the election matters to them. I don't even know why I'm reaching out
you're just a guy on YouTube. I'm just looking for any hope whatsoever to not
live in debilitating dread for four months and then worse after that I feel
myself wanting to follow my friends to escape but I guess in the past few
months I've learned all kinds of breathing exercises from hotlines dot
dot dot. You reach out to whoever you have to. Hotlines, some guy on YouTube,
whoever you have to, people you haven't talked to in years, you continue to reach
out, you continue to get help no matter what. You have to do that. I can give you
the endless list of platitudes, you know, that it's a temporary solution, this too
shall pass and all of that. I imagine you've heard them. The thing is, if you
want motivation to maintain hope, which is what you're saying, you don't have a
choice. There's gonna be somebody like you after you and that person is
counting on you. People you have never met are counting on you. The three dots
at the end of this, they worry me. They worry me. I've sent you a message back.
I hope you respond. Don't, don't follow your friends. Don't do your breathing
exercises, make the phone calls, do whatever you have to. You're somebody who
obviously cares. The world needs people who care. It would be lesser without those
people. It would be lesser without you. You have to maintain hope. You don't have a choice.
You're needed. Even if you feel isolated, you are needed. You reach out, you can reach
out to me anytime. And to broaden this, so it isn't just for one person because there's
There's a lot of people who have issues with hope right now.
It doesn't matter where you come from.
It doesn't matter what you've done in the past.
It doesn't matter the challenges you are facing.
If you are somebody who wants that better world, you have to maintain hope.
You have to keep looking forward, not backward.
The world is counting on you to stick around.
You don't have a choice.
You have to.
I can give you all of the recommendations, but it sounds like you watch the channel.
Get a dog.
to Pat. If there's a bunch of things, you can't lose hope, keep making phone calls,
you have to stick around. Those three dots, they really concern me. You know, the story's
not over type of thing. You don't want that to be the end of the story. People
are counting on you. I would like it if you would put the the link the donation
link to whatever hotline you've been calling in the comment section. If you
You don't want to put it there yourself.
You don't want your profile associated with this message.
That's fine, send it to me, I'll do it.
Again, I've reached out, I hope you respond.
You have to maintain hope.
You have to.
And there's probably a lot of people for various reasons, not necessarily for the ones listed,
that have a hard time maintaining hope because you're constantly bombarded with bad news.
Reach out.
Reach out to people, anybody.
can be some random guy on YouTube. I promise you that there are people who
care and if the first person you reach out to doesn't respond, reach out to
somebody else, there will always be somebody you can reach out to even if
If it's a hotline, if you're having thoughts like that, make sure that you exercise the
self-care that's necessary.
Use the hotlines.
Reach out to professionals.
Reach out to people who have been there.
Just you can't give up.
You want what comes after those three dots to be the story of change, the story of perseverance
of you overcoming it, of you working to make the world better.
I'm going to suggest that by opening up the way that you did and sending this to me, that
have and you'll continue to. You have to maintain that hope. Again, I hope you
respond. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}