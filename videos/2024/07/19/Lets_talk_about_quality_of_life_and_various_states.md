---
title: Let's talk about quality of life and various states....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Hc-QaPLjImM) |
| Published | 2024/07/19 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about quality of life
in the various states and rankings.
We're going to do this because I like this one.
You see these rankings all the time
about the various states, best for business,
best for investment, so on and so forth.
The quality of life one, well, that's
about the average person.
not something specifically directed and tailored to those people who are part of
the owner class. It's about the workers. So this is put out by CNBC every year
and what they do is they look at a whole bunch of different metrics and compare
surveys. They look at everything from environmental factors to access to
healthcare to crime, to whether or not you can get child care, voting rights, just
all kinds of things. They look at surveys about discrimination and this year they
even added a thing about reproductive rights because surveys indicated that
younger people, well they're just not going to live somewhere where their
rights have been taken away. So they have included that as well. So we're going to
go through them. Okay, so the 10th worst state for quality of life, Arizona.
Particular weaknesses with air quality, reproductive rights, and health care.
Number nine is Kansas. Child care, crime, and a lack of worker protections.
Number eight is Louisiana. Crime, health, and reproductive rights.
Number seven, Missouri, voting rights, crime, and reproductive rights.
Number six, Tennessee, crime, inclusiveness, and healthcare.
Number five, Arkansas, inclusiveness, crime, and voting rights.
Number four is Oklahoma, with reproductive rights, health, lack of worker protections,
and voting rights.
Number three is Alabama, voting rights, inclusiveness, and a lack of worker protections.
Number two is Indiana, child care, reproductive rights, inclusiveness, voting rights.
And the worst state to live in when it comes to quality of life is Texas.
Reproductive rights, health care, voting rights, lack of worker protections, and inclusiveness.
Now for the people who watch this channel, you probably noticed a trend when you are
talking about these states with the worst quality of life and let's say electoral maps.
That trend holds true pretty much every year.
There are exceptions, but that's generally the case.
So, again, this is about quality of life.
This gets sometimes, this particular one actually does get factored into a best for business
thing, I want to say it makes up 13% of that score.
Where a state does here makes up 13% of the overall whether or not the state is good for
business.
That is probably commentary in and of itself as far as, you know, the quality of life of...
Anyway.
So these are the trends.
These are the states.
It is something that I have always found interesting and probably always will.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}