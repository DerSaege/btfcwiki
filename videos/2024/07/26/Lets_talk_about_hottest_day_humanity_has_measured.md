---
title: Let's talk about hottest day humanity has measured.....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ikEGSsQ1xHM) |
| Published | 2024/07/26 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about the hottest
day humans have ever measured.
The hottest day they've ever recorded.
We're going to talk about that record,
and we're going to talk about where
that fits in to other trends.
So now that I've said that, obviously the question
everybody's mind, when was it? The hottest day humanity has ever measured. It was
Monday. It was Monday. The hottest day on earth. The previous record, you know,
the record that was set before that one, that was that was Sunday, the day
before. Before those two, it was July 6th, 2023. The information is coming from a
European climate service. Now surely you're gonna have some people say, well
it's July, just you know a one-off event, okay a two-off event, you know just add
normally hot, all of that stuff. I mean sure you can say that. The problem with
that is that the earth has had 13 consecutive months of heat records. It's
here. It's here. And people want to debate about whether or not it's man-made. I'm
I'm going to suggest that the people who said, hey, these emissions are going to cause this
in the future, and then what they said was going to happen happened.
I'm going to suggest that they're right and that, yeah, that's what it is.
It's not something that you can ignore until it goes away.
work that way. One of the important questions that you're going to have to
ask yourself is who you want trying to mitigate this. Who you want trying to
come up with policies to deal with the effects of it. Do you want the people who
actually, you know, are trying or do you want the people who are promising to
roll the very modest efforts back? Because I mean it's better for profit.
are a number of things that are very pressing right now. Global issues, issues
that affect all of humanity. When you are trying to decide who to vote for, when
you are trying to decide who to throw your support behind, this is definitely
something that you factor into it. Because it doesn't matter what your pet
cause is. This is going to impact it. This is going to matter to whatever your
cause is. This isn't going to be limited in scope. It's going to impact everybody.
in different ways, but this is a global issue. Nothing's going to be untouched.
Do you want the people who are going to try to do something or do you want the
people who are going to close their eyes, stick their fingers in their ears and
say, no, I don't want to believe it because it's too hard to deal with because realistically
are kind of your options. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}