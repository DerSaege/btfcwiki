---
title: Let's talk about lunar pits, caves, and humans on the moon....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=f1muvM-NNeo) |
| Published | 2024/07/26 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about lunar pits
and how they may be used by humanity in the future.
Lunar pits are exactly what they sound like.
They are pits on the moon.
The reason we're going to be talking about this
is because the Lunar Reconnaissance Orbiter,
which is a satellite orbiting the moon,
it's making like a 3D map,
it detected the entrance to a cave system.
Now, the idea of caves on the moon, not new.
They've been talked about for half a century or so.
But this one is something that appears accessible.
The cave system, the entrance to it, is inside of a lunar pit.
This lunar pit, in particular, is kind of close to where
Apollo 11 landed back in 1969.
It is reportedly, I want to say, 100 meters deep.
So it were to acquire a space elevator or something to get down to it.
And the current belief is that they were created by lava flows, is where the pits came from.
And it indicates the presence of an undiscovered underground world that we really don't know
anything about.
However, in the event that humanity does start placing bases on the moon, these are probably
going to come into play, because just like here on Earth, you want some kind of shelter.
So having the base in a lunar pit, in a cave system that is accessible that way, creates
a situation where you have something that has a stable temperature, is somewhat
shielded from all of the, you know, vague threats that exist in space like radiation
and, you know, little baby meteors and stuff like that. It provides shielding
from the elements. The thing that caught my ear when people were talking about it,
But they said that people might be living in them within the next 20 to 30 years.
To me that's hopeful.
It's interesting.
Now of course it wouldn't just be going up there and living in a cave.
They would build a lunar base inside the cave.
But it would provide an additional degree of protection even though it's harder to
get to. I still haven't seen anything that suggests how they're going to deal
with getting up and down constantly. Again, it's gonna have to be an elevator
or I don't know a rock climbing wall or something. That seems to be the one
thing I haven't really heard discussed, but it's an interesting premise and
And it's something that seems to be coming closer and closer
to reality, even though it kind of started in science fiction.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}