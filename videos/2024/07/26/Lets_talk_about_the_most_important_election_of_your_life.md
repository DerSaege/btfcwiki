---
title: Let's talk about the most important election of your life....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Bcfe7WBUQb4) |
| Published | 2024/07/26 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about
the most important election of your life.
You've probably heard that before, right?
You've heard that phrase, that saying.
We're going to talk about that,
and we're going to talk about a fairy tale.
We're going to do this because I got a message.
We'll start by reading that.
Beau, I'm getting real heavy, most important election of your life vibes from the Democratic
Party, just like 2016 and 2020.
I just keep thinking about the boy who cried wolf.
People who know me, you're already laughing because you know I do not like this story.
I don't like that story.
The boy who cried wolf, I do not like that story.
It never made any sense to me.
I'm not going to go full Garrick on you and tell you that the real moral to the story
never tell the same lie twice. I'm gonna tell you it doesn't make any sense. The story doesn't add
up. Your entire life you have interacted with that story in one way. Every time you hear it in a
traditional telling you are interacting with it as a random passerby hearing the story of this
rebellious child from the villagers. That's how you hear the story. Traditional telling very,
very quickly. Little boy top of the hill and he's guarding the flock. He cries out,
wolf. Villagers run up the hill. In some versions they pat him on the back
because they think he scared the wolf off. In other versions the little boys
laughing. Okay. They go down the hill because they don't see a wolf. He cries
out, wolf, again. They run up the hill. They don't see a wolf. They go back down
the hill. Third time he cries out. They don't come up and a wolf eats him in the
flock. That's the story. It doesn't make any sense. Interact with it a different
way. We're going to change this so you don't just have to accept the
word of villagers who just let a little boy get eaten. You're the sheriff. The
king has sent you there to figure out what happened. You are up on top of that
hill, little boy laying at your feet and you're trying to find out what happened.
So you ask one of the villagers, OK, what occurred?
Well, see, he was up here guarding the flock,
and he cried out wolf, and we came up here,
and there wasn't a wolf.
I mean, it seems like there was a wolf.
We have teeth marks and everything here.
OK, so OK, what else?
What happened then?
Well, we went back down the hill.
OK, so wait.
A little boy you trust with the village's economic livelihood
told you there was a wolf.
You came up here looking, didn't see one,
and you just went back down the hill.
Did y'all leave anybody else up here with him?
No?
OK.
OK, so what happened then?
Well, he cried out wolf again, and we came up here,
and there wasn't a wolf.
Okay, stop saying that there wasn't a wolf.
There was obviously a wolf.
We have physical evidence of the wolf right here.
What happened then?
Well, we didn't see a wolf, so we went back down the hill.
You didn't leave anybody else up here with him,
check in the woods, nothing.
Just went back down the hill.
Okay.
Then what happened?
Well, he cried out wolf a third time,
and we didn't believe him.
Wait, okay, stop, wait.
So you no longer believe that this kid could do the job,
but you didn't replace him?
You didn't leave anybody else up here with him,
even though he's tasked with guarding the financial
well-being of the entire village.
Yeah, I'm not buying this story.
I'm going to be honest.
What did you have against this kid?"
If you interact with that story any other way, it's not a story that has a moral about
false alarms.
It's a story that has a moral about actively looking for risks, about risk assessment.
That's what the story's about.
Because there's physical evidence to support the boys' story.
none to support the villagers and let's be honest I mean it certainly sounds
like they got a pretty good reason to lie. It just when you interact with it
in any other way it certainly appears that maybe the wolf was there but when
you know all the humans started running up the hill he went and hid in the woods
And they didn't look for him. Not well enough. Okay, so now that that's out of
the way. Do I think that this is the most important election of your life, even
though they said that in 2016 and 2020? No, I don't think that this is the most
important election of your life. I think the most important election of your life
was 2016 and I think the country did not, well it didn't look in the woods, didn't
see the risk that was there. You're going to continue to hear people say that this
is the most important election of your life until the the Trumpism fades. Those
people who heard it in 2016, you heard people talk about the things that were
at risk. You remember all of those women wearing those pink hats and everybody
made fun of them? Yeah, it turns out they were right. People didn't accurately
assess risk. You're going to continue to hear that until
Trumpism fades because there's a wolf in the woods and the thing is in this
case that first time people at least saw the wolf. It's not like the boy who cried
wolf in the traditional telling because we know what happened in 2016. We know
what happened with the Supreme Court. We know the lot of the rights that were
lost, we know the rulings that have come down. You're gonna keep hearing it. I think
it would have been better if people recognized the wolf back in 2016, but
they didn't. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}