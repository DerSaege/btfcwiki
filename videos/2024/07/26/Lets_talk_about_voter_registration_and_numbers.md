---
title: Let's talk about voter registration and numbers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ofHDCSWUuKk) |
| Published | 2024/07/26 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about voter registrations,
increases, and what it means.
Because it factors into some other math.
And I think it's important for people to remember that.
If you miss the news, vote.org, they
say that there was a 700% increase in daily new voter
registrations in the 48 hours after Biden said that he was not going to
pursue the nomination. That amounts to 38,500 or so voters. 80% of those were
aged 18 to 34. This is very similar to what happened when Taylor Swift put out
her her Instagram post back in September created a huge spike in new voter
registrations. So here's the thing and this is something that it is important
to bear in mind as you're looking at other numbers. These people they're not
polled. They don't get polled. They're not part of the registered voter
polling and they're definitely not part of the likely voter polling because they
haven't voted before. I'm going to suggest people who are registering in
this fashion they plan on voting those unlikely voters. We have talked about
about them since I don't know what 2018 somewhere in there they've been a
thing really important in 2020 and in 2022. They will probably be important in
2024. You're getting pretty big numbers that are suggesting you're going to have
a higher turnout in the 18 to 34 range. Now I mean they may decide not to but
that seems incredibly unlikely. My guess is that Harris has provided more
enthusiasm to a lot of voters, particularly a lot of younger voters.
Now we are still a ways from the election, a lot can happen, but if the momentum continues
I'm going to suggest that unlikely voters are going to play a really big part again
And it is something to keep in mind when you are looking at the polling, especially considering
the other issues that have been present.
So this is just one of those things to keep in mind because when you take all of these
spikes and you create a cumulative effect, you're talking about a whole lot of new voters.
And their opinions, they haven't been recorded, much less reported.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}