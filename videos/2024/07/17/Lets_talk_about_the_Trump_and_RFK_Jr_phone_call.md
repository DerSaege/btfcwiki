---
title: Let's talk about the Trump and RFK Jr phone call....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=EUzJUc45N08) |
| Published | 2024/07/17 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about Trump and RFK Jr. and a phone call that made it
into the public's view.
The immediate read on it right after it came out from a whole lot of people and what else
it may mean and then just something I've noticed.
Okay, so if you haven't heard about any of this yet, Trump and RFK Jr., they spoke on
the phone.
This conversation was videoed.
That video wound up on social media.
RFK Jr. apologized for it getting out, so that kind of confirms that it is authentic.
Now what was it discussed?
Really what you might expect, there was a conversation about vaccines, stuff like that.
It's worth noting that some people have said that it almost seems like RFK Jr. wanted it
to get out because he had Trump on speakerphone.
If I remember correctly, and maybe somebody who's more familiar with him can chime in
in the comments but I seem to I seem to remember him not liking having cell
phones by his ear. I think he's pretty much always on speaker so I don't know
that that necessarily means anything nefarious coming from the Kennedy side.
Now the immediate read that showed up on social media was well this
conversation kind of seems like they're in it together because Trump's like, oh
we're gonna win something along those lines. And that's the speculation that
they are somehow chatting and cooperating. That's actually not even
close to the read that I got. To me that seemed like Trump trying to get
him to maybe throw his weight behind him, trying to get RFK to maybe drop out and come
to Team Trump.
There's a little bit in the conversation where Trump, he doesn't come right out and say it,
but he kind of indicates, hey, maybe you could do something big with us.
suggesting that there's a possibility of a place for him in the administration.
It did not seem like they were working together as far as the campaigns.
That's not my read.
My read is Trump is starting to realize that RFK Jr. is taking way more votes away from
him than from Biden, and he's trying to bring him on board.
That's what I'm getting from it.
It's worth remembering that this is a pretty short clip of a conversation that we don't
have a whole lot of context to.
For all we know, they've spoke at other points and times.
There's a whole lot that we don't know.
But just going off of that clip, I'm not getting what a lot of people seem to have heard there.
It's just, to me, it was more like, hey, I'd love to help you with that, but I need a favor.
It was more one of those conversations.
Now this is obviously going to hit the news cycle, and undoubtedly there's going to be
just loads of speculation.
Keep in mind, it's all speculation, even what I just said.
To add to that, something I've noticed, and this could totally be correlation.
There may be no link whatsoever, but I kind of feel like Trump is paying a whole lot of
attention to FiveThirtyEight.
If you're not familiar with it, it is an election modeling site, ABC runs it, I think.
And what they do is they take a whole bunch of polls, they weight them on their trustworthiness
or whatever, they combine economic factors, all kinds of other things, and they create
simulations of the electoral college and the outcome of the elections.
They have been, historically speaking, they've been relatively accurate.
Biden has, overall Biden has been ahead over on 538 since like April, you know, there have
been ups and downs obviously, but for the most part he's been up.
Something and again, there may be nothing to this, but it seems to me that there is
a, there's an overlap between some of Trump's behavior and Biden trending up over there.
It, it almost seems like Trump gets worried when those numbers aren't going his way and
he starts reaching out or making really weird statements and it just again it's a feeling
more than more than anything concrete but it just it's something I've noticed and there
may be nothing to it but it's worth noting that this call does come at a time when Biden
is trending back up. In fact, I want to say the last simulation they ran, I think
Biden won it 53 times, Trump won 46, something like that. So we might want to
just start paying attention. When Trump starts acting weird, maybe go see what's
happening over on that site because there might be some overlap. Anyway, it's
It's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}