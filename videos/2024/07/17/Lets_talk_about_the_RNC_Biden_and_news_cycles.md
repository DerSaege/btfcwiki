---
title: Let's talk about the RNC, Biden, and news cycles....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=cAoBsHUJ1vs) |
| Published | 2024/07/17 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Biden and the RNC
and coverage and how it plays out sometimes
and how news cycles work because for weeks
since that debate, we have been treated
to never ending coverage about Biden,
questions about his his fitness. We'd get a clip that shows him close his eyes for
a little bit too long and then did he fall asleep? He might trip. He's obviously
too feeble, old sleepy Joe. You had commentators constantly saying that he
couldn't do anything without a teleprompter. And I have been waiting for
this to happen. Anybody been watching the RNC? So at the Republican National
Convention a number of things occurred. One, a clip emerged of Trump sitting down
closing his eyes on social media. It is being read as him falling asleep.
sleepy Donald Trump. Then you have Rudy Giuliani walking along a red carpet and tripping for
whatever reason and falling down. My personal favorite, because he was one of the people who
provided the commentary about Biden not being able to do anything without a teleprompter, was when
Speaker of the House, Mike Johnson, he gets up there to do an introduction and
he says, it is my honor to introduce the Attorney General. And there goes the
teleprompter and then he wanders off stage very stiffly. Which Attorney
General was it? It's weird, you know, with all the talk about memory, I mean if you
don't know that it was the Attorney General of Iowa, Bird. I don't know, maybe
maybe you're just not up to being speaker.
Do you think you're going to get the same kind of coverage? Do you think that there's going to be
weeks of coverage suggesting that basically every major figure within the
Republican Party is unfit because they're too old? And keep in mind there
were actually other incidents with the teleprompter as well, which I found funny every time.
Here's the thing, as we talked about at the time, they had a good storyline.
All they had to do when they were talking about Biden was wait for a human to behave
in a human way and then ask the question, is he unfit?
In a very short period of time, all of those behaviors were exhibited by people in the
Republican Party.
You're not going to get that same coverage.
Why?
Because the news cycle has moved on.
It's no longer an easy story.
So it won't get that kind of coverage.
Now I want to be clear about something.
I don't think that Trump actually fell asleep.
If he did, it was for a second.
it is just like those Biden clips. It's the exact same thing. Giuliani, I don't know, I have no idea
why he fell. Johnson, I mean, I feel like you should know who you're introducing, but sometimes
when there's a technical glitch, you kind of panic. You don't know what to do, and that overrides
you're thinking and you just wander off to music. These are just humans being
human, but that applies across the board. The storyline that was kept up, that
created a bunch of pressure, which realistically is based on a debate. It
was because it was an easy storyline. You're not going to get the same thing
right now because there are other things in the news cycle, things that are easier
to lead back to and provide commentary on. They won't want to rekindle that
conversation about the other side because, well, then it just kind of shows that all
of that commentary from before, well, it's just human behavior. Unless, of course, we
are going to say that tripping or not knowing what to say when there's a
technical malfunction or closing your eyes too long does actually make you
unfit for office. Either way, I have found this pretty entertaining and I imagine
there will be more.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}