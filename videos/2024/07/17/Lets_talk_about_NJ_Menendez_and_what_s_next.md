---
title: Let's talk about NJ, Menendez, and what's next....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=XSEuwj8gwHA) |
| Published | 2024/07/17 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about New Jersey
and Senator Menendez, the outcome of those proceedings
and where things go from here politically
because there's a lot of questions about it.
So if you have missed the news with everything else
going on, you would definitely be forgiven.
but the proceedings involving Senator Bob Goldbar Menendez have concluded he was found
guilty on all counts.
Now it is worth noting that the jury at one point sent out a note asking about whether
or not an acquittal had to be unanimous.
So where does it go from here politically speaking?
was immediate pressure on Menendez to resign. Now they can't force the resignation, they can put
pressure, they could expel. That is a process that would start with the Ethics Committee.
They have indicated that they are going to pursue an investigation and present options.
I believe they use the term speedy to describe it.
Speedy in DC is a relative term.
So I don't know if it is going to occur quickly enough
to happen before the election.
It may not.
They may want to do that just to help preserve the seat.
If they were to move for expulsion,
would take 67 votes. Odds are they probably have the votes. They probably
have the votes to do that. Expelling a senator isn't something that occurs very
often. I want to say, I don't know, maybe 15 times. It's not something that happens
often and I can only think of one that didn't directly have to do with the
Civil War. So that's where Menendez is. Now the obvious political question is
well what's gonna happen to his seat? I don't like to do election predictions
but given the fact that my Freudian slip about Senator Andy Kim already
happened, at this point I feel like something would have to go horribly wrong for Kim not
to win.
That seems like the likely outcome, but it depends on who shows up, just like anything
else.
So my guess is that Kim would retain that seat for the Democratic Party.
it all depends on turnout. Okay and then the last thing because a whole bunch of
questions came in about this. Apparently a major outlet put out an article that
said Menendez had almost half a billion dollars in cash. That is incorrect. He had
almost half a million according to the allegations. I want to say like four
$480,000. Not half a billion, which I mean that would be a whole lot of
currency. That was a typo that managed to get through. So that
should wrap up the political side of the Menendez events. Whether or not he
resigns or tries to ride it out, continues his candidacy as an independent,
gets expelled. All of that is up in the air, but I don't feel like, I don't feel
like he is going to be as successful as he might have been. It definitely seems
like Kim's race to lose at this point. Anyway, it's just a thought. Y'all have a
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}