---
title: Let's talk about the RNC, O'Brien, and the right questions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=yStDsKSOP88) |
| Published | 2024/07/17 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Sean O'Brien
being at the RNC, giving a speech
at the Republican National Convention.
And then we're going to talk about the right questions.
Because I had a whole bunch of people send in
one specific question.
And most of them were just inquisitive,
because it doesn't make sense right away.
some were, let's just say they weren't happy with O'Brien. If you don't know who
he is, he's a lead over at the Teamsters. Big, big Union guy. So what did he do? He
showed up at the RNC, I want to say he's the first Teamster to ever speak there,
gave a primetime speech called for bipartisan action in support of unions
at a partisan event, went after the big money that's behind the Republican Party
and didn't endorse Trump. That's what he did. Contrary to popular belief, because
there is a there's a huge overlap between the Democratic Party and most
unions because the Democratic Party is the pro-union party. It's not O'Brien's
job to get Biden re-elected. O'Brien's job is to get legislation that helps the
people he's supposed to be representing. That's his job. So the question, why is
he there? Why did he go? He went to do exactly what he did. Exactly what he did.
But, I don't think that's the right question, especially if you are a Democrat.
This is where we get into the Democratic Party not having ideal messaging, because the real
questions that Democratic leaders need to be out asking is, hey, hey, solve that speech
at the convention, that's cool.
y'all just cheering for that that's that's that's good stuff that's good
stuff right there so which which one of y'all is gonna help us out with the pro
act sponsor it who's that gonna be or they could ask about who which pro union
members of the NLRB the Republican Party is now going to vote for. Or they could ask, hey, you know, Project 2025,
what's that say about unions or the NLRB?
Y'all cheered for it. Y'all acted like y'all supported it. So, uh, we gonna get anywhere with that or was that just a
show for the cameras?  That's what the Democratic Party should be out asking. Those are the right questions.
are the right questions. That's how the Democratic Party can showcase that they
are the pro-union party, not just expecting O'Brien to not do everything he
can for the people that he he represents. If you want to showcase that the
Republican Party is not pro-union, all you have to do is point to their record but
But you have to do it loudly, because they'll cheer and then sell the unions out.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}