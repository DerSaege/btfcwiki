---
title: Let's talk about Trump, new info, and a reminder about intent....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=dsDI12WNYhQ) |
| Published | 2024/07/16 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump, Pennsylvania,
and new information that has surfaced.
Because while everybody on social media,
and a lot of people in the media,
are trying to stick with their established storyline,
and there are competing cat and mouse versions
what's going on, the information that is coming out about the suspect, it leads
us back to something that I talked about early on. The people who are providing
commentary about this, they are political commentators. Therefore, they are looking
for a political motive. Now, so far one has not been discovered. You
have interviews that have occurred with his classmates and they describe him in
a bunch of various ways. You know, very kind, kind of nerdy, some people saying he
was picked on, some people saying he wasn't, and you have people saying, at
least one saying that he was conservative, definitely quote conservative.
And I know that a lot of a lot of people are going to jump on that. Here's the
thing though, in all of these interviews you haven't had anybody come forward
and describe any leakage of any extreme views of any kind. That hasn't happened
yet. It may not. Now the reality is that most times there are signs. So far we
haven't seen any of that and because of the lack of a digital footprint it seems
unlikely that he was able to keep it bottled up without some other outlet for
it. The feds now have his phone, they've got it open, and still no word of any
extreme political view. This could change. Again, we don't know, but given the
number of questions about it, particularly about his classmates saying
he's conservative, I feel like it's important to remind people, even
though this is an event that has a lot of political implications, and on the
surface, it would appear to be political, that doesn't necessarily mean it has an
overriding political motive. It may be something else. Now that could
change with more information but based on what we're hearing and specifically
what we're not hearing about it it may be a more a more complicated reason than
people want to assign to it. Again we don't know yet but the interviews
suggesting he was conservative, none of them painted him as somebody who was
extreme, and that may not have actually had anything to do with his actions. So
again, you have to wait until more information surfaces. But right now, based
on what is available. The motive for this, it may not line up with anybody's
pre-established presumed narrative of events. You may have a kind of an
off-the-wall motive for it when it's discovered, but we don't know yet. Anyway,
It's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}