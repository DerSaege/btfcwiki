---
title: Let's talk about trends, Taylor Swift, and a project....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=01U8PHhtvEs) |
| Published | 2024/07/16 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about trends
and Taylor Swift, people's internet search habits
and a certain project that has gotten
a lot of media coverage lately.
And a metric I am probably going to start using
A lot more often because it's an interesting concept.
I think the first time I heard about it was on July 4th, maybe July 5th.
Somebody brought it up and it was actually about this topic.
They said that Project 2025 was being searched for more on Google than Taylor Swift.
It stuck with me.
was an interesting metric because there's a baseline of Taylor Swift's
popularity and you can use something called Google Trends to see what's
being searched more often. Then it came up again, I don't know, a few days ago
sometime in the last week. Y'all probably understand it's been a blur for me and
And so I went and looked, and basically since July 3rd, almost every day, there have been more searches about Project
2025 than there have been about Taylor Swift.  is definitely a topic that is going to become more central to the various
campaigns because there's an interest there that it doesn't seem like it's
going to fade away very quickly. And if you're not familiar with what Project
2025 is, it's a think tank proposal that was put together that has a whole lot of
proposals on how to reshape the federal government and a lot of the people who
are linked to it are people who are in Trump's world.
So there's a connection there.
Now he is publicly saying he doesn't know anything about it, but with the
number of allies that he has that had a direct hand in its drafting, I don't know
that he's just going to be able to say, oh I don't know, those people just brought
me coffee or whatever. So I feel like this is going to continue to be a
campaign issue and it's going to shape conversations because just like Taylor
Swift can be a baseline for search habits, that proposal from the think tank
is probably going to end up being a baseline for policy questions. So it is
like a 900 page document. I'll try to put the link down below again. It's a read.
but if you are interested in knowing the kind of questions that are going to be
asked in the future, it's probably worth looking into. Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}