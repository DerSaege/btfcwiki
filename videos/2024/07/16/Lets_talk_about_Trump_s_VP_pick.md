---
title: Let's talk about Trump's VP pick....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=14brKebjIPk) |
| Published | 2024/07/16 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about Trump
and his vice presidential pick, J.D. Vance.
And we're basically going to talk about how
Trump just made a pretty big,
a pretty big error, a mistake.
Generally speaking, the vice presidential pick
somebody who shores up the president's weaknesses. Trump picked somebody who was
very similar to himself. That's not really a surprise, given, you know, last time but
it's not going to help him. It exacerbates all of Trump's weaknesses
because they share them. Foreign policy, trade, their habit of using inflammatory
rhetoric. It's all there. It doesn't help Trump, and showcases his weaknesses even further.
The other thing is that because of the events in Pennsylvania, the Democratic Party was on its
back foot trying to figure out how to adjust messaging. They don't have to now, because
Because Vance has used his inflammatory rhetoric against Trump.
Vance has said in the past that he thought Trump was either a, let's see, I think he
called him a cynical, oh yeah, a cynical person, he didn't say person, like Nixon, or, and
And then he went on to compare him to that guy with the funny mustache that ran Germany.
His own vice presidential candidate did that.
The Democratic Party just got a gift when it comes to their messaging.
And then you have the electoral issue.
I am certain that Vance pulled very, very, very well among Trump's base at a national
level.
The thing is, and I got questions about this immediately from people, Vance is not well
liked in Appalachia, in the Appalachians.
He's not well-liked there because of some of the things that he has said, and his book.
Here's the thing, when you hear that term, you think of the deep dark hills of eastern
Kentucky, you think of West Virginia, that cultural region runs into Pennsylvania, runs
into North Carolina, Virginia, places that Trump hoped to make inroads, and those most
likely to maybe swing towards him, would have been those people in those states that were
in this region, but they really don't like Vance.
Is it going to be enough to alter the electoral outcome as far as the electoral college?
I don't know.
But this did not help in that regard.
This was a mistake for Trump.
He picked somebody very much like himself, which didn't help him.
There's a reason that presidential candidates, normally their VP isn't a whole lot like
them.
It's to appeal to a wider base of people.
That's not what Trump did.
He is defying conventional wisdom.
Now I'm sure there's going to be some people who are going to say, well, you know, that's
what Trump does because he's a maverick or whatever.
I don't know that this is going to pay off.
I don't know that this was a good move.
So it frees up the Democratic Party.
It creates a situation where he didn't really broaden his base, his appeal, and he very
well might have hurt his own campaign in areas that he was kind of targeting.
So I mean, we'll have to wait and see how it plays out.
But first reaction, this, this was a mistake from Trump.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}