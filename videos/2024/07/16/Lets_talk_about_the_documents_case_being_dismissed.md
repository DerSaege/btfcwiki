---
title: Let's talk about the documents case being dismissed....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=-CfSzCUpwa8) |
| Published | 2024/07/16 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Trump,
the documents case, the decision that was just reached,
what just happened, and what it realistically means
for you as an American.
If you have missed the news, if you have not heard,
the judge overseeing the documents case,
this is the case involving all of the classified material that was in like a pool closet, has
determined basically that the special counsel is unconstitutional, like as a whole, saying
that none of the statutes that were cited, quote, gives the attorney general broad inferior
officer appointing power or bestows upon him the right to appoint a federal officer with
kind of prosecutorial power wielded by the special counsel's office, nor do the special
counsel's strained statutory arguments appeals to inconsistent history or reliance on out-of-circuit
authority, persuade otherwise."
Okay, so the obvious question from here is, does the special counsel's office appeal
to the 11th Circuit?
answer to that is yes. Spokesperson for special counsel says the dismissal of the case deviates
from the uniform conclusion of all previous courts to have considered the issue that the
attorney general is statutorily authorized to appoint a special counsel. The justice department
has authorized the special counsel to appeal the court's order. So it's going to go to the 11th
circuit. It is worth noting that it is incredibly likely that the appeals
court goes to Smith, sides with Smith. Why? Because the special counsel's office
traces its lineage back to, oh I don't know, 1875 or so. The arguments will be
incredibly convoluted and involve a whole lot of historical precedent as to
which authorization was in place at which time, so on and so forth. But the
end result, 98% or so, is that the appeals court is going to side with Smith, at
which point in time it will go to the Supreme Court. Maybe. If it does, we know
that one person, one justice, is in favor of the judge's interpretation. However,
However, the reason we know that and the way it came out suggests that a majority of them
do not hold that interpretation.
However, even though that sounds like good news, the goal isn't to actually have the
case squashed.
The goal, from Trump's point of view, is to delay it beyond the election.
He succeeded.
It would take a miracle for that to change at this point.
He has succeeded.
So what does that mean?
It means that nobody is coming to help.
If you want to see Trump and Trumpism defeated, you're going to have to do it at the ballot
box.
That's what it means.
The end takeaway, all of the analysis, everything that is going to be talked about, all of the
commentary, it boils down to that.
That's the end takeaway.
And it doesn't look like anything in regards to this case, this national security case,
it doesn't look like anything is going to change that, not realistically.
So it means it is up to you at the ballot box.
Because this is not going to be resolved before the election.
Incredibly unlikely, ridiculously unlikely.
So that's where you're at.
That's what it means.
There will probably be days' worth of commentary on this, however we have a whole lot of stuff
going on.
And since the end result of this is very apparent, this is probably going to be the last video
for me on it until it gets to the appeals.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}