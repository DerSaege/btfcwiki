---
title: Let's talk about Biden's speech....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=wq-GVxzy6Ps) |
| Published | 2024/07/25 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
Lidsbo again.
So today, we are going to talk about President Biden's speech.
We'll talk about the speech that Biden just gave a few hours ago.
We're going to talk about the theme that's in it,
the purpose of the speech, the major theme, I should say.
There are smaller ones, but they all feed into the main one.
just kind of go over it. Now the speech itself for Biden, this is the defining
speech of his career. This is the speech for him. It's short. I didn't look at the
time. Less than 15 minutes if I had to guess. 12? I don't know. I'll put a link
down below so you can watch it but this was the speech for Biden. This is the
speech that when books are put together about presidents and their speeches this
will be the one that gets included from him and he knew that because one of the
purposes of the speech was to do something that pretty much every
president does, and that's try to shape their legacy. Kind of reach into the
future and hand historians little notes. Every president does it, almost every.
And that was present. That purpose was definitely there. He talked about the
things his administration had accomplished, the things that were left
undone, the things that he still wanted to see happen in the months he has left
in office. The speeches like this, they are referenced often and that's
important because when you get to the major theme, it's worth
understanding that he included this theme in a speech that is going to be
referenced throughout future history I guess. It's the defining speech and as he
tried to engage in that presidential behavior of shaping his legacy. He also
had a plea, a request of the American people and that's the theme. That's the
real theme in the speech. Yeah, it's about passing the torch and there's a lot of
elements of hope, but the reason you have to have the hope, the reason the hope is
important, the reason he felt the need to pass the torch, and all of that in the
speech, it all draws back to that request of the American people. And it's real
simple. Reject authoritarianism and embrace democracy and the fundamental
American ideals. That's the request and that's the theme. One of the things that
has come up often when Biden talks about this is whether or not it's just
politics, it's just rhetoric. He chose the defining speech of his career, the speech
that he knew historians would look back at, to inject a plea about choosing
democracy over authoritarianism. I feel incredibly comfortable saying that it is
not just politics to him. The irony in it is that if the American people choose to
embrace democracy. His defining speech, the speech historians will look back at,
will cast him as a silly alarmist because you know as time marches on and
you know that distance occurs people will look back and be like there's no way they would do
that, you know, and he knows that. He's been in politics a really long time, he's
seen a lot of these speeches. The message of rejecting authoritarianism and
embracing democracy, it was something that he was willing to tie his legacy to
and something that he felt the message was important enough to include it in
something that would be referenced later and that if people followed the plea he
would look silly. I have zero problem saying that for Biden, it's not just
rhetoric. It's not just politics. He believes the United States is at a
crossroads and that the choice is going to be made in November. The speech will
be down below.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}