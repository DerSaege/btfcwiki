---
title: Let's talk about shifting opinions on Project 2025....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=3PIlmU1PWDA) |
| Published | 2024/07/25 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about shifting opinions
when it comes to a certain project.
We're going to talk about who views something favorably
and who views it unfavorably and how those numbers have
changed from June to July.
We recently talked about how I had
discovered a new metric. I've been informed of a new metric when it comes to
whether or not something was popular when it comes to searches. People were
searching Project 2025 more than they were searching for Taylor Swift. So what's
happened. Back in June, Project 2025 had a 10% favorability and 19% unfavorable.
In July, 11% favorable, 43% unfavorable. Seems like when people find out about it
that they really don't like it. A big issue is not enough people are from
with it. They don't really know what's in it and how it's going to impact them.
What's interesting beyond the growth of the people who really don't like this is the breakdowns.
When it comes to the members of the Democratic Party, 7% view it favorably, 62% unfavorably.
unfavorably. I think it's actually very unfavorably. When it comes to
independence, 8% view it favorably, 28% view it unfavorably. When it comes to
non-MAGA Republicans, as defined as to whether or not they are a supporter of
the MAGA movement. Non-MAGA Republicans, 13% view it favorably, 32% view it
unfavorably. When it comes to MAGO Republicans, 20% view it favorably, 19%
view it unfavorably. The only demographic that actually supports it? Well, it's MAGO
Republicans. So don't let them tell you that it's not their plan. As the country
collectively booze the 900 page document. Don't let them say that, oh that's not
them. They're literally the only demographic that supports it. The project
itself, it was put out by a think tank, and it was a piece designed to be kind of a road
map for the next Republican president. A lot of the people who were involved and had a
hand in making it are people in Trump's orbit. He continually tries to distance himself from
it, but that's really hard when it's his allies that put it together. It's even
harder when it's his base that supports it. My guess is more people will find out
about it and these numbers will grow. You know initially you only had 29% of
people that had an opinion. It has grown pretty substantially since then, and I
feel like it's going to continue. This is one of those things that it's going to
be part of the campaign. It will continue to be discussed. If you're not familiar
with it, you might want to get familiar with it. Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}