---
title: Let's talk about Trump's nephew and healthcare....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=b22bGn_69_M) |
| Published | 2024/07/25 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump
and Trump's nephew and his book.
Some quotes in it that are certainly going to come up
again and again any time the subject of health care
or social security or, well, anything along those lines
comes up, because Trump's nephew has a book, the name of the book is All in the
Family, The Trumps and How We Got This Way, and there are some parts in it that are
certain to feature pretty prominently in any health care discussion. According to
Trump's nephew, there was a meeting and Trump's nephew, his kid is sick, has a
complex medical issue and there was a discussion about people with
complex health issues and according to Trump's nephew, Trump said those people
the shape they're in, all the expenses, maybe those kinds of people should just
die. You would think that this was the worst part. It's not. Later there was
another interaction, and Trump's nephew is basically going to billionaire
Trump because he needs to shore up the fund that he's using to provide care for
his son, and says that Trump said, I don't know, he doesn't recognize you, maybe you
should just let him die and move down to Florida.
I mean, I feel like these statements are probably going to factor into discussions during this
election cycle.
And these are the statements that are made in this context.
You have to wonder the statements that are going to be made when it's just nameless people.
It's just a policy decision.
doesn't really know any of those people. The US health care system, it's not, you
know, that great to begin with. I'm not sure that this kind of mentality is
something that's going to make it better. I'm certain that there are people who
have loved ones or who they themselves may be in and out of hospitals with a
lot of expenses, with complex care. That these statements they're going to
weigh pretty heavily on their minds.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}