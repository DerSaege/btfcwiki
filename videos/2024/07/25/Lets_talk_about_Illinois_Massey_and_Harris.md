---
title: Let's talk about Illinois, Massey, and Harris....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=4RyJ1VxXAIg) |
| Published | 2024/07/25 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about a situation in Illinois.
And it's something that we mentioned on Sunday
over on the other channel.
And I said at the time that each piece of information that came out
was more eyebrow raising than the information that came before.
And that trend hasn't changed.
That trend is still what's occurring.
Of course, we are talking about a situation where a now former deputy shot and killed
a 36-year-old woman named Sonia Massey.
So the footage has been released.
There's going to be tons of commentary and breakdowns on that all over YouTube.
The short version for people who are familiar with the breakdowns that we do, you know,
I talk to other people, you know, and have people kind of play devil's advocate as we're
going through it.
The two people who generally play devil's advocate for the cops in those conversations
have described it as one saying it was the least explainable shooting they'd ever seen.
Least explainable being the quote.
The other was completely uninsert a word explainable.
Those are the people that take up for the cops.
So the former deputy has been charged, multiple charges, the most serious is murder.
My understanding is that DOJ has also launched an investigation.
family is in contact with Ben Crump, attorney Ben Crump. Now, as far as eyebrow
raising beyond the footage, reporting now says that the the cop had been with, I
I want to say six departments in the previous four years.
And I haven't personally checked this information out myself yet.
And it hasn't broke at time of filming.
I imagine it will.
But I would be prepared to hear about his time before he was a cop as well.
So there's there's more coming. Vice President Harris has called for police
reform and and said that Massey should still be alive which I mean that seems
like something that should go without saying. So that is, that's where everything
sits at the moment. I would imagine that this is going to be a long-running story.
You're going to hear more and more about this as as things move along. It will
probably turn into a situation that actually influences the presidential
election as well. This is something that will probably become a campaign issue.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}