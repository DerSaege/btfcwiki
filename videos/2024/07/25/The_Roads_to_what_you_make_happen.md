---
title: The Roads to what you make happen
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=an74Q7kiUvA) |
| Published | 2024/07/25 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Bo again, and welcome to the Roads with Bo.
So today will be the roads to, I guess, five or six years of doing this.
In some ways, it'll be a clip show, and sometimes of clips y'all never saw.
You know, we have a whole lot of questions that came in over the last few days, and a
A lot of them had to do with the second channel
and how it was expanding and all of that stuff.
And those questions will be in the
Road's Not Taken episode on Sunday.
But I also mentioned recently how the production schedule,
the extra video over on the other channel and all of this,
how it has impacted and kind of slowed me being able to do
being able to do the charity and the aid work. We just haven't done as much this
year. Nothing has convinced me more that we need to get back to doing that more
often than the number of questions that came in from people asking, well what
did y'all do? It's sad, it's sad because it is a core part of this community. That
kind of work, doing that kind of stuff and making that real-world impact, it
has been central to this community for half a decade and yeah it has slowed. So
So, what would we like to get back to doing?
This.
Well howdy there internet people, it's Bo again.
The goal of this is to raise money for a Wolf Conservation Center up in New York.
Project Rebound.
An organizing action fund.
Miners in Alabama.
Children's home.
A union teacher.
She's the money she needs for these tables.
in these books and everything else.
Hurricane Ian.
This trans lifeline.
Gonna try to put together a tent city.
Domestic violence shelters have been a favorite charity of mine.
It's the second year we've done it.
Last year we had to put together five bags.
This year we had to put together 12.
We do this to benefit kids in domestic violence shelters,
teens particularly, because the Marines take care
of the little kids, but teens often get left out.
What we've been doing the last couple of years is getting
tablets and putting together a little package for them.
The reason we do the tablets is because the younger kids, if
they're around, they may be monopolizing the TV.
This gives them a space to kind of retreat to, be alone
with their own thoughts.
It may just be an eight-inch screen, but it's their
eight-inch screen.
fourth annual fundraiser for this.
This is something we do every year,
and it's just kind of kept growing and growing and growing.
Well, howdy there, internet people, it's Bo Gadd.
Y'all made a mess.
And welcome to the fifth annual fundraiser.
If you are watching this, I have been overtaken by events.
I have decided to go down to South Florida and help with relief efforts, or I'm organizing
supplies from here, or I'm driving them back and forth.
Something has occurred that will keep me from my normal updates and my normal routine.
There's a strike going on with miners in Alabama.
have been on strike for seven months. If you have been following the channel for a while,
you know that we have helped some miners in Alabama. We went and did a fundraiser, so
the striking miners, so their kids could get Christmas presents, help with that. And by
help with that, what I really mean is we got the resources together and basically handed
it off. When it comes to the stuff like this or any of the other stuff that we do, I get
the credit, but y'all do the work. I'm a shipping clerk. I'm a logistics clerk for all of it.
So today we're going to do a little live stream on and talk about helping miners up in Alabama
at Warrior Mech Call, they are moving into their second holiday season on strike.
The hero in that video is not the dude that had gas money because he had a YouTube channel.
The hero were the people that got their neighborhood supplied.
A year ago, your support saved my life.
That's addressed to me.
But the message is for y'all.
I cannot thank y'all enough for making this channel a force for good.
Because it really is y'all that accomplished that.
So there you go, a little more information, a little more context, and having the right
information will make all the difference.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}