---
title: Research Road EP 1
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=5aOeI1le5_g) |
| Published | 2024/07/23 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
It's Beau again, and welcome to the Roads with Beau.
So today, we are going to do something
a little bit different, obviously.
We're gonna try something out.
We're gonna try something out and see if y'all like it.
When it comes to this channel,
we get tons of requests for additional types of content,
wider formats, basically.
But one of the things that we get asked about the most is putting together like a weekly
roundup of news that kind of got missed on specific topics.
The most commonly requested is one for science and tech news.
So it would be like the road's not taken, just specific to stuff related to science
and tech.
And we get these for other categories of news as well.
science and tech is definitely the most commonly requested. I think it's a good idea. I think it
would add a lot of value because people can tune into the stuff that they're specifically interested
in. At the same time, I am just totally out of production time. I don't have time to record
more videos each week. In fact, I would kind of like to step back so we can focus more on the aid
and the other stuff that we do here.
So we are going to try something out.
And it seems very fitting to do this with science and tech.
Now, we have other stuff in the pipeline
that has other people involved, other voices that
isn't as unique as this one is.
But let us know what you think about it.
And be very honest, you're obviously
not going to hurt anybody's feelings over this one.
If this is something that y'all like,
this would become a weekly feature starting like now.
This would be the first episode.
Other things that we definitely have coming,
deal with like a this day in history thing,
more deep dives into documents, similar to how
we did with the Declaration of Independence, a whole bunch of stuff along those lines.
And if this is something that you all like, you'll also see it appear in other ways with
other categories of news.
So yeah, let us know what you think.
Anyway, here's the episode.
Well howdy there internet people.
you're at Research Road and it's the 23rd of July 2024. This will hopefully be a weekly series,
not unlike the road's not taken, but will cover science and related news. We might even sneak in
some sci-fi stuff. Okay, and we're starting off with tech news. Crowdstrike was at the heart of
the largest IT outage in history. It caused flights to be canceled or grounded, TV broadcasts
to stop and disrupted financial services. It was reportedly a faulty update that caused the
outage. The memes in the tech world were glorious. Project 2025 reportedly plans
on breaking up the National Oceanic and Atmospheric Administration. The National Weather Service would
end up privatized. This would create a profit motive when reporting climate and emergency
weather news. I'm not exactly certain that would prove to be a good thing for most Americans.
AI companies reportedly scraped the subtitles of YouTube videos to train their AI models on human
speech. Moving on to out of this world news, let's talk about space. A pre-print paper reported a solar
super storm in May thickened Earth's atmosphere, causing satellites to descend by a speed of 590
feet per day. The low-earth orbit satellites had to use their thrusters to push themselves back out
into space. On May 30th, scientists discovered there are elemental sulfur crystals on Mars.
Prior to this, only sulfur-based minerals were found on Mars. The discovery occurred when the
Curiosity rover drove over a rock, and it cracked open, revealing the crystals. The significance is
is that elemental crystals are pure.
And onto everyone's favorite topic, climate change.
Climate change is real and it's here.
According to a study from ICF, a data consulting firm,
the average person born today is expected to spend
roughly $500,000 throughout their life
to counter the impacts of climate change.
These costs are associated
with rising property insurance rates.
the costs of rebuilding or relocating after extreme weather, the impact on food costs
and decreased wages.
Reporting also says the world is spinning slower due to climate change lengthening our
days by milliseconds.
Melting ice caps are filling our oceans with more water.
That water is then flowing to the equator, causing the increased mass in the center to
slow the Earth's rotation.
In other climate and polar news, a recently released study suggests current models of
dealing with the ice caps' reflection of the sun's rays are off and are overestimating
the reflective properties.
The widely used model, which measures the reflectiveness of ice, is off by around 5%,
meaning that there is less solar radiation being reflected back into space than present
and earlier models indicated.
Now let's talk about animals. In news reminiscent of a plotline from Futurama, U.S. wildlife
officials plan on killing hundreds of thousands of invasive owls in hopes of protecting two
native species of owl. The northern spotted owl and the California spotted owl are in
danger of going extinct as they are too small to compete for food against the other species.
rights activists are heavily against the killing the invasive owls.
The San Diego Zoo will be giving their new pandas their first public debut on August
8.
The pandas are Yunquan and Xinbao and are part of China's panda diplomacy.
They are the first new pandas to be sent to the U.S. in 21 years.
In health news, scientists may have discovered a method to create a universal vaccine for
the influenza virus. Rather than targeting spike proteins, the method targets an interior
protein that does not evolve over time. This means that a single vaccine could be
used against all mutations of the virus. A German team reportedly cured a seventh
HIV patient through a stem cell transplant. A 60-year-old patient was diagnosed in 2009
with HIV and a kind of leukemia in 2015. After they gave the patient stem cells to cure the
leukemia, it appeared to cure the HIV. He has now been HIV free for five years. Now on to news about
digging up the past. 230 million year old fossil remains of an as yet unidentified dinosaur from
the Triassic period were discovered in Brazil after rain caused massive erosion revealing the
bones. Researchers have removed the fossilized bones for further study. Billionaire hedge fund
owner Ken Griffin reportedly bought a stegosaurus fossil known as Apex. He reportedly paid a little
more than $44 million for the mounted skeletal fossil that was only expected to fetch about $6
Now on to oddities. Due to the recent anniversary of the moon landing, here is a lesser-known
fact about the flag planted on the moon back in 1969. It was not securely planted at the
time the iconic photos were taken, and when the lunar module took off it was reportedly
knocked over, leaving it covered in lunar dust. A meteor reportedly passed over the
Statue of Liberty, causing loud booms in New York.
On July 16, NASA reported a meteor entering our atmosphere and breaking apart upon passing
over Manhattan.
So that's all the data we have to date.
I hope you enjoyed it and hope to see you again soon.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}