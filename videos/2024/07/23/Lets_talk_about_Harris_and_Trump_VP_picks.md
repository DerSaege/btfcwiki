---
title: Let's talk about Harris and Trump VP picks....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=hW44HnByv3c) |
| Published | 2024/07/23 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well howdy there internet people, it's Bo again.
So today we are going to talk about vice-presidential picks.
We're going to talk about Harris, we're going to talk about Trump, we are going to
talk about how important selecting a vice-president really is.
And we're going to answer a question that has come in a whole lot.
And it's asking me
who I think she should pick.
And the thing is, that question,
it's really three different questions.
It can be taken in three different ways,
which is who I want, who I think would be best for the role
in normal political times, and who I think
would be most effective at helping her campaign and winning?
You know, those are three very different questions, and none of those are the same people.
Picking a VP is important.
We talked about it when Trump selected Vance, and we talked about how it didn't expand his
base.
When you are choosing a vice president, you want that vice president to shore up your
weaknesses.
You want that vice president to help you swing voters.
You want that vice president to help you in states that maybe you need help in.
And when Trump picked Vance, we talked about it.
It didn't do that.
doubled down on his base got them more enthusiastic but it didn't extend a hand
to anybody and we talked about how even in the region he is associated with
there's a whole lot of people that have big issues with him and that's already
come up. So now you have reporting that says Trump is either regretting or is at
least second-guessing Vance as the vice presidential pick. So Harris has to put a
lot of thought in it. Okay, who would I want? I don't do endorsements. I don't do
endorsements and I think that would qualify. Who I think would be best in a
normal political setting? Mark Kelly. Mark Kelly, the astronaut. I think he would be
the best choice for in a normal political climate. He would help her the
most under normal political math. It's not a normal election though, so you have
to look at this specifically. Governor Beshear out of Kentucky, I think that's
probably her strongest pick. Why? Because he is from the region that Vance was
supposed to appeal to and he shores her up in other ways and he's already started
in advance. He said something to the effect of he quote ain't from here. If
you are not familiar with the region that is way more of an insult than it
sounds like. And when he said it, it started trending on social media and the
jokes started rolling in. I will repeat some of them, if you're not from the
region you may not get it, but they talked about how they believe that Vance
has margarine in his country crock-tubs that he washes his skillets that he
made fun of how he how they believe he takes his sweet tea I'm sorry it's not
sweet and I think the the most entertaining was talking about how he
pronounced a town in Kentucky. How they believe he pronounces it, I guess. The
town is Versailles. If you're not from the region, it's Versailles. Everywhere else in
the world, that's how you would say it.
These are people from the region that he was supposed to appeal to, and they are
making just normal little social media jabs at him in a way that suggests that
he probably doesn't have the support from that region that people may believe.
Bashir? He does. He does. He is very well liked. I think that that might, that might
really matter in a lot of ways. Now, all that being said, it's good to talk about
this but I really hope she does not, does not confirm who she is picking for a
while. They need to keep the buzz going. They need to keep the buzz going and
And it's also worth remembering that when I did this, I removed the people who have
flat out said they don't want to be a part of this race.
So I hope that her team makes a good choice.
I imagine they will, and I really hope that they don't announce it.
It doesn't get leaked until the convention.
I think that's really important, but we'll see what happens as Trump manages the new
political situation when it comes to Vance. Again, Trump built his campaign to
lean into Biden is too old, you know, we don't want an old guy. Trump is now the
like oldest nominee in American history or something. A lot of his previous
rhetoric, it's probably gonna come back to bite him. So there may be some
adjustments that the Trump campaign tries to make. We'll see how
everything plays out. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}