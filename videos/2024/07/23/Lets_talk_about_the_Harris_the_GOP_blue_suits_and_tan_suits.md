---
title: Let's talk about the Harris, the GOP, blue suits, and tan suits....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ljwIvB9hpFA) |
| Published | 2024/07/23 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Vice President Harris.
Something she said, the video of it that is circulating, and what that circulation can
tell us about how the Republican Party plans to proceed now that Harris is the front-runner
when it comes to the nomination.
a number of people have sent messages in asking if this was real and what's the story behind it.
Okay, so this is uh what she said. I am Kamala Harris. My pronouns are she and her.
And I am a woman sitting at the table wearing a blue suit.
I mean, yeah, I mean that seems weird.
I mean, could you imagine me walking out and saying, you know, my name's Bo, my pronouns
are whatever you feel comfortable with, I'm a man standing in a shop wearing a shirt that
says be kind.
I mean, that's strange.
That's really weird, except it's not.
Especially if you're in a room with a bunch of disability activists, which is the context
of that video. It was incredibly likely that there were people in that room who could not
see her, who were blind, who had bad vision. That's why she said it. This video is being
circulated by the by the Republican Party and it's it's very telling because
it shows that they are not going to try to engage Harris on policy. I would
imagine that you can guess why, but the framing is that they're going to try to
to make her look weird, look strange, make everything that she does appear very
bizarre, when if there's a little bit of context to it, it makes complete sense. I
find it very hard to believe that they just didn't know what was occurring in
this video. It's worth noting that for the actual politicians, this should kind
of be at the front of their mind, because it's July, which is Disability Pride Month.
Like they should be thinking about this.
They should have just gotten a refresher on how to do this.
So it seems pretty clear from the first two things that have started circulating, being
rehash of the whole birth certificate thing and oh wow she said this weird
thing that nobody knows what it meant. Now everybody knows what it meant. The
context is there. If you knew what the event was it was very clear. I'm going to
suggest that it is incredibly likely the Republican Party just starts flooding
everything with any out-of-context clip that makes her look incredibly bizarre
and they will remove any context and then they'll pretend like they don't
know. That's what I'm guessing we're going to see and you can infer
from that what you will about their willingness to address Harris on policy.
Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}