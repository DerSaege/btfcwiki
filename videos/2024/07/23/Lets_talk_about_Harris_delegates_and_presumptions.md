---
title: Let's talk about Harris, delegates, and presumptions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=oMCX_ew1sEE) |
| Published | 2024/07/23 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there internet people, it's Bo again.
So today we are going to talk about Harris and delegates
and how things are shaping up when it comes to the count.
OK, so the short version here, we
talked about how almost immediately Harris started
making calls trying to shore up support.
The reporting suggests that that has paid off.
That she now has a majority of delegates that have pledged that they're going to support
her when it comes to the nomination.
She has enough to clinch the nomination.
Now one of the things that has come up and it's interesting that this question has come
in so much is why you have a lot of outlets, I think the AP did it, they're
not calling her the presumptive nominee even though these delegates have pledged
to her because she's not. These delegates can still change their mind, that's why
they're not using that term. It certainly appears based on everything that I have
seen, everything that I have heard, that it's not even going to be close. That it
is going to be a very unified thing that those calls she put out early on
definitely created a situation where everybody decided to come together and
unite. So should you feel like she's the presumptive nominee? I'm gonna say yes.
Based on everything that I have heard, my understanding is that even in some of
the surveys you had some people that were undecided but you didn't have a
single person, a single delegate saying they were going a different way. So it
seems as though she is the presumptive nominee when it comes to how that term
normally gets used but from a technical point of view she's not because they
could change their minds. For me, this greatly reduces the risk of chaos at the
convention, and that is something that the Democratic Party definitely needed
to avoid. It looks like that's happened, so they have a good start
when it comes to the convention based on everything that has come out, everything
that we have heard, even stuff in the rumor mill, everything looks like this is
going to move smoothly. So now they have to put on a show, they have to make their
case, they have to talk about policy. Harris still has to make her selection
when it comes to vice president and have that ready. Again, I would hope that it
isn't announced. People have already sent in a whole bunch of messages. There's a
video about that coming out later. So at this point, Harris has it, I don't want to
say she has it in the bag, but it certainly, it certainly appears that it
would take something incredibly dramatic to change that outcome at this point.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}