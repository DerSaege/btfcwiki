---
title: Let's talk about the days getting longer and warmth....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=YCXMUDL6ExQ) |
| Published | 2024/07/23 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about the days getting longer.
And it's an interesting little side note.
This isn't something that is immediately pressing,
but it's definitely something to note
because it shows yet another example of how
the Earth is changing.
OK, so basically what is occurring
is the planet is getting warmer.
As the planet gets warmer, the ice sheets melt.
That water, well, it flows towards the equator.
There is more mass at the equator.
Therefore, it alters the way the Earth spins.
making it slower, lengthening the days. It is basically imperceptible but it is
something that can be measured and the rate at which it is occurring is
speeding up. So it's something that on the giant list of issues that a warming
planet causes, it's not a super high priority at the moment but it's
definitely something to notice. You know there's one example that everybody
keeps going to when they're trying to explain this and I don't like using the
analogies that everybody else uses but honestly I can't think of a better one.
Figure skaters. When they're spinning, if you raise or lower your hands while
you're spinning, it adjusts the speed at which the skater spins. And it's the same
basic premise. Again, right now we're talking about something that is
measured in milliseconds. So unlike most climate news, it's not doom and gloom, but
But it's something to note and it adds to the body of evidence that suggests a warming
planet is having significant impacts on the world as a whole and the dynamics around it.
One of the reports that I read said that if this continued with the higher end of emissions,
that it's likely that by the end of the century, this dynamic, where the water is, will have
more of an impact on the length of days than the moon.
We cannot foresee all of the impacts of climate change, but it's real and it's here.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}