---
title: Research Road EP 2
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=E66ebL0e1_Y) |
| Published | 2024/07/30 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
Today, you're at Research Road and it's episode two.
Today's date is the 30th of July, 2024.
This will be a weekly series, not unlike The Road's Not Taken,
but will cover science and related news.
We might even sneak in some sci-fi stuff.
OK, today we're starting off with tech news.
NASA and Sierra Space pushed an inflatable habitat module
to its breaking point.
The inflatable habitat is designed
to be part of a space station.
The structure is known as the Large Integrated Flexible Environment.
The module eventually failed and burst, but only after far exceeding safety requirements.
This tech might one day lead to an entirely different kind of space station.
China's high-tech and very secretive unmanned spacecraft is suspected of having military
applications.
The reusable spacecraft may also have the capability to operate against satellites.
Now we're moving on to out of this world news.
On Mars, NASA's Perseverance rover reportedly found rocks with a distinctive leopard-like
pattern of spots.
On Earth, these spots tend to indicate a fossil record of microbial life.
Two meteor showers coincided, and according to reports this morning, somewhere between
15 and 20 meteors per hour were estimated to be visible.
The showers will remain visible until August 21st, though at a slower pace.
And on to everyone's favorite topic, climate change.
Project 2025 reportedly calls for the reduction or elimination of the National Oceanic and
Atmospheric Administration and the National Weather Service.
The two services are imperative for keeping Americans informed about extreme weather and
climate news.
A tropical system is expected to develop and approach Florida this week.
A study suggests that as the Great Salt Lake dries, it is releasing millions of tons of
CO2 emissions.
Moving on to animals, wild chimps are said to share human communication styles.
The animals communicate mostly with gestures and facial expressions.
Those observing the interactions have noted that the chimps have fast-paced conversations
and even sometimes interrupt each other.
In health news, more mosquito samples in Texas reportedly tested positive for West Nile virus.
About a third of human cases in Texas have been severe.
Be sure to take precautions.
Now on to news about digging up the pest.
Some cave art on an Indonesian island has scientists rethinking the length of time humans
have been expressing creative thought.
The painting of a wild pig and three humans is being dated to more than 51,000 years ago.
That's 5,000 years before other cave paintings.
Thirty-four million-year-old fossils of snakes in Wyoming are informing scientists' opinions
of how snake diversity and evolution occurred.
Now on to oddities.
A NASA spacecraft has reportedly sent back information indicating the planet Mercury
might have a 10-mile deep layer of diamond under the surface.
So that's all the data we have to date.
Hope you enjoyed it and hope to see you again soon.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}