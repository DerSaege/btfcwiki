---
title: Let's talk about Harris VP picks, who's in, and who's out....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=QG_n5mieUyU) |
| Published | 2024/07/30 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
It's Miss Bo.
You may not have seen me before, but you have heard of me.
So today we're going to talk about Vice President Harris
and her potential running mates.
We're going to talk about who's in and who's out.
North Carolina Governor Roy Cooper has withdrawn.
Michigan Governor Gretchen Whitmer
has gone on record saying that she is not
in the vetting process.
It appears that she just wants to finish out her term
as governor in Michigan.
Roy Cooper has gone on record as saying
his only goal is to defeat Trump. He is not interested in political jockeying or
anything that would distract from that goal. So those two are out, but there's
still quite the list. In no particular order, it's Kentucky Governor Andy
Beshear, Senator Mark Kelly of Arizona, Minnesota Governor Tim Walz, Pennsylvania
Governor Josh Shapiro, and Transportation Secretary Pete Buttigieg is seen as an
outsider but still in the list of considerations. Harris has not come out
and said anything. The Harris campaign is doing a lot of research and private
polling trying to find the best person to shore her up for the demographics and
states that she needs to secure the election. So we're waiting. August 7th is
the Ohio deadline so she has to make a decision by then. But at this point all
All of it is speculation.
We're all just waiting.
Anyway, it's just a thought.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}