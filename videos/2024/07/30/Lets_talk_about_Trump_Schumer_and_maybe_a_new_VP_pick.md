---
title: Let's talk about Trump, Schumer, and maybe a new VP pick....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=OP6VJb2Snzg) |
| Published | 2024/07/30 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about former president Trump and his choice of
running mate, his VP pick and something that Senate Majority Leader Schumer said.
And then the conversations that followed because something was brought up and it
just set off a wave of conversation.
So we will go over what Schumer said
and what the Trump campaign has said about this.
OK, so for those who don't know, former President Trump
chose JD Vance as his running mate,
his vice presidential pick.
When that occurred, people who were political commentators
came out and were like, hey, that's surprising and probably
not the best choice.
Since the selection was made, those statements have become more and more pronounced.
And then you had this from Schumer.
The addition of JD Vance to this ticket is incredibly a bad choice.
I think Donald Trump, I know him, and I think he's probably sitting and watching the TV
and every day it comes out Vance has done something more extreme, more weird, more erratic.
And I bet Trump is sitting there scratching his head and wondering, why did I pick this
guy?
Okay, all of this, this is normal politics.
This is something you would expect the Senate Majority Leader, Schumer, who is a member
of the Democratic Party, to say about the Republican VP pick.
There's nothing really here.
But this was the lead in.
He went on to say that Trump only has a few more days, I think at the time it was 10,
before the Ohio ballot is locked in and he has a choice.
Does he keep Vance on the ticket and then he goes on or does he pick someone new?
And that is something that has sparked a lot of conversation because Schumer is pretty
plugged in. Schumer hears things. He's been around up in D.C. a very, very long
time. So it has led people to believe that this is being considered. Now it is
worth noting that the Trump campaign has repeatedly said they're not looking at
replacing Vance and that they're happy with their choice. This has come out in
numerous ways through numerous surrogates. Now the question is what do
you believe? The rumor mill that it's being considered or the statements from
the Trump campaign? Honestly when it comes to this normal political math I
could see a campaign considering replacing Vance. That being said this is
not a normal political campaign, it's Trump. If Trump replaces Vance, Trump has
to admit that he made a mistake. That seems incredibly unlikely. That seems
incredibly unlikely. It's not what Trump normally does when you're talking about
a campaign. So it's worth acknowledging that both ideas here, replacing him or
leaving him, they both have strong arguments in their favor. What's going to
happen? We don't know. I don't know that Trump knows. There is time to make a
switch who that would be no clue if it was to even occur but it is one of those
things that Trump has to consider because when this election was made the
the campaign was different now you have Harris though momentum is swinging her
way the entire dynamics of the race is different so it wouldn't be completely
surprising if he did, but it would be unusual. But either way, we'll find out
soon. Again, there isn't much time before the Ohio ballot is locked in, and Trump
has to have that Ohio ballot. So it isn't outside the realm of possibility that we
We see another massive shift in the dynamics of this race.
But at this point, it's all speculation coming from all over the political spectrum, and
the statements we have from the campaign say they're not even considering it.
Anyway, it's just a thought.
have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}