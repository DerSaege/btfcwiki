---
title: Let's talk about the person running against Boebert....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=uZT50mOnxOI) |
| Published | 2024/07/30 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about Tricia Calvaryse.
Tricia Calvaryse.
That is the woman running against Lauren Boebert
out there in the 4th District of Colorado.
If you are not aware, that is a super, super red district.
Recently, over on the roads, a question came in about her.
And I didn't think much of it at the time,
But afterward, I got a number of messages from y'all.
And they all pretty much said the same thing.
Hey, you need to look into her for these two reasons.
One, she's very much a union person.
And two, you need to look into her story.
And that's it.
No explanation beyond, you need to look into her story.
I did not know I was in for a Lifetime movie.
Thanks for the heads up.
So I started trying to piece together
how she wound up to be running against Bobert.
Now, I could be off in a little bit of this
as far as order or something like that.
But as I understand it, she grew up
in Stirlings and Highland Ranch, which is the area where she's running.
Did well in school, went to Johns Hopkins.
At some point she was a, I want to say a speechwriter for a union or a labor organization.
And then she was working for the National Science Foundation.
And then she had to move back home because both of her parents had cancer.
And she moved back to Colorado to take care of them.
And as I understand it, both of them were conservative, very Republican.
Again, it's a very red district.
But if I have this part of the story right, it certainly seems like maybe she was expressing
her displeasure with the current political state.
And I guess her dad was like, you know, don't talk about it, be about it, step up, do something.
So she's running.
Now as far as endorsements, Colorado, AFL-CIO, United Mine Workers of America, Coalition
of Black Trade Unionists, and just like a giant list of union endorsements.
Also a bunch from Colorado politicians.
As far as platform, it really seems to center on healthcare, big surprise, you know, here
in the story but I think it's again I don't know this but it seems to be very
much about the political dynamics and the rhetoric and all of that I think
that has a lot to do with it so that is who is running against her that's who's
running against Boebert. Now understand, I mean it's a long shot. This is a very,
very, very red district. That being said, for a long time the idea is that all
politics is local. She is and Boebert's not. So there might be an upset here, it's
one to watch, but it is definitely interesting. If you are in that district,
might be worth taking a look at. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}