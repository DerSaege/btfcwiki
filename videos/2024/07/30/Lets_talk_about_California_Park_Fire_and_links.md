---
title: Let's talk about California, Park Fire, and links....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=v45LjliLaLs) |
| Published | 2024/07/30 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, let's bow again.
So today, we are going to talk about California
and go over what's occurring,
provide some tips if we can, and some resources,
and just kind of keep everybody up to date
on what is happening out there.
If you haven't heard anything about it yet,
It is being called the Park Fire, and it is big.
At time of filming, it is the sixth largest
in California state history.
I say it that way because when I started pulling information
for this video, it was the seventh largest.
It is big.
I want to say the last thing I saw
was 360,000 acres, which is more than
560 square miles. Big. As far as structures, I want to say 130 to 140 have
already been lost. Thousands have been evacuated. It is so big it is creating
its own weather. The current theory is that it was allegedly started on purpose
on July 24th. Okay so there's going to be links down below to provide more
information just in general about how to deal with wildfires and how to stage
your home and you know what to have ready to evacuate all of that stuff and
then stuff specific to this fire as well. But quick tips if you are somebody who
may end up being evacuated, make sure you have two evacuation locations that you
can go to with different routes to get to each one because you don't know
what's going to be open due to the fire, due to smoke, due to traffic, so on and so
forth. One of the things I always recommend is to have somebody out of
state to serve as a point of contact, meaning if you have multiple people that
are going to go to an evacuation location, you're going to be traveling in
different ways. Have somebody out of the area who you know their communications
will be working that you both know to contact. That way information can be
passed back and forth. It's less likely that their communications get disrupted.
Make sure you have extra clothes, you know, your evacuation bag, copies of your
documents, stuff like that. Make sure you have a plan for your animals. Now, I will
find links and put down below because the best practices on this stuff always
changes, but the last time I looked this stuff up, firefighters were advising to
make sure that you kept your doors and windows closed, but your lights on and
the doors unlocked so they could get in if they needed to. The lights help them
find it in the smoke, I guess. Make sure that you have a ladder outside near the
corner of your house. I think it was advised to move your furniture towards
the center of the room. Make sure that gas, AC, propane, all that stuff is turned
off. Grills, move them away from the side of your house. I think one of the big
things that pretty much any firefighter that I have ever talked to about this
subject as mentioned, is make sure you turn off your sprinklers, anything that
could impact water pressure for them. So again, I will have more information and
links down below. Just for those who are in this area and help with relief of
this kind, you've got your work cut out for you. There's going to be a lot
evacuated and it is it's a big one. Anyway it's just a thought y'all have a
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}