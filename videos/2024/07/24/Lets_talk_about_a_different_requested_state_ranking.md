---
title: Let's talk about a different requested state ranking....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=U5vWkZNlzos) |
| Published | 2024/07/24 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about another state ranking,
a different one.
Recently, we talked about a model
that kind of combined a whole bunch of different things,
and it came up with a good basis
for looking at what the general quality of life of a worker
in the state would be.
And we talked about the 10 worst.
Somebody did not like that particular metric,
and they requested something instead.
And I figured I would do it because I haven't done a monkey
paw video in a while.
Okay, I saw your stupid video, there's another word there,
about quality of life.
I noticed you didn't take crime into account.
You used some woke bottle.
What about the most dangerous states?
Are those going to be red or blue?
You know the states where you get killed
walking down the streets.
They're Democrat states.
You waited to give people a false impression.
You're not fooling anybody.
Show us a model on where it's most dangerous.
OK.
First, I would like it noted that in that other bottle,
crime actually is factored in.
Just putting that there.
And also, I didn't create the model.
Anyway.
So I just went ahead and looked for what you asked for,
a model of the most dangerous states.
This is from Forbes, and it's from March of this year.
Number 10, Alaska.
Number 9, Nevada.
Number 8, South Carolina.
Number 7, Missouri.
Number 6, Tennessee.
Number 5, California.
Hey, look, there you go.
Number 4, Colorado.
Number 3, Louisiana.
Number 2, Arkansas.
Number 1, New Mexico.
Now, you did seem to have a problem
with like weighted models.
So I went ahead and took the liberty
of just pulling the violent crime rate,
the per capita rate,
because that seemed to be what you were worried about.
So we'll go through that one too.
It does change it a little bit.
Alaska is no longer number 10.
Number 10 is Michigan.
And then it goes to Missouri, South Carolina,
Colorado, California, Tennessee, Louisiana, Arkansas,
Alaska is now number two, and New Mexico.
I'm going to be honest, number one and number two
both surprised me.
They actually have a pretty high rate there.
I'm willing to bet that is not what you were expecting to
here, just as a guess.
A lot of what people believe is not accurate.
When you look at violent crime rates in particular, generally speaking, it actually is red states,
just to be clear.
That's why that other model that factored those in, even
though it was weighted with other woke things, like
wanting to have rights and clean air, that's why it
looked the way it did.
So there's that.
You could avoid this in the future by using Google.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}