---
title: Let's talk about primaries and democratic process....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=EHONDr42pQo) |
| Published | 2024/07/24 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there internet people, let's bow again.
So today we are going to talk about the democratic process
within the democratic party.
And we're gonna answer a question that keeps coming in
and I haven't seen anybody really kind of break it down.
So we're going to do that now.
The question is basically,
why didn't they have another primary
after Biden decided he was going to decline the nomination.
Why didn't they let everybody vote again?
Okay, first, I don't actually know
that there's a mechanism to do that, okay?
We'll start with that.
But we're gonna skip over that part
because I'm sure some people would say,
well, you make one or something like that.
Okay, so you want to have an additional primary
from that point.
So what you have to do is you have to condense
all the timelines.
You have to do everything very, very quickly
so you can get it done.
So we're gonna say that from the second,
I mean the absolute second,
that Biden said he wasn't gonna pursue the nomination,
that candidates,
people who wanted to be part of this new primary,
they had two weeks, two weeks, that's it,
to get the signatures necessary in the various states,
to set up what they need to,
to be represented at different caucuses,
and so on and so forth, okay?
Two weeks.
And then we're gonna allow three days
to verify those signatures.
know the signatures necessary to be on the ballots three days to verify and
challenge them and then you're out of time that's why even if there was a
mechanism to do it there was not time period now I know people would say okay
well then let's just dispense with the signatures well so who chooses who gets
on the ballot, the DNC, right? You're right back to where you started. Because the people
who have this question, most of them don't like the idea of the Democratic establishment,
just the word used most is anointing somebody. They would do that with the candidates. That's
why it didn't happen. You had from July 21st to the 7th. That's your time period.
I mean you don't really have enough time to even print the ballots and roll out
the machines, much less schedule an election. I mean think about all of the
counties, all of the voting locations, everything that happens. It's just, it's
not there. That's why. Now, any way that you try to speed that timeline up, you
end up back with the Democratic establishment just choosing candidates
and maybe doing like an online poll or something, but it's still the same thing.
thing. It was a matter of time. Even if there is a mechanism to do it, the time
is just not there. And then you have the question, and the reason it comes up is
because, you know, we have to throw away democracy to defend democracy. No, this is
an inner party thing. It's not an election for a government office. Those
aren't quite the same. I mean I understand the sentiment, but I think a
whole lot of people who voted in the primary for Biden would say that they
voted for Biden knowing Harris was his VP and that that change should follow.
I would point out that I couldn't find anything in the way the Democratic Party
set up to say that that chain has to be followed as far as the nomination is
concerned. I don't actually think that there is any mechanism like that but I
think there would be a whole lot of people that felt that way. Now you could
say we need to have an open convention and you're hearing that a lot because
you're hearing the media say that Harris has the delegates. There's a reason
that to my knowledge AP is still not referring to her as the presumptive
nominee because those delegates don't actually have to vote that way once they
get to the convention. I would suggest that as quickly as she shored up support
I don't think it would have mattered. Now you could make the argument that
there should have been a more lively primary the first time. I mean sure, okay.
I don't think it would have altered the outcome, but that's an argument that
could be made, you know, hindsight 2020 and all of that stuff. But it's worth
remembering that a lot of the people who who are being thrown out as people who
should have primaried him, they didn't want to. So it probably wouldn't have
altered the outcome even if you had a more lively primary and you would have
wound up back here anyway. So it isn't a situation where the the simple answer
was, well, we hold another primary. One, again, as far as mechanisms, I don't know
that they're there. And two, there wasn't time. Like, I don't even think you could
realistically schedule an election in the United States that quickly. So,
there's your answer. I'm not saying it's ideal, but there's a lot of people asking
about this and it really it boils down to calendar days and anything you do to
dispense with with the delays that occur it just hands the selection process to
the Democratic establishment anyway anyway it's just a thought you'll have a
a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}