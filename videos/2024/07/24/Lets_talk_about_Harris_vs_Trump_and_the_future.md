---
title: Let's talk about Harris vs Trump and the future....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=6kYyuu8R4A0) |
| Published | 2024/07/24 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Harris and Trump
and the future of American politics beyond 2024,
beyond that election, further down the road, 2028.
And even further than that, really.
This is something that we have talked about
numerous times on the channel
And a bunch of questions have come in about this since Harris has become the front-runner.
I have one here, it's just the most concise, that's the reason it was picked.
One of the things you've talked about repeatedly is Trump needing to lose decisively to get
back to a normal political cycle with less authoritarian politics.
I'm wondering how Harris fits into that math.
I understand how Biden winning decisively could cause that.
Is the same true for Harris?
No, I think it's very different.
The key element, I don't think Harris has to win decisively.
I don't think she has to win decisively.
When people are talking about this,
what most people are talking about
seeing Trumpism fade from American politics. You know, they put it in
broader terms, but it's really about Trumpism. Harris doesn't have to win by
as big a margin as an old white guy to show that the United States is rejecting
that. You know, when it's baked in to Trumpism, when people will say, make
America great again, you ask them when. During that period, would somebody like
Harris be even considered to be on the ticket, much less at the head of the
ticket, much less seeing a major party rally behind her, she doesn't have to
win by as big a margin to achieve the same effect. Her winning at all
demonstrates a rejection of something that is baked in to Trumpism on a
fundamental level, on a level that's so deep. There are probably people
who support Trumpism, who may not really fully understand that it's there.
Her winning, she doesn't have to just absolutely crush Trump at the polls to achieve that same
effect.
If she wins, I would imagine that you would see Trumpism fade very quickly.
Probably before 2028.
Now that doesn't mean that a new brand of authoritarian politics doesn't emerge.
But it would be different.
It wouldn't be centered around a personality.
I imagine it would be more like the Tea Party in how it functioned.
That may happen, but I would suggest that any win by Harris would be as effective as
a huge win by Biden at demonstrating to the Republican decision-makers that that
brand of politics is not a winning political strategy because electing
Harris doesn't just reject the policies, it rejects something on a more
fundamental level that is baked into it. So, no, it's not the same. Is the
same true for Harris? No, it's very different math. Her win, if she wins,
that's, that's gonna be, I'd be super surprised if there was a, a Trumpism
faction that still remained in 2028. Again, you would have something similar,
some way for them to kind of transfer, but it wouldn't be the same in any way.
Harris winning, even by a small margin, that's going to spell the end of Trumpism as we know it.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}