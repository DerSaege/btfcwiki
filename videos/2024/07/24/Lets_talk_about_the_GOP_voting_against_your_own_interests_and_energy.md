---
title: Let's talk about the GOP, voting against your own interests, and energy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=jwqZetDqzG0) |
| Published | 2024/07/24 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about the Republican party, conservatives in
general, we're going to talk about energy and we're going to talk about voting
against your own interests and we're going to do this because I got a
message, you've said more than once that Republicans have us voting against
own interests when you were talking about the Green New Scam. I don't dislike
your show. I like it enough to have watched enough to know that you like it
or at least answer direct questions about policy. So I'll try to keep this
short. Please explain how wanting to keep our infrastructure the same and
independent by drilling more is against my interests. I got you. I got you.
keeping it the same. That's a big part of it because people generally are
reluctant to change and that's especially true of conservatives.
That's kind of the whole basis of the ideology really. So how's the system
work right now? Stuff is yanked out of the ground and if you want to drill more
here to be independent, more of that will be occurring here, there is a higher probability
that there will be accidents that will damage the water, air, and soil.
It damages the water, air, and soil anyway when the energy is generated using whatever
is extracted.
And it costs you more, right?
cost you more because oil is a global market and it's constantly, it has a lot
of pressures on it. So you pay for it. Okay, so I'm going to suggest that it is
not in your interest to have dirty air, water, and soil. I don't think you would
argue with that, okay? You may not put a high premium on it but I would suggest
you believe it is in your best interest to have those things. Green energy
incidentally, I don't really talk about the Green New Deal, haven't in years, but
green energy is better in that regard. Spain has done a lot with green
energy so much so that demand can't keep up with supply at times which means the
price of the energy drops. I'm going to suggest that it is in your interest to
You pay less for energy, and then you want to be independent.
You want to be independent so you are not reliant on other countries.
One of the big talking points that comes out of Republican circles, and honestly, I wish
you had used it, is that even if we change, it's not going to help the environment because
China, well, they're not changing. China has two-thirds of the world's solar and
wind projects right now. Two-thirds. What does that mean? Long-term. Their companies?
Oh, they? They're going to be well practiced so as the world transitions who's
going to get those jobs? Where's that money going to go? It's not coming here. It's going there.
As that technology gets exported, as the infrastructure gets exported, as the panels
and everything, as that happens, it's not going to be going to U.S. businesses.
Whether you like it or not, the transition's coming. That is going to occur. The question is,
is, do you really want to sit there and scream,
hey, I want to use an antiquated technology that
is really dirty, causes health issues,
and damages the environment all around me,
and may lead to real, real big issues in the very near future
while paying more?
I want to do that while paying more and losing
jobs in the future. Does that really sound like it's in your
interests? Because that's what they've got you doing there when
it comes to switching to green energy, when it comes to the
transitions, there is no downside for the consumer. Now,
the reason you get this kind of pressure to stay with it, do you
know what's happening in Spain? One of their big concerns is because the energy
is so cheap for the consumer. It is so cheap for the average person. They are
concerned that they're not gonna be able to get investors for more projects
because they're not making enough money. Because they're used to what they're
getting from you for the energy they want to keep. And they've got you saying
no please charge me more to damage everything around me and keep doing it
long enough so all the jobs go somewhere else. And that money doesn't come here.
Yeah, that has you voting against your own interest. If you want, feel free to
to send me a message back. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}