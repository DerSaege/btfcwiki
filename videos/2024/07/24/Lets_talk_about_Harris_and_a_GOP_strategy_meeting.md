---
title: Let's talk about Harris and a GOP strategy meeting....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=sbmlTaQyW5Y) |
| Published | 2024/07/24 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about a Republican meeting,
a GOP strategy meeting dealing with how they wanted
to handle the Harris campaign.
And we're going to focus on one point in particular
that was made.
There's going to be a video coming out on the channel.
It was actually going to go out right now.
In that video, I talk about how there
is something that is baked in to certain segments
of the Republican Party.
And I knew when I said that that there
were going to be people asking for a specific, incredibly
recent example.
And honestly, right off the top of my head,
I couldn't think of one.
But now we have this.
So we'll go ahead and talk about this now.
And then you can see the other video when it comes out.
This meeting, it was party leadership
talking to other national Republicans, politicians.
And afterward, I think it was afterward, publicly,
Speaker Johnson said this.
Her ethnicity, her gender has nothing
do with this whatsoever, talking about Harris. I mean, that's a unique statement
because according to the reporting, one of the points that was made during this
meeting was the leadership, the strategists telling the other Republicans,
the other Republican members to stop commenting on her race. Why? Because a
a whole bunch of Republican politicians and people in the the public eye have
been saying that she was a DEI candidate and they definitely were putting an R in
there. And other comments about her ethnicity, about her gender. They had to
come out in a strategy meeting and say, hey, maybe don't make these kind of
comments. I'm going to suggest that if that has to occur, maybe that's baked in
to certain segments of the Republican Party. It gets better. One of the people
who was in the meeting said, quote, we'll give you a cheat sheet if you don't know
what else to talk about. So the leadership feels that they have members
who not only don't know that that's bad, don't know that it's not a politically
tenable position but they're also worried that they don't know what else
to talk about. I'm going to suggest that that's something that's probably baked
in to certain segments of the Republican Party. Now to hear the Republican
leadership talk about it. They want to make it about her record. Okay. I'm
fairly certain based on what we've seen as far as the out-of-context clips that
have been circulated and stuff like that, that means exactly what we thought it
meant. They want to take things that she has done in the past, present them out of
of context and make her seem weird, make her seem flighty, you know, just some
ditzy woman with her silly lady brain, something along those lines. That's
probably what they mean by that. Now, I mean, I could be wrong. Maybe, maybe there
are some people in the Republican leadership who are like, maybe we
actually should try to have like a policy debate. I find that incredibly
unlikely but it could mean that as well. My guess is that it means the other
thing. Now even though this statement went out and they were told not to do
this anymore and there's even the offer of a cheat sheet if you just don't know
what to say, you know, if you just can't help yourself and you feel like you have
to say something about a race, they'll give you something else to say. Even
with that, I don't think that that's going to stop. I think it's going to
continue. Because, again, in certain segments of the Republican Party, I
believe it's to be I believe it's baked in. So we got a glimpse of what the
Republican leadership wants the campaign to look like okay either they
legitimately want a policy debate seems incredibly unlikely or they want to kind
of cast her as flighty and loopy and you know too weird and emotional to hold an
office like that. And we also got a glimpse of what they're concerned about
and it seems that one of their biggest concerns is that their own members are
are going to say things that basically make her more popular because their own
members end up making racist statements about her. I'm going to suggest that
that's not something I say often. The Republican leadership is probably right
about that. If they continue down that road, it's not going to help them. That is
not a politically tenable position. And if it drives people to vote, it will
definitely sway the election. So that's a little glimpse of what the Republican
party would like and what they're worried about. And it certainly appears
that one of their biggest concerns is their own members being racist. Anyway
It's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}