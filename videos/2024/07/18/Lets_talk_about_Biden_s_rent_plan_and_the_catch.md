---
title: Let's talk about Biden's rent plan and the catch....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=CpgNuACKay8) |
| Published | 2024/07/18 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about President Biden,
housing, rent, his plan that was just announced,
the questions that came in about it almost immediately,
and then the big catch that goes along with this plan.
Okay, so rough sketch of the plan.
It's relatively simple.
caps rent increases at 5% per year for entities that have 50 units or more or
maybe more than 50 units, one of the two. And then there's a little bit more about
public land use and surplus government land that might be used for creating
more housing. It's interesting, but realistically it doesn't seem like a
game-changer. The cap on increasing rent seems more important. There are
two exemptions to it. New construction and those places that were substantially
renovated. And that's where all the questions came in. That's what it was all
about. And generally the question is why? Why exempt new construction and places
that were substantially renovated? To try to address the route, if you incentivize
creating new construction or renovations of buildings that already exist, you
create more housing. You create more housing, you increase the supply, the
price should come down. That's why those are in there. So this is a major issue
for a whole lot of people, especially those people who live in places that
housing has, well frankly, got out of control as far as the price. Now, overall
that sounds like a plan that most people watching this channel would definitely
be behind. What's the catch? This requires Congress, which means you won't see any
action on this unless he wins the election and there's enough Democrats
elected to the House and the Senate to get it through it because it seems
incredibly unlikely that the Republican Party is going to have a change of heart
all of a sudden and get behind something like this. So you probably won't see it
move forward until after the election. And that's only if the Democratic Party
comes out on top. I could be wrong. You might have some Republicans in the House
that are in districts that have a real shortage when it comes to housing that
might cross over on it. But I don't know if the votes are there and I certainly
don't know if the votes are there in the Senate. So it seems unlikely that this is
something that would move forward unless the Democratic Party comes out on top of
November. So that's the plan. The questions that came in, it's really about
supply and demand. If they can incentivize increasing the supply, the
price should go down because there's more of it available. So anyway, it's
It's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}