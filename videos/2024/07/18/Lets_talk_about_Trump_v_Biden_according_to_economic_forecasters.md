---
title: Let's talk about Trump v Biden according to economic forecasters....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=i3v_4ROj79U) |
| Published | 2024/07/18 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump and Biden
and economics and inflation and deficits
and what the pros say
because there's been a lot of discussion about this
when it comes to political commentators.
So, what we have is a look at a Wall Street Journal survey of professional economic forecasters.
These are the people who theoretically, they know what they're talking about.
And they were surveyed between July 5th and July 9th.
And this comes on the hills of 16 Nobel Prize-winning economists coming out and saying that Biden's
policies, Biden's economic agenda is, quote, vastly superior to Trump's.
And that Trump's, well, it would lead to a situation that would reignite inflation.
So Wall Street Journal, not exactly an outlet that is known as being super friendly to the
Biden administration.
So what did they say?
What did the survey say?
When it came to inflation, 56% of those professional forecasters surveyed, they said that Trump's
policies would lead to greater inflation. 16% said Biden's would and 28% said
there would be no real difference between the two. So you have 56% to 16%.
What about deficits? 51% said that Trump's economic policies would lead to
greater deficits that they would be worse and 22% said Biden's would. So you
have at this point a developing consensus among the people who this is
their thing that Biden's economic policies are actually superior. You also
now have stuff dealing with the normal people economy. You know, as we have
talked about, when you talk about economics most times, the numbers being
discussed, those are numbers that matter to rich people. The numbers that matter
to the average household, those numbers are starting to shift as well under
Biden. I will put that video down below if you haven't seen it. This is one of those things that
the Biden administration, the Biden campaign, they're really going to have to get it out there
because there is a perception, a nostalgia for Trump's economy that realistically doesn't line
up with the reality of Trump's economy even before the pandemic.
There's a reason that economists are saying this.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}