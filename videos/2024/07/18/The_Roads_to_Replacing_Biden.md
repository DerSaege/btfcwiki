---
title: The Roads to Replacing Biden
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=wPRxuOWkr1E) |
| Published | 2024/07/18 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
It's Beau again, and welcome to The Roads with Beau.
Today, we are going to talk about the roads
to replacing Biden.
Replacing Biden on the Democratic Party ticket.
We're going to do this because it's a question
that has come up a lot, and my guess is that
it's gonna continue to come up for a while.
And I received a question that is phrased in such a way that it allows us to explore
various options, go down different paths to stick with the channel theme.
Okay so here's the question.
Now that you have a lot more Dems calling for Biden to pass the torch, are you willing
to give up on the old man?
I'm just wondering, we hear that a lot of Democrats question him in private.
I'm wondering what you say in private.
Is it different than what you say in public?
You know, when I first read that, it seemed very snarky.
Rereading it, it doesn't seem as snarky.
But snarky or not, it's a fair question.
A couple of points I want to make real quick, though.
First, I'm not a Democrat, just a reminder.
Second, Biden's not actually my man.
Go back and look at some of the videos from before he took office during the primary.
He's not my guy.
He has pleasantly surprised me.
He has far exceeded my expectations, but those expectations weren't very high.
So that's just a reminder.
Now as far as what I say off camera, the answer to that is not much.
I think one of the things that would be most surprising to a whole lot of people is that
I'm a pretty quiet guy.
I don't talk a lot, which is, I mean, funny given, you know, this.
But generally, I listen, and I ask questions.
I have a bunch of those.
So we can use that.
Those are things that I haven't necessarily talked about on the channel.
Concerns that I have.
Okay.
So we hear that all the time.
Biden needs to pass the torch.
Okay.
first question. And the answer is, well we don't know. I'm gonna suggest that
that's a bigger issue than people think it is. Okay, we don't know who, but we do
have this list of people who, you know, they poll better in some polls in some
states at some times. Okay, fair enough. Do they want the job? I'm asking that one
as a question, but I happen to know that some of them don't.
Some of the names being floated do not want the position.
They wouldn't take it if they didn't have to campaign for it.
They don't want to be president, which, I mean, to my way of thinking, that actually
qualifies them to be president more than anybody who actually wants the job.
But that's not how our system works, and until we can figure out how to draft people
into the role we need somebody who would actually want to campaign. So you have
that. Now that's only some of them though. There are some left, but we don't know
who. Are they going to vie for that position? Is there going to be a
competition, a little, you know, behind closed doors, you know, Washington
two-step thing going on trying for political jockeying trying to get ahead
and if so when the inevitable leaks come about the candidates that are released
by the other candidates trying to shore up their position so they end up with
the nomination are the poll numbers that you are currently looking at going to
hold? What about when the GOP starts slinging mud at them? Because you know
right now it's kind of all based on vibes. I have a whole lot of questions
about the who, a bunch. Then you can kind of move on to the how. How are you going
to get them on the ballot? And I know that sounds like a silly question because
they'd be the Democratic nominee. But do you remember all of those boring videos about the
paperwork over on the other channel when we were talking about Alabama and Ohio with Biden having
a hard time getting on the ballot? If the Democratic Party switches out candidates now,
it is almost inevitable that there will be a series of legal challenges. Now, will
those legal challenges be grounded in good interpretation of the law? Not
necessarily. So, I mean, even if they lose, I guess the Democratic Party could
appeal to where? The Supreme Court. Has good interpretation of the law, precedent,
long-standing, you know, things. Has that mattered a whole lot up there lately?
I have questions about that as well, and I don't know how they can know the answer
to that. That's a big issue. Then you have the campaign finance
questions. Now this was one that I have looked into. Those people that I have
talked to that really understand campaign finance, the general consensus
was, eh, there's ways, as in, it would be annoying, not impossible.
So you have that.
But these are pretty big questions.
They are pretty big questions.
The Democratic Party, the establishment within the Democratic Party, the leadership, they
They may have answers to all of these questions, they may have good answers to them, but those
answers aren't public.
And as far as public conversation goes, some of this I don't even know that there's been
real consideration of, much less resolution.
And I felt like that would be important.
To my way of thinking, it's very late in the game.
And if those people who want to switch Biden out, if they don't have answers to these
questions and good ones, I'm going to suggest that we're through the door.
We can't stop now.
That this is what it is.
Now if they do have good answers, that changes things.
If the Democratic leadership has answers to these questions and they have a candidate
who they believe is going to beat Trump in a landslide and it is guaranteed, well then
do it.
Do it.
Again, Biden's not my man, I'm not married to him.
But we don't know that, for some of this, we don't even know that the questions have
been considered, much less that they have answers.
There seems to be a little bit of panic going on.
So I, what I would say, you know, my private comments, they're about that.
If they have answers to these questions, then okay, do it.
If they don't, stop talking about it.
You live in the social media age.
Nobody wants to throw their support behind someone who may not be the nominee.
Nobody wants to throw their support behind someone whose own party leadership is resigned
to them losing.
This is not how you create voter enthusiasm.
So either do it or stop talking about it.
I have questions. Now, going along with this, another question that came in, asked
about something that we talked about years ago, which was Biden acting as a
lightning rod. You know, he said he was going to be a one-term president, and
then once he, you know, got into office, he's like, well, I'll run. You know, Will
see and he kept it very open and there was a theory that he was acting as a
lightning rod meaning he was absorbing all of the mud that the GOP was going
to sling so the new candidate would be would be fresh and the other side to
that is that if he came out early on and said oh I'm a one-term president well
his presidency is effectively over, he loses his political capital because they know he's
not running again.
So supporting him doesn't carry any potential political benefit later.
I had somebody, actually more than one person, ask whether or not that's what Biden is doing
now.
I don't know.
I would be surprised if that's what's going on.
There's a difference between a whole lot of ambiguity and coming out and saying, I'm
definitely running.
There's no way that I wouldn't run and all of that.
If that's what's happening, you will know almost immediately.
Once Trump announces his VP, which by the time you all watch this has already happened,
it would occur very, very quickly after that because the goal would have been to keep it
under wraps until Trump makes his selection so the Democratic Party could pick people
to best counter that.
So if that's what's going on, we will have an answer very, very quickly, but that seems
incredibly unlikely.
That is a level of 4D chess that you do not see from the Democratic Party.
Again, just keeping it, well, I don't know if I'm going to run, yes, I'm going to run,
you know, going that route with it.
one thing, coming out and saying stuff like, you know, I'm not gonna stop running unless
the Lord Almighty tells me to, that's a little bit different.
That's a little over the top to just be acting as a lightning rod.
It seems unlikely.
But if, you know, things change, like immediately after Trump announces his VP, I mean, maybe?
I still don't think that's what it would be.
I would be looking for another reason that they made that decision.
I would be absolutely shocked if they had it planned out like that.
We talked about it, but a lot of things have changed since then.
At the end of this, you have this topic that I imagine is going to keep going.
It's going to keep coming up.
At the end of it, what we're missing to make a good determination is information.
To have a good opinion on this, there's a whole lot of questions that have to be answered.
Just do it?
That's a shoe slogan.
It's not a good political strategy.
There has to be a plan behind it.
It can't just be, oh, we got cold feet.
There has to be something else, and if there's not, if there's not a real plan, if these
questions can't be answered, I'm going to suggest we are through the door and everybody
needs to stop talking about it because it is self-fulfilling at that point.
If Democratic Party leadership is resigned to him losing, you're not going to have enthusiasm
when it comes to voter turnout.
That is not the type of messaging that impresses swing voters.
The Republican Party is running their candidate with all that that entails, and they are acting
like he is unstoppable.
For a lot of people, in the age that we're in, there are a lot of voters that are going
to be swayed by sheer confidence and the appearance of it.
I understand that it's something that people want to do.
There's a lot of ambiguity.
There are a lot of potential pitfalls.
There's a lot of questions.
Anyway, so there's a little more information, a little more context, and having the right
right information will make all the difference.
y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}