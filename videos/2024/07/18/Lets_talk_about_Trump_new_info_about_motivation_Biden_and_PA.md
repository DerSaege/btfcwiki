---
title: Let's talk about Trump, new info about motivation, Biden, and PA....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=lEQNTDomIzI) |
| Published | 2024/07/18 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about some news,
some new information that has come out
about the situation with Trump in Pennsylvania
and where that information might take us.
Okay, so there was a briefing of, I believe,
House lawmakers.
They were given information, some of that information
has come out and made it into the public sphere.
There are a number of things that
might be of note for those people who
are asking the question of why, looking
for that elusive motive.
OK, the first bit that is worth noting
is you have lawmakers who are saying
they were told that the suspect was diagnosed with a major depression disorder. Other people
who were part of this briefing are saying that they were told that he had searched for information
on major depression disorders. Which one is correct or maybe they're both correct?
we don't have that yet, but major depression disorder is part of it. There
are historic incidents in the past that have led to a similar diagnosis when it
comes to, I believe it was manic depression. So there is that. Another
Another thing that was discovered on the phone was photos of both Trump and Biden and searches
related to the dates where they would be places for both Trump and Biden, I say again.
I think one was looking at the DNC and then more for Trump events.
The more information that comes out about the suspect, the more this seems to be a more
convoluted motive, one that may not readily be explained by a political
motivation. Say again, just because something has massive political
implications doesn't necessarily mean that the motive was political. This is
shaping up to look like other situations where the motive is harder to define.
So that information is, it isn't the end.
There is still more information that could come out, but this is the type of thing that
It makes people who study this particular topic start leaning to it not being a directly
political motive.
So there's that.
Again, something we have to stress throughout this.
We don't know everything yet.
that the more information we gain access to, the less and less this appears clear cut.
Now that of course is going to spark conversations about whether or not it was appropriate to
have commentary about cooling rhetoric about political violence.
I'm just going to go ahead and say this now.
always appropriate to have conversations about schooling that kind of rhetoric I
know that there are going to be people who are going to say well you know we
had all of these conversations we talked about this and all of this was
happening and now doesn't even look like that's what it was those
conversations needed to happen anyway so I don't I don't see that as an issue
you. We will keep looking and trying to find out what was said. There is more
information coming out about how perhaps the Republican campaign had already
identified his family as strong GOP supporters, but I want to look into that
a little bit more before we talk about it. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}