---
title: Let's talk about Trump's new campaign strategy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=TLTGdDBhjIQ) |
| Published | 2024/07/18 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about Trump
and his new campaign strategy, the new messaging,
his new way of trying to present himself,
and whether or not it's gonna pay off for him,
whether or not it's gonna be successful,
and whether or not that messaging is going to resonate
with the voters he is trying to reach.
It, uh, it wasn't that long ago, just a couple of days really, that I mentioned
that I had heard that what happened, that the attempt, that it created a marked
shift in Trump, uh, the comments revealed that y'all were skeptical of that.
Uh, since then, what we know is that the campaign has embraced this.
Trump is expected to come out and start talking about unity to drop all of his wild rhetoric
and to present himself as a statesman.
That's the idea.
Now, what we also know is that the Trump campaign has edited speeches and not just Trump's.
According to reporting, the campaign talked to speakers at the RNC, gave them new talking points, probably told them
things to avoid, and according to reporting, they physically edited and rewrote some people's speeches to get rid of the
wild rhetoric, to recast under that unity framework.
So, is it going to work? Maybe. Maybe. That is something that might resonate with a whole
lot of swing voters. However, there's also a pretty big obstacle that the Trump campaign
has to overcome, Trump.
Since that initial messaging went out, since I heard that, there have been some comments
and some statements, social media stuff, from Trump that didn't really fit in with this
new image.
He has more than a hundred days until the election if he goes out there and preaches
the unity messaging and then at 2 a.m. on a Tuesday he gets annoyed with whoever and
gets on social media and let's just say undermines that messaging.
That will probably have a very marked effect on the swing voters.
He will lose the swing voters that this messaging appealed to and he will probably lose more.
The question is whether or not this messaging can last for more than three months.
That's the real question.
Trump has always been very much out front.
It's his way.
He's going to run his campaign the way he sees fit.
For this messaging to work, he has to be handled.
Everything that he has ever complained about, about other politicians, he is going to have
to do. He is going to have to let his speech writers do their thing with no
changes. He is going to have to have a more curated set of public
statements. In short, for this messaging to work, Trump can't be the Trump that
many of his supporters expect. We'll have to see how it plays out, but it is a
unique situation. It is something that I have a lot of questions about, both ways.
But, at time of filming, it seems incredibly apparent that the Trump campaign's plan is
to recast Trump and maybe make him a little bit more like Reagan.
That appears to be what they're going for.
If they're successful at it, he will probably pick up some veds.
If his campaign team can't keep him under control, it may have a very opposite effect.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}