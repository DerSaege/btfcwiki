---
title: Let's talk about Harris, Trump, and the debate....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=MPNaOR3SGAs) |
| Published | 2024/07/27 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Bob again.
So today we are going to talk about Trump and Harris
and the debate and the ambiguity that has arisen,
the perceived waffling and backpedaling
that is coming out of the Trump campaign.
And we're going to talk about what it's probably really about.
If you've missed it, the Trump campaign has basically said,
well they're not going to agree to that next debate, you know the one that was
already scheduled, they're not going to agree to that and confirm that they
would do it with Harris because well maybe she's not even going to be the
nominee. I mean sure that sounds good except the reality is he would want to
debate whoever the nominee is, right? It's not like he would forego a debate
because he's afraid of them, I mean that's not really what's going on. What's
actually happening? Because Trump knows he either shows up to the debate or he
doesn't. If he shows up they have a debate, if he doesn't he loses. He's gonna
show up. So why create that ambiguity? Because it's Trump, it's what he does.
He's not in the headlines. He sees the momentum going towards Harris and it
makes him nervous. His campaign is in the headlines because of his vice
presidential pick and it's not flattering. So he's trying to change the
story. That would be my guess. Why? Because it's what Trump's done for eight
years. My guess is that he will probably also try to use that ambiguity to kind
of maybe reframe some of the rules to the debate or maybe even the location to
make it a little bit more favorable to him because he doesn't want, he doesn't
want to create a situation in which Harris wins so he's going to try to use
that ambiguity to get an edge. I wouldn't lean into this too much. He's
going to show up and if he doesn't whatever that's his choice but the idea
that he's gonna pass it up and just basically forfeit and declare himself
the loser it seems incredibly unlikely the the reason he's saying this is to
create ambiguity to get himself back in the headlines to change the story from
everybody coalescing and supporting Harris. That would be the only reason to
really do this unless he's just gonna declare that, you know, the person who
isn't running, well, I did well in the debate against them. You don't need to
see me debate the actual candidate, you know. That doesn't seem like Trump. What
seems like Trump is that he's trying to change the story and get the headlines
back on himself because he knows that if it continues to go the way it's going
he's probably gonna lose. So he's going to try to to shift things. He doesn't have
bold policy initiatives to announce. His speeches aren't doing it anymore. This is
the way to get back in the headlines. That would be my guess. I wouldn't get
stressed out over this. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}