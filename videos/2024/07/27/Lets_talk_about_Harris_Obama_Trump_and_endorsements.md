---
title: Let's talk about Harris, Obama, Trump, and endorsements....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=-5V1Dh48GGk) |
| Published | 2024/07/27 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Harris and Obama and the Trump campaign and endorsements
and how everything is played out.
As Harris had the Democratic Party just come in behind her and show lots of support, there
was one particular person who didn't offer an endorsement.
that was former President Obama. And this created a whole lot of speculation. Now
when we talked about it before, the obvious answer to me was that he was
kind of keeping himself in reserve in case there was some kind of issue, there
were any shenanigans when it came to the nomination process, and whether or not
Harris was really going to get the support she needed to kind of clinch
it. And if it created a situation where there were competing people, he would be
able to act as the elder statesman, be a disinterested party, and kind of be a
mediator. He wouldn't be able to do that if he offered an endorsement. That is
the, to me, what seemed to be the obvious reason for him holding back his
endorsement. The Trump campaign felt differently. They went out on the media
circuit basically proclaiming through Trump surrogates that Obama wasn't
endorsing her because Obama didn't think she could win, and that he wasn't going
to endorse her because you know she's a big loser and all of this stuff, and they
made a big point about it, and they really leaned into that hard trying to
showcase some kind of division within the Democratic Party. Okay, so the
Obamas have publicly endorsed Harris now. So yeah, everything that the Trump world
said, that has now proven to be less than accurate. It was them kind of
wish casting and trying to create the image of disunity where it probably
didn't exist. Now with the endorsement what does that mean? Now that former
President Obama has come out and said yes we're behind Harris. What's the
likely read on it in the real world not Trump world? It seems to be a clear
indication that the support for Harris within the Democratic Party is it has
reached the point where it would be insurmountable for anybody who wanted to
launch a challenge. That's what it looks like. Now there's a whole bunch of
different ways you could measure it from the Zoom call to the way her TikTok
account expanded as quickly as it did to donations to a whole bunch of delegates
just openly coming out behind her. There's a whole bunch of reasons for it
but at this point it would take something drastic to alter the outcome
and it does appear that Harris is going to be the nominee. You know you really
shouldn't use the term presumptive yet but I mean yeah that that's that's where
we're at. If former President Obama felt comfortable enough making this public
statement those people who are trying for a different candidate they don't
have the support they need. Obama has always had a really good read on the
Democratic Party, so if he's come out and made that statement, it seems very very
clear that Harris is your nominee. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}