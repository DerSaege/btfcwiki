---
title: Let's talk about Trump, flags, and the Constitution....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=kP3yzWMBvZY) |
| Published | 2024/07/27 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we're going to talk about Trump and flags
in the United States Constitution.
We're gonna do this because Trump announced
a bold new initiative, a new plan,
and it involves both flags and the Constitution.
And we're just going to go over what he said and then talk about the realities of what
he said.
Okay so here is Trump.
You should get a one year jail sentence if you do anything to desecrate the American
flag.
Now people will say, oh it's unconstitutional.
Those are stupid people.
Those are stupid people that say that.
I mean, yeah, people will say that creating a law that specifically violates the First
Amendment to the United States Constitution is in fact unconstitutional.
People are going to say that because it is.
It's unconstitutional.
have a First Amendment right to speech, to expression. So his statement is that
if you're somebody who supports the US Constitution and can accurately
understand it, well you're a stupid person. That's what's here. I'll read
it again. Now people will say, oh it's unconstitutional. Those are stupid people.
Those are stupid people that say that. This is very clear. You have this right
in the US Constitution. So those people who understand it, they're stupid. What
What he is wanting is for you to value the symbol more than the document.
That's what's at its core here.
He knows that people are going to say it's unconstitutional because it is, but he doesn't
care.
Those people who would support the document rather than his word, those are stupid people.
Those people who choose the US Constitution over dear leader, those are stupid people.
And people will come up with all kinds of reasons, of exemptions to say, oh, you really
shouldn't do this.
Think about all the people who fought under that flag.
That's going to be the rhetoric.
Just out of curiosity, did they swear an oath to the flag or to the document that if you
believe in it you support it, well, you're stupid?
Which one?
It's a small infringement.
It's something he's saying up front.
You're going to be told this is unconstitutional, but you should support it anyway.
You should throw away the Constitution because it's what I want.
I feel like that would probably lead to some pretty bad places.
People will absolutely say this is unconstitutional because it is.
That doesn't make him stupid.
It makes him people who actually understand the document that he's supposed to, you know,
support.
Anyway, it's just a thought.
have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}