---
title: Let's talk about Trump, speeches, and changes....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=qOY9u5bcxzc) |
| Published | 2024/07/15 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump
and speeches and changes.
And I'll go ahead and tell you now this is going to be a very
unique video in the sense that I feel like the overwhelming majority of you
are going to immediately dismiss the information in it.
I feel comfortable saying that because I immediately dismissed the information
I got it. But it's the information I got so I feel like I have to share it. We will go over what you
can publicly confirm, what's publicly available, and then I will tell you what I heard in the
rumor mill. Okay, so Trump had a speech all prepared for the convention. That speech, short version,
Biden is the devil.
I mean, not quite that, but the idea was to just relentlessly go after Biden.
Normal Trump stump speech, just more excitable, more full of venom for the convention.
He has thrown it away, according to reporting.
You can confirm this in an interview.
he said it himself, and that instead he will be giving a speech on unity. All of
this makes sense from a political level. Because of the situation he
finds himself in, he has the ability to score some points, but that can only
occur if he is a sympathetic figure, and he is not. He is aware of this, so the
idea, present himself as a statesman, maybe pick up some swing voters and some
moderates. All of this makes sense. All of this is public information. The rumor
mill says that this profoundly affected him. To the point, what I heard was, I
I would not be surprised if he eliminated all inflammatory rhetoric from his speeches.
That is what I heard, that it truly affected him.
When I heard it, I immediately dismissed it, but at the same time I do have to say I know
people that have had close calls and that it in some cases it altered them on
the fundamental level. So I can't just reject it out of hand. I'm just
incredibly skeptical. It will be apparent if it's true. If it did provide some kind
of clarity about inflammatory rhetoric? Well we won't see any more of it. We'll
we'll see what happens. I just found it to be a very interesting statement that
didn't need to be made. So that's what I've heard. Now it is also worth noting
that this was entirely limited to rhetoric. It doesn't mean even if it's
true, there was nothing to suggest it would mean a policy change of any kind
would accompany his newfound position, I guess. Again, I know what the reaction to
this is going to be, but it's the information I have. We will see what
happens whether or not it is a fundamental change or it's just a show
for moderate voters if that's what it is it's Trump he won't be able to hold it
up for long you know he won't be able to put up a front so there you go there's
something to look forward to this week as everything gets started at the
convention. I would expect some surprises of some sort whether or not
they are long-lasting or real or for all we know we're going to see just
something that is frankly incredibly hard to believe but it's the information
I have and I have seen it in other cases. So we'll just have to wait and see what
the curves I guess.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}