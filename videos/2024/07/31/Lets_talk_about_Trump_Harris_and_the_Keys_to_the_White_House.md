---
title: Let's talk about Trump, Harris, and the Keys to the White House....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=7gH0IS14W30) |
| Published | 2024/07/31 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people. It's Ms. Bo again.
So, today we're going to talk about historian Alan Lichtman's keys to the presidency.
We talked about it on the channel before as it related to Biden, but now we need to take a look at Harris.
If you're not familiar with Mr. Lichtman's keys to the presidency, it's a series of 13 true-false statements.
And based on those answers, he's been predicting elections since 1984.
and if you don't count the 2000 election and the courts getting involved he's
had a pretty stellar record almost a hundred percent. So let's take a look at
Harris's 13 keys. Party mandate. That's a matter of did they pick up incumbent
seats in the House of Representatives during the midterms. For that Harris
gets a false. No primary contest she gets a true. Incumbent advantage is false. No
third-party is true. Strong short-term economy true. Strong long-term economy
is also true. A major policy change is true. No social unrest is apparently
true. No scandal, true. No foreign or military failures, she gets a false. Major
foreign or military success, she gets a false. Though I know a few people in
foreign policy and I'm not sure they would concur with that assessment.
Charismatic incumbent, she gets a false. We had questions about it from the last
video. When you say a charismatic incumbent, this is somebody that the
United States it's just the people are fully behind like JFK charisma and an
uncharismatic challenger that's true. So I've watched a lot of Dr. Lichtman on
media and he has his own YouTube channel if you're looking for more information
it's it's very interesting I find this subject fascinating but I've watched a
lot of his his YouTubes and he's not going to make a prediction right away it
should be coming soon but we're 90 days over 90 days till election day so a lot
of things could change but to put it in the simplest terms if the incumbent has
or the incumbent party has six or more false statements then they're not going
win. Harris has five false statements, so right now her numbers are looking good.
She's looking to possibly take the presidency, but we're gonna have to
wait and see. And again Lichtman hasn't predicted this yet. This is just where
she stands right now at the moment. So between this and the polling, we'll see
what happens. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}