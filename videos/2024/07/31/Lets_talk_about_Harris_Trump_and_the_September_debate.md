---
title: Let's talk about Harris, Trump, and the September debate....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=djoM6RSQUPs) |
| Published | 2024/07/31 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there internet people, it's Ms. Bo again.
So today we're gonna talk about Trump, Harris,
and the September 10th presidential debate,
the one that was originally scheduled
between Biden and Trump and how things are shaping up.
Trump has come out in a non-committal fashion saying,
and I quote, if you're gonna have a debate,
you gotta do it.
I think before the votes are cast,
I think it's very important that you do that.
So the answer is yes.
But I can also make a case for not doing it.
The Trump campaign has said that he will ultimately debate whoever the Democratic nominee is,
but Harris has accused him of backtracking.
But she's made it really clear, she's going to be there.
She's going to present her policies and whether he's there or not, she's going.
So there's been some discussion about perceived notions or potential reasons that the Trump
campaign may be holding back or may be concerned that Trump shows up.
He has a tendency to make off-the-cuff remarks, attacks, things that can be considered racist
or sexist, and the GOP is concerned that could drive voters toward Harris and away from the
Trump camp. It's a legit concern. With President Harris casting Trump as afraid,
it almost ensures he will show he is not going to be seen as a coward. He is not
going to be seen as weak. He'll be there. And it's important that he does show.
This may be the only presidential debate that we get. This may be the only time
for them to really shine and get their voices out there to the people. This is
where policies and your plans are going to matter. It's the chance to catch that
undecided voter. So it's a big deal. You know, we have 98 days left till the
election. The numbers in the polling that we have, it's very close. Vice
President Harris may have a lot of momentum, but this election is not in the
bag. This matters. Now, Trump may feel differently once her VP decision is
made, and that may make all the difference, but we're just gonna have to
wait and see. Anyway, it's just a thought. I hope you all have a really nice day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}