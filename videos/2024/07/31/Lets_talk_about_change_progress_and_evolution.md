---
title: Let's talk about change, progress, and evolution....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=UWyPPw6vv8M) |
| Published | 2024/07/31 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about change
and how things progress over time,
evolution, old ideas and new ideas.
When this channel started more than half a decade ago,
almost immediately, it started to foster a community
that had real-world impacts, that really got out there and did good.
That video the other day, I'll put it down below, it really drove it home for me because
that was just a fraction, just a fraction of what this community has done over the years.
And one of the things that we've always said that it's been a, I don't know, a guiding
principle on the channel is that ideas stand and fall on their own.
Not because of some perceived authority of the person saying it, not because of the messenger,
because the idea and the information is valid. That has always kind of been the
core here and luckily as the community grew that stayed a part of it. It's why
the channel never really succumbed all the sensationalism because that was a
key element and as the channel grew and the community grew the team behind it
did too. As many of you have noted I have been doing this for years without a day
off. And many of you cautioned that I was burning myself out. You were right. You
were right. So I am stepping away from the channel. As many of you warned
repeatedly, I was gonna over-exhaust myself. I did. The years have definitely
taken their toll, but I am so proud of this community and what it has done and
what it will do. And that brings us back to the team. You know, the people whose
work and efforts y'all saw for years, but you never saw them. You will still get
the same high quality, calm, non-sensational information you have been
getting on this channel for years. It's gonna be new voices, some new faces, some
of them are new, some of them are young, and there will be growing pains I'm sure
as they find their voice, but I am confident that as they find their voice
they will be as good or better than I was. I encourage you to stick around and
keep the community that y'all built and watch, take a chance on the new crop. The
same way you took a chance years ago scrolling on a YouTube channel and
clicking on a video of some weird looking redneck dude with a beard. You'll
be happy where it goes. The ideas they have for the second channel, they are
things that I always wanted to do, but did not have the energy. They'll make it
happen. They will make it happen. And with your support, it won't be just a dude in
the shed. It'll be a team providing a much wider array of content and
information in the same nonsensational manner that we all have grown accustomed
to. The channel will continue to have real-world impacts. It will continue
to help people at shelters or people on strike or people who are in the midst of
some situation which they need relief. There's not going to be a big change in
the community itself because it was never about a person. Ideas and
information stand and fall on their own and that can continue, that can continue
you and it will. So, right now what they have, they have a plan for a whole bunch of
stuff and I'm certain that they'll put it into action and I'm certain that you'll
enjoy it. This channel became way more than just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}