---
title: Let's talk about Harris and outreach to dirt road Democrats....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=SXAT7Otqzic) |
| Published | 2024/07/31 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
It's Miss Bo again.
So today, we're going to talk about a Zoom call.
Rural Americans, dirt road Democrats,
and Vice President Harris.
So I got a message.
A group called Rural Americans for Harris
is putting together a Zoom call, similar to other Zoom calls
that have already occurred recently.
And it might be something that a lot of y'all find interesting.
The Harris campaign statement says freedom
value we hold in our rural towns. We like to say what doesn't bother the cattle
doesn't bother us, meaning mind your own business. We also don't like big
corporations with greedy shareholders who take advantages of our small towns. So
obviously it's for the people who live in small towns. What do they mean by
small? Five to fifty thousand people. A couple of things that have been
discussed on this channel over the years come into play. First, the Democratic
Party generally doesn't have great messaging to dirt road Democrats. To be
honest, they barely have any messaging at all. And second, rural people, we live our
lives by lefty economic principles every single day, from the union to the co-op
to farm aid. The image of the stereotypical rural Republican comes
from the tradition of dad-voted Republican and from socially conservative
attitudes. But if those socially conservative attitudes are placed in
contrast with economic and kitchen table issues, there's another far more accurate
stereotype about rural people that come into play. What happens on your side of
the fence is your business. Republican rhetoric coming in and telling us who to
hate is barking at the wrong tree. Most times because we've been taught to love
our neighbor even from the other side of the fence. The problem for a long time
though is that Republicans have been the only dogs barking. This kind of outreach
it can only be good for Harris. It can only be good for the Democratic Party as
long as she focuses on the economic concerns and the infrastructure. If you
If you want to tune in and see what the Harris campaign has to say, it's August 6th at 7pm
Central Standard Time.
You can go to ruleforharris.com to RSVP.
We'll put the link below.
Just realize, if you do RSVP, you might be getting a few more of those messages.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}