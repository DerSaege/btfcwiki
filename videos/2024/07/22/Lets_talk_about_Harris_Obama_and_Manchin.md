---
title: Let's talk about Harris, Obama, and Manchin....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=OXU3zv_utUM) |
| Published | 2024/07/22 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there internet people, it's Bo again.
So today we are going to talk about Vice President Harris
and former President Obama and Senator Manchin.
And we are going to talk about the differences
and similarities between Obama and Manchin.
Yeah, let me tell you since I never thought I'd say.
Okay, so shortly after everything occurred
with Biden and Harris becoming the apparent front runner.
Manchin decided to offer his opinion.
Now, I know almost everybody watching this channel,
you were sitting at home wondering and waiting
for the advice of Manchin.
So let me tell you what he said.
He briefly entertained the idea of, I guess,
rejoining the Democratic Party and running
to get the nomination.
That did not last long.
He then said that he would not serve as Harris's vice president, has not endorsed her at time of filming,
and has called for a, quote, mini-primary.
Now, most political analysts kind of view the risks associated with that mini-primary
as something that could literally just hand the election to Trump,
something that most people would see as a bad move.
So what about former President Obama?
He has also, at time of filming, not endorsed Harris.
And he has signaled his openness to an open convention.
Why would he do that?
If I had to guess, he understands
the risk of a chaotic convention.
he is positioning himself as the impartial moderator, the elder statesman
who's been there, who can't be president, who doesn't have anything to gain, who
can help smooth over any issues at the convention. If he endorses somebody he
can't do that, it would make it very very hard for him to do that. My guess is that
he is kind of holding back to preserve his political capital to try to make
sure that that convention moves smoothly. So that is that's what's going on and
this is one of those instances where you have very similar very similar actions
that are probably coming from very very different places. I'm going to suggest
that the person who entertained trying to get the nomination suddenly saying
oh maybe we should have a mini-primary. Maybe they didn't really give up on the
idea of trying to obtain the nomination. I mean maybe he did and he just thinks
that this is the right move to you know trigger a bunch of democratic infighting
at the convention. Maybe there is some political motive that I do not see,
but I'm going to suggest that if things get too chaotic and too combative at
that convention, it will hand the election to Trump. This convention is way
more important than than any convention in most in most people's lives that are
watching this channel not all because some were around for for one a while
back but it is going to matter and it has to run smoothly it has to run
smoothly. It is incredibly important. So I would continue to watch these
developments. It is worth noting that despite, you know, the important mansion
endorsement not going her way, she has already picked up scores of endorsements
from across the Democratic coalition, from establishment Democrats to
progressive Democrats to to unions like they're just pouring in at this point. It
does appear that the overwhelming majority of the Democratic Party is
coalescing behind her. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}