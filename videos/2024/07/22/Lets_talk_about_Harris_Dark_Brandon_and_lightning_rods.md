---
title: Let's talk about Harris, Dark Brandon, and lightning rods....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=b9kd4tehkEA) |
| Published | 2024/07/22 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we are going to talk about Biden and Harris
and planning, long-term planning,
4D chess level of planning.
We're going to talk about the theory
and then just kind of go over some things
that might be worth remembering.
Okay, so, if you haven't heard about this, there's a theory, a lightning rod theory.
Basically, the idea is that Biden always knew he was going to drop out, but
he stayed in to continually attract the Republican mudslinging to save whoever
the eventual candidate is from being hit by it.
This theory has been around a while.
It's worth remembering Biden said that he wasn't going to run for a second term.
The first time I can actually find a video talking about it is in 2022 on this channel.
I'm fairly certain that we actually talked about it in 2020.
I just don't know where the video is.
So we talked about it then, and basically laid out,
not just is there a benefit to the eventual nominee
who would run in 2024, but for Biden as president,
if he says he's not running, he loses political capital.
So this has been discussed for years.
Then, recently, somebody asked about it over on the other channel, and I basically said,
well, if that's really what's going on, he'll drop out right after Trump picks his VP,
because that's you know it allows the Democratic Party to to pick candidates
that better that better combat the the Republican candidates okay so now all of
that's happened the obvious question do I really think that's what was going on
No, not really. It's a possibility. You can't discount it, but I would be
absolutely shocked if all of this was planned. Honestly, it seems to me like
initially he was keeping the door open and then he felt like he could win again.
so he decided to run again. Then political winds turned against him, so
he's dropping out. That's, when you look at the totality of the evidence, that's
really what it looks like. I don't like assigning 4D chess to people unless
there's a lot of evidence backing it up. The only evidence that we have for this
is that it's been talked about a long time. That it is a normal political move,
but given everything else that's involved here, it doesn't seem likely.
Now, I mean, I may be wrong. Maybe he really is Dark Brandon and two years from now
we're gonna find out that this was all planned and the poor performance at
the debate just gave him a good window to do it. Sure, maybe. I don't I don't
foresee that happening. I think that there was a lot of pressure put on him
based on polling and then the polling kind of scaring the the donors and then
the donors scaring the other politicians which then created the pressure for him
to drop out. That's what I think happened. It's an interesting theory. The lightning
rod theory is interesting. And as somebody who has talked about it for two years or more,
you can't completely discount it. But there's not any real evidence to support that that's
what occurred. So I wouldn't, I wouldn't turn Biden into, you know, some mastermind
on this one just yet. You need some kind of evidence to suggest that that's what
happened. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}