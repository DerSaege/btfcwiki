---
title: Let's talk about Biden stepping down and the new dynamics....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ucfeU5lXARc) |
| Published | 2024/07/22 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Biden and Harris
and passing the torch and all of that stuff.
We're gonna talk about the dynamics currently at play
and run through some information
that is important to understand.
If you have missed the news, which seems unlikely,
but President Biden has declined
to continue to seek the nomination for the Democratic Party for 2024, dropped out of the race.
There is a growing consensus around Harris as putting her forth as the nominee.
In some places, it's being treated like that as a foregone conclusion.
It is not.
There is no line where it just passes like that.
She still has to win at the convention.
She is getting support being thrown her way
from across the Democratic Party.
You've had the Clintons, you've had Elizabeth Warren,
Cori Bush, obviously Biden come out and provide support for her.
The FEC filings have already been changed.
Whatever it was before, I want to say the Biden-Harris campaign
has now been renamed Harris for President.
That has already happened.
paperwork is already in. Currently Harris is making literally hundreds of
phone calls trying to shore up support ahead of the convention which is August
19th, starts August 19th. And at this point we don't know how that's going to
play out. There could be other people who kind of throw their hat in the ring or
it could be very orderly. We don't know yet. The, and this seems like it goes
without saying, but the race has fundamentally changed in a whole bunch of
different ways. Trump is now the old guy. There's a lot of rhetoric that may come
back to bite the Republican Party. Now at the same time be ready for a bunch of
cheap political attacks on Harris. There's going to be tons and they will
probably echo the same sort of attacks that went against Obama for the exact
same reason. So that's where everything is at the moment. This is obviously a
dynamic situation. The question that has come in the most is, you know, how do I
feel about it? It's risky. It's risky. This was a decision made in large part
based on information, polling, and perceptions that I'm not sure were
accurate. I have a lot of questions about the practical logistics. I will put a
video down below from the other channel where I kind of go over all of them at
once. I don't have answers to most of those still. There has been an
indication from the Democratic leadership that they're going to put out
information about this very quickly. Probably should have gone out first just
saying. So that's where things are now. There are a bunch of questions coming in
right now as far as you know about the whole lightning rod theory which yeah I
will put out a video on that and then how the convention is fundamentally
changed now, and those will be coming out shortly, videos on those topics. But this
is just a brief overview of where things are at at the moment. Anyway, it's just a
that, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}