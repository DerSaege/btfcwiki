---
title: Let's talk about NJ, Menendez, and replacements....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=8pLIlagRX4M) |
| Published | 2024/07/28 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about New Jersey
and Senator Menendez and his replacement
because that is something the governor has to consider now.
According to reporting, Senator Menendez
is resigning, effective, I believe, August 20th.
The governor in New Jersey has the power to appoint a replacement to the United States Senate.
Now, there are a bunch of names being tossed around, one of which was an interesting development,
the governor's wife.
She ran in the primary to attempt to get Menendez a seat and did not win, Kim won.
was a brief moment in time where there was a lot of speculation about her
being appointed to basically babysit, be a caretaker for the seat until the next
election. So whoever takes the seat is going to have it until the very beginning
of January. My understanding is that she was basically like, yeah thanks but no
things. I'm not doing that. So at least according to the rumor mill, she has no
interest in being a caretaker for that seat. There are other possibilities that
are being thrown around. There is a slight move to get the governor to just
appoint Kim. Kim is favored to win the seat and just get him up there early.
What's interesting about that is that Kim is kind of like, you know, there's a
whole lot of considerations with that. It's the governor's call. I don't want to
put any pressure, you know, and seems to be very...almost like he doesn't want the
seat until he wins it. That seems to be the position. I mean that's my read on it
which is, I mean that's novel, that's cool, but it does leave a question as to
who might end up in that seat. One of the names being thrown around is the
lieutenant governor and then there's just a list of other people. I don't think
anybody to include the governor has a clue yet. Not at time of filming. So that's
something we're gonna have to wait to see how it plays out. But the important
news Menendez does appear to be resigning August 20th. Still no word
about whether or not he plans on running as an independent. The governor's wife
was like, nah, not doing it as far as being the caretaker for the seat. And Kim
seems aware that people are trying to get the governor to appoint him, but it
really does seem like he wants to win the seat, not be appointed to it, and then
win it again a few months later. So we'll have to wait and see, but at
this point it does look like the Menendez Saga is winding down. Anyway, it's
It's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}