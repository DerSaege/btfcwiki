---
title: Let's talk about SCOTUS plans, justices, and Biden....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=wwvJSPJ_zV0) |
| Published | 2024/07/28 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about the Supreme Court
of the United States, Justice Kagan, and plans, comments,
and then an announcement that is likely to come next week.
OK, so starting off, what do we have?
We have Justice Kagan coming out in favor
of oversight, of some kind of mechanism
when it comes to the Supreme Court and ethics.
One of the things that she suggested
was a committee of judges to examine any potential
or perceived violation of ethics by the Supreme Court,
by members of the Supreme Court.
She seemed to suggest lower court judges.
And I mean, that would definitely be unique to have lower court judges reviewing the actions of the Supreme Court.
But it appeared to her, at least the way it came across, was that something had to be done.
And she said, it's a hard thing to do to figure out who exactly should be doing this
and what kinds of sanctions would be appropriate for violations of the rules.
But I feel as though we, however hard it is,
that we could and should try to figure out
some mechanism for doing this.
Basically, she went through and talked
about how most sets of rules have outside oversight.
And in this case, it doesn't exist.
And she seemed to be very much in favor of altering that.
Now, again, not knowing what the sanctions would be,
not knowing exactly how it would be carried out,
not exactly a plan.
Just more of general support for the idea
of actually doing something.
That's coming from within the Supreme Court itself.
And my guess is the justice does not want a situation where some justices are tasked
with reviewing the actions of another justice on the Supreme Court.
My guess is that she sees how that would, well, generate a lot of conflict.
She has been very concerned ever since Roe about the legitimacy of the court in the eyes
of the American people.
It certainly seems as though she wants something done.
Now in what I'm sure is completely and totally unrelated news, there are indications that
that the Biden administration will announce plans
to reform the Supreme Court next week.
We don't know exactly what those plans are.
Little bits have kind of trickled out
through the rumor mill,
but honestly, from what I have heard
and what I have seen in print, it's all over the place.
I don't have a good read on exactly what it is, but it certainly appears as though we
will find out soon.
And then from there, we'll see what Congress wants to do about it.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}