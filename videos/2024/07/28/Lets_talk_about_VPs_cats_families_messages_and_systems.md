---
title: Let's talk about VPs, cats, families, messages, and systems....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=TiCzD8VdWh4) |
| Published | 2024/07/28 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about
vice presidential candidates,
and cats and women, and families,
and messages and systems,
and go through a topic that we've discussed before but it has arisen again.
So we'll talk about it one more time.
If you've missed it Trump's vice presidential pick has made comments
about childless cat ladies and basically indicated that that one of the real
issues is people who don't want to have kids. These comments have been echoed
online by people. One of the saddest things I deal with on any consistent
basis doing this is reading messages from the adult children of people who
are very online conservatives who who repeat the talking point of the week
online and their kids see it and a lot of times they don't realize that the
rhetoric that they are using is directly aimed at their own children. It happened
during the Kavanaugh hearings, it happened after Roe, it happens a lot, way
more than it should, and it's happening now. Now Vance, Trump's VP pick, has tried
to clean up the comment a little bit, and said, obviously it was a sarcastic
comment. I've got nothing against cats. I've got nothing against dogs. And it goes
on. People are focusing so much on the sarcasm and not on the substance of
what I actually said. The substance of what I said, Megan. I'm sorry, it's true.
true. These people want to conflate the personal situation here with the fact
that I'm making an argument that our entire society has become
skeptical and even hateful towards the idea of having kids.
No, it has not. Our entire society has become hostile to the idea of having
kids, and I don't mean the people, I mean the systems. One of the reasons people
don't have kids is because they can't afford them. Every time the Republican
Party voted against raising the minimum wage, every time they voted against
health care, every time that they voted against family planning, they built this.
they created this situation. People are making a choice and it's a choice that
is informed by the system that exists. There are people who are echoing these
talking points that are putting it out on their social media and their
daughters are seeing it. And it is driving a wedge between the daughter and
their parent, normally their father. Some pizza cutter comment, all edge, no point
to get likes on social media. There are people who are disrupting, maybe
destroying their families because they're echoing bad talking points on
family values. It seems to me that your family would probably be more important,
should be more important to you than some politician you probably didn't even
know the name of two years ago. It's a consistent theme. If you want a society
where people are having a bunch of kids, you have to create a society that allows
that. You have to create a system that makes that something that isn't a
massive burden. But rather than do that, they feed you rhetoric about how those
people are somehow bad. And people repeat it, unaware that this time the person
that they're othering is their own daughter. This has happened over and over
and over again and it's going to continue because right now the current
version of the Republican Party that exists, it's not about fixing anything.
It's not about creating a society where this can happen. It's about scapegoating
people hoping that that will energize a base that will allow those who want
power to maintain the status quo to keep that exact situation in place to get
that power. That's what it's about. That's what the
rhetoric is about. And it makes it a whole lot easier if you don't receive
any pushback from the people in your life, from your own family. So the wedge
that's being caused, it doesn't hurt those people who are putting out that
rhetoric. It just hurts you for repeating it. Anyway, it's just a thought. Y'all have
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}