---
title: Let's talk about NY, Trump, and a constitutional amendment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=5Mo3T2YzTw8) |
| Published | 2024/07/28 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about a proposed amendment
to the United States Constitution coming out of a,
a representative from New York.
It has been introduced.
And basically the point of the amendment
would be to undo the Supreme Court's ruling
ruling regarding, well, regarding presidential immunity.
That's the goal behind it.
There is some other stuff in it as well.
So let's just run through what's actually
in the proposed amendment first.
It says, no officer of the United States,
including the president and the vice president,
or a senator or representative in Congress,
shall be immune from criminal prosecution
any violation of otherwise valid federal law, nor for any violation of state law, unless
the alleged criminal act was authorized by valid federal law, on the sole ground that
their alleged criminal act was within the conclusive and preclusive constitutional authority
of their office or related to their official duties, except for senators and representatives
pursuant to the first clause of the sixth section of the first article.
That's the speech and debate clause of the US Constitution. Okay, so that was
section one. Section two says the president shall have no power to grant
a reprieve or pardon for offenses against the United States to himself or
herself. Section three, this amendment is self-executing and Congress shall have
power to enact legislation to facilitate the implementation of this amendment okay so that's
the proposed amendment short version the immunity decision would go away that's really what it boils
down to and it prohibits the president from pardoning themselves okay so a whole lot of people
are going to be happy about this. Now here's the part you need to remember. One
of the things that makes the US Constitution such a beautiful document
is the fact that it does include the machinery for change within the
document. You can change the Constitution because it includes the
machinery to do so. However, that machinery is very slow moving. This is
not something that you are likely to see become a part of the Constitution for
quite some time. This isn't a solution for any current issues facing the United
sites. This is going to be a very long and drawn-out process. So it is important
to remember that while something like this may be important, it isn't a fix
for any current situation because it's going to take too long to implement. The
machinery is there, but that machinery is not well oiled. It grinds on at an
incredibly slow pace. So you have a unique moment where something like this
has been introduced. Just be aware it's not speedy in any way shape or form.
It's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}