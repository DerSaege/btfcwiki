---
title: Roads Not Taken EP 49
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=YAgUYBjRZBc) |
| Published | 2024/07/28 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again,
and welcome to The Roads with Beau.
Today is July 28th, 2024,
and this is episode 49 of The Road's Not Taken,
which is a weekly series
where we go through the previous week's events
and talk about news that was unreported, underreported,
didn't get the coverage I thought it deserved,
or I just found it interesting.
Then we go through some questions from y'all.
Okay, so starting off in foreign policy news, the G20 has loosely agreed to kind of sort
of cooperate more when it comes to taxing the super rich.
The statement says, with full respect to tax sovereignty, we will seek to engage cooperatively
to ensure that ultra high net worth individuals are effectively taxed.
One of the things they wanted from this discussion that did not occur was a 2% tax that would
raise somewhere between $200 billion and $250 billion per year from 3,000 people.
The EU, the European Union, sent the first dispersal of cash from seized Russian assets
to Ukraine.
and Russia engaged in joint air exercises near Alaska, prompting US planes to fly up
and intercept and check them out.
These are normal Cold War games, of course, as has been par for the course.
Most media coverage painted it as something very sensational.
It's really not.
I do have to admit I'm not, off the top of my head, I'm not aware of any joint exercise
there, but this isn't unusual. There were disruptions to rail transit prior to
the opening ceremony of the Olympic Games. Let's see, Netanyahu spoke to
Congress amid boycotts and demonstrations. It garnered a whole lot of
media coverage, but it didn't actually seem to alter the foreign policy
situation at all.
Meanwhile, the United Kingdom appears ready to completely drop its objection to the ICC
warrant for Netanyahu.
Moving on to US news, the Trump campaign is suffering a consistent series of leaks depicting
the campaign, I mean, for lack of a better word, in total panic and disarray ever since
Harris became the apparent opposition during the election.
There was an arrest made in connection with the Park Fire out in California, and California's
governor signed an executive order paving the way for cities to clear homeless encampments.
Your periodic reminder that that doesn't actually address the issue.
Harris mocked Trump after it appeared that he was backing out of the debate and honestly
appeared nervous about sharing the stage with her.
Since then, his resolve seems to have steeled a little bit and we'll have to wait and see
how it plays out.
Plans have been announced for the Chicago Democratic Convention.
includes a multi-block security perimeter, which is a clear indication that they expect
demonstrations.
In cultural news, Jennifer Aniston took major issue with JD Vance's single Cat Lady comments,
and Inside Out 2 became the highest grossing animated film of all time, sending a message
to producers to, well, get woke or go broke.
In science news, there was a hydrothermal eruption in Biscuit Basin, which sits near
Old Faithful out on Yellowstone.
It led to the area being closed by the Park Service.
It's a really unique set of circumstances that leads to something like that.
It's worth checking out.
In oddities, the FBI director caused a major stir by suggesting that perhaps Trump had
not actually been hit with a round but maybe it was shrapnel. Later, the FBI
released a statement suggesting that they believe he probably was hit, although
it may have only been from a fragment. This is going to be a thing throughout
this election. I'm not exactly sure why, but those people who have already made
up their minds, it's unlikely that anything coming out is going to shift
their opinion. I would I would be ready for a lot of conversation about this.
Okay, moving on to the questions. Are you going to tell us all of the upcoming
projects? I want to hear more about the myth thing. Okay, as some of you already
know, this channel is introducing more formats. And I know the science one, the
first one of those has already gone out. There will also be like a this day in
history thing. I think we've talked about that briefly. There are others. Am I going
to tell you what all of them are? No, we're just gonna let them roll out. There
is one involving like myths and legends and stuff like that. All of it is content
that I feel like the majority of people watching this channel would enjoy, but it
is a wide variety of content put out by various people.
There are other people behind it.
So you say you want to have more time
to do aid than launch a new series, Typical Bo.
Yeah, right.
OK, so that's worth all right.
My part in the new stuff is training everybody
on fact checking and how that's done
to make sure the information is accurate.
The idea is to get more voices and more
viewpoints on this channel.
So it shouldn't add to my workload.
In fact, long term, it should allow
me to actually relax a little bit.
Are all of the new formats going to be AI-based?
No, definitely not, definitely not.
If you haven't heard the Science and Tech one,
That one is using a text-to-speech thing.
The that, they are definitely not all going to be AI based,
only the science and tech one.
The text-to-speech stuff may get used for other stuff
at different points in time.
But no, that's going to be the only one like that.
The other ones will have individuals behind them.
some of them are new so you're gonna have to let them completely find their
voice you know there's a little bit of shattering going on when when when
they're reading their scripts at times but I mean I think overall as all of the
bugs get worked out people are gonna be really happy with them are you making an
an AI point with a research road, which is the name of the science and tech roundup?
Yes. Yes. One thing I should point out,
and this may be a question further down,
but it's only text to speech.
The content there is actually from a person.
You can't use AI to generate that kind of stuff yet.
That's just not a thing,
And maybe that's part of the point, but there there's more to it.
And they are, I know that there were a lot of comments about the voice.
That's something that's being worked out, they're working on that.
Okay, are live streams coming back more often?
Maybe, maybe.
We'll have to wait and see how everything plays out and
see if I can actually find a better balance.
Were you really a teacher, or was that a skit?
That was a skit.
That was a skit.
For those who don't know, sometimes I insert like,
I do news in the first person and do like satire
and try to make a point that way.
If that is your first exposure to me at times,
that can be an issue.
But no, that was a skit, to use your term.
Why don't you call her Kamala?
For the longest time I thought you couldn't pronounce her name.
I don't know, maybe it's because I'm from the South, but I don't know her like that.
We're not on a first name basis.
She is Vice President of the United States.
I wouldn't call Vice President Pence Mike in coverage.
I know that she is leaning into it as part of her brand.
And if that becomes a thing, then you may hear it more often.
If somebody uses something for advertising
and that's how they get known, that's one thing.
Like an example would be AOC.
But yeah, I feel like when providing coverage,
it would be more appropriate to use her last name.
Weird question, are you autistic?
Not that I'm aware of.
I've never been diagnosed as it.
I wish there was more to this question
to explain what it was about.
If you're looking for information on that,
There are people on YouTube who are, who can provide much better insights.
Why did you upload two morning videos the other day?
It was, what's the word, an accident.
Was an accident.
It happens sometimes.
They were, one was scheduled and then it was supposed to be moved and it didn't happen
so they both went out early in the morning.
What are you currently reading?
I just want to see if it's always to your right.
It is not actually to my right, it is to my left.
Right here.
You know, normally when I get asked this question, it's always something, it's always like a
non-fiction book of some kind or something very, very much on brand for the channel.
In this case, it's not.
It's a novel, just what I'm reading.
Will you take a vacation after the election?
Probably, I mean, I'm saying it that way because it may come before.
I'm getting a lot of questions from people about, you know, taking a vacation and a break
and stuff like that.
And believe me, I get it.
I understand.
At some point soon, I will definitely be taking a step back.
We'll go down to three videos over on the other channel.
There's a lot of stuff I'm trying to do to give me a better balance.
But at the same time, y'all have been with me for years.
I want there to be something there for y'all.
So we're working on that.
It's here on this channel in particular.
It's really starting to kick off.
And you'll see more.
And I know it's probably going to be unusual at first,
having other people in other formats.
But I, I feel like everything that we have coming is something that most people are going to really enjoy.
And with stuff like that out there, it would give me, it would give me more of an ability.
I would feel more comfortable, you know, stepping back.
I mean, we've gone, I don't know, four, maybe five years
without missing a single day as far as uploads.
So yeah, I mean, I feel like a vacation
is in order when that's going to happen.
I'm not sure yet.
But yeah, I think you all are in for a lot of stuff
here that you're going to like.
So even if the first one going out
being the science and tech roundup,
that may not have been what most people wanted.
I feel as they work out the pacing and everything
like that, I feel like people are going to enjoy that as well.
But OK, so that's it.
And so there you go.
A little more information, a little more context,
and having the right information will
make all the difference.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}