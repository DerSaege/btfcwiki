---
title: Let's talk about your candidates and finding out about them....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=jNSWGpxqjiE) |
| Published | 2024/07/21 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about your candidates,
the people running to represent you in this election.
And we're gonna talk about how you can find out
the information you want to know about them.
Over the last, I don't know, week, maybe two,
I have gotten just inundated with questions
about various candidates and various races.
Where do these candidates stand on X issue?
How did this incumbent vote on these bills?
Who funds this candidate?
So on and so forth.
Enough to where if I was just to sit down and answer
the questions, I'd have like two, maybe three weeks
worth of videos and think about how many I put out a day.
So rather than do that, what we're going to do
is talk about how you can pull that information yourself,
because you really only need three sites.
And these sites, not just do they
work for general elections, they also
work for the primaries, which is where most of y'all
have indicated a desire for more choices.
Those choices are there, but they're in the primary.
And a lot of times, you don't know who to vote for.
This will provide you with that information.
These are the tools to get the information you want.
OK, so the first site, and all of these
will be linked down in the description.
The first site is Ballotpedia, like Wikipedia, but not.
Ballotpedia.
This site probably has the most expansive library
of information.
It is also the most complicated to use.
The site design is a little dated.
The one thing you need to know right off the bat
is that there are navigation bars
on both sides of the screen, not just one.
So be aware of that.
But this site, it has a sample ballot.
If you put in your zip code, I think,
it will give you a sample ballot.
So you will know the names of all of the candidates
that are trying to catch your vote.
It sends out a survey to candidates,
asking them where they stand on various issues.
tracks federal, state, and even some local elections. It is just volumes of
information. It takes some time to get to get accustomed to moving around on it.
But once you do that, you can get pretty much whatever information you want from
there. Another one is called Vote Smart. Okay, this has a bunch of information as
well, but I tend to just use it to pull candidates voting records. If you want to
know how an incumbent voted, this is the easiest place to find it. When you go
there, there's a search bar. Type in the name of the candidate, click the
candidate's name and picture, then it will pull up. Down below there'll be like
file icons. One will be for bio, one will be about funding, and one will be voting
record. You click on that, it tells you how that candidate voted on various bills.
I'm sure that information is available over on Ballotpedia. It just seems way
easier to get to on VoteSmart. I think it also gives you excerpts from some of
their speeches as well. Okay, and then the last one is Open Secrets. I have
talked about this one before because it's it's an important one. This one is
who owns your candidate. The money. It tracks all of the money and I mean all
of it. It does a really good job of putting all of that information into
something that is digestible. The one thing you have to note is you have to
read all of the notes, when you type in a candidate, it will give you a bunch of
information. At the bottom of each little chart, sometimes there's a note. Make sure
you read it because you could type in, you know, Jack Johnson or John Jackson,
whoever the candidate is, and it'll pop up and say, MomCorp donated a billion
dollars but the note says that well it's really not MomCorp that did it it's the
employees because it tracks it down to that level but it simplifies it when it
provides it to you. So with those three websites you can get any information you
want that is relevant to figuring out the candidates positions their record
their bio, their speeches, who is donating to get them to vote various ways, it's all there.
Those are the three websites you need. Again, this is important during the primaries as well
because it can help you find the candidates that a lot of you want. When you talk about the
candidates that you have to vote for you're like I really don't like either
of these people you know the selection process in the primary is where you can
introduce candidates that are for most of the people watching this more
progressive that's what most of you want and you can find that information and
you can help move your party along. But the information is out there, relatively
easy to get to most times. But all of these will be linked below. Check them
out, bookmark them so you have them and you can answer the questions yourself.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}