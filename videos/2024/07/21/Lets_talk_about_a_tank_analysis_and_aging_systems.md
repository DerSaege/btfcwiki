---
title: Let's talk about a tank analysis and aging systems....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=HFrp6Qa7meY) |
| Published | 2024/07/21 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about Russia and Ukraine
and an analysis and tanks.
And we will do this in the least technical way possible
because I had some questions, plural,
come in about an analysis that came out.
And even for people who aren't really that interested in the more technical side of these
questions, the overall signals coming out of these decisions, kind of noteworthy.
Okay, so here's the question.
I was told that a good analysis came out that said Russia would soon have more T-62s than
any other tank in Ukraine as they pull them from storage.
I'm wondering if you've heard this
and if you have, why they're using those
instead of the T-64.
If you can answer those, do you think the less modern tanks
will matter greatly?
Also, why not pull the tanks that are actually modern
that they have in storage?
OK, so quick kind of recap on what's going on.
An analysis came out talking about what Russia is pulling
out of long-term storage, very long-term storage,
and refurbishing, getting it up and running,
and getting it to the field.
I didn't read the full analysis.
I saw a write-up on it.
looked good to me. The one thing that I noticed was the person who did the
analysis actually kind of said where they were making assumptions, what those
assumptions were, what information they were kind of filling in the blanks on.
Normally when somebody does that they are trying to put out a good product. If
those assumptions are correct the product's probably good. So the
information is probably good. Overall, looking at what's there in totality, yeah,
it tracks that soon Russia will have more T-62s than any other type of tank
there. For people who do not understand any of these numbers, it is not an
accurate way to do it, as in it's not exact, but those numbers, those last two
digits, just add a 19 in front of it, and that's kind of close to when they came
out. It's not exact. I want to say a T-62 was actually introduced in like 61, but
it gives you a good point of reference for it. Russia is pulling out T-62s. We
know they're actually doing it. The analysis says they're going to be doing
it more and more and more because of the rate of loss of tank in Ukraine.
So going back to the questions.
Have I heard it?
Yes, it tracks to me.
Why are they pulling the T-62s instead of the T-64s?
Looking at them by those numbers, they're close.
So they should be similar, but they're not.
The T-62 is an evolution of the T-55.
The T-64, something completely new, well, I mean, not new by today's standards.
It's still 60 years old.
But it was, for its time, it was a big deal.
When NATO finally found out about it, they were concerned about it.
It has a lot of more advanced stuff in it, which means it's more complicated.
So if you're Russia and you're facing a bunch of sanctions, you're having a hard time getting
parts, equipment, so on and so forth, you would probably want to pull the more simple
machine, which is the T-62.
They also have more of them in storage.
So they pull those out.
get them refurbished faster and get them to the field, whereas the T-64 is more complicated.
So the sanctions probably factor into this a whole lot.
And even though I believe the T-62 requires an extra person as far as crew, it's simpler
to use, therefore easier to train the people.
And all of those right now with the way Russia is running their war, having it require not
a lot of training and being cheap, those are kind of selling points for them right now
between the lack of personnel and the difficulty in obtaining quality training and parts, the
T-62 is probably the smart choice for their side.
Do you think the less modern tanks will matter greatly?
If I understand the question, you mean T-62 versus T-64?
The 64 is a superior tank all the way around, despite some issues.
That being said, the way the war is running in Ukraine right now, a tank's a tank.
And if Russia can pull two T-62s out of storage for the same amount of resources they could
get one T-64, it would probably be more effective for them on the battlefield to use the older
tank in this case.
The problem isn't going to be, the problem for Russia isn't that they're going to have
T-62s instead of T-64s, the problem is all of the contributing factors that led to that
decision.
It's not like they looked at the tanks and were like, oh, we definitely prefer the T-62.
They chose it.
It wasn't a choice.
It was a lack of options, would be my guess.
They didn't have a choice.
They needed more tanks because they're losing so many.
They've suffered a very high rate of loss on those.
And then the last question, why not pull the tanks that are actually modern in storage?
So for those who are not familiar, there's more modern tanks that Russia has in storage.
I know they have some 80s and T-72s.
Why not pull those?
With the way things are going, there is a high probability that what is being sent into
Ukraine will not come back.
They will be destroyed.
They are prosecuting an offensive war right now.
If something was to happen and they had to go on the defensive and protect their own
country, they would want to have at least some modern stuff in reserve.
They're burning through their reserves.
My guess is you're not going to see them pull a lot of the more modern stuff out simply
because A, again, sanctions, B, the additional time it would take to refurbish them, and
C, they still have to be prepared for another conflict arising.
The stuff that people talk about with the United States, oh, what if they have to fight
Russia and China at the same time, and all of that stuff.
Russia has a lot of territorial disputes, and in its weakened state, some countries
might want to try to take advantage of that.
They have to be prepared for a defensive war as well.
There you go.
than most of y'all ever wanted to know about Russian tanks but the key take
away the reason they're making the decisions they are is they don't have
the production capability and the sanctions are are having a real impact
Anyway, it's just a thought, and I'll have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}