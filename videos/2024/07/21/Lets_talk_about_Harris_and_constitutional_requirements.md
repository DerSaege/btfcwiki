---
title: Let's talk about Harris and constitutional requirements....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Rkax42v9mFE) |
| Published | 2024/07/21 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about Vice President Harris
and whether or not she really has the qualifications
and meets all of the requirements
per the United States Constitution
to be president of the United States.
Why are we doing this?
because some people get their news from Facebook memes, that's why.
Some of you might remember a time when large segments of the Republican Party were requesting
a birth certificate from President Obama.
Rinse and repeat.
So the allegation in the theory, the way this meme goes, is that she doesn't meet the requirements
because both of her parents were not citizens.
Now to be clear, I didn't fact check whether or not her parents were citizens.
Why?
Because it doesn't matter.
That's not how this works.
That's not how any of this works.
The relevant part of the Constitution that all of these people claim that they love and
have definitely read says, no person except a natural born citizen or a citizen of the
United States at the time of the adoption of this Constitution shall be eligible to
the office of president.
Neither shall any person be eligible to that office who shall not have attained to the
age of 35 years.
been 14 years a resident within the United States.
Harris is more than 35.
No person except a natural born citizen.
I mean, I'm fairly certain that Oakland is part of the United States.
being born there makes her a natural born citizen. End of analysis. That's
it. So people are falling for the same type of rumor, allegation, conspiracy
theory, whatever that they have fallen for before, so they get to be played by
the same type of thing twice now because they did not learn the first time. Here's
the thing. What this shows is that despite the Trump campaign's insistence
that they know they're gonna win in a landslide, if they're resorting to this
kind of stuff, those people spreading this, those people buying into it, they're
not that convinced. They are not that convinced. These types of, these types of
moves, they don't come from politicians who are secure in their next election.
So, again, I can't believe we're doing this.
There is no basis to the idea that the current vice president cannot be president.
That there's nothing to that.
That's not how the Constitution works.
That's not the requirement.
They've made it up.
And many people who definitely are real live patriots and true understanders of the U.S.
fell for it. The sad thing is while trying to find like the original memes
and everything so I could get the information, I found out that this isn't
even the first time it was circulated about her. This was circulated about
her way back in 2020 as well. It wasn't true then either just if you were
wondering. If those people who you follow, you get information from, if they
believe that you will fall for this, you probably want to seek out a different
source of information. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}