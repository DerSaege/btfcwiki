---
title: Roads Not Taken EP 48
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=NM2GyLQK1fE) |
| Published | 2024/07/21 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again,
and welcome to The Roads with Beau.
Today is July 21st, 2024,
and this is episode 48 of The Road's Not Taken,
which is a weekly series
where we go through the previous week's events
and talk about news that was unreported, underreported,
didn't get the coverage I thought it deserved,
or I just found it interesting.
Then at the end, we go through some questions
from y'all picked by the team.
Okay, we're going to start off with a PSA this week because with all of the news, the
information about this really isn't getting out in the way that it should to kind of make
sure that people are exercising caution.
So there is a Listeria outbreak believed to be spreading with sliced deli meats.
It is impacting at least 12 states, but the CDC actually thinks it's a wider spread than
and they just don't have the information on it yet. So just if you are,
particularly if you're in an at-risk group, make sure you are taking the
precautions necessary there. Okay, starting off with foreign policy. Trump
is facing accusations that he violated the Logan Act. Based on his statements,
he very well may have. He won't be charged. It is incredibly unlikely that
that he will be charged, the reason being nobody's ever charged with this.
Let's see, you have a senior North Korean diplomat who defected to the South, and there
is a brewing situation in the Middle East in a whole bunch of different ways.
The two main ones have to do with an ICJ decision.
an opinion, it is non-binding and all of that stuff. However, it will create some diplomatic
pressure. The general tone of it is the settlements are illegal. Now, as far as US foreign policy,
that's not really a departure. I don't think they used the term illegal. I think they used
inconsistent with international law.
And that's pretty much been every administration for decades, with the exception of Trump.
Trump said that those were totally cool.
So there's likely to be diplomatic pressure, but that's not going to be...
It's probably not going to cause a lot of immediate changes right now, but it's another
one of those things that's a shift.
The other thing that is occurring is the Houthis in Yemen reportedly launched a drone that
was able to get through the Iron Dome inside and actually cause damage.
Israel responded to that and went after the area in and around a port.
Now questions are coming in about this because they're expecting people to say escalation
wider regional conflict all of that stuff yes that's a risk depending on
how it goes but also the exact opposite could happen with this one because it's
a shift in dynamics anything that the Houthis have if this is something that
can be reliably done this technology it is cheap it is easy to manufacture
it is easily replicated and it is a safe assumption that if they have it, other non-state actors
in the region probably have something very similar if not the exact same thing.
There has been a lot of rhetoric from Netanyahu about going north and expanding north.
This may actually cool the rhetoric on that in a very weird way.
This may actually cool the temperature.
Obviously, we have to wait and see how everybody responds.
But if everybody is thinking logically, this should reduce the temperature over there.
We'll see how it plays out.
Okay, in U.S. news, Disneyland workers voted to authorize a strike.
Reporting indicates that almost 99% of workers voted in favor of it.
In Ohio, they are underway, purging 160,000 voters from the rolls.
There will be links down below to check to see if you're impacted by this.
I know I put a video about this over on the other channel.
This is just one of those things I feel like people shouldn't be caught off guard by.
A deputy in Illinois was charged with the murder of a woman who called for help.
This is shaping up to be a really bad one.
Every piece of information that comes out makes it even more eyebrow raising.
A federal appeals court dismissed a lawsuit over the Tennessee anti-drag law, but it wasn't
dismissed on merits.
It was dismissed because the court found the people who brought the case, the entity that
brought the suit to overturn the law, didn't have standing to do that.
So you're going to see all of this play out again.
Kerry Lake had quite the exchange with an interviewer who asked her about election claims
and defamation claims.
Now I would expect to see this referenced a lot, if you want to see it you could probably
search for Kerry Lake and head examined would probably get you the video.
The RNC from the perspective of reaching moderates, independents, undecided voters was an abject
disaster.
It failed to reach the tone that the Trump campaign needed, and it was made even worse
by them messaging about it ahead of time and saying, you know, you're going to see a new
Trump.
He's going to be a statesman in a different tone and unity.
And then when that really didn't happen, it made it worse because it drew attention
to how far removed he is from normal behavior when you're talking about a presidential candidate.
RFK Jr. is now set to get Secret Service protection due to recent events.
In cultural news, the movie Twisters is under fire for failing to acknowledge climate change
in the movie.
That seems like it might be an important part of a film like that.
In science news, a U.S. appeals court has sided with Biden and is allowing the EPA rule
on coal-fired plants to remain in place while the legal challenges play out.
This rule, of course, is aimed at mitigating climate change.
There are warnings now going out about dengue fever in Georgia.
In oddities, Hulk Hogan was at the RNC and he appeared to rip the American flag on his
shirt as he ripped his shirt open to reveal his support for Trump with a Trump shirt underneath.
That has caught a lot of attention for a whole bunch of reasons.
There is reportedly outrage in Florence over a woman who is on vacation and her, let's
just say, dancing suggestively on a statue of Bacchus.
I mean, I get it from the standpoint of it being a historic statue and all of that.
At the same time, if you're going to do that to a statue, that's kind of the perfect one,
right?
I mean, I'm having a hard time with that one, I'm going to be honest.
Okay moving on to question and answers.
Did you ever hear back from the person looking for help?
Yes, yes, we have messaged.
How it impacts you when you get messages like the hope one or does it at all? Oh, of course it does
Of course it does. In fact that one actually helped me make a decision
Channels they've been around a while and one of the things that always made me really happy was all of this stuff
off-line that we've done you know and for those who are newer during the the
pandemics we were able to get masks and medical supplies for rural hospitals
small hospitals so they weren't on the list to help you know we were able to
supply Christmas presents to the children of a mine that was on strike the
workers, their kids. We, you know, have done disaster relief. We, you know, we do
our yearly fundraiser for the shelters. And we do all of this. And to me, that
was, it's always been a big part of it. I've realized since we added that fourth
video a day over on the other channel, that I don't have as much time for that.
And this year, I mean, you know, yeah, we did the thing for Project Rebound, which was
super cool.
I mean, don't get me wrong.
And there's a couple of other smaller things, but it's less than it normally was, and I
feel like those two things are related.
So that's, I had been toying with the idea of going back to three videos a day.
That's probably going to happen now.
And this, in a weird way, kind of helped me make that decision.
On this channel, you know, we've been talking about expanding the content, and all of that's
going to happen, but you are going to hear some different voices.
You're going to hear some different voices, so.
And then I'm sure that, you know, this is asking about, you know, how it makes me feel.
I mean, yeah, it definitely has an impact.
And I think a lot of what I just said really comes down to,
I don't want to be in a situation where I miss an email.
So OK, should I really be concerned about Project 2025?
I mean, yeah.
You know, people are trying to frame it in different ways.
And when you look at the whole thing, it is obviously something that is concerning to
a whole lot of Americans.
And the counter to that is, you know, it's not like they're going to do all of it.
When you break it up, what you realize is that a lot of those talking points are the
talking points of a lot of different candidates.
It is definitely something that should be looked at and considered.
Given the new info about it not necessarily being politically motivated, do you think
he would have gone for Biden if he crossed the picket line.
I hadn't really thought about that.
That was in Pennsylvania.
There's a lot of speculation now that what happened to Trump, that the person who did
it that that person was really just looking for somebody high-profile and close.
I don't know.
I had not thought about that, and if you don't know what this is referencing, not long before
there was a pretty well-publicized event in Pennsylvania that Biden was going to, and
There were changes at the last minute.
That's definitely an interesting thought.
I don't know what he would have done to answer the question.
Have you ever tried growing flax before?
No, I have not.
I have heard it can be picky.
Bo, in light of the 55th anniversary of the moon landing,
I wanted to ask a question oriented towards the impact
of the event on the youth.
It is said that more people in the 70s and 80s
went into these sciences as a career path
as a result of seeing the moon landing on TV.
One of the top things a kid wanted to be when they grew up
an astronaut? Do you think that the kids who end up seeing the landing of Artemis will be inspired
to go into the sciences themselves into finding climate solutions and even further exploring in
space. Yes. Unqualified. Yes. I think that we messed up. I think we as a country
messed up when we stopped making space travel a spectacle. I do believe that
would rekindle it as long as everybody's on board with the idea that it needs to be rekindled and
people talk about it. What exactly are the Teamsters doing? You said they were handling
it internally. That sounds like a cop-out. Specifically, what? I understand him speaking
there but the almost endorsements of other anti-union GOPers was too much.
So again, what?
Unless they're coordinating a push to oust him in the next election, it isn't enough.
So funny story, that's exactly what they're telling actually.
So for those who don't know what this is about, O'Brien with the Teamsters, he spoke
at the RNC.
Looking at the RNC, that took a lot of people off guard because the Republican Party is
anti-union.
They're not pro-union.
They pretended that they are, but you can look at their votes and see that they are
plainly anti-union.
So there was a lot of animosity about that.
Me personally, I think that was a good move.
Some of the other things that O'Brien did on his social media after the fact, or even
some of the comments that were made, it didn't sit well with a whole lot of people.
Because for a whole lot of people, they saw not just an endorsement of rhetoric supporting
unions, but an endorsement of everything else that the candidates stand for, and a lot of
it is not just anti-union, it's anti- like the core identity of a lot of union members.
So I did a video talking about why he went, and then, you know, saying the internal stuff,
they're handling it internally.
The question is, what exactly are they doing, unless there's a coordinated push to oust
him in the next election?
It isn't enough.
They're literally doing that.
That's what's happening.
My understanding is that it didn't have to go that far, but from what I have been told,
that is where they are at right now.
My understanding is that there were other options on the table, but not everybody wanted
to pursue those. So, there is a coordinated push would be a good term for it. So, I mean,
If you believe that is enough, that is definitely happening.
All that's here, the question that's here, is it normal to handcuff a body?
So obviously, context, that has to do with Pennsylvania.
Yes, in a situation like that, yes.
If you watch enough of footage like that, you will see situations in which it is very
clear that the person being cuffed does not need to be cuffed.
But it is so ingrained that, yes, that always happens.
Do you think Biden means what he says about SCOTUS reform?
Yes.
One of the things that has happened with a lot of politicians lately is the, I'm going
to do this and leaving it unqualified.
A lot of what he is suggesting, I totally believe he wants to do it.
I totally believe he would do it, but he's not a senator anymore.
And that's where a lot of that stuff has to happen.
If he had a House and a Senate that was on board with him, he would absolutely be pursuing
it.
But some of the ways it has been phrased at times has kind of given me that, yeah, that's
not your job anymore.
You can't...that's in the Senate.
But no, I truly believe that he does want to do that.
He would need allies in the House and the Senate to make it happen, though.
And that looks like it.
Okay, so there you go.
A little more information, a little more context, and having the right information will make
all the difference.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}