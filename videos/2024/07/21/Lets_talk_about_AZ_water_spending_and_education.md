---
title: Let's talk about AZ, water, spending, and education....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=nRlFyPKSZuQ) |
| Published | 2024/07/21 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about Arizona
and water infrastructure
and cuts to spending on water infrastructure, big ones.
Curiosity and a potential reason
why those cuts are happening.
And school vouchers.
Stick with me because there's this
of a winding road. Okay, so recently I saw this thing saying that there was going to be a $330
million cut, $333 million cut to water infrastructure spending in Arizona and I was kind of taken aback
by that for two reasons. One, it's desperately needed. Two, the governor there, Hobbs, I mean I
don't expect a whole lot of politicians most times, but this is something that she's kind of been on,
so it was incredibly surprising. So I wanted to know why, the eternal question, why. So I started
looking around, I found out there was a $1.4 or $1.5 billion shortfall in the budget.
Okay, well that explains why there were cuts, but why did the shortfall happen?
And you find out that there was lower revenue, you know, normal stuff that leads
to shortfalls, but there was something else. I stumbled across an article from
ProPublica. If you're not familiar with this outlet, I've talked about them
before. Good, old-fashioned journalism. Really digging into stuff and putting it
together in a very consumable format. They put together a piece about the
school voucher program. Back in 2022, the previous governor signed into law this
this voucher program and the idea behind it is that the state would provide a
voucher for private school tuition or supplies to be used for homeschooling, so
on and so forth. This idea has been used as a model in a whole bunch of
other states. What does any of this have to do with water infrastructure cuts,
right? It was supposed to have a price tag of 65 million dollars. That's what it
was supposed to cost, 65 million dollars. Current estimates put it somewhere
between 330 million dollars and 430 million dollars. Well that kind of
explains it then. That certainly appears to possibly be a contributing factor. One
One of the things that I noticed when I started looking around, because a whole bunch of states
have implemented programs, some of which were like directly almost copy and pasted, is that
there are some states that kind of, it looks like they saw this coming and altered their
programs a little bit.
that, this is one of those things that has become a major topic in the United States.
And when you're looking at expenditures like that, especially the way it gets marketed,
it could probably use a little bit more discussion as to what it actually costs to facilitate
a voucher program like this, because the state that a whole lot of other states kind of modeled
their stuff on, they're having some big issues, some big overruns that are leading to
very tough choices being made. That cut $333 million from water infrastructure, and as far
As far as I can tell, there's no guarantee that this is like a temporary thing.
That money in Arizona, that is desperately needed for that.
The situation with the water out there, it's not over, not by any means.
There was kind of a short-term deal worked out, but that situation is not over.
money it's desperately needed if there is any desire for people to still
continue to live in the state because the infrastructure is going to have to
be updated as we have issues with water at the same time as rising temperatures
it's going to be an issue. So this is probably something that needs to be
discussed a little bit more. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}