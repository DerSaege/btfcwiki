---
title: Let's talk about Ohio removing 160,000 voter registrations....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=bWRWCv3W1gY) |
| Published | 2024/07/20 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about Ohio and voting and you.
You, something you need to check if you live in Ohio.
And we'll go through the information
and just make sure everybody is aware of how significant
and expansive this move really kind of is
when you put it in perspective.
OK, so what's the news?
The great state of Ohio has decided to get rid of some, quote, inactive voter registrations,
get them off the rolls, 160,000 according to reporting, 160,000 inactive registrations.
To put that into perspective, in 2023, the number I found was a little less than 8 million.
That's how many registered voters there were in the state.
So removing 160,000, well, that's about one in 52%.
I want you to imagine the impact on some races that
might be close if there are, of course,
unintentionally people who want to devote that
have their registrations removed.
So the good news is down below, there
will be a link to, actually, one is to a database.
lets you plug in your information
and see if
your registration
was one of the ones that they got rid of.
The other
is
in case they did do that,
it's
the link to voter registration.
You do not have a lot of time to correct this.
You should probably do it
the day you see this video.
Again, now
To hear the Secretary of State's office up there talk about it, they are making it seem
as though they were pretty careful with this process.
Basically, somebody had to be inactive for four years.
I don't know how many people that might accidentally sweep up, but I can think of a lot of situations
where it might get somebody who intends to vote this year, but now they won't be registered.
So if you live in Ohio, definitely check out the links.
It's not like some random website that you're putting your information in, it's the Secretary
of State's office in Ohio, it's a government website.
So check it out and make sure that your name doesn't get removed.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}