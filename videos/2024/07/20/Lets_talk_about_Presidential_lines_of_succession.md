---
title: Let's talk about Presidential lines of succession....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=8laKejapWZ8) |
| Published | 2024/07/20 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about
presidential lines of succession.
We're going to do this because of a whole bunch
of recent events and the prospect of having two candidates
both of advanced age.
That has led to a whole bunch of questions coming in.
And the way they're phrased, it seems pretty apparent
that most of them, if not all, are from Gen Z.
us a minute to figure out why and then we realized they weren't alive during
the Cold War so realistically there's no reason for this to be common knowledge
among that generation. So we will go through it. At the beginning all of this
is governed by the 25th Amendment to the Constitution. If the president is unable
to fulfill the duties of the office, the vice president becomes president. The new
president, the old vice president, then nominates a new vice president. The House
and the Senate both confirm that person with a majority vote. And now you're back
to the beginning. Now, if the president and vice president are both unavailable
to serve for whatever reason at the same time, it goes to the Speaker of the House.
House. Then it would go to the President pro tempore of the Senate. After that, it
goes to Secretary of State. Then it goes to Treasury, Defense, the Attorney General,
Interior, Agriculture, Commerce, Labor, Health and Human Services, HUD,
Transportation, Energy, Education, Veterans Affairs, and then finally Homeland
and security.
The only catch here is that if any of these people are not a natural born citizen, well,
it skips over them.
If they're not a natural born citizen, they cannot serve as president.
So that they just, they get skipped.
Now, my understanding is that if this list was to be exhausted, there are cog plans,
continuity of government plans that extend this list even further.
is zero reason to worry about this process. It is pretty well outlined. Now,
the one thing that should probably be talked about more often is that these
cabinet positions, they are in line for the presidency. That's something that
should probably be discussed when people are being confirmed. You do not want a
president who hands out cabinet positions as political favors and then
runs with a kitchen cabinet made up of family members because they are in fact
in line for the presidency and understand if it ever gets to that point
if something occurs and we're looking at one of the cabinet members assuming the
presidency, understand that things have gone sideways. The reason people who were
alive during the Cold War know this is because it was discussed pretty
often because of the risk of a strike on DC. As that risk fell from public
consciousness, I guess this didn't get discussed very often, even though I think
think there was like a TV show based on this premise. It's something to keep in
mind because if something unthinkable was to occur, that person is thrust into
that office at a very inopportune time. There's not a lot of time to, you know,
So learn as you go, not a lot of on-the-job training, because if it occurs, something
really, really bad happened.
Your most likely person after the first four is the Secretary of State.
The odds are that they will probably be around because they travel a lot, so they're not
always in D.C.
That's the answer, again, as far as the system in place for the transfer of executive authority,
it's there, it's very well outlined, it's been around since the Cold War, nothing to
really worry about there.
However, the fact that this part isn't really considered during confirmations or during
nominations more frequently, that's probably something that should change.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}