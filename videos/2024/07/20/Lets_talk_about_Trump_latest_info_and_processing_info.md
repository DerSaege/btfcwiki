---
title: Let's talk about Trump, latest info, and processing info....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=us_-VRjJRmM) |
| Published | 2024/07/20 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about the latest information
concerning Trump and what happened in Pennsylvania.
We are going to talk about good practices
for processing information.
And we're going to talk about a video that went out
that was definitely not a Bostradamus video,
despite how it appears today.
going to do this because I got a message. Okay, so I just saw what the
investigators are saying. They're now saying he may have not had a motive
related to politics. So I went back to find the first video where you said that.
WTF. How did you know that right away? Then I wound up watching the whole video.
You were right about everything. Motive, where the Secret Service messed up, and
and even him being an amateur. I'm not here to stroke your ego. That's not what
he said. Literally, how did you know? What is the process? Okay, so if you have
missed the reporting, according to the reporting, the investigators are now
openly speculating that he really didn't have much of a political motive, that he
was looking for somebody high profile and, well, whoever had the highest profile who
came closest first, that person might have just done.
In addition to what we've already talked about with him having photos of Biden and scheduling
information, my understanding is that there were other photos of national figures as well.
Okay so to the question, how did you know? I didn't. I didn't. If you go back and watch
that video, again I'll put it down below for those who may not have seen it, I didn't know.
The only thing that I was certain about was that the critiques for the Marksman, the Secret
service marksman that those were wrong. That was the only thing in that video
that I was certain about. Everything else was me listing options, things that could
be. What's the process behind that? When there is a dramatic event and pretty
much the entire media latches on to what seems apparent even when there is more
unknown information than known.
Don't follow that idea.
Don't follow that idea.
Don't follow that presentation because if you do, just go ahead and wait for the corrections.
It's almost never right.
I didn't know that it was going to turn out that way.
It was an option because he appeared amateurish.
So it was included, but it was included in a list of other options.
It wasn't that I knew, it's that I didn't rule it out because I didn't have any evidence
to rule it out.
I didn't have anything at that point in time saying that it wasn't the case and it is something
that is pretty common.
So without evidence that supported another theory and there's no evidence to rule it
out, well you don't.
You keep it on the table.
You keep it as an option.
I was fairly certain about the advance and a miscommunication because what happened doesn't
happen without that occurring.
But again, that was a suggestion.
This is something that likely happened.
Not for certain that it did.
Even with the speculation coming from the investigators, I'm not at the point where
I would say for certain that he didn't have a political motive, but it is certainly leaning
more that way the more information that comes out.
It wasn't something that was, it wasn't a possibility that was entertained by many people.
It should have been.
There was no reason to rule that out yet.
It wasn't that I knew, it was part of a list of options.
The whole point of that video could be summed up with be comfortable with uncertainty.
Look for actual evidence before you marry yourself to a theory.
This is a good example of that, but it just as easily could have turned out that everything
was exactly as it appeared from the beginning.
The evidence wasn't there to say that yet, so I didn't.
I did not know.
Anything other than all of the critiques about the marksman, those were wrong.
the only thing in that video that I say for certain. Anyway, it's just a thought. Y'all
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}