---
title: Let's talk about a video, physics, and changing a tire....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=DtVf0P2GAX0) |
| Published | 2024/07/29 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people. Let's bow again.
So, today we are going to
talk about a video that is circulating on the internet,
physics,
and
how to change a tire, basically.
This is going to be a little different than most of the videos that have gone out recently,
but
I have been sent
this video a number of times.
And the video depicts three women, and they are
stranded on the side of the road.
They're trying to change their tire.
They have the tire out of the back of the vehicle, but they
can't get the lug nuts off.
They're stuck.
And they're trying everything.
They're pushing on it.
They're jumping up and down on it.
And most of the people who have sent me this video, they're
using it for some kind of sexist gotcha question.
So, it wasn't a high priority for me, but then I got this one.
Bo, I feel so stupid asking this, but when I saw this video, I had a new fear unlocked.
I'm 22, I didn't have a dad in my life.
Your video about the crank can opener came in real handy during a power outage.
I was just wondering if there's something I can do if I end up in a situation like this.
And please make a video, because I asked my friends, and none of us know.
asked the guys and they said it would never happen but it happened to the
women in the video. Being stranded on the side of the road like that seems
terrifying and dangerous. Okay, first your guy friends are definitely wrong.
Lug nuts get stuck all the time. That's not like a rare occurrence. So most times
what you see them doing in that video where they're bouncing up and down on
the handle trying to get it to spin, most times that works. But if this is
something that concerns you, take your lug wrench with you
the next time you go into a hardware store.
Go to where the pipes are, the metal pipes,
and find a pipe that the handle of your lug wrench
will slide into.
It doesn't have to fit super securely,
but you don't want it to be too wobbly on the inside.
And then get the longest version of that pipe
that will still fit in the trunk of your vehicle.
If it ever happens and you can't get the lug nut to break free,
put the wrench on and then slide the pipe over the handle.
It allows you to be further away.
So force times radius equals torque.
So you can apply the same amount of force,
but because you're further away, it generates some more torque
And it'll probably break the lug nut free.
And then you'll be able to change the tire.
If it doesn't work just by pushing on it
with the additional distance, if you do the little bouncing up
and down on it, like you see in the video,
that will almost certainly work.
Yeah.
And then as far as it being terrifying and dangerous,
Now you have a pipe.
So if you ever end up stranded on the side of the road, and the people who might be able
to assist you decide to make a video for the internet instead of helping, now you know
what to do.
Y'all are the reason they choose the bear.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}