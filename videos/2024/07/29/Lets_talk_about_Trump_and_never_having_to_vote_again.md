---
title: Let's talk about Trump and never having to vote again....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=WFrnIfYUntY) |
| Published | 2024/07/29 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about Trump and you
and something Trump said and, well,
never having to vote again.
If you've missed it, the former president
of the United States was giving a speech
and he said something that raised a whole bunch of eyebrows.
And this is being discussed in a number of different ways.
And we're just going to kind of talk about exactly what was said.
And then we will talk about how people are receiving what was said.
OK, so what did he say?
Get out and vote just this time.
You won't have to do it anymore.
Four more years, it will be fixed.
It'll be fine.
You won't have to vote anymore.
Okay, so obviously there are some people who heard that and are viewing it in light of
other events.
And they are taking it to mean that the former president has publicly said that if you vote
for him, you'll never have to vote anymore because he'll fix it to where you don't have
to.
Basically, the read is he is saying vote for me this one time and then from then on out
your betters will take care of it.
Given other events and other rhetoric, I understand where that's coming from.
The flip side, his supporters are saying that this is all hyperbole.
What he's really saying is, well, if you vote for me, then I'll fix it and everything will
be wonderful.
I'll fix all of your problems and therefore you won't feel the need to vote anymore.
That's how they're taking it, that it is typical Trump promising a whole bunch of things and
just kind of overselling on what he plans to deliver.
And that he really doesn't mean that you won't have to vote or you won't be able to vote.
That what he means is you won't want to because everything is going to be great.
Let's read it one more time.
Get out and vote just this time.
You won't have to do it anymore.
Four more years, it will be fixed.
It will be fine.
You won't have to vote anymore.
I mean, the idea that in four years, he is going to cure all of the country's
Ails and do it to the point where nobody would ever have to vote again, I
mean, even for Trump, that seems a little grandiose, it's also worth
remembering, that is literally not how our system works, what he meant, I
don't know. I don't know what he meant by it. What I do know is that you have a
person who every time they misspeak, they misspeak in a way that gets misread as
him wanting to be a dictator. Then maybe that's me being a little hyperbolic
Maybe it's not every time. Sometimes he just confuses people's names. But when
this topic comes up of, oh that's not really what Trump meant, why is it always
about this subject? It certainly seems like it's always about this subject,
about him wanting just unchecked power. I feel like that is something that is
is probably going to weigh on a whole bunch of people's minds
come November.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}