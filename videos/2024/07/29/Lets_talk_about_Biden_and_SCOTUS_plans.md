---
title: Let's talk about Biden and SCOTUS plans....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=2jtE0obSsPk) |
| Published | 2024/07/29 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about President Biden
and his suggestions for the Supreme Court
of the United States.
And what we're going to do is go through
just kind of an outline of what the suggestions are.
We will talk about whether or not you're likely
to see them during his term.
We'll talk about why they came out in the way that they did
and what Biden is hoping to accomplish here.
Okay, so just a quick recap, if you've missed the lead up to this.
For quite some time, the rumor mill has said that the Biden administration
is going to announce reforms for the Supreme Court of the United States.
Recently, the rumor mill shifted to, it's going to come out this week.
So, I guess today counts.
In the suggested changes, the biggest, the most consequential is that Supreme Court justices
would no longer be appointed for life.
They would be appointed for terms of 18 years, meaning that each president would appoint
a Supreme Court justice every two years.
This is something that has been suggested for a very long time as a method of depoliticizing
the court.
before people were saying, pack the court, this was something that was being suggested.
So you have that.
And the idea is that on a long enough timeline, it would create equilibrium and it would no
longer be a political thing.
Good luck.
But that's the idea.
The next suggestion is a binding enforceable code of ethics.
already see some movement on this in Congress. The third thing is a constitutional amendment,
basically just getting rid of the immunity ruling. I've already gotten messages about
this saying that it's unconstitutional. No, if you amend the constitution, it is literally
constitutional. That's how it works. Okay, so that's it. That's the rough sketch. And this
came out along with an op-ed basically saying that hey you know people are
really questioning the legitimacy of the court paraphrasing. Are you likely to see
any of this during Biden's term? No. No. Almost impossible. I mean I don't want to
say it is totally impossible but it is incredibly incredibly unlikely. So why did
do it? Three reasons. First, legacy building. Biden is very aware of his legacy. He has a
large legislative legacy, much larger than people think it is to be honest, that he can build off of.
This is him setting the stage and saying, hey, if I had another four years, I would do this too.
Presidents do this when they're fairly certain these changes are going to occur.
That way they get linked to them in the history books and it builds their legacy.
You know, and that's what he's doing right now. He is trying to build that
legacy and build it big. The second part is it helps set the stage. It helps with
political capital. I mean, think about it. You had a constitutional amendment
introduced, and it barely got any coverage because it doesn't have the
political capital. I bet people talk about it now. That's the other part.
Actually kind of using the political capital he has to get the ball rolling
on this. And then the third thing is the election. He is very aware that the
American people have questions about the Supreme Court in his op-ed, in his
statements, that's very apparent. Doing this says that his administration, and by
extension Harris, has a plan and he's helping with the election in that way.
That's what this is about. Don't expect to see any of this soon, but the way it's
being tied in, Biden expects this to occur at some point in the near enough
future to be linked back to him and his legacy. Anyway, it's just a thought y'all
have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}