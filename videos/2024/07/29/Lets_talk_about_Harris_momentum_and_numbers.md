---
title: Let's talk about Harris, momentum, and numbers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=KSBhd_VHUi0) |
| Published | 2024/07/29 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about Vice President Harris
and momentum, numbers, how things are shaping up
over the last week or so since she became
the most likely candidate
for the Democratic Party for President.
And just see if there's any trends
that might be worth noticing.
So, according to the reporting, she has brought in around $200 million, and two-thirds of
those, two-thirds of their donations came from people who this was the first time they
had donated during this election cycle.
That is a pretty strong indication that there are people who were not enthusiastic before
who are definitely enthusiastic now. Those are new people. That's a big
deal. That shows momentum for her campaign. The other thing is that it's
being reported that I want to say 170,000 people have volunteered to work with her
campaign, you know, to canvas and make phone calls and stuff like that. It's a
big number. That's a big number. There is a clear indication here that not just
the Democratic establishment, but large segments of the Democratic Party, the
rank-and-file are supportive of Harris and enthusiastic in that support so
much so that they'd volunteer that they would donate. This is probably one of
those things that the Democratic establishment knew early on and it
definitely, it's the sort of thing that would definitely impact their thinking
as far as whether or not they wanted to have somebody else present a challenge,
somebody else attempt to get the nomination. These numbers are big. They
don't even have to continue at this rate to suggest that momentum is definitely
moving in Harris's direction. You also have some polling that is showing a
shift as far as favorability and all of that stuff but again I am I am still
very skeptical of the polling. Obviously a shift in her direction is something
that would be measured as a good thing for her campaign regardless of the
accuracy of the polling, but it's hard to gauge how much the polling and the shifts
in the polling are actually going to matter at the real polls because, again, I have a
lot of skepticism, and it's skepticism that has kind of played out over the last few years,
it has shown to be, it's shown to be a good idea to be a little bit skeptical.
But even without the polling, these numbers, that isn't something that can be
swayed by a non-response bias. That isn't something that can be swayed by a less
than accurate census count or them extrapolating too much from way too
small of a sample. These numbers show a whole lot of momentum headed to Harris's
direction. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}