# All videos from July, 2024
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-07-31 23:14:09
<details>
<summary>
2024-07-31: Let's talk about change, progress, and evolution.... (<a href="https://youtube.com/watch?v=UWyPPw6vv8M">watch</a> || <a href="/videos/2024/07/31/Lets_talk_about_change_progress_and_evolution">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-31: Let's talk about Trump, Harris, and the Keys to the White House.... (<a href="https://youtube.com/watch?v=7gH0IS14W30">watch</a> || <a href="/videos/2024/07/31/Lets_talk_about_Trump_Harris_and_the_Keys_to_the_White_House">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-31: Let's talk about Harris, Trump, and the September debate.... (<a href="https://youtube.com/watch?v=djoM6RSQUPs">watch</a> || <a href="/videos/2024/07/31/Lets_talk_about_Harris_Trump_and_the_September_debate">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-31: Let's talk about Harris and outreach to dirt road Democrats.... (<a href="https://youtube.com/watch?v=SXAT7Otqzic">watch</a> || <a href="/videos/2024/07/31/Lets_talk_about_Harris_and_outreach_to_dirt_road_Democrats">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-30: Research Road EP 2 (<a href="https://youtube.com/watch?v=E66ebL0e1_Y">watch</a> || <a href="/videos/2024/07/30/Research_Road_EP_2">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-30: Let's talk about the person running against Boebert.... (<a href="https://youtube.com/watch?v=uZT50mOnxOI">watch</a> || <a href="/videos/2024/07/30/Lets_talk_about_the_person_running_against_Boebert">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-30: Let's talk about Trump, Schumer, and maybe a new VP pick.... (<a href="https://youtube.com/watch?v=OP6VJb2Snzg">watch</a> || <a href="/videos/2024/07/30/Lets_talk_about_Trump_Schumer_and_maybe_a_new_VP_pick">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-30: Let's talk about Harris VP picks, who's in, and who's out.... (<a href="https://youtube.com/watch?v=QG_n5mieUyU">watch</a> || <a href="/videos/2024/07/30/Lets_talk_about_Harris_VP_picks_who_s_in_and_who_s_out">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-30: Let's talk about California, Park Fire, and links.... (<a href="https://youtube.com/watch?v=v45LjliLaLs">watch</a> || <a href="/videos/2024/07/30/Lets_talk_about_California_Park_Fire_and_links">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-29: Let's talk about a video, physics, and changing a tire.... (<a href="https://youtube.com/watch?v=DtVf0P2GAX0">watch</a> || <a href="/videos/2024/07/29/Lets_talk_about_a_video_physics_and_changing_a_tire">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-29: Let's talk about Trump and never having to vote again.... (<a href="https://youtube.com/watch?v=WFrnIfYUntY">watch</a> || <a href="/videos/2024/07/29/Lets_talk_about_Trump_and_never_having_to_vote_again">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-29: Let's talk about Harris, momentum, and numbers.... (<a href="https://youtube.com/watch?v=KSBhd_VHUi0">watch</a> || <a href="/videos/2024/07/29/Lets_talk_about_Harris_momentum_and_numbers">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-29: Let's talk about Biden and SCOTUS plans.... (<a href="https://youtube.com/watch?v=2jtE0obSsPk">watch</a> || <a href="/videos/2024/07/29/Lets_talk_about_Biden_and_SCOTUS_plans">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-28: Roads Not Taken EP 49 (<a href="https://youtube.com/watch?v=YAgUYBjRZBc">watch</a> || <a href="/videos/2024/07/28/Roads_Not_Taken_EP_49">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-28: Let's talk about VPs, cats, families, messages, and systems.... (<a href="https://youtube.com/watch?v=TiCzD8VdWh4">watch</a> || <a href="/videos/2024/07/28/Lets_talk_about_VPs_cats_families_messages_and_systems">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-28: Let's talk about SCOTUS plans, justices, and Biden.... (<a href="https://youtube.com/watch?v=wwvJSPJ_zV0">watch</a> || <a href="/videos/2024/07/28/Lets_talk_about_SCOTUS_plans_justices_and_Biden">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-28: Let's talk about NY, Trump, and a constitutional amendment.... (<a href="https://youtube.com/watch?v=5Mo3T2YzTw8">watch</a> || <a href="/videos/2024/07/28/Lets_talk_about_NY_Trump_and_a_constitutional_amendment">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-28: Let's talk about NJ, Menendez, and replacements.... (<a href="https://youtube.com/watch?v=8pLIlagRX4M">watch</a> || <a href="/videos/2024/07/28/Lets_talk_about_NJ_Menendez_and_replacements">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-27: Let's talk about the House GOP and a break.... (<a href="https://youtube.com/watch?v=vGzUxz2Ie2o">watch</a> || <a href="/videos/2024/07/27/Lets_talk_about_the_House_GOP_and_a_break">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-27: Let's talk about Trump, flags, and the Constitution.... (<a href="https://youtube.com/watch?v=kP3yzWMBvZY">watch</a> || <a href="/videos/2024/07/27/Lets_talk_about_Trump_flags_and_the_Constitution">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-27: Let's talk about Harris, Trump, and the debate.... (<a href="https://youtube.com/watch?v=MPNaOR3SGAs">watch</a> || <a href="/videos/2024/07/27/Lets_talk_about_Harris_Trump_and_the_debate">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-27: Let's talk about Harris, Obama, Trump, and endorsements.... (<a href="https://youtube.com/watch?v=-5V1Dh48GGk">watch</a> || <a href="/videos/2024/07/27/Lets_talk_about_Harris_Obama_Trump_and_endorsements">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-26: Let's talk about voter registration and numbers.... (<a href="https://youtube.com/watch?v=ofHDCSWUuKk">watch</a> || <a href="/videos/2024/07/26/Lets_talk_about_voter_registration_and_numbers">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-26: Let's talk about the most important election of your life.... (<a href="https://youtube.com/watch?v=Bcfe7WBUQb4">watch</a> || <a href="/videos/2024/07/26/Lets_talk_about_the_most_important_election_of_your_life">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-26: Let's talk about lunar pits, caves, and humans on the moon.... (<a href="https://youtube.com/watch?v=f1muvM-NNeo">watch</a> || <a href="/videos/2024/07/26/Lets_talk_about_lunar_pits_caves_and_humans_on_the_moon">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-26: Let's talk about hottest day humanity has measured..... (<a href="https://youtube.com/watch?v=ikEGSsQ1xHM">watch</a> || <a href="/videos/2024/07/26/Lets_talk_about_hottest_day_humanity_has_measured">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-25: The Roads to what you make happen (<a href="https://youtube.com/watch?v=an74Q7kiUvA">watch</a> || <a href="/videos/2024/07/25/The_Roads_to_what_you_make_happen">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-25: Let's talk about shifting opinions on Project 2025.... (<a href="https://youtube.com/watch?v=3PIlmU1PWDA">watch</a> || <a href="/videos/2024/07/25/Lets_talk_about_shifting_opinions_on_Project_2025">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-25: Let's talk about Trump's nephew and healthcare.... (<a href="https://youtube.com/watch?v=b22bGn_69_M">watch</a> || <a href="/videos/2024/07/25/Lets_talk_about_Trump_s_nephew_and_healthcare">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-25: Let's talk about Illinois, Massey, and Harris.... (<a href="https://youtube.com/watch?v=4RyJ1VxXAIg">watch</a> || <a href="/videos/2024/07/25/Lets_talk_about_Illinois_Massey_and_Harris">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-25: Let's talk about Biden's speech.... (<a href="https://youtube.com/watch?v=wq-GVxzy6Ps">watch</a> || <a href="/videos/2024/07/25/Lets_talk_about_Biden_s_speech">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-24: Let's talk about the GOP, voting against your own interests, and energy.... (<a href="https://youtube.com/watch?v=jwqZetDqzG0">watch</a> || <a href="/videos/2024/07/24/Lets_talk_about_the_GOP_voting_against_your_own_interests_and_energy">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-24: Let's talk about primaries and democratic process.... (<a href="https://youtube.com/watch?v=EHONDr42pQo">watch</a> || <a href="/videos/2024/07/24/Lets_talk_about_primaries_and_democratic_process">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-24: Let's talk about a different requested state ranking.... (<a href="https://youtube.com/watch?v=U5vWkZNlzos">watch</a> || <a href="/videos/2024/07/24/Lets_talk_about_a_different_requested_state_ranking">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-24: Let's talk about Harris vs Trump and the future.... (<a href="https://youtube.com/watch?v=6kYyuu8R4A0">watch</a> || <a href="/videos/2024/07/24/Lets_talk_about_Harris_vs_Trump_and_the_future">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-24: Let's talk about Harris and a GOP strategy meeting.... (<a href="https://youtube.com/watch?v=sbmlTaQyW5Y">watch</a> || <a href="/videos/2024/07/24/Lets_talk_about_Harris_and_a_GOP_strategy_meeting">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-23: Research Road EP 1 (<a href="https://youtube.com/watch?v=5aOeI1le5_g">watch</a> || <a href="/videos/2024/07/23/Research_Road_EP_1">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-23: Let's talk about the days getting longer and warmth.... (<a href="https://youtube.com/watch?v=YCXMUDL6ExQ">watch</a> || <a href="/videos/2024/07/23/Lets_talk_about_the_days_getting_longer_and_warmth">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-23: Let's talk about the Harris, the GOP, blue suits, and tan suits.... (<a href="https://youtube.com/watch?v=ljwIvB9hpFA">watch</a> || <a href="/videos/2024/07/23/Lets_talk_about_the_Harris_the_GOP_blue_suits_and_tan_suits">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-23: Let's talk about Harris, delegates, and presumptions.... (<a href="https://youtube.com/watch?v=oMCX_ew1sEE">watch</a> || <a href="/videos/2024/07/23/Lets_talk_about_Harris_delegates_and_presumptions">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-23: Let's talk about Harris and Trump VP picks.... (<a href="https://youtube.com/watch?v=hW44HnByv3c">watch</a> || <a href="/videos/2024/07/23/Lets_talk_about_Harris_and_Trump_VP_picks">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-22: Let's talk about Harris, Obama, and Manchin.... (<a href="https://youtube.com/watch?v=OXU3zv_utUM">watch</a> || <a href="/videos/2024/07/22/Lets_talk_about_Harris_Obama_and_Manchin">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-22: Let's talk about Harris, Dark Brandon, and lightning rods.... (<a href="https://youtube.com/watch?v=b9kd4tehkEA">watch</a> || <a href="/videos/2024/07/22/Lets_talk_about_Harris_Dark_Brandon_and_lightning_rods">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-22: Let's talk about Democratic Convention's importance now.... (<a href="https://youtube.com/watch?v=69fD12x-hEg">watch</a> || <a href="/videos/2024/07/22/Lets_talk_about_Democratic_Convention_s_importance_now">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-22: Let's talk about Biden stepping down and the new dynamics.... (<a href="https://youtube.com/watch?v=ucfeU5lXARc">watch</a> || <a href="/videos/2024/07/22/Lets_talk_about_Biden_stepping_down_and_the_new_dynamics">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-21: Roads Not Taken EP 48 (<a href="https://youtube.com/watch?v=NM2GyLQK1fE">watch</a> || <a href="/videos/2024/07/21/Roads_Not_Taken_EP_48">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-21: Let's talk about your candidates and finding out about them.... (<a href="https://youtube.com/watch?v=jNSWGpxqjiE">watch</a> || <a href="/videos/2024/07/21/Lets_talk_about_your_candidates_and_finding_out_about_them">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-21: Let's talk about a tank analysis and aging systems.... (<a href="https://youtube.com/watch?v=HFrp6Qa7meY">watch</a> || <a href="/videos/2024/07/21/Lets_talk_about_a_tank_analysis_and_aging_systems">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-21: Let's talk about Harris and constitutional requirements.... (<a href="https://youtube.com/watch?v=Rkax42v9mFE">watch</a> || <a href="/videos/2024/07/21/Lets_talk_about_Harris_and_constitutional_requirements">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-21: Let's talk about AZ, water, spending, and education.... (<a href="https://youtube.com/watch?v=nRlFyPKSZuQ">watch</a> || <a href="/videos/2024/07/21/Lets_talk_about_AZ_water_spending_and_education">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-20: Let's talk about your vote, red and blue states, and mandates.... (<a href="https://youtube.com/watch?v=pLdJq8jeocE">watch</a> || <a href="/videos/2024/07/20/Lets_talk_about_your_vote_red_and_blue_states_and_mandates">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-20: Let's talk about Trump, latest info, and processing info.... (<a href="https://youtube.com/watch?v=us_-VRjJRmM">watch</a> || <a href="/videos/2024/07/20/Lets_talk_about_Trump_latest_info_and_processing_info">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-20: Let's talk about Presidential lines of succession.... (<a href="https://youtube.com/watch?v=8laKejapWZ8">watch</a> || <a href="/videos/2024/07/20/Lets_talk_about_Presidential_lines_of_succession">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-20: Let's talk about Ohio removing 160,000 voter registrations.... (<a href="https://youtube.com/watch?v=bWRWCv3W1gY">watch</a> || <a href="/videos/2024/07/20/Lets_talk_about_Ohio_removing_160_000_voter_registrations">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-19: Let's talk about why Biden can't fix Trump's documents case.... (<a href="https://youtube.com/watch?v=cbwlb6EtBWQ">watch</a> || <a href="/videos/2024/07/19/Lets_talk_about_why_Biden_can_t_fix_Trump_s_documents_case">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-19: Let's talk about quality of life and various states.... (<a href="https://youtube.com/watch?v=Hc-QaPLjImM">watch</a> || <a href="/videos/2024/07/19/Lets_talk_about_quality_of_life_and_various_states">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-19: Let's talk about maintaining hope.... (<a href="https://youtube.com/watch?v=vcWBtsAQ0ww">watch</a> || <a href="/videos/2024/07/19/Lets_talk_about_maintaining_hope">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-19: Let's talk about Trump's acceptance speech.... (<a href="https://youtube.com/watch?v=Pnbqq-G9nUE">watch</a> || <a href="/videos/2024/07/19/Lets_talk_about_Trump_s_acceptance_speech">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-18: The Roads to Replacing Biden (<a href="https://youtube.com/watch?v=wPRxuOWkr1E">watch</a> || <a href="/videos/2024/07/18/The_Roads_to_Replacing_Biden">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-18: Let's talk about Trump, new info about motivation, Biden, and PA.... (<a href="https://youtube.com/watch?v=lEQNTDomIzI">watch</a> || <a href="/videos/2024/07/18/Lets_talk_about_Trump_new_info_about_motivation_Biden_and_PA">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-18: Let's talk about Trump's new campaign strategy.... (<a href="https://youtube.com/watch?v=TLTGdDBhjIQ">watch</a> || <a href="/videos/2024/07/18/Lets_talk_about_Trump_s_new_campaign_strategy">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-18: Let's talk about Trump v Biden according to economic forecasters.... (<a href="https://youtube.com/watch?v=i3v_4ROj79U">watch</a> || <a href="/videos/2024/07/18/Lets_talk_about_Trump_v_Biden_according_to_economic_forecasters">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-18: Let's talk about Biden's rent plan and the catch.... (<a href="https://youtube.com/watch?v=CpgNuACKay8">watch</a> || <a href="/videos/2024/07/18/Lets_talk_about_Biden_s_rent_plan_and_the_catch">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-17: Let's talk about the Trump and RFK Jr phone call.... (<a href="https://youtube.com/watch?v=EUzJUc45N08">watch</a> || <a href="/videos/2024/07/17/Lets_talk_about_the_Trump_and_RFK_Jr_phone_call">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-17: Let's talk about the RNC, O'Brien, and the right questions.... (<a href="https://youtube.com/watch?v=yStDsKSOP88">watch</a> || <a href="/videos/2024/07/17/Lets_talk_about_the_RNC_O_Brien_and_the_right_questions">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-17: Let's talk about the RNC, Biden, and news cycles.... (<a href="https://youtube.com/watch?v=cAoBsHUJ1vs">watch</a> || <a href="/videos/2024/07/17/Lets_talk_about_the_RNC_Biden_and_news_cycles">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-17: Let's talk about NJ, Menendez, and what's next.... (<a href="https://youtube.com/watch?v=XSEuwj8gwHA">watch</a> || <a href="/videos/2024/07/17/Lets_talk_about_NJ_Menendez_and_what_s_next">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-16: Let's talk about trends, Taylor Swift, and a project.... (<a href="https://youtube.com/watch?v=01U8PHhtvEs">watch</a> || <a href="/videos/2024/07/16/Lets_talk_about_trends_Taylor_Swift_and_a_project">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-16: Let's talk about the documents case being dismissed.... (<a href="https://youtube.com/watch?v=-CfSzCUpwa8">watch</a> || <a href="/videos/2024/07/16/Lets_talk_about_the_documents_case_being_dismissed">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-16: Let's talk about Trump, new info, and a reminder about intent.... (<a href="https://youtube.com/watch?v=dsDI12WNYhQ">watch</a> || <a href="/videos/2024/07/16/Lets_talk_about_Trump_new_info_and_a_reminder_about_intent">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-16: Let's talk about Trump's VP pick.... (<a href="https://youtube.com/watch?v=14brKebjIPk">watch</a> || <a href="/videos/2024/07/16/Lets_talk_about_Trump_s_VP_pick">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-07-15: Let's talk about Trump, speeches, and changes.... (<a href="https://youtube.com/watch?v=qOY9u5bcxzc">watch</a> || <a href="/videos/2024/07/15/Lets_talk_about_Trump_speeches_and_changes">transcript &amp; editable summary</a>)



</summary>
</details>
