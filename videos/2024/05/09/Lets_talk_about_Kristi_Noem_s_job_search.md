---
title: Let's talk about Kristi Noem's job search....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=nUXF4ruYZq0) |
| Published | 2024/05/09 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk
about the governor of South Dakota, Christine Ohm,
once again, and go through the latest developments
in what seems to be a never-ending string
of developments concerning her.
So we will talk about a position that she was reportedly interested in and a little
bit more about her book and where all of this has led her.
Okay so the reporting says that Gnome wanted to be boss over at the NRA, the National Rifle
Association and that she kind of pitched herself last fall and indicated she
would be willing to step down as governor to take that position. That is
obviously something that is probably going to resonate with voters in South
Dakota on top of everything else. It is worth noting that her spokesperson has
unequivocally denied that she was looking into this and was going to step
down. Basically none of it's true. So there's that. In more developments
concerning her book, it went on sale on Amazon, which means the reviews were
open. They were scathing, for lack of a better word. There was one that caught my
eye that said, my cat loved this, and he says that Cricket had it coming. That was
pretty representative of the overall tone. Let's just say it did not have a
bunch of stars. It did not have a high rating. So much so that from my
understanding Amazon actually turned off the reviews on the book. Now all of this
has led to a situation that has even turned Trump away. According to the
rumors coming out of Trump world, she is no longer on his short list for vice
president. That he's basically decided that she is too much of a
liability. So there is that development. That is one more name off the list of
people that Trump is reportedly considering to be his vice president. So,
I would imagine this is the end of the story, but there's, there may be more to
come. Anyway, it's just a thought. Y'all have
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}