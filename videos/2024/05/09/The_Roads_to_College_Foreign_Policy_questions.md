---
title: The Roads to College Foreign Policy questions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=N2zsjzwJaBM) |
| Published | 2024/05/09 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
It's Beau again, and welcome to The Roads with Beau.
So today, we're gonna go through a message,
and according to the team,
it has a whole bunch of questions
about foreign policy in it,
and they say it's a good list of questions,
and it leads up to a big one that is really important.
We'll find out here in a second.
And all I know is that it's from somebody at college.
Okay, Hi Bo or person reading Bo's email. I have a bunch of questions. I've watched
Bo since 2019, but most times I turn off the video when the opening includes today we're
going to talk about foreign policy. Nice. I had a long conversation with a friend after
attending a demonstration at my university, and it sent me down a diplomacy rabbit hole
YouTube videos and blogs and articles. I notice that there are two separate
worlds. There's the conversations that we have on campus where we talk about the
big events of each day and then there's the world of people who talk about
diplomatic efforts where people focus on the smallest change in language. Yeah.
Honestly, I thought you had really lost touch from the few videos I saw of
of yours on Gaza over the last few months.
But what you're saying is mostly the same thing
that is being said by other people who talk about this.
And you all turn out to be right more often than not
when looking at things that occurred
after you all said them.
Even if you're not from the South, please start using y'all.
I have a list of questions in no particular order
and they're all leading up to a big one
really matters to me. So if you can only answer one, answer that one. Okay, all of you say
the U.S. will never leave Israel. Why? It's just an accepted thing. The closest to an explanation
I found was you saying there are three powerful countries, Iran, Saudi Arabia, and Israel,
and that we want to have two of them on our side. So because of that, they can do whatever.
I mean, it's not quite that simple, but kind of.
Those countries will definitely get a lot more leeway, Israel and Saudi Arabia.
They will get more leeway from the United States because the balance of power is that
important.
Maintaining regional stability, which in real life is a regional stability with the U.S.
at the top, is more important to the US than any one particular thing in the region.
So because Israel is important to the current landscape of that region, and they're in
the US sphere, they're helping the US side of the balance of power, the US is there.
The U.S. is never going to leave Israel as far as breaking an alliance.
That is incredibly, incredibly, incredibly unlikely.
Okay.
You all say stuff like, nothing simple, there are no simple solutions, or the easy path
is always mine, which by the way seemed like a Minecraft reference to me.
What exactly makes everything complicated to the point that we can't call bad, bad?
Everything's intertwined.
Everything is connected to something else.
The simple solution isn't.
Something as simple as a defense pact can alter shipping routes.
It's everything is intertwined and one thing impacts another that impacts another that
impacts another.
A lot of times the simple solution could make the problem worse or it could have unintended
consequences or it may not actually do what people think it'll do.
There's a lot of moving parts when you are talking about foreign policy.
What actually determines U.S. policy toward other countries, because it darn sure isn't
democracy.
No, it's not.
You're right, it is not.
What actually determines U.S. policy?
This would also feed into the previous question.
Resources in that country, economic policy, trade routes through or near the country,
the domestic demographic information related to that country, meaning like Ireland, because
there are a lot of people in the United States who identify as Irish American, they will
have more interest, they'll be treated better, for lack of a better term, than a country
that doesn't have a lot of representation within the United States.
Then you have to tie in military power, the internal stability of those countries.
Like, if the country goes through a rapid change of government, a coup, every few years,
the U.S. doesn't put a lot of effort into long-term planning.
It just deals with the chaos.
Obviously, the economic power of the country, aside from the economic policy and the duration
of that, meaning if the country is currently rich, but in five years, they're going to have burned through all of their
resources,
the U.S. isn't going to spend much time with them. Conversely, if it's an emerging economy,
and even though they may not be wealthy now, the U.S. may spend a whole bunch of time trying to get them into our
sphere,
on our side. Then you have defense pacts, nuclear power, all kinds of other things.
That's what really determines U.S. policy and the interplay between all of it.
And that's why there are no simple solutions because it's all connected.
And one thing impacts the next, that impacts the next.
Why is the UN so weak?
Because people misunderstand it, mainly.
The UN is not, it's not there to bring about justice.
the UN is weak for two reasons. One, it doesn't have its own troops. Two, it doesn't do what
people think it does, and it was never meant to do that. It's not there to bring about
justice, it is there to maintain the status quo that existed at the end of World War II
and keep those major powers as major powers. And it's actually really good at that. People
just have a bad view of what it's supposed to do.
It is weak when it comes to stopping conflicts, once they start.
Okay, why do all of you, and I mean every single person I watch, roll your eyes, every
single person I watched roll your eyes when you get a question with the word
just in it. Any message that says Israel slash Biden slash Hamas can just do a
thing seems to make you all unhappy because nothing is that simple. I'll go
ahead and tell you any solution that includes the word just in that context
and really just as in meaning justice as well really doesn't have much to do
with foreign policy.
It's not just that simple.
Anything that includes that, all Israel has to do is just X.
That's not true.
I don't even care what X is because there's way more to it.
There aren't simple solutions.
They're complicated.
So most times when somebody includes just in their statement or their talking point,
what follows is something that is, if it was that easy, the army of PhDs that have 2,000
years of combined experience probably would have thought of it.
It's just an indication that what's going to follow is probably not going to be a serious
foreign policy take.
Why do all of them get mad when people ask why Arab countries don't take the Palestinians
in, as in visibly angry?
I say them because I haven't seen you get mad over this.
This is still a question that makes me uncomfortable.
I don't get mad because I have a friend who happens to be Jewish who talked to another
Jewish person about this question in front of me.
This is one of those situations where what is meant by what's being asked is not what
What is heard by foreign policy people?
When people ask, well, why won't Arab countries take them in?
Generally speaking, the person asking this is Jewish.
What they're really asking is why aren't Arab countries like us?
That's really the question.
It boils down to because of the history of Jewish people and all the horrible things
that happen to them, the country that they have, it is interested in protecting its own
people.
So they wonder why an Arab country wouldn't take in refugees that are right next door.
The real question is why don't they have the same bond?
Or why doesn't the country behave in the same way?
That's really what they mean.
I watched my friend use the Socratic method to just ask question after question after
question about this and then at the end the person was like oh no no no no that is totally
not what I meant because if you don't know what foreign policy people hear when this
question is asked is very very different.
They hear why can't we take this specific ethnic group and remove them.
That's why they get mad.
But in my experience, that's not what is meant by the question.
It's a legitimate question about the differences between the countries and cultures.
It isn't a call to action, which until I watched that conversation unfold, that's
what I thought it was too.
And it always made me uncomfortable.
But that's not what's meant by it.
But yeah, foreign policy people tend to get super uncomfortable when people start suggesting
cleansing.
And even though that's not what's meant, a whole lot of foreign policy people hear that
when that question is asked.
So there seems to be consensus around the idea that the Palestinian authority is weak
or not respected by Palestinians, but nobody explains why.
So short answer on this one.
A whole lot of Palestinians view the Palestinian authority
as too friendly with Israel, to the point of maybe even
collaboration.
There's a lot of hostility around that.
And again, it's hard to gauge how widespread that is.
it seems, in my opinion, it's pretty widespread.
So when they talk about using the Palestinian authority
as the governing body, there's a lot of work
that has to be done.
And a lot of it has been, but I mean,
maybe it has done enough to rehabilitate the image,
or maybe there are people that have
a good idea of what they're going to do,
and they're more accepting of it.
But that's a sticking point.
That's a sticking point that would
be a problem in any long-term plan.
Because if the Palestinian people do not
accept the governance that is in place,
your plan doesn't matter.
This is one of those things where it could break down.
so.
Is there ever any hope of going from the Palestinians getting all their land back?
Is there ever any hope of the Palestinians getting all their land back?
No.
No, not really.
You're now, depending on how you count it, you would be talking about displacing 7 to
10 million people.
That is one of those things that is not in the cards.
There's a chance of getting land back as in getting a state of their own.
All of their land back, meaning like back to the before the forties?
No, that is not in the cards.
After watching all of the videos I have, I believe Biden, or at least his team, is trying
to get to a two-state solution.
And how do they overlook all of the people dying right now?
Man, that's a question that I have been very happy that nobody has asked bluntly.
Okay, so how do they overlook of it?
How do they overlook it?
Some of them don't.
Some of them don't.
You probably have three different phases of this.
You have younger people, idealistic, new to State Department, new to the teams, and they're
unhappy with it because this is not what they pictured.
They want to do something about it right now and they feel like more pressure should be
exerted.
These are the people you see that are resigning.
These are the people you see that are leaking stuff or they're in the dissent channel.
who they are. Then you probably have a second group, which would be the majority of them,
who rationalize it. They understand that a Palestinian state would stop this from happening
again. And if there isn't a Palestinian state, then it almost certainly will happen
again. So they view it as this was going to happen no matter what, we can use this to
build a two-state solution. Literally, if you want to make an omelet. I mean, that's
horrible, but that's probably how they overlook it. They don't focus on that. They focus
on the end goal.
And then you have the people who have done that for so long that they probably realize
that the severity of it increases the likelihood of it being an oma moment, of it being a moment
where peace is possible.
And that, once you have crossed into that territory, that's how people like Henry Kissinger
are made.
It's foreign policy.
Morality's got nothing to do with it.
It is literal, ends justifies the means.
They can't do anything to stop what is actually happening at this moment.
So they use it to build off of.
The big one.
I've seen a number of channels say that if Biden can get his plan in place, that we will
look ridiculous because we're just asking for a ceasefire and calling him names while
he was busy getting them a state.
I don't care about looking ridiculous, but have we hurt his chances of succeeding?
No no no no no no no no.
Definitely not.
No.
Nothing exists in a vacuum.
That includes his decision making.
There is nothing.
There is nothing that suggests he and his team would be trying to do what they're
doing without the pressure.
I mean, yes, in fact, I made a joke about this earlier where I was like, yeah, you know,
you're going to keep saying this and then if it happens, you're going to immediately
be to the right of Joe.
But that's okay.
No, I personally do not believe that they would have the position they have and be doing
the things that they are if there wasn't pressure.
I don't believe that.
I do not think people understand how big a deal it is that they paused a weapon shipment
to a U.S. ally during wartime.
That's huge, huge.
I do not believe that that would have happened without pressure.
No, you have not hurt his chances of succeeding.
It may not have even been attempted without the pressure.
And as far as the other part, yeah, don't worry about looking ridiculous.
Your job as an activist is to affect change.
It's not to have the most radical take in the room.
It's to actually succeed.
And there is a very decent chance that these, not just demonstrations, because it started
before then, but the outcry that occurred, that it shifted what they were attempting.
Shifting U.S. foreign policy is not an easy thing.
This will probably be the only time you're actually successful in your life.
And then, if because of that shift, a two-state solution does occur, yeah, sure, you've been
calling them names that maybe you shouldn't have, or whatever, but there's no way to
say that a two-state solution would have occurred if you didn't do that.
exists in a vacuum. No, I do not believe you have hurt his chances of succeeding. If anything,
I think you have laid the groundwork for the attempt. The only ways that the pressure could
actually hurt would be if so if he succeeds okay and there's a two-state
solution and that pressure isn't converted to Congress to get the funding
or the ratification of a treaty or something like that through that would
hurt. That would hurt. So it just has to stay mobilized. Or if they're there, they have
the plan in place, but because of because of calling him names, he doesn't get reelected,
and it doesn't go into effect. As in, they're there, but it hasn't been pushed through
That's it.
Those are the only two ways I can see it actually being harmful.
Again, you're allowed to put pressure on your politicians.
I do not believe that it would have hurt his chances of succeeding.
If there's somebody out there saying that, I don't want to say that there
I mean, I can't think of a way that would be true.
Not in past tense.
There are things coming up that might hurt the chance of succeeding, but nothing so far.
Your job as an activist is to affect change.
There is nothing in the last 30 years of foreign policy that suggests the moves that are being
made now would have been made without pressure.
Whether or not it's successful, that's another question.
That's an entirely different thing because that relies on multiple countries and non-state
actors all agreeing to do something.
It's a mess.
But this is the best chance I've seen in 30 years.
And I wouldn't even worry about looking ridiculous.
If he's successful, if a real peace process is started, you'll know.
I mean, it's overnight, sure, once it's signed, but you'll know it's coming.
I would not be concerned about this.
I would not be concerned about your actions impeding a long-term peace plan.
That's not really a thing.
Yeah.
I'm sitting here trying to think of other ways where it could happen, but they're getting
out there.
It's like the people who take a more militant stance than the actual combatants, even though
they're here in the United States, they would have to gain a massive platform within that
movement and say that the deal doesn't go far enough for something like that to
derail it. Like you are talking about huge outlier scenarios that would
probably just be ignored. So I do not think you should worry about that. And
again when it comes to calling him names, it's foreign policy. Nothing about it is
fair. That goes for them too. For all we know, he's doing even more than what we know about.
Okay? And he's getting called names for it. That's the name of the game. He's a big boy.
So I don't think anything here is a legitimate thing that you have to worry about.
And definitely do not let it, if you genuinely felt that Biden was doing nothing and you,
because after reading all of this, this is kind of what I suspect, that you felt that
he was doing nothing and now you're starting to realize that he has been doing some stuff.
All that means is that maybe don't turn off the videos that say, you know, today we're
going to talk about foreign policy.
I mean, that's it.
You're allowed to make mistakes, and again, there's still no guarantee that this is going
to succeed.
What has been confirmed right now and what foreign policy people are out there taking
victory laps about is that they knew that he was making moves.
I mean, yeah, but that's kind of what we're supposed to know.
It'll be a victory lap if he's successful.
That's what matters.
And honestly, at that moment, there's a whole bunch of people that will be able to say,
I played a small part in this, and I do not think that it would be occurring if there
wasn't pressure.
So even if you do very much dislike him, or have called him names, or whatever, that doesn't
mean that you don't have a part in what's happening if it's successful.
Of all the things to be worried about, this shouldn't be one.
And we'll just leave it at that.
OK, so there we go.
A little more information, a little more context,
and having the right information will make all the difference.
Now have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}