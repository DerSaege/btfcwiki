---
title: Let's talk about Trump, Biden, and polling....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=wWibk3_SOuo) |
| Published | 2024/05/09 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about Trump and Biden and polling.
And something that Biden said in an interview on TV,
and we'll talk about a few things that he said during this,
but right now we are going to focus on what he said about the polling.
Because during the interview, he kind of indicated that he believed
that the polling was off due to a non-response bias.
Now, it's Biden, he did not say it's off due to a non-response bias.
I think he said something like, come on, Jack,
how many thousand phone calls do you have to make to get one answer?
Which is a very President Biden way of indicating that he believed it was a non-response bias.
If you don't know what that term means, it means a group of people who are likely
to poll one way, meaning they are likely to support Biden.
They are less likely to participate in the survey.
Therefore, it skews your results.
Normally, that's not a good answer.
Normally, non-response bias is not a good answer.
It's normally not a huge thing.
However, right now, the way a lot of polling
being conducted, it's by phone. The people who are being polled are the
people who are willing to answer their phone to an unknown number. That is not a
trait that is shared by all demographics. A whole lot of people don't do that just
as a general rule. Do I think he's right it's non-response bias? No. I don't think
that's all of it. He seemed to indicate that he believed that's what was
causing it. I think that's part of it. I don't think it's all of it. I also think
that the pollsters are having a hard time determining who the likely voters
are going to be. Those two things in conjunction seem to be throwing things
off. Is there any evidence that they are off? Yeah, Trump keeps
underperforming. Trump keeps underperforming. If he's, if the polling says that he's going to get
80%, he's only getting 70, something's wrong there. A good example, if you want to look at something
that's going to be in the news right now, look at Indiana. If you look at most polling from there,
Trump is blowing Biden out of the water, I mean, big time, to the point where to get those numbers,
he has to get all of the Republican votes and most of the independent.
But then you look at the Republican primary.
Trump lost one out of five Republican voters.
Nikki Haley got more than 20%.
She's not even running it.
dropped out like two months ago. There is not as much enthusiasm for Trump as is
being portrayed in the media. We can see that when we look at the results of the
primaries. Now the question is, when it comes down to the general, are those
voters just going to look at it and vote for the name that has the R behind it? Or
or are they going to vote on some kind of principle?
Or the real issue that comes into play
when you talk about enthusiasm is,
are they going to vote?
End of sentence.
Because they may not.
They may not.
So I think he's right, and we've talked about it before.
I think the polling is off.
There's something wrong.
Do I think that it's entirely a non-response bias? No.
I think there's more to it than that.
Is it enough to overcome the gap in the polling, in some of the polling?
I don't know.
I don't know because I don't have
a clear answer on it.
It's something we'll have to pay more attention to as time goes on.
When he said that,
that's what he was saying.
When he was talking about the polling
and kind of blowing it off, he was saying that he believed there was a significant non-response
bias in a very, very Biden way.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}