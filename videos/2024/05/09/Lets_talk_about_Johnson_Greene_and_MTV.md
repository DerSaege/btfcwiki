---
title: Let's talk about Johnson, Greene, and MTV....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=MBM6Kuw4pX4) |
| Published | 2024/05/09 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about Speaker Johnson,
who looks like he will remain Speaker Johnson,
at least for a little bit.
We'll talk about that, Marjorie Taylor Greene,
and her move to get rid of the speaker,
how that played out, and where it might go from here.
So Marjorie Taylor Greene, she wanted her MTV. She wanted her motion to vacate. She
pushed it to the forefront and it was tabled almost immediately. If you're not
familiar with the language, it was defeated. It's gone. And it was defeated
by a wide margin, 354 to 243. So Johnson kept his job with Democratic Party
support. Whole bunch of questions asking why the Democratic Party helped. There
will be a video down below explaining the options that they had and why they
went this route. So the MAGA faction, Twitter faction, they took a hit. Even
Even though most of them actually voted to table it, they voted in support of Johnson,
it doesn't matter because it's going to impact the whole crew and help undermine their influence.
Johnson got to keep his job as speaker because the Democratic Party allowed him to keep his
job.
The other benefit of going this route was the potential to cause more Republican infighting
in discord.
Doing that, it might require a little bit of democratic encouragement to keep that little
feud going.
Like something that might do it is coming out and saying, okay, we protected him once,
that doesn't mean we're going to do it again.
And just leave it at that.
Marjorie Taylor Greene takes the bait and tries to get more votes for another
attempt. Incidentally, in totally unrelated news, almost immediately after
tabling the motion, the Democratic Party came out and said, we did it once, that
doesn't mean we'll do it again. Nice. One of the other, one of the other
developments here is that the 43 votes in favor of Marjorie Taylor Greene's move, 32
of them came from the Democratic Party, most of them progressive Democrats.
Only 11 of the votes came from right-wing Republicans.
So her move was supported primarily by progressive Democrats, by like three to one almost.
The attack ads write themselves on that one.
That was, again, nice.
So from here, for the time being, Johnson has control of the House.
There will certainly be murmurs that will go up on social media basically saying that
he's in the Democratic Party's pocket and all of this stuff because they protected him.
I mean sure, maybe he owes them a favor or two, but I wouldn't say he's in their pocket.
It isn't very often that the Democratic Party in the House plays politics in kind of a hardball
way.
They did here.
They did.
Okay, if that little bit about the progressive Democrats voting in support of Marjorie Taylor
Greene's motion, if that was planned, genius.
That was really smart because that is certainly going to come up because the establishment
Republicans, the non-Twitter, non-maga faction, can use that to paint her as if she's truly
damaging the Republican Party because those evil progressives are on her side.
And they probably will.
Now one of the interesting bits about this was one of her key complaints about Johnson
was bringing stuff to the floor that the majority of the majority didn't support, meaning the
majority of the Republican Party because they have the majority right now.
And the fact that her motion, her move, was not supported by the majority of the majority,
that's probably also going to come up.
I know they've already kind of poked at her about it.
I would imagine that's going to become a thing too.
For once, when it comes to the Democratic Party playing politics in the House, no notes.
I mean, it puts the Republican Party on its back foot.
The infighting remains, but it allows for some stability in case something important
like a budget or something needed to be passed.
If something came up, there's enough reaching across the aisle now that it wouldn't create
long delays, at least hopefully.
Yeah, overall, this is a win.
Even though Johnson is still speaker, this is a win.
This is about as good as they could do with the situation
they were in.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}