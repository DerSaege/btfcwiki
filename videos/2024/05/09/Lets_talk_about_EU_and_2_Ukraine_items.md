---
title: Let's talk about EU and 2 Ukraine items....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=tzK5gkfSCUY) |
| Published | 2024/05/09 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, Internet people. It's Bo again.
So today we are going to talk about a couple of news items
both dealing with Ukraine. But we will talk about Ukraine,
the EU, Zelensky, Russia, and just run through a few brief items.
The first deals with the European Union. The EU has
reportedly kind of come to an agreement because when everything started, when
this all kicked off, when Russia invaded, there was a lot of movement when it came
to Russian state assets and by movement I mean they were frozen. The thing is
those assets generate interest. It appears that the European Union has come
to an agreement in principle. It's not finalized yet but an agreement in
principle that would allow Ukraine to, well, benefit a little bit from that
interest through weapons and through humanitarian aid. My understand is the
overwhelming majority of it will be weapons. My guess is that the
humanitarian aid is really there to represent those countries within the
European Union that are neutral. So the obvious question is how much money are
we talking about, right? Around 250 million dollars a month, a month. You know,
when we talk about aid for foreign countries, it's normally in billions.
This is 250 million a month so around 3 billion a year. Now there are a couple of
things about this. One, Russia is going to be super mad. And two, this is another
sign that the West is setting up the infrastructure to support Ukraine for
years. They're setting up the infrastructure to continue support for a
very very long time. That is also going to be very upsetting to Russia because
they had counted on the Republican Party in the United States basically not
letting the aid package go through and they have been counting on the West to
lose interest. That doesn't appear to be happening. Now, this agreement is supposed
to be finalized later this month from what I understand. The other bit of news
is something that has occurred a few times before and will probably happen
a few times, you know, later on, but it appears that there was another plot to
after Zelensky. It failed because he dodges hits like he's in the Matrix, but
it does appear that there was a plot to take him out. And early indications are
that it was linked to Russia. It's not confirmed yet, but all signs are pointing
that way right now. There have been arrests within Ukraine and I'm sure that
there will be more developments and more information coming soon.
So that's where things are there.
Anyway, it's just a thought.
Y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}