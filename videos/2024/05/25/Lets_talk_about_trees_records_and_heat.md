---
title: Let's talk about trees, records, and heat....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=kGG-1j3Cf60) |
| Published | 2024/05/25 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about trees
and what trees can tell us
because they store information in a unique way.
And a lot of it, I think people know,
but maybe didn't think of applying it this way.
We knew that last year was the hottest year on record. Now the problem with that
sentence is that those people who are committed to believing that there isn't
an issue, those people who are committed to convincing people that there isn't an
issues so they can continue to rake in giant profits, well, they can always lean into the
fact that our records only go back so far.
We only have like 150 years or whatever.
How conclusive can it be?
So trees store, they store records about temperature.
They don't mean to, but they do.
Most people know that rings inside of a tree, it can give you the age.
The distance apart between the rings can tell you the temperature because trees grow more
when it's warmer, therefore the rings are further apart.
So when it's warmer, the space between the rings is wider.
When it is colder, well, it's a skinny ring.
A study went through and compared tree rings.
And what they determined was that the Northern Hemisphere, the summer of 2023, was the hottest
summer in 2,000 years.
That's a pretty extensive period of time to be at the top of.
This method is probably going to continue to be used.
My guess is they will find a way to do it in the southern hemisphere as well.
When you are talking to people and they bring up the fact that our records only go back
so far, and we don't really know, well now we do, because those rings are available.
They were counted, they were catalogued, they were analyzed.
Last summer was definitely the hottest summer of your life, so far.
Anyway, it's just a thought y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}