---
title: Let's talk about North Carolina, the GOP, and veterans....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=L5DVcU2Pg80) |
| Published | 2024/05/25 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
Let's battle again.
So today, we are going to talk about North Carolina.
We're gonna talk about the Republican Party
in North Carolina.
We're going to talk about a chain of events
and some veterans groups,
and just kinda go over what happened.
There will be links down below.
I normally don't put a whole bunch of links down below,
but I will put them there in this case
because y'all aren't gonna believe this.
Okay, so a couple of veterans groups,
I know there were two,
Common Defense and Veterans for Responsible Leadership.
They started their day on Friday,
I think at the military park there
by the courthouse in Grangeboro and you know,
They're getting together, as veterans groups tend to do on Memorial Day weekend.
After that, they went to the North Carolina GOP party convention, and well, I'm just going
to read some social media posts here.
Quote, after delivering a letter calling on the GOP and Donald Trump to reject calls for
for violence and inflammatory rhetoric, they were, quote,
escorted out.
Here's another one.
We went to the North Carolina GOP convention with a demand
that they pledge to honor the election
and support a peaceful transfer of power.
They didn't want to hear from us.
They haven't signed that pledge until they do.
leadership and their nominees are unfit to lead. If you're wondering there is
footage on social media. So veterans for responsible leadership they have found
quote, Donald Trump unfit to serve as commander-in-chief if elected again his
prior actions demonstrate that he is a known and serious risk to service
members are a political-military tradition and to the U.S. Constitution.
There's also this, which looks like it's from a press release.
We are demanding that Republican leaders now in Greensboro use all their influence to force
Donald Trump to renounce these awful and dangerous threats of violence, which have no place in
democracy.
must commit to a peaceful and non-violent election season.
I mean, taking people from veterans groups who are trying to get a pledge to honor the
basic principles of the Constitution and, you know, just the core stuff of the country,
and telling them that they need to leave on Memorial Day weekend, I mean, that is certainly
a choice that a political party could make.
I believe that's what the kids would say is not a good look, though.
Yeah, so this is a relatively small thing.
This is something that occurred in North Carolina.
And at time of filming, really hadn't gotten a lot of coverage.
In normal political times, this would be front page news.
Even in the very bizarre timeline
we are currently existing in, I feel
like this is going to become a story.
Generally speaking, veterans groups
tend to be treated relatively well
by politicians who really want to tap into that,
that voting block.
I feel like them reportedly being asked to leave
to leave on Memorial Day weekend given the pledge that should be incredibly
non-controversial. I feel like this is probably going to become a national news
story. So I would watch for this to show up again. I will have links down below to
some information about the organizations and any coverage I can find of it. So
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}