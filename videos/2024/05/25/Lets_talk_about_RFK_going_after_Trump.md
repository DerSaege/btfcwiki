---
title: Let's talk about RFK going after Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=jSd42BL4zz4) |
| Published | 2024/05/25 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about RFK.
We're gonna talk about Kennedy,
and we are going to talk about Trump
and what Kennedy said about Trump,
whether or not Trump will really be able to respond
in any meaningful way, what brought all of this about,
and what this dynamic tells us about the Trump campaign.
Okay, so both candidates, RFK Jr. and Trump, are addressing the libertarian party.
And Kennedy went yesterday, and he spoke about things that are core to
the Libertarian belief system.
He said, I think he had the right instinct when he came into office talking about Trump.
He was initially very reluctant to impose lockdowns, but then he got rolled by his bureaucrats.
He caved in and many of our most fundamental rights disappeared practically overnight.
He went on to say President Trump said he was going to run America like a business and
he came in and he gave the keys to all of our businesses to a 50 year bureaucrat who'd
never been elected to anything and had no accountability.
Now I know for most people watching this channel, this is not something that appeals to you,
but to a whole lot of libertarians, that's going to resonate.
The proof of that is that at one point when Trump's name was mentioned, booze erupted.
There was also a moment where I'm assuming it was a joke, even though it appeared to
be seconded, there was the idea that the party should tell Trump to go have fun
with himself and somebody replied that was my motion too. It elicited applause.
So whether or not the Libertarian Party supports RFK Jr. it seems unlikely
based on what happened yesterday that they're going to support Trump in any meaningful numbers.
Now Trump could try, he's reportedly supposed to speak to them today, he could try to turn
it around, but going over what Kennedy said, he's going to have a hard time.
Now it is worth noting that there are some libertarians who are upset with this whole
thing because they feel that they should be promoting libertarian candidates
rather than bringing in non-libertarians. I think one of the more interesting
things is Trump saying he would do this. If you talk to Trump's campaign, if you
listen to their statements, they cast the image that Trump is unstoppable.
That he doesn't need votes.
He doesn't even need Nikki Haley's votes.
He's got this in the bag.
The Libertarian Party, they're not a huge organization, but Trump is going out of
his way to try to capture those votes.
So, I feel like maybe the campaign is overestimating how well they think they're doing.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}