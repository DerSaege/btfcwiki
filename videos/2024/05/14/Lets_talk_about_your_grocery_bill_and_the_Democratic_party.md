---
title: Let's talk about your grocery bill and the Democratic party....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=DxtZQxJSEbo) |
| Published | 2024/05/14 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk
about the Federal Trade Commission,
the Democratic Party, and your grocery bill,
because some Democratic lawmakers are trying to lower it.
Okay, so the FTC recently released a report,
and that report said
that some of the major grocery store chains...
Well, it kind of looked like maybe they took advantage
of the pandemic and the supply chain disruptions
to raise prices and increase their profits.
Now, some lawmakers in the Democratic Party,
they introduced legislation about this.
I think Warren has won, but they're stalled, it's stuck in Congress.
So they are reaching out to Biden and they're asking Biden to kind of reel in the grocery
stores.
Now they provided a menu of options, menu of things that Biden can do to maybe help
alleviate the issue and encourage any grocery store that is engaging in, let's just say,
less than competitive activities to stop.
The interesting part about the menu is that, well, it kind of all looks like it already
exists in law.
So it would be executive action, wouldn't have to go through Congress because Congress
already approved it.
It's just underused laws, I guess would be the way to describe it.
And there's a number of things, some of them are long term, but a few of them might actually
reduce your grocery bill pretty quickly.
Now at this point in time, we know that the letter is going out and they're asking for
Biden to do this.
We have no idea what the White House's position is on it yet.
But it's a pretty lengthy list of things that he can do.
There are a few options there, and my guess is the White House has more options as well.
This is something to watch.
Given the concerns about inflation, this is probably a move that Biden will want to get
behind.
Because if he can demonstrate that some of the inflation is really just going straight
into corporate profits, that might alter people's perceptions.
So this is something to watch.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}