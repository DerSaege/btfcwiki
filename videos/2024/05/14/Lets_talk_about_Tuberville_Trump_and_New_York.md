---
title: Let's talk about Tuberville, Trump, and New York....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=fuEBFJ-qj5M) |
| Published | 2024/05/14 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about Trump and his friends showing up in New York
because some of his friends showed up to support him and some things were said.
One, I feel like we'll hear a little bit of news about, and one thing that was
said, I just personally find it interesting.
One of the people who showed up to support him was Senator Tuberville.
And he said that the Republican candidate for president of the United States is going
through mental anguish in a courtroom that's very depressing.
Very depressing.
I'm glad to stand by President Trump, I'm a friend of his.
I'm here more as a friend than backing him as candidate as president."
And then he goes on to go after Cohen a little bit, said that he was incredible.
I believe the term he used was serial liar.
And then he seemed to indicate that he believed that it was political.
said that at the end of the day it was the Democrats and seemed to seem to
indicate that he believed that since the Democratic Party couldn't beat the
former president at the ballot box they were going to try to do it at the jury
box. Now I have no idea whether or not Tuberville believes that himself.
I can't speak to that, he very well might.
And if he does, accepting it at face value, he's upset because his friend is suffering
mental anguish and being stopped from advancement.
Trump should be out on the campaign trail.
And that this is happening because of a political talking point.
Does anybody remember why they recognize Senator Doverville's name?
If you don't, he is the senator that pretty much single-handedly held up the promotions
stop the advancement of hundreds of service members. I don't know if it
caused them mental anguish, but they certainly were not happy about it over a
political talking point. Just irony in this country's dead. So there's that part.
That's the part I find interesting. I don't expect that to become a new story,
But the mental anguish part, I do, because people are going to want to know why he said it that way.
I feel like Trump's campaign, his team, they are not going to like that phrasing.
Not with some of the speculation that's going around.
I would expect that Tuberville comes out with another statement sometime soon
that says something to the effect of, you know, Trump is handling this middle
language, he's going through it, they're really doing everything they can but
he's pulling through like a trooper or something along those lines. I feel like
the campaign is probably going to ask for that because with all of the
speculation going on about, about Trump's erratic behavior and how it might, you
know, how he might be handling things. That's probably not something the
campaign wants coming from, you know, a friend, somebody on their side. So I would
I would expect to hear more about that side. The statement and the roadblocks
that that Tuberville threw up for, you know, members of the military, probably
I don't feel like that's going to become news, but mental anguish is probably not
a term that the Trump campaign wants out there. Anyway, it's just a thought.
Bye, have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}