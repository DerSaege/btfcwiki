---
title: Let's talk about why they're going back up north....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Cmf4e3l4yuk) |
| Published | 2024/05/14 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about
why they're going back north.
Because a whole bunch of questions
have come in about this.
They're all phrased in different ways.
But that's the question, is why is that occurring?
So we'll talk about that.
And then we will talk about a completely unrelated question
that has come in a number of times, and even though they're totally unrelated, they link back
to the same videos, so it would be weird to do them separately. Okay, so, the news.
Israeli forces are having to go back north, into northern Gaza, to go after groups of Hamas fighters,
saying that they're regrouping there. And the question is, why is that happening?
Because these are areas that, you know, were cleared or secured. The reason it's
happening is something we have talked about before, and maybe people
are ready to hear this now. This doesn't work. The overwhelming force trying to do
conventionally. It doesn't work, it never works. Most times when you're
talking about this kind of conflict, the smaller force, their goal is to provoke
the establishment force into an overreaction. That's what they want
because it helps their side. I know looking at that scene, that's hard to imagine, but the video
that will be linked down below, there's a few of them, there will be three, ones from the seventh,
the eighth, and the ninth. The one on the ninth gets into this in detail, but when that overwhelming
force occurs, it helps force generate. They get recruitment. Sympathizer becomes active.
And when you watch that and you hear bystander become sympathizer, the next question that's
going to come in is going to be, is that what happened at the campuses? Yeah. This doesn't
work. This is, yeah, this is something I know a whole lot about. I don't know more
than the current subject matter experts that advise Netanyahu and I cannot say
for 100% certainty that they told him this but I'm sure they did. I'm sure at
least one of them did. You know, I don't know that, but I'm sure it occurred. I do
know that the American advisors did. I'm sure that at least one person advising
that in Yahoo was like, this isn't gonna work. We just watched the Americans try
this for 20 years. And I think the RAND study on it said you needed to be ready
to commit to six or seven years if you were going to pursue the strategy. And
understand once you start playing whack-a-mole and chasing them around, you
you get locked into it pretty quickly.
So that's why.
This type of response is not successful.
It doesn't have a good track record of success when you are talking about unconventional
forces on the other side.
I do know, now that I think about it, I do know that maybe three months ago this was
brought up and I know that it made the papers that Israeli advisers were saying this.
So with this in mind, it didn't work up north, it's not going to work in Rafah either.
This is why people talk about realignment and shaping, because that has a much better
track record of success, getting them to the table that way.
Now the other question, in a recent video I said that early on I decided I was just
covering the foreign policy stuff and I wasn't going to provide a lot of other commentary.
whole bunch of people asked why. The simple answer is y'all told me to. I mean
not directly, but if you go back and watch those same videos down below, again
7th, 8th, and 9th, they're all built off of questions from y'all. And I think it
It was, I think it was the eighth, I could be wrong about that, I was talking to other
people on YouTube and everybody was talking about the questions that they were getting.
I was the only person getting questions like this, so I figured that meant that I should
probably answer them and seven months later they're still they're still coming
in. That's why. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}