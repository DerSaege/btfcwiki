---
title: Let's talk about Putin's big shakeup and what it means....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=wrZ6nKdu4C8) |
| Published | 2024/05/14 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about some big changes
going on inside Russia, inside Putin's government.
There are, there's a little bit of a shakeup going on
and it's worth going over because what's happening
can give us a whole lot of insight
into what Putin believes is going to occur in the future.
Normally when I'm talking about stuff like this, I don't go through and name everybody, but in this case it's kind of
important.
Okay, so, Shoigu, defense minister, been around a really long time in this field.
A decade or more. He's out. He's gone.
He's going to be leaving. Why is the obvious question.
obvious question. One, the war isn't going the way they wanted. Two, he's incredibly
unpopular with the troops. Three, one of his guys just got hit with some corruption stuff.
It can be any of those, all of those, or a combination thereof. The thing is, he's not
just being tossed aside. He's being moved to be the secretary of their National Security
counsel. We'll come back to this because this is important. There's something
else there. So who is he being replaced by? A guy named Belousov. Who is that?
An economist, a civilian economist is now going to be their head of
defense. What does that tell us and why is that happening? Why is it happening?
Wars cost money. It tells us that they view this as going to be a long war.
That's what they see. They see a long war coming and they want to gear the
economy to do that. There's another piece to it. We talked about how when NATO
started expanding, Russia had lost the war in the wider geopolitical sense.
Russia's already lost. NATO started expanding. At that point, even if
they win the fighting, they've lost the wider war. Overall, right now, at that
international poker game where everybody's cheating, the country's sit at
different tables. The high stakes game, it had the United States, China, and Russia.
China and the U.S. right now, they're just stacking up chips and Russia's
losing them. This guy's being brought in to reverse that course because not just
do they have to deal with the actual conflict that that they've started here.
They have to deal with the wider near-peer contest that they want to stay
in. And if they allow Ukraine to be a drain on their economy, well they won't.
So that's what's going on.
Now back to the National Security Council where Shoigu is going.
The question is, where is Patrushev going?
This guy, him and Putin are tight and Shoigu has taken his position.
Where he goes is going to tell us a lot.
You can view him as, he is Giuliani or Bannon to Trump, only Putin is a lot more loyal to
his people than Trump is.
So there's, that's a big question that is left hanging out there.
And once that gets, once that gets announced, it will tell us a little bit more about what's
going on. But overall this move is about gearing Russia for a long-term war in
Ukraine and them attempting to stay in the near-peer contest. That's really what
this is about. Even though you had the other issues, you know, with him being
unpopular and everything not going the way they wanted it to, it's really about
the bigger picture on this one. So that's that's what we know at the moment. I
would expect more news on this. I would expect more news. And again, where
Patrick Evans up is is gonna be important. Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}