---
title: Let's talk about the latest US offer from Biden....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=syuD4KJekao) |
| Published | 2024/05/11 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about some new developments,
some information that has just come to light.
And we will talk about how it came to light
and why it came to light in the way that it did
and what that probably means for the future.
And this is a topic that we have kind of talked about before,
it's about to become important. Okay, so information has surfaced that the United
States has offered Netanyahu information, high-level intelligence, on exactly
where Hamas leadership is, offering this in exchange for them not going into
Rafa. If you take this and combine this with the rumor mill stuff about small
diameters being offered, it certainly appears, it doesn't appear, the United
States is pushing Netanyahu to engage in a strategy of realignment and shaping,
which is going after the leadership in the leadership alone and degrading
warfighting capability in that way. That offer is now public. It has been quote
leaked. The people who said this were apparently not authorized to speak on
it. I feel fairly certain that this was an authorized leak because there
are a lot of signs that suggest that Netanyahu is right on the cusp of sending forces into
RAFA, into population centers, and this is an attempt to get that to stop.
Separately, the United States has reportedly offered water, food, medicine, tents, and
kind of offered an assistance in developing a delivery system for all of it, and said
that they have to keep the conditions livable, that they can't expose those people who had
to flee to additional hardships of famine and disease.
This has also been leaked.
OK, so this is authorized.
Why is it coming out the way that it is?
Number one, to hopefully dissuade Netanyahu.
Number two, I want you to think back
to what the most likely charges from the ICC
were, not exercising due care around civilians
and interfering with humanitarian aid.
The United States has now, prior to anything happening, has made it known that they have
offered incredibly precise munitions designed to mitigate civilian harm and the intelligence
necessary to use them properly.
If somebody was to avoid using that when it was offered, they may not be exercising due
care around civilians.
And then they have offered basically to take care of all of the humanitarian needs.
It is setting up a situation where if Netanyahu goes into Rafa and, let's just say, isn't
careful. It's a choice. I would be willing to bet almost anything that if Netanyahu said,
well, we don't know that our pilots can do it, the US would secretly offer to do it for
So, in the way that this came out, it certainly appears that the United States is offering
everything that they need to avoid additional issues.
And making it public creates a situation where it's really hard to deny that it was a choice.
The signs that Netanyahu is about to order stuff into the middle of Rafa, meaning population
centers, the signs of that are pretty significant.
You know, there have been stuff that's been reported on, but that could easily be real
or it could be a psychological operation.
But there are other signs beyond that, that he is getting ready to order that.
And this is something that the United States has tried desperately to avoid.
They have now offered everything necessary to avoid even the need to go into Rafa to
complete the objectives that Netanyahu has laid out.
So even with those objectives, the US is like, here, you can do it this way.
And they made it publicly known before a move into Rafa occurred.
Hopefully Netanyahu will get the message.
The relationship between Biden and Netanyahu, regardless of what is said in public, it probably
couldn't be any frostier.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}