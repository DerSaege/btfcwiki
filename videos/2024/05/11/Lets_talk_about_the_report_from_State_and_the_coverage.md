---
title: Let's talk about the report from State and the coverage....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=B3lZbS-M-DM) |
| Published | 2024/05/11 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about the report
coming out from state.
And we're gonna talk about the coverage
that is likely to follow
and why it is likely to be incredibly inconsistent
and why that's a good thing.
I know a whole lot of people want to talk about
the vote that took place at the UN.
We will talk about that maybe tonight or over this weekend.
But this is more important because of how it is likely to be handled in the media.
Okay, so you're probably going to see two kinds of coverage on this.
One is going to say that the United States cannot confirm that Israel used U.S. defense
articles in a manner inconsistent with international humanitarian law.
The U.S. cannot confirm it.
set of coverage is probably going to say that the US assesses that they did. Why is the
coverage going to diverge? Because that's what the report says. Why does it say that
is the more important question. There are going to be people who immediately say it's
phrased like this so they don't have to do anything about it. It's phrased like this
so they're saying that they can't confirm it even though they know they reasonably assess
it. No, if they were trying to just allow weapons to flow freely, they would have
said they can't confirm it, period. End of story. End of report. But that's not
what it says. It says that they can't confirm it, meaning 100%. They're
probably at like 98. But then it goes on to say that because Israel uses so much
U.S. stuff that it's reasonable to assess that they, quote, have been used by Israeli
security forces since October 7 in instances inconsistent with its international humanitarian
law obligations or with best practices for mitigating civilian harm.
So why?
Why does it include both?
They can't confirm it, but it's reasonable to assess, yes, because of the quantity of
US stuff that they use.
But putting it in the report this way, number one, it allows Biden to maintain his leverage.
The reasonable to assess part is what he can lean on to continue his pauses.
The other thing is that it allows a conversation to occur.
It allows Biden to talk to Netanyahu and say something to the effect of, hey, hey, I saw
your guy on TV.
That was cute.
You've got all the stuff you need, huh?
That's good.
I guess we don't need to send you any more.
Yeah.
You've got all the stuff you need to go into RAFA.
That's probably true.
That's probably true.
But I mean, come on, Jack.
Do you have the stuff to go into Rafa and defend against a possible counterattack from
Hezbollah?
You don't, do you?
We know you don't because we send you this stuff.
So why don't you come over here and have some ice cream and let's talk about this because
you know what would be really bad?
What happens if we do confirm it when you go into Rafa?
What happens then?
My hands would be tied.
We don't want that.
You know the best way to avoid that?
Avoid population centers in RAFA.
That would be the best way.
So why don't we talk about how we can achieve that?
That's the move.
How Netanyahu will respond to that?
No clue.
that's... I don't want to say that's why the report was written the way it was, but
given the fact that the report is written that way, that's the move. Anyway,
It's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}