---
title: Let's talk about Trump and Biden doing the same thing....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=cmjnZwG-IaY) |
| Published | 2024/05/11 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump and Biden
doing the same thing,
because there's a whole lot of people
who believe that they did.
So we're gonna go through just a handful of messages
that I've gotten that are just a sample of the number
that I have received along these lines.
Then we're going to go over some facts,
And then I'm going to ask a question now, if you are one of the people who believes
with these initial comments, say, stick around for the question because it's
important, okay, I assume you'll support Biden's impeachment since he did the
same thing Trump did.
Yeah, yeah, yeah.
Biden just caught got caught doing the same thing Trump did.
Biden will be impeached over this or its rules for thee and not for me.
Withholding aid approved by Congress is what Trump was impeached for.
If you don't know, there is now a push to impeach Biden for putting a pause or a delay on the aid that is headed to
Israel.
Withholding aid approved by Congress is what Trump was impeached for.
Was it though? I mean, is that really what it was?
It's been a while, you know, it's been a minute.
So I went back and pulled it up and looked.
Okay, let's see if we can find it.
Article 1, delaying aid approved by Congress.
There it is.
No, no, it says abuse of power.
Okay, let's go to Article 2.
Article 2, delaying, no, it's obstruction of Congress. That's not what he was
impeached for, but maybe it's at least mentioned somewhere. So let's go back up
to Article 1. Using the powers of his high office, President Trump solicited
the interference of a foreign government, Ukraine, in the 2020 United States
presidential election. He did so through a scheme or course of conduct that
included soliciting the government of Ukraine to publicly announce investigations that would
benefit his re-election, harm the election prospects of a political opponent, and influence
the 2020 United States presidential election to his advantage.
President Trump also sought to pressure the government of Ukraine to take these steps
by conditioning, here it is, look, here it is, conditioning official United States government
acts of significant value to Ukraine on its public announcement of the investigations.
President Trump engaged in this scheme or course of conduct for corrupt purposes in
pursuit of personal political benefit."
Wow, it's almost like that's not the same at all.
I mean, those are kind of different because Biden is trying to get like a change to policy,
which is what the executive branch is actually supposed to do when it comes to this, when
it comes to negotiations.
Yeah, they're supposed to leverage those acts in that way for policy, not for personal benefit,
which is what was alleged, about Trump.
That's the difference.
And I mean, that's kind of a big one when you think about it, because every president
ever conditions aid on promises from another country.
That's actually not abnormal.
What has happened is that people truly believe, let's go back to it, that he was
impeached for withholding aid approved by Congress. He wasn't. That wasn't the
issue. That was not the allegation. Why do you believe that? I'll have a link to
this down below, a link to the past, if you want to read the articles yourself.
Why do you believe something that isn't the case?
So much so that you would send messages about it.
Because the sources of information that you have told you that.
That's why you believe it.
The information you consume led you to believe something that wasn't true about something
that is incredibly important.
Most people who consider themselves Republicans, you support the Constitution, and you probably
have a high opinion of the founders. They created a situation where they raised
the threshold when it comes to the votes needed for an impeachment because it was
that important. But the source of information that you use that led you
to believe he was impeached for withholding aid, even in something that
was that important, they failed to provide the context. It seems like
something you might want to consider in the future. Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}