---
title: Let's talk about the UN vote....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=8zOPWVCfS9o) |
| Published | 2024/05/11 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, Internet people, it's Beau again.
So today we are going to talk about the vote up at the UN
and just kind of go over briefly what happened
and then address some of the questions that have come in
because they're all over the place.
They are very wide ranging from, you know,
does this totally undermine the UN to is this a big deal?
this a big deal? So we're going to go through that. If you don't know what
occurred, there was a vote in the General Assembly and really its goal was to
recommend that the Security Council looked favorably on recognizing a
Palestinian state. It's really what it boiled down to. You have some people
saying that this totally undermines the UN. No, it doesn't. That's silly. They're saying
this because really a recommendation for a country to join should come from the Security
Council to the General Assembly. But there's nothing inherently horrendous about the General
assembly making its feeling known prior to that.
That's not a major thing.
It's very similar to that legislation that was trying to force Biden to do something
that he doesn't want to do.
He just doesn't sign it.
It doesn't actually do anything.
If the Security Council doesn't want to look favorably upon doing that, they won't.
It doesn't undermine anything.
It's just kind of pointless.
It doesn't change anything, other than put the idea out
there that 143 countries are in favor of it.
That's what it does.
Is it a big deal?
No, because it still has to go through the Security Council.
And somebody on the Security Council, most likely the
United States, is going to veto it.
But if the US doesn't, another country will.
And one of the questions that
kind of keeps popping up is why.
Somebody, a country on the Security Council will veto
the membership of a Palestinian state
until that state has
established borders,
a recognized system of governance
that has consent of the people,
and security arrangements.
That's going to happen.
And a lot of people think that's a cop-out, but it's not.
This is where it gets complicated.
What are the borders?
Okay, just off the top of your head, okay, let's go with the smallest.
Whatever the Palestinians currently occupy becomes the Palestinian state.
Odds are the Palestinians will not accept that.
We can't ask them because they don't have a government with consent of the people.
The other side, let's say, okay, well fine, what gets called the 67 borders?
That's what gets used.
Israel is not going to accept that.
What happens if those borders become recognized internationally?
war, a wider war that the Palestinians will lose because they don't have
security arrangements, they don't have a military. That's why. The UN cannot force
the recognition of borders. I know a lot of people think that they can and all of
those people are being side-eyed by Ukrainians right now. The UN is not good
at that. It's not really what they're there for. So it did show that a wide
range of countries support, in theory, a Palestinian state. But it's kind of like
an online poll. Do they really support it? And the reason I say this is that
This is a lot like a group of millionaires showing up at a city council meeting to protest
the fact that a homeless shelter is going to be shut down due to a lack of funding.
Doesn't make sense.
They could solve that problem.
They're not without means to address it themselves.
of those 143 countries, how many of them use their own economic or diplomatic
methods to exert pressure on Netanyahu to get him to the table? The answer isn't
zero, just so you know. There are a number of countries that try, but it's not 143.
It's in many ways it's a feel-good thing. So it's not... it doesn't undermine the
UN. It also doesn't really apply the pressure that I think some people may
think it applies. It's important to remember that the UN is not actually a
world government. If you take away one thing from this video, it's that the UN
is not a world government. It is not an institution that is there to affect
change. It's an instrument that realistically the thing that it does
really well is maintain the status quo that existed at the end of World War II.
keep those major powers major powers. That it does incredibly well. It does
other things along the way and some of it is objectively good, but it's not a
world government. It doesn't have the ability to force change the way a lot of
people seem to think that it does. You know, you have on the fringes, you have a
lot of beliefs about the UN and the only thing that I can say about that is that
those people who hold those beliefs have never actually dealt with the UN. It's
not, it isn't capable of doing a lot of the things that people believe it can. It
It can't just create a state because the countries nearby are the ones that really make that
determination because of the military force involved.
If you have a state that does not have security arrangements, it's entirely up to the countries
around it. Until there is an agreement between the Israeli government and a
Palestinian delegation or government that has consent of the Palestinians,
until that agreement exists, the UN really can't do anything. The most it
can do is maybe send in peacekeepers, but you would need other countries to
commit those troops and frankly the UN doesn't have the teeth for that. They're
not really going to be able to force that because they can't respond. So it's
143 countries voting in this way. It does show a slight increase from earlier on
when it comes to support. That much is true, but it also creates a situation
where you're looking at countries that are voting this way and asking, okay so
this is what you want, maybe help get them to the table. Everybody wants to be
a cat until it's time to do cat stuff. The reason a lot of the countries don't,
and again I do want to acknowledge there are some countries that really do try
to put that pressure on Netanyahu to get him to the table who voted that way but
the reason a lot of countries don't try to exert that pressure is because it is
complicated. It's complicated for them too. They end up taking an economic loss
or maybe they upset a different ally, it's all intertwined. So it's not some
major blow to the UN, but it's also not something that is going to be hugely
effective. It's a signal, but that's kind of it. Anyway, it's just a
thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}