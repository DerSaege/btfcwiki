---
title: Let's talk about the new energy rule....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=XrMlpAZR7So) |
| Published | 2024/05/18 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about
a long-awaited rule change
and the Federal Energy Regulatory Commission.
Not an agency, not an organization
that people hear about very often.
But we're going to talk about their new rule
and what is likely to occur because of that.
Okay, so the headlines are going to say something like, you know, F-E-R-C pushes new rule, pushes
out new rule, approves new rule.
When you or I hear the word rule, how long do you think a rule is?
It's a sentence, right?
Maybe a paragraph.
This is 1,300 pages.
Obviously, I have not read it all yet.
I have been talking to people who
are very aware of what's in it.
And the short version is this 1,300 page rule
that they have been working on for about two years
will do a lot to fix aging energy infrastructure,
like the power grid.
It will decrease disruptions, make
the grid more resilient to climate change, transmit green energy faster, make it, I guess
not transmit it faster, make it to where those new sites come online faster, and kind of
set the grid up to engage in a higher volume to help with data centers and AI and all of
this stuff and to accommodate the increased amount of manufacturing that is occurring
domestically.
It's set up to do all of this.
This is a huge win for Biden that's probably not going to get any press whatsoever.
Because his big thing when it comes to 2035 and the reducing carbon, getting it out of
certain industries, all of that stuff, it wasn't going to happen without this.
And they approved the rule change.
So it's a huge win for Biden, odds are even though I'm not aware of anything in it that
would actually negatively impact dirty energy profits, my guess is that the GOP is just
going to come out swinging against this simply because it does support green energy.
My guess is that they're going to lose it over this, but we'll have to wait and see.
hasn't as of yet at time of filming happened. But if you are somebody who
cares about meeting climate goals or reducing emissions this is a big deal
and it's I really don't think this is going to get any press because it's a
1,300 page rule from an agency that nobody really has ever paid a whole lot
of attention to. So if those topics are important to you, you might want to look
into this as more details come out about the 1300 page rule. Anyway, it's just a
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}