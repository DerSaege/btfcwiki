---
title: Let's talk about Rudy's birthday present from Arizona....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=d1IoNeYfXmY) |
| Published | 2024/05/18 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Rudy.
We're gonna talk about Rudy Giuliani
and the developments that occurred last night.
And we will just kind of run through
the overall chain of events
and talk about some of the stuff that was said
in the process, both before and after.
And just kind of fill everybody in.
So if you don't know, as far as the situation goes in Arizona with the allegations and the
indictment out there, the Arizona's Attorney General's office, they hadn't been able to
find him.
And last night, Giuliani, in what appears to be a now deleted tweet, put a photo of
himself appears to be at a party, saying, If Arizona authorities can't find me by tomorrow
morning, one, they must dismiss the indictment, two, they must concede they can't count votes.
It looks to be about an hour later that the Arizona Attorney General retweeted this with,
The final defendant was served moments ago, Rudy Giuliani, nobody is above the law.
It appears they caught him monologuing.
So as I understand the events, a few Arizona Attorney General agents from that office caught
him at his 80th birthday party and served him there. Obviously that is
probably going to be used in a very political manner. You know, they will, I
feel like some outlets may not disclose the fact that he appears to have been
actively trying to avoid them and they will just focus on him being served at a
birthday party. One of the descriptions of this event said that it was, quote,
like it was Normandy. That the AG's office stormed it, quote, like it was Normandy.
I looked for footage and photos. I could not find anything that appeared to show
a naval bombardment, artillery, gliders, anything even approaching Normandy, that might be just
a little bit of hyperbole.
I would also suggest that given the nature of the allegations, given the fact that those
allegations do kind of evoke a lot of authoritarianism.
Maybe it's not a good idea to compare yourself to those who were stormed at Normandy.
That might not go over well.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}