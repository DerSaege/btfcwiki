---
title: Let's talk about a request for advice about grandpa....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=_OwsgzktjLY) |
| Published | 2024/05/18 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there internet people, it's Beau again.
So today, we are going to talk about meeting up with grandpa
and a request for advice.
And we're going to do this because I got a message.
And I'm going to go through it and provide
what little advice I can.
But there's a part in it for y'all, too.
So maybe y'all can provide some that I can't.
OK.
So let's see.
To summarize everything, my granddad
hasn't been in my life until recent years.
There's a lot of history, but it boils down to him being a freak
and making my mom go no contact when
she became pregnant with me.
Now he's shown his face to us after 19 years
and is trying to make amends before he tries
to go through the pearly gates,
but ultimately fails because of how much
of a not nice person he's been in his life.
He decided to come visit my mom and I in person this Sunday,
probably in a public place like a restaurant,
and is apparently trying to get me to vote for Trump.
He's one of those that had his brain fermented
and now thinks Trump is the only thing
will stop America from becoming a not nice place. First in
background, my mom is completely on his side with that side of thinking, but after
a few years she stopped trying to change how I think. Very left-leaning person and
it goes on. I was wondering if you or anyone else has any tips or suggestions
on what I should do in this situation. I'm awful at speaking and I hate
confrontation so this whole situation is it's bothersome. So I don't know enough
to provide any direct advice but there are two little things in here that give
me pause about providing like much at all because of the whole no contact and
meeting in a public place. So just being me, regardless of whatever advice you
take from whomever, just make sure that it's safe to follow that advice. Maybe
I'm reading too much into that, but it was something that kind of jumped out at
me. I don't know enough about him or you specifically, your personalities, but I do
have a series of videos, I think it's titled something like, let's talk about
how to ruin Thanksgiving dinner, or something along those lines. I'll try to
find them and put them down below, that provide advice on how to deal with
relatives that have views that, you know, they won't change their mind about and
can't change the subject. And maybe somebody else has advice as well. If it
is something that is conversational, if it was me, I would probably, based on
which you've said, I would think of one topic that I know a lot about and that I
am certain that is just one of those things that would disqualify Trump and
just stick to that. I won't vote for him for this reason. Here are my reasons why
nothing is going to change that and just leave it at that. Or if you don't want to
go a policy way, again reading into subtext, just be like I would never vote
for somebody who doesn't, who touches people without their consent and leave
it at that. There are a number of things that you could do but I'm not sure
enough about the situation to offer any real hard advice. I would imagine, given the way
people normally are on this channel, there'll be tons below and there may even be an opportunity
for you to talk to them and get a clearer picture. Just, I mean, I would keep in mind
that based on everything in this, it doesn't seem like you have a strong desire to rekindle
any bond.
So I wouldn't put too much stress on it.
I wouldn't let this weigh you down because based on what's there, it's not going to matter
long anyway is what it seems like.
So I wouldn't let this ruin your week.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}