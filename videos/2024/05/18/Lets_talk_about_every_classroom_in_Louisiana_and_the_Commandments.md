---
title: Let's talk about every classroom in Louisiana and the Commandments....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=lunMO6ZvSvg) |
| Published | 2024/05/18 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we're going to talk about Louisiana.
We're going to talk about Louisiana
and a piece of legislation that appears
to be making its way through.
And we'll talk about what the Supreme Court has said
in the past about similar legislation,
the discussion around this particular piece of legislation,
and where things may end up.
Okay, so what's going on?
It looks like Louisiana is,
it might enact a piece of legislation
that would require the 10 commandments
to be posted in every classroom, every classroom.
Now, I know if you're a little bit older,
you're like, hey, I've already seen this one.
Yeah, this has already been decided
the Supreme Court. However, it does appear that some people believe that the Trump-influenced
Supreme Court may be a little bit more open to theocracy.
So what are the arguments at play? The first is that, well, it's really there for multiple
reasons, not just the religious one. So it doesn't run afoul of that pesky little constitution
that's always getting in the way of things. The other is that it isn't
divisive. It's not divisive, and this is something that that was asked of a state
senator there, I believe, who said, I don't feel like thou shall not murder is
divisive. I would say that everybody here would agree with that. I mean, yeah, yeah,
but why'd you have to go so far down the list? What about that first one? The one
that explicitly says which God comes first? I feel like some people might view
that as divisive. It might actually have to do with the exercise of a certain
religion. That seems to, again, that pesky little Constitution getting in the
way. The other thing is that it appears they're saying that all laws are based
on it, or US laws are based on it. Yeah, Hammurabi and Ernamu have some
questions about that. I feel like maybe they don't understand how BC dates work.
So, what's going to happen?
If it gets signed, it's going to the Supreme Court.
Let's be clear about that.
It will definitely be appealed there.
Now, realistically, this is pretty cut and dry, and the answers should be either, no,
you can't do it, or fine, you can do it, but you have to provide every other religion
with equal space.
Those would be the standard answers for this from a normal Supreme Court.
We don't actually have one of those.
I feel like this would still be struck down, but I can't say that for sure.
I cannot say that for sure.
So there are going to be just a ton of legal challenges over this if it moves forward.
A lot.
I would imagine that they are already, they're already lining up just to be ready for it.
The state of Louisiana is going to have to pay a massive amount of money.
They're going to spend tons and tons of money to support a piece of legislation that more
than likely will be deemed unconstitutional, as it kind of has in the past, and will probably
fail at the Supreme Court.
And if it doesn't, realistically, it's probably going to be one of those things that is divisive
in the classroom and probably has the exact opposite effect that lawmakers want it to
have on the youth of Louisiana. They're probably not going to be fond of something that is
being viewed as a state religion. It will probably actually push more students away.
Then it will help convert, I guess. So that's what's going on. Yeah, we're still doing
this in 2024. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}