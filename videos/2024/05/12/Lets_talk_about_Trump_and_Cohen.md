---
title: Let's talk about Trump and Cohen....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=fuEG3HuIPcE) |
| Published | 2024/05/12 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump
and his New York entanglement.
So we'll talk about Trump and we will talk about Cohen.
And what it means that Cohen may be showing up soon
and something that has occurred throughout this process
related to Cohen, that's probably going to become important.
Okay, so reporting suggests
that Cohen will be taking the stand
next week, maybe even Monday.
That means that the prosecution
has reached the tell them what you told them phase.
Tell them what you're going to tell them, tell them, tell them what you told them.
They're at the end of that.
They are at the point where they are retelling the story
and giving the jury that context and putting it
into story fashion with a witness that can describe
a whole lot of things.
So that means the prosecution feels
that they have made their case pretty quickly.
Pretty quickly.
They feel that they have put on the evidence that they need.
While they were doing that, one of the things that has occurred is Cohen has just been trashed
constantly by the prosecution's own witnesses.
They have painted him as selfish and self-serving.
serving. That's, I know this sounds weird, but that is probably going to make him
more credible because it's going to be hard for Trump's team to say that he did
all of this out of the goodness of his heart because he was super loyal to
Trump because according to the testimony, he was always out for himself.
He always wanted credit, which means Trump would have been told about his activities.
I don't think that was an accident that he was painted that way throughout the entire
thing, like any time he got brought up.
of a way of preempting what Trump's team may try to say.
So in theory, the prosecution could rest this week, and it's a short week as well.
So after that, we'll hear from the defense.
And this is moving along a lot faster than most people anticipated.
And we'll eventually end up waiting to hear what the jury decides.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}