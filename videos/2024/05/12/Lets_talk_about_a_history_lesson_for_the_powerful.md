---
title: Let's talk about a history lesson for the powerful....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=xT0mWXjOMfU) |
| Published | 2024/05/12 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about history and lessons that
can be learned from history.
You know, seven months ago, all this started.
I decided to cover it in a very specific way.
I was going to cover the foreign policy stuff and limit
my commentary to that.
And I have stuck to that.
An incredibly prominent person came out and said some things about those people who are
upset by what's going on.
And since I have been dealing with it every day for more than half a year, I have some
thoughts.
So you're going to have to bear with me on this.
I will go back to covering things the way I have been, but I really feel like I need
to say this.
She came out and she said that, you know, those young people, those college students,
they don't really know the history of the region.
They don't know much history at all.
And went on.
subtext was that they don't get to have an opinion or that their opinion
shouldn't matter. My inbox is a testament to the fact that that
observation, not the conclusion, but the observation... I mean, yeah, it's true. It's
true. There are a whole lot of well-meaning people out there who, they
don't know the history, they don't know how foreign policy works, they're talking
about rivers and seas and if their life depended on it they could not tell you
which river or sea. Fact, fact, there's a whole lot of people like that out there.
but they don't have to know that. They're not, they don't have hard policy
positions. The overwhelming majority of people who are upset by this, it's not
because of some historical issue, it's not because of a view of a policy
position that they're really attached to, they just want people to stop dying.
You don't actually have to know the history for that.
You don't need to understand how foreign policy works to hold that view.
The thing is that there's probably a history lesson that those in power have forgot.
Do you think in the 60s, the people out in the streets, do you think they really knew
the history of Vietnam?
Do you think they knew the history of the region?
They didn't.
Not most of them.
What happened between World War II, Korea, and Vietnam?
What occurred?
What changed?
Why did you have such a movement during Vietnam that did not exist before?
Because things changed.
The images were beamed into people's living rooms.
It wasn't sanitized on some newsreel with that guy with the weird voice.
Oh, here's Jimmy.
He's from Brooklyn.
He's going to give the Reds what for?
Go get him, Jimmy.
No.
get loaded on a Huey. And it changed the way people viewed things. It changed what
they were willing to accept because they got the imagery. Everybody with a
smartphone today has the production capability of a TV station in the 1960s
the means to disseminate it. The history lesson for those in power is that things
have changed again. The world is less accepting of civilian loss. This is
something that every person in power around the world and every military
needs to accept and understand. Because it's no longer just numbers. It's not a
graph. They see it. They suffer a moral injury and they want it to stop.
That's it. I have hundreds of videos explaining exactly how complicated it is.
The biggest pushback I get is that it needs to be simple because that's all they care about.
They don't care. The overwhelming majority of people who have an issue right now,
Now, they do not care about the policy considerations at all.
They just want people to stop dying.
It is sad that large portions of the world have gotten to the point where they're saying,
hey, we need to have less civilian loss and that those people who are leading the world
that as a bad thing anyway it's just a thought y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}