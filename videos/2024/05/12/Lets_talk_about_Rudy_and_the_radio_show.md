---
title: Let's talk about Rudy and the radio show....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=8-zvdwwLKT4) |
| Published | 2024/05/12 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Rudy.
We're gonna talk about Rudy Giuliani,
New York, a radio show,
and diverging opinions on what happened
because there have been some conflicting statements.
statements if you don't know Giuliani has had a show a radio show two maybe
three years on WABC now according to reporting Giuliani has been suspended
and the show has been canceled. Why? Take a guess. Just take a guess. Yeah. It
appears that Giuliani was continuing to echo claims about the election. Now the
station's owner has indicated that Giuliani was warned about this and did
it again. Giuliani has kind of said that there wasn't a warning or that he didn't
know ahead of time. It was a new policy. It was a free speech violation. He even
went on a live stream to say that you know he would have to be a fool to
continue to do it because he talked about the election in almost every show.
So, there is a divergence when it comes to the chain of events.
You have two competing claims.
The station's owner really seemed to indicate that he was warned and basically was like,
yeah, I'm going to do what I want.
And the quote is, quote, he did it to himself.
So this is going to factor into a whole bunch of other stories.
Because of Giuliani's suits and all of the stuff involving that, the finances behind
the radio show are going to end up being discussed in other developments.
this was a show that Giuliani was paid for, which now he's been suspended, and the show
reportedly canceled.
We don't know if that income is still coming in.
So yeah, there's that.
Giuliani also kind of suggested that perhaps the station owner was pressured by Biden and
company that is not happy about the things that Giuliani has been
saying. So that's what's going on with him. Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}