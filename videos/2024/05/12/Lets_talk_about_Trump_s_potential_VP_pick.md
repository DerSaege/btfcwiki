---
title: Let's talk about Trump's potential VP pick....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=CYYA2RwhMx8) |
| Published | 2024/05/12 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about Trump
and his potential vice president picks
because we're at the point now
he's gonna have to make a decision soon
and we're starting to get some insight
into who he is looking at
and the type of candidate he wants.
So we'll talk about two,
One that is interesting and one that seems to be kind of leading at the moment.
The interesting one is Nikki Haley.
It appears that Trump's team was looking into Nikki Haley for about a day.
There was interest for about a day.
The reporting came out saying that they were looking at her and then a day later Trump's
team said that she wasn't being considered.
So as far as Trump pulling in Haley, that would have been a smart move.
She's still pulling 20% of the vote even though she suspended her campaign a while ago.
It is clear that there are a lot of Republicans who would prefer Haley.
In hindsight it probably would have been smarter for the Republican Party to nominate her.
So he could bring her on and hopefully shore up his numbers within the Republican Party.
At the same time it would have made him look a little silly because he said a lot of less
than flattering things about her.
So that's something he could get over because most of his base wouldn't care, but he had
some stuff to gain if Haley came on.
Her not so much, not really.
I know people are going to say that if she doesn't support him, then she'll pay later
in politics for it, I mean maybe, but we don't know what her goals are.
And if the, if MAGA does not win this election, that's probably it.
They're probably gone.
And if she ties herself to them, well, what matters later doesn't really matter because
she will be the losing candidate with a brand of the Republican party that is fading.
And their policy positions differ.
Their way of doing things differs.
If she steers clear of him, she has a chance at other offices later.
And I would say she has that chance even if she does upset the MAGA base.
So while it might have helped Trump, there's not really much in it for her except for risk.
Now the person that seems to be leading is Doug Burgum, governor of North Dakota.
Now, kind of a political novice, wasn't really in politics, and I want to say in 2016 decided
to run for governor.
Didn't get the endorsement from the Republican Party, but wound up winning the primary.
And that's it.
That's the political career.
I would imagine that if Trump brings him in, they will frame it as, you know, we're business
people.
And unlike Trump, my understanding is that Burgum actually was pretty successful.
So that would probably be the framing of the campaign.
It is probably also beneficial for Trump to have somebody who is not really used to Washington
politics.
Somebody that might be more easily swayed because Trump is Trump.
So it appears right now that the governor of North Dakota is kind of leading the pack.
There are other people that have been mentioned, but it seems as though maybe they weren't
really performing as well during their auditions, for lack of a better word.
So right now it really does look like it's going to be the governor of North Dakota,
but that could change if he deviates from Trump too much or doesn't handle questioning
well or something like that.
But Trump is going to have to make a decision soon and it's probably going to be pretty
consequential.
also not just as far as Republican voters and independents, but it will probably also
give us some insight into how he plans on running the campaign.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}