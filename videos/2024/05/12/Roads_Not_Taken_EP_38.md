---
title: Roads Not Taken EP 38
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=50tLFy3XrSE) |
| Published | 2024/05/12 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Bo again,
and welcome to The Roads with Bo.
Today is May 12th, 2024, and this is episode 38
of The Roads Not Taken, which is a weekly series
where we go through the previous week's events
and talk about news that was unreported, underreported,
didn't get the coverage I thought it deserved,
or I just found it interesting.
So, on top of that, happy Mother's Day,
And we will start off with two items. One is a PSA. And basically over the last
week a whole lot of people probably could have benefited from a NOAA radio.
N-O-A-A radio. If you live in an area with inclement weather, probably worth
having one. The other thing is that somebody said that they were
going to send me something and by the photo I couldn't actually tell how big
it was going to be but yeah so that's me as a as a puppet I have no idea what
I'm gonna use this for yet but expect for this to make an appearance a few
times thank you by the way okay so moving on to foreign policy the US
U.S. announced a new $400 million package for Ukraine.
There is some reporting that says Ukraine might have used a U.S.-made system to hit
a target inside Russia.
It looks like Russia launched a surprise offensive near Kharkiv.
Moody's has left Israel's credit outlook unchanged, so it is still negative.
A US soldier stationed in Korea has been arrested in Russia after traveling there to reportedly
meet his girlfriend.
His wife said he was having an affair and indicated that he was not engaged in espionage.
There are obviously a lot of developments when it comes to what is going on with Israel.
We're going to have to take those as they come because by the time this goes out it
could all change.
So that's why there's not a lot on that.
But it does appear that leaflets have been dropped suggesting that there may still be
a move into a very populated portion of RAFA.
Okay, so U.S. news.
There are suggestions that Democratic advisors are considering holding part of the convention
online to cut down on the risk of it being disrupted by protests.
Barron Trump was slated to be a delegate at the RNC where he would have, you know, voted
for his dad.
But a statement from his mother's office indicated that he had declined that opportunity.
There's a lot of speculation about what happened there.
Not something I'm going to delve into.
Okay, there are still demonstrations.
They're continuing throughout commencement ceremonies
at universities across the country.
Steve Bannon had his appeal rejected,
and it appears that Rudy might be taking over his show
or at least going to work for him for some period,
something like that, because Rudy was reportedly suspended
and had his show canceled over at WABC.
Let's see.
Governor Noem is facing a petition to resign.
It's gathered more than 20,000 signatures.
A Utah activist flooded a tip line
for one of those bathroom bills.
the state auditor is furious. Basically said that he, that they didn't come in to be a bathroom
monitor. Senator Menendez is slated to go to trial on Monday. This is the, this is the one with the
gold bars? That one. Virginia Commonwealth University graduates walked out during Governor
Youngkin's speech. In cultural news, TikTok has filed their suit in court, as expected.
Most analysts are saying that they have an incredibly strong case, which, I mean,
they probably do. Miss USA and Miss Teen USA have both resigned their positions.
There's a lot of speculation about that as well. A Virginia school board voted to
restore Confederate names to two schools. They were taken off and a few
years later they have now decided to put them back on. A descendant of Stonewall
Jackson who one of the schools is named after said I respect their right to do
what is morally wrong and is definitely not in favor of having the school
renamed after their ancestor. In something that just struck me as a
little odd, USA Today ran a headline, Trump and Republicans are lying to you
about non-citizens of voting. That is an incredibly direct headline, particularly
from the USA today.
In science news, a study is suggesting that the biggest driver of new infectious diseases
is biodiversity loss, followed by climate change, and then followed by the introduction
of non-native species.
In related news, the World Health Organization missed a deadline for a pandemic treaty, but
talks will reportedly continue to go on. In oddities, a woman was found living inside
a Michigan grocery store's rooftop sign. That might be a sign in and of itself that something
really needs to be done about housing.
OK, so this is something I think it's just worth mentioning.
I am not somebody who tries to make mental health diagnosis.
It's not my field.
But Trump's behavior in speech is becoming more and more
and more erratic.
I actually have a video coming out about that really weird video he put on social
media about RFK Jr.
I'm not even sure I'm going to release it now because Trump went on a rant about
Hannibal Lecter.
It's just getting more and more bizarre.
And I think it's probably something that may continue to get more erratic.
Okay, moving on to the question and answer section.
Why can't they just get rid of the veto at the Security Council?
Because the UN isn't...
The veto is necessary for the UN to function as it is supposed to. Keep in mind what people think
it does and what it's supposed to do aren't the same thing. The other reason is general assembly
votes aren't actually binding. So there wouldn't be a... that it would remove the ability to even
borrow dentures. You know, when I say the UN doesn't have teeth, nothing would be binding.
And if you made General Assembly votes binding, you could easily create a situation where,
I don't know, let's say 120 countries decide that the United States should invade Canada.
If you actually try to turn it into a world government, you run into really big issues
real quick.
Again, it's not a world government, it's an international body.
I was kind of venting about this because a lot of people do view it as something that
It has a lot of power.
One of the longest running analogies on this channel is that foreign policy, well it's
an international poker game and everybody's cheating.
The casino that's hosting the tournament probably isn't on the up and up either.
Just bear that in mind.
You say you don't script.
Do you think your videos might be better if you did?
Not saying they're bad, just wondering if extra organization would make them better.
No, I sound incredibly robotic.
It does not work for me.
When history looks back at Biden's foreign policy, how do you think he will be judged?
That entirely depends on whether or not what he's doing right now works.
If he can get them on the path to a Palestinian state, to a two-state solution, his foreign
policy will go down as one of the best in history.
If he can't, the nickname will stick.
That's again, foreign policy isn't fair.
It's not fair for politicians either.
Overall when you look at all of the different foreign policy things that he's had to deal
with, that his administration has had to deal with, overall it's been really good.
This is an incredibly big deal that will overshadow everything else.
So if this doesn't turn out well, it's going to, when history looks back at it, this is
going to be the one that matters.
So it depends on how this works out.
Note, you might want to explain the events from the UN resolution creating Israel to
them being admitted.
You have a lot of questions about this.
So this came up a lot when I said that the UN can't just create a state.
I wish I had known this was going to be here.
So resolution 181 was passed by the General Assembly I think in 1947.
It's worth noting that a whole lot of people also view that as something that provides
a legal basis for Palestinian statehood as well, a basis.
But that didn't actually create Israel the state.
That's not what happened.
So the resolution was put into place and then a war happened, two really.
If you want to be technical, there was the one that ended on May 14, 1948, and that is
when Israel declared independence.
It immediately turned into another war that ran until early 1949 and then in May of 1949
they were admitted to the UN.
They were admitted to the UN once they had a government, they had taken territory, so
They had borders, they had a government, and had the means to defend it.
They had a security arrangement.
The resolution did not admit them to the UN, and I think that's where a lot of the confusion
happens.
The thing is, this period is incredibly important.
Recently Hillary Clinton talked about how a lot of people don't know the history.
And I'm going to be honest, that may be something I'm not going to be able to bite my tongue
about because, you know, I said I was going to cover the foreign policy stuff and stick
to that and not engage in other commentary way back when this started.
What she said really bothered me in the way she said it.
But regardless of your view, that period in the 40s shaped a whole lot of stuff.
The brief summary from memory is not enough.
If you want to have a good base, you have to know that part.
Even though I do not believe what Clinton seemed to be indicating there.
Okay, so please, yes, okay.
So the lighthearted question to end on, what do you think of RFK's brain worm?
I don't know, I never met him.
The, yeah, so if you missed this, some information came out about a health issue that R.F.K.
Jr. had, and reportedly he had a worm in his brain, this isn't a joke, and it died.
What do I think about it?
I mean, I do not think that this is really a...
I don't think this is a campaign issue.
This is something, if I remember, this was like a decade ago.
It happened a while back.
I would think that if there were going to be major issues from it, they are apparent.
I don't, I mean, I get it.
It's a very bizarre thing to come out about a presidential candidate.
I don't think it's going to alter anybody's opinion though.
It's going to just be...
It's going to be joke material is what it's going to turn into.
So yeah.
I would definitely, going back to the actual creation, I would go back, one, check my dates.
Two, I would, it's a period that you have to know about.
And it is not, it's probably not material you can get from a meme.
You're going to have to do some reading on that one.
So anyway, okay, so there you go, a little more information, a little more context and
having the right information will make all the difference.
I'll have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}