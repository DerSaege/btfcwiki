---
title: Let's talk about Trump's video issue....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=JMylRae9_fE) |
| Published | 2024/05/22 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we will talk about Trump and Tones
and Trump's video on social media.
I guess it's not his video.
It's a video that was on his social media account.
We'll get into all that.
And we'll just kind of go through the appearance,
and we'll talk about what the campaign said,
and what to me at this point is the obvious read
on this entire situation.
If you don't know what occurred,
while Trump was reportedly on lunch break from his trial,
a video showed up on his social media
posted by we don't know who, I guess.
The video itself was a video that was depicting headlines from the future.
If he won, if he won the election, these would be the headlines.
And one of them included the phrase, Unified Reich.
Like everything's going great because of this.
Obviously, due to the fashy tones there,
there was a little bit of pushback on this video.
So much so that it was actually deleted,
which is kind of rare for Trump to back down on something.
Now, the campaign is saying,
this was not a campaign video.
It was created by a random account online
and reposted by a staffer who clearly did not see the word.
while the president was in court.
Fine, whatever.
Let's just say that's true.
Okay, there's no point in arguing about that.
Let's say that is exactly what occurred.
It reminds me a whole lot of the situation in Missouri.
And if you don't know about that,
a person who wants to run for governor wants to run as a Republican.
Other Republicans are a little uncomfortable with this, try to keep them
off the ballot.
off the primary ballot, mainly because the guy who wants to run has a history with, well,
the Klan.
This reminds me a whole lot of that.
Fine.
Some random person who created a video that has some flashy tones to it, they did it on
their own. I'm not saying that Trump's a fascist. I'm saying that it certainly
appears like people who make videos with flashy tones think he's a fascist. Maybe
that should be enough. You know, maybe that should be the sign in and of itself.
You know, if the person with the the history with the Klan wants to run in
your party, there might be a reason. If people continually create media with
fashy tones about you it's probably because people who enjoy you know fashy
stuff think you're a fascist. I mean that seems to go without saying. I'm not
sure that this answer really... I don't know that it really responds to the
question. I mean, I get it. Fine, the campaign didn't commission something using
the word right. Alright, but there's enough of it out there that it's showing
up from the official social media accounts. I feel like that should give
people pause.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}