---
title: Let's talk about Georgia, Willis, and McAfee....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=oMfs2ym99nY) |
| Published | 2024/05/22 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about Georgia
and elections and signs and what people are paying attention
to and what I think they should be paying attention to.
OK, so there were a number of elections today in Georgia.
And a whole lot of people were focused
one in particular. They were looking at the Democratic primary for Willis, for
the DA that is overseeing the Trump case in Georgia. They figure that you can look
at that and get a good feel as to how people feel about the case. It's a good
indication of it, at least those locally. And that makes sense on some level and
And if you're wondering what the results are, with 94% of the votes in, she has 87.1%.
So it does appear that people in the area approve of her handling of the case.
I mean, that's an easy read.
The thing is though, that's a Democratic primary.
I mean, it stands to reason that most people that are going to vote in the Democratic primary
would support her. I don't know that that's a really good indication of widespread support.
I think that's the wrong race to look at. I think the more important one is the one
for the judge because McAfee, also running, with 93% of the votes in, 82.9%.
Seems like people approve of it.
That's a nonpartisan race.
So why does any of this matter?
The theory goes this way.
These races, they are made up of the jury pool.
If this is the way the potential jury pool fills, that's probably how the jury fills
as well.
So that's the indication that people are looking at.
I don't know that that's necessarily a good indication itself.
I don't know that the theory is sound because I feel like attorneys picking the jury might
have a little bit of influence over how effective this theory is.
But that's why people are paying close attention to it.
For those who want to use it as some kind of metric to determine support within the
jury pool. If they wanted to see support, they got what they were looking for,
regardless of the race you're looking at. It's important to remember that a
number of right-wing outlets put a whole lot of effort into casting McAfee as
bad because he let Willis stay on the case. It doesn't appear that it did much
good. So it's a snapshot. It's a quick look at what's going on. This is an area that would
be supportive of the Democratic Party anyway, but that nonpartisan result is the one that
matters. That's the one that could give you any indication if that theory is sound. Anyway,
It's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}