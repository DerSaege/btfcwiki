---
title: Let's talk about Trump's NY schedule....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=w2rAew6G0VI) |
| Published | 2024/05/22 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump,
New York, schedules, what we can expect next,
and just kind of go over what appears to be
the coming events, because Trump's New York entanglement
entanglement has been moving along at a pretty quick pace. So we'll go over what's
happened. The short version here is that the defense has rested. Now normally what
you would expect at this point is for closing arguments to occur. They get the
instructions from the judge and they go deliberate and all of that is going to
happen. It looks like next Tuesday. There's going to be a delay. But with
that in mind, it is likely that we will have a verdict on this entanglement by
next week. The jury, after hearing the instructions, they will go deliberate on
the 34 counts. Now, I know people are going to want to know. I don't have a clue. I have
no idea what's going to happen. I have read the analysis and the inside views on all of
this stuff, and I've read about how the prosecution made points here and the defense made points
there and one thing I know is that all of that is that analysis is provided by
legal experts and their view of it. Not everybody on the jury is a legal expert.
They may have a different view. I would imagine that the jury understands the
the weight of what they're being asked to decide and understands that it isn't a normal
case.
But whether that means they feel that the prosecution should prove their case even more
of the situation or maybe they feel the other way. Don't know. Don't know. I don't
think anybody does. I have listened to so many experts talk about this and most of
them believe what they believed when they when they first started talking
about it their opinions have not changed based on any new information it's still
they still believe if at the beginning of it they believed Trump was going to
be convicted they still believe that now I'm not aware of anybody that has
changed their opinion that to me is that's not a sign that I want to put a
lot of stock in the analysis because I think people are very wrapped up in it
and it makes sense. I mean it's a huge development. It's a
historic event. Trump and his team of course are saying they won. I don't know
that to be true based on what I have read and what I have heard but I mean
they did make some points. Whether or not that resonates with the jury, that's
anybody's guess. So it appears there will be about a week-long break and then
we'll all be on Verdict Watch, I guess. Anyway, it's just a thought. Y'all have a
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}