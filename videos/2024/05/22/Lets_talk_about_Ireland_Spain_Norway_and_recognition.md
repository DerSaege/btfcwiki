---
title: Let's talk about Ireland, Spain, Norway, and recognition....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=-OD8EvyXXj0) |
| Published | 2024/05/22 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about Spain and Norway
and Ireland and recognition and responses.
And we are going to talk about what it means
not just on the surface level, but on the deeper level
when it comes to the diplomatic side.
We'll talk about how things might be influenced
because of this, okay.
So if you missed the news, because it occurred while most of y'all were asleep,
Spain, Norway, and Ireland recognized the state of Palestine.
I believe it becomes official on May 28th.
The obvious first question is, well, does this mean that Palestine's a state?
No, no.
Recognition alone does not create a state.
In fact, most countries in the world already recognize the state of Palestine.
However, while most countries recognize that state, most Western countries do not.
That's where this becomes kind of a big deal as far as signaling.
Is this going to change the life of the average Palestinian, which I would like to remind
everybody, that should kind of be the goal in this conversation. Not right away, no.
It's a morale boost. It is a morale boost. Now, what does it do? If it doesn't really
create a state, if it's really just kind of adding on to a whole bunch of other countries
who already believe that way, who have already taken those steps, and it's not going to
affect the life of the average Palestinian beyond a morale boost, what does it actually
do, if anything?
It gets into some deeper diplomacy stuff, but there are two big signals being sent.
The first is to the US, and it basically says, you're not actually in control of this.
And it means it, not just in the way that I've been saying on this channel, where the
US can't just make things happen, it means it that way.
But it also is saying it in the sense of, you don't get to decide which route the rest
of the world takes on this matter.
Ireland and Norway in particular are definitely sending this message.
Ireland has again the long history
and then Norway
think Oslo.
That message is definitely being sent.
So what other messages being sent?
The other big development
is that for a long time
there has been a concern
the Israeli government that, well, if bricks start falling out of the wall, the wall may fall.
This might be something that sways even Netanyahu's government, because these countries
kind of expected, Ireland and Norway in particular, they would definitely be the
next countries to do this. But what if it doesn't stop here? That's going to be the
concern inside Netanyahu's government. So those are the signals being sent and they're
intentional. One, it's telling the U.S. you don't own this situation, other countries
might decide to get involved on their own and lead follower get out of the way.
You know, you've gotten it this far with the mega deal but apparently you can't close it.
Maybe hand it off to somebody else to get it across the finish line type of thing.
And then it is sending the message to the Israeli government, maybe we're just the first
countries to do this, because there are other countries that have more geopolitical
sway that have been open to the idea. If you're looking at a map, look at France
and Australia. So that's the big news. This in and of itself doesn't do
anything. It's that deeper diplomacy stuff. Now what was the response? The
Foreign Minister of Israel said, Ireland and Norway intend to send a message to
today to the Palestinians and the whole world. Terrorism pays. Let me tell you
what probably was not good terminology. That's not going to sway the Irish, I
don't think. I think the only thing that might prompt is for a certain song about
dreamers to be put on loop. There are a number of countries that would not
respond well to that particular line of rhetoric and Ireland is definitely one
of them. But, I don't know, maybe that was another year. So, what else is going on?
There will be a video coming out later today that I recorded last night that talks about
Lindsey Graham saying stuff when he shouldn't. There might be more to this because he said
something and in that video I attribute it to the Megadil being pushed but they
may have known about this and there may be more to come because he said kind of
in kind of a cryptic way he said you know from what I understand the ball is
about to be in Israel's court and Blinken was like yeah that's my
assessment. This might have been that, but there might be more, or it could be
about the mega deal. So that's what's going on. Again, it's a morale boost, but
this doesn't create a functioning state of Palestine. They have to have a
government security arrangements and borders that are defined before that
actually happens. The long-standing thought process on this is that those
borders would eventually be defined via a peace process including Israel and
directed by the United States. This, if it is the first bricks to fall on the wall,
if enough bricks fall, all that changes and all the commentary that foreign
policy people have made over the last seven months, it all kind of becomes
obsolete because a new card is played. I don't anticipate that and I don't think
that most do, but honestly the only country I saw doing this was Ireland.
That's why they're the only one that got a video about it in the past. The
addition of Norway and Spain does make it a little bit more significant. Ireland
was kind of... Ireland was anticipated. Norway could be expected. Spain, it makes it a little
bit more real as far as the bricks falling. Anyway, it's just a thought. Y'all have a
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}