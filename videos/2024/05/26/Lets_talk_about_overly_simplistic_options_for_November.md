---
title: Let's talk about overly simplistic options for November....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=-vHn-I3ddEA) |
| Published | 2024/05/26 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we're going to talk about, I guess, something
the person views as overly simplistic, an overly
simplistic explanation of the choices come November.
And they may view it that way, but it may not be.
Okay, so here's the message.
So I've got a genuine question that's going to sound like it's picking a fight, but it's
not.
How exactly is anyone supposed to protest Biden at this point?
Because at the end of the day, as cynical as it sounds, if you're not voting to retain
him, you might as well be voting to put Trump back in.
I'll be darned if I'm in favor of that.
But past the primaries and the uncommitted movement, which was always going to be a thing
to register disapproval, not change the outcome, what exactly is there at this point?
I really wish I saw more options and more nuance, because that seems overly simplistic
to the point of stupidity, but I look at the options come November, and unless I'm reading
things wrong, that's how it's going to play out.
I mean there are things that can change that, but your most likely outcome is the overly
simplistic one.
Those are going to be the choices.
Those are going to be the options.
And people have to make their choice or not.
Those are the two options there.
And if they're going to make a choice, they have to make it between Biden, Trump, or some
kind of protest vote.
Because it is, in fact, a two-party system.
When people talk about this, and they talk about, you know, people who would generally
align most with Biden. And they're saying they're not going to support him this time. The idea is
to punish Biden. It's not going to punish Biden. Biden is going to retire and go home if he loses.
He will not be punished. The people who will be punished will be those people impacted by Trump's
policies. That's who gets punished, not Biden. And even with the reason that most people are
considering some form of protest vote, or choosing not to choose, the reason is Gaza.
And if Trump wins, the best case scenario is that he's just a little bit worse than
Biden because of things he did in his first term, objectively, he is worse than Biden
on this issue. Go look at the other channel, we talked about the Pompeo Doctrine, that's
just one of a few examples. So, the best case scenario is that it's just a little bit worse
for the people they want to help. Maybe it doesn't really change that much on the ground.
The worst case scenario if this is still active and it's still occurring is that Trump leans
into his rhetoric and leans into the worst inclinations of those people around him.
You know, you have those people in his circle saying that the answer is to nuke Gaza.
That's not going to happen.
But there are other things that could make it worse, and since I've said that, somebody's
going to ask.
He could say, hey, you know, your whole problem is that this is taking too long.
Here's a handful of GBU-43s, why don't you just solve this quickly?
And what?
population of Gaza has gone in minutes. And you're saying, well they wouldn't
really do that. Do you believe the rhetoric or not? And they have the
perfect pretext to do it. When did the US use it? To go after tunnel complexes. The
The worst case scenario is far, far worse.
The best case scenario is no change.
So I don't know that it is overly simplistic to the point of stupidity.
That kind of is the situation.
Not saying it's good, I'm saying that's what it is.
But the question here is, what exactly is there at this point?
What you are dealing with is the product of a two-party system, which is what exists.
Your options are, if you want to change it, you either build a new party from the ground
up, starting in the House of Representatives, eventually moving to the Senate, and the effort
and time gets put in to create a new national party.
Understand this is a long process.
If this had started the last time this issue came up back in 2016, still wouldn't be ready
to run a president by now.
It would be very surprising.
And then the other option is to run progressive candidates within the Democratic Party to
shift it over time.
Again, it takes time.
Those are the choices.
A third party presidential candidate means nothing because that presidential candidate
doesn't have any cover in Congress, therefore they can't do anything.
especially given the fact that both parties in Congress will be committed to defeating that
person for partisan reasons. That president, they're not going to want that party to gain
any power. Otherwise, they may lose their seat. There's going to be no help from Congress.
They're not going to get anywhere. It has to be built from the ground up, which means you have to
You have to start with community networks.
You have to start building that power structure outside of the parties that exist, even if
you plan on shifting the Democratic Party.
It needs to be a separate power structure that can swing primaries and elections.
The short version, and there isn't anything that will change this, the short version when
it comes to dealing with the two-party system is you either compromise or you organize.
That's it.
Nothing else is going to shift that.
You will accept the options that are provided or you'll work to change them.
But working to change them is not something that occurs in months and certainly not the
months before a presidential election.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}