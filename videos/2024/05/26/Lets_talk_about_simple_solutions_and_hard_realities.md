---
title: Let's talk about simple solutions and hard realities....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=eaLz-MgwyDE) |
| Published | 2024/05/26 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about simple solutions
and hard realities.
Give a little pep talk, because a whole bunch of people
who believed in simple solutions are starting to face
those hard realities.
So we're going to kind of go over some things,
because my inbox over the last couple of days, it has filled up with messages containing
the words pointless, hopeless, worthless, because they believed in simple solutions
and they've realized the simple solution isn't.
If you don't know what happened, the ICJ, they put out a ruling, a binding ruling, to
telling Netanyahu not to go into Rafa.
They finally caught up to Biden's position.
And it certainly appears that Netanyahu's response is a single-fingered salute.
And this has raised a lot of doubts in people because they've gotten most of their simple
solutions now.
They got the General Assembly vote at the UN.
They got applications for warrants with the ICC.
They got an ICJ ruling.
They got it all, but nothing has changed.
That doesn't mean that pursuing peace is pointless or hopeless or worthless.
just means it's not easy. The easy path is always mined. And I've realized
recently that people don't know what I mean when I say that, not really. They
understand that it is secretly hard, but that's not what I mean. I mean
there's something there that people see as easy, but as they go down on it, well
they might discover something that's even worse along the way. As an example the
ICC thing. Okay so warrants they go out and the next day Netanyahu is like I
want to go to Germany so he goes and the Germans are like hey I know this is
really weird but I need you to get in the van. What happens next? And don't say
he goes to the Hague because this isn't about making you feel better, right? This
is about creating peace and helping them. That's what it's about. So if that occurs,
Netanyahu is off the board. What happens next? That's the question you always have
to ask with the Simple Solutions. What happens next? Who takes his position? I'm
I'm willing to bet that a whole lot of people who are very supportive of these ICC warrants
couldn't guess.
There's a list of options and some of them, as hard as this may be to believe for some
people, some of them are worse than Netanyahu.
Now do I think that's likely?
No.
I don't.
point, I am skeptical of whether or not the ICC will ever actually get their hands on
Netanyahu.
But that's one of those minds.
One of those unintended consequences that weren't considered when people were looking
at the easy path.
All of the simple solutions have stuff like that.
that doesn't mean that it's pointless or hopeless. It just means it's not going to
be easy. That's it. You're entering a game of world domination against nation states.
Why would you think it was going to be easy?
This doesn't end the way people are imagining. It ends at a negotiation table between
the two sides. That's how this will end. It doesn't end in New York. It doesn't
end at The Hague. It ends at some hotel conference room. And I know somebody's
gonna say, but you know we still haven't got that UN Security Council vote. Yeah
that'll be the one. Okay, you've got it. Furthermore, you don't just have one
recognizing a Palestinian state, okay, because that in and of itself means
nothing. You don't have a Security Council vote saying that they recognize
a Palestinian state and the borders and government and all that stuff is to be
filled in later. You don't have that. You have that and you have the authorization
for the use of military force to stop the fighting. You've got them both. What
happens next? The U.S. walks out to the General Assembly, looks out at all of
those countries that have had a whole lot of rhetoric but not a lot of support
of keeping them at the table. There are exceptions to that of course. But looks
out at them and ask who's sending the troops? Because we're the US. We'll invade
any country, anytime, for any reason or no reason. But even we don't invade
nuclear power so who's going? That is the quietest that room will ever be. And now
Now the Security Council resolution doesn't mean anything either.
That's what gives the Security Council the weight, is the ability to use military force.
But because Israel is a nuclear power, that's not really going to happen.
It ends at a negotiation table.
The question of whether or not is it worth it?
The same people are suffering today that were suffering five days ago.
It's no longer worth it because it got hard?
A lot of these simple solutions, all of them.
They have a whole lot of minds on those paths.
When you start asking what happens next, you realize those simple solutions aren't.
It ends at a negotiation between the two sides.
That's how it ends.
And I know somebody's going to say, how can they do that?
The Palestinians, they don't even agree amongst themselves.
You know, the school I went to taught me that it took from 1783 to 1788 to get a constitution
that was signed and ratified five years before there was agreement, after there was peace.
It's not an easy process, it's not a simple process, but the pursuit of peace is always
It's worth it, always.
You live in, most people watching this channel, you live in a core country.
The majority of people, I want to say it's like 73%, live in a core country.
Your voice matters when it comes to your government providing support to keep them at the table
as long as they have to.
It's not easy.
It doesn't fit on a bumper sticker, but it's worth it.
somebody who for seven months has been the the person who has been saying this
isn't going to work, this isn't going to work, these simple solutions really
aren't. This is the best chance for peace, for real peace, that has existed in my
lifetime. And I have a reputation as saying this isn't going to work, but the
negotiations can. They can. There's your hope. The question is not whether or not
supporting an attempt at real peace. The question isn't whether or not it's
pointless or worthless. The question is whether or not your beliefs are because
it got hard. Because it wasn't simple. Anyway, it's just a thought. Y'all have a
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}