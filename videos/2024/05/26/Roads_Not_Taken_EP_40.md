---
title: Roads Not Taken EP 40
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=PnG8z6eK6o4) |
| Published | 2024/05/26 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again,
and welcome to The Roads with Beau.
Today is May 26th, 2024,
and this is episode 40 of The Roads Not Taken,
which is a weekly series
where we go through the previous week's events
and talk about news that was unreported, underreported,
didn't get the coverage I thought it deserved,
or I just found it interesting.
Starting off today, we are going to just point something out
later on today over on the other channel.
A video will come out, because we had a bunch of questions
come in about a recent ruling concerning international law.
And a lot of messages came in with words
like pointless, hopeless, worthless, stuff like that.
And it's intended to be a pep talk.
However, if you are not somebody who responds well
to tough love, maybe skip that one.
OK, so starting off with foreign policy,
there was a coup attempt in Congo.
The presence of a handful of Americans
led to speculation on social media
that it was something from the Central Intelligence Agency.
At this point, it looks more like an incredibly bizarre,
non-CIA thing.
Normally, I would mock people for random speculation
on something like this, but I mean, saying the CIA
is behind a coup in Africa is normally
pretty safe bet. So I think we can let the speculation slide on this one.
Russia removed buoys marking the border with Estonia. That's probably going to become
an issue internationally. McConnell recently admitted on Face the Nation that the Republican
party is largely to blame for Ukraine's recent setbacks on the field.
As if that wasn't well known.
I guess it is nice to hear it, though.
General Brown, chair of the Joint Chiefs, gave some very blunt remarks about Israeli
strategy.
It could be summed up as you're doing it wrong.
It was pretty direct.
The Israeli government seized equipment belonging to Associated Press.
There was a bunch of outrage.
The equipment, I think it might have actually already been returned by now.
Sanders is saying that he is going to boycott Netanyahu's address to Congress.
Moving on to U.S. news, Senator Tuberville continued his very unique streak when it comes
to foreign policy statements.
He was told that NATO members want to at some point in the future add Ukraine to NATO.
He asked the defense secretary, do you think Russia likes that?
and sovereignty is not actually up to the whims of Russia, to be clear.
Okay, so Trump whined to his audience that, quote, if Democrats get their way they will pass a
federal law protecting abortion rights. Biden shared the clip on social media, adding the
commentary of yes. Giuliani started to go on kind of a political rant during a court proceeding
dealing with the Arizona case. The judge threatened to mute him. There was also another thing that
may have occurred while on that call a leak of sorts in which maybe Giuliani should have muted
himself. Conservative politicians appear to be getting super angry because a quote foreign
company is attempting to buy an ammo maker in the U.S. They say that they're worried about it
falling into the quote, wrong hands, it's a Czech company, a NATO ally, just saying Biden passed a
a pretty big milestone with 200 judges on the bench. Trump continues to inflate his crowd size.
No big surprise there. We saw that one last season.
Then the Supreme Court approved a South Carolina congressional map that was held at one point
to have diluted black voting power because, I mean, of course, that's what the Supreme
Court did.
In cultural news, Trump campaign has vowed to sue the filmmakers behind The Apprentice.
There are a couple of scenes in it that depict Trump, that character, in a way that Trump
certainly did not like.
I feel like this might be a mistake on their part because, well, it might just encourage
more people to see it.
Let's see...Fox ran a headline stating, quote, embattled Trump prosecutor fights to keep
job as Georgia among five states holding elections Tuesday.
I mean, that's a headline.
That sounds like the DA there is really at risk of losing.
I wonder what happened.
Oh yeah, she captured 87%.
I'm sure that's just a mishap.
no way that Fox was trying to sensationalize election coverage to boost views and play
its readers for rubes.
In science news, the CDC is prepping for quote increased risk to human health due to bird
flu.
Senate appropriations were basically told to wait by the head of NASA.
The short version there was that Artemis pilots, the safety comes first.
Good.
Politicians wouldn't be the ones calling the families if something went wrong.
I would suggest that maybe politicians should stop arguing with the literal rocket scientists
about this.
Okay, moving on to oddities.
in the house wanted to create a drone corps inside the army. The army said that it would disrupt
their plans. It looks like the current plan is to have drones completely integrated across the
service. So there's a bizarre look at the future. Okay, moving on to the Q&A. Is the peer working
or not. Every outlet I see has a different take. Also possibly related, WTF is a CRAM.
C-RAM. C-RAM. Okay. Okay. Is the peer working or not? Is it working? Yes. Is it working up
to capacity? Not even close. There have been logistical issues. Since we're on this topic,
I will go ahead and correct some information that is going out.
The pier did not wash away due to high seas.
I believe it was four boats came unmoored and they floated away.
That led to a bunch of information going out that isn't entirely accurate.
Now as far as the different takes from the different outlets, understand it's in
flux one day it's running one day there's something wrong the high seas
caused an issue and then you have to keep in mind that different outlets have
a vested interest in it working or not so it is it has been getting in food I
want to say they're at like a million pounds or something like that of food
and which is I mean to those people who got it that's that's substantial it's
life-saving to them but that's nowhere near enough and it is nowhere near
capacity so they still have some stuff to work out now as far as a cram a
CRAM that is a system that I believe was installed by the pier or on a floating
dock near the pier that is an air defense system.
Basically to put this in really simple terms, it is a super machine gun that shoots things
out of the sky.
It's pretty automated.
For all of those people, when the topic of the pier first came up, everybody said the
other side, whoever the other side is, was going to blow it up. That they were
going to, you know, send in something to destroy it. That would come
in by air. The CRAM would stop that, at least make the attempt, and it is
generally pretty effective. So that's why that's there. Okay, my
My question is why do so many Americans, but in my experience more often older Americans,
have this general attitude of preferring to see everyone lose something, have a disadvantage
or struggle in some way, rather than see a single other person have a perceived handout
or get assistance?
know something that I have noticed about this is generally it's not people who
have struggled. I feel like this is a this is a byproduct of people buying
into the identity that you know I did all of this by myself and I you know
bootstrapped and all of that when maybe they they had a little bit more of an
advantage than they realize. That's generally where I see this come from as
far as why people why people hold this idea that you know nobody's supposed to
help because there is a lot of media designed to give people that idea because it keeps
it keeps those on the bottom from understanding their their real power.
Why is the USA not giving Ukraine the same weapons we would use if attacked by Russia?
Some of it has to do with the technology in it.
Some of it has to do with that's our stuff and you know they don't want it to be used
somewhere else in case we need it.
There's a list of reasons for this.
Some are good, some aren't when you're looking at it objectively.
But there's a giant list.
I was going through your preparedness back catalog, which is very helpful, and you made
an offhand dismissive comment about what passes for civil defense in this country.
I would agree, based on the fact that I, a 40-year-old U.S. citizen, have no idea what
is considered civil defense.
It seems that community preparedness, if that is part of civil defense, is left entirely
to individual citizens or a patchwork of law enforcement, first responder state, municipality,
non-profit, and or corporate emergency planners, at least from what I see.
However, I would like to ask you what would constitute good civil defense in your opinion?
What would it look like?
How would it be implemented and administered?
I ask in part because there are groups in my region who are trying to establish better
mutual aid and community preparedness for emergency situations.
I'm curious to know your opinion and those of your viewers who have expertise in the
subject.
Okay so this is a subject that I will do an entire video on this.
I will do an entire video on this particular topic.
It's just one of those things where
I have a large interest and a lot of information
and practical experience.
So that's not something I can boil down into a short answer.
I have a question I've been wondering about
and that you might be able to answer.
normal is the practice that a state uses non-state actors or state actors, see example below,
as a way to influence other nations and how often is this public knowledge in that state?
Let me give you some examples to clarify what I'm asking.
We all know about Russia's green little men and we know that Iran uses military groups
around the Middle East.
In Russia's case, it is clear these green little men are connected to Russia and led
by Russia, would you call this a non-state actor or would you call it something else?
I'm aware that they are used because of plausible deniability.
Do western states use the same tactic?
Am I naive if I believe that this is something that authoritarian states use more than western
liberal democracies?
My main question is how normal is this practice?
I'm from Norway and I would be surprised if I learned that Norway had a small group
of green little men somewhere in the world.
Likewise, should I learn that Germany or any other country in Europe did?
Could you talk a bit about this?
Okay, short version here.
I don't want to say you're naive because the whole purpose is that people don't know.
But Western liberal democracies absolutely do it.
I don't know if it's more or less, okay, but it's this is a common practice.
Now as far as the little green men, yeah, those were they were non state actors, but
everybody knew that they really weren't.
They were civilianized, but everybody knew where they came from.
The plausible deniability was there, but it was also very open.
Would I still call them non-state actors?
Yes, with the qualification that their state directed.
Does Norway have something like that?
I mean, if not, they have access to another ally's network.
Yeah, this is incredibly common.
It is incredibly common.
I might just do a video on this, too,
going through and providing different examples of how
they're used, how sometimes they're organic but then used for larger countries' purposes,
how sometimes they're organic and the interests truly do align, how sometimes they're just
straight-up co-opted, how sometimes they're not organic at all.
There's a lot of different categories here, but the short answer is yes, the West uses
them.
Which takes longer, finding a specific shirt or finding an old video that you've just referenced
to put in the current video's description?
I guess it would depend.
You know, there's close to 5,000 videos over on the other channel, and it can get confusing.
There's something I don't want to put, there is a way that I have been finding them lately,
relatively easy, but I'm not sure that I'm supposed to tell people about it yet.
The shirts are normally pretty easy to find.
The real issue comes when I have one on and I leave with it.
It hasn't made it back yet, but I'm still looking for it.
I noticed there are few, if any, cuts in your videos.
On average, how many takes does it take for you to create one of your videos?
Has there ever been a video that took way too many takes?
Sometimes, most times, one.
This is the first take of this, and this appears to be the last question.
So as long as I don't mess up here in the next few seconds, it'll be okay.
Sometimes it takes more, particularly if it's one where I'm a little heated, because sometimes
my language is not always as clean as it is on this channel, and sometimes it slips out.
those. I just I reshoot those. Has there ever been a video that
took way too many takes? The one where I was talking about I was
talking about politicians basically using the deaths of
service members for political points. I had to record that
like 20 times because I kept slipping up
and saying things that were not fit for public consumption.
So I guess if I had to average it, maybe two, I would think.
But a lot of times, it's just one.
And sometimes when I'm recording a bunch at one time,
I get tongue-tied somewhere in and have
to take a break, but, okay, well, that looks like it.
So a little more information, a little more context,
and having the right information
will make all the difference.
Y'all have a good day..
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}