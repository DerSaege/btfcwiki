---
title: Let's talk about Trump's bad night....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=xhm9geEg9uM) |
| Published | 2024/05/26 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, Internet people.
Let's bow again.
So today we are going to talk about Trump's evening, how things went last
night for him, we'll go over just some highlights and then we'll talk about the
one statement that really kind of, I find it very telling.
So if you missed yesterday's coverage, Trump was invited to go talk to the
Libertarian Party, now the day before, R.F.K.
Jr. had talked to them, and when he talked to them, he spoke to issues that they
cared about.
Trump, I knew it was going to be bad.
But I did not know it was going to be that bad.
He was heckled pretty consistently.
Things were shouted at him during his speech that, I mean, it's not something I'm going
to repeat on the channel, any of it, and there was a lot.
He was booed pretty consistently.
There was a person with a sign that said no wannabe dictators that was dragged off by
security.
When his supporters would start like pro-Trump chants, the libertarians would start chanting
libertarian slogans.
It did not go well.
And he tried.
his pitch it just did not it did not resonate based on what I saw and while
he was talking and trying to encourage them to join up with him he said only if
you want to win only if you want to win maybe you don't want to win maybe you
don't want to win. Only do that if you want to win. If you want to lose, don't do
that. Keep getting your 3% every four years. Now, obviously that's not, that
doesn't come across very well. That doesn't come across as something that
would endear you to the people that you're talking to. Basically saying y'all
are losers without me. It doesn't really, doesn't seem like something that would
resonate. But there's something else about this that just kind of piqued my
interest a little bit. It doesn't matter what you think of the Libertarian Party.
Doesn't matter if you disagree with every one of their beliefs. I think that most
people who have ever dealt with libertarians would acknowledge that even if you disagree
with all of it, you know that they believe their beliefs.
That it's not just rhetoric, right?
That they actually believe their platform much more than either of the major parties.
ideologues. They're people who actually believe their own rhetoric. Telling
them that, you know, well all you have to do is sell out and you'll win. That, that's
a very unique method of trying to reach ideologues. And what it tells me, it
It certainly appears that Trump doesn't have any experience dealing with ideologues.
What does that say about the people in his inner circle?
People who have dealt with true believers before, they would not make this mistake.
It seems incredibly unlikely to me that Trump has ever dealt with anybody who actually believes
their stated positions.
And honestly, I find that incredibly telling.
I don't see how somebody can make this mistake.
if they have any experience with people who actually believe their stated positions.
So based on how things went, I don't believe that Trump will be getting the Libertarian
nomination and it doesn't seem like he's going to capture a large portion of those
votes even if it is only three percent. I feel like most of them would probably
rather keep their three percent. It was a very entertaining site. I would imagine
there's going to be clips everywhere today. It will probably almost certainly
be on the news. That did not go the way the Trump campaign imagined it would. I
definitely believe that RFK Jr. pulled more support from the appearance.
And one thing that I'm pretty sure of after this is that I think Trump will
try to avoid being on stage with RFK Jr. any way that he can. I don't think he's
going to want a repeat of that because I don't think he's going to understand
that he was talking to ideologues and that had a lot to do with their response.
He's going to read it as RFK Jr. beat me. So he's probably going to try to avoid
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}