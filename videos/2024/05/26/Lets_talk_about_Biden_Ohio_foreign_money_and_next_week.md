---
title: Let's talk about Biden, Ohio, foreign money, and next week....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=PEc_5b5b3Yo) |
| Published | 2024/05/26 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Ohio
and Biden and the ballot
and a potential special legislative session
and money and all of the claims that are going on
and just kind of run through what's happening up there.
A quick recap of this, greatly oversimplifying it,
is that Ohio State Law is broken and it would require Biden to be nominated before the convention
so he can be on the ballot. It's a mess. The House and the Senate in Ohio, they were trying to work
this out and it kind of fell apart. The governor appears to be calling a special legislative session
for next week to sort this out and to get Biden on the ballot. However, there is also the
Republican concern of foreign money in Ohio ballot initiatives. Now, to be clear,
the Democratic Party is also against foreign contributions when it comes to Ohio politics.
However, the Republican stance on it is a little bit more, let's just say convoluted
than the Democratic parties.
It certainly appears that the Republican party doesn't really just want to ban contributions
from foreign nationals.
They also want to stop money coming from US entities that at some point have received
money from foreign nationals.
The example that I was given when talking to somebody about this was, I won't say the
person's name, but a billionaire, a Swiss billionaire, who gave money to a non-profit
in DC.
That money went into their account.
A whole bunch of other money went into that nonprofit's account.
Later, that nonprofit gave money to support an Ohio ballot initiative.
It's also worth noting that the Swiss billionaire actually lives in Wyoming, but still maintains
this Swiss citizenship.
From my understanding, the Republican Party doesn't want that to happen either.
Keep in mind all of those transactions are completely legal.
It's just, it seems to me to be a red herring.
I have yet to have anybody give me an example where a foreign millionaire or billionaire
intentionally routed money through a third party or anything like that because they truly
care about, you know, internal Ohio politics.
It certainly seems to me that this is the Republican Party kind of lashing out at the
ballot initiative process because the people in Ohio don't want to be ruled.
They want to be represented.
It's worth remembering that recently the people in Ohio, I mean, they basically told the Republican
Party to sit down through the ballot initiatives.
And my guess is the Republican establishment is unhappy about this and they're lashing
it out.
The two things collide, this foreign money, quote, and Biden being on the ballot because
some Republicans want to link the two, and they're trying to get the Democratic Party
to go along with something that certainly seems as though it would block way more than
foreign money, and it would kind of jeopardize the voice of people in Ohio in order to get
Biden on the ballot, trying to tie the two together.
Now the Democratic Party does have other options, such as going to court, you know, following
a lawsuit that they are almost certain to win, or nominating him for Ohio outside of
the convention, there's a bunch of other options.
But if those other options are recognized or used, it would basically leave the Ohio
state law broken.
So we'll see how it plays out.
get more details on it next week, I'm sure. But it is shaping up to be a situation where
the Republican Party is trying to tie two unrelated issues together, and one of them
certainly appears to be a situation where the Republican Party is telling its constituents
to be quiet, sit down, do what you're told, obey like the obedient lackeys that you are,
And they want to make sure that the Democratic Party signs onto it as well to make it look
bipartisan.
That's my take on it.
We'll have to wait and see how it plays out.
But that's early, how it's shaping up.
Hopefully that will all be adjusted.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}