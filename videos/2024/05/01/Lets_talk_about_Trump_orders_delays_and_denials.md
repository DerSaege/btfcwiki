---
title: Let's talk about Trump, orders, delays,  and denials....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=cIA6BxlMPPw) |
| Published | 2024/05/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Overview of the legal proceedings involving former President Donald J. Trump in New York.
- Judge found Trump violated the gag order nine times, resulting in a $9,000 fine.
- Judge expressed dissatisfaction with the fine and hinted at the possibility of jail time for wealthy defendants.
- Trump's appeal to have the judge removed from the case and to halt proceedings based on claims of presidential immunity was denied by the court.
- Right-wing media misrepresented a judge's decision regarding Trump attending a family function.
- Contrary to the outrage manufactured by some, Trump was granted permission to attend the graduation.
- The focus inside the courtroom is on providing detailed information.
- The judge's stance on potential jail time for Trump remains uncertain.
- The manufactured outrage surrounding the judge's decision was baseless.
- Trump's only victory was being allowed to attend the graduation.

### Quotes

1. "The outrage of the week was completely over nothing, totally manufactured."
2. "Everything else was a loss for him today."
3. "Y'all have a good day."

### Oneliner

Beau provides an overview of the legal proceedings involving former President Donald J. Trump in New York, including violations of a gag order, appeals, and a victory to attend a family function, debunking manufactured outrage along the way.

### Audience

Legal observers

### On-the-ground actions from transcript

- None mentioned in the transcript.

### Whats missing in summary

Insights on the potential implications of the legal proceedings and the significance of the judge's decisions.

### Tags

#LegalProceedings #DonaldJTrump #NewYork #GagOrder #Outrage #Courtroom drama


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about former president
Donald J. Trump and New York and decisions and orders
and denials and the one thing that he got granted
and just kind of run through the events of the day
when we're talking about the stuff that occurred outside
the main proceeding. Okay, so starting off, one of the things that everybody
wanted to know about the gag order, the violations of that, the judge found that
he had violated it nine times, gave him a $9,000 fine. Judge did not seem happy
with that and indicated that he believed in cases where the defendant
was wealthy, that maybe a higher fine was more appropriate, but then kind of
turned it into the alternative is jail kind of statement. We'll see how that
plays out. Trump had the appeal pending. He appealed trying to get the judge
thrown off the case and get a stay based on his claims of presidential immunity.
He said that the whole thing had to stop until the Supreme Court made
its decision. The court just denied it. Just no. So there's that. The judge who he
said told him he couldn't go or it looked like he wasn't going to be able
to go to a family function, a graduation, and then right-wing media got super mad
and told everybody that it had actually happened. Okay, I said at the time none
of that was true and the judge actually said well we'll have to see where we're
at as far as the trial goes. The judge did make the decision on that today and
And yes, he gets to go.
So all of those people that pushed that idea that the judge had said, no, he's not going
to be able to go, and it's a great travesty, and this shows that the judge is very, very
unfair and blah, blah, blah.
Yeah all of that outrage, that outrage of the week was completely over nothing, totally
manufactured. The order that many of them led people to believe had been, you know,
had been extended. It never happened. So that is the one thing that Trump got.
He's going to be able to go to the graduation. Everything else was a loss
for him today. I know there's going to be people who are looking at the gag
order violations and saying well that's not enough and I mean it certainly
appeared that the judge agreed but it doesn't seem like the judge was ready to
toss him in jail just yet. Again, we'll see how that plays out in the future. So the
stuff inside the courtroom, it's more of the same. It is at this point they're
providing a lot of details and they're they're doing the tell them phase of it.
Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}