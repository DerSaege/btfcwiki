---
title: Let's talk about Dems protecting Johnson....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=7LgCNRtOkew) |
| Published | 2024/05/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The US House of Representatives and the Democratic Party are helping Johnson retain his position as Speaker of the House.
- The Democratic Party is choosing to protect Johnson and keep him as Speaker of the House.
- Beau presents the options the Democratic Party had: trying to get Speaker Jeffries, doing nothing, helping Marjorie Taylor Greene, or protecting Johnson.
- If the Democratic Party does nothing, it may result in chaos and gridlock in the House.
- Helping Marjorie Taylor Greene may give a win to the MAGA faction and Trump, creating chaos and uncertainty.
- Protecting Johnson could weaken him in the Republican Party, especially among the Twitter and MAGA factions.
- By allowing Johnson to keep his position, the Democratic Party may generate some goodwill, but it weakens Johnson within his own party.
- Beau suggests that ensuring Johnson does not become like McConnell should be a high priority for the Democratic Party.
- Using a procedural method to address the situation discreetly seems to be the course the Democratic Party is leaning towards.
- Beau concludes by expressing uncertainty about the final decision and advises waiting to see how things progress.

### Quotes

- "Is this the right move for the Democratic Party?"
- "But if it happens again and they come out and say, the Democratic Party, they were just intractable, even after we compromised on the aid package or whatever they use, that might shift the blame to the Democratic Party."
- "At this point, we'll have to wait and see what actually occurs."
- "It's just a thought, y'all have a good day."

### Oneliner

The Democratic Party faces strategic choices regarding Speaker Johnson, with options ranging from helping Marjorie Taylor Greene to protecting Johnson and potential consequences on party dynamics.

### Audience

Democratic Party members

### On-the-ground actions from transcript

- Monitor the developments within the Democratic Party regarding Speaker Johnson (implied)
- Stay informed about procedural decisions related to retaining Speaker Johnson (implied)

### Whats missing in summary

Insights on the potential implications of the Democratic Party's decision for future party dynamics and Speaker Johnson's position.

### Tags

#USHouseofRepresentatives #DemocraticParty #SpeakerJohnson #StrategicDecisions #PoliticalAnalysis


## Transcript
Well, howdy there, internet people, it's Bill again.
So today, we are going to talk about
the US House of Representatives
and the Democratic Party helping Johnson retain his position
as chief babysitter, I'm sorry, as Speaker of the House,
because reporting suggests that that decision has been made
and that that's what they're gonna do.
They are going to protect Johnson and keep him as Speaker of the House.
As soon as that reporting came out, questions came in.
General tone of most of them, is this the right move for the Democratic Party?
Why would they do this?
So on and so forth.
Okay.
Well, the easiest way to do this is go through the options that they had available and then
you pick the one you would have done.
one. You try to get Speaker Jeffries. I mean, that would be the goal if you're
the Democratic Party, right? You try to swing things that way. I know that there
are people who believe that the Democratic Party is not always that
politically savvy or capable, but I do believe that even those people think
that the Democratic Party can count. If they had the votes for that, that's what
they'd be doing. If there was a shot of that occurring, that is what they would
be doing right now. They obviously don't have the votes. So that one, it's an
option. Option one is an option, but only in theory. It's off the table. Option two,
you do nothing. You're the Democratic Party, you do nothing. That is a Republican
problem, we're not getting involved. They have to decide for themselves. If that
occurs, what happens? Motion to vacate moves forward, probably a bunch of chaos,
a bunch of gridlock, take some time to get a new speaker, so on and so forth. Now
last time this occurred, the Republican Party caught the blame for it because it
It really is their issue.
But if it happens again and they come out and say, the Democratic Party, they were just
intractable, even after we compromised on the aid package or whatever they use, that
might shift the blame to the Democratic Party.
Is it certain?
No, but it's a possibility.
And then you look at who you might get as the next speaker.
Also uncertain.
Generally speaking, politicians, they don't like uncertainty.
That's not something that they enjoy.
But that is still an option.
May not be one that a lot of them like, but it gets to stay on the table, okay?
So what's the next option?
You help the space laser lady.
You help Marjorie Taylor Greene.
If you do that, what occurs?
You give the Twitter faction, the MAGA faction, and by extension Trump, a win.
Something that he can take credit for, because he will.
And then you have the chaos, the uncertainty about who's going to be speakers, so on and
so forth. And now the Democratic Party has taken an active role in creating
that chaos. That doesn't seem like a good idea. It seems like doing nothing would
be the better option there. There's still the uncertainty, but the uncertainty
exists with both and doing nothing you don't guarantee the the MAGA faction a
win. And then you have the option of protecting Johnson. What does that do?
Well, you're stuck with Johnson, who is definitely proving to be better than
people anticipated at doing this. At the same time, you hand the Twitter faction,
the MAGA faction, a loss because they didn't get to do their little motion to
vacate, and cause the chaos that they wanted, and get the prolonged social media clicks.
And you also give Johnson a loss.
He gets to keep his job, he gets to be the babysitter, but he only gets to keep it because
you allowed it.
And sure, there is probably going to be some goodwill generated there.
But beyond that, him keeping that position because you allowed it, weakens him in the
Republican Party, especially among the Twitter faction, MAGA faction, because he's just
a rhino.
He needed help from the Democrats, and they're evil, bad people.
So going that route, you create red on red fire.
You encourage the infighting that is already occurring.
And you get the certainty of knowing who the speaker is going to be.
And realistically, you look like you're just paying back the votes that he gave you for
Ukraine.
Which would you choose?
It's not the option they wanted, obviously.
that it appears that out of the options that they could realistically perform, those that
they could really choose, it's probably the best one.
And again, I think that it should be a high priority for the Democratic Party to make
sure that Johnson does not become McConnell.
him out of the speaker's seat, maybe it does that, but maybe it doesn't.
This way, you kind of guarantee that there is an undercurrent within the Republican Party
that views him as part of the uniparty or whatever, and he gets resistance from inside
his own party.
just up on the House, but maybe among voters nationally, even outside of his district,
they may view him as somebody who betrayed the Republican Party.
That seems like the better option.
Again, at this point, we'll have to wait and see what actually occurs, but from what I
understand, they are going to use a procedural thing to kind of just get rid of this before
it starts. Which means while the Democratic Party is, you know, apparently
committed to that course, they don't really want to do it like way out in
public. At least that's the way it seems right now. We'll see as things progress.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}