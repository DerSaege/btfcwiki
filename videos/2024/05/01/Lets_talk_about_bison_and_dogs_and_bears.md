---
title: Let's talk about bison and dogs and bears....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=YbKMP8Q0FsM) |
| Published | 2024/05/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing internet people, discussing animals and a recent news item about a man who kicked a bison at Yellowstone, resulting in minor injuries.
- Advising against feeding bears and kicking bison due to their potential to cause serious harm.
- Mentioning a separate animal-related incident involving the governor of South Dakota, but not delving into it deeply.
- Mentioning the training process for certain dog breeds and the responsibility of owners in training and behavior management.
- Encouraging responsible dog ownership and rehoming if needed, rather than resorting to extreme measures.
- Signing off by wishing everyone a good day.

### Quotes

1. "Do not feed the giant hamsters, those are bears, and do not kick the fluffy cows."
2. "The animals, they're not domesticated at all, doing that is not a good idea."
3. "Those dogs were trained to do those tasks long before the invention of an electric collar."
4. "If you ever find yourself in that situation, it would probably be best to rehome the dog."
5. "Y'all have a good day."

### Oneliner

Beau addresses animal incidents and responsible dog ownership while sharing tips on interacting with wildlife, ending with a wish for a good day.

### Audience

Animal lovers and responsible pet owners.

### On-the-ground actions from transcript

- Refrain from feeding wild animals or engaging with them in a harmful manner (implied)
- Ensure proper training and care for working dogs to avoid behavior issues (implied)
- Prioritize responsible pet ownership and rehoming if necessary (implied)

### Whats missing in summary

Beau's engaging storytelling and humor elements are missing in the summary.

### Tags

#Animals #Wildlife #PetOwnership #Responsibility #Safety


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about animals.
We're going to talk about animals,
primarily focusing on a news item
that I feel almost obligated to cover
because so many of y'all sent me little bits of information
about it, knowing that I would find it
incredibly interesting story. Okay, so we'll start there. According to reporting,
a 40 year old man visiting Yellowstone, well, he suffered minor injuries after
allegedly kicking a bison in the leg. As somebody whose last words are very
likely to be, I'm a pettit, if you are ever up in that part of the country, just
some quick tips. Do not feed the giant hamsters, those are bears, and do not
kick the fluffy cows. The fact that these are minor injuries is nothing short of a
miracle. These things weigh like 2,000 pounds. They can hurt you like badly. So
just just something to bear in mind if you are up that way. The animals, they're
they're not domesticated at all, doing that is not a good idea.
Okay, so moving on, there's been a whole bunch of questions about another animal-related
incident from that general part of the country that also crosses over into politics and dealing
with the governor of South Dakota.
Yeah, I'm going to just say not really something I want to talk about at length, just a couple
of key things here.
First, you cannot expect any dog to be trained as a puppy.
particular breed is a puppy until I think 24 months. Aside from that, something
that occurs a lot when people get dogs that are working dogs or they are dogs
or pointers, they are often dogs that if you want them to perform in that
roll, you have to spend a lot of time training them. And it is worth noting that
those dogs were trained to do those tasks long before the invention of an
electric collar. Oftentimes when people have a dog that does not behave the
way they expect it to, they blame the wrong end of the leash. If you ever find
yourself in that situation, it would probably be best to rehome the dog or if
necessary take it to a shelter instead of taking it to a gravel pit. Anyway, it's
It's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}