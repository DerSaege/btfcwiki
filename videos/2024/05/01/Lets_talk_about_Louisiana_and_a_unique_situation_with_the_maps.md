---
title: Let's talk about Louisiana and a unique situation with the maps....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=WfOjR2kpaH4) |
| Published | 2024/05/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Louisiana Congressional maps were gerrymandered, leading to a dilution of the black vote.
- Typically, in the South, Republicans either challenge the court ruling or ignore it when faced with fixing gerrymandered maps.
- Surprisingly, Republicans in Louisiana agreed to create another majority black district to comply with the court's order.
- Despite passing the new map, a group of non-African American voters challenged it in court, claiming it violated the Voting Rights Act.
- The court agreed with the challengers and ruled against using the new map with the second majority black district.
- This decision likely means a Democratic win in the upcoming elections and a loss of a House seat for the Republican Party.
- Republicans in Louisiana seem to be supporting the less gerrymandered map, even though it goes against their usual behavior.
- The deadline for deciding which map to use is May 15th, with a potential direct appeal to the Supreme Court expected soon.
- The situation is unique as it defies the typical dynamics seen in Southern politics.
- Both Republican and Democratic members are unhappy with the recent ruling, making this a significant development to follow closely.

### Quotes

1. "Louisiana Congressional maps were gerrymandered, diluting the power of the black vote."
2. "Republicans in Louisiana agreed to create another majority black district, but faced legal challenges."
3. "The court ruled against using the new map, leading to potential Democratic gains and a loss for Republicans."
4. "Republicans supporting a less gerrymandered map is unexpected in this situation."
5. "The May 15th deadline for choosing a map adds urgency to the unfolding events."

### Oneliner

Louisiana faces a unique political situation as Republicans navigate unexpected support for a less gerrymandered map amidst legal challenges, potentially altering election outcomes and partisan dynamics by the May 15th deadline.

### Audience

Louisiana residents, Political activists

### On-the-ground actions from transcript

- Contact local representatives to express support for fair redistricting (suggested)
- Stay informed about the legal developments and potential Supreme Court appeal (implied)

### Whats missing in summary

The full transcript provides a detailed insight into the unique political landscape in Louisiana regarding redistricting and legal challenges, offering a comprehensive understanding of the unfolding events.

### Tags

#Louisiana #Redistricting #Gerrymandering #LegalChallenges #SupremeCourt #PoliticalDynamics


## Transcript
Well, howdy there, internet people.
Led Zebo again.
So today, we are going to talk about
Louisiana and maps
and a unique situation that is developing
in Louisiana about a news item
that we kind of thought was done but it is
definitely going to come back into the news
in a big way because of recent developments.
OK, so a real quick recap on this.
Louisiana Congressional maps, they had an issue.
And that issue was that, well, I mean, they were gerrymattered.
And basically, the courts told Louisiana, hey,
you need to fix your maps.
You have diluted the power of the black vote.
You know, normal stuff that you hear about all the time.
Now, under normal circumstances, especially in the South,
when this occurs, it's at this point
that the Republican Party loses their mind,
starts screaming about going to the Supreme Court
and state's rights and all of this stuff,
or they just flat out ignore the ruling.
That's just what normally happens.
Republicans in Louisiana, they were kind of like, okay, fine, I guess we have to create
another majority black district.
And then they did.
It wasn't ideal.
The map wasn't ideal.
It did not look like any of the sample maps, but they did, in fact, create another majority
black district.
And then they passed it.
It went through the House, it went through the Senate, and the governor signed it.
So you would think that would be the end of it.
Twelve people who are, I believe, described as non-African-American voters, I think that's
actually how they're described in the suit, they took the new map to court alleging that
it was bad too, that it violated, I think they said it violated the Voting Rights Act.
kind of suggesting that maybe it was racially gerrymandered against white
people maybe. I really don't understand the exact suit but the court agreed. The
court agreed and said that the new map with the second majority black district
Well, that one there, you can't use that one.
Now, that new district, it is almost certainly going to be a Democratic win.
The Republican Party loses a seat in the House because of this.
So you would expect Republican politicians in Louisiana to kind of be like, well, okay,
you know, but apparently not. Early indications and their statements, it
certainly appears that Republicans in Louisiana, Republican politicians in
Louisiana, are going to fight in favor of the map that has the second-majority
black district. The Attorney General said something to the effect of, of course, we
We will seek Supreme Court review.
It appears that the Republican Party is going to fight in favor of the less gerrymandered
map.
Not a sentence I ever thought I'd say.
That's what's going on.
You can take a guess as to why they're behaving in this way.
Maybe it's just old political reflex that's saying the feds aren't going to tell us how
to draw our maps or something like that.
Or maybe they're worried that if the federal government does start to draw up the map,
it will look more like the sample map rather than the one that they approved, and that
might hurt them a little bit more.
And by hurt them, I mean the incumbents.
I don't know that it would really alter the makeup of the partisan divide that goes to
the house, but it might hurt the incumbents.
It's unique, and here's the thing, May 15th, Louisiana has to know which map to use by
May 15th.
So what occurs from here?
My guess is there's probably going to be a direct appeal to the Supreme Court sometime
in the next eight or nine days, and that will try to get some kind of stay and allow them
to use the map that has the second majority black district.
If I had to guess, based on the early statements from Republican politicians, that's probably
to move. And then where it goes from there, I don't have a clue. The standard
way this stuff plays out in the South, none of the normal dynamics are at play
here. You actually have both the Republican Party, members of the
Republican Party at least, and the Democratic candidate or the Democratic
representative from Louisiana are both saying that the most recent ruling was
bad. So yeah, this is one to watch. It's probably gonna get a bunch of coverage
because realistically this map it's going to it's probably going to decide
which party gets a seat and with the way the house is divided right now
everybody's gonna be fighting for every seat they can get anyway it's just a
thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}