---
title: The Roads to Trump, Jenga, and Foreign Policy
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=asYJ8eQy5YI) |
| Published | 2024/05/23 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people. It's Bo again, and welcome to The Roads with Bo. Today
we are going to talk about the roads to foreign policy jingo.
We're going to do this because one of y'all asked a question, and I was trying to figure out a good
way to talk about it and illustrate it, and I was playing jingo while this was happening,
And I realized that might be the right way to describe it.
When you're playing that game, if everybody knows what's going on and everybody sticks
to the general idea of finesse and subtlety, well you can build something.
I mean, the real purpose of the game isn't actually to watch somebody lose.
The purpose of the game is to, well, build something, to build it up as high as you can.
That's what makes it interesting.
The question was, why should anybody worry about a president's foreign policy?
foreign policy is so hard to shift. And it was in context of Trump, and I'm
paraphrasing the question here, and they mentioned the Pompeo Doctrine was
something that was in it. And the real idea here was that maybe
Trump's foreign policy really doesn't matter because there's only so much that
can be done and we've seen that very plainly in a couple of different
instances lately that the the president can't just make things happen the way a
lot of people believe that he can but that's assuming that he's playing the
game. The Pompeo Doctrine was one of the things in that message. If you don't know
what that was, for decades, going back to the 70s, U.S. policy was that the
settlements, the Israeli settlements, they were inconsistent with international
law. Trump came along, I want to say 2019, changed that. It became known as the
Pompeo Doctrine. Now, I mean really does it matter when you're when you're
thinking of it from a US perspective? Does that really matter? I mean it's not
like the U.S. was doing anything about it before.
I mean, it's just words, doesn't really shift anything, and for Trump, it's a good
way to appeal to a base, get a voting block.
So yeah, I mean, just switch it, who cares?
Doesn't really mean anything.
Now let's pretend you're a Palestinian.
And the world superpower that for decades, since the 70s, has said these settlements
are inconsistent with international law.
But now they're saying that, hey, everything's fine, don't worry about it.
If you are a Palestinian who is opposed to that, you might start to believe that, well,
losing. You're losing and you might need to do something to draw the world's eyes
back to the situation. You might start planning something. Now I want to be
clear. I am not saying that the Pompeo doctrine caused what happened on the
seventh. Nothing exists in a vacuum. It's not causal like that. It's a cumulative
thing. It didn't cause it, but it certainly didn't help. That's why it
matters. Because the little changes that they can make, if they don't actually
understand what's going on, if they are ignorant to foreign policy, they can have
pretty disastrous consequences. When it comes to the Pompeo doctrine, that's
probably not a great example because his base, they don't even know what that is.
I mean, let's be honest, most of them don't because it wasn't really
covered on Fox. They probably don't know what it is and if they do, it's really
unlikely that they would see a connection to anything else. So let's go
to something that we know that they're aware of because we hear it all the time
right now. You know when Trump was president, Iran wasn't attacking our
allies. I mean that's true. That's a true statement because they were attacking us.
And you will probably hear something about, yeah, but that stopped because Trump, he hit
Soleimani.
Did it though?
It didn't, just in case you're wondering.
Iran responded to that.
wounded more than a hundred American service members, TBIs, traumatic brain injuries.
It is a life-altering injury.
Like many things with the brain, it varies.
But I have seen some people with them that it's really bad.
They responded, wounded more than 100, and when asked about it Trump said, I heard some
of them got some headaches.
That's how he described it.
And didn't respond.
Did nothing.
Because Trump's foreign policy is about getting that headline for the domestic base.
That's all it's about.
It's not actually about doing anything over there.
So they responded, wounded a bunch of Americans, and Trump did nothing.
Now the weird part is, if you're actually engaging in foreign policy rather than just
pandering to a domestic base, you would have done nothing then too, initially, and then
There would have been follow-on covert actions or other events to say, hey, you can't do
that to our people.
But he didn't do any of that because he didn't care anymore.
He got his headline, he impressed his base, showed everybody he was a tough guy, still
talks about it today.
That was a failure across the board, and it had a lot of blowback.
You had the immediate response, then you had the perception in Iran, which, I mean, I don't
even know if it's a perception.
They stood the U.S. down, so of course they, well, they grew a little bit more bold.
Why wouldn't they?
weak. He ran. That's the perception. And the fact that his base doesn't know this
is why you have to. His base, they are not normally great consumers of
information. Most of them don't even know that happened either. Because they don't
know you have to. Because they consume media that tells them what they want to
hear and plays into the narrative that they already have, those preconceived
notions? Well, you have to pick up the slack. The other one, you're hearing all
the time, you know Putin didn't attack Ukraine under Trump. Well, I mean first
he didn't really need to, did he? Because Trump was running around undermining
NATO because it played at home, it played domestically.
The other part to that story is that Putin actually did attack Ukraine under Trump.
And I'm not talking about the proxy stuff, the non-state actors, the stuff where Putin
has deniability.
I'm talking about uniformed Russian troops, uniformed Ukrainian troops.
The first time it happened was under Trump, it's called the Kurt Strait incident.
You know what Trump did, nothing.
That probably led Putin to believe that there might not be any US response.
list of using Trump, this could go on and on and on. In fact, I have videos from
four years ago when he was leaving just talking about what was going to need to
be done to repair the mess he was leaving behind and how the next
president was going to catch all the blame for it. When you are talking about
foreign policy it's not that a president who is actually engaging in foreign
policy, it's not that they're going to be able to do a lot because foreign policy
is hard to shift in everything. It's a lot like the game. The move the new person gets
to make, the person whose turn it is right now, it depends on the moves that the people
before them made. And it goes around. And their moves are limited. But if they are playing
with finesse, subtlety, diplomacy, what foreign policy is supposed to be, the game can go
on and things can get built.
But when you are talking about somebody who doesn't engage in foreign policy, somebody
who just does whatever they feel like at the moment, because it's good to rile up the
base isn't playing with any degree of finesse or subtlety, one move and things
start to fall and the thing is when it's foreign policy what falls down it's not
Wood blocks its people. That's where the real issue comes in. That's why it
matters. You're not going to get drastic foreign policy change simply because you
elect a certain person. At least none that's good. But if you elect the wrong
person you can get a bunch that's bad that takes years to recover from. Anyway
It's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}