---
title: Let's talk about Lindsay Graham, the US, and the ICC....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=_c8UXIEGmk4) |
| Published | 2024/05/23 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Senator Lindsey Graham
and something he said.
And we will talk a little bit more
about why the US has the position it does concerning
international law right now.
When I make a video and I'm saying something
that is outside of what is typically
being discussed when we're talking about the inner functions of how the
government works. There are certain people that I immediately start watching
very closely because they tend to forget that they're not supposed to talk about
Bruno. They have a habit of saying the quiet part aloud when
they're not supposed to. Lindsey Graham is famous for that. And during a Senate
Appropriations Committee meeting, or subcommittee meeting, with Blinken
present, he said, it is imperative that the Senate in a bipartisan way comes up
with crippling sanctions against the ICC not only to support Israel but to
deter any future action against American personnel. That's what it's about. Doesn't
have anything to do with the situation on the ground over there. Remember what
they say about the stuff that comes before the word but. He went on to say,
if they do this to Israel, we are next. And then goes on, they tried to come after our
soldiers in Afghanistan, but reason prevailed. In a situation where it was an
establishment state actor that isn't a signatory in an area of pursuing a
non-state actor that the ICC claimed jurisdiction over. That's what they're
concerned about. That's why the US is going to have the response it has to the
ICC doesn't have anything to do with what's happening. As far as the US
position on what's actually happening over there, doesn't really look like it's
changed much. Doesn't look like this has had an impact on it. Blinken is still
pushing for a two-state solution tied to normalization with the Saudis through
the Megadill. That appears to be what they're moving towards. And they have a
new framing for that. The whole point of normalization, but also the whole point
of the establishment of a Palestinian state, is to make sure that Israel's
security is better insured. That's Blinken. So basically what he's saying is
give him a state, get a real peace, won't just be cyclical, all the stuff that we've
been talking about. He is still, does look like he's still in line with the
whole idea that Hamas cannot be part of the governing body, not really a surprise
there, but there is this interesting statement. Graham, again somebody who often
says stuff that isn't supposed to be publicly known or acknowledged, I've
heard this statement, the ball is about to be in Israel's court, is that fair to
say? That's Graham asking Blinken, and Blinken says that's my assessment, yes, so
So it appears that the state and the Saudis, probably the Egyptians as well, because they
are still really, really upset about the border crossing.
It appears that they have worked out their side of it, and they're probably about to
present it to Israel, and we'll see where it goes from there.
There is going to be a whole lot of rhetoric about the ICC coming from U.S. officials.
Understand, none of it is related to the merits of the ICC case.
They literally do not care.
That is not why they're upset.
The hostility that exists between the U.S. and the ICC goes back a while.
And again, you're talking about a country that invaded the Hague.
It's about making sure that U.S. personnel do not fall under their jurisdiction.
That's what it's about.
It's not about the merits of the case.
It doesn't appear that it has shifted U.S. policy in any way, shape, or form.
The discussion about supplying weapons isn't in anything that I've been able to get access
to.
So I don't know if there's been a shift there.
But overall, the idea for a peace plan, it's the same as it was.
It hasn't changed anything.
In some ways, they would never admit this, but in some ways, the ICC case might actually
help Blinken move things along because the establishment of a Palestinian state
or a very credible pathway to one might go a long way right now. So there might
be an incentive to move in that direction, but we don't know that. So
we'll have to keep waiting and seeing. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}