---
title: Let's talk about Senate shenanigans showing signs of success....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=XwLNlfGv4B4) |
| Published | 2024/05/23 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about Schumer's
Senate shenanigans already showing signs
of working in the House.
We talked about it recently,
how Schumer was bringing back a piece of legislation,
was gonna get a vote on it,
and that it was really all a political ploy
to put Republicans in a tough spot
it comes to a vote because they can either vote in favor of border security or they can listen to
Trump and vote against it. If they vote for it, they suffer the wrath of Trump. If they vote against
it, well, their voters might be upset. So Schumer put this plan in motion and almost immediately
The Republicans in the House have announced that this thing is not going anywhere in the House.
There's no way it's going to pass. They are not going to do it, and they are calling it a fake border bill.
Now, it is worth noting that if this passed, it doesn't look like it will, but if this was to pass,
It would be the most comprehensive border security bill and overhaul in decades.
And the Republican Party is coming out against it, which is what Schumer kind of wanted him to do.
I know people are going to say, but no, Trump overhauled the border.
No, he didn't do anything via legislation. He used executive order.
order. That's why it was a Band-Aid and didn't actually fix anything. It just
created the appearance of fixing an issue that the Republican Party
perceives, but only to those people who have absolutely no idea how the
government works. So what happens? Odds are the Republicans will stay true to
word in the House. They will shoot this down. It will not go anywhere. They will call it a fake
border bill. My guess is most of them haven't read it. Then, once that is done, my guess is that Schumer
and the Democratic Party as a whole will use that for ads because it will list the things that are
in the bill alongside the Republican who voted against it.
This person isn't serious about border security.
That's the plan.
And it certainly appears that it's going to work.
Now a question that came in, you know, is this a good bill?
Not in my opinion.
I actually think it goes too far and gives the president too much power.
But at this point, it's not actually about the legislation.
Schumer knew what he was doing here. This is a political ploy. He knew the Republican party
would rather be able to scare people who are lesser informed with, you know, the idea of the
immigrants are coming to get you or whatever, they would rather have that tool than actually
fix the problem. Schumer was basically banking on Republicans being disingenuous fear-mongers.
Lately that seems to be a pretty safe bet and I would like it noted it certainly appears
that it's going to pay off. But no, in my opinion, this is actually a horrible bill.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}