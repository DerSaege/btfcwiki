---
title: Let's talk about Trump and rapidly changing views....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=q1LODa73K-8) |
| Published | 2024/05/23 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Trump.
We're gonna talk about Trump and his rapidly evolving views
on a certain topic because he was asked about something
and he said, well, I've got a policy coming on that.
And then he changed his mind, I guess.
So what we'll do is we'll go over the chain of events,
quotes in question and then just take a look at it from there. So what happened was he was asked
a pretty simple question. Do you support any restrictions on a person's right to contraception?
That's a yes or no question to be honest, but it's Trump. So of course that's not what you got.
What you got was, we are looking at that and I'm going to have a policy on that very shortly
and I think it's something you'll find interesting, you will find it very smart, I think it's a smart
decision." He was uh pressed a little bit on it and asked about certain types of contraception
and he said, things really do have a lot to do with the states and some states are going
to have different policy than others.
OK, I mean, that would indicate that there
is a spectrum of restrictions that
would be in place because there will be different policies
and that he's OK with the states restricting it
in the way that they choose.
That seems to be what was said.
Shortly after he got a whole bunch of pushback on it, Trump said,
this is a Democrat-fabricated lie.
Misinformation slash disinformation, all caps.
Just as a reminder to everybody, the misinformation, disinformation
in this case that is a Democrat-fabricated lie
is on video.
So I find it hard to believe
that's what
is at play there. I didn't see
you know, a Democrat making him talk.
And then he goes on to say,
I do not support a man on birth control and neither will the Republican Party.
All caps.
I mean
that's not true
Because you have Republicans, currently, who are pushing for it.
And you have Republicans standing in the way of legislation that would guarantee it as
a right.
We saw that recently.
We covered it on the channel.
This goes back to why we call it reproductive rights.
Because they lied.
It's not just about abortion.
They made that up to manipulate low-information people.
If you are being incredibly generous with this chain of events, and you're looking
at it, and you're reading what was said, it seems like the most generous interpretation
of this chain of events that you can give to Trump is that he didn't know what contraceptives
were.
Plug that in and then go back through the quotes.
His first answer, we'll have a policy on that soon, is because he didn't know what it was.
That is the most generous interpretation of this chain of events, is that he was ignorant
to the subject matter and didn't want to say that.
The less generous interpretation is that he wouldn't advocate for it, you know, but he
wouldn't stand in its way because it would be up to the states. People need to
acknowledge that there were a whole lot of people in 2016 that did not believe
Roe was going to be overturned. Just like there's a whole lot of people who don't
believe what's in this case on video. It's worth keeping these things in mind.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}