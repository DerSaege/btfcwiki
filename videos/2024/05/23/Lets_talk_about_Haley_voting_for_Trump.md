---
title: Let's talk about Haley voting for Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=n61WS5gC-RI) |
| Published | 2024/05/23 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about the curious case
of two Haley's because she made an announcement
and we're going to go over some of her reasoning
for that announcement and then we're gonna go over
some other things that might better inform anybody
who is weighing her announcement.
Along the way, we will end up talking about Trump, because if you missed the news, Nikki
Haley has indicated she plans on voting for Trump.
Part of her reasoning for that is, quote, as a voter, I put my priorities on a president
who's going to have the backs of our allies and hold our enemies to account.
She goes on and she also says that she wants, quote,
a president who would support capitalism and freedom,
a president who understands we need less debt, not more debt.
So I'm going to flip through some other quotes real quick.
You'll never guess who they're from.
These are some other things that Haley has said.
President Trump has surrounded himself with the political elite, but they are the same
political elite that have spent like drunken sailors.
They've raided Social Security and continue to waste taxpayer dollars.
Everybody talks about the economy when Trump was president.
He put us eight trillion in debt in just four years.
Another quote, if you mock the service of a combat veteran, you don't deserve a driver's
license, let alone being President of the United States.
Another, we can't have someone who sits there and mocks our men and women who are trying
to protect America.
I mean, it certainly seems that she believes, at least at this point, that, I mean, I don't
I don't know how she could think he would have the backs of our allies.
She certainly appears to believe that he doesn't even have the backs of our troops.
And here's another when it comes to, you know, facing down our enemies and all that stuff.
He was completely wrong because every time he was in the room with him, meaning Putin,
he got weak in the knees.
We can't have a president that gets weak in the knees with Putin.
We have to have a president that's going to be strong with Putin in every sense of the
word.
And as far as freedom and all of that stuff, he sided with a thug that arrests American
journalists and holds them hostage.
And he sided with a guy who wanted to make a point to the Russian people.
challenge me in the next election or this will happen to you too. It is
unconscionable to me that a candidate would spend 50 million in legal fees. It
explains why he's not doing many rallies. He doesn't have the money to do it. It
explains why he doesn't want to get on a debate stage because he doesn't want to
talk about why he's doing it. And then the one that I think is probably, probably
one that might resonate with people the most, I don't know if he waited too long,
but I'll tell you Donald Trump is everything we hear and teach our kids
not to do in kindergarten.
And then back to the whole, you know, our enemies thing.
That means Donald Trump is going to side with a thug, where half a million people have died
or been wounded because of Russia invading Ukraine.
That means Donald Trump is going to side with a madman.
That's quite the shift.
That is quite the shift.
People's positions evolve over time.
It happens.
But this is quite the evolution.
after the moment when it would have made sense to make this announcement. You know
because she had that retreat with her donors. Would have made sense to do it
then. Doing it now, I mean that's a little weird I'm gonna be honest. Now it
it is unique because she didn't really seem to totally endorse him. I feel like
Like those people, the one out of five Republicans that have been voting for her, I feel like
you were voting for her for not her reasoning for voting for Trump, but for all of those
other quotes.
Anyway it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}