---
title: Let's talk about 2 student debt relief items and Biden....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=je_viee5uYA) |
| Published | 2024/05/27 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about President Biden
and two news items, two different news items
that fall under the heading of student debt relief news.
The first we'll just go through real quick.
The second is where the real news is.
Okay, so the first thing is another small batch
has gone through.
This is $7.7 billion worth for roughly 160,000 people.
This particular program is focused on IDR plans,
income-driven repayment plans.
That's who's gonna benefit from this.
You know, since the first big attempt didn't go well, the administration has been doing
small batches.
The total is now up to $167 billion, impacting 4.75 million people.
Okay, now on to the big news.
The first big plan did not work.
The second big plan was announced.
has made it through the public comment phase. So now they have to review the
comments and move on from there. This is another attempt at a very large plan.
Now the issue is that it will almost certainly be challenged. Republican
lawmakers and Attorneys General have gone out of their way to go ahead and voice their opposition to it.
So this will probably also go to the Supreme Court.
Now, this is using a different law.
So it's not a duplicate case.
Basically what the Biden administration did was they sent up the first batch,
that first program, when it got shot down, they tailored the second attempt to stay within what the court said using a
different law.  So they have a better chance, but with this Supreme Court, you don't really know.
And there is a lot of Republican opposition to helping people, I guess, to student debt relief.
I'm sure it has something to do with bootstraps.
So if everything goes according to plan for the Biden administration, the larger plan,
the second attempt at the big relief, should roll out and fall.
Should roll out and fall.
Now if everything goes according to plan, it should be enacted.
The issue here is that a different president might stop it.
So there's that.
But again, before anybody starts celebrating on the second attempt, you have to wait and
see what happens in the courts because this is definitely going there.
My understanding is that a whole bunch of Republican groups already have their suits
drawn up.
So anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}