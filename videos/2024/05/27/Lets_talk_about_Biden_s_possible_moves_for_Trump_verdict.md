---
title: Let's talk about Biden's possible moves for Trump verdict....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=QI9z22z8_dw) |
| Published | 2024/05/27 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Trump and Biden, Trump's busy week and how
Biden is likely to respond to it.
Because as the proceedings in New York move forward, that's obviously
becoming the question, right?
How is this going to impact the campaigns?
And more importantly, what is Biden going to do?
Obviously, it depends on what happens, you know, if Trump is found guilty, the talking points for the Biden campaign are
incredibly obvious.
But if it's a hung jury or Trump is acquitted, they're still there, the talking points are still there, they're just not
as obvious.  as obvious, and they require a little bit of finesse, a little bit of subtlety.
If that is what occurs, I would imagine you would see the Biden administration, the campaign,
come out very quickly and be like, hey, look, we respect the system, the system works.
This is what the jury determined.
This is what the system discovered, and we respect that outcome, and that would be the
end of it for a day or two.
And they would let people sit with that.
Then they would probably come back a little bit later and say something to the effect
of, look, the system works.
Everything that Trump said, trying to cast doubt on the system, shows it's not true.
Shows it's not true because the system works.
Then let people sit on that for a day or two.
then come back and say, hey, you know, since we are now all in agreement that the system
works and never ever makes errors, what about all those other cases?
What about the other determinations that have already occurred?
What about E. Jean Carroll?
And they would shift it to that.
But they would build on the idea that the system functioned, and it functioned for Trump.
And everything that he said was untrue when it came to his rhetoric about the system.
And they would lean into it in that way.
Now I totally understand how a lot of people watching this would view that rhetoric.
for a whole lot of the US it would play well so that's probably going to be their
move. There are other routes that they could go with it but that seems most
likely. So again closing arguments are supposed to start tomorrow and then
from there, well, it's in the hands of the jury. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}