---
title: Let's talk about Schumer playing Trump and the GOP....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=cS0rlmuf-TA) |
| Published | 2024/05/27 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump and Schumer
and how Republicans, Republican voters,
are less than thrilled with the performance
of Republican senators and Trump himself.
And we're just gonna kind of go through a message
and explain something in a little bit more detail.
We've talked about it,
but we were focusing on one particular aspect of it and maybe it's a good idea to step back and take
a look at the whole thing. So here's the message. Can you explain exactly how the Dems played
Republicans on the border bill? I was at a memorial weekend fishing trip with my family. Most of my
family are Republicans. They were all saying that Trump got played by Schumer and that Republicans
in the Senate were too dumb to even understand what happened. Then they said
Schumer was right, he's playing chess, and the Republicans were playing checkers. I
don't get it. Schumer brought it up for a vote twice. Republicans voted against it.
How did he play them? By giving them what they wanted, and why won't Republicans
be able to get the bill later. Okay, so the border bill itself was a negotiated
bill. It was a bipartisan thing and understand the Republicans won that
negotiation. Most people watching this channel, if you actually sat down and
looked at this bill, you would be adamantly opposed to it. It is very much
a Republican immigration bill. Now the reason the Democratic Party gave them so
much was because really it seemed like it was going to end up tied to Ukraine
and they wanted that package to go through. The quote that keeps getting
attributed to Schumer and to be honest I don't even know if he said this but
there are so many Republicans saying that he said it even if he didn't he
might as well have. He reportedly said, we are playing chess, they are playing
checkers, and we got our Ukraine bill, and we are in much better shape on the border
than we were three months ago. That is a 100% accurate statement. So it was
negotiated and it was a bipartisan thing. The Republicans came out on top on this
bill. It gave them pretty much everything they wanted. The Democratic Party got
their Ukraine bill through. Schumer brought this up for a vote. Why? Because
Trump said not to do anything about the border. He wants to be able to run on the
border. If they pass incredibly comprehensive Republican immigration
legislation, he can't do that anymore. So he told the Republican people in
Congress to vote against it. He didn't want it to go through. And for whatever
Whatever reason they did, twice, it has not gone through.
The reason Schumer played them is because he brought it up and let them vote it down.
This bill contains a lot of things that Republican voters want.
They really want it.
They've been told they were going to get it since 2016 when Trump was running the first
time and they had the chance to get it.
And Biden, honestly, I think Biden would have signed it if it went through.
But they voted against it.
So Schumer and the Democratic Party, they're going to be able to use this.
They're going to be able to pull out the different pieces about how it was going to be harder
to get asylum and just all kinds of little things and use that against any Republican
that is running on this issue.
The Republican Party had a choice to make.
could get what they wanted or they could continue to fear monger with it and they
chose to leave it open to give them something to run on. Now this next part
and this is kind of the big part that we didn't talk about before I don't think,
why won't Republicans be able to get the bill later? Because it was a bipartisan
thing and because the Democratic Party was really concerned about something
else at the time, they got a whole lot. The Republican Party got a whole lot in
this. The only way the Republican Party is going to get this deal again is if
they have a filibuster-proof Senate, control of the House, and the presidency.
because the Democratic Party will block it, because it's not a Democratic Party
bill, not really. You have some Democrats that would support this,
but not many, but not many. And if it was a situation where, let's say the
The Republicans have the White House and the House.
If it goes through the House, the Democratic Party, in theory, would kind of close ranks
and block it.
It is unlikely the Republican Party will ever get a deal that good again anytime soon.
they voted against it because they want
they want to be able to scare
middle-aged middle-income white people
that's really what it is
they want to be able to fear-monger about it
and you know they're saying that it didn't meet everything that they wanted
I mean that's probably true but I mean
when it first came out, they were referring to it as their bill.
And it is definitely
not a Democratic Party bill.
This is one of those rare instances
where the Democratic Party actually played hardball.
And hopefully, based on the results,
maybe they'll start doing it a little bit more.
Because
you have
Republicans at family functions talking about how Trump got played by Schumer and how the
Republicans in the Senate aren't that bright because they don't even understand what happened.
My guess is that Republicans in the Senate believed that their constituents weren't
bright and they wouldn't understand what happened. I don't think that's how it's
going to play out because for this to work for the Democratic Party, all they
have left to complete this plan and play true hardball is to use this vote in ads
during the campaign. You know, candidate Jack Johnson said that he wants to be
tough on the border, but he voted against this. Would have provided this kind of
security and you know X number more border patrol and all of this stuff.
That's the only step they have left. So I would assume that they would follow
through with it since they already got through the hard part. But at the end the
Republican Party chose to to hang on to that talking point so they could kick
down during the election when they they could have got it. I mean it was
is definitely something they could have gotten the Senate and they could have
moved it through the house and I, I do, I believe Biden would have signed it.
So, anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}