---
title: Let's talk about the conclusion of the Trump-Kennedy show....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Ox7DCwZgFnA) |
| Published | 2024/05/27 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, Ledzebo again.
So today we are going to talk about the conclusion
of the Trump-Kennedy show from this weekend
and just kind of go over where things stand now
and the statements that were made after the fact
and just kind of run through everything.
If you missed it, both Kennedy and Trump
were trying to pick up some votes.
They were trying to increase their base,
and they spoke to the Libertarian Party.
Kennedy went first.
He spoke to issues that they truly care about,
and was relatively warmly received.
Trump went the next day,
and was booed and heckled pretty consistently
throughout the entire ordeal.
Um, now following that, it was determined that Trump didn't
fill out the proper paperwork to really even be considered.
Kennedy picked up some delegates in their convention.
Uh, I think 19 could be off one or two there.
Uh, not a huge amount, not enough to win by any, by, by any means.
But there was some genuine support for it.
Afterward, Trump released this statement,
The reason I didn't file paperwork for the Libertarian nomination, which I would have
absolutely gotten if I wanted it, as everyone could tell by the enthusiasm of the crowd
last night, was the fact that as the Republican nominee, I am not allowed to have the nomination
of another party.
Regardless, I believe I will get a majority of the Libertarian votes.
Junior Kennedy is a radical left Democrat who's destroyed everything he's touched,
especially in New York and New England, and in particular as it relates to the cost and
practicality of energy.
He's not a Libertarian.
Only a fool, all caps, would vote for him.
This does not seem like something that would endear him to the Libertarian Party, as everyone
could tell by the enthusiasm of the crowd last night.
I mean, there was enthusiasm, don't get me wrong.
It just wasn't very supportive.
But don't worry, he absolutely could have got the nomination if he wanted it.
He would have won that race if he had worn the right shoes.
Okay. So Kennedy said, what an unexpected honor to wake up this morning to a groundswell in the
libertarian party seeking to nominate me. I mean, that's a bit of an exaggeration,
but he did have real support. So I would have accepted the nomination if offered because
independents and third parties need to unite right now to reclaim our country from the corrupt two
party system.
And then he went on to say, while we may not agree on every downstream issue, our core
values of peace, free speech, and civil liberties make us natural allies.
I mean that, again, that is something that is a little bit more in tune with the libertarian
party.
But it certainly appears that the libertarian party is going to be running their own candidates
and pursuing things their own way which is probably a really smart move on the
part of their organization. So given the situation I would expect Kennedy to
still attempt to reach out to this particular voting block. I don't
think it's over for him because I think he sees a window.
As far as Trump, it's Trump.
My guess is if the media picks up on how poorly he was received at the libertarian convention,
my guess is that Trump is going to kind of go on the attack against libertarians in general
because he'll be embarrassed by it.
I don't know that the media is going to cover this because they don't generally cover a
lot of third party stuff, but if it's covered and it's embarrassing to him, he'll probably
start taking swings at the Libertarian Party, which would be a big mistake on his part.
But just based on his past behavior, probably what he'll do.
Now if there's no coverage, or it doesn't seep into the information silo that he's
a part of, he'll probably just continue to say that the libertarians love him and go
with it from there.
But if that talking point of his begins to get questioned, he'll probably respond because
he normally does.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}