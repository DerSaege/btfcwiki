---
title: Let's talk about 2 events over there and what might change....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=94aHmqZ6Ddg) |
| Published | 2024/05/28 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about two events.
We're gonna talk about two events that occurred over there
and what it means for the wider situation.
One of the events is getting a whole bunch of coverage
because it was larger and there was footage of it.
The other event comparatively was a smaller event,
but when it comes to foreign policy,
it might end up being the thing that sways country's actions.
Okay, so the first event.
Israel conducted a strike.
My understanding was that they were going after two people.
At time of filming, I do not believe
there is an accurate count.
Suffice it to say, it was more than two people.
It was bad in any way that it could be bad.
It was so bad that Netanyahu came out and said it was a
tragic mishap.
If you are doing something that pretty much every expert in
the world said not to do, I don't know that you get to call
it a mishap.
The other event was Israeli and Egyptian troops exchanging fire. They shot at each
other. The West is not going to be okay with that. The West is not going to be
okay with that even if even if it is de-escalated and a time of filming it
seems as though it is being de-escalated. The West will not risk
Israel and Egypt going at it. You might start to get more pressure from
countries that have been big on rhetoric and symbolism but haven't really applied
any pressure of their own, they may start playing bad cop to the US good cop.
I would not expect economic pressure, but it wouldn't surprise me either.
I would watch developments in Europe. I would watch developments in Europe because
they may take a tougher stance on this and people are going to want to know why.
The Suez. The West is not going to be okay with that in any way shape or form
being jeopardized. That's what it's about. Now the country will probably frame it
around the ICJ or the strike. Anything except for that because nobody's going
want to say that, but it is incredibly unlikely that the West doesn't do
anything, that there isn't some kind of ratcheting up of pressure, unless this
was just a completely freak incident.
Now, saying Israel and Egypt going at it, you had two groups of people right then.
One laughed, to y'all I would remind you that the Egyptian military of today is not the
Egyptian military of back then.
They're not rocking T-55s and T-62s anymore.
And then there was probably a group of people who thinks, well, maybe it'll help, it won't.
It will not.
Israel is not going to want to fight on their own dirt, and they're not going to want the
Palestinians behind them.
It will not help.
So hopefully that is being avoided.
I know that the two countries are in contact, so that's a hopeful sign for de-escalation.
But again, foreign policy is about power.
A whole lot of power coupons flow through the Suez.
So you might actually get some kind of movement from countries that were just kind of following
the US lead.
They may start to do something a little bit more than just express their displeasure.
So this is a situation to watch.
Again, they're not going to say that it's over the canal.
That's not what they're going to say.
But if you start to see Western nations, particularly those in Europe, really start to ratchet up
pressure, that's what they're concerned about.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}