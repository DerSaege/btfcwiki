---
title: Let's talk about the wisdom of McConnell....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=gV5HEAM4JSE) |
| Published | 2024/05/28 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there internet people, it's Beau again.
So today we're going to talk about the wisdom of Mitch McConnell
and how the Republican Party really didn't seem to learn much from it.
Because he talked about something back in 2022
and he said it was going to be an issue and it turned out it was in fact an issue
And it looks like the Republican party is setting up to make the same mistakes again.
Back then, he talked about what he was calling candidate quality.
To put it into the channel's terms, he was saying you can't win a primary without Trump,
but you can't win a general with him.
And it seems that in a lot of states that the Republicans need, if they want to get
control of the Senate, they're making the exact same mistake again.
When you look at people like Kerry Lake or any of the others that have a high profile
nationally because they're linked to Trump, a lot of them don't really have
the support of the people who actually have to vote for them. They have a
social media presence, they have a TV presence, but they're not really present
where they need to be and the voters kind of recognize that. Now to my
knowledge, McConnell hasn't come out and said, hey, we're making the same mistakes
we made in 2022 all over again. But I feel like he's probably thinking it
because it is the exact same scenario. A bunch of rich people, a lot of them not
from the state, they don't have the the same connections, and their main selling
point is that they're linked to Trump and you know based on polling maybe that
would be a good thing but we've talked about how I feel about the polling right
now. When it comes to Lake in particular she looks like she's going to win the
primary. If that happens I mean I don't like to make predictions about
elections because you never know who's going to show up and all of that stuff.
But I feel like Senator Gallego, I'm sorry, Representative Gallego probably
stands a pretty decent chance. He definitely seems to have more support
and he is not linked in the same way that she is to Trump. He's not linked that
way to bite. And in many cases in the swing states, both presidential candidates
might be kind of a drain on the senatorial candidates. So it's something
to remember as we're looking at everything shaping up. McConnell is
McConnell, but he was politically savvy. And this is the exact same issue he
identified when it didn't go the Republican's way.
It's something to keep in mind.
It's something to keep your eyes open for because it certainly appears that they didn't learn much.
They're just still banking on the Trump endorsement and that social media presence.
But that social media presence is an echo chamber, so it may not be representative of the whole state.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}