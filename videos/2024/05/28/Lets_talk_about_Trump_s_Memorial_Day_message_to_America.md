---
title: Let's talk about Trump's Memorial Day message to America....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=uiHcLlL0-_8) |
| Published | 2024/05/28 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about Trump.
We're gonna talk about Trump
and his wonderful Memorial Day message.
And we're gonna talk about the surprise
that it created in a lot of people
and why that surprise shouldn't exist at all.
And I'm going to talk about one particular instance
that can't be downplayed, it can't be argued, it can't be debated that
should put this conversation to bed forever. There shouldn't be any
surprise when Trump does something that is
disrespectful or denigrates the troops. It
shouldn't be a surprise ever again.
Okay, so I'm going to read part of his message.
Happy Memorial Day to all, including the human scum that is working hard to destroy our once
great country and to the radical left Trump-hating, and it just goes on.
I'm not going to read all of it because honestly, this appears to me to be something that is
very likely to end up in yet another defamation case.
I'm not going to go through it all, but let's just say he had some unkind words to say about
Eugene Carroll.
And people are shocked, because there's no mention of the troops.
There's nothing about remembrance.
He didn't accidentally celebrate Veterans Day, which is an entirely different holiday,
for a different purpose.
He just kind of skimmed over it.
This isn't surprising.
This is who he is.
And you can point to dozens of instances where he's done this, but most times his base will
debate them.
Oh, he didn't really call them losers and suckers.
They made that up.
Fake news.
So we're going to go to the one instance that is inarguable, because it's something he
said in public.
When he was in Austin, he said, when I was in charge, in 18 months, we didn't lose one
American soldier.
In 18 months in Afghanistan, we lost nobody.
Okay, that never happened.
And I don't mean that in some way where, you know, what he said is close and it's debatable.
I mean, it never occurred under any president.
That never happened.
There was never an 18-month period where we didn't lose somebody in Afghanistan.
It did not happen.
It is a factually untrue statement and there is no arguing that.
It's not even close.
So with that in mind, what are your options?
Either he lied knowingly, downplaying American lost for political points, or as commander-in-chief,
he didn't care enough to know, or maybe he forgot that people were lost and he created
this random period of time in which he believed that nobody was lost.
There is no explanation for that statement that doesn't completely demonstrate where
he stands on this issue.
He doesn't care.
Either he intentionally lied or he didn't care to know the truth.
That's what happened.
His statements about these days, they're always kind of like this.
He goes out there and he says that he supports the troops.
He goes out there and says that he cares about all of these issues related to veterans, related
to those who were lost, related to active duty, but only when it's time to get political
points. If it is beneficial to say he went 18 months without a loss, he'll say
it. And it's either because he's intentionally lying or he doesn't know.
And honestly, I don't know which is worse. The key point is stop being
surprised when Trump downplays, forgets, disrespects, or denigrates those lost.
It's who he is.
It's what he's always done.
There is nothing surprising about this.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}