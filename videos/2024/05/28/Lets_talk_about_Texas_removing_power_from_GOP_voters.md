---
title: Let's talk about Texas removing power from GOP voters....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ym9gbDpewwc) |
| Published | 2024/05/28 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there internet people, it's Bo again.
So today we are going to talk about Texas
and how the Republican Party in Texas is making a move
through supporting something called a concurrent majority
that will alter Texas politics.
And it's gonna be framed in a way
that basically appeals to rural GOP voters,
GOP voters, but it's not for their benefit. It's just going to be packaged in that way.
The idea is to make sure that candidates for statewide office in Texas, not just do they
have to win a majority of the votes, they also have to win a majority of the 254, I
think, counties.
So it says, the state legislature shall cause to be enacted a state constitutional amendment
to add the additional criteria for election to a statewide office to include the majority
vote of the counties with each individual county being assigned one vote allocated to
the popular majority vote winner of each individual county.
So basically they're creating an electoral college for the state of Texas.
And to most Republicans at first glance, this is something that you're like, ha, those
people in the cities, those Democrats, they won't be able to outvote us.
That's not what it's for, just to let you know.
Because let's be real, Texas is a red state, right?
It's already a red state.
It's already that way.
So who is this aimed at?
You.
people who think this is going to benefit you, it's aimed at you because if this was
to go into place, okay, the Republicans, they don't have to worry about the Democratic
Party anymore.
They also don't have to worry about independence.
They also don't have to worry about getting your approval or working for you because it
doesn't matter.
They're going to win the majority of the rural counties because those people who are
just traditionally bound to vote for a Republican regardless of what they do, well, they always
will.
So what's happening is a situation in which the Republican Party is setting it up so they
can rule rather than represent you.
They don't have to work to get your vote.
They don't have to do anything for you.
They will automatically win.
It's not aimed at Democrats.
It's aimed at making sure you don't have a voice as a rural Republican.
they won't have to work for you, they won't have to take your calls, they won't have
to address your concerns because they're going to win no matter what.
That's what this is about.
Don't let them repackage this.
It's known as Proposal 21.
It's about making sure that the minority in the state wins.
It's about making sure that a popular vote doesn't really matter as long as the Republican
Party can keep a majority in a bunch of little counties.
That's it.
That's what it's about.
Once they get this in place, you don't matter.
You will do what you're told, you will obey, and well, there's nothing you can really do
about it except maybe move to California.
Anyway, it's just a thought.
have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}