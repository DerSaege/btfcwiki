---
title: Let's talk about a rabbit hole with foreign policy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ffRmh49HrEs) |
| Published | 2024/05/20 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about how deep is too deep
when it comes to an understanding
of foreign policy. Meaning how much does the average person really need to know
about it.
We're going to do this because we've got a question about it,
got a message about it, and because it reveals
something about domestic policy
well because the two are inextricably tied. Okay so it starts off with Kiora
from Eotera. I meant to look that up before this video. Okay and then says you
know I knew very little about the actual functions of foreign policy beyond Iraq
was about oil. That content in particular has been super valuable to me
especially in these recent years of international conflict and the accompanying images of horror
and pain from around the world."
And then it goes on to say that YouTube's algorithm has done its thing.
So this person is now seeing other videos about foreign policy and they're recommending
books that even the people recommending them are describing as, quote, not fun and incredibly
dense. And then it goes on to say what you've taught me about foreign policy
has been a strange source of comfort these past years the fact that these
things do tend to repeat themselves that the cycles are somewhat understandable
if you look closely and crucially that there are people doing good work in the
world at almost every level outnumbered as they may be. And then this is all to
say, at what point does the foreign policy rabbit hole go too deep for
civilian to have to worry about? I'm deeply curious about these books and all
the new understanding I could gain, but it also feels like opening the
Echronomicon and maybe I won't like what I find. Okay, so the deeper you go, you're
going to notice two things. First is that the systems are entrenched. Entrenched.
And yeah, there's a whole lot in there that you're not going to like. Your
foreign policy is nowhere near as horrible as, you know, American foreign
policy. There's not as many horrible things to discover, but most countries
have foreign policies that their citizens would rather not know about in
most cases, and those systems are entrenched very, very hard to change. And
and the more you learn about it the more you realize that is true.
So you hit a level
at some point where you realize you're not going to be able to directly
influence these items
because they are too entrenched.
And that is, I mean, it's not great information,
but it informs something else, which does matter.
Which does matter.
And that's that foreign policy is inextricably
tied to domestic policy.
You can't get a deep systemic shift when
comes to American foreign policy, because that foreign policy is in support of domestic
policy in the systems in place here.
And that's what is most important to those in power.
Our foreign policy is directly tied to domestic policy.
So if you want a deep shift in foreign policy, you have to achieve deep systemic change in
domestic policy first. Otherwise you won't get it. This is why when we've
talked about this in the past I've been like yeah you know if this is going to
be your thing and you're going to try to shift American foreign policy I mean
you're gonna lose way more than you win. Until the system in the US is more
cooperative rather than competitive. Foreign policy is going to look the way
foreign policy looks now and it is incredibly important to understand that
and no knowledge is wasted. You know you're curious about it, my advice is
yeah read about it, but yeah there's going to be things you don't like and a
A lot of the information is information that can inform other things, but it's not going
to be directly useful unless you decide to go into that as a field.
But the most important lesson you'll learn is that domestic policy has to shift first.
And there's a lot of debate about what I'm about to say next.
And I enjoy watching the debates because it's a good discussion, but my personal opinion
on this is that that change to a more cooperative system, a more cooperative society, that deep
systemic change that most people watching this channel really want, it has
to start here. It has to start in the more powerful countries because most
smaller countries, if their domestic policy shifts to a more cooperative
society, well it doesn't change foreign policy. And if it is a country that has
enough resources to shift foreign policy and it puts those resources in jeopardy
concerning US interests. Oh, they're going to need some democracy. We'll invade
them. So it has to start here. And again, that is a subject of a whole lot of
of debate and that's my opinion. At the same time I can see the arguments coming
from the other way saying that if it starts in the smaller countries first
then because of how in some ways because of how extractive the US is well it
would shift domestic policy in the US, and I can see it, it's just, I just feel
like it would work the other way. So yeah, there's going to be uncomfortable
stuff. Yes, you're going to realize that a lot of it is, it's entrenched, it's
intractable. The information itself, learning about the different systems,
unless you're going into it for a field, not going to be super useful directly, but
no information is wasted. No knowledge is wasted. It can always be applied in a
different way. And the, to me, the most important lesson to learn about getting
into the deeper aspects of foreign policy and how all the different
international systems really function when you get down to it is that they're
not good, but they're also not going to change as long as we have a system that
is competitive in nature, domestically, because we need the resources, we need
that power to maintain the system that we have. So if you want to change that, we
have to have a less authoritarian and more cooperative society within the
United States first. That's where the change is going to come. But yeah, I have
never read a book that I didn't pull something out of that was valuable. So I
mean my advice is it's a not fun incredibly dense book. Read it. There's
probably a lot of information. You may not use it directly, but you can use it
for something else. But yeah, your worry about it being a rabbit hole too
deep? I mean, it is. You will get so deep that the information that you learn
learn isn't directly applicable to anything other than a motivation to help that systemic
shift occur.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}