---
title: Let's talk about Trump backing away from one of his wins....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=eJRIBd-s3vY) |
| Published | 2024/05/20 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about Trump and how Trump is making a strategic
decision to ignore something that he did and not use it as a talking point.
Although, it would probably help him with a lot of people, with a lot of voters.
And he has to do this because he is concerned about RFK Jr.
Trump lately has been just kind of going off about saying that they're going to defund
any school that has a mask mandate or they have a COVID-19 vaccine mandate or anything
like that.
They're not going to get a single penny and he has to do this because he cultivated a
base that is very susceptible to a lot of wild theories and that base, in a lot of ways,
to RFK Jr. when it comes to the topic of vaccines. So Trump has to kind of prove
his credentials in this regard, which is weird because, you know, I answered a
whole lot of the questions about it when it was happening. And one of the
biggest ones that came up was, you know, they just they came out with them so
fast. And for whatever reason that worried people when really that was a
good thing. The thing is if you have that concern you have to look at Trump
Operation Warp Speed. It was a public-private partnership that worked
to get the vaccines out as soon as they could. That was him. When it comes to our
public health issue, it was like the one thing that he did where even though, yes,
I have a litany of complaints about it, it was, as far as Trump goes, it was one
of the only things he he did that actually there was an actual attempt to
benefit the nation and he's having to walk away from it to keep this portion
of his base that would be energized by RFK jr. He can't claim that because if
he does all of the complaints about it, well they get directed at him. So he has
to pretend like he didn't do that and spout off a bunch of rhetoric to try to
appeal to this base. That's why that topic is coming up again. A bunch of
people have asked why he's talking about this. That's why. Because he is losing
that portion of his base to RFK Jr. Now realistically, I hope that this isn't a
significant number of people just for, you know, just on general principle. But
But the way Trump is acting, it certainly appears that he believes it's a significant
amount of people because he is really out there catering to them and he is ignoring
something that, I mean, when it comes to the public health issue, his response, the only
word for it is botched.
It was bad, bad.
And the one thing that seemed like a genuine attempt he can't claim credit for, because
his base won't go for it.
They'll view it as a bad thing.
That explains at least that portion of his unusual behavior and rhetoric lately.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}