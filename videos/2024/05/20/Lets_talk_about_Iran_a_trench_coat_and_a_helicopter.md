---
title: Let's talk about Iran, a trench coat, and a helicopter....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=V4iaFCsSons) |
| Published | 2024/05/20 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about a helicopter.
Three governments in a trench coat, Iran,
recent developments, and your questions.
Because there were developments,
and immediately, there was a bunch of speculation.
So we're going to kind of go over what occurred,
and then run through the changes that will occur domestically, internationally, and then
we'll talk about the speculation.
Okay so what happened?
A helicopter carrying the president, Raisi, and the foreign minister went down.
Does not appear that they made it.
So the immediate questions are about what happens next.
The vice president of the country will take over.
That's what should happen is the VP takes over.
I actually can't remember his name right now, but he was an officer during the Iran-Iraq
war.
He was in the Revolutionary Guard.
I want to say he was a doctor, maybe.
He has ties to Russia, pretty friendly with them.
No big changes there.
The thing that people need to remember about all of the international questions is that
the president of Iran is not the president the way we think of a president.
The president there is the head of government, not the head of state.
That is the supreme leader.
I always make that joke.
Anytime we talk about Iran's government, it's three governments in a trench coat.
The president is not the head.
So internationally, probably not a lot of big change is coming.
It's not going to be drastic.
It shouldn't be anyway.
And realistically, the foreign minister is probably more important than that.
That's probably a more important development.
My understanding is that he was very much somebody who represented Iranian interests
and was a hardliner, but wasn't unreasonable.
That's the read that I've gotten.
Now, domestically, the question's just going to cause big changes at home because there
is a lot of internal dispute.
With what most people are talking about, as far as the generational issues between younger
people wanting a more liberal government and older people not, this alone wouldn't cause
that because, again, the president isn't the president the way we think about it.
Could this prompt some other catalyst for change?
Yeah, but it could, it doesn't mean that it will.
The thing that will happen domestically that is probably the most important development
the political jockeying that is going to occur among the powerful players in Iran.
Because while Raisi was not supreme leader, there were a lot of people who believed he
was going to be the next one.
So there's kind of an open position now.
So there is a decent chance of the political games inside of Iran getting turned up a notch.
And then the question that always comes up when something like this happens, and I want
to be clear about this, there is precisely zero evidence of this.
But it came in a lot just because of speculation.
this a foreign intelligence service? I don't think so. I would be very
surprised. Not because they wouldn't do something like this, but because there's
nothing, there's no gain because he isn't the president the way we think about him.
So that to me seems really unlikely. If it does turn out that this was
something nefarious, I would suggest it's something internal. The call was coming
from inside the house type of thing. Because with Raisi gone, there is more latitude for
other political factions inside of Iran to make a move. I don't, based off of what has
happened so far, I don't see a lot of changes coming from this. That being said, what happens
with the foreign minister's replacement, that's important.
If there was some other catalyst,
say a bunch of people go to the streets
or something like that, that could change something.
But overall, it doesn't seem like this is going
to cause a whole bunch of disruptions.
Anyway, it's just a thought.
Y'all have a good day.
Thanks for watching!
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}