---
title: Let's talk about the ICC seeking charges....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=4PxmvQUOuMg) |
| Published | 2024/05/20 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
Let's bowl again.
So today we are going to talk about
some breaking developments related to over there.
I know a whole lot of people are waiting for a video
on the helicopter situation with Iran that is coming.
It just seems like this is probably going to be
more pressing this morning,
so we're going to start with that.
Okay, so what happened? The chief prosecutor with the ICC has applied for
warrants. I feel like that part's going to be misreported. These are not warrants
yet. This is an application for warrants and it will have to go to a panel of
judges, I think three, first. And then the judge, the judges will decide what becomes
warrant if any what doesn't and kind of sort through everything. I will have a
link to the statement from the prosecutor down below if you want to
read through the narrative as well as the list of specific charges. When we
talked about this before said you'd see two sets. You would see a set for Hamas
leadership and a set for Israeli leadership. That is what occurred.
On the Hamas side you have three people in leadership that are named in the
applications. The conduct that is alleged is exactly what you would expect it to
be. You can go through and read it and it is related to what occurred on the
seventh. That is again exactly what was anticipated. For the Israeli leadership
there are two people Netanyahu and Galant, both named in the application, that
is a little different than what was anticipated. When we talked about it, so
what you would most likely see is stuff related to delaying, denying, hindering
aid and failure to exercise due care. The charges listed in the application, they
they suggest intentionality. The prosecution believes that they can prove
that that was willful. There are same core crime being alleged but it is no
longer a situation where they're going to have to demonstrate why a building
lost its protected status. The allegations are a little bit more than
that. If you read through it and you get to the narrative part, there's a section
in it where the prosecutor says that some of this came from statements from
the alleged perpetrator group. That is probably where the willful part came in.
Okay, so what happens from here? It goes to the judges and they determine whether
or not they become warrants. Now Israel is going to say that they don't fall
under ICC jurisdiction because they're not a signatory and that's true. However,
However, in practice, since 2015, the ICC has said that they have jurisdiction over
Gaza and other Palestinian territories.
That position has been held a while.
Israel is going to need a pretty heavy argument to overcome that.
The prosecutor said that, quote, they are free not withstanding their objections to
jurisdiction to raise a challenge before the judges of the court and that's what I advise
them to do.
That is prosecutor speak for test that at your earliest convenience.
It appears that the prosecution feels very comfortable with their case.
So that's what's going on.
When it goes before the judges, don't have that information yet, not a time of filming.
What happens and how the world responds, especially the United States, don't know yet.
If the warrants are issued, travel is going to become a significant issue for anybody
named in it.
So that's where everything sits at the moment.
Anyway, it's just a thought.
Y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}