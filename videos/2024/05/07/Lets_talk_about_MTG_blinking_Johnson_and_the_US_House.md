---
title: Let's talk about MTG blinking, Johnson, and the US House....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=pQJAWNpclao) |
| Published | 2024/05/07 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Marjorie Taylor
Greene and Speaker Johnson and the push
to potentially vacate him from the speakership
and how that may be changing.
There may be an altering dynamic there that's
worth going over because it certainly
appears that, well, Marjorie Taylor Greene just blinked.
Maybe she had a space laser in her eye or something.
But she had a two-hour meeting with Johnson, not something
that would typically occur if you were planning on trying
to get him fired, which is what she had said very, very
recently.
But coming out when talking to reporters, she said,
I have been patient, I have been diligent,
I have been steady, and I have been focused on the facts.
And none of that has changed.
Goes on to say, I just had a long discussion
with the speaker in his office about ways
to move forward for a Republican-controlled House
of Representatives.
We're talking to him again tomorrow
based on our discussion today.
Move forward.
Sounds like the speaker would still be the speaker.
Weird, because again, it wasn't that long ago, just a few days,
that she basically vowed to bring the motion to the floor
to get rid of it.
So dynamics have changed.
I feel as though this is a clear sign that Johnson has the leash on the Twitter faction.
And given the fact that it certainly appears that Johnson got an invitation to one of Trump's
little things and she didn't, he might have outmaneuvered him again.
have outmaneuvered Trump again. I feel like Johnson is definitely well
positioned and he has been making moves for quite some time to get him to where
he is at. He's also indicated that if they maintain the majority with the next
Congress there will be some rule changes and it certainly seems as though the
ability to oust a speaker so easily will no longer be present. Now, there wasn't a
clear statement from Green that she wasn't going to continue with her
motion, but if they're having another meeting it certainly appears that she's
trying to make a deal because at this point she can't win so it's not a good
move to go forward. She has now seen enough of trying to force votes and
stuff like that and watching them fail to realize that that doesn't actually
get you anywhere. It removes your political capital. So this, let's just say,
disagreement within the Republican Party might finally be coming to a close, at
at least for the time being.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}