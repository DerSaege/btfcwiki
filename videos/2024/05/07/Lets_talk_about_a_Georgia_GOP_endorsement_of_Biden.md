---
title: Let's talk about a Georgia GOP endorsement of Biden....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Ia6Sql8SQ7E) |
| Published | 2024/05/07 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about an incredibly surprising endorsement
of President Biden, an endorsement that I think most people probably would
have seen as inconceivable not too long ago, comes from a Republican, a
Georgia Republican, no less, former Lieutenant Governor, Jeff Duncan.
and it wasn't a, had nothing to do with his policies and it wasn't a just an
offhand comment. He wrote an op-ed for the Atlanta Journal Constitution for
those outside of the South. That's like the big paper. He said the GOP will
never rebuild until we move from the Trump era, leaving conservative but not angry Republicans
like me no choice but to pull the lever for Biden.
At the same time, we should work to elect GOP congressional majorities to block his
second term legislative agenda and provide a check and balance."
So it's obvious that Duncan is still very much the conservative that everybody knows
him as, and he doesn't actually want to see Biden's policies move forward.
He just views Trump as that harmful to the country.
And he goes on talking about Trump's entanglements, and then he says, most important, Trump fanned
the flames of unfounded conspiracy theories that led to the horrific events of January 6, 2021.
He refuses to admit he lost the last election, and has hinted he might do so again after the next one.
Now, outside of Georgia, I don't know what weight this is going to have. Duncan's pretty well-liked
white in Georgia, among normal conservatives, among people who would see themselves as Reagan
Republicans, who would see themselves as Bush Republicans. Those who, to most people watching
this channel are still far right wing and authoritarian but not as far right
wing or as authoritarian as MAGA, as Trump. In a lot of southern states, the
people who feel that way, who are that brand of conservative, it's not an
insignificant number of people. I don't know how many will be swayed by somebody
coming out and saying what Duncan said, but I mean it can't hurt. And again it's
important to know that throughout his op-ed he made it clear that he was not a
fan of Biden when it comes to, well really any of his policies, but that he
viewed Trump as somebody who was without a moral compass and he made it pretty
clear that he felt that would be devastating for the United States as a
Anyway, it's just a thought.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}