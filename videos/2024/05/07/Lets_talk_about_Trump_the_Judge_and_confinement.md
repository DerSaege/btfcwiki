---
title: Let's talk about Trump, the Judge, and confinement....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=8AstM56QSFc) |
| Published | 2024/05/07 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about Trump, New York.
We're going to talk about the former president's abilities
and wants, the advice that he might be getting,
and how that advice might play out.
Because there has definitely been a development
And it seems as though, well, it seems as though the judge up
there has had enough when it comes to the violations of the
gag order.
Skipping straight to the headline on this one, going
forward, this court will have to consider a jail sanction if
it is recommended.
And the subtext certainly reads to me as if the judge is
telling the state, if this comes up again, recommend it.
The thing is, the judge understands the gravity of this situation.
The judge doesn't want to be the person who put Trump in confinement.
And said as much, Mr. Trump, it's important to understand that the last thing I want to
do is to put you in jail. And there's more but it goes on to, at the end of the day I have a job
and part of that job is to protect the dignity of the judicial system and compel respect.
Your continued violations of this court's lawful order threaten to interfere with the administration
of justice in constant attacks which constitute a direct attack on the rule of law."
So, the main thing that's being discussed right now is whether or not Trump has the ability
to be quiet, is basically what it boils down to, for two reasons. One, because he doesn't think
about what he's saying at times and two because he's Trump and he in many ways
has it's like a toddler you tell it to go to their room they're going to stand
right outside the door you know try to defy it for defiance's sake that's where
the conversation is. It's probably worth considering that he might on some level
want to be placed in confinement because I am certain that some of his advisors
are telling him that it will energize his base and help him politically. And
the thing is they're probably right when it comes to energizing his base. I think
that's probably true. Those people who at this point are still
behind Trump and eager to be behind Trump, they probably will just feed into the narrative
and view it as something that was just totally unnecessary and out of bounds and so on and
so forth.
For his base, for that core base, the real supporters, the people who believe everything
that he says, I don't believe that that is the case with independence.
Independence that Trump needs, needs to win.
He cannot win with an energized base.
The votes are not there for him to win in that manner.
He has to get the independence.
I do not believe that they will view it the same way.
I think they'll probably see a timeline of events and just be like, no.
Trump has to make the choice.
Nobody knows what he's going to do.
Nobody knows if he has the ability to enact something if he decides it or he'll just be
Trump and trump it up. But it certainly seemed like the judge was done. It seemed
like the the period of, well, he's the former president, he talks a lot, is over.
we'll have to wait and see. Odds are if Trump lacks the ability or believes it
will be politically advantageous for him to be confined, we probably won't
have to wait long to see what happens, at which point we'll see what he decides
and we'll see whether or not the state recommends it and whether or not the
judge takes that recommendation. But the tone of this is different. Anyway, it's
It's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}