---
title: Let's talk about Trump, Scott, and the GOP plan for 2024....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=29zzUlv6TxE) |
| Published | 2024/05/07 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump, the GOP, the strategy moving towards 2024
and what that strategy is, because the shenanigans have already started and it's
going to be the same as last time.
It's the same overall strategy.
They're going to roll it out again and hope that the American
people, at least a significant number of them, fall for it again.
The goal is to cast doubt on basic American institutions to undermine the republic through
rhetoric.
Recently, Trump, again, tried to claim that he won Wisconsin.
He didn't.
He lost.
20,000 votes, he lost.
But he repeats that idea that something was wrong, that he really won.
The idea is to cast doubt on the very foundations of the American Republic.
That's the goal.
They've got the lawsuits up and running.
And they're framing them as election integrity or election security, saying that's what
they're about.
Okay, I'll play your silly little game.
Explain to me how two ballots that go through the exact same security precautions are different.
How one is less secure because it shows up a day later.
can't, right? Because that's not really what it's about. It's about having the
suits and claiming that there's an issue, creating that appearance, and hoping
that the American people buy into it. Now you have a potential VP pick for Trump.
Senator Scott, asked repeatedly if he would accept the results of the election if Trump didn't win.
Asked over and over again, I don't even know how many times, during the exchange.
Because he was asked, and it was basically, you know, the next president's going to be Trump.
And the reporter's like, yeah, but will you accept the results if they're not?
they're not. He's like, I made my statement. It's a yes or no question. Just a yes or no
answer would be fine. Would you accept the results no matter who wins? Wouldn't answer.
They keep saying stuff like, well, we would accept them if they're honest. If they're honest,
That implies that somewhere along the line they weren't. That's the claim that
they keep repeating, but they have had years to demonstrate this claim. They
haven't been able to. They're just repeating the same claim in hopes that
the American people buy into it. That they are willing to give up the
foundational elements of this country for a red hat. That's what it's about.
That's the strategy. So expect to see a whole bunch of claims about the election.
Just more and more and more in hopes of duping people into believing it. They had
years to come out and really demonstrate it, and they haven't been able to. There
might be a reason why. So that's what you can expect to see. It's the same thing as
the last time. It's creating a situation in which there might be doubt, and that
that doubt can be exploited to alter the outcome of the election in, you know, in
defiance of American tradition, the Constitution, the basic principles of the
country, all of that stuff. Expect more of the same. Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}