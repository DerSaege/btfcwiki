# All videos from May, 2024
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-07-31 16:19:46
<details>
<summary>
2024-05-31: Let's talk about adapting rules in Ukraine.... (<a href="https://youtube.com/watch?v=4fAt0ryxZqw">watch</a> || <a href="/videos/2024/05/31/Lets_talk_about_adapting_rules_in_Ukraine">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-31: Let's talk about Trump being found guilty and what's next.... (<a href="https://youtube.com/watch?v=L3iKY4gbL0w">watch</a> || <a href="/videos/2024/05/31/Lets_talk_about_Trump_being_found_guilty_and_what_s_next">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-31: Let's talk about Menendez and New Jersey.... (<a href="https://youtube.com/watch?v=b89R5ld_Hi4">watch</a> || <a href="/videos/2024/05/31/Lets_talk_about_Menendez_and_New_Jersey">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-31: Let's talk about Biden and the peace proposal.... (<a href="https://youtube.com/watch?v=1vEI3GXjx1c">watch</a> || <a href="/videos/2024/05/31/Lets_talk_about_Biden_and_the_peace_proposal">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-30: The Roads to a Q&A on verdict watch day.... (<a href="https://youtube.com/watch?v=dXakGWVyphE">watch</a> || <a href="/videos/2024/05/30/The_Roads_to_a_Q_A_on_verdict_watch_day">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-30: Let's talk about the pier, timelines, and questions.... (<a href="https://youtube.com/watch?v=iNnBFhvIyQM">watch</a> || <a href="/videos/2024/05/30/Lets_talk_about_the_pier_timelines_and_questions">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-30: Let's talk about Trump and verdict watch day 2.... (<a href="https://youtube.com/watch?v=SM-4QrpDl0E">watch</a> || <a href="/videos/2024/05/30/Lets_talk_about_Trump_and_verdict_watch_day_2">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-30: Let's talk about Biden, shifting positions, and the ICC.... (<a href="https://youtube.com/watch?v=rwIXIcuVc8w">watch</a> || <a href="/videos/2024/05/30/Lets_talk_about_Biden_shifting_positions_and_the_ICC">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-30: Let's talk about Biden, Ohio, and the GOP.... (<a href="https://youtube.com/watch?v=8ymS0Y_y1JA">watch</a> || <a href="/videos/2024/05/30/Lets_talk_about_Biden_Ohio_and_the_GOP">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-29: Let's talk about Trump, history, and verdict watch.... (<a href="https://youtube.com/watch?v=CH_2V8_CYy4">watch</a> || <a href="/videos/2024/05/29/Lets_talk_about_Trump_history_and_verdict_watch">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-29: Let's talk about Trump, Freedom, Good, and Johnson.... (<a href="https://youtube.com/watch?v=8xOXPNHWljE">watch</a> || <a href="/videos/2024/05/29/Lets_talk_about_Trump_Freedom_Good_and_Johnson">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-29: Let's talk about Trump feelin' bad in Texas.... (<a href="https://youtube.com/watch?v=V-jr8AxD0Xo">watch</a> || <a href="/videos/2024/05/29/Lets_talk_about_Trump_feelin_bad_in_Texas">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-29: Let's talk about Nikki Haley, trips, and images.... (<a href="https://youtube.com/watch?v=vaV1mUDPxi8">watch</a> || <a href="/videos/2024/05/29/Lets_talk_about_Nikki_Haley_trips_and_images">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-28: Let's talk about the wisdom of McConnell.... (<a href="https://youtube.com/watch?v=gV5HEAM4JSE">watch</a> || <a href="/videos/2024/05/28/Lets_talk_about_the_wisdom_of_McConnell">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-28: Let's talk about Trump's Memorial Day message to America.... (<a href="https://youtube.com/watch?v=uiHcLlL0-_8">watch</a> || <a href="/videos/2024/05/28/Lets_talk_about_Trump_s_Memorial_Day_message_to_America">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-28: Let's talk about Texas removing power from GOP voters.... (<a href="https://youtube.com/watch?v=ym9gbDpewwc">watch</a> || <a href="/videos/2024/05/28/Lets_talk_about_Texas_removing_power_from_GOP_voters">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-28: Let's talk about 2 events over there and what might change.... (<a href="https://youtube.com/watch?v=94aHmqZ6Ddg">watch</a> || <a href="/videos/2024/05/28/Lets_talk_about_2_events_over_there_and_what_might_change">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-27: Let's talk about the conclusion of the Trump-Kennedy show.... (<a href="https://youtube.com/watch?v=Ox7DCwZgFnA">watch</a> || <a href="/videos/2024/05/27/Lets_talk_about_the_conclusion_of_the_Trump-Kennedy_show">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-27: Let's talk about Schumer playing Trump and the GOP.... (<a href="https://youtube.com/watch?v=cS0rlmuf-TA">watch</a> || <a href="/videos/2024/05/27/Lets_talk_about_Schumer_playing_Trump_and_the_GOP">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-27: Let's talk about Biden's possible moves for Trump verdict.... (<a href="https://youtube.com/watch?v=QI9z22z8_dw">watch</a> || <a href="/videos/2024/05/27/Lets_talk_about_Biden_s_possible_moves_for_Trump_verdict">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-27: Let's talk about 2 student debt relief items and Biden.... (<a href="https://youtube.com/watch?v=je_viee5uYA">watch</a> || <a href="/videos/2024/05/27/Lets_talk_about_2_student_debt_relief_items_and_Biden">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-26: Roads Not Taken EP 40 (<a href="https://youtube.com/watch?v=PnG8z6eK6o4">watch</a> || <a href="/videos/2024/05/26/Roads_Not_Taken_EP_40">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-26: Let's talk about simple solutions and hard realities.... (<a href="https://youtube.com/watch?v=eaLz-MgwyDE">watch</a> || <a href="/videos/2024/05/26/Lets_talk_about_simple_solutions_and_hard_realities">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-26: Let's talk about overly simplistic options for November.... (<a href="https://youtube.com/watch?v=-vHn-I3ddEA">watch</a> || <a href="/videos/2024/05/26/Lets_talk_about_overly_simplistic_options_for_November">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-26: Let's talk about Trump's bad night.... (<a href="https://youtube.com/watch?v=xhm9geEg9uM">watch</a> || <a href="/videos/2024/05/26/Lets_talk_about_Trump_s_bad_night">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-26: Let's talk about Biden, Ohio, foreign money, and next week.... (<a href="https://youtube.com/watch?v=PEc_5b5b3Yo">watch</a> || <a href="/videos/2024/05/26/Lets_talk_about_Biden_Ohio_foreign_money_and_next_week">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-25: Let's talk about trees, records, and heat.... (<a href="https://youtube.com/watch?v=kGG-1j3Cf60">watch</a> || <a href="/videos/2024/05/25/Lets_talk_about_trees_records_and_heat">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-25: Let's talk about RFK going after Trump.... (<a href="https://youtube.com/watch?v=jSd42BL4zz4">watch</a> || <a href="/videos/2024/05/25/Lets_talk_about_RFK_going_after_Trump">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-25: Let's talk about Putin's promise of peace in Ukraine.... (<a href="https://youtube.com/watch?v=_uZDeM6FY3w">watch</a> || <a href="/videos/2024/05/25/Lets_talk_about_Putin_s_promise_of_peace_in_Ukraine">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-25: Let's talk about North Carolina, the GOP, and veterans.... (<a href="https://youtube.com/watch?v=L5DVcU2Pg80">watch</a> || <a href="/videos/2024/05/25/Lets_talk_about_North_Carolina_the_GOP_and_veterans">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-24: Let's talk about and update on New Year's message guy.... (<a href="https://youtube.com/watch?v=NaCKvNBhJHE">watch</a> || <a href="/videos/2024/05/24/Lets_talk_about_and_update_on_New_Year_s_message_guy">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-24: Let's talk about Trump, Putin, promises, and questions.... (<a href="https://youtube.com/watch?v=tOBqMIdMx2M">watch</a> || <a href="/videos/2024/05/24/Lets_talk_about_Trump_Putin_promises_and_questions">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-24: Let's talk about Trump, Biden, executive orders, and legislation.... (<a href="https://youtube.com/watch?v=VZHTElQCJMQ">watch</a> || <a href="/videos/2024/05/24/Lets_talk_about_Trump_Biden_executive_orders_and_legislation">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-24: Let's talk about Biden, Obama, Kenya, and the future.... (<a href="https://youtube.com/watch?v=g4_ZWMZqv8o">watch</a> || <a href="/videos/2024/05/24/Lets_talk_about_Biden_Obama_Kenya_and_the_future">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-23: The Roads to Trump, Jenga, and Foreign Policy (<a href="https://youtube.com/watch?v=asYJ8eQy5YI">watch</a> || <a href="/videos/2024/05/23/The_Roads_to_Trump_Jenga_and_Foreign_Policy">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-23: Let's talk about Trump and rapidly changing views.... (<a href="https://youtube.com/watch?v=q1LODa73K-8">watch</a> || <a href="/videos/2024/05/23/Lets_talk_about_Trump_and_rapidly_changing_views">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-23: Let's talk about Senate shenanigans showing signs of success.... (<a href="https://youtube.com/watch?v=XwLNlfGv4B4">watch</a> || <a href="/videos/2024/05/23/Lets_talk_about_Senate_shenanigans_showing_signs_of_success">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-23: Let's talk about Lindsay Graham, the US, and the ICC.... (<a href="https://youtube.com/watch?v=_c8UXIEGmk4">watch</a> || <a href="/videos/2024/05/23/Lets_talk_about_Lindsay_Graham_the_US_and_the_ICC">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-23: Let's talk about Haley voting for Trump.... (<a href="https://youtube.com/watch?v=n61WS5gC-RI">watch</a> || <a href="/videos/2024/05/23/Lets_talk_about_Haley_voting_for_Trump">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-22: Let's talk about Trump's video issue.... (<a href="https://youtube.com/watch?v=JMylRae9_fE">watch</a> || <a href="/videos/2024/05/22/Lets_talk_about_Trump_s_video_issue">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-22: Let's talk about Trump's NY schedule.... (<a href="https://youtube.com/watch?v=w2rAew6G0VI">watch</a> || <a href="/videos/2024/05/22/Lets_talk_about_Trump_s_NY_schedule">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-22: Let's talk about Ireland, Spain, Norway, and recognition.... (<a href="https://youtube.com/watch?v=-OD8EvyXXj0">watch</a> || <a href="/videos/2024/05/22/Lets_talk_about_Ireland_Spain_Norway_and_recognition">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-22: Let's talk about Georgia, Willis, and McAfee.... (<a href="https://youtube.com/watch?v=oMfs2ym99nY">watch</a> || <a href="/videos/2024/05/22/Lets_talk_about_Georgia_Willis_and_McAfee">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-21: Let's talk about the US response to the ICC.... (<a href="https://youtube.com/watch?v=L_IdMNT-5bw">watch</a> || <a href="/videos/2024/05/21/Lets_talk_about_the_US_response_to_the_ICC">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-21: Let's talk about Trump, Truth, and money.... (<a href="https://youtube.com/watch?v=z2y4bNpMUAM">watch</a> || <a href="/videos/2024/05/21/Lets_talk_about_Trump_Truth_and_money">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-21: Let's talk about Schumer's move against Trump and the GOP.... (<a href="https://youtube.com/watch?v=DDF_Ug6s3f4">watch</a> || <a href="/videos/2024/05/21/Lets_talk_about_Schumer_s_move_against_Trump_and_the_GOP">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-21: Let's talk about Russia's campaign in Europe.... (<a href="https://youtube.com/watch?v=4m1EuQf68Qg">watch</a> || <a href="/videos/2024/05/21/Lets_talk_about_Russia_s_campaign_in_Europe">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-20: Let's talk about the ICC seeking charges.... (<a href="https://youtube.com/watch?v=4PxmvQUOuMg">watch</a> || <a href="/videos/2024/05/20/Lets_talk_about_the_ICC_seeking_charges">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-20: Let's talk about a rabbit hole with foreign policy.... (<a href="https://youtube.com/watch?v=ffRmh49HrEs">watch</a> || <a href="/videos/2024/05/20/Lets_talk_about_a_rabbit_hole_with_foreign_policy">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-20: Let's talk about Trump backing away from one of his wins.... (<a href="https://youtube.com/watch?v=eJRIBd-s3vY">watch</a> || <a href="/videos/2024/05/20/Lets_talk_about_Trump_backing_away_from_one_of_his_wins">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-20: Let's talk about Iran, a trench coat, and a helicopter.... (<a href="https://youtube.com/watch?v=V4iaFCsSons">watch</a> || <a href="/videos/2024/05/20/Lets_talk_about_Iran_a_trench_coat_and_a_helicopter">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-19: Roads Not Taken EP 39 (<a href="https://youtube.com/watch?v=nuf8pN8Rd7Q">watch</a> || <a href="/videos/2024/05/19/Roads_Not_Taken_EP_39">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-19: Let's talk about the GOP, Missouri, and the Governor's race.... (<a href="https://youtube.com/watch?v=7MsKpz0Hf6c">watch</a> || <a href="/videos/2024/05/19/Lets_talk_about_the_GOP_Missouri_and_the_Governor_s_race">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-19: Let's talk about splits, diplomacy, and over there.... (<a href="https://youtube.com/watch?v=SLZkrB63gCE">watch</a> || <a href="/videos/2024/05/19/Lets_talk_about_splits_diplomacy_and_over_there">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-19: Let's talk about a big win for people in Missouri.... (<a href="https://youtube.com/watch?v=7ums5MeTDWA">watch</a> || <a href="/videos/2024/05/19/Lets_talk_about_a_big_win_for_people_in_Missouri">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-19: Let's talk about GOP language and a veto in VA.... (<a href="https://youtube.com/watch?v=TA0Cu2InzKc">watch</a> || <a href="/videos/2024/05/19/Lets_talk_about_GOP_language_and_a_veto_in_VA">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-18: Let's talk about the new energy rule.... (<a href="https://youtube.com/watch?v=XrMlpAZR7So">watch</a> || <a href="/videos/2024/05/18/Lets_talk_about_the_new_energy_rule">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-18: Let's talk about every classroom in Louisiana and the Commandments.... (<a href="https://youtube.com/watch?v=lunMO6ZvSvg">watch</a> || <a href="/videos/2024/05/18/Lets_talk_about_every_classroom_in_Louisiana_and_the_Commandments">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-18: Let's talk about a request for advice about grandpa.... (<a href="https://youtube.com/watch?v=_OwsgzktjLY">watch</a> || <a href="/videos/2024/05/18/Lets_talk_about_a_request_for_advice_about_grandpa">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-18: Let's talk about Rudy's birthday present from Arizona.... (<a href="https://youtube.com/watch?v=d1IoNeYfXmY">watch</a> || <a href="/videos/2024/05/18/Lets_talk_about_Rudy_s_birthday_present_from_Arizona">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-17: Let's talk about the oversight committee disaster.... (<a href="https://youtube.com/watch?v=a-49-GFi2n4">watch</a> || <a href="/videos/2024/05/17/Lets_talk_about_the_oversight_committee_disaster">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-17: Let's talk about a pier in place and what's next.... (<a href="https://youtube.com/watch?v=gNgP1kCY9LU">watch</a> || <a href="/videos/2024/05/17/Lets_talk_about_a_pier_in_place_and_what_s_next">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-17: Let's talk about a Biden dilemma.... (<a href="https://youtube.com/watch?v=hu5K-RcUqD4">watch</a> || <a href="/videos/2024/05/17/Lets_talk_about_a_Biden_dilemma">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-17: Let's talk about Haley voters, a retreat, and Trump.... (<a href="https://youtube.com/watch?v=aN3lpuNh-Z0">watch</a> || <a href="/videos/2024/05/17/Lets_talk_about_Haley_voters_a_retreat_and_Trump">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-16: The Roads to a May Q&A.... (<a href="https://youtube.com/watch?v=_RVuhm2Y1oU">watch</a> || <a href="/videos/2024/05/16/The_Roads_to_a_May_Q_A">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-16: Let's talk about the Trump-Biden debates.... (<a href="https://youtube.com/watch?v=bbU_g6IghmU">watch</a> || <a href="/videos/2024/05/16/Lets_talk_about_the_Trump-Biden_debates">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-16: Let's talk about an American general, Ukraine, phrasing, and raised eyebrows.... (<a href="https://youtube.com/watch?v=H0h4T9_EtTU">watch</a> || <a href="/videos/2024/05/16/Lets_talk_about_an_American_general_Ukraine_phrasing_and_raised_eyebrows">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-16: Let's talk about a public disagreement about ingredients and a quote.... (<a href="https://youtube.com/watch?v=Q4dInNkQSBs">watch</a> || <a href="/videos/2024/05/16/Lets_talk_about_a_public_disagreement_about_ingredients_and_a_quote">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-16: Let's talk about Trump debate bingo cards.... (<a href="https://youtube.com/watch?v=Dzs96psczvg">watch</a> || <a href="/videos/2024/05/16/Lets_talk_about_Trump_debate_bingo_cards">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-15: Let's talk about Trump's primary performance polling problems.... (<a href="https://youtube.com/watch?v=82FCXFx57QY">watch</a> || <a href="/videos/2024/05/15/Lets_talk_about_Trump_s_primary_performance_polling_problems">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-15: Let's talk about Trump and the next congressional investigation.... (<a href="https://youtube.com/watch?v=MdHRcyfWhOM">watch</a> || <a href="/videos/2024/05/15/Lets_talk_about_Trump_and_the_next_congressional_investigation">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-15: Let's talk about Biden's veto promise.... (<a href="https://youtube.com/watch?v=dRcx2H-y-fM">watch</a> || <a href="/videos/2024/05/15/Lets_talk_about_Biden_s_veto_promise">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-15: Let's talk about AZ, good but not great news, and the 1800s.... (<a href="https://youtube.com/watch?v=gT9Hk_Y3u6A">watch</a> || <a href="/videos/2024/05/15/Lets_talk_about_AZ_good_but_not_great_news_and_the_1800s">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-14: Let's talk about your grocery bill and the Democratic party.... (<a href="https://youtube.com/watch?v=DxtZQxJSEbo">watch</a> || <a href="/videos/2024/05/14/Lets_talk_about_your_grocery_bill_and_the_Democratic_party">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-14: Let's talk about why they're going back up north.... (<a href="https://youtube.com/watch?v=Cmf4e3l4yuk">watch</a> || <a href="/videos/2024/05/14/Lets_talk_about_why_they_re_going_back_up_north">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-14: Let's talk about Tuberville, Trump, and New York.... (<a href="https://youtube.com/watch?v=fuEBFJ-qj5M">watch</a> || <a href="/videos/2024/05/14/Lets_talk_about_Tuberville_Trump_and_New_York">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-14: Let's talk about Putin's big shakeup and what it means.... (<a href="https://youtube.com/watch?v=wrZ6nKdu4C8">watch</a> || <a href="/videos/2024/05/14/Lets_talk_about_Putin_s_big_shakeup_and_what_it_means">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-13: Let's talk about the GOP going after MTG.... (<a href="https://youtube.com/watch?v=ndpx1kskvC0">watch</a> || <a href="/videos/2024/05/13/Lets_talk_about_the_GOP_going_after_MTG">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-13: Let's talk about how you might be able to influence US foreign policy.... (<a href="https://youtube.com/watch?v=sahS0lPM3tM">watch</a> || <a href="/videos/2024/05/13/Lets_talk_about_how_you_might_be_able_to_influence_US_foreign_policy">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-13: Let's talk about Trump being worried about Junior.... (<a href="https://youtube.com/watch?v=KBkIJgQG1RM">watch</a> || <a href="/videos/2024/05/13/Lets_talk_about_Trump_being_worried_about_Junior">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-13: Let's talk about Cohen's first day, Trump, and NY.... (<a href="https://youtube.com/watch?v=wDk7LpHGOtg">watch</a> || <a href="/videos/2024/05/13/Lets_talk_about_Cohen_s_first_day_Trump_and_NY">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-12: Roads Not Taken EP 38 (<a href="https://youtube.com/watch?v=50tLFy3XrSE">watch</a> || <a href="/videos/2024/05/12/Roads_Not_Taken_EP_38">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-12: Let's talk about a history lesson for the powerful.... (<a href="https://youtube.com/watch?v=xT0mWXjOMfU">watch</a> || <a href="/videos/2024/05/12/Lets_talk_about_a_history_lesson_for_the_powerful">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-12: Let's talk about Trump's potential VP pick.... (<a href="https://youtube.com/watch?v=CYYA2RwhMx8">watch</a> || <a href="/videos/2024/05/12/Lets_talk_about_Trump_s_potential_VP_pick">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-12: Let's talk about Trump and Cohen.... (<a href="https://youtube.com/watch?v=fuEG3HuIPcE">watch</a> || <a href="/videos/2024/05/12/Lets_talk_about_Trump_and_Cohen">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-12: Let's talk about Rudy and the radio show.... (<a href="https://youtube.com/watch?v=8-zvdwwLKT4">watch</a> || <a href="/videos/2024/05/12/Lets_talk_about_Rudy_and_the_radio_show">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-11: Let's talk about the report from State and the coverage.... (<a href="https://youtube.com/watch?v=B3lZbS-M-DM">watch</a> || <a href="/videos/2024/05/11/Lets_talk_about_the_report_from_State_and_the_coverage">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-11: Let's talk about the latest US offer from Biden.... (<a href="https://youtube.com/watch?v=syuD4KJekao">watch</a> || <a href="/videos/2024/05/11/Lets_talk_about_the_latest_US_offer_from_Biden">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-11: Let's talk about the UN vote.... (<a href="https://youtube.com/watch?v=8zOPWVCfS9o">watch</a> || <a href="/videos/2024/05/11/Lets_talk_about_the_UN_vote">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-11: Let's talk about Trump and Biden doing the same thing.... (<a href="https://youtube.com/watch?v=cmjnZwG-IaY">watch</a> || <a href="/videos/2024/05/11/Lets_talk_about_Trump_and_Biden_doing_the_same_thing">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-10: Let's talk about the House GOP vs Biden delays.... (<a href="https://youtube.com/watch?v=aam1IcXf-9s">watch</a> || <a href="/videos/2024/05/10/Lets_talk_about_the_House_GOP_vs_Biden_delays">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-10: Let's talk about Trump, oil, and 1 billion dollars.... (<a href="https://youtube.com/watch?v=EcW4xUnNzrc">watch</a> || <a href="/videos/2024/05/10/Lets_talk_about_Trump_oil_and_1_billion_dollars">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-10: Let's talk about Biden's statements on over there.... (<a href="https://youtube.com/watch?v=BX_yaLwyCB4">watch</a> || <a href="/videos/2024/05/10/Lets_talk_about_Biden_s_statements_on_over_there">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-10: Let's talk about Biden doing his job and the Constitution.... (<a href="https://youtube.com/watch?v=eM9JcP_vMfM">watch</a> || <a href="/videos/2024/05/10/Lets_talk_about_Biden_doing_his_job_and_the_Constitution">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-09: The Roads to College Foreign Policy questions.... (<a href="https://youtube.com/watch?v=N2zsjzwJaBM">watch</a> || <a href="/videos/2024/05/09/The_Roads_to_College_Foreign_Policy_questions">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-09: Let's talk about Trump, Biden, and polling.... (<a href="https://youtube.com/watch?v=wWibk3_SOuo">watch</a> || <a href="/videos/2024/05/09/Lets_talk_about_Trump_Biden_and_polling">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-09: Let's talk about Kristi Noem's job search.... (<a href="https://youtube.com/watch?v=nUXF4ruYZq0">watch</a> || <a href="/videos/2024/05/09/Lets_talk_about_Kristi_Noem_s_job_search">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-09: Let's talk about Johnson, Greene, and MTV.... (<a href="https://youtube.com/watch?v=MBM6Kuw4pX4">watch</a> || <a href="/videos/2024/05/09/Lets_talk_about_Johnson_Greene_and_MTV">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-09: Let's talk about EU and 2 Ukraine items.... (<a href="https://youtube.com/watch?v=tzK5gkfSCUY">watch</a> || <a href="/videos/2024/05/09/Lets_talk_about_EU_and_2_Ukraine_items">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-08: Let's talk about over there, coverage, and questions.... (<a href="https://youtube.com/watch?v=8grlKB_UrTw">watch</a> || <a href="/videos/2024/05/08/Lets_talk_about_over_there_coverage_and_questions">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-08: Let's talk about a trip to the gas station, politics, and youth.... (<a href="https://youtube.com/watch?v=lie1TNMpBms">watch</a> || <a href="/videos/2024/05/08/Lets_talk_about_a_trip_to_the_gas_station_politics_and_youth">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-08: Let's talk about Trump, definitions, documents, and delays.... (<a href="https://youtube.com/watch?v=R7mTGu7P7t4">watch</a> || <a href="/videos/2024/05/08/Lets_talk_about_Trump_definitions_documents_and_delays">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-08: Let's talk about Alabama, Biden, ballots, and Ohio.... (<a href="https://youtube.com/watch?v=-kTEt7U4bF4">watch</a> || <a href="/videos/2024/05/08/Lets_talk_about_Alabama_Biden_ballots_and_Ohio">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-07: Let's talk about a Georgia GOP endorsement of Biden.... (<a href="https://youtube.com/watch?v=Ia6Sql8SQ7E">watch</a> || <a href="/videos/2024/05/07/Lets_talk_about_a_Georgia_GOP_endorsement_of_Biden">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-07: Let's talk about Trump, the Judge, and confinement.... (<a href="https://youtube.com/watch?v=8AstM56QSFc">watch</a> || <a href="/videos/2024/05/07/Lets_talk_about_Trump_the_Judge_and_confinement">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-07: Let's talk about Trump, Scott, and the GOP plan for 2024.... (<a href="https://youtube.com/watch?v=29zzUlv6TxE">watch</a> || <a href="/videos/2024/05/07/Lets_talk_about_Trump_Scott_and_the_GOP_plan_for_2024">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-07: Let's talk about MTG blinking, Johnson, and the US House.... (<a href="https://youtube.com/watch?v=pQJAWNpclao">watch</a> || <a href="/videos/2024/05/07/Lets_talk_about_MTG_blinking_Johnson_and_the_US_House">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-06: Let's talk about what we know about over there.... (<a href="https://youtube.com/watch?v=t6ogvfjrmi0">watch</a> || <a href="/videos/2024/05/06/Lets_talk_about_what_we_know_about_over_there">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-06: Let's talk about the UK, Ukraine, Russia, and messaging.... (<a href="https://youtube.com/watch?v=zb-k2TmS3UY">watch</a> || <a href="/videos/2024/05/06/Lets_talk_about_the_UK_Ukraine_Russia_and_messaging">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-06: Let's talk about Trump, Romney, welfare, and being entitled.... (<a href="https://youtube.com/watch?v=VOem-jQkweA">watch</a> || <a href="/videos/2024/05/06/Lets_talk_about_Trump_Romney_welfare_and_being_entitled">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-06: Let's talk about Trump, NY, and Sleeping Beauty.... (<a href="https://youtube.com/watch?v=ru6nnwgbyLI">watch</a> || <a href="/videos/2024/05/06/Lets_talk_about_Trump_NY_and_Sleeping_Beauty">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-05: Roads Not Taken EP 37 (<a href="https://youtube.com/watch?v=A88D3NifK7k">watch</a> || <a href="/videos/2024/05/05/Roads_Not_Taken_EP_37">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-05: Let's talk about the negotiations and the coverage.... (<a href="https://youtube.com/watch?v=kcxdQ4g9R4s">watch</a> || <a href="/videos/2024/05/05/Lets_talk_about_the_negotiations_and_the_coverage">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-05: Let's talk about the SEC, an auditor, and a Trump company.... (<a href="https://youtube.com/watch?v=4MRYKd3jSE8">watch</a> || <a href="/videos/2024/05/05/Lets_talk_about_the_SEC_an_auditor_and_a_Trump_company">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-05: Let's talk about the House, Texas, and $600,000.... (<a href="https://youtube.com/watch?v=m-Lt4RpKp44">watch</a> || <a href="/videos/2024/05/05/Lets_talk_about_the_House_Texas_and_600_000">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-05: Let's talk about South Dakota, a book, and a trend.... (<a href="https://youtube.com/watch?v=7BKDChuHkkk">watch</a> || <a href="/videos/2024/05/05/Lets_talk_about_South_Dakota_a_book_and_a_trend">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-04: Let's talk about the UN, food, and an interview airing tomorrow.... (<a href="https://youtube.com/watch?v=qNJv9yzxjuM">watch</a> || <a href="/videos/2024/05/04/Lets_talk_about_the_UN_food_and_an_interview_airing_tomorrow">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-04: Let's talk about some interesting reporting and the day after.... (<a href="https://youtube.com/watch?v=1LbB7iVtXxM">watch</a> || <a href="/videos/2024/05/04/Lets_talk_about_some_interesting_reporting_and_the_day_after">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-05-04: Let's talk about one party and two messages.... (<a href="https://youtube.com/watch?v=cGP3TBHQIfE">watch</a> || <a href="/videos/2024/05/04/Lets_talk_about_one_party_and_two_messages">transcript &amp; editable summary</a>)

Beau explains the term "uniparty" and challenges the notion of no differences between the two parties, urging perspective beyond one's own demographics to recognize the significant impacts on various communities.

</summary>

1. "There is zero difference between the two parties."
2. "If you can't find anything that is different for you, that changes your life, maybe look at it from a different point of view."
3. "Realistically the two parties have never been further apart."
4. "There's a difference and it really matters to a whole lot of people."
5. "Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains the term "uniparty" that arises when something bipartisan happens on Capitol Hill.
Describes the implication of the term as suggesting that the two parties are essentially the same, putting on a show of differences.
Shares a message from a trans woman expressing fear and concern about potential outcomes in November.
Notes the impact of political decisions on women's reproductive rights, gay rights, unions, education, and more.
Mentions the fear of Supreme Court appointments and the consequences for marginalized communities.
Talks about the perceived lack of differences between the two parties based on demographics.
Acknowledges that there are tangible differences between the parties that affect people's lives.
Points out areas where both parties generally agree, like large-scale economics and national security.
Addresses the misconception that there are no differences between the parties and explains nuanced policy distinctions.
Encourages considering viewpoints beyond one's own demographics to see the real differences in political stances.

Actions:

for voters, political observers,
Reach out to marginalized communities to understand their concerns and perspectives (suggested).
Support organizations advocating for reproductive rights, gay rights, education, and unions (exemplified).
Educate yourself on policy differences between political parties (implied).
</details>
<details>
<summary>
2024-05-04: Let's talk about Trump, Hicks, NY, and strategies.... (<a href="https://youtube.com/watch?v=RwT9z0ix1Dc">watch</a> || <a href="/videos/2024/05/04/Lets_talk_about_Trump_Hicks_NY_and_strategies">transcript &amp; editable summary</a>)

Trump's strategy of claiming a "Democratic witch hunt" is being undercut as damaging revelations come from his own supporters, not political rivals.

</summary>

1. "It's hard to say that when the most critical pieces of information are coming from people who, well, I mean, they're partisan, but they're on your side."
2. "You know, you've already seen friends and people that worked with him for years, and you can expect to see more of that."
3. "Get used to that surprise because I think we're going to see it a lot."
4. "It's manufactured, that it's a political witch hunt."
5. "It'll only play within that base who, honestly, short of Trump walking out and telling them to believe that it was true, nothing would convince them."

### AI summary (High error rate! Edit errors on video page)

Examines Trump, New York, Hicks, and strategy.
Strategy seen before that diminishes Trump's claims.
Hicks' statements viewed as damaging to Trump.
Trump's go-to claim of "Democratic witch hunt."
Strategy used during January 6 hearings.
Most damaging statements not from political rivals but from supporters.
Undercutting Trump's main narrative.
Strategy likely to play out in future entanglements.
Most critical information coming from people on Trump's side.
Expect more revelations from Trump's supporters.

Actions:

for political analysts, activists, voters,
Analyze and understand the strategy being used by political figures (implied)
Stay informed about developments in political entanglements (implied)
Be prepared for potential surprises in future revelations (implied)
</details>
<details>
<summary>
2024-05-03: Let's talk about how Johnson's Democratic support is working.... (<a href="https://youtube.com/watch?v=GIMO3hkgw7g">watch</a> || <a href="/videos/2024/05/03/Lets_talk_about_how_Johnson_s_Democratic_support_is_working">transcript &amp; editable summary</a>)

Beau explains Speaker Johnson's dilemma with Democratic support, leading to baseless claims and potential Republican Party divisions.

</summary>

1. "It does appear that in pursuit of a more conservative framework for the Republican Party that Green got outplayed by the Democratic Party and now she's doing all of the work for them."
2. "The lack of evidence in the modern Republican Party doesn't hinder belief in baseless claims."

### AI summary (High error rate! Edit errors on video page)

Explains the situation Speaker of the House Johnson finds himself in, with newfound support from the Democratic Party.
Johnson is now forced to defend himself against claims of being a "rhino" or cutting deals with Democrats.
Marjorie Taylor Greene is moving forward with a motion to vacate Johnson.
Johnson, initially secure, now has to defend his conservative record and faces rumors of engaging in backroom deals.
The lack of evidence in the modern Republican Party doesn't hinder belief in baseless claims.
Democratic Party's offer to help Johnson might actually be a liability rather than assistance.
Greene claims she's not defying Trump's wishes, hinting at a McConnell statement that Johnson neutralized Trump on an aid package.
Speculation arises that Trump may be using Greene to oust Johnson to save face.
If Trump fails in this move, he risks looking weaker to Republicans.
The Democratic Party's support could ensure Johnson retains his seat, further dividing the Republican Party.
Greene's actions might unintentionally strengthen the Democratic Party's position.

Actions:

for political analysts, party members,
Support candidates based on policies and actions, not baseless claims or party affiliation (exemplified)
Stay informed about political developments and understand the dynamics within parties (exemplified)
Engage in constructive dialogues to bridge divides within political factions (exemplified)
</details>
<details>
<summary>
2024-05-03: Let's talk about big news for a plan for the region.... (<a href="https://youtube.com/watch?v=0jOXh3Yhdg0">watch</a> || <a href="/videos/2024/05/03/Lets_talk_about_big_news_for_a_plan_for_the_region">transcript &amp; editable summary</a>)

Beau outlines a potential game-changer in Middle Eastern diplomacy, involving Saudi Arabia, Israel, the US, and Palestine, with far-reaching implications.

</summary>

1. "It alters not just the map."
2. "It is a huge thing."
3. "This is definitely something to watch."
4. "It might be the sweetener to alter some attitudes."
5. "Anyway, it's just a thought."

### AI summary (High error rate! Edit errors on video page)

Three major players involved: Saudi Arabia, Israel, and the United States.
Saudi Arabia seeks a defense pact with the US and normalized relations with Israel.
If successful, it could reshape the Middle East landscape.
US aims to deprioritize the region by fostering alliances between regional powers.
Involvement of Palestine adds a new dimension to the negotiations.
Potential for a credible pathway to a Palestinian state as part of the agreements.
Implications extend beyond just political boundaries, impacting the entire Middle East.
Possibility of expanding alliances to include other countries like Bahrain.
The development signifies a shift in Saudi Arabia's long-term planning.
Raises questions about potential US military engagement in other regions post-deprioritization of the Middle East.

Actions:

for diplomacy observers,
Monitor the developments closely to understand the evolving diplomatic landscape (suggested).
Stay informed about the potential implications for regional stability (suggested).
</details>
<details>
<summary>
2024-05-03: Let's talk about Trump and acceptance.... (<a href="https://youtube.com/watch?v=lyBYAecbs6g">watch</a> || <a href="/videos/2024/05/03/Lets_talk_about_Trump_and_acceptance">transcript &amp; editable summary</a>)

Beau sheds light on Trump's refusal to accept election results, sowing doubt and manipulating his followers for political gain.

</summary>

"He's cranking up that rhetoric, trying to manipulate them, just like he did last time."
"He may not find Fox News to be quite so willing to air his baseless claims, but they will be on social media."
"The real question is whether or not his supporters are going to be willing to pay the cost yet again."

### AI summary (High error rate! Edit errors on video page)

Talks about Trump's refusal to commit to accepting election results.
Trump believes he will lose the election and is already sowing doubt.
Mentions Trump's history of not accepting past election results.
Trump expects his supporters to fight for him if he doesn't accept the results.
Trump claims he won the last election despite evidence proving otherwise.
Points out that Trump won't bear the consequences of his actions; his followers will.
Trump is manipulating and trying to control his supporters' thoughts and actions.
Indicates that Trump is setting the stage for a repeat of his behavior from the last election.
Notes that some media outlets may not support Trump's baseless claims this time.
Raises the question of whether Trump's supporters will continue to follow him blindly.

Actions:

for voters, concerned citizens,
Question and critically analyze political statements (implied)
</details>
<details>
<summary>
2024-05-03: Let's talk about Biden and $6.1 Billion.... (<a href="https://youtube.com/watch?v=pHjCiALzAsk">watch</a> || <a href="/videos/2024/05/03/Lets_talk_about_Biden_and_6_1_Billion">transcript &amp; editable summary</a>)

President Biden announces $6.1 billion relief for 315,000 affected by the Art Institutes, forgiving debt due to deceptive practices; tax implications uncertain; keep watch for potential relief at other institutions.

</summary>

1. "We will never stop fighting to deliver relief to borrowers."
2. "I have no idea how that works and there's no way we're going to be able to basically give advice on that."
3. "Just keep watching."
4. "Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

President Biden announced $6.1 billion in relief for 315,000 people affected by the Art Institutes.
Debt from attending the Art Institutes between 2004 and 2017 may be forgiven automatically.
The Art Institutes are accused of falsifying data, misleading students, and burdening them with debt without promising career prospects.
This relief option, known as borrower defense to repayment, is not a new program.
Questions about the forgiven debt being taxed are unanswered; individuals are advised to consult a tax professional.
Around $20,000 per person is being forgiven in this latest round of relief.
Over $160 billion has already been forgiven, impacting 4.6 million people.
Another institution may see similar relief in the future, although details are scarce.
Beau suggests keeping an eye out for updates on potential relief for other affected institutions.
The forgiven debt is specific to individuals impacted by the Art Institutes' deceptive practices.

Actions:

for taxpayers, student loan borrowers,
Keep an eye out for updates on potential relief for other affected institutions (suggested)
Consult a tax professional regarding the tax implications of forgiven debt (suggested)
</details>
<details>
<summary>
2024-05-02: The Roads to touching our boats and foreign policy.... (<a href="https://youtube.com/watch?v=6DRdI_VVpXE">watch</a> || <a href="/videos/2024/05/02/The_Roads_to_touching_our_boats_and_foreign_policy">transcript &amp; editable summary</a>)

Beau explains the historical and ongoing significance of the "Don't Touch My Boats" meme, linking it to the U.S.'s foreign policy focus on projecting power through boat protection.

</summary>

1. "It's foreign policy. It is always, always about power."
2. "The United States is, in fact, really weird about its boats because for a very long time that was power."
4. "Is the U.S. really that crazy about other countries touching its boat? Yes."
5. "The ability to have your flag, your vessel show up somewhere, especially a military one, is power."

### AI summary (High error rate! Edit errors on video page)

Explains the "Don't Touch My Boats" meme and its significance in foreign policy.
Mentions how the meme portrays the U.S. Navy protecting its boats like a toddler with plastic boats.
Traces back historical events where the U.S. reacted strongly when its boats were touched, leading to wars.
Covers examples from the Revolutionary War to modern incidents like Operation Praying Mantis and conflicts with the Houthis.
States that power is the driving force behind the U.S. protecting its boats, as it symbolizes projection of power and control over shipping routes.
Attributes the U.S.'s fixation on its boats to its geographical separation from other world powers by oceans, making the Navy a key part of foreign policy.
Emphasizes that even today, the protection of boats and shipping routes signifies power and projection of that power in foreign policy.
Connects the importance of boats to American military might, especially aircraft carriers.
Concludes that the U.S.'s sensitivity about its boats is deeply rooted in its historical approach to power projection through naval strength.
Summarizes that the meme's resonance lies in its truth about the U.S.'s historical and continued emphasis on boat protection as a display of power in foreign policy.

Actions:

for history buffs, policymakers, scholars,
Study historical events related to boat protection in U.S. foreign policy (suggested)
Analyze current conflicts involving the protection of shipping routes (suggested)
</details>
<details>
<summary>
2024-05-02: Let's talk about the US, Ukraine, Russia, and chemicals.... (<a href="https://youtube.com/watch?v=cLOqyuYZp3w">watch</a> || <a href="/videos/2024/05/02/Lets_talk_about_the_US_Ukraine_Russia_and_chemicals">transcript &amp; editable summary</a>)

Beau addresses the use of tear gas by Russia in Ukraine, explaining the chemicals used and the potential implications, suggesting that it may not lead to significant consequences.

</summary>

1. "Russia uses chemical weapons in Ukraine."
2. "It looks like CS, CN, and PS. are CS and CN. They are not dissimilar from things that have been deployed at universities in the United States."
3. "But I wouldn't let the headlines worry you too much on this one."

### AI summary (High error rate! Edit errors on video page)

Addresses the headline: "Russia uses chemical weapons in Ukraine."
Confirms that it appears to have happened.
Explains that tear gas, specifically CS, CN, and PS, were used.
Notes that these chemicals are not supposed to be used in combat.
Describes the purpose of using these chemicals: to dislodge Ukrainian forces from fortified positions.
Mentions the risk involved with the use of tear gas in such situations.
Predicts that the US will likely impose more sanctions in response.
Believes this incident may not escalate into a large-scale conflict.
Indicates that although using tear gas is against accepted practices, it may not lead to significant consequences.
Emphasizes that despite the seriousness of the situation, it may not become a major issue.

Actions:

for global citizens,
Monitor the situation and stay informed about developments (implied)
Advocate for diplomatic solutions to international conflicts (implied)
Support organizations working towards peace and conflict resolution in the region (implied)
</details>
<details>
<summary>
2024-05-02: Let's talk about piers, plans, and projections.... (<a href="https://youtube.com/watch?v=Gzche34VyiE">watch</a> || <a href="/videos/2024/05/02/Lets_talk_about_piers_plans_and_projections">transcript &amp; editable summary</a>)

Beau explains the nuances of the pier's operational status off the coast of Gaza and advises managing expectations regarding aid delivery.

</summary>

1. "Being operational this weekend does not mean that people will be eating this weekend."
2. "You might have food going into Gaza middle of next week."
3. "It's the best to manage expectations."
4. "That is a true enough statement."
5. "Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains the context of the information regarding the pier being operational off the coast of Gaza
Acknowledges that the pier being operational doesn't necessarily mean immediate aid delivery to Gaza
Mentions the potential delays in getting food into Gaza even if the pier becomes operational this weekend
Mentions the opening of at least one gate providing access to the northern part of Gaza for the first time since the seventh
Notes the uncertainty about the future openings of the gate and the aid flow through it
Suggests that operational security concerns may prevent the release of firm dates for the pier's operation
Advises managing expectations regarding the developments related to the pier and gate in Gaza
Emphasizes that just because the pier is operational, it doesn't guarantee immediate aid distribution
Raises awareness about the process of offloading food onto the pier and the additional steps needed for aid distribution
Concludes by wishing everyone a good day

Actions:

for humanitarian aid supporters,
Monitor updates on aid delivery to Gaza (implied)
Stay informed about the situation in Gaza and potential delays in aid distribution (implied)
</details>
<details>
<summary>
2024-05-02: Let's talk about Biden's statements about the universities.... (<a href="https://youtube.com/watch?v=_p7tjOjBTm4">watch</a> || <a href="/videos/2024/05/02/Lets_talk_about_Biden_s_statements_about_the_universities">transcript &amp; editable summary</a>)

Beau dissects President Biden's concise responses on protests and National Guard intervention, stressing honesty and the governors' role in handling such situations.

</summary>

1. "That's the entirety of the answer."
2. "That should not be a goal of them because that's not going to occur."
3. "That is not what that means."
4. "They're honest."
5. "Do not take his statements to be dismissive."

### AI summary (High error rate! Edit errors on video page)

President Biden's short and honest responses to questions regarding protests and the National Guard.
Biden's response indicated that protests wouldn't change his decision-making in foreign policy.
The question about National Guard intervention was met with a straightforward "no" from Biden.
The ability to call out the National Guard lies with the governors, not solely the president.
Beau suggests that immediate federal intervention may not be the most suitable response to protests.
Using excessive force like a security clampdown might exacerbate tensions in protest situations.
Handling demonstrations through establishment means is emphasized.
Beau advises against interpreting Biden's statements as dismissive, rather as honest.
The role of governors in calling out the National Guard is underscored.
Beau concludes by urging viewers not to assume Biden's stance precludes National Guard intervention.

Actions:

for activists, concerned citizens,
Contact local governors to understand their stance on National Guard intervention (implied).
Join community-led efforts to address protests and advocate for peaceful resolutions (generated).
</details>
<details>
<summary>
2024-05-02: Let's talk about AZ, changes, and what's next.... (<a href="https://youtube.com/watch?v=pQKLe8nsfeY">watch</a> || <a href="/videos/2024/05/02/Lets_talk_about_AZ_changes_and_what_s_next">transcript &amp; editable summary</a>)

A court ruling in Arizona reinstated a law banning reproductive rights, but despite its repeal, reproductive rights remain a major issue set to impact the 2024 election.

</summary>

1. "You are to be ruled, not represented."
2. "Even though this has been resolved, it is still incredibly likely to end up being a major issue and a deciding factor in the 2024 election."

### AI summary (High error rate! Edit errors on video page)

A court ruling in Arizona reinstated a law from 1864 that essentially banned reproductive rights.
The Arizona House and Senate have repealed this law, which is expected to be signed by the governor soon.
Due to a unique structure, the repealed law will still be in effect for a period of time before being replaced by a 15-week ban.
Most Republicans voted against repealing the law, with only two Republican votes in the Senate and three in the House supporting its repeal.
There will likely be a ballot initiative in November regarding reproductive rights in Arizona.
Reproductive rights are expected to be a major issue in the 2024 election.

Actions:

for arizona residents, reproductive rights advocates,
Support and get involved in ballot initiatives dealing with reproductive rights in Arizona (implied)
</details>
<details>
<summary>
2024-05-01: Let's talk about bison and dogs and bears.... (<a href="https://youtube.com/watch?v=YbKMP8Q0FsM">watch</a> || <a href="/videos/2024/05/01/Lets_talk_about_bison_and_dogs_and_bears">transcript &amp; editable summary</a>)

Beau addresses animal incidents and responsible dog ownership while sharing tips on interacting with wildlife, ending with a wish for a good day.

</summary>

1. "Do not feed the giant hamsters, those are bears, and do not kick the fluffy cows."
2. "The animals, they're not domesticated at all, doing that is not a good idea."
3. "Those dogs were trained to do those tasks long before the invention of an electric collar."
4. "If you ever find yourself in that situation, it would probably be best to rehome the dog."
5. "Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Addressing internet people, discussing animals and a recent news item about a man who kicked a bison at Yellowstone, resulting in minor injuries.
Advising against feeding bears and kicking bison due to their potential to cause serious harm.
Mentioning a separate animal-related incident involving the governor of South Dakota, but not delving into it deeply.
Mentioning the training process for certain dog breeds and the responsibility of owners in training and behavior management.
Encouraging responsible dog ownership and rehoming if needed, rather than resorting to extreme measures.
Signing off by wishing everyone a good day.

Actions:

for animal lovers and responsible pet owners.,
Refrain from feeding wild animals or engaging with them in a harmful manner (implied)
Ensure proper training and care for working dogs to avoid behavior issues (implied)
Prioritize responsible pet ownership and rehoming if necessary (implied)
</details>
<details>
<summary>
2024-05-01: Let's talk about Trump, orders, delays,  and denials.... (<a href="https://youtube.com/watch?v=cIA6BxlMPPw">watch</a> || <a href="/videos/2024/05/01/Lets_talk_about_Trump_orders_delays_and_denials">transcript &amp; editable summary</a>)

Beau provides an overview of the legal proceedings involving former President Donald J. Trump in New York, including violations of a gag order, appeals, and a victory to attend a family function, debunking manufactured outrage along the way.

</summary>

1. "The outrage of the week was completely over nothing, totally manufactured."
2. "Everything else was a loss for him today."
3. "Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Overview of the legal proceedings involving former President Donald J. Trump in New York.
Judge found Trump violated the gag order nine times, resulting in a $9,000 fine.
Judge expressed dissatisfaction with the fine and hinted at the possibility of jail time for wealthy defendants.
Trump's appeal to have the judge removed from the case and to halt proceedings based on claims of presidential immunity was denied by the court.
Right-wing media misrepresented a judge's decision regarding Trump attending a family function.
Contrary to the outrage manufactured by some, Trump was granted permission to attend the graduation.
The focus inside the courtroom is on providing detailed information.
The judge's stance on potential jail time for Trump remains uncertain.
The manufactured outrage surrounding the judge's decision was baseless.
Trump's only victory was being allowed to attend the graduation.

Actions:

for legal observers,
None mentioned in the transcript.
</details>
<details>
<summary>
2024-05-01: Let's talk about Louisiana and a unique situation with the maps.... (<a href="https://youtube.com/watch?v=WfOjR2kpaH4">watch</a> || <a href="/videos/2024/05/01/Lets_talk_about_Louisiana_and_a_unique_situation_with_the_maps">transcript &amp; editable summary</a>)

Louisiana faces a unique political situation as Republicans navigate unexpected support for a less gerrymandered map amidst legal challenges, potentially altering election outcomes and partisan dynamics by the May 15th deadline.

</summary>

1. "Louisiana Congressional maps were gerrymandered, diluting the power of the black vote."
2. "Republicans in Louisiana agreed to create another majority black district, but faced legal challenges."
3. "The court ruled against using the new map, leading to potential Democratic gains and a loss for Republicans."
4. "Republicans supporting a less gerrymandered map is unexpected in this situation."
5. "The May 15th deadline for choosing a map adds urgency to the unfolding events."

### AI summary (High error rate! Edit errors on video page)

Louisiana Congressional maps were gerrymandered, leading to a dilution of the black vote.
Typically, in the South, Republicans either challenge the court ruling or ignore it when faced with fixing gerrymandered maps.
Surprisingly, Republicans in Louisiana agreed to create another majority black district to comply with the court's order.
Despite passing the new map, a group of non-African American voters challenged it in court, claiming it violated the Voting Rights Act.
The court agreed with the challengers and ruled against using the new map with the second majority black district.
This decision likely means a Democratic win in the upcoming elections and a loss of a House seat for the Republican Party.
Republicans in Louisiana seem to be supporting the less gerrymandered map, even though it goes against their usual behavior.
The deadline for deciding which map to use is May 15th, with a potential direct appeal to the Supreme Court expected soon.
The situation is unique as it defies the typical dynamics seen in Southern politics.
Both Republican and Democratic members are unhappy with the recent ruling, making this a significant development to follow closely.

Actions:

for louisiana residents, political activists,
Contact local representatives to express support for fair redistricting (suggested)
Stay informed about the legal developments and potential Supreme Court appeal (implied)
</details>
<details>
<summary>
2024-05-01: Let's talk about Dems protecting Johnson.... (<a href="https://youtube.com/watch?v=7LgCNRtOkew">watch</a> || <a href="/videos/2024/05/01/Lets_talk_about_Dems_protecting_Johnson">transcript &amp; editable summary</a>)

The Democratic Party faces strategic choices regarding Speaker Johnson, with options ranging from helping Marjorie Taylor Greene to protecting Johnson and potential consequences on party dynamics.

</summary>

"Is this the right move for the Democratic Party?"
"But if it happens again and they come out and say, the Democratic Party, they were just intractable, even after we compromised on the aid package or whatever they use, that might shift the blame to the Democratic Party."
"At this point, we'll have to wait and see what actually occurs."
"It's just a thought, y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

The US House of Representatives and the Democratic Party are helping Johnson retain his position as Speaker of the House.
The Democratic Party is choosing to protect Johnson and keep him as Speaker of the House.
Beau presents the options the Democratic Party had: trying to get Speaker Jeffries, doing nothing, helping Marjorie Taylor Greene, or protecting Johnson.
If the Democratic Party does nothing, it may result in chaos and gridlock in the House.
Helping Marjorie Taylor Greene may give a win to the MAGA faction and Trump, creating chaos and uncertainty.
Protecting Johnson could weaken him in the Republican Party, especially among the Twitter and MAGA factions.
By allowing Johnson to keep his position, the Democratic Party may generate some goodwill, but it weakens Johnson within his own party.
Beau suggests that ensuring Johnson does not become like McConnell should be a high priority for the Democratic Party.
Using a procedural method to address the situation discreetly seems to be the course the Democratic Party is leaning towards.
Beau concludes by expressing uncertainty about the final decision and advises waiting to see how things progress.

Actions:

for democratic party members,
Monitor the developments within the Democratic Party regarding Speaker Johnson (implied)
Stay informed about procedural decisions related to retaining Speaker Johnson (implied)
</details>
