---
title: Let's talk about Biden's statements about the universities....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=_p7tjOjBTm4) |
| Published | 2024/05/02 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Biden's short and honest responses to questions regarding protests and the National Guard.
- Biden's response indicated that protests wouldn't change his decision-making in foreign policy.
- The question about National Guard intervention was met with a straightforward "no" from Biden.
- The ability to call out the National Guard lies with the governors, not solely the president.
- Beau suggests that immediate federal intervention may not be the most suitable response to protests.
- Using excessive force like a security clampdown might exacerbate tensions in protest situations.
- Handling demonstrations through establishment means is emphasized.
- Beau advises against interpreting Biden's statements as dismissive, rather as honest.
- The role of governors in calling out the National Guard is underscored.
- Beau concludes by urging viewers not to assume Biden's stance precludes National Guard intervention.

### Quotes

1. "That's the entirety of the answer."
2. "That should not be a goal of them because that's not going to occur."
3. "That is not what that means."
4. "They're honest."
5. "Do not take his statements to be dismissive."

### Oneliner

Beau dissects President Biden's concise responses on protests and National Guard intervention, stressing honesty and the governors' role in handling such situations.

### Audience

Activists, concerned citizens

### On-the-ground actions from transcript

- Contact local governors to understand their stance on National Guard intervention (implied).
- Join community-led efforts to address protests and advocate for peaceful resolutions (generated).

### Whats missing in summary

The nuances of Beau's analysis and commentary on the political responses to protests and potential National Guard involvement.

### Tags

#PresidentBiden #Protests #NationalGuard #Honesty #Governors


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about President Biden
and a couple of questions that he received
and his incredibly short answers to those questions.
Then we're going to kind of talk about what he said.
And then we're going to talk about the coverage that
has emerged since those answers were given.
OK.
So to start off with, we'll start at the beginning,
the first question that he was asked,
dealing with what's going on at the universities
here in the United States right now.
Have the protests forced you to reconsider any of the policies
with regard to the region?
To which Biden responded, no.
That's the entirety of the answer.
Okay, so that's an incredibly honest answer.
That is an incredibly honest answer.
Probably not how it should have been answered politically,
but when you are talking about foreign policy,
if you want to go back to the really long video
that was answering questions
from people who were at these demonstrations,
don't expect immediate results.
It's a true answer.
No, they're not going to impact his decision-making.
Probably would have been wiser to say something along the lines of, you know,
we listen to everybody and we take everybody's opinion into account and to
consideration and
you know, da-da-da-da-da, but we
defer to our experts.
Probably would have gone over better
than just
no, that's not going to
to alter anything. Again, those demonstrations, that should not be a goal of them because
that's not going to occur. Foreign policy does not turn on a dime. Then the next question,
do you think the National Guard should intervene? Please listen to the phrasing of that. Do
you think the National Guard should intervene? And he said no. You already have headlines
saying that the National Guard will not be used. Once again, the governor can
call out the National Guard. Him saying no here does not mean that the National
Guard isn't going to show up later. That is not what that means.
So as far as that answer, I mean that is what it is, do you think the
National Guard should intervene and he said no. Didn't really expand on it, you
know. He could have said no and I won't do it or anything like that. He just said
no. But again despite what a lot of people who are playing on an anticipated
lack of coverage and explanation, people who are up on Capitol Hill, the governor
has that ability. The governor can call out the National Guard. Doesn't have to
be the president. In fact, really in this situation, it didn't even make
sense initially. Now that there are demonstrations in various states, sure
you could say maybe it makes sense to federalize the National Guard and have
the president do it and blah blah blah blah, but I mean at the end if he did
And that would be a massive overreach, historically speaking.
That's not something that occurs when you're talking about demonstrations like this.
That's up to the governors.
The governors don't want to do it because, well, I mean, it's a bad idea.
If you look, those places where they have gone in and knocked everybody around, it almost
seems like that kind of security clampdown caused them to spread.
It's not actually how you handle that stuff.
You're supposed to handle that stuff if you are on the establishment side.
They don't read their manuals.
So that's what's going on.
Do not take his statements to be dismissive.
They're honest.
They are honest.
And do not take him saying that he doesn't think the National Guard should intervene
to mean that they won't because the governor has the ability to call out the National Guard.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}