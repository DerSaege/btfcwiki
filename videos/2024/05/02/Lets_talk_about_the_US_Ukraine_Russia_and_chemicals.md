---
title: Let's talk about the US, Ukraine, Russia, and chemicals....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=cLOqyuYZp3w) |
| Published | 2024/05/02 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the headline: "Russia uses chemical weapons in Ukraine."
- Confirms that it appears to have happened.
- Explains that tear gas, specifically CS, CN, and PS, were used.
- Notes that these chemicals are not supposed to be used in combat.
- Describes the purpose of using these chemicals: to dislodge Ukrainian forces from fortified positions.
- Mentions the risk involved with the use of tear gas in such situations.
- Predicts that the US will likely impose more sanctions in response.
- Believes this incident may not escalate into a large-scale conflict.
- Indicates that although using tear gas is against accepted practices, it may not lead to significant consequences.
- Emphasizes that despite the seriousness of the situation, it may not become a major issue.

### Quotes

1. "Russia uses chemical weapons in Ukraine."
2. "It looks like CS, CN, and PS. are CS and CN. They are not dissimilar from things that have been deployed at universities in the United States."
3. "But I wouldn't let the headlines worry you too much on this one."

### Oneliner

Beau addresses the use of tear gas by Russia in Ukraine, explaining the chemicals used and the potential implications, suggesting that it may not lead to significant consequences.

### Audience

Global citizens

### On-the-ground actions from transcript

- Monitor the situation and stay informed about developments (implied)
- Advocate for diplomatic solutions to international conflicts (implied)
- Support organizations working towards peace and conflict resolution in the region (implied)

### Whats missing in summary

Analysis of the broader geopolitical implications and potential diplomatic responses to the situation in Ukraine.

### Tags

#Russia #Ukraine #ChemicalWeapons #TearGas #InternationalRelations


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about the United States
and Ukraine and Russia
and that eyebrow-raising headline
that you have either seen already
or will undoubtedly see soon.
And we will go through that
and answer the very obvious questions
that would arise with a headline like that
and then talk about whether or not it really is
as eyebrow-raising as it first appears.
Okay, so if you haven't seen the headline,
what are you gonna see?
Something to the effect of
Russia uses chemical weapons in Ukraine.
Okay, so first question, did that really happen?
Certainly appears so, certainly appears so.
Is it what you were thinking
And when most people hear the phrase chemical weapons in thinking about war, no, that's
not what was deployed.
It's worth noting that it is still seen as totally uncool to use this kind of stuff in
combat.
It's one of those rules.
So what was used?
It looks like CS, CN, and PS.
are CS and CN. They are not dissimilar from things that have been deployed at universities
in the United States. It's tear gas. It's tear gas. Yeah, despite it being used here in the U.S.,
you're not actually supposed to use that stuff on a battlefield. What is PS? Old tiny tear gas basically.
World War I style.
Okay, so why is Russia using it?
I mean, World War I.
Here's the official statement here.
The use of such chemicals is not an isolated incident and is probably driven by Russian
forces' desire to dislodge Ukrainian forces from fortified positions and achieve tactical
gains on the battlefield.
chase them out of their trenches. I mean it's a little bit more than that, but
that's what it's being used for. So probably not what people are
picturing when they see that headline. But again it is worth noting a couple of
things. One, this is not an accepted practice. And two, it's not without risk.
In some situations, if the person cannot get out, they won't make it. There is a
lot of risk with this stuff.
So what happens from here?
Realistically, the US is going to throw on some more sanctions.
Probably not much beyond that.
This probably isn't going to amount to a large-scale thing, and I doubt it would trigger a back
and forth of use of riot control agents on the battlefield.
That seems unlikely as well.
This is one of those things where, yes, you're not supposed to do this.
Yes, it's against accepted practices and rules.
But generally speaking, if it's being used on a battlefield, there's already a war.
This is probably going to be, I don't want to say ignored, but it's probably not going
to turn into a giant thing.
I think the first allegations about this that I heard were January, February, somewhere
in there.
I mean, it's taken that long for them to just produce the finding on it, so this is
not a high priority for them. But I would not let the headlines worry you too much on
this one.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}