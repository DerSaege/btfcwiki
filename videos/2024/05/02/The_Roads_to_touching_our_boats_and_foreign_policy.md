---
title: The Roads to touching our boats and foreign policy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=6DRdI_VVpXE) |
| Published | 2024/05/02 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the "Don't Touch My Boats" meme and its significance in foreign policy.
- Mentions how the meme portrays the U.S. Navy protecting its boats like a toddler with plastic boats.
- Traces back historical events where the U.S. reacted strongly when its boats were touched, leading to wars.
- Covers examples from the Revolutionary War to modern incidents like Operation Praying Mantis and conflicts with the Houthis.
- States that power is the driving force behind the U.S. protecting its boats, as it symbolizes projection of power and control over shipping routes.
- Attributes the U.S.'s fixation on its boats to its geographical separation from other world powers by oceans, making the Navy a key part of foreign policy.
- Emphasizes that even today, the protection of boats and shipping routes signifies power and projection of that power in foreign policy.
- Connects the importance of boats to American military might, especially aircraft carriers.
- Concludes that the U.S.'s sensitivity about its boats is deeply rooted in its historical approach to power projection through naval strength.
- Summarizes that the meme's resonance lies in its truth about the U.S.'s historical and continued emphasis on boat protection as a display of power in foreign policy.

### Quotes

1. "It's foreign policy. It is always, always about power."
2. "The United States is, in fact, really weird about its boats because for a very long time that was power."
3. "That's what the meme says in a foreign policy context."
4. "Is the U.S. really that crazy about other countries touching its boat? Yes."
5. "The ability to have your flag, your vessel show up somewhere, especially a military one, is power."

### Oneliner

Beau explains the historical and ongoing significance of the "Don't Touch My Boats" meme, linking it to the U.S.'s foreign policy focus on projecting power through boat protection.

### Audience

History buffs, policymakers, scholars

### On-the-ground actions from transcript

- Study historical events related to boat protection in U.S. foreign policy (suggested)
- Analyze current conflicts involving the protection of shipping routes (suggested)

### Whats missing in summary

The detailed historical context and examples that provide a thorough understanding of the U.S.'s focus on protecting its boats as a symbol of power projection in foreign policy.

### Tags

#ForeignPolicy #USNavy #PowerProjection #BoatProtection #HistoricalContext


## Transcript
Well, howdy there, internet people.
It's Beau again, and welcome to the Roads with Beau.
Today is going to be the roads to not touching the boats
of the United States.
The roads to understanding a meme, basically.
A whole bunch of questions have come in
about this particular meme.
And it gives us a good jumping off point
to talk about a foreign policy principle
that we have discussed a lot, and to showcase exactly how
long it's been a guiding principle of foreign policy.
It's called the Don't Touch My Boats meme.
If you haven't seen it, normally it's skits, and they're normally pretty similar.
A country or a non-state actor says that it is going to touch U.S. boats, it's going
to someway impede them, or impede shipping, or whatever.
And the person that is portraying the U.S. Navy is like, don't touch my boats, acting
like a toddler in a kiddie pool with little plastic boats.
Generally speaking, after that, a map of Japan or maybe the Japanese flag shows up and it's
like, don't touch their boats.
This resonates for a whole bunch of reasons, but partially because there's a pretty big
element of truth in it.
So here are some of the questions.
What's up with the don't touch our boats meme?
Is the US really that crazy about other countries touching its boats?
Why is the US so weird about its boats?
I don't get the don't touch our boats meme.
Any country would respond to Pearl Harbor that way.
And that's true.
countries would respond to Pearl Harbor in a in a pretty pronounced fashion.
And that is definitely, you know, what brought the U.S. into World War II.
What brought the U.S. into World War I?
Okay let's just go back to the beginning.
The Revolutionary War.
You can't really count it because it's not the U.S. yet, right?
there's definitely some spillage that occurs in as far as the reasons, and it
has to do with the shipping. Don't touch the boats. The American-Algerian War
1785 to 1795, what started it? They touched our boats. The Quasi War between
the American and the French. It was really about loan repayment, but it got
hot when they touched our boats. First, Barbary War. To the shores of Tripoli,
they touched our boats. War of 1812. The tensions were already there, but
impressment. They touched our boats. Second, Barbary War. They touched
our boats. I think the point's been made. Let's just go and hit the ones that
people are more familiar with. Spanish-American War wasn't actually what
started the war, but do you remember the Maine? We thought they touched our boats.
World War I, the US got involved because of the Lusitania. Again, not the cause,
but it definitely shifted public opinion in the United States. And keep in mind
mind, that wasn't even a U.S. boat, but there were 128 Americans on board.
World War II, obvious, Vietnam, Gulf of Tonkin incident.
If you haven't looked into that recently, you might want to do so, you might believe
some things about that that aren't entirely accurate.
It's clear that this is an overriding theme, and it goes on to this day.
Operation Praying Mantis, the tanker wars in general, I mean what's going on right
now with the Houthis?
A lot of don't touch our boats going on.
Okay so the meme is real, it's true, why?
Why does this happen?
foreign policy. Why does anything always happen? Power, right? It's about power.
Votes, especially back in the day, that was how a country projected its power,
showed it. And keep in mind, there's way more incidents.
The ability to have your flag, your vessel show up somewhere, especially a military one,
is power.
It's a projection of that power.
Aside from that, the shipping itself gives you power coupons, the money.
That has a lot to do with it.
Protecting shipping protects the cash, the cash that is then turned into power, because
that's what it's really about, right?
But this is true of all world powers.
Why is the United States so incredibly weird about their boats?
Because we're separated from all of the other world powers by two oceans.
So the Navy, it became an integral part of U.S. foreign policy from a very early point.
And it still is today.
It's about being able to project that power.
That's what the meme says in a foreign policy context.
Now when you look at all of the memes out there and all the little skits and the jokes,
most of them are not actually about the foreign policy aspects.
But one of the reasons it resonates is because it's true.
The United States is, in fact, really weird about its boats because for a very long time
that was power.
That was how the United States achieved and projected power, gunboat diplomacy.
The shipping was incredibly important to protect.
It's like many things with foreign policy.
If you follow it down to its root, it's about power, the ability to project it.
Even today, when you really think about it, what is one of the most recognizable symbols
of American military might?
An aircraft carrier.
The same people who make the jokes about, you know, don't touch our boats, also point
out that for most countries an aircraft carrier or at least a carrier group is
enough to topple it. It's about power. So to answer the questions, what's up
with the don't touch our boats meme? There you go. Is the U.S. really that
crazy about other countries touching its boat? Yes. And why? Because it's power.
It's foreign policy. It is always, always about power. So there you go. A little more
information, a little more context, and having the right information will make
Make all the difference.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}