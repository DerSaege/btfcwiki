---
title: Let's talk about Trump being worried about Junior....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=KBkIJgQG1RM) |
| Published | 2024/05/13 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump
and a video that Trump released on social media
that definitely gave us some insight
into something that he might have some concerns about
because he went on at length about something.
And it certainly seemed to indicate that, well, he's a little worried.
He posted a video talking about RFK Jr.
And honestly, I don't know that I've ever wanted to just play a video for y'all before
like this.
It opens up by calling him a Democrat plant, a radical left liberal who's been put in
place to help Biden.
It goes on to say that RFK Jr. is like the most liberal leftist person.
He wants open borders so people from mental institutions can come in.
He went on a rant, within a rant, talking about RFK Jr.'s VP pick and basically called
her a gold digger.
He didn't use those terms.
He just constantly talked about the money that she got from her ex-husband.
And to be clear, I don't even know if that's true.
It's Trump, so he could have just made it up.
And then it gets into this interesting part where he talks about the fact that Junior's
not really an anti-vaxxer, so don't vote for him for that reason.
He called basically the entire Kennedy family lunatics was, I believe, the word he used.
And he said, Republicans, get it out of your mind that you're going to vote for this guy
because he's conservative.
She's not.
It is the most panic-ridden, pleading thing that I have seen come from Trump.
It appears that somewhere in Trump world, the idea is starting to circulate that, well,
Junior's taking more votes from him than he is from Biden.
clue if that's true, but it certainly appears that Trump believes that. Because Trump, he
definitely seemed scared. I mean, that's the word I would use. If I saw somebody talking
in this manner about anything else, I would assume that they were truly concerned, that
They were scared about something.
So there may be a worry within Trump world that some of his base, the base that believes
in all the theories and everything, are leaning towards going to RFK Jr.
And just as another measure of how bizarre this rant is.
video is shorter than it. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}