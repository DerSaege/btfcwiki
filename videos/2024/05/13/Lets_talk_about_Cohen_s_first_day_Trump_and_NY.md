---
title: Let's talk about Cohen's first day, Trump, and NY....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=wDk7LpHGOtg) |
| Published | 2024/05/13 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about New York
and Trump, Cohen, and expectations,
and whether or not they've been met.
And I mean, the short version is, yeah,
that's what has widely been anticipated
certainly appears to be what's happening.
So if you don't know,
Cohen finally got up there today and he took the stand this morning and so far everything
has been, has anticipated.
He is the beginning of the tell him what you told him phase, putting everything into context,
put it back into a story for the jury.
They have gone over text messages, call records, documents, and Cohen has basically testified
that the whole goal of everything was to keep it separate from Trump.
And he also said at one point that the reason they were delaying something was to try to
get it done after the election because it would have been bad for the campaign.
All of this is what was anticipated.
It's what was expected.
It does appear that the prosecution is hitting the points that they need to.
You have to remember we don't have cameras in the courtroom.
This is all based off of people posting to social media or live vlogging and stuff like
that.
There has been a little bit about Trump's reactions to Cohen's testimony, and a lot
of it seemed to indicate that Trump was not pleased.
However, that's very subjective.
It's based on the interpretation of the person watching it.
So we don't know how accurate that is.
But it has all gone as was expected.
They're summing everything up.
They're putting it back into a story.
Now when this is over, then Trump's team gets to go over everything again and present
their side.
And then it's at that point that it starts to head towards the jury.
The prosecution appears to feel very confident that they have made their case, that they
have presented their side.
We'll see what occurs once the defense tries to start poking holes in it and see how it
plays out from there.
But so far today, no real surprises.
In fact, it might be because I've heard a lot of this before, but almost boring.
It's not...
There weren't any major theatrics.
It's just a story, a chain of events, as is being presented by the prosecution.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}