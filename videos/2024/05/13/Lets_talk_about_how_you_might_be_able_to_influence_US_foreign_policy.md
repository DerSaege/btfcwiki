---
title: Let's talk about how you might be able to influence US foreign policy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=sahS0lPM3tM) |
| Published | 2024/05/13 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about unusual ways
that the average person in the United States
might be able to influence foreign policy.
We're gonna do this because I got a message
with a really cool question in it.
The message is really long.
I'm not going to read the whole thing,
But in relevant part, it says,
I've realized how much influence regional power allies have
over US foreign policy.
And then it goes on, this person is definitely
more concerned about Saudi Arabia having influence
than what most people are right now.
And then the next relevant piece,
in the video where you were answering the questions
from the college girl and talking about all the different things that influence U.S. policymaking,
I became a little depressed because, as you say, it's complicated.
But then I thought, because there's so many factors, maybe there's a way for average everyday
people like me to do something to modify one of those factors to influence foreign policy.
So my question is, is there something the average person can do to get the U.S. out
of the Middle East? Yes. Yes, there is. And you're gonna laugh. I think a lot of
people are gonna laugh when I say it, but it's true. And it's two words. Go green.
If you want to get the U.S. out of the Middle East, go green.
Why is the U.S. in the Middle East?
Why is it so important to U.S. foreign policy?
And everybody right now is saying oil, right?
I mean, it's more complicated than that.
It's foreign policy.
It's never that simple.
But I mean, yeah, that's kind of like a big part of it.
I don't think I have to go into a lengthy explanation of that.
We're talking about the U.S., there's jokes about it.
That chicken's so oily, the U.S. drew up plans to invade it.
But you still have to know why.
Why does the U.S. care?
Because U.S. infrastructure runs on oil, and though we have our own supplies, the oil market
is global.
If something happens on the other side of the world that disrupts oil there, it impacts
U.S. prices, which means everything goes up and it hurts the economy.
That's why the U.S. keeps such a close eye on the Middle East, because there's a whole
lot of oil there that has a whole lot to do with it.
So if the US infrastructure, meaning your car, didn't require as much oil, it becomes
less important.
The disturbances might still occur, but they're not something that would be a national security
risk because they disrupt the entire economy.
You go green.
Now certainly people are going to say there is more to it, and there is, and there's
two main things that people are going to say.
The first is, don't touch our boats.
The Suez Canal, right, that can be done from the med.
The US doesn't need a physical presence in the Middle East to secure the Suez.
That can be done from ships.
other is extremism. I'm going to suggest that if maybe the US wasn't taking such
let's call it an active role in the Middle East there there might not be as
much animosity. Kind of one of those out of sight, out of mind kind of things. Now
Now would that get the U.S. out of the Middle East totally?
No, because you still have other issues.
Like you have states trying to become nuclear states.
And the U.S. would still try to check that.
But it would be more like what the U.S. does in regards to North Korea than what you've
seen in the Middle East for decades.
If you want to get the U.S. out of the Middle East, go green.
Because what you're doing, you're not really influencing foreign policy, I mean you are,
but you're doing it in a very roundabout way.
The reason U.S. foreign policy is what it is, is because of the demand.
If you remove or significantly reduce that demand, foreign policy will change because
it's no longer as important. Go green. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}