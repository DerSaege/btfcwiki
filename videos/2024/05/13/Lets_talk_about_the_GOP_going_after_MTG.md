---
title: Let's talk about the GOP going after MTG....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ndpx1kskvC0) |
| Published | 2024/05/13 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Marjorie Taylor Greene
and some of the developments that have occurred
since her attempt to remove the Speaker of the House
and just kind of check in to see how that's going.
So short version here, there are a number of Republicans
are getting behind a growing push to in some way punish the space laser lady for her attempted
ousting him.
Republicans have said that she is not the face of the Republican Party, that she's
not really representative of the Republican Party.
That's right, Marjorie Taylor Greene has become a rhino.
And they've also indicated that they think that all she wants is attention.
So there is some kind of consideration going when it comes to doing something to punish
her in some way.
enough, it doesn't actually stop with Marjorie Taylor Greene. There have also
been suggestions of some kind of disciplinary action against those
Republicans, the 11 I think, that voted in favor of it just as a whole. There have
been suggestions of removing them from important committee assignments and so
on. It's important to remember that Marjorie Taylor Greene, for whatever
reason, is central to the Twitter faction, MAGA faction. The move to punish her,
the move to engage in some kind of disciplinary action, is indicative of
that faction really losing its relevance. So it does appear that Johnson is
really starting to take control of the House and doing so at the expense of the
Twitter faction and the MAGA faction. They're probably not gonna like that. So
So if the Democratic Party was hoping to spark a little bit of feuding within the Republican
Party in the House, I would say that that's probably going to be successful because it's
one of those things where because of her behavior and some of the people she alienated along
the way, people in DC tend to have long memories and it appears that some of
them are looking to get a little payback. So even though the main portion of the
drama appears to be over, I don't think this storyline has completely ended yet.
My guess is that there's going to be a few more developments before the
attempt to remove Johnson, the MTV, the Motion to Vacate, and Marjorie Taylor-Green
green, end up out of the news cycle. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}