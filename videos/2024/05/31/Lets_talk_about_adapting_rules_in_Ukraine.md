---
title: Let's talk about adapting rules in Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=4fAt0ryxZqw) |
| Published | 2024/05/31 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about the possibility
of adjusted rules for Ukraine.
And we're going to go through what a number of countries
are kind of suggesting, and then we
will talk about the game of, well, what happens next,
and just kind of go over everything.
because eventually this is gonna pick up in coverage
and there will probably be a whole lot of questions about it.
Okay, so what's happening?
France and Germany, along with the NATO secretary general,
they have expressed support for the idea of allowing Ukraine
to use Western supplied equipment
to hit targets deep within Russia.
Now, those targets are limited to include military installations that Russia uses to
fire missiles at Ukraine.
And the quote is, we should not allow them to touch other targets in Russia and obviously
not civilian capacities.
incredibly limited in what they want to authorize. Now, the US response to this
has basically been, well, it's possible we might adapt and adjust the way we look
at things because they don't want to hinder Ukraine's ability to defend
itself. That's basically the US position. The obvious question, is this risky? I
I mean, yeah, yeah, it is.
It could definitely, it could definitely provoke a response.
That response, there's a high likelihood
that any response would be confined to Ukraine.
NATO, NATO is not likely to be directly impacted by it.
Russia is having a hard time going toe to toe with Ukraine.
They're not going to want to engage all of NATO, because NATO has a decided advantage.
Is there a risk?
Yeah, there is.
The thing is that risk is carried by Ukraine, so it would be their call.
It's not like they don't know the risk.
Now, to be clear, when I'm talking about risk, I'm not talking about anything unthinkable.
I'm talking about an intensive response via missiles, something like that.
like that.
It seems unlikely that Putin would use strategic weapons, nuclear weapons.
So this is risky, and it is definitely a step for NATO to suggest, okay, fine, well, it's
time to allow this. That's a pretty big departure. At the same time, as long as
it is limited to the targets they're talking about, the response would be
limited as well. At least that's the theory. I would suggest that this
This is something that the Ukrainians consider carefully because they're the ones carrying
the risk, but at the end of the day, this is their call.
Them having the latitude to do it based on the West, yeah, I mean it makes sense.
It makes sense, but there probably needs to be a lot of discussion about that before it's
done but it is at the end of it it's it's a Ukrainian decision because they're
the ones that have to deal with any potential consequences. Anyway it's just
a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}