---
title: Let's talk about Trump being found guilty and what's next....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=L3iKY4gbL0w) |
| Published | 2024/05/31 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there internet people, it's Bo again.
Welcome to history.
So today we are going to talk about Trump in New York
and the resolution to the New York entanglement.
I am certain that most of you have already heard,
but for those who haven't,
the former president of the United States, Donald J. Trump,
was found guilty of 34 counts.
Now currently, messages are coming in
at the rate of about one every 15 seconds.
Obviously, we are not going to get to all of those today.
So what we're going to do is go over
some of the basic information that everybody wants to know,
and then we'll touch on a few different things.
There will probably be more in-depth videos
about this stuff later.
Okay, so sentencing is currently scheduled to occur on July 11th.
Between now and then, the two sides will make their recommendations to the judge.
Obviously, Trump's team is going to say it needs to be a very, very light sentence.
The prosecution will probably use some kind of guidelines.
During this period, it is also pretty likely that Trump will talk to a probation officer.
that person will kind of develop an independent recommendation for the judge.
Now one of the things that needs to be noted is that from this point forward, Trump's behavior
will influence a lot of things come July 11th.
Now, this case and the outcome of it will also influence other cases down the road.
I understand that there's going to be a whole lot of people on social media and there's
going to be a whole lot of political commentators who are going to try to rile people up, people
who are Trump supporters.
I know some of y'all watched this.
fall for that. Don't fall for that. Don't get in trouble for this man. One thing
that you need to understand is that out of the cases, this was the weakest. It
wasn't weak, but it was the weakest. I'll put a video down below on this. Don't
don't let them rile you up and get you in trouble for him. Now, to the other end
of the spectrum, to all of those people who were saying for quite some time that
this would never occur, it has. So, Trump's behavior unrelated to the cases. I would
imagine that his rhetoric is going to become more inflammatory, he is going to
become more erratic. We may see a lot of major differences in how Trump is
behaving. It's worth paying attention to them. Now, one thing that is worth
remembering is that Trump spent a lot of time casting this as a witch hunt. At
At least at this point, there does seem to be one case of election interference, which
was the underlying allegation here, that has been proven in court.
There's going to be appeals.
There's going to be a lot of them.
He will try to find some appealable issue and work on it.
It is realistically, there are a number of ways he can go about this.
One of them is to just say, okay, well, this has happened and stop dumping so much money
into attorneys to delay the cases.
The other is that he continues on the course that he's on and tries to delay, delay, delay
in the hopes that the American people don't care.
Those are two options, and then the third is that he just goes off the rails, which
is something people need to be aware of.
You need to be ready for it.
Obviously, there are a whole bunch of other questions coming in about how this impacts
various things, and we will go through all of them.
I will try not to dump them all on you in one day, so you just have day after day of
Trump news.
We'll try to space them out.
So give me some time to get to all of those questions.
Anyway, it's just a thought.
have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}