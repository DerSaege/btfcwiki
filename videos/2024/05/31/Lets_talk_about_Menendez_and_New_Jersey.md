---
title: Let's talk about Menendez and New Jersey....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=b89R5ld_Hi4) |
| Published | 2024/05/31 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about
the other major entanglement that's going on right now.
We will talk about Menendez and New Jersey
and Egypt and gold bars and all of that stuff.
And just talk about where that is sitting at the moment
because that's proceeding now as well.
Okay, so for a quick recap, the Democratic senator from New Jersey, Bob Menendez, has
been accused of a scheme that the short version of this is the allegation seems to center
on him taking money from Egypt to then support or help Egypt get aid.
Now he appears to be trying to shift the blame to his wife.
Now one of the things that happened as far as the prosecution is concerned, they wanted
to use a number of messages, texts, emails, stuff like that, that dealt with the aid.
The judge determined that, well, that fell under speech and debate, the speech and debate
clause of the U.S. Constitution, which means they can't use it.
The prosecution described that evidence as critical, so there's no telling how damaging
that's going to be to their case, but they are still attempting to make it. Now, for
reference, this is the one that had all of the images surface of the gold bars
and the wads of cash and all of that stuff. That's this one. Sadly, there are
enough of these going on right now that you kind of have to specify. Now Menendez
has indicated that he is not going to run for re-election as a Democrat.
However, he is kind of indicating that if he is acquitted he might run as an
independent. Now as the trial proceeds, so far there hasn't been any, you know,
giant moment or anything like that. It's moving along. This is a very tedious
paperwork one, which is probably why it's not getting a whole lot of coverage. I
mean, aside from the whole, you know, former president on trial thing. But this
is going to have some, this is going to have some far-reaching impacts depending
how it gets decided. You know as far as Menendez is concerned he's basically
saying hey everybody does what I do and that may be true. You know you don't
know. If he is found guilty of it there will probably be changes to the way
things work up on Capitol Hill. If his statement about this is pretty standard
being true. If that statement is true
and he's found guilty
you're gonna see a lot of changes. If he's found not guilty
you will probably see a more
unique situations like this in the future
but it's moving along and this is one that probably shouldn't
fall out of everybody's memory because as it moves along
it's going to have more and more political consequences.
So anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}