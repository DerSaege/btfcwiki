---
title: Let's talk about a Biden dilemma....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=hu5K-RcUqD4) |
| Published | 2024/05/17 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about Biden and a dilemma.
A dilemma that somebody is facing and we're gonna do this
because I got a message.
It's not a short one,
but it's not an incredibly long one either.
I'm gonna read the whole thing
and then I'm gonna do what it asks.
Okay, so it starts off,
I've watched you since I was 17.
You're the reason I know the difference
between a liberal and a leftist.
And you're the reason I'm aware enough
to even have the dilemma I do.
So I'm coming back to you for advice,
since it really is your fault.
Thanks.
Biden is not your man.
He's not mine either,
but he's the strongest ally I have in federal government
to make sure my birth control isn't in a database.
He's the one that will do the most to protect my trans friends.
He's a real ally to organized labor.
He's done more for climate and student debt than any other president.
And the list goes on and on.
He's not a perfect ally, but he's at least on the bus with us.
But I can't condone what he does with Gaza.
Yes, I'm one of those that says it needs to be simple.
I just want people to stop dying.
How am I supposed to vote for him again?
I can't sell them out, but I also can't sell out my friends here.
No matter what I do, I'll betray something I fill in my soul.
I know your policy, so I know you won't tell me how to vote, but I really liked those videos
you used to do where you connected messages from internet people to historical figures
who faced similar problems.
need one of those right now. Yeah, I don't tell people how to vote. I also don't
tell stories that secretly tell people how to vote either. But you want some
food for thought type of thing. You want a historical figure who at some point
had to deal with an imperfect ally to use your terminology. All of them. All of
All of them have had to do that. Every major historical figure ever. It's a
really common problem. As far as one that I think would be poignant and probably
the most relevant, there was a leader of this country in the 20s and his country
was allied with another country. They'd been allies a pretty long time and if
were to sit down and write out all of the ways that they aligned and all of the ways that they
diverged, the ways they aligned would far outnumber the ways they diverged. However,
one of the ways that they diverged, it really bothered a significant portion of this guy's
country. More importantly, it bothered a portion of the country that was on his
side of the political structure. So he had to make a choice and that choice,
the choice that he made, is the reason you're debating whether or not to vote
for him again.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}