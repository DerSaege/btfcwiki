---
title: Let's talk about the oversight committee disaster....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=a-49-GFi2n4) |
| Published | 2024/05/17 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, I don't know people, let's bow again.
So today, we are going to talk about the US House of Representatives.
In particular, the Oversight Committee and
something that happened there yesterday that I feel like is going to be,
well, pretty much everywhere today.
So what we'll do is go through the chain of events and what was said and
just add a little bit of context to all of the clips you are going to see on
social media today. Okay, so again this is the Oversight Committee, you know, the
committee assigned with, you know, stuff like impeachment inquiries and stuff
like that. So they're having a meeting, it's late in the day, and Marjorie Taylor
Greene asks if anybody in the room was employing Judge Murchon's daughter. This
is the judge from the New York Trump case. Obviously this doesn't have
anything to do with the meeting and Representative Crockett pointed this out.
and said something like, do you know why you're here? Something along those lines.
To which Green replied, I think your fake eyelashes are messing up what you're
reading. If you are not familiar with the rules in the House, yeah, you can't
say something like this. This is out of line. You know, they have a lot of leeway
talking about other members of Congress in this way, it's against the rules. At
this point there is a little bit of an uproar. AOC gets involved and wants to
take down Green's words, which is something that can like lead to a
reprimand basically, and she says, that is absolutely unacceptable. How dare you
attack the physical appearance of another person, to which Green replies,
are your feelings hurt? Comer suspends the meeting and tries just
desperately and in vain to control what's going on and I mean at one point
he's like, come on guys, it just wasn't going well for him. At one point, I guess Green agreed
to strike words but not apologize, something like this. They talked to the parliamentarian,
and by the end of it, things were going back to normal, kind of. It looked like Green was going
going to be able to speak again, but while they were talking to the
parliamentarian it is worth noting that Green, for whatever reason, was begging
AOC to debate her and then said that AOC was not intelligent enough to to debate
Green. Which, I mean, that's funny. But right as things were about to get back to
normal. Crockett says, I'm just curious, just to better understand your ruling. If
someone on this committee then starts talking about somebody's bleach blonde
bad-built butch body, that would not be engaging in personalities, correct? Comer,
the chair is up there and he says what now? Meanwhile the room has erupted in
laughter and a little bit of an uproar there and things just kind of descend
into chaos again with motions to strike and well I mean Comer is up there
basically saying he's doing the best he can please bear with him talking about
his hearing aids it is it was a mess it was a mess you know it was a mess
because after the break Lauren Boebert came back in and apologized to the
American people, because what happened was embarrassing.
Lauren Boebert did that.
I mean when Boebert is the person that is saying, wow, maybe we got out of line on this
one.
That's not a good sign.
So yeah, I feel like these clips are going to be all over the place.
The order of events is probably going to be important.
I feel as though certain media outlets are going to try to villainize Crockett for obvious
reasons.
I mean after what was said to her and then the ruling, I mean to me she's just looking
for clarification in a very unique way.
Again, this is the Oversight Committee.
It is doing wonderfully under Republican leadership, just saying.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}