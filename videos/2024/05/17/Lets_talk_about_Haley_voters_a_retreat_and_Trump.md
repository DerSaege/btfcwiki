---
title: Let's talk about Haley voters, a retreat, and Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=aN3lpuNh-Z0) |
| Published | 2024/05/17 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Nikki Haley.
We're going to talk about Nikki Haley.
I know it's been a couple of months, more than,
since people were really paying close attention to her.
But I figure now might be a good time to just kind of check in
and see what she's up to.
Because, I don't know, maybe something's going on.
So it has been a little more than two months since she
suspended her campaign.
And the media keeps calling her campaign a zombie campaign.
And I get it.
I mean, that makes sense, because it just keeps on
moving along, even though it was suspended.
She's pulling in 22% of votes in some places
during primaries.
I get it because nothing is happening in that campaign, it is a zombie campaign.
See the thing is though, she just had a two-day retreat with her donors this week, this week
in South Carolina, a two-day private retreat, a whole bunch of her donors.
Now that in and of itself, that doesn't necessarily mean anything.
That's not really noteworthy because that could just be keeping those relationships
alive, you know, continuing to talk to people and all of that stuff. That by
itself doesn't mean much. And when you listen to the statements that have come
out from the people who were there and talked about it, it was really kind of
anticlimactic. No big announcement at all. Now that's weird. It seems like there
might be one, but according to the people who were there, she basically just went
over the amount of money that they took in, where it went, kept everybody up to
date on all of that stuff. Now, this would be a good time if one was so
inclined to endorse the presumptive nominee of the Republican Party because
you have all your donors there in one spot. Be a good time to do that. She did
not. She did not. In fact, she told her her audience there that they should vote
their conscience, which is unique because most of the people who were voting for
Haley were Republicans who couldn't in good conscience vote for Trump.
It's also kind of worth noting that she hasn't given Trump access to her donor
network either. I mean, again, none of this necessarily means anything. This
could just be her standing on principle because politicians stand on principle
all the time. There's nothing unusual about that. Or, given the fact that she
gave absolutely no indication, no major announcement about her future either,
didn't say whether or not she was gonna run for president in the future or maybe
tried to get a VP slot, some other political office, leave politics, no announcement whatsoever.
In fact, a lot of the people who left said that it seemed like even she didn't know.
And I was thinking to myself, I was like, you know, that sounds like Haley, totally
without a plan, you know?
Not somebody who spent years cultivating the relationships necessary to run for a higher
office.
I don't know. It seems unique. It seems unique. Again, maybe it's nothing. Or maybe
she's holding back because there is a small part of her that believes the
presumptive nominee may not be able to run. Or maybe it is. Maybe it's just
principle. Yes, it could be. And speaking of principle, when it comes to that, you
know, one in five of the Republican primary voters, it's worth noting that
Trump said, and those voters are all coming to me. He is convinced that people
people will not vote their conscience.
Let's be honest, if you are a Haley voter, if you're a Haley supporter, especially at
this point, you vote that way because you can't in good conscience vote for Trump.
That's the way you feel.
Trump is convinced that your principle doesn't mean as much as your party.
All of y'all are going to vote for him.
He's convinced that Haley voters will put party over country because, let's be honest,
nobody can hear you if you were to answer to the screen.
But I feel like the overwhelming majority of Haley voters that I've talked to believe
that Trump is a genuine risk to the Republic, to democracy in the United States.
And that's the reason that they can't vote for him in good conscience.
Now, most of them don't say that publicly, and you don't see that on social media.
But that seems to be the case.
I am very curious to find out whether or not Trump's right.
And whether or not those Haley voters who she expects to vote their conscience are just
going to vote for the R, regardless of principle.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}