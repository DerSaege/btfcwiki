---
title: Let's talk about a pier in place and what's next....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=gNgP1kCY9LU) |
| Published | 2024/05/17 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, Internet people, it's Beau again.
So today, we are going to talk about the pier because,
according to reporting, the long-awaited pier
is finally in place.
Enough pieces of it are in place to actually start
moving the trucks.
So what we're going to do is we're going to go over
how it functions, what the order of events are.
We'll answer some questions that have already come in
and talk about expectations and goals because apparently they attached a piece to the beach
in Gaza and that was the final piece that they needed to actually start moving stuff.
Alright so how does it function?
Ships will go to Cyprus first.
There the aid, the supplies will be screened and processed.
will move from Cyprus on large ships to a floating platform, be offloaded from
the large ships to the floating platform, and then put on smaller boats. Those
smaller boats will take it to the causeway where the supplies will come
off the smaller boats and go into trucks. Those trucks will then be driven up the
causeway onto the beach by quote third-party contractors. We'll come back
to that. So they go up onto the beach. Once at the beach, it will go to a UN
distribution center, which apparently is going to be ran by the World Food
Program. From there, it will be distributed in Gaza. How that occurs is
outside of DOD. That's not part of the Department of Defense. That is a UN thing
from that point, and there hasn't really been a lot of briefings on that yet.
Okay, so that's the order of events.
Questions.
How soon can they start?
I have seen estimates that say 24 hours.
That seems ambitious to me, but not ridiculously so.
I would be surprised if trucks weren't rolling onto the beach by Monday.
It could very well happen sooner, but I would be surprised if it didn't occur by then.
Now, the next obvious question, how many trucks?
It looks like initially they're hoping that they can run 90 truckloads per day.
And they want, as a goal, to get it up to 150 trucks per day.
And that will, if they can keep that rate up and just constantly push it in and focus
on the right kind of very compact aid, it can do a lot to mitigate the problems that
are already well out of hand.
Now everybody is asking about who's driving the trucks because third-party
contractor is the term that was used.
Does that mean contractor, contractor or just local people that they contracted?
I don't think anybody knows, I mean obviously some people do, but that does
not look like it's information that is coming out anytime soon.
unless there is some kind of leak.
It appears that they are going to try to keep that quiet.
And realistically, the desire to keep it quiet could apply to either.
If you're using locals, you don't really want them identified.
And if you are using contractors, you probably don't want that being discussed either.
So the big question that came in the most, I don't really have an answer for.
Another question that came in was, why are they using this, which is the most complicated
method?
My understanding is that they prioritized making sure that there were no US boots on
ground and the safety of American service members over the speed at which they could
start.
The truckloads per day, that's not really that different than the speedier method of
construction.
It's about the same.
This is safer for U.S. personnel, and that appears to have been the deciding factor.
It has been repeatedly stressed that there will be no U.S. boots on the ground, and this
was just an easy way to make sure that that didn't happen and realistically keep U.S.
service members out of range to reduce the risk of anything happening.
still risk but it is greatly mitigated. If something had gone wrong it probably
would have been seized upon if they had used a simple construction method and
something happened to US forces it would have been seized upon via domestic
politics and the whole thing would have been canceled. So this is their their
method. And then the other questions that have come in are all about moving parts,
so to speak. Yeah, there's a lot of them. That is complicated. That's a lot of stuff
moving and at any point any any link in that chain can break it's going to cause
a delay. I don't have an estimate as to whether or not that's going to occur.
There's no way for me to know that, but I understand the concern because
generally speaking you want to keep things as simple as possible, and this is...
there's a lot of movement of the aid shipments before it ever hits the beach
and transferring vehicles and all kinds of things. So there is room for error.
However, the situation there, it has to be done. I mean the supplies have got to
start getting in. It is way past time for them to start arriving. So I hope they
meet the ambitious goal of, you know, 24 hours. That seems unlikely, but it shouldn't be
much longer.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}