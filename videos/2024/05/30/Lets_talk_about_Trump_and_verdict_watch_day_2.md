---
title: Let's talk about Trump and verdict watch day 2....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=SM-4QrpDl0E) |
| Published | 2024/05/30 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Bill again.
So today we are going to talk about the former president
of the United States, Donald J. Trump,
and the second day of deliberations
in his New York entanglement.
And what we'll do is run through the events of yesterday
and then talk about what the experts,
legal analysts seem to think that those events mean and just kind of go over everything as
things restart.
Okay so yesterday, Trump's New York entanglement, it went to the jury.
Seven men, five women, spent four and a half hours deliberating.
During this period, they asked for two things.
They asked for a readback of a certain section of testimony, and that testimony is reportedly
pretty central to the case.
And they asked for clarification on jury instructions.
That's what they asked for.
And they had not reached a decision yet.
Okay, so what do the experts say?
They say that that kind of means one of two things.
One is that they're deliberating the way a jury should.
Asking for evidence that is central to the case and jury instructions could be indicative
of that.
The other popular opinion is that you have one person who is holding out and the jury
is trying to convince them.
Here's this important piece of testimony.
Here are the jury instructions.
This is why we have to find this way kind of thing.
Now given that information, it's not really a consensus, but I would say that most believe
when you're talking about the legal analysts, the experts.
It seems that most believe that at this point it is unlikely that Trump is acquitted, but
they don't know or they are in conflict as to whether or not they think it will be
a hung jury or a conviction.
So that's where things sit at the moment.
Now Trump, Bing Trump, came out and said that he didn't think even Mother Teresa could
beat those charges.
Said that he didn't even know what the charges were.
All kinds of stuff.
He is very close to finding out whether or not the jury believes that this was the witch
hunt that he proclaimed it to be. We'll continue watching. When news breaks, we
will let you know. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}