---
title: The Roads to a Q&A on verdict watch day....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=dXakGWVyphE) |
| Published | 2024/05/30 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there internet people, it's Beau again,
and welcome to the Roads with Beau.
Today is May 30th, 2024.
And today is a Q&A session, I guess.
So we're putting this out.
This should be lighthearted, at least in theory,
we'll find out as we go through.
And we're putting this out today because it's Thursday,
and because I feel like there's going to be a lot of people
Kind of hanging out online, waiting for some news that
might be coming out of New York today.
So this is here to help you pass the time.
OK.
If you haven't seen one of these before,
these are questions sent in by y'all.
If you ever want to send one in, it is question4boe at Gmail.
OK.
I know you like to do safety PSAs.
Just yesterday it was reported on that muscles on the Oregon coast should be avoided due
to a strange influx of paralytic shellfish poisoning.
Might be something for the oddities on the roads.
Okay, so if you're in the area, I would check that out.
It is worth noting that I am seeing this for the first time, all of these messages.
So I have done zero fact checking on this.
I mean, that sounds bad.
OK.
And then I am supposed to mention,
without going too into detail on it,
that I saw somebody in one of the channel shirts saving
ducks.
I thought that was really cool.
Alright, seem to have a little Henry Kissinger on my shoulder, so the question there is other
than correctly anticipating worst case scenarios, trying to figure out next steps so as to avoid
harm, is there any beneficial side to my mini Henry Kissinger, or is it just not being taken
by surprise by the terribly dreadful?
It's a reduction of surprise, I mean I get this, you understand what's happening foreign
policy wise, you can see it coming, but you can't, in most cases you cannot do much about
it.
Just as a case in point, months ago we ran through like the steps as to what was going
to occur over there and you know mission accomplished banner then they're gonna
have to go back into the north and after that it's mission creep and yeah we're
now at the mission creep part of things that's that's what's going on there'll
be a video about it later but you get to see it coming you just can't do
anything to stop it welcome to the world of Cassandra complex will you do a
live stream when there's a Trump verdict? I don't know. Maybe? I don't know.
For the other segments you want to do on the second channel, why don't you use blank screens
for the people who don't want to be on camera? We've thought about that, actually, and we've
We've talked about it.
The thing is people don't really engage with blank screens or static screens, but this
is something that we've thought about.
And there may be something like that later, just different.
Let's see.
I don't get why Biden opposes the use of US weapons inside Russia while more and more
European governments support it.
Can you explain it?
So again, the idea of creating an existential threat to a nuclear power.
All bad against US doctrine, against European doctrine as well.
The situation that is arising, and there's already a video recorded on this, the situation
that's arising is there are European governments that want to provide very limited permission
to do certain things.
The Biden administration is actually warming to it.
I think it'll probably take a little bit to fully come on board with it.
The idea would be for it to be incredibly limited to specific targets.
It still carries a risk.
It's not a huge one, but again, that nuclear deterrent, it changes a lot of rules and that's
why Russia is being treated the way it is.
If this same thing had been done by a non-nuclear power, NATO would have invaded them.
There's a question that I can't really sum up because it's really long and has a lot
of detail, but the core is, in primaries, should you worry about the electability or
or just vote for the most progressive.
I don't tell people how to vote.
That being said, a piece of information
that might be useful here is that generally speaking,
those people who are the standard centrist Democrat,
the vote blue no matter who people,
They will vote blue no matter who.
If you manage to get a super progressive candidate,
and their odds are most of them are going to vote for them.
You probably won't put a lot of people off that way.
That is important information to have.
Your risk comes from getting somebody
that is so progressive that you lose the independence.
But that doesn't really happen much unless they say something
and they phrase it in a bad way.
That's when that term normally occurs.
You have to remember that most progressive policies are
actually pretty popular with the center, with the
independence, as long as they're phrased properly.
If you were to look into the crystal ball of Bostrodamus, what do you think a post-Putin
Russia would look like?
Well, you'd either end up with another authoritarian goon, or you would have some kind of real
reform in their government, where you have some form of functioning representative democracy.
My money would be on a replacement, more than giant, deep change.
At the same time, you also have the risk of a balkanization.
It's not a huge risk, but it's there, it can't be ignored.
What was your first job?
I grew up a long time ago when children did manual labor and mowed lawns and stuff like
that.
But if you're talking about the first job I had that wasn't for neighbors, I was a
dishwasher.
I was a dishwasher in an Italian restaurant.
I'm Jewish, but I'm horrified at what Israel has been doing in Gaza.
I'm a bit of an outlier in my family.
My grandmother passed away a few days ago.
Sorry for your loss.
And in her obituary, there's a request to make donations in my grandmother's name to
any charity that supports the state of Israel. I don't want to be the person who brings
charged political speech. Is there any way of expressing my dismay at this request without
becoming that guy at a funeral? Do I just keep my mouth shut about it and maybe make
a donation to an organization in Israel that supports children, reforestation, or some
other non-controversial topic, how do I navigate this?
When it comes to events that are of the sort where you don't
want to cause any discord, one thing to remember is that you
don't have to have the conversation at that moment.
You don't have to have it right then.
It could be days later.
If you were so inclined to have that conversation, yeah, you can make a, you can make a donation
that helps children in Israel or whatever.
if you want to honor your grandmother's wishes.
That's, I get the,
I get the conflict.
Just, I don't know that it would do any good.
It might make you feel better to do it then,
But if you're if you're horrified about something you want it to stop, you want
to sway opinion, that may not be the way to do it. It may not be a conversation to
have there, it may be a conversation to have somewhere else, and this could open
the door to it. Again, I mean you're getting into some real sensitive stuff
here. I don't like without knowing your family I don't know that I'd really want
to make a recommendation or give any advice on this one. Just remember there
are there are always options other than you know kicking in the front door.
Hello Bo. I'm trying to remember what bags you suggested in a video you made a few
years ago. That's got to be about medical kits. So the company that I recommended,
they are no longer in business. So even if you find the video, the link isn't
going to help you because they're no longer in business. And now we're at the
point where when you recommend something, you're also cosigning like all of their
politics and everything, what I would do is I would look up the different types of military
medical kits and they all have like an M and then a number.
I would look those up, look at the contents of those and then find a supplier that makes
duplicate or something similar. That's what I would do. There is one, I can't
remember the number, I want to say it's M17, M35, something like that, but it comes
in a, it's a olive drab bag, I mean of course it is, and it's rectangular shaped
and it's flat. It is longer and wider than it is tall and that's that's a
really good one that is relatively inexpensive when you're talking about
medical kits. Have you ever gotten something from a viewer that you haven't
been able to use yet like a shirt or an Easter egg? Oh yeah, tons. I think the
longest running one is... I have two tie-dye shirts. One is like an American
flag and the other one is Curious George. And I haven't, like I haven't found the
perfect opportunity to use those. And I've had those a while and I still
haven't haven't used them. But yeah, that happens where, you know, people send
stuff and I just haven't had a chance to use it.
People have sent stuff from their states.
I have one from Montana and of course there was this period where I had a bunch of news
coming out of Montana and then the shirt arrived and all the news stopped.
What's the best advice you've ever received?
not going to make it. I mean that sounds like a joke but it's not. Advice that led me to
come to the resolute acceptance of my own death. That's the best advice that I've ever
gotten. The kind of advice that leads you to understand that you have a limited amount
of time, so use it well.
How did this not cross a red line in Israel's prosecution of the war in Gaza, according
to the White House?
I'm sure there's more to this message.
The messed up part about it is I don't even know what this is referring to, because it
could be a couple of different things.
So this is what's happened.
The White House established its red line, and the way they did it was talking about
something that was slated to happen, and they said, don't do X.
They still haven't done X. That much is true.
But it's a lot like when Netanyahu's team redesigned it so it wasn't really a major
offensive.
They took the major offensive and divided it up.
So you had a bunch of stuff that was operational, but if you had done it all at the same time
it would be a major offensive.
They gave people exactly what they asked for and nothing that they wanted.
The White House is going to have to come up with better phrasing for its red line.
What is happening is not going to be accepted by the American people, and they are not going
to continue to accept the answer of, well, it's, I mean, it's not really a major ground
offensive.
You know, they're not going to go for that.
They're going, the White House is going to have to come up with something here.
And how they want to phrase it, how they want to do it, I don't know that I have a good
answer for that.
At this point, with the new projection of them being there another seven months, I would
feel like more options for pressure have come onto the table, at least I would hope so.
The purpose of the red line was to mitigate civilian harm.
That was really what it was about.
Nobody cared.
If they could do a major offensive and not harm civilians, nobody would have cared.
They're focusing on the wrong thing.
Why do I feel like you have something big coming for the second channel?
Hey, person reading this, tell me I won't tell.
He always telegraphs what's coming.
All those things people wanted are things he's trying to do, right?
Not all of them.
Am I really that predictable?
Okay.
Question, would you support the idea that if a Martian or an alien from outer space
landed on earth in the USA and had a child, should that Martian baby be considered an
American?
Yes, yes, that's how it works here.
They were born here.
Yeah, no hesitation on that.
Yep, they're an American.
When I saw Nikki Haley suddenly endorse Trump, I couldn't help but be reminded of what happened
back in 2016 to the Democrats, when Bernie Sanders spent the primary attacking Hillary
Clinton, appealing to the Democrats that really hated her.
When he turned around and endorsed her, many in the Bernie block ended up treating him
as a traitor to his own cause and either refused to vote entirely out of protest or even voted
for Trump.
My question is, do you think something similar might happen with the people that wanted to
vote for Haley?
Yes, yeah.
It won't be as big because people who are on the right wing are generally, they are
generally more authoritarian so they are more willing and accepting of taking orders and
that endorsement, even though it really wasn't an endorsement, the implied endorsement, that
That'll sway some people, but I do think there are going to be a lot of people who I feel
like they're going to vote for Biden, or they may go to Kennedy or something like that.
Has talking on the channel affected how you see different issues?
talking about an idea kind of crystallizes it a bit and maybe you realize you see it differently
than you thought you did. If I remember correctly back during 2020 you mentioned discussing UBI
with one of your kids and you mentioned you didn't think it was a very effective tool for
reducing inequality. I thought I remember you mentioning it in a softer light more recently,
not sure when, any issues you feel as though you've shifted on. I mean I've
been doing this for, I don't even know how many, six years, seven years? I don't know.
I've been doing this a long time now. Yes, I have definitely shifted views on
things. I mean if you, if you hold the same opinion on everything that you did
last year, you're not growing. As far as UBI goes, it's not really about it being a very
effective tool. I have an ideological issue with it, and I do remember that. My son, you
I hate it when your kids use your own words against you, and he got me.
But any issues you feel you've shifted on?
I don't really know.
I can't think of something off the top of my head that I'm like, I had a major shift
I think, you know, I do this literally every day and I hear feedback and
criticism from, you know, a hundred people a day and I mean it obviously, there are
obviously times when it shifts, oh I know one, I know a good one, when we were talking
about reparations, this was years ago, the conversation came up and somebody else, somebody
basically was like, hey, you know, this is the issue with it, you need to move this direction
with it and it turned into a whole conversation and I was one of those people because I did
have somebody at one point be like, hey, you know, if you, if I borrow money from you,
when I pay it back, I don't get to tell you how to spend it and I was one of those people
who gave a lot of, I gave a lot of support to the idea of programs as a form of reparations
to help and the more I talked to people about it the more I was like yeah no
that's not how it needs to be done it needs to be cash like you cut the check
basically that's one that I definitely remember just because of the way it
happened but I'm sure there are more my question for you essentially is what is
What is the United States so afraid of when it comes to applying some substantial or meaningful
pressure to our ally in the Middle East?
Making things worse.
I don't want to do it because it's just going to make people mad, but at some point maybe
I'll sit down and do all of the forms of pressure that can be used, and talk about the outcome
of what happens if they go too far with it, because a lot of them could turn into a situation
where you take that toll, that number, and you add a zero to it.
It really isn't as simple as people want to make it.
Now, to be clear, there's room for more pressure.
I want to be clear on that.
It's just not dramatic or all-encompassing the way people want to do it.
A good example, you know, you see a lot now, you know, cut all military aid.
No, that's a horrible idea that might actually end up having the exact opposite effect of
what people want.
But cut offensive military aid, that's something, sure, there are minds, you know, the easy
path is always mind.
There are minds, but they're political, they're foreign policy, they don't change that number.
It's fear of influence stuff.
It's not causing really horrible outcomes.
And I think that has more to do with it than people want to admit because there are a lot
of ways for this to go sideways.
I was hoping that when there was an incident in which Egypt and Israel exchanged fire,
Some of that would come into focus, but at some point I'll go through them all, but
again I do want to make it very clear there is room for more pressure and I think some
is coming from Europe.
I think you're about to see more.
Why do you call it over there?
That's the worst optimization strategy ever.
If the goal was to be discovered for those videos, then yeah, it would be.
But if the goal is not wanting to profit off of that or build an audience on that, it's
good strategy.
And to be clear, that's not a criticism of anybody else.
It's the way I do it, and I have done it with other situations as well, and normally using
the term over there, if it's a military thing.
But yeah, no, there would be a lot more views if I plugged, you know, Gaza into every title.
You are correct.
It's just not, it's not something I want to cash in on.
Being a sci-fi fan for many years, it occurred to me, if everyone is worried about AIs going
out of control, how about making it required to bake in something similar to Asimov's
Three Laws of Robotics.
I don't really think AI is there yet.
The way it has been explained to me is that it's not really AI yet as the way we envision
it from sci-fi.
It is something that is scraping a large amount of information and sifting through it very
It's not really decision-making yet, so oh and I guess that's it, okay so those
were all the questions. All right well hopefully that helped you pass some time
and maybe we will get some news from New York because I imagine a whole bunch of
people are waiting on that.
And then once we have a better idea of what's
going to occur with that, we can talk about all
of the downstream effects.
Because no matter what happens, there's
going to be a bunch of them.
Anyway, so it's a little more information,
a little more context.
And having the right information will
make all the difference.
Yeah, I almost said the wrong sign off.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}