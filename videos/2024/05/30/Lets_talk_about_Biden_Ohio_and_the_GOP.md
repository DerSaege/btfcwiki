---
title: Let's talk about Biden, Ohio, and the GOP....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=8ymS0Y_y1JA) |
| Published | 2024/05/30 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Ohio and Biden being on the ballot and a change to
the convention because it appears the Democratic Party was not willing to rely on the good
faith of the Republican Party in Ohio.
So they have already started making alternate plans under the assumption that the Republican
Party in Ohio will drop the ball.
Probably a safe bet.
Either they would mean to or they literally just wouldn't be able to pull it off.
Okay so what's going on?
There is a law in Ohio that says the candidate for president must have the nomination by
X date blah blah blah blah.
Long story short, the Democratic convention, when the Democratic party would officially
say Biden is the nominee, occurs after this date.
This presents a problem.
It has been a problem before, not just in Ohio, but similar laws in other states.
Every time it's come up before, the legislature in that state quickly moves to fix the problem.
Republicans in Ohio, well, they want to play games and try to reduce the effectiveness
of the people in Ohio having a voice and they want to tie the two together.
The Democratic Party in Ohio is not willing to do this.
So it creates a situation where, well, they're not sure if Biden is going to be on the ballot
in Ohio.
So the Democratic Party has decided to kind of creep around the issue by holding a virtual
convention for at least part of it to go ahead and do a roll call vote to get Biden the official
nominee for that state.
The convention will still occur.
Now one thing that is worth noting as everybody's going to be like, wow, that was slick and
they threw that together very quickly. The Democratic Party has been toying with
the idea of moving a lot of the convention online and there are a couple
of different reasons for it. One being just ease and speed and all of that
stuff. The other being that there's a high likelihood that there are going to
be demonstrations at the convention and this is an easy way to get around that.
I'm not sure that is the best reason to do it, but it probably is factoring in to
their discussions. So they didn't just like throw this together out of nowhere.
They already had been talking about doing something like this and they can
do it for Ohio and remove the leverage that the Republican Party has that
they're trying to use to basically remove the voice of the people in Ohio
when it comes to ballot initiatives because it's really what it's about. So
the Democratic Party, I don't know if you would call this playing a hardball, more
outside-of-the-box thinking, but it does appear that they have solved the issue
with Biden being on the ballot and removed the leverage of the Republican
Party in Ohio all at once. So anyway, it's just a thought. Y'all have a good
day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}