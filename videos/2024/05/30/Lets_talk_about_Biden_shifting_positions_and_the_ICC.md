---
title: Let's talk about Biden, shifting positions, and the ICC....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=rwIXIcuVc8w) |
| Published | 2024/05/30 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about President Biden and the
White House in general and Congress and shifting positions, or at
least the appearance of shifting positions, and we're going to go
through the developments and just kind of talk about this again,
because I feel like it's probably worth going over because the
recent developments have prompted a whole bunch of questions. So what are we
talking about? The U.S. position when it comes to the ICC, when it comes to the
International Criminal Court, the U.S. position on this is you don't have
jurisdiction for this. Period. That's it. Don't assign anything else to it. The
The reason the U.S. has this position is that they don't want to come under ICC jurisdiction
later.
That's what it's about.
This actually isn't about Netanyahu.
So I'm going to read something from Speaker Johnson.
And this is actually him upset about Biden.
But it showcases the overall U.S. position.
The ICC should clearly be sanctioned for its outrageous and unfounded claims of authority.
Claiming the jurisdiction.
That's the issue.
The White House's refusal to protect Israelis and Americans is a terrible decision that
will set a dangerous precedent.
concerned about what's happening right now. It's a terrible decision because it
will set a dangerous precedent something later. And that's from Johnson upset about
what is being seen as a new position from the White House on this. And that
That new position, that isn't really a new position, but is the White House saying sanctions
on the ICC are not an effective or appropriate tool to address US concerns.
We will work with Congress on other options to address the ICC overreach, again, outside
their jurisdiction.
That's the issue.
attribute it to anything else or it will get confusing. Okay, so what is the policy shift?
Biden and the White House said that they would be willing to work with Congress in a bipartisan
fashion to come up with some kind of response to the ICC. And the Republican party, they just
kind of wanted to go scorched earth with sanctions. That's not something that the
White House is apparently willing to do. Why? Because it's a horrible idea. Again,
the goal is to send a message about jurisdiction, not undermine the entire
ICC. I would imagine that on some level the White House is, I don't know if it's
okay with the application for the warrants, but it's not upset about the
pressure that it's causing because the White House is trying to work a large
deal and that pressure is helpful. But they still want to send a message about
future jurisdiction. So why does the White House hold the position that it
does when it comes to not using sanctions? Because that's too much. It's
diplomacy, it's supposed to be finesse, subtlety. The Republican Party wants to do
it because they have adopted the foreign policy strategy of Trump. Everything to
extreme. Scorched earth. The thing is Trump's foreign policy was an abject
failure. You don't want to duplicate that. So the shift in position about
the ICC, it's not really a shift. The White House still absolutely wants to
send the same signal. They just don't want to go scorched earth. And since I
said that about Trump's foreign policy I know that there's gonna be comments. I
will put a video down below and if you haven't seen it it's the one where I
have the Jenga tower in the video. Watch it. It's probably going to answer all of
your questions and all of your comments about how great Trump's foreign policy
really was. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}