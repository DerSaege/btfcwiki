---
title: Let's talk about the pier, timelines, and questions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=iNnBFhvIyQM) |
| Published | 2024/05/30 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about the pier.
We're going to talk about the pier.
I guess on Sunday, over on the other channel,
I corrected some bad reporting that went out.
The reporting that went out said the pier had broken in half
and had washed ashore.
I corrected that by saying that that's not what happened, for boats had become unmoored
during high seas and washed ashore.
It is now 72 hours later, the pier is broke.
Now this is, of course, the pier for Gaza.
What I was told is that it will be repaired in about 10 days.
We'll see.
We'll see what happens.
This has been plagued with logistical issues.
Now people have asked about the effectiveness.
Is it worth fixing?
Yes, it's worth fixing.
So far it's a little more than a million pounds of food has made it ashore.
Not all of that has been distributed, as I understand it, but a million pounds has made
it ashore.
When you say it like that, it's a lot, and if you are one of the people who has gotten
some of it, yeah, it matters.
A million pounds is not enough, and it is not capacity, not even close to capacity of
what that peer is supposed to be able to handle.
My understanding is that the issue is with distribution once it gets to shore.
And I can understand that on some level, but that shouldn't slow down the amount coming
off the pier.
Even if they can't get it all out, put it on the beach.
They'll come get it.
So it is worth fixing, 10 days is a delay in a period of time that they don't have.
And that brings us to a question that came in, because apparently one of your grandfathers,
a woman who, her grandfather was a CB, she wrote exactly what he said.
So obviously I can't read the message because it was written exactly as a Navy chief would
say it, but the short version is why is the Army handling this?
For those that don't know, CBs are like the logical choice to do this.
It's like their whole thing.
fact I think in the first video I even just assumed it was them. I don't have an
answer for that. I do not know why this is an army thing. I don't know why CBs
don't have total control over this. It does seem as though some people with
willing hearts and skillful hands and a can-do attitude might be super helpful
here. I don't have an answer for that. So the end result here is that right now
it's down. Given what happened between Israel and Egypt, I don't have an update
on the Rafa crossing either. It is unlikely that there is even remotely
enough food going in. So that issue has once again come to the forefront. Keep in
mind it was never solved. It was just being mitigated slightly. So you have a
whole new situation developing. Hopefully they'll get the pier back up
and running quickly, and they will get it back up to capacity quickly. So, yeah,
that's where it's at, and that's all the information I have at time of
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}