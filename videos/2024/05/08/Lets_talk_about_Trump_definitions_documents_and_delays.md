---
title: Let's talk about Trump, definitions, documents, and delays....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=R7mTGu7P7t4) |
| Published | 2024/05/08 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk
about Trump and documents and definitions,
particularly the meaning of the word indefinite
because there's a whole bunch of headlines
out there using that term,
and it is creating,
it's creating a little bit of confusion.
So we're going to go over a few things real quick
and just clarify that.
If you have missed all of the headlines, Trump's documents case has been delayed indefinitely.
Indefinite in this context does not mean forever.
It does not mean that the case has been dismissed.
It does not mean that it has stopped, that there is some major issue with it.
It means that it has been delayed indefinitely.
means lasting for an unknown or unstated length of time. The headlines are using
that because it's catchy and it elevates people's anxiety and gets them to click.
So what's happening? The judge in the case has determined that some hearings
need to occur before the trial. I know that there's one on June 21st which is
to determine whether or not Smith was unlawfully appointed. Another is on the
24th of June to go over a request for records from a whole bunch of different
agencies because it certainly appears as though Trump's team is suggesting that
the White House is part of the prosecution team. I think that's what
the statement is. Now Smith is saying that the threshold that is needed to
actually have these hearings has not been met because it's a very high
threshold. I would have to agree. However, I am not the judge. So, this doesn't
actually stop the trial. It moves it back. It's not an indefinite delay in the
sense of this means it'll never happen. It means there's been a delay and they
don't know how long that delay will be. Now, for Trump's team, is this a win? Yeah,
Yeah, yeah, of course it is, of course it is, because he doesn't want to, you know,
have this before the election.
You might want to think about why.
If you are somebody who is supportive of Trump, you might want to think about why.
I mean, it is obvious that the former president will lose votes if this isn't resolved because
there's going to be this question hanging out there.
Knowing that, knowing that it will cause a loss of votes, why would he not want to have
it before the election?
can probably come up with your own answer there. So it's a win. Does this
push it beyond the election? It's possible. Not necessarily, but it's
possible. The other thing that is coming up is people are saying that Smith needs
to you know take it to the appeals court and this is what needs to be used to get
the judge to recuse herself if necessary have the appeals court basically
force the recusal. Generally speaking an appeals court is not going to force the
recusal of a judge simply for taking their time with something, which that's
that is how this would be presented. I know a lot of people are going to say
well these are things that would need to be brought up on appeal afterward and
all of this stuff. I mean sure, but an appeals court, they would need more than
she's delayed it. Smith would have to demonstrate that she's doing it
intentionally to help the defendant or that perhaps she was getting
guidance or something like that. It's not a matter of she delayed it and
therefore she needs to be recused. That's not how it works. So that's what's going
on when you see the headlines that say indefinitely understand it doesn't mean
forever. The only way it would mean forever is that if it does extend past
the election and Trump wins. So anyway it's just a thought. Y'all have a good
day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}