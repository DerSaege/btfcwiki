---
title: Let's talk about a trip to the gas station, politics, and youth....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=lie1TNMpBms) |
| Published | 2024/05/08 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about a trip to the gas
station and a message I received.
If you are under 25, watch this video.
Watch this video.
Before we get to the message, I'm going to tell you a story
about going to the gas station.
It is about 3 AM, and for me to get to something that
open at 3 a.m. it's it's a drive but I go inside and there is this old man and
by old man I mean substantially older than me and he is staring at me with
like ice in his eyes and I you know I ignore it for a minute or two but he
just keeps staring at me and I finally give him that look of like you know
what's going on and he smiles and he's like you look too young to have got those the way I got
mine. I have no idea what this means. I have no clue what he is trying to tell me when he says this.
Then I realize he's looking at my feet. I'm wearing a pair of Ho Chi Minh sandals. If you don't know
what those are, they are sandals that were worn by Vietnamese troops and a lot of Americans wound
up with them. The ones the Americans had were, well, they were typically on their
second owner. And I was like, no, no, I ordered mine. And he's like, ordered them
big belly laugh, like heartfelt Santa Claus style belly laugh. He's like, so
they got McDonald's and we got Ho Chi Minh sandals. I think we got the better
into that deal. And it's at this point that his, I'm going to guess, granddaughter looks
up at him with surprise. And she's like, you had a pair of sandals like that? And he's
like, more than a couple actually. And I was really afraid that this conversation was about
to take an incredibly dark turn when she looks at me and says, Ho Chi Minh, would they have
those at, and then names a department store and I can't remember which one.
The subtext being that she believed Ho Chi Minh was a fashion designer.
The look of, I don't know, disappointment, embarrassment on this man's face was very
real.
They paid for their stuff and left, I paid for mine, left.
As I'm walking outside, there is a guy.
He is late teens, early 20s, has the hood of his car up.
It is painfully obvious that he has no idea what he's looking at.
He has the woman inside trying to crank the car.
The battery's dead, like dead, dead, and I asked him, I was like, did you notice anything
before it stopped, and he's like, the headlights were acting weird.
okay so your battery's dead you probably need a new alternator as well and he's
like okay well where can I get one I was like well where isn't really the issue
it's right there the wind is the problem because it opens in like four hours but
and I don't know three three and a half a guy in a white truck is gonna pull up
and walk inside if you go up and knock on the door he'll probably let you in and
I ask him if he knows how to change the alternator, if he needs to, and he says, I can YouTube
it.
I'm like, all right.
So I go on about my day.
Here's the message.
Half your videos are about how the youth is going to save us and the country, and the
other half are talking about how things really work and they go against what the youth is
saying.
them to help when by your own videos they're getting it wrong. I mean, I
disagree on the percentages. I think I have more content than that, but I get
the point. I get the point. That guy, that old man, when he got his sandals, he was
probably a teenager. He was probably a teenager. We use the youth in this
country to fight our wars, both literal and metaphorical. Those for systemic
change, for social change, if not led by, the heavy lifting is often done by the
youth. And it's always been that way since the very, very beginning. You know,
we have this image of the Founding Fathers and they're viewed as all older
because they have white hair. That's for wigs. Henry Knox, Henry Lee, Hamilton, Betsy Ross,
Hale, Burr, Monroe, Trumbull, all of them under 30, most of them under 25 when the Declaration of
Independence was signed. We had some people who are considered founding fathers who were teens.
It's always been that way. We lean heavily on the youth when we expect
there to be a big change, but they are young and they don't always know how
things work. It's true. When I make those videos, particularly those about foreign
policy. When I'm talking about how it works, I'm talking about how it is and
normally I say this is how it is, not how it has to be, not how it should be, but
it's how it is. Because I'm of the firm opinion that if you want to fix
something you have to know what's broke. Just like that guy with his car. Once he
knew what the problem was, I'll figure it out. That's most of them. They don't need
leadership. They need, they might be able to use somebody to advise them on what's
broke, what needs the most attention. They've got the rest. They always have.
they'll figure it out. And I know somebody is going to say well it was
different way back then. You know those people you named they had better
educations or whatever and that's debatable. But I think the most
important part to remember about that is that that's not on the kids that's on
us. It was our job to make sure that they had the education to address the
issues. It wasn't something that they just were supposed to pick up, but we
don't fund education. We don't really value it the way we should. Yeah, there is
There's a lot of idealism.
There's a lot of stuff that could be considered them being naive.
But they'll figure it out.
They always have.
Anyway, it's just a thought.
have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}