---
title: Let's talk about over there, coverage, and questions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=8grlKB_UrTw) |
| Published | 2024/05/08 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about four different news
items about over there, and your questions related to
those news items, because a whole bunch of
questions have come in.
So we're going to just briefly go over the coverage and the
event, and then answer the questions that
came in related to it.
Okay, so starting off, for those who aren't aware, there was a move into Rafa, and in
it they took the Rafa crossing.
The Israelis took the Rafa crossing, they took control of it.
Okay, so the first question, is this the large-scale ground offensive?
The reason this question is being asked is because half of the reporting says it is,
half the reporting says it isn't.
If you go to most military commentators, they are answering it in divided ways, but they're
actually answering the question correctly, but with a lot of jargon.
Okay, so it wasn't that long ago that we talked about how the plan that Netanyahu's team
brought to the US, it appeared that they took the original large-scale offensive plan and
broke it up into different pieces.
Each piece was more of an operation rather than a massive offensive.
I think I said in the video that will be down below, there will be a video down below, that
if they did it all at once, it would be an offensive, but they're breaking it up.
This is piece one of that plan.
So is it a large scale ground offensive?
Not technically, but it's part of trying to accomplish the same thing without technically
becoming a large-scale ground defensive. Okay, next question on this. Is them
taking the Rafa Crossing bad? Now, a lot of times this question came in as why is
it because people are very upset about it. There are two reasons. The reason
that most people are providing is that they feel that it will impede aid and
and obviously impeding aid is bad.
The other reason that is less discussed
is that this is probably really gonna upset the Egyptians.
It's been a long-standing thing
that Israel is not supposed to control
the other side of that crossing,
a very long-standing thing.
So that is probably going to upset them.
And given the fact that they are playing a role
the negotiations, that's bad. Okay, there is a rumor, and it is a very persistent
rumor, that Netanyahu's team wants to hand off control of that crossing to a
private company. A lot of the rumors says that it's a U.S. private company. I know
people at the companies in the US that are capable of handling it. None of them
have heard anything about it. That gives us three options. One, the rumor isn't
true. Two, the rumor is true but they have decided to go with a company that maybe
doesn't have an established track record of handling something like this. The
The third option is that, well, the people I talk to either don't know because the leadership
of that company is keeping it very quiet because it is sensitive, or they do know and they're
lying to me because they know that if they told me I'd tell you.
I mean that's something that has to be considered in this case.
obvious follow-on question, is that a good idea to hand it off to a private
company? Under normal circumstances I would say no, that's not a good idea,
it's a horrible idea. Given the current situation, it's probably the least bad
idea. I do believe that they would probably be better at moving aid and it
might mitigate any issue that Egypt has with it. So that's that. Again, that last
piece, it's a rumor but it is one that is, it's making the rounds very, very
quickly in circles that people normally don't buy into rumors but I can't trace
it back. Okay so the the next question is the report I guess the next piece of
news is that the report that was supposed to go to Congress about the
behavior of Israeli troops and whether or not it would be cause to interrupt
US military aid has been delayed. The reporting says that it's been delayed
indefinitely. From my understanding about a week, less than, but we'll see. The
question is, why? Why was it delayed? There are three real options. One is that
the administration has just thrown their hands up and they're re-evaluating what
to do because they put a lot of pressure on Netanyahu to make sure there
wasn't a move into RAFA and even though this one is not, it's not done in the way
that the US truly objected to, it's still not what the US wanted. So they may be
delaying everything in an attempt to figure out how to proceed from here.
Option two would be to make Netanyahu think that it's option three and just
let him sweat not knowing what's going on. And then option three is like what we
talked about with the Kingdom of Danovia. They got that whatever was in their eye,
they got it out. And that's how they initially wrote the report, was just
about a limited set of events, but because of what's going on they may
decide to be a little bit more detailed. And there will be a video down below
going over that. Okay, Biden is reported to have paused weapon shipments and
there's a whole bunch of different questions about this as far as why he
didn't announce it, why the aid was approved but then he's not delivering it,
what is this just a move after the fact, did this really happen, all of that stuff.
The most common was a phrasing that basically boiled down to he's
He's only doing this because of the negative pressure and I don't really think that he
did it otherwise he would have announced it.
The whole point of doing it this way, which is delaying the shipment and pretending that
it's not delayed, just making it seem as though it's being delayed for some reason other
than applying pressure is to make sure that there's not really a record of the
U.S. denying an ally arms during wartime. The reason it wasn't announced is
because he's not trying to win a Twitter war. He's trying to mitigate a real one
and it wouldn't be announced. They would just do it. They would slow roll, slow
walk the shipments. Normally, when they do stuff like this, they delay one
component that is important when it comes to the military. It's one of
Murphy's laws. Things that have to be together to work can't be shipped
together. So you delay one component and it really ends up delaying a whole bunch
of different shipments because this stuff won't work until that main portion gets there.
There is confirmation of this happening once.
I am almost certain that it has happened more than once.
That's just the one time they have confirmation of.
This process of slow-walking military aid like this has been discussed on this channel
as something that Biden was probably doing since March.
There will be a video down below.
And then the last one, well the last news item and then we'll get to where things are
going but the last one's really just me being snarky.
The peer has completed construction and the question, you said it would be now if everything
went perfectly.
It didn't go perfectly and it's done.
Now what?
First, that's not what I said.
And it's done, meaning the peer is complete.
It has been completed.
The construction is complete.
However, it is not in place, which means regardless of the press releases, aid will not start
flowing in tomorrow.
There will be a video down below.
Now where does it go from here?
So the Palestinians believed that there was a ceasefire to the point that they were celebrating.
celebratory fire over it and then and then the move in Arafah occurred. At that
point the talks collapsed. They appear to have kind of been restarted through the
efforts of Barnes and the Egyptian team and the team from Qatar. So that is where
it's at, it's moving again, they're talking again, but this episode, it damaged the
the trust and understand there wasn't a lot there to begin with on any side,
directed at any side. Nobody trusted anybody and even in the way the various
sides see how this event transpired. They can't even agree on that. It was
incredibly damaging the way this played out and it will, it's not something that
can't be salvaged but it's going to take work. It is going to take work. So they're
We're talking again and that's about the best we can hope for right now.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}