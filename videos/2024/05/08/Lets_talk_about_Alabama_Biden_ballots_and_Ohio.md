---
title: Let's talk about Alabama, Biden, ballots, and Ohio....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=-kTEt7U4bF4) |
| Published | 2024/05/08 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Alabama, Ohio, and political machines and how they work.
Both Alabama and Ohio had situations arise where the state could theoretically leave
Biden off the ballot because of misdeadlines and stuff like that and I got a message from somebody
who lives in Ohio and the basic summary was hey I was checking to see if we had figured out what
we were going to do with Biden being on the ballot and we still haven't done anything but Alabama
already fixed their problem and Biden will be on the ballot there. What's taking so long? Are we
slower than Alabama. No, Alabama's math is clearer. I put out a video at the time. I'll try to find
it and put it down below. But for Republicans in Alabama, they don't actually benefit if Biden
isn't on the ballot. It hurts them if he's not on the ballot. And I know that
doesn't make any sense, but down-ballot, it's important for Biden to be on there
for Republicans. Trump has a lot of supporters that are really just voting
for him. They don't care about the other races. If Biden isn't on the ballot and
Trump doesn't need their help, they may not show up to vote, which means down
ballot they may lose some votes because those people are really just showing up
to vote for Trump. And further down the ballot it may just be a point or two but
that may be enough to swing an election. An election of the people who actually
to decide whether or not to fix this problem. So in Alabama, the Republican Party was in a position
where if they did the fair thing and they let Biden be on the ballot and they fixed the issue,
then they got Trump's opposition on the ballot. They know Trump's going to win anyway.
It's Alabama, it's not going blue, but with Biden there, there will be more people
who show up to vote for Trump because they don't want Biden to win, which helps them
in their elections down ballot.
The math is really simple for them.
Ohio is seen as a swing state, so it's probably a little bit more complicated up there.
I've seen math run on this and I've seen some that say it actually helps Biden if he's not
on it. I have a hard time believing that. Ohio and Alabama have very different dynamics so
if I was Biden I wouldn't leave it up to chance but at the same time if I'm a Republican I might
be a little bit concerned about those, about the math that says leaving him off
will actually energize his base. So I don't know what Ohio is going to do but
it was it was pretty apparent to me that Alabama was going to fix this. I think in
the video I was actually like I dare you to leave him off because it very well
might have swung a number of races down ballot in favor of the Democratic Party
if he wasn't there simply because less Republicans show up. So we'll have to
wait and see what happens in Ohio but what happened in Alabama was I would
have been really surprised if they left him off the ballot because it would have
hurt them and they're the people who make the decision about whether or not
he's on. So we'll just have to keep waiting and see what happens in Ohio. I
would imagine that the Biden team is is on this. I would hope so anyway. Anyway,
It's just a thought y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}