---
title: Let's talk about an American general, Ukraine, phrasing, and raised eyebrows....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=H0h4T9_EtTU) |
| Published | 2024/05/16 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about an American general,
phrasing, and a statement that certainly raised eyebrows.
And we'll just kind of run through what occurred, and
then talk about the possibilities.
Okay, so this is what happened.
An American general was talking about how the United States might be restructuring the
way our Green Beret teams are put together, what kind of specializations they have.
And in the process of this, it kind of seemed to indicate that they were basing this on
lessons learned from Ukraine, which would be one way to say it. Lessons learned
from Ukraine through our special operations partner's eyes would be
another. And that is, that's quite the statement and it definitely raised a lot
of eyebrows because in order to do that it would seem to indicate that a partner
nation had special operations teams, well, in Ukraine. I don't know whoever
would dare do that, but they'd probably win. I'll just go ahead and do the thing
for all of the partner nations out there. The country of, fill in the blank, has a
long-standing policy of not commenting on locations, missions, or deployments of our
special operations teams. We thank you for honoring this long-standing tradition. No more questions.
Okay, so there are a number of things to note about this.
It is widely being taken to suggest that there are active-duty special operations teams in
Ukraine.
Now one of the interesting things about this, and this is what makes all of the other possibilities
way more likely, is that if that was happening, you would think the United States would have
them there.
If it was active duty special operations, you would think that the U.S. would be doing
it as well.
However, based on the statement, the U.S. wouldn't be learning lessons through somebody
else's eyes if they did that.
It's probably a case of bad phrasing because it's important to remember that in today's
day and age, you don't necessarily have to be physically present to be a special operations
advisor.
It's also worth remembering that there are a lot more options today than there were in
the past.
It could be a company with a light touch, a private company that is staffed by former
special operations, so they're not actually active duty, but they would still be able
to provide the same kind of lessons learned.
That's another option.
There are a number of options that are in play that wouldn't necessarily mean that
A country has special operations teams active in Ukraine that are active duty.
There are a lot of other options.
But the way it was taken by most people was that a U.S. partner has at least one team
there doing something.
Again, based on full context, it really seems more advisory probably outside of the country
if they are active duty.
If they're a private company, they very well may be inside.
But that's a different matter altogether.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}