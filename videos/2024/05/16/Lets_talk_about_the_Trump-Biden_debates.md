---
title: Let's talk about the Trump-Biden debates....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=bbU_g6IghmU) |
| Published | 2024/05/16 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump and Biden
and the debates, what's been set
and how this process is rolling out,
how these debates will be different
and just go through some of the questions.
So if you missed the news today,
the candidates agreed to engage in two debates.
During this process there was a lot of back and forth from the candidates.
Very reminiscent of what occurs before like a pro-wrestling match.
At the end two debates have been set.
One with CNN on June 27th, the other on ABC September 10th.
Now, this is a deviation because normally it is not an individual outlet hosting a debate.
That's new.
Who will be at these debates?
Trump and Biden.
There is a very, very slim chance that RFK Jr. might be able to qualify, but it's incredibly
slim.
is very upset and views it as them trying to keep him off the stage. Now I
know people are going to ask about the other candidates. None of the other
candidates are even remotely close to meeting anything that would would be
viewed as a traditional threshold for being included, like not even close. Only
RFK Jr. is even remotely there and that is very unlikely. So those debates were
agreed to the CNN 1 and the ABC after the back-and-forth. All of the rules for
those are not exactly clear yet. They're not public yet. They've probably been
agreed to, but they're not announced.
After this, Trump put this out, please let this truth serve to represent that I hereby
accept debating Crooked Joe Biden on Fox News.
The date will be Wednesday, October 2nd.
The host will be, and then he picks his moderators.
The Biden team responded by saying, Donald Trump has a long history of playing games
with debates, complaining about the rules, breaking those rules, pulling out at the last
minute or not showing up at all, which he's done repeatedly in all three cycles he's
run for president.
President Biden made his terms clear for two one-on-one debates and Donald Trump accepted
those terms.
No more games, no more chaos, no more debate about debates.
see Donald Trump on June 27th in Atlanta if he shows up.
Okay, so that last debate, what was Trump trying to do?
Trying to get the last debate, one close to the election, held on friendly ground.
Held on Fox News, somewhere that will cater to him.
The reason that seems apparent is that he is concerned about his performance in the
first two debates. Biden is not going to accept this for the same reason Trump would not accept
Biden saying, okay, the last debate is going to be with Rachel Maddow as the moderator. It's just
not going to happen. Trump trying to alter the deal after it was made, not generally how it works.
Maybe he can find a book on it or something.
So that's what's going on.
What's going to happen at the debates?
If Biden brings his State of the Union energy, he's probably got it in the bag, to be honest.
If he doesn't, it could go any way.
Both candidates seem pretty convinced that this is going to help them.
Trump believes that Biden is just incapable of doing this despite previous
results which is kind of weird it's one of those situations where seems to be
believing his side's propaganda like don't buy into your own propaganda it's
kind of a rule it doesn't seem like he's really abiding by that one so Trump
Trump believes that Biden's going to be up there all sleepy and incoherent.
Biden believes that Trump is going to show up and be very erratic and maybe talk about
Hannibal Lecter or whatever.
So that's what they're both counting on.
They're both counting on the fact that not that they would actually win the debate on
policy, but more that the behavior of their opponent will push voters away.
That seems to be the strategy.
Now, I personally, as far as the way Biden's team did this, this is totally not how I would
have tried to go about it, but at the end of the day, it worked.
They have two debates on centrist outlets.
So I don't want to criticize it because it got to the same point just through a bizarre
way.
I personally would have made Trump deny the commission debates and made that completely
his decision.
I understand the Biden campaign has issues with the commission for presidential debates
as well.
Trump is adamantly opposed to those. So it seems like it would have been a good idea to make him
say, no I'm not going to do those. So it puts him on his back foot. But the goal of that would be to
get this. So they got there just by Trump not really knowing how to make a deal and letting
his ego get ahead of him.
So that's what's going on.
These are probably going to be the only debates.
Maybe another one gets worked out, but it seems unlikely.
And there is no word yet on any vice presidential debate because Trump still hasn't figured
that out yet.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}