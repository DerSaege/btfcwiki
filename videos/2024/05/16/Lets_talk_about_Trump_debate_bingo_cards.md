---
title: Let's talk about Trump debate bingo cards....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Dzs96psczvg) |
| Published | 2024/05/16 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about
your Trump bingo card for the debates.
We're gonna go over some phrases
that you are likely to hear at the debates
should Trump choose to show up.
We are going to talk about some phrases
that he has used a lot.
These are phrases that he has said more than 15 times
between January 2023 and April 2024.
This list is actually much longer
than what I'm going to read.
It's from an article by Axios.
I will have the link down below
because it's totally worth going to look at.
Okay, so the top six items that you are likely to hear,
because these are the things
that it is a priority for him to talk about.
These are the things that when it comes to policy,
These are the phrases that he uses the most.
Number six, keep men out of women's sports
because that is obviously a high priority
that is going to affect a whole lot of Americans.
It's not just something that he's fear-mongering about.
Okay, number five, indemnify all police officers.
Make qualified immunity stronger
because that is in line with what most people believe.
Number four, rebuild our cities.
Maybe he thinks he's running for mayor or governor or something, I don't know.
Number three, defund any school pushing
critical race theory.
You know, that thing that
when it is
said as CRT
People say they don't want it, but when it is actually explained in the definition in the same poll, people say they
want it.  Again, not something just that he's using to fearmonger and stir up, you know, hatred.
It's a super important thing to most people in this country.
Number two, largest domestic deportation operation.
And number one, defund any school with a vaccine or mask mandate.
Again, something that is totally in line with what most Americans want, right?
I mean, it seems weird that out of his top six, two of them are about defunding schools.
It's almost like somebody would be forgiven if they believed he was trying to create a
permanent underclass of people through poor education so they were, you know, easy to
manipulate, easy to control.
Again, this is from a list by Axios.
They have two more categories, things that he said from 10 to 14 times and 5 to 9 times.
So if you are going to make bingo cards for the debates, I would strongly suggest going
over there and taking a look and to just go ahead and answer the question.
No, I mean, as you get further down the policy considerations, they don't really get much
better, just so you know.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}