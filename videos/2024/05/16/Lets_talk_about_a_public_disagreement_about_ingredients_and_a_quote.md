---
title: Let's talk about a public disagreement about ingredients and a quote....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Q4dInNkQSBs) |
| Published | 2024/05/16 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about a quote.
We're gonna talk about a quote.
We're gonna talk about something that was said
that is, it's a pretty big development.
It's something that has been hinted to for quite some time,
It's something that has been hinted to for quite some time,
but it just went out in a very, very public way.
And the reason, the reported reason
went out in the public way is a story in and of itself. But we're going to go over
it but to make it as accessible as possible I'm gonna read the quote first
and then tell you who said it because I think that that might be the best way to
to do this. Okay so this was somebody talking about the situation over there
and they said that peace could quote only be achieved by Palestinian entities
taking control of Gaza accompanied by international actors. So a Palestinian
government and a regional security force, just to be clear. Goes on to say, I must
reiterate I will not agree to the establishment of Israeli military rule
in Gaza. Israel must not establish civilian rule in Gaza. I call on Prime
Minister Benjamin Netanyahu to make a decision and declare that Israel will
not establish civilian control over the Gaza Strip, that Israel will not
establish military governance in the Gaza Strip, and that a governing
alternative to Hamas in the Gaza Strip will be raised immediately. You've heard
this before from a whole bunch of different places. Aid, the third
ingredient in the three ingredients, you know, dump trucks full of cash, not
specifically listed, but at this point, that's kind of a foregone conclusion. So,
who said it? A person that could be described as a former Israeli commando,
a former Israeli general, or the current defense minister of Israel? According to
The reporting, the reason it went out in this very public fashion was because he'd been
trying to raise this issue with Netanyahu for months.
So I don't know what this means.
going out this publicly, I feel as though recent developments have led the defense minister
to kind of push even harder and even more publicly for a strategic plan rather than
just an operational one. So does this mean that they're going to come online
with all of the other countries that have basically agreed to a rough sketch
very similar to what's being described here? I don't know. I don't know. But this
This is a very public disagreement, and the defense minister was not alone in expressing
these concerns.
So that sketch, those ingredients, it seems pretty obvious at this point because according
to the reporting, he said that he had been raising this issue since October.
It does seem as though Israeli advisors told them the same thing.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}