---
title: The Roads to a May Q&A....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=_RVuhm2Y1oU) |
| Published | 2024/05/16 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Bo again,
and welcome to the Roads with Bo.
Today is May 16th, 2024, and we're doing a quick Q&A.
Questions from y'all.
The questions are picked by the team
and they're supposed to be kind of lighthearted-ish.
We're doing this today because normally
we have something go out on Thursday mornings
and I don't have anything.
So we're gonna go over questions that came in from y'all
the last few days. If you ever want to send something in, it is questionforboe at gmail,
and that is F-O-R. Okay, Louisiana maps. I saw the ruling and said, great. Then I read the AP
article and it said liberal judges were in dissent. Can you break this down? What am I missing?
Recap. Congressional maps in Louisiana have been in dispute for a while, and we've been covering
bring it over on the other channel and there had been a lot of court cases
about it. It looked like it was all resolved and everything was fine. Then
another suit came out of nowhere after a a map had been approved. The Republicans
in Louisiana decided to fight back even though the map really helps the
Democratic Party. They took it to court, goes to the Supreme Court, Supreme Court
says you can use that map.
The liberal justices are in dissent.
It appears more over procedural things than the actual outcome.
The map is likely to favor the Democratic Party.
It appears that they didn't like the precedent that was being set by getting involved that
early and what amounts to the courts having even more sway over congressional maps is
what it seems like.
The other thing to know about this is that the map looks like it's going to be used
this time, but it may only be used this time because there are still more challenges coming.
Biden's tariffs, more targeted and focused than Trump's, but good or bad for the economy.
I see he's trying to support the economic stimulus that he's given to certain industries
to give them a chance to get up and running.
But what are the long-term economic implications?
OK, generally speaking, I don't like tariffs.
I don't.
I understand that, yeah, they're more targeted.
They're more focused.
I still, just on a matter of principle, I don't like them.
The idea behind his is to help basically green energies
in the US, kind of get their base up and running.
And I get it, and that's important.
At the same time, when we talk about going green,
one of the biggest things that I get
is that those people that don't have a ton of money,
they have a hard time going green because it's expensive.
Tariffs aren't going to help that problem.
So there's that.
Okay, so just curious about your response to the news that Biden will send another billion
in aid.
We were all saying that the pause was performative, and you came out giving Biden so much credit,
saying it really mattered and that he was doing some magic behind the scenes.
How exactly does this billion and more aid to bomb civilians fit into that plan?
I will be talking about this over on the other channel.
I mentioned it in the video talking about the potential Biden veto that went out just a few days ago.
I mentioned it at the end of it and I said that it was light reporting and listed off questions that I had.
The answers to those questions might alter your question here.
None of that stuff is showing up anytime soon.
I want to say the projected delivery date is 2026, the last half of 2026, which means
in real life it wouldn't show up until the middle of 2027 because those things always
run behind.
There will be a video about this because one of the first questions you would ask about
a defense deal like that is when the delivery is and what type of transfer is it?
Is it a drawdown?
Is it new production?
And none of that was in the reporting.
And there are a whole bunch of people now who got very riled up about none of this is
going to be used in RAFA.
I got a whole bunch of nasty messages about this.
So this is one of those situations
where there's a teachable moment about information consumption.
But yeah, if you go back to that video,
I'll try to find it and put it down below,
it's at the end where I start talking about it.
And again, if you are somebody who is concerned about this
and based on the phrasing of this,
Your question should not be about anything in that other than what those tactical vehicles
are and what they're going to be used for and why they're ordering them.
That's your real question.
The stuff about Biden is doing a transfer, the US is not going to sever ties with Israel.
It's just not going to happen.
So you will hear about more, but understand they're years out.
They are years out.
It doesn't have anything to do with what's going on right now at this moment.
The uh, and I can't, I can't say this for sure the way that I want to say it.
But I would not put out reporting that didn't have the basic questions answered, as far
as when it would be delivered and what it was really for, especially in a time where
you know that the information that you have would have a massive effect on people and
how they viewed things.
Are we ever going to get an update on the New Year's message trailer guy?
I think about him a lot.
Yes, actually.
In fact, it's already recorded.
We have gotten updates from him.
I will put this video down below, too, just in case you don't know what this is about.
But a guy was looking to make a change around New Year's, and we did a video about it.
We do have an update, and it'll come out probably sometime in the next week or so.
With potential flooding in our local forecast, just as I'm prepared to go camping, I had
this random thought considering all the homeless in Honolulu and my brothers in Louisiana.
about doing a PSA for flooding just aimed at quote campers, shall we call them.
You know, anyone who happens to be unhoused when the water rises, there would be a fundamental
difference between having a vehicle or not and whether or not it runs.
Doing floods indoors is easy, brah.
Be a pioneer and address my peeps.
Much love.
Yeah, we can do something about that.
It's not something I'm going to be able to answer right now other than the very first
thing you should do, pull out your phone, get a topographic map, find out where the
high ground is and get to there.
That would be the goal.
You don't want to be near something that could cause a mudslide as well.
you want to be high and I'll do a video on it okay okay would you please for the
love of God stop talking about your stupid three-point plan nobody in the
Israeli government will ever agree to that it's stupid that you keep pointing
it simply because you think it's a good idea. I'll give you a hundred dollars if you just shut up
about it, and I bet you a thousand dollars nobody in the Israeli government will ever even listen
to such an idiotic plan. Wow, this is funny on so many levels, and I totally know why this question
is in here, all I have to say about this is make sure you watch all of the videos over
on the other channel today.
And why don't you just send that money to World Central Kitchen.
I just watched your video, let's talk about Putin's big shakeup and what it means.
You talk about how they are getting ready for a long war.
I do agree.
However, I was wondering what is the actual possibility of a long war with the amount
of losses Russia has taken both in equipment and more importantly personnel.
Sorry if this is an impossible question to answer or if it would require too much speculation.
It's a lot of either or is what it is.
So if the West continues to push in equipment and aid, yeah, there's not much that Russia's
going to be able to do.
But it's been like that since the beginning.
The only reason Russia is having the success it's having right now is because Republicans
in Congress held everything up.
That had a massive effect on the field over there.
Now, what is the actual possibility of a long war?
It depends on how good that new economist is.
That's really what it boils down to.
There's a lot, there's a lot riding on his ability to deliver on whatever it is he promised.
And I would not want to be him if he can't deliver on it.
So is it possible for Russia to wage a long war?
Yeah.
They have a lot of low-end resources that they can use.
Is it good for them?
No.
At this point, every month they stay in, they're just falling further and further behind in
that near-peer game that they want to be in so bad.
But it appears that Putin is very locked into this.
Again, he should have pulled back almost immediately.
From a foreign policy standpoint, when it didn't succeed initially, it wasn't going
to get better from there.
And the longer it dragged on, the more he lost on the wider international scale.
So Russia will always have plenty of infantry to throw that way.
The other stuff, eventually they'll run out, but we'll have to wait and see what the
economists can do.
What do you think of a famous commentator on the right who decided to be a, quote, trad
wife, who is going around talking about how bad it was?
What do I think of her?
I don't, yeah, I don't have any comment on that.
The whole concept of tradwife, it's not trad, traditional wife, it's not traditional,
that's not real.
That's what people believe it was like because they watched Nick at Night.
That's not actually what it was like, it's not based in reality.
They're playing at it.
So I mean, yeah, it is not surprising to me that that, that when people commit to having
that kind of relationship in that way and super defined and, you know, everything having
to be a certain way and fit into this mold that is shaped by the internet, that it doesn't
work.
doesn't surprise me in any way shape or form. I have heard a little bit of the
the the story. I feel bad that she went through that but that's as far as her
that's all the comment I have. Who is your second favorite female character
in G.I. Joe? Oh, because the first is obviously Baroness Got It. Probably, I guess, Lady J.
I liked her voice as a kid, I guess. I don't know. Okay, and that looks like it. So, there
you go. A little more information, a little more context, and having the right information
and we'll make all the difference.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}