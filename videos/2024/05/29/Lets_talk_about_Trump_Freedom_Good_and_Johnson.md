---
title: Let's talk about Trump, Freedom, Good, and Johnson....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=8xOXPNHWljE) |
| Published | 2024/05/29 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about Trump
and Speaker of the House, Johnson and good
and freedom in Virginia and how a chain of events
is finally starting to kind of show itself
in a very public way, something we've been talking about
on the channel for a while, but it's been in the background.
You might remember us talking about how Johnson, in trying to get control of the house,
one of the major things that he had to do was either get the Freedom Caucus, the Twitter faction,
to kind of come to heel or make them irrelevant.
Those were his options.
And we talked about how he was working to do that.
The chair of the Freedom Caucus is a guy named Good, Bob Good.
He is currently facing a primary in Virginia.
He is one of the people that put on the blue suit and went up there to New York to back
Trump up and really put the information out there for him and all that stuff, showing
he was a good, loyal, MAGA Republican. So, this is what Trump had to say.
day.
Bob Good is bad for Virginia and bad for the USA.
He turned his back on our incredible movement and was constantly attacking and fighting
me until recently when he gave me a warm and loving endorsement.
But really, it was too late.
The damage had been done.
And then he goes on to endorse somebody else, Good's primary opponent.
That meeting between Johnson and Trump where they took photos together and held hands and
sang kumbaya, it certainly appears that Johnson came out on top.
Now part of this, undoubtedly, is Trump just being Trump.
Back then, good, I think he came out in support of DeSantis at first.
And this is partially Trump's way of getting even.
He doesn't have anything to do with a movement.
The other thing, undoubtedly, is Johnson probably needs, good, the chair of the Freedom Caucus
to have a little less influence.
So Johnson started it making that caucus more and more irrelevant, or at least trying to,
But it certainly looks like Trump really just kind of pushed him down.
Especially after good going up to New York and doing what he did.
That's quite the move.
Now we don't know how much impact it's going to have on the primary, which is June 18th.
But in a lot of ways, when it comes to the primaries, Trump's endorsement carries some
weight or tends to.
So we'll have to wait and see, but that might be kind of near the end when it comes to Good
serving as chair of the Freedom Caucus.
And it might signal a decline of the Freedom Caucus in general.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}