---
title: Let's talk about Trump feelin' bad in Texas....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=V-jr8AxD0Xo) |
| Published | 2024/05/29 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
It's Bill again.
So today, we're going to talk about how Trump is feeling.
We'll talk about Trump, a primary out in Texas
that did not go Trump's way,
and what it means for Texas politics and maybe beyond.
Okay, so the Texas House Speaker, Phelan, he was being primaried, being primaried by
a far-right candidate, Covey.
Covey had the backing of Trump, Paxton, Patrick, just a list, a list of far-right stars.
He lost.
Phelan was instrumental in bringing about the impeachment effort against Paxton.
And this was, I mean for lack of a better word, this was a revenge tour.
That's really what it was about.
And Phelan apparently won with 366 votes.
So it was close, but he won.
The reason he won, a whole bunch of businesses, business owners, they put together a well
financed campaign to keep him because far-right policies are actually bad for business.
In non-Trump fashion, Covey conceded.
Even with it being that close, didn't go the route of, oh, you know, there's something
wrong and let's recount everything 40 times and all of that stuff, played by
normal political rules. So here's the thing. The speaker retains his spot.
He's going on to the general and he's almost certainly going to win. Trump
endorsed a candidate against a big political figure in Texas and lost and this was high-profile.
His endorsement should have meant something here.
We've talked about it, can't win a primary without Trump, can't win a general with him.
Apparently you can win a primary without Trump, even in Texas.
This is a surprising outcome to be honest, so it's one of those things that
it's not enough to to say hey Trump has totally lost his sway, but it means maybe
it's time to start keeping an eye on these primaries because maybe it's the
beginning of a trend where Trump is not quite the potent force that he was when
it comes to primaries within the Republican Party. Because if he doesn't
have sway in Texas, even with the other backers, that might translate to primary
losses in other states as well. Anyway, it's just a thought. Y'all have a good
day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}