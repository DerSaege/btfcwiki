---
title: Let's talk about Nikki Haley, trips, and images....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=vaV1mUDPxi8) |
| Published | 2024/05/29 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about Nikki Haley.
We're going to talk about Nikki Haley, her trip, the imagery that came out of that trip,
and the long-term effects of that imagery.
Because as this starts to go out in the United States and people hear about it, they are
going to want to talk about how it makes them feel or how it makes other people
feel in the United States. In the grand scheme, that's completely irrelevant. It
does not matter. If you don't know what happened yet, Nikki Haley, former US
ambassador to the United Nations took a trip to Israel. While she was there, she
went up north and while she was there, she wrote on some shells, finish them.
Finish them. And undoubtedly in the United States, there's going to be a lot
of conversation about that. And we'll go ahead and address the elephant in the
real quick because I know people are going to want to talk about it. Yeah for
those people who believe that they'll be able to send a message to the
Democratic Party, please understand, she's the moderate in the Republican Party.
Finish them.
Yeah, people are going to talk about how it appears here, how it makes them feel.
whether or not it's a good look and there will be a debate about it. There
will not be a debate about it in the Middle East, at all. There will be no
discussion about whether or not it was appropriate.
Anytime a US media outlet wants to stir up angst about the Middle East, say in the
run up to a war. The drums are beating. What kind of imagery do they show to get
the blood pressure up of the right-wing reactionaries in this country?
What's the imagery? It's the same every time. They could probably use the exact same
footage and nobody would notice because it's that similar. It's a nondescript
Arab street. There's a crowd and there's a burning American flag and because of
that people in the United States, a whole lot of people, become willing to defend the
indefensible.
That kind of propaganda, it's not something that just affects Americans.
There will be no discussion, no debate about whether or not it was appropriate over there.
nowhere in that region is it going to be in question.
The former ambassador to the UN wrote, finish them on the shelves of a military currently
under investigation by the ICJ and the ICC has already got applications for warrants.
There's no discussion.
just created their flag-burning imagery for the next decade. It will be used. It
will be used by people who are well-meaning, who just want the violence
to stop. It'll be used by them to illustrate a point. It will also be used
by those people who are not well-meaning, and it will work. Regardless of whatever
the all-important American opinion of that imagery turns out to be, I can
assure you that across the Middle East the imagery will be received in one way,
in one way alone. And that is the U.S. co-signing everything and co-signing
finish them. The effects of that, the US stability in the Middle East will be paying for however
many social media likes that got for years and years to come. Anyway, it's just a thought.
have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}