---
title: Let's talk about Trump, history, and verdict watch....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=CH_2V8_CYy4) |
| Published | 2024/05/29 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Trump
and New York and history.
And we will begin what may be a pretty lengthy verdict watch
because that starts.
The instructions will be provided to the jury
And they will begin their deliberations and the rest of the country will
wait to see what they determine.
Um, now one thing that's worth acknowledging, and it's one of those
things you kind of have to remind yourself of, this is history, this is
history, it's easy to forget because Trump is so un-presidential that this is its
history in the making. A former president of the United States, well, his fate is
in the hands of 12 citizens. What those citizens determine, it's going to shape a
locked. So we're just going to wait. That's all we get to do. We are spectators
to this historic event. As a good spectator, I spent a lot of time watching
legal analysis last night. And I'm not sure they know any more than we do. The
the assessments are pretty scattered. The one thing there seemed to be consensus
on was that the jury was going to take its time. That it wouldn't be a quick
process. I don't even know if that's true to be honest, but that was the one thing
that stuck out as most of them agreeing on. You know, they all made their
predictions and most of them leaned one way, but most of those that are providing
coverage of it while they have an opinion about it. So we will have to wait
and see. Please understand when the verdict comes out that there's still
going to be appeals. It's still not over. But this is further than most people
people believe it was going to happen.
This was one of those things that people viewed as impossible, and well, that day is here.
So we will wait and allow the jury to make its determination.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}