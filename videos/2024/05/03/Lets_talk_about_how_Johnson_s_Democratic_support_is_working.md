---
title: Let's talk about how Johnson's Democratic support is working....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=GIMO3hkgw7g) |
| Published | 2024/05/03 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the situation Speaker of the House Johnson finds himself in, with newfound support from the Democratic Party.
- Johnson is now forced to defend himself against claims of being a "rhino" or cutting deals with Democrats.
- Marjorie Taylor Greene is moving forward with a motion to vacate Johnson.
- Johnson, initially secure, now has to defend his conservative record and faces rumors of engaging in backroom deals.
- The lack of evidence in the modern Republican Party doesn't hinder belief in baseless claims.
- Democratic Party's offer to help Johnson might actually be a liability rather than assistance.
- Greene claims she's not defying Trump's wishes, hinting at a McConnell statement that Johnson neutralized Trump on an aid package.
- Speculation arises that Trump may be using Greene to oust Johnson to save face.
- If Trump fails in this move, he risks looking weaker to Republicans.
- The Democratic Party's support could ensure Johnson retains his seat, further dividing the Republican Party.
- Greene's actions might unintentionally strengthen the Democratic Party's position.

### Quotes

1. "It does appear that in pursuit of a more conservative framework for the Republican Party that Green got outplayed by the Democratic Party and now she's doing all of the work for them."
2. "The lack of evidence in the modern Republican Party doesn't hinder belief in baseless claims."

### Oneliner

Beau explains Speaker Johnson's dilemma with Democratic support, leading to baseless claims and potential Republican Party divisions.

### Audience

Political analysts, party members

### On-the-ground actions from transcript

- Support candidates based on policies and actions, not baseless claims or party affiliation (exemplified)
- Stay informed about political developments and understand the dynamics within parties (exemplified)
- Engage in constructive dialogues to bridge divides within political factions (exemplified)

### Whats missing in summary

Insights on the potential long-term impact on the Republican Party and upcoming elections.

### Tags

#Politics #RepublicanParty #DemocraticParty #SpeakerJohnson #MarjorieTaylorGreene


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk a little bit more
about the situation the Speaker of the House
finds himself in.
We're gonna talk about Johnson
and how he is trying to deal with his new found support
from the Democratic Party.
It wasn't too long ago that we talked about this
we ran through the various options that the Democratic Party had. By the end of it, I think
most people understood that even though it seems counterintuitive that protecting Johnson actually
gave the Democratic Party an edge and it was likely to create a situation where Johnson had
to defend himself against claims of him being a rhino or cutting some kind of deal with the
the Democratic Party, so on and so forth. So, Marjorie Taylor Greene is, you know,
continuing to say she's going to move forward with a motion to vacate. Johnson,
who seemed pretty secure in his position not too long ago, is now at the point
where he is saying stuff like, the motion to vacate does not offer a recipe for
for conservative policy. It's a recipe for chaos. Which, that in and of itself, not a
big deal. But he's also had to say, I'm the most conservative member who has ever
held the gavel as speaker. He's having to defend himself now. He is now in a
position where he's being viewed as somebody who cut a deal. In fact, it
certainly appears that the space laser lady herself is kind of pushing out the
idea that he engaged in some slimy backroom deal with the Democrats. And
maybe even there's rumors that suggest there's a shared
speakership with the Democratic Party. Now these are all within the Twitter
faction and MAGA faction. To be clear, there is absolutely no evidence that any
of that occurred, but it's the modern Republican Party. It's not like evidence
is going to really be needed for these claims to be believed. So Johnson, who
was on relatively firm footing is now having to defend his conservative record.
It does appear that the Democratic Party offering to help in the way that they did might have
been more of a liability than assistance.
Now there is one other very interesting development, something that I find incredibly interesting.
Greene is saying that she's not going against Trump's wishes by doing this.
Which is weird because, you know, they went down there and, you know, Johnson and Trump
and they met and sang, you know, songs together and did that little press conference.
But she's not going against his wishes.
That's weird unless you remember what McConnell said.
And he said that Johnson, I believe the word he used was neutralized Trump on the Ukraine
aid package.
That somehow Johnson had gotten the better of Trump.
And we know that Trump is a very forgiving person, right?
You might be forgiven for believing that Trump was using green as a surrogate to get rid
of Johnson.
The thing is, if Trump fails, he looks even weaker to Republicans up on the Hill.
That's quite the move.
has to succeed or he is going to lose some political capital. And since the
Democratic Party is being oh so accommodating in protecting Johnson, it
seems pretty likely that Johnson will hold on to his seat as long as they, you
know, make good on their their pledge of support. There were a lot of people
questioning the wisdom of that. It hasn't been that long and it's already starting
to pay off. This will deepen the divides within the Republican Party between
what what's viewed as mainstream or moderate Republicans and the MAGA
faction, Twitter faction. This is especially true if what Green is saying
is accurate. If Trump is okay or maybe even pushing for this move, the
divisions that are going to be very apparent here soon, that can only help
the Democratic Party come election time. It does appear that in pursuit of a more
conservative, you know, framework for the Republican Party that Green got outplayed
by the Democratic Party and now she's doing all of the work for them. Anyway,
It's just a thought y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}