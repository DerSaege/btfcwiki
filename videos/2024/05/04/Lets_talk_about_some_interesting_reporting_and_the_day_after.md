---
title: Let's talk about some interesting reporting and the day after....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=1LbB7iVtXxM) |
| Published | 2024/05/04 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about some very interesting reporting that came out last night.
And we'll just kind of go through it.
As soon as it came out, long time viewers of the channel, they sent quite a few messages.
And they're expecting a very specific video.
specific style of video for me. I can't make it. Not yet. I hope I get to, but this is
not all the way there yet. Okay, so for quite some time we have been talking about what
it would take, the ingredients needed, to achieve a real piece. You need dump trucks
full of cash for aid and reconstruction.
You need a regional security force
that would include Arab nations
that have the best interest of the Palestinians at heart
and then Western partners
to make sure that Israel is comfortable.
You also need
a Palestinian state or a viable pathway to one.
The dump trucks full of cash. They're there. Not enough dump trucks yet.
Not for total reconstruction. But to get started, they're there. They exist.
I recently indicated that I thought they had the regional security force.
The New York Times has some very interesting reporting that is heavily sourced. Eight sources.
It says that Israel is open to an alliance or power sharing, not a regional security force,
so I'm not going to use that term, but it would include Saudi Arabia, the UAE, Egypt, and Western
partners. Now, Jordan is not in the reporting, but I'm pretty sure that
Jordan is just not in the reporting. I actually think they're on board. So that
is one of the huge hurdles that had to be overcome. Israel has a plan that they
have been toying with that includes this and they have been looking at it for months.
It's the only way.
Now so why can't I make the I told you so video pieces at hand because it's not.
Not yet.
The Israeli plan on this doesn't include a real pathway, it's not there.
Governance but not a state.
So it doesn't have all of the ingredients yet.
Now what is interesting is that the Arab partners who you have to have to make this work, they
They have said, well at least Saudi Arabia has said, they're not doing it unless there
is a credible pathway, an irreversible pathway.
That's their bare minimum to get involved.
Something else that is worth remembering when it comes to stuff like this, when you are
doing this kind of reporting, it doesn't take place in a day.
The developments that have occurred over the last 48 hours, the information gathering for
that report probably occurred before then.
So now that you have the United States, you know, making the problem bigger, doing the
Eisenhower thing, whoever pulled that quote, that perfectly fits.
Eisenhower once said, if you can't solve a problem, enlarge it.
The U.S. is doing that with that deal, with the Saudi Arabia-Israeli normalization of
ties, the Defense Pact, that's what it's all about and the goal is to push for a
irreversible pathway to a Palestinian state. This is what all of their moves
have been towards. This is what Biden's been doing. They're close. They're close.
The reason I can't make that video is because it's not there yet, and as somebody who has said that this is what it was
going to take for months, and I'm optimistic of it, I give them a 50-50 shot.  There's a lot of details that have to be
worked out.  out, but what all of the reporting over the last 72 hours indicates is that all of the
major players are open to dump trucks full of cash for aid and reconstruction, a regional
– well, not a regional security force – an alliance and power sharing that does the exact
same thing as a regional security force.
And most are not open to the idea of a Palestinian state or a pathway to one.
They are demanding it.
This is an omah moment.
This is the chance.
This is the chance for peace.
I hope that everybody involved is wise enough to make a good faith effort to make it work.
These moments, they don't come around very often, and it normally takes something really
bad to bring them up. It would be great if everybody seized on this one and made it happen.
They are more than halfway there and they have all of the support that they need. It's
the time to do it. This is the chance. I hope I get to make that video. Anyway, it's just
Just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}