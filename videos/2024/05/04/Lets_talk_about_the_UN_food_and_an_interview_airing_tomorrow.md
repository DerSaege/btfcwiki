---
title: Let's talk about the UN, food, and an interview airing tomorrow....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=qNJv9yzxjuM) |
| Published | 2024/05/04 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, let's bow again.
So today, we are going to talk about an interview
that is scheduled to go out tomorrow.
It's scheduled to air tomorrow,
but because of some of the things that were said
during the interview, there's already reporting about it.
So we're going to go through what was said,
the important part, and talk about how it is different
than what has been said before.
The interview was for Meet the Press and it was with Cindy McCain who is an American with
the United Nations World Food Program.
The important quote, there is famine, full-blown famine in the North and it's moving its
way south. Okay, this term has come up before. When we talked about it before, we
talked about how there's a scale. This, what is being described, as I understand
what is going to be said, it's the top end. This isn't we're on the brink of
of famine, this isn't, famine is inevitable if you don't act now, it is already occurring.
Now this moment, when you hear that word and it brings up all of the imagery that it brings
up, that's what's occurring, that's what's being said.
She is near as I can tell from what is going to be said in the interview.
She is going to say that there has to be a ceasefire and that the gates up north have
to be open and basically running 24 hours a day.
This is no longer trying to stop it from getting to that point.
It's already at that point and it is incredibly hard to come back from it.
She is reportedly going to make all of this very clear in the interview.
Blinken is over there right now trying to apply pressure to get the gates opened.
Jordan is sending tons and tons and tons of food still.
does this impact other things? Obviously, if there is famine in the North and it
is moving south, please understand this is not a large area. If you're not
familiar with the geography, this isn't big. If that is already occurring, you
You cannot displace hundreds of thousands of people.
The move into Ra'afah, regardless, is a bad idea.
It will make this situation worse.
If things don't change, it's according to what is going to be said, it's heading that
way anyway.
This is something the United States has to pull out all the stops on.
Everything.
This is one of those things that makes normal foreign policy constraints go away.
The time to act was months ago when the reporting about this first started.
The person over the World Food Program has said what needs to happen.
The pieces in place for a lasting peace, they're there, they exist.
It would be incredibly wise to use them.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}