---
title: Let's talk about Trump, Hicks, NY, and strategies....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=RwT9z0ix1Dc) |
| Published | 2024/05/04 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Examines Trump, New York, Hicks, and strategy.
- Strategy seen before that diminishes Trump's claims.
- Hicks' statements viewed as damaging to Trump.
- Trump's go-to claim of "Democratic witch hunt."
- Strategy used during January 6 hearings.
- Most damaging statements not from political rivals but from supporters.
- Undercutting Trump's main narrative.
- Strategy likely to play out in future entanglements.
- Most critical information coming from people on Trump's side.
- Expect more revelations from Trump's supporters.

### Quotes

1. "It's hard to say that when the most critical pieces of information are coming from people who, well, I mean, they're partisan, but they're on your side."
2. "You know, you've already seen friends and people that worked with him for years, and you can expect to see more of that."
3. "Get used to that surprise because I think we're going to see it a lot."
4. "It's manufactured, that it's a political witch hunt."
5. "It'll only play within that base who, honestly, short of Trump walking out and telling them to believe that it was true, nothing would convince them."

### Oneliner

Trump's strategy of claiming a "Democratic witch hunt" is being undercut as damaging revelations come from his own supporters, not political rivals.

### Audience

Political analysts, activists, voters

### On-the-ground actions from transcript

- Analyze and understand the strategy being used by political figures (implied)
- Stay informed about developments in political entanglements (implied)
- Be prepared for potential surprises in future revelations (implied)

### Whats missing in summary

Analysis on the potential impact of these damaging revelations on Trump's political standing and future strategies.

### Tags

#Trump #Strategy #PoliticalAnalysis #Supporters #Revelations


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about
Trump and New York and Hicks and the strategy.
Because we're seeing a strategy start to play out
and it's one that we've seen before.
And when it gets used, it diminishes.
one of Trump's just go-to statements, one of his big claims, that his base really leans into.
Okay, so if you miss the news, Hope Hicks, she said some things that are widely being viewed as
pretty damaging to the former president.
And there's a lot of conversation about it.
But we've seen this before.
We've seen it before.
We saw the same thing during the January 6 hearings.
Because they used the same strategy.
And I'm not talking about the tell them what you're going to tell them, tell them, tell them what you told them thing.
Yeah, they're doing that too.
But there's something else.
There's something else.
Anytime one of these situations comes up and Trump has to talk about it, what does he say?
What does he call them?
Every one of these cases.
It's a democratic witch hunt, right?
And normally his most conforming base, they can lean into that because somewhere in some
part of the case, there is somebody who's connected to the Democratic Party.
So there's that kernel of truth that they can lean into and say, that's it, that's
what it is.
But that only applies to his most believing base.
When you think back to the January 6 hearings, I mean, a whole lot of the people involved
in that really were members of the Democratic Party, like, I mean, like openly, full-blown
members of the Democratic Party.
Why didn't that work then when you're talking about the majority of people?
Because while those people who might be behind the inquiry are members of the Democratic
Party, the witnesses weren't.
They were his supporters, in some cases, his most ardent supporters, people who really
believed in him.
It appears that New York is doing the same thing.
They're creating a situation where the most damaging statements are not coming from Trump's
political rivals.
They're coming from his supporters.
coming from people who have been with him for years.
It makes it much harder for Trump to just walk out there and blow it off as something
that is partisan in nature.
You know, you've already seen friends and people that worked with him for years, and
you can expect to see more of that.
This is probably a strategy that we are going to see play out in every one of his entanglements
because it undercuts his main narrative, that it's manufactured, that it's a political
witch hunt.
It's hard to say that when the most critical pieces of information are coming from people
who, well, I mean, they're partisan, but they're on your side.
This claim, he'll still make it.
But I don't think it's going to play well to the average American.
It'll only play within that base who, honestly, short of Trump walking out and telling them
to believe that it was true, nothing would convince them.
The people that they're bringing up, they undercut that narrative and they take away
one of his biggest talking points.
I would expect to see a whole lot more of that.
So right now, you're getting a lot of coverage about how, you know, it's so surprising that,
know, this came from her. I'd get used to it. Get used to that surprise because I
I think we're going to see it a lot.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}