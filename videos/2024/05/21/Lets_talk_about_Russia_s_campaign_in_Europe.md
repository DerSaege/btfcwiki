---
title: Let's talk about Russia's campaign in Europe....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=4m1EuQf68Qg) |
| Published | 2024/05/21 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about something
that I mentioned over on the other channel
that prompted a whole lot of questions.
So we're going to kind of delve into that topic
a little bit deeper.
Some of this, a lot of this we've actually mentioned
on the channel before, but referring to it the way I did
apparently caught people's attention a little bit more.
So today, we are going to talk about Russia's campaign
in Europe related to sabotage, for lack of a better word.
I know that we talked about a couple of people
who were picked up in Germany who were allegedly
gathering information about military facilities in Germany
so they could conduct sabotage operations at them.
Some of those facilities were US facilities.
But on top of that, when it comes to the physical sabotage, the stuff that people normally associate
with that word, you also have jamming of GPS signals, which impacts civilian aviation.
You have a number of buildings that have caught fire that happen to be linked in some way
to Ukrainian defense industries.
Leaving the traditional stuff and moving into the computer-based stuff, there's a whole
lot of hacking operations going on, and they do appear to be directed by Moscow.
You have the ever-present disinformation operations that we have talked about a lot on the channel.
And then there are a number of other cyber attacks happening.
Then you go to the espionage side of it.
You have bugs, listening devices, that have been found.
Poland, I think, yeah it was Poland, caught a guy trying to, allegedly, trying to pass
information about an airport to Russian intelligence.
airport in question happens to be one that is pretty critical to moving
supplies into Ukraine. There's a bunch of it and European intelligence agencies
have kind of been signaling more and more that this is escalating, that
there's a lot more of it happening and in fact some of the stuff that they
thought was unconnected is connected. So this will probably become more of a
conversation. What do you need to know about this? First, this is pretty normal.
I mean it's not normal obviously, but when you are talking about these, this
type of conflict, the, an opposition force coming along and trying to disrupt supplies
from behind, gather intelligence, gather information to allow them to better
respond. All of this is normal. Even the the buildings catching fire, that's
something else that occurs when you're talking about these kinds of
intelligence operations. Please keep in mind that for a long time there was a
pretty active string of buildings in Russia that caught fire. Those weren't
all from, you know, a Russian soldier being careless with his matches. It
occurs. This type of blended warfare happens. You're just going to start
hearing more and more about it because some of it is significant and now there
There have been reported hacking attempts aimed at government officials at a level that
is going to require a response.
Now in Europe, making these allegations officially, it gets kind of hard because of the burden
of proof.
But I would imagine you're going to start getting leaks, quote, authorized leaks that
detail some of the things that Western intelligence believes is happening, and it probably is.
Russian intelligence wouldn't be doing their job if some of this wasn't occurring.
Some of it is outside of what is best practices as far as protocol, but I mean it's nothing
that it is nothing that is outside of the norm when you're talking about Cold War games.
And make no mistake about it, Western intelligence is playing the same game there.
Done a little bit differently.
There are reports, and this is light reporting, not fully sourced to my satisfaction, but
There is light reporting that suggests Russian intelligence is actually hiring far-right
elements and just criminals to engage in some of these activities, which would explain why
they're kind of all over the place and they haven't really been that successful.
But I would expect over the next month to hear more and more about this in the U.S.
and certainly in Europe.
So that's what that was about.
Russia escalating their sabotage campaign or whatever I said.
This is what it's about.
It's becoming more frequent.
It really doesn't appear to be more successful, but when you're talking about this kind of
remember the failures are known, the successes are not. So it's hard to gauge
that, but it's something that is occurring in the background that will
probably get more coverage soon.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}