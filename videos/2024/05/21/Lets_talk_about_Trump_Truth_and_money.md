---
title: Let's talk about Trump, Truth, and money....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=z2y4bNpMUAM) |
| Published | 2024/05/21 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump and truth
and numbers.
We're gonna talk about money.
The former president's social media platform,
we have some numbers from the first quarter
that we're gonna go over, so it is from TMTG,
which is the parent company of Truth Social.
And the numbers indicate that they had $770,500 in ad revenue.
And they had $327.6 million in losses.
I mean, that's a pretty big discrepancy, that's a lot of money there.
It is worth noting that a decent chunk of the $327 million in losses was related to
the merger and their one-time losses.
Even setting that aside though, that's a pretty big gap for a company that has a market
value of more than $6 billion, billion with a B.
And even if you were to set aside the one-time losses and the losses in general, there's
still another issue when it comes just to the revenue.
$170,500 this year, $1.1 million last year.
Their revenue is going down, which is not something you want to see in a company that
is expected to be growing.
Even if you are generous and you say that it's due to ad rates being lower, which could
be true.
That still would indicate it would probably be a slight decline, but even being generous,
it would be running flat as far as usage.
These are not good numbers for the former president's social media platform.
It appears at this point that this endeavor is not going as well as Trump might have wanted
it to go.
There were a lot of expectations when it came to this platform, a lot of things that Trump
said about what it was going to do and who it was going to compete with and all of that
stuff.
Based on these numbers, it seems incredibly unlikely that this is going to be a rival
to Twitter or Facebook or anything like that.
So I guess you can add this to the list of previous Trump endeavors and make the comparisons
that you see fit.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}