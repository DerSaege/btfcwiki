---
title: Let's talk about Schumer's move against Trump and the GOP....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=DDF_Ug6s3f4) |
| Published | 2024/05/21 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Schumer's move.
We're gonna talk about what Schumer's doing in the Senate,
why he is doing it, what it's actually about,
and why it puts Republicans in such a tight spot
because he's setting them up for a very difficult vote.
Okay, so what's going on?
Schumer has indicated that sometime this week he is going to bring to the floor the border
bill, actually I think it might actually be called the Border Act, that long stalled border
security bill.
It's bipartisan.
Why is he doing it?
It doesn't have anything to do with the legislation at this point.
It's pure politics.
It's not about the legislation at this point.
What he's doing is setting up a difficult vote for Republicans in the Senate because
they have two options.
They can vote for it or vote against it.
If they vote against it, the Democratic Party gets to come out and say, see, Republicans
don't actually care about the border.
It's just something they're trying to blame Biden for.
And in fact, remember Trump said not to do this and they all knew it was because he wanted
to blame Biden.
They're actively standing in the way of border security.
They're putting you at risk and all of this stuff.
And that sounds like a hard sell, but it's not because Trump reached out to try to undermine
this bill.
And when he did it, it upset people.
So there's a bunch of quotes out there like this, the border is a very important issue
for Donald Trump and the fact that he would communicate to Republican senators and Congress
people that he doesn't want us to solve the border problem because he wants to blame Biden
for it is really appalling.
That's Romney.
Now I know for a whole lot of people, you know, Rhino Romney or whatever, but you also
have this, I didn't come here to have the president as a boss or a candidate as a boss.
I came here to pass good solid policy.
It is immoral for me to think you look to the other way because you think this is the
linchpin for Trump to win.
That's Tillis.
There's no shortage of these quotes.
So if they vote against it, well, they take a hit because the Democratic Party will be
able to use this in attack ads against the senators who say they're for border security
but then vote against it.
And they get to use it against Trump because there's all these quotes out there because
people were upset that Trump, a private citizen, tried to inject himself in this way.
So why don't they just vote for it?
Because then Trump will be mad at them.
It puts them in a situation where they have to decide whether they're going to do with
their rhetoric and constituents want or whether or not they're going to obey dear leader.
That's what this is about at this point.
Now as far as the Democratic Party is concerned, my guess is Schumer is going to tell them
to vote their conscience, which again, in case you missed the previous videos where
we talked about that, that doesn't actually mean vote your conscience.
It means vote the way that will help you most politically.
If voting for this polls best, do that.
If your constituents don't want you to vote for it, don't vote for it.
That's what it means.
But there won't be a party position on it.
So what happens if the whole Schoolhouse Rock thing occurs and this bill actually gets through
both houses and everything happens?
My guess is that Biden will sign it, but I don't know how likely that is.
At this point, this move that you're going to see this week, it's just drawing attention
to the fact that while Republicans run around screaming about the border, they have been
actively actively stopping anything from happening.
And when you look at the quotes, there are a lot of Republicans who believe this legislation
would quote solve the problem, solve from their perspective.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}