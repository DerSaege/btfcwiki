---
title: Let's talk about the US response to the ICC....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=L_IdMNT-5bw) |
| Published | 2024/05/21 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about the US response
to recent developments concerning international law.
We're gonna do this because
there's a lot of questions coming in
and a lot of them are based on the desire to fit new events
new events into preconceived notions or narratives. And the main question at the
heart of it is why is the US response what it is? And that question actually
assumes that we know what the US response is already. And I know the news
came out, the ICC said, hey we have these applications for these warrants, and
pretty quickly US officials from across the government, regardless of partisan
divide, they came out and they said, it's shocking, we're in disbelief, this is
outrageous, can't believe there's an equivalency, so on and so forth. People
are taking that as the response to the merits of the case or the situation on
the ground. If you want to believe that, you have to believe two things. One, that
it's a good idea to accept statements from politicians at face value when they
kind of appear coordinated, oftentimes to include the same terms. So you
You have to believe that, and you have to believe that I can assess what the ICC was
going to do weeks in advance better than the entirety of the US State Department, the intelligence
community, and the White House.
I'm not that good.
ago we talked about how the warrants were going to come out and the
applications were going to go in. There was going to be a set for Hamas, a set
for the Israelis. You have the entire government acting like this is a
surprise right now. It's not. The equivalency, that was pretty apparent, that
was going to be there. That's what the ICC was going to do and they knew that
as well. That terminology, that was probably drawn up weeks ago. The
equivalency thing, probably about the same time that video went out when it
became very apparent what the ICC was about to do. The the US probably is not
really that upset about what the ICC is alleging. I mean, if you remember, let me
get my notepad here, it was recently that the US government said it was, quote,
reasonable to assess that Israel did pretty much exactly what's alleged in
application for the warrant. So it's not that. This doesn't have anything to do
with the merits of the case. It doesn't have anything to do with the situation
on the ground, and it certainly doesn't have anything to do with loyalty to
anybody. If you look at all of the evidence about what the US reaction is,
you start to realize it's not about this exact situation. So what's it about?
It's foreign policy. What's it always about? It's about power. Take away all
country names, all locations, all individual names. Take it all away and
describe the mechanics of what the ICC is doing. They are attempting to
prosecute people from an establishment state that is a non-signatory for
actions that occurred while they were pursuing a non-state actor in an area
that the ICC claims jurisdiction over. How many times a year does the United
States do that? That's what the reaction is about. The ICC, as far as the US is
concerned, there's always been a little, let's call it hostility from the US to
the ICC, invade the Hague, and all that. If the US does not oppose the ICC move
here, it kind of, well, it relinquishes that power on the international scene.
The US isn't going to do that. That is what this early reaction is about. It has
nothing to do with the actual situation over there. Nothing. It's about kind of
undermining it on a on a general principle level because the US would not
be happy if the ICC pursued charges against an establishment state that's a
a non-signatory for going after non-state actors in an area they claim
jurisdiction over. That could very easily be applied to the US. So what is the US
response? Don't know yet. They haven't got that far. You haven't seen any real
signals about that yet. What you are seeing right now is general pushback to
the mechanics of what the ICC is doing. That's it. If you want to know what the
US is doing as far as the response and how they're going to deal with the
situation when it comes to Israel specifically, I would pay real close
attention to a potential budding relationship between US officials and a
a person named Gantz.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}