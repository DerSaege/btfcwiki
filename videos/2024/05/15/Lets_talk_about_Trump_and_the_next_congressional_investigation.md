---
title: Let's talk about Trump and the next congressional investigation....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=MdHRcyfWhOM) |
| Published | 2024/05/15 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump, oil, energy companies, and potential congressional
investigations.
We're going to do this because even though he's not in office, those appear to be on
the horizon.
Recently, we talked about a reported incident in which Trump was at a dinner with some energy
and oil executives.
And the reporting certainly seemed to indicate that he made a pitch of a request for like
a billion dollars in donations, the whole Dr. Evil thing, while also talking about what
he could do for them. So it appears that the Oversight Committee in the House and
the Budget Committee in the Senate, at least parts of them, are interested in
well, looking into the matter. Out of the two, the one in the Senate would have the
most teeth with subpoena power. Now it appears that representative Raskin sent
out letters to the CEOs of the energy companies as well as to the American
Petroleum Institute, which I think is a trade group.
And they include phrasing like, media reports raise significant potential ethical campaign
finance and legal issues that would flow from the effective sale of American energy and
regulatory policy to commercial interest in return for large campaign contributions.
And basically they're asking for information about what occurred.
If this was to be duplicated in the Senate, that means they would have to answer.
They would have to answer the questions.
The letters from the House, I mean it's probably not a good idea to just ignore them.
But ones that would start with the budget committee, which the chair of that committee
has expressed some interest in it, those would have a lot more power.
But it does appear at this point that there is going to be some kind of congressional
investigation into Trump again.
And this will be about the appearance of what occurred there and they will try to find out
exactly what happened because the phrase quid pro quo is being tossed around a lot by people
who would be responsible for investigations like this.
So expect to see that coming soon.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}