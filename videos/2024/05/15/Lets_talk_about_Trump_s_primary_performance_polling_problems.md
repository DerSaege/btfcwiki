---
title: Let's talk about Trump's primary performance polling problems....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=82FCXFx57QY) |
| Published | 2024/05/15 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Bill again.
So today we are going to talk about Trump
and performance, primaries, polling and problems.
This is something that we have talked about before,
but it happened again.
And we also had a question last time
that I didn't have the information for.
I couldn't answer it because there was nothing
really to go on, but now I have that information.
So we'll go over that as well.
So what's the issue?
The issue is that when you're looking at the polling about Trump, I mean, to capture
the lead that a lot of these polls say that he has, he has to be capturing independents
and moderates outside of his own party.
The problem is, when you look at the primaries, that's not really what it seems to indicate
people actually go to vote. Recently he lost one out of five Republican voters
in a primary to Nikki Haley. It seems unlikely that he has a good reach
outside of his own party when that's occurring. There were three primaries
today. What happened? In Maryland, with 71% reporting, he's losing one out of
of five Republican voters. Nebraska, 55 percent reporting he's losing one out of
five. West Virginia, he's doing better there. He's doing all right. It's like one
out of ten, which is good. That's okay. So you have a pretty consistent theme of
him not being able to capture the moderates inside his own party. If you
can't capture Republican moderates it seems unlikely that you're getting those
that don't have a party affiliation, those that are true moderates outside of
the Republican Party, those that are more centrist. It just it seems off and I
I don't have an answer as to why.
Last time we went over it, I went over the various options, you know, non-response bias,
not being able to figure out who the likely voters are, all of that stuff.
But I can't say any of that for certain.
Meanwhile, those very supportive of the polls are like, well, trust us.
I mean, okay.
And when I talked about it last time, the one thing they kept coming in was, well, this
is just cope.
It's just cope.
You're pointing to it being off with Republican candidates.
You'd have to show it being off with Democratic candidates too.
You're right.
You are right.
That is something I would need to do.
So to check it, you would have to go to a different race.
You can't use the presidential race because that's leaning the other way.
You would have to find a different race that had polling that might capture some of the
same dynamics.
So in Maryland, there was a Democratic Senate primary.
You had two main candidates.
One was Trone, one's also Brooks.
And up until May 8th, May 9th, Trone was in the lead.
Around May 8th, May 9th, they were neck and neck, dead heat type of thing.
You had some polls that showed also Brooks a point or two up, but nothing major.
They were really close.
That was just a few days ago.
So with 60% of the vote in, they've called it.
Why?
Because Oslo Brooks is up by double digits, 12, 13 points.
So that indicates that, again, something is off.
Is there any more we can read into it?
Well, Trone, self-funded candidate, pretty wealthy guy.
The other thing is that out of the two I would assume that he was perceived to be the more
conservative because there was a lot of reporting about some donations that were made and that
probably made people believe that he was the more conservative of the two
candidates. It favored him. The polling favored him. The polling favored the more
conservative candidate in a race that was just the Democratic Party. Is that
scientific? Is it something that you can say this is definitely what's happening?
No, no absolutely not. What it shows is that something is off. Maybe it's a
non-response bias. Maybe it is that, even though I do not like that as an answer.
That is generally not the answer, but maybe that's really what it is this time.
Maybe they have a hard time figuring out who's going to show up, who the likely
voters are. We had issues with unlikely voters in 2022 and 2020 when it came to the polling but it
wasn't this pronounced. There is something off and I know that Biden's team did the whole interview
about this I think yesterday or maybe this morning but when you look at the
results of the primaries you see issues you see issues and they exist outside of
you know the margin of error so it is something to keep in mind when you see
the polling. And keep in mind, if it starts to swing the other way, it's
something to keep in mind. Even if your preferred candidate starts, you know,
really doing well in the polling, just remember there have been issues. So at
this point, I'm gonna say the only poll that matters is the one in November.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}