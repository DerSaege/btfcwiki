---
title: Let's talk about Biden's veto promise....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=dRcx2H-y-fM) |
| Published | 2024/05/15 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about some proposed legislation from Republicans
in the house again, uh, Biden's response to it, some questions from y'all about
it, and then we will talk a little bit about some light reporting on something
that is related, but separate, and just kind of go over the information that's
available. Some of y'all will probably find this very funny. Okay, so Republicans
in the House decided they were going to try to actually pass some legislation,
and that legislation would force Biden to get rid of the pauses that he has on
the shipments to Israel. Okay, now I know it's been a while since Republicans in
the House actually got any legislation through.
But I put out a video kind of indicating
that maybe they should watch some Schoolhouse Rocks
because once it passed the House,
it would have to go to the Senate.
And then it would go to Biden, the person they're
trying to compel to do something.
And that if he didn't want to do it, well, he would just veto it
because he has to sign it because that's
how the US government works.
Apparently, that's not a concern of theirs, because for whatever reason, they're going
to try to move ahead with it.
Biden has said if it passes, that he will veto it.
So there's that.
Okay, on to the questions.
Do I think it will pass?
I still don't have a full vote count yet, but it doesn't actually look like it.
It looks like it, it would stall out in the Senate.
Um, then can he veto it?
Yeah, will he?
I believe he will.
Um, and then a question about why do Republicans suddenly care about this?
They don't.
They, I don't even think that the legislation that's written actually
covers the shipment that's on pause, just to be clear.
This is something, I believe Biden said it was a misguided effort based on the deliberate
distortion of the White House position.
Yeah, I mean, that seems to track.
I don't actually think they care or understand.
I think that this is, they believe this is an issue they can latch on to.
And I don't believe it will ultimately be successful.
The next question, is Biden's constitutional argument sound?
And that's an interesting question to be honest, because when looking at it, I'm not sure.
It's a sound argument, but the counter argument would also be sound.
Biden said this bill could raise serious concerns about infringement on the president's authorities
under Article 2 of the Constitution, including his duties as commander-in-chief and chief executive
and his power to conduct foreign relations. All of this is a sound argument. The thing is,
Congress has the money. They have the power when it comes to the money. The money was approved and
he signed it. And I think that that would be their argument, that this is now legislation and he has
to do it. I mean, if for whatever reason the new legislation was to pass and he
signed it by accident, I think it would end up going to court because I think
there are, I think there are good arguments for this from either side. I
I believe that this would end up in court.
But I don't actually think it's going to get to that point, to be honest.
Okay.
The other thing to remember when people are talking about the Republicans caring, it's
important to understand that Biden didn't stop all arm shipments.
He stopped stuff that he wanted to make sure was not used in RAFA, is really what
it appears. There's other stuff that is still moving through and that actually
brings us to the light reporting. Some reporting came out that said there was a
new deal for new weapons to go to Israel. And it doesn't have a delivery date, can't
even tell if this is new or old. The reporting is very, very thin. But in it
there's one thing that's really interesting. It really looks like it's
three different things that are looking at being transferred. Tank ammo, mortar
ammo and tactical vehicles. I wonder what those are. Every American that was in Iraq
or Afghanistan is picturing something right now. That would be my guess too.
We'll have to wait and find out, but it certainly appears based on phrasing and
stuff like that and the time that this is that this is occurring that whole
thing about you know chasing them around playing whack-a-mole very long
commitment they would need something that would probably be described in the
media as tactical vehicles.
So this is where the mission creep stuff starts.
We need to wait to find out exactly what these are, but
there is a high probability that this is for a long-term
commitment.
And I don't mean another few months.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}