---
title: Let's talk about AZ, good but not great news, and the 1800s....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=gT9Hk_Y3u6A) |
| Published | 2024/05/15 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about Arizona.
And good news, good news, not great news,
but something definitely worth noting.
There's been a development and it does help
a whole lot of people, but it's not perfect.
Okay, so quick recap.
The Supreme Court in Arizona made a decision.
that decision basically made a law that had its roots in the 1800s enforceable.
And it had to do with reproductive rights. It curtailed reproductive rights
extensively. Through hard work from the Democratic Party and even a couple of
Republicans, that law got repealed. However, due to a weird quirk in the way
the laws work there, a repeal wouldn't go into effect until 90 days after the
legislative session ends. So this really old law that is already repealed would
actually go into effect for a pretty lengthy period. The Democratic Attorney
General went back to the Supreme Court to get a delay, to get a stay of
of enforcement on this really, really old law.
The Supreme Court provided one.
So that's the good news.
The reason it's not great news is because they provided a 90-day stay and the legislative
session is not over yet.
So this incredibly old law will go into effect for a limited period.
I want to say it will begin September 26th.
It will go into effect.
Now judging by the way the legislative session typically ends there, it'll probably be in
effect from September 26th to mid to late October, which is a whole lot less time.
But it is still going to go into effect.
The state is currently under a 15-week ban.
So that's what will be operating until it goes into effect and then until the repeal
takes place, which would happen probably about a month later.
So good news, you know, but not great news because there still will be a period where
this law will technically be in effect.
The odds of enforcement during that period, I don't believe they're high given the opposition
to it.
However, it will probably impact everything.
I would definitely consult an attorney about it.
Good news, not great news.
It does appear that everything is on track to get something on the ballot about this
in November.
the people of Arizona so choose they can deliver great news there. Anyway, it's
And it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}