---
title: Let's talk about Trump, NY, and Sleeping Beauty....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ru6nnwgbyLI) |
| Published | 2024/05/06 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about
former President Donald J. Trump
and his proceedings up in New York.
And Sleeping Beauty,
and something that is apparently beginning to really bother
the former president,
so much so that he's now starting to comment on it.
If you've missed the reporting on it, there have been a number of instances where it was
reported that the former president fell asleep during his proceedings, multiple times, on
different days, actually.
He apparently really doesn't like this.
This is something that is really starting to bother him.
he took to social media to set the record straight. So, old Dozing Don, he
said, contrary to the fake news media, all caps, I don't fall asleep during the
Crooked DA's witch hunt, random capitalizations, especially not today.
I don't fall asleep during the Crooked DA's witch hunt, especially not today.
simply close my beautiful blue eyes sometimes listen intensely and take it
all all caps in exclamation point exclamation point exclamation point how
do you especially not fall asleep and that's one of those things like you know
you're pregnant or you're not right do you have like I don't know it seems like
like an odd statement but it also seems weird that he goes ahead and says that
he closes his eyes. I didn't fall asleep I was just resting my eyes. This is one
of those things because it's Trump. It's probably going to become a personal
grievance for him. This is something he might even start talking about at his
rallies. When that happens it wouldn't be surprising if if the media started to
wonder why he was often late to his rallies. And they'll probably eventually
start asking whether or not you know he was hard to wake up. This is one of those
things that bothers him because he ran with the Sleepy Joe stuff for so long
but now that he's in public for an extended period and he rests his eyes, he
doesn't like that coming back on him. And because of his personality it will be
one of those things that he latches on to. And when it does, it will
probably create a situation where even more people start to talk about whether
or not he is awake or simply intensely taking it all in while he rests his eyes.
Anyways, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}