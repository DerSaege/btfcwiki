---
title: Let's talk about the UK, Ukraine, Russia, and messaging....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=zb-k2TmS3UY) |
| Published | 2024/05/06 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about the United Kingdom
and Ukraine and Russia and international signaling
in a very, very, very British way.
So we'll talk about some other developments,
but we'll talk primarily about that
because it's probably gonna make the biggest headlines
here in the US.
The headlines are probably going to say something along the lines of UK OK's use of their
weapons to strike the Kremlin or something like that, which isn't actually what happened,
but it's going to be American headlines.
What did occur is the Foreign Secretary, Lord Cameron, he did that very British thing where
he said something without actually saying it but everybody knew what he was really meaning
by it, kind of.
And it's being taken to mean that the UK is giving authorization for Ukraine to use
its weapons within Russia, which is something that there's apparently been a quiet understanding
about.
That tone shift seems to be pretty present.
So what did he actually say?
Just as Russia is striking inside Ukraine, you can quite understand why Ukraine feels
the need to make sure it's defending itself.
Doesn't actually come straight out and say, yeah, hit them.
But there's an understanding, and you would understand that too, right?
This is one of those things where if this was a change that was being made, it wouldn't
be announced this way.
It would be announced with a conversation with Ukraine.
It wouldn't go public.
This is messaging to Russia.
The tone shift has to do with the fact that the United Kingdom has kind of signed off
on providing, I want to say it's 3.7 billion U.S. per year, somewhere around there.
Per year.
British intelligence believes that this is going to continue to be a multi-year event.
Most Western intelligence agrees.
You also have some mixed messaging.
The United States is reportedly asking Ukraine to please stop hitting the oil refineries.
And then the French kind of indicated that, honestly, they're alarmingly cool with the
idea of sending Western troops to Ukraine to fight.
I would not want to be a Russian analyst right now trying to figure out what the Western
intent is because it's all over the place and the messaging is all over the place.
There is probably a concerted effort to make it be confusing.
So as far as the statement about understanding why Ukraine would do that.
My read on it, yeah, they're saying that they could use the storm shadow inside Russia.
That's how I take it.
But any agreement like that, any change to the status quo, it wouldn't really be signaled
publicly first.
Ukraine would know.
So I would just wait and see if it's used that way.
Before reading too much into this statement, even though that is how I read it, I would
wait to see if Ukraine actually sends one of those things inside Russia's borders.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}