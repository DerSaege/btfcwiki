---
title: Let's talk about what we know about over there....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=t6ogvfjrmi0) |
| Published | 2024/05/06 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there internet people, let's bow again.
So today, we are going to talk about what we know.
We're gonna go over what we know.
We will talk about some events that occurred
over the weekend to add a little bit of context.
We will talk about some reporting.
Then we'll talk about what is happening right now
at time of filming.
And then we will talk about what is likely to occur
probably by the time you all watch this.
not shortly thereafter. Okay, so over the weekend, the talks over there, they were
moving along. No big breakthrough, but they were moving. You had all the right
people in place, and the reporting coming out of the talks was back and forth.
forth. Normally a decent sign. The people that were there, good sign. But there
weren't any major breakthroughs. Also over the weekend you had an American
with the United Nations World Food Program do an interview and say that
famine was full-blown in the north. Reporting also surfaced that suggested
Biden had paused a shipment of ammunition headed to Israel. Some kind of delay occurred.
I'm sure that it was probably just being prioritized.
You had an Israeli official reportedly tell the New York Times that Netanyahu's rhetoric
torpedoed the talks. The talk about continuing to go into Rafa no matter
what hardened the stances. Netanyahu of course says that this is not true. At
time of filming it does appear that the talks have collapsed and right now at
time of filming text messages are reportedly going out telling people in
the eastern portion of Rafa to evacuate, to leave. The IDF is saying that this
will be an operation of limited scope. Quote, reporting says that it's about
a hundred thousand people that are being asked to leave. The Department of
Defense here in the United States, their statement is more of the same, no
changes. It's basically, we support Israel, be super careful in Ra'afah, or don't go in.
At time of filming, Biden has not made a statement. I would imagine that by the time y'all see this,
that statement will have been made. If not, it will come shortly thereafter.
I know in the United States, that's what's going to get the coverage, because whatever the statement
is however it goes, it is going to carry pretty big political consequences. I would like to remind
everybody that those consequences will pale in comparison to the consequences that will occur
over there if any move into RAFA goes sideways in any of the thousand ways that it can.
The situation is unstable. There will probably be an update later on today but
right now this is the information that we have. There is no reported timeline on
when a move into RAFA might occur. There is some movement from countries that
have been acting as mediators trying to get Netanyahu to call off any potential move into
Rafa. No clue on how successful that would be, but this is this is how it stands at this moment.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}