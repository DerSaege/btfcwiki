---
title: Let's talk about Trump, Romney, welfare, and being entitled....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=VOem-jQkweA) |
| Published | 2024/05/06 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're going to talk about Trump and something that he has reported to have said.
And we'll talk about Romney as well, because the two things go together, they share an attitude.
And we will talk about entitlement.
We'll talk about that term and what it is.
Okay, so what did Trump reportedly say?
When you are Democrat, you start off essentially at 40 percent
because you have civil service, you have the unions, you have welfare.
And don't underestimate welfare.
They get welfare to vote.
All I could hear when reading that the first time was President Obama rapping, talking
about being 47% through beating Mitt Romney.
Do y'all remember that?
Romney said, all right, there are 47 percent who are with him talking about President Obama,
who are dependent on government, who believe that they are victims, who believe the government
has a responsibility to care for them, who believe that they are entitled to health care,
to food, to housing, to you name it.
that's an entitlement. I don't think that that's entitlement. Feeling that, you
know, the government should ensure food, health care, and housing, shelter, things
that we give to POWs? Yeah, I don't think that that's really having a huge sense
of entitlement. Not at all. I'll say it. I think people should have food, shelter,
and health care. It's the same attitude running through both of them. Running
both of those statements. It's the same attitude. And that attitude is an attitude
of entitlement. Believing that you are entitled to sit in the highest office in
the land and do absolutely nothing for those people who are struggling the most.
most. That is a sense of entitlement. The idea that gets used here, and it's been
very present in the Republican Party for a long time, is that doing things for
your constituents, the people that you're supposed to represent, that doing those
things, that's buying votes. That's the idea. And that's what Trump was
getting at too when he said they get welfare to vote. Buying votes because the
government did a thing to help the constituents. If you are part of a
representative democracy, which is what our Republic is, you're supposed to
represent the interests of the people who elected you. That's your job. So doing
things for them is literally your job. Your job isn't to rule over them. I know
know that there's been a lot of confusion about that lately. If you feel
that you are entitled to be President of the United States but you don't feel
that you have to do anything for those who are struggling, let me tell you
something, those people who need food, shelter, stuff like that, I have never
met any of them that have a sense of entitlement.
If you feel that they're only used to you, that their only purpose, that the only reason
you have to care about them as a politician is to use them to kick down at so you can
make the people who are doing a little bit better than them feel better about
themselves and therefore support you, you are entitled. The attitude of
entitlement is not something that exists among a whole lot of people who are, you
know, living in poverty or don't have homes. That's not where you find
entitlement. One of the ideas of this country was to make it better for
everybody. Didn't get it right, but even close in the beginning, but that was the
general idea. This attitude of politicians saying that they don't have
to do anything for you, that's them being entitled. They feel entitled to your
obedience. They don't have to earn it. They don't have to do anything for you.
You already bought the red hat.
Politicians are supposed to work for you.
They're supposed to represent you.
You'll see more of this in the lead up to the election.
You'll see more politicians try to cast Biden's efforts at student debt relief.
or lowering the cost of insulin, or any of the other things that he's done as far as
things that are aimed at those who need help.
You will see Republicans try to cast that as buying votes rather than him simply doing
his job, which is what it is.
That's his job.
His job is to, well, it's to increase the well-being of the people in this country.
That's what he's supposed to do.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}