---
title: Let's talk about GOP language and a veto in VA....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=TA0Cu2InzKc) |
| Published | 2024/05/19 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about tone, language,
wording, rights, and Virginia and vetoes.
And we're going to talk about something that occurs pretty
much any time I use a specific term and the messages that
come in, and then we're going to talk about the reality.
because the language surrounding a debate is incorrect.
Anytime I use the term reproductive rights
to describe what the Republican party
is trying to curtail well, people come out of nowhere
to say, no, that's not what they're going after.
They're just trying to stop abortion.
No, they are not.
That's not true.
That is not true.
That's how they framed it, but it's not the case, and I've pointed out examples of this
before, but since it has happened again, I will point out another one.
The governor of Virginia just vetoed a bill, and its purpose was to make sure that, quote,
A person shall have the right to obtain contraceptives and to engage in contraception."
And it goes on to say that that right shall not be infringed.
I mean, that's terminology Republicans love, right?
It says, shall not be infringed upon by any law, regulation, or policy that expressly
or effectively limits, delays, or impedes access to contraceptives or information related
to contraception.
It was vetoed.
This is not about abortion.
This is about contraception.
It's different.
They're not just going after one thing.
They're going after reproductive rights.
Now, to be fair, the governor did say that he supports contraception, that he thinks
that's important.
did say that, but he vetoed it and according to the reporting, he said that it was issues
with language.
See, he wanted to make it a policy rather than an actual law.
I mean, I'm not completely familiar with all of Virginia's laws and how everything
works, but I'm willing to bet that a policy is a whole lot easier to undo than a law.
They are not just going after one thing.
are going after reproductive rights. It is that simple. Now the chair of the
Virginia Democratic Party had this to say, Governor Yonkin just proved to
Virginians that once again he does not care about their health or rights.
Contraception is used to manage a wide variety of health conditions and by
choosing not to protect it, Yonkin is hurting thousands of people. This comes
down to health and freedom, and Yonkin and Republicans continue to carelessly and blatantly
disregard both.
The correct terminology is reproductive rights because the Republican Party is not trying
to curtail one thing.
They're trying to curtail an umbrella of rights.
They're going after women's rights.
It's about control.
It's not about any of the moral arguments that they try to frame it around.
It's just another way to say, you little lady need to do what you're told.
Obey.
Because that's what the Republican Party has become.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}