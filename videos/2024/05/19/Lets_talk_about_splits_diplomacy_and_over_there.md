---
title: Let's talk about splits, diplomacy, and over there....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=SLZkrB63gCE) |
| Published | 2024/05/19 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about over there
and the diplomatic and political situation
that is developing.
We'll go through the differences
between the various groups that are emerging
and talk about where the U.S. is at,
what they're doing, and what's happening inside.
So if you have missed it, we talked about it last week.
Large segments of the government in Israel at this moment,
they are questioning Netanyahu, short version.
We talked about Galant coming out and very publicly saying
that there were issues.
He's now been joined publicly by Gantz.
Short version is that at this point, Netanyahu's allies,
it's the far right in the country.
That's what he has left.
Now, Gallant and Gantz actually aren't,
they don't see eye to eye either,
but they are much closer to international consensus.
And they actually have some sort of plan
can be articulated.
Galantz has a lot more cooperation with Palestinians.
In fact, one of the things that came up was, I guess, part of it includes arming some.
That did not go over well immediately.
It is worth noting that unless you want to occupy it, yeah, that's going to happen.
Keep in mind, he's an ex-commando.
He kind of knows what he's talking about.
Just saying.
Now as far as Gantz goes, one of the big things on his side of it is one, he's basically said
June 8th or sooner, if he doesn't see some kind of movement from Netanyahu, he's going
to walk and pull his support.
One of the interesting things about what he's saying, I think people are misinterpreting
something because it's coming across as he doesn't want the Palestinian authority involved.
That's not actually what I'm hearing.
I'm hearing he doesn't want the current leader of the Palestinian authority involved.
I mean, as near as I can tell, most Palestinians don't want the current leader of the Palestinian
authority involved.
in the U.S. plan with the revitalized Palestinian Authority more of a figurehead than any active
role. So there is discord within Netanyahu's government, to say the least. It is important
to remember that the purpose of any kind of military action is not the fighting.
It's supposed to be to accomplish some kind of goal. This is what we talked
about in Ukraine. At this point it appears that Gallant and Gantz are
basically saying, you know, give us a plan. Show us something other than we're gonna
speed run all of the US mistakes and they are not they are not being incredibly
diplomatic with what they're saying. They're basically saying that he is
putting his personal interests over that of the country. Okay so as far as a plan
Blinken has said that he hasn't seen one. Reading the subtext, he doesn't believe
that there is one either. You know, doesn't believe that there is some kind
of goal, some kind of endgame to this. Now on top of all of that news, you have
the national security advisor Sullivan. He's still working on the mega deal, the deal that gets Saudi
Arabia to normalize ties with Israel, which gives Saudi Arabia a defense pact with the US, which
also requires a Palestinian state, which is that three-point plan. The dump trucks full of cash,
the regional security force, and a Palestinian state or pathway to one. A
bunch of questions about the pier. They did get trucks rolling across it, if not
within 24 hours, really close to it. But those appear to be the, I mean don't get
wrong it was aid. They weren't just empty trucks but that appeared to be the test
batch and at time of filming we haven't seen a massive follow-on yet. I would
expect that soon though. I would expect the trucks to actually start rolling
pretty quickly now and again I know people are talking about the aid that
went in. Yes, those trucks had aid, but that was maybe a fifth of what the base
goal of that peer is. And I don't know, 10% of the large goal. So yes, aid went in.
it is not the the large number of trucks per day and it's not moving in
consistently based on the information I have. But that that very well could
change between the time I record this and the time y'all watch it. So that's a
brief overview of the foreign policy situation over there. My gut tells me
that if Netanyahu doesn't do something soon, I don't think Gantz is gonna wait
till the 8th. I'm gonna be honest. There was actually speculation that he was
leaving when he made the announcement about the 8th, that he was already done.
So there's a lot going on when it comes to the foreign policy scene. It is
worth noting that both of those that are openly criticizing Netanyahu, both of
them have pretty decent ties with the West. And there might be a hope
within Western circles that soon they might be dealing with somebody other
than Netanyahu.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}