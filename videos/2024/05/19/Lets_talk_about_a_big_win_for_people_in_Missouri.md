---
title: Let's talk about a big win for people in Missouri....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=7ums5MeTDWA) |
| Published | 2024/05/19 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Missouri.
And we're going to talk about how the people of Missouri
just got a huge, huge win
and how it's the Democratic Party that gave it to them.
Even if you're a Republican,
even if you are a Republican,
it was the Democratic Party that just protected your rights
and made sure that you were able to be represented.
And I don't think they're going to get the credit they should,
because it's going to be framed almost entirely
around something else.
So what was going on?
Republicans in the state, they wanted
to make it harder for the ballot initiatives to succeed
and amend the Constitution, the state Constitution.
Stop there.
Why?
Why would they want to do that?
I mean, keep in mind, it's kind of a red state, right?
They have control.
Why would they want to make it harder
to pass those initiatives?
What are those initiatives used for?
Those initiatives are used when the state government does
something that the people don't approve of.
That's when they come around.
The Republican Party wanted to make sure that it could rule over the people of
Missouri rather than represent them. That was the purpose. That's what this is
about. The goal was to make it to where it's not a majority of people that need
to support it because, I mean, your voice doesn't matter. It needs to be a majority
within a majority of districts. So what's the point? To dilute the vote of demographics
that they don't like. That's the point. And who draws those districts anyway? Oh, that's
right, they do. They were actively trying to take away your voice to make it harder
for you to have a say about them in using a mechanism that is used to reel
them in. Okay, so they wanted to push this forward, but they knew the people
wouldn't go for it. They knew that it wouldn't pass on its own merits because
who wants to lose their voice? People generally don't vote in favor of that, so
they wanted to add ballot candy to it. That's where you add something
unrelated to the language to scare people, to entice them to vote in favor
of it. In this case they talked about non-citizen voting. You know those other
people be afraid of them that's already illegal in Missouri. The Democratic Party
they said no and they would not shut up about that and I mean it literally.
They filibustered it for 50 hours. So at that point the Republican Party said
that they were going to break the filibuster. Turns out they didn't have
the votes for that. So it was sent back to be renegotiated. But now it's over.
The session's over. So the Democratic Party getting up there and just running
their mouths. This was a talking filibuster to be clear. Running their
mouths for 50 hours protected the voices of everybody in Missouri and if I'm not
mistaken it's the longest filibuster in state history. This is going to be framed
almost exclusively around why the Republican Party specifically wanted to
do it now. And that was because they wanted to deny women their rights.
That's what it's going to be framed around. It's all going to be about
reproductive rights. That's the coverage. But make sure you understand, this
applied to everything. If you live in Missouri, they were trying to make sure
you couldn't curtail them, that you didn't really get represented, you just
did what you were told because you don't matter. You need to obey your opinion. A
majority of citizens, that's not important. You need to listen to your
betters. It was the Democratic Party that stopped, you know, small government
conservatives from doing that.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}