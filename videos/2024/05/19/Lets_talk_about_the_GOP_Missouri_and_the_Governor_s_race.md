---
title: Let's talk about the GOP, Missouri, and the Governor's race....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=7MsKpz0Hf6c) |
| Published | 2024/05/19 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about some news out of Missouri.
We're going to talk about Missouri, a decision,
the Republican primary ballot, and just kind of go through
some news that y'all may not actually believe.
The last time we talked about Missouri, it was good news.
I mean, it was.
In this case, it's about the Republican Party losing in court, and I feel like almost everybody
watching this channel is going to wish they had won.
Okay, so a Missouri judge has decided that a candidate for governor will remain on the
Republican primary ballot.
in Missouri were trying to get him booted. That move came after it became
known that the candidate was a quote honorary member at one point in time of
of the Klan. He in a statement described himself as quote not racist. There's also
reporting saying that he was at a cross burning and this person will be on the
Republican primary ballot. In a quote that is one of the more interesting,
McClanahan's lawyer, that's the candidate, said, I'm not sure they ever
actually intended to win this case. I think the case got filed because the
Republican Party wanted to make a very big public show that they don't want to
be associated with racism or anti-Semitism.
And the best way that they could do that was filing a case that they knew was almost certain
to lose. Yeah, so that's that's what's going on in Missouri in the Republican
Party in 2024. I feel like this is one of those situations where them having to go
to court over this, I mean even if they even if they did intend to win, and this
attorney is incorrect, even if they did really want to win, it may be one of
those things where I'm not saying that the Republican Party up there is racist.
but the one-time honorary member of the Klan apparently feels like that's the
ballot he should be on.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}