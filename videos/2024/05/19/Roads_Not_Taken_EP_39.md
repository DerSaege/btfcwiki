---
title: Roads Not Taken EP 39
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=nuf8pN8Rd7Q) |
| Published | 2024/05/19 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Bo again,
and welcome to the Roads with Bo.
Today is May 19th, 2024,
and this is episode 39 of The Road's Not Taken,
a weekly series where we go through
the previous week's events and talk about news
that was unreported, underreported,
didn't get the coverage I thought it deserved,
or I just found it interesting.
Then at the end, we go through some questions from y'all.
OK, starting off, we are going to start with a PSA, a public service announcement for those
overseas, and this deals with the Houston area.
The anticipated heat combined with the lack of electricity, probably a good idea to check
on each other and really keep the heat in mind. I will try to find a video and
put it down below on ways to stay cool without electricity. Short version, never
underestimate the cooling power of water whether it be wet rags or whatever. Okay
moving on. Foreign policy. The US State Department issued a worldwide warning
for Pride Month, suggesting an increased risk of attacks.
It is unusual to get a worldwide warning from state.
So just keep that in mind.
Russia appears to be running a sabotage campaign
across Europe, Netanyahu is facing a very, very
Very, very public revolt in his war cabinet.
Very public.
Basically they're asking for a plan.
The saying is that war is a continuation of politics by other means.
That means there needs to be some kind of political goal.
There needs to be some kind of end game.
And a growing number of people within the government there don't believe that he has
one. Okay and then you're you're finally getting widespread reporting that is
kind of acknowledging something we've been talking about on the channel for a
while and is honestly not incredibly insightful. It's common sense but Putin
is now being viewed as the junior partner in the Chinese-Russian relationship
which, I mean, yeah. Okay, moving on to US news. Trump once again bizarrely claimed
that he won Minnesota. Again, it's worth noting that I don't think a Republican
has won the state in like half a century, but he keeps saying that. Republicans in
In Wisconsin, in the Senate there, they overrode a number of vetoes from the governor.
It does not look like they have the votes in the House.
So it seems to be a symbolic move.
Colorado and South Dakota have reproductive rights amendments that have qualified for
the ballot.
The governor of Texas, Abbott, pardoned the man who killed 28-year-old Air Force veteran
Garrett Foster.
And I know, that's not how you're seeing it in the media.
And this is how the Democratic Party loses control of a narrative.
Okay, the Associated Press did an investigation
of police encounters.
It was pretty extensive.
And what it showcased was incidents
where law enforcement used what should be non-lethal force,
but best practices were not applied.
And those situations turned lethal.
There are hundreds of incidents.
Let's see.
Paul Pelosi's attacker was sentenced to 30 years,
but it looks like the judge has reopened the sentencing.
Another group in Texas wants to break away
from the United States, blah, blah, blah.
That may come up this week.
not because it's going to be successful, just to illustrate something else.
Let's see.
UNEDOS US Action Fund looks like they're endorsing Biden.
Moving on to cultural news.
TikTok and DOJ are both seeking expedited consideration of the law that would force
the cell of TikTok. Lindsey Graham appeared to condone the nuking of Gaza and said that military
officials, you shouldn't, you know, basically saying you shouldn't listen to them. They don't
know what they're talking about. Okay, Colonel Lindsey Graham, I'll bear that in mind. We'll
probably go over this at some point. This is not a thing. Okay, moving on to science
news. There were fewer overdoses, fewer overdose deaths reported in the U.S. last year. There
are eight new red wolf pups that were born in North Carolina. They were fathered by a
wolf from Washington State. If you haven't paid attention to that coverage
here on the channel, short version, that's a really good news as far as
genetic diversity in the species. Ground was broken on the high-speed rail that
that was pushed into motion by Biden's infrastructure plan.
In oddities, the Vatican set out some new guidelines
for dealing with supernatural events.
The short version here is you go to the Vatican now,
not to the bishop.
The idea is that the internet allows
a more immediate clarification of whether or not it actually is a supernatural event.
Senator Menendez, this is the gold bar senator, appears to be attempting to throw somebody else
under the bus in his trial, his wife.
Okay, moving on to the questions and answers. What is Ernamoo? So it's another code like
Hammurabi. It's U-R-N-A-M-M-U, if you want to hit the Wikipedia on it. My question is,
do you have quick tips for resources to spot misinformation online? But there's a wrinkle.
I and my children are autistic." I don't know. I will put a link below. I don't know if it's
going to help. That's not a situation I've dealt with, nor am I... I don't believe I'm
qualified to try to tailor something in that way.
What is an unexpected green flag when meeting a guy?
OK.
One, I would say the guy having multiple interests.
Another would be having women authors on the bookshelves.
And then one that I have heard a lot lately,
guys take note of this, a woman was incredibly happy.
She and everybody I've heard talk about this is in their 20s,
but she was incredibly happy when
she found out that her new guy did not
own a PlayStation or Xbox. You know, I have a lot of... when it comes to the green
flag, red flag thing... I mean, a lot of... a lot of things that are being thrown out
this way, they're very person-dependent. You know, it may be something that...
Obviously some red flags are red flags, but I'm seeing more and more stuff that
is really a matter of taste, and I don't put a whole lot of stock in them. I will
tell you a story. I went on a date, obviously, this was a very long time ago,
But, when the food came, I salted my food and she asked me why, and I was like, what?
Because I want salt on it.
And I didn't really understand the question.
And then she's like, well, how do you know that it's not the way you like it?
You know, you're changing it before you've given it a shot.
And I was like, well, I've eaten here like 75 times.
I know what the food tastes like.
Turns out that is something that she watched for.
And the idea is that if the person adjusts their food
before tasting it, they are somebody
who makes decisions without all of the information.
So I would have, that would have been a red flag.
We have been married a very long time.
So I don't, I don't, I don't put a lot of stock in some of them.
Okay.
I'm beginning an aviation maintenance class in August, which will take about two years
to complete.
When I'm finished, I'll be very familiar with metalworking, turbines, piston engines,
and anything else to do with fixing airplanes.
I have a strong desire to help Ukraine and want to help when I'm finished with the
program. I originally wanted to fight but because I have asthma, it would be a
readiness issue. Once getting the certification would my skills be useful
in Ukraine and could I have a strong future ahead of me in the airlines? Um, I
will tell you this I know one mechanic who who works for a major airline they
They make bank like way more than I thought they would.
As far as going to Ukraine, you're talking about something two years from now.
Whether or not it would be needed, whether or not you would still want to go, I would
cross that bridge when you get there.
There's a whole lot that can change between now and then.
Okay.
I just got hired for a new job.
going to be okay it's more money I'm gonna paraphrase this one not sure how
to figure out a better budget what to do with the extra income I know some of it
I want to go into savings some towards student loans my spouse thankfully has
been making enough money to cover the bulk of the bills they have a couple of
of like wish items like a lawnmower,
but don't want to get carried away with spending money
because more is coming in.
I grew up in a tight financial environment
and still tend to default to the we can't afford that whenever
I see something I want but don't need.
I know you and Ms. Bowe are sensible.
Yeah.
If this is your first like big raise,
Act like you didn't get it.
Put the money away.
I mean, obviously, if you want to go to a law
mower, go to a law mower.
But what I was always told was basically always be one
raise behind as far as your lifestyle.
So don't commit that money.
Save it if you want to use it for something, OK.
But generally, save it up and don't adjust your lifestyle
until you get your next one.
And when you get your next one, you
adjust to the previous one.
I mean, I'm sure there are people
that are like financial advisors that would probably
have much better information.
But yeah, I mean, you can't go wrong with saving it,
which seems to be what you want to do with the bulk of it, or paying off debt.
Why do people care more about civilians in Gaza than Ukraine?
I know you normally say coverage, but there's coverage of Ukraine.
There's not protest for Ukrainian civilians.
Because the perception is that right away the US acted to help the Ukrainian civilians,
there's no reason for protest. And then the perception is the opposite when it
comes to those protesting about Gaza. So it is the idea that when Russia
invaded the US, you know, they got involved and were trying to mitigate and
all of that stuff. That's, that's why that's not there. There's lots of questions about,
um, so there was an airman that had an encounter with law enforcement.
That is not something I will be covering. I know people in his unit. I'm too, I am too close to
of that to cover that.
I am certain that that story is not going to go away anytime soon though.
That is not something that is going to just go away.
They're not going to let that go.
If you want to look it up, I'm sure there is tons of reporting out there about it.
happened in Okaloosa County.
What if tomorrow we were attacked by Independence Day type aliens with political climate and
stuff like that?
I mean honestly, I used to be a believer in the idea that if we found out there was intelligent
life somewhere out there in the world, that all of our petty differences here on earth
would just disappear.
We'd forget about all of those ridiculous lines on the map, but we would become more
cooperative.
It would just lead to a much, much better world.
I'm now concerned that a large portion of the American population would claim that it
It was a plot by the government and there would be memes on Facebook saying to not believe
it because it's really not aliens, it's like the UN or something.
When it comes to my faith in people's ability to adapt to new events and not be swayed
by ridiculous propaganda and act in furtherance of the species, yeah, let me tell you something.
COVID did a number on how much faith I have.
So yeah, I don't know that somebody would give a rousing speech and everything would
be okay.
I feel like the polarization in the United States would probably lead to us letting the
world down.
So and that's the last one because I know they thought I was going to have fun with
that.
I'll tell you what, I have a video where I talk about meeting aliens and being first
contact because there was a joke for some reason and I can't remember why about me
being like nominated to meet the aliens.
I'll put that down below for a good laugh to end on.
But that looks like all of the questions.
So there we go.
A little more information, a little more context,
and having the right information will make all the difference.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}