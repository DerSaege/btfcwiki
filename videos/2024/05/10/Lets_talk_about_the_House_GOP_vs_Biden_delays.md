---
title: Let's talk about the House GOP vs Biden delays....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=aam1IcXf-9s) |
| Published | 2024/05/10 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Biden.
We're going to talk about president Biden and the U S house of representatives,
particularly Republicans in the house, um, because it does appear that the GOP
in the house, they're kind of gearing up to attempt to change Biden's mind on
something.
And by that, I really mean they appear to be gearing up to go toe
toe with him in an attempt to force him to do something. If you've missed the
news, President Biden, he introduced some delays, a pause, when it comes to military
aid shipments to Israel. Republicans in the House super unhappy about this, and
so much so that something called the Immediate Support for Israel Act is
starting to gain some traction and it might actually go somewhere. The purpose
of this legislation would be to force Biden to basically get the aid there
within 30 days. Now, just as an aside, I don't know that this legislation is
actually written in a way that would cover the aid currently on pause. Just
saying, but let's just say that it is. What is it? It's a bill. It's only a bill
and it's sitting up on Capitol Hill. If it makes it through the House, where does
it go next? It goes to the Senate. It goes to the Senate and I don't have a vote
count but I'm fairly certain it wouldn't pass because there are a number of
incredibly influential senators on the Democratic side of the aisle who probably
wouldn't let it. So it's unlikely that it makes it through the Senate based at
you know first glance. Again no vote count yet. But let's just say that it
does. Where does it go next? To Biden. It goes to Biden. The legislation designed
to force Biden to do something he apparently doesn't want to do, would go
to him to be signed. I'm fairly certain that if he doesn't want to do it, he
probably won't sign the legislation forcing him to do it. Just saying. If
that occurs, it would go back to Congress. Sure, they could make the attempt to
gather the votes to override a veto. They don't have the votes for that. That much I know.
So it's unlikely that this is actually going to go anywhere. But again, you're looking at a situation
where you have a member of Congress introducing a piece of legislation that would have to go to the
person they are trying to influence to sign for it to be effective. That's a bold strategy.
Maybe he signs it by accident.
I don't know.
So that is where things are now.
It's also worth noting that the United States, and this includes Biden, does not
want to sever its relationship with Israel.
So any legislation designed to force him to do something would probably be moot
by the time it got anywhere, because this would, in theory, this should be resolved pretty quickly.
I would hope, anyway, because the United States does not have the desire to break ties with Israel.
That's not something the U.S. wants to do. It's not something Biden wants to do.
So there have been a number of questions about this particular piece of legislation and about
whether or not they can do it. Yeah, I mean, Congress can absolutely do this, but it would
have to be signed. Congress can totally mandate that something occurs within a set period of time.
Yes, all of the components, it's totally something Congress can do. However, put together the way it
is, where it's trying to compel the person who is in theory responsible for the delay
to end the delay, that just, it just doesn't really make sense.
That's not how it would work.
Because again, it would need him to sign it into law to be effective.
So in some ways, this is just something for the Republicans in the House to signal about
and say, hey, we're really unhappy about this.
But it's not actually going to change the outcome of anything.
So don't count on that occurring.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}