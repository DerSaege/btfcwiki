---
title: Let's talk about Biden's statements on over there....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=BX_yaLwyCB4) |
| Published | 2024/05/10 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Biden, his statements,
some small changes in language that actually make a pretty big difference.
We will talk about the response to these statements.
And then we will go over one message from somebody who
disagrees but makes a really good point in the process.
Now most of this, if you have been following the coverage on this channel, short version,
a lot of the stuff that we have been talking about as happening behind the scenes is now
super public.
It's out in the open.
It's not behind the scenes anymore.
Okay.
So Biden has basically said if they go into RAFA that they're not going to get US support.
already talked about this, but now it is very public. There are, let's call them,
delays when it comes to supplies. It looks like 2,500 pounders, maybe some 155s.
Short version, if you don't know what any of this stuff is, U.S. boom. U.S. supply
boom. And the general tone is that if they go into RAFA, they don't get U.S.
boom. Now, when it comes to going into Rafa and what that means, initially that
language was large-scale ground offensive and then there was the plan
that was presented by Netanyahu's team that divided it up. So it wasn't actually
technically a large-scale ground offensive. I'll put the video down
below if you really want to hear about it in detail, but the short version is I
I think I called it a distinction without a difference.
Sure, it is no longer a large-scale ground offensive,
but it doesn't actually accomplish the goal
of mitigating civilian harm.
So, a slight change in language for those who watched
the video over on the other channel this morning has occurred.
They're no longer saying they can't have
a large-scale ground offensive.
They're saying they need to stay out of population centers.
That is a distinction with a difference.
That is more in line with what was trying to be accomplished.
Now, it does change some things, though.
Because if Netanyahu was to order an operation that
went into the outskirts of RAFA.
It might not actually
break the rules
because it's not part of the population center. We're talking about like let's say
they go into a
an outskirts area where there's not a lot of people.
That would be going into RAFA
but it wouldn't actually be violating
what the U.S. has said.
So, be aware that that might happen.
But restricting it around population centers is way better than just saying you can't do a ground offensive.
There's not a lot of easy ways or semantic ways to get around staying out of population centers.
Another thing that is worth noting is that it appears the US is kind of pushing small
diameters.
If you don't know anything about this stuff, just think of how they're described.
2,000 pounders and small diameters.
One is really, really big, the other isn't.
The US seems to be pushing these, saying, why don't you go this route with it?
That is most likely something that is also signaling the U.S. is pushing for a doctrine
shift.
Basically telling Netanyahu to engage in realignment or shaping, going after the leadership rather
than trying to fight an unconventional opposition force conventionally.
This is the same advice that the U.S. general gave when he went over to advise six months
ago. The use of small diameters would mitigate civilian harm immensely. Even if there was
a higher frequency of their use, it would be less harmful to civilians. I know that
some of Netanyahu's advisors advised that six months ago. I don't know how
they're going to view this suggestion today and I have no idea whether or not
Netanyahu will listen to it. But that's something else that's occurring.
Now, what has Netanyahu said? What's the response? Short version, he and one of his
advisors have basically said we have what we need to go into RAFA. When we're
going to do what we're going to do we'll stand alone, we'll do whatever we want
even if we have to do it by ourselves. I would imagine that right now he has a
roomful of advisors probably saying sure we have the munitions to do this but we
don't have the munitions to do that and deal with any potential counter-attack
and they're probably trying to talk him out of it. However, we don't have a
long history of Netanyahu listening to his subject matter experts over the
last seven months or so, so no clue what's actually going to happen with that.
So, again, the short version here is the stuff that we have been talking about behind the
scenes is now out in the open, very public, to include Biden basically saying, we're going
to give you the Iron Dome and defensive stuff, but nothing else, specifying, once again,
the Iron Dome.
So that is, that's where it sits right now.
Everybody is puffing out their chest, waiting for somebody to back down.
I don't know how that's going to play out.
Okay moving on to the message and again this person, you'll see pretty quickly, doesn't
actually agree that Biden is is trying here. But there's an
interesting development in the message. Okay, in relevant part,
I left the military a while ago. Have 2500 pounders changed? You
liberals will fall for anything. It just shows you how much Biden
is lying to you, and why you need to come to Team Trump. I
I don't see how this is even remotely sending a message at all.
They don't even need these.
You don't even use those in urban areas, especially the 2,000 pounders.
You sir are correct.
You are correct.
Generally speaking, there are exceptions, but generally speaking, you do not use 2,000
in densely populated areas where there are civilians present. You are
correct. That is not something that I'm imagining, what you were trained, it would
be okay. And you are so close to getting it. Anyway, it's just a thought. Y'all
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}