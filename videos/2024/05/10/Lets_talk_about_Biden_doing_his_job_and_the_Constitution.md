---
title: Let's talk about Biden doing his job and the Constitution....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=eM9JcP_vMfM) |
| Published | 2024/05/10 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about president Biden and him doing his job,
what his job is, the U S constitution, and just kind of take a look at something.
Because in a recent video, I suggested that Biden was just doing his job when he was
trying to increase the wellbeing of the people.
I have since been told that I was wrong and that I needed to clarify that.
So we're going to do that now.
This is the message.
Know you rabid communist, and then there's a whole bunch of stuff that I can't read.
Biden's job is not to increase the wellbeing of the people.
Biden's job is to support and defend the Constitution.
The Constitution is entirely about property rights.
You are not entitled to anything that requires somebody else to do the work.
I've watched your videos on the Constitution.
You know what you said is wrong.
You need to do a video clarifying this."
Apparently I do, and I also need to do one on how the Constitution is entirely not, it's
It's not entirely about property rights, that's not...
That's a topic for a different day.
Okay, Biden's job is to support and defend the Constitution.
No it is not.
No it is not.
If you're going by the oath, his is to preserve, protect, and defend.
That's his job.
This one is from the military, support and defend.
That language comes from the oath for the military.
Hey, you know what?
Let's start there.
Article 1, Section A, Clause 13, if you want to talk about the Navy.
Article 1, Section 8, Clause 12, if you want to talk about the other branches.
Those are the ones that authorize them.
Do you believe
that uh...
somebody works
in those branches? That they do something?
They're providing a service.
Something authorized by the Constitution
requires somebody to provide a service. In this case it would be defense. And you
as an individual, you don't have to go do the work. Somebody else does the work to
provide that service, to do that thing. If this one isn't doing it for you, you
could also use the post office. Okay, so if you don't have to do the work for the
army or the navy, but they're going to provide you a service through the government, what happens?
Like how does that occur? Well, it occurs because of Article 1, Section 8, Clause 1, which says the
Congress shall have power to lay and collect taxes, duties, imposts, and excises to pay the debts and
provide for the common defense. You get something that you benefit from, in theory, you're benefiting
from the common defense, it is paid for via taxes through the government.
So this idea that, what was it, you're not entitled to anything that requires somebody
else to do the work.
From a constitutional perspective, that is not true.
That is definitely not true.
That's not the case.
We have it right here.
But I know somebody is going to say that, well, defense is different.
Defense is different.
I'm going to finish the rest of Article 1, Section 8, Clause 1, because I stopped a little
early to pay the debts and provide for the common defense and general welfare.
So taxes can be collected and used for the general welfare, to promote the general welfare.
It's actually common defense and general welfare are in the Constitution a couple of times,
pretty close together.
Now I know you're saying, well, how would they determine what promotes the general welfare?
Whatever they think is necessary and proper, I guess.
So, no, when you think back to the phrasing in that video, increase the well-being of
the people, that might be another way to say promote the general welfare.
Yeah, no, it's his job.
It's his job.
The vast majority of the Constitution is not the portion of the Bill of Rights dealing
with property rights.
I'm just going to throw that in there as well.
The U.S. government, it has changed a lot since the Constitution first came around,
But one of the beautiful parts about it is that the Constitution contained the machinery
to change the Constitution and how to interpret it.
It's all there.
It is, in the modern context, it very much is the President's job to look out for those
people who are struggling.
That's part of it.
would be promoting the general welfare or providing for the general welfare of the United
States, helping it in that way.
And you do not have to be a rabid communist to believe that.
Anyway, it's just a thought.
You all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}