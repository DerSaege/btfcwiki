---
title: Let's talk about Trump, oil, and 1 billion dollars....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=EcW4xUnNzrc) |
| Published | 2024/05/10 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Trump, oil,
a dinner, executive orders, and $1 billion.
According to reporting, the former president
was at his little club at a dinner
with 20 or so executives.
During this dinner, he talked about things
that they might be interested in,
such as rolling back all of Biden's
environmental regulations and stopping anything new
that might help the climate or help the environment,
preventing new environmental regulations.
So he was talking about that and asking
for $1 billion in campaign donations.
$1 billion.
That has led some commentators to wonder
if a wannabe Dr. Evil also asked for sharks
with frickin' laser beams attached to their heads.
According to the reporting, he said, quote,
you'll get it on the first day.
Now, Trump World has, of course, denied this.
I am sure that the supporters of Trump would like to avoid any suggestion that he might
engage in like a quid pro quo or anything transactional of that nature.
campaign said Trump was quote, selling out working families to big oil for
campaign checks. Trump isn't fighting for what's best for American families,
cheaper energy, or our climate. He only cares about winning this election and
will sell out working families to special interests to do it. In separate
but possibly related reporting. The oil industry is reportedly writing executive
orders, just getting them ready for Trump to sign in the event that he is elected
to office again. They're putting together executive orders to provide to the
President of the United States to sign, just writing them for him. The reason
they're doing this is because they don't believe that his White House would be
able to attract people capable of writing the orders themselves. That's not
a joke. That's in the reporting. That is in the reporting. The reporting seems to
suggest the reason the oil industry is writing the executive orders is because
they don't believe that Trump will be able to get, get people skillful or smart
enough to write the orders themselves. So they would need help with that.
That is, I mean I feel like that's worth, worth really kind of thinking about. The
The executive orders would really just be rolling back Biden's orders.
These would be pretty simple documents, I mean, just saying.
As far as things that a White House is required to do, being able to write an executive order
effectively, that's one of those basic things.
So that being mentioned in the reporting as a possible motivation is, I mean, telling,
I guess.
So here is another massive divergence between Biden and Trump.
Biden has engaged in a lot of efforts for the environment.
I know there's going to be people down below saying that it hasn't been enough and yeah,
I mean, I get it.
It does appear that Trump's promise would be to get rid of what progress has been made.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}