---
title: Let's talk about South Dakota, a book, and a trend....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=7BKDChuHkkk) |
| Published | 2024/05/05 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about
the governor of South Dakota, a book,
and some claimed inconsistencies.
And we'll just kind of go through what is being said
and see if we can maybe spot a trend
that might shed some light on some things.
If you have missed the news,
the governor of South Dakota, Christy Noem,
has a book coming out soon.
And it has already created a situation
with some slight controversy regarding
a story involving a puppy.
Beyond that, there are now questions
being raised about the depiction of certain events.
One of them, I remember when I met with the North Korean
dictator, Kim Jong-un.
sure he underestimated me, having no clue about my experience staring down little tyrants. I've
been a children's pastor after all." There's this one, I think there's one involving Nikki Haley,
and then another involving France. These stories are being called into question as to whether or
not they occurred in the way that she's suggesting or in some cases at all. Now
this is something that can actually occur if a ghostwriter is used and notes
are made or they're using references. Things like this happen and it is worth
noting that at this point the book still isn't really released. These are advanced
copies. We'll see what her team has to say about some of the the questions that
are being raised. But there is an interesting trend, right? All of these
events, they all deal with foreign policy. They all deal with international
affairs. If someone was writing a book and the goal of that book was to convince
another person who you know read it between naps to maybe make them his vice
president, having stories related to international affairs would be an
important part of it. Again, it's early and if she had help writing the book maybe
that's what occurred. We'll have to wait and see. But the trend that exists and
and the knowledge that she certainly was gunning for Trump's approval,
there might be more to it.
Again, we have to wait.
But at this point, it seems like the book might have been better off
if there were more dogs and less people in it.
And given the reaction to the book,
book, the stories that are contained in it, I would suggest her chances of being Trump's
VP are probably somewhere in a gravel pit.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}