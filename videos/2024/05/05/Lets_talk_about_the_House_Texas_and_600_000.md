---
title: Let's talk about the House, Texas, and $600,000....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=m-Lt4RpKp44) |
| Published | 2024/05/05 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about something
that is becoming a surprisingly common occurrence,
a member of the U.S. House of Representatives,
Texas, and what looks to be around $600,000.
Okay, so what's going on?
Representative Henry Cuellar of Texas,
a member of the Democratic Party, has been brought up by the feds on allegations related
to influence peddling and bribery.
The allegations suggest that he accepted payments in exchange for influencing U.S. policy.
It's still early at time of filming, but it looks like the allegations center on it
coming from a foreign oil company and a Mexican bank. Now, the National Republican Congressional
Committee immediately called for his resignation, which given recent events is humorous in and
of itself. Jeffries seems to be taking the position of there is, you know, a presumption
of innocence, at least at this point. There is at least one member of the
Democratic Party that has already called for his resignation just based on what
has come out so far. Normally, it starts slow and then builds as more details
emerge. Now, for his part, the representative appears to have
voluntarily stepped down from his committee assignment, which is a big one on appropriations.
And he is saying that he and his wife are innocent and the actions I took in Congress
were consistent with the actions of many of my colleagues and in the interest of the American
people. That's where it's at right now. There will probably be more developments on Monday.
You might have some more movement as far as calls for resignation and stuff like that over the
weekend. But where it's at right now, this is a story that is going to be developing and is
definitely going to alter some math in the U.S. House of Representatives because of how close
the majority is there.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}