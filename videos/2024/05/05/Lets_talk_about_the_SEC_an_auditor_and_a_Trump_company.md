---
title: Let's talk about the SEC, an auditor, and a Trump company....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=4MRYKd3jSE8) |
| Published | 2024/05/05 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about the SEC and an auditing
firm.
And we're going to answer a whole bunch of your questions,
which was really just one question coming in over
and over again, because it's something
that people are interested in.
And I think people expected a very specific outcome here.
But we'll go through it and then answer the question.
OK, so what happened?
On Friday, the SEC charged an auditing company
with what was described as, quote, massive fraud.
And it impacted, I want to say, 1,500 different audits.
And the allegations range from just never doing the audit
to making up documentation, just all kinds of things,
across the board. Now, an agreement has already been reached. The company will
pay a 12 million dollar fine, the owner will pay a 2 million dollar fine, and
there will be a like a permanent suspension for anything related to the
SEC as far as being an accountant for anything related to the SEC. Okay, so why
Why did this interest people?
Because on March 28th, Trump Media and Technology Group hired this company.
And the question that came in, is Trump linked to it?
It's just like the last time this happened.
Based on all available evidence, no.
is no link whatsoever other than this company was hired by Trump Media and
Technology Group. That's the link. As near as I can tell, the audits in
question aren't related to the Trump company and there is some reporting
that suggests that maybe this company did some work for them before, before it
public, but as far as I can find out, there's no link other than the Trump
company just hired this company back in March, at the end of March. The question
has surfaced so much that Trump media even put out a statement and it
basically just says that they are looking quote forward to working with
new auditing partners in accordance with today's SEC order. So no, as near as I can
tell, this is not another Trump entanglement that is surfacing. Doesn't
look like he has anything to do with this one. Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}