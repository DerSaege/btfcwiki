---
title: Roads Not Taken EP 37
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=A88D3NifK7k) |
| Published | 2024/05/05 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again,
and welcome to The Roads with Beau.
Today is May 5th, 2024,
and this is episode 37 of The Road's Not Taken,
which is a weekly series
where we go through the previous week's events
and talk about news that was unreported, underreported.
Didn't we get the coverage I thought it deserved,
or I just found it interesting for some reason.
After that, we go through some messages from y'all,
picked by the team, and you get kind of an off-the-cuff
response.
OK, so we are starting off in foreign policy.
And it appears that Ukraine will begin operating F-16s
sometime this month.
Now, they are not being entirely forthcoming about
exactly when, which makes a whole lot of sense from a
security standpoint. Russia has reportedly put Zelensky on its wanted
list. I mean, that's a development. According to some polling, over 40% of
Americans now see China as an enemy. We have talked about how opinion is swayed
when it comes to foreign policy. It's worth noting that while that percentage
is going up, nothing has really changed. Just the coverage, the amount of coverage
and how the coverage is done. That's really all that has shifted. People are
just becoming more aware of a near peer contest that that is underway and so
it's altering their opinion.
Columbia is reportedly cutting ties with Israel.
When this news broke, social media went overboard with people talking about how self-important
Columbia was because who cares if a university cuts ties with Israel?
That's Columbia with two O's, the country.
The country is reportedly cutting ties.
Now it's also worth noting that that's going to be harder than they think.
and Israel have a long history when it comes to military cooperation and equipment.
A lot of the equipment that Colombia currently has, it needs Israel to maintain it, and Colombia
does not have the budget to just let that equipment go and replace it.
So keeping this promise, matching this rhetoric, may be harder than they imagine.
In kind of related news, Turkey has indicated that they're going to halt trade with Israel.
That they may actually be able to do.
They'll have an easier time keeping that promise if they chose to.
86 Democrats in the House have asked Biden to restrict military aid at this point.
Okay, moving on to U.S. news, Medgar Evans was given a Medal of Freedom.
Normally, I don't really care much about the Medal of Freedom awards or cover them,
but in this case, I'll make an exception.
This is a person, if you're not familiar, look him up.
He's a person I use to talk about how we teach history in the United States and how it's
often taught by personifying an era with a single person.
When you do that, you lose a lot of history and a lot of important flavor to the story.
is a person that often gets left out. Okay. Republicans this week suggested,
because of everything that was going on on campus, suggested that those who broke
windows and entered a building for a political cause were, in fact, traitors
who should be deported."
I'll say that again.
Republicans suggested those who broke windows and entered a building for a political cause
should be deported.
They were traitors and should be deported because they broke windows and entered a building
they weren't supposed to be in.
Take as much time with that as you need to.
Irony in this country is just dead.
Texas is suffering from severe flooding.
Trump's legal team reportedly has an emergency plan to attempt to stop him from going to
jail in case an order like that is given because of his behavior.
The fact that his legal team feels it necessary to have a plan like that in case the former
president violates pretty simple rules is commentary in and of itself.
Trump promised to give police even more immunity from prosecution if he was elected again.
Moving on to cultural news, United Methodists
ended, I want to say, like a 40-year ban on LGBTQ clergy
and on the penalties for recognizing gay marriage.
Anne Hathaway is reportedly five years sober.
Now, my understanding is that it wasn't a huge issue that
led to her sobriety, but when you're talking about a public statement like that from somebody
like her, it always helps destigmatize recovery.
So Fox News reportedly took down a mini-series about Hunter Biden shortly after they were
contacted by Biden's attorneys.
The North Carolina Republican Party apparently put out a meme for May the 4th Be With You
featuring Trump.
If you're not familiar with the whole May the 4th thing, it's a Star Wars reference.
And in the meme, they gave Trump a red lightsaber.
I guess he's Darth Dozer or something, if you don't, I'm sure it'll be in the comments.
Moving on to science news, China released a video about its moon-based plans and it
sparked a whole bunch of conversation because it included an image of what appeared to be
a US space shuttle.
And the thing is, it's really not that bizarre.
scientists, particularly those involved in space from around the world.
They've tried to keep space something that is outside the confines of the political jockeying that occurs here on Earth
over imaginary lines on a map.  So the idea of international cooperation or something like that, it's not really that
bizarre.
Maybe it was just a mistake that it was included.
But oftentimes, scientists from both the US and China have recently asked that it not
be called a space race because they don't want that competition, that competitive, the
competitive nature that exists between governments to infect space. They want to
keep it civilian and safe. A wounded orangutan was reportedly observed using
medicinal plants to treat a wound. Using medicine. Amy want raindrop drink. Okay
Moving on to oddities, Elon Musk, noted free speech absolutist, amplified calls to forcibly
deport people for taking down an American flag.
The Houthis have reportedly offered education at the major university there in Yemen to
students who were suspended for participating in the demonstrations.
An F-16 took flight and was controlled by artificial intelligence, not a human pilot.
But riding in the plane was the Secretary of the Air Force.
Moving on to the Q&A.
Are you a German Shepherd only guy, or is that just the breed you bond with the best?
I never intended on being a German Shepherd guy.
Baroness was an accident.
She was trained to be a working dog, and she couldn't go.
She had an issue with her ear that was never supposed to fix itself.
So I wound up with her.
It's worth knowing that within days of her coming to the house, her ear miraculously
fixed itself. That dog dodged the draft. That's what happened. She heard what was going on
and was like, I'm not going to Afghanistan. After her, yeah, probably. I'm very fond of
the breed now. We did have a husky as well, and they complement each other. They complement
each other very well as breeds.
Did Africa discover what's the interest there?
Wow, mad.
Sometimes American foreign policy is really just that transparent.
This is obviously about deprioritizing the Middle East and going to Africa.
Minerals.
There's a lot of resource wealth in Africa.
the United States appears to be gearing up to try to protect its interests there, would
be the terminology.
Same type of stuff.
I've heard you use the term normalize in a few of your recent foreign policy videos,
And I'm wondering if you could describe what it means in a foreign policy context.
So it's not an event.
It's a journey.
It is taking a situation that is icy and normalizing it.
Wow.
I've never, okay, an example.
The United States and Iran don't have real diplomatic ties.
If the US was to put an embassy there
and Iran was to, you know, basically accept the diplomats,
that would be a piece of normalization.
This goes on to go beyond just exchanging diplomats
include trade deals and creating the conditions for peace to be very, very long-lasting, reducing
any chance of war or conflict.
In the current context, it's basically taking a lot of things that are done unofficially
between Saudi Arabia and Israel and making them official, getting Saudi Arabia to openly
state that Israel is in the region, it belongs in the region, it's a partner country in
all of this stuff.
Israel really wants that because Saudi Arabia has a lot of sway in the region.
So they're hoping that it would alter the perception of the country in the region and
kind of turn down the temperature.
the plan released by the PM's office work even though it doesn't have a Palestinian
state.
It looks like a pretty sound way to raise the standard of living and get to self-governance.
This is something that I will probably end up doing a full video on.
It could.
It could work.
I have major doubts.
I think it relies too heavily on the idea that economic stability would trump a drive
for a nation of their own, and I'm not sure that self-governance would cut it.
When it comes to whether or not this works, it's up to the Palestinians.
It does not meet what is perceived to be the minimum requirements that they want, but that
might change.
I personally do not see a way to see long-term peace without a Palestinian state.
I don't see a way for that to occur.
But again, that's probably going to be a full video.
For those that don't know, some documents surfaced that appear to be from the Prime
Minister's office there.
And it basically lays out like an 11-year plan, more or less.
Okay.
These are channel-related questions.
Are you ever going to do the This Day in History thing?
I'd like to, you know?
That's a big undertaking, given our production schedule,
but I would like to.
Did you ever find somebody to fill in for you
so you can get a day off?
No, I haven't.
I haven't.
But, well, we'll see.
Have you ever thought of doing a letter to the editor series,
where you read other people's messages
and lend them the platform like you just did?
That would be a nice hat tip to the original fifth column idea.
Yeah, for those who don't know, the whole term fifth column
was about opening the gates to journalism,
making it more accessible.
And I mean, that's kind of a cool idea.
I mean, I guess I do that sometimes.
But that's, I don't know.
I'll think about that.
We should get a Rhodes Not Taken,
but it's nothing but good news that doesn't get reported.
Just something to help people keep their sanity.
Y'all are adding a whole lot to my workload here.
I miss all the stories with a moral or with a twist that you used to tell.
I know that news is moving very fast right now, but those were some of the best videos
on the channel.
I'll never forget the one about reporters paying for the tunnel under the border.
That's one of my favorite videos.
criminally under-viewed, in my opinion.
Yeah, I mean, eventually I would like to think that the news would slow down, that, you know,
the world just wouldn't constantly be on fire, but we'll see.
We'll see how all that plays out.
Yeah, nothing has convinced me more that I need another person willing to be on screen.
than these questions.
OK.
So that looks like it.
I will give these some thought and see how I can work them
in, or I don't know.
I really don't.
We don't have more time to put out more videos.
But yeah, we'll take a look at these.
OK.
So there we go.
a little bit more information, a little more context,
and having the right information
will make all the difference.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}