---
title: Let's talk about and update on New Year's message guy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=NaCKvNBhJHE) |
| Published | 2024/05/24 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about an update
that I'm fairly certain a lot of y'all have been waiting for
or hoping for.
I know I've gotten messages about it.
It's about the New Year's message guy,
if y'all remember that.
If you haven't seen that video, it'll
be down below in the description,
and I'll try to remember to put it in a comment as well.
If you don't remember it, I would strongly suggest watching that before watching this.
So we're definitely taking a little break from news for this video.
So he sent a message with an update.
The first part had a bunch of personal information on it, and then it gets to this part.
I'll write the next part so you can read it without changing anything, because if you
decide to make a video because they want to know. I've been wanting to write this
since you put out the video about the guy who said he was just like me. I'm the
New Year's resolution guy. I watched that video and read the comments. I figured I
didn't have anything left to lose, so I did what Bo said. But as importantly, what
a bunch of you said in the comments. You all told me to go do the 12 steps while
I was on the road. I did. And at the second town we were at, I walked into the
meeting and my boss man was there. He had been going to different meetings in the
first town. He didn't try to be friends at first. In fact, he pretty much avoided
me. But that was okay. I like my job and I love seeing the country. I had never
I left the area before this. But after a while, he took me to lunch and told me
he'd seen others come to meetings, but they never stayed going to the meetings
or even working. So he wanted to give me time to show myself. At one meeting, I
talked about the video and even played it. Afterward, he asked me if I followed
the advice. I told him I did. He asked if I had figured out a place to stay yet.
I told him no. He told me I could stay in a little place he had behind his house
when we got back until I found something better. It's about 100 miles from where I
lived before. It may not seem like a lot, but that's going to make it easier for
me. Just one more little step that makes it all easier. Thank all of you. Y'all gave me
more encouragement than anybody ever has. And then I period will period not period go
period back. Actually, no period. So, there's your update. Pretty good reminder that, you
know, when you're online, it's all ones and zeros, you know, not real people. Sometimes
your comments in
comment sections or on social media
they might have a really profound effect on somebody.
So to all of y'all that
provided that advice
looks like it really paid off.
Anyway, it's just a thought.
I hope you all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}