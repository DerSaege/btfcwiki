---
title: Let's talk about Biden, Obama, Kenya, and the future....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=g4_ZWMZqv8o) |
| Published | 2024/05/24 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Biden and Obama
and a star-studded event put on by the US government,
the president of Kenya, the future,
and just kind of go over a few things,
including some that is a little funny,
We'll talk about an error, and we'll get some insight.
OK, so the US short version put on a star-studded event,
rolled out the red carpet for the president of Kenya,
getting just an overwhelming event.
And it wasn't without reason.
There was a purpose behind it.
We will start with one of the funny events for the younger people watching this right now have to explain something
that's going to sound really weird.  So when Obama was in office, the Republicans kind of put it out there that he was
Kenyan.
that that happened it was a lot like the the claims about the election of today it
was something that they strung along put out there lots of innuendo to to sway
the most easily manipulated of their base with Obama at this event for the
president of Kenya, it was implied that the reason he was there is because he
was Kenyan, which I personally found hilarious. But that's not what it was
about. It's about foreign policy. One thing to note that's important because
people are going to ask why, it's not just this but to show the the
relationship that exists between the US and Kenya, Kenya will be putting up the
troops for Haiti. That's what it looks like right now. And to the people in Kenya
who might be watching, thanks because you did everybody a solid with that one. We
didn't want to go and they didn't want us there. But the event wasn't just about
that. During this event, closening the ties between the two countries, Kenya was
designated a major non-NATO ally. If I'm not mistaken, I think it's the first
Sub-Saharan nation to get that designation. What does it mean? Does it
mean that the two countries have to defend each other or anything like that?
No. It doesn't, it's not a real defense pact like NATO. It's also not
symbolic either. The military in Kenya, they will be able to get more modern
stuff, more technologically advanced stuff than other players. There will be
trade benefits, that's what it's about. It's about strengthening those ties.
This is the beginning of the US kind of positioning to say, hey, Kenya could be
Saudi Arabia. They could be that regional ally. We have talked about it for a very
long time on the channel. The idea long-term strategy of the US to
deprioritize the Middle East and focus more on Africa. All this stuff that we
have talked about has been very low-key stuff, very in the background, stuff that
if it's reported on it's on page 9. This is front page news. Countries in the
Middle East with outstanding issues, they probably want to pay attention to
this. So, it is deepening those ties, bringing the two countries together in
hopes that Kenya becomes a regional player that is closely aligned with the
United States. That's what it's about. It does signal with how public this was
being front page news, that the US is ready to start doing that a little bit
more prominently. Okay, so let's get to the error. The Speaker of the House did
not invite the President of Kenya to speak to Congress. That should have
happened. That should have happened. Is it a huge deal? No. There's not even a
guarantee that the Kenyan president would have accepted, but the invitation
should have been extended. That was just a completely unforced error. With the US
putting on the show that it was, that should have been part of it. It
really should have. But it's not a huge error, but it's one that's definitely
going to be noticed. So that's what's going on and the show that was put on,
this red carpet event, this is a signal that the US is definitely pursuing that
course and is going to start doing it way more publicly than before. Because of
this, just go ahead and get ready for it because as the US starts partnering with
nations in Africa, just be ready for all the racism to come out because it's
going to. There are a lot of people that do not understand the significance of
Africa as a whole and the power of some of these countries and the racism
is just going to be huge in the beginning. It's going to be massive so
just be ready for that. But I think one of the other big pieces here is that
that the US is starting this openly now.
It's definitely more prominent than before.
It is probably a clear indication that that strategy of deprioritizing the Middle East
is underway, which means there are a lot of countries in the Middle East that have for
long time relied on the United States. I'm gonna say that the clock just started
on how long that support is going to exist in the way it has in the past. Anyway,
It's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}