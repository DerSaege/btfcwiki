---
title: Let's talk about Trump, Putin, promises, and questions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=tOBqMIdMx2M) |
| Published | 2024/05/24 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about Trump
and Putin and promises and a question,
a couple of questions that I have about this promise
because the former president has said something
and I feel like we might need to go over the realities
when it comes to what he is saying here. If you missed it, the former president of
the United States, Donald Trump, said, quote, the reporter from the Wall Street
Journal who is being held by Russia will be released almost immediately after
the election, but definitely before I assume office. He will be home, safe, and
with his family. Vladimir Putin, president of Russia, will do that for me, but
But not for anyone else.
And we will be paying nothing.
All caps.
I mean, okay.
That is quite the claim.
I'm wondering why he believes that, if he's been maybe talking to anybody over there.
I would assume not.
But my question is if this relationship that Putin has with Trump, if it's so good that
Trump can just snap his fingers and get Americans released, why didn't he do it when he was
president?
You know, because there were Americans who were picked up during Trump's term.
Why didn't they come home?
Why didn't Trump use this wonderful relationship that he has with Putin to get them home?
I guess Trevor Reed, Paul Whelan, I guess they just didn't write.
They weren't a good enough headline.
Both Marine Corps vets that I guess Trump just didn't feel like making the call for.
Because I would assume that whatever relationship exists between Trump and Putin, well that
was formed while he was president.
So he could have made that call and got that done while he was in office for them.
Because he'll do that for me, but not for anyone else, because they have a special relationship.
So if he didn't do it for them, the question is why?
Did their relationship get better after he left office?
I wonder what Trump gave him.
That's assuming it's true, which is not always a safe bet.
making a claim that his record while he was in office seems to indicate is, well, unlikely.
If he could do what he is saying here, he has to explain why he didn't do it when he was president.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}