---
title: Let's talk about Trump, Biden, executive orders, and legislation....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=VZHTElQCJMQ) |
| Published | 2024/05/24 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, Internet people, it's Bo again.
So today, we are going to talk about executive orders,
executive actions, and legislation.
And we're going to talk about how they work and
how they lead to something that really doesn't make sense.
We do this because a whole bunch of y'all have the exact same question when we were
talking about the Democratic lawmakers who wanted to
do something about your grocery bill, wanted to do something about competition
when it comes to grocery stores. Basically they have legislation but it
kind of stalled so now they're reaching out to Biden to do an executive order or
some kind of executive action. And the question that came in over and over and
over again was why didn't they just start there? That would make sense, right?
like it would make complete sense to do it that way. You, as soon as you come into
office, you reach out to the president, you get the executive action going, and
then you try to push your legislation through. That way if it stalls, the
executive action is there to help a little bit at least. That would make
sense. But here are the problems with that. First, if you don't know, an
executive order really doesn't mean much. Picture any place you have ever worked
that has a regional manager. The regional manager says do it this way. When that
regional manager gets fired or moves to a different position, the new regional
manager shows up. Do they always want it done the same way? No, they change
things. Right when they take over, you get that list of things that's going to
change within a few months. That's what executive orders are. They can be undone,
in most cases, very easily by the next president. So keep that part in mind. But
still, you would have a length of time while you were trying to get your
legislation through. You'd have like three and a half years. So why not do it?
do it. Because us, the American citizens, we're not logical. Therefore, they can't
do the logical thing. If there is legislation pending, there is support
for that legislation. When an executive order gets signed, well that problem has
been addressed and that support disappears. That's why. Because Americans
view things the way they do when it comes to executive orders, and they view
it as law when it's not, they feel like the problem's already been addressed.
And it reduces support for the legislation and increases the likelihood
that it stalls. It's really that simple because we as a population do
not practice advanced citizenship. They can't use that route because they
have to leave it unaddressed so the support remains. It's
It's really that simple.
The executive actions, those are last-ditch efforts to help because they couldn't get
the support.
So this is one of the reasons that Trump doesn't have a real legacy beyond the Supreme Court
and a mismanaged pandemic.
So his legacy is the Supreme Court.
He doesn't have a legislative legacy, really, because he used executive order for most things.
And all of that can easily be undone.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}