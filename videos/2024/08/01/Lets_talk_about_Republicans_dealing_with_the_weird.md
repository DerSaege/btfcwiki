---
title: Let's talk about Republicans dealing with the weird....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=vgTNXH-jdBQ) |
| Published | 2024/08/01 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there internet people, it's Miss Bo again.
So today we're going to talk about the weird situation Republicans find themselves in.
How it's actually pretty similar to another recent political dynamic that was race altering.
Lately the Democratic Party has been calling Republican policies and candidates weird.
Simply weird.
At first glance it seems like a mild and kind of silly line of political rhetoric.
However, it creates a dynamic we just saw the conclusion of.
After Biden's poor debate performance,
the media piled on to the idea that the performance meant
it was time for him to go, that he was unfit.
Once the perception was planted, any move he made,
any mistake in speaking, was seen as further evidence
that he was slipping, regardless of whether it was.
Both Trump and Biden could make the same mistake,
say, using the wrong name for a person.
But the seed was planted about Biden,
so the perception stuck.
Weirdly enough, Republicans being called weird
is having the same effect.
The more they react, the more the perception sticks.
The more they try to defend their policies as not weird,
the more the American people hear the policies
through fresh ears and realize many of the statements
rhetoric coming from the Republican side of the aisle are outside the norm of the
way most Americans think. Making the ideas, for lack of a better word, well
weird. Much of the Republican Party's rhetoric, and this is true of politics
across the world, is boiled down to talking points. Simple, quick statements
that are repeated often enough to be accepted without critical thought. The
second that talking point is examined, it often falls apart. People wonder the
obvious question behind the talking points. Why wouldn't we need to vote
again in four years? Why are they saying they don't want a nationwide band when
they've worked decades to achieve one and seem to be trying to implement it
everywhere they have the power? Why can't people without children be invested in
the country's future exactly? The Republican Party has taken a cue from
from Trump and refuses to acknowledge mistakes. So as they double down on the
rhetoric, the explanations become more and more, well, weird. Much like the
situation the Democratic Party found itself in, the only way to shake the
label is to make a large change. And if there's one thing we know about the
Republican Party, it's that they are completely open to change. Yeah, so that
seems unlikely. In fact, if they did it, that would be weird. Anyway, it's just a
thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}