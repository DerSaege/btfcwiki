---
title: The Roads to your questions about the changes
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=KCq4RFwgBks) |
| Published | 2024/08/01 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there internet people, it's Miss Bo again, and welcome to the Roads with Miss
Bo. Today we're going to ride down the road of your questions about recent developments
on the channel, and we're just going to dive right in. What do we call you other than Miss
Bo? A lot of people in the comment section and emails have suggested Belle. I think I
like it. It fits with the channel and the themes. I don't know if I'll change the intro
to It's Belle Again, but we can go with Belle. I like it. Now this is probably the
question that came in the most. It's basically, is Bo's health okay? His health
is fine in the sense that he hasn't received a terminal illness diagnosis or
anything like that. Any physical issues he has are the result of overworking
himself and they'll resolve as he rests and regroups.
Can Bo come on just a day, a week, or something along those lines?
Bo's problem stems from being a workaholic.
He has to totally step away for it to work.
If he was here just one day a week, one day would turn into two, then three, then four.
The support in the comment section has really helped him to feel a lot better about making
this decision.
We really appreciate that.
The kind welcome and feedback has been very helpful and we really do appreciate it as
a team.
His announcement made it sound like he's never coming back.
Is he planning to return later?
For this to work, he has to commit to stepping away.
If there was an anticipation of return, it wouldn't work.
He just bide his time, so there are no plans for him to come back.
Everything that made him so good at this is working against him right now.
His obsessive nature needs a rest.
Is the channel going to continue the aid and charity projects?
Of course, it wouldn't be the channel without that.
As we talked about, we are going to keep this channel on the same path it's been on.
Other than adding the shirts to the background, are there any other changes you already know
you're going to make?
I may sit down or lean on a stool or something but that's it and I definitely think I might
get a patch that suits me so that's it for now though.
Are you going to keep the same schedule Bo did?
That seems like a very bad idea.
is a bad idea. So we're going to be going down to three videos per day effective
today. We'll be keeping the 9 a.m. 3 p.m. and the 1145 central slots. The 730 p.m.
slot will go away or maybe we'll share a post from the archive or maybe from the
second channel or maybe if there's a break in the news but yes we don't want
recreate the same issue. So we will be going to three a day. No offense, but how
are you going to provide all the insights on so many different topics the
way Bo did? He was kind of one-of-a-kind in that regard. Remember that Bo would
always say he was only an expert in one thing. What gave him the ability to have
so many insights was a network of experts in various fields. He would call
on them when he needed more information. We have his phone book. We have his
sources. His friends are my friends. It's also important to understand that none
of us are trying to fill both shoes. We just want to keep walking the same path
and give me a chance. I did things. I know stuff. Just hang in there with me.
Are you going to rebrand the channel? That's being discussed. Beau said we should. The
team isn't sure. We want to make sure that any change is well thought out and at a slow
enough pace to not be jarring. This whole changeover, it occurred in a matter of hours.
We kind of all had concerns that Bo was going to hit a wall and had a rough plan, but we
haven't really worked out the finer points yet.
When Bo said he was ready, it was eight hours to my first video.
So we made a plan and rolled it out.
We're just not in a hurry to rebrand and change a lot yet.
How do you plan on not falling prey to sensationalism like so many other channels?
Is there some kind of safeguard?
The entire team is committed to that aspect.
We are the keep calm and convey ideas.
We're not trying to change that, and we're committed.
This might sound like a dumb question, but when he said bring on different views, it
made me worry about people with ideas that Bo would see as harmful being platformed.
It might be selfish, but this channel has always been one of the few channels that would
always stand up for the LGBTQIA plus community. That wasn't actually part of
us. At least I assume wasn't part of us. I don't mean to be dismissive, but this
is not something that you need to worry about at all. The message of this channel
will not change.
Hi, Ms. Bo. Your husband was kind of an internet dad to me and a lot of other people. Is he
still going to get emails and maybe give us advice through you or return an email? Oh,
you can still send Bo personal emails. I can't guarantee he'll respond right away, but I
I think it might actually be good for him.
And you have an internet mom now, too.
Do you think you'll be using scripts?
I remember always being shocked every time
that Bo said he didn't script.
We will definitely be using scripts.
Part of our fact checking process
requires a script anyway.
That was something Bo was insistent on,
Which is totally unsurprising coming from the guy who was obsessed over the whole honeybee, bumblebee, fiasco, whatever.
I can't remember. He messed it up and was upset about it.
So yes, there will be scripts, but hopefully we can pull that casual vibe that Bo did so well.
One question that I noticed was, will Bo be taking over the horses?
not. He will just go down there and play and no work will get done. So yeah, no.
He's not going to be doing that. Okay, so that looks like all the questions. So
there you go. A little more information, a little more context, and having the
right information will make all the difference.
Y'all have a good night.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}