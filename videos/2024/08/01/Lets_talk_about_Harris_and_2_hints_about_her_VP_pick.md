---
title: Let's talk about Harris and 2 hints about her VP pick....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=vzMlx2t_QXk) |
| Published | 2024/08/01 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there internet people. It's Miss Bo again.
So today we're going to talk about the scheduled first appearance of Harris's
chosen running mate and what the rumor mill says about the news.
Speculation about Harris's choice for her VP candidate is running rampant after two
pieces of information surfaced. The first piece of information is that Harris is
expected to appear in person for the first time with the still unnamed
running mate next Tuesday. That's the first time we've had a clear date of
when to expect the announcement, other than by knowing that we would find out
when the Ohio deadline comes. But it's the location of the appearance, not the
date that raised eyebrows and fueled speculation. Philadelphia, Pennsylvania.
That location would seem to favor a selection of Pennsylvania Governor Josh
Shapiro, and speculation has run wild. However, it's worth noting that a Harris
campaign aide is reported to have cautioned against reading too much into
the first city chosen for the tour. This seems to indicate that Shapiro isn't
actually the choice, but that perhaps the location was chosen based on other
factors, such as it being a state that the Democratic Party desperately needs
to win, and therefore campaign in, and being one of the last possible moments to announce
before the Ohio deadline.
The speculation about the VP pick is helping fuel the buzz and momentum the Harris campaign
is currently capitalizing on.
Keeping that going as long as possible is just good politics.
Now onto the second piece of information that has people talking.
The Harris campaign, while speaking to large Wall Street donors, is reported to have encouraged
them to get their checks in as soon as possible.
That has those donors speculating that Hervey P. Choice is a governor because the SEC has
rules for some Wall Street employees about donating to federal campaigns with a state
official on the ticket.
If this line of thinking is correct, Senator Kelly would be out of the running.
But even though all of this is speculation is fun, there's also the possibility that
the Harris campaign just wants to keep their fundraising momentum going.
And it's pressing potential donors to make their contributions now, because earlier is
better for them, it has nothing to do with the decision being made.
One of the most important decisions for the Harris campaign is shrouded in secrecy and
overrun by speculation, which when you look at the benefits of that, speculation for the
campaign, it's again really smart politics to keep the speculation running
for as long as they can. It wouldn't be a surprise if there were a little more
information that comes out over the next few days that lead to more speculation
in various probably contradicting ways just to keep the press busy trying to
solve the Scooby-Doo mystery. Because if the campaign can do that, they'll keep
getting headlines and generating buzz and momentum. Anyway, it's just a thought.
Y'all have a good day. and I'll see you next time.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}