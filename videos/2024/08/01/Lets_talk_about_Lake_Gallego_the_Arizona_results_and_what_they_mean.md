---
title: Let's talk about Lake, Gallego, the Arizona results, and what they mean....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=F4vDxJq_moQ) |
| Published | 2024/08/01 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well howdy there internet people, it's Miss Bo again. So today we're going to
talk about the Arizona primary results and what those results can tell us about
the situation in November. The big question revolves around the Senate race.
The Arizona race for US Senate is incredibly important for both parties. For
the Republicans they need to net one seat in the Senate and win the White
House or net two seats in the Senate to gain control. Obviously the Democratic
Party would like to prevent either of these scenarios. The winner of the
Republican primary was Kerry Lake, so she'll be taking on Ruben
Gallego. This wasn't really unexpected, but the part that's likely making Mitch
McConnell's blood pressure rise isn't that Lake won. It's that her victory
wasn't decisive. It indicates a divide in the Republican Party. From angering
McCain Republicans to her claims about the elections, Lake has been a divisive
figure in the Republican politics, to put it mildly. For Republicans to pull off a
win in Arizona, it would require a level of unity that seems an unlikely dream at
this point. Meanwhile, the Democratic Party is very much lining up behind
Gallego, who also seems to be pulling a large amount of support from
independence. Polling tends to suggest Gallego has a substantial lead, and most
times it appears that that leads outside the margin of error. Other key takeaways
from the primary shows that Arizona GOP's aren't quite done flirting with
election denial. A number of candidates who have made weird claims about recent
elections advanced through the primaries. A lot of political commentators felt
that rank-and-file had already had their fill of these candidates, but it appears
that even though many Republicans are rejecting the election claims, they
decided it wasn't a deal-breaker for them. In perhaps more entertaining news,
Trump finally did something we probably all knew was coming on some level. Trump
endorsed both leading candidates in the primary race for the 8th district.
Trump's endorsement power has been called into question in recent primaries.
Endorsing rival candidates gave Trump an opportunity to boost that perceived
power of his endorsement because no matter who won they could say that he
endorsed the candidate. They were his guy. Trump is pulling the same thing in
Missouri where at this point he has endorsed all three of the candidates for
governor. No matter who wins he looks like he influenced the outcome when in
reality this move just renders his endorsement irrelevant. So while there
weren't many surprises in the Arizona results. The dynamics that gave us those
results definitely shed a little light on what we can expect as we approach the
general election in November. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}