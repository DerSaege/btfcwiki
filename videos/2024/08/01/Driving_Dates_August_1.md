---
title: 'Driving Dates: August 1'
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=A7LRlPPOHGg) |
| Published | 2024/08/01 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people. Welcome to the Rhodes.
And this is Driving Dates, a series where we discuss dates that drive history forward.
Today is August 1st, 2024.
Starting off in 1774, Joseph Priestley discovered oxygen,
even though at first he hadn't realized exactly what he discovered.
In 1834, the Slavery Abolition Act was put into effect and being enforced.
The Act outlawed the British trade of enslaved people and compensated their former owners.
In 1876, Colorado became the 38th state to become part of the Union.
It was admitted 100 years after the Declaration of Independence, earning it the nickname the
The Centennial State.
Its first attempt at statehood was vetoed by President Andrew Johnson.
In 1932, the George Washington quarter was released into circulation.
It occurred in 1932 to mark the 1732 birth of Washington.
In 1944, Anne Frank gave her last entry into her diary.
In 1946, the Atomic Energy Commission was established by President Harry Truman when
he signed the Atomic Energy Act.
It transferred a lot of responsibilities to the AEC from the military.
In 1960, Aretha Franklin first walked into a recording studio, starting a career that
would shape music.
In 1972, an article by Woodward and Bernstein titled Bug Suspect Got Campaign Funds appeared
in the Washington Post.
The Watergate scandal was underway.
In 2020, Egypt's Minister of International Cooperation suggested Elon Musk look into
the writings about how the pyramids were constructed, after Elon tweeted,
Aliens built the pyramids, obvi!
In 2023, former President Trump was indicted.
The indictment said,
The purpose of this conspiracy was to overturn the legitimate results of the 2020 presidential
election by using knowingly false claims of election fraud to obstruct the government
function by which those results are collected, counted, and certified.
And finally, today's quote of the day is,
You may encounter many defeats, but you must not be defeated.
In fact, it may be necessary to encounter the defeats so you can know who you are, what
you can rise from, and how you can still come out of it," said by Maya Angelou.
And that's all we have to date for this date.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}