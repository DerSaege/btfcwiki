---
title: Let's talk about Trump at a convention for black journalists....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=MKjzRdiRxho) |
| Published | 2024/08/01 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there internet people, it's Ms. Bo again.
So today we're going to talk about Trump
attending a convention for black journalists.
It went exactly how you would expect it to go.
Trump was attending an annual convention
held by the National Association of Black Journalists.
He took the stage about an hour late.
The discussion soon developed into a very chaotic exchange.
Trump was asked about the Republican lines of rhetoric,
Harris a DEI candidate. He said, and I quote, so I've known her a long time,
indirectly, not directly very much, and she was always of Indian heritage and she
was only promoting Indian heritage. He went on to say, I didn't know she was
black until a number of years ago when she happened to turn black and now she
she wants to be known as black. The cringeworthy statement concluded with so
I don't know is she Indian or is she black? For those unaware setting aside
the apparent inability to understand multiracial possibilities, Harris went to
Howard, joined a historical black sorority, was a member of the Congressional
Black Caucus and has spoken repeatedly and at length about her heritage. In any
other political world, the opportunity presented to Trump would have resulted
in him condemning the weird statements by fellow Republicans about Harris. But
in this one, it served as a launching pad for easily disproven statements about
her heritage. The Trump campaigners response after this exchange was to say,
Today's biased and rude treatment from certain hostile members of the media will backfire massively.
During a White House briefing, the press secretary was asked about Trump's statements and had this to say,
As a person of color, as a Black woman who is in the position that is standing before you at this podium, behind this
lectern,
what he just said, what you just read out to me is repulsive. It's insulting. As the
dynamics of the presidential race continue to change, comments like that
are likely to have an impact on the support the candidate experiences.
Negative voter turnout is becoming a fixture in American politics. In today's
political world, there's almost as much of a chance of a person showing up to
vote against a candidate, as there is for a person showing up to vote for a
candidate. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}