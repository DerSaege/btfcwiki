---
title: Let's talk about Gloria Johnson, Tennessee, and the state of play....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=WPB6Aqo1myE) |
| Published | 2024/08/04 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Bell again.
So today, we're going to talk about Tennessee, the U.S. Senate, and a person you'll probably
remember, even if you don't remember her name.
Outside of Tennessee, you might not remember the name Gloria Johnson, but you'll probably
remember the incident last year when Republicans in Tennessee expelled two black lawmakers
from the state house and tried to expel a white woman lawmaker, barely missing the number
votes needed. That woman was Gloria Johnson. Since her debut on the national
stage as a member of the Tennessee Three, she has used her elevated political
profile to launch her campaign for the US Senate. She recently won the primary
and will be the Democratic candidate. She'll be facing Marsha Blackburn.
Blackburn is a pretty entrenched figure both in Tennessee and nationally. She has
a lot of support from both the normal traditional Republicans and MAGA
Republicans. She'll be hard to defeat and Johnson is facing an uphill climb in
the volunteer state that is traditionally very red. However, Johnson has
heavy name recognition in the state now, was part of a historic event, should be
able to decently fundraise, and might be able to capitalize on the growing
enthusiasm for Vice President Harris. It's way too early to say that Tennessee
is truly in play by any normal political metric, but it's more in play than it
has been in recent memory. The balance of power in the Senate is likely to hang by
a thread, and if there's even a chance that Johnson might win, the Republican
Party will be forced to expend resources, time, and money to defend a state that is
traditionally an easy victory and an easy seat for them to hold. Using them in
Tennessee denies their use elsewhere. Again, it would take massive voter
turnout of Johnson supporters to flip the seat, but there's a small glimmer of
hope that the enthusiasm is there. That hope on the Democratic side of the aisle
will end up being a fear of an upset on the Republican side. Anyway, it's just a
thought. Y'all have a good day. Bye.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}