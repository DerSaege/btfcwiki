---
title: Roads Not Taken EP 50
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=lbJ2VQ1W4mo) |
| Published | 2024/08/04 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
It's Bell again, and welcome to The Roads with Bell.
Today is August 4th, 2024,
and this is episode 50 of The Roads Not Taken,
which is a weekly series where we go over news
that was unreported, under-reported,
didn't get the coverage we thought it should,
or we just found it interesting.
Then we go through a few questions from y'all.
foreign policy. So, the big news in foreign policy, which was also about 50 questions,
what happened over there? There's been a back and forth between Israel and non-state
actors as well as targets hit within Iran. It's raised tensions across the region and
elevated fears of wider regional conflict. US forces have been deployed to the region.
Their most likely use would be to duplicate the interception of a barrage similar to the
previous one from Iran, but their use if any would depend on the actions
undertaken by those aligned with Iran. It appears that the risk of regional
conflict is being de-escalated, but there will certainly be a round of response
from the Iranian allies and tensions are high. Two mistakes at the same time could
send things spiraling and spark wider conflict. As a result of the tensions
citizens of the US and UK have been urged to leave Lebanon immediately. There
is early reporting that Ukraine might have hit a Russian submarine the Rostov
Don. This isn't duplicate news item it looks like they hit it again and this
time possibly sunk it so we're waiting for their confirmation. China's economy
slowed to its slowest pace in more than a year, prompting the government to
introduce a stimulus aimed at micro businesses as well as a tax incentive
for those with children under the age of three. That's right y'all. China's
giving tax breaks to people with kids while Republicans are voting it down
here. In US news, Maui wildfire plaintiffs have reached a four billion
settlement. Former President Jimmy Carter says he hopes to vote for Harris
in the upcoming election. There will be a special election in Texas to fill the
seat left vacant by Representative Sheila Jackson Lee. It will occur at the
same time as the general election on November 5th and will select the person
who will hold the seat until January. The general election will select the person
who will hold the seat from January into 2027. I'm sure that's not going to lead
to any confusion. The mayor of Tulsa established a commission to look into
the possibility of reparations over what happened in 1921. If you're unfamiliar
with the story, it's definitely worth looking up. A tropical depression in the
golf is expected to strengthen as it nears Florida. I don't know why I stay
here. I don't. In cultural news, Rittenhouse stated he would not be voting
for Trump and very predictably right-wing backlash occurred and he is
seems to be reversing course. In science news, millions in Texas were asked to
avoid drive-through lanes to limit emissions. The National Weather Service
also said, quote, you can help prevent ozone pollution by sharing a ride,
walking, riding a bicycle, taking your lunch to work, avoiding drive-through
lanes, conserving energy, and keeping your vehicle properly tuned. Ozone
pollution is what most of us would call smog, but drill baby drill. The Park
Park fire has grown to be the fourth largest in California history.
In oddities, a man attempting to base jump at the Grand Canyon did not survive the attempt.
Base jumping is not allowed anywhere in the Grand Canyon National Park.
So now we're on to the Q&A.
So is what we're seeing over there realignment operations?
Maybe. Generally, they're a little more subtle.
This seems more like going after command and control more broadly,
but we don't know what the targets they're passing on. If any,
your biggest clues will be what happens at the negotiation table.
What are the signs that Bo needed to step back?
One that you all may have seen was he was asked repeatedly time and again,
time and again, what are you reading?
And I can't remember Bo reading a fiction novel in over five
years, and he reads a lot.
I think another factor, one that hit us kind of hard,
was that this summer has flown by.
And there were things that we did,
and he just wasn't able to join us.
He felt he needed to be working and that there
things to do. So it's a it's a combination of things and I'm just proud
of him for saying I have to step back and really doing it. So this came in
more than I would have guessed. Are you looking for more people to be on screen
and if so what's the audition process? Maybe we haven't gotten that far
yet but if we do we'll let you all know we're we're still trying to slow changes
still trying to keep the channels where they are and and slow but as soon as we
figure something like that out we will definitely let you all know how are you
going to present prevent yourself from burning out both tasks are now divided
among four people and we've cut down video production by 25%. We should be
good but on this topic in the future we may have to do a little playing with the
the evening recording or the the night video but at this point we're not making
any more changes. It seems to be going okay. It turns out that I do have a
couple of important questions. One, are those plaid shirts flannel? And two, does
the plaid shirt collection rival Bo's t-shirt collection? I'm sure I won't be
the only person who wants to know. Some of them are flannel, some of them aren't,
and as for rivaling Bo's t-shirt collection, nothing I have would rival
Bo's t-shirt collection. I don't think anybody can rival Bo's t-shirt collection.
It's... it's unreal. Where do you get your plaid shirts? Well, I got them from a
place that I don't really shop at anymore, so I'm gonna be looking for more
now, and when I know, when I figure out who has the cute colors and this
the soft fit I'll let you know. Will Bo go do relief efforts if
needed after the Florida storm? I hope it's not needed but if it is I'm sure
he'll go and it will probably be good for him. He likes to drive. He needs to
have a place to drive too and he loves to help so those things will help keep
him centered and I hope if it is needed he's able to go. I know you were behind
the scenes forever, but what's something you noticed since you've been in front
of the camera? That all those things I feared I had good reason. No. The
The funniest thing is there's a whole set of shelves to my left that are exactly like
the ones behind me and they are full, full of props.
Like there is so much stuff in here and I'm finding my Halloween decorations.
So I'm going to have to come up here and clean this place out and find all my decorations
because it's almost that time, y'all, but I had no idea how much stuff he had up here.
no clue. It's impressive. I didn't even know you could put this much stuff up
here. Um, so finding all these Easter eggs, it's crazy. So, and that looks like
it's it. So, there you go. A little more information, a little more context, and
having the right information will make the difference. Y'all have a good day!
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}