---
title: Let's talk about Trump's DC entanglements showing up again...
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ekF03JaIWPA) |
| Published | 2024/08/04 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Bell again.
So today, we're going to talk about Trump's entanglements resurfacing in DC.
After a lengthy seven-month delay, things will start moving in earnest again on August
16th in the federal elections interference case against the former president.
Even though Trump himself will not be required to attend, the parties will be back before
Judge Chetkin.
attorney sought to dismiss the case on statutory grounds, but that move was
denied, though the judge said they could refile the motion once presidential
immunity issues were resolved. The judge will have to make determinations in
applying the Supreme Court's decision. She will decide which actions were
official acts and which actions, if any, were private acts. Even though a trial
before the election is incredibly unlikely and most would say impossible,
the Trump campaign has a whole different set of issues to worry about now. The
procedural hearings and the attempt to sort out whether acts were official or
not will probably draw more attention to an entanglement the campaign would most
certainly like to bury. Trump's other cases are winding their way through
various appeals and hearings and will continue to pop up now and then, but none
are likely to move forward in earnest before the election. The development in
the DC case, on the other hand, will create a new barrage of headlines,
discussion, and analysis that will end up dominating the news cycles some days and
maybe some weeks. We have to wonder how these headlines will impact the thought
process of the American voter. There's no real historical parallel to draw on.
Those voters who haven't already been swayed one way or another have a new
dimension to consider as the election nears. It also raises the possibility of
an unplanned October surprise arising simply through the proceedings. This
story isn't over. You'll see the material again. Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}