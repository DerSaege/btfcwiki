---
title: Let's talk about Trump, empty podiums, weakness, and concerns....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=lCj6A_z0VDo) |
| Published | 2024/08/04 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Bell again.
So today, we're going to talk about Trump excuses, weakness, and concerns.
The ever-growing list of excuses Trump is trying out to avoid debating Vice President
Harris is getting pretty tedious, and his reasoning is strained at best.
Trump has stated he will no longer participate in ABC's September 10th debate.
His first excuse was that Harris wasn't really the nominee yet.
though Biden wasn't really the nominee when he agreed. Well, she is now. Then he
said he didn't need to because he was leading in the polls, but he's not. Now he
cited a pending defamation lawsuit against ABC and an anchor, but that suit
was filed months before he initially agreed to the debate. None of his stated
reasons stand up to even the slightest bit of objective scrutiny. Trump once
said anytime, anyplace, but now that has apparently turned into him taking his
ball and going home unless the debate is hosted on Fox. The Harris campaign
director said, Donald Trump is running scared and trying to back out of a
debate he already agreed to and running straight to Fox to bail him out. He
needs to stop playing games and show up to the debate he already committed to on
September 10th. The Harris campaign will get a lot of political mileage out of
this. The erratic behavior will be attributed to him being afraid of her
or unfit, or maybe just him being, dare I say it, weird. It doesn't matter which
track the Harris campaign uses. It will land with some of the undecided voters.
In a development that might alter Trump's thinking, reporting suggests that ABC
plans to keep the time slot, and if Trump doesn't show, the airtime will be given
to Harris to address a national primetime audience. While all this makes for
interesting political drama viewed strictly through the political lens, this
demonstrates something to the American people that might be even more important
than anything going to be said during a potential debate. If Trump can adapt to a
a change in opponent for a debate with weeks of lead time, how can he possibly
be expected to handle the ever-changing dynamic of domestic or international
issues? In real life, he won't be able to tell the opposing interests to wait and
meet him at his favorite safe space. For a man who just called the US negotiators
an embarrassment without knowing the details of the deal, he doesn't seem to
be handling the debate negotiations well. If he follows through with dropping out
of the debate, he is in essence surrendering to Harris and giving her
the airtime without doing anything more than throwing a social media temper
tantrum. The image of an empty podium could be a defining characteristic of
the 2024 race. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}