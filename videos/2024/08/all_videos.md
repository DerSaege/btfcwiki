# All videos from August, 2024
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-08-07 23:08:15
<details>
<summary>
2024-08-07: Let's talk about another Arizona AG win and Trump.... (<a href="https://youtube.com/watch?v=7nvZ189D1XU">watch</a> || <a href="/videos/2024/08/07/Lets_talk_about_another_Arizona_AG_win_and_Trump">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-08-07: Let's talk about Trump being Trump in Washington state.... (<a href="https://youtube.com/watch?v=Kv9TJIbLKgQ">watch</a> || <a href="/videos/2024/08/07/Lets_talk_about_Trump_being_Trump_in_Washington_state">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-08-07: Let's talk about Trump and GOP criticism about the swap.... (<a href="https://youtube.com/watch?v=NEJYBE6ljNc">watch</a> || <a href="/videos/2024/08/07/Lets_talk_about_Trump_and_GOP_criticism_about_the_swap">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-08-06: Research Road EP  3 (<a href="https://youtube.com/watch?v=P4c0Ge9czVg">watch</a> || <a href="/videos/2024/08/06/Research_Road_EP_3">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-08-06: Let's talk about the difference a tenth of a degree makes for humanity.... (<a href="https://youtube.com/watch?v=kQgV395a134">watch</a> || <a href="/videos/2024/08/06/Lets_talk_about_the_difference_a_tenth_of_a_degree_makes_for_humanity">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-08-06: Let's talk about Walz, Harris, and Trump's moves.... (<a href="https://youtube.com/watch?v=AmzdnIs7FrE">watch</a> || <a href="/videos/2024/08/06/Lets_talk_about_Walz_Harris_and_Trump_s_moves">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-08-06: Let's talk about 2 Trumpworld setbacks in AZ and SCOTUS.... (<a href="https://youtube.com/watch?v=v3tnoUmOQbc">watch</a> || <a href="/videos/2024/08/06/Lets_talk_about_2_Trumpworld_setbacks_in_AZ_and_SCOTUS">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-08-05: Let's talk about Ukraine, planes, and what comes next.... (<a href="https://youtube.com/watch?v=m1JDBNHxJQ0">watch</a> || <a href="/videos/2024/08/05/Lets_talk_about_Ukraine_planes_and_what_comes_next">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-08-05: Let's talk about Trump, Georgia, Kemp, and mistakes.... (<a href="https://youtube.com/watch?v=5bqIsnnZwvY">watch</a> || <a href="/videos/2024/08/05/Lets_talk_about_Trump_Georgia_Kemp_and_mistakes">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-08-05: Let's talk about Republicans for Harris.... (<a href="https://youtube.com/watch?v=F_S57UTs6Q8">watch</a> || <a href="/videos/2024/08/05/Lets_talk_about_Republicans_for_Harris">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-08-04: Roads Not Taken EP 50 (<a href="https://youtube.com/watch?v=lbJ2VQ1W4mo">watch</a> || <a href="/videos/2024/08/04/Roads_Not_Taken_EP_50">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-08-04: Let's talk about Trump, empty podiums, weakness, and concerns.... (<a href="https://youtube.com/watch?v=lCj6A_z0VDo">watch</a> || <a href="/videos/2024/08/04/Lets_talk_about_Trump_empty_podiums_weakness_and_concerns">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-08-04: Let's talk about Trump's DC entanglements showing up again... (<a href="https://youtube.com/watch?v=ekF03JaIWPA">watch</a> || <a href="/videos/2024/08/04/Lets_talk_about_Trump_s_DC_entanglements_showing_up_again">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-08-04: Let's talk about Gloria Johnson, Tennessee, and the state of play.... (<a href="https://youtube.com/watch?v=WPB6Aqo1myE">watch</a> || <a href="/videos/2024/08/04/Lets_talk_about_Gloria_Johnson_Tennessee_and_the_state_of_play">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-08-03: Let's talk about the Sahm rule, jobs, and economics.... (<a href="https://youtube.com/watch?v=wkf5thpiy7Y">watch</a> || <a href="/videos/2024/08/03/Lets_talk_about_the_Sahm_rule_jobs_and_economics">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-08-03: Let's talk about Trump, doubt, polling, and debates... (<a href="https://youtube.com/watch?v=DL1shtCW3Tc">watch</a> || <a href="/videos/2024/08/03/Lets_talk_about_Trump_doubt_polling_and_debates">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-08-03: Let's talk about Trump shaking senatorial support.... (<a href="https://youtube.com/watch?v=Oj0nA0FXfvg">watch</a> || <a href="/videos/2024/08/03/Lets_talk_about_Trump_shaking_senatorial_support">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-08-02: Let's talk about the US-Russia swap, Trump, and realities.... (<a href="https://youtube.com/watch?v=px00nvLovqU">watch</a> || <a href="/videos/2024/08/02/Lets_talk_about_the_US-Russia_swap_Trump_and_realities">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-08-02: Let's talk about Schumer setting up the GOP with kids and Kings.... (<a href="https://youtube.com/watch?v=nSAEqhmm2ok">watch</a> || <a href="/videos/2024/08/02/Lets_talk_about_Schumer_setting_up_the_GOP_with_kids_and_Kings">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-08-02: Let's talk about Harris and the virtual roll call.... (<a href="https://youtube.com/watch?v=tW60wiGFLZs">watch</a> || <a href="/videos/2024/08/02/Lets_talk_about_Harris_and_the_virtual_roll_call">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-08-01: The Roads to your questions about the changes (<a href="https://youtube.com/watch?v=KCq4RFwgBks">watch</a> || <a href="/videos/2024/08/01/The_Roads_to_your_questions_about_the_changes">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-08-01: Let's talk about Trump at a convention for black journalists.... (<a href="https://youtube.com/watch?v=MKjzRdiRxho">watch</a> || <a href="/videos/2024/08/01/Lets_talk_about_Trump_at_a_convention_for_black_journalists">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-08-01: Let's talk about Republicans dealing with the weird.... (<a href="https://youtube.com/watch?v=vgTNXH-jdBQ">watch</a> || <a href="/videos/2024/08/01/Lets_talk_about_Republicans_dealing_with_the_weird">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-08-01: Let's talk about Lake, Gallego, the Arizona results, and what they mean.... (<a href="https://youtube.com/watch?v=F4vDxJq_moQ">watch</a> || <a href="/videos/2024/08/01/Lets_talk_about_Lake_Gallego_the_Arizona_results_and_what_they_mean">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-08-01: Let's talk about Harris and 2 hints about her VP pick.... (<a href="https://youtube.com/watch?v=vzMlx2t_QXk">watch</a> || <a href="/videos/2024/08/01/Lets_talk_about_Harris_and_2_hints_about_her_VP_pick">transcript &amp; editable summary</a>)



</summary>
</details>
<details>
<summary>
2024-08-01: Driving Dates: August 1 (<a href="https://youtube.com/watch?v=A7LRlPPOHGg">watch</a> || <a href="/videos/2024/08/01/Driving_Dates_August_1">transcript &amp; editable summary</a>)



</summary>
</details>
