---
title: Let's talk about 2 Trumpworld setbacks in AZ and SCOTUS....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=v3tnoUmOQbc) |
| Published | 2024/08/06 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there internet people, it's Bell again.
So today we're going to talk about the Supreme Court of the United States,
Arizona, and two major setbacks for Trump world.
First, the Supreme Court decided against intervening in a suit brought by Missouri against New York.
The goal of the long-shot effort would have been to delay the New York sentencing of
Trump in the hush money case and lift what was left of the gag order against him.
It would have been unusual for the court to allow one state to interfere in proceedings
in another state in this way, but anymore, who can be sure?
Most experts saw this outcome well in advance and felt that the request filed by Missouri
Attorney General was, well, weird.
It seemed to be a last-ditch effort to continue Trump's apparent preferred legal strategy
of delay, delay, delay. As things currently stand, Trump is scheduled to be sentenced
in New York on September 18th.
Now on to Arizona. Jenna Ellis is a former Trump campaign attorney. On Monday, she reportedly
finalized an agreement with the state of Arizona to help out with a case widely referred to
as the Arizona fake elector case. She was one of 18 initially brought in. Arizona Attorney
Attorney General Chris Mays said, I'm grateful to Ms. Ellis for her cooperation with our
investigation and prosecution.
She went on to say, her insights are invaluable and will greatly aid the state in proving
its case in court.
As I stated when the initial charges were announced, I will not allow American democracy
to be undermined.
It is far too important.
In exchange for Ellis' testimony, her charges were dismissed.
And if you recall last year, Ellis pled guilty in the Georgia case.
Trump himself is not named in the indictment, but is widely believed to be quote,
unindicted co-conspirator number one.
The testimony of Ellis will likely bolster the state's case.
May said, we feel good about the case when discussing how she feels it would ultimately
be resolved. As these cases and the D.C. case start to move forward again, it adds another
layer of risk to the Trump campaign's efforts to recast Trump in a more favorable light.
Anyway, it's just a thought, y'all have a really good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}