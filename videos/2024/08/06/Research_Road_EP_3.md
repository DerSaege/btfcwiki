---
title: Research Road EP  3
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=P4c0Ge9czVg) |
| Published | 2024/08/06 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Dana.
Today you're at Research Road and it's the 6th of August 2024.
This will be a weekly series, not unlike The Road's Not Taken,
but we'll cover science and related news.
We will definitely start sneaking in some sci-fi stuff.
But today we're starting off with tech news.
A study led by Washington State University researchers
suggests that use of the term artificial intelligence
product descriptions lowers emotional trust among people, which then lowers purchase intention.
Oh wait, that's not good for me, is it? Just as a friendly reminder, this text is researched,
fact-checked, and prepared by a living, breathing human. I also read the comments, so let me
know if there's any topics you'd like to see added.
Now on to news about space. The chair of the U.S. National Space Council,
Vice President Harris has been trying to get other nations to sign on to the Artemis Accords.
The Accords are a set of principles for the exploration and use of space and, in particular,
the Moon.
In related news, some scientists are proposing using the Moon as a vault for DNA material
in case of calamity on Earth.
It would serve as a lunar Noah's Ark.
Venus, a planet famed for how inhospitable it is, has once again created curiosity about
the possibility of life existing on it.
New evidence of ammonia and phosphine existing in the atmosphere has led some scientists
to consider the possibility of a biological source for the chemicals that are often associated
with life on Earth.
Now it's time to talk about climate change.
A dead zone off the coast of Louisiana has grown in size.
It is currently estimated to be about 4 million acres.
A dead zone occurs when the waters run out or low on oxygen.
This is often the result of excessive fertilizer flowing into the area and feeding algae growth.
That algae then dies and depletes the area of oxygen.
Hypoxic dead zones can kill fish and other marine life.
Fossils recovered from the center of the Greenland ice sheet indicate that it is less stable
than originally believed.
The discovery is increasing concerns about sea level rise.
In the world of animals, videos of research from the 1960s that has been deemed by most
as highly unethical are being reviewed to determine whether chimpanzees can talk.
In this case, it isn't a question of whether there's basic communication, but the question
of whether they can actually say basic words.
Research circulated online showing a tourist being pinned to the ground by a bison at Yellowstone.
Please do not approach the fluffy cows.
In health news, the World Health Organization is urging governments to engage in more research
to prepare for the next pandemic.
The one thing to remember about pandemics is that there will always be another one.
Now let's dig up the past a little.
have uncovered more than sixty ancient Egyptian tombs. Unlike many tombs in Egypt, they were
undisturbed and contained a large collection of bronze coins, pottery, and gold.
A tiny 520 million-year-old fossilized creature was examined using an advanced X-ray system.
The specimen was viewed in such detail that researchers were able to pick out different
parts of its anatomy. It's a detailed view of an early ancestor of modern creatures like
insects and crabs.
And finally, on to oddities. The blade of a wind turbine off the shore on Nantucket
broke. Debris from the blade washed ashore and is being cleaned up. It prompted conversations
on social media that attempted to cast wind energy as dirty. People should feel free to
compare the debris clean up to the impact of an oil spill.
On this date in 2020, Star Trek Lower Decks premiered.
The animated series views the universe through the eyes of the lower-ranking members of a
ship and is the first comedy in the franchise.
This week's quote is from Martin Luther King, Jr.
Our scientific power has outrun our spiritual power.
We have guided missiles and misguided men.
So that's all the data we have to date.
Hope you enjoyed it and hope to see you here again soon at Research Road.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}