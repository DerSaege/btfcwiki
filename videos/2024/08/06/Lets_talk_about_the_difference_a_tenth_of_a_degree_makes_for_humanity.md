---
title: Let's talk about the difference a tenth of a degree makes for humanity....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=kQgV395a134) |
| Published | 2024/08/06 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
It's Bell again.
So today, we're going to talk about a climate study
and the difference a tenth of a degree makes over the long term.
A benchmark in an effort to offset the global issues caused
by temperature increase related to climate
is the Paris Agreement.
The agreement sets the goal of holding
the increase in temperature to 1.5 degrees
Celsius above the pre-industrial levels or below.
A secondary threshold is holding it to 2 degrees Celsius or below.
A new study sought to quantify the risks of overshooting
these thresholds and the risk of triggering tipping points.
Tipping points are the changes in the Earth's climate
that once set into motion may not
be possible to actually reverse.
The tipping point causes another event
that feeds into another and then another with each event altering the climate further.
The major tipping points are the ones you've heard about for years, the ones those silly
experts have been saying are necessary for humanity to stay, things like the Amazon,
the ice sheets, and so on.
The study found that these features of the Earth are necessary for human survival and
And that if one falls, it leads to the next falling.
The risk of this chain of events being triggered increases with every tenth of a degree above
the Paris goals.
One of the more notable lines in the study is following current policies, this century
would commit to a 45% tipping risk by 2300, even if temperatures are brought back to below
1.5.
a 45% chance of reaching a point of no return, and whether he reached that is determined
by the decisions made by those in power today, not 100 years from now.
This channel is viewed by people all over the world.
Those of us in countries that set international trends must take the responsibility seriously.
Humanity is counting on us.
has to be part of everybody's decision-making when choosing who to put
empower. Anyway, it's just a thought. Y'all have a good night.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}