---
title: Let's talk about Walz, Harris, and Trump's moves....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=AmzdnIs7FrE) |
| Published | 2024/08/06 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there internet people, it's Bell again.
So today, we're gonna talk about Vice President Harris
and her running mate, because she announced her selection.
It's Minnesota Governor Tim Walz.
We'll talk about his background, his image,
and how Trump may try to counter him.
Okay, so who is he beyond the governor of Minnesota?
He was in the US House of Representatives
from 2007 to 2019.
Before he ran for Congress, he was a teacher and coached football.
Along the way, he did 24 years enlisted in the National Guard, becoming a command sergeant
major.
He reportedly grew up working on a family farm and did some blue-collar work before
becoming a teacher.
Walls has been giving off some dad vibes, and something the campaign is likely going
to lean into.
There's already talk on social media about him seeming like Harris' dad, which is funny
because he's like 5 minutes older than her.
But the appearance, it's going to help the campaign.
It also helps the campaign in another way.
Walz is being portrayed as an older dad even though he's 60 and Harris is 59.
It draws attention to another issue for Trump.
Trump, at 78, is literally old enough to be their dad.
In age, it's become an issue in the 2024 race.
Walls helps Harris shore up different demographics and helps maintain the Blue Wall in the area
around the Great Lakes.
He also does this without upsetting the coalition of political viewpoints that make up likely
and unlikely Democratic voters.
has the standard lines of attack on walls, but since Republican positions are dated and haven't
kept up with the changing attitudes of American positions, they may not resonate. Walls is in
favor of reproductive rights and family planning. This would normally be a line of attack for the
Republican party, but the GOP probably doesn't want to draw attention to the fact that they upset
well over half the country, and stripped away the rights of millions.
As governor, Walls was in favor of universal background checks and red flag laws.
These are moderate Republican positions today, and in the gun control debate, and probably
not worth the ad-buy for the GOP.
Walls has been a pretty decent ally for the LGBTQIA community, but the GOP focusing on
this issue will just make them seem, well, weird. So what's the line of attack
Trump is likely to go with? Support for law enforcement. Walls was governor in
2020 after Floyd. He convened a special session of the legislature to try
to advance police reform. When the reforms didn't pass, he ordered them back
for a second special session. The subtext was that he would keep ordering
them back until something was done. Now, while being soft on crime is a line of
rhetoric that the Trump campaign might try to seize, is it one that will really
resonate? Probably not. In fact, it might energize democratic voters. It doesn't
seem the Trump campaign understands that, as they continue to advocate for
expended immunity for officers. Regardless of the moves that the Trump
campaign might make, the 2024 presidential race is now fully
underway. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}