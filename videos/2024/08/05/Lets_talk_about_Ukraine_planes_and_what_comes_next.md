---
title: Let's talk about Ukraine, planes, and what comes next....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=m1JDBNHxJQ0) |
| Published | 2024/08/05 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well howdy there internet people, it's Bell again.
So today we're going to talk about Ukraine, planes, and what's likely to happen now that
they have some of the ones they've been waiting for.
Then we're going to go through a little bit of totally unrelated news at the end.
Ukraine has officially taken possession of a currently unknown number of F-16s.
The iconic aircraft is a core piece of how the United States maintains air superiority
in conflicts.
These particular F-16s came from countries in Europe, and more will slowly arrive as
time goes on.
Some of the F-16s are already airborne over Ukraine.
President Zelensky said,
We made possible what was our ambition, our defense need, and now.
It's a reality in our sky.
F-16s in Ukraine. For quite some time Ukraine has been lobbying trying to get
more advanced aircraft to run up against the Russian aircraft overhead. It's also
being reported that soon Ukraine will have some Mirage 2000s. The F-16s have
been presented as a wonder weapon by some commentators, but it's unlikely
they'll turn the tide of the war on their own. They will instead add
increase deterrence to Russian aircraft and perhaps provide another method of
hitting Russian resources further from the lines, especially if the US follows
through with the ideas of providing advanced munitions for the plane. The F-16
isn't a wonder weapon, but it is a workhorse and it can be used in a number
of roles to help add an additional layer of protection to Ukraine's umbrella. It's
It's important to manage expectations and understand some of these may be lost because
Ukraine doesn't have the entirety of the U.S. net that allows it to own the skies.
But the addition of the aircraft providing Ukrainian pilots an edge they didn't have
last month or at any point in the war.
Now in some totally unrelated news, a few messages have come in asking about whether
I, a mere horse mom, can talk about some of these subjects. Y'all know me as a
horse mom and a nurse. I understand that. One of the core values of this channel
is that ideas stand and fall on their own, not based on a perceived authority
of the presenter. And I'm not going to stand here and give you my entire CV or
read my DD 214, but rest assured I have one. Two actually. My original one from
enlisted days, and then another one when I left after serving as an officer. I've
worn pretty much every hat the Air Force has had to offer, and not only as a nurse.
I like my cowboy hat better, but that doesn't mean I forgot what I learned. So
as I said on the other channel, I've done things. I know stuff. At least a little
bit. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}