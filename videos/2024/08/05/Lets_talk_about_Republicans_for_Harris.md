---
title: Let's talk about Republicans for Harris....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=F_S57UTs6Q8) |
| Published | 2024/08/05 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people, it's Bell again.
So today, we're going to talk about Republicans endorsing Harris and a campaign within a campaign.
Republicans for Harris launched with a list of Republicans who have publicly endorsed
Vice President Harris.
The same campaign will focus on Republican outreach and was described as a grassroot
organizing program to further outreach efforts to the millions of Republican voters who continue
to reject the chaos, division, and violence of Donald Trump and his Project 2025 agenda.
Many on the list of endorsements publicly provided their reasoning for crossing party
lines.
Former Trump Press Secretary Stephanie Grisham said, �I might not agree with Vice President
Kamala Harris on everything, but I know that she will fight for our freedom, protect our
democracy, and represent America with honor.�
Olivia Troy, a former Homeland Security and Counter-Terror Advisor, said,
As a national security officer and lifelong Republican, I witnessed firsthand the threat
Donald Trump posed to our country while working in the Trump White House.
The stakes are too high to let partisanship jeopardize our freedoms and the Constitution.
Adam Kinzinger shared the list of Republican endorsements featuring his name and added
there's nothing conservative about Donald Trump. Conservatives believe in
the Constitution, not a man's ego, endorsing American democracy in the
future today, leaving the past in the dust. Former Lieutenant Governor of
Georgia Jeff Duncan said, millions of Americans are fed up with his
grievance-filled campaign focused only on himself. Tonight we heard a
particularly unhinged angry version of the same Donald Trump that Georgia
rejected in 2020. He went on to say, elections are binary choices. As a
lifelong conservative Republican, it was not an easy decision for me to endorse
Vice President Harris, but I know that she fights for all Americans, right, left, or
center, and will stand up for the Constitution. The interesting thing
about the mini campaign is that it seems to be developing a ground game. In North
Carolina, for example, operations will be spearheaded by Bob Orr, who was on the
North Carolina Supreme Court. This doesn't look like just a media campaign
or a social media listing. It looks like they plan on doing real outreach. The
list of Republicans include former Republican governors, secretaries,
congresspeople, and so on. It's not an insignificant number of people and won't
be easy to ignore. In scrolling their stated reason, there's one unifying theme.
They're putting their country and the American people over their party. Anyway,
It's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}