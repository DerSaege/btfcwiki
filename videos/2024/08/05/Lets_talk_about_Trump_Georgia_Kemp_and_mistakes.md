---
title: Let's talk about Trump, Georgia, Kemp, and mistakes....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=5bqIsnnZwvY) |
| Published | 2024/08/05 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there internet people, it's Bell again.
So today, we're going to talk about Trump, Georgia rallies and realities.
Trump held a rally in Georgia over the weekend.
He drew an energetic crowd to an important swing state, but
chose to continue with attacks against Georgia Republicans.
Trump has long held a grudge against Georgia Republicans who wouldn't help him
votes after he lost the 2020 election. Georgia Governor Brian Kemp and
Georgia Secretary of State Raffensperger were Trump's main targets. In a social
media post the same day as the rally, Trump attacked Governor Kemp as weak on
crime and said that Raffensperger has to do his job and make sure this election
is not stolen. The election was not stolen. Trump lost and seems incapable of
accepting that. I don't agree with Georgia Republicans on much, but when
push came to shove, they supported the Constitution and the will of the
American people over the will of a man who was desperately trying to cling to
power after he lost. During this rally, Trump said, Atlanta is like a killing
field and your governor ought to get off his rear and do something about it.
it, and followed that with an attack on Raffensperger, accusing him baselessly of doing everything
possible to make 2024 difficult for Republicans to win.
Kemp later responded on social media by saying he was, quote, not engaging in petty personal
insults, attacking fellow Republicans, or dwelling on the past.
You should do the same, Mr. President, and leave my family out of it.
Oh yeah, there's that part too.
Trump is also very upset because Kemp's wife wouldn't endorse him, and he brought that
up in his list of grievances as well.
Raffensperger responded by saying,
Georgia's elections are secure.
The winner here in November will reflect the will of the people.
History has taught us this type of message doesn't sell well here in Georgia, sir.
Georgia conservative commentators weren't happy about Trump's statements either.
Eric Erickson said, Donald Trump is really trying to build unity in Georgia by attacking
the sitting Republican governor, whose ground game he will need to win, and also that governor's
wife?
FFS.
He can't help himself, and if he loses, it will be because of this stuff, not a stolen
election.
Trump apparently already forgot what happened in 2022, when he expended massive amounts
of political capital and influence in an attempt to oust Raffensperger and Kemp.
Kemp won in a landslide, so did Raffensperger.
He beat his nearest primary opponent by 20 or so points.
Maybe Trump should remember that Georgia just isn't that into him.
The two people he's attacking are the governor and secretary of the state of Georgia.
While he is the former president who lost Georgia and called them begging for help.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}