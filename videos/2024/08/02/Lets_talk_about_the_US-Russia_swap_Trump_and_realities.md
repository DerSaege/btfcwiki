---
title: Let's talk about the US-Russia swap, Trump, and realities....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=px00nvLovqU) |
| Published | 2024/08/02 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people.
It's Miss Bo again.
So today we're going to talk about Russia, the US, exchanges, Trump, Biden,
and the realities of actual foreign policy work.
The big international news today is, of course,
the exchange that is reportedly underway between the United States and Russia.
National coverage is focused on two of the Americans coming home
because of the massive political implications.
But the rumor mill says that if all parts of this multi-part deal go according to plan,
this will be the largest prisoner swap brokered between D.C. and Moscow since the end of the
Cold War.
Turkish intelligence is reported to have played a role as a mediator, and at time of recording,
the deal is said to include more than 20 people in at least seven countries, which are believed
to be the US, Russia, Belarus, Poland, Germany, Norway, and Slovenia. It is a
product of months of dedication and most importantly quiet behind-the-scenes
negotiation. Coverage in the US is primarily focused on Wall Street Journal
reporter Evan Gorshkovich and former Marine Paul Whelan, both of whom have
been sentenced to 16 years by Russia. The deal should clearly demonstrate the
complicated nature of that international poker game where everybody is cheating.
The overriding theme for months in the sensational news coverage has been that
the US and Russia are on the verge of starting a war at any moment. The
complicated and multi-part deal suggests conversations between diplomats at a
very high level have been proceeding even while tensions might be elevated.
The willingness of Russia to participate is probably an attempt to send a signal
to the US that Russia can be negotiated with in good faith in hopes that might
encourage negotiations about other issues. For the Biden administration, the
successful completion of the deal will be a foreign policy legacy, a centerpiece.
For the diplomats and professionals behind the deal, it will be a career
highlight and the subject of future books. It cannot be stressed enough how
long a deal like this takes to put together. The ups and downs of the
negotiations would have lasted months without a word leaking to the public.
During sensitive operations and negotiations, failures are known.
Successes are not. Given that the public and the media was in the dark until
prisoners started moving, it appears this will likely be successful. Politically,
in the US, this carries massive implications. Trump attempted to make
Gershkovich, who was arrested during the Biden term, a political talking
point recently. Trump said the reporter would be released if he was elected,
saying Vladimir Putin, the president of Russia, will do it for me but not for
anyone else. Given the length of time negotiations like this take and how long
it takes to finalize details once the negotiations are complete, it is incredibly likely that
when Trump said, quote, Biden will never get him out, his release had already been agreed
to.
Adding to potential embarrassment for Trump, former Marine Paul Whelan is also reported
to be part of the exchange.
Whelan was snatched by Russian security services in 2018 during Trump's presidency, and Trump
Trump failed to secure the release.
It adds to a list of foreign policy entanglements for the Trump administration that the Biden
administration is cleaning up, while Trump tries to cast himself strong on foreign policy
for purposes of campaigning.
Almost immediately after news broke, Trump began criticizing the deal, saying, our negotiators
are always an embarrassment to us.
He also asked, so when are they going to release the details?
It should be remembered that Whalen was in Russian hands during Trump's presidency,
and Trump in fact did not work out some magical deal to bring him home, much less work out
a deal the size and scope of this one underway.
Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}