---
title: Let's talk about Harris and the virtual roll call....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=tW60wiGFLZs) |
| Published | 2024/08/02 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people. It's Ms. Bo again.
So today, we're going to talk about Harris virtual roll calls, formalities, and timelines.
The virtual roll call to formally nominate Harris as the Democratic presidential candidate is underway.
It will conclude on Monday.
The DNC stated that it will share the complete results of the vote once it's completed.
But many are hoping for an update when Harris crosses the number of delegates needed to
clinch the nomination, which should happen very quickly.
It might already happen by the time y'all watch this video.
While many are hoping for the announcement when she crosses the line, the DNC has not
yet said if they will provide that information.
It's possible that the Democratic establishment wants to wait and hope for the unanimous vote
will signal the unity the party's hoping to establish behind the candidate. With
the rapid and unusual chain of events that led to Harris being in this
position, it's important politically for the party to be united behind their
candidate so they can continue the momentum they've built since Harris
became the potential nominee. The outcome of the vote, it really isn't in question.
According to the Democratic establishment potential candidates needed
300 delegates to sign a petition of support with no more than 50 of those
delegates being from any one state. Those wanting to qualify needed to have that
pulled together by 6 p.m. on Tuesday. To clinch the nomination Harris needs the
support of 1,976 delegates. About twice that number have already indicated or
pledged their support. This process is very much a formality and it
accomplishes the democratic goal of going into the August 19th convention
united behind a single candidate. Harris is expected to have already named her
vice presidential candidate prior to August 7th, so definitely before the
start of the convention. Her campaign is currently sending out mixed signals
about who that pick might be. Present rumors, which at this point we have to
believe are authorized leaks, put Senator Mark Kelly back in the running, a person
who was eliminated by the last set of rumors. The speculation continues. With
still three months to the election, the outcome is anything but certain, but a
series of missteps by the Republican Party occurring at the same time as
renewed enthusiasm on the side of the Democratic Party, has cautiously raised
hopes seeing the end of Trumpism in November. Anyway, it's just a thought.
Y'all have a good day.
Have a good day. Bye.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}