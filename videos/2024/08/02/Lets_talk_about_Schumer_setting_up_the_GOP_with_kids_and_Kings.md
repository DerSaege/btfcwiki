---
title: Let's talk about Schumer setting up the GOP with kids and Kings....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=nSAEqhmm2ok) |
| Published | 2024/08/02 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well howdy there internet people, it's Miss Bo again.
So today we're going to talk about the US Senate.
Schumer setting up key votes, kings, and kids.
Senate Majority Leader Schumer is pushing Republicans with key votes,
both of which will factor into national campaigns and
showcase rhetoric versus policy in DC.
The first was about the expanded child tax credit.
bill would have helped multi-child families the most. Schumer described it
as quote, a no-brainer, but Republicans in the Senate felt otherwise, shooting it
down. As Republicans complain nationally about childless cat ladies, women across
the country will be looking at Republicans in the Senate and seeing
they did not match policy to their rhetoric. Economics often factors into
family planning decisions. And if Republicans were serious about making
families a priority, we'd be having conversations about wages, maternal leave
and care, and so on. If Republicans can't come across in support of a child tax
credit, which doesn't even meet the bare minimum bar, all of their defense,
advance, and their recent rhetoric will likely be seen as empty. Schumer put
Republicans in a tough spot with this vote. The second vote Schumer's putting
together is over the No Kings Act, which is an attempt to use congressional
authority to invalidate the Supreme Court's recent presidential immunity
ruling. It would declare that the president is not immune and that
Congress, not the Supreme Court, determines who federal law applies to.
Schumer said that, quote, legislation would be the fastest, most efficient
method to correct the grave precedent the Trump ruling presented. The
The alternative is a constitutional amendment that is unlikely to gain support from the
more authoritarian members of Congress.
And even if it was successful, it would take years to bring about.
Giving rising concerns about the stability of democracy in the U.S., this vote will also
be closely watched by large segments of Americans.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}