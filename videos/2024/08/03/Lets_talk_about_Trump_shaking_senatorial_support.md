---
title: Let's talk about Trump shaking senatorial support....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Oj0nA0FXfvg) |
| Published | 2024/08/03 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there, internet people. It's Bell again.
So today, we're going to talk about how and why Trump's antics are starting to shake the support of some of those
in the Senate,
and how that's causing cracks in the wall of universal support Trump has so far enjoyed from his base.
There are two recent things making senators nervous.
The first is Trump's erratic foreign policy statements.
with the prisoner swap showed the value of quiet and discreet diplomacy as the
Biden administration brought home Paul Whelan, an American left in Russian hands
by the Trump administration. Trump's public statements have called the US
support of NATO, Ukraine, and Taiwan into question. Senator Mitch McConnell
recently highlighted how US allies watch developments all over the world to see
what to expect and to see whether the US can be trusted by saying, we don't know
yet who's going to be the new administration, but it's pretty clear
that our allies in Asia and now you can add the Philippines to the group are all
concerned about Chinese aggressions. They're watching what happens to Russia
and Ukraine carefully. He's pointing out that a lot of allies in Asia are
concerned with China's moves and will weigh the importance of the U.S. not by
the social media rhetoric of its leader but by its actions and he's right. If the
U.S. isn't reliable with regards to NATO, how can it be expected to be reliable
to less formalized allies? Trump's selection of Vance as VP only heightened
these concerns because Vance has had some less than supportive rhetoric. Then
When you add Trump's meeting with Hungary's Orban, McConnell said he's the one member
of NATO who's essentially turned the country over to the Chinese and the Russians, and
went on to say he's the most recent problem.
And while policy concerns are being discussed by McConnell and other senators, there's
another concern about Trump gnawing at them that is probably more motivating.
politicians truly want what almost every other politician wants. Another term.
Trump's rhetoric isn't resonating with voters the way it did in 2016. There are
serious concerns on Capitol Hill about whether Trump's insults will drag their
own re-election chances down. Senators who have been incredibly reluctant to
criticize Trump have talked about it. Even Lindsey Graham, when asked if he
Trump's recent comments about Harris were appropriate, he said, I don't think so,
adding, she's always embraced her heritage proudly, as she should. Senator
Wicker of Mississippi said, I don't think it was helpful. Senator Murkowski of
Alaska, who is more likely to break with Trump, said, if you're running for
president or if you want to be CEO of a company, a campaign built on insults of
individual? We should be so far beyond that. It should not be about what nasty
name you call somebody. Trump's statements and actions seem to be
shaking senatorial support in a number of ways that are leading to weakening
resolve about his candidacy. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}