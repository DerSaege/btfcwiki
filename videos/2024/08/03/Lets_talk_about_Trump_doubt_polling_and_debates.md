---
title: Let's talk about Trump, doubt, polling, and debates...
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=DL1shtCW3Tc) |
| Published | 2024/08/03 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there internet people, it's Bell again.
So today we're going to talk about Trump debates, doubt, and polls.
Trump is once again trying to create doubt about whether he'll show up to the September
debate with Vice President Harris.
But now it's for a different reason.
His first excuse was that Harris wasn't officially the nominee yet.
Well that's gone, she's got the delegates.
Now Trump is saying quote, if I didn't do the debate, they'd say, oh, Trump's
not doing the debate. Well yeah, if he doesn't do the debate, people will say he
didn't do the debate. That's the kind of brilliant insight Trump is known for. He
went on and that's when it got, well, a little weird. I mean, right now I say, why
should I do a debate? I'm leading in the polls and everybody knows her, everybody
knows me. First, the purpose of a debate isn't supposed to be self-serving and be
all about whether he thinks he can win without it. It's supposed to be about
giving the American people the choice and presenting policies and visions.
Second, he's leading in the polls? Is he though? We could go through and pick and
choose polls and then debate whether each poll is a good poll or we could
just use a polling average. 538's national polling average has Harris up
by one and a half points at the time of recording. Almost all of the recent
polling in average shows Harris leading and one of the polls gives her a five
point lead. All of this has to be taken with the reminder that Trump has
previously underperformed a lot of polling, but even if you take away the
belief that the polling is skewing in the favor of Republicans, he's still
generally trailing, and certainly isn't ahead by enough to think that forgoing a
debate is an option. Maybe Trump isn't aware of the polling, or maybe he's just
refusing to believe it, or perhaps he's just being Trump and saying anything he
can to get himself back in the spotlight. Either way, I'm pretty sure he'll show up
for the debate, unless he's just giving up hope of winning. Anyway, it's just a thought. Y'all have a good night.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}