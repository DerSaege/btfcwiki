---
title: Let's talk about the Sahm rule, jobs, and economics....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=wkf5thpiy7Y) |
| Published | 2024/08/03 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there internet people, it's Bell again.
So today, we're going to talk about unemployment, economics, rules, and where we're at.
All over social media right now, we're seeing talk about unemployment rates and references
to something known as the SOM rule.
So first, let's go over the information that started the discussion.
The U.S. unemployment rate is now at 4.3%.
Okay, so if you're familiar with basic economic ideals under a capitalist system, you're probably
shrugging your shoulders and wondering why we're talking about it.
4.3% is definitely within the 3-5% ideal that economists like.
The discussion was sparked because of something called the Psalm Rule.
It uses a moving average over a three-month period to measure unemployment and compare
it to earlier data to watch for an increase over time.
an early warning system to recession. One of the key benefits of using the metric like
this instead of a simple glance at the monthly number is avoiding overreacting to a single
data point, an element that might have been forgotten judging by current reactions. The
Psalm Rule has been triggered and social media is full of takes about what it means. After
talking to people familiar with the history of the rule, some of these takes are pretty
amusing. My personal favorites are those talking about it as if it's an ancient
rule and described what the economist behind it would have thought. They
confidently state how he would have reacted and what today's readings would
have meant to him. Okay, well she is still alive and given interviews. Her name is
Claudia Assam, a former Fed economist. Under the rule, we would be in a
recession by October. She said, I expect the unemployment rate to drift up
further this year, but it's unlikely that come September or October that the U.S.
economy is in a recession. Her appearance seems to be one of conveying calm and
reminding people of the numerous policy tools available. She told Fortune, no one
should be in a panic mode today, though it appears that some might be. If you're
going to cite a rule as authoritative, maybe listen to her when she says this
time really could be different, and then provides a list of reasons the U.S. is
currently in a unique situation. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}