---
title: Let's talk about Trump and GOP criticism about the swap....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=NEJYBE6ljNc) |
| Published | 2024/08/07 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well howdy there internet people, it's Bell again. So today we're going to talk about
Republican criticism of the swap with Russia and talk about how political rhetoric designed to
make some politicians sound tough rarely matches up with reality when it comes to foreign policy.
Trump is currently pretending that he could have done better. Of course that's after he
pretended nobody could get them home except for him. Both claims are wrong. Many public
GOP members have tried saying that the deal will encourage Russia to take more people.
Roger Carson, who is a retired special forces lieutenant colonel and the current special
presidential envoy for hostage affairs said, the funny thing is it's just not proving true.
I mean there's a time when I had 54 cases. I'm now down to just over 20. So we've made hard
changes. We've traded some bad people to get good people, innocent people back."
And he went on to say, so the math proves that assertion could be wrong. When we
make these hard decisions and the president makes that tough call to send
someone back in a trade like this, our numbers are actually going down. I know, I
know, partisan Republicans will forget about his experience and expertise and
and just say, silly Biden appointee, just backing his guy, except he's not Biden appointee.
He was appointed by Trump in 2020.
The West got back 16 people in the exchange for 8 heading to Russia.
The next GOP talking point will undoubtedly be that the Russians were spies, and they'll
be put back in play.
Yes, some of the Russian prisoners were spies.
To all the espionage understanders out there, how easy do you think it is to work abroad
when Western intelligence has your face, prints, DNA, and all other biometrics?
This was the same concern they expressed about Victor Bout, who is now a politician in Russia.
Trump congratulated Putin on the deal.
Just let that marinate for a moment as you think about Mr. Art of the Deal and his understanding
of these events.
Anyway, it's just a thought.
Y'all have a good day!
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}