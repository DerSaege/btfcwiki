---
title: Let's talk about another Arizona AG win and Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=7nvZ189D1XU) |
| Published | 2024/08/07 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there Internet people, it's Bell again.
So today we're going to talk about Arizona and yet another development concerning the failed attempt to keep Trump in
power
after the last election.
The Attorney General of Arizona, Chris Mays,
recently secured the cooperation of former Trump campaign attorney Jenna Ellis in exchange for dismissing charges
against her.  It was seen as an obvious win for the Attorney General.
Immediately on the heels of that, it looks like the AG's office has yet another reason
to celebrate.
Now, it's being reported that one of the fake electors will enter a guilty plea.
Lorraine Pellegrino is one of 11 Arizona Republicans who are alleged to have posed as an elector.
She reportedly signed a document falsely claiming that Trump had won.
Pellegrino will reportedly enter a guilty plea, or perhaps already has entered a guilty
to one misdemeanor count of filing a false instrument. It seems as though all
the other charges against her will be dismissed. Pellegrino's attorney said,
she has taken full responsibility for her actions, demonstrating her
commitment to upholding the law and contributing positively to the
community. The sentence of unsupervised probation with community service
acknowledges her remorse and willingness to make amends. It appears that
she'll receive three years of probation. The Attorney General's Office declined a
comment on the developments on Tuesday. The fake electors scheme in various
states have led to multiple charges and each case is slowly moving forward. On
top of Arizona, there are known cases in Georgia, Michigan, Nevada, and Wisconsin
all related to fake electors. Trump's team was relatively successful in
in delaying the various legal entanglements the former president and his allies faced.
Now with less than 100 days before the election, several of those cases are coming back to life
or starting to gain momentum. As the country considers the candidate in the 2024 presidential
election, the consistent reminders of the chaos of Trump's last term and the news of the ongoing
efforts to deal with the aftermath are probably unwelcomed developments for the Trump campaign.
Even though Trump himself isn't a defendant in some of the cases, Trump's circle has to wonder
whether independent and even many Republicans will risk allowing him back in the White House.
The memory of the American voter may prove a crucial factor in the 2024 race.
Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}