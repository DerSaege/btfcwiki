---
title: Let's talk about Trump being Trump in Washington state....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Kv9TJIbLKgQ) |
| Published | 2024/08/07 |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
Well, howdy there internet people, it's Bell again. So today we're going to talk about Trump, Washington State,
endorsements, and classic Trump.
Trump injected himself into the Republican primary election in Washington State with his endorsements in the 4th
congressional district primary.  Yes, endorsements.
Plural. The Republican incumbent in that district is Dan Newhouse.
He's one of those Republicans who put his country and conscience over party and
voted to impeach Trump. Those looking for rhyme or reason in Trump's actions other
than the obvious are trying to attribute the multiple endorsements to the state's
relatively unique process where the top two candidates regardless of party
advance implying or openly stating that Trump wanted to block Newhouse
completely out of the running. It's all a grand strategy by a very
stable genius. Okay, sure, it's 4D chess in Washington state, but then why did he
do it in Missouri where he endorsed the top three highest polling Republican
candidates for governor? Why did he do it in Arizona? They don't have that primary
process there. If you're not ignoring the overwhelming evidence that's staring you
in the face, it's clear that Trump isn't engaged in playing 4D chess or even
playing a checkers tutorial. It looks like he's letting other people play
checkers and saying that whoever wins won because he rooted for them. It seems
pretty obvious that he's terrified of it being reported that his endorsement
isn't enough to sway a primary race anymore. That reporting would undermine
his primary method of controlling Republican candidates and lead to more
and more Republican candidates breaking with Trump on a more frequent basis.
So to avoid the embarrassing reporting, he makes multiple endorsements in hopes that
one of them comes out on top, and he can claim victory.
Trump was probably aware of how his sway had diminished when he couldn't oust Ralphensberger
in Georgia.
It's the little thing that he's probably hoping nobody notices.
The funny part is that the more he makes multiple endorsements, the less weight his endorsement
carries, and the faster he loses command over the Republican candidate. The only
question is whether Republicans catch on before the general election in November.
Anyway, it's just a thought y'all, y'all have a good night.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}