---
title: Let's talk about Trump being booted off the Forbes list....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=54vTQU1bMbw) |
| Published | 2023/10/05 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump has fallen off the Forbes 400 list of wealthiest people in the country, being $300 million short.
- His financial losses are mainly attributed to Truth Social and the diminishing value of his stake in the parent company.
- Despite his struggling businesses, Trump's golf courses seem to be the only aspect doing well.
- The former president's self-worth appears tied to his net worth, which is currently under scrutiny in a New York case.
- Beau predicts that Trump's erratic behavior may escalate due to these financial setbacks, potentially leading to social media outbursts.
- Trump’s increasing erratic behavior could pose challenges for his legal team and may incite his followers to take actions that could land them in trouble.
- Beau anticipates Trump's response to this situation, likely blaming it on a conspiracy.
- Beau suggests that even Republicans may reach a tipping point where they can no longer overlook Trump's erratic behavior.
- Overall, Beau points out the troubling implications of Trump's financial and behavioral decline on both his legal matters and the country.

### Quotes

- "Trump has fallen completely off the list now."
- "All of this coming together at once is creating a more and more erratic person."
- "My guess is by the time this video goes out there will already be commentary from Trump about this."
- "I feel like the former president is going to hit a point soon where his erratic behavior becomes too much for even a lot of republicans to ignore."
- "Anyway, it's just a thought."

### Oneliner

Trump's fall off the Forbes list and his erratic behavior may lead to escalating social media outbursts, posing challenges for his legal team and potentially causing trouble for the country.

### Audience

Political observers, concerned citizens

### On-the-ground actions from transcript

- Monitor Trump's erratic behavior and social media activity for potential impacts on public discourse (suggested).
- Stay informed about developments related to Trump's financial status and legal entanglements (suggested).

### Whats missing in summary

Insights into the potential wider implications of Trump's financial decline and erratic behavior on political dynamics and public discourse.

### Tags

#Trump #Forbes #Wealth #ErraticBehavior #LegalIssues #PoliticalImpact


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump and Forbes
and yet more bad news for the former president.
Trump has been very interested
and in many ways has made a lot of poor decisions
based on his desire to be on a list
the Forbes 400 list. It's a list of the wealthiest people in the country.
Trump has fallen completely off the list now. He is about 300 million dollars shy
of the amount needed to be on the list. That's probably going to bother him,
them, especially given the current proceedings in New York, because it has been believed
for a very long time that a lot of the numbers that Trump provided to Forbes were fuzzy,
for lack of a better term.
It appears that his main losses this year, a lot of it stems from Truth Social and that
whole endeavor and the parent company and all that stuff.
His stake in that company that at one point in time was valued at $730 million is now
right around a hundred million dollars. There is also less interest in his
buildings. About the only thing that seems to be going well is golf courses.
This is something that even on a good day would bother the former president a
a whole lot and cause a lot of social media activity.
This occurring at the same time as the New York case, which is also questioning his wealth,
which is outside looking in, I'm not a psychologist or anything like that, but it does seem to
me that the former president places a lot of his self-worth in his net worth.
That being called into question, at the same time as this is happening, I would imagine
that we are in for a lot of erratic behavior from Trump.
may be one of those things that causes him to lash out about other topics when
he's on his social media site. All of this coming together at once is creating
a more and more erratic person, at least what is public facing. That's not great
for his attorneys, given all of his legal entanglements, and it's really not great
for the country because when he becomes more erratic and his posts become more
economical with the truth, he tends to fire people up more and then those
people go on to do things to get themselves in trouble. So I would be
watching for this. My guess is by the time this video goes out there will
will already be commentary from Trump about this.
My guess is undoubtedly saying that it's all part of some giant plot or something.
But I feel like the former president is going to hit a point soon where his erratic behavior
becomes too much for even a lot of republicans to ignore.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}