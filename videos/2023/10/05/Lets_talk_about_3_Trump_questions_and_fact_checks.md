---
title: Let's talk about 3 Trump questions and fact checks....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Al74hCIdsNE) |
| Published | 2023/10/05 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing questions from conservatives about Trump, New York, and fact-checking.
- Explaining why Trump doesn't have to be in New York for a civil case.
- Clarifying misconceptions about the judge's valuation of Mar-a-Lago.
- Noting that Trump was not denied a jury trial; his attorneys simply didn't ask for one.
- Emphasizing the importance of fact-checking Trump's statements.
- Encouraging continued scrutiny of Trump's claims.
- Mentioning an interesting statement from Trump about Mexico paying for the wall.
- Directing viewers to his second channel for more information on related topics.

### Quotes

- "His statements mean nothing."
- "Just because something is said by Trump does not mean it's true."
- "I don't automatically assume he's lying, but I also don't assume he's telling the truth."
- "I hope that you continue to fact-check what he says."
- "Y'all have a good day."

### Oneliner

Beau addresses questions from conservatives regarding Trump, New York, and fact-checking, debunking misconceptions and encouraging ongoing scrutiny of Trump's statements.

### Audience

Fact-checkers, concerned citizens

### On-the-ground actions from transcript

- Fact-check statements made by public figures (suggested)
- Stay informed and continue questioning information presented (exemplified)

### Whats missing in summary

Insights into specific claims and actions taken by Trump

### Tags

#FactChecking #Trump #NewYork #Conservatives #CivilCase


## Transcript
Well, howdy there internet people, let's bow again.
So today we are going to talk about Trump, New York,
and fact checking, three different questions.
Three questions that have come in over and over again.
Don't worry, we're not gonna fact check
all of Trump's statements.
We don't have that kind of time.
But we are gonna go over three
because these questions keep coming in
and they are coming in and the way they're phrased
lets me know that most of them are from conservatives
who are questioning what they are hearing from the former president.
I think it is important to address those.
The first one, why are they keeping him in New York for a civil case?
There's another version of it.
The media says that he can leave.
He says he can't.
There's quite a few of these, all revolving around the idea that Trump has to be at the
New York proceedings because, you know, he said, I'm stuck here.
He didn't actually say they made him come, but he kind of led people to believe that.
A lot of people know that you don't have to show up for a civil case in New York because,
Well, they've seen Trump's many other civil cases in New York.
So why did he say, I'm stuck here?
Don't know.
But he's not.
He's not.
That's not real.
It's not true.
He's not being forced to be there.
In fact, shortly after he said that, he left the state.
It's just not true.
Another one deals with, this is my favorite version, you can't tell me you believe with
judge and say that Mar-a-Lago is worth 18 million. Even you know it's worth more than that. I also
know that the judge didn't come up with that number. That was what was on the tax papers.
Trump is saying that the judge ruled and made that determination. That's not true. It's not
trick. It's not how it went down. I've been in civil cases before. How can New
York deny Trump a jury trial? If you got a jury trial in your civil case, I'm
willing to bet you asked for one. According to the paperwork that I've
seen, and I'll be honest I didn't track this down all the way because I just
chalked it up to his attorneys making a mistake, they didn't ask for one.
That's why.
And that was something that was reiterated by the judge.
Nobody asked for one.
I think is the way he phrased it.
So the state didn't ask for one.
and his attorneys didn't ask for one, therefore it's not one. He's totally
entitled to a jury trial. Not even entitled, he has a right to one. But you
have to exercise the right. And based on what I've seen, he did not. Based on what
the judges said he did not. The only source of any evidence that says that he was denied
his right to a jury trial is him saying it. Nothing else. Look at the other two questions
that were asked. Just because something is said by Trump does not mean it's true. I
covered him for a very, very long time. His statements mean nothing. Like, I don't give them
any weight. I don't automatically assume he's lying, but I also don't assume he's telling the
truth. And if I had to guess when it comes to fact checking stuff that he said, most times he's not
telling the truth. These questions over and over again, and when the email addresses have stuff
like 1776 and Patriot and stuff like that in it, one even having the word MAGA, I have
to assume that these are from conservatives.
I hope that you continue to fact check what he says.
There was an interesting statement from him recently about how he was going to get Mexico
to pay for the wall. You might want to look into that as well. And in fact, over
on the second channel, The Roads with Bo, today's video, the video that went out
this morning about an hour ago, it actually goes into it. And if you are
concerned about the motion to dismiss in Georgia, there's information about that
over there too. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}