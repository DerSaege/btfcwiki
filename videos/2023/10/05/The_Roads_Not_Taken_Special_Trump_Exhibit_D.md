---
title: The Roads Not Taken Special Trump Exhibit D
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=frgBFCMMwPU) |
| Published | 2023/10/05 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau provides a detailed overview of recent developments related to Trump's legal cases in Georgia, New York, and federal courts.
- In Georgia, there is a motion seeking the dismissal of the entire indictment based on a technicality regarding the special prosecutor's oath of office.
- Despite the motion, legal experts find it unlikely that the indictment will be thrown out.
- Multiple Trump co-defendants in the Georgia case have been offered plea deals, with one already accepted.
- In New York, a limited gag order was issued after a Trump social media post violated court rules.
- Trump is appealing a judge's decision regarding fraud and has expressed willingness to testify.
- The DOJ accuses Trump's team of using delaying tactics in the federal documents case.
- In the D.C. election interference case, Trump's statements could lead to additional gag orders on him.
- The Supreme Court rejected John Eastman's appeal to withhold emails from Congress.
- An IRS contractor was charged with stealing Trump's tax returns, which were later leaked to the press.
- Despite legal challenges and entanglements, Trump's campaign reportedly raised over $45 million in the third quarter of 2023.
- Rudy Giuliani is suing Biden for defamation, claiming monetary damages.
- Trump admitted that his Mexico wall funding promise was baseless, revealing the lack of a legal mechanism.
- Trump's supporters seemingly accepted this admission without much reaction.

### Quotes

- "The right information will make all the difference."
- "There was no way to actually do that, I was just making that up."
- "That acknowledgement of how just absolutely ridiculous that campaign promise was."
- "It does appear that a lot of people, to include his supporters, seem to just be very accustomed to less than accurate statements from the man."
- "Trump is appealing the decision from the judge saying that he, in his circle, committed fraud."

### Oneliner

Beau provides a comprehensive update on the legal developments surrounding Trump's cases, including plea deals, gag orders, appeals, and fundraising, revealing a mix of serious implications and surprising admissions.

### Audience

Legal analysts

### On-the-ground actions from transcript

- Stay informed about the ongoing legal cases involving Trump and their implications (suggested).
- Monitor updates from reliable sources to understand the evolving legal landscape surrounding Trump (suggested).

### Whats missing in summary

Insights on potential future developments and the impact of these legal cases on Trump's political standing. 

### Tags

#Trump #LegalCases #PleaDeals #GagOrders #Fundraising


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we have a Rhodes Not Taken special Trump exhibit D.
This series of videos goes over kind of weekly recaps of news
that was either not reported, under-reported,
or just wasn't reported on the main channel.
There is one development that is probably
being reported here before you hear it in other places.
In Georgia, which is where we will start as far as the cases
go, Chishpro is seeking dismissal of the entire
Georgia indictment.
His attorney filed a motion alleging that a special
prosecutor who was brought in, Nathan Wade, didn't properly
file the oath of office.
Basically, the motion alleges that Wade filled out the paperwork, but it wasn't filed properly.
The motion says that that makes all of Wade's work void as a matter of law.
It's important to understand that Wade presented to the grand jury.
So if that argument was to be accepted, then the indictment would be thrown out.
Now, I have talked to a few lawyers about this.
Only one is really up to speed on Georgia, which is kind of super important at this point.
But all of them said the same thing.
Basically it's embarrassing for the DA, but it is incredibly doubtful that this would
lead to the entire indictment being tossed.
The words grasping at straws, stuff like that were used.
But also, it is embarrassing that this little technical thing wasn't taken care of.
The DA in that case has proposed a March 4th trial date for Trump and the others, except
for Chiefs Breaux and Powell, who are set to go on October 23rd as long as nothing changes.
There is reporting that multiple Trump co-defendants have now officially been offered plea deals
in the Georgia case.
So far, we only know about the one who has accepted.
Multiple is pretty vague.
We don't know how many were offered deals.
The DA has indicated that a former New York police commissioner would be called to testify,
get a subpoena.
The attorney for the police commissioner basically said that without immunity, they're going
to take the fifth over and over and over again.
At this point, there is no sign as to whether or not the DA is willing to offer that.
But it kind of seems likely because the commissioner wasn't in the indictment, wasn't charged
in the indictment.
So it seems like an ask of immunity is probably not out of the question.
Okay, moving on to New York.
If you missed it, a limited gag order was put in place after a Trump social media post
that was apparently made while Trump was in the courtroom that it talked about a member
of court staff.
The judge is not having that.
So, that was pretty big news.
In other news, Trump is appealing the decision from the judge saying that he, in his circle,
committed fraud.
That was kind of, that was obviously coming.
Trump also said he would testify when asked.
Now keep in mind this was not a courtroom thing, there's no obligation to do this,
but he said yes I will at the appropriate time, blah blah blah blah.
I'm going to say that's a really horrible idea.
I cannot imagine an attorney recommending, allowing, not doing everything within their
power to stop the former president from testifying.
On a list of really bad ideas, that's the worst.
He is not a person who seems like they would testify well.
It seems like something that his legal teams would want to avoid at pretty much any cost.
But Trump did say that.
Okay, now moving on to the documents case, this is the federal one.
DOJ filed saying that Trump's team was trying to delay the case by weaponizing rules when
it comes to how secret documents get used in situations like this.
I read through it.
Yeah, what they're asking is unusual.
In fact, I've never heard of anything like that.
But that argument is being made.
We'll see how that plays out.
In the D.C. election interference case, Trump's recent statements about General Milley, and
well a lot of people really, but General Milley in particular, are going to be likely to bolster
the special counsel's argument when it comes to the request for a narrow gag order
on Trump in that case.
I would imagine now that kind of the gates are open, I would imagine there's probably
going to be more gag orders put on Trump because he will likely as pressure mounts become more
and more and more erratic.
And a lot of those statements are not the kind judges want.
So I would imagine there's going to be more of them.
There will be a hearing on that in the DC case on October 16th.
Now moving on to wider ranging news from Trump world, there's a bunch of it.
The Supreme Court of the United States rejected John Eastman's appeal.
Eastman wanted to stop Congress from getting their hands on 10 emails he sent to Trump
that he said, I believe he was claiming that they were covered by executive privilege.
The Supreme Court rejected it, but also kind of moot.
From my understanding, the House committee, the Jan 6 committee, already had all of this,
so I'm not really following this one.
An IRS contractor was charged with stealing Trump's tax returns and a whole bunch of
other people.
Those returns were, we assume those returns were later leaked to the press.
Trump's tax returns wound up in the hands of the press after they were allegedly stolen
by this person.
The irony of somebody being charged with stealing government documents about Trump is not lost
on me.
Despite all of his entanglements, Trump's campaign reportedly raised more than $45 million
during the third quarter of 2023.
So there's that.
An interesting part of it is that a whole lot of this amount could be used in a primary,
like a very large portion, whereas a lot of the other candidates, the amount of money
they have to spend in the primary is pretty limited.
Rudy is reportedly suing Biden for an unspecified amount of money because Biden apparently called
him a Russian pawn during an October 2020 debate.
I guess Rudy is saying that that cost him podcast listeners and stuff like that.
So there are some monetary damages in theory.
We'll see how that plays out.
In an appearance, Trump was just kind of doing what he's been doing lately and talking about
random things.
And he went off on this little side tangent, and he said the media would, quote, say Trump
didn't get anything from Mexico.
Well, you know, there was no legal mechanism.
I said they're going to help fund this wall, but there was no legal mechanism.
How do you go to a country you say, by the way, I'm building a wall.
us a lot of money.
That acknowledgement of how just absolutely ridiculous that campaign promise was and that
talking point, you know, Mexico is going to pay for the wall and all that stuff.
This was apparently just met with shrugs by the people that he told it to, his supporters.
That was, it was surprising to me that he said this and just admitted, yeah, there was
no way to actually do that, I was just making that up.
It surprised me that he said it, and it surprised me that literally nobody cared.
It does appear that a lot of people, to include his supporters, seem to just be very accustomed
to less than accurate statements from the man.
They just kind of shrugged their shoulders.
Now there is undoubtedly going to be some more news today.
It certainly appears like it.
So I will keep you posted.
If you end up hearing about the cheeseburger motion to have the indictment dismissed over
on the main channel, that's just because a whole bunch of people sent in messages and
it concerned them.
So that you may hear again, just as a little bit of a heads up.
But that is all the information we have on this topic for right now, and the right information
will make all the difference.
Y'all have a good night.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}