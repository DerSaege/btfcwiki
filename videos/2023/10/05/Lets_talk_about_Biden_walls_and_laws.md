---
title: Let's talk about Biden, walls, and laws....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=E2mrYIymkRQ) |
| Published | 2023/10/05 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden administration used executive power to clear the way for more border wall construction in a specific area.
- Biden did not reverse course on the wall, but the appropriations from Congress in 2019 mandated the continuation.
- Two political arguments exist for why Biden delayed action on the wall: wanting to rescind appropriations and creating distance from campaign promises.
- Actions, not statements, should be considered when determining intent, such as the auctioning off of unused border wall segments by the Biden administration in August.
- The border wall expansion under Biden is concerning due to its negative impact on the environment and wildlife migration.
- There are calls for real immigration reform and addressing the root causes leading people to flee their countries.
- The focus should be on helping countries address internal issues rather than intervening in their affairs.
- The need for immigration reform is urgent, and simply building walls is not a sustainable solution.
- It's vital to understand the reasons behind migration and work towards solutions that address the root causes effectively.
- Public awareness and acknowledgment of ineffective border policies and the importance of real reform are necessary.

### Quotes

- "Cowering behind a wall is not the answer."
- "There needs to be immigration reform, real immigration reform."
- "The real answer here is to help them address the issues."
- "You need to think about what it would take, how bad things would have to be for you to give up everything that you know."
- "The easiest way to do that is to stop intervening in their countries when it comes to their votes."

### Oneliner

Biden's delayed action on the border wall raises questions about intent, pointing towards the need for real immigration reform and addressing root causes of migration.

### Audience

Policy Advocates, Activists

### On-the-ground actions from transcript

- Advocate for real immigration reform by contacting policymakers and participating in advocacy groups (implied).
- Support organizations working towards addressing the root causes of migration in countries of origin (implied).

### Whats missing in summary

The full transcript dives deeper into the political maneuvering behind Biden's actions regarding the border wall and the potential impact on migration and the environment.

### Tags

#Biden #BorderWall #ImmigrationReform #PoliticalManeuvering #RootCauses #Migration


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about Biden and laws
and walls and what happened.
We will talk about political maneuvering,
political expediency and how to sift through arguments
when you're trying to determine intent
because it's about to become important.
I would imagine by this point, most people have seen the headlines regarding Biden and
the wall because it's 2023 and we're still talking about this wall.
Okay so two variations of the headline.
One is something like Biden administration waves dozens of laws to build border wall.
Is this true?
Yeah, that's accurate, the Biden administration engaged in a massive sweeping use of executive
power, some might say too sweeping, to clear the way for additional construction of a border
wall in a specific area.
Another version of the headline is Biden reverses course, continues Trump's wall.
Is that true?
Not really. It didn't reverse course. When asked if he thought it would be effective, he said,
no. Flat out. Flat out. Just no. So why is he doing it? The appropriations from Congress,
the order from Congress to do this, came through in 2019. That's why. It wasn't that the
administration pushed for it or decided to do it. Okay, so that leaves us with a
couple of different questions because the smart move politically, if you are
an incoming president as Biden was years ago, the smart move would have been to
just do it right then because then it would have been really easy to explain
And be like, no, no, 2019, that they did the money, they directed it, don't have a choice,
rule of law, it's the former guy, all that stuff.
That would have been the smart move politically, but he didn't do that.
So you have to ask why.
And there are two political arguments as to why he would have delayed it.
The first, being charitable to the Biden administration, would be he wanted to have some time to try
to get it rescinded.
Fair enough, sound political argument.
He is genuinely opposed to it, didn't want it to go through, tried to get some time to
rescind those appropriations, to make them go away.
That's one argument.
the other, that well prior to being elected he was campaigning and while he
was campaigning he said something to the effect of come on Jack not another foot
of more of that wall, something like that. So since he campaigned on that he wants
to put some time between him saying that and continuing the wall. That's a strong
argument as well. That would be good political maneuvering. So, given that you have two different
explanations and they're in direct contradiction to each other, how do you know which one is true?
You have to move away from statements. You have to move away from something that a politician said.
whether you like Biden or not, he is in fact a politician. So you have to look
for actions rather than statements. Is there anything that would tip the scales?
There is. It happened in August. In August, which incidentally would be around the
time somebody was walking into the Oval Office and being like, hey, this 2019 appropriations thing,
we got to do something about that. In August, the Biden administration started auctioning off
millions upon millions of dollars of unused segments of border wall. They were just sitting
around construction material for what was appropriated for that kind of
project. If the federal government still had possession of it, what might happen?
It might get used, right, in the 2019 appropriations for that project. But if
they don't have it, well, the money that was appropriated by Congress, it has to
buy more. There is evidence that he tried to limit it, but that's all you've got.
Looking back, you can't really find much else. So, where are we at? The wall is
going to be expanded, yet again. The wall is bad. Beyond Biden's response of, no, it
it won't be effective. There's also the fact that it will be effective at messing
up the environment. It will be effective at stopping migration of animals. There
There are a whole bunch of reasons to oppose this wall.
This is being built.
Some believe that Biden probably could have done something about this as far as getting
the appropriations rescinded early on, early on in his administration, but that's outside
looking in.
don't know. What we know is that there are two good reasons for this timeline
to have played out the way it did. There may be other things that the Biden
administration has done recently that might, we don't know yet, but might be set
up in a way to, let's just say slow the wall.
But we don't know.
We have two sound arguments that are in direct contradiction to each other.
One saying Biden didn't want it and was trying to fight it.
The other saying Biden didn't care and just wanted to put some time between his campaign
promise, and this action. The only real tipping evidence is him auctioning off
the unused chunks. Whether that is enough for you to say one way or the other,
Well, that's up to you. But that's what you have. What happens next is important,
and I don't mean what Biden does next. There probably isn't going to be much
that Biden can do. Different agencies might be able to do something, but Biden
himself. Probably not a whole lot. But at some point, the American people are going
to have to acknowledge that cowering behind a wall is not the answer. Destroying the environment
is not the answer. There needs to be immigration reform, real immigration reform. Maybe allow
people to file for asylum before they arrive. A whole bunch of things can be
done. The most important would be to address the conditions that exist in the
areas that people are coming from. And that's not because I care if they come
here. I don't. But the reality is, despite how right-wing media likes to portray
them, these are people who are fleeing. You need to think about what it would
take, how bad things would have to be for you to give up everything that you know
know, and move to a country that is openly hostile to you a lot of the time.
Things would have to be pretty bad. The real answer here is to help them address
the issues. And in a large part, the easiest way to do that is to stop
intervening in their countries when it comes to their votes, which happens way
more than we would like to admit anyway it's just a thought y'all have a good
day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}