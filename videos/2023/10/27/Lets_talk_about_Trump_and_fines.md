---
title: Let's talk about Trump and fines....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=-F89J-OdiJA) |
| Published | 2023/10/27 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former President Trump's behavior in court was likened to a toddler throwing a tantrum, with an audible gasp from onlookers.
- He received a $10,000 fine for violating a gag order, the second time this has occurred, totaling $15,000.
- The concept of monetary fines as the only penalty raises issues, suggesting it is a license for the wealthy to misbehave.
- Beau points out the disparities in consequences for actions based on financial status.
- Trump's repeated violation of the gag order showcases his lack of intention to change his behavior.
- Beau implies that Trump's behavior may worsen as stress from ongoing proceedings increases.
- The judge may need to reassess if monetary fines alone can alter Trump's behavior, considering potential escalation in rhetoric.
- Beau hints at the possibility of more severe consequences beyond monetary penalties in the future.

### Quotes

- "He's just got to pay a little bit."
- "If the only penalty for an action is a monetary fine, well, that means it's not illegal for rich people."
- "Trump is having more and more bad news delivered to him every day."

### Oneliner

Former President Trump's repeated violation of a gag order and minimal consequences raise concerns about the efficacy of monetary fines and his escalating behavior under stress.

### Audience

Court observers

### On-the-ground actions from transcript

- Monitor legal proceedings involving influential figures (implied)
- Advocate for fair and equal consequences regardless of financial status (implied)

### Whats missing in summary

The detailed nuances of Beau's analysis and commentary on the implications of Trump's behavior and the effectiveness of current consequences.

### Tags

#Trump #LegalSystem #MonetaryFines #Consequences #Inequality #Behavior #Justice


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about Trump a little bit, but it leads us to something else.
Leads us to talking about license, fines, stuff like that.
If you missed the news, the former president did not have a good day yesterday.
According to the people who were in the courtroom, he, at one point,
slammed his hands on the table got up and huffed and stormed out of the
courtroom during proceedings
like a toddler throwing a tantrum
there was an audible gasp as it occurred
and I mean yeah that fits
but before then he got called up to the stand
and the judge hit him
with a $10,000 fine for once again violating the gag order.
This is the second time.
First time it was $5,000, now it's $10,000.
$15,000 for a whole lot of people.
That is a life-changing amount of money.
That's a big deal for a whole lot of people.
And it kind of brings us back to a hard reality.
If the only penalty for an action is a monetary fine,
well, that means it's not illegal for rich people.
It's not a fine, it's a license to do it.
they're just paying installments on the license.
That applies in a whole lot of different ways.
There are things that, due to a lack of power coupons,
a lack of cash, that are just impossible for a whole lot
of people, sometimes illegal, just existing in some places
if you don't have enough money is a crime.
But
for the former president
storming out of a courtroom,
slamming his hands on the table,
directly violating
a gag order from a judge twice,
well, he's just got to pay a little bit.
I feel like for somebody who didn't have that kind of money, they might end up a guest of
the state for a night or two.
So the other thing that this kind of shows us is that Trump has no intention of changing.
He's definitely set the example that he intends on violating this gag order.
These proceedings, they're going to go on for some time.
And Trump is having more and more bad news delivered to him every day.
His behavior is becoming more and more erratic.
statements that violate that gag order will become more and more direct. And I
feel like over time they'll probably become more and more angry. There are a
lot of people in this country who look up to that person and view him as
somebody to follow, somebody to listen to, somebody to act on their requests.
At some point the judge in this case is going to have to determine whether or not a monetary
fine alone is enough to alter his behavior. Because as the stress mounts, it seems incredibly
likely that his rhetoric becomes more and more, well let's just say problematic. We've
seen it before. The last time he faced a whole bunch of stress, a situation that he couldn't
alter the outcome of, it didn't end well. Something that the judge probably has gnawing
at the back of his mind, and I feel like it won't be long before the judge is in
a situation where he has Trump in front of him again, deciding what route to
take because the former president once again, violated that gag order.
I feel like monetary penalties will only be something that is on the table for so long.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}