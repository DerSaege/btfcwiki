---
title: Let's talk about some economic news and a message....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=vQ1uktR00Hk) |
| Published | 2023/10/27 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Receives a message criticizing his video for explaining the economy and blaming Trump, forecasting a recession caused by Biden.
- Message predicts a century's worst economic collapse under Biden, warning Beau about affording his trailer and heating bill.
- Beau received the message on October 18th, 2022, but still has lights.
- One year ago, forecasts predicted a recession with 100% certainty due to be a major blow to Biden, which did not occur.
- GDP numbers for the third quarter are out, up by 4.9%, indicating no recession.
- Beau reminds viewers that presidents don't control the economy; individual beliefs and faith in the economic system play a significant role.
- Urges people to stop listening to fear-mongering sources and recalls the failed predictions of the worst economic collapse in a century.
- Encourages viewers not to believe headlines that aim to scare them and to be cautious about alarming forecasts.

### Quotes

- "Stop listening to people who get their paycheck by scaring you."
- "100% is what they said. It didn't happen."
- "When you see headlines that are just 'Oh, unbelievable,' don't believe them."

### Oneliner

Beau debunks fear-mongering economic forecasts, reminding viewers of individual control over economic belief systems and urging skepticism towards alarming headlines.

### Audience

Viewers

### On-the-ground actions from transcript

- Stop listening to fear-mongering sources (implied)
- Be cautious about alarming forecasts (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of failed economic forecasts, urging viewers to question fear-mongering narratives and take control of their economic beliefs.

### Tags

#Economy #Forecasts #FearMongering #IndividualControl #Skepticism


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Biden
and the economy and forecasts and the GDP
and a whole bunch of different things.
And we're gonna do this because I got a message
and it's very illuminating in a bunch of different ways.
Illuminating is a pun.
Um, but I just think it's worth going over.
Bo, I was so excited today when I saw the title of your video.
I thought for once you were going to tell your viewers the truth about the economic
situation Biden has caused.
Instead, it was some video for idiots, all caps, explaining the economy and messaging,
blaming Trump, all caps.
usual for the forecast. I may not know about supply chains, but I know what 100% is. And
there's a 100% chance of a recession now. You'll never be able to talk your way out
of this. You say presidents don't control the economy, but that's wrong, all caps.
They do, all caps. When Biden, all caps, triggers the worst, all caps, economic collapse, all
caps. In a century, we'll see if you can still afford that lot payment on that raggedy trailer
you're in. Put up some drywall. You'll need it when you can't afford your heating bill,
if you're lucky enough to be able to get power with Biden's communist Green New Deal.
Mark my words, in one year, this country will be experiencing the worst all caps economy
a century, and you won't have power to make your stupid videos.
That's quite a statement. You said, mark your words, I did. I received this message on October 18th, 2022.
I still have lights, just saying.
If you don't remember, one year ago, the forecasts were everywhere saying that the
United States was headed into a recession.
One hundred percent certainty, and it was a major blow to Biden.
There's headlines all over the place like that.
100% certainty. That did not occur, obviously, but it's worth going back and
looking at those forecasts, especially forecasts that are made around election
time, because sometimes they may not really be that accurate. It's worth
noting that you got to be kidding me now I'm just messing with you okay in
totally unrelated news the GDP numbers for the third quarter are out up 4.9
percent if you're not up to speed on that kind of that's good that's good it's
definitely not a recession but I would remind everybody presidents don't
actually control the economy, you do. What you believe in, how much faith that you
have in the economic system has way more to do with it than most things that
people talk about. I would strongly suggest that those people who were
terrified about the worst economic collapse, all caps, in a hundred years
Remember this.
Take note of this.
Stop listening to people who get their paycheck by scaring you.
100% is what they said.
It didn't happen.
100% confidence, and it did not occur.
They scared you.
plain to see by the message. It's not the only time and you're not the only
person that it's happened to. When you see headlines that are just
Oh, unbelievable.
Don't believe them.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}