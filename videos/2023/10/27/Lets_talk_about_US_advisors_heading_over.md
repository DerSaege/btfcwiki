---
title: Let's talk about US advisors heading over....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=qWhK3-mjPoI) |
| Published | 2023/10/27 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The United States has sent advisors to Israel to help Israeli officials think through difficult questions and to ensure civilian protection in the crossfire.
- Lieutenant General James Glenn, a Special Operations Marine, is one of the advisors drawing attention for his involvement.
- The question arises about sending Lieutenant General Glenn, who was in Fallujah, to advise on avoiding full war.
- Fallujah was a horrific battle with immense civilian loss, and sending someone with first-hand experience like Lieutenant General Glenn may offer valuable insights.
- There is skepticism around sending an advisor with experience in intense combat situations to prevent war escalation.
- Advisors are likely trying to explain the risks of a large-scale ground offensive to the Israeli government.
- The effectiveness of these advisors in conveying their advice clearly and persuasively is critical.
- The hope is that the advisors can dissuade the Israeli government from pursuing a potentially disastrous course of action.

### Quotes

- "There is no advice that can be provided that is going to say a large-scale ground offensive into that area is going to be okay."
- "The advisors are hopefully explaining that a large-scale ground offensive is a bad idea."
- "They're not sending the general that was over Fallujah. They're sending somebody who was in Fallujah and then later became a lieutenant general."

### Oneliner

The United States sending advisors to Israel raises questions about preventing full-scale war, particularly regarding a battle-experienced advisor's role in advising against a large-scale ground offensive.

### Audience

Policy analysts, peace advocates

### On-the-ground actions from transcript

- Contact policymakers to advocate for peaceful resolutions (implied)
- Join peace organizations to support diplomatic solutions (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the implications of sending advisors to Israel and the importance of experienced advisors in conflict prevention.

### Tags

#US #Israel #MilitaryAdvisors #ConflictResolution #PeaceAdvocacy


## Transcript
Well, howdy there, Internet people.
Let's bow again.
So today, we are going to talk about some news, and we're
going to talk about a question that was
prompted by that news.
And it has come in at least a dozen times
since the news broke.
And we're just going to kind of go over what's occurring, and
then we'll shed a little bit of light on the question.
OK, so if you missed it, if you don't know what's occurring,
the United States has sent advisors to Israel.
And by advisors, this time it's actually advisors, not
advisors, wink, wink.
We'll go over what the officials are saying first.
And let's see.
We have asked several officials with relevant experience
simply to help Israeli officials think
through the difficult questions ahead and explore their options.
IDF will, as always, make its own decisions. That was the Pentagon. This is state. We give them our
best advice. It's important, as we said, not only what they do, but how they do it, particularly
when it comes to making sure that civilians are as protected as they possibly can be in this crossfire.
Okay, so, remember back to the video with the question from France.
influence. This is how they're getting that influence. They're sending advisors, they're
not expanding that coalition. This is how they're doing it.
One of the people that has drawn a lot of attention, because he's one of the advisors,
is a person named Lieutenant General James Glenn, United States Marines, Special Operations.
like we talked about in the video. Okay, so here is the question, and this came in
a bunch, and they all pretty much had this tone. How can you think they're
trying to avoid full war when they're sending the Lieutenant General over
Fallujah to advise? He's, and there's a whole bunch here about him, I'm gonna
summarize it as a monster. Please explain to me how they're sending him in the
interest of peace. Okay, so for those who don't know, before we get into this,
Fallujah was horrible. It was a battle, two battles really, and it was bad. In
in any way that fighting can be bad, from the fighting itself to the amount of
civilian loss. It was horrific. And I cannot, I honestly can't think of
something comparable to say it was like this. It was that bad. So, to the question,
please explain to me how they're sending the lieutenant general over Fallujah.
in the interest of peace. I don't know how they would be doing that because the
first thing they would have to do is bring him out of retirement. He retired
in 2010. He was Lieutenant General Stadler. Lieutenant General Glenn was
not a lieutenant general almost 20 years ago. That happened in 2004. I don't know
what he was in 04. I know in 07-ish he was a lieutenant colonel, so my guess is
he was a major, but it doesn't matter. Captain, major, lieutenant colonel, what
this means is that everything you've heard about that fighting, everything
that you've seen in still images, he saw with his own eyes. Then he went on to a
a field that specializes in a low footprint. I would imagine after seeing
that and having 20 years to process it and entering a field where the the idea
is to not do that, yeah he probably has some thoughts. He might be the perfect
person to go over there because he'll be able to say, I was a part of something
like you're planning, only smaller, and this is how bad it was. Because it's
worth remembering that Fallujah, I want to say it has a population of 250,000, more
or less, somewhere in there. So a tenth, roughly. There is no advice that can be
provided that is going to say a large-scale ground offensive into that
area is going to be okay. It doesn't exist. If they do it, it's going to be bad.
A full-scale offensive will be bad and however it is read by Tehran, however
they perceive it, that is what determines whether or not this expands
into a wider conflict. I would imagine the fact that this is a person who has experience
not just with massive urban combat operations, but also the alternative, the low footprint stuff.
I don't think that's a coincidence. Now, what occurs is up to the Israelis. It's up to the Israeli government.
The advisors are hopefully explaining that a large-scale ground offensive is a bad idea.
idea. There isn't any advice that they can provide that will make it not a bad
idea. It just doesn't exist. So hopefully they're explaining that and hopefully
they're persuasive and the Israeli government is open to to hearing the
other side of it. I don't know that they will be, but that's what's going on. So
So what Biden and the French president were kind of angling towards, it looks like it's
happening, but there's no way to know how effective it's going to be.
And there's a lot riding on the ability of these advisors to state their advice very
clearly and persuasively. So the short version as far as all of the questions,
they're not sending the general that was over Fallujah. They're sending somebody
who was in Fallujah and then later became a lieutenant general. It's not
the same guy. But I would hope that his experiences there are something that
would help him provide the kind of information that would make
them realize that maybe this isn't what they want to do. Anyway, it's just a
thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}