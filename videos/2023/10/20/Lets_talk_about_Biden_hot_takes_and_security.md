---
title: Let's talk about Biden, hot takes, and security....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Q--dqCzZbOQ) |
| Published | 2023/10/20 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The White House posted a picture of President Biden with individuals who appeared to be special operations personnel.
- Right-wing commentators blacked out faces of the individuals in the image and spread it online.
- The internet erupted with hot takes on the situation without considering the full implications.
- The focus was on operational security, but the tattoos on the individuals' arms were visible.
- Beau points out the ease of altering facial features compared to tattoos for identification.
- Rushing to provide hot takes on social media can often worsen a situation.
- The White House Instagram page gained attention due to the incident, potentially harming its reputation.
- Beau warns against spreading sensitive images online for clicks and provoking outrage.
- Mistakes in handling security-sensitive content can have serious consequences.
- Taking time to think before reacting online is emphasized to avoid escalating issues.


### Quotes

- "Hot takes are not necessarily the greatest thing that humanity has come up with."
- "A whole bunch of people in a rush to talk about how bad something was made it worse."
- "If they did it and then it was spread everywhere because of people in search of clicks and provoking outrage, they just made it worse."
- "If the White House did this, it's bad. It is."
- "Rushing to provide hot takes on social media can often worsen a situation."


### Oneliner

Beau talks about the White House, Biden, security, and internet hot takes causing potential harm through rushed actions and spreading sensitive images.


### Audience

Social media users


### On-the-ground actions from transcript

- Think before sharing sensitive or potentially damaging content online (implied)


### Whats missing in summary

Importance of careful consideration and responsible sharing on social media platforms.


### Tags

#WhiteHouse #Biden #Security #InternetCulture #SocialMedia


## Transcript
Well, howdy there, internet people, Lidsbo again.
So today we are going to talk about Biden and Instagram
and special guys and different kinds of security
and hot takes on the internet
and why they're not good.
Okay, so before I start and tell you the backstory to this,
I wanna tell you, I didn't check this at all
because it doesn't matter.
What actually occurred in the back story is kind of irrelevant to the point, but the story goes like this.
The White House posted an image of President Biden shaking hands with some people,
and these people absolutely look like they are special guys.
They are part of the special operations community based on what they are wearing,
the the relaxed grooming that they have as far as beards and hairstyles and
stuff like that. They definitely do look like that crew of people. And then
according to the story it was deleted. But don't worry, a bunch of right-wing
commentators to illustrate how bad it was to show this image, they took a
screenshot of it on the White House Instagram page, blacked out the faces, and
then spread it all over the internet.
Okay, I mean, I get it. Let's start with this. If the White House actually did
this yeah that's bad that is bad and the the people in it as it was spread around
the internet the people in it were variously identified as either sills
Delta a term I'm not gonna say or my favorite a sill ace element which I mean
And that's high speed right there.
But it was okay to spread it everywhere because the faces were blocked out.
If the White House did this, it's bad.
Some staffer who obviously did not know what they were looking at, put it on there and
then it got deleted.
That staffer probably never had appropriate training in that kind of security.
And that if it occurred, understand, yeah, the White House messed up.
Illustrating that by blacking out the faces and posting it everywhere to get that hot
take out there and show how much you know about this topic and all that stuff, that
may not have been a good idea because you didn't obscure their arms.
all those tattoos? Some staffer who knows nothing about the topic, they made a
mistake. Everybody ranting about operational security and having that at
the forefront of their mind while they were making this post, they at least knew
something about it and they didn't think to cover up the tattoos. I would point
out that altering your face at first glance, that's actually pretty easy.
Imagine me without a hat, glasses, and no beard. You probably can't even do it.
A little bit of dye, those are things that are actually easy to overcome. Getting rid
of tattoos, especially if it's somebody trying to identify the person and
and they're close enough to be using an image to compare the face, they can
probably pull up the sleeves.
Hot takes are not necessarily the greatest thing that humanity has come up with.
Take your time. Think about what you're doing. A whole bunch of people in a rush
to talk about how bad something was made it worse a lot worse because odds are
the White House Instagram page yeah that doesn't get a whole lot of traffic but
when things start getting tagged the way they were tagged a lot of people saw
that it wasn't good and it would be especially bad if it was a still ace
element because I'll tell you I know a lot of guys from that world never even
heard of an army seal before. So if the White House did this it's bad. It is. If
if they did it and then it was spread everywhere because of people in search
of clicks and provoking outrage. They just made it worse. Anyway, it's just a
With all, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}