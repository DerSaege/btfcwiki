---
title: Let's talk about Chesebro, Trump, and Jordan this morning....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=nQ7UwMzQkw0) |
| Published | 2023/10/20 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Reports suggest that ChiefsPro is in the process of entering a guilty plea with the state, including terms like probation, fine, evidence provision, and potential community service.
- The plea agreements occurring before jury selection have significant implications for Trump's defense cases and co-defendants, causing apprehension in Trump's circles.
- In New York, a judge is considering jail time for Trump, as his attorneys had not fully complied with removing social media posts related to the case.
- Jordan's attempt to become Speaker of the House faced significant opposition, with 25 Republicans voting against him, rejecting both Jordan and Trump's influence in the House.
- Unrelated to the political events, reports indicate the release of two American captives, possibly attributed to Biden's aid efforts, but further information is needed for confirmation.
- Anticipated more co-defendants in the Georgia case may take deals, with some expected to wait until closer to trial before making decisions.

### Quotes

- "Expect this to play out in the future."
- "Frankly, this is absolutely devastating to Trump's defense cases."
- "Republicans in the US House of Representatives are rejecting Jordan, Trump, his influence, and his leadership."
- "I don't think that's it. It could be, but that seems unlikely."
- "Y'all have a good day."

### Oneliner

Reports of guilty pleas in ChiefsPro case impact Trump's defense, while a judge considers jail for Trump in NY and Jordan faces House opposition; two captives released, Biden's aid possibly involved.

### Audience

Political observers, activists

### On-the-ground actions from transcript

- Monitor the developments in the legal cases mentioned and stay informed (implied)
- Support efforts for justice and accountability in political matters (implied)

### Whats missing in summary

Analysis on the potential broader implications of these legal and political events.

### Tags

#LegalCases #PoliticalDevelopments #Trump #Jordan #ChiefsPro #GuiltyPlea


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about ChiefsPro
and Trump and Jordan,
because we have a whole bunch of events
occurring simultaneously right now
that really kind of help wrap up the week's coverage.
And we'll just kind of run through the events real quick
and go from there.
Okay, so we will start with the news regarding Cheesbrough.
According to reports at time of filming,
Cheesbrough is in the process of entering a guilty plea.
It appears that he came to some agreement with the state.
The terms of that agreement, this is early, so this is early reports.
Early reports are almost never right,
but this is what it appears to be.
One felony count, five years probation,
that could be shortened to three if he's a good little boy,
$5,000 fine or restitution,
an agreement to provide evidence in the future,
the apology letter, maybe that's a standard thing in Georgia,
and there is some conflicting reports,
there are conflicting reports
when it comes to maybe community service, but even if it is there, it wasn't a lot.
And so that's it.
There won't be a trial because everybody that was going to be tried at this point in
time has decided to enter a guilty plea.
Expect this to play out in the future.
Similar scenarios.
I said that generally speaking, the agreements
have to be reached before jury selection.
This was cutting it a little close,
because my understanding is they'd already
been given the questionnaires.
But in future cases, you will see people
go through pretrial motions and try
to get their case thrown out, try to position themselves
in a better way.
And then if it doesn't work, they enter a plea.
Frankly, this is absolutely devastating
to Trump's defense cases and his co-defendants.
And it is probably sending a lot of apprehension
through Trump's circles.
OK.
So that's where that sits at the moment.
Meanwhile, in New York, a judge is basically saying, hey, attorneys that are
representing Trump, give me a reason not to throw him in jail right now.
Y'all remember the social media post that the judge ordered removed?
Well, it apparently got removed from social media, but it was still on a
website managed, I guess, by the Trump team in some way. I believe Midas Touch
were the people who caught that. And so the judge and the attorneys up there are
having a little discussion about that. The judge does not seem happy at all.
can't wait till he finds out about the other one, the other post involving the
address of the prosecutor. Okay so that's happening. My guess is we won't get a
resolution on that today. It's possible that we do but we probably won't hear
the end of that today. Now, meanwhile in DC, Jordan tried yet again to get the
vote to become the Speaker of the House. It did not go well. In fact, it went
worse. Now, 25 Republicans are voting against him. That does not seem like
something that's in the cards. And remember, Trump threw all of his weight
behind Jordan. Republicans in the US House of Representatives are not just
rejecting Jordan when they do this. They are rejecting Trump. They are rejecting
his influence and his leadership. It matters. And then one thing that is
completely unrelated to this, there are reports breaking right now that two
American captives are being released. There are people that want to credit
Biden's trip in the aid with this. Maybe, that may be true, but I would not jump to
that conclusion yet. There have been a lot of people with state, there have
been a lot of people with other agencies that have been over there working on
this. I don't necessarily think it is the aid. I don't think that's it. It could be,
but that seems unlikely. I would hold off until there is more information and the
former captives themselves, not just are they out of harm's way, but they have
been debriefed and interviewed. You'll find out a whole bunch more from them
Then you're going to find out from anything else because when they talk, they'll probably
have a pretty clear understanding of what happened and they will be able to say more
than anybody involved with State Department.
Okay, so I guess that's it.
That looks like all of the news, but yeah, I would expect more Trump co-defendants in
the Georgia case to to take deals, I would imagine that you're gonna have a
couple almost right away and then the rest are gonna kind of hold themselves
and wait until just before trial. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}