---
title: Let's talk about rhetoric, representatives, and Frankenstein....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=JB3llNKEhPQ) |
| Published | 2023/10/20 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Marionette Miller Meeks, a U.S. House representative, faced death threats and harassment after changing her vote for Speaker of the House.
- The Republican Party's inflammatory rhetoric and labeling of Americans as enemies has contributed to the normalization of threatening behavior.
- The rhetoric has escalated to the point of discussing Civil War and targeting those who don't follow the MAGA ideology.
- Constantly pushing for division and conflict within the party keeps the base on edge and drives voter turnout.
- Beau calls out the Republican Party for years of spreading lies and fueling anger through inflammatory rhetoric.
- The analogy of Frankenstein is used to illustrate that sometimes the creator, not the creation, is the true monster.

### Quotes

- "One thing I cannot stomach or support is a bully."
- "The rhetoric needs to stop."
- "Frankenstein isn't the monster in that story."
- "A wise person knows that Frankenstein is the monster."
- "Y'all have a good day."

### Oneliner

Marionette Miller Meeks faced threats, revealing how Republican rhetoric fuels division and anger, akin to Frankenstein's tale.

### Audience

American citizens

### On-the-ground actions from transcript

- Contact authorities if you witness threats or harassment (suggested)
- Read and share information to counter lies and misinformation (suggested)

### Whats missing in summary

The detailed analysis and examples provided by Beau in the full transcript are missing in this summary.

### Tags

#USPolitics #RepublicanParty #InflammatoryRhetoric #Unity #CivilWar


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are once again going to talk about the U.S.
House of Representatives, and we're going to talk about one representative in
particular because something happened and the Republican party seems
surprised that it happened.
And I'm not sure why, to be honest, so if you missed it as of yet, the
United States House of Representatives still does not have a Speaker of the
House. But one representative, see she started off supporting Jordan initially
and then she changed her mind and she voted for a different candidate. Her name
is Marionette Miller Meeks and when she changed her vote away from Jordan she
started getting calls. Since my vote, I have received credible death threats and
a barrage of threatening calls. The proper authorities have been notified and
my office is cooperating fully. It goes on. One thing I cannot stomach or support is
a bully. Someone who threatens another with bodily harm or tries to suppress
differing opinions undermines the opportunity for unity and regard for
freedom of speech. Shortly after this, becoming public, Jordan, hey, he tweets
out, no Americans should accost another for their beliefs. We condemn all
threats against our colleagues. It is imperative that we come together. Stop.
It's abhorrent. It is abhorrent. It is abhorrent. But why is it happening?
How did this start?
When did it become normalized?
You think that maybe years of rhetoric labeling other Americans as enemies of the state, enemies
of the Constitution, enemies of the people, I think that might have had something to do
with just winding up that base.
It was okay as long as it was directed outward apparently.
And then there was all the talk about Civil War, that rhetoric getting more and more and
more inflammatory.
What about when it turned to going rhino hunting?
The Republican Party has used more and more inflammatory rhetoric over the years.
Has riled up their base to just an unbelievable level.
We've seen it over and over and over again.
And now that base, it's acting on its own apparently.
They've been told for years that if it's not the MAGA way, well it's going to destroy the
country.
That rhetoric.
It leads to this.
leads to this type of stuff being normalized. The surprising part is that a
whole bunch of people apparently thought it would only be directed outward. Once
you adopt those authoritarian stances and that rhetoric, there always has to be
a bad guy, somebody to go after. And anybody who's not with you, anybody
who's not in lockstep, well they're obviously the bad guy. And when you
constantly have your political party, your pundits, your commentators saying
that it may be time for civil war. It keeps the base on edge and when there's
division within your party they're still on edge, maybe more so. The rhetoric needs
to stop. People can sit there and say you know this is horrible it should never
happen. The party of personal responsibility probably needs to take
some responsibility for this because there has been years of increasingly
inflammatory rhetoric coupled with lies. Lies about the election. Lies about I
mean almost everything and that's coupled with that rhetoric. It makes
people angry and the Republican Party wants people angry because it drives
voter turnout energizes the base.
I think there's a whole lot of people who should read Frankenstein.
A smart person, after reading that, they know that Frankenstein isn't the monster in that
story.
That's the monster that Frankenstein created.
A wise person knows that Frankenstein is the monster.
Anyway it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}