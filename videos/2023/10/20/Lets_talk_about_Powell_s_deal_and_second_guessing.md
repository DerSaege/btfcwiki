---
title: Let's talk about Powell's deal and second guessing....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=BMQ-zVvyxc8) |
| Published | 2023/10/20 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the deal Sidney Powell received, including guilty plea, cooperation, misdemeanors, probation, fine, apology, recorded statement, and testimony against co-defendants.
- Mentions the timing of the deal before jury selection for the case.
- Questions why high-profile figures often escape jail time while lower-level individuals take the fall.
- Notes Fulton County's strategy of offering deals to Trump's alleged co-conspirators in exchange for testimony against Trump.
- Points out that deals become less favorable over time from indictment to trial as prosecutors seek stronger evidence.
- Supports Fulton County DA's approach of building a case against the top rather than settling for lower-level convictions.
- Dispels doubts about the DA's strategy by citing successful convictions already secured.
- Suggests that Powell may not have been the mastermind behind larger disruptive actions and that her testimony could lead to higher convictions.
- Emphasizes the importance of testimonies in other related cases and potential future subpoenas based on provided testimony.
- Encourages refraining from second-guessing the process before trials are completed.

### Quotes

- "These are slaps on the wrists. They're not a big deal."
- "The deal is part of Fulton County trying to build a case against the top."
- "They are, they're putting together a case to get to the top."
- "I wouldn't second-guess them too much when they haven't even gone to trial yet."
- "It's just a thought, y'all have a good day."

### Oneliner

Beau explains the lenient deals offered to high-profile figures like Sidney Powell to secure testimony against more significant targets in legal cases.

### Audience

Legal observers, justice advocates.

### On-the-ground actions from transcript

- Support the process of building cases against higher-level individuals by trusting legal strategies (implied).
- Await trial outcomes before passing judgment on legal maneuvers (implied).

### Whats missing in summary

Insights on the potential impact of testimonies from lower-level individuals on securing convictions against higher-profile figures.

### Tags

#LegalSystem #Justice #Collaboration #HighProfileCases #Testimony


## Transcript
Well, howdy there internet people, it's Bo again.
So today we are going to talk about Powell and the deal
and why she got the deal she did
and how all of this factors into the future
because there's a bunch of questions
that are coming in about it.
And I think there needs to be some context
in what it appears Fulton County is trying to do.
And honestly, it's the opposite
how cases of this size, dealing with public figures, tend to work. So we're
just going to run through that real quick. If you missed the news, Sidney Powell
entered a guilty plea. She has agreed to cooperate. So it's six misdemeanors and
six years probation, $6,000 fine, an apology, a recorded statement for the
prosecution and has to agree to testify truthfully against her co-defendants.
And to answer one question, because I mentioned it both in both videos this
morning, now I did not know that this was going to happen. I had a suspicion that
it might. If it's gonna happen for this case, it has to start, the deals have to
be in place before jury selection, generally speaking, and jury selection
starts tomorrow. So that's why I mentioned it. I didn't know anything. But
the big question that's coming in is why are they getting these deals? These are
slaps on the wrists. They're not a big deal.
What generally happens when some rich and powerful person gets caught up in
something? Who normally goes to jail? Some staffer, some intern, somebody you've
never heard about. They take the fall, right? Some consultant, some
outsider, they take the fall, and it insulates those at the top from any
real accountability generally. That's normally what happens. It appears that
Fulton County doesn't want that to occur in this case. So they are offering,
offering deals like this to Trump's alleged co-conspirators in exchange for
their testimony, which would give them Trump. I'm pretty sure that that's the
plan. Now the other thing to note as people talk about the six years
probation and no jail time and all of this stuff, the other thing that's
important to remember is that as time goes on, the deals aren't as good. The
more time that passes from the time of indictment, generally speaking, the
less prosecutors are willing to come across with something that doesn't
include jail time. This helps them from having to prepare a whole bunch of stuff
that they would need is evidence that could be substituted or put into context
by testimony. So that's the trade-off there. The deal is part of Fulton
County trying to build a case against the top. There are a lot of people
second-guessing the Fulton County DA on this one, and that's been going on
since the beginning. They'll never get a grand jury. They'll never get past a
special purpose grand jury. They'll never get a recommendation to charge. They'll
never get through the indicted grand jury. They'll never get an indictment.
It'll get thrown out. They'll never get anybody to plea. The people who've been
saying that have been wrong every step of the way. They are, they're putting
together a case to get to the top and there's a whole lot of people on the
lower rungs and I know with Powell it doesn't seem that way because very
public, the Kraken and all of that stuff, as far as the actual planning and the
actual move to disrupt things at a wider level, do you really believe that
that Powell was a mastermind? Probably not. This testimony helps secure convictions above it.
That's what it's about. The other thing to keep in mind is that a lot of the people involved in
this case, once they provide their testimony, they have provided it.
And it's important to remember that there are other cases that might look at that testimony
and say, hey, that'd be super useful, here's your subpoena.
So I wouldn't second-guess them too much when they haven't even gone to trial yet and they've
already got two convictions.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}