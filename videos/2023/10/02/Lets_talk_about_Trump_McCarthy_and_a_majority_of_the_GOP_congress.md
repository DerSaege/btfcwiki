---
title: Let's talk about Trump, McCarthy, and a majority of the GOP congress....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=CGMqQhBL9uQ) |
| Published | 2023/10/02 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the House, Senate, and the Republican Party regarding a recent deal.
- Mentions the lack of coverage on a significant political force within the Republican Party.
- Points out that despite Trump's order, a majority of Republicans voted in favor of the deal.
- Suggests that Trump's influence over the Republican Party is diminishing.
- Notes that many Republicans are not enthusiastic about the former president.
- Emphasizes the shift in the Republican Party's dynamics away from following Trump's directives.
- Indicates a decline in Trump's political power within the Republican Party.
- Urges attention to the changing landscape within the party.
- Raises the question of the significant number of Republicans now ignoring Trump's directives.
- Concludes by suggesting Trump is a leader in name only within the party.

### Quotes

- "Trump's spell, it may be wearing off."
- "He is losing his grip on the Republican Party."
- "Those polls, I wouldn't put too much stock in them because these votes, they matter too."
- "It's worth remembering that he's not the force he once was even inside the Republican Party."
- "He's leader in name only anyway."

### Oneliner

Despite Trump's orders, a majority of Republicans voted for a recent deal, signaling his diminishing influence within the party.

### Audience

Political observers

### On-the-ground actions from transcript

- Reach out to Republican representatives to express opinions on their recent votes (exemplified)

### Whats missing in summary

Insights into the potential future direction of the Republican Party post-Trump era.

### Tags

#RepublicanParty #Trump #PoliticalPower #GOP #Influence


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about the House,
a little bit about the Senate,
talk about the Republican Party as a whole,
and we're gonna talk about the deal
because there's another political force
within the Republican Party,
and nobody's really talking about
how this deal impacts them and what it says about them,
because that other force that's out there creeping around,
Somehow, it's not really getting mentioned in the coverage, and it should be because it's kind of a big deal.
Okay.
So, that deal, when it came to the House, 126 Republicans voted in favor of it, 90 voted against it.
A majority of Republicans voted for it. When it went to the Senate, I don't remember the exact numbers, but 88
8 senators voted for it.
So overwhelming majority of Republicans as well.
Unless you get everything, shut it down.
That's the message from dear leader.
That's the message from Trump.
That's the order from Trump in all caps with an exclamation point.
That's what Trump told the Republican Party to do, and they ignored him.
Trump's spell, it may be wearing off.
For a long time, the Republican Party did everything that man told them to do.
He ordered it by tweet, and that was an edict, something that they just had to follow.
majority of Republicans in Congress ignored him. That spell's wearing off.
It's wearing off because his political power is not what it once was. And Republican
insiders, they know it. Those people up there on Capitol Hill, they know it.
We've talked a lot about it on the channel. The Republican Party, they're the
Party of Family Values, sure, if you're talking about the Sopranos, because
realistically, all that matters is that last envelope of poll numbers. And Trump's
poll numbers, well, his envelope, it's been coming up a little light. Despite
what a lot of the coverage is showing you, a whole lot of Republicans are not
enthusiastic about the former president. This shows it. If this same situation
had played out when he still had power, you'd have a government shutdown. He is
losing his grip on the Republican Party. It's important to notice this. It's not
getting a lot of coverage but it's worth remembering that he's not the force he
once was even inside the Republican Party. Could you imagine this many
Republicans ignoring something he said and it not even being a topic a year
ago, two years ago? Probably not, right? His situation, the political power, the
pool that he has within the Republican Party is diminishing. Those polls, I
wouldn't put too much stock in them because these votes, they matter too. And when
more than half of the Republican Party is effectively telling the quote leader
of the Republican Party, well we're not going to follow your direction, your
public direction on something this high-profile? It's because, well, he's
leader in name only anyway it's just a thought y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}