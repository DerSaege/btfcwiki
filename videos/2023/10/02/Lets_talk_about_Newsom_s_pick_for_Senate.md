---
title: Let's talk about Newsom's pick for Senate....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=290GNQW6T6Q) |
| Published | 2023/10/02 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- California's Governor Newsom faced a politically difficult decision about appointing someone to Feinstein's vacancy.
- Newsom had made statements about who he would appoint, creating limitations on his choices.
- Despite initial statements, Newsom appointed Lafonza Butler, a relatively unknown figure outside certain circles.
- The appointment of Butler was a strategic move politically to avoid controversy and shift focus from recent negative actions.
- By appointing a union leader like Butler, Newsom was able to change the narrative surrounding his recent veto of a union bill.
- The choice of Butler allows Newsom to move past a potentially damaging news story.
- Butler's appointment did not alienate any specific group, making it a politically wise decision.
- Lafonza Butler has experience in the university system, unions, and reproductive rights.
- Butler does not have a history of holding elected positions, making her a neutral choice politically.
- Newsom's decision to appoint Butler kept his promises of choosing a black woman who was not actively running for the seat.

### Quotes

- "The appointment of Butler was a strategic move politically to avoid controversy and shift focus from recent negative actions."
- "By appointing a union leader like Butler, Newsom was able to change the narrative surrounding his recent veto of a union bill."

### Oneliner

California Governor Newsom strategically appointed Lafonza Butler to navigate political challenges and shift focus from controversial decisions.

### Audience

Politically active Californians

### On-the-ground actions from transcript

- Support community organizations working on reproductive rights and labor issues (implied)
- Educate yourself on the candidates and decisions made by political leaders in your state (generated)

### Whats missing in summary

The full transcript provides more context on the political landscape in California and insights into decision-making processes at a state level.


## Transcript
Well, howdy there internet people, it's Bo again.
So today we are going to talk about California and Newsom
and his politically difficult decision
as far as who to appoint to Feinstein's vacancy.
I said I was interested in this a few days ago
because it would show how good he was at political decisions,
the kind that would have to be made on the national stage
where he wants to go because he had talked himself
into a corner on this one.
He had made a number of statements
about who he would appoint.
And given the statements, he wasn't left with options
that people were cheering for because he
said he would appoint a black woman at one point in time.
Pretty much everybody was like, well, that's Barbara Lee.
And then he said he would appoint somebody
who wasn't running, and she was, and that put him in a very difficult position.
You also had a whole bunch of people that wanted various other candidates who were already
vying for that seat to be appointed to it.
He didn't want to do that because it would give the incumbent an unfair advantage in
the election.
So who did he choose?
Lafonza Butler.
Now if you are not super active in union circles or maybe educational circles as well in California
or really active in the fight for reproductive rights, you probably have no idea who she
is.
Smart politically it's smart on two levels.
Then all of the different camps that had their favorite candidate, none of them are happy,
but none of them are alienated.
That was a good move politically.
The other thing at play and the thing that makes this a really smart move politically
is that it allows Newsom to kind of just get right past a news story that he probably doesn't
want to deal with.
Newsom just vetoed a union bill.
He just vetoed a bill that would help workers who were on strike.
Now is not the time to be opposed to organized labor.
That's not going to go well moving to the national scene.
So him being stuck with that image based off of that last veto, that's not good for him.
If you appoint somebody who's a union leader, it allows that story to be replaced.
Moving the story along.
This is something that is oftentimes hard for state politicians to get used to on the
national scene because basically when you're talking about something like this politically,
the smart move is to do something in the same category that changes the story.
of a sudden Newsom goes from the guy who vetoed this bill to the person who
appointed a union leader to the Senate. Politically it's smart. Now I'm not
talking about anything other than the politics right now. Obviously people are
going to want to know about her. She has a lot of experience when it comes to the
university system, and a lot of experience when it comes to unions and reproductive rights.
I know you're waiting for the list of offices that she's held, like elected
positions. I don't think she's held one. It's a smart move. This isn't somebody
that has a whole lot of ties to the different political camps. So
appointing her isn't going to alienate anybody. Politically, yeah, this is the
type of deal making and maneuvering somebody has to have if they want to be
on the, if they want to be on the national stage and he demonstrated that
he has it. Now for those who are wondering, because I'm sure people are
gonna wonder if he kept his promises, yes, Lafonza is black. So he kept them.
Black woman, not actively running for the seat. And he managed to change the story.
I mean, yeah. Now, as soon as this news broke, I got a question from a friend and
just in case anybody else is wondering this. Union thug. That term, that's not
that's not an insult. That's not an insult. For white people who are active
with unions, maybe not a term to use for black union leaders.
It may not carry the same connotation to them.
So yeah, that term is actually a compliment when coming from union people.
So that wasn't an insult.
So if you see that, it doesn't mean what that term normally means.
It's a stamp of approval, not an insult.
Just throwing that out there because it apparently happened within minutes of the news breaking.
Anyway, it's just a thought.
Y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}