---
title: Let's talk about Rudy's trouble in Georgia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=qZXog2mBKvI) |
| Published | 2023/10/15 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau provides an update on Rudy Giuliani's situation in Georgia, focusing on a defamation case involving two election workers.
- Giuliani was ordered to hand over documents related to his finances, but he disregarded the court's order, leading to severe consequences.
- The jury will be instructed to assume that Giuliani intentionally hid relevant financial information to shield his assets and deflate his net worth.
- Due to lack of information provided by Giuliani, the jury is directed to believe he has more money than perceived.
- Giuliani's attorneys are prohibited from suggesting that he is insolvent or bankrupt during the case.
- The election workers are likely to receive a substantial award due to the instructions given to the jury.
- This legal issue adds to Giuliani's series of setbacks in recent times, mirroring challenges faced by Trump and other defendants.
- The situation in Georgia marks a significant setback for Giuliani, potentially leading to a hefty penalty in the defamation case.

### Quotes

- "The jury will be instructed to assume the worst and believe Giuliani is hiding money."
- "Giuliani's legal entanglements have produced a long string of setbacks for him."
- "This is probably the worst thing that could have happened to him in this case."

### Oneliner

Beau provides an update on Rudy Giuliani's legal troubles in Georgia, where his intentional concealment of financial information may lead to hefty penalties.

### Audience

Legal observers

### On-the-ground actions from transcript

- Stay informed about legal proceedings and outcomes related to public figures (implied)

### Whats missing in summary

Full context and details of Giuliani's legal troubles and the potential implications for the defamation case.

### Tags

#RudyGiuliani #LegalTroubles #Defamation #Georgia #FinancialDisclosure


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about Rudy Giuliani.
We haven't talked about him in a while.
So today we're gonna talk about Rudy
and his situation in Georgia
and how it went from bad to worse on Friday
because he got some information
that I am certain he is not appreciative of.
So, this deals with the situation with the two election workers.
The ones that he allegedly defamed, we're talking about the two people that he said
were passing around a USB drive or whatever.
The jury that is going to be deciding how much he has to pay, they are going to get
some very unique instructions and they're going to get them because the
court basically told Giuliani to hand over documents related to his finances
and Giuliani continually and flagrantly disregarded that order. That's in this,
that Giuliani's continued and flagrant disregard of this court's August 30
order. You know it's going to be bad when it starts off like that. So he was told
to produce this information about his finances to the court and apparently he
didn't. The judge was very unhappy about that. So quote, the jury will be
instructed that it must, when determining an appropriate sum of compensatory presumed and
punitive damages, infer that Defendant Giuliani was intentionally trying to hide relevant discovery
about the Giuliani business's finances for the purpose of shielding his assets from discovery
and artificially deflating his net worth. Must infer that Defendant Giuliani was intentionally
trying to hide. That's something else. So the jury, because of a lack of
information, they're going to be told to basically assume the worst and to believe
and infer that he has more money than anybody thinks. That's bad. It still gets
worse. His attorneys will be they'll be stopped from quote making any argument
or introducing any evidence stating or suggesting that he is insolvent,
bankrupt. Basically they can't even say he's broke. The short version here is
is that the election workers they won and based on this my guess is that the
jury is probably going to award them a pretty hefty sum because they are being
instructed by the judge to to operate under the assumption that Giuliani is
hiding money, so they're probably going to award a substantial amount.
This is just the latest in a long string of, let's just call them setbacks, that Giuliani
and his legal entanglements have produced for him.
We haven't been covering them all because there's a lot of other stuff going on.
But you know how things have been going really bad for Trump and for the defendants in the
Georgia case?
Basically the same kind of stuff's been happening to Giuliani.
This is probably the worst thing that could have happened to him in this case, in this
defamation case.
So we'll hear more about that as the jury comes together to determine how much he has
to pay.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}