---
title: Let's talk about joking about confidence....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=g7_xZrVfzn8) |
| Published | 2023/10/15 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Receives screenshots with stories of unwarranted confidence in surviving extreme situations.
- Examples include a man believing he could survive on a sunken sub, another thinking he could fight a grizzly bear with a pocket knife, and someone feeling they could have survived the Titanic sinking.
- Expresses admiration for the unearned confidence that some have, particularly middle-class white American men.
- Wishes people worldwide had such confidence to chase their dreams and make the world a better place.
- States the importance of confidence in pursuing achievable dreams rather than surviving the unsurvivable.
- Notes that this level of confidence is necessary to pursue actual dreams.
- Acknowledges the deep-seated confidence found in some individuals but deems it necessary for achieving goals.
- Encourages individuals who doubt themselves to adopt the confidence of a middle-class white American man on Twitter to go after their dreams and change the world.
- Emphasizes that sometimes all it takes to make a difference is the confidence to start.
- Concludes by suggesting that despite the confidence boost, one wouldn't have survived on the sub mentioned in the screenshots.

### Quotes

- "Pretend that you are a middle-class white American man on Twitter and just let that confidence wash over you."
- "Go change the world because a lot of times it just takes the confidence to start."
- "You wouldn't have survived on that sub."

### Oneliner

Beau encourages adopting unwarranted confidence to pursue dreams and change the world, contrasting it with survival odds in extreme situations.

### Audience

Dreamers and world-changers.

### On-the-ground actions from transcript

- Embrace unwarranted confidence in pursuing dreams (suggested).
- Start with the confidence needed to make a difference in the world (suggested).

### Whats missing in summary

The full transcript dives deeper into the concept of unwarranted confidence and its role in pursuing achievable dreams compared to surviving extreme situations.

### Tags

#Confidence #Dreams #MiddleClass #UnachievableSurvival #ChangeTheWorld


## Transcript
Well, howdy there, internet people.
It's Bill again.
So today, we're going to talk about some screenshots
that I was sent, confidence and dreams
and how I really wish I hadn't already used
that crush shirt prior to making this video.
Okay, so the screenshots, the first one was a man describing how he believed that if he had
been on that sub, you know, the one that is no longer a sub, that if he had been on it he would
have survived. Something would have happened and he would have been able to swim to the
the surface, I'm not joking, and that he fully understands the odds, that that
was like a 0.00001% chance or something like that, I mean sure whatever,
but he just doesn't believe those odds apply to him personally. Merrick I hope
you're watching this, I think most of this came out of your Twitter account.
The next one was a guy basically convinced that he he could fight a
grizzly bear with a pocket knife and win because he knew how to climb trees I
think was part of it. Okay and then another one was about a guy saying that
he had always felt that he would have been able to survive in the waters
around the Titanic, just through will. And the message that accompanied this was
basically like, hey, what do you think about this idea and the way these people
like view the world? I think it's wonderful. I think it's great. I wish that
people all over the world. Somehow we're able to have the just absolutely
unearned confidence that the average middle-class white American man has.
It would be an amazing place. Everybody would be out there chasing their dreams.
It would be like Star Trek. Nothing could stand in your way. That level of
confidence. I know in this light it's ridiculous because, yeah, none of those
people are actually making it, but that level of confidence is something
that is needed to pursue your actual dreams. Dreams that, you know, could be
achieved. Not surviving the insurvivable. I really don't think people understand
how deep they were. But it is something that, to my knowledge, is a uniquely American thing.
This thing that is just guaranteed to just wipe people out, I'll make it. You know, all
All of that's a joke, deep down, I mean, it's not really, but a lot of messages I get that
are a bit more serious, they boil down to, I want to do this to make the world better,
but I don't think I can.
If you are ever in that situation, please just pretend that you are a middle-class white
American man on Twitter and just let that confidence wash over you and go chase your
dreams.
Go change the world because a lot of times it just takes the confidence to start.
At the same time, I assure you, you would not have survived on that sub.
Anyway, it's just a thought.
Y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}