---
title: The Roads Not Taken EP9
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=wkBPYq2FptE) |
| Published | 2023/10/15 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Overview of under-reported news that shapes future events.
- George W. Bush advising Netanyahu on peace in the Middle East.
- United States authorizing the departure of non-essentials from Israel and the West Bank.
- Afghanistan facing multiple earthquakes causing over 1000 deaths.
- Saudi Arabia halting talks on normalization with Israel.
- House of Representatives in the US without a speaker, with Jim Jordan as a nominee.
- Possibility of a centrist coalition being the best politically.
- Positive news: economists no longer predict a recession in the next year.
- California passing legislation for healthcare workers to have a minimum wage of $25 per hour.
- American College of Emergency Physicians withdrawing approval of a 2009 paper on excited delirium.
- Republican candidate winning the governorship in Louisiana.
- NASA embarking on a mission to an asteroid with valuable metals.
- Concerns over falling Starlink satellite fragments by 2035.
- Archaeologists discovering the oldest wooden structure in Zambia dating back 476,000 years.
- Insight into potential expansion of war operations based on Iran's rhetoric.

### Quotes

- "Information that will probably be important later and may shape future events."
- "For most people in the United States, the best thing politically is a centrist coalition."
- "Putting them out for a whole lot of people, it takes a lot of work to get back from that."
- "Run your advertising and your content decisions through the filter of 'will the viewer like this?'"
- "Having the right information will make all the difference."

### Oneliner

Beau provides under-reported news shaping future events, from George W. Bush advising peace in the Middle East to potential war expansion based on Iran's rhetoric.

### Audience

Policy watchers, news enthusiasts.

### On-the-ground actions from transcript

- Contact representatives to prioritize bipartisan solutions in the US House of Representatives (implied).
- Support legislation for fair wages for healthcare workers like in California (exemplified).
- Stay informed on economic predictions and support policies that strengthen the economy (exemplified).
- Advocate for accountability in police-related cases (exemplified).

### Whats missing in summary

Insights into potential consequences of underreported news stories, encouraging critical thinking and informed action.

### Tags

#UnderreportedNews #ForeignPolicy #EconomicPredictions #PoliceAccountability #BipartisanSolutions #CommunityAction #EnvironmentalConcerns


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are doing The Road's Not Taken, episode nine.
And this is a weekly series
where we go through under-reported or unreported news,
stuff that just didn't make it over on the main channel.
But it's information that will probably be important later
and may shape future events.
So it's kind of providing some background context
to everything.
Okay, so starting off, we're going to be looking at foreign policy. George W. Bush
offered advice to Netanyahu on the current situation because if there's
anybody who really understands how to bring peace to the Middle East, it's
George W. Bush. The United States has authorized the departure of non-essential
personnel from Israel and, I believe, the West Bank as well.
So this is not like a large evacuation.
This is telling people who don't have to be there to get out.
This is a clear indication that the promised ground offensive is probably coming really
soon.
Afghanistan was hit with yet another earthquake, the third
this week.
The quakes have killed more than 1,000 people.
My understanding is that the last one wasn't
actually as bad.
They seem to be kind of tapering off.
But the crisis has been overshadowed by other events.
So eventually, there's going to be a humanitarian issue here
that will crop up in the future.
Saudi Arabia has put a hold on the talks
about the normalization of relations with Israel.
If the hold turns out to be permanent or long-lasting,
it may put an end to the hopes of a region that
is stable because of regional powers, which would eventually
That would eventually lead to a situation where another major world power is stepping
into play, policemen.
So that's not great news.
Okay, moving on to the United States.
At the time of filming, the U.S. House of Representatives is still without a speaker
as Republicans struggle to form a coherent message or unite in any way, shape, or form,
current nominee, definitely subject to change, is Jim Jordan. It's worth noting that by my count,
he doesn't actually have the votes to win. That might change over the weekend. Maybe
maybe they're working the phones, but by my count he's not there. Now one of the
interesting things is that Mike Rogers has now publicly stated that he's not
going to back Jordan. He also publicly seemed pretty open to working with
Democrats to achieve a bipartisan solution to a very Republican problem.
McCarthy has gone out of his way to try to blame the Democratic Party for the
current situation, which is, I mean, that's an interesting
strategy. We'll see how that plays out.
In some good news, a recession coming in the next year is no longer the most widely held
belief of economists.
This is probably the best news possible for the Biden administration because while presidents
don't actually control the economy, most voters feel like they do and they tend to
vote that way.
So for quite some time, actually I believe more than a year, the general consensus was
that there was going to be a recession in the next year.
And it hasn't happened and it hasn't happened.
Now the reason for this, some of it is yes, there's a lot of infrastructure jobs and
stuff like that.
There's a lot of stuff like that that's going on and that's helping.
But another pretty big piece of it is you.
You're not panicking, which is kind of what economists expected you to do.
The US economy in many ways, shapes and forms runs on faith.
And so far, faith in the economy really hasn't been shaken.
So consumer spending and your behaviors are keeping the economy going.
Healthcare workers in California will have a minimum wage of $25 per hour thanks to new
legislation signed by Newsom.
I feel like that is going to play into some labor disputes later.
In a pretty interesting piece of news that may not sound really important at first, but
trust me it is, the American College of Emergency Physicians withdrew their approval for a 2009
paper on excited delirium.
This is a term that gets thrown out after somebody dies in police custody, and it's
It's often used as a justification for the death.
The removal of this approval means that people can't use the ACEP's endorsement of this
concept as a way to bolster their defense case.
They were kind of the last holdout.
The National Association of Medical Examiners and the AMA have also rejected this term.
This is going to matter when it comes to police accountability cases in a big way.
I know it seems weird that this organization withdrawing their approval of this paper,
But this is going to matter in court.
The Republican candidate for governor won in Louisiana, flipping the state from blue
to red.
On Friday, a Republican New York City councilwoman was arrested after she allegedly brought a
firearm to a pro-Palestinian rally at a college there.
17 allegedly bad apples, employees of Broward County Sheriff's Department, have been
indicted for COVID relief fraud schemes.
In a pretty surprising turn of events in this, I mean, the fact that this occurred isn't
surprising.
surprising is that from what I can tell, the investigation was
actually started kind of at the request of the sheriff. If you
are familiar with Broward County, the idea that they were
taking proactive steps on, well, anything to be honest, is that
surprising but it appears if I'm understanding this correctly the
leadership of the Sheriff's Department kind of got like a hint something was
going on and ordered a review of literally everyone in the department and
it has led to 17 indictments. Okay the governor of Arkansas Sarah Huckabee
Sanders is involved in a, I don't know if I would call it a scandal, a whole lot of
questions are being asked about a lectern, a podium for most people. It
cost $19,000 and there is apparently a whistleblower that has accused her
office of like covering up the spending it's it's a very strange case and it's
one of those that's so weird I don't understand like I don't even understand
why it would be done like set aside okay let's just assume the governor there is
a politician and does shady things this doesn't even make sense like I don't
get it, but it's something that appears to be a little scandal there in Arkansas
that is growing rather than receding. In cultural news, Taylor Swift's movie is
seeing an opening weekend of about a hundred million dollars. Please keep in
mind that for whatever reason, large segments of the Republican
right have decided that opposing Taylor Swift is going to be good
for them. $100 million opening weekend. I am sure that going
after somebody of that stature with that loyal a fan base, I am sure that that will not alienate
voters in any way. Okay, moving on to environmental news. After years of delay, NASA has a mission
underway to go to this asteroid that is heavily metallic. The reason this is
interesting is because it has prompted a whole lot of conversations. Now NASA's
going there for research, obviously, but the the makeup of the asteroid, if it was
brought to earth. The metals in it would be worth a hundred thousand
quadrillion dollars. This of course has prompted all of the space capitalists
and got their attention and they are very interested in bringing it back to
do that to make the hundred thousand quadrillion dollars proving once and for
all that the online capitalist gurus definitely understand the basic rules
of supply and demand. And if there is that much of things that are rare
and the rarity is why they're expensive and you bring it there it is no longer
longer rare, so the price would then go down substantially. Anyway, the FAA is
saying that by 2035, thousands of fragments from Starlink satellites
falling back to Earth could survive reentry each year. The satellites are
supposed to burn up. They're not supposed to have fragments coming
down. If these numbers are right, it would raise the chance of a person being killed
by a falling satellite to about a 60% chance per year. It is worth noting that SpaceX has
basically said the FAA is wrong and they have serious, serious errors in their analysis.
I would imagine we're going to hear more about this,
and there's probably going to be dueling studies and analysis
going on.
And we'll eventually find out.
I, for one, hope the FAA is wrong,
because there's already a whole lot of those satellites
up there.
Moving on to some odd news.
I don't know if this is really odd, but it's rare.
A Ring of Fire solar eclipse occurred over the Americas.
The next one viewable from this region is in 2046,
but there will be a total solar eclipse
that will be visible from Mexico, the U.S.,
and Canada on April 8th, 2024, so relatively close.
So, relatively close. If you missed this, there's an opportunity to see a different
celestial event coming.
In really odd news, truly odd news, in Zambia, archaeologists have reportedly discovered
the oldest wooden structure.
It's reported to date back 476,000 years.
If you're not familiar with the timeline,
that's before homo sapiens.
That is before homo sapiens.
And they also believe that they found tools at the site.
So either they're wrong about the age
Or we're going to have to start looking at our early ancestors
in an entirely different way.
Obviously, there's going to be more study,
and there will definitely be more news about this.
But this might be one of those things that
rewrites history books.
So moving on to the Q&A. When will we
have a clear picture of whether or not the war will expand. Once ground
operations start. Once ground operations start. What you need to look for is once
the ground operations begin, you need to look at Iran's rhetoric. Because what
matters, as weird as this sounds, what matters isn't what happens, it's how Iran
perceives it. It very easily could spiral, it could expand. They have said
that, you know, a ground operation is just out of the question. That's probably not
true, that's probably not really their red line, but how they perceive those
events will definitely shape a whole lot of other organizations and entities.
That's going to be the moment we find out.
If they don't say anything, there's not a lot of wild rhetoric, there's probably not
a big risk of it expanding.
If they're kind of upset, you'll probably see other non-state actors kind of get involved.
If the rhetoric is really fiery, there may be state on state, which is, that's really,
really bad for just everybody, because if that starts, it could expand even further.
So right now what really matters as far as whether or not it expands is how Iran perceives
those events.
Nobody else's opinion really matters at the moment.
Okay.
I'm a Jewish leftist.
I've heard my Jewish friends say things about how to deal with things that are just horrible.
And I've had leftist friends say things about us that I don't know if I can ever forgive.
How am I supposed to look at people like this the same, ever again?
Yeah, there's going to be a lot of this.
I don't know.
I don't know.
There are moments when people respond to world events and how they respond alters the way
other people see them.
In some instances it could be something as simple as a tweet that is just so out of line
for your beliefs that there's really not a way you can look at them the same way again.
And sometimes the person might show that they were caught up in the moment and they said
something they didn't mean.
But generally speaking, you didn't give specifics here, but when people advocate for things
that should just be totally off the table, remember it.
Because it indicates a lot of other things about people, I mean I feel for you, I just,
How am I supposed to look at people like this the same ever again?
I don't know that you can't.
I don't know that you can.
I'm sure some will do things that show that that's not really who they are.
But I have a feeling that a whole lot of people are showing who they are.
It's a lot like the Kavanaugh hearings.
During that period, I got a lot of messages from women talking about male family members
or male friends, and the statements they made, they forever altered their relationship because
you can't look at the person the same way again.
This is one of the reasons that it's important to remember that not every thought that you
have is suitable for public consumption.
You can be angry and you can think things, but putting them out for a whole lot of people,
it takes a lot of work to get back from that.
So I don't have any advice for you.
I have no idea how you can or if you even want to.
This may be one of those things where you're just venting and that's fine too, but I would
imagine right now, as far as spectators, those who are not actively involved in the situation,
a Jewish leftist is probably in one of the worst positions.
You're going to get a lot of crosstalk.
What's your Halloween special going to be this year?
I haven't decided.
I have not decided.
I haven't picked a topic or anything yet.
So if you have any suggestions, feel free to put them down below.
You should be the speaker.
Your personal opinion on what would be best politically, not what's likely, fair enough.
What's best politically?
I mean, I guess that would depend on your politics.
For most people in the United States, the best thing would be for a centrist coalition
of Republicans and Democrats to put somebody up.
That would be the best thing, politically,
for the majority of people.
You're not going to get a progressive speaker.
It seems unlikely that a far-right speaker
is going to come through.
So the real options that would actually be on the table
would be a centrist coalition or a moderate-right Republican.
Out of those, I would choose the centrist coalition.
There is a slim chance, because now you have them saying things
publicly now, that the discord within the Republican Party
lead some to actually cross over and go to Jeffries.
I don't know if it's going to be the votes necessary, though.
Do I think a centrist coalition is likely?
I don't know.
At this point, we're kind of in uncharted territory here.
The Republican Party is so just broken and incapable of accomplishing basic tasks because of their reliance on social
media hot takes that I don't know that there's going to be an internal solution.
I can't really think of anybody, I can't think of anybody in the party that would actually
be able to rally the party.
So I don't really know what's likely, I can tell you as situations arise, like it seems
pretty unlikely that Jordan's going to get it.
But as far as a final outcome, I don't know.
But it needs to be worked out pretty quickly, because the clock is ticking on a whole lot
of different things.
Okay.
I'm a new YouTuber.
I just qualified for the brand deal thing.
You've repeatedly said you don't do them.
Is there some downside I'm not seeing?"
It depends on your content.
So on the main channel, you can't add 30 seconds to 90 seconds of ad into one of those
videos.
Some of those videos are only three and a half minutes long.
If it was me, I would only do things that add value for the people watching you.
If you want my advice, that's what I would do.
If you are...
I can't think of a...
Oh, here.
Okay, so right now YouTube has that little shopping thing, right?
a little button that pops up every once in a while in the videos, you can tag products.
I've used it to showcase shirts, or put little things that were Easter eggs and stuff
like that, but recently on this channel, well I don't know about recently, but on this
channel I did that review of Hart Tools, because they're really inexpensive, they're good
for relief work.
It wasn't sponsored.
And the shopping thing, I don't think that that company is involved in it, so you couldn't
tag them.
If a situation existed where you were going to do something like that, like this is a
video you're gonna make, and that company wants to like help with it, that's one thing.
Going out of your way to make content to cater to a company, it may not fit with your channel
but you don't say what your channel is.
Like if you do makeup reviews, then yeah, do them all day.
But if it's a political channel, if it's a news channel, if it's a philosophy channel,
it has to be something that you would want to watch on a channel that you are watching.
That would be my advice.
There's nothing inherently wrong with them.
They just don't fit for me.
I would run your advertising and your content decisions, just always run it through the
filter of would the viewer like this?
If the answer is no, don't do it.
If the answer is yes, do it.
Going back to that question, I understand that right now there's a lot of hot takes.
There's a lot of political discussion, there's a lot of people staking out their positions
all of that, and that's fine.
I would suggest that not just morally, but for the sake of your friends, your own personal
relationships, just as a general rule, don't collectivize anybody based on the actions
of whoever rules over them, or I mean, I feel like not collectivizing people on their race
or ethnicity, I feel like that's a given, but in case not, I'll go ahead and say that
too, but collectivizing an entire people based off of their ruler, the government, the authorities
that are over them.
You're probably going to sweep a lot of people up into that generalization that you don't
really want to be there, that you don't really mean that about.
And some of them might be your friends, until you say it.
Just think back to the Trump years.
Would you want to be identified as his supporters?
Would you want to be viewed through the actions of Trump?
Probably not.
This is way more serious than that.
I know that social media discourse being what it is, getting that position out there and
going through and just constantly getting that hot take out, that's how you get your
engagement.
there's actual engagements going on it's probably not the best policy. Anyway, so
that's the... that's a quick recap of the stuff that was missed and provides the
context. Gives you the information that you need and having the right information
information will make all of the difference.
Y'all have a good night.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}