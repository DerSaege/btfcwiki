---
title: Let's talk about the Speaker options for Republicans....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=CwMfwl_PHwA) |
| Published | 2023/10/15 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the current situation in the US House of Representatives and the struggle to find a speaker.
- The Republican Party faces challenges in selecting a viable speaker within the current election cycle.
- Options like rallying behind Jordan or bringing back McCarthy have significant drawbacks.
- The possibility of a new, unknown candidate emerging is uncertain due to the party's fractured state.
- Social media presence seems to be a factor in considering potential speakers.
- The idea of extending McHenry's term with Democratic help is another option, albeit with limited influence.
- A coalition of moderate Republicans and centrist Democrats could present a bipartisan candidate, with Jeffries being mentioned.
- Acknowledgment that maintaining the status quo may be the best course of action for the Republican Party in the current climate.
- Concerns about the influence of far-right Republicans and the difficulty in steering the party towards a cohesive agenda.
- Reflection on the challenges of removing authoritarian influence from the Republican Party.

### Quotes

- "This is the discomfort."
- "Once it got into the Republican Party, it was going to be really hard to get it out."
- "At some point they're going to have to acknowledge that."
- "Just keeping things open is the best they can do for right now."
- "Y'all have a good day."

### Oneliner

The US House of Representatives faces challenges in selecting a speaker amid Republican Party disarray and potential paths to a cohesive agenda seem limited.

### Audience

Political analysts

### On-the-ground actions from transcript

- Form bipartisan coalitions to support moderate candidates for leadership positions (suggested)
- Advocate for strategies that bridge divides within political parties (implied)
- Encourage open discourse and cooperation between different political factions (implied)

### Whats missing in summary

Insights into the potential consequences of the Republican Party's current internal struggles and the impact on legislative progress.

### Tags

#USHouse #SpeakerElection #RepublicanParty #PoliticalChallenges #ModerateLeadership


## Transcript
Well, howdy there, internet people, let's bow again.
So today, we are going to talk about the US House of Representatives and the attempt to
find a speaker of the House, something that, as of yet, has still not happened.
There are a bunch of options that people are talking about, and we're going to run through
them, but we're going to run through them and we're going to look at them kind of
from the Republican Party's side for a moment because the options that are out
there, some of them have major pitfalls and it's hard to see a clear path from
where they're at, to where they want to be, within this election cycle.
Okay, so option one. Right now, Jordan. The party is trying to rally people behind
Jordan. He has opposition. If a fraction of that opposition holds, he doesn't have
votes. The other thing is because of the opposition being sizable enough, even if
he wins, he's McCarthy again. He doesn't really have the influence to lead the
party because of all of this mess and how he would have came to power. He's not
going to have that influence.
Another option would be McCarthy coming back and just kind of resetting everything.
It's an option.
McCarthy has publicly signaled his support for Jordan, but some of McCarthy supporters
have been like, no, not doing it.
there are at least two that are McCarthy or Bust. That's the only option they want
to entertain right now. If he winds up back with the gavel, he's back right back
to where he started. Again, leadership will be called into question because of
all of this. So won't be able to actually lead the party, won't be able to
exercise the power. Another one is somebody new, somebody that we haven't
thought of yet, somebody who maybe throws out their hat today or tomorrow.
I don't know anybody that actually could get support to be honest. After thinking
about it, I don't know anybody that's going to be able to get the votes. I'm
I'm sure there's a person out there, but I can't think of them.
The Republican party is out fractured at the moment.
But let's say it happens.
Some person that we haven't entertained yet, no matter what, their second choice,
they're not going to have the ability to lead either.
They're not going to have the power that normally comes with the speakership,
Because they're not going to have the influence.
And this person, my guess would be
that it would have to be somebody that does the social media thing.
And Jordan leans that way, too.
And that's one of those things where there's
a lot of people in the party who, like, this
is how we have to do it.
This is how we have to do it.
And they're doing a lot of performative stuff.
And it plays well in deep red areas,
but they were going to get deep red areas anyway.
It doesn't do well in areas that are contested.
You would think that by now, they
would realize that social media clicks aren't actually
a great electoral strategy, given the fact
that it hasn't really worked well for them since 2016.
But it's an option.
Another option would be the party itself looks at McHenry and just extends it.
Makes him de facto, temporary, whatever, speaker, maybe with a little bit of help from the Democratic
party because they'll probably need it.
And then there's that.
But again, no influence and no real power.
that the Republican Party's not divided. And then the last option would be a coalition of
moderate Republicans, what passes for moderate Republicans, and centrist and
corporate Dems. And them putting something together and coming up with
some bipartisan candidate. There's that. Now when that comes up people think
Jeffries. Jeffries is an option. It's not impossible because it doesn't really need that many votes.
It seems less likely than something a little bit more deliberate.
The problem with that is, sure, it'll keep the government open. It'll allow basic functions to
occur, but the Republican Party isn't going to like it because they're not going to be able to
exercise any power it's at this point that hopefully the Republican Party
realizes they're not going to be able to exercise any power you don't have
options that lead to a house that can push a Republican agenda now that was
all blown out of the water it they don't have the votes and now because of
all of the tensions that have arisen, there aren't many paths to get somebody
to be speaker that would even be able to move a Republican agenda. It's at this
point that the Republican Party may have to acknowledge that just keeping things
open is the best they can do for right now and they have to wait until the
next election cycle and hope that the hard-right Republicans don't do so well
in their election. That's about the only way at this point I can see them being
able to correct the path the Republican Party is on because those far-right
Republicans, they're gonna keep doing what they've been doing as long as
they're up there. There were a lot of people who said, you know, once that type
of authoritarianism, Trumpism, whatever term they used, once it got into the
Republican Party, it was going to be really hard to get it out and it might
cause a lot of discomfort along the way. This is the discomfort. At some point
they're going to have to acknowledge that.
They're going to have to sit there and look over at the Senate and think Lindsey Graham
was right.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}