---
title: Let's talk about Ukraine, BTR-90s, and F-16s....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=3LyQAYy5PwM) |
| Published | 2023/10/15 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Three news items related to Ukraine discussed, tying into readiness and fourth-generation warfare.
- Ukrainian pilots to start training to fly the F-16 in Arizona next week.
- Ukraine used more artillery rounds than Russia for the first time in the conflict.
- Russia shifting doctrine to be more accurate with artillery, possibly due to lack of resources.
- Russia received a shipment of some artillery from North Korea.
- Introduction of the BTR-90, a larger version of the BTR-80 with a BMP-2 turret, as an infantry fighting vehicle.
- BTR-90 vehicles were pulled out of storage by Russia due to the dire need for infantry fighting vehicles.
- The BTR-90 was developed in the 1990s, indicating Russia's struggle with force generation capabilities.
- Lack of training for the individuals using the BTR-90 due to them being experimental vehicles pulled out of storage.
- Uncertainty about the interchangeability of parts in the BTR-90 and potential operational issues.
- Prediction that the BTR-90 may not stay in service for long and is a stopgap measure due to production constraints.

### Quotes

- "Ukraine used more artillery rounds than Russia for the first time in the conflict."
- "Russia is in such dire straits when it comes to infantry fighting vehicles they have pulled some experimental ones out of storage."
- "That is a really bad sign for their force generation capabilities."

### Oneliner

Beau gives insights on Ukraine's F-16 training, artillery use, and Russia's dire need for infantry fighting vehicles, revealing concerning signs for force readiness.

### Audience

Military analysts, policymakers

### On-the-ground actions from transcript

- Monitor developments in Ukrainian military training and equipment (implied)
- Stay informed about Russia's military capabilities and readiness (implied)

### Whats missing in summary

Detailed analysis and context on the Ukraine-Russia conflict and its impact on military strategies.

### Tags

#Ukraine #Russia #MilitaryConflict #ForceGeneration #Artillery


## Transcript
Well, howdy there, Internet people.
Let's bow again.
So today, we are going to talk about a few different news
items, I guess three, really.
And they all relate to Ukraine, different aspects of it.
But they all tie in there.
And they all also kind of tie into the general tone
of readiness overall and how that's
being achieved in fourth generation and all that stuff.
Okay, so we will start with the F-16.
There has been a lot of roadblocks along the way
when it comes to Ukrainian pilots being trained
to fly the F-16.
They're apparently all gone.
Next week in Arizona, that starts.
It will take some time, but the process is underway.
OK, the next piece has to do with artillery.
I think it was last month for the first time Ukraine used
more artillery rounds than Russia for the first time
in the conflict.
Now, we talked, I think, over on the other channel,
we talked briefly about how Russia was shifting its doctrine
to try to be more accurate and use less rounds.
That might have something to do with it.
It also probably has a whole lot to do with the fact
that they don't have much.
Granted, they just got a shipment of some kind
from North Korea.
We don't know the quantity, we don't know the quality,
but we know some stuff arrived.
It's an interesting shift.
Generally speaking, Russia is artillery intensive, so them not using them to the same degree,
you can read into that a little bit with, it's probably a clear indication that they
are trying to be sparing with what they have.
Now for the really interesting news.
And I know there are some people who showed up here
just to tell me I have a typo in the title.
I don't, BTR 90s.
They're a thing, they actually, they exist.
So for normal people, those giant armored vehicles
that look like tanks that have wheels, those are BTRs.
Generally speaking, the BTR 90 isn't
something that anybody has seen. In fact, the first time it was ever in action was
I want to say on the 13th, maybe the 11th, in the last few days. It was never really
produced in any large quantities. Basically, it is a larger version of the
BTR-80 with a BMP-2 turret duct tape to it. It's not really duct tape, but they
a BMP-2 turret on top of it. So it's an infantry fighting vehicle. Russia is
apparently so in need of IFVs after losing as many as they have that they
have pulled these out. Now the BTR-90 by the specs, not actually a bad vehicle.
That's not the interesting part. The interesting part is where they got it. The
38th Research Institute. It is a relatively safe assumption that these are
the original ones developed in 93-ish. Russia is in such dire straits when it
comes to infantry fighting vehicles they have pulled some experimental ones out
out of storage, the test vehicles, and press them into service and send them on
over. That is a really bad sign for their force generation
capabilities. It's also a pretty safe assumption that the individuals that
will be using the BTR90 or 90s, we've only seen one so far, maybe they sent the
rest of them, it's a pretty safe assumption that they're not well
trained on them since they just came out of storage and were never really used in
large numbers. That's not a good sign for the people who are going to be
operating them. I am not sure about the interchangeability. I know the interior
layout is roughly the same as the other BTRs, but I'm not sure about the
interchangeability of parts. I'm sure that there will be some things that are
different and I assure you that Murphy is a real thing on a battlefield. Whatever
part is different, that's what's going to break. I don't feel like these are going
to be in service too long. This is definitely a stopgap of some kind. They
can't keep up with the production that they need. So those are the three pieces
of news that we'll probably see this information again. It'll be
useful context for later. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}