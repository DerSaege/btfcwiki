---
title: Let's talk about what they wanted and manuals....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=UKvzOF-toIA) |
| Published | 2023/10/10 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the strategy behind irregular conflict and unconventional war.
- Addresses the questions of what the small force aims to achieve and why the big force should show restraint.
- Notes that the small force seeks to provoke an overreaction from the big force to generate sympathy and support.
- Emphasizes that in this type of conflict, violence is part of a PR campaign.
- Argues that politically realigning the small force is more effective than using massive force.
- Provides historical examples like the Boston Massacre and Bloody Sunday to illustrate the effectiveness of restraint.
- Urges giving peace a chance in resolving such conflicts.
- Advocates for bringing opposing forces to the table instead of crushing them.
- Mentions the existence of literal manuals outlining these strategies.
- Points out the cyclical nature of conflict escalation and the ineffectiveness of continual back-and-forth responses.

### Quotes

- "Give peace a chance when you're talking about this kind of conflict, that's how you win."
- "The goal should be to bring them to the table, to bring them out. That does work."
- "Those who made that decision, those who decided to deploy that strategy, I'm willing to bet they're somewhere safe."
- "It's a PR campaign with violence. Nothing more."
- "When they were brought to the table, is when things calm down."

### Oneliner

In irregular conflict, provoke sympathy through overreaction, and prioritize peace talks over crushing opposition.

### Audience

Conflict analysts, peace advocates.

### On-the-ground actions from transcript

- Study and understand the strategies behind irregular conflict (implied).
- Advocate for peaceful resolution and diplomacy in conflicts within your community (implied).

### Whats missing in summary

In-depth analysis of historical examples and the importance of restraint in conflict resolution.

### Tags

#IrregularConflict #Peacebuilding #Diplomacy #ConflictResolution #Strategy


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about two questions,
two very, very, very different questions
that more than likely originated from opposite ends
of the current situation, from different sides.
But the answer is the same.
And it's one of those things
that seems very, very counterintuitive at first.
It doesn't make any sense unless this is a subject
you have studied intensely or you were trained in it,
you've read the manual, or you watched the playlist titled
It's in the Manual.
I'll put it down below.
It just doesn't add up because it's not
what you see in movies.
questions. They revolve around strategy when it comes to irregular conflict,
unconventional war, low intent, whatever term you want to use for it. Big force,
little force. It involves the strategy for that. The first question is what on
earth was the little force hoping to achieve? And then the other question is
why on earth would the big force show restraint? Because in a recent video, I
said for anybody that was hinging their hopes on international pressure to
restrain Israel, just because they snatched a bunch of people from going to
a bunch of different countries, that's really unlikely. And there were a whole
lot of people asking why, why would they be restrained?
Okay, so let's answer the first question first.
What were they hoping to achieve?
They wanted what they're probably about to get.
They wanted a security clampdown.
That's what they want.
That's the job of the little force when applying this theory.
The goal is to provoke the big force into an overreaction, into a security clampdown.
Why?
Because it makes sympathizers active and bystanders sympathizers.
When you are talking about this kind of conflict, it is a PR campaign with violence.
Nothing more.
It doesn't matter what flags you wrap around it.
doesn't matter how you church it up. That's what it is. So, anything that can
be perceived as a security clampdown is an overreaction. That generates sympathy
for the small force. That's why generally you have people calling for a
restraint. This doesn't make any sense. For most people, when they first hear
this, it doesn't make any sense because movies have told you the way you win is
go in there and, you know, knock people around. Here's a challenge. Show me when
that's worked. Show me when politically realigning the small force, shifting it
to a more political force through whatever means. Doesn't work. I hate to be
the one to break it to you, but give peace a chance when you're talking about this kind
of conflict, that's how you win.
And for the idea that massive use of force and to understand how it works, if you're
the U.S. How did our revolution start? The Boston Massacre, right? It was a big
catalyst. Overreaction, use of force. You're in Europe. Bloody Sunday, it's how
it works. You can say that it doesn't make any sense,
but I would point out that playlist is years old, and in the first few videos it lays out the theory.
You don't find exceptions to it. It's how it works. The goal of the big force should not be
to crush the opposition because that doesn't work. It doesn't work. The goal
should be to bring them to the table, to bring them out. That does work. I would
point out that when in Ireland, when they were brought to the table, is when
things calm down. The overwhelming use of force, it didn't work, it made it grow.
That plays out pretty much everywhere.
So they are probably about to get what they wanted and all it does is create
the next cycle, because now you'll have new active people, new sympathizers.
The playlist goes through the manuals, and to be clear, when I say manual, that's not
a metaphorical thing, it's not a plot device.
There are literal manuals.
I give the numbers, and you can download them if you want to read them.
I think when it started, I think we were talking about Trump dealing with the stuff up in the
Northwest and how his use of force was going to cause it to grow.
And then it did.
And then they backed off.
They talked to people and everything calmed down.
That's in the early, early phases.
But the same strategy gets deployed when things get way more serious.
That's what's happening.
So that's why there were a lot of people who want Israel to show restraint.
strategy of being cyclical and the responses back and forth, back and forth,
back and forth. It doesn't work. It doesn't work. It doesn't actually stop it.
So they wanted what they're about to get and one thing that I would like noted
because it's probably going to happen and it's going to be horrible. Those who
made that decision, those who decided to deploy that strategy, I'm willing to bet
they're somewhere safe. That doesn't change. When you're talking about little
force and big force, the little force of leadership, most of them, those who are
truly calling the shots, they're somewhere else.
It's not going to be them.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}