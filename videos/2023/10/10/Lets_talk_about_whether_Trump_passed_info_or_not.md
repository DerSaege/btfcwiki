---
title: Let's talk about whether Trump passed info or not....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=jGextw4ILdg) |
| Published | 2023/10/10 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing a theory on social media about former President Trump sharing sensitive information with Russian officials.
- Clarifying that the theory likely doesn't accurately represent the real concern.
- Emphasizing that safeguarding means and methods, capabilities, and how information is obtained is critical.
- Noting Trump's history of inadvertently disclosing sensitive information.
- Mentioning incidents where Trump may have revealed U.S. capabilities.
- Speculating on how information could have been passed from Russia to Iran without concrete evidence.
- Comparing the theory to recent events involving air defense systems.
- Explaining the importance of understanding the source and evolution of the theory circulating on social media.
- Stating the significance of safeguarding capabilities and methods rather than just the information itself.
- Mentioning the upcoming trial and the importance of understanding the theory in relation to it.

### Quotes

- "Understanding that the information itself isn't always what's important to safeguard is going to become significant, I assume, in Trump's upcoming trial."
- "It's how they obtain it. Means and methods. Capabilities. That information needs to be safeguarded."
- "The current versions floating around right now, super unlikely."
- "But you need to understand the theory."
- "Y'all have a good day."

### Oneliner

Beau clarifies a theory about Trump sharing information, stressing the importance of safeguarding capabilities over just the information itself, particularly in his upcoming trial.

### Audience

Social media users

### On-the-ground actions from transcript

- Understand the theory and its implications (suggested)
- Stay informed about evolving narratives on social media (suggested)

### Whats missing in summary

The full transcript provides detailed insights into the misconceptions surrounding a theory about Trump sharing sensitive information, stressing the need to safeguard capabilities over just the information itself.


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about something
that is spreading on social media.
And it's spreading in a way that is probably inaccurate
and it doesn't actually encapsulate the real worry.
So we're going to go through that.
If you missed it, there is a theory floating around
that former President Trump when he passed information he said things that
maybe he shouldn't have said to Russian officials that during that well he gave
them the plans Israel's defensive plans and Russia then provided that
information to Iran, and then it was used for recent events. That is probably not
what happened. That's probably not accurate, but the people who are talking
about this and where this originated, that's also not what they were saying.
They weren't saying that Trump provided a map of weak points. Remember,
we've talked about it on the channel multiple times. When spooky people are
doing spooky things. The secret isn't always the information they obtain. It's
how they obtain it. Means and methods. Capabilities. That information needs to
be safeguarded. Trump has shown more than once that he doesn't understand this
concept. This is why in the past he posted imagery that probably shouldn't
been posted. This is why he talked about how things were done in ways that made counterintelligence
people pull their hair out. And then you have the more recent allegation about the sub thing.
All of these are incidents where he put information out that betrayed capabilities.
What the U.S. is able to do or how it's done.
That information could have been passed.
Israel provided it.
Trump, showing off how much in the know he is, said it in a conversation.
He described their means and methods.
That is something that could have, in theory, been passed from Russia to Iran and then over
and then used.
I would point out, I have seen no evidence that this actually occurred.
But that is way more likely than him passing a map of how their defenses are set up or
something along those lines, which is what this is kind of transformed into as
it goes along. Him doing that is incredibly unlikely. Him talking too much
about capabilities and how things are done in a way that would allow the
opposition to exploit it. I mean, yeah, I could definitely see him doing that, but
But I also haven't seen any evidence to say that.
Another variation of this is that he told Russia how to get around the Iron Dome and
all of that stuff and then that got passed along.
That I don't think that's really there.
That appears to have been done the same way Ukraine defeated an air defense system recently,
which is just saturation.
Every air defense system, every automated defense system of pretty much any kind, it
has a threshold.
It only has so much capability.
If you put so much stuff up in the air at once, well, it can't hit at all.
This is wave after wave when we were talking about it before, the Futurama references.
Same thing.
That would be my guess as to how that was done.
I'm not saying this to defend Trump in any way, shape, or form.
I'm saying this because it is important for people to understand sometimes the information
itself is not really what's secret.
It's how that information was obtained.
It's the capabilities that are demonstrated by disclosing that information.
As an example, a U.S. satellite photo of a training camp in pick-a-bad-country, okay?
That photo, there's nothing secret in the photo, right?
They know what they built.
They built it.
But the clarity of that photo, the time it was taken, all of that stuff has to remain
concealed, because it betrays the capabilities.
It's that kind of information that a whole lot of people believe Trump may
have inadvertently passed on and then it been transferred over.
Understanding that the information itself isn't always what's important to
Safeguard is going to become important, I would assume, in Trump's upcoming trial.
So it's important for people to understand what that theory is, and understand why it
developed, and understand how it has changed as it has gone through social media. Because
the current versions floating around right now, super unlikely. Like, and I am not somebody who
would say Trump would never do that. I'm saying that I don't think he would even
have that information. Like he wouldn't have that with him. He wouldn't look at
the maps close enough to understand any of it. But how it was obtained or
something I believe the actual stated reason for the conversation had
something to do with Israeli intelligence going after a different
group. And if in that process he talked about how they gathered the intelligence
on them, that information is directly transferable to the current situation.
And it definitely could have been passed through the route everybody's talking
about. It's possible, but I haven't seen the evidence of it. But you need to
understand the theory.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}