---
title: Let's talk about the end of the Alabama map show....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=WN0cLJOiPi4) |
| Published | 2023/10/08 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Republican Party in Alabama and state officials attempted to use maps that diminish the power of black voters.
- The federal court appointed a special master to create alternative map options, leading to the approval of a new map.
- The approved map separates Birmingham and Montgomery, two cities with significant black populations, into different districts.
- The new map did not create a second majority black district but split the voting power across different districts.
- By separating the two cities, it is likely that another district will flip blue in the next election.
- Gerrymandering is a critical issue as it can significantly impact election outcomes by manipulating district boundaries.
- The intention behind drawing districts a certain way is to influence election results in favor of a specific party.
- The federal courts aim to prevent the Republican Party from ruling rather than representing fairly.
- The Alabama Secretary of State, Wes Allen, acknowledged that the court-forced map will be used in the 2024 elections.
- Opposition to fair maps raises questions about the intentions behind manipulating voting power within districts.

### Quotes

- "If it was a fair map, it shouldn't matter, right?"
- "That's why gerrymandering matters. That's why it has to be opposed."
- "The federal courts aren't trying to give the Democratic Party an edge. What they're trying to do is stop the Republican Party from ruling rather than representing."

### Oneliner

The Republican Party in Alabama attempted to use gerrymandered maps to diminish black voting power, but federal courts intervened to ensure fair representation and prevent partisan ruling.

### Audience

Alabama residents, Voting Rights Advocates

### On-the-ground actions from transcript

- Contact local voting rights organizations to understand how gerrymandering impacts elections and get involved in advocacy efforts (implied).
- Join community initiatives working towards fair redistricting processes to ensure equitable representation for all voters (implied).

### Whats missing in summary

The full transcript provides additional context on the political dynamics behind gerrymandering and the implications for fair representation in elections.

### Tags

#Alabama #Gerrymandering #VotingRights #FairRepresentation #PoliticalManipulation


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about Alabama and maps
one more time, and this should be the end of it, in theory,
because it does appear that that show is finally over.
A quick recap is basically the Republican Party in Alabama,
Alabama state officials, have been
trying to use maps that dilutes the power of black voters.
It went to court repeatedly to include two trips to the Supreme Court after the state
officials in Alabama were just like, no, we're not going to do what you said.
At that point, the federal court appointed a special master, and the special master was
told to provide a couple of different options for maps. Those maps were provided. The three
judge panel, I think it was three judges, chose one and we now have it. The short version
is the old map had Birmingham and Montgomery, if you're not familiar with Alabama, two of
three largest cities in Alabama. Incidentally, two cities that have a
large black population had them in the same district, even though if you were to
look at the old map you can see where they drew the district to encompass them
intentionally. So it put all of that voting power into one district. The new
map, the one that was approved by the federal courts, has them in different
districts. It does not actually create a second majority black district. It's like
48.7% or something like that. It's incredibly close, which was what the
ruling initially called for. So you have that in place now, and the Alabama Secretary of State,
Wes Allen, he went on to say that for the 2024 elections that that map would be used. What he
said was they would be run in accordance with the map the federal court has forced upon
Alabama, forced upon Alabama, why are Republicans so opposed to this map?
If it was a fair map, it shouldn't matter, right?
If they really had support of the people, it wouldn't matter.
But the reality is, by pushing all of black voting power into one district the way they
did, it made it very hard for them to be represented.
They were being ruled.
Breaking those two cities up, in all likelihood, means that there will be another district
and it will flip blue.
That district will probably flip blue during the next election.
That is why gerrymandering matters.
That's why it has to be opposed.
Something as simple as them drawing a district to include two large cities, and if you pull
up the map and look, you can see how there's little fingers that reach out to grab those
two cities and bring them into one district.
that might alter the outcome of the race.
That's why it was drawn that way to begin with.
The federal courts aren't trying to give the Democratic Party an edge.
What they're trying to do is stop the Republican Party from ruling rather than representing.
That's what was going on.
Now, the Secretary of State does appear to have basically said, okay, we've been to the
Supreme Court twice, we're not going to win this one. So we're going to basically
whine and complain and say, the federal government forced this map on us. Yeah,
they forced you to represent the people in your state. It's kind of your job. So
that's what's going on. Theoretically, this should be the end of this saga. There
shouldn't be more updates about this. It should be, it should finally be resolved.
At least, we hope so.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}