---
title: Let's talk about Trump endorsing Jordan when he didn't need to....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=VN0gx0_bXb8) |
| Published | 2023/10/08 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump endorsing Jordan for Speaker of the House is different and more consequential than his previous endorsements.
- Endorsing someone for Speaker of the House is a significant political move.
- Beau shares a story about an old man and a young guy, illustrating the consequences of a confrontation.
- If Jordan wins, Trump's support doesn't really gain him anything noteworthy.
- However, if Jordan loses, it signifies the Republican conference rejecting Trump's leadership.
- Trump has set himself up for a major loss by endorsing Jordan.
- There was no need for Trump to take this risk; he could have avoided it by staying silent.
- Trump's endorsement could potentially lead to negative consequences for him politically.
- The ads against Trump practically write themselves if Jordan loses.
- By endorsing Jordan, Trump has made a public statement to Republican members of the House that they need to pick Jordan.

### Quotes

- "Never fight an old man. If you win, you don't gain anything."
- "If he loses, it's a big deal. Nobody will let him forget it."
- "He gets nothing if he wins."
- "It's a huge, huge loss politically for Trump."
- "It's one of those things where you don't want to root for one side or the other."

### Oneliner

Trump's endorsement of Jordan for Speaker of the House could lead to significant political repercussions if Jordan loses, showcasing potential rejection of Trump's leadership within the Republican Party.

### Audience

Political observers

### On-the-ground actions from transcript

- Analyze and understand the potential political consequences of Trump's endorsement (implied).

### Whats missing in summary

The tone and nuances of Beau's storytelling and analysis can be best experienced by watching the full transcript. 

### Tags

#Trump #Endorsement #Consequences #SpeakerOfTheHouse #RepublicanParty


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about Trump endorsing Jordan
and a story about an old man and why this endorsement might be Trump's most consequential endorsement.
He's made a lot of endorsements over the years, but this one's different.
If he missed the news, he endorsed Jordan for Speaker of the House.
Endorsing somebody for Speaker of the House is a little bit different than endorsing somebody
in a campaign, in a primary, something like that.
It's very, very different.
Long time ago, there's this old guy, this young guy is kind of running off at the mouth
to him.
And I can tell that it's probably getting a little too heated, but my friend is like
no, don't worry about it.
And the young guy kind of indicates that he is ready to fight this old man.
Pop pop pop.
Old man lays him out.
Never fight an old man.
If you win, you don't gain anything.
You beat up an old man, good for you.
If you lose, nobody's ever going to let you forget it.
That's the situation Trump's in now.
If Jordan wins, okay, Trump supported the MAGA guy, and?
But if he loses, it's not voters.
It's not a situation in which he can scream that it was rigged or whatever.
It's the Republican conference rejecting Trump's leadership.
If he wins, he gets nothing.
If he loses, it's a big deal.
Nobody will let him forget it.
The people in the House, in the Republican Party, do not follow his leadership.
Why should you?
The ads write themselves.
Republicans know he's bad.
Why should you vote for him?
It probably would have been wise for Trump to stay out of this, but I don't know that
many people have ever accused Trump of being wise.
He has set himself up for a major loss that's totally unnecessary.
He didn't have to take this risk.
All he had to do was say nothing, but he gets nothing if he wins.
There's no benefit to him, politically speaking.
who were going to support Trump in the House, they would have supported him either way.
Those who won't, well, they weren't going to.
But now he's put it out there into the public sphere that he has told the Republican Party,
Republican members of the House of Representatives, that they need to pick Jordan.
If they don't, it is a huge, huge loss politically for Trump.
It's one of those things where you don't want to root for one side or the other, but
you would be happier with one outcome over the other.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}