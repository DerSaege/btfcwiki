---
title: Let's talk about Louisiana, deja vu, and maps....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=WG_q9W8QDa0) |
| Published | 2023/10/08 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republicans in Louisiana are resisting changing maps despite one-third of the state being black, but only one out of six districts having a majority black population.
- This scenario mirrors Alabama's situation, where the maps were found to violate the Voting Rights Act.
- Voting rights activists are pushing for more representative maps in Louisiana's democracy.
- Comparing Louisiana to Alabama and Wisconsin, Louisiana's gerrymandering situation seems less severe.
- The outcome of the maps, regardless of intent, can still dilute the voting power of specific demographics.
- The judges' views on the issue are uncertain, with their leanings not clearly indicated during the hearings.
- The judges' backgrounds suggest a mix of political affiliations, hinting at a potentially balanced perspective.
- The judges have yet to make a decision, leaving the situation similar to Alabama's storyline.
- Despite uncertainties, it appears unlikely that Louisiana Republicans will outright disregard any rulings.
- The final verdict on the maps in Louisiana remains unclear, and the outcome is uncertain.

### Quotes

- "The outcome is still the same."
- "It does not seem like Republicans in Louisiana are just going to defy, you know, the rulings."
- "Anyway, it's just a thought."

### Oneliner

Republicans in Louisiana resist changing maps despite diluting voting power, mirroring Alabama's situation, as judges' leanings remain uncertain.

### Audience

Louisiana residents, voting rights activists

### On-the-ground actions from transcript

- Advocate for more representative maps in Louisiana's democracy (implied)
- Stay informed about the ongoing legal proceedings regarding the maps (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the gerrymandering situation in Louisiana, shedding light on the potential implications for voting rights and representation.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Louisiana
and maps and Republicans and fractions and deja vu.
And this is gonna sound real familiar
if you paid attention to some recent coverage.
Okay, so Republicans in Louisiana
were in front of a federal appeals court
and they were talking and arguing about maps.
They don't want to change them.
So Louisiana is one third black.
However, only one out of six districts
has a majority black population.
Sounds like Alabama, right?
Sounds like that whole storyline,
because it's pretty similar.
and it actually factors into it and came up in the discussions.
Because basically what they're trying to figure out is whether or not
Louisiana needs to follow the guidance that came down from the Supreme Court
when they decided that Alabama's maps likely violated the Voting Rights Act.
It's the same story playing out again.
You have a third of the state, and it's being, I guess,
represented by one sixth.
So obviously, voting rights activists and advocates
are trying to get the maps to be more
representative in our representative democracy.
Republicans are opposed to this.
So one of the questions that came in after this news broke
was from somebody in Louisiana.
And I looked into it, and I talked to somebody
from Louisiana to answer this question.
Is Louisiana as bad when it comes to gerrymandering
as Alabama or Wisconsin?
Not really.
When you look at the maps, no.
Sometimes you can look at a map and you're like, wow, that's gerrymandered.
After talking to somebody from Louisiana and looking at the map, they seem to be under
the impression that you could actually come up with the Louisiana map trying to keep communities
of interest together and being, quote, colorblind.
Not paying attention to that as a factor, like in any way, and you could still come
up with a map pretty close to this.
I don't know that I entirely believe that, I think that's just too convenient, it takes
a lot of work to make that kind of coincidence happen.
But it is not as bad as Alabama's was, it's not as bad as Wisconsin's is, but the outcome,
No matter how the map was created, even if it was created with good intentions, the outcome
is still the same.
Communities of interest, keeping communities of interest together, that's important.
However, if that is used as an excuse or even just by happenstance, it dilutes the voting
power of a specific demographic by this much you have to realign the maps. I mean
you should. Now as far as what the judges are gonna say at time of filming we
don't know and normally during these during these hearings you can tell the
way they're leaning based on the questions they ask. In this case the
judges were like, yeah, don't try to do that right now. And the judges themselves,
from what I was able to find with a quick glance, you really can't tell. Two
of them were appointed by Bush, one by Carter, and they seemed kind of middle of
the road at first glance. Now, I didn't dig into every single ruling, but
Normally, I have a hunch on how it's going to go.
In this case, I really don't.
In some cases, the same judge kind of sent mixed signals about what they were thinking
at the time.
The judges themselves may not have decided yet, but it's the same storyline as Alabama.
It's playing out in the exact same way.
It does not seem like Republicans in Louisiana are just going to defy, you know, the rulings.
But I mean, I guess we'll wait and see what happens.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}