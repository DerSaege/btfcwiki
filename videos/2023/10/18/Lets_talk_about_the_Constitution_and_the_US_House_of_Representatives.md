---
title: Let's talk about the Constitution and the US House of Representatives....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=3W5OlqifRT8) |
| Published | 2023/10/18 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the dynamics in the House of Representatives regarding choosing a Speaker, debunking claims of Democratic Party blocking Republicans.
- Republican Party unable to unite behind a candidate, leading to Democrats voting for their candidate who is getting more votes.
- Addresses the false claim that Democrats should help Republicans choose a Speaker, clarifying that the entire House should do so as per the US Constitution.
- Criticizes the entitlement of the Republican Party expecting Democratic help to choose a Speaker, indicating a broken system.
- Emphasizes that normally the Speaker is from the majority party because they are more functional, not because it's a constitutional requirement.
- Points out that Republicans are trying to shift blame for the failure by targeting their base's lack of understanding of the process.
- Suggests that if Jeffries is receiving the most votes, perhaps Republicans should cross over to end the situation.

### Quotes

- "The Republican Party is unable to put up a candidate that Republicans can unite behind."
- "The Democratic Party is voting for their candidate. That's what's occurring."
- "The US Constitution is pretty clear about this. The House of Representatives chooses the speaker not the majority party."
- "The only reason they can make [talking points] is because they believe their base is too ignorant to understand the process."
- "Maybe Republicans should cross over and end this."

### Oneliner

Beau breaks down House of Representatives dynamics, debunking claims of Democratic obstruction and criticizing Republican entitlement in choosing a Speaker.

### Audience

Politically engaged citizens

### On-the-ground actions from transcript

- Contact your representatives to express support for transparent and fair processes in choosing a Speaker (suggested)
- Stay informed about House of Representatives proceedings and hold elected officials accountable for upholding democratic principles (implied)

### Whats missing in summary

A deep dive into the nuances and implications of House of Representatives dynamics and the impact of party politics on governance.

### Tags

#HouseOfRepresentatives #SpeakerSelection #USConstitution #DemocraticParty #RepublicanParty


## Transcript
Well, howdy there, Internet people, it's Beau again.
So today we're going to talk about
the United States House of Representatives
and the United States Constitution
and some talking points that are coming out
and how all of them interplay
as things move along to choose a Speaker of the House.
Well, I guess as things don't move along
to choose a Speaker of the House.
So, the first talking point that we're going to go over is the repeated claim that the
Democratic Party is blocking the Republicans from choosing a speaker.
The reason the Republicans are saying this is because they believe their base is too
ignorant of the Constitution to understand how it works.
There is no vote to block.
That's not an option.
The Democratic Party is voting for their candidate.
The Republican Party is unable to put up a candidate that Republicans can unite behind.
That's how that works.
That's what's occurring.
It's worth noting that the Democratic candidate is getting more votes.
That's not the Democratic Party voting no on a chosen Republican speaker.
That's not how it works.
They made that up.
They're lying to you.
The other one that has come out, and this was broadcast on national TV, is that the
Democratic Party should help the Republicans choose their speaker.
Help the Republicans choose their speaker.
Weird.
Because I'm pretty sure in the Constitution, it doesn't say that the speaker is a member
of the majority party. It says the US House of Representatives, the whole House
of Representatives, shall choose a speaker. The Republican Party is so
entitled that they feel the Democratic Party has to come to their rescue to
help them choose a speaker. That's not really a message that I would want to
send out to people this party is so broken right now it can't even govern
the house much less govern the country. The US Constitution is pretty clear
about this. The House of Representatives chooses the speaker not the majority
party. Yeah, generally speaking, the speaker is from the majority
party. Why? Because normally the majority party isn't broke. They
are interested in actually fulfilling the purpose of
government, not getting clicks on Twitter. That's what's going
on. The talking points that are going out from Republicans trying
to shift blame for this failure.
The only reason they can make them is because they believe their base is too ignorant to
understand the process, too ignorant to understand the Constitution.
There's no other way you could make those.
And as far as Democrats crossing over to help Republicans choose a speaker, I'd remind
everybody that it is Jeffries that's been getting the most votes.
So maybe Republicans should cross over and end this.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}