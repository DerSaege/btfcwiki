---
title: Let's talk about Biden getting on Truth Social....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=TzBD0ylpnjQ) |
| Published | 2023/10/18 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Biden campaign joined Truth Social, a social media site associated with Trump, known for his supporters.
- It seems odd for the Biden administration to be present on a platform dominated by Trump supporters.
- The move appears to be a mix of humor, innocent poking, and strategic impact.
- Trump's departure from the Republican Party's "11th Commandment" of not speaking ill of another Republican led to public mudslinging among Republicans.
- The Biden administration might leverage clips of Republicans criticizing other Republicans to spark engagement and arguments on Truth Social.
- By stoking infighting among Republicans on the platform, the Biden administration could influence the elections.
- This strategy involves trolling the Republican Party into arguing with itself to potentially sway voters away from certain Republican candidates.
- The Biden administration seems to have social media experts planning to exploit the platform for strategic purposes.

### Quotes

- "Seeing Republicans talk bad about other Republicans is a little different than a Democrat doing it through Republican eyes."
- "There's probably something a little bit more strategic going on because they can create a lot of red on red fire."
- "They plan on trolling the Republican Party into arguing with itself."
- "It will probably also have an impact on the elections."
- "If they do it with enough frequency and they time it to coincide with the infighting that is naturally occurring within the Republican Party, it would probably be super effective."

### Oneliner

The Biden administration strategically leverages Truth Social to spark red-on-red fire within the Republican Party and potentially influence elections.

### Audience

Social media strategists

### On-the-ground actions from transcript

- Strategically leverage social media platforms to spark engagement and influence political discourse (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the Biden administration's strategic move to join Truth Social and incite infighting within the Republican Party, potentially impacting elections.

### Tags

#Biden #TruthSocial #RepublicanParty #SocialMediaStrategy #Elections


## Transcript
Well, howdy there, internet people, let's bow again.
So today we are going to talk about Biden and the Biden campaign and truth
social and why that's happening beyond it's funny
and how it might actually have an impact
of some kind beyond just the current headlines.
OK, so if you missed it and you have
I have no idea what I'm talking about.
The Biden campaign has set up an account on Truth Social.
Truth Social is the social media site that is branded with Trump.
It's his thing.
Everybody there is basically a supporter of his.
So it seems like a weird place for the Biden administration to show up.
The first reason they did it is obviously it's funny.
The second is probably just a little bit of innocent poking.
The fact that this is occurring at the same time as Trump being hit with a gag order.
That's probably not a coincidence.
But then there's something that might actually have an impact.
See prior to Trump, the Republican Party had this thing called the 11th Commandment.
shalt not speak ill of another Republican." They didn't publicly sling mud at each
other. Trump started that. He did that because, well, let's be honest, that was
his entire platform. So once he started, other Republicans responded in kind and
they've continued to do it. So there are a whole bunch of clips out there of
Republicans talking bad about other Republicans. Tons. Libraries full. And I'm
sure the Biden administration has them. My guess would be that's going to be a
lot of what the Biden administration posts. When that happens, because
basically everybody on the site is a Republican or to the right of
Republicans, they're going to comment, they will take sides, they will argue.
When they do this in the comment section, it increases engagement, which means more
of them will see it.
Seeing Republicans talk bad about other Republicans is a little different than a Democrat doing
it through Republican eyes.
So my guess is beyond the obvious dark Brandon humor that's going on.
My guess is there's probably something a little bit more strategic going on because they can
create a lot of red on red fire.
They can feed into arguments.
It appears that somewhere in the Biden administration, there are people who are pretty good at social
media, and they plan on trolling the Republican Party into arguing with itself which, I mean,
that's funny in and of itself, but it will probably also have an impact on the elections
because those who are super supportive of Trump, as an example, well they may not like
Crenshaw. So posting videos of Crenshaw saying things that go after a Republican
that is aligned with Trump, well that may cause Republicans that would vote for
Crenshaw to not. There's, I think that this is a little bit more than just
simple humor. I think there's a little more to it and if they do it with enough frequency and they
time it to coincide with the infighting that is naturally occurring within the Republican Party,
it would probably be super effective anyway it's just a thought y'all have a
good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}