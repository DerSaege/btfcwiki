---
title: Let's talk about rumor about Ukrainian aid....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=eq7pu0ol724) |
| Published | 2023/10/29 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing a persistent rumor about American rifles being sent to Ukraine and secretly shipped to Palestine.
- Explaining the confusion around the connection between American-made rifles and Palestinian groups.
- Pointing out that the rifles in question are actually Iranian-made clones of American weapons.
- Mentioning a popular US company in the 90s that used to sell similar weapons but is no longer allowed to.
- Warning about the potential for US-made weapons sent to Ukraine ending up in Palestine due to the spread of this rumor.
- Noting how the rumor could be exploited by Russian intelligence to undermine US support for providing aid to Ukraine.
- Emphasizing the real-world effects of commentary and rumors, even if they may not impact the fighting directly.
- Clarifying that while some weapons found in Palestine may be older American-made ones, the newer ones are from Iran.
- Speculating on the possibility of captured US weapons being planted in Palestine by Russian intelligence.
- Urging caution against being misled by rumors and the desire to provoke outrage.

### Quotes

- "Just a thought y'all have a good day"
- "The overwhelming majority, especially the newer stuff, that's made in Iran, not made in the USA."
- "But the rumor, the desire to provoke outrage, it's definitely going to make Russian intelligence do this."

### Oneliner

Addressing a persistent rumor about American rifles being sent to Ukraine and secretly shipped to Palestine, Beau clarifies that the rifles in question are actually Iranian-made clones of American weapons, warning about the potential implications and exploitation by Russian intelligence.

### Audience

Internet users

### On-the-ground actions from transcript

- Search for accurate information about weapon origins (implied)
- Avoid spreading unverified rumors (implied)

### Whats missing in summary

More details on the potential consequences of spreading false information and the impact on international relations.

### Tags

#Rumor #AmericanRifles #Ukraine #Palestine #IranianWeapons #RussianIntelligence


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about a rumor,
a rumor that just will not go away,
and a connection that's getting made
that maybe the dots aren't actually there
to make the connection yet,
but they will be later
because people tried to make them now.
It's a really weird situation,
and it's worth going over
because you will definitely see this material again later.
Okay, so here's the question.
Bo, I'm wondering if there's any truth
to this story I keep hearing
because I keep seeing gun nut guys
who say they're experts on the internet
saying they've seen them.
Are American rifles sent to Ukraine
being secretly shipped to Palestine?
No.
No, that's not happening yet.
It will later, and we'll get to that.
But what's occurring is people are seeing an image,
and they're like, hey, I know what that is.
And then they're connecting dots, and they're saying,
well, the US gave a bunch of them away in Ukraine,
so they obviously came from there.
Before we get to the technical stuff and the fun stuff,
let's start with the easy stuff.
The name of the group that everybody thinks has them, type it into Google.
The Palestinian group that everybody's saying has them.
Type it into Google and type in the word patch afterward.
Click images.
That's their logo.
It has an M16 on it.
But it's not actually an M16, not really.
So Palestinian groups, they have had weapons that look like American-made M16s or M4s for
quite some time, long enough to where it's on their logo.
But they're not American-made.
What people are seeing the most right now is not an M4.
I know it looks like an M4, but it's not.
It's a Fajr.
Iran makes a clone of the M4.
They also make one of the M16.
I can't remember the name of that one, but it looks like an M16A2.
I want to say they've been doing it since the 90s, and if I'm not mistaken, there was
a company that was pretty popular in the US back in the 90s, used to sell here but is
no longer allowed to because if I understand the story correctly they
might have sold some to well countries that the US really didn't want to have
them. So that's actually where they're coming from. Iran is an open and known
supplier of these groups and they make the weapon that they have. For some
people it is an honest mistake you know they're they're looking at it and they
look very similar if you were to take an M4 and a Fajar and put them next to
each other I mean most people the average shooter would not be able to
tell the difference just by looking at them I mean they're they're incredibly
similar. So, but that's where they're coming from. Now, but in the future there
will absolutely be US-made weapons that were sent to Ukraine that end up there.
Guaranteed. Because while people were running around with this rumor and
provoking outrage and getting clicks. They made the story but they also made
a ready-made Russian intelligence operation. It provoked outrage and the
outrage kind of led people to say well we shouldn't be sending Ukraine stuff. If
you were Russian intelligence what would you do with captured weapons? You wouldn't
doing your job if you didn't send them there.
Just remember that commentary and rumors like this, sometimes they have real world effects.
Now realistically, as far as the fighting there, it doesn't matter.
There's no shortage of small arms there.
That's not something that is in short supply.
But I would be very surprised if some didn't show up and then they're found in like an
abandoned truck or something.
So the numbers get ran and all of that stuff.
But what you're seeing now, they're not American made.
Now some of them may be from like 30 years ago or something like that, but the overwhelming
majority, especially the newer stuff, that's made in Iran, not made in the USA.
And if you wanted to see this stuff and make a comparison, I'm sure you could go to Wikipedia
and type in like Iranian infantry weapons or something like that and you'd find them.
there it's not a secret it's just not the well-known I guess but the rumor the
desire to provoke outrage it's definitely going to make Russian
intelligence do this I would be shocked if they didn't if they don't somebody
needs to get fired because it's it is the perfect way for Russian intelligence
to try to undermine U.S. support for providing aid to Ukraine.
In fact, I wouldn't be surprised if the initial rumor came from them.
Anyway, it's just a thought y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}