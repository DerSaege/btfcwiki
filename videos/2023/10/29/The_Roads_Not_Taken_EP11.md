---
title: The Roads Not Taken EP11
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=6FCXiqUkc7A) |
| Published | 2023/10/29 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces "The Roads Not Taken," a series to address unreported or under-reported news from the previous week, providing context for upcoming events.
- Acapulco faces devastation from Hurricane Otis, a Category 5 hurricane causing significant damage and the need for aid.
- The US Commerce Department temporarily bans most US-made firearms exports due to foreign policy concerns about falling into the wrong hands.
- Israel enters the second stage of war with an expected large-scale ground offensive in Gaza, independent of US influence.
- Elon Musk announces providing internet service in Gaza for relief organizations, vital for communication during conflicts.
- Negotiations facilitated by Qatar between Israel and Palestinian groups continue with uncertain outcomes.
- Saudi Arabia sends its defense minister to the White House to prevent the conflict from expanding regionally, showing significant involvement and influence.
- Palestinian forces seek assistance from external actors as tensions rise, risking potential escalation beyond current levels.
- In Crimea, reports suggest Oleg, a potential candidate favored by Putin, has been targeted, reflecting internal struggles within Russia.
- Mike Pence suspends his presidential campaign, General Motors faces labor strikes, and Biden's administration prepares for AI-related legislation.

### Quotes

- "Climate change is real and it's here."
- "The US Commerce Department has banned exports of most US-made firearms temporarily out of fear that they might undermine US foreign policy interests."
- "If we get out of this without it turning into a regional conflict, it will be because a whole lot of people put in a whole lot of work and they got really lucky."
- "It really, the United States, from a foreign policy perspective, gets a country in the region that absorbs most of the anger."
- "So much for ending that on a light note, which is what we're supposed to be doing now."

### Oneliner

Be informed about global events and foreign policy decisions, from hurricanes to international conflicts, with insights on gun exports and regional tensions.

### Audience

Global citizens

### On-the-ground actions from transcript

- Provide aid and support to communities affected by Hurricane Otis in Acapulco (implied).
- Stay informed on international conflicts and support efforts to prevent regional escalations (implied).
- Advocate for peaceful resolutions and humanitarian aid in conflict zones like Gaza (implied).
- Support organizations working towards climate change awareness and mitigation efforts (implied).

### Whats missing in summary

Insights into current international events and foreign policy decisions, offering a nuanced perspective on global affairs beyond mainstream reporting.

### Tags

#GlobalEvents #ForeignPolicy #GunControl #HumanitarianAid #ClimateChange #InternationalConflicts #CommunitySupport


## Transcript
Well, howdy there, internet people, it's Beau again.
And today on The Roads with Beau,
we will be doing another episode of The Roads Not Taken,
which is a weekly series
where we go through the previous week's events
and we talk about news that was unreported or under-reported
or it was reported on,
but it needs to be repeated because it's context
for events that are going to occur in the coming week.
And then at the end of it, we go through
and I answer a few questions from y'all.
If you're interested in sending a question,
the email address is questionforboe at Gmail.
And that's for F-O-R. OK, so starting off
with foreign policy.
Acapulco was hit by Hurricane Otis, a Category 5 hurricane.
It caused a massive amount of damage and disruption.
Aid is desperately needed,
but because of other events in the world,
it is being overshadowed.
The toll is currently at 38, it is expected to rise.
One thing to note about this is that Otis
was another hurricane that grew just enormously in strength
right before it came ashore.
It's at the point where we just need to start anticipating
that with hurricanes.
It can't go off of the numbers the way that we used to.
Climate change is real and it's here.
The US Commerce Department has banned exports of most US-made
firearms temporarily out of fear that they might undermine US foreign policy interests.
They're worried about them falling into quote the wrong hands.
The rule applies to new export licenses and it exempts the countries that you would expect.
This doesn't apply to Ukraine or Israel or probably three dozen, more or less, other
countries that have entered into arms control agreements.
So submissions that companies are turning in now, they're basically just going into
an inbox where they'll wait.
The expectation is that this is going to be done for 90 days.
If this is being done for the reason that seems very, very apparent, I think 90 days
is incredibly optimistic.
Okay.
Israel is saying it has entered the second stage of the war, which is expected to include
a large-scale ground offensive. U.S. officials have said, quote, make no
mistake, what is, has, or will unfold in Gaza is purely an Israeli decision. It
seems pretty clear that the Israeli government did not take the advice that
the advisors presented. That seems painfully evident at this point. I know
that even after we talked about it on the other channel there were still
people that were like no they're going there to advise and tell them how to do
this invasion and and they're gonna be there to supervise and all of that stuff.
It is worth noting that my understanding is they're already on their way home. It
It certainly seems as though they went over there, they made their pitch, and they were
politely told, Israeli forces are already committed to a course of action.
Have a nice day.
That certainly seems to be what occurred.
Elon Musk says his company will provide internet service to internationally recognized relief
organizations in Gaza.
That's good because while right now at this exact moment the phones and internet are back
up, we have no idea how long that's going to last.
There are military reasons for cutting communications to an area.
It is generally considered best practices to avoid doing that for extended periods because
while, yes, it disrupts your opposition's communications, it also disrupts the ability
to call for an ambulance.
So there's that.
Negotiations that are being facilitated by Qatar for most Americans between Israel and
Palestinian groups are continuing, but there is no indication of how they're going.
The one bit of good news that we have in this bunch, the Saudi defense minister will be
visiting the White House to discuss ways to avoid this becoming a regional issue, basically
trying to figure out how to stop the conflict from expanding.
The reason this is good news, A, Saudi Arabia, they have a whole lot of pool in the region.
They have a whole lot of influence.
So them apparently understanding the situation, that's good.
The fact that they're sending their defense minister, not some envoy, not some assistant,
but the defense minister.
a really good sign because normally that buffer person, the negotiator, the envoy, whatever,
their real job is to talk about it, present their country's case and then be like, oh,
I love what you're saying, but I have to talk to my boss.
They're sending the defense minister.
That's a really good sign.
It indicates they understand how sideways this can go.
And on that note, Palestinian forces have openly called for assistance from outside
actors.
If you watched the video on this channel, the very last one, the one that came out just
before this one, they're calling their allies.
So far, nobody's picking up the phone.
calls are going unanswered for the most part. There's some light stuff, but nothing major.
But each one of these calls carries the risk of it expanding. And to clarify again, this
can expand, I think to a degree, more than most Americans want to admit. This could get
real bad.
And this is coming from the same guy who, throughout the conflict in Ukraine, every
other week there was a news story, this is how it's going to expand, and the next day
there would be a video from me.
No, it's not, and this is why.
This one can expand.
And that is the top priority.
Honestly, if we get out of this without it turning into a regional conflict, it will
be because a whole lot of people put in a whole lot of work and they got really lucky.
Okay, switching conflicts.
In Crimea, old Oleg, it looks like he took a round.
So there's nothing known about his condition, but this...
He was once viewed as the most likely, I do not have a nice word for this, the most likely
candidate that Putin would install.
And he has been hit.
So there's a whole bunch of reporting, and it's basically Russia is said to be shooting,
retreating troops or those who are disobeying orders.
Looking through the claims, yes, it has occurred.
I don't know that it's quite a pattern large enough to say that it's policy yet.
It's happened and it's happened more than once, but it could be a chain of pretty isolated
incidents at this moment based on what I've been able to actually visually confirm.
So this is one of those moments where you're kind of seeing the ideal propaganda win.
What's being said, it's true, it happened.
The framing of it as in these are orders that came from Moscow, I can't confirm that.
In related news, Russia is engaging in an attempt to turn the tide and make some territorial
gains.
It's not really going well for them.
They've been unsuccessful for the most part, and the reported losses are being measured
at the brigade level.
That's a number in thousands.
brigade structure is variable at minimum 2,000 up to 8,000 and we are talking
about these losses in a very short period of time. Okay, on to US news. Mike
Pence announced the suspension of his presidential campaign. The United Auto
workers are winning their fight. General Motors is now the only company that
hasn't kind of come around and the strike against GM is expanding. Santos
entered a not guilty plea on his newest charges. The trial is set for September.
Biden has an executive order coming out about AI. I haven't read the whole draft yet. It's more
than a hundred pages, but a scan of it, it really looks like it's gearing up for legislation that
they assume is coming. It looks like more like they're setting up offices to monitor
and assess risks and that makes sense because the executive branch can't
really just start regulating without authorization from Congress but it looks
like my guess is the Biden administration got tired of waiting for
Republicans to get their act together in the House and put this together to kind
be ready when the inevitable legislation comes.
Trump believes Pence should publicly endorse him since he dropped out of the race.
Because I had a great successful presidency and he was the vice president, he should endorse
me.
I chose him, made him vice president.
people in politics can be very disloyal. I've never seen anything like it. I've
never seen anything like this quote. You know, reading this and having a memory
that, you know, goes back to 2019? This is wild. You put this in the
context of January 6th and that is just such a a very bizarre statement. Okay
moving on to cultural news Matthew Perry was found dead. That's the that's the guy
from Friends if you don't know. Right-wing commentators almost immediately
started a conspiracy theory that it was because he was vaccinated. It's being
described in reporting and by local authorities as an apparent drowning. In
environmental news, something that you might not expect to be in environmental
news. Open AI has launched a new team of employees and it's discussed in their
blog. They have put together a preparedness team to assess risks and
talk and kind of look into the dangers that AI poses to avoid catastrophic
issues like nuclear annihilation. I'm not joking, CBRN is actually mentioned in
their blog. So there's that. I really hope that this new team of employees does
not let their employer down. Okay, moving on to odd news. Right-wing commentators
are currently upset that a drag artist hit number one on the Christian music
charts. This becomes even more interesting or entertaining when you learn that it was months
ago, that this happened months ago. But apparently, many people who are very vested in the Christian
music scene went months without noticing that Flamy Grant was topping the music scene, pun intended.
Flamey Grant is yes that's a reference to Amy Grant. It seems like if this was
something you were interested in you you might notice before now I mean the name
Flamey Grant really isn't uh I mean that's not really hiding a whole lot
just saying. In other odd news, Australia has apparently decided to rid a park of
wild horses or at least reduce their number by shooting them from the air. May
you have as much luck as you did with your war on emus. For Americans just
Google emu war. Okay, moving on to the questions. What's the difference between
MAGA, Ultra MAGA, and Christian nationalism? See, I have a very snarky
response to this, but I don't know if this person wants a real answer or not.
Christian nationalism exists independently of MAGA. But so not all Christian nationalists are MAGA.
All MAGA except Christian nationalist talking points. Ultra MAGA is just the development of
of what happens in an authoritarian culture,
where you have to become more and more pure.
And that's just the natural outgrowth of that.
They're all very much interlocked.
If I was to give them uniforms, I
would say that MAGA had brown clothing,
Ultra-mega had gray, and Christian nationalism had black.
Let's see.
What does this say?
This isn't good.
A bunch of people have asked what
the US gets out of the relationship with Israel.
Foreign policy question.
OK.
it's all about power, right? See, this is one of those moments where it gets
uncomfortable. In the Middle East, most countries in the Middle East, especially
when this relationship started, they didn't like the US. Having a country that
is allied with you, in the middle of a bunch of countries that doesn't like you.
It's kind of like having a lightning rod.
That's what the U.S. gets out of it.
Sure, there's intelligence sharing and stuff like that, but that's about it.
It's not like the U.S. uses Israel to launch from because, like when you're talking about
major operations, because of the regional view of Israel, that just makes it worse.
So it really, the United States, from a foreign policy perspective, gets a country in the
the region that absorbs most of the anger. That's the main benefit to the US. Again,
it's all very cynical. You seem to be very sympathetic to the Palestinians, and I feel
for them but you have to admit that they could have avoided this by throwing Hamas out.
So in this case, they're obviously talking about the civilians because they would be
throwing out the militant force.
Let's just see how easy it is for one of the most advanced militaries on the planet
to do it.
Let's start with that.
And then we'll talk about whether or not a whole bunch of people with no assistance,
because they wouldn't get any, that nobody would help them.
Like as far as with military aid, nobody would assist them with no money, barely making it
as it is, if they would be able to just simply throw them out? Yeah, let's see
how one of the most advanced militaries in the world handles it first. The guy in
main used a different kind of rifle and there's some descriptions here of of the
scene apparently it's a higher powered it's a higher powered than normal AR is
that the one you warned us about in those videos? Unfortunately, no. No, it's
not. So the normal AR is 223. The one that the guy in Maine used was 308. So that
is it brings it into a new category and this is something I've talked about for
years the AR the normal AR is not a high-powered rifle it doesn't matter how
many times CNN says that it is it's not it's low intermediate a 308 is a that's
That's a full power charge, that's a full power round there.
And so much so that it takes it out of the normal category.
This and just to avoid the debates that will undoubtedly arise if terminology is wrong,
is a battle rifle. And you can define that as a full powered round in a
semi-automatic rifle that has a detachable magazine that holds 10 to 20
rounds. That's a battle rifle. And that's what he had in an AR platform. Using
And that terminology will avoid the whole deflection of, I don't know what an assault
weapon is.
But no, it's not the same.
This is more powerful than the normal AR.
It is not as powerful as the new rifle that the Army will be getting.
because of the way gun culture is, a whole bunch of people
are going to want that new rifle.
So sadly, no, it isn't.
It's more powerful, but it's not the same.
That's the same one.
OK.
And that is all of the questions.
OK.
Wow, so much for ending that on a light note, which
is what we're supposed to be doing now.
Recently, somebody said that I needed to tell a ranch story
in every one of these.
And I don't know that this is exactly a rant story,
but it's something that's kind of entertaining.
I put a lot of Easter eggs behind me on the shelves,
just little items to reference other things.
And a lot of them are props, for lack of a better word,
from horror movies, because, frankly, it's
fitting for what I talk about most of the time.
I planned on ordering the little statue from the exorcist.
And my wife, who is not a superstitious person
in any way, shape, or form,
she basically acted like a Kansas mom
and I was trying to bring a Ouija board
into the house or something.
She was basically like, no, you do not order that.
Do not bring that thing here.
I found out that apparently when she was younger that that movie did a number on her and she still hasn't recovered
But so we will
Will end on that instead
So I hope this provides a little bit more information a little bit more context and having the right information
Information will make all the difference y'all have a good night
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}