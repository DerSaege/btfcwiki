---
title: Let's talk about the Trumps and testimony....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=m9Y9TeGMXx4) |
| Published | 2023/10/29 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Ivanka Trump is set to testify in the New York case next week.
- She was successful in getting removed from the case but has to testify about a specific deal.
- The judge pushed back the potential testimony to allow time for an appeal, indicating a chance of success.
- Trump's older sons are expected to testify, followed by Trump himself the week after.
- The legal proceedings are moving surprisingly quickly, contrary to initial expectations.
- There may be significant media coverage and sensation surrounding Ivanka Trump's testimony.
- The New York civil case is expected to have big developments in the upcoming weeks.

### Quotes

- "We will be talking about Ivanka Trump because some developments in the New York case are leading to her having to testify."
- "Next week and the week after are likely to be big weeks in the New York civil case."
- "Ivanka Trump does have to testify. I expect a circus because of the perceived separation between Ivanka and the rest of the family."

### Oneliner

Ivanka Trump is set to testify in the New York case, signaling significant legal developments and media attention in the upcoming weeks.

### Audience

Legal observers

### On-the-ground actions from transcript

- Stay informed about the developments in the New York case and the testimonies of key individuals (implied)

### Whats missing in summary

Insights into the potential implications of Ivanka Trump's testimony and its impact on the legal proceedings. 

### Tags

#IvankaTrump #NewYorkCase #LegalDevelopments #Testimony #MediaAttention


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump, but not the one we normally talk about.
We will be talking about Ivanka Trump because some developments in the New
York case are leading to her having to testify.
It was ruled that she would have to testify if need be next week.
And that's the news that's going out.
The short version of this is that she was successful in getting herself removed from
this case.
However, the AG's office successfully argued that she has knowledge about one deal in particular.
I think it's something to do with an old post office or something.
So she will have to show up to testify about that.
The thing about the way it's being reported is that most places are acting like this is
a done deal, it's probably worth noting that the judge kind of pushed the
potential testimony back a little bit to to give her time to appeal and I feel
like that was done because he thinks the appeal will be successful, that
there's a decent chance of that occurring. Regardless of whether or not
Ivanka Trump testifies next week, we are expected to hear from Trump's two older
sons and they will be testifying. And then the week after is supposed to be
Trump himself. I am, I'm surprised at the speed at which this is going. You know,
they said that it was going to take until Christmas and all of that stuff,
and I said I didn't think that was likely once the determinations were made,
but I didn't think it was going to move this quickly. They seemed to be getting
through this at a surprisingly brisk pace.
And I don't know if that is because the Attorney General's office is very confident or there's
just not a lot of delays, which might have been expected because, well, it's Trump.
That's the legal strategy that he generally employs.
So next week and the week after are likely to be big weeks in the New York civil case.
We'll hear a lot of testimony.
Undoubtedly, there will be a ton of coverage, particularly if Ivanka Trump does have to
testify.
to testify I would expect a circus because of the perceived separation
between Ivanka and the rest of the family. There's a pretty high
probability that the media is just going to be all over that and turn it into
something sensational even if it's not. Anyway it's just a thought. Y'all have a
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}