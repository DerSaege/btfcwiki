---
title: Let's talk about Georgia maps changing....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=pH_b0tO2Q4w) |
| Published | 2023/10/29 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Georgia's maps are being redrawn following a federal judge's ruling that Republican legislators diluted the voting power of Black Americans after the 2020 census.
- The judge ordered the creation of new maps to be in use for the next election, including additional majority Black districts.
- The governor of Georgia surprisingly scheduled a special session to draw up new maps on November 29th, complying with the law.
- Republicans in Georgia seem to be accepting the need for new maps, with the situation potentially coming to a close.
- The judge specified that a new congressional district will be in the western part of Atlanta, being very meticulous in the order.
- The resolution of this situation depends on avoiding Republican shenanigans, with hopes for everything to be settled before the next election.

### Quotes

- "Republicans comply with the law that it's kind of surprising and noteworthy when they do."
- "The judge actually said that the new congressional district was going to be in the western part of Atlanta."
- "It looks like the Republicans in Georgia are just like, well, okay, I guess we have to draw new maps."

### Oneliner

Georgia's maps are being redrawn to address the dilution of Black voters' power, with Republicans surprisingly complying, potentially resolving the issue before the next election.

### Audience

Georgia residents, Voting Rights Advocates

### On-the-ground actions from transcript

- Contact local voting rights organizations to stay informed and involved in monitoring the redrawing of maps (implied).
- Attend local community meetings or town halls to ensure fair representation during the map redrawing process (implied).

### Whats missing in summary

The full transcript may provide more details on the specific requirements outlined by the federal judge and potential challenges that could arise during the map redrawing process.


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Georgia and maps
and them being redrawn.
This could almost be a series of videos at this point,
a series of videos where it details federal judges
playing a game of cat and mouse with Republicans in the South
who apparently don't understand that like the Civil Rights Act
and the Voting Rights Act and all of that stuff passed.
OK, so in Georgia, a federal judge has made the ruling.
Basically, the judge determined that the state,
Republican legislators, diluted the voting power
of Black Americans following the 2020 census.
This sounds real familiar, right?
The judge has ordered the creation of new maps
because it violates the Voting Rights Act.
These new maps are to be in use in the next election.
It's not just congressional maps either.
So the judge ordered for there to be districts,
creation of additional districts that are majority black. One congressional
district, two state senate districts, and five state house districts. Basically
basically saying hey you know your job is to represent not to rule, voters get
to pick you, you don't get to pick the voters, that kind of thing.
Now, in what's actually kind of a surprising turn of events given everything that happened
in the other states, the governor of Georgia was like, okay, well, we'll have a special
session on November 29th and we'll draw up new maps.
It is so odd to see Republicans comply with the law that it's kind of surprising and
noteworthy when they do.
But it looks like, at least in Georgia, the situation is coming to a close.
It looks like the Republicans in Georgia are just like, well, okay, I guess we have to
draw new maps.
I think how specific the judge was in this case had a lot to do with it.
In fact, if I'm not mistaken, the judge actually said that the new congressional district was
going to be in the western part of Atlanta.
Judge was very meticulous in the order.
So as long as there aren't any Republican shenanigans, this one looks to be resolved.
It looks like the new maps will be ready.
Everything will be fine in time for the next election.
But again, that's counting on a lack of Republican shenanigans.
So we'll just have to wait and see.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}