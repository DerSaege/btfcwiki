---
title: Let's talk about secrets, deals, and a man from Colorado....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=B-C91dv4PPQ) |
| Published | 2023/10/24 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A Colorado man pleaded guilty to attempting to sell U.S. secrets to Russia.
- He was paid $16,000 initially and was looking to get another $85,000 for the rest of the information.
- The secrets related to a threat assessment of an unnamed third country.
- The people he thought he was dealing with from Russia were actually FBI agents in an undercover operation.
- The man was $237,000 in debt and had only worked for the NSA for a month.
- He was an Army vet and was in his early 30s.
- This incident raises questions about the screening process for individuals handling classified information.
- The judge could still sentence the man to more than the agreed 22 years in prison.
- Comparing this case to Trump's disclosures to foreign nationals is not the same as selling secrets to a hostile power.
- The fallout from this incident is expected to be extensive, leading to numerous investigations and potential changes in security protocols.

### Quotes

- "I don't think that this story is over."
- "The fallout from this is going to be pretty lengthy."

### Oneliner

A Colorado man pleads guilty to selling U.S. secrets to Russia, raising questions about national security and the screening process, with expected extensive fallout and investigations.

### Audience

Security authorities, policymakers

### On-the-ground actions from transcript

- Conduct thorough security clearance checks for individuals handling classified information (implied).
- Implement changes in security protocols based on the outcome of investigations (implied).

### Whats missing in summary

Context on the potential long-term impacts and implications of such security breaches.

### Tags

#NationalSecurity #USecrets #ColoradoMan #Debt #NSA


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about a man from Colorado
and secrets and deals,
and just provide a little bit of framing
on something that was interesting that happened
that is certain to raise a whole lot of questions
about how we safeguard secrets in the United States,
especially given everything else that's going on. Okay, so a Colorado man, a man
from Colorado, has pleaded guilty to attempting to sell U.S. secrets to Russia.
The allegations include the general idea that he passed information and I think
Initially, he was paid 16 grand, something like that, for excerpts, and then he was looking
at getting another 85,000 for the rest of it, and the information was related to a threat
assessment of an unnamed third country.
The people that he was dealing with, who he is reported to have told that he chose their
country, Russia, because his family has ties to there. Well, they weren't from
Russia, they were FBI. It was an undercover operation. According to the reporting, the guy was
unhappy with the US's role in the world and was looking to make a change, but
But more importantly, he was $237,000 in debt.
The agreement that he entered into says that prosecutors will not ask for more than 22
years.
But it is worth noting that the judge gets final say on something like this.
So it could still be more than that.
Now the guy himself, I want to say he was 31, he's an Army vet, and one of the interesting
pieces of information is that according to what his public defender said, he'd only worked
for the NSA for a month, had access after about a month, and was almost a quarter million
dollars in debt.
I have a feeling like there might be some questions that are raised about the screening
process.
People have asked if this is comparable to Trump.
Not based off of what we know.
Not based off of what we know.
Disclosing something to a foreign national because you are ego-driven is not quite the
same and it's not treated the same as literally selling secrets to a hostile power.
But I doubt that this is the end of this.
I don't think that this is the end of the story.
The fallout from this is going to be pretty lengthy.
I would imagine that there are going to be a lot of reviews done.
Having that kind of debt, and to be clear at this point in time, we aren't certain
what kind of debt that is.
That could be a bunch of different kinds, and some of it would be overlooked.
Most of it was from a house or something like that.
But if it's not that, if it isn't what is considered normal debt, the questions that
are going to be raised as far as how this person had access when they had this much
debt, I imagine it will probably lead to some changes and a lot of reviews.
So even though the person has pleaded guilty, I don't think that this story is over.
I imagine there's going to be a lot more that comes out of this.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}