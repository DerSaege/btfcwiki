---
title: Let's talk about some paintings in Russia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=HHNAuGooRCQ) |
| Published | 2023/10/03 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Russia has been painting bombers on the ground at their air bases as decoys, revealed by satellite imagery.
- Back in the day, this tactic might have been effective, but in today's world, it's easy to tell that these are just paintings.
- The purpose behind painting fake bombers on the ground is likely to disrupt targeting of actual bombers that have been expensive targets.
- Russia's resolution with their satellites and other airborne imagery isn't great, leading them to believe this tactic will work.
- One suggestion is that Russia may plan on parking their actual bombers on top of the painted ones, hoping they will be marked as "do not hit" targets.
- There's doubt about the effectiveness of this strategy, especially considering the advanced technology available today.
- Russia might aim to outsmart mission planners by hiding their aircraft in the open on top of the painted targets.
- The hope is that the real bombers will be missed due to being flagged as decoys.
- While this tactic may have worked in the past, it is unlikely to be successful in 2023.
- It remains to be seen whether Ukraine will fall for this strategy.

### Quotes

- "They may later plan on parking their actual bombers on the painted ones."
- "I think they're trying to outsmart the mission planners."
- "It's just a thought, y'all have a good day."

### Oneliner

Russia's tactic of painting bombers as decoys may not be as silly as it seems, with potential plans to outsmart mission planners.

### Audience

Military Analysts

### On-the-ground actions from transcript

- Monitor and analyze Russia's tactics in the region (implied).
- Stay informed about developments in military strategies and technologies (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of Russia's unconventional military tactics, offering insights into potential strategies and their implications for modern warfare.

### Tags

#Russia #MilitaryTactics #Decoys #Bombers #SatelliteImagery


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about some paintings
that have surfaced in Russia.
We are going to talk about some tactics from a bygone era.
We're going to talk about how this may be an update
to older tactics and may not be as silly
as it seems at first glance.
OK, so satellite imagery has revealed
that Russia is painting bombers on the ground
at their air bases as decoys.
Now, before you laugh, back in the day,
this would have been effective.
It really would have.
Today, not so much.
It's pretty easy to tell that these are not
three-dimensional objects, even though they
have put tires on the paintings on the ground.
So the obvious read is that Russia is hoping this will disrupt targeting of these bombers.
They're expensive and somebody, I don't think Ukraine has said it was them, but somebody
has been targeting them.
So they've painted fake ones on the ground and put tires on them.
The obvious read here is that Russia's resolution when it comes to their satellites and their
other airborne imagery isn't great and they think this will still work.
It won't.
good enough to pass like the civilian stuff that we have access to. It's pretty obvious
that they are paintings, especially when in at least one case they didn't finish the painting.
So that's the obvious read and that's the take that is out there. I have another suggestion and
it's something that just might be what's going on. They may later plan on parking
their actual bombers on the painted ones. The hope might be that the painted
bombers get marked as do not hit targets. So if they put their real bomber on top
of the painted bomber, it may not be hit due to just not being targeted because it was
flagged as something to avoid.
I don't think that would actually be super effective, especially given the apparent angle
that some of the stuff has come in at when it was coming at the bombers, but I think
that might be the move. I find it hard to believe that their imagery is that far
behind. I mean, even with everything else going on, I find that hard to believe. I
think they plan on hiding their aircraft in the open on top of the painted
targets on top of the painted aircraft in hopes that in hopes that they get
missed. I think that's the move because they they can't expect this to work in
2023. Again there was a point in time when this would have been super
effective but those days are long since past and I find it hard to
believe that they don't know that. I think they're trying to outsmart the
the mission planners. But we'll have to wait and see and we'll have to wait and
see if Ukraine falls for it. I'm fairly certain they won't. Anyway, it's just a
thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}