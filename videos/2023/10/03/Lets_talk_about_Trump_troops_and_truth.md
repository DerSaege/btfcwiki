---
title: Let's talk about Trump, troops, and truth....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=aZAx-LDyv8U) |
| Published | 2023/10/03 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Calls out the lack of coverage on disturbing claims made against Trump during his presidency.
- Mentions that Trump's derogatory statements towards military personnel barely registered in the news.
- Quotes attributed to Trump about military POWs, wounded, and fallen soldiers are brought up.
- John Kelly, Trump's longest-serving Chief of Staff, speaks out about Trump's disrespectful remarks towards military personnel.
- Kelly's statements could have been a career-ender in any other political climate.
- Despite the gravity of Kelly's claims, it received minimal media attention.
- Kelly's acknowledgment of these events on the record may help sway Trump supporters who are still under his spell.
- Beau suggests that Kelly's confirmation of Trump's statements may help people see the former president's true character.
- Emphasizes the importance of acknowledging and addressing these concerning statements made by a former high-ranking official.
- Encourages viewers to seek out videos or information regarding Kelly's statements for further understanding and awareness.

### Quotes

- "A person that thinks those who defend their country in uniform or are shot down or seriously wounded in combat or spend years being tortured as POWs are all suckers because there is nothing in it for them."
- "God help us."
- "To my knowledge, this is the first time Kelly has acknowledged any of this on the record."
- "For those people who are in your circles, who are still under the spell of Trump, this might be something that would help."
- "It might be useful to show that that loyalty is not returned."

### Oneliner

Despite minimal coverage, Trump's disrespectful remarks towards military personnel are confirmed by his longest-serving Chief of Staff, potentially changing perspectives on the former president.

### Audience

Trump supporters, concerned citizens

### On-the-ground actions from transcript

- Share videos or information about John Kelly's statements with Trump supporters (suggested)
- Use Kelly's confirmation to help sway individuals still supportive of Trump (suggested)

### Whats missing in summary

The full transcript provides a comprehensive breakdown of statements made against Trump regarding military personnel, urging further awareness and action.

### Tags

#Trump #Military #JohnKelly #Statements #Awareness


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about something
that I feel like didn't get the coverage it should've.
Because in any other time in American history,
if what happened had happened,
it would've been front page news,
it would've been talked about for weeks,
but it barely even registered.
And I think it's important to make sure
this information gets out there. We live in a Trump era, and there was the news of
the quarter of a billion dollar civil trial. There's the fallout from Trumpism
over in the House. There's all of that, and that took precedence. That's what got
the coverage. But during Trump's presidency, there were a lot of claims
that were made. There were a lot of quotes that were attributed to him that
he of course denied and they had to do with how he viewed the military POWs, the
wounded, those who were lost. I don't get it. What was in it for them? Didn't want
wounded around because it's not a good look. He didn't like looking at them. There
There were tons of them.
These statements, according to the allegations in the reporting, they were said around senior
staff.
And that's why it was always really hard to get anybody on the record about it.
I want to read you something, and this was in an interview.
What can I add that has not already been said?
A person that thinks those who defend their country in uniform or are shot down or seriously
wounded in combat or spend years being tortured as POWs are all suckers because there is nothing
in it for them.
A person that did not want to be seen in the presence of military amputees because it doesn't
look good for me.
A person who demonstrated open contempt for a Gold Star family, for all Gold Star families,
on TV during the 2016 campaign, and rants that our most precious heroes who gave their
lives in America's defense are losers and wouldn't visit their graves in France.
A person who is not truthful regarding his position on the protection of unborn life,
women, on minorities, on evangelical Christians, on Jews, on working men and women. A person that
has no idea what America stands for and has no idea what America is all about. A person who
cavalierly suggests that a selfless warrior who has served this country for 40 years in peacetime
and war should lose his life for treason, in expectation that someone will take action.
a person who admires autocrats and murderous dictators, a person that has
nothing but contempt for our democratic institutions, our Constitution, and the
rule of law. There is nothing more that can be said. God help us. That's quite
the rant. That is quite the rant. A statement like that you would probably
expect to come from, you know, somebody from the other team, a Democrat, that's
John Kelly, longest-serving White House chief of staff. If the chief of staff of
any other president said this, that would have been front-page news. That would have
been huge in any other political climate, just this statement, but this is where it
gets a little bit more important.
Kelly was senior staff.
A lot of the words and terms used here, they're the quotes that were attributed to Trump throughout
his administration.
The things he denied, the things his supporters denied, he would never say that.
This is the guy that would have been in the room when it happened.
And he's quoting them.
If they weren't true, if they weren't actually said, he probably wouldn't be using the exact
same words. Looking at a whole bunch of the fallen and saying I don't get it, what was
in it for them, I can, I imagine that that would stick with you. This would have been
a political career ender for anybody else at any point in time.
Just this statement.
This is Trump's longest serving White House Chief of Staff.
Who said that?
It barely got covered.
This little segment, and I'm sure that there will be video about this out there if you
look for it.
For those people who are in your circles, who are still under the spell of Trump, this
might be something that would help.
This isn't a Democrat.
This isn't some unnamed source.
To my knowledge, this is the first time Kelly has acknowledged any of this on the record.
This might be something that would help sway them.
That would help them see Trump for who he is.
Because in the United States, these comments for most people who are supporters of Trump,
these comments, they would be unbelievable.
That was part of the reason I think he got away with it when the reporting came out.
Because for his supporters, this kind of statement is just so unbelievable, but you have Kelly
using those exact same terms, saying that that's what he said, and Kelly would have
been there.
In fact, if you go to the reporting, a lot of it's from the Atlantic, you'll see his
name. He would know if this wasn't said because in a lot of the reporting it was said to him.
He's confirming it. Certainly appears to be. I know there's a lot of other news, but for
those people who see themselves as MAGA orphans, a term that I heard the other day, or those
Those who have become separated from their families because of their unquestioning loyalty
to dear leader, it might be useful to show that that loyalty is not returned.
And all of those things that were just unbelievable, they certainly appear to be being confirmed
by Trump's longest-serving White House Chief of Staff.
Anyway, it's just a thought.
have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}