---
title: Let's talk about an important message about phones....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=a7QGjOSbmlw) |
| Published | 2023/10/03 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing three seemingly unrelated news stories that converge.
- Mentioning a social media rumor about turning off cell phones on October 4th to avoid activating the Marburg virus and turning people into zombies.
- Clarifying that the Emergency Broadcast System (EBS) doesn't exist anymore, but the emergency alert system will be tested the following day between 2:20 pm and 2:50 pm Eastern.
- Advising those with multiple phones for safety reasons to power off any secondary phones during the test to avoid issues.
- Emphasizing the importance of turning off phones for safety in specific situations even if the sound might come through on silent mode.
- Noting the misinformation about vaccines and the importance of protecting oneself.
- Mentioning two scientists receiving the Nobel Prize for their work on mRNA and saving thousands of lives.
- Stating that despite conspiracy theories being useless, they help spread the message to turn off phones to avoid potential attacks.

### Quotes

- "Turn off your phone tomorrow, not because zombies, but for safety."
- "Two scientists awarded the Nobel Prize for saving lives."
- "Conspiracy theories may be useless, but they spread the message of safety."

### Oneliner

Beau addresses rumors of a zombie apocalypse through a social media post, warns about misinformation on vaccines, and advises turning off secondary phones during an emergency alert system test for safety.

### Audience

Phone users

### On-the-ground actions from transcript

- Power off secondary phones during emergency alert system test (implied)
- Spread the message of phone safety during the test (implied)

### Whats missing in summary

Importance of staying informed and cautious during times of misinformation.

### Tags

#EmergencyAlertSystem #ZombieApocalypse #PhoneSafety #Vaccines #Misinformation


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about
three seemingly unrelated news stories
that all kind of converge.
The first part is kind of funny in a sad way,
but it's also really important.
If you are somebody who maintains more than
one phone for any reason, please watch this whole video.
Okay, so what are the big pieces of news? First, there is something floating around on social media
right now. One of the many versions of this says, turn off your cell phones on October 4th. The EBS,
that's the Emergency Broadcast System, which incidentally is a thing that doesn't exist
anymore, is going to test the system using 5G. This will activate the Marburg virus, if you're
not familiar familiar with that think outbreak, in people who have been vaccinated and sadly some of
them will be turned into zombies. Now, I feel like I don't need to say that this isn't going to happen,
but just in case, this isn't going to happen. I do not anticipate myself turning into a zombie tomorrow.
However, there is an element of truth in this, and this is what's important. While the EBS system
does not exist anymore, there is something that is very similar. The emergency alert system.
It will be tested tomorrow. It will be tested. My understanding is that it will happen between 2 20 pm
Eastern to 2.50 p.m. Eastern. This is when it will be tested. If you maintain a second phone
for safety reasons and the discovery of that phone might cause an issue for you, power it off,
all the way off. Turn it off. If you are in a living situation that requires you
to have a phone for your own safety, one that other people in the household may
not know about, turn it off. My understanding is that the sound will
come through even if it is on silent. Turn it off. Okay, so what's the third
piece of news? While there are still tons of conspiracy theories, misinformation,
disinformation floating around telling people not to protect themselves and
putting them at risk, it is worth noting, at least in my opinion, that two of the
scientists who did a whole lot of groundbreaking work in the study of MRNA
and how it can be used have been awarded the Nobel Prize because you know they
like help save thousands upon thousands of lives. I feel like that's worth
mentioning, as the theories continue to spread. While the theory is useless,
I mean it's just absolute garbage, it is useful in making sure that the message
to turn your phone off gets out. Because while you aren't going to be attacked by
zombie tomorrow, there are some people who the discovery of that phone might
lead to an attack.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}