---
title: Let's talk about McCarthy and voting your conscience....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=sb4vhGGCJGM) |
| Published | 2023/10/03 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explaining the disconnect between the term "voting your conscience" for the public versus Capitol Hill.
- Mentioning the resolution to oust McCarthy and the need for movement on it within two legislative days.
- Describing how both far-right Republicans and McCarthy need Democrats to achieve their goals.
- Pointing out that voting your conscience in Capitol Hill means voting politically expediently, not as you think it should go.
- Explaining how representatives should vote according to their district's wishes, but parties often steer the agenda.
- Noting George Washington's warning about the negative impact of political parties.
- Illustrating how party interests often overshadow individual district interests in national politics.
- Criticizing representatives for not truly representing their constituents' interests but rather following party platforms.
- Emphasizing the importance of representatives voting according to their district's needs in every vote.
- Condemning loyalty to party over representing the interests of the district.

### Quotes

- "Voting your conscience up on Capitol Hill does not mean vote the way you think it should go."
- "They're ruling you. They are telling you what's important."
- "Loyalty to party is not a good thing."
- "That's not actually how it's supposed to function."
- "It's that extreme nature."

### Oneliner

Beau explains the disparity between public perception of voting and Capitol Hill reality, criticizing party loyalty over representing constituents.

### Audience

Activists, Voters, Representatives

### On-the-ground actions from transcript

- Contact your representatives to express your district's needs and hold them accountable (implied).
- Participate in local politics to ensure your interests are represented (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of how party loyalty hampers representative democracy, urging for a return to voting based on constituents' needs.

### Tags

#Voting #RepresentativeDemocracy #PartyLoyalty #PoliticalParties #Accountability


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about McCarthy
and everything going on in the House of Representatives
and voting your conscience.
That term, what it really means up on Capitol Hill.
Because it means one thing to us.
It's an entirely different language up there.
And that term may become important.
And it's a good time to highlight it.
because if you miss the news, the resolution that the far-right Republicans were promising
talking about ousting McCarthy, well, it was put forward.
So two legislative days, there has to be movement on it.
Now there's a bunch of different ways that could happen, and we have talked about them
all before, and we have talked about how both sides of this, both the far-right Republicans
and McCarthy, whatever it is their goal is, they need the Democrats to make it
happen. I know that the talking point is just the other side needs them, but in
real life they both need them. And we've talked about the various ways that
Jeffries might handle that and kind of steer the party. We talked about how
how there will be some Democrats who no matter what they will not work to save
McCarthy. Even if in private they think it's a good idea, they won't do it
because they'll be voting their conscience. Doesn't make any sense, right?
Because if they were voting their conscience and they thought it was a
good idea, they would vote to save McCarthy. But they won't because that's
That's not what that term means up there.
Voting your conscience up on Capitol Hill does not mean vote the way you think it should
go.
It means vote whatever is most politically expedient for you.
Whatever would poll best in your district.
Here's the thing.
Every vote in the U.S. House of Representatives should be one where the representatives are
voting their conscience, meaning voting the way the people in their district want them
to vote.
That's how the system is set up.
That's how it's supposed to function.
But it's such a rarity that there's a special term for it.
Why?
Why does it matter?
When Washington gave his farewell address, one of the things he warned about, he told
the entire country, and he saw it coming, was basically that parties were a bad thing.
I mean, it's a little bit more nuanced than that, but that's the short version, and he
was right.
what happens, especially when you get to national politics, is the parties need a
platform and then they steer the conversation. They tell their voters
what's important and then the voters go along with it. That's not how the system
is set up. That's not how it's supposed to function. The reality is the voters of
each district are supposed to tell their representative what's important and
their representative is supposed to carry that to DC. That's how it is
supposed to function but that's not what happens because they need that platform.
So the individual interests of all of the districts they get swallowed up, they
get gobbled up, they get cannibalized so they can be turned into a national
platform that the party can agree on and this is this is truly something that is
both sides. Since we just talked about a situation with the Democratic Party,
let's go to the other side. For the Republicans that watch this channel,
most of y'all are rural. You really are. That's what that is definitely what has
come across over the years. Have you ever actually had an issue of any kind with a trans
person? Have any of your friends? No, right? So why is your representative campaigning
on that? Because the party told him to. If they're talking about that, if they're using
that rhetoric. They're not representing your interests. They're not your
representative. They're your ruler. They're even telling you what to be mad
about. What's really important to you. Something that I would be willing to bet
most Republicans watching this channel have never had an issue with. But that's
the talking point. And how does that happen? They find something that they can
use on the Republican side of the aisle it's fear. They find something that they
can use that most conservatives are afraid of. Generally it's change of some
kind. And they talk about it over and over again. And mainly those people who
use the most extreme rhetoric, they shape that conversation. So they shape what
your representative cares about because that's what they need, right?
It's how you lose your voice.
It's how the system fails to function because they're not representing you.
They're ruling you.
They are telling you what's important.
And voting the way the district wants, that's a special occasion, it's when the leadership
of the party says, okay, on this one, you can actually represent your district.
When that should be every single vote.
Loyalty to party is not a good thing.
You want to see another example of why it's so bad?
This situation.
This exact situation.
When you were talking about the Speaker of the House, McCarthy, right?
But is he really Speaker of the House?
And I'm not talking about the ineffective leadership stuff.
Or is he the Republican Speaker?
The Speaker of the House, their job should be to basically schedule and make sure that
the interests of all of the districts can be represented by their representative.
But that's not what happens, right?
It's not like McCarthy is giving Democrats from district whatever equal time.
Because loyalty to the party is more important.
It's how it happens.
It's one of those things that it's been around for the entirety of anybody watching this.
This system has existed your entire life.
That's not actually how it's supposed to function.
A lot of the, let's just say, less than productive efforts of Congress are because of this.
That's because of the parties pushing the agenda rather than the people who elect the
representative who should be representing their interest.
Now sure, naturally there would be alliances that formed between districts that had shared
interests.
would vote the same way. But it wouldn't always be party. Party is a useful identifier. It's
one of those things that makes it so the average person doesn't have to pay as close attention
to politics. It's shorthand. It even gets used that way. Democratic party liberal, Republican
party conservative, but we know that that's not always how it works out.
The party system itself is part of the issue and it's something that was warned about.
There's not any method that is speedy to address this issue.
Requires a lot of education over years and years.
But this is one of the issues, and it's one of the issues that the first president was
actually like, hey, this is going to be a problem.
Nobody really listened.
And it's showing itself right now because the party loyalty has gotten to such an extreme.
The idea that the Speaker of the House, somebody who is supposed to represent the whole House
and schedule for the whole House, the idea that they would need votes from the other
side of the aisle, well that is just unthinkable because they don't represent the whole house,
right? It's that polarization. It's that extreme nature. It should not be controversial for
a potential speaker from either party to get votes from the other side. Theoretically,
the idea would be to pick somebody who would represent both. That's how you would get
progress. That's how you would get your issues, your interests in your district acknowledged.
The fact that it doesn't work that way is why you don't.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}