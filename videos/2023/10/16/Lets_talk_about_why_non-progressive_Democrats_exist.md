---
title: Let's talk about why non-progressive Democrats exist....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=oX2L02jQ24Q) |
| Published | 2023/10/16 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains why some Democrats aren't progressive, despite being part of the progressive party.
- Analyzes the upcoming Arizona election and the dynamics within the Democratic Party.
- Clarifies that the Democratic Party is unlikely to heavily target Sinema, who is running as an independent but caucuses with them to maintain Senate majority.
- Describes how Democrats in various areas may not be progressive to secure wins and maintain majority.
- Points out the strategy of the Democratic Party to focus on maintaining majority rather than pushing for all members to adopt more progressive positions.
- Mentions the importance of majority in setting the legislative agenda and sliding in progressive elements.
- Observes that newer politicians in the House may not fully grasp political strategies and their impact on party and re-election chances.
- Explains why the Democratic Party doesn't pressure members like Manchin to adopt more progressive stances due to the need to secure seats for majority.

### Quotes

- "The Democratic Party doesn't push them too hard to take more progressive positions."
- "If she wins, well, we didn't go after you, so you need to still caucus with us."
- "That's why you have such divergent views."
- "Having the majority allows them to set the legislative agenda."
- "Maintaining the majority over an individual candidate's personal platform."

### Oneliner

Understanding why some Democrats aren't progressive and the dynamics of maintaining majority in the Democratic Party, as explained through the Arizona election.

### Audience

Political enthusiasts, Democratic voters.

### On-the-ground actions from transcript

- Analyze local candidates' stances and voting records (suggested).
- Support candidates who prioritize progressive policies (suggested).
- Stay informed about political strategies and party dynamics (suggested).

### Whats missing in summary

Nuances of political strategy and party dynamics.

### Tags

#Democrats #Progressive #Arizona #Election #PartyDynamics


## Transcript
Well, howdy there, internet people.
Let's battle again.
So today, we're going to talk about two questions,
and Arizona, and how Arizona can teach us
about why there are Democrats that,
well, they really don't seem like Democrats.
They're not progressive, not particularly progressive.
They're kind of centrist.
Some might even call them corporate, right?
Okay, so one of the questions was, hey, why are there Democrats who aren't progressive?
They're supposed to be the progressive party.
And the other question is, since Sinema in Arizona is going to run as an independent
and the Democratic Party is probably going to have Gallego, does that mean that the Democratic
Party is going to go after Sinema in the way that she voted?
No, no, definitely not. No, as far as the Democratic establishment,
you're probably not going to hear much at all about her. They're not going to devote a lot of
money to going after her. The main person they're going to go after is Lake, and they'll probably
run positive stuff about Gallego, but they're not going to go after Sinema at all. And the
And the reason for that is simple.
She's an independent now, but she currently caucuses with the Democratic Party, which
helps them maintain the majority in the Senate.
So the progressives that are in the Democratic Party, they obviously, they're not a fan
of hers, right? Because her votes are not progressive in any way. But she didn't
get booted out. They're not gonna waste money going after her because if
she wins, she continues to caucus with the Democratic Party, which is the
expectation. It's a seat. It helps them maintain the majority and as we can see
from the giant show over in the House, maintaining the majority is kind of important.
So that also explains why you have Democrats in various areas that aren't really progressive,
and why the Democratic Party doesn't push them too hard to take more progressive positions.
This is particularly true in the House, where districts are little chunks of the state,
and the message that it takes to get elected in that district may not be very progressive.
But if that person wins as a Democrat, it helps them get the majority, set the legislative agenda,
and they can count on them for votes for like the budget and in the budget they
could slide some progressive stuff in that the candidate really won't have to
answer for come election time or reelection time but that same candidate
they may vote against a super progressive standalone bill because
that message doesn't play where they're at. That's how you end up with that. And
it's it's pretty common, but I would not expect the Democratic establishment, the
Democratic machine, to go after Sinema. They'll promote Gallego, my guess, I would
hope they would do that, and they'll go after Lake hard. But that's it. If she
wins, well, we didn't go after you, so you need to still caucus with us. If she loses, well, okay.
To the upper echelons of the political machine, one's just as good as the other because it helps
to maintain the majority. The same thing plays out on the other side to a lesser degree because
as maintaining a conservative position, it's relatively easy, just nothing gets to change
and we try to go back to the good old days type of thing.
So there's not as much deviation, but there is still some, which is why you have the show
you have over in the House, because there are a lot of, let's just call them newer
politicians that maybe don't understand politics too well and they don't
understand how much they're damaging their own party and by extension their
own re-election chances by what's happening over in the house. But that
explains it, that's why it happens, that's why you have such divergent views, it's
It's why the Democratic Party never really went after Manchin or tried to push him into
a more progressive position.
West Virginia, and they got that seat, counts towards the majority.
That's what it's about.
So hopefully, as you watch this election in Arizona, because it is going to be consequential,
you'll get to see a very ready example of a political party putting, maintaining the
majority over an individual candidate's personal platform, because having the majority allows
them to set the legislative agenda which allows them to move at least some
progressive stuff forward and that's why it takes so long. Anyway it's just a
thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}