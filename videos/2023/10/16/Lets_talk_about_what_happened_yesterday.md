---
title: Let's talk about what happened yesterday....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=QlXGWTz6zZc) |
| Published | 2023/10/16 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the aftermath of a video titled "Let's Talk About What Didn't Happen Yesterday."
- Talks about commentators pushing a misleading narrative for clicks.
- Warns about the impact of fear-mongering narratives leading to negative outcomes.
- Received messages from viewers expressing concern about his well-being.
- Reads a critical message questioning personal responsibility and reacting to the narrative.
- Responds to the criticism by pointing out dramatic real-world events like an old man showing up at a Palestinian family's home with a knife.
- Mentions the interconnected nature of fear-mongering and its impact on social media.
- Urges people not to let fear-mongering guide their thoughts and actions.
- Encourages being a force for positive change in the world.
- Concludes with a message advising against falling prey to fear and negative influences.

### Quotes

- "We are going to be reactionary children."
- "Be one of them. Don't react to the scare of the weak."
- "Don't let people who do not have your best interests at heart guide your thoughts."

### Oneliner

Beau addresses fear-mongering, urges against reactionary behavior, and encourages being a force for positive change in the world.

### Audience

Social media users

### On-the-ground actions from transcript

- Be a force for positive change in the world (implied)
- Don't let fear and negativity guide your thoughts and actions (implied)

### Whats missing in summary

The emotional impact and intensity of Beau's response can be best understood by watching the full video.

### Tags

#FearMongering #ReactionaryBehavior #PositiveChange #SocialMedia #CommunityPolicing


## Transcript
Well, howdy there, internet people, Litzbo again.
So today we're going to talk about what happened yesterday.
I did that video titled, Let's Talk About What
Didn't Happen Yesterday.
And if you missed it, it was about how a lot of commentators
were pushing a narrative for clicks
And how that narrative was less than accurate.
And how it was going to lead to bad things.
Because that kind of fear-mongering always does.
And after what happened happened,
I got a whole bunch of messages from you all basically
asking if I was OK.
I'm OK.
I'm not going to read those messages.
I'm going to read something else.
Hello.
Do you want some wine with your cheese?
We all must fall in line with your wine.
No one has any personal responsibility.
No one can make up their own mind.
We are going to be reactionary children.
No, it was a reactionary old man.
It was a reactionary old man.
Whining about what others might think.
We can't have anyone saying anything not agreeable.
first clean up your own act. You have made dramatic statements. Maybe you should shut
up. Just go away." I don't know that my statements were that
dramatic. I would suggest that an old man showing up at the home of a Palestinian family
with a knife. That's dramatic. I'm not going to go into the details if you want to know
what happened, you can google six-year-old Palestinian boy, I'm sure it's still a headline.
That kind of fear-mongering, that kind of spewing of hatred to get clicks, it doesn't
exist in a vacuum. It doesn't exist in a vacuum. Social media comments do not
exist in a vacuum. Other people see them and some of them are reactionary.
There are a lot of bad actors out there and there are a lot of bad actors
some of whom probably wanted something bad to happen. Probably not the way it did though.
Remember as that fear-mongering takes hold that there are people out there that are trying to
the world better. Be one of them. Don't react to the scare of the weak. Don't let
people who do not have your best interests at heart guide your thoughts
because they'll lead you somewhere bad. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}