---
title: Let's talk about what fiction can teach us about politics....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=o83cIeEh_j4) |
| Published | 2023/10/16 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains how fiction, especially science fiction, aims to create the suspension of disbelief to draw readers or viewers into the story.
- Notes the importance of getting details right in fiction to maintain the immersion and avoid jarring experiences that pull people out of the narrative.
- Science fiction writers face the challenge of balancing scientific accuracy with speculative elements that may become outdated.
- Mentions how authors use creative devices like magic or advanced alien technology to explain scientific concepts in fiction without risking factual inaccuracies.
- References the use of dark matter as fuel in the show Futurama as an example of choosing a fuel source that is not well understood to avoid disputes.
- Draws parallels between how fiction writers manipulate unknown concepts and how authoritarian figures exploit fear of the unknown to control and manipulate audiences.
- Suggests that conservative fearmongering often targets concepts or groups that are not well understood to spread misinformation and maintain control through fear.
- Points out that ignorance leads to fear, and when people are scared, they seek safety and are more easily manipulated by those who understand and exploit their fears.
- Observes that conservative leaders capitalize on topics like Critical Race Theory (CRT) and marginalized communities, which are often unfamiliar to their audience, to stoke fear and maintain control.
- Contrasts the forward-thinking nature of science fiction, which encourages looking ahead, with conservative fearmongering that aims to keep people in the past and resistant to change.

### Quotes

- "Ignorance really does lead to fear, especially if you have people motivated to keep people afraid because people who are scared, well, they want safety."
- "Suspension of disbelief, that willing avoidance of critical thinking. It can be used to help people see the future and what could exist."
- "Science fiction, it's about getting rid of fear. It's about looking ahead, not behind."

### Oneliner

Beau explains how fiction's suspension of disbelief can either inspire or control by manipulating the unknown, reflecting on conservative fearmongering tactics that exploit ignorance and unfamiliarity to maintain influence and spread misinformation.

### Audience

Storytellers, activists, educators

### On-the-ground actions from transcript

- Educate others on the tactics used in fearmongering to empower them to critically analyze information and resist manipulation (implied).
- Encourage community engagement and communication to foster understanding and combat fear of the unknown (implied).

### Whats missing in summary

The full transcript delves into the power dynamics of manipulation through fear and ignorance, exploring how storytelling techniques can be used positively or negatively to shape perceptions and influence behavior. Viewing the entire text provides a comprehensive understanding of how fear can be both a tool for control and a catalyst for change.

### Tags

#Fiction #ScienceFiction #Fearmongering #Conservatism #Education


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about fiction,
science fiction, mostly and fans and Futurama and fuel
and how those things come together to help explain, well,
all of this and why it seems like a whole
lot of Americans have become, well, I mean, kind of easy to trick. In writing, in science
fiction and, well, I mean any kind of fiction really, you're trying to create something.
You're trying to create something called the suspension of disbelief, which is the
The willing avoidance of critical thinking.
You want the person to get sucked into the story.
And I would imagine that all fiction writers have an issue with this.
Because they have to get certain things right.
Because if they get them wrong, well, it kind of ruins the story.
It's jarring.
It pulls people out of that suspension of disbelief.
about any movie or book that you have read or seen that deals with your
profession or something you know a whole lot about. When they get something wrong
you notice it right and it pulls you out for a second. You realize it's a story.
You're no longer engrossed. For science fiction writers this is really hard
because generally speaking people who like science fiction well I mean they
know a lot about science so if you get a scientific fact wrong it pulls them out
of the story and not all writers are scientists and it gets even harder
because a lot of science fiction is let's just call it speculative it's on
the edge of what we know. And science changes. So your current understanding of something,
by the time the book's published, new research may say that's not right, especially when
you're talking about things on the fringe of science.
Over the years, science fiction writers, they came up with different ways to deal with this.
they just created a magic device, you know. The time travel, rather than really trying to even
explain how it happens, well it just happens because of the flux capacitor, right. Another
way to do it is blame it on some alien technology that is just so advanced it seems like magic to us,
right? So that's how they do it because they don't want to get a scientific fact
wrong. There's a show called Futurama. I've talked about it before but if you
don't know what it is it's a science fiction cartoon and in this cartoon they
use dark matter as fuel and according to a story I recently heard early on in the
development, one of the voice actors asked one of the producers, did you come
up with that? And the producer was like, yeah, I invented it before Einstein or
something like that. And then went on to say, we wanted to think of something
which was not well understood so that no matter what we said about it, people
couldn't dispute it. They wanted to choose a fuel source that no matter what
they said about it, people couldn't dispute it because it wasn't well
understood. And this is why education is important, right? I want you to think
about the things that conservatives fearmonger about. How the authoritarian
right motivates people, scares them into submission. What do they do? They choose
something that's not well understood. That way they can say whatever they want
about it and nobody can dispute it, at least not anybody in conservative circles.
This is why you find conservatives attacking people on the fringes or ideas that aren't
well understood yet.
This is why it's easy to get them super upset about a new school curriculum because they
don't understand it. You don't really often see authoritarians try to scapegoat
a group that is well understood, right? It's not what they do. They pick a group
that's small, just big enough to be a perceived threat if you say enough
horrible things about them. And then they just make stuff up, but because it's not
well understood by the base, by the people they're trying to scare, nobody
can dispute it. That's why the enemies that they choose, they're external or
Or they're such a small minority that most people never had any contact with them.
Because if you had contact with them, well, that wouldn't be scary because they wouldn't
be unknown.
It's amazing to think that it really is that simple.
When you go through and you think about all of the different things that conservatives
fearmonger about, it all hinges on their base, their audience, not understanding it, not
being familiar with it.
Because then, when they say something completely ridiculous, well, it still gets believed,
Because nobody in that circle can dispute it because they don't know anything about
it.
Ignorance really does lead to fear, especially if you have people motivated to keep people
afraid because people who are scared, well, they want safety.
They want somebody to tell them, it'll be like the good old days, it'll be like it
used to be.
Nothing will ever change, and if you scare them enough, well they will give up pretty
much everything to not be scared anymore, and they'll follow along with whatever they're
told because they're scared and they don't understand it.
And you have a lot of conservative leaders who understand this.
So they pick topics like CRT.
They pick people, LGBTQ community, people that aren't well-known, right?
They don't have a lot of contact with conservative circles, at least not that they know of.
So it's easy to fear monger about them.
The irony is that, generally speaking, science fiction, it's about getting rid of fear.
It's about looking ahead, not behind.
It's not about wanting things to stay the same.
suspension of disbelief, that willing avoidance of critical thinking. It can
be used to help people see the future and what could exist. Or it can be used
to terrify and control people. All by talking about something that isn't well
understood. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}