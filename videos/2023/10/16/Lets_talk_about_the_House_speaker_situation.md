---
title: Let's talk about the House speaker situation....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=zzV82ru46_Y) |
| Published | 2023/10/16 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The US House of Representatives still lacks a speaker, with the Republican Party unable to agree on a candidate.
- A vote was planned for Tuesday at noon, with Jordan being the preferred candidate by the Republican Party.
- An internal vote among Republicans showed 152 in favor and 55 opposed to Jordan, falling short of a consensus.
- Supporters of Jordan engaged in a pressure campaign to sway the opposition, resulting in mixed outcomes.
- Some individuals felt pressured and changed their stance, while others became more entrenched in their opposition.
- Reports suggest that some viewed the pressure tactics as bullying, leading them to reconsider supporting Jordan.
- If the vote on Tuesday fails, the possibility of a bipartisan speaker arrangement becomes more likely.
- Talks of bipartisan cooperation have already been initiated to ensure bills have support from both Democrats and Republicans.
- Failure to elect a speaker could lead to a stalemate in legislative progress, impacting all political factions.
- Progressives are advised not to be overly disappointed as they are already facing challenges in getting their agenda through the House.
- Options include either electing McHenry or pursuing a centrist coalition if Jordan's candidacy fails to gain enough support.
- The outcome hinges on whether Jordan's tactics can sway enough Republicans to secure the speakership.

### Quotes

- "We want to ensure that votes are taken on bills that have substantial Democratic support and substantial Republican support so that the extremists aren't able to dictate the agenda."
- "The idea of a bipartisan speaker arrangement becomes a whole lot more likely."
- "It is just going to be super status quo."
- "Progressives, please keep in mind, you aren't getting anything anyway right now through the House."
- "If this deal is struck, try not to be too upset about it."

### Oneliner

The US House of Representatives is in limbo without a speaker, with pressure tactics and potential bipartisan cooperation looming as solutions amidst internal party struggles.

### Audience

Politically engaged citizens

### On-the-ground actions from transcript

- Support bipartisan efforts for bills with substantial support from both Democrats and Republicans (suggested)
- Stay informed and engaged with the developments in the US House of Representatives (exemplified)

### Whats missing in summary

Insights into the specific bills or issues at stake in the House proceedings.


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about
the US House of Representatives
being without a speaker, still,
and the Republican Party being unable to offer up somebody
that Republicans will agree on, still.
Latest information at time of filming
says that there will not be a vote today,
there will be one Tuesday at noon, and that the Republican Party is still kind of offering up and
leaning into Jordan as their chosen candidate. Now, on Friday, there was a vote about this among
Republicans. 152 in favor, 55 opposed. Those numbers don't cut it. They don't even come close.
Over the weekend, those in in support of Jordan, they kind of worked the phones
and there has been a lot of reporting about a pressure campaign designed to
almost shame the 55 who were opposed into supporting it. Now depending on who
you talk to, some are saying it worked with some of the people and others are
saying that not just did it not work, it actually made them more entrenched in
opposing Jordan. I guess some people kind of saw it as bullying and when that
occurred, they basically realized they didn't want somebody like that as
speaker. That was the takeaway. That's how it's been described. The reporting on
that is pretty light. We don't really know what went on behind closed doors,
but that's what we've heard. So between now and Tuesday at noon, a whole bunch
can change, obviously. Keep in mind, it wasn't that long ago that Scalise was
supposed to be the speaker. So things may change, but if this vote doesn't work, if
the vote fails, or if they have to cancel the vote to avoid embarrassment, at that
point the idea of a bipartisan speaker arrangement becomes a whole lot more
likely. Jeffries has indicated there have been, like, background conversations about it already.
One of the things that he said was, we want to ensure that votes are taken on bills that have
substantial Democratic support and substantial Republican support so that the extremists
aren't able to dictate the agenda.
That's kind of what we talked about before.
If that happens, the lights will stay on.
But there's not going to be any forward movement
on anybody's agenda.
Progressives, far right, nobody's getting anything.
It is just going to be super status quo.
For progressives, please keep in mind,
you aren't getting anything anyway
right now through the House.
So if this deal is struck, try not to be too upset about it.
It becomes a whole lot more likely if they can't do
anything on Tuesday.
The other option still on the table is to leave McHenry in
the spot.
But that doesn't seem to be gaining as much ground as kind
centrist coalition. But it all kind of hinges on whether or not Jordan was able
to push the other Republicans around. And we'll have to wait and see what the
vote looks like or if it occurs to know how that really played out. But that's
That's where we're at now.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}