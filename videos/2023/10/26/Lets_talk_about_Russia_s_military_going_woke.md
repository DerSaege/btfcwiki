---
title: Let's talk about Russia's military going woke....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=brsd-rXMB3U) |
| Published | 2023/10/26 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Russia's Ministry of Defense has set up a contracting company after the fall of Wagner, with a focus on recruiting women for certain roles like snipers and drone operators.
- In the United States, there was a narrative that the military should not go "woke" and should be tough like Russia's military.
- The idea of needing a military like Russia's, focusing on physical skills, faded as Ukrainian women soldiers proved otherwise.
- The contracting company associated with Russia's Ministry of Defense is actively recruiting women for combat roles, not just traditional support roles like nurses and cooks.
- A woman sniper, especially one working for a contracting company and possibly not in uniform, may have an advantage in moving around the country.
- Going "woke" does not harm national security; embracing new ideas and education is vital.
- The United States' first secretive special operations teams sought "PhDs who could win a bar fight," showing the importance of diverse skills.
- Russian recruitment practices may change the perception of some in the United States, particularly those who trust Russian generals and troops more than American ones.

### Quotes

- "Going woke is not a detriment to national security."
- "Russia, a little late in the game, has finally realized this."
- "The first thing that you need to know is that after the fall from grace of Wagner..." 

### Oneliner

Russia's Ministry of Defense contracting company actively recruiting women for combat roles challenges misconceptions about military toughness.

### Audience

Military analysts, policymakers.

### On-the-ground actions from transcript

- Contact organizations supporting women in combat roles for potential partnerships (implied).
- Join advocacy groups promoting gender diversity in military positions (implied).

### Whats missing in summary

The full video provides deeper insights into changing perceptions of military recruitment and the implications of diverse skills in combat scenarios. 

### Tags

#Russia #MilitaryRecruitment #GenderDiversity #NationalSecurity #CombatRoles


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about Russia
and a new recruitment practice
that will be surprising to some, no doubt.
But it might work to shift thought
in the United States about something
because there's a long-held talking point
that is
about to go away or at least it should go away if you're one of those people
who allows
new information
to change your opinion.
The first thing that you need to know
is that after the
fall from grace
of Wagner
the Ministry of Defense in Russia
they basically set up their own contracting company.
Now, officially
there's a separation
But in real life, it's MOD's contracting company.
That contracting company has started recruiting,
and they are filling certain roles,
and they are targeting a specific demographic
for recruitment, women.
It wasn't too long ago that a whole lot
people in the United States, particularly among the American right were going all
in on the idea that the military was going woke and it was a super bad idea.
We need military like Russia's where they run on logs and throw a hatchet while
doing a backflip or whatever because those are very important skills in
modern combat. The idea, it slowly kind of faded away as soldiers who are Ukrainian
women. Well, let's just say that the Russian troops, knowing how to do a
backflip, didn't do a lot of good. It's kind of faded away, but what you see now
is the contracting company that is associated with the Ministry of Defense
actively recruiting women and I know somebody's about to say well they've
always done that they've allowed them to be nurses and cooks for a really long
time yeah that's true but they're recruiting them to be snipers and drone
operators, not in the rear. It's worth noting that a woman sniper, especially one
working for a contracting company who may not be in uniform, might actually
have an easier time moving around the country.
Going woke is not a detriment to national security. It's important to
remember that when you are talking about education and embracing new ideas and
all of that stuff. The first real secretive special operations teams that
the United States had, they described the people who they were looking for as
PhDs who could win a bar fight. Russia, a little late in the game, has finally
realize this. So that might alter the perception of some in the United States
because while they won't trust, you know, American generals or American troops they
seem to have a lot of loyalty to Russian ones. Anyway, it's just a thought.
You all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}