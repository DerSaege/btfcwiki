---
title: Let's talk about the new Speaker of the House and independents....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=SiNF40Is-6I) |
| Published | 2023/10/26 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing independents and moderate Republicans who are feeling alienated by the current state of the Republican Party.
- Pointing out that the moderate Republicans have lost power within the party and the party is moving further towards authoritarianism.
- Mentioning that moderate Republicans either endorsed the new Speaker's views or were powerless to stop them.
- Describing how the Republican Party has shifted right to the point of overt authoritarianism.
- Emphasizing that there is no place for moderates within the current Republican Party.
- Noting that moderate Republicans have compromised so much that there seems to be no way back for them.
- Comparing moderates in the party to a tiger that cannot be turned vegetarian by feeding it steak.
- Warning independents and moderate conservatives that the Republican Party does not represent them anymore.
- Urging individuals to realize that Trumpism has infected the Republican Party and that the far-right faction is still in control.
- Stating that the purpose of the events of January 6th was to ensure that the voices and votes of moderates and independents do not matter in the party's decisions.

### Quotes

- "The Republican Party has no place for you anymore."
- "The inability of moderate Republicans to alter the conversation shows that the Republican party as you knew it is gone."
- "They don't represent you and it will probably be a long time before they will."
- "You're a rhino. You're not conservative enough."
- "They want to rule you, and they've shown that they'll use just about any means to do it."

### Oneliner

Beau warns independents and moderate Republicans that the Republican Party has shifted too far right, leaving no space for them, as Trumpism continues to control and suppress their voices and votes.

### Audience

Independents, Moderate Republicans

### On-the-ground actions from transcript

- Join or support political movements or parties that better represent your beliefs (implied).
- Vote strategically in elections to support candidates who uphold moderate and independent values (implied).

### Whats missing in summary

The emotional impact and urgency conveyed by Beau's message is best experienced by watching the full transcript.

### Tags

#RepublicanParty #Moderates #Independents #Trumpism #Authoritarianism


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about
the Speaker of the House, but we're going to talk
specifically to independents, to people who are independents
and maybe to a few moderate Republicans
who just realized what's going on.
As you hear more and more about the new Speaker of the House
and you hear about the things that he said, or alleged to have been involved with, his views,
you have to acknowledge that for people who consider themselves in the center,
or people who consider themselves independents, or moderate Republicans,
You have to acknowledge that the Republican Party has no place for you anymore.
Because the moderate Republican, that nominal conservative, they don't have any power,
not within the Republican Party, it's completely been overridden.
As you look at that new speaker, please understand that the moderate Republicans, the so-called
moderate Republicans within the Republican Party, they either explicitly endorse having
somebody with those views as speaker or they were powerless to stop it.
It's one of the two.
The Republican party is continuing to slide right, further and further right.
To the right of the traditional Republican is overt authoritarianism.
That's what's waiting for you over there.
The attempted coup probably should have been a sign.
Those people who are in the House who consider themselves moderate Republicans, they are
more prone to compromise. That's what makes the moderates, right? Because of
that, they continue to give in to the far-right factions. So the Republican
Party continues to move further and further right. Further and further
towards authoritarianism. Further and further towards determining what you get
to do in your daily life and what you're not allowed to do.
The inability of moderate Republicans to alter the conversation shows that the Republican
party as you knew it is gone because they either explicitly endorsed having
the new speaker in that position or they were totally powerless to stop it. They
have compromised so much over and over again that they don't see a way back.
they have forgotten that you do not turn a tiger into a vegetarian by
continuing to feed its steak. If you're an independent, if you're a moderate
conservative, you have to acknowledge there is no place for you in the
Republican Party anymore. It's gone. They don't represent you and it will probably
be a long time before they would because it's going to take a long time for the
Republican Party to move back to a position that would be similar to Bush
or Reagan or any of the conservative icons. They don't have a home for you and
They don't want you either.
You're a rhino.
You're not conservative enough.
And just like they turned on McCarthy, they will turn on you.
For those people, this has to be the moment where you realize it wasn't just Trump.
A whole lot of you in 2020, you're like, yeah, no more of this.
And you went the other way.
You're going to have to do that a few times because it wasn't just Trump.
Trumpism infected the Republican Party.
That far-right faction still has control.
And they still want to make sure that your voice, your vote, does not matter.
Make no mistake about it, the whole purpose of everything going on with January 6th was
to make sure that those moderates, those independents, those centrists that voted
the other way, was to make sure that your vote didn't matter, that your voice
didn't matter. They don't want to represent you, they want to rule you, and
they've shown that they'll use just about any means to do it. This needs to be
wake-up call because that clown show isn't changing. Not anytime soon. Anyway,
It's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}