---
title: Let's talk about water, ice, and Antarctica....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=yYhNqQZ7h7o) |
| Published | 2023/10/26 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talking about West Antarctica, water, and ice, with recent information suggesting a loss of control over the West Antarctic ice shelf.
- Regardless of efforts to control or reduce emissions, rapid decline is expected for the rest of the century.
- Even ambitious climate goals won't prevent the decline, leading to inevitable consequences.
- When the ice melts, the water goes into the ocean, raising concerns about sea level rise.
- The potential impact could be up to 5.3 meters, equivalent to around 17 feet for Americans.
- Recommends using a sea level rise viewer by Noah to visualize the potential effects of rising sea levels.
- Moving the slider to simulate rising sea levels shows the potential impact on cities like Miami, Charleston, San Francisco, and parts of New York.
- Infrastructure to accommodate rising sea levels, even at just 10 feet, will be a significant challenge.
- The process of rising sea levels is uncertain, but preparing for a 10-foot rise could take around 80 years.
- Climate change is a matter of national security and economic viability, not something that can be ignored.

### Quotes

- "Climate change is a matter of national security."
- "This should be a campaign issue in every election, everywhere, forever."
- "This is bad news."
- "It's just a thought, y'all have a good day."

### Oneliner

Beau talks about the loss of control over the West Antarctic ice shelf, leading to inevitable consequences like rising sea levels and the urgent need to address climate change as a matter of national security and economic viability.

### Audience

Climate advocates, policymakers

### On-the-ground actions from transcript

- Use the sea level rise viewer by Noah to understand the potential impact of rising sea levels (suggested).
- Recognize climate change as a matter of national security and economic viability, advocating for it to be a campaign issue in every election (implied).

### Whats missing in summary

Visuals and interactivity from the sea level rise viewer by Noah are missing in the summary.

### Tags

#Antarctica #ClimateChange #SeaLevelRise #NationalSecurity #CampaignIssue


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about Antarctica,
West Antarctica, to be precise.
And we're gonna talk about water and ice.
And what happens when ice turns to water?
Because if you have missed the news,
the latest information suggests that
when you are talking about the West Antarctic ice shelf,
we kind of lost control that basically no matter what we do now as far as
attempting to control or reduce emissions well it it's lost in that we
will see rapid decline for the rest of the century. That's obviously bad news
that no matter what we do even if we hit those like ambitious climate goals
that you hear about on the news that we never really hit anyway but even if we
were to hit those that well there's nothing nothing can be done when it
comes to this particular issue. Now the obvious question is if it melts where's
the water go? And the answer is into the ocean. Then the next obvious question is
sea level rise, and how much it's really going to impact. And we don't really have
the answer to that. We know that it could be up to 5.3 meters. 5.3 meters for
Americans it's I don't know like 17 feet so here's the thing I'm gonna have a
link down below and it is the sea level rise viewer by Noah and you can go
there and there's like a slider that you can move and like determine how many
feet the sea level has gone up and all of that stuff and there's little landmarks
and you can click on it and actually see photos and what it would look like if
the sea level rose that much. It's interesting and as you sit there and
play with the slider, you know, move it and make Miami disappear, Charleston most
to Louisiana, think San Francisco, parts of New York. Anyway, yeah, there's a whole
lot to it. One of the things to note is that when you move the slider all the
way up, that's only 10 feet. That's only 10 feet. This is really bad news. Now, how
quickly will it occur? Don't really know that. They seem to believe that it'll be
a semi-gradual process, but understand creating the infrastructure to
accommodate let's just say 10 feet. Let's not do you know the whole 17 point
whatever. That's if we started work today it would probably take 80 years to get
it all in place.
This is bad news.
For those people who really don't like people from other places and all of that stuff, you
only think we have a refugee crisis right now.
Climate change is a matter of national security.
It is a matter of economic viability.
This isn't something we can continue to ignore.
This should be a campaign issue in every election, everywhere, forever, now.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}