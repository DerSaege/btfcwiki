---
title: The Roads Not Taken EP7
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=RzgLJdpXSoI) |
| Published | 2023/10/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces episode seven of "The Road's Not Taken," a series discussing under-reported events from the previous week.
- The US is providing more than just arms to Ukraine, funding everything from seeds to salaries of first responders, showcasing a different style of foreign policy.
- Reports indicate a Russian anti-Putin group operating in Russia, raising concerns.
- Only 5% of tanks destroyed in Ukraine were taken out by other tanks, hinting at a shift in military strategy.
- Putin is reportedly making battlefield decisions without consulting his generals, a concerning authoritarian trend.
- Tensions rise in Kosovo, with Serbia staging a buildup near the border, but signs of withdrawal suggest de-escalation.
- China's economy stabilizes due to a boost in manufacturing, contrary to previous reports of stumbling.
- Kaitlyn from LA proposes a plan for funeral reform, mirroring existing government policies but not yet implemented.
- The Biden administration allocates $1.4 billion to improve rail safety across 35 states.
- California raises minimum wage for fast food workers to $20 per hour, potentially setting a precedent for neighboring areas.
- General Milley's speech sparks controversy as he affirms loyalty to the Constitution over individuals, drawing criticism from Trump supporters.
- Hunter Biden sues Rudy Giuliani over spreading misinformation about his laptop.
- Cultural news includes Republicans targeting Taylor Swift and the American Library Association celebrating the freedom to read.
- Lego continues efforts to find sustainable materials for their iconic blocks, despite setbacks in previous attempts.
- Concerns arise over excessive plant growth in Antarctica and Swiss glaciers losing 10% of their volume in two years.
- Elon Musk engages with the German Foreign Office on social media, discussing migrant rescue efforts at sea.

### Quotes

- "This is a style of foreign policy that a whole lot of experts have been advocating for 20 years and the US has never taken it seriously."
- "Swearing an oath to an idea, it isn't uniquely American, but it's not common and this is something that is referenced all the time."
- "Money is a tool there, not the ends. But again, for the people caught up in the middle of it, I don't really think that that distinction matters."
- "Having another country's economy tied in some way or dependent on the US economy is power. It's all about power, always."
- "There is no way to section it off and say, oh, it's not going to impact us. It absolutely will."

### Oneliner

Be the world's EMT, not the world's policeman: redefining foreign policy towards aiding communities over military intervention and power struggles.

### Audience

Policy analysts, activists, global citizens

### On-the-ground actions from transcript
- Monitor and advocate for sustainable foreign policies that prioritize community aid and support (implied)
- Stay informed about international tensions and conflicts to support peaceful resolutions (implied)
- Support initiatives for rail safety and infrastructure improvements in your local area (implied)

### Whats missing in summary

Detailed insights on Biden administration's foreign policies and potential impacts on global dynamics.

### Tags

#ForeignPolicy #CommunityAid #Sustainability #RailSafety #EconomicPower #GlobalImpacts


## Transcript
Well, howdy there, internet people, it's Bo again,
and welcome to episode seven of The Road's Not Taken.
This is a weekly series where we go through
and we talk about events that occurred in the previous week
that were either under-reported in general,
not reported on my channel,
or it's information that I think we'll end up seeing again,
And it will be useful context for future events.
After last week and revealing my little boo-boo on my thumb, a lot of people actually wanted
to see the werewolf decoration that attacked me.
That's it behind me.
Okay.
So starting off, foreign policy.
So 60 Minutes put out a piece explaining that the U.S. was providing more than just arms
to Ukraine.
The U.S. funding is going to everything from seeds and fertilizer to paying the salaries
of first responders to helping businesses.
Good.
When I talk about being the world's EMT rather than the world's policeman, this is what
I'm talking about. This is a style of foreign policy that a whole lot of
experts have been advocating for 20 years and the US has never taken it
seriously. Generally speaking that style of foreign policy would be something you
would do before the war would start but seeing it implemented in any way is good
in my opinion. Okay, there are reports that a Russian partisan group that is
anti-Putin is operating inside Russian territory. Probably going to come up
again. There was an interesting report that is suggesting that only one out of
20 tanks, just like 5% of the tanks that were destroyed in Ukraine were destroyed by other
tanks.
This is probably going to trigger a shift in what's provided to Ukraine.
Short version, super high tech tanks may not really be that important, which is odd given
how much debate was put into that, but it may be a situation where AT missiles and stuff
like that are far more important, because tank-on-tank battles not really occurring
the way people pictured.
There is reporting that suggests Putin has been increasingly making battlefield decisions
without input from his generals.
Wow, does this sound really familiar?
Like every authoritarian strongman ever.
Taking command away from them saying he can do better, that kind of stuff.
It's worth noting Putin was never like a theater commander or anything like that.
So we've seen this play out before, normally in bunkers.
There is a major situation developing, but also news coming out just hours before I filmed
this seems to indicate that it might actually be diffusing.
But there's a major situation that's been developing in Kosovo, which has raised tensions
between Serbia and, well, I mean, pretty much everybody, to be honest.
Serbia appeared to have staged a buildup near their border.
And at this point, they said they had no intentions of doing anything with it.
And there does appear, as of latest information, to be at least some withdrawal occurring.
So it may be over, but the situation itself is probably something we're going to see
referenced and it may come up again pretty quickly. We have talked recently
about how China's economy was stumbling but not falling the way a lot of
reporting was suggesting. It appears that it is starting to stabilize and it looks
like that's mainly occurring because of a pretty good boost in manufacturing
Which, not really surprising.
Okay, moving on to U.S. news.
Okay, Kaitlyn, over on another YouTube channel, I'll put the link down below, I first like
really became aware of her during the pandemic.
She runs some funeral stuff out in LA.
And she put out this video and she was talking about how bad things had gotten and how kind
overworked they were, and she came up with this plan, and I was like, yeah, that's actually
the plan.
They're just not implementing it.
That's the mass cash plan that the US government has.
Given how close her plan was to what experts working decades developed, I started paying
attention to her because, you know, expert in the field and all that stuff.
She is working on something dealing with funeral reform and how basically how consumers are
treated during this process.
And I found it really interesting.
I'm saying it's US News because I think it should be.
It'll be down below.
Definitely worth checking out.
The Biden administration announced 1.4 billion to improve rail safety and capacity.
money is being divided up over 35 states. So if the system in your area starts
getting better or you see the work occurring, that's what's going on.
California fast food workers will be getting $20 per hour as a
minimum wage after some new legislation. That will probably get a lot of push
back from the right and will also set the tone for some other locations nearby.
RFK Jr. is reportedly looking at a third-party run.
I don't think anybody's really surprised by that.
There is a lot of talk about him being a spoiler for Biden.
I'm not sure that's going to work out the way people think.
Given a lot of his views on a lot of hot button topics, he may actually end up taking
votes away from the right in larger or equal number.
So we'll have to wait and see and wait and see whether or not it really happens and all
of that stuff.
But it is, it looks like we'll hear an announcement about that soon.
Okay, General Milley gave a speech and you probably saw coverage of it.
And the coverage said, well, he called Trump a wannabe dictator.
And that was the coverage.
In that coverage, it spanned the political spectrum.
Trump supporters were very upset about this.
I'm going to read what he actually said because I think that context is important.
So we're just going to go through this.
He said, we don't take an oath to a king or queen or tyrant or a dictator.
we don't take an oath to a wannabe dictator.
We don't take an oath to an individual.
We take an oath to the Constitution.
We take an oath to the idea that is America and we're willing to die to protect it.
Trump's not in that.
So my question is that if you are a Trump supporter and this bothered you, exactly which
one of these do you think he is?
This type of speech, this is actually super common in military circles.
Swearing an oath to an idea, it isn't uniquely American, but it's not common and this is
something that is referenced all the time.
So seeing this and immediately assuming that it's about Trump, even given some of the things
that Trump has said, I feel like it's a situation where people might believe that Trump is one
of these things.
But Milley didn't say that, just putting that out there.
Hunter Biden is suing Rudy, apparently, over the laptop and the spread of that information.
Biden stood on the picket line with striking workers.
That was covered pretty well.
Trump also went to see workers, however, it was at a non-union plant.
That part wasn't covered so well, but there's been an interesting development.
One individual in the crowd who held a sign that said, Union Members for Trump, acknowledged
that she wasn't a union member when approached by a Detroit news reporter after the event.
Another person with a sign that read, Auto Workers for Trump, said he wasn't an auto
worker when asked for an interview.
I find that interesting.
Biden's dog, a secret service officer again.
I have German shepherds and I trust German shepherds a lot.
One thing that I will tell you that I find very interesting is that one of mine, there
There are certain medications that somebody can be on, and if they're on them and their
heart rate is elevated, she does not like them.
She does not want them around.
I just think that might be interesting.
That might be something to keep in mind when it comes to other events.
That's something that I have found interesting since this whole dog biting thing started.
The judge in the DC case for Trump has denied the request for a recusal.
The US Navy will start randomly testing special operations troops for performance enhancing
drugs.
This has been something they've been resisting for a whole bunch of reasons.
I think that there has been an overlap between things that people were kind of OK with looking
the other way on and substances they don't want them taking.
And I think that's leading to this.
Joseph Roberts, a man held up by parts of the Trump administration as a victim of the
MeToo movement who faced false allegations and all of that stuff, has been arrested for
killing his girlfriend, possibly fiancé.
The reporting varies.
These circumstances are, they're something else and you will undoubtedly see coverage
about that this week.
Moving on to cultural news.
Republicans are going after Taylor Swift, basically really kind of pushing at the idea
that somehow she's causing the decline of the United States.
You know what?
after Taylor Swift and Disney and Lego. Go right ahead. I think that that's going to
be a great election strategy right there. I think that'll really pay off for them in
the end. Okay, the American Library Association. Every year they have a week to celebrate the
freedom to read, to talk about censorship and all of that stuff.
Incidentally, it starts today this year.
A district in North Carolina reportedly canceled school events related to it and banned books
week.
After some media attention, that directive was reportedly retracted.
We'll see how it plays out from there.
of Lego, the toy maker for the little bricks that everybody knows, they are continuing
on a project to find a sustainable material to make those little iconic blocks out of.
Their last attempt was found to not be as sustainable as they hoped, but rather than
just throw in the towel, they're doubling down and they're going to try again.
The company has spent years trying to find a substitute and testing just hundreds of
different materials.
Their goal, they want to move away from something that is oil-based, however what they're running
into is a situation where, okay, we solved one problem but now the emissions are higher.
So they're putting in good work trying to find a sustainable way to produce their product
and they're investing a lot in it.
When companies do that, I think it's important to acknowledge it, especially since they're
not being forced to.
In more climate news, there is reportedly a massive amount of plant growth occurring
in Antarctica.
This is alarming a lot of people who study the climate.
Basically, the amount and the density of the plant growth
there is just way more than it should be.
And it's something that they have found very concerning.
In more climate news, Swiss glaciers
lost roughly 10 percent of their volume over the last two years.
Not good.
In some odd news, an ex-Wagner commander has reportedly been asked to form a new force
and head back to Ukraine.
It's important to note that Old Andre, the person that they have tasked for this, is
kind of deemed a traitor by most of the rank and file of Wagner, who believe he kind of
sold them out to move and work with a group that is more closely linked to the Ministry
of Defense.
I'm not sure how this is going to play out, but it should be interesting.
Elon Musk got into it with the German Foreign Office.
Musk apparently saw imagery of migrants at sea being rescued and asked is the German
public aware of this on his social media platform.
The German Foreign Office responded and said yes, and it's called saving lives.
I wish our State Department was that cool.
Video is giving the world its first real clear images of aircraft carriers that were lost
during the Battle of Midway.
That footage should be widely available probably by the time this video goes out.
For those who aren't up on this portion of history, this was a major battle during World
War II.
Okay, so that's a quick review of the week, and now we will move on to a very quick Q&A.
Doesn't look like there's many of them.
Do you ever evaluate the U.S. foreign policy?
If so, do you think U.S. foreign policy is driven by big business?
Yes, but not in the way I think most people believe it is.
Foreign policy is about one thing and one thing only, right?
Power.
What's money?
Power coupons.
So U.S. foreign policy, along with most countries' foreign policies, this one isn't as universal.
There are some countries that actually have rules about this.
But most countries' foreign policy favors the business interests of their own country.
The United States has turned this into an art form.
So yes, but it isn't necessarily out of a profit-driven interest in the way of literal
cash for use.
It's more about having the economic power, those power coupons, to be able to effect
power and project power on the international stage.
What does this mean for you or the people impacted by it?
Nothing.
Most people's perception, if you're asking this question, your perception of it is probably
right.
The difference is motive.
The people making these foreign policy decisions, they're not really pocketing much cash from
it.
That's not what's going on.
They're doing it from a perspective of a strong US economy is power.
Having another country's economy tied in some way or dependent on the US economy is power.
It's all about power, always.
Money is a tool there, not the ends.
But again, for the people caught up in the middle of it, I don't really think that that
distinction matters.
That's more of an academic thing.
If you're asking this question, you're probably headed down a road to a pretty deep understanding
of foreign policy because it's definitely tied into it.
Just don't think that it's exercised in that way for the personal enrichment of the people
implementing it because it may not be.
This is a thing, when we talk about foreign policy, most times I'm like every country
in the world does X, Y, and Z. Not every country in the world does this, but every world power
does.
That would be a good line.
Most regional powers do as well.
Okay, Bill Gates said no temperate country is going to be uninhabitable.
Shouldn't that stop climate change this area?
I saw this.
I would point out that he wasn't saying don't worry about it, but I think the important
note here is no temperate country.
That's the US. We're a temperate country for the most part. I mean that that's us.
Okay. It's not going to become uninhabitable. So let's say that's true.
The implication here is that countries that aren't temperate will be. What do
you think the people in those countries are going to do? Do you think they're
just going to sit there and just be like, well, I lost the geographic lottery.
I guess I'll die.
Or do you think they will migrate?
This will not exist in a vacuum and affect only certain areas.
This is a global issue.
There is no way to section it off and say, oh, it's not going to impact us.
It absolutely will.
There is nothing that is going to change that.
It doesn't matter who says it.
We are already going to feel the impacts and please understand something that just because
something isn't uninhabitable doesn't mean there wasn't a significant change that caused
internal displacement within the United States.
to read this statement and think everything's going to be fine.
That's not what he was saying.
Okay, answer the big question.
Did Elon have his hat on backward?
Oh, man.
Okay, so if you missed the news, this wasn't something I was going to bring up.
Elon Musk did the obligatory trip to the border for those people who want to rile up the
right wing. He went down there and did that. While he was there, he was wearing a cowboy hat, which
he looked very comfortable with. It was discussed as whether or not he was wearing it backwards.
Okay, so I actually don't think so. I think he just has a weird hat.
Yeah, the dents are normally up front, they were on the back, however the buckle is generally
supposed to be where he had it.
So if I had to guess, this is maybe a designer hat, something like that, that maybe it was
trying to make a statement. I would not wear that hat. I'm just saying. It does look like
it's on backwards. However, by tradition, the buckle was in the right spot, and that's
kind of one of the things that matters. I can't believe I'm doing fashion stuff now.
But yeah, he definitely needs a different hat.
I watched that old video about the Kennedy Green Beret story recently.
It sent me down a rabbit hole.
I did the math, and 94% of those who resisted Trump's coup were or were at one time Green
Berets.
Is that a real story?
People who are Green Brace, people who are Special Forces, they often end up in command
positions.
It is very hard to become a Green Beret.
That opens a lot of doors for you, so they would likely be in command positions.
That in and of itself is not unusual.
94% of those who resisted... I mean, I can't check your math on that.
I don't know how you calculated that.
As far as this story, and for those who aren't aware, there is a rumor that won't die that
says that Kennedy basically set up a secret organization inside the Green Berets that
Is there to stop a president from becoming tyrannical or Congress, I guess.
It's a story I relayed years ago on the main channel.
Is it a real story?
I mean, it's a real story in the sense that it's a legend that has survived for more
than half a century.
Is that what happened?
I don't know, and I have a feeling that, let's say that it is true, okay?
Failures are known, successes are not.
Generally speaking, Special Forces guys know how to keep a secret.
If it's true, we'll never know.
But the legend itself, yeah, it's been around a really long time.
So how many tan caps and patches do you actually have?
Every video your cap looks new or barely worn.
I change them out.
The patches are Velcro.
I change out the hats and it's funny because in a recent Q&A, like the hour and a half
long one, I talked about this.
They're sitting over there.
I don't know why I haven't thrown them away.
I still have the one I wore in Ferguson.
So when they get worn or stained, I get a new one.
As far as how many patches do I have, I have no idea.
50, 100, somewhere in there.
Most of them are not necessarily fit for YouTube.
They are generally things that are given to you by other people, those patches.
A lot of the ones I have are just not really fit for public consumption.
I probably have 20 that at some point would make an appearance on the channel.
That might actually be a fun story time.
In fact, I got a question the other day that I never answered that's kind of related to
this.
I had somebody ask if, since a lot of these patches, if you don't know, a lot of the
moral patches. They have multiple meanings and sometimes they are incredibly subtle and
they're not always well known. And I had somebody send a message asking if I had ever
encountered a weird situation where somebody was wearing something and they didn't really
know what it meant. And the answer to that is yes. I was covering something in DC and
I saw this guy wearing a patch that said Soul Stealer.
And I was like, oh, OK, so he's part of the club.
So I go over, and I start talking to him.
And I'm like, so who are you here with, assuming that he's there covering one of the many groups
that was there?
And he's like, well, I don't really see how that's any of your business.
And I'm like, okay, weird, whatever.
And then I asked, just at this point, I knew I was annoying him and was kind of enjoying
it.
I was like, so what kind of glass are you using?
And he's like, no, I use iron sights.
I'm like, okay, Zoll Stiller.
So that is actually a patch related to being a photographer.
I know it sounds like, you know, super hardcore, but yeah, he was actually part of one of the
right wing groups that was there demonstrating.
And I guess he just thought it made him look tough.
But yeah, that's actually a photographer's patch.
That to me was entertaining and that kind of brought that story back to mind.
So that is it for the Q&A and for this week.
I would definitely watch the situation in Serbia because at time of filming it does
appear to be kind of calming down but that could change at any moment.
And other than that, most of this is context for stuff there's going to be more reporting
about next week.
But it will help shape the information, and having the right information will make all
the difference.
Y'all have a good night.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}