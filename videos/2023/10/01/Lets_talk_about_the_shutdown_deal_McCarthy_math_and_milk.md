---
title: Let's talk about the shutdown deal, McCarthy, math, and milk....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=MHGPz0xec0s) |
| Published | 2023/10/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The deal was reached, averting a shutdown, opening up 45 days to finalize a real budget.
- McCarthy surprisingly voted with Dems, showcasing a shift in power dynamics within the Republican Party.
- The House vote was 335 to 91, with 209 Democrats for it and 126 Republicans for it.
- Despite social media appearances, the far-right faction of the Republican Party is not as powerful as believed.
- The Senate swiftly passed the resolution 88 to 9, extending the shutdown deadline.
- McCarthy's delay weakened but did not render the far-right faction irrelevant.
- Progressives and far-right Republicans are unlikely to push through legislation until the next election.
- If McCarthy stands firm, bipartisan cooperation will freeze House activity until the next election.
- The real budget negotiations in the next 45 days are expected to be basic, with separate votes for aid to Ukraine.
- The Democratic Party showed power by beating Republicans, but the bigger win is McCarthy beating the far-right Republicans.

### Quotes

- "The Democratic Party, they beat the Republicans."
- "McCarthy caved in that sense in the partisan fight."
- "It is that faction of the Republican Party that is beating the drums of authoritarianism."
- "Every time the Republican Party reaches across the aisle, they become more and more irrelevant."
- "Shutdown temporarily averted for the next 45 days."

### Oneliner

The deal reached averts a shutdown, showcasing a shift in Republican power dynamics and setting the stage for basic legislation until the next election.

### Audience

Politically active citizens

### On-the-ground actions from transcript

- Contact local representatives to push for bipartisan cooperation (suggested)
- Stay informed about budget negotiations and political developments (exemplified)
- Support initiatives that prioritize basic legislation and cooperation (implied)

### Whats missing in summary

Insights into the potential long-term effects of bipartisan cooperation and McCarthy's role in shaping Republican dynamics.

### Tags

#BudgetNegotiations #BipartisanCooperation #RepublicanParty #DemocraticParty #PowerDynamics #ShutdownAverted


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about the deal
because it happened.
The deal was made, shut down averted.
So we are going to talk about the deal, how it happened.
We're gonna talk about some simple math
and that math is incredibly telling.
It tells us a whole lot about how much power
a particular segment within the Republican Party
actually has.
So we'll talk about that and we'll talk about where it goes from here.
Now if you missed the news and have no idea what I'm talking about,
deal was reached.
Continuing resolution.
Basically they bought 45 days
to come up with a real budget.
McCarthy,
something I was told,
would not, could not happen because vote with Dems and never get elected again.
Yeah, that happened. McCarthy reached across the aisle. That message aged like fine milk.
Okay, so what were the votes?
335 to 91 in the House.
335 to
91 in the House. That tells us a whole lot.
209 Democrats voted for it.
One voted against it.
it. People are obviously going to ask what's up with that one Democrat. They
absolutely would not vote for it unless it had aid for Ukraine and it doesn't
because this is the continuing resolution. So that's that's that. Now
for the Republicans, 126 voted for it and 90 voted against it. Which one of
those numbers is bigger. 126, right? 126. That is definitely more than 90. The far
right faction of the Republican Party is not as powerful as they appear on social
media, a majority of Republicans, not even close, 126 to 90, a majority of
Republicans were in favor of a normal bill, of just a clean bill, keeping the
government open, keeping the lights on, and letting everything move forward.
So keep that in mind as we move forward in this conversation.
Once that happened, went to the Senate. Got to the Senate as we have talked about
before. The Senate is full of more deliberate people past very very quickly
88 to 9. Okay, so the shutdown is averted for 45 days. During this time they have
to work out a deal, but aren't they in the same situation they were yesterday?
No. Because the Band-Aid was ripped off. For months we have been talking about
how McCarthy's real move here, the McConnell move, you know, the just
absolutely ruthless move, was to reach across the aisle and render that far-right
faction of Republicans irrelevant. Because he waited so long, they're not
irrelevant, but they are weakened. They are weakened. They might try to file a
motion to vacate. In fact, it appears that one of them tried to do that. At least
that's the assumption. We don't know because WOMAC just wasn't hearing it.
Like literally just would not acknowledge them and gaveled out and
adjourned, which I find entertaining. Womack made of an enemy for life, I'm sure,
but whatever. They might try to do a motion to vacate, maybe the Democratic
Party protects McCarthy, but at this point what was demonstrated is that the
majority of Republicans are willing in a very high-profile way to reach across
the aisle. That's what happened. Now, what does that mean from here? Nine days ago I
put out a video, I'm not going to repeat the whole thing, but the short version
is nothing but super bland bipartisan stuff gets through. Progressives, until
the next election, you're not really going to get anything through the House.
But you weren't getting anything before. The far-right, they're not going to get
anything either. It will be the center of the House, the moderate Republicans, that
term being what it is and what people watching this channel what y'all would
call corporate dims. That's what's going through. Very basic, just keep everything
running type of legislation until the next election. Now that's assuming two
things. One is that McCarthy doesn't back down. If he backs down, he'll find
himself back in the same position he was in yesterday. And I know people are like,
why would he do that? Why didn't he do this months ago? It was the right move
then. He waited until the absolute last possible moment. So there is a little bit
a concern about him backing down. He shouldn't because he has the power now in the Republican
Party. And if he exercises it, maybe talks to McConnell's staff, gets some pointers,
he can actually start to be Speaker of the House instead of just constantly trying to keep
the clowns at bay. But if the deal holds, if the bipartisan cooperation holds,
basically the house freezes. Nothing happens until the next election. You'll
get real basic stuff. That's it. I'll put the other video down below. And that's
where we're at. Now they have 45 days to work out a real budget. My guess is that
it's going to be super basic and then there will probably be a separate vote
for any aid to Ukraine and I feel like it would pass but that may not be part
of the main budget. So Crenshaw was right. Crenshaw was right. We talked about it
and said, you know, he doesn't have a huge track record of predicting what's
going to happen, but as soon as the far-right Republicans voted down the
Republican bill, he said, well now we're just going to get a more liberal bill.
That's what happened. The bill that went through, it doesn't have any of the stuff
that the far-right Republicans wanted. They lost. Now, most people watching this
channel, the real happy news is the Democratic Party for once held that they
actually exercised power. It's kind of a weird feeling, right? They did it. And
and they beat McCarthy, they beat the Republicans.
That's the important thing for most people
watching this channel.
I think more important for the country
is that as long as he doesn't cave,
McCarthy beat the far-right Republicans.
That's more important.
It is that faction of the Republican Party
that is beating the drums of authoritarianism, and as long as he holds, as long as he doesn't cave,
every time the Republican Party reaches across the aisle, they become more and more irrelevant.
To me, that's actually the bigger, that's the bigger development and the bigger win.
Because that's something that's a little bit longer lasting than a ceasefire between
Republicans and Democrats in the House.
But yes, short recap.
Shutdown temporarily averted for the next 45 days.
The Democratic Party, they beat the Republicans.
McCarthy caved in that sense in the partisan fight.
In the internal fight within the Republican Party, McCarthy won.
But it's McCarthy, so just keep your fingers crossed that he doesn't back down because
he still might.
And that's it.
So we will have more about this later, I'm sure, as the negotiations for the final deal
start to happen.
But for right now, everybody can breathe easy.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}