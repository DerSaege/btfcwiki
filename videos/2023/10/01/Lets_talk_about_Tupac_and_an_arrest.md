---
title: Let's talk about Tupac and an arrest....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=1rJX3-iCLmM) |
| Published | 2023/10/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Two months ago, discussed a search in Vegas related to Tupac's murder, indicating strong evidence due to the search's conduct.
- Nevada grand jury indicted Dwayne Davis (Keefee Dee) secretly after months of investigation.
- Davis allegedly ordered Tupac's murder in 1996 as the on-ground commander.
- Indictment implies concrete evidence or testimony linking Davis to the event.
- Speculation arises about potential involvement of other individuals requiring legal representation.
- Developments in this case may continue.

### Quotes

- "A lot of times you can tell how much evidence the cops have by how they conduct a search."
- "Keefe D was described as the on-ground, on-site commander."
- "They have evidence that links him to the events, or they have testimony that links him to the events."
- "Based on that statement, kind of like the search, on-site, on-ground commander, that means that there were probably other people beyond those that have talked about it."
- "Anyway, it's just a thought. Y'all have a good day."

### Oneliner

Two months after discussing a search in Vegas related to Tupac's murder, a Nevada grand jury secretly indicts Keefee Dee for allegedly ordering Tupac's death in 1996, implying concrete evidence or testimony linking him to the event and potentially involving other individuals.

### Audience

True Crime Enthusiasts

### On-the-ground actions from transcript

- Stay updated on developments in the case (suggested)
- Support efforts seeking justice for Tupac and his family (implied)

### Whats missing in summary

Details on the potential implications and outcomes of the Nevada grand jury indictment could be best understood by watching the full transcript.

### Tags

#Tupac #NevadaGrandJury #Justice #Indictment #TrueCrime


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about Tupac again,
because there have been some developments.
So about two months ago, back in July,
we talked about a search that was conducted out in Vegas.
And at the time, I said that because of the way
the search was conducted, the odds were pretty high that they thought they had something
pretty concrete.
A lot of times you can tell how much evidence the cops have by how they conduct a search.
The way they conducted this one, it screamed, we've got a bunch.
Now at the time, the cops were telling us nothing.
you would call they'd be like oh well we'll transfer you to somebody who who
can give you some information and then you got disconnected they weren't
giving up anything but the overall consensus was that it had something to
do with the murder of Tupac. So there has been a development. Dwayne Davis has
been indicted. That's Keefee Dee, if you don't know. That is Keefee Dee. He has been indicted
by a Nevada grand jury, Nevada grand jury, and that grand jury has been working apparently
in secret for months, months. And this is all in reference to a 1996 murder. This is
what has come out so far. Keefe D was described as the on-ground, on-site commander, and the
allegation is that he ordered the death of Tupac. So that's the news. That's what we
know so far. Now there's going to be a whole bunch of people who are sitting there right
now going, yeah, we know was in the memoir. He talked about it in interviews.
Like, yeah, the indictment though rings that they have evidence, not just a
statement. They have evidence that links him to the events, or they have testimony
that links him to the events, and my understanding is puts him in the Cadillac.
But we'll have to wait and see. Based on that statement, kind of like the search,
on-site, on-ground commander, that means that there were probably other people
beyond those that have talked about it, that may be needing attorneys soon and
that the developments from this probably aren't over. Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}