---
title: Let's talk about the UK sending troops to Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=_IUNpjk0Wl4) |
| Published | 2023/10/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- UK considering sending troops to Ukraine to train Ukrainians.
- Beau questions the rationale behind sending uniformed troops into Ukraine.
- Beau sees sending uniformed NATO troops as a risky move without clear benefits.
- Beau suggests using civilian instructors instead of official troops for training.
- Beau expresses concerns about the potential risks of sending troops to Ukraine.
- Beau points out the existence of companies that specialize in military training.
- Beau questions the necessity of sending official troops when civilian instructors could suffice.
- Beau expresses skepticism about the decision-making process behind sending troops to Ukraine.
- Beau mentions the possibility of the proposal being a form of international signaling rather than a practical necessity.
- Beau hopes for an alternate, less risky method of providing training to Ukrainians.

### Quotes

- "I think it's a horrible idea. An absolutely horrible idea."
- "it does not seem like a good idea to me."
- "I don't understand the actual reasoning."
- "Special situations call for special solutions."
- "Anyway, it's just a thought."

### Oneliner

Beau questions the logic of sending British troops to Ukraine for training, advocating for civilian instructors over official NATO presence due to perceived risks and lack of clear benefits.

### Audience

Military policy analysts

### On-the-ground actions from transcript

- Hire civilian instructors for military training in Ukraine (implied)

### Whats missing in summary

Beau's nuanced analysis and skepticism regarding the UK's proposal to send troops to Ukraine can be fully appreciated only by watching the full transcript.

### Tags

#MilitaryPolicy #UK #Ukraine #NATO #Training


## Transcript
Well, howdy there, Internet people.
Let's bow again.
So today we are going to talk about the United Kingdom and a proposal that has
hit the news, and we're going to answer a question about it.
I know you normally don't like to tell people in other countries what their
governments or militaries should do, but our defense secretary is talking about
sending our troops to Ukraine to train Ukrainians in Ukraine.
I was wondering what you were thinking about this.
OK, so I would like to believe that this is just messaging,
that it's signaling to Russia and that that's all that it is.
On the off chance that it's not, what
What do I think about the United Kingdom sending British troops to Ukraine to be inside Ukraine
working?
Uniformed troops.
I think it's a horrible idea.
An absolutely horrible idea.
What is the benefit to the added risk?
Risk versus reward.
What are you getting for it?
Saving on transportation costs?
I don't think that's, I don't think that that's, uh, worth putting NATO troops there.
I don't think that's a good move and understand, I'm not opposed to the idea
of sending instructors in, but they shouldn't be troops pretend it's
2023, and hire one of the 15 companies that would be begging for that contract.
Sending in NATO troops on the ground, uniformed, regular troops that are there officially,
I don't think it's a good idea.
There's a lot of ways that that could go bad and I don't see the benefit.
If there is a situation that would greatly enhance Ukraine's war fighting capability by having instructors there,
there are ways to do that without committing official troops to it.
You're right, I don't like saying, hey, this is what this other military or other
country should do when it comes to stuff like this, but I don't really mind doing this
one because of NATO.
This seems to be, I don't want to say reckless because I'm not sure what prompted this.
it could have been a request that that we don't have the information to know
why they need it but based on available information I do not see any any value
that would justify the risk and I can't think of any situation period where the
The same outcome could not be achieved by using civilian instructors.
It seems very risky without a lot of reward.
The UK has contractors, same as everybody else.
And I'm sure that they would love a job going to the western side of Ukraine and just setting
up a little baby fort Benning.
I don't understand the actual reasoning.
So either there is something massive that I don't know and nobody I've talked to about
this it knows or hopefully it's just international signaling or they come
up with an alternate way to provide the training because if it was requested
and it's something that can be provided it can be done through contractors it
can be done through outside personnel people that aren't officially NATO
troops. That would seem a whole lot better to me. Yeah, it would cost more per
instructor per year when you're just looking at the salary, but not that
much more when you weigh the the risk of what could occur. It does not seem like a
good idea to me. I mean I understand that in conflicts there are special
situations. Special situations call for special solutions and given the fact
that this has been discussed publicly when they're saying troops, I'm
picturing uniformed troops and I can't I can't come up with a scenario which
would require that. The the British special operations community they're
not new like they know what they're doing and they have there are companies
that exist for this. Now my understanding is that the defense secretary is new, and
maybe it's just signaling. That's what I'm hoping, is that it's just messaging. Because
that just doesn't, it doesn't make a whole lot of sense to me. Anyway, it's just a thought.
I hope you all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}