---
title: Let's talk about Texas, Montana, and 2 cases about rights....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=dT9182HUIGs) |
| Published | 2023/10/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Texas and Montana cases aren't linked, but the legislation topics and tactics are similar.
- Texas legislature passed a bill under the guise of protecting kids from drag shows.
- The goal was to provoke outrage towards a marginalized group to manipulate voters.
- The court found that the Texas law could infringe on constitutionally protected activities like cheerleading and dancing.
- Authoritarian governments trick people by blaming out groups, passing laws that restrict rights.
- Montana passed a law banning gender-affirming care, claiming it's to protect kids.
- The judge in Montana called out the legislature for being disingenuous in their intentions.
- Legislatures use fear tactics to pass laws that other people and restrict rights.
- If authoritarian legislatures target marginalized groups, they're also eroding everyone's rights.
- It's vital for people in states with such legislatures to understand the impact on their own rights.

### Quotes

- "When you take away people's rights, understand you're signing away your own."
- "If authoritarian legislatures target marginalized groups, they're also eroding everyone's rights."

### Oneliner

Texas and Montana cases show how legislatures manipulate fear to pass laws that erode rights for all.

### Audience

Voters, Community Members

### On-the-ground actions from transcript

- Question legislative intentions and demand transparency (implied)
- Support organizations working to protect marginalized communities' rights (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of how fear tactics and manipulation are used to pass laws that erode rights for all individuals.

### Tags

#Legislation #Rights #Authoritarianism #FearTactics #Community


## Transcript
Well, howdy there, internet people, it's Bill again.
So today we are going to talk about Texas, Montana,
and two core cases.
The cases aren't linked.
Not, the suits themselves are not linked
in any way, shape, or form.
But they were both ruled on recently.
And while the cases aren't connected,
the topics that the state was trying to legislate,
those are connected.
The way they were sold to the public is connected.
And the reason the judges were like,
no, you actually can't do this.
This is unconstitutional.
That's connected as well.
And what it does is it shows how the Republican Party, when
it engages in its culture war nonsense,
manipulates the base and centrist independence
the expense of groups that don't have a lot of political representation. So we'll start with
the one in Texas. In Texas, they passed a piece of legislation. They sold it to the public
as protecting the kids from drag shows. And when they talked about it in public, it was all about
protecting the kids, they made it seem very narrow in scope. The goal certainly
appeared to be to rile people up, get them angry, provoke some outrage, direct
to that anger at a marginalized group so you know you can't really suffer any
backlash because they don't have big enough numbers. And then you can keep the
voting base angry about something, give them a little bit of moral validation,
and get their vote because that's how you win an election. You tell people who
to be mad at and say you'll punish them. That's what American politics is
devolved into. So it goes to court. What did the court find? The court sees no way
to read the provisions of SB 12 without concluding that a large amount of
constitutionally protected conduct can and will be wrapped up in the enforcement of SB-12.
When your average person hears this, they're like, well, maybe that kind of conduct,
maybe that shouldn't be constitutionally protected. Maybe those people, maybe they
don't need rights. But wait, there's more. It is not unreasonable to read SB-12 and conclude
activities such as cheerleading, dancing, live theater, and other common public
occurrences could possibly become a civil or criminal violation. See what
happened? They got people angry at a marginalized group, directed all the
anger at them, said, oh we have to protect the children from those others.
And then when nobody was looking, well they took your rights too. They passed a
law that would infringe on your constitutionally protected activities.
That's how it works.
That's what happens.
And that's how authoritarian governments trick people into giving up their rights.
They pick an out group and say, blame them.
just going to pass this legislation to protect you and your kids from them, those people,
those others over there because they're bad.
But when that legislation comes out, nobody reads it until it goes to court.
And maybe if you're lucky, if you're in a country that has a functioning check on legislative
If tyranny, you have a judicial branch that steps in, it's like you can't do this.
Because understand, if the judge hadn't have stepped in here, which I'm sure this judge
is going to be called woke or whatever, if the judge hadn't have stepped in here, it
would not be unreasonable to assume that cheerleading, dancing, live theater, and all this other
stuff.
could arrest you for that too. When you take away people's rights, understand
you're signing away your own. Okay, let's go to Montana. Montana passed a piece of
legislation that was banning gender-affirming care. And same, same thing.
We're gonna protect the kids. We have to protect the kids and that's how they
sold it, and they threw out words like experimental and all of this stuff. Then
it goes to court, and what we find out is that the judge, not just was
there an issue with the legislation itself, the judge said that the legislature was
disingenuous and that the purpose was to quote ban an outcome deemed undesirable
by the Montana legislature veiled as protection of minors. Why'd the judge say
that the legislature was being disingenuous? Because the same
legislature that sold this as super scary and experimental and all of this
stuff they actually passed legislation allowing patients to include minors to
opt to receive treatment that actually was experimental as long as they had a
a health care provider sign off. So what was it about? Othering people, getting you
to kick down. If they had stood, what would really be the case? Oh, you can
control your own health care because we're Montana and we're super free out
here. We'll even let you do experimental stuff. But we get to define everything
and if we don't think you need that treatment, we'll just ban it. When you
sign away other people's rights, you're signing away your own. Luckily, the
judicial branch stepped in and pointed out in this case exactly what
happened. It might be time for people who are in states that have authoritarian
legislatures to really take a look at what's being whittled away when they
tell you they're going after those other people because understand that they're
going after your rights as well anyway it's just a thought y'all have a good
day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}