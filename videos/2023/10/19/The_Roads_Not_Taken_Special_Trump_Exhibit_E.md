---
title: The Roads Not Taken Special Trump Exhibit E
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ZakfXWw3ME0) |
| Published | 2023/10/19 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The transcript covers various ongoing legal proceedings involving Trump in New York, Georgia, and federal cases.
- Trump showed up in court in New York, got animated, and was told to quiet down by the judge.
- A Trump supporter court employee was arrested for disrupting proceedings to get Trump's attention.
- Emails in the Georgia case suggest political motivations that could undercut the defense.
- Jury selection for co-defendants in the Georgia case is beginning soon.
- In the federal DC case, Trump faces a narrow gag order with potential severe sanctions for violations.
- Trump's attorney faces a deadline to address issues with the gag order appeal.
- Various civil cases, including one involving disenfranchisement of black voters, are progressing.
- Trump's political donations are heavily impacted by legal fees, hindering his campaign funding.
- Trump's influence within the Republican Party is waning, with politicians disregarding his endorsements.

### Quotes

- "Trump's legal entanglements get more and more tangled."
- "Having the right information will make all the difference."

### Oneliner

Beau gives an overview of ongoing legal proceedings involving Trump, from animated court appearances to waning political influence within the Republican Party.

### Audience

Political analysts, Trump critics

### On-the-ground actions from transcript

- Contact organizations involved in civil cases against disenfranchisement of black voters (suggested)
- Stay updated on legal proceedings and political developments (implied)

### Whats missing in summary

Insights into the potential implications of Trump's legal battles and political standing on future events.

### Tags

#Trump #LegalProceedings #RepublicanParty #PoliticalInfluence #CivilCases


## Transcript
Well, howdy there, internet people.
It's Beau again, and welcome to The Road's Not Taken,
special Trump Exhibit-E, I believe.
This is a series where we go through
and cover Trump events that were not covered
over on the main channel that you'll end up seeing later,
or it's something that's kind of interesting.
This is just to keep the main channel from devolving into Trump's central.
All right, so we will start off with the New York case and what's going on there.
Trump made a reappearance, you know, he showed up, he was there for a few days and then he left.
He showed back up at court, this case, he doesn't have to be there the whole time.
But while he was there, he reportedly became, well let's just call it animated, he became
animated and was told to quiet down by the judge.
In other events, a woman was arrested after reportedly disrupting the proceedings and
trying to get Trump's attention so she could talk to him because she wanted to offer her
assistance in some way, is what the reporting says. She is apparently a Trump supporter.
The woman is a court employee of some kind, and she was reportedly charged with contempt of
court and disrupting a court proceeding. Moving on to Georgia, the Georgia case.
The big news is that the New York Times is reporting that there are emails, and those
emails show Cheesebro basically talking about the political value of the suits.
This is something that might undercut his defense.
I would imagine that his defense would rest on, hey, I was doing my job.
The New York Times reporting is saying that in these emails, he is clearly talking about
how it would benefit politically, which that kind of falls outside of what was supposed
to be going on, but we'll see how it plays out in court.
Now, so far, in this case, only Scott Hall is publicly known
to have taken a deal with prosecutors.
But the question of who has or who might flip
is still reportedly being widely discussed and debated
within Trump's circles.
Now, jury selection for Cheesebro and Powell's case,
that begins on Friday.
Begins on Friday.
The jury is going to have to answer a questionnaire that
reportedly has more than 100 questions on it.
And it'll be filled out by, I want
to say the first batch is 450 Fulton County residents.
And that will help narrow down who can keep an open mind about the proceedings.
Now, in the federal D.C. election interference case, so Trump was subjected to a gag order,
a very narrow gag order, and we talked about that on the main channel.
The prosecution has indicated it will seek a wide range of sanctions if Trump violates
it.
That's been one of the big questions that has come in.
You know, what are they going to do if Trump breaks the gag order?
Those sanctions include moving up the trial date, financial penalties, criminal contempt,
and a whole bunch of others all the way up to and including revoking release.
That would mean he'd go to jail.
Now, Trump has filed appealing the gag order in the U.S. Court of Appeals, however, the
clerk noted that Trump's attorney was not a member of the bar of the court in question.
attorney has until November 2nd, I believe, to square all of that away.
This is made even more embarrassing when you remember that a Trump co-defendant in the
Georgia case recently argued that the whole case should be thrown out because a special
prosecutor didn't properly fill out some paperwork involving an oath of office.
A federal judge ruled against,
so this isn't part of the DC case, but it relates to it.
There's another civil case
that we really haven't talked about much on the channel,
but it is moving through its process very slowly.
And in that case, a federal judge ruled
against Trump's attempt to delay a civil case
seeking damages over the January 6th thing, Trump's attorneys were arguing that basically
there was too much overlap between this civil case and the pending criminal case, and the
judge said that that kind of claim, it's only going to matter if it matters ever once
discovery starts.
So that was not a reason to delay it, according to the judge.
Now moving on to additional cases and information.
There's yet another civil case that we haven't really talked about much on the channel, and
it's a case where Trump is being sued by the NAACP and some other organizations who
accused the former president, his campaign, the RNC, I think that's it, of disenfranchising
black voters by committing civil rights violation through the, quote, targeted harassment intimidation
and efforts to prevent the complete counting and certification of valid votes.
The reason this is interesting is because the calendar committee for this, well, they
gave the case to Judge Chutkin.
That's the judge that is overseeing the criminal case against him that just put a gag order
on him.
I'm sure he'll take that well.
Now, in yet another civil case
that we haven't talked about much,
Trump was deposed on Tuesday,
and this is involving a lawsuit
over the termination of an FBI official
and the release of text messages.
Now, in other Trump-related news,
In a recent video, I mentioned I wasn't sure how much,
if any, of Trump's political donations
were going to legal fees
and how that would impact his standing.
Because again, how much they raise per quarter
doesn't matter if they spend it all.
It's the cash on hand as they get closer to the election.
Well, the AP answered that,
and they pointed out that during the first half of 2023,
One of those PACs that is related to him, I think it's PAC, Save America is the name of it,
they spent more than $20 million on legal fees.
And that's basically more than any other committee that reports to the FEC.
It's more than the RNC, the DNC and the National Republican Senatorial Committee spent during
that period all added up.
So yeah, the legal fees are definitely hitting him and that will impact his ability to run
a normal campaign as if any campaign with Trump was going to be normal to begin with.
Now in related news, and this is more important than it's really getting, than it's really
being portrayed as and the coverage is kind of telling people, Trump tried hard and he
He threw his weight behind Jordan.
So far he's failed.
And he's failed because there are Republicans who are not following him anymore.
Trump's position within the Republican Party, as far as other politicians, it is fading
faster and it's weakening more quickly than his position with the base.
That may lead to some really interesting dynamics later.
There may be a moment where the Republican Party apparatus makes the decision for the
base.
If you were a Bernie supporter, you know how this can play out.
Obviously, this would be done for different reasons.
But when you have this much of the House of Representatives not following Trump's lead
on this, it's a pretty clear indication that his influence, it's not what it used to be.
And they know that.
The members of the House of Representatives know that.
And they've known it for a while.
But now, they're starting to act on it.
And they're disregarding his endorsements, his suggestions, his orders.
This happened before.
And it's just becoming more and more pronounced as time goes on.
And his legal entanglements get more and more tangled.
You know, when you are looking, even here where we're covering basically a recap of
his legal proceedings, we've been doing this a while and there are cases that have been
ongoing that are just now working their way into this cycle because there's that many.
You know, the big ones are New York, Georgia, the DC federal case, and the Florida federal
case.
Those are the big ones.
But there is just an almost endless stream of civil cases that are also working their
way through.
And then there are challenges to him even being on the ballot.
Trump is going to spend a lot of time in court.
So that's a brief overview.
This week was relatively slow.
I know that doesn't sound slow, but for Trump legal news, that's actually not a lot.
I would imagine that things are probably
going to start moving a whole lot faster what
with the New York case moving along and jury
selection beginning for his co-defendants
in the Georgia case.
That is going, there's going to be a lot of developments
there that impact Trump's Georgia case whenever
that happens.
So we will continue to monitor it and keep y'all up to date, provide you with as much
information as we can.
And having the right information will make all the difference.
Y'all have a good night.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}