---
title: Let's talk about an update on Biden's trip and more....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=vHuNmDGb5eU) |
| Published | 2023/10/19 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Providing an update on Biden's trip and the push for a limited ground offensive.
- Analyzing the conflict situation by relying on confirmed information and not trusting either side.
- Mentioning voice intercepts lacking confirmation, but Israel's drone footage and Palestinian photos corroborating each other's information.
- Refuting the idea of a joint direct attack munition based on evidence from the parking lot and the hospital's roof.
- Speculating on the burns' patterns indicating an errant rocket, with an 80% chance of it being an R160.
- Addressing doubts about the credibility of post-fact analysis in the current climate.
- Pointing out the importance of where the weapon hit to determine responsibility, discussing Israel's precision in munitions.
- Considering the possibility of another group being involved, albeit lacking evidence to confirm this theory.
- Emphasizing the need to look at available images from independent news outlets or the Palestinian side to verify claims in conflicts.
- Urging people to wait for evidence before jumping to conclusions and encouraging evidence-based claims.

### Quotes

- "The first casualty of conflict is the truth."
- "Wait until you get the evidence."
- "Y'all are free to do that, but if you're going to do it, try to support it with evidence."

### Oneliner

Beau provides insights on Biden's trip, conflict analysis, and evidence-based reasoning in determining responsibility in a crisis.

### Audience

Conflict analysts and evidence-based thinkers.

### On-the-ground actions from transcript

- Verify information from independent news outlets or the Palestinian side (suggested).
- Wait for conclusive evidence before drawing conclusions (implied).

### Whats missing in summary

Contextual understanding and deeper analysis can be gained by watching the full transcript.

### Tags

#ConflictAnalysis #EvidenceBased #BidenTrip #Responsibility #IndependentVerification


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to provide a little bit of an update
on what's going on over there.
We will talk about Biden, his trip,
what he is said to have tried to do and then we will talk
about the question that is consistently filling up
my inbox, even though I said I wasn't going to talk
about it until we had certain kinds of information. We needed certain kinds of
evidence before I was going to say anything.
Okay, so starting with the trip. By the reporting, Biden went over there and he
made the push. He pushed for a limited ground offensive. Some of this
was done in public. The assumption is that a whole lot was done in private. The
short version was that he used US experiences post 2001 to try to explain
that a full-on ground offensive is not the answer and just creates more
problems. Not just for the civilians on the ground but for the parties involved.
His goal was to limit that ground offensive and he made his pitch. Now we
get to see, we get to wait and see whether or not it worked and hope that it did.
Now as far as what everybody wants to know, who did it, right? That's the big
question. That's what everybody's interested in. When you are trying to
analyze something that is coming out of a war zone, you have to do it the same
way we've been doing it when we talk about Ukraine. Even though this is a much
more hot-button issue for a lot of people, you have to go off of what you
actually know, which means you can't trust either side. The first casualty of
conflict is the truth. So, those voice intercepts? Yeah, I mean they sound good,
they sound real, but there's no confirmation from the other side. So we
don't use those, but we don't need them. Something that was confirmed, Israel
showed some drone footage or some aerial surveillance footage. Let's just leave it
at that. And later a Palestinian posted photos of the parking lot. The dispersal
of the cars lines up. So now you have information coming from both sides that
confirms each other. Okay, so that you can trust. Okay, so based on that the idea
that it was a joint direct attack munition, no. Not based on those images.
That would be what Israel would have used. The dispersal of the cars is a
a pretty clear indication, another one being the clay tiles that are still on
the roof of the hospital. It is unlikely that those would be undisturbed.
All evidence suggests that it was not what Israel was using. So you have that.
the flip side to it. The patterns of the burns, I don't know much about that but I
sent it to somebody who does and they said that it is consistent with the
propellant burning off of an errant rocket. So I asked him to put a
percentage on that and he says 80% that it was an R160. So you have 98% chance
that it was not what Israel was using and an 80% chance that it was what the
Palestinian forces or one aligned to it because there's another option there
we're using. That's what you have. You also have this. There's a lot of people
using terms flattened and leveled. It hit the parking lot. It hit the parking lot.
So, all of this sounds good after the fact, right, but you would, you'd be
forgiven if in this climate you were like, hey, well this guy's just making it
up afterward, trying to find things that line up with what he wants it to be or
what people think I want it to be or whatever. Under the video, let's talk
about Biden getting on Truth Social. There is a pinned comment and that
pinned comment is 19 hours old at time of filming. Some highlights from it, based
based on available evidence. It's either a joint direct attack munition or an R-160,
which is absolutely no help because one side has one, one side has the other. The best
evidence will be where it hit. Was it on target? If so, it was Israel. If it was off center,
it wasn't. That's another thing that's important here. I personally do not put it
past Israel. If there was a high enough value target there, I think they would
have hit it. But if they were going to hit it, they would have hit it. Israeli
munitions are incredibly precise. So now not just are you talking about the
weapon itself malfunctioning to not create the typical pattern, but you're
also talking about the guidance malfunctioning and both happening at
the same time. And then more yeah that's the best evidence is whether or not it's
a direct hit and I know people who are favorable to Israel are going to say
well what would happen if it an errant rocket just randomly hit dead center. The
odds of that are super slim but yeah it's a possibility. I guess that's the
the cost of being the state in the situation. And then somebody said that
there was a another group at play other than the one that everybody's been
talking about. And that actually makes a whole lot more sense. Do not have
evidence to confirm this part. But there's another group that has the same
type of stuff that is reportedly active. They are also less well-trained, so they
would be more likely to make a mistake.
So, that's based off of the evidence that we have. Now, is there more evidence that
might change that? Maybe, but at this point, based off of what's available, it
It does not look like Israel is responsible for this one.
For those who have been repeating everything that they heard on social media,
I strongly suggest you look at the images that are now available from the daytime,
independent news outlets or from the Palestinian side itself and tell me if
what you have been saying lines up with those images. Not just in who's
responsible but for all of the other claims. Please remember these types of
conflicts it's a PR campaign with violence controlling the narrative
controlling the information that is put out is that that's the top priority. Wait
until you get the evidence. Now I know there's going to be a whole bunch of
other questions and trying to trying to create different scenarios to where it's
certain it was the side you don't like.
That's fine, y'all are free to do that, but if you're going to do it, try to support it
with evidence.
I'm sure there's evidence I haven't seen, but based on what's available, that's what
happened.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}