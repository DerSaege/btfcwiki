---
title: Let's talk about the Trump post we'll see again....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=L9sBjumMajU) |
| Published | 2023/10/19 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump posted about dropping a "fake case" by the Attorney General of New York, Letitia James, involving his financial statements.
- The post also included James's home address, which could lead to legal repercussions.
- Trump's post may result in widening the two gag orders against him and potentially introducing new orders in other cases.
- Beau questions why Trump's post is still up and suggests his attorneys should have advised him to remove it.
- Speculation arises whether Trump's team has a strategy behind keeping the post up, possibly to challenge the broadness of the gag order in future legal proceedings.

### Quotes

- "Her fake case against me should be dropped immediately."
- "It seems like that would be a good idea, but I mean I don't know. I'm not a lawyer."
- "It's just a thought y'all have a good day."

### Oneliner

Trump's social media post attacking the Attorney General of New York may have legal repercussions due to including her home address, potentially impacting future proceedings involving the former president.

### Audience

Legal observers, political analysts

### On-the-ground actions from transcript

- Contact legal experts to understand the implications of including personal information in public statements (implied).

### Whats missing in summary

Full context and analysis of the potential legal consequences and strategies related to Trump's social media post.

### Tags

#Trump #Legal #SocialMedia #AttorneyGeneral #GagOrder


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Trump
and something he posted on social media.
Big surprise, right?
And how this particular post may end up
being discussed in a whole lot of future proceedings involving
the former president.
I'll read the post, but it's not really the post itself that's the issue.
So this is what he says, he says, her fake case, and he is talking about the Attorney
General of New York, Letitia James, her fake case against me should be dropped immediately,
My financial statements are extremely conservative and her numbers are way off, including the
fact that she undervalued Mar-a-Lago and Doral by billions of dollars.
She also didn't reveal the 100% disclaimer clause at the front of the financial statements,
and that she sued me under a statute that was never used before.
carriage of justice and election interference, all wrapped up in one." All caps, exclamation point.
There's nothing really, you know, unusual about that particular post. However, it has an attached
article, and in the article is James's home address. I feel like this is
probably going to end up before a judge and it probably won't take too long.
The former president has been hit with two different gag orders. This, in
particular, is probably going to lead to those orders being widened and maybe
other orders being put in place in other cases because anytime somebody
asks for one now they'll be able to point to this and have a reason for the
request. I'm not sure why the former president would do this or why at time
of filming the post is still up. It seems as though his attorneys probably
should have told him to remove it.
It seems like that would be a good idea, but I mean I don't know. I'm not a lawyer.
Maybe they have a strategy behind it.
Maybe they're going to try to use this to argue that
the gag order
is too broad. I don't know, but I would imagine
we're going to see this material again. Anyway,
It's just a thought y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}