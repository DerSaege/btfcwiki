---
title: Let's talk about Trump, Biden, and money....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=xmS_zI_G4VE) |
| Published | 2023/10/19 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing third quarter fundraising numbers for Biden and Trump from July 1st to September 30th.
- Biden raised $71 million, while Trump raised $45.5 million during this period.
- Speculation arises on whether Biden's fundraising advantage indicates his potential victory.
- Biden's $71 million and Trump's $45.5 million should not be directly compared due to different contexts.
- Trump is still in an active primary, unlike Biden who is backed by the DNC.
- Trump's fundraising is not heavily influenced by controversial issues like witch hunts.
- The focus should shift from how much they raised to cash on hand.
- Biden has $91 million cash on hand, while Trump has $37.5 million.
- Having more cash on hand positions Biden better for the future but doesn't guarantee victory.
- Cash on hand is a more critical number for predicting campaign sustainability and success.

### Quotes

- "You need to look at what's going out as well."
- "That's the number that can tell you a little bit more about what's going on."
- "Cash on hand is the number that matters."
- "Biden is off to a pretty healthy lead."
- "A lot of things can change."

### Oneliner

Analyzing third quarter fundraising numbers: Biden raised $71 million, Trump $45.5 million, but cash on hand is the key indicator for campaign sustainability and success.

### Audience

Campaign strategists

### On-the-ground actions from transcript

- Compare cash on hand for political campaigns (suggested)
- Stay informed about fundraising numbers and cash on hand of political candidates (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of Biden's and Trump's fundraising numbers and the significance of cash on hand in predicting campaign success.


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are calling to talk about money,
cold, hard cash, elections, Biden and Trump,
and what we can tell from the numbers that came out.
The third quarter numbers, they have been released.
So this is what the groups were able to fundraise
from July 1st to September 30th.
Once the numbers came out, a whole bunch of questions came in.
And the big one is, is this as good for Biden as it looks?
And people were looking specifically at the amount raised during the third quarter.
During the third quarter, Biden and affiliated groups, because you have the
campaign and all of the other stuff that are, that are linked, but not officially
the campaign, $71 million, Trump, $45.5 million, that's a big gap, there's a lot
of people looking at that and saying, Biden's going to win because, you know,
that's a lot of, that's a lot of ad space right there, these are not the numbers
need to look at? I mean, they're helpful and don't get me wrong, those are definitely not bad numbers
for Biden, but they don't help with the long-term picture. This is what they raised. It's what they
brought in. 71 million? Yeah, that's good. That's good for a campaign. 45.5? Well,
Well, you can't compare it to the 71 million directly for a couple of reasons.
One is Trump is still in an active primary, an active primary.
I know that there are people making their play for the nomination within the Democratic
Party, but the DNC is actively working to keep the incumbent in office.
I want to say two, maybe three fundraisers were joint between the DNC and Biden.
Um, Trump doesn't have that yet.
Uh, the RNC is holding their money until after the primary, before anything
starts getting pulled and any extra stuff like that happens, but that
doesn't make a massive difference, shouldn't, not with Trump, the thing
that you can kind of infer from Trump's numbers here is that he's not
fundraising off of all of his witch hunts. All of the things he's calling
witch hunts, he's not fundraising off of those. I know that you're gonna say, no he
is, you gotta see these emails. Yeah, I mean he may be sending the emails out,
but it's not working. So you have that as well. But really these aren't the
numbers that you need to be looking at. This is just what's coming in during the
quarter. You need to look at what's going out as well. The number you should look
at is not how much they raise per quarter, it's cash on hand. That's the
number that can tell you a little bit more about what's going on.
Biden, 91 million.
So he has more cash on hand than he raised in the third quarter.
Trump, 37.5 million, less than he raised in the third quarter.
That's the numbers that you need to be looking at.
numbers right there. Now, does this mean that Biden's a shoo-in because obviously
he has more support? No, we're still a long way away from the election. This
shows that he is positioning for a better run. He's better positioned. That's
what it shows, but we are a long way away from that election. A lot of things can
change. I don't know specifically if the organizations that are included in
Trump's numbers are the same organizations that are paying legal
bills and stuff like that because that may also have an impact on how much
staying power there is in that 37.5, but right now, as far as cash on hand, which is the number
that matters, Biden is off to a pretty healthy lead, but there's a long way to go and things
can change. So this is a lot like polling this early on. Useful to know where things are,
but not really useful as far as knowing where they will be when it matters. Anyway,
It's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}