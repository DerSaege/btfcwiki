---
title: Let's talk about the new Speaker and 2024....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=oWzEZyrg4Zw) |
| Published | 2023/10/28 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses concerns about the U.S. House of Representatives and the new speaker in relation to the 2024 election.
- Mentions worries about trusting the new speaker with American democracy due to fears of another attempted coup.
- Credits Joe Manchin and Susan Collins for passing reforms at the end of 2022 that have made it more difficult to challenge state election results.
- Explains that the process to challenge election results now requires a larger number of House representatives, making it harder to attempt a coup.
- Points out that while the possibility of another attempted coup exists, it is now much less likely due to increased preparedness within government institutions.
- Emphasizes that the motivations behind the previous coup attempt, to keep someone in power, are not present this time.
- Concludes by expressing optimism about the decreased odds of a successful disruption compared to the past.

### Quotes

- "Can they do what they did last time, only this time be more committed?"
- "There's always a chance, you know, never say it can't happen here because it absolutely can."
- "The odds of it being successful are even lower."
- "It's also worth remembering that last time the reason they did it was to keep somebody in power."
- "Y'all have a good day."

### Oneliner

Beau addresses concerns about the U.S. House, the new speaker, and the 2024 election, expressing optimism but acknowledging the possibility of another attempted coup.

### Audience

Concerned citizens

### On-the-ground actions from transcript

- Stay informed and engaged with political developments (exemplified)
- Support and advocate for electoral reforms to strengthen democracy (exemplified)
  
### Whats missing in summary

Insights on specific reforms passed by Joe Manchin and Susan Collins in 2022. 

### Tags

#HouseOfRepresentatives #Speaker #2024Election #CoupAttempt #Democracy


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about
the U.S. House of Representatives,
the new speaker, the 2024 election and something
that has been concerning to a lot of y'all
because there's been a lot of messages about it.
And for once in the last couple of weeks,
I actually get to give some good news, which is nice.
So ever since the new speaker was named,
There are a lot of people who are concerned.
They are less than thrilled about trusting him with American democracy because of, well,
a concern that there will be another attempted coup.
They were worried about a replay of January 6th.
So that's the question.
Can they do what they did last time, only this time be more committed?
Not really.
Not really.
have to thank for that is kind of surprising. It's Joe Manchin and Susan Collins. They put a whole
lot of work at the end of 2022, I think, into getting some reform passed there. And they made
it happen. You know, credit where credit is due and all of that. So before, basically to
challenge a state result, you needed one person in the House of Representatives and one person
in the Senate. Now, I don't know, the House of Representatives, it's probably close to
hundred people you would need just to start. They put a lot of roadblocks in
the way. So they might attempt it and theoretically sure if the entire
Republican Party was behind the attempt. Yeah, I mean they could cause a headache
again. I think that that's unlikely because I feel like by the time it's
it's the moment to certify I feel like there will be very clear examples when
it comes to the criminal liability that goes with that kind of stuff. So it's
possible, sure. It is way, way less likely and most institutions within the US
government are now even more prepared for it. They're, you know, in the lead-up to
January 6th because unlike a lot of government entities, most political
observers kind of saw it coming. We talked a lot about how resilient a lot
of systems are in the United States and how it's it would actually be very
difficult to engage in a paper coup. They have been strengthened since then.
and last time it would have it was a surprise to them this time it won't be
they'll be ready for it so I feel like we're relatively safe there's always a
chance you know never say it can't happen here because it absolutely can but
But the odds of it occurring are much less.
The odds of it being successful are even, they're even lower.
Now no doubt they can still create a headache if they wanted to, but disrupting it is, disrupting
it completely doesn't seem likely.
And it's also worth remembering that last time the reason they did it was to keep somebody
in power.
They won't have that this time.
They won't have the executive branch being directed in a way that might help them.
So it seems less likely.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}