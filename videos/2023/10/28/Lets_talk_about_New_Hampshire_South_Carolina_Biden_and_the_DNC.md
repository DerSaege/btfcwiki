---
title: Let's talk about New Hampshire, South Carolina, Biden, and the DNC....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=b3_UdqJVpoM) |
| Published | 2023/10/28 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden is not on the ballot in New Hampshire for the primary, but this does not mean he is not running.
- Conflicting rule sets in New Hampshire and South Carolina are causing this situation.
- New Hampshire's primary must be the first in the nation, but the DNC wants the first primary to be more representative, so they chose South Carolina.
- South Carolina is chosen because it is more representative of the diversity in the Democratic Party.
- Biden is following DNC rules by not filing to have his name on the ballot in New Hampshire.
- Democrats in New Hampshire are being encouraged to write Biden's name in.
- Election officials are figuring out how to handle the influx of write-in ballots.

### Quotes

- "The answer is simple."
- "No great mystery here."
- "There are two rule sets and one of them he has to follow if he wants delegates."

### Oneliner

Biden faces conflicting rule sets in NH and SC, where DNC's push for diversity led to his absence from NH ballot, urging write-ins instead.

### Audience

Primary voters

### On-the-ground actions from transcript

- Write Biden's name in on the ballot (suggested)
- Election officials should prepare for handling a significant number of write-in ballots (implied)

### Whats missing in summary

The full transcript provides a detailed explanation of the conflicting rule sets impacting Biden's presence on the primary ballot and encourages understanding of the situation.

### Tags

#Biden #PrimaryElections #DNC #RuleSets #Voters


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Biden in New
Hampshire and South Carolina and conflicting rule sets
that are likely to set up a situation in which a bunch of
bad information can get out.
Like, I already see the memes being created.
So what we're going to do today is go over what's
actually happening, and that way hopefully we can head some of that off.
Okay, the dramatic news, Biden will not be on the ballot in New Hampshire for the primary.
That doesn't mean he's not running.
It doesn't mean that he's a clone or a robot or whatever is going to be said.
you have are two conflicting rule sets. In New Hampshire, I think it's a state
law there, that their primary has to be the first in the nation. They have to be
first. Well, the Democratic Party as a whole, the DNC, they want the first
primary that the Democratic Party has. They want it to be more representative of
what the Democratic Party actually looks like. So they have decided that the first
real primary, I guess, the first one that's going to count as far as delegates,
well that's South Carolina. If you don't know what I mean, the Democratic Party is
not like all white, the Democratic Party. It looks like the United States. It's a
pretty broad cross-section. New Hampshire is like 89% white, so it's not
representative of the Democratic Party. So that's where the DNC is coming for on
this, or coming from on this. So they want it to be South Carolina, which is a
little bit closer, especially considering that black voters have kind of been a
deciding factor for the Democratic Party. So they want them to have representation early on in the
process. It's really that simple. Because the DNC sent out these rules, Biden is obligated to follow
them. So he's not going to file to have his name on the ballot. If you're in New Hampshire, you can
still write him in, but he will not file to have his name on the ballot because according to the
DNC rules, it needs to be South Carolina. And from my understanding, I think Biden was actually one
of the advocates for moving it to South Carolina. The fact that all of this didn't get worked out
is... I mean, it's like that old joke. No, I don't belong to an organized political
party. I'm a Democrat type of thing. This should have been handled before now, but
doesn't look like it is, and it doesn't look like it'll be rectified by the time
New Hampshire's primary rolls around. But because Biden isn't gonna be on it, be
ready for just a litany of wild claims about why he's not on it?
The answer is simple.
There are two rule sets and one of them he has to follow if he wants delegates, which
is what he needs to win the primary.
So my understanding is that New Hampshire, Democrats in New Hampshire are actually encouraging
people to write Biden's name in.
And I think election officials are trying to figure out how to deal with that because
that creates an entirely new issue when it comes to counting a whole bunch of write-in
ballots.
But that's what's going on.
No great mystery here.
Anyway, it's just a thought.
So have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}