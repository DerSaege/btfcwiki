---
title: Let's talk about Trump, the docs, and an extra element....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=GoPvK0pEiXE) |
| Published | 2023/10/13 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Providing an overview of the Trump case, focusing on a specific element of interest.
- Trump is not denying that classified materials were taken from the White House and kept at Mar-a-Lago.
- The dispute lies in how and why this occurred, what Trump knew, and his intentions in retaining the documents.
- Intent is critical in investigations, but legally, for willful retention, it's not necessary to prove intent.
- The government claims to know Trump's intent and plans to present this to the jury.
- The prosecution will tell a story to the jury, explaining not just that Trump had the documents but why he kept them.
- There are various theories about why Trump retained the documents, ranging from innocent hoarding to more nefarious reasons.
- The government's confidence in proving intent suggests they believe Trump's actions were not as nefarious as some may think.
- Intent becomes significant with more serious charges, indicating the government's strong case against Trump.
- The special counsel's office seems very confident in their ability to secure a conviction, based on their approach to the trial.

### Quotes

- "Intent is the holy grail."
- "They're going to give them a narrative, something that they can follow and explain not just that he had them, but why he had them."
- "If you are one of those who believes his intent was far more nefarious, you might want to dial it back because at this point the government believes they can prove his intent."
- "The government believes they know Trump's intent. That's big."
- "What this tells us most of all is that Smith is incredibly confident in this case."

### Oneliner

Beau delves into the Trump case, focusing on the disputed intent behind retaining classified documents and the government's confidence in proving it.

### Audience

Legal analysts

### On-the-ground actions from transcript

- Follow updates on the Trump case and the upcoming trial (suggested)
- Stay informed about legal proceedings related to government officials' actions (suggested)

### Whats missing in summary

Insights on potential implications of the trial proceedings and the case's broader significance.

### Tags

#Trump #Trial #Intent #Government #Confidence


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Trump,
the documents case, and an element
that doesn't have to be there, but apparently is.
And I find it incredibly interesting.
We have not been making videos
about every single development in every single Trump case,
frankly, because I just don't wanna talk about it anymore.
But I've still been staying up to date on all of it,
reading the motions and just kind of keeping an eye on it.
And when there's something worth talking about,
something a little bit more important
than the procedural day-to-day stuff, I bring it up.
And in a motion, something caught my eye.
And it is incredibly interesting.
It says that the classified materials at issue in this case
were taken from the White House and retained at Mar-a-Lago
is not in dispute.
That's what the special counsel's office is saying.
And that's true.
Trump really isn't denying a lot of this.
He's just saying for some reason he's allowed to do it.
That's going to be interesting to see demonstrated in a courtroom.
It goes on.
What is in dispute is how that occurred, why it occurred, what Trump knew, and what Trump
intended in retaining them.
If you've heard me talk about investigations or intelligence work or anything like that,
intent is the holy grail.
That's what matters.
Incidentally, for willful retention, that's not really something that matters, as far
as legally speaking.
That the documents were there and the person wasn't supposed to have them and they didn't
give them back, that's enough.
You don't have to say why.
But what it says, what the motion says is what is in dispute is how that occurred, why
it occurred, what Trump knew, and what Trump intended in retaining them, all issues that
the government will prove at trial primarily with unclassified evidence.
The government believes they know Trump's intent.
That's big.
is big news. First, because they don't have to. It's not something that they
really have to demonstrate, but they have it pieced together. And they have
it in a way that they are so confident in. They are so confident that they
have figured out his intent that they plan on presenting it to the jury. So
So they're not going to give the jury a, here's the law, here's where the documents were,
here's the photograph, he wasn't supposed to have them.
They're going to give them a story.
They're going to give them a narrative, something that they can follow and explain not just
that he had them, but why he had them.
And there's a bunch of theories about why Trump kept these.
from he's a pack rat and likes to keep trophies to he's an ego-driven loon to
things far more nefarious. If you are one of those who believes his intent was far
more nefarious, you might want to dial it back because at this point the
government believes they can prove his intent. If that intent was something far
more nefarious, there would be a lot more people indicted. I mean that's a
pretty safe assumption. Now that doesn't mean that he didn't cause damage. It
doesn't mean that he didn't intentionally or inadvertently, I don't
know, disclose information that might end up in opposition hands. There's a whole
bunch of things that go along with this, but based on what we've seen the
government doesn't believe that was the intent. It's worth noting as you get to
more serious charges, intent is something that that kind of matters. So what this
tells us most of all is that Smith is incredibly confident in this
case, because they're going above and beyond. If they weren't certain, they
wouldn't be saying they're going to demonstrate at a trial, because they
don't have to. It seems that the former president might have talked about his
intent, might have told people why he wanted them, and those people have told
the vets. Or maybe he wrote it down in his diary so he wouldn't have to
remember. But either way, the special counsel's office, they want to move
forward with the trial. In fact, that's what the motion was really about. They
want to move forward with it because they are incredibly confident in their
ability to secure a conviction, and I believe they intend to do so.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}