---
title: Let's talk about Trump not knowing Powell....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=_MHSSTYe5cw) |
| Published | 2023/10/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's response to recent news involving Powell and Cheesebro is questioned by many.
- Trump distanced himself from Powell, claiming she was never his attorney.
- Cheesebro might have more troubling testimony for Trump in the Georgia case.
- Trump seems more concerned about Powell despite potential risks in Georgia case.
- Powell's potential cooperation in Georgia could lead to revelations in other cases.
- Allegations suggest Powell was present at a meeting discussing military seizure of voting machines.
- Trump underestimates the risks posed by the Georgia case.
- Social media comments by Trump do not seem directly related to the Georgia case.
- Trump's response may be based on unknown factors rather than court proceedings.
- The Georgia case might experience a lull due to guilty pleas entering, but more legal news is expected soon.

### Quotes

- "She might flip in other cases."
- "I think Trump is underestimating the risk that the Georgia case poses."
- "The more apprehensive he will be and the more active he'll be in trying to distance himself."
- "We may be in for a little bit of a lull unless recent developments prompt some of the others to go ahead and start taking deals."
- "There will be more Trump legal news coming soon."

### Oneliner

Trump's response to recent legal developments, particularly in the Georgia case, raises questions about his concerns and potential risks in various legal proceedings.

### Audience

Legal analysts, political commentators

### On-the-ground actions from transcript

- Stay informed about legal developments and implications (implied)
- Monitor social media for updates on legal cases (implied)

### Whats missing in summary

Insights on the potential impact of legal actions on Trump's future and ongoing investigations.

### Tags

#Trump #LegalNews #GeorgiaCase #LegalProceedings #PoliticalAnalysis


## Transcript
Well, howdy there Internet people, it's Beau again.
So today we are going to, we're going to talk a little bit
about Trump and Powell and cheese bro
and why Trump's response to the news kind of doesn't fit,
or at least quite a few of you don't think it fits
because a bunch of messages came in.
So if you missed it over the weekend, Trump did the standard Trump thing.
He was basically like, I don't even know that woman, she just got coffee.
Now, he said something to the effect of she was never my attorney, well, good,
I guess attorney-client privilege won't come into play.
And when both of them decided to cooperate, when you look at it, and
as y'all pointed out, it certainly seems like Cheesebro actually has more, let's just say,
more testimony that would be troubling to the former president in the Georgia case.
But if you look at Trump's social media, he certainly seems more concerned about Powell.
I mean, I get it, I definitely understand where this idea is coming from, but I think
there are two things at play here.
One, I think Trump is underestimating the risk that the Georgia case poses.
And two, I think it's important to remember that Powell is linked to a lot of things.
And if Powell flipped in the Georgia case, she might flip in other cases.
I think there's more of a concern not necessarily related to the Georgia case.
I think it's important to remember that there was an alleged meeting where, I mean, people
discussed having the military seize voting machines according to the allegations.
Powell was said to be in attendance.
There's a whole lot that I think is troubling the former president when it comes to that
particular development because she can link him to a lot of stuff based off of
what we we think we know at this point. Again, innocent till proven guilty and
all that stuff. But I don't think the apprehension and I don't think the
social media comments are really related to the Georgia case because at this
point I still think Trump is grossly underestimating the risk that case poses
So that's what I think is going on. I am sure that we will probably see a
pattern assuming more people decide to become cooperative. We'll probably see a
pattern develop when it comes to how Trump responds on social media. And if I
had to guess at this point in time, his response won't be based off of what they
actually say in court at the time of their plea or anything like that. It will
be based off of things that we don't necessarily know. We may have heard about,
we may have heard allegations about, rumors about, but we don't know. I feel
like the more the more a person is rumored to know about some of the
wilder ideas and maybe things we don't even know about yet. I think the more
apprehensive he will be and the more active he'll be in trying to distance
himself. That would be my guess at this point in time. But we will undoubtedly get
to see more. It is worth noting that we may be in for a little bit of a lull in
the Georgia case now because the trial that was supposed to happen isn't going
to happen because everybody, everybody entered a guilty plea.
Um, so we may be in for a little bit of a lull unless recent developments
prompt some of the others to go ahead and start taking deals.
Um, but don't worry, there will be more Trump legal news coming soon.
I'm sure anyway, it's just a thought you all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}