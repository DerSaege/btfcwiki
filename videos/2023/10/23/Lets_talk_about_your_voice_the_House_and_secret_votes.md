---
title: Let's talk about your voice, the House, and secret votes....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=oRY5XsgPKX0) |
| Published | 2023/10/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the dynamics of influence in the US House of Representatives.
- Mentions how representatives prioritize opinions based on financial contributions.
- Explains the impact of multiple voices versus individual engagement.
- Provides an example of a public versus secret vote scenario.
- Emphasizes the significance of public perception on representatives' decisions.
- Encourages individuals to recognize the power of collective action.
- Challenges the notion that contacting representatives is ineffective.
- Urges people to mobilize together to counterbalance wealthy influences.
- Raises awareness about the difference in representatives' votes in public versus secret settings.
- Stresses the importance of public engagement in influencing elected officials.

### Quotes

- "Your voice does matter."
- "You just need more of you."
- "It's not meaningless."
- "Your voice, it's not meaningless."
- "But it's not meaningless."

### Oneliner

Your voice matters, but collective action is needed to counter wealthy influences in politics.

### Audience

US constituents

### On-the-ground actions from transcript

- Mobilize with others to collectively reach out to representatives (exemplified)
- Encourage friends and community members to join in engaging with elected officials (exemplified)

### Whats missing in summary

The importance of public engagement in shaping political decisions.

### Tags

#USPolitics #Representatives #CollectiveAction #PublicEngagement #Influence


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about something
we can actually learn from the giant show going
on in the US House of Representatives.
There's a common refrain that representatives, senators,
elected officials in general, well, they don't listen to us.
And to some degree, that's true.
But it doesn't mean that they don't listen.
It's more like a weighted vote.
Those people who have a bunch of money, well,
their opinion means more.
It's really how it works.
It's not that your call, letter, social media engagement,
email, whatever. It's not that it doesn't matter at all. It just doesn't matter as
much. For it to matter as much, there needs to be a bunch of them. People can
say that that's not true, but I would like to point something out. When we're
We're talking about Jordan in his attempt to become speaker.
In the last floor vote that they had, 194 Republicans voted for him to become speaker.
But when they had the vote behind closed doors, a secret ballot, he had 86 people want him
to stay in the race, and 112 wanted him to drop out.
Do you really think that that many people thought he was a good speaker, but then just
like, ah, don't worry about it, now you should drop out?
You think that's what occurred?
Or do you think the fact that the vote was secret had something to do with it?
See that vote out there on the floor?
constituents. They see that. It's that voting record. They'll get angry tweets and there
might be enough of them to actually cause a problem for them. That's a large
part of why there was such a dramatic drop. Sure there were some people who
were just like yeah okay fine he can't he can't reach the number that we need
and they're like okay you need to drop out yeah I mean that's some of them but
it's not all of them it's not all of them they're Republicans they're
conservative by nature they don't like to change their opinion the reason the
votes are so dramatically different. It's because one was done in private. Out of
the view of the voters who could call, who could email, who could tweet, that's
a big part of it. What that shows you is that your voice, it's not meaningless. It
It doesn't mean as much as people who contribute tens of thousands of dollars.
But it's not meaningless.
You just need more of you.
It's something to file away because for as long as I can remember, the idea that contacting
your representative really wouldn't do anything.
That was there.
has been around for as long as I can remember and honestly I think
politicians like to encourage it because that means they get less contact with us
commoners. But it's important to note how different their votes are when they're
in public and when they're in secret and the only reason that matters is because
of the public.
Your voice does matter.
You just need to bring some friends with you to equal out the millionaire or billionaire.
Anyway, it's just a thought.
Y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}