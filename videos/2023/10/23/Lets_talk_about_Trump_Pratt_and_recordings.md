---
title: Let's talk about Trump, Pratt, and recordings....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=hvCqMWV7eEc) |
| Published | 2023/10/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about recordings of an Australian billionaire named Pratt discussing Trump.
- Pratt shared classified information from Trump.
- Questions about potential indictment for Trump and using the recordings.
- Indicates that there isn't a law against sharing information as president.
- Notes that anything shared by Trump while president is within his rights.
- Suggests the potential use of the recordings for credibility.
- Emphasizes the irresponsibility but lack of legal consequences.
- Stresses the importance of electing someone who can maintain confidentiality.

### Quotes

- "Well, howdy there, internet people, it's Beau again."
- "The safeguard against this is supposed to be the American people understanding that they should probably not elect somebody who doesn't know how to keep their mouth shut."

### Oneliner

Beau explains recordings of Pratt sharing classified info from Trump, indicating no legal consequences but stressing the importance of electing individuals who can maintain confidentiality.

### Audience

Political observers

### On-the-ground actions from transcript

- Elect individuals who value confidentiality (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the implications of recordings where classified information was shared, focusing on the lack of legal consequences for Trump and the importance of choosing leaders who prioritize confidentiality.

### Tags

#Trump #Pratt #ClassifiedInformation #Indictment #Confidentiality


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Trump and Pratt
and recordings and secrets
and the two most common questions that came in
as soon as that news broke.
If you missed the news, some recordings have surfaced.
Those recordings are recordings
of an Australian billionaire named Pratt.
And he's basically just talking about Trump
his experiences, his relationship with Trump, and he talks about things that
Trump had told him. And for context, yes, this is the same Australian billionaire
involved in the sub-allegations that Trump told a bunch of stuff to, according
to the allegations. Okay, so some of the information that Pratt is saying Trump
told him it would be classified. The number one question that came in, is
Trump going to be indicted for this too? The second most popular question that
came in was, how can Jack Smith use this? I'm assuming that the people that are
asking the second question know the answer to the first. So yeah, based on
what is said, it certainly appears that some of that information would have been
classified. Is Trump going to be indicted for sharing that? Oddly enough, no. Nah.
There's actually not a law against that when you're talking about the president.
I don't see how that could lead to charges. My understanding is that those
recordings were made while Trump was still president. Therefore, anything that
was discussed occurred while Trump was president. And Trump is the kind of the
ultimate consumer when it comes to intelligence products. The president is.
They kind of can show whatever they want, say whatever they want while they're
president. If you go back to the video, I actually think it was the last video
talking about the first allegations involving Pratt with the subs. I make a
point to say President Trump can share information. Golf course owner Trump
needs to, I think I said close the hole under his nose, Trump as president can
share information. The second he leaves office that doesn't apply. So anything
he shared with Pratt while he was president. That's kind of cool. I mean, it's wildly
irresponsible, probably should be illegal, but it's not. The stuff that is alleged to
have occurred afterward, that's a different story. How can Smith use that? Well, I mean,
if you have recordings of somebody sharing classified information with someone and then
that person says it happened at a later date, it does kind of give them a little bit of
credibility. That's how I would imagine we will see this material again, but I
don't foresee this leading to a brand new indictment. It's wildly irresponsible,
it's not something that should have occurred, but I don't think there's any
laws against it. I think the safeguard against this is supposed to be the
American people understanding that they should probably not elect somebody who
doesn't know how to keep their mouth shut. Anyway, it's just a thought. Y'all
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}