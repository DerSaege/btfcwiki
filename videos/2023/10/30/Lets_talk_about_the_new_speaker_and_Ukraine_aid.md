---
title: Let's talk about the new speaker and Ukraine aid....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=UgYUjt7qyLU) |
| Published | 2023/10/30 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The House of Republicans is divided on continuing aid to Ukraine.
- Concerns were raised about the new speaker's stance on Ukraine aid.
- The new speaker expressed the importance of not allowing Vladimir Putin to prevail in Ukraine.
- There is a call for accountability in the usage of aid money for Ukraine.
- Suggestions are made to split aid packages between Ukraine and Israel.
- The Republican Party is firmly supportive of aid to Israel but not as united on Ukraine.
- The speaker aims to separate the aid packages for separate votes to accommodate different stances within the party.
- Despite procedural changes, aid for both Ukraine and Israel seems secure.
- Cutting off funding to Ukraine is not favored by the speaker, which may come as a relief to many.
- The aid situations for Ukraine and Israel are starkly different in their significance and impact.

### Quotes

- "We can't allow Vladimir Putin to prevail in Ukraine because I don't believe it would stop there."
- "The Republican Party is all in on Israel. They are going to fund Israel no matter what."
- "Realistically, it seems like nothing is going to change other than the procedures in which it happens by."
- "If they want to stay in the fight, they need it."
- "Y'all have a good day."

### Oneliner

Beau delves into the Republican Party's divided stance on Ukraine aid, proposing a split of aid packages between Ukraine and Israel to accommodate differing views within the party.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Contact your representatives to express your views on aid packages for Ukraine and Israel (suggested).
- Stay informed about the decisions and proceedings related to aid funding for Ukraine and Israel (implied).

### Whats missing in summary

Detailed insights on the potential impacts of splitting aid packages between Ukraine and Israel.

### Tags

#Ukraine #Israel #AidFunding #RepublicanParty #Accountability


## Transcript
Well, howdy there, Internet people, it's Beau again.
So today we are going to talk a little bit about the new speaker and aid and where
the speaker and the Republican party as a whole sits on a couple of issues that
ever since the new speaker took the position, there have been a lot of
questions and concerns about.
It's important to remember that the House of Republicans in particular are very, let's
just say divided when it comes to continuing aid to Ukraine.
You have some that are willing to do so, you have some that are very kind of up in the
air with it, and then you have some that honestly seem like they're more interested in fulfilling
Moscow's foreign policy than the US's. And nobody really knew where the new speaker
fell on that spectrum. But now we have a statement here. We can't allow Vladimir Putin to prevail
in Ukraine because I don't believe it would stop there, and it would probably encourage and empower
China to perhaps make a move on Taiwan. We have these concerns. It goes on to say we're not going
to abandon them, but we have a responsibility, a stewardship responsibility, over the precious
treasure of the American people and we have to make sure that the White House is providing the
people with some accountability for the dollars okay so he does not seem to be
of the position that it's time to cut aid to Ukraine and that that seems
pretty clear what it sounds like is that they want inventories they want to know
where that money is going and how it's being used and they want to create a
a whole bunch of bureaucracy, probably that will end up being filled by their friends
or something, the jobs.
Just saying that's how it often works, no, I have nothing to suggest that the new speaker
actually wants to do that, it's just the Potomac two-step.
So the only real change that appears to be on the horizon is the speaker seems to want
to split the aid packages between Ukraine and Israel, you know, Biden just wants to
push through this giant package that includes aid for Ukraine, aid for Israel, a whole bunch
of other stuff.
I went through it over on the other channel.
It looks like the speaker wants to split these packages up and vote on them separately.
The reason for that is simple.
The Republican Party is all in on Israel.
They are going to fund Israel no matter what, no matter how much they need.
It's the Republican Party, it's what they do.
The Republican Party is not as united on Ukraine.
So my guess is he wants the bill split so those people who have taken a public stance
against supplying aid to Ukraine can vote against it and not have to explain why they
voted against aid for Israel.
That's really what this boils down to.
Realistically, it seems like nothing is going to change other than the procedures in which
it happens by.
There are more than enough votes on the Republican side of the aisle to get aid for Ukraine moving.
For those who are concerned about Israel, there are more than enough votes on the Democratic
side of the aisle to keep that moving.
It doesn't look like either one of these are in jeopardy in any way.
But the speaker not being of the position that we have to cut off funding to Ukraine,
that's probably a relief to a whole lot of people who are literally in harm's way depending
on it.
the aid situations when it comes to Ukraine and Israel, very, very, very different.
Israel, the aid really doesn't matter that much to them.
They will be able to do what they're doing even if they got no aid.
Ukraine, not so much.
If they want to stay in the fight, they need it.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}