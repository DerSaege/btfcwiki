---
title: Let's talk about the intersection of politics and policy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=6868qsRLbWY) |
| Published | 2023/10/30 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the analogy of foreign policy as a poker game where everyone cheats, representing the power dynamics between countries.
- Addresses the intersection of foreign policy and domestic politics, focusing on how politicians prioritize issues based on electability rather than public interest.
- Points out the inverted nature of domestic politics where politicians dictate what's significant rather than listening to constituents.
- Emphasizes that politicians prioritize foreign policy moves that can secure votes, resulting in a disconnect between public awareness and actual decisions made.
- Criticizes the expectation for politicians to have pure motivations in foreign policy, stating that their actions are self-serving and aimed at re-election.
- Urges viewers to understand the pragmatic, power-driven nature of foreign policy instead of seeking moral absolutes.
- Mentions specific examples like Iran to illustrate how historical events and propaganda shape current foreign policy decisions.
- Concludes by stressing the importance of grasping the realpolitik behind foreign policy to enact meaningful change.

### Quotes

- "Foreign policy, it's about power. How do they get it? It's a poker table. Countries are the players and they slide cards to each other. Everybody's cheating."
- "Politicians prioritize foreign policy moves that can secure votes, resulting in a disconnect between public awareness and actual decisions made."
- "For some reason, when it goes to foreign policy, we expect their motivations to be pure. They're not. They're self-serving."
- "It's about power coupons. When it crosses over into domestic politics, it's still about power."
- "It's never about morals."

### Oneliner

Beau breaks down the power dynamics of foreign policy, revealing how politicians prioritize issues for votes rather than public interest, urging viewers to understand the pragmatic nature to drive real change.

### Audience

Politically engaged citizens

### On-the-ground actions from transcript

- Understand the pragmatic, power-driven nature of foreign policy (implied)
- Advocate for transparency and accountability in foreign policy decisions (implied)
- Educate others on the intricacies of how foreign policy intersects with domestic politics (implied)

### Whats missing in summary

The full transcript provides a comprehensive breakdown of the intersection between foreign policy and domestic politics, urging viewers to critically analyze the motivations behind political decisions and advocating for a deeper understanding of realpolitik in shaping global dynamics.

### Tags

#ForeignPolicy #Politics #PowerDynamics #Realpolitik #PublicInterest


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about the intersection
of politics and policy, because they're not the same thing.
We're going to talk about foreign policy
and its interplay with domestic politics.
We're going to do this because recently, over
on the other channel, I did that video with the whiteboard,
And we made up the region.
And we talked about some of the dynamics
that occur in foreign policy.
Afterward, I got this message.
And in it was an addition to the analogy
that I use as far as foreign policy being a great big poker
game and everybody's cheating.
They used that and expanded on it in a very insightful way,
so much so that I'm about to still like a large chunk of it. But the question was
about that intersection between foreign policy and the political situation at
home. So that's what we're going to talk about. Okay, so first let's run through
the analogy. Foreign policy, it's about power. How do they get it? It's a poker
table. Countries are the players and they slide cards to each other. Everybody's
cheating. There are different tables. The superpowers, they're at one table.
Regional powers at another. Other powers at a different table. So what are the
winnings? What are the winnings from that poker table? Your standard of living. Your
way of life. The reason the American population has such a high standard of
living compared to the rest of the world. When we are talking, and I know somebody
is going to go into domestic politics here, but that's not what I'm talking
about. I'm talking about access to goods, because that's a big part of it. Generally,
the American population has a higher standard of living. Maybe not as many days off or access
to healthcare, but overall, those are your winnings. That's what it's about. Most Americans
don't like to admit that. So when it comes to the domestic side of things, how
is it supposed to work? Domestic politics. In the United States, us, the
commoners, we are supposed to tell our betters, the representatives, what matters
to us and that's how they're supposed to vote and they're supposed to represent
your interests. I think we all know it doesn't work this way but that's how
it's supposed to work. How does it work? There are party platforms and the
politicians tell us what's important. This is why we don't have movement on
on climate change or healthcare or days off or stuff like that.
It's been inverted.
Why do they have the platforms they do to increase their electability, right?
When politicians talk to you about foreign policy, it's the same thing.
changes. When they choose to tell you about a particular foreign policy move,
it's because it's one that a large chunk of Americans would agree with. Just like
a real player, they don't necessarily tell their family, which is us, about going
into that game in the back room of that seedy place.
They only talk about the ones that are a little bit more acceptable.
There are a lot of foreign policy moves that you never hear about.
And it's not just because it's, you know, a small interest.
It's because they don't need to tell you because there's no votes to be gotten.
the family of the players. We don't need to know, we're just commoners and there's
no votes for them to get so it never becomes a topic. You want an example? How
many troops does the United States have in Africa right now? I just wager a
guess. Most people don't know. Which countries, plural, are they in? Probably
don't know. There's no votes to be got. So it doesn't become a campaign issue. It
doesn't become a political issue because there's no way for them to capitalize on
it. To us, now they definitely talk about foreign policy moves in Africa in a
boardroom somewhere because that's going to get them donations. But to us, it
doesn't shape somebody's vote. So they don't care and they don't tell us to
care about. Foreign policy moves that become political issues are ones that
can get politicians votes. It's really that simple. So you could expect a
politician to grandstand on a foreign policy move that is in opposition to, I
I don't know, a country that's had a whole bunch of propaganda pushed out about it,
against it, for decades.
They might grandstand on something like that.
Or if the people are really afraid of a new power emerging, they might grandstand on that
one because that fear translates to votes.
A much more simple way of looking at it, and one that happens way more than people want
to admit, is just math.
If there are two sides to a conflict, and the United States doesn't have something
that the public readily sees as its interests, but a lot of people are talking about it,
side did the politicians come down on? Whichever one has the most represented
in the US population. If we go back to that map and it's the blues and the reds
and the blues have 10 million people living in the United States that share
some kind of heritage to that country and a quarter million that share
heritage with the red country? Oh, they're siding with blue. It's a bigger
voting bloc to appeal to. It's that simple. One of the the weirdest things to
me is that Americans, generally speaking, when we are talking about politicians, we
We don't think moral, upstanding, ideological people, they're politicians.
We generally don't hold a high view of them.
But for some reason, when it goes to foreign policy, we expect their motivations to be
pure.
They're not.
They're self-serving.
It's about getting re-elected.
So foreign policy issues that become political talking points, they're ones that a politician
believes that they're going to get a bunch of votes on.
And because we don't actually tell them what we care about and them represent our interests,
they tell us what matters.
And generally what ends up happening is it fosters a pretty big divide, creates a lot
of other groups.
It's not actually a good way.
Again, this is one of those videos where I'm talking about what is, not the way it should
be, but I strongly believe that if you want to change something, you have to know how
it works.
And when it comes to foreign policy, you've got to stop looking for right and wrong.
The way it is exercised today doesn't have anything to do with that.
It's about power, it's about power coupons.
When it crosses over into domestic politics, it's still about power.
It's about that politician's power and preserving it.
That pretty much always sums up the stance they're going to take.
And if you think about the recent hot spots, that's where it comes from.
That's a lot of why the decisions are made the way they are.
And it also, let's take Iran as an example.
Iran is seen as a very adversarial nation.
I doubt that most Americans know, like, what actually started all of that?
It was the overthrow of, it was an undemocratic overthrow of their government initiated by
the CIA.
And now, just so you know, weird fact, on the CIA's podcast recently, they admitted
that for the first time that I'm aware of.
They actually use those terms.
But that's where it all started.
Because of years and years and years of animosity and propaganda and othering that nation.
When it was actually in the cards to bring them out, to bring them out and end that,
The propaganda, domestically, was still so strong against that country that you had a
candidate sabotage good foreign policy for votes, and it's all about power at that level.
It is rare for a politician to truly sabotage good foreign policy.
Generally speaking, the politician isn't so impressionable when it comes to the talking
points of their favorite media.
They haven't bought into the propaganda, but recently that wasn't the case.
So that intersection, when you are thinking about it, you're the family of the poker
player.
They don't tell you everything.
They tell you what you want to hear.
They tell you what they think you'll approve of, and they tell you what they believe will
get you to say, yeah, why don't you go play another game.
That's really what it's about.
It's never about morals.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}