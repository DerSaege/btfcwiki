---
title: Let's talk about getting around Tuberville....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ZWvR-ibqWnE) |
| Published | 2023/10/30 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Senator Tuberville's block on promotions is being discussed for resolution through a standing order, bypassing the Senate's typical process.
- Tuberville views the administration's push for this resolution as damaging to the Senate, but negotiations are not likely as the process is meant to be apolitical.
- Tuberville's concerns about recruitment and readiness being damaged are refuted, as the rules being changed are by senators, not the administration.
- The current situation has put officers' careers on hold for headlines and is causing damage to recruitment and retention.
- The possibility of the U.S. moving to a wartime footing makes resolving this issue quickly imperative.

### Quotes

- "The administration is not going to negotiate over this. That seems incredibly unlikely."
- "The Senate changing its own roles, that's just what the Senate can do."
- "It's damaging recruitment. It's also damaging retention because these officers, they've had their careers put on hold for a senator to get some headlines."
- "The United States could be on a wartime footing in the next five minutes."
- "It's cinema and Republicans. It's not the administration."

### Oneliner

Senator Tuberville's block on promotions sparks political tension as the Senate navigates a resolution process amid concerns over readiness and recruitment.

### Audience

Senate staff, political activists

### On-the-ground actions from transcript

- Contact Republican senators to urge resolution of the promotion block (suggested)
- Stay informed about the ongoing Senate proceedings and resolutions (exemplified)

### Whats missing in summary

Insights on the potential implications of the promotion block on national security.

### Tags

#Senate #Promotions #Resolution #Recruitment #NationalSecurity


## Transcript
Well, howdy there internet people, it's Bo again.
So today we are going to talk about the attempts
to get around Senator Tuberville's block on the promotions
because there's conversations that are occurring about that,
about how to end his hold.
And the short version is getting a standing order
resolution that would allow the Senate
to use a different process, basically.
It's a procedural thing.
To move promotions through in bulk through the end of 2024.
And those are conversations that are being had.
Obviously, Senator Tuberville is not real happy about this.
He said, it's typical of this place.
This administration would rather burn the Senate down.
And that's what would happen.
And he goes on, and basically the idea is that the administration would rather cause
damage to the Senate than negotiate.
I mean that's funny in its own way given what this is about, but here's the thing.
No, the administration is not going to negotiate over this.
That seems incredibly unlikely.
your job is to protect things like recruitment and readiness, you don't
negotiate with people who are using damaging that as a political tool. This
process is supposed to be apolitical. It's not supposed to be partisan. If you
allow negotiations because somebody politicizes it, it sets that as a
precedent and that's not something that they would want to do. I don't know who
told him that people would want to negotiate if he went after an apolitical
organization in an apolitical process. It just sets the tone to allow it in the
future. So I mean that what he's saying here is kind of true. I mean the
administration would certainly not do this, but this administration would
rather burn the Senate down. That's weird. That's weird. We'll have to come back to that part.
And when it comes to the idea that it is damaging recruitment and readiness and all that stuff,
he says, if I thought this was happening, and by this he means damage to readiness,
I wouldn't be doing this. And I've told you that all along, and the people that I trust tell me
it's not. The problem is that we can't actually trust this statement. It doesn't
make any sense. Just like this administration would rather burn the
Senate down because how this is happening is the Senate changing its
rules. I'm pretty sure that Biden can't change the Senate's rules. I'm pretty sure that's not how
it works. I'm pretty sure it's not this administration that's doing that. I'm pretty
sure it's other senators. I'm also pretty sure that they're not even Democrats. They're Republican
senators and one independent, from what I understand. That's where the
conversations are happening. Now don't get me wrong, the Democratic Party would
definitely back it, but that's not who's trying to change the rules. It's a
handful of Republicans and Sinema. That's where it's coming from. So it's
not this administration. It's not the Biden administration, right? It's other
senators. I mean, the Senate changing its own roles, that's just what the Senate
can do. I don't know why that's a thing that's damaging to the Senate. It sounds
like the Senate is functioning and trying to figure out how to, you know,
fulfill its duties when there's one senator that doesn't seem to want to.
As far as it damaging readiness, really, this move, what it did was it gave up the Senate's
power and it gave it all to Biden.
People are in these roles and they're in them in an acting capacity.
It's not horribly damaging readiness at the moment.
It is a little bit, but it is damaging recruitment.
It's also damaging retention because these officers, they've had their careers put on
hold for a senator to get some headlines.
That's not an organization that you'd probably want to stay part of.
But see, here's the thing, and this probably has something to do with why there's suddenly
Republican senators trying to figure out a way to get around Tuberville.
The United States could be on a wartime footing in the next five minutes.
And the biggest issue when it comes to its organization and being able to field things
the way it should, it's not somebody overseas.
somebody in the Senate.
The other senators probably understand that.
I wouldn't get too mad at them because they're probably trying to save Tuberville from himself.
It is a very tense world right now and it is not out of the question that the United
States move through a wartime footing.
That's the reality.
If that happens, yes, this hold, it goes from something that is annoying and something that
is disruptive to retention and recruitment to a real issue real fast.
And as much time as senators like to take off during this period, maybe it's something
that should be worked out. It's cinema and Republicans. It's not the administration.
Biden does not have the power to change the Senate's rules. Senators do. As far as it
forward. I'm guessing that it probably will. There will be some opposition to
it, no doubt, but I feel like this is something that will move forward even
among the Republican Party in the Senate. There's probably a few that are still
left that actually like read their briefings and understand what's going on
and understand that it would be a really bad situation if this hold was in place
and the United States had to respond to something major. And I think that it only
needs like nine Republicans to cross and I would imagine that that's
something something that's in the cards. Anyway, it's just a thought. Y'all have a
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}