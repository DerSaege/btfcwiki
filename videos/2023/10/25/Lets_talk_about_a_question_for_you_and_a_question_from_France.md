---
title: Let's talk about a question for you and a question from France....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=l9nFsH7q4KE) |
| Published | 2023/10/25 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Raises the question of which country is the most militaristic and ready for war.
- Reads a message from a French person criticizing Biden's contradictory statements on peace and war.
- Points out the French government's hawkish stance compared to the United States.
- Explains that both Biden and the French President are trying to avoid a larger war by restarting peace dialogues.
- Mentions the suggestion to widen the coalition fighting in Iraq and Syria to include groups from Palestine.
- Talks about Israel's role and the potential influence in the conflict if the coalition expands.
- Speculates on the goal of trying to prevent the war from spreading.
- Comments on the French President's hawkish approach and suggests there might be better ways to achieve influence.
- Emphasizes the importance of preventing the conflict from escalating and spreading to other countries.
- Mentions the increasing calls for restraint in foreign policy to avoid further spreading of conflicts.

### Quotes

- "The French have adopted a more militaristic standpoint than the United States."
- "The goal is to try to stop this war from spreading."
- "The fact that the US didn't go in guns blazing is amazing."
- "Expect to see a whole bunch of world leaders talk out of their mouth and somewhere else at the same time."
- "Anyway, it's just a thought."

### Oneliner

Beau questions militaristic countries, criticizes contradictory peace talks, and warns against conflict spread, urging restraint in foreign policy.

### Audience

Global citizens

### On-the-ground actions from transcript

- Contact local representatives to advocate for peaceful and diplomatic solutions to conflicts (implied).
- Support organizations working towards conflict resolution and peacebuilding efforts in affected regions (implied).
- Stay informed about international developments and advocate for policies that prioritize peace and diplomacy (implied).

### Whats missing in summary

Deeper analysis on the implications of expanding coalitions and potential consequences of militaristic approaches in foreign policy.

### Tags

#ForeignPolicy #Militarism #PeaceDialogues #ConflictPrevention #GlobalRelations


## Transcript
Well, howdy there, internet people, Lidsbo again.
So today, we are going to talk about a question for you.
A question for you to answer to yourself.
And then a question that came in from France and foreign
policy, and how sometimes things just don't make any sense
at first glance.
And kind of go over what these developments really
When I ask this first question, remember your answer for the rest of the video.
What country on this planet is the absolute most militaristic?
The one ready to go to war at the drop of a hat?
The one that really never shows restraint.
The one that has a maze of military installations all over the world because war, that is just
their jam.
That's what they do.
Keep your answer in mind as I read this question.
Okay so it says, I'm sorry, I'm French.
We say Biden is confusing Americans.
Our president is also talking from his mouth and, well, not his mouth.
It says something else.
He says a peace dialogue must start again and then says to expand our war against the
group that the coalition is fighting in Iraq and Syria.
I know President Trump said that they were defeated, but here in the real world, there's
still an active campaign against them and in fact just recently I want to say
in like the last 24 hours they engaged in a hit in eastern Congo with like 26
people lost. Of course that's not making the news because as we talked about
before when an area is deprioritized for foreign policy well we don't really
respond very much. It's kind of thoughts and prayers. Okay, so he uses a term
there. It's not the group's name, but it's definitely not a term I can say on
YouTube. So it says, he says a peace dialogue must start again and then says
to expand our war against the group in Iraq and Syria to include the attackers
from Palestine. This is not our war. What is he doing? Okay, so, for Americans, please
just take note. The French have adopted a more militaristic standpoint than the United
States on this. The French government has a more hawkish position than the U.S. It's
to note that. At the same time, to answer the question, he's doing the exact same thing Biden
is. He's doing the same thing as Biden is. He is trying to avoid a much larger war, and I know that
doesn't make any sense. So what he's saying is that the peace process, it has to restart, for real.
and there has to be real conversations and it has to move forward because
militarily there's not really a solution here. Something else that he said that
isn't in the message but it's important is roughly Israel should prosecute its
war without mercy, but that doesn't mean without rules. And then he suggests
widening the coalition. Okay, so Israel is not part of that coalition. Israel is
not part of the coalition that is active against the group in Iraq and Syria. If
Both that coalition expanded the scope of their mission to include groups from Palestine,
who suddenly gets a whole lot more influence over how that war is prosecuted.
The guys from McDill, U.S. Army Special Operations, or not U.S. Army, just U.S. Special Operations,
and their very first piece of advice is going to be, do not do a major ground offensive.
That's going to be the first piece of advice.
It's still an Israeli operation.
it would still be an Israeli thing, the US wouldn't be calling the shots in any way.
But the advice, the influence would be there and it would be much greater than it is right
now.
That's what he's angling for.
I personally think there's probably better ways to get that influence, but that is undoubtedly
the goal.
The goal is to try to stop this war from spreading.
People are looking at it right now like this is horrible, and it is.
That's not in dispute.
It can get worse, a lot worse.
It can spread to other countries, in which case, if Israel is having to engage other
countries. They are going to be more unrestrained. So the goal is to try to
stop the conflict from spreading. Do I think the route the French president took
is the best route? No, no I don't. I understand it but I think there's better
ways to get the influence. But that's what's happening. And again, for
Americans, the French have adopted a more hawkish position than the United States.
I know people want to talk about the moral implications and all of the horror
and I get it. But from a foreign policy standpoint, please remember Israel is an
ally. The fact that the US didn't go in guns blazing is amazing. The calls for
restraint, they're showing up more and more frequently and it's to stop the
conflict from spreading. My guess is that very soon we will probably start hearing
about countries that you might not expect, maybe China, to also start playing
a role. Now they may play it from a different side but the goal would be to
stop the conflict from spreading and that's what's going on. Expect to see a
a whole bunch of world leaders talk out of their mouth and somewhere else at the same
time.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}