---
title: Let's talk about Emmer and late breaking news....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=c1kFlWUdoa0) |
| Published | 2023/10/25 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the U.S. House of Representatives and the new GOP nominee for Speaker.
- Reads a message from a viewer about being late with news on Emmer.
- Mentions the Booster Thomas joke and upcoming merchandise related to it.
- Refers to October 6th predictions about the Speaker nomination contenders.
- Updates about Emmer withdrawing his candidacy due to lack of support.
- Criticizes the Republican Party's inability to decide on a Speaker.
- Urges moderate Republicans to seek bipartisan agreements.
- Questions the Republican Party's capability to lead if they can't decide on a Speaker.
- Emphasizes the urgency for a budget and international affairs.
- Concludes with a call for action and reflection.

### Quotes

- "The Republican Party is running out of time to get their act together."
- "It's time to start talking to the Democratic Party and working out some kind of bipartisan agreement."
- "If they cannot fulfill the most basic task of being in the US House of Representatives, I don't think it makes sense to trust them with any other branch of the government."

### Oneliner

Beau criticizes the Republican Party's inability to decide on a Speaker, urging for bipartisan collaboration and questioning their leadership capabilities amid urgent issues like the budget and international affairs.

### Audience

Political enthusiasts, bipartisan advocates

### On-the-ground actions from transcript

- Reach out to moderate Republicans and encourage bipartisan agreements (suggested)
- Stay informed and engaged in political developments (implied)

### Whats missing in summary

The full transcript provides detailed insights into the GOP's struggle in selecting a Speaker, urging for bipartisan collaboration amid urgent political issues.

### Tags

#USPolitics #GOP #Bipartisanship #Leadership #Urgency


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are once again going to talk
about the U.S. House of Representatives,
and once again going to talk about the new GOP nominee
for Speaker of the House of Representatives.
But before we get into all of the news about Emmer,
I have a message that I want to read that's related,
and just kind of address it real quick.
It says,
Beau, your Boaster Domus rep is ruined.
I didn't hear Emmer's name from you until a couple of days
ago when I heard it on CNN at the same time.
Worst fortune teller ever, lol.
I'm just joking.
It's a mess up there.
OK, so a couple things about this real quick.
First is the Booster Thomas thing is a joke.
Second, for all those people who are asking for merch for
that, it's coming.
And then third is October 6th.
I said on October 6th that the three leading contenders
for that nomination would be Scalise, Jordan, and Emmer.
But once Scalise really didn't have the support,
I didn't see the point in really talking about Emmer much more.
Because as I said in the video, anybody happy with Emmer
would be happy with Scalise.
And hang on, apparently we have some late breaking news.
put. And Emmer has withdrawn his candidacy. He does not have the support much the same
way. Scalise didn't. So we are once again back to the drawing board. The Republican
Party is going on TV every night telling people that they know how to run the country. That
they are the ones that the country should trust to lead the government. That they are the ones
that can actually lead the world and they know what's best. Let's be super clear about something.
The Republican Party cannot decide a winner when it comes to a popularity contest at this point.
They are running out of time to get their act together. It does not appear at this point in time
that anybody on the Republican side of the aisle has enough support from within
their own conference to become speaker. It is time for those people who like to
portray themselves as normal Republicans, as moderates, as you know the non-mega
faction. It's time to start talking to the Democratic Party and working out some
kind of bipartisan agreement because the Republican Party is incapable of
deciding who's the most popular. They're incapable of making this decision. If
they cannot fulfill the most basic task of being in the US House of
Representatives, I don't think it makes sense to trust them with any other
branch of the government.
Time is of the essence, not just for the international situations that everybody keeps talking about,
but please remember that we still don't actually have a budget.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}