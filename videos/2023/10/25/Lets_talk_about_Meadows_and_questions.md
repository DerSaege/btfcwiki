---
title: Let's talk about Meadows and questions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=-UJZFKN_Z9I) |
| Published | 2023/10/25 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the reporting that broke about Meadows coming to an agreement where he flipped and got total immunity.
- Expresses cautiousness and skepticism about the reporting, mentioning his desire to believe it but having questions about its accuracy and nuance.
- Explains the different kinds of immunity, mentioning a scenario where limited or temporary immunity is granted for specific testimony.
- Stresses that he cannot confirm the reporting of Meadows flipping or cooperating.
- Suggests that if Meadows did flip and cooperate with total immunity, it could be devastating to Trump's case.
- Mentions that the lack of confirmation and availability of other options make him hesitant to celebrate the news too soon.
- Points out that there are aspects of the story that don't fit, raising questions about the accuracy of Meadows getting total immunity and actively cooperating.
- Encourages fact-checking oneself, especially when the information confirms pre-existing beliefs.

### Quotes

- "There are other options that might even be more likely."
- "But there are some pieces of this that don't really fit."
- "If you ever find yourself wanting to believe a story, fact-check yourself."

### Oneliner

Beau expresses skepticism about the reporting on Meadows flipping and getting total immunity, stressing the importance of fact-checking and caution in celebrating unconfirmed news.

### Audience

News consumers

### On-the-ground actions from transcript

- Fact-check any news before spreading it (suggested)
- Exercise caution in celebrating unconfirmed news (implied)

### Whats missing in summary

Importance of verifying information before sharing widely.

### Tags

#FactChecking #ImmunityDeals #Skepticism #NewsConsumers #Verification


## Transcript
Well, howdy there, Internet people, Lidsbo again.
So today, we are going to talk about Meadows
and the reporting that came out
and just kind of go through everything.
Tonight, well, last night to y'all, y'all are in the future.
Tonight, reporting broke that Meadows
come to an agreement that he had flipped, got total immunity. That was the
reporting and I wanted to believe that really bad. Anytime I want to believe
something I am incredibly cautious. I wanted to believe this so much that I
I actually made the video based on that reporting. But while I was making it, well
it led me to put a post, a comment, underneath the evening video last
night for y'all that says Meadows will probably be tomorrow morning's video
because there's a lot to process through. If you missed it, reporting says Meadows
flipped. I'm going to cut to the chase. I have questions about the reporting and I
may wake up tomorrow and be... this may be a video that shows me being overly
cautious but I have questions about the reporting. Not that it's wrong but that
maybe it lacks a little bit of nuance.
There are different kinds of immunity.
There is an immunity deal, which is kind of how this was being framed.
And then there's other kinds that you can receive.
Now generally speaking, the other kinds, they lead to an immunity deal most times, but they
don't have to.
Hypothetically speaking, if you are called before a grand jury and you don't want to
testify, so you start taking the fifth over and over and over again, they can grant you
limited immunity, temporary immunity basically, and it applies to what's discussed then.
It doesn't necessarily mean you have total immunity.
I cannot confirm the reporting.
And Meadows actually flipping, Meadows actually cooperating, especially with a total immunity
deal where he has nothing to worry about.
That would be absolutely devastating to Trump's case.
Case is, actually.
I don't know that that happened, though.
I don't know that that happened.
The people that I normally rely on for information about this kind of stuff, it's late, they're
not answering.
version is I can't confirm it and based on what is publicly available at time
of filming, there are other options than Meadow's flipped, made an agreement,
and has total immunity. There are other options that might even be more
likely. So, short version, I'm not saying the reporting is wrong. I'm not saying he
didn't flip. I've been saying for a year that he would be a high priority for
them to, for them to encourage him to be a little bit more cooperative. What I'm
saying is that I can't confirm what is being reported and if that reporting is
accurate it is a massive turning point but I can't confirm it and I don't like
being wrong so I would wait before you celebrate this too much I would wait and
and see if there's a little bit more confirmation that comes out.
Because at time of filming, yeah, he appears to have gotten immunity.
What kind and how much?
Very much up in the air.
Again, maybe I'm being overly cautious.
But there are some pieces of this that don't really fit as far as him getting a total
immunity deal and actively cooperating. There are little chunks of the story
that it doesn't matter how you turn the puzzle piece it doesn't fit with
everything else that's going on. So I have questions so I'm just I'm just not
ready to to come out and say what a great thing it is that he has flipped
because if he didn't, man, that would be kind of embarrassing. So if you ever find
yourself wanting to believe a story, fact-check yourself, especially if it
confirms something that you have believed and talked about for a year. Go
through and make sure that you're right. There is nothing wrong with being the
last... if you're right. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}