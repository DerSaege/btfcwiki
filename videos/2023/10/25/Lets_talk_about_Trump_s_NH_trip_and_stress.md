---
title: Let's talk about Trump's NH trip and stress....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=3D7s4aMeouU) |
| Published | 2023/10/25 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analysis of Trump's behavior in New Hampshire reveals stress and poor handling.
- Trump faced chants of "lock him up" and made questionable statements like promising to build an iron dome.
- He praised Viktor Orban of Turkey, a country he confused with Hungary.
- Trump bizarrely told people not to vote and compared himself to Mandela.
- He claimed to have never been indicted, possibly showing signs of stress and campaign fatigue.
- Beau notes Trump's acceptance of the possibility of going to jail, a shift from denial.
- Trump's statements now lack the inflammatory nature of his past rhetoric.
- Beau observes similarities in Trump's behaviors to what Republicans mock Biden for.
- The change in Trump's speaking patterns indicates a person under significant stress.
- Beau suggests that even supporters may be noticing Trump's struggles.

### Quotes

- "He doesn't mind being Mandela."
- "He might really be at the point where he's starting to understand the situation that he's in."
- "We are witnessing somebody under a lot of stress who is starting to come to terms with the situation they're facing."

### Oneliner

Beau analyzes Trump's behavior, revealing signs of stress and potential acceptance of his situation, shifting from denial to understanding.

### Audience

Political observers

### On-the-ground actions from transcript

- Monitor for signs of stress and mental health in political figures (suggested)
- Stay informed about political figures' behaviors and statements (suggested)

### Whats missing in summary

Insight into the potential impact of Trump's behavior on his supporters. 

### Tags

#Trump #PoliticalAnalysis #Behavior #Stress #Acceptance


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump's very unique trip to New
Hampshire and the series of things that he said along the way and what it can
tell us, what it might actually be able to demonstrate because this isn't just
a series of misspeaking. It's a series of events showing a man under a
whole lot of stress and who may not be handling it well. So if you don't know
what happened, showing up in New Hampshire, you know, at one point he was greeted by
by people chanting, lock him up. He certainly didn't like that. Another point
in time, he seemed to promise to build an iron dome for the United States, which,
I mean, set aside that, like, whether or not it's needed, I would remind everybody
that the United States is like way bigger than Israel it would that's a
whole lot of air defense just saying he said that Viktor Orban was just a great
leader of Turkey which is not the country he's anyway one of the more
interesting ones was when he told people not to vote he said you don't have to
vote. Don't worry about voting. The voting, we got plenty of votes. He seemed to be
saying that the people there shouldn't vote. They should go watch people vote
for Democrats, I think, is what he was trying to get at. I'm not sure. He
compared himself to Mandela, basically said, you know, that he didn't mind being Mandela
because he's doing it for a reason. That one's incredibly interesting to me. We'll come back
to that. And then he very bizarrely claimed that he had never been indicted, which I don't...
It kind of shows a person under a lot of stress that may not be up to campaigning.
There may be a lot going on, but one of the interesting things for me is the Mandela bit.
He doesn't mind being Mandela.
It's not the first time that he said that he wouldn't mind going to jail.
About four months ago, I put out a video talking about Trump supporters and how if you listen
to their statements and you listen to what they're talking about and how they're framing
they're running through denial, bargaining, anger. They're running
through the stages of grief. That video was very much focused on Trump's
supporters. The sudden fascination with putting it out there that he's okay with
going to jail. That might be acceptance from him. He might really be at the point
where he's starting to understand the situation that he's in. You know I've been
saying for a while that I don't really think he gets it. Like I think he's kind
stuck in the denial phase, the new trend of him expressing his willingness to go to jail.
I think he's starting to understand how strong some of these cases are.
And I think that may have a lot to do with some of his behavior, some of his statements
that don't make any sense, some of him misspeaking, a lot more than he used to.
You know, before when he would say something and it would get coverage, it was because
it was inflammatory, it was offensive, it was something that most Americans, at least
at the time in 2016, it's not something they normally would have stood for until they had
enough people give them permission to be their worst.
Now it's, I mean, honestly, he's kind of, he's showing a lot of the behaviors that
Republicans say Biden chose, you know, in their memes where they talk about him being
sleepy and eating ice cream and all of that stuff.
That's what his speaking patterns like now.
I think we are witnessing somebody under a lot of stress who is starting to come to terms
with the situation that they're facing.
I'm not sure where it goes from here, but I know that if I'm noticing it, those people
who viewed him as some mythical figure, they've got to be noticing it too.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}