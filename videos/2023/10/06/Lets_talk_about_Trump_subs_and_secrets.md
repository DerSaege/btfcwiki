---
title: Let's talk about Trump, subs, and secrets....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=K_WFBqf_-Kg) |
| Published | 2023/10/06 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recaps previous channel topics before diving into Trump document developments.
- Willful retention of classified information is bad, especially if it betrays means and methods.
- Transmitting classified information, even to a foreign national, is worse than retention.
- Recent reporting suggests Trump shared classified info on nuclear capabilities of US subs with an Australian businessman.
- The Australian businessman reportedly shared this info with about 45 other people, including journalists and officials.
- Trump playing the game of who's in the know with classified information is a dangerous ego trip.
- The information shared could potentially cost lives and is now likely in opposition hands.
- Trump's actions are considered worse than initial allegations and could have severe consequences.
- Beau no longer believes the US government will make accommodations for Trump if these allegations are proven true.
- Uncertainties remain about the extent of the information's spread and whether Trump provided accurate information.

### Quotes

- "Willful retention of classified information is bad."
- "Trump playing the game of who's in the know with classified information is a dangerous ego trip."
- "U.S. nuclear sub-capabilities should not be discussed in a club for clout."

### Oneliner

Beau recaps prior channel topics, then reveals how recent reporting suggests Trump shared classified US sub capabilities with an Australian businessman, leading to potential severe consequences if proven true.

### Audience

Concerned citizens

### On-the-ground actions from transcript

- Contact relevant authorities or organizations to report any knowledge or suspicions regarding the sharing of classified information (implied)

### Whats missing in summary

Full context and depth of analysis on the severity and potential ramifications of Trump's actions.

### Tags

#Trump #ClassifiedInformation #NationalSecurity #USGovernment #Accountability


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Trump
and the most recent developments
concerning the documents.
But before we get into that I wanna recap
some things that we have talked about on the channel
since this whole thing started
and just kind of go over them
because some of it is going to come into play.
When we're talking about the importance
and the severity of the situation.
You take documents that you're not supposed to have,
and you either are asked for them back and say no,
or you just know you have them and you don't give them back.
That's willful retention.
Willful retention is bad.
If that information is something that betrays means and methods,
how the US gathers secrets or its capabilities that are unknown to the opposition, that's
worse.
If you were to take information and then transmit it, and that doesn't have to be like some
spy thing, you could just be telling somebody, that's worse than retention.
It becomes even worse if it is the means and methods type stuff.
And then it becomes worse if the person you transmitted it to is a foreign national.
And then it becomes even worse if it's means and methods type stuff.
Like I don't know, hypothetically speaking, how close US subs can get to opposition subs
without being detected.
That would fall into that category of the capabilities that are unknown to the opposition.
would be providing that information to a foreign national, even from an allied
country, when you're not supposed to, especially about a topic like that. I
mean realistically the only thing that's kind of worse would be directly handing
it to a hostile power. So why am I saying all of this? Because recent reporting
suggests that Trump did that. The most recent reporting suggests that after
Trump left the White House he was at his club and he met with an Australian
businessman. During this conversation the topic turned to subs and that makes
sense. Australia's during this period. The idea of Australia getting subs from
the US and all of that, that's, it's a topic. So, sure, they talk about that.
During this conversation, according to the reporting, the former president of
the United States leans in and tells him information about nuclear capabilities
of those subs and how close they can get to opposition subs without being detected.
Reading the reporting, it certainly sounds like the former President of the United States
was playing the game of who's in the know with information that could quite literally
cost people their lives.
I don't know what kind of ego it requires to be the former president of the United States
and still play the game of who's in the know.
So all of this is bad.
This is definitely worse than the initial allegations.
But it actually gets worse from here because according to the reporting, the Australian
businessman then shared the information with about 45 other people to include journalists,
to include Australian officials.
It has to be assumed at this point that that information is now in opposition hands.
There is not a way to safely assume that that secret is still secret.
Incidentally, if the former president had wanted to actually be in the know, probably
would have read his intelligence briefings.
And had he read those, he probably would have understood that providing that kind of information
to a foreign national particularly one from a Five Eyes nation was gonna get
noticed especially if that businessman then went on to tell Australian
officials about it. Some very high-ranking ones. I don't know it seems like
something they might make a note of and then that information would of course
course get back to the United States. There might be a whole lot of
documentation on this. Way more than is actually in the reporting and is talked
about. So there's that. Now given this I would like to amend a position that
I had. I was under the impression that if Trump was found guilty that the US
government, because he's a former president, would make certain
accommodations concerning his sentence. I no longer believe that. If these
allegations are true and this is proven in court, I do not believe that. I don't
believe that to be the case anymore. The things that we don't know are, of course,
whether or not the information actually got out beyond these 45 people and
whether it's in hostile hands, but it has to be treated that way now. The other
thing that we don't know is whether or not Trump provided the real information
because he often just, you know, makes stuff up. We don't know that either, but
But I'm going to suggest that U.S. nuclear sub-capabilities and the United States' nuclear
deterrent are probably not something that should be discussed in a club for clout.
This is bad.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}