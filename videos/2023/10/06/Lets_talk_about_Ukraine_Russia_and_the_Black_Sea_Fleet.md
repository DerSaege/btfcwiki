---
title: Let's talk about Ukraine, Russia, and the Black Sea Fleet....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=1lqeWVewjQQ) |
| Published | 2023/10/06 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Russia's Black Sea Fleet stationed in Crimea, occupied Ukraine, had to withdraw as Ukraine forced a withdrawal.
- Despite Ukraine not having a strong Navy, they managed to bottle up the Black Sea Fleet on the other side of the Kerch Bridge.
- Analysts are surprised by Ukraine's success, as it was deemed impossible for them to defeat the Black Sea Fleet.
- DC analysts discussing the length of Ukraine's success should acknowledge that it never should have lasted this long.
- Politicians in DC, some leaning into far-right and authoritarian rhetoric, are trying to disrupt aid to Ukraine because a Russian loss makes them look bad.
- Supporting Ukraine's efforts is critical for the West, as the consequences of abandoning them will lead to regret.
- Those undermining Ukraine's success for social media points are willing to abandon a force that has repeatedly achieved the impossible.
- The United States needs to continue supporting Ukraine despite internal political division to ensure international success.
- Some politicians are more focused on personal gains and looking good on Twitter than on supporting foreign policy wins.
- It is imperative for those who understand foreign policy to advocate for continued support to Ukraine to avoid failure on the international stage.

### Quotes

- "A country without a Navy to speak of forced the withdrawal of the Black Sea Fleet."
- "Those people who actually understand foreign policy need to be out there screaming and explaining what the consequences of not failing."
- "Supporting Ukraine's efforts is critical for the West, as the consequences of abandoning them will lead to regret."

### Oneliner

Russia's Black Sea Fleet withdraws from Crimea as Ukraine, without a strong Navy, achieves the impossible, while DC analysts and politicians disrupt aid, risking foreign policy success.

### Audience

Foreign policy advocates

### On-the-ground actions from transcript

- Advocate for continued support to Ukraine (suggested)
- Raise awareness about the importance of supporting Ukraine's efforts (implied)

### Whats missing in summary

The emotional intensity and urgency conveyed by Beau in urging support for Ukraine's foreign policy success. 

### Tags

#Ukraine #Russia #BlackSeaFleet #ForeignPolicy #Support #Advocacy


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we're going to talk about Ukraine and Russia and
the attitudes that are shaping up in DC and the Black Sea
fleet, because something really interesting happened.
And it is something that's definitely worth noting and
should help shape policy. Russia's Black Sea Fleet was stationed in Crimea, occupied Ukraine.
It's had to withdraw. It had to withdraw. Ukraine, a country without a Navy to speak
of early, defeated the Black Sea Fleet. They had to withdraw. They forced a
withdrawal. They've got it bottled up on the other side of the Kerch Bridge back
in Russia. This was done by a country that really doesn't have a Navy. Not
Not really.
Not one that can match it.
By any real standard, really any analysis, this never should have happened.
Just like the advance never should have been stopped.
Just like Ukraine shouldn't be making gains.
But all of that happened.
And right now, in DC, you have a whole bunch of people talking about how long it's taking.
I would remind everybody that it never should have lasted this long.
Those same analysts that are feeding the idea that, well, we just can't keep up, they're
just not going to be able to do it because they didn't do it on our timetable.
They were the same people that had the timetable of the Capitol falling in two weeks.
A country without a Navy to speak of forced the withdrawal of the Black Sea Fleet.
It's one of those moments where maybe those who say something is impossible shouldn't
interrupt those people doing it. There are a bunch of politicians right now,
many of whom have leaned into far-right rhetoric, that authoritarian rhetoric.
Therefore, if Russia loses, well it looks bad. It looks bad on them because that's
what they bought into. So they're doing everything they can to disrupt the aid.
There is not a single person who has ever gotten a foreign policy briefing
who doesn't understand how much of a win this is on the foreign policy scene for
the United States. If they say they don't get it, they say well I don't know what's
in it for the United States, they're lying.
Their rhetoric, what they have pushed, how we need this ultra-masculine military like
Russia has, that's what it's about.
They're willing to undermine a force that has done the impossible over and over and
over again for a Twitter talking point.
If the United States and the West in general does not continue to support Ukraine's efforts,
the West will regret it.
The West will regret it.
This is one of those moments where those people who actually understand foreign policy need
to be out there screaming and explaining what the consequences of not failing because it's
It's not failure of abandoning a force that has done the impossible over and over again.
The United States has a lot of division within its political parties and that's been around
for a long time. But generally speaking, most times, those who had diverging views, they
all wanted the U.S. to succeed on the international stage. Those people who are now doing everything
they can to disrupt this aid, they want to see failure.
Because it will make them look good on Twitter.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}