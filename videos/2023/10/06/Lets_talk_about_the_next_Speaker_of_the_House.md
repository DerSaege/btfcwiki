---
title: Let's talk about the next Speaker of the House....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Ux4VyotRuls) |
| Published | 2023/10/06 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Speculates on potential next speaker of the US House of Representatives within the Republican Party.
- Notes the main contenders as Scalise, Jordan, and possibly Emmer, with Scalise and Jordan emerging as the primary focus.
- Describes Scalise as more deliberate and capable of making deals, while Jordan is more into social media engagement and performative actions.
- Points out that Scalise is favored among moderate Republicans, while Jordan is closely linked to Trump.
- Suggests that Scalise's ability to make deals and Jordan's connection to Trump may influence their chances of becoming the speaker.
- Emphasizes the importance of the speaker being able to make compromises and bring factions together.
- Predicts potential challenges for Jordan if he becomes speaker due to his extreme persona.
- Addresses the disarray within the Republican Party and advises the Democratic Party to capitalize on it.
- Urges the Democratic Party to allow the split in the Republican Party to be on full display, showcasing the far-right faction's dynamics.
- Stresses the need for the Democratic Party to prioritize stopping authoritarianism over other governing objectives.

### Quotes

- "Everybody wants to be speaker until it's time to do speaker stuff."
- "Take the win and run with it."
- "The most important duty of the Democratic Party right now is to stop the march of authoritarianism."

### Oneliner

Beau speculates on potential speakers in the Republican Party, advises Democrats to capitalize on GOP disarray, and prioritize halting authoritarianism.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Allow the split in the Republican Party to be on full display by not intervening (implied)
- Prioritize stopping authoritarianism over other governing objectives (implied)

### What's missing in summary

Insights on potential strategies for Democrats to effectively address the Republican Party's disarray and prioritize combating authoritarianism. 

### Tags

#USPolitics #RepublicanParty #Speaker #DemocraticParty #Authoritarianism


## Transcript
Well, howdy there, Internet people, let's bow again.
So today, we are going to talk about
the US House of Representatives
and the next speaker, who the next speaker might be,
because we are starting to see the camps kind of form up
in the Republican Party.
And generally speaking, there are around two people.
Now, there are more names out there
and this doesn't rule out the possibility of an outlier,
But right now, it seems like the focus is on two main personalities.
One being Scalise, the other being Jordan.
Now, there's a third one, Emmer, but there's a lot of opposition.
And anybody who would be happy with Emmer would be happy with Scalise.
So I'm saying that Scalise and Jordan are your leading contenders for a right now.
So who are they?
Scalise is, he's definitely right wing, hard right to be honest.
But he's a little bit more deliberate than Jordan, a little bit more of the type that
could make deals make progress and get things moving. Jordan is a little bit
more into the social media engagement performative stuff. Scalise probably the
favorite among moderate Republicans. Jordan pretty well linked to Trump.
Scalise has the ability to make deals so might be able to get the votes. Jordan
might have Trump make calls for him. So it's hard to say who would come out on
top, especially this early on, but if the Republican Party had any sense at all, it
would probably be Scalise. Realistically, on a policy level, Jordan and Scalise
they're not that different. They're really not. It's just Jordan is more inflammatory,
gets that engagement, seems more popular. But the reality is that Scalise is probably
more suited to being speaker. If you're speaker, you have to make the deals, you have to make
the compromises. You have to bring various factions together. You can't do
that if your whole persona is that you're super extreme. Jordan's gonna
have an issue with that. But at this point we don't know. The other thing that
I think everybody needs to keep in mind is that, you know, everybody wants to be
cat until it's time to do cat stuff. Everybody wants to be speaker until it's
time to do speaker stuff. I don't know that Jordan in particular understands
the situation he's kind of volunteering to put himself in. If he takes that
spot and he tries to actually be a speaker, he's going to have problems with
the same faction McCarthy did. Scalise will too. If he doesn't try to make the
deals, Jordan would be more likely to do this and just align with that extreme
far-right faction of the Republican Party then nothing happens and the
Republican Party gets nothing done. They have nothing to show for themselves.
Moving into election time that's not good. The other thing that's worth
noting is the Democratic Party's response to this and a lot of the
commentators response to this. There's a lot of people second-guessing how it
went down. Well, maybe they should have helped McCarthy. Maybe they should have
done this. Maybe they should have tried to get this in exchange. Maybe blah blah
blah. A whole bunch of different options. The Republican Party is in total and
utter disarray. It's on display for the entire country. That should probably be
the focus. This is a problem not just among leftists but even among liberals,
the American left, take the win. Take the win and run with it. If the Democratic
Party was so divided they couldn't even choose a speaker, what do you think
Republicans would be doing? They'd be out there in front of cameras every day
talking about how incapable of governing the Democratic Party is because they're
so extreme because they try to to cater to that radical faction. They'd be doing it
all the time. The Democratic Party is busy second-guessing itself. Take the
win. Capitalize on it for once. Now there are, there's one thing that the
Democratic Party can do right now. And the good news is it doesn't require them
playing a hardball because that's not something the Democratic Party is really
good at. It's allowing the Republican Party to bring their Twitter rhetoric to
the House floor. A lot of these people, they are products of Trump. A lot of
representatives? They are vindictive. They are the type of people that will hold a
grudge and there's a lot of bad blood right now. Let them bring it to the house
floor. That would be the wisest thing for the Democratic Party to do, would be to
allow that split that is so apparent right now in the Republican Party to be
on full display. I understand that the Democratic Party actually wants to
govern, like I get that, but the most important duty of the Democratic Party
right now is to stop the march of authoritarianism. That far-right faction,
that's the primary duty. More than anything else, that's what has to be done.
done. There's an opportunity to allow that faction to put itself on full
display and the Democratic Party doesn't really have to do anything. They just
have to allow it to happen. Don't save the Republican Party out of some spirit
of bipartisanship because you want to get back to business as normal. Business
as normal for the Republican Party is a slow march towards abject
authoritarianism. You don't want business as normal. You don't want things to get
back to that. Allow it to go on display. Anyway, it's just a thought. Y'all have a
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}