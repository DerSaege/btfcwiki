---
title: Let's talk about Trump's motion to dismiss the DC case....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=lOdNFT4VpuQ) |
| Published | 2023/10/06 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau dives into Trump's recent motion in the DC case, distinct from the Georgia, New York, and Florida cases.
- Trump is seeking a motion to dismiss based on being charged for acts that allegedly fall within his official responsibilities as president.
- Beau doubts the motion will gain traction, especially the argument that impeached individuals cannot be charged afterward.
- He references Article 1, Section 3, Clause 7 of the Constitution, pointing out that impeachment does not preclude indictment and trial.
- The strategy behind Trump's motion seems to be creating appealable issues for the Supreme Court in case of a conviction.
- Beau suggests that multiple cases might require similar appealable issues to reach the Supreme Court.
- Trump's defense appears weak, relying on claims of innocence and official duties protection.
- Beau anticipates that the evidence against Trump is substantial and doubts a technicality will make the charges disappear.
- He stresses the importance of a stronger defense than simply denying wrongdoing or claiming actions were part of official duties.
- Despite potentially grabbing headlines, Beau advises not to worry too much about the situation.

### Quotes

- "They're going to try to get in front of the Supreme Court in the eventuality that he's convicted."
- "I wouldn't worry about this too much."
- "There's a lot of evidence in some of these."
- "I think there's going to need to be a stronger defense than that."
- "It's attention grabbing."

### Oneliner

Beau breaks down Trump's weak defense strategy in the DC case, hinting at a Supreme Court plan and the need for a stronger defense against substantial evidence.

### Audience

Legal analysts

### On-the-ground actions from transcript

- Monitor legal developments closely (suggested)
- Stay informed about constitutional interpretations (suggested)
  
### Whats missing in summary

Insights on the potential implications of Trump's defense strategy and the significance of evidence against him.

### Tags

#Trump #Constitution #Impeachment #LegalStrategy #SupremeCourt


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about Trump
and the Constitution and his most recent motion
in the DC case.
So to be clear for those keeping track,
not the Georgia one, not the New York one,
not the Florida one, the DC one.
And we're just gonna kind of run through that
and talk about what he's actually probably trying to do
this because I would imagine that anybody who submitted this or has read
it so far knows that this is probably not gonna go anywhere. So Trump has filed
for a motion to dismiss based on the idea that he was charged quote for acts
that lie not just within the outer perimeter but at the heart of his
official responsibilities as president." Yeah, I don't think that that's gonna go
over well. I don't think that categorizing the alleged conduct in that
indictment as the heart of his official duties, I don't think that that's going
to get any traction at all. Also, he seems to suggest that somebody who was
impeached can't be charged afterward. The impeachment clauses provide that the
president may be charged by indictment only in cases where the president has
been impeached and convicted by trial in the Senate.
I get what's being said here.
It's Article 1, Section 3, Clause 7.
But they're adding a word that I think is probably really important.
This is what the Constitution actually says.
Judgment in cases of impeachment shall not extend further than to removal from office
and disqualification to hold and enjoy any office of honor, trust, or profit under the
United States, but the party convicted shall nevertheless be liable and subject to indictment,
trial, judgment, and punishment according to law."
It doesn't indicate that only those convicted.
saying that even if somebody went through the impeachment process and
suffered a penalty under the impeachment process, even then they can still be
indicted. That's what's there. Making it seem as though that only applies
to people who were convicted, that's a bit of a stretch. Given that this is
incredibly unlikely to go anywhere, you have to wonder what the plan is, or at
least I do. And it seems pretty clear to me that at this point they are trying to
create issues that they see as something that could be appealable to the Supreme
Court. That seems to be the plan. They're going to try to get in front of the
Supreme Court in the eventuality that he's convicted, if that's what happens.
That seems to be what they're doing here because there's no way they can think
this will fly. I wouldn't worry about this too much. I don't feel as though
manufacturing a pillable issues like that is going to matter because
by the way things are shaping up, it seems like there might be multiple cases
that the former president would have to manufacture a pillable issues on to get
in front of the Supreme Court. It seems to me, and again I'm not an attorney, but
But it certainly appears to me that there is a high expectation among some in Trump's
orbit that maybe these cases don't go his way and they're trying to plan an out.
I also feel like some of them may not understand exactly how serious the situation is.
There's a lot of evidence in some of these.
To support the allegations, there's a lot of evidence.
I find it incredibly unlikely that there's going to be some technicality that ends up being a magic bullet that makes it
all go away, not based on what I've seen, and thus far, what we have seen from Trump is I didn't do anything wrong,
and even if I did, it was part of my official duties, and you can't charge me even if I did do something wrong that I
didn't do.
I think there's going to need to be a stronger defense than that. Just saying.
This is probably going to get a lot of headlines because it's attention grabbing.
I wouldn't worry about it too much anyway it's just a thought y'all have a
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}