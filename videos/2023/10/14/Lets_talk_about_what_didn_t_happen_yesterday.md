---
title: Let's talk about what didn't happen yesterday....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ysCWaQovjsg) |
| Published | 2023/10/14 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Large segments of the country were scared yesterday due to a false story about a global day of struggle being called by a well-known organization leader based in Gaza.
- The story created fear of potential attacks, particularly in the US, even though the actual message was misreported.
- The leader of the organization in question is not the current leader and did not call for what was reported.
- The actual message contained geographically limited calls to action, none of which targeted the US for any negative actions.
- Some media outlets sensationalized the story for engagement and ad revenue, spreading fear and misinformation.
- Reframing calls to action to make them worse and broadcasting them widely is irresponsible and dangerous.
- Sensationalism and fear-mongering in the media only serve to create unnecessary conflict and harm.
- Sharing false or dangerous calls to action can lead to real harm if the wrong person acts on them.
- The media's role in amplifying and sensationalizing misinformation can have serious consequences for public safety.
- Engaging with and spreading harmful messages, even for the sake of debate or clicks, can have real-world repercussions.
- Conflict and violence should not be promoted or amplified through irresponsible reporting or sharing of misinformation.
- It's vital to be cautious about what information is shared online, especially when it involves calls to action that could incite harm.
- The irresponsible sensationalism and fear-mongering in the media can have far-reaching consequences beyond just generating clicks and engagement.
- Creating unnecessary panic and spreading false information can lead to real harm and dangerous consequences.

### Quotes

- "If you have something that crosses your feed that appears to be a call to action where people will get hurt, don't share it."
- "Please remember that that type of conflict, it's a PR campaign with violence."
- "Sensationalism and fear-mongering in the media only serve to create unnecessary conflict and harm."
- "The irresponsible sensationalism and fear-mongering in the media can have far-reaching consequences beyond just generating clicks and engagement."
- "Creating unnecessary panic and spreading false information can lead to real harm and dangerous consequences."

### Oneliner

Large segments of the country were scared due to a false story about a global day of struggle, sensationalized by media for engagement, spreading fear and misinformation.

### Audience

Media Consumers

### On-the-ground actions from transcript

- Refrain from sharing potentially harmful calls to action online (implied).
- Verify information before sharing potentially dangerous messages (implied).

### Whats missing in summary

The full transcript provides a detailed account of how sensationalism and fear-mongering in media can lead to real-world harm and conflict. Viewing the entire transcript gives a comprehensive understanding of the dangers of spreading misinformation.

### Tags

#MediaConsumers #Misinformation #FearMongering #Sensationalism #CallToAction


## Transcript
Well, howdy there, internet people.
It's Bob again.
So today, we're going to talk about yesterday
and what didn't happen yesterday,
and why it didn't happen yesterday,
but why it absolutely could have happened yesterday.
Go ahead and tell you now, this is going to be one of those
videos where I'm real vague in the beginning,
but it'll all come together on this one.
There may be some people who don't
Participate in social media and don't consume certain outlets who have no idea what I'm
talking about at first, but it'll all come together.
See yesterday, large segments of this country were scared, terrified.
Yet a sitting member of Congress indicate that they were concerned about traveling yesterday,
Friday the 13th.
The reason they were scared, as the story goes, the leader of this organization that's
very well known right now, it's in all the papers, it's based in Gaza, the leader of
that organization, well he called for a global day of blank.
I'm not going to say the word, Americans in particular have come to associate it with
attacks.
It's not necessarily what it means, it starts with a J.
But that was the story that went out from commentators, a couple of outlets, that the
leader of that organization had called for a global day of struggle, a global day
of struggle. And then this weird thing happened. While everybody was saying that
there were going to be a bunch of attacks yesterday, there weren't any in
the US, right? That's weird. That's weird. For somebody like that, the leader of
that organization to call for it and for it not to happen? That's odd. It's odd
unless you know two really important things. First, he's not the leader of that
organization. Not anymore. He was at one time. Now he's kind of like the foreign
affairs PR person. And two, way more important. That's not what he called for.
for that. That phrase you see in quotes, find it in what he said. So, he put out a message and there
were some calls to action in it and one of them, I'm not going to repeat, there were like three
calls to action. One, I wouldn't repeat, because it would be irresponsible to amplify that message.
But that message was geographically limited to people in certain countries, none of them in the
the US.
And then another was geographically limited to, I think he said, the Arab world or the
Arab and Islamic world.
And then the only one that wasn't geographically limited was the call for relief, for aid.
That was the global call that went out.
So the message went out a couple days ago, why didn't I put out this video yesterday?
Because it still could have happened.
Even though he didn't put out that call, it still could have happened.
commentators and some outlets in this country said that he did. They reported
that if that message had gotten to somebody who was sympathetic, who was
radical, who was waiting for a call, and who doesn't fact-check, really bad things
could have happened. And it was all in a quest for clicks, for engagement, ad
revenue, fear-mongering, ad revenue. If you think the payout from Fox was big, I
to imagine what it would be if you put out a call like that, attributed to somebody who
people might listen to when they didn't, and then somebody acted on it.
That wasn't the call that went out.
But that's how it got reported.
I'm going to suggest that reframing a call to make it worse and then broadcasting it
everywhere, I'm going to suggest that's a really bad idea because if the wrong person
hears it, they might act on it.
The thing is, I don't even get it.
I make four videos a day and still have to do a weekly recap.
This world does not need more sensationalism.
It doesn't need more bad things.
There's plenty to talk about.
There's plenty to cover.
Even in just general practice, a message like that, a call to action, like the one that
was reported, that's actually not something you're supposed to broadcast.
That's not something you're supposed to put out and hold a megaphone to.
people would not have heard about that.
But it gave commentators in this country the opportunity to talk tough.
My guess is that most of them knew that that's not really what it called for.
It gave them that opportunity, but it also created the opportunity for somebody to hear
that and to act on it when that wasn't the call that went out.
That was a really bad move by a whole lot of people.
If you have something that crosses your feed that appears to be a call to action where
people would get hurt, don't share it.
Don't share it because you might be the person who puts it in front of somebody that would
act on it.
engage with those posts. Spreading that message, especially, like here's the thing,
even if that was what he called for, broadcasting it the way that it was,
that's still a bad move. People could have got hurt over social media clicks.
Please remember that that type of conflict, it's a PR campaign with
violence. There were a lot of commentators and a lot of outlets that
did the PR part for them. Not even for them.
Eventually, something like this is going to be reframed.
They're going to create a scare of the week.
They're going to create some call to action and somebody's going to pick up the phone.
When that happens, the cost is not going to be measured in dollars.
It's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}