---
title: Let's talk about Senator Menendez and Egypt....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=pboDfNWPfts) |
| Published | 2023/10/14 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Senator Menendez faced trouble due to allegations of corruption involving Egypt.
- The word "agent" in this case doesn't necessarily mean espionage.
- The allegations involve general corruption with international implications.
- The allegations focus on business corruption rather than espionage.
- The Democratic Party has called for Menendez's resignation.
- Menendez has resigned from committees but not from Congress.
- The calls for his resignation have intensified.
- The federal government continues to pursue investigations.
- The situation does not seem to involve information damaging to the United States.
- The allegations suggest Menendez used his office to influence government policy.

### Quotes

- "The allegations appear to center on general corruption and Egypt benefited."
- "It changes from a normal corruption case to a situation where somebody's being called an agent."

### Oneliner

Senator Menendez faces allegations of corruption involving Egypt, prompting calls for resignation amidst ongoing investigations and implications of influencing government policy.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Support efforts calling for accountability for political figures (implied)

### Whats missing in summary

Full context and depth of evidence and developments in the Senator Menendez corruption case

### Tags

#Corruption #SenatorMenendez #ResignationCalls #PoliticalInfluence #DemocraticParty


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to once again
talk about Senator Menendez.
And we will go over the most recent developments
and my personal relief at the most recent developments
and where it goes from here.
Okay, so if you have missed the news,
a little bit of a recap, even though we've talked about it.
The senator recently ran into a little bit of trouble.
He was indicted on allegations of a pretty wide-ranging
corruption scheme.
And we talked about it at the time, and I was like,
honestly, this isn't something I would normally cover.
But there's this weird little foreign policy angle to it
where it appears, according to these allegations, that he
might have been kind of working for Egypt.
The Senator has been hit with a superseding indictment
for working as an agent of Egypt.
Now, when Americans hear that word agent, especially when you're talking about
something that crosses international lines, you're
thinking espionage, James Bond type stuff. It has
wider meaning. If you were to be engaged in just general corruption and it
crosses international lines and the person on the other side of that
international line is a representative of a foreign government, hey guess what?
You're an agent. That really appears to be the heart of the allegations, which to
To be honest, I found very, I was relieved.
Not to say the allegations aren't bad, but this senator sat on some committees that would
have given him a lot of access to a lot of information that could have got people hurt.
That's not really part of the allegations.
It really looks more like what you would see as business corruption, for the most part.
But the allegations cross international lines, representatives of a foreign government, so
on and so forth, makes them an agent.
So not to downplay the charges, they're pretty serious.
It's just not as damaging to the United States as I thought it might end up being, which
is good news.
As far as the strength of it, going off the indictment, nothing's changed.
It appears to be stronger than the Trump-DC case, but not as strong as the Trump-Documents
case.
That's where it's at.
The Democratic Party has called for him to resign.
At this point, he's resigned from committees but hasn't resigned from Congress yet, hasn't
resigned from the Senate, which is keeping in line with the new tradition that exists
up there.
The calls for him to resign have kind of intensified a little bit.
I don't know how hard the Democratic Party is going to push for it, for the same reason
Republicans haven't pushed too hard to get rid of Santos. The narrow majorities.
That's the concern, I am sure, from both sides on this.
What ends up happening from here as far as him resigning? Don't know. Don't know how that's
going to play out. This does raise the stakes for him as far as the
superseding indictment in a couple of different ways. One, obviously it's
another charge, but more importantly it shows that the federal government is not
exactly done investigating or anything like that. So we'll have to wait and see
but at time of filming it doesn't look like it was a situation where information
was getting passed that was incredibly damaging to the United States, which was
why I was interested in this case. Doesn't look like that's happened. It
looks like the allegations center on general corruption and Egypt benefited.
So, that's where it kind of, it changes from a normal corruption case to a situation where somebody's being called an
agent.
There appear to be allegations of him using his office to influence government policy, which as we have talked about
before, agent of influence type stuff.  We'll have to wait and see what other evidence comes up because I still don't
think this is over.  Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}