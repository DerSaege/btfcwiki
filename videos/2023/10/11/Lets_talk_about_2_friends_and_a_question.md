---
title: Let's talk about 2 friends and a question....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Uhd61gX2KjA) |
| Published | 2023/10/11 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introducing two friends who have never met but share similarities in their differences.
- One friend comes from a loaded family but chose a blue-collar path as a mechanic to not embarrass his family.
- The other friend came from poverty, received a foundation's help, and became a surgeon but still lived modestly to honor his roots.
- Beau wanted to introduce them, believing they'd be close friends, but they both moved back home before that could happen.
- Beau fears they might meet now because the powers that be are pushing them to harm each other.
- He has personal connections in the affected areas and is worried about the potential loss of lives.
- Beau stresses that those involved in conflicts are real people with friends, pasts, and futures.
- He challenges viewers cheering for sides to recognize the humanity in the conflict.
- Beau ends with a thought-provoking message, urging viewers to think about the consequences of their actions.

### Quotes

- "When you are watching this on your screens and you are cheerleading for your side. Just remember, those are real people."
- "They've got friends, they've got a past, some of them have a future."
- "But I'd be willing to bet that in a different circumstance, there wouldn't be a fight."

### Oneliner

Beau introduces two friends with contrasting backgrounds and hopes they don't meet amid external pressures to harm each other, reminding viewers of the humanity behind conflicts.

### Audience

Viewers

### On-the-ground actions from transcript

- Reach out to those in affected areas or their families to offer support and solidarity (suggested).
- Raise awareness about the human impact of conflicts and advocate for peaceful resolutions (implied).

### Whats missing in summary

The emotional depth and personal connections shared by Beau in his story can be best appreciated by watching the full transcript. 

### Tags

#Friendship #Conflict #Humanity #PersonalConnections #Peacekeeping


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we're gonna do things a little bit different.
We're gonna do things backwards.
Today, I'm gonna tell you a story,
and then I'll give you the question that prompted it.
So today, we will talk about two of my friends.
Two of my friends who, they've never met each other,
but I've always wanted to introduce them
because even though they are different,
in just about every visible way,
They are also just utterly identical in so many others.
One of them is this blue collar, working class guy, but his family is not.
They're loaded.
And by loaded, I mean loaded.
Dad is like chief of cardiology at wherever.
Older brother is a doctor.
older brother was in medical school when I met him. But my friend, he didn't go to
college. He liked cars, he liked turning wrenches, he became a mechanic. Eventually he got his
own shop and did it without money from dad, but even though he was very much
blue collar, he never put out that appearance. He always still dressed like
he had money because he didn't want to embarrass his parents. The other guy did
not come from money, came from total, total poverty, but some foundation came
along and swooped him up and he went to all the right schools, all the right
schools. Ironically went to medical school and when he went to medical
school I lost track of him for a while and then I ran into him randomly on the
other side of the country and when I saw him honestly I didn't recognize him at
first, and if the coffee cup he had, if it had been empty, I would have put money in it.
Guy's a surgeon and still looked like he lived in just total poverty because he
didn't want to embarrass his parents, didn't want to forget where he came from.
I always wanted to introduce them because I knew they would be friends,
they would become close friends, and I never got that opportunity because both
of them wound up moving back home. One moved for a girl and because he thought
his skills would be better used there. The other moved for a girl and because
never really fit in in the U.S. I have wanted to introduce them for years, years,
and now I find myself hoping they don't meet this week because the powers that be,
their betters, well their betters have decided that they should try to kill
each other. The question, you don't seem to like talking about this. I have a lot
of personal connections here. There's probably 30 people that I know that are
either in the affected areas or their family is and it is an almost certainty
that one of them or
one of their family members will be lost. And these are 30 people
that if I had a party and
they were all there, I'm willing to bet there would not be a fight.
When you are watching this
on your screens and you are
cheerleading for your side. Just remember, those are real people. They've got friends,
they've got a past, some of them have a future. But I'd be willing to bet that in a different
circumstance, there wouldn't be a fight. Anyway, it's just a thought. You'll have a good day
Bye.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}