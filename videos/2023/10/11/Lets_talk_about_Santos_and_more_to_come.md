---
title: Let's talk about Santos and more to come....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Og-EPCqMIcs) |
| Published | 2023/10/11 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Representative George Santos faces a superseding indictment with 10 additional counts, alleging unauthorized use of donor credit cards.
- Nancy Marx, Santos's former campaign treasurer, entered into a plea agreement with the federal government for a sentence of around three and a half to four years.
- The new allegations suggest that money from donor credit cards may have ended up in Santos's own account.
- The situation has escalated, expanding the scandal within the Republican Party.
- With evidence mounting and a cooperating witness from Santos's team, his options seem limited to cooperating and providing bigger fish.
- The slim majority and disorganization within the Republican Party may prevent immediate action to oust Santos.
- The scandal is likely to snowball further until the party is forced to act, despite ongoing developments and increasing counts in the indictment.

### Quotes

- "Ten additional counts is what it looks like."
- "The situation has escalated, expanding the scandal within the Republican Party."

### Oneliner

Representative George Santos faces a superseding indictment with 10 additional counts, escalating a scandal within the Republican Party as evidence mounts and options narrow.

### Audience

Political observers

### On-the-ground actions from transcript

- Contact political representatives about accountability within political parties. (implied)

### Whats missing in summary

Details on the potential consequences for Representative George Santos and the Republican Party.

### Tags

#Politics #Scandal #RepublicanParty #Indictment #Accountability


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about
Representative George Santos, and more to come.
So, I wanna say it was sometime within the last 48 hours
or so, it's getting fuzzy right now,
we talked about how the Santos former campaign treasurer,
campaign treasurer, Nancy Marx, had entered into a plea agreement with the
federal government and that the plea agreement called for a sentence in the
range of I want to say three and a half to four years, something like that. At
that time I said there was more to come on this. Well, more has come. George Santos
has been hit with a superseding indictment. It looks like an additional
10 counts. It appears, according to these allegations, that on top of everything
that has been previously covered, there may have been donor credit cards used
for other expenses that were not authorized or known about by the card
holder. I want to say that some of the money may have ended up in Santos's own
account as far as what's contained in the allegations. So this is a whiter,
a whiter thing than was previously known. It also ups the stakes. When people were
talking about that plea agreement and talking about the amount of time that
she was looking at as far as a plea agreement in which she certainly
appeared to also be cooperating and providing additional information, they
thought that was high. The new indictment kind of spells it out. The
The Republican Party is now in a situation that it probably doesn't want to be in.
It is expanding, eventually it will expand further.
And the other thing to note is that at this point, when looking at the evidence and the
amount of paper trail involved and the fact that now there is a person who's entered
into a plea agreement. It seems like Santos's only real move would be to give them a bigger
fish. I would imagine that nobody in the House of Representatives is talking to Santos right
now. Now, under normal circumstances, a political party would be expected to kind of oust someone
over this. However, due to the slim majority that Republicans hold in the
House and their just absolute inability to to get their act together, that seems
unlikely. So the Republican Party is going to continue to have this scandal
because they don't have the ability to control their own conference in the
house and it's just going to continue to snowball. It's going to get bigger and
bigger until they're finally forced to act. I don't know how bad they're going
to let it get before they act though. But that's the developments as of today.
Ten additional counts is what it looks like. There is reporting saying it's 23
additional counts. I think that's misreading it. There were 13 counts
initially. I believe the total is 23 but I might be wrong about that but there
are at least 10 new counts.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}