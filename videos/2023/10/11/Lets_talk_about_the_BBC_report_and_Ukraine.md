---
title: Let's talk about the BBC report and Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=iXdIp1TFJu0) |
| Published | 2023/10/11 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the audience as "internet people" and discussing the BBC report and its credibility.
- Mentioning a report circulating on social media citing Bellingcat, an investigative journalism outfit.
- Emphasizing the clout of Bellingcat's reports regardless of personal opinions about them.
- Clarifying that Bellingcat did not make the specific claims attributed to them in the report.
- Exposing the manufactured nature of the BBC video and labeling it as an information operation.
- Pointing out that the misinformation spread was designed to provoke reactions and push a Russian talking point.
- Warning about the sophistication of misinformation tactics in the information space.
- Urging people to be cautious with news sources, especially on social media, and to seek confirmation before believing in a report.
- Stressing the potential harm misinformation can cause, not only in conflicts but also in elections and policymaking.
- Mentioning the European Commission's communication with Twitter regarding content moderation.

### Quotes

- "If you don't read the news, you're uninformed. If you get your news from Twitter, you're probably misinformed."
- "Assume it's not true at first. Wait until you get more confirmation."
- "We have to train ourselves for this."
- "It's something we need to get ready for."
- "It's just a thought, y'all have a good day."

### Oneliner

Beau warns about sophisticated misinformation tactics, urging caution with news sources, especially on social media, to prevent harm and misinformation.

### Audience

Information Consumers

### On-the-ground actions from transcript

- Verify news from multiple sources before believing or sharing (suggested)
- Be cautious with information on social media platforms (suggested)

### Whats missing in summary

The full transcript provides detailed insights into the spread of misinformation, the impact on societal beliefs and policies, and the need for critical thinking and vigilance in consuming news.

### Tags

#Misinformation #SocialMedia #NewsConsumption #Verification #CriticalThinking


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about the BBC.
We're going to talk about a report and what it means.
We're going to talk about social media and we're going to
kind of run through that report and talk about the real
impacts of it and where the information really came from
and whether or not it's something that you should really
pay attention to. So if you have no idea what I'm talking about, there is a report
floating around on social media. It's a BBC report and it cites Bellingcat. Now
if you're not familiar with Bellingcat, it's an investigative journalism outfit.
There's undoubtedly going to be a lot of commentary about Bellingcat in the
comments and that's fine. Y'all can have that conversation. But even if you don't
like them. You have to acknowledge that their reports carry a lot of clout. They
mean a lot to a lot of people even if you disagree with them. They have a
heavy focus on conflict and intelligence work. Okay, so what does the report say?
It says that Bellingcat concluded that arms from Ukraine provided by the West
made their way and were used in that recent event in Israel.
Okay, so that's big news.
That is big news.
I would like to read a statement from Bellingcat.
We've reached no such conclusions
or made any such claims.
We'd like to stress that this is a fabrication
and should be treated accordingly.
So that's important.
the organization that is cited said that they didn't do that. I mean I don't know
how BBC could be that wrong.
Well, it's simple. That's not a BBC video. It's not real. The whole thing's
manufactured. It looks real. It looks good.
good. It's an information operation. Basically, it is putting out a fear that Western assistance
to Ukraine could end up being used in some other way. It is a Russian talking point.
I know somebody's going to say it's a Republican talking point. Look, at this point they're
pretty much indistinguishable. It's an information operation designed to provoke the exact reaction
it was having until those who are credited with it were just like, yeah, no, none of
this is right.
You know, I've got a shirt that says, don't listen to my father.
He gets his news from Facebook.
I need one for Twitter now as well.
If you don't read the news, you're uninformed.
If you get your news from Twitter, you're probably misinformed.
The American people, people in the West in general, you got to get ready for this.
You have to get ready for this.
You can't fall for this every time.
The tactics that are being deployed in the information space as far as information operations,
They are getting more and more sophisticated.
In this case, they're using two sources that have a lot of recognition, a lot of clout.
And it was hours and hours of circulating before there was clarity.
And to be clear, the clarity has not caught up to the spread of the bad information yet.
We're going to have a major issue with this, not just when it comes to conflict, but when
it comes to our own elections and our own policymaking.
So this is something that you kind of have to train yourself for.
If you get a report that you can't find anywhere else and it totally confirms a talking point
of a political side, assume it's not true at first.
Wait until you get more confirmation.
There's never a story that totally confirms somebody's talking point.
That doesn't really happen, especially if you can only find it on social media and you
You cannot find that report on the site of the organization credited with the report
on social media.
We have to train ourselves for this.
This is something that can cause a lot of harm to not just to national security and
stuff like that, but to people, innocent people, as people believe made-up stories
and it shifts policy. It's going to have negative impacts. We have to get ready
for this. In a totally unrelated story, it does appear that the European
Commission has basically told Twitter that they need to get their app together
when it comes to content moderation. It looks like a letter went out about that.
So we'll see what happens, but at this point when I'm going through my messages
and I'm looking at the footage and requests for an explanation of a story
and they're false, it's almost all coming from Twitter at this point, which is a
change. It used to all be from Facebook, so it's something we need to get ready
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}