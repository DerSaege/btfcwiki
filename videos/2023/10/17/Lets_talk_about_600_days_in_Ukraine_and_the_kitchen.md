---
title: Let's talk about 600 days in Ukraine and the kitchen....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=mqXYrHjtf4Q) |
| Published | 2023/10/17 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Russia's strategy in the conflict with Ukraine has not been favorable, both geopolitically and on the ground.
- The conflict has devolved into a hyper-violent real estate transaction, with Russia losing territory it initially secured.
- Russia's use of high-end missiles like kitchen and Kodiak missiles against grain silos is puzzling to Western analysts.
- Western analysts speculate that Russia may lack precision guided munitions (PGMs), leading to the misuse of these high-value weapons.
- Disrupting the grain flow might have shifted from limiting support to Ukraine to becoming a strategic objective for Russia.
- Russia's potential aim could be to pressure Western powers by targeting grain silos, viewing them as high-value targets.
- Russia's shift from self-reliance to seeking military aid from North Korea indicates a reevaluation of victory conditions and strategies.
- Targeting grain silos could be a tactic to pressure Western powers into conceding or negotiating deals to retain captured territory.
- Russia's disregard for civilians in their strategy to disrupt grain supply underscores their focus on exerting pressure on the West.
- While Russia aims to manipulate Western powers, their actions may lead to unintended consequences, similar to their NATO encirclement in Ukraine.
- By targeting grain supplies, Russia may inadvertently remind Europe of the importance of not relying solely on Russian resources.
- Europe's past reliance on Russian energy serves as a lesson, potentially influencing their response to Russia's current tactics.
- The use of high-value missiles against grain silos may seem bizarre, but it likely serves a strategic purpose for Russia.
- Russia's goal may be to create global devastation to coerce Europe into backing off, although this outcome seems unlikely.
- Advisers and policymakers are likely considering long-term implications rather than just the immediate conflict dynamics.
- The strikes on grain silos may reinforce the importance of supporting Ukraine in the conflict for future stability and security considerations.

### Quotes

- "It's devolved into a hyper-violent real estate transaction."
- "Every one of these million dollar missiles that hits one of these grain silos might be serving as a reminder to Europe."
- "I think Russia is hoping to create so much devastation around the world that it puts pressure on Europe to back off."
- "Advisors that are talking directly to policymakers, they're probably not thinking in a transactional looking just at this conflict type of thing."
- "Which reminds them of the importance of keeping Ukraine in the fight."

### Oneliner

Russia's unconventional strategy of targeting grain silos with high-end missiles may backfire, potentially reminding Europe of the risks of relying solely on Russian resources in the conflict with Ukraine.

### Audience

Analysts, policymakers, strategists

### On-the-ground actions from transcript

- Analyze and monitor Russia's actions and strategies in the conflict with Ukraine to understand their potential long-term implications (implied).
- Advocate for diversified resource partnerships to reduce reliance on any single country for critical supplies (implied).

### Whats missing in summary

Insights into potential future geopolitical shifts and implications of Russia's strategic decisions in the conflict with Ukraine.

### Tags

#Russia #Ukraine #Geopolitics #Strategy #Conflict #Europe #ResourceSecurity #GlobalPressure #Policymakers


## Transcript
Well, howdy there, internet people, it's Bill again.
So today, we are going to talk about Russia
and Ukraine and strategy because we are roughly 600 days
into this three-day operation and the results
The results for Russia have not really been favorable thus far.
In the sense of the wider geopolitical war, Russia lost almost immediately.
As soon as other nations started signing up with NATO and citing this conflict as a reason,
it was over for the wider geopolitical war. The fighting on the ground is something else.
Russia has not done well there either. It's devolved into a hyper-violent real estate
transaction. Russia retains, I don't know, roughly less than half of what it was able to secure in
the opening days. They're losing that too. One of the more interesting developments that has recently
come to light is Russia's use of kitchen and Kodiak missiles, how they're being used. For those that
don't know a lot about these things, these are high-end items. They are high-end
items. A kitchen is something you would deploy against like a carrier group.
They're for high-value targets. Russia is hitting grain silos with them. Now
Western analysts, they're saying, well that's obviously because they lack PGMs,
precision guided munitions and that's probably true that's probably true but
there's also a little bit of mockery going on with it saying haha silly
Russians look at them wasting these things that should really be used for
high-value targets on on grain silos yeah the saying isn't silly Russians
Since the start of the conflict disrupting the grain flow, that was something that Russia did,
but it may have transferred from something that was done to limit support and the finances of
Ukraine to more of a strategic objective, making the grain silo a high-value target.
Given the fact that Russia has gone from a country that was perceived as a near
pure to a country that is getting military aid from North Korea in 600
days, they may be re-evaluating their victory conditions and how they hope to
achieve them. They may be hoping to disrupt the grain supply enough to put
a lot of pressure on Western powers to get them to say it's just not worth it
or maybe to push for some kind of deal that lets Russia keep what it's gotten
so far, what it's been able to hold on to. And the civilians that need the food
all over the world, all over the global south, well from the Russian perspective I
guess that really just doesn't matter. Because the louder they cry out for help,
the more pressure there is on the West. That's the way they're looking at it.
Western analysts, those that are actually advising the policymakers, they're
probably looking at that in a very different way and in fact this too may
end up creating the exact opposite reaction that Russia is hoping to
achieve, just like we don't want NATO countries near us so we're gonna
invade Ukraine, and then they get a bunch of NATO countries near them. This is
probably going to have a similar result because right now the grain from Ukraine,
yeah, it's being shipped all over the world, but in a hundred years a lot of
the place is getting it they're going to be uninhabitable and that grain will go
to Europe. Every one of these million dollar missiles that hits one of these
grain silos might be serving as a reminder to Europe that for the sake of
of European food security, they can't let Russia get it. Europe learned by
becoming over reliant on Russia for energy. I doubt they're going to make
that mistake again. You're probably going to see a whole bunch of analysts
mocking them using these missiles against these targets and I get it on
on some level. It is a very bizarre turn of events, but I don't think it's without reason.
I think Russia is hoping to create so much devastation around the world that it puts
pressure on Europe to back off. I don't think that's going to be the outcome, but I think
that's their plan. I think the outcome, if they're paying attention at all, the
advisors that are talking directly to policymakers, they're probably not
thinking in a transactional looking just at this conflict type of thing. They're
thinking 20, 50, 100 years down the road and every one of those strikes just
Which reminds them of the importance of keeping Ukraine in the fight.
Anyway, it's just a thought.
have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}