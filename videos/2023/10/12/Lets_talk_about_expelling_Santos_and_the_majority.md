---
title: Let's talk about expelling Santos and the majority....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=765jY31HWTk) |
| Published | 2023/10/12 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- George Santos, a Republican representative from New York, facing allegations and a superseding indictment.
- Santos has denied the allegations, indicating he has no intention of pleading guilty.
- The Republican Party is moving forward with a resolution to expel him due to the narrow majority.
- Despite previous procedural blocks from Republicans, there's a new resolution to expel Santos.
- The Democratic Party tried to expel Santos in May but was blocked by Republicans.
- Republicans are shielding Santos to maintain their narrow majority in votes.
- If the resolution reaches the floor, Santos' political career is likely over.
- Regardless of the resolution's outcome, Santos' future in politics seems bleak.
- The Republican Party may offer half-hearted measures to maintain a buffer for their majority.
- Santos may view these measures as a form of betrayal.
- There's a slim majority, and there's an active movement to get Republicans to cross over and change the speaker.
- If the Democratic Party falls short by one vote, they might reach out to Santos.
- Pragmatic members of the Democratic Party may talk to Santos if needed for a critical vote.
- Republicans have been protecting Santos due to the narrow majority they hold.

### Quotes

- "He doesn't have any intention of saying, you know, I'm guilty."
- "The reason they are protecting him, the reason they have protected him for so long is because of how narrow that majority is."
- "If that vote comes up one vote short, I have a feeling that one of the more pragmatic members the Democratic Party is probably going to talk to Santos."

### Oneliner

George Santos, embattled Republican representative facing expulsion due to allegations, as the Republican Party maneuvers to maintain a narrow majority.

### Audience

Politically Engaged Individuals

### On-the-ground actions from transcript

- Contact your representatives to ensure they uphold ethical standards (implied).

### Whats missing in summary

Insights on the potential repercussions of political maneuvering on maintaining political power and the impact on individual careers.

### Tags

#GeorgeSantos #RepublicanParty #PoliticalFuture #Allegations #Expulsion


## Transcript
Well, howdy there, internet people, let's bow again.
So today we are going to talk about the embattled Republican
representative, George Santos, and more developments there.
Talk a little bit about his potential political future, some of the things that
he has said in response to the allegations and some movement from the
Republican side, which my guess is kind of half-hearted, but we'll, we'll find
out, okay, so if you have no idea what I'm talking about, a representative from
New York, George Santos, was indicted a few months ago, got a superseding
indictment recently, he has made it pretty clear when talking to reporters
that he doesn't have any intention of saying, you know, I'm guilty.
It doesn't look like something he's going to do.
When asked about the more recent allegations, he said that he, quote, pretty much denies them.
Okay.
The Republican Party is starting to move forward with a resolution to expel him.
That's being brought forward now.
Now, the Democratic Party tried to expel him back in May,
but Republicans did some procedural stuff
and basically blocked it.
Why are Republicans shielding him?
It's because the majority is so narrow.
They need that vote, they need that vote.
So they shielded him once.
This resolution may be one of those things
get sent to the ethics committee or something like that to help maintain that majority.
So it may not be a real resolution, but given the fact that the Democratic Party was hoping
to expel him, if it gets to the floor, it's over.
It is over.
I do not believe that Santos would make it through that vote.
Regardless of what happens in regards to the resolution, his political career is probably
done.
It seems very, very unlikely that he would be reelected after all of this, which puts
the Republican Party in a position where it's like, okay, so maybe we shield him, help maintain
a little bit more of a buffer for the majority and eventually this will all
sort itself out. We'll put forth some half-hearted measures to make it look
like we're trying to take care of it but don't really push it forward, all of that.
The thing is I have a feeling that that would bother Santos a whole lot. I have a
a feeling that Santos might see that as some form of betrayal, even the half-hearted
motions. The majority is very, very slim. There is an active movement to get just
handful of Republicans to cross over and make Jeffrey speaker. I'm not saying that
the Democratic Party should reach out to Santos immediately, but if right before
for the vote, you're a one vote shy, I would imagine given his history that he might be
up for doing something that would put his name in history books, especially if he feels
slighted by the Republican Party.
The reason they are protecting him, the reason they have protected him for so long is because
of how narrow that majority is.
If that vote comes up one vote short, I have a feeling that one of the more pragmatic members
the Democratic Party is probably going to talk to Santos. Anyway, it's just a
thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}