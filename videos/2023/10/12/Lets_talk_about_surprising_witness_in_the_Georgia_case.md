---
title: Let's talk about surprising witness in the Georgia case....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=o5G8XItk0Og) |
| Published | 2023/10/12 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Strange news out of Georgia related to the Trump case.
- Fulton County DA intends to call high-profile witnesses in the October 23rd trial.
- Names like McDaniel and Alex Jones on the witness list raised eyebrows.
- McDaniel expected to talk about alleged phone calls with Trump and Eastman.
- Alex Jones, a conspiracy-like guru, to testify about activities related to the sixth.
- Jones's testimony might reveal his interactions with certain individuals on that day.
- Jones's presence as a witness is unexpected but sheds light on pervasive conspiracy theories.
- The need to counter misinformation is underscored by Jones being involved in a criminal case.
- Despite the entertaining aspect, the case involving high-profile figures shows the seriousness of these theories.
- Jones's involvement indicates the belief in these theories by some individuals in positions of power.

### Quotes

- "The fact that Alex Jones is apparently going to be a needed witness in a criminal case should tell people a lot."
- "This definitely shows the necessity of countering misinformation."
- "There are a lot of people that believe them earnestly and there are a lot of people who are in positions of power who associate with people like Alex Jones."

### Oneliner

Strange news from Georgia involving high-profile witnesses like McDaniel and Alex Jones underscores the pervasive influence of conspiracy theories in circles of power.

### Audience

Legal analysts, activists, concerned citizens

### On-the-ground actions from transcript

- Watch the developments of the trial closely (suggested)
- Stay informed about the implications of high-profile figures being involved in legal cases (suggested)

### Whats missing in summary

Insights on the potential impact of high-profile figures testifying in legal cases

### Tags

#Georgia #TrumpCase #ConspiracyTheories #AlexJones #Misinformation


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about some strange news
out of Georgia.
I don't know if strange is the right word,
surprising maybe.
It pertains to the overall Trump case,
but I guess right now it is specific
to the October 23rd trial of Powell and Chisbro.
and Cheesebro. The Fulton County DA has put out some of the names of the people
that they intend to call as witnesses and a number of them raised eyebrows
because they're high-profile people. McDaniel being one, you know, current RNC
chairperson. That's a big name to call in a criminal trial. I guess as I
understand it, she will be asked to talk about alleged phone calls between her
and Trump and maybe Eastman and how that might establish Cheese Bros.
culpability. We'll have to wait and see how it plays out. Again, all allegations at
this point. Another name on the list that raised eyebrows is Alex Jones and to
think. I wasn't gonna watch the trial. I do believe I'll reconsider. If you are
not familiar with this person, I'm not sure that we've covered him a lot on the
channel. This is a conspiracy-like guru. A big name when it comes to a lot of
unusual theories. I guess as I understand it, Jones's testimony will have to do
with activities related to the sixth and I guess he's kind of paled around
with him that day as I understand it at this point and that's what the testimony
will be about. I personally cannot wait to see him testify. That's something else. That's going
to be a unique experience. He has testified before in civil trials. I feel like this might be a little
bit different and I feel like his demeanor and maybe his presentation might be a little different.
And I have to admit, that is not a name I expected to see on any witness list.
While entertaining, it also goes to show how, let's just say, pervasive some of these theories
are and how close they get to the circles and the levers of power.
There are a lot of times when people question, you know, why a lot of commentators, myself
included, spend a lot of time, maybe too much time at times, countering just bad information
that most people would know is just bad information. This should be the answer
from now until forever. The fact that Alex Jones is apparently going to be a
needed witness in a criminal case that is part of a sprawling case involving
Having the former president should tell people that while these theories are at times amusing
and you think back to the days of National Enquirer and stuff like that, there are a
lot of people that believe them earnestly and there are a lot of people who are in positions
of power who associate with people like Alex Jones. As, I don't know, annoying maybe, as
it might get at times to try to counter some of this information, this definitely shows
the necessity of it. All of that aside, I cannot wait to see him on the stand. Anyway,
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}