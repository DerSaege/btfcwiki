---
title: Let's talk about the House GOP rejecting Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=wxn5eUYw7ks) |
| Published | 2023/10/12 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- House GOP chose Scalise over Jordan for Speaker of the House, rejecting Trump's endorsement.
- Republicans in the US House of Representatives showed that Trump's decision-making capabilities are not to be followed.
- Jordan still remains in the House of Representatives despite not getting the nomination.
- Trump's endorsement failed to secure Jordan the nomination and may have actually helped Scalise.
- The House GOP's rejection of Trump's leadership abilities is a significant message.
- Trump's dream of returning to the White House is turning into a nightmare.
- More revelations and obstructions are making things difficult for Trump.
- Social media engagement does not always represent true support, as seen in this political decision.
- The voting booth and closed doors can reveal different truths compared to social media.
- This event is a bad sign for Trump and Trumpism.

### Quotes

- "House GOP rejected Trump's leadership."
- "Trump's dream of heading back to the White House is definitely turning into a nightmare."
- "This was a really bad sign for Trump and Trumpism."

### Oneliner

House GOP rejects Trump's leadership by choosing Scalise over Jordan, signaling a nightmare for Trump's political ambitions.

### Audience

Republicans

### On-the-ground actions from transcript

- Acknowledge and understand the significance of the House GOP's rejection of Trump's leadership (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the implications of the House GOP's decision on Trump's leadership and political future.


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we are going to talk about Trump and how the house GOP just told America
what they really think about Trump and his leadership capabilities.
We talked about it when he made the endorsement.
It was a very risky proposition for Trump because if he wins, well, okay, so we all
know that House Republicans are kind of loyal to Trump, but if he loses shows that they're not.
Trump endorsed Jordan for Speaker of the House.
Jordan did not get it.
Behind closed doors with only Republicans voting, Jordan didn't get it.
Republicans in the US House of Representatives just told America that Trump's decision-making
capabilities, his ability to pick a leader, is not one to follow.
That's what just happened.
They chose Scalise.
It's worth noting we still don't know whether or not Scalise has the 217 votes necessary.
But Trump is the bigger loser there.
Jordan sure, Jordan didn't get the nomination, but Jordan is still Jordan in the House of
Representatives.
Trump threw his weight behind Jordan and it did not matter.
anything, it very well may have given the nomination to Scalise. The Republican
voters, the rank-and-file, they definitely need to acknowledge what just happened.
They need to understand what just happened. The House GOP rejected Trump's
leadership. They're saying he's not fit to make those decisions. They didn't follow
them. For them, Trump was just somebody they could use to rile up their base,
ride his coattails. For Trump, his dream of heading back to the White House is
definitely turning into a nightmare because as more and more adds up, as
there are more obstructions, as there are more revelations about the number of
people who actually believed in him. Things aren't looking so good. This was
just Republicans voting with Trump saying pick Jordan and Jordan already
having a significant base. Trump's endorsement was not enough to get
Jordan over the line just among Republicans. This is a prime example of
why judging some of these political capabilities and how much people truly
support them based off of social media engagement is a really bad idea because
behind closed doors, just like a whole lot of people are going to be in the voting booth.
Well, it's a little different than on Facebook.
This was a really bad sign for Trump and Trumpism.
I hope that the rank-and-file Republicans get the message that the House GOP was...
I mean, I don't see how they could have unintentionally done this.
They're sending a message.
They're rejecting Trump.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}