---
title: The Roads to your questions from this week....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Uu3dKkD6obw) |
| Published | 2023/10/12 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing a Q&A session with a variety of questions and breaking news updates.
- Shares a heartwarming story about being sent a Halloween decoration of himself as a werewolf.
- Advises on having fact-checking dialogues during Thanksgiving dinners to combat misinformation.
- Comments on a false alarm in northern Israel and the potential implications.
- Talks about why some leftists have naive foreign policy takes rooted in their beliefs in a fair world.
- Explains the lack of evidence linking recent events to planned attacks or political strategies.
- Touches on the Fermi Paradox and his belief in extraterrestrial life.
- Shares thoughts on Israel's adherence to rules of war and addresses a story involving friends.
- Recommends reliable news sources amidst sensationalism in the media.
- Responds to a viewer's query about discussing politics with a Republican girlfriend's family.
- Mentions having tattoos and offers advice on rehoming a horse for a disabled friend.

### Quotes

- "Ask her where the evidence is. If something is presented without evidence, it can be dismissed without evidence."
- "Sometimes, with some people, it is useful to do that because when they try to find the evidence they realize it's not true."
- "A lot of times their takes are bad because they're good people."
- "First time? If you're younger, what you're experiencing right now is what it was like in September of 2001."
- "Don't share footage. Don't share footage."

### Oneliner

Beau navigates various topics from fact-checking at Thanksgiving to discussing foreign policy and personal beliefs, offering insights with a touch of humor and practical advice.

### Audience

Viewers interested in diverse perspectives and practical advice.

### On-the-ground actions from transcript

- Contact local rescue organizations in Ocala to rehome the mare (suggested).
- Avoid sharing distressing footage and discourage others from doing so (implied).

### Whats missing in summary

Insights into Beau's views on unconventional warfare, the potential implications of widening conflicts, and his cautious approach to discussing politics with differing perspectives.

### Tags

#FactChecking #ForeignPolicy #Sensationalism #Relationships #Tattoos #CommunityAid


## Transcript
Well, howdy there, internet people.
It's Beau again, and welcome to another episode
of the Roads with Beau.
This is a Q&A.
We've had a whole bunch of questions come in this week
that probably would have been videos
over on the main channel,
but there's also a lot of breaking news,
and we just don't have enough slots for all of them.
So we're going to go through a bunch of them now,
And I'll give you as good an answer as I can give with no preparation whatsoever.
So we'll go through it. But before we do that, since y'all know about the whole incident with me being attacked by the
Halloween decoration, the werewolf, I have to show you something that somebody sent.  That's me, look at the hat, that's
me as a werewolf.  I found that just absolutely adorable.
I would put it behind the, in the background, but I feel like my daughter is going to take
that and I don't know that I'm going to be able to get it back to be honest.
Okay, so moving on to the questions.
Can you give a primer on having a fact-checking discussion slash education slash demo at Thanksgiving?
It's almost that time of year again to start that series of videos, How to Ruin Thanksgiving
Dinner.
Yeah, we'll definitely put out more about that, but to answer the question now, if you're
asking this, there's obviously people at your Thanksgiving table that are going to need the
information.
be asking it otherwise. I would early on listen to something that they believe to be true that you
know isn't and guide them through the process of fact checking it. Don't just give them the source
and say look you're wrong but ask the questions. Show where a misleading narrative was created.
It showed what information was left out
or where connections were made that are not necessarily
stable connections.
And just go through it slowly and let
them see you do it in real time.
I have videos on misinformation, on how
to deal with misinformation, how to fact check,
stuff like that, just tons.
There's a playlist on the main channel
called Information Consumption that
has a whole bunch of tips.
And I think there's one on this channel that
goes through a whole bunch of them all at once.
But sometimes the easiest way for people who really get it
is to have it done right in front of them about something
that they actually believe.
So just listen to them when they start talking about whatever
and go through it.
OK, next question.
What do you think about the false alarm
of AD in northern Israel?
Can you imagine how terrifying that was only
to find out it was nothing?
Yeah, you're looking at that from the wrong side.
OK, so if you missed the news, in northern Israel,
a whole bunch of censors went off
saying that there were things in the air,
there was incoming stuff in the air. And it turned out to be nothing. My guess is that they were
making their sensors more sensitive and they wound up picking up a bunch of birds or something.
But there was a decent amount of time where it seemed like things were about to pop off.
And yes, if you were the person looking at that screen, that would be unnerving.
but I want you to picture being the group to the north of Israel and knowing that you didn't do
anything and seeing all of that stuff pop up on your newsfeed because your side is not ready
because you didn't do anything and you know that Israel is well they're having a moment
and they're gonna respond. And I imagine it was probably more terrifying for the people who would
have been blamed for it. But yeah, it turned out to be nothing so far. Except for, and there's a few
YouTubers listed leftists have just awful takes on foreign policy why?
I'm assuming you're talking about actual leftists, not just the American left, but
I mean leftists.
The answer is it's in the ideology.
What do leftists want, generally speaking?
You're talking about a decentralization of power.
You're talking about money not meaning everything, it not being the end all be all.
You're talking about a fair world.
Does any of that sound like foreign policy?
No right?
Because foreign policy is about one thing and one thing only, power.
The reason a lot of leftists have, I don't know that they're awful, they're maybe naive
foreign policy takes is because they're good people.
They have a hard time taking the world they want, the world they know could exist, and
squaring it with all of this.
So a lot of their foreign policy takes reflect that, and it gets even more infuriating when
you realize that most leftists, they could detail every dirty deed that has been done
in the name of foreign policy for the last 75 years.
They can tell you what powers did what, what the leadership was, what they were hoping
to obtain and they have a hard time taking that information that they have and putting
it into the context of the modern world.
Their takes are based on what they wish the world was and they have a hard time squaring
it with how bad it really is.
a lot of times their takes are bad because they're good people. I think the
world would be a better place if nobody could understand foreign policy as it
exists today. I don't necessarily see it as a criticism of them and it happens a
lot and you even have them, you will see moments where stating what is going to happen based
on the fact that it's about power and they think you're advocating for it.
That happened recently to me and I'm like, that's actually not what I was saying, but
it is what it is.
I wouldn't go too hard on them because most times, not always, but most times, that comes
from a place of them genuinely being unable to square the world that could exist with
what's here.
Sometimes it's just sloganeering, but a lot of times it's based on being a good person.
You said they must have been planning this coordinated attack for a long time.
Can you estimate how long it would take to plan an attack like that?
Do we know when Biden would have told Iran he was going to allow South Korea to release
Iran's money to Qatar, Qatar for Americans, months and months.
At the end of it, we're going to find out those two things aren't related.
In fact, the most recent intelligence says that the government in Iran was surprised
by what happened.
Now to be clear, my understanding is that is about the actual government.
Keep in mind Iranian intelligence doesn't always tell the government everything.
So you can't rule out that Iranian intelligence had something to do with it.
But it doesn't look like it was something that was sanctioned by the government or that
that they really even knew that it was going to happen.
So the idea that there's a link between that and the money, the more information that comes
out the further that gets from reality.
You haven't done a video on how Putin is behind the recent events because I don't know that.
I don't know that.
I try to only present what I know.
What I know.
And if I'm talking about a hunch, like in the last question, you can't rule this out.
Deep down, I'm 99% sure that Iranian intelligence helped train them, okay?
But that's not something I would present because I can't say that as fact.
I cannot say for fact that Putin is deploying chaos agent strategy type stuff.
I can't say that.
It's a possibility, but yeah, I don't have the evidence to back that up.
I'm watching people I know to be good people call for the extermination of people.
I never knew glass was a verb before this week.
Ah.
First time?
If you're younger, what you're experiencing right now
is what it was like in September of 2001.
A whole bunch of people who you know to be good people
saying things that betray the darker animal underneath.
Six months, two years from now, they'll pretend like they never said it.
It's something that happens after an event like this, especially if there's a lot of
coverage, especially if it is about a politically divisive issue to begin with, yeah.
There's no question in that, but it's not the first time it's happened.
When you say expand or widen, do you mean into a regional conflict or a wider world
conflict in which nuclear weapons and such could be on the table.
There are not a lot of scenarios in which nuclear weapons really end up on the table.
When I'm talking about it widening or expanding, I'm talking about on a regional level, there
are a lot of ways this could go sideways.
There's a lot of potential for it to expand.
In fact, the false alarm that occurred in northern Israel, I mean, it wasn't even surprising.
When the news broke and everybody thought it was real, I was not surprised.
This is why the U.S. moved the carrier group in as kind of a deterrent, saying don't let
it expand.
But I don't know how effective that's going to be.
There's a lot of ways that this can still get worse.
I have been here in Israel on a work assignment since early 2021.
There have certainly been some ups and downs in the security situation, but this last week
has been understandably more serious.
I forced myself to limit my news intake to half an hour in the morning and half an hour
in the evening to keep myself sane-ish.
I have noticed a lot of sensationalizing of the current situation with the news taking
any scrap of information and hypothesizing on what terrible things this may mean.
It isn't helpful for anyone here who wants to be informed of the actual situation.
Lately, I've just stopped looking at anything other than AP, Associated Press, for news,
and I think that has helped, but are there any other sources that you would recommend
I keep up with? No. I would do exactly what you're doing. I wouldn't change that. Not
for the time being. It is the cost of a 24 hour a day news cycle. There's a lot of sensationalism.
There's a lot about what if, what if, what if. And a lot of times they do. They get a
scrap of information and then they say well what about this and then somebody
else comes along and then builds off of the what-if and five or six hours later
they're nowhere near what actually occurred. Right now yeah I would stick to
AP for the most part because that that's not gonna end anytime soon until until
there is a resolution with the captives that's just going to keep going and it's going to
get worse to be honest.
I watch your Fifth Column channel but not your Rhodes one, well so you're not going
to see this.
Sometimes you start your podcast assuming your viewers know what you're talking about
particularly this Israel stuff.
first sentence of this podcast had me wondering what failure. I suggest a simple sentence
spelling out what the podcast is about would be helpful. Not everyone is watching all of
your products every single day. A casual viewer might start off in the dark until you finally
say something more specific.
Yeah. I try to do that. There's, you know, I'll say in case you missed the news or if
if you haven't heard, if you have an idea what I'm talking about, and do a recap normally
about 30 seconds in.
I have noticed that the bigger the news, the more important the news, the less likely I
am to do that.
I don't know why.
I'll try to work on that.
There's not a question in here, but I'll try to work on that a little bit.
You mentioned the history of unconventional warfare when talking about why they did what
they did. Can you think of a past group that committed actions on the same level?
Yeah. Yeah. And please remember, because I saw a conversation in the comments
section the other day, if you are very up to speed on this type of conflict, I do
I do not always use DOD terminology when I am talking about something.
This isn't for people who have studied that type of fighting.
The videos I put out, y'all already know that stuff.
If you've spent a lot of time studying it, you already know.
It's more for putting the concepts in the hands of people who didn't have to study
it. And when you use too technical of terminology, at times it can lead to
misunderstanding. Recently I used the term political realignment and most
people took that to mean a totally political peaceful process. That term is
It's a strategy that is used to realign an organization.
Some of the leadership is removed and basically what happens is the more militant or the more
competent commanders are removed, leaving the incompetent or those who are politically
savvy may be interested in peace.
time the organization shifts its goals and their strategy.
The joke about that term is that the realignment of some of the leadership is generally horizontal.
That term did not come across, I guess I haven't done a video about that recently, but yes,
There have been groups that have engaged in similar acts.
It's different now because the footage is out there.
It's visible.
Whereas before, it would be reported on.
You wouldn't see it.
You've made your love of space well known and I believe you stated you believe in extraterrestrial
intelligent life.
So what are your personal thoughts on the Fermi Paradox, which says that if intelligent
life in space is statistically possible, then it should be statistically impossible for
sub-life to have not contacted us when factoring in the estimated lifespan of the universe?
Well, it's funny, I did a video on this, I think it was last year's Halloween special.
I am a believer in the idea of the great filters.
I'll try to find the link to it and put it down below.
Do you think Israel will follow the rules of war?
No, no.
I know this is hard with this conflict.
Take all history out of it for a second.
take all history out of it for a second and understand that if Israel goes in on the ground
the way they have suggested they're going to, some of the people who cleaned up are
going to be some of the people who go in.
I do not expect them to follow the rules.
I don't see that as being likely.
Is the story about the two friends real?
Or is it like a parable?
It's real enough.
Realistically, the doctor, my understanding, the last time I had any contact, he was going
to the bank, not to the strip, he was going to the bank, where probably a little bit safer,
but he's also dumb enough to think he could go there and help.
There are times when I am telling a story where I will take poetic license or something,
I just read an article in ProPublica, a publication I knew nothing about until you mentioned it
in a video concerning Justice Clarence Thomas and his pricey trips with a billionaire.
I learned today about a man I didn't even know existed, but seems to be a very strong
force in seeing who does and doesn't receive a place in our courts, in his influence particularly
in who is nominated to the highest one.
I wonder if it wouldn't be worth mentioning Leonard Leo in a video.
If you did forgive me, I must have missed it.
I have not, at least not in the last couple of years, but don't worry if Pro Publica
has put out a piece on him, he will be in network news soon.
If you're not familiar with this outlet,
definitely one worth watching.
When it comes to stuff like this,
they're normally about a week ahead of the news cycle.
They're really good.
I've watched you from Perth for some time now.
Today I received a video of a distressed... I'm going to respond to that via email.
Short version, don't share footage.
Don't share footage.
There are a lot of bad things that are very freely available to watch, and I know right
now it's like a thing and you see it captioned on videos, don't look away.
As somebody who's looked at that kind of stuff my entire life, look away.
You don't need that.
Part of the reason people are using glass as a verb is because we have become very desensitized.
A lot of the footage is not something that most people should see and definitely don't
share it.
are you... you're inadvertently helping because their their goal is to get that
footage out. The other thing to keep in mind is that there are a lot of people
who do not know what happened to their loved ones and that is not how you want
to find out.
I don't understand.
You warned your audience away from the social media narrative regarding Trump's apparently
handing off of Israeli defenses to the Russians, which according to the narrative went from
the Russians to the Iranians and on over.
Also you've told your audience that the real secrets aren't the facts conveyed by intelligence
reports, but the methods and processes used to gather the intelligence.
Now in your video, let's talk about the intelligence failure, you say the Israelis missed it when
the comms traffic dropped off.
Isn't that drop off of traffic evidence that they came to understand some of Israel's
capabilities, means and methods of gathering intel, and couldn't it possibly be that those
methods or clues about them were conveyed by Trump to the Russians, to the
Iranians and on over. It's possible. It's possible. I put the information out there
that people could use to draw that conclusion, but I'm not going to say that
because I don't know that and right now in with the situation as it is it is
incredibly important to only put out what you know what you can confirm and I
cannot I have no way of confirming that the information that Trump reportedly
leaked that came from Israel that went to Russia even contained those specific
means and methods. Very well might have and it makes sense but I can't prove
that. Bo can you help me understand the danger my family in North Carolina is
in with the new government ops program? Anything I can find has a hysterical
edge to it that I don't trust. How much damage could this do before it ends up in court?
I'm going to have to look into it. I'm going to have to look into it. I have seen this pop up,
but I haven't read anything about it yet. I've had, I've been more focused on the foreign policy
stuff right now, but I will, I'll look into it. Would it be plausible to assume that Putin
Putin has indirectly asked for this to distract NATO.
Is it more likely unrelated to the Ukraine war?
Is it plausible to assume that Putin wanted this influence, maybe even asked for it?
Yeah.
can't prove that though but it does this does help in some ways it helps Russia
more than just about anybody and you always want to know who benefits at the
same time if that was the if that was what happened I have a feeling it's not
going to go the way he planned. What are you reading right now? My screen? No. I'm reading
two things. One is it's called Stuff We Forgot. Stuff We Forgot to Remember Iraq Evidence in a
memory hole, which is obviously a foreign policy thing.
And I'm reading, on this day in history, stuff went down.
It's by James Fell.
If you know who he is, you know that that didn't say stuff.
He is known as the sweary historian or something like that.
pretty cool. Let's see. Today is October 12th. It has stuff like this in it. Desmond Doss
became the first conscientious objector to be awarded the Medal of Honor for his actions
during the bloodiest battle of the Pacific Theater of World War II, and he never touched
a rifle. I'm not going to read any more than that, but. So, both of them are pretty good
so far from what I've read of them, and I've just realized that both of those are advanced
copies that were given to me as complimentary copies. I don't know if you can buy them yet.
And now that I have, I didn't mean to plug them either. I have not received any compensation for
them, blah, blah, blah, blah, but I did get the books for free. They were sent to me.
But yeah, that was not intentional.
But James Fell is, if you are somebody who likes the story, if you like your history
presented in a story fashion, you would probably like it as long as you're not bothered by
the language.
You are one of those people who really like the outtakes from this channel.
You would probably really like his stuff.
And then the other one is, it has to do with a lot of information that was public knowledge
the early 2000s, but it does seem like we've kind of forgotten about it.
I have a Republican girlfriend.
Whenever politics comes up, it's because she sends me a meme about Biden giving money
away to Ukraine or whatever.
I don't know if you're skipping over that part, there's a lot of personal details there.
Oftentimes the articles I find she is referring to are conspiratorial and they force me to
play defense and justify why I believe in the criminal justice system and don't come
to conclusions until trial with a jury or why I support Ukraine.
Whenever politics comes up, I carefully use Republican sources, Trump's Attorney General,
Trump's testimony, Trumpy House Republicans, Trump's FBI Director, so on and so forth.
Yet still the conversations always end up dropping with no conclusions and no one is
meeting each other halfway.
In my experience, this is just the nature of talking to 80% of Republicans.
Her whole family is Republican, I bet her friends are too.
Is there ever going to be a time when discussing politics isn't so alienating between us?
Is there anything else I can do to argue against the mega-strategy of throw a ton of conspiracy
theories at a lib and then roll your eyes when they try to explain them all?
Am I doing the right thing, and is this something that'll just take time and require patience?
I take pride in having a relationship across the aisle.
Disagreement is what this country was founded on.
But sometimes I don't feel like we disagree.
I feel like we don't respect each other's opinions, which is different."
Okay, I'm just going to answer all the questions that are here.
Is there ever going to be a time when discussing politics isn't so alienating between us?
I don't know.
Is there anything else I can do?
Yeah.
You go on the defensive.
Why?
They're conspiratorial?
Ask her where the evidence is.
If something is presented without evidence, it can be dismissed without evidence.
Sometimes, with some people, it is useful to do that because when they try to find the
evidence they realize it's not true.
There's a lot more I want to say here, but those were not the questions asked.
So I'm going to move along.
Do you have any tattoos?
If so, where and what are they?
Do you have any tattoos?
Yes.
Where and what are they?
No.
Yeah, I do have tattoos.
are not visible.
And I'm going to leave that at that.
Just a quick question.
I have a disabled friend in Oklawaha area that needs to rehome a mare.
If you know of a local trusted source that does rescue, I can provide her with details
and contact information.
I'm up here in a different place.
So I'm not sure what's available.
I would look at places in Ocala.
I would look at places in Ocala.
We don't have any space.
We do not have any space at the moment for an additional horse.
But I would look at the area around Ocala.
I feel like somebody else just asked me
a very similar question, or maybe it was you.
But yeah, I would look there and find out a little bit about the mayor.
Find out how merry the mayor is.
That would help if there was, the more information I had with that, the more I would be able
to say, oh, this person might come get them and find a good home for them.
But other than that, Ocala's probably your best bet.
There's probably some kind of network there that does this.
There's a very large horse community there.
Okay, and that's it.
Those are all of the questions.
And I hope that helped.
For the person I told I would respond via email, I will do that in the next day or so.
I would honestly avoid footage like that if you can.
So that's everything and I guess that's it.
Anyway, it's just a thought.
Y'all have a good night.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}