---
title: Let's talk about Trump being worried about another candidate....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=b7-Myka2raA) |
| Published | 2023/10/09 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's team is concerned about another candidate, Robert F. Kennedy Jr., potentially taking more votes from Trump than Biden.
- Right-wing commentators have given airtime to Kennedy, hoping to elevate his candidacy within the Democratic party.
- Kennedy's controversial and conspiratorial views have endeared him to elements supporting Trump, making him an alternative to Trump.
- Trump's circle is preparing a campaign to go after Kennedy to reduce the harm he may cause to Trump's base.
- The right-wing commentators wanted Kennedy to run as an independent and pull votes from Biden, but it seems to have backfired.
- Kennedy's fringe views aren't resonating with Democrats but are with the right wing, causing a unique situation for Trump.
- Trump is in a predicament because someone else is influencing his easily influenced base.
- Trump may have to attack Kennedy's more liberal views rather than his fringe ideas to avoid alienating voters.
- The situation of Trump going after Kennedy is a unique and interesting development in American politics.

### Quotes

- "Trump's team is concerned about another candidate, Robert F. Kennedy Jr., potentially taking more votes from Trump than Biden."
- "Trump may have to attack Kennedy's more liberal views rather than his fringe ideas to avoid alienating voters."
- "Seeing Trump go after Kennedy, it's going to be interesting because I have a feeling that the only route he can go is to attack some of Kennedy's more liberal views and point those out."

### Oneliner

Trump's team fears Robert F. Kennedy Jr. may draw more votes from Trump than Biden, causing a unique situation requiring Trump to navigate carefully to avoid alienating voters.

### Audience

Political strategists, voters

### On-the-ground actions from transcript

- Monitor the developments in American politics regarding the potential impact of Robert F. Kennedy Jr. on the upcoming elections (implied)

### Whats missing in summary

Insights into the potential implications of Robert F. Kennedy Jr.'s candidacy on the political landscape and voter behavior.

### Tags

#RobertFKennedyJr #Trump #Elections #PoliticalStrategy #VoterBehavior


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about a concern that has arisen in Trump
world and what they plan on doing about it.
See, there's another candidate that they're concerned about, and it's not Biden.
A lot of right-wing commentators, they gave air to a political candidate hoping to elevate
that candidate on the Democratic side because he is running as a Democrat, at least for
the time being, I think, still running as a Democrat.
R.F.K.
Jr., according to reports, internal polling from Trump's team shows him taking more votes
away from Trump than from Biden.
So he may end up being a spoiler for Trump.
When the commentators brought him on their shows, they asked him mostly about his more,
let's just call them controversial views.
Some would call them conspiratorial.
This endeared him to a lot of the elements that support Trump.
So they see him as an alternative to Trump.
More importantly, they see him as an alternative to Trump who hasn't already lost to Biden.
And they believe that he will pull votes from the other side of the aisle, which may actually
be the case.
He may pull some votes from Biden as well.
that according to Trump world, he's going to pull more from Trump than from Biden.
So the reporting says that Trump's circle is preparing a campaign to go after RFK Jr.
to try to mitigate and reduce the harm he does to Trump's base.
That's an interesting development, considering I would imagine that most of those right-wing
commentators who brought him on did so in hopes that he would run as an independent,
which looks like it may be the case, but that he would pull more votes from Biden.
It seems to have backfired, at least for the moment, but the fringe views that have been
expressed, they're not really resonating with the base of Democrats.
However, because the right wing was exposed to them, those fringe
views are resonating with them.
So Trump, who built a base of people who, for the most part, can be easily influenced.
He has a problem because somebody else is influencing them.
It's a unique situation.
I can't think of a parallel in American politics and I have no idea how it's going to work
out, but seeing Trump go after Kennedy, it's going to be interesting because I have a feeling
that the only route he can go is to attack some of Kennedy's more liberal views and point
those out.
Because if he goes after the fringe ideas, well, he'll still alienate those voters.
So that's something to be on the lookout for, a unique and interesting development in American
politics.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}