---
title: Let's talk about Wisconsin, judges, and impeachment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=oB7PZ6LTAdc) |
| Published | 2023/10/09 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Justice Protasewicz's election shifted the Wisconsin Supreme Court to a more liberal majority.
- There is a challenge to the unfair voting maps in Wisconsin, with a 4-3 Supreme Court decision to hear the case.
- Republicans want Protasewicz to recuse herself, alleging bias due to her campaign stance on unfair maps.
- Despite pressure to recuse, Protasewicz has refused, citing that her decisions are bound by law, not personal preference.
- Republicans have threatened to impeach Protasewicz for not recusing herself, fearing fair maps will shift power balance.
- The extreme measures by Republicans to impeach a Supreme Court Justice showcase their authoritarian tactics.
- Fair maps threaten Republican control, leading them to politicize impeachment and disregard the will of the people.
- Republicans are willing to impeach to maintain power, showing a lack of concern for the democratic process in Wisconsin.
- Protasewicz's identification of unfair maps could lead to their striking down, a move feared by the Republican Party.
- The situation in Wisconsin exemplifies the high stakes involved in gerrymandering and power dynamics within the state.

### Quotes
- "During the next election, the voters of Wisconsin will make them pay for it."
- "If you have fair maps, that judge, she might give the state fair maps. We got to impeach her."
- "They're not hiding the reason they want to impeach her either."

### Oneliner
Justice Protasewicz's refusal to recuse herself from a case involving unfair voting maps in Wisconsin sparks Republican threats of impeachment and exposes authoritarian tactics to maintain power.

### Audience
Wisconsin voters

### On-the-ground actions from transcript
- Pay attention to local elections and hold representatives accountable for their actions (implied)

### Whats missing in summary
The full transcript gives a detailed insight into the power struggles and authoritarian tactics surrounding voting maps in Wisconsin.

### Tags
#Wisconsin #SupremeCourt #Gerrymandering #PowerStruggle #Impeachment


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about Wisconsin
and the Supreme Court and MAPS.
And once again, we will be talking
about Justice Protasewicz.
We're gonna do this because all of the previous coverage
was saying, hey, something's coming.
Well, it's here.
Okay, so very quick recap
for those who have missed previous coverage.
Protasewicz was just elected
the Supreme Court, it shifted the balance of power to a more liberal majority.
There is a challenge to the maps, the voting maps in Wisconsin, and the Supreme Court decided
to hear it in a 4-3 decision.
Republicans want Protozeewicz to accuse herself.
Why?
Obviously, she did something really shady, right?
No, of course not.
her campaign. She said the maps were unfair. Okay, well, that's probably because they're unfair.
If you listen to any organization that looks at gerrymandering, or if you just go to Google and type in
most gerrymandered state, the first word you're going to see is Wisconsin.
Apparently, Republicans want her to recuse herself because she possesses the ability
to identify an objective fact. That's like saying, hey that thing over there,
that's a book. It is what it is. She has declined their request to recuse herself.
They were like, you're still gonna sit on this case? And she was like, you betcha.
What she actually said was, recusal decisions are controlled by the law.
They are not a matter of personal preference. If precedent requires it, I
must recuse. But if precedent does not warrant recusal, my oath binds me to
participate. So it does not look like she's going to recuse. Republicans have
threatened to impeach her. Now there's two things about this. First, I feel like if
they do it, and they very well might, the voters of Wisconsin will make them pay
for it. During the next election, the voters of Wisconsin will make them pay
for it. It would be wise of the Republican Party up there to understand
that, yes, during her campaign she said the maps were unfair. That might have had
something to do with her getting elected. And if the Republican Party acts in
concert and politicizes impeachment to disrupt that, I imagine they're gonna have a whole
lot of upset people.
That's one thing.
The other thing and the thing that might be more important is to understand these maps
shift the balance of power unfairly in Wisconsin so much that the Republican party is willing
to go to these extremes will impeach a Supreme Court justice because it'll be devastating
to our case.
I want you to think about the machinery there.
They know that if it is seen by a judge who understands what gerrymandering is, that the
maps will probably get struck down. Now it is worth noting that, to my knowledge,
at no point in time did Proto-Sawitz say what she thought should happen. She just
identified the same thing that tons of organizations have. But their assumption
is that she would strike down the maps and that if there were fair maps, well
they would lose their grip on power. And it is so important to them they're
willing to impeach somebody for not recusing themselves. I'm not sure that
that really uh that that's really an impeachable offense but I guess we'll
have to see. The people in Wisconsin need to pay attention to this. We talk
about how the Republican Party has become more authoritarian, how they want
to rule rather than represent. This is a perfect example. If you have fair maps,
that judge, she might give the state fair maps. We got to impeach her. That
sure seems like somebody who doesn't actually care about the will of the
people. Doesn't care what the people of Wisconsin want for their state. They're
going to overrule the voters and just tell them, well you better just do what
you're told. Say thank you for it because you don't need a voice. That's how
important those maps are to the Republican Party. This is all on full
display. They're not hiding the reason they want to impeach her either. I feel
like this is probably not going to go the way they think, but the chain of
events that we have been talking about saying, hey this is an important race,
this is going to have downstream effects they're here and that's we'll probably
hear some more about this next week anyway it's just a thought y'all have a
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}