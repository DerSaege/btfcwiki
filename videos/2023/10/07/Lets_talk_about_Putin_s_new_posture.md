---
title: Let's talk about Putin's new posture....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=M0PxyJcQ_iI) |
| Published | 2023/10/07 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Putin's boasts about a new super rocket are met with skepticism by NGO and intelligence assessments.
- Russia's potential withdrawal from a test ban treaty and resuming nuclear testing is concerning.
- Russia's shift in foreign policy strategy from a near-peer to a stance resembling North Korea is alarming.
- The change in Russia's position indicates a lack of security and technological advancement.
- The rhetoric of flaunting missiles is a sign of fear rather than strength.
- Comparisons are drawn between Russia's current stance and North Korea's approach due to a lost force projection capability.
- Beau doesn't see immediate concern but notes the shift towards nuclear rhetoric as a fallback for Russia.

### Quotes

- "Russia is having to fall back on nuclear rhetoric."
- "That's not something world powers do because they don't need to."
- "The real takeaway isn't what he said about wonder weapons or pulling out of a treaty."
- "This is not rhetoric you would expect from somebody who believes they are going to win the elective war they started."
- "I know it's unnerving."

### Oneliner

Beau outlines Putin's questionable missile claims, Russia's concerning foreign policy shift towards nuclear rhetoric, and the underlying fear driving these actions.

### Audience

Global citizens

### On-the-ground actions from transcript

- Monitor international relations and Russia's foreign policy stance (implied).
- Advocate for diplomatic solutions and nuclear disarmament (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of Russia's changing foreign policy dynamics and the implications of Putin's recent announcements.

### Tags

#Russia #Putin #ForeignPolicy #NuclearWeapons #Diplomacy


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Putin's new posture
and what it means, what was said, and what the reality is.
And then we're going to talk about the foreign policy
strategy that Russia seems to be shifting to.
So if you missed the news, Putin has come out and said,
You know, we have this new super rocket, this missile that's amazing, it's great, we've
tested it, it's the most wonderful thing in the world, you can never stop it, blah, blah,
blah, blah, blah.
It's worth noting that the NGO and intelligence assessments, they all pretty much have a consensus
on this, and it doesn't work.
They've been working on it for years, and it actually doesn't really function.
So it's just more, it's more saber rattling from Putin.
The other thing that is of note is that he has indicated that, well, they may withdraw
from the test ban treaty.
They may start testing their nukes again.
Well okay, I mean I would prefer they not do that.
that is a little a little bit of a step but you need to think about the the real
message it's sending. Russia was a country that not too long ago was viewed
as a potential near-peer. This was a country that theoretically and
And realistically, had Putin not squandered the youth of his country,
theoretically, it could have rivaled the US and China.
A multipolar Cold War.
Go back three or four years, and that's what you were hearing,
because people expected Russia to behave in a certain way.
They expected Russia to be a role player and meet the United States and China as equals.
That means that they would have to behave in a similar fashion.
Not identically, but in the way China is starting to experiment with soft power more and really
come into its own there and more on a diplomatic and monetary scene, then we've got missiles.
Over the last year or so, their position has gone from being seen as a near peer or a potential
near peer to basically adopting the foreign policy stance of North Korea.
We have missiles and we're going to test them.
This is not a position of a country that is secure in its position.
This is not rhetoric you would expect from somebody who believes they are going to win
the elective war they started.
It's certainly unnerving.
It's not something you want to hear.
likes to hear about the potential use or test of strategic arms, but I would remind everybody
what a lot of people are experiencing for the first time.
That was just life prior to the end of the Cold War.
And the real message that's being sent by this, it's not one of strength.
It's not one of a country that is powerful and technologically advanced.
It's one of a country that is scared.
The United States doesn't often talk about its nuclear deterrent.
It's not something that comes up.
China really doesn't either.
You know why?
they have a functioning military. They don't have to use nukes. They don't have to threaten to use nukes.
They have force projection capabilities that are functional. Russia has lost that. So, now the rhetoric is very similar
to that of North Korea. That's how far Russia has fallen during this period.
I know it's unnerving. I wouldn't personally worry about it too much, you know, easy for
me to say living in the middle of nowhere, but I don't see this as an immediate concern
or something you should lose sleep over.
The real takeaway isn't what he said about wonder weapons or pulling out of a treaty.
It's really that Russia is having to fall back on nuclear rhetoric.
That's not something world powers do because they don't need to.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}