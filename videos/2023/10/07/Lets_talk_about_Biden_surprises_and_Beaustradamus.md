---
title: Let's talk about Biden, surprises, and Beaustradamus....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=535OYEAZMmQ) |
| Published | 2023/10/07 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing Biden, surprises, and the wall, hinting at news and a message received.
- Criticism for appearing biased and hinting at dark "Brandon" memes.
- Revealing the difference in design between Biden's and Trump's walls, focusing on portability and height.
- Mentioning suspicion before Biden's presidency due to his reputation for "malicious compliance."
- Exploring the possibility of the portable wall being redeployed elsewhere in the future.
- Acknowledging the environmental impact and limitations of the wall, despite efforts to mitigate harm.
- Emphasizing the uncertainty surrounding the reasons for making the wall portable.
- Expressing a commitment to accuracy in statements and not speculating without confirmation.
- Defending the practice of hinting at information rather than outright stating it.
- Concluding with a reflection on journalistic standards and the importance of accuracy and informed speculation.

### Quotes

- "It seemed like you suddenly believed the dark Brandon memes."
- "Walls are definitely not monuments to the stupidity of man."
- "Sometimes past performance does predict future results."
- "If I ever know something, I'll tell you."
- "It's not fortune-telling, it's reading a lot and listening to people."

### Oneliner

Beau addresses Biden, surprises, and the wall, hinting at news, critiquing assumptions, and exploring the portable design differences, all while maintaining a commitment to accuracy and journalistic integrity.

### Audience

Journalists, political analysts

### On-the-ground actions from transcript

- Contact organizations working on environmental conservation to raise awareness about the impact of border walls (implied).
- Stay informed about political developments and policies affecting border security and immigration (generated).

### Whats missing in summary

The full transcript provides a detailed insight into Beau's thought process and approach to sharing information, offering a deeper understanding of his commitment to accuracy and informed speculation.

### Tags

#Biden #BorderWall #Journalism #Accuracy #Speculation


## Transcript
Well, howdy there, internet people, let's bow again.
So today we are going to, uh, talk about Biden and surprises and the wall and
Bostro Domus, um, because I guess some news has come out and we'll talk about
it because I got a message, I watched your wall video in it, you seem to hint
that Biden or quote, agencies within the administration might have a surprise for the GOP plans.
I saw nothing but cope in your video.
It kind of hurt because you've always been so objective.
It seemed like you suddenly believed the dark Brandon memes.
I just heard about the wall design.
If you knew when you made that video, why not just say it?
You obviously knew they had a trick up their sleeve and just didn't say.
This is why people think your Bostradamus is their more coming.
So apparently that news broke.
Biden's wall is different than Trump's.
And I don't mean that in some partisan sense that says, oh, he's doing it for a different
reason, his hands are tied or something like that.
I mean, no, literally the design is different.
If you were picturing a continuation of that 30 foot monstrosity that's falling apart,
Well, that's not what's being built.
The key parts, without getting too far into it,
is it's just a little bit more than half as tall.
And the key word in the description would be portable.
Now, on to the rest of that message. If I knew, I didn't know, I suspected. Long before President
Biden was President Biden, he was Senator Biden. And Senator Biden had a reputation for malicious
compliance. Just saying. I also saw something about Border Patrol looking at
portable walls put two and two together. I thought, I suspected, I did not know.
That's why I hinted and did not say. I try to only say things that I know to be
accurate. And is there more? Maybe. Maybe. There might be more. Now, here's something
that's important to note about this. Yeah, it's portable. Now, we don't know that the
Biden administration plans on using that capability later, but the wall is portable. From my understanding,
Again, I haven't seen this officially confirmed yet, but I have seen people report on it now
that it's portable.
So maybe it gets redeployed somewhere else later, and that's a complete possibility.
But the thing to remember is that the entire time this thing is up, it will stop the migration
of animals.
will hurt the environment. It will stop everything except what the people who
designed it wanted to. It's still not good. Don't get me wrong, okay? Don't
misread that. I am, I'm fond of finding ways to mitigate harm and maybe this is
what they were planning. Maybe that's the reason they did it this way. Keep in
mind, we don't actually know that that's why they've made it portable. We don't
know that. It's a relatively safe assumption, but we don't know that, which
Which is why I wouldn't say that.
But there is a possibility that the requirements will be fulfilled.
It will be constructed in that area.
It just happens to be constructed in a way that makes it portable.
that's to help, you know, do other things later with it to make it super secure
because walls are definitely not monuments to the stupidity of man. Now
as far as the Bostra Dhamma thing, yeah there's a lot of times when I think
something or I have clues that suggest something or I have one source that
says something I hint to it I don't say it because I like being right I don't
want those all of those guidelines that journalists use it it's not just random
ethical standards it makes you more accurate that's that's why I still use
them yeah it's not fortune-telling it's reading a lot and listening to people
And sometimes past performance does predict future results.
I didn't know.
I suspected.
If I ever know something, I'll tell you.
Anyway, it's just a thought.
So have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}