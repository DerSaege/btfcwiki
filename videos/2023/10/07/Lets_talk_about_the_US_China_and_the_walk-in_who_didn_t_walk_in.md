---
title: Let's talk about the US, China, and the walk-in who didn't walk in....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=7l6qHOQYYfo) |
| Published | 2023/10/07 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces the topic of foreign policy intrigue between the United States and China, focusing on a former army sergeant named Joseph Schmidt.
- Schmidt allegedly went to great lengths to try to get himself recruited by Chinese intelligence after separating from the army in early 2020.
- Schmidt's unconventional approach involved sending an email expressing his desire to move to China, share information as an interrogator, and meet in person to talk about it.
- Despite concerns about discussing sensitive information over email, Schmidt continued his attempts, including searching for information on treason extradition and creating a document for the Chinese government stored in the cloud.
- The recruitment attempt lasted for some time, but it remains unclear if Schmidt ever made contact with Chinese intelligence or had a meeting.
- Upon returning to San Francisco, Schmidt was arrested by the federal government on charges of retention and attempting to deliver, suggesting he may not have successfully made contact with Chinese intelligence.
- Beau humorously speculates on potential future books and a spy comedy made in China about Schmidt's failed recruitment attempts.
- Beau questions the effectiveness of the US Army's intelligence training program, suggesting that more guidance could have prevented Schmidt's actions.

### Quotes

- "I am a United States citizen looking to move to China."
- "I have a current top secret clearance and would like to talk to someone from the government to share this information with you if that is possible."
- "Please contact me at your earliest convenience. I can set up a time to meet with you."

### Oneliner

Former army sergeant attempts unconventional recruitment by Chinese intelligence, leading to his arrest upon returning to the US, raising questions about intelligence training programs.

### Audience

Foreign policy observers

### On-the-ground actions from transcript

- Contact relevant authorities if you suspect any suspicious activities related to espionage (implied)

### Whats missing in summary

Insight into the potential consequences of failed espionage attempts.

### Tags

#ForeignPolicy #Espionage #USChinaRelations #IntelligenceTraining #Arrest


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about the United States
and China and a little bit of foreign policy intrigue,
something that doesn't happen often
but does occasionally occur.
Although I've never seen it or heard of it occur
in quite this fashion.
So today, we will talk about the walk-in
who apparently couldn't walk in.
There's a former army sergeant named Joseph Schmidt,
intelligence.
And according to the reporting and allegations,
after he separated, he went to great lengths
to try to get himself recruited by Chinese intelligence.
This started in early 2020, according to the allegations.
Now, walk-ins, a walk-in agent, it happens.
It's not routine, but it occurs.
A walk-in is somebody who presents themselves
at an embassy or a consulate,
or sometimes even an intelligence agency headquarters,
And it's just like, hey, I'd like to spy for you.
I'm disaffected.
Normally, it's a little bit different than that.
But as the name suggests, the first step in this process
is walking in.
Schmidt apparently had other plans.
He sent an email, apparently.
I'm going to read selections here.
I am a United States citizen looking to move to China.
I currently reside in Istanbul
and am trying to set up an appointment
at the consulate in Istanbul.
I also am trying to share information I learned
during my career as an interrogator
with the Chinese government.
I have a current top secret clearance
and would like to talk to someone from the government
to share this information with you if that is possible."
And he goes on, I would like to go over the details with you in person, if possible.
As I am concerned with discussing this over email, given the fact that I'm reading it
to you right now, it's a safe bet that those concerns were well founded.
I'm sorry for using English, but I want to make sure that I do not miscommunicate.
Please contact me at your earliest convenience.
I can set up a time to meet with you.
Thank you, Joe Schmidt."
So this was the first attempt of Minnie.
Along the way, he tried several different routes according to the allegations.
At one point, even apparently offering him a sipper card.
During this period, he was also using search engines, and one of the things he searched
for was, can you be extradited for treason?
He also, according to the allegations, created a document, important information to share
with Chinese government and he stored it on the cloud.
The attempt to be recruited went on for quite some time.
It is unclear at this point if he ever actually was able to make contact with Chinese intelligence
and have any kind of meeting. That is not disclosed as of yet, but as one
might expect when he flew back to San Francisco after repeatedly allegedly
trying to get himself recruited, the federal government arrested him. He is
reportedly charged with retention, obviously, and I believe attempting to
deliver. The fact that it says attempting to deliver indicates that that he may
never have actually walked in. That's a story. Books will be written about
this from both sides. There's probably going to be a spy comedy made in China
about this guy who they obviously blew off repeatedly because they didn't
believe him. Who was for real apparently. So there's your weird news to start off
the weekend with I guess. I feel like, I feel like maybe the US Army's intelligence
training program could use some work. This was somebody who was in intelligence, I think
in human intelligence.
It seems like training should have covered that this wasn't a good idea.
Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}