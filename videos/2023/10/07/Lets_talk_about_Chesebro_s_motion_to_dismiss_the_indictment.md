---
title: Let's talk about Chesebro's motion to dismiss the indictment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=PK44AjqHsBg) |
| Published | 2023/10/07 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about a motion to dismiss the indictment in the Georgia case involving Cheese Bro.
- Describes the grounds for the motion to dismiss, focusing on an issue with the special ADA Wade not properly filing the oath of office.
- Mentions speaking with lawyers who believed the motion to dismiss was a stretch, referring to it as "grasping at straws."
- The judge, after examining the situation, denied the motion to dismiss.
- Indicates that Cheese Bro and Powell are likely headed to trial in two weeks.
- Suggests that as trial dates approach, similar motions may arise, particularly in relation to Trump's appearances.
- Notes the judge's reaction to the failed motion and advises against attempting novel legal arguments.
- Concludes by mentioning that the trial in Georgia is imminent and humorously suggests having popcorn ready.

### Quotes

- "The judge was a little bit more direct."
- "It doesn't seem like the judge is appreciative of that."
- "So in about two weeks in Georgia, the show starts."

### Oneliner

Beau talks about a failed motion to dismiss in the Georgia case involving Cheese Bro, leading to an upcoming trial and a caution against novel legal arguments.

### Audience

Legal enthusiasts

### On-the-ground actions from transcript

- Stay updated on legal proceedings (implied)
- Follow cases of public interest (implied)

### Whats missing in summary

Analysis of the potential implications of the upcoming trial and the broader context of legal strategies in high-profile cases.

### Tags

#LegalCase #Georgia #MotionToDismiss #Trial #LegalStrategy


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about Cheese Bro
and the Georgia case and the motion to dismiss.
The day before yesterday, over on the other channel,
the Roads with Beau, we talked about a motion
that was filed and it sought the dismissal
of the indictment, like all of it,
based basically on the grounds that the special ADA Wade had not properly
filled it out but didn't file the oath of office properly and that this was somehow being viewed
as grounds to dismiss the entire indictment. I said then that I had talked to a couple of lawyers,
one of which kind of up to speed on Georgia, and they were like, no, that's not going to
happen. They used the term grasping at straws. The judge was a little bit more direct.
After going through de facto officer theory, which is based in Georgia, if you're in the office,
you're in the office and explaining to them how they never even demonstrated that this code
applied to Special ADA Wade. The judge went on to say, and if this parrot of emotion is somehow
not yet dead, the defendant has failed to establish how Special ADA Wade's actions resulted in
prejudice, how his assignment single-handedly changed any specific actions taken during the
investigation or resulted in the true bill of indictment. Nor has defendant established
a constitutional violation or structural defect in the grand jury process sufficient
to justify outright dismissal. This motion is denied."
Yeah, that didn't go so well, even just reading what was said, the judge did not seem happy.
I didn't listen or watch, but just the language used, the judge seemed kind of irritated
emotion just in general. I think it might have been a a little bit of an
overreach to hope for a total dismissal of the indictment based on improperly
filing an oath of office. So where does that leave us? It certainly appears that
that Cheesepro and Powell will be headed to trial in like two weeks.
These dates, they're getting closer and closer and the motions are becoming more like this.
You can expect the same type of thing to play out as the dates for Trump's appearances get
closer.
will start to see more, well, let's just see if it works type of motions. Based on
the judge's reaction to this, that might not be a good idea. It may not be a wise
decision to try a, let's just call it a novel legal argument. It doesn't seem
like the judge is appreciative of that. But as it stands right now, I don't really see
anything else that is going to derail it. So in about two weeks in Georgia, the show
starts. So make sure you have your popcorn laid on. Anyway, it's just a thought. Y'all
So have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}