---
title: Let's talk about the US House, pledges, and parties....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=MtHvVngzv-g) |
| Published | 2023/10/22 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Republican Party is struggling to choose a speaker, starting fresh after previous attempts.
- People aspiring to be the speaker are seeking allies ahead of a meeting on Monday to designate someone.
- There's a new suggestion for a loyalty pledge for Republicans to back the chosen speaker, but there's opposition from the far-right Freedom Caucus.
- Some members are not concerned about a functioning government and may not follow through on the loyalty pledge.
- Emmer is currently the best shot for a partisan speaker, but it may be time for Republicans to reach across the aisle if this attempt fails.
- Beau suspects some members are purposely delaying the speaker selection to potentially cause a government shutdown.
- He believes that certain Republicans prioritize Twitter engagement over effective governance, aiming to cause a shutdown to then "fix" it and gain political points.
- The infighting within the Republican Party may lead to a divide between being a traditional political party or a mere sideshow.
- There's a concern that the Republican Party is becoming unserious and more focused on launching careers in other fields rather than governing effectively.

### Quotes

- "The Republican Party is kind of fighting amongst itself to determine whether or not they're going to be a political party in the traditional sense, or whether or not they're going to be a sideshow."
- "You all have a good day."

### Oneliner

The Republican Party's internal struggle over choosing a speaker reveals a potential motive for causing a government shutdown to gain political advantage, showcasing a shift towards prioritizing personal gain over effective governance.

### Audience

Political observers

### On-the-ground actions from transcript

- Reach out to Republican representatives urging them to prioritize effective governance over political gamesmanship (suggested)
- Support efforts to bridge partisan divides and encourage cooperation for the good of the country (implied)

### Whats missing in summary

Insights on the potential consequences of the Republican Party's internal struggles and governance priorities.

### Tags

#RepublicanParty #SpeakerSelection #GovernmentShutdown #PartisanPolitics #PoliticalStrategy


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about the US House of Representatives and pledges
and where it goes from here.
So if you have missed the news, the Republican Party is having some issues choosing a speaker.
Basically, they've kind of scrapped everybody
that has tried thus far, starting fresh.
This weekend, people who wish to be speaker,
why anybody would want this job, I do not know,
they're calling and trying to gather allies.
And on Monday, they're going to get together
and try to, once again, designate somebody to be the speaker.
be the speaker and then hopefully at some point next week there will be a
floor vote on it and we may or may not find out something then. A new wrinkle
has emerged. A number of Republicans in the House are suggesting that there needs
to be a loyalty pledge. That basically everybody signs this and they're agreeing to back the
whoever the speaker is, whoever the designee is that gets chosen, they agree to back that person.
There's some opposition to it unsurprisingly from the Freedom Caucus, which is the the far right
caucus. Here's the thing about this. While I understand the general idea, the real issue
kind of emerges when you realize that those people who are, let's just say, not that concerned
about having a functioning government, they don't care about the pledge.
So I wouldn't even necessarily count on them to follow through with it even if they do
sign it.
The Republican Party needs help.
Right now it looks like the best shot is Emmer if they're going to have a partisan speaker
of the House.
If it doesn't work this time, it's past time for them to try to reach across the aisle.
This is getting out of hand and I'm starting to believe that there may be some members
of the House of Representatives who are using this to run out the clock or try to run out
the clock to cause a government shutdown and this way they can say, oh, it's not
that we chose to shut it down, we're just too bad at politics to choose a
speaker. The strategy of delay, delay, delay seems to have moved from Trump
through the ranks of the Republican Party. And it certainly seems at this
point that you have a whole lot of people in the Republican Party who are
more concerned about Twitter clicks than governing. And it does seem at this point
that their intention is to cause a government shutdown, cause harm
economically to the average American, so they think Republicans will fix it. I'm
not joking. That certainly appears to be what the strategy is from a number of
them. The Republican Party is kind of fighting amongst itself to determine
whether or not they're going to be a political party in the traditional sense, or whether or not
they're going to be a sideshow.
An unserious political party really meant for launching a career in some other field.
That's how it's starting to look, and that's how it's starting to devolve.
Anyway, it's just a thought.
You all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}