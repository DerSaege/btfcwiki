---
title: Let's talk about West, Harlan, and a maximum donation....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=b6VwCEYBZ7A) |
| Published | 2023/10/22 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- GOP mega donor Harlan Crow gave the max donation to noted progressive Cornell West for his independent third-party presidential campaign.
- The $3,300 donation is being sensationalized in the news, implying that West may compromise his values for money.
- West taking the money is not about selling out but about the necessity of funding his campaign.
- Crow likely provided the donation because he wants Biden to lose, believing West could draw votes away from him.
- Beau believes that the impact of West running on Biden's votes is overstated and that it might actually encourage more people to vote.
- The coverage of the donation is heavily influenced by partisanship, but Beau doesn't think $3,300 is enough to buy someone like West.
- Even a GOP mega-donor like Crow can support a progressive if it benefits his cause, showing a crossing of ideological lines.
- Beau suggests looking beyond headlines and sensationalism, considering the actual dollar amount involved in the donation.

### Quotes

- "The coverage of this is over 3,300 bucks and in the grand scheme, I don't think that's enough to buy somebody like West."
- "West running might get people to show up to vote who lean towards Biden, but don't like him enough to actually show up and vote for him."
- "Even a GOP mega-donor can reach across party and ideological lines if he thinks it's going to benefit him and his cause."

### Oneliner

GOP mega donor supports noted progressive with max donation for presidential campaign amidst sensationalized news coverage and partisan influence.

### Audience

Voters, Media Consumers

### On-the-ground actions from transcript

- Support third-party candidates in elections (implied)

### Whats missing in summary

The full transcript provides a nuanced perspective on political donations, partisan influences, and the impact of third-party candidates beyond sensationalized headlines.

### Tags

#PoliticalDonations #ThirdPartyCandidates #PartisanInfluence #Sensationalism #ElectionImpact


## Transcript
Well, howdy there internet people, it's Bo again.
So today we are going to talk about West
and a max donation from a GOP mega donor
and why that donation was made to a progressive
and what it says, not just about the people involved
and their intents, but also how we cover news and why we still have to be
careful when consuming news. Because that's the headline. GOP mega donor
Harlan Crow, a name everybody knows, gave the max donation to Cornell West, noted
progressive. Launching an independent campaign was initially with the Green
Party, I think, but launching an independent third-party campaign for
president. Let's start with this. Max donation. When we hear about max
donations and you put it into the frame of what presidential campaigns normally
run, we just went over the numbers. In the tens of millions of dollars, you would
think that a max donation that's a lot of money. It's 3,300 bucks. I do not care
what you think about West. Okay, you may like him, you may not, you may like him
but don't think he should run. There's a whole bunch of different options on
those lines. Do you believe that West would sell out his values for 3300
dollars? It seems really unlikely but that's kind of the way the headlines
are, that's what they're hoping to imply. I don't think that that's, I don't think
that's fair and I don't think it's accurate. Now if you don't, if you don't
want West to run for political reasons or partisan reasons. That's one thing, but I
don't think that West would alter his progressive positions based on a $3,300
donation from Harlan Crow. I don't see that as something that anybody would
really believe. So why would West take the money? Because he needs it. Because he
needs it. We've talked about the amount of money that presidential campaigns cost.
He didn't have anywhere near that. He needs the money. He can't turn away money right now.
now. I don't think it's because of some agreement. Now, why would Harlan Crow provide that money?
Because he can, and because he is probably somebody who would like to see Biden lose.
there is a strong belief that West running means that people who would vote
for Biden will instead vote for West making it more likely that Biden lose. I
mean that makes sense and we've seen stuff like that before. I actually have
a very different opinion on that. No doubt West would quote take some votes
from Biden, but I don't think it's the numbers that people are throwing around.
I think that, I think the amount of people who would switch their vote from
Biden to West is actually pretty small.
I think West running might get people to show up to vote who lean towards Biden,
but don't like him enough to actually show up and vote for him.
So
that's that's the news. All of the coverage
about this is over 3,300 bucks
and in the grand scheme I don't think that's enough
to buy somebody like West and I
I think the
the partisan nature of it is influencing
a lot of the coverage. And again, it doesn't matter if you like him or not,
it doesn't matter if you would vote for him, I just don't, I don't believe that's the
case.
And it does go to show
that even a GOP mega-donor
quote can reach across party lines,
ideological lines,
if he thinks it's going to benefit him and his cause.
Now as far as the whole
third-party taking votes and all of that, I mean that's a conversation y'all can have.
But I think it's important
rather than to just run with the headlines of Max Donation and let
people's
imaginations run wild to put an actual dollar amount on it.
three thousand three hundred dollars anyway it's just a thought y'all have a
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}