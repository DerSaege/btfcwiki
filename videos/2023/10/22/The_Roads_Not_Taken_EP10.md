---
title: The Roads Not Taken EP10
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=LtItBLpyITY) |
| Published | 2023/10/22 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Episode 10 of The Road's Not Taken dives into under-reported news, including Biden's $106 billion funding request.
- Biden's funding request covers various programs beyond just Ukraine and Israel, including U.S. border, submarine production, countering China, and humanitarian aid.
- A U.S. soldier who crossed into North Korea from South Korea has been returned but charged with desertion.
- Poland's election results are expected to improve relations with Ukraine, potentially shifting closer to the EU.
- Israeli Arabs are reportedly being arrested over social media posts showing solidarity with Palestinians.
- Starting in 2025, travelers to Europe, including visa-free countries like the U.S., will need advanced travel authorization.
- New York bill A8132 proposes background checks for certain 3D printers to prevent firearm production.
- Judge Chutkin temporarily lifted a gag order on Trump, allowing time for arguments on its appeal.
- In the U.S. House of Representatives, Jordan is out as speaker, with potential shifts in leadership roles.
- Since Elon Musk acquired Twitter, traffic has dropped significantly in the U.S. and worldwide.
- Analysis indicates 10 billion snow crabs disappeared from 2018 to 2021 due to extreme ocean heat.
- A Florida lawmaker behind the "Don't Say Gay" legislation faces federal prison time for loan fraud.
- Beau tackles questions on naming conflicts, Palestinian refugees, and ongoing Republican attacks on Biden.

### Quotes

- "Having the right information will make all the difference."
- "Look at it through the lens of power, not right and wrong, because it's foreign policy."
- "This is episode 76,212 of them trying to come up with something to support all of the allegations they made all of those years about Biden."

### Oneliner

Beau provides insights on Biden's funding request, international conflicts, and domestic challenges, urging a focus on accurate information.

### Audience

Community members, activists

### On-the-ground actions from transcript

- Contact local representatives to advocate for transparent distribution of Biden's funding request (suggested).
- Join organizations supporting Palestinian rights and raise awareness on social media (implied).
- Organize community events to address visa policy changes for European travel (implied).

### Whats missing in summary

Insights on the impact of misinformation in political narratives.

### Tags

#ForeignPolicy #Biden #InternationalRelations #CommunityAction #InformationAccuracy


## Transcript
Well, howdy there, internet people,
and welcome to The Roads with Bo.
Today will be episode 10 of The Road's Not Taken,
which is a weekly series
where we go through the previous week's events,
and we talk about under-reported, unreported news
or information that is going to be important later,
and just kind of get context
on things that are going to matter.
Okay, so as is typically the case, we will be starting off with foreign policy.
By the way, it is October 22nd is the day this will go out.
People have asked for dates on these.
Okay, so Biden is asking for about $106 billion in funding for various programs.
Now the framing in most news outlets is it's for Ukraine and Israel.
Some will even say it's for Ukraine, Israel and Taiwan.
It is a little bit more complicated than that and we're just going to kind of run through
it real quick.
There's about 60 billion for Ukraine.
Now some of that is military assistance and some of that is for assistance for Ukrainian
refugees in the United States, and maybe even in other places.
That's not exactly clear at the moment.
There's around $14 billion in funding for the U.S. border.
Now that's to add more immigration judges, asylum officers, and border patrol.
There's around $3 billion for building out U.S. submarine production infrastructure.
about $4 billion to counter China. I'm sure in the request it doesn't say to
counter China, but that's what it's for. And that's split between money that's
going to be used to counter their soft power initiatives. They've been having a
lot of success that way, so the US is probably looking to counter that. So it'll
be loans to less powerful nations in the region, and then the rest of it is for security, exactly
what you're picturing.
Then there's 10 billion in humanitarian aid for those impacted by the Russo-Ukrainian
and Israeli-Palestinian conflicts.
There's about 14 billion related to security for Israel.
Included in this is a whole bunch of money to harden U.S. embassies in the region.
So it's all over the place.
There's a whole bunch of stuff in here that the Democratic Party and especially the Democratic
base is not going to be happy about.
My guess is that's to try to get Republicans in the House to vote for it.
had to guess. Okay, the U.S. soldier who crossed over from South Korea to North Korea,
everybody wanted him back. It was a big thing a while ago. Well, he's been returned, and since
he has been returned, he has been charged with desertion and possessing images of children.
Poland's election results are expected to reset relations with Ukraine.
So Ukraine and Poland, they're close.
They've been really good allies.
There's been some tension lately.
The election results indicate that that tension is going to melt away.
It is also likely that this means Poland is going to kind of move back a little bit closer
to the EU.
The BBC is reporting that Israeli Arabs are being arrested over social media posts expressing
solidarity with Palestinians.
An Israeli police commissioner said anyone inciting against the state of Israel, its
government symbols, elected officials, military personnel, and police should be aware that
Israel police will respond firmly and without leniency. They've had a law like this on the
books. There are a lot of people who believe it is being reinterpreted and the interpretation
is wider than it used to be. I would imagine to hear a whole bunch about this over the next week.
Okay, starting in the spring of 2025, people headed to Europe will need advanced travel
authorization.
A bunch of visa-free countries, including the U.S., the UK, and Canada will now need
approval through the European Travel Information and Authorization System.
Okay, moving on to the U.S.
A New York bill, A8132, would require a background check for some 3D printers.
Because you could theoretically make firearms with them, they want the same kind of background
check for them.
I'm sure that'll go over well.
Okay, Judge Chutkin temporarily froze the gag order she placed on Trump to give the
parties more time to prepare arguments about whether or not it should stay in place while
he is appealing it, if that makes any sense.
Trump was recently fined $5,000 for violation of a gag order.
That's a different one.
Okay, on to the U.S. House of Representatives.
Jordan is out as speaker. The 86 votes that were in favor of him in the secret ballot
among Republicans, that needs a lot of commentary on its own, and I'm sure that'll come next week,
but suffice it to say that's not enough. Options on the table include expanding McHenry's powers.
The problem with that is it is almost certain to create a whole bunch of legal challenges.
an even bigger problem with that is that it kind of seems like McHenry doesn't want them to do
that. I mean, he kind of said that if they did it, he'd quit. I mean, just, I don't think that's
going to work out. As far as all of the people that are being talked about right now, the odds
are kind of on Emmer as being the next person they try for.
But that could change at any moment.
It's a mess.
OK, according to analysis from a similar web,
since Musk got Twitter about a year ago,
traffic has dropped by 19% in the US and 14% worldwide.
A judge has ruled that Alex Jones can't use bankruptcy protection to avoid paying damages
to the families he was ordered to pay damages to.
I have to admit, I'm a little upset about Cheese Broth's plea on this one because now
means Jones won't testify, and I kind of wanted to see that. SpaceX is getting irritated with
the regulatory processes, saying licensing, including environmental, often takes longer
than rocket development. This should never happen, and it's only getting worse. It's worth noting
that one of the company's rockets is currently grounded, because it blew up after development.
The fifth case of West Nile virus was detected in Massachusetts.
In Detroit, the president of a Detroit synagogue board was found outside her home with multiple stab wounds,
was pronounced dead at the scene. A motive at time of filming has not been
determined, but given the current political climate, suspicion and tension
and fear is running pretty high. There's a lot of speculation about
that already, and it's not unwarranted speculation. In New York, two
brothers were arrested after allegedly attacking a group of people and
screaming derogatory remarks about Palestinians and Islam. That
investigation is ongoing. Yeah, the country's doing great right now,
handling this very well. Okay, moving on to environmental news. Roughly 10 billion
snow crabs disappeared from 2018 to 2021. This severely damaged the local
economies dependent on that industry. Analysis suggests the reason for the
disappearing crabs was a mass starvation event triggered by extreme ocean heat.
Climate change is real and it's here. In odd news, the Florida lawmaker who
sponsored the infamous legislation commonly known as Don't Say Gay has been
sentenced to four months in federal prison after he entered a guilty plea for
a scheme to fraudulently obtain a $150,000 federal COVID relief loan.
Okay, moving on to the Q&A. Doesn't look like there's many. Let's see what we have here.
You seem to be intentionally avoiding saying the name of, quote, the group over there. Why? I am.
You're right. We've talked about it a couple of times since this started. This type of conflict
What is a PR campaign with violence?
What is the most important aspect of a PR campaign?
I'll give you a hint.
Justice Protasewicz did a really good job with it.
Name recognition.
It's generally not a good idea to do that.
Now the problem with it in this case is there is a whole lot of conflating between that
organization and the civilians. I don't have a good answer for that. I know that
most of the people who are constantly using that name, they're doing it to
draw that distinction and it is an important distinction to make. I have been
using groups and Palestinians to mean civilians or actually saying civilians.
That is, I don't have a good answer, like I don't have a good alternative.
But this isn't a new thing, this is, you can go back to US operations in Iraq and there
was a group with the letters IS, if you go back I will refer to them as pretty much anything
except that.
the guys with that black flag, I think is what I called them.
But generally speaking, you don't want to give them press.
Okay.
Why don't Arab countries let the Palestinians come to their countries?
Okay.
So there is a historic reason.
There's a moral or ethical reason, and we'll go over those first.
The moral and ethical one, from their point of view, they shouldn't have to leave.
It's their home.
Makes sense.
Easy enough.
The historical one is that in past conflicts, refugees were created and they went to other
countries and then Israel didn't allow them back in.
So that gives them basically the ability to say, well, we don't want to let them in because
Israel will never let them go home.
Both of these things, they're moral, they're ethical, all of that stuff.
This is about foreign policy.
What is foreign policy about?
Power.
So what's the real reason?
at it a lot, like the US relationship with the Kurds.
So Israel, it is a very powerful nation for the region.
It would be even more powerful if this conflict was resolved, right?
So it is at that poker game where everybody's cheating, it's in a lot of Arab nations,
it's in their interest to keep Palestinians in the situation that they're in.
It keeps Israel off balance.
same way the United States always helped the Kurds when we had a country that we were both
opposed to, but we never helped them enough to actually let them get their own country.
It's the same kind of power play.
I know that with this conflict in particular, people want to look at it through moral, historic,
ethical lenses.
When you get to the foreign policy aspects of it, it's much easier to understand if
you just look at it through the lens of power.
else's moves make a whole lot more sense. That doesn't help and it doesn't negate
the moral issues that are occurring. It doesn't negate the impacts to
Palestinian or Israeli civilians. But if you want to understand it and you want
to understand why the players are behaving the way they are, look at it
it through the lens of power, not right and wrong, because it's foreign policy.
Do you believe Emily Blunt's apology for her comments?
There's a note here from my team.
She's the new Mary Poppins.
She described a server as enormous in 2012.
Server as in waiter or waitress got it.
And said she's sorry she ever said anything so insensitive.
It was said in 2012?
I don't know if I have not.
To be clear, if this hadn't have said the new Mary Poppins, I would have no idea who
she is.
It's not a name I recognize, and I am not familiar with this particular situation.
I haven't seen her apology.
But I would hope that just about anybody has changed since 2012.
So, I mean, that's all I can say about it.
I'm not familiar enough with the situation.
What do you think of the Biden check?
Okay.
So if you missed this, with everything going on all over the world right now, with the
unmitigated horror that is occurring in so many different places, the Republican Party
is still trying to manufacture something to go after Biden for.
And they released a check from, I think it was his brother, it was a relative, to him
for $200,000.
In the memo line, it says for loan repayment or something like that.
This is the Republican Party showing direct money going to Biden.
The problem with it is that the relative's attorney has already come out and said the
committee has the financial records showing the initial loan, which I guess was like six
weeks before, that they have that already.
And I guess they had it at the time they released the check.
So look, this is episode 76,212 of them trying to come up with something to support all of
the allegations they made all of those years about Biden.
And this one appears to have already just already fizzled before it even really got
anywhere.
I mean, I would like it noted, though, that the Republican Party, as far as these allegations
go, they have lost so much credibility that basically at this point, I mean, we're taking
the word of some random lawyer and just being like, yeah, that's probably true because every
time they've made one of these allegations, the slightest bit of fact checking reveals
it to be false and to be manufactured.
But my understanding is there is a check from a relative to Biden, I want to say it was
in 2018.
I know he wasn't in office at the time, which again, just causes more issues for their narrative.
But it was 2018, $200,000.
It said loan repayment on it.
And according to the person's attorney, the financial records for the initial loan coming
from President Biden, or at the time, I guess, former Vice President Biden, whatever, that
they already have those records.
So my guess is that this will be another just giant nothing.
Okay.
So that looks like all of it, and okay, so I hope that gives you a little bit more
context, a little bit more information, and having the right information will make all
the difference.
I'll have a good night.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}