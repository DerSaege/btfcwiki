---
title: Let's talk about Biden having bigger crowd sizes on Truth Social....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=WXHWL6uVOTo) |
| Published | 2023/10/22 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden campaign set up an account on TruthSocial, Trump's social media site, to exploit Republican infighting.
- Majority of TruthSocial users seek anger and outrage as a release, making them likely to follow the Biden campaign for that content.
- Biden-Harris HQ has 32,100 followers on TruthSocial, while the Trump campaign has 26,600.
- Strategy involves letting Republicans attack each other, eliminating the need for Democratic attack ads.
- Biden's larger crowd sizes on TruthSocial are noteworthy.
- The approach appears to be effective in exploiting the disarray within the Republican Party.
- Democratic Party benefits from Republicans constantly attacking each other on TruthSocial.

### Quotes

- "People on Truth Social, the majority of them, they want to be mad."
- "The anger, that's a release for their anger."
- "I definitely thought they were going to get a decent amount of subscribers just for the outrage."
- "I talked about this, I think last week, maybe earlier this week."
- "Biden has bigger crowd sizes."

### Oneliner

Biden's campaign strategically engages in TruthSocial to exploit Republican infighting and anger for effective engagement.

### Audience

Political strategists

### On-the-ground actions from transcript

- Join and follow political campaigns on social media to stay informed (exemplified)
- Monitor and analyze social media strategies of political parties for insights (exemplified)

### Whats missing in summary

Analysis of the implications of political campaigns leveraging social media platforms for strategic advantages.

### Tags

#PoliticalStrategy #TruthSocial #RepublicanParty #DemocraticParty #SocialMedia


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk a little bit more about the Biden
campaign's trip over to TruthSocial.
If you missed it, a quick recap, the Biden campaign set up an account on
TruthSocial, TruthSocial, of course, being Trump, the Trump branded social
media site. It is his home base. Now, given the current state of the Republican
party and all of the infighting, there are tons of clips of Republicans talking
bad about each other. This presents the Biden campaign with an opportunity to
post that kind of stuff and get Republicans fighting amongst themselves.
And it's just a funny little thing, and honestly, it would probably be very effective.
I talked about this, I think last week, maybe earlier this week, and immediately after the
video went out, I got a bunch of messages saying, I don't think anybody's going to
follow the Biden administration or the Biden campaign on Truth Social.
You obviously do not understand the ecosystem there.
People on Truth Social, the majority of them, they want to be mad.
The anger, that's a release for their anger.
That's why a whole lot of them are there.
It's how the Republican Party has motivated people for the last six years or so, and it's
kind of caught on.
want that outrage. So, of course, they would follow the source of their outrage, the person
they blame for everything. And when they post clips of Republicans attacking each other,
there will be engagement, which will spread it to other people. Now, here's the interesting
bit about this, I definitely thought they were going to get a decent amount of
subscribers just for the outrage.
So the Biden-Harris HQ, which is their campaign account, at time of
filming has 32,100 followers.
The Trump campaign has 26,600.
Now, obviously, a lot of people on Truth Social, they follow Trump directly.
They don't follow his campaign.
But at the same time, those are good numbers.
I would like it noted that Biden has bigger crowd sizes.
And all of this is, it's funny in a haha kind of way, but this is also a good
election strategy. The Republican Party is in disarray and because of getting
rid of the 11th commandment, thou shalt not speak ill of another Republican,
which Trump is the person that did away with it, they're constantly attacking
each other. Which means the Democratic Party doesn't have to generate attack ads
on Republicans. Republicans will do it for them. You just have to get them to
the target audience. This is definitely a method to do it and so far it
It certainly appears to be working.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}