---
title: Let's talk about Ukraine and Russia's stocks going down....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Wehaoz9uMIM) |
| Published | 2023/10/31 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the reasons behind Ukraine's pause in being hit with cruise missiles by Russia.
- Russia typically deployed cruise missiles from the sky to target Ukrainian grain depots and civilian infrastructure.
- Russia has not used cruise missiles for a month, causing noticeable concern in Ukraine.
- Speculations include Russia running out of cruise missiles, holding them for an offensive, or building a stockpile for targeting Ukrainian energy infrastructure in winter.
- The theory favored by British intelligence suggests Russia is producing cruise missiles, running low, and planning to target energy infrastructure.
- Beau believes the British intelligence explanation is the most likely, as it fits with Russia's strategy and capabilities.
- Warning Ukrainians to prepare for potential attacks on energy infrastructure in winter, potentially to freeze them out.

### Quotes

- "They are producing them, they're running low, but they're trying to build up a stockpile to hit Ukrainian energy infrastructure this winter."
- "If there is something you can do to mitigate that as an individual, I'd probably start."
- "The outlier explanation comes from British intelligence, and I think they're right."
- "That seems like the most likely answer."
- "I think the British have it right on this one."

### Oneliner

Beau delves into possible reasons behind Russia's pause in using cruise missiles on Ukraine, pointing towards British intelligence's theory of building a stockpile to target Ukrainian energy infrastructure in winter as the most likely explanation.

### Audience

Ukrainian residents

### On-the-ground actions from transcript

- Prepare for potential attacks on Ukrainian energy infrastructure in winter (suggested)
- Stay informed and alert about the situation (suggested)

### Whats missing in summary

Further analysis on the implications and potential actions for individuals and communities in Ukraine. 

### Tags

#Ukraine #Russia #CruiseMissiles #BritishIntelligence #EnergyInfrastructure


## Transcript
Well, howdy there, I don't know people.
Let's bow again.
So today we are going to talk about Ukraine
and Russian stocks falling and why something else isn't.
And we're gonna go through the various possible reasons
for that occurrence and just kind of see
if we can figure out which one is most likely.
And we're gonna do this because we got a question
from somebody in Ukraine and they're basically asking why have they stopped
hitting us with cruise missiles. That's the question. Now for Americans, when you
think cruise missile, you think tomahawk, you think ship, most likely. We like to
deploy cruise missiles from ships. Russia likes to drop them from the sky from
their bombers and they haven't done it. They went a whole month without doing it
and they had been doing it pretty regularly to hit grain depots, civilian
infrastructure, stuff like that. When you hear about something hitting in a
Ukrainian city that's normally what it was and there's kind of been an absence
of it and obviously if you've been living for something living with
something for a long time and it stops you notice it and it makes you concerned
okay so the the explanation that is getting all of the attention right now
is that they're out, Russia ran out of them, it's not it. Russia makes those, they
make those themselves, they don't buy them from anybody else, they make them.
The sanctions, they are not strong enough to be able to stop production of
those, so they're not out, they could be running low, that they could be having a
hard time producing them but they're not out and if they're low why aren't they
using the ones that they have at the same rate as they're being produced? It's
an easy question. So that's one option and that's the one that most people are
focused on and most people believe. I don't think that's right. The other one
is the one that is being championed by those who are super favorable when it
comes to Russia. They support Russia. And they're saying that Russia is holding
them in reserve for their offensive. No, just no. First, Russia is trying to make
territorial gains like right now while I'm filming this. It's not going well.
They are measuring the losses in brigade size. Believe me, if they had them and
they planned on using them during it, they would. So that doesn't seem likely.
The outlier explanation comes from British intelligence, and I think they're
right. They are producing them, they're running low, but they're trying to build
up a stockpile to hit Ukrainian energy infrastructure this winter. That seems
like the most likely answer. It fits with their overall strategy on how they do
things and it fits with what they're actually capable of producing and
everything and that's the only one that actually explains all of the little
pieces. So that would be my guess. My guess would be they're gonna try to
they're going to try to freeze you out. They're going to try to make it really
cold. If it was me, I would do everything I could to prepare for that because
that's... even though this is one of those times where you have this
explanation coming from one group and you have a bunch of people repeating the
same thing I think the the outlier is correct and this happens it just
remember you all were supposed to already be like part of Russia if you
went off of the majority of the estimates it was the outliers that said
you had a chance and I think that this is this is very much the same thing my
guess is they will wait for it to get cold cold and then start hitting energy
infrastructure. That's... I think the British have it right on this one. So, I would...
if there is something you can do to mitigate that as an individual, I'd
probably start. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}