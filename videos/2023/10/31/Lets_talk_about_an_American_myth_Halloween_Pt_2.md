---
title: Let's talk about an American myth (Halloween Pt 2)....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=GEzWdGjOprI) |
| Published | 2023/10/31 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- American myth collides with individualism, creating a truly American monster.
- American zombie's origin roots in equatorial Africa and Haiti, where it was a victim, not a monster.
- Zombie myth evolved in the U.S., reflecting the American psyche's desire for individualism.
- The myth showcases the false notion of rugged individualism; survival requires teaming up with others.
- Reframe the zombie myth as a way to encourage emergency preparedness and community building.
- Small dedicated groups are the key to creating real change in the world.

### Quotes

- "One person can't fight off a zombie apocalypse. A network, a group can."
- "The myth showcases that the myth is wrong."
- "It shows that dedicated people teaming up well."

### Oneliner

American myth collides with individualism, creating the truly American monster of the zombie, reflecting the false notion of rugged individualism and the necessity of small dedicated groups for real change.

### Audience

Horror enthusiasts, community builders

### On-the-ground actions from transcript

- Prepare for emergencies like a zombie apocalypse to teach emergency preparedness and build community networks (suggested)
- Work in small dedicated groups to bring about real change (implied)

### Whats missing in summary

The full transcript delves into the origins of the American zombie myth and how it symbolizes the American psyche's desire for individualism, juxtaposed with the reality that true survival and change require collaboration and community building.

### Tags

#AmericanMyth #Zombie #Individualism #CommunityBuilding #EmergencyPreparedness


## Transcript
Well, howdy there, internet people, it's Bo again.
Happy Halloween.
So today we are going to talk about an American myth.
One of the few items, topics, entities
that gets discussed around this holiday that is truly American.
And we're going to talk about how
that myth collides with individualism because those two things they come together and they
created that truly American monster in every way and a big part of that means that we stole
it from somebody else.
So the zombie, the American zombie that most people are familiar with is nothing like the
origin story, where the inspiration came from. If you talk to most people about it, they will tell
you it came from Haiti, and that's true, but it has its roots in equatorial Africa. During slavery,
people were brought over to Haiti, their beliefs mixed with the beliefs of Catholicism, and they
created what Americans will call voodoo. And something kind of close to the
American zombie existed there, but there the zombie was the victim. The zombie was
the victim, not the monster. The monster was the person who made the zombies
because what they did was they took them and they used them for free labor and it
was something that even death couldn't free you from. Seems pretty obvious that
it's a metaphor for slavery but as things typically happen Americans found out
about it. Why? Because we invaded the country and it came back and it came back to the United
States. We put our own spin on it and then eventually it evolved into its own creature,
the American Zombie, the one that everybody is familiar with, the herds, the masses of
people crawling around trying to eat you and all of that stuff. That was not the original
zombie. That's the American one. And the way that came about reflects something
about the American psyche. And it's its desire for individualism. When you think
about the zombie myth, there's these herds of people. And they're not real
people, there are others, obviously. And they all think the same way, all about
consuming. And in some variations it becomes even more overt that they want
you to join them. They're after your brain the way you think. And in those
movies in that myth it's always the the real individualists that are able to
fight back right and because of that you see a lot of people who still believe in
the rugged individualist myth that Johnny get your gun Rosie the Riveter
type thing, you see them lean into it. You see them really identify with those
storylines. But one of the things that I've always found interesting is that
when you actually follow the stories, the only way the rugged individualist
survives is by teaming up with rugged individualists and forming a network, a
group who all have the same purpose. It's one of those things where the myth
showcases that the myth is wrong. One person can't fight off a zombie apocalypse.
A network, a group can. One person can't change the world. That's great man theory.
A network, a group can, a small one.
The zombie myth, it doesn't show that the individualist out there acting alone saves
the world.
It shows that dedicated people teaming up well.
We should probably try to reframe that and use that.
I've talked about it on the channel before, but it's been a while because basically it's
starting to do 24 hour a day news channel.
But one of the best ways to get people interested in emergency preparedness is to not talk about
hurricanes or wildfires or earthquakes or blizzards or whatever happens in your area,
but to talk about zombies.
Those people who are not interested, who don't believe the extreme weather or natural phenomena
will impact them, don't want to talk about that.
They will absolutely discuss the zombie apocalypse with you.
And if you are prepared for the zombie apocalypse, if you have an idea what to do then you can
probably make it through the other stuff.
So it's a good teaching tool.
It also can be used jumping from that to building community networks, which is something that
we have been talking about on this channel for years, you know, except for
recently, that we will get back to because everything that is just a world
on fire right now, it has to be changed. And the way it's going to be changed, the
only way it really can be, is by small dedicated groups of people working
together to fix it.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}