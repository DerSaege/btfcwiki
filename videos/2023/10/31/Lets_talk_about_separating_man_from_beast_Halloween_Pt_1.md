---
title: Let's talk about separating man from beast (Halloween Pt 1)....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=QpOqS1h0eD8) |
| Published | 2023/10/31 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the concept of the separation between man and beast, and questioning if it truly exists or if it's a construct.
- The historical context of the werewolf myth as a punishment for doing something bad, turning humans into wolves.
- Over time, the werewolf narrative changes from a punishment to a condition representing the beast within.
- The idea that humans may have created monsters like the werewolf to distance themselves from beastly acts.
- An example from 1521 where a story about Frenchmen turning into wolves was possibly a rationalization for their inhuman actions.
- Challenging the notion of what separates humanity from other animals, discussing our ability to transmit complex ideas.
- The observation that while humans have the ability to share complex ideas, it is often used to justify beastly actions towards others.
- The recurring rhetoric of dehumanizing others by labeling them as animals to justify mistreatment.
- Those who dehumanize others are often the ones responsible for committing the most inhumane acts throughout history.
- Posing questions about the evolution of myths and whether good humans allow inhumane acts to persist.

### Quotes

- "Those who dehumanize others are often the ones responsible for committing the most inhumane acts."
- "There's an evolution, you have to wonder if those original myths really meant the person was actually turned into an animal."
- "Humans doing something so beastly that nobody wanted to believe that it was a human that did it."

### Oneliner

Exploring the origins of the separation between man and beast, questioning humanity's treatment of others as less than human.

### Audience

Activists, Philosophers, Movie Buffs

### On-the-ground actions from transcript

- Challenge dehumanizing rhetoric towards marginalized groups by actively promoting empathy and understanding (implied).

### Whats missing in summary

A deeper dive into the historical origins of myths and their impact on societal views today.

### Tags

#Mythology #Dehumanization #Humanity #Werewolf #History


## Transcript
Well, howdy there, internet people.
It's Bo again.
Happy Halloween.
So today, we are going to talk about the difference
between man and beast, and if that actually exists,
if that separation is real, or we've just created it.
But before we get into that, I want
to say that today, hopefully, all the videos
going to be Halloween themed. That's the plan anyway. And one coming out later
tonight is going to have a whole lot to do with a film called Dark Harvest. It's
a pretty new movie so if you haven't seen it and you like scary movies you
might want to watch it before then because otherwise I'm going to spoil it.
Okay, so, man and beast.
When you think of that contradiction, what typically comes to mind?
What monster highlights the separation between man and beast?
The werewolf.
And if you look back through history, and you look at old myths and old literature,
you find out that it was kind of there from the very beginning.
You can certainly find it in Greek myths.
You can find it in Nordic myths.
And there's even some that would say that there's a reference to the idea of a werewolf
in Gilgamesh.
In the beginning, it wasn't the same though, in the beginning it was a punishment.
The person did something and because they did something bad, they were punished by being
turned into a wolf.
Mainly because animals are below us, that's how it's viewed.
separate from them. We're different. So it's a punishment. In some cases doing
something really horrible would get you turned into a wolf. Makes sense on some
level when you think about it through the context of the time and how humanity
possessed skills and abilities that animals didn't have. At least we think
they don't have. That's one. But see, the interesting thing is that as time goes on,
the story changes. It's no longer a punishment that is placed on somebody because they've
offended a powerful being or a deity or whatever. It's a condition. It's the beast lying right
underneath. And it's still used as a way to other and look down on animals. Then what
you realize is that the origin of the werewolf and a whole bunch of other monsters might
have actually come from humans doing something so beastly that nobody wanted
to believe that it was a human that did it. So the wolf, the werewolf became the
scapegoat, became the reason this horrible beastly thing happened. In 1521
there's a story that exists about these two Frenchmen. I guess as the story goes
they swore an oath to the devil. They made a deal with the devil and they got
this ointment that turned them into wolves. But as more comes out what you
of realizing is, well, they killed a bunch of kids. And that maybe that story was the rationalization,
the explanation for that behavior, because it's so inhuman. It's beastly. When you think about what
What actually separates humanity from other animals?
Because I would point out, we are animals.
What do you normally think of?
There's a whole bunch of things that you could point to, but most times, I think most people
would think about this.
What's happening right now?
Transmitting complex ideas to each other.
something that separates us from other animals. That's why they're lower because they do those
beastly things. But when you look around the world today, that ability to share complex
ideas. What does it get used for? Often times, to transmit ideas that are beastly. And if
you follow it, what you realize is that while that's happening, while people are trying
to convince others around them that it's necessary to do something beastly.
What rhetoric comes up?
Those people, those other people, they're not like us.
They're different from us.
Those people, they're animals.
That rhetoric still exists, that justification to view it as lesser.
Not that they're actually physically turned into an animal as punishment, but they're
called an animal.
They're treated as an animal.
The rhetoric grows and it becomes the justification to take that group of people who have been
othered and bombed them into submission, to round them up, ask if they're really
supposed to be on this side of the line, because they're animals. They're
different. And in the process, though we would never want to admit it because
modern humans like to pretend that we're all enlightened, those who throw out that
rhetoric, those who have the desire to dehumanize others and to other others,
Those are the ones who end up responsible for the most beastly acts
that can be attributed to humanity and it's been there since the beginning.
There's an evolution of course. You have to wonder if those original myths, if they
really meant the person was actually turned into an animal or if they were
just viewed as one as punishment to other them to separate them from the
rest of the good humans. And you have to ask whether or not the good humans would
would allow things that are so inhumane to continually occur.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}