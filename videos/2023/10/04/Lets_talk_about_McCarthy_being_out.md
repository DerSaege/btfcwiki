---
title: Let's talk about McCarthy being out....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=O_P0b1QRq2s) |
| Published | 2023/10/04 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- McCarthy was ousted, marking a historic moment in US history.
- Eight Republicans crossed party lines to work with Democrats to oust McCarthy.
- Patrick McHenry becomes the temporary speaker after McCarthy's removal.
- The situation stems from a continuity of government issue post-September 2001.
- McCarthy might attempt to regain the speaker position or a replacement like Emmer, Stefanik, or Scalise could step in.
- The far-right Republicans misunderstand the current political landscape with a Democratic Senate and President.
- The Republican Party's inability to govern is evident through their struggles in selecting a speaker.
- Trump's influence and abandonment of the "11th commandment" has led to the current chaos within the Republican Party.
- To address the issues, the Republican Party must distance itself from Trumpism.
- There is no set time limit for McHenry's temporary speakership.

### Quotes

- "The big takeaway from today is that the Republican Party is incapable of governing."
- "Unless the Republican Party wants this to continue, they're going to have to get rid of Trumpism."
- "The Democratic Party got something out of it. They showed the entire country that the Republican Party can't even handle the basics of picking a house speaker."

### Oneliner

McCarthy's ousting showcases the Republican Party's governance incapability, fueled by Trumpism, urging a necessary shift away from it.

### Audience

Political observers

### On-the-ground actions from transcript

- Contact local representatives to express opinions on party governance (suggested)
- Organize community dialogues on political accountability and party dynamics (implied)

### Whats missing in summary

Insights on the potential long-term impacts of the Republican Party's current struggles.

### Tags

#GOP #McCarthy #Trumpism #PartyPolitics #USPolitics


## Transcript
Well, howdy there, internet people, it's Bill again.
So today, we are going to talk about McCarthy, obviously.
And what happens next?
OK, so if you missed the news, McCarthy is out.
First speaker in US history, I believe,
to be ousted in such a fashion.
First, we'll go through what happened.
The motion to vacate was put forward.
There was a motion to table, the motion to vacate.
It failed.
So the motion to vacate moves forward.
Eight Republicans, I think it was eight, crossed the aisle and worked in a bipartisan fashion
with Democrats making sure that, you know, Democrats making sure they could get the votes
to oust McCarthy because those Republicans believed that McCarthy was working with Democrats.
Take as much time with that as you need to.
So McCarthy is out, the speaker's chair vacated, Patrick McHenry becomes the temporary speaker.
If you took civics a while back, we'll do a video about why this happened in the way
that it did because it doesn't match up with what a lot of people were taught.
The answer lies in September of 2001-ish. This is a continuity of government thing.
Okay, so that's where it is right now. What happens next? Well, McCarthy could try to go
back. That's one option. Another one would be they try to find a replacement. Your likely
replacements would be Emmer, Stefanik, Scalise, people like that. Not necessarily those three
in those order or in that order or anything like that, but it would be people like that.
One of them becomes Speaker. What changes? I mean, nothing. Nothing really.
The situation is still the same. One of the issues with what the far-right Republicans,
those who crossed over to work with the Democratic Party, what they don't understand is that there's
a Democratic Senate and a Democratic President. The idea that you're going to be able to push
through a far-right agenda is ludicrous. It means you don't understand basic civics,
so nothing really changes. It is worth noting that those three names that I mentioned,
all of them fairly recently have said they don't want to be speaker and that
may be an issue that Republicans run into. Nobody may want the job because
nobody wants to put up with the far-right Republicans. So the big takeaway
from today is that the Republican Party is incapable of governing. They can't
even govern their house. They can't even agree on somebody whose real job is to
schedule votes. It doesn't really inspire a lot of confidence in
their ability to tackle tough issues like the budget or climate change. One of
the questions that has come in since all of this transpired. Who's fault is it?
And it's an interesting thing because it's not coming from, it's not coming
from Republicans for once. It's generally Democrats who are asking this question
or people who are at least left-leaning. It's Trump. This is Trump's fault. The
utter disarray that the Republican Party finds itself in, it's the fault of Trump
and those people who continue to support him. You never had issues like this with
Republican Party before because that 11th commandment was in effect.
Thou shalt never speak ill of another Republican.
Trump threw that away.
He used a different kind of rhetoric.
He created and inspired an inflammatory style.
This is the effect.
Everybody told you it was going to happen.
I don't know why you're surprised.
We all knew this was coming.
Even Lindsey Graham knew this was coming.
If you want to trace it back, that's really what it is.
You could say it was McCarthy's fault for not rendering the far-right faction irrelevant.
That would have mitigated Trump's damage, at least some of it.
But that far-right faction still would have been there.
You could say it was that faction.
You could say it was their fault because they don't apparently understand how the relationship
between the House and the Senate.
But if you're going back to root cause, it's Trump.
It's Trump and his brand of politics.
Unless the Republican Party wants this to continue, they're going to have to get
rid of Trumpism. It's that simple. There's no other answer. An embrace of
Trumpism will just lead to more factions, each saying that they're the real
Republicans. So we're in for an interesting week. It is worth noting that
to my knowledge there's no time limit. McHenry's speaker, and I don't think that
there is a mechanism that forces a vote within a certain number of days or
anything like that. He's the temporary speaker, but my understanding is that
that that temporary speakership could last, theoretically, until the next election.
I don't know what the Republican Party, any faction of the Republican Party, got out of today.
The Democratic Party got something out of it. They showed the entire country that the Republican Party
can't even handle the basics of picking a house speaker.
Anyway, it's just a thought.
It's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}