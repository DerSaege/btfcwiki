---
title: Let's talk about Trump getting a gag order in NY....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Wc46-lm7tlU) |
| Published | 2023/10/04 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Mentioning developments in the New York Trump case amidst focus on Capitol Hill and McCarthy.
- Trump's response after the first day of the trial, claiming 80% of the case going his way.
- Judge clarifying that statutes of limitations bar claims, not evidence.
- Judge stating the trial is not an occasion to revisit previous decisions.
- Trump's tweet about a court staff member on a fake Twitter account.
- The judge issuing a limited gag order in response to the tweet, criticizing it as disparaging and untrue.
- The gag order preventing Trump from discussing that specific topic.
- Speculation about potential future gag orders for Trump due to his social media posts.
- Emphasizing the real impact of Trump's posts on individuals involved.
- Noting the importance of Trump maintaining silence during the legal process.

### Quotes

- "I want to emphasize this trial is not an opportunity to relitigate what I have already decided."
- "Personal attacks on members of my court staff are not appropriate and I will not tolerate it under any circumstance."
- "His, as the judge calls it, untrue social media posts. They can be damaging."

### Oneliner

Beau provides insights on the New York Trump case and potential consequences of Trump's social media posts during the trial.

### Audience

Legal observers

### On-the-ground actions from transcript

- Stay updated on the developments of the New York Trump case (implied).
- Respect legal proceedings and avoid discussing ongoing cases in a disparaging manner (implied).
- Support the accountability of public figures through legal processes (implied).

### Whats missing in summary

Insights into the potential legal consequences and public reactions to Trump's social media engagement during trials. 

### Tags

#NewYork #TrumpCase #LegalProceedings #GagOrder #SocialMedia


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk a little bit
about the New York Trump case,
because even though all eyes are currently focused
on Capitol Hill and everything going on there with McCarthy,
there were some developments in the Trump case
that are definitely worth mentioning, if I'm allowed to.
We'll start with the first one.
Yesterday, after the first day of the trial,
Trump came out and was basically like,
ah, the judge is making good rulings, and 80% of the case is going our way, and all of this stuff.
The judge then clarified something today, saying that statutes of limitations
bar claims, not evidence, and then went on to say, I want to emphasize this trial is not an
opportunity to relitigate what I have already decided." Remembering what he
decided last week. So that in and of itself is entertaining. The next part is,
for most people watching this, it'll be entertaining. For the subject of it,
probably not. Trump tweeted something or truthed whatever. He put it on that
little fake Twitter.
And it was a comment about court staff, about somebody on the court staff.
The judge issued a limited gag rule, issued a limited gag order, and didn't actually identify
Trump as the reason for it, but said one of the defendants put something out that was
quote, disparaging, untrue, and personally identifying, and went on to say that there
were millions of recipients.
Continued by saying, personal attacks on members of my court staff are not appropriate and
I will not tolerate it under any circumstance.
So the limited gag order went into effect.
Trump can no longer talk about that category of topic. I don't know how Trump
is going to respond to this but if he isn't careful he's going to end up with
another mugshot. I don't anticipate this being the last gag order that is
applied to the former president. I imagine that there will be more and more
as time goes on. His, as the judge calls it, untrue social media posts. They can be
damaging, not just to the process itself, but most times those posts focus on
people and they are real people. Many times they are not even central to
what's happening. They're not public figures. So I know that there's a lot of
popcorn eating and high drama going on today, but this is something that is
important because this is going to be a semi-lengthy process and Trump is going
to have to keep his mouth shut during an extended period of time. We'll see how
that plays out. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}