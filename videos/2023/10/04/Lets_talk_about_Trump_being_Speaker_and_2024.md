---
title: Let's talk about Trump being Speaker and 2024....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ldRl3z8XYqM) |
| Published | 2023/10/04 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Speculates about the idea of Republicans nominating Trump to be Speaker of the House.
- Questions if Trump has enough support in the House to become Speaker.
- Envisions Trump using the position for social media engagement and to further his political campaigns.
- Suggests Trump may push extreme legislation through with the support of House Republicans.
- Contemplates the potential consequences of Trump becoming Speaker, including repeated losses in an election year.
- Raises concerns about House members being obligated to support Trump if he becomes Speaker.
- Dismisses the notion of Trump becoming Speaker as a serious proposition, possibly aimed at riling up the Republican base.
- Concludes that making Trump Speaker could lead to a Democratic victory in 2024.

### Quotes

- "Don't talk about it, be about it."
- "I don't think you got it in you."
- "He gets the calendar, he gets the gavel, and he can lead the Republican Party to victory."
- "If they do that, and they push him forward, I think you'd have Republicans cross and Jeffries become speaker."
- "Making Trump Speaker of the House is basically guaranteeing a democratic victory in 2024."

### Oneliner

Beau questions the feasibility and consequences of Republicans nominating Trump as Speaker, foreseeing potential losses and a Democratic victory in 2024.

### Audience

Political observers

### On-the-ground actions from transcript

- Contact your representatives to express your thoughts on political strategies and decisions (implied).

### Whats missing in summary

Insights into the current political climate and strategic considerations for upcoming elections.

### Tags

#Republicans #Trump #SpeakeroftheHouse #PoliticalStrategy #Election2024


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about the speaker of the house and talk about the
idea that Republicans will nominate Trump to be speaker of the house.
They talked about this about 10 months ago as well.
Don't talk about it, be about it.
Do it.
Do it. I don't think you got it in you.
I also don't think Trump has the votes to win.
If he's nominated, I don't think he's got the votes.
I don't think there are enough Republicans in the House that support him to make him speaker.
And unlike what just happened, you're not going to get help from the Democrats.
So, let's just say hypothetically that Trump becomes speaker.
What happens?
He gets the calendar, he gets the gavel, and he can lead the Republican Party to victory.
How?
How exactly do they see this playing out?
What would Trump do?
Let's be real.
Trump being Trump, he would probably try to get a whole bunch of social media engagement.
He would try to use that position, the highest ranking elected position in the Republican
Party, to help his political campaign, his presidential campaign.
That's probably what he'd do, right?
So he would push through a bunch of legislation that is just incredibly extreme.
And Republicans in the House, well, they would have to vote for it because Trump, using his
newfound platform, would make them.
That's the kind of person he's always been.
It stands to reason.
It's what he would do now.
If they didn't vote the way he wanted, he would get out in front of those cameras as
Speaker of the House and encourage the primaries against them.
about how horrible they are, how they're not really Republicans, he would control
the calendar and the legislative agenda and he would put forth all of those
ideas that the far-right uses to energize their base. And he might even
get him through the House because he, I believe that he would bully people. But
But then it goes to the Senate and it gets defeated.
So in an election year where he's running for president, he would be handed loss after
loss after loss after loss from Dark Brandon.
That's who would get the credit for it.
I mean, realistically, it would never get through the Senate, but Biden would get the
credit for it.
But I don't see how that would impact the results of the 2024 election.
And then you have to consider the idea that given Trump's current entanglements, that
if the House made him Speaker now, they're signing up to have the Speaker of the House
dealing with all of that in an election year.
And everybody in the house is going to be obligated to support him and talk about what
a great guy he is.
Because if you don't, you know, he'll be out there in front of those cameras again.
And then I can't see any way that those clips would be used in ad campaigns later.
I don't think that this is a serious proposition by those suggesting it.
It's something to rile up their base.
I honestly do not believe that Trump has the votes to become Speaker.
I think if they do that, and they push him forward, I think you would have
Republicans cross and Jeffries would become speaker.
I don't think the votes are there.
I don't think that those people who are suggesting it, I don't think that
they're really suggesting it.
They're just saying something to appease their base, who they believe is
ignorant enough to believe it. Because making Trump Speaker of the House that's
basically guaranteeing a democratic victory in 2024 doesn't seem likely. I
know that they haven't been great at political maneuvering lately, but they're
They're not that bad.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}