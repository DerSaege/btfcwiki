---
title: Let's talk about the GOP majority of the majority....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=TJLO9Ubb9E4) |
| Published | 2023/10/04 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the Hastert Rule, an informal rule among House Republicans where a majority of Republicans must support bringing something to the floor.
- Points out that because of polarization, this rule means just 26% of the House can prevent something from getting a vote.
- Criticizes the rule as undermining democratic principles, giving minority rule power over majority support.
- Emphasizes that Republicans are prioritizing ruling over representing, wanting obedience rather than true democracy.
- Notes that the current Republican Party differs from the past, becoming more authoritarian and less about small government conservatism.

### Quotes

- "They want 26 percent of the House of Representatives to be able to determine everything."
- "It's not representation. It's an example of minority rule."
- "They're authoritarians who want to tell you what to do, tell you what to be afraid of, so they can do whatever they want."

### Oneliner

Beau explains the Hastert Rule, revealing how a minority of Republicans can prevent majority-supported measures in the House, undermining democratic principles.

### Audience

House Republicans

### On-the-ground actions from transcript

- Read up on the Hastert Rule and its implications (suggested)
- Educate others on the potential consequences of minority rule in the House (suggested)

### Whats missing in summary

The full transcript provides a detailed breakdown of the Hastert Rule and its impact on democracy, offering insights into Republican Party shifts towards authoritarianism.

### Tags

#HastertRule #HouseRepublicans #MinorityRule #Authoritarianism #Democracy


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about
the majority of the majority.
We're going to talk about a rule
that exists among House Republicans.
And we are going to talk about ruling
rather than representing.
Because I got this, it wasn't really a question,
it was more of a message, just a statement.
And it said, hey, you know, you always talk about
how Republicans don't really want to represent,
they want to rule, that just seems like, you know,
they're not voting the way you want and you're upset.
No, I mean, they want to rule rather than represent.
There is a rule among House speakers that are Republican.
It's informal, but it's something they apply.
And we've heard the term recently.
The majority of the majority.
It's called the Hastert Rule, if you want to look it up.
And what it means is that a Republican speaker
of the House, well, they won't bring something to the floor
unless a majority of Republicans support it.
And because we're so polarized, you kind of
think that makes sense at first until you
put any thought into it.
The speaker of the House is not the speaker
of the Republican Party.
They're the Speaker of the House.
A majority of Republicans right now, that's roughly 26% of the House.
74% of the House can support something, but it won't get a vote.
Because 26% of the House, who happen to be Republicans, oppose it.
It's literally minority rule.
They don't want to represent, they want to rule you, do what you're told, obey.
You can't tell me that that is what the Constitution wanted.
If that's what the Constitution wanted, it would have required three quarters to pass
something in the House.
It doesn't.
It's undermining those principles, it's undermining the principles of the republic, it's undermining
the principles of democracy, because 26% of the House gets say, it's ruling, not representing.
We're hearing this term come up, and the reason we're hearing it come up is because those
opposed to McCarthy, well, they thought that a majority of Republicans would be against
the deal.
that's not how things, you know, broke down, but that's what they thought and that's why they got
mad. This whole thing is because a faction, hardline Republicans, want to rule rather than
represent. They want 26 percent of the House of Representatives to be able to
determine everything. That isn't what the Constitution wanted. It doesn't
represent the founding ideals of this country. It's not an example of
representative democracy. It's not an example of representation. It's an
example of minority rule. People talk about the filibuster over in the Senate.
That's two-thirds. This rule makes it three-quarters in the House, depending on
the party, of course. That doesn't apply the other way. Doesn't apply if, you know,
it's 51% of Republicans that support something. Then it just, they can go
through. That's not representation. At some point, the rank-and-file Republicans
are going to have to realize the Republican Party that was your dad's
Republican Party, it's not the same. They're not small government
conservatives who want you to be left alone. They're authoritarians who want to
tell you what to do, tell you what to be afraid of, so they can do whatever they want.
It's called the Hastert Rule, I'll put a link to the Wikipedia article down below.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}