---
title: Let's talk about cops, reasonable expectations, and training....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=zKflxAAKm1A) |
| Published | 2023/04/02 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau Young says:

- Explains why cops couldn't perform at a top tier level in a specific situation in Nashville.
- Mentions the unrealistic comparison made between cops and top tier U.S. military.
- Emphasizes the extensive training required to reach a certain skill level.
- Points out that constant training and mindset changes might affect cops' roles.
- Argues that pushing cops to meet unrealistic standards may hinder their primary duties.
- Suggests that departments should focus on getting cops good enough to complete missions.
- States that aiming for perfection in every cop may not be practical or beneficial.
- Raises concerns about the impact of intense training on cops' roles as patrol officers.
- Acknowledges the importance of training but questions the need for perfection in all situations.
- Concludes by suggesting that the unrealistic expectations placed on cops may be unreasonable.

### Quotes

- "Unless you've gone through a door yourself, shut the hole under your nose."
- "The goal of departments should be to get them good enough to do it and complete the mission."
- "It's not that people are opposed to them training. It's that getting them to the level that those critiques were based on is going to prohibit them from being first on the scene."
- "The level of training it takes to get to the level that people apparently wanted them to be at, it prohibits them from also being patrol officers."
- "I think that's kind of unreasonable."

### Oneliner

Beau Young explains why cops couldn't meet unrealistic top-tier expectations and why it's impractical for them to strive for perfection in every situation.

### Audience

Police reform advocates

### On-the-ground actions from transcript

- Contact local police departments for community policing initiatives (implied)
- Attend community meetings to address concerns about police training and expectations (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the challenges and limitations faced by police officers in meeting unrealistic standards set by some critics, ultimately questioning the practicality and necessity of expecting perfection in every situation.

### Tags

#PoliceReform #TrainingStandards #CommunityPolicing #RealisticExpectations #LawEnforcement


## Transcript
Well, howdy there Internet people, it's Bo Young. So today we're going to
talk about why the cops
couldn't get to that level.
Why they did as good as can be expected.
And Carnegie Hall.
Okay, I'm gonna do this because I got a message.
And it's not, uh,
it's not an uncommon question.
So, I'm wondering why you said the cops did as well as could be reasonably expected, in
quotes.
I listened to a podcast by an ex-green beret who said the cops operated at the highest
level you could expect from cops, in quotes.
Both of you have always stressed training.
Why the reluctance to want them to up their game?
That's not a gotcha question.
I'm genuinely curious why it seems like cops can't get it perfect.
I'm sending this to him, too, in hopes that one of you replies.
OK.
So this is about the cops in Nashville.
And right after the footage came out,
there were a bunch of critiques that
came out about some of the stuff that they
did which wasn't perfect.
Those critiques, first, I would like
to point out that most of them are gone now because
the community that they were being compared to,
that the cops were being compared to,
came out and was basically like,
look, unless you've gone through a door yourself,
shut the hole under your nose.
But the critiques were comparing the cops
to like the top tier of the U.S. military.
Like the best of the best when it comes to shooters
and people who do this.
That was the comparison that was being made.
And it's not realistic.
And it's also impossible.
Getting patrol cops to that level,
it's not really a thing that can happen.
OK.
How do you become the best of the best?
The same way you get to Carnegie Hall.
practice, right? Training, lots and lots and lots of training with each other, all
training together. If you take cops and they're on a schedule that involves that
much training together, where are they not? They're not on patrol, which means
they're not the cops who show up first. To get them to the standard that Internet
Commandos were trying to hold them to, it takes so much training that if they had
been involved in that level of training, they wouldn't have been the ones that
went in. If you do that, you end up just creating another SWAT team, but the SWAT
team isn't the first group to show up.
And to be clear, the critiques were, I don't want to say unfounded because technically
yes, you know, you shouldn't flag somebody, you know?
But when you're talking about real life and it's a real situation and you're at the end
of it, they accomplished the mission.
And they're not top tier, they're cops and you can't have it both ways.
They can't be patrol officers and be trained to the level that allows them to meet the
standards that were being imposed on them.
And it's important to remember that this stuff is a perishable skill.
It's not like you can go and train for eight weeks and then never train again.
It's a constant thing.
And then the other thing that you have to keep in mind is the mindset that develops
when you constantly train to do that.
You don't want that person to be a patrol officer anymore.
So the goal of departments should not be to get every cop to that level.
The goal of departments should be to get them good enough to do it and complete the mission.
It's not that people are opposed to them training.
It's that getting them to the level that those critiques were based on is going to prohibit
them from being first on the scene.
It's not going, they won't be patrol officers anymore.
So it's self-defeating.
And I would imagine that the Green Beret, whose podcast you listen to, is probably going
to have a more colorful and probably more detailed answer, but that's the general part
of it there, is that the level of training it takes to get to the level that people apparently
wanted them to be at, it prohibits them from also being patrol officers.
It's, I guess, not just do they want them to do it quickly and successfully, they have
to look pretty doing it too.
I think that's kind of unreasonable.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}