---
title: Let's talk about SECDEF vs Republicans....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ofH1CYyyAo4) |
| Published | 2023/04/02 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Secretary of Defense appealed to Republicans in Congress to stop blocking promotions for higher ranking members of the US military, impacting around 160 officers.
- Republicans are blocking promotions because they are upset that the Department of Defense is supporting women in the military with leave time for family planning.
- Women make up about 20% of the military force, and the DOD wants to ensure their readiness.
- The holdup in promotions is causing ripple effects and downstream issues within the military.
- Troops moving around installations and promotions being associated with different commands are being hindered by the promotion block.
- The promotion block affects retirements, readiness, and the ability of the US military to respond to challenges.
- Republicans are being critiqued for focusing on controlling women rather than considering the broader implications of their actions.
- The Secretary of Defense emphasized how the promotion block is impeding readiness as the US faces complex geopolitical situations.
- The impact on retirement for high-ranking officers will worsen if the promotion block continues.
- The Republican Party's actions are seen as grandstanding and prioritizing political points over military readiness and personnel well-being.

### Quotes

- "It is about, once again, the Republican Party wanting to control women."
- "Republicans attempting to grandstand and score political points while jeopardizing U.S. readiness."
- "The legal mechanism that they used to do this is holding up."

### Oneliner

The Secretary of Defense appeals to Republicans to stop blocking military promotions, citing impacts on readiness and retirements, revealing Republican focus on controlling women rather than prioritizing national security.

### Audience

Congressional constituents

### On-the-ground actions from transcript

- Contact your Congressional representatives to express support for ending the promotion block (suggested)
- Join advocacy groups supporting gender equality in the military (implied)
- Organize community dialogues on the importance of military readiness and gender equality in the armed forces (implied)

### Whats missing in summary

The full transcript provides additional context on the impact of the promotion block on military operations and personnel well-being, along with a deeper dive into the political dynamics at play.

### Tags

#Military #Promotions #GenderEquality #Republicans #Readiness


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about the Secretary of Defense
and Republicans in Congress, and roadblocks
that are being thrown up, and promotions,
and what all of this is really about.
OK, so the Secretary of Defense basically
made an appeal to Republicans in Congress,
asking them to stop blocking promotions for higher ranking members of the US
military because right now they're doing that. They're basically putting
all on hold and it is dozens upon dozens, I want to say 160 officers right now are
being impacted by it. The reason Republicans are doing this, the reason
this holdup is occurring is the Republicans are mad that the Department of Defense is
standing by the women in the military and giving them leave time for family planning
or whatever and this has upset Republicans.
It's worth remembering that women now make up about 20% of the force, so DOD has a very
vested interest in making sure that those troops maintain a readiness level, and that's
a big part of the Secretary of Defense's appeal to stop this holdup.
talked about ripple effects where basically these promotions not happening
are holding things up and they're causing downstream effects. Is this true?
Yeah, absolutely. We've talked about it on the channel before, oddly enough, about
the same type of stuff. When we talked about the legislation when it
comes to family planning or different identities and stuff like that that a lot
of red states were passing, we talked about how some troops may not be able to
go to certain installations which could create a readiness issue. Which
basically, troops in the military move around a lot. They change the installation
they're at. When one moves to a new installation, another one has to come
in to fill that position. Promotions typically are associated with different
commands. You're getting a new job a lot of the time, especially when
you're talking about higher ranking officers. If they can't get promoted, it
makes it harder for them to go to where they're supposed to be. It also stops
somebody further down the chain from moving up and taking their spot. This
also has implications when it comes to their retirements and all kinds of stuff.
Now at this point we are talking about high-ranking officers. The impact to
their retirement isn't going to be a whole lot. It won't really be noticed
as long as this gets resolved. If it goes on, their time and grade, it will cause more and more
issues. Aside from that, the Secretary of Defense pointed out how it's impeding readiness in the
sense of people not being able to take the command that they need to be in as we move into a near
peer situation, or I think the term he used was geopolitically complex, something along
those lines.
But basically, stopping the promotions slows the ability of the US military to respond
to anything.
So, is that true?
Yeah.
Everything in what the Secretary of Defense said, that's accurate.
And it is about, once again, the Republican Party wanting to control women.
That's the entirety of the issue.
Republicans are framing it more around, you know, we didn't authorize this and so on
and so forth.
Well, I mean, that's all fine and good.
But the legal mechanism that they used to do this, from everything that we've seen
so far is holding up. So it's another example of Republicans attempting to grandstand and
score political points with their lesser informed members of their base while jeopardizing U.S.
readiness, people's retirements, a whole bunch of different things.
So it's just par for the course, honestly.
So anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}