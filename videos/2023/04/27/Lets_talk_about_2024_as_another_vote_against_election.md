---
title: Let's talk about 2024 as another "vote against" election....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=cifoBJnwdVA) |
| Published | 2023/04/27 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The 2024 election is shaping up to be driven by negative voter turnout, meaning people voting against somebody rather than for somebody.
- Biden is the presumptive nominee for the Democratic side, and Trump for the Republican side.
- A poll by NBC News shows favorability ratings: BLM 38%, Biden 38%, Democratic Party 36%, Republican Party 33%, MAGA 24%, Trump 34%.
- Unfavorability ratings: BLM 40%, Biden 48%, Democratic Party 46%, Republican Party 43%, MAGA 45%, Trump 53%.
- The gap between favorability and unfavorability is critical; BLM is 2% underwater, Biden, Democratic Party, and Republican Party are 10% underwater, MAGA 21%, Trump 19%.
- The Democratic Party has an edge assuming normal voter turnout.
- The question is who will become more likable over time: Trump and MAGA or Biden and BLM.
- Black Lives Matter is more favorable than the other entities, with a smaller gap between favorability and unfavorability.
- There's a clear divide in the Republican Party between normal Republicans and the MAGA movement.
- Winning the primary without Trump is impossible, but winning the general with him is challenging.

### Quotes

- "You can't win a primary without Trump. You can't win a general with him."
- "Black Lives Matter is much more favorable than anything else."
- "A majority of Americans view Trump unfavorably."

### Oneliner

The 2024 election is likely to be driven by negative voter turnout, with Trump's unfavorability potentially playing a significant role in the outcome.

### Audience

Voters

### On-the-ground actions from transcript

- Analyze and understand the favorability and unfavorability ratings of different entities in the upcoming election (suggested).
- Stay informed about how public opinion shifts and impacts the political landscape (suggested).
  
### Whats missing in summary

Detailed analysis of the potential impact of negative voter turnout on the 2024 election. 

### Tags

#2024Election #NegativeVoterTurnout #BlackLivesMatter #Trump #MAGA #PoliticalAnalysis


## Transcript
Well, howdy there, internet people, Lidsbo again.
So today we are going to talk about the 2024 election and how it's shaping up to
be driven kind of almost entirely by negative voter turnout, meaning people who
are showing up to vote against somebody rather than showing up to vote for somebody.
because, you know, Biden has announced the presumptive nominee from the Republican side
right now is Trump and there are touchstones, you know, there are cultural things that get
associated with each party. You have the MAGA movement and you have BLM and then you have
the parties themselves, Republican and Democrat.
Okay, so NBC News, there's a poll.
It's pretty telling, and it gives us a good starting point for the 2024 election.
We're going to run through the percentage of people that view each of
these entities as favorable first. BLM 38% favorability, Biden 38%, Democratic
Party 36%, Republican Party 33%, MAGA 24%, Trump 34%. Okay, so that is the favorability.
this is the amount of people who hold a happy view. They really like
these entities. If you notice, none of these have a majority. So it does appear
to be another election that will be driven by negative voter turnout.
Now let's look at the unfavorability. BLM 40%, Biden 48%, the Democratic Party 46%,
Republican Party 43%, MAGA 45%, Trump 53%. We finally got a majority. A majority of
Americans view Trump unfavorably. Now what really matters is the gap between
these two numbers. How underwater are these entities? That's what's really
going to come into play. So we're going to go through those numbers now. BLM, 2%.
Biden, 10%. Democratic Party, 10%. Republican Party, 10%. MAGA, 21% underwater.
Trump, 19%. Assuming a normal voter turnout, the Democratic Party has an edge.
Going into this, now the question is who is going to become more likable over
time? Will it be Trump and MAGA or will it be Biden and BLM, the Democratic and
Republican parties, Democratic and Republican parties? They're pretty much
the same. They're both 10 points. This is a starting point to kind of gauge other
polling by because people will change back and forth as different developments
occur. People will go up and people will go down in the polls. The important thing
to remember is the overall view rather than just the primary view when
those numbers start coming out and the gap. A 21-point space between favorability
and unfavorability for MAGA, that's huge. That's, to use Trump's word, that might be
insurmountable. 19 points for Trump himself, that's not much better. So it
depends on how dislikable the opposition is. That looks like what is going to run
this election season. Not exactly how things are supposed to work, but that is
the situation we are in. I would note that by this poll, Black Lives Matter,
yeah, it is much more favorable than anything else. It has a smaller gap. It's
only two points under water where everything else is double digits. That
might be something that people should pay attention to. Generally speaking, I
think that you can see a divide in the Republican Party between normal
Republicans being only 10 points under water and the MAGA movement being more
than double that, 21 points.
You can't win a primary without Trump.
You can't win a general with him.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}