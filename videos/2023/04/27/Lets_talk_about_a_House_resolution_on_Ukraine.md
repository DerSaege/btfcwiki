---
title: Let's talk about a House resolution on Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=mcaowdpTBOU) |
| Published | 2023/04/27 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The US House is considering a resolution regarding Ukraine, which is expected to garner significant attention.
- Congress frequently uses resolutions to establish US policy, but sometimes they go beyond their scope.
- A current resolution aims to prevent a repeat of the mistake made on September 1st, 1939, when Germany invaded Poland.
- The resolution asserts the US policy is to support Ukraine in regaining its 1991 borders, including Crimea.
- While some see this goal as impossible, others believe it is difficult but achievable.
- The decision on victory conditions should rest with Ukraine, not dictated by the US.
- Beau suggests that the resolution should stop at affirming US support for Ukraine's victory.
- The US should not dictate Ukraine's actions or push them to continue fighting based on specific conditions set by the resolution.
- Beau urges the House Foreign Affairs Committee to revise the resolution and remove the clause defining victory conditions for Ukraine.
- Ultimately, Beau stresses that it is not the place of the US to dictate another country's decisions or define their victory conditions.

### Quotes

- "They get to make the decisions on what victory is."
- "It is not the place of the United States to encourage them to fight for, again, this is something that there are a lot of military analysts who will tell you it is impossible."
- "The United States doesn't get to make that decision."

### Oneliner

The US should not set victory conditions for Ukraine, respecting their autonomy and decision-making in conflict resolution.

### Audience

US House Foreign Affairs Committee

### On-the-ground actions from transcript

- Revise the resolution to remove the clause defining victory conditions for Ukraine (suggested)
- Advocate for respecting Ukraine's autonomy in determining their victory conditions (implied)

### Whats missing in summary

The full transcript provides additional context on the potential implications of the US House resolution on Ukraine and the importance of respecting Ukraine's sovereignty in determining their course of action.


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about a potential resolution in the US House and
Ukraine, and we are going to talk about how this is certain to make headlines.
It is definitely going to draw attention from all over the place.
Something that is relatively common for Congress to do is to make these
resolutions and to basically set US policy through them. Say, hey, this is
what we believe. Generally speaking, okay, not a huge deal. Sometimes they get a
little ambitious and sometimes they plainly exceed their brief. A resolution
is being put forward right now, and it still has a number of steps to get through.
It basically says, hey, we can't make the same mistake that we made September 1st, 1939,
when Germany went into Poland.
The part that is going to draw the most attention, it affirms that it is the policy of the United
States to see Ukraine victorious against the invasion, and restored to its internationally
recognized 1991 borders."
Okay, so there are going to be a whole lot of people who watch this channel.
They're going to be like, yeah, that's right.
There are going to be some people who are more interested in military analysis who are
going to say that's impossible.
There are going to be some who say that's difficult, but not impossible.
For the record, I'm in that camp.
The 1991 borders would include Crimea.
For Ukraine to retake that, it's going to be hard, hard, okay?
It's not impossible, I know a lot of analysts that put that out there.
That's just not even something that can be done.
Anything can be done if you wish hard enough.
Or with enough will, resources, and time, anything can be done.
It's not impossible.
It's difficult.
But that's not actually the issue.
The issue is the United States does not get to set victory conditions for other countries.
If this becomes policy, and this is what the US supports, what happens if Ukraine wants
to take a peace deal without Crimea. I know right now that's not something they
would want, but that is their option, their prerogative, their choice. That is a
decision to be made in Ukraine, not in DC. If they choose to do something else does
Does the United States push them to keep fighting if it becomes policy?
It's that last sentence.
It's the and restored to its internationally recognized 1991 borders part.
The United States doesn't get to make that decision.
That is a decision for the people of Ukraine.
The rest of it, you know, it affirms that it is the policy of the United States to see
Ukraine victorious, period, end it there.
If you want to put out a resolution, fine, but end it there.
They get to make the decisions on what victory is.
They get to make those determinations.
It is not the place of the United States to encourage them to fight for, again, this is
something that there are a lot of military analysts who will tell you it is impossible.
I don't believe that, but I'm also not there fighting.
It's not my choice.
It's their choice.
The House Foreign Affairs Committee needs to look at this, it needs to cut that sentence.
It needs to be a little bit shorter than it is.
Regardless of your personal opinion on what Ukraine should do, it is not the US's place
to set victory conditions and it certainly should not be described as US policy.
This needs to be changed.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}