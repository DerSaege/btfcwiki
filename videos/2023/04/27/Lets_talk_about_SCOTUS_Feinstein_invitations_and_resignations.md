---
title: Let's talk about SCOTUS, Feinstein, invitations, and resignations....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=DA5nTUA0-MI) |
| Published | 2023/04/27 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Chief Justice Roberts declined the Senate Judiciary Committee's request to talk, prompting questions about why the Committee couldn't subpoena him.
- The Senate Judiciary Committee is facing obstacles in addressing concerns about ethics at the Supreme Court due to the "Feinstein situation."
- Senator Feinstein's absence and the Republican Party's block on a temporary replacement prevent the Committee from issuing a subpoena.
- The only way to get someone new on the Committee is for Feinstein to resign, and pressure for her resignation may increase.
- Without Feinstein's replacement or return, the Committee lacks the votes needed to issue a subpoena.
- Congressional subpoenas can be difficult to enforce and may be especially challenging for a Supreme Court Justice.
- Feinstein's absence hinders the Democratic Party's ability to push judges through without the House.
- With the combination of slow judge approvals and lack of subpoena power, there may be mounting pressure for Feinstein to resign.
- The Senate Judiciary Committee is currently unable to take significant action due to the lack of votes and Republican opposition.
- Beau expresses disbelief at the lack of interest from some Republicans on the Committee in addressing ethics concerns at the Supreme Court.

### Quotes

- "They don't have the votes to get a subpoena."
- "There may be renewed pressure on Feinstein to actually resign."
- "It kind of stops the Democratic Party from doing one of the few things they can actually do without the House."
- "I kind of do expect there to be a little bit more pressure for her to just resign."
- "That to me just kind of boggles the mind that those Republicans on the Senate Judiciary Committee are comfortable, which is being like, yeah, we don't care."

### Oneliner

Chief Justice Roberts declined to talk, Senate Judiciary Committee faces hurdles due to Feinstein's absence; pressure for resignation may increase, hindering Democratic Party actions.

### Audience

Political Activists

### On-the-ground actions from transcript

- Contact Governor Newsom to urge him to appoint an interim replacement for Senator Feinstein (suggested).
- Reach out to Senator Feinstein's office to express support for her resignation (implied).

### Whats missing in summary

The full transcript provides a detailed explanation of the obstacles faced by the Senate Judiciary Committee in addressing ethics concerns at the Supreme Court and the potential impact of Senator Feinstein's resignation on their ability to take action effectively.

### Tags

#SupremeCourt #SenateJudiciaryCommittee #EthicsConcerns #SenatorFeinstein #ResignationPressure


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about the Supreme Court
and the Feinstein situation
and the possibility of resignation
and why the Senate Judiciary Committee
can't just look at the Supreme Court
and be like, you better come talk to me.
So if you have missed the news,
Chief Justice Roberts has politely declined
Senate Judiciary Committee's request to come talk to them. As soon as that happened, a whole bunch
of people wanted to know why the Judiciary Committee doesn't just say, yeah, here's your subpoena come
talk to me. And somebody pointed out that in the video where I was talking about this, I said they
couldn't do that because of the Feinstein situation, but I never explained what the Feinstein situation
was. Fair enough. So there are growing concerns about ethics at the Supreme
Court. The Senate Judiciary Committee would like to ask some questions. Under
normal circumstances, because the Democratic Party controls that committee,
they could just issue a subpoena. The problem is that Diane Feinstein, Senator
Feinstein, has been ill and absent. Can't vote. The Republican Party has blocked
any temporary replacement, which means the only way to get somebody else on to
that committee is for her to resign. Now she's not going to run again and that
announcement kind of dulled the pressure for her to resign at the time because
it wasn't pressing. Now it is becoming more and more pressing so there may be
be renewed pressure on Feinstein to actually resign.
If that occurs, Governor Newsom would appoint her replacement, an interim replacement.
So until something like that occurs, either she returns or there is a new appointment
made, they don't have the votes to issue a subpoena because the Republican Party has
no interest in pursuing ethics, I guess. I guess they don't have any interest in
this topic. And they have gone out of their way to make sure that a
replacement can't be put forth. So they don't have the votes to get a subpoena.
Now, it is worth noting that congressional subpoenas are notoriously hard to enforce.
They can be drug out for a very, very long time.
And that might definitely be the case if you're talking about a Supreme Court judge.
You know, a Justice on the Supreme Court not wanting to go.
I'm not sure how federal judges would rule on that.
So that's another thing to keep in mind as everybody pushes for a subpoena.
It may not be a quick process no matter what happens.
Another thing to keep in mind about the Feinstein situation is that in her absence, it kind
of stops the Democratic Party from doing one of the few things they can actually do without
the House, which is push judges through.
The combination of the inability to move the judges through quickly and the lack of subpoena
power now, I kind of do expect there to be a little bit more pressure for her to
just resign, to leave office early. I'm not sure, but it seems like calls might
get made about that. But until something changes there, there's really nothing
that the Senate Judiciary Committee can do. They don't have the votes. The
Republican parties made it clear that they don't want to know, I guess.
That to me just kind of boggles the mind that those Republicans on the Senate Judiciary
Committee are comfortable, which is being like, yeah, we don't care, and not moving
forward on that.
seems odd but that is where we're at. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}