---
title: Let's talk about Russia not being able to deny it anymore....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=csikoLiV11E) |
| Published | 2023/04/08 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Russia can no longer deny recent events after downplaying or denying them for months.
- An investigation into an incident at a cafe revealed the bombing of a military blogger critical to Russia's information operations.
- The blogger was believed to be giving a class on writing to further Russian narratives before the explosion.
- Russia has been blaming recent unfortunate events on anything but acknowledging them as operations.
- Possible perpetrators behind the attacks include Ukrainian special services, a rebel group, or an internal power struggle within the Russian elite.
- The recent cafe bombing indicates an internal power struggle or potentially a rebel group's involvement.
- Speculations include the bombing as a message to a high-ranking figure associated with the cafe.
- The individual in custody may have been manipulated into carrying out the attack, as suggested by her behavior.
- The incident will likely result in increased security measures in Russia and divert resources from the war effort.
- This event confirms suspicions of covert operations in Russia, leading to a shift in public acknowledgment of such incidents.

### Quotes

- "Russia can no longer deny recent events."
- "We no longer have to just repeat the official statements with sarcasm."
- "They have to acknowledge it, which means it's going to become public inside Russia."

### Oneliner

Russia can no longer deny covert operations following a cafe bombing, revealing internal power struggles and confirming suspicions.

### Audience

International Observers

### On-the-ground actions from transcript

- Monitor developments in Russia and stay informed on emerging details (implied).
- Support efforts to increase transparency and accountability in Russia (implied).

### Whats missing in summary

Exploration of potential international implications and responses to Russia's acknowledgment of covert operations.

### Tags

#Russia #CafeBombing #InternalStruggle #CovertOperations #Confirmation


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we're going to talk about Russia
and how Russia can, well, no longer deny what's happening.
They have successfully downplayed
or denied certain events for months.
And they finally hit a point where something happened
and they have to admit what's going on.
But with that admission comes more questions than answers, really.
So if you have no idea what I'm talking about, over the last few days,
there's been an investigation into something that happened at a cafe.
A device went off and took out what is being described,
he's being described as a military blogger.
That is a true statement, but it doesn't really do him justice.
He wasn't just some blogger.
This was a person who was widely believed
to be involved and critical to Russia's information operations.
My understanding is that if it hadn't
have gone the way it went at that cafe,
he would have actually kind of been giving a class
how best to write to further Russian narratives. He wasn't just some random
blogger and that's important when you start trying to look at what actually
happened. But because there was video and because this was very dramatic they
can't just say it was an electrical thing or it was a spill or it was an
accident. For months there have been a series of unfortunate events that have
occurred inside Russia that the Russian government has repeatedly just blamed on
anything other than what was obvious, which was that they were operations. They
attacks of some kind. Now, who's behind them? There are three real options. The
first is, well obviously, it's Ukraine. Ukrainian special services doing
unconventional stuff inside of Russia. Possible. Another is that it is a
a homegrown rebel group that is opposed to the war, or opposed to Putin, or both, or
multiple groups like that.
Also possible.
Then there is the idea that it is an internal power struggle between the Russian elite.
Also possible.
In fact, I would be willing to bet just about anything that out of the giant series of these
events at least one is something that could be attributed to each one of these.
They're probably all true.
Right now people are focused on the cafe and what happened there.
I don't know.
It seems most likely that this was an internal power struggle between the Russian elite,
although you can't rule out a Russian rebel group.
I don't think this was Ukrainian special services just because of where it occurred.
That's a pretty surveilled area.
That seems unlikely.
Now you do have the chairman of the Russian state Duma saying that it was Biden.
Those dark Brandon memes are definitely working.
That one, no, that's not in play.
That's not real.
But any of the other ones could be.
Now what does this really mean?
First we're not going to know any time soon what actually happened here.
They have somebody in custody.
I am leaning towards the idea that the person they have in custody did not know what they
were bringing into the cafe, that they were manipulated into it, which occurs pretty often.
I think the strongest evidence of that was the fact that she didn't immediately leave.
Generally, if you're bringing something like that into a place and you know what's about
to happen, you don't stick around for it.
But Russian state security has arrested her and they seem to be open to the idea that
maybe she didn't know what was going on, which I mean that seems pretty obvious at
this point.
But as far as who was really behind it, you don't know.
Because this could have been a message to the head of Wagner, who doesn't own the cafe
anymore.
I think his son-in-law or ex-son-in-law owns it, something like that.
There's a connection there.
There's also a rumor that he was supposed to be present.
I don't know that that's true, though.
If it is an anti-war group from within Russia, the target makes more sense.
The blogger makes more sense.
He was super-nationalistic and probably said a lot of things that they disagreed with beyond
just his support for the Russian invasion of Ukraine.
At the end of this, we're probably not going to know who was behind it, not anytime soon.
But what we do know now is that Russia has finally hit the point where they can't pretend
that this isn't happening.
They can't pretend that all of their critical buildings have electrical faults.
They're going to have to acknowledge it, which means it's going to become public inside
Russia, which means increased security measures, which means less resources for the war.
So there are going to be downstream effects from this.
But the big takeaway from this is that all of the suspicions that everybody had, they
are pretty much confirmed now.
We no longer have to just repeat the official statements with a whole bunch of sarcasm because
It is now admitted that these types of things are occurring.
That's your big change from this.
Anyway, it's just a thought.
So have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}