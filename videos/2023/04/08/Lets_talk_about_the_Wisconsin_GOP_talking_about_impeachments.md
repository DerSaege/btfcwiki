---
title: Let's talk about the Wisconsin GOP talking about impeachments....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=5ZMjc-YTsJ0) |
| Published | 2023/04/08 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the recent events in Wisconsin, particularly focusing on the Republican-controlled legislature.
- Mentions the Supreme Court election in Wisconsin where a progressive judge won by a significant margin.
- Points out the possibility of the legislature overriding a governor's veto or impeaching a judge due to the seats they picked up.
- Raises concerns about Republicans potentially disregarding the will of voters and undermining democracy.
- Emphasizes the repercussions for Republicans in the upcoming elections if they take authoritarian measures.
- Suggests that going against the voters' will may lead to significant losses for the Republican Party in 2024.
- Urges Republicans to reconsider their actions and the impact on voters who crossed party lines in the previous election.
- Warns that upsetting voters now could result in a tougher race for Republicans later on.
- Expresses a belief that such actions could ultimately benefit the Democratic party.
- Advises leaving the judge alone to avoid potential electoral consequences.

### Quotes

- "If they go against the will of those voters and they try to engage in authoritarian measures from the legislature, they'll pay for it in 2024."
- "You should probably just leave that judge alone."
- "They will have angered voters. They will have canceled out the will of voters who voted for them and crossed party lines to vote for her."
- "I'm fairly certain that you'll end up handing the Democratic party a trifecta."
- "Her massive win means that people that voted for them crossed party lines to vote for her."

### Oneliner

Beau warns Wisconsin Republicans of potential backlash in upcoming elections if they disregard voters' will and undermine democracy by impeaching a judge.

### Audience

Wisconsin voters

### On-the-ground actions from transcript

- Reassess actions and decisions considering the impact on voters who crossed party lines (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the political situation in Wisconsin and offers insights into potential electoral consequences for the Republican Party.


## Transcript
Well, howdy there, internet people, it's Bo again.
So we're going to talk about Wisconsin again
and some of the ideas that are being tossed around
by the Republican party up there and math.
So we have talked about the election up there.
There was a Supreme Court election, very important,
And the more progressive judge won.
Big.
Double digits.
Big, big win.
But the legislature there, it's a Republican controlled
legislature.
And they picked up some seats.
Enough seats to where, in theory, they
could override a governor's veto.
Or, as rumors suggest, something that might be entertaining,
impeach a judge.
I mean, I get it.
The numbers are there on a party line vote.
I understand that.
That math checks out.
Theoretically, it's something that they could do.
I would like to point out that most of the comments that
has spurred these rumors were actually
made talking about a lower court position.
But the Republican Party isn't going out of their way to shut these rumors down.
So let's go through it a little bit.
She, the Supreme Court Judge, Protasewicz, won by double digits.
Massive margin.
Did the seats that they pick up win like that?
They didn't, right?
They didn't.
Her massive margin, her massive win, means that people that voted for them crossed party
lines to vote for her.
If Republicans decide that they want to impeach a judge, throw away the will of the voters,
undermine democracy, the republic, constitution, all that stuff, if they decide they want to
that route, they have the votes to do it on a party line vote. But what happens next? What happens
in 2024? They will have angered voters. They will have canceled out the will of voters who voted for
them and crossed party lines to vote for her. They will be upsetting voters who have already shown
they are willing to cross party lines. I'm fairly certain that those voters might cross party lines
in other races, like in the legislature. Republicans here need to do the math.
unless they are in a very, very red district where they have a 10-point
lead, a double-digit lead, if they do this they're in trouble. They're going to have
a much harder race because they'll be upsetting voters who have already
determined that the authoritarian push from the Republican Party isn't
something they want. They demonstrated that in this election. They showed by
double digits that that's not what they want. If they go against the will of
those voters and they try to engage in authoritarian measures from the legislature, they'll pay for it
in 2024. Me, personally? Do it. Do it. I'm fairly certain that you'll end up handing the Democratic
party a trifecta. If they discount the will of the voters of Wisconsin to that
degree, those voters will not vote for them again. A massive amount of them. She
won by double digits. You should probably just leave that judge alone. Anyway, it's
It's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}