---
title: Let's talk about new Fox decisions and the pillow guy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=OkmoOSYJq48) |
| Published | 2023/04/08 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explaining about Fox, rulings, and the Murdochs, and related topics.
- Fox isn't doing well in the early stages before the trial.
- The recent ruling suggests the Murdochs may testify.
- The Fox legal team wanted to avoid the Murdochs testifying.
- Surprisingly, the case won't involve discussing January 6th events.
- The case focuses on whether Fox defamed Dominion.
- Texts and comments from Tucker's team surfaced regarding the pillow guy.
- There were concerns about what the pillow guy might say on air.
- Financial considerations influenced decisions about what to air on Fox.
- Beau hopes this information doesn't get buried amidst other news.

### Quotes

- "Some texts and the idea that decisions related to the accuracy of what might be said on the air on Fox were heavily influenced by financial considerations, that seems like something that should be talked about."
- "It keeps people focused on the matter at hand."
- "This is about whether or not Fox defamed Dominion."

### Oneliner

Beau explains Fox's struggles, potential Murdochs testimony, and financial influences on air content while urging attention to these critical issues.

### Audience

News consumers, activists, watchdogs

### On-the-ground actions from transcript

- Share and raise awareness about how financial considerations impact media content (implied).
- Support unbiased media outlets that prioritize truth over financial gain (implied).

### Whats missing in summary

Context on the potential implications of media decisions influenced by financial considerations.

### Tags

#FoxNews #Murdochs #MediaManipulation #FinancialInfluence #NewsCoverage


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Fox and rulings
and the Murdochs and then some other stuff
that relates to Fox but doesn't necessarily relate
to the overall thing, at least at this point,
that may change in the future.
So if you have no idea what I'm talking about,
We've been talking a lot about the case that Fox is involved with and how it's moving forward and
how in the early stages before it goes to trial, which I think is supposed to happen later this
month, Fox isn't doing well. They're not racking up a lot of wins in the early proceedings here.
Now, the more recent ruling is that basically the Murdochs may end up in the witness box.
They may end up there talking.
Judge indicated that if the subpoena went out, well, it wouldn't be quashed.
So we may get to hear directly from them.
It's worth noting that this would be in addition to the expected appearance of a lot of the
hosts to include Carlson, Hannity, all of them.
That was something that the Fox legal team, as far as the Murdochs talking, they were
trying to avoid that.
But it does look like that's probably going to happen.
And one of the more surprising things, at least something I think people will find surprising,
during this case they will not talk about what happened on January 6th.
The judge has basically said, yeah, that's not part of this.
Now for us watching it as spectators to this doesn't make sense because if the case is
about the allegations of Fox knowingly lying and and defaming and creating this
atmosphere and it leads to the sixth. The thing is this is a simple case not a
criminal one. This is about whether or not Fox defamed Dominion. What happened
on January 6th has nothing to do with that. So as weird as the judge saying
that is, it's actually the right call.
It keeps people focused on the matter at hand.
So that's all the information coming out of that case.
However, there is something kind of related to it that I'm a little worried may end up
getting buried because of all of the other news going on this week.
Some texts and conversations have surfaced from people that were on Tucker's team or
coordinating with Tucker's team, people at Fox.
And it was about bringing the pillow guy on after the inauguration, so after the 6th.
And there are some real gems of quotes in there talking about the pillow guy, referring
to him as off the deep end or completely crazy.
There were also some expressing concern about what he might say on their error.
But there were also comments about how he was one of their biggest advertisers.
And it certainly appears that signaling to the pillow guy that they weren't going to
desert him had a lot to do with it.
This was shortly after, I think he had just been banned from Twitter or something.
And just because this is something that has to do with information consumption and how
the media works, I don't think this should get lost.
I would hope that other news outlets would see this and draw attention to it, but you
never know.
So those texts and the idea that decisions related to the accuracy of what might be said
the air on Fox were heavily influenced by financial considerations, that seems
like something that should be talked about. Seems like something that a lot
of people might should know. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}