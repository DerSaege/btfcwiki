---
title: Let's talk about Cruz, a recording, and a commission....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=mgyvYWqqB6E) |
| Published | 2023/04/26 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses Senator Cruz and a recording that has been publicized regarding a commission to address perceived issues with the 2020 election.
- He mentions that some view this commission as potentially furthering a coup, though he questions if there's anything criminal in the recording.
- The recording features ideas Senator Cruz presented publicly before, possibly offering legal cover due to his position as a senator.
- Beau suggests that despite being election fodder and problematic, the recording may not be as significant as portrayed.
- There are reports indicating that the special counsel's office will acquire these recordings, hinting at more candid recordings to surface.
- Beau anticipates more revealing recordings to emerge, potentially causing real issues, but he doesn't see the current one as a major story.
- While he acknowledges the importance of the recording, Beau cautions against overreacting based on social media commentary and urges a measured response.

### Quotes

- "It's worth noting that a lot of what's on this recording that people find so shocking today, he said in public."
- "I think this might be more of a taste test of what's to come than anything else."
- "I don't really see this as the blockbuster story that it's being made out to be."

### Oneliner

Beau addresses a controversial recording involving Senator Cruz, hinting at potential legal nuances and downplaying its immediate significance, anticipating more revealing recordings in the future.

### Audience

Political analysts, concerned citizens

### On-the-ground actions from transcript

- Stay informed on political developments and implications (exemplified)
- Monitor for further revelations and recordings (exemplified)

### Whats missing in summary

The full transcript provides additional context on the controversy surrounding Senator Cruz's recording and hints at potential future developments, encouraging vigilance in following the story.

### Tags

#SenatorCruz #Controversy #ElectionIssues #PoliticalTransparency #FutureDevelopments


## Transcript
Well, howdy there Internet people, Lidsabow again.
So today we are going to talk about
Senator Cruz, a recording that has surfaced,
and whether or not it's as big a deal
as the media is currently making it out to be.
If you don't know what I'm talking about,
MSNBC got their hands on a recording.
I believe it came from the Fox News whistleblower.
There is a recording of Senator Cruz, and in that, he describes a commission that would
go through the different states if they had perceived issues with the 2020 election, and
that that commission would then report on findings.
There are a lot of people viewing this as a mechanism to further a potential coup.
And I get what they're saying.
It's definitely not a good look.
But at the same time, I don't know that there's anything criminal in that recording.
He is a senator.
He is a senator and that does give him a lot of legal cover.
It's not me listening to it without something else, major, emails, other recordings, stuff
like that, I don't know that it is as huge a deal as the media is making it out to be.
I mean, it's worth noting that a lot of what's on this recording that people find so shocking
today, he said in public, in fact, if I'm not mistaken, some of these ideas were presented
on the floor in Congress.
And that in and of itself would give him a lot of immunity through the Speech and Debate
Clause.
I don't know that it is as important as it is being made out to be.
Certainly election fodder, certainly problematic, not a good look, and I'm sure that there are
a lot of people who feel that it doesn't speak very highly of his view of democracy.
And I don't know that any of what's on that recording rises to the level of criminality.
It is worth noting that there are reports that suggest the special counsel's office
is going to be taking possession of these recordings.
It's also worth noting that we don't know that the reason they're doing this is the
Ted Cruz recording.
My understanding is that there are a lot of recordings.
I would buckle up because I have a feeling there might be more recordings coming that
are incredibly candid and might lead to some real issues.
I think this might be more of a taste test of what's to come than anything else.
So it might turn into something, but based on what we've seen so far, I don't really
see this as the blockbuster story that it's being made out to be.
I mean it's certainly interesting, it's something that people need to know about, but a lot
of the social media commentary, you know, showing Jack Smith now, you know, cuffing
up crews. I don't know that that would be my first reaction to that recording. Anyway,
just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}