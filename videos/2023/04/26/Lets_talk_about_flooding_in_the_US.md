---
title: Let's talk about flooding in the US....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=R3KNCH4F5dg) |
| Published | 2023/04/26 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing internet people about snow melt and flooding in California and along the Mississippi.
- Snowmelt from the Northern part heading down towards the rivers, causing flooding warnings.
- 20 gauges along the river already in major flood stage, with more expected.
- Urging people to have their flood plans ready and be cautious.
- La Crosse in Minnesota expecting a crest at 16.1 feet, with a possibility of further rise.
- Yosemite Park closing until at least May 3rd due to snowmelt, with warnings for hikers about the wet and muddy conditions.
- Extreme weather becoming the norm and increasing with climate change.
- Mitigation is necessary for the future as extreme weather events will worsen.
- Alerting to the need for action to prevent worsening situations in the future.
- Urging awareness and preparation for the increasing frequency and severity of extreme weather events.

### Quotes

- "The United States is going to have to come to terms with the fact that extreme weather is no longer extreme."
- "We're going to start seeing it more and it's gonna get worse if we don't do something."
- "It's gonna be wet, it's gonna be muddy, it's gonna be nasty."
- "We can mitigate in the future."
- "Just understand it's gonna be the norm."

### Oneliner

Beau addresses internet people about the increasing frequency and severity of extreme weather events, urging preparedness and mitigation for the future.

### Audience

Residents in flood-prone areas

### On-the-ground actions from transcript

- Prepare a flood plan and ensure readiness for potential flooding (implied)
- Stay cautious and aware of flood warnings in your area (implied)
- Stay informed about the weather conditions in regions prone to extreme weather events (implied)
- Support and advocate for climate change mitigation efforts in your community (implied)

### Whats missing in summary

Beau's engaging delivery and emphasis on the need for immediate action to address climate change impacts. 

### Tags

#ExtremeWeather #ClimateChange #Flooding #Preparedness #Mitigation


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about snow melt
and flooding in a couple of different places
in the United States, California and along the Mississippi.
So we'll start with the Mississippi side of things
for people in the South.
Right now we are talking about way up
in the Northern part.
the snowmelt is heading down towards the rivers.
This is something that I know y'all go through it up there like every year.
A lot of warnings have gone out that this year may be worse.
There's already flooding in some areas, but
the part that kind of got my attention is that those little gauges along the river,
20 of them are already in major flood stage, and it's not over.
So make sure that you have your stuff together. Whatever your plan is for the big floods, when they happen,
again, I know y'all are kind of used to this, just make sure that you have your stuff together.
And that you know what to do. I think La Crosse, up in Minnesota, is looking at cresting, I think today,
at 16.1 feet and it's expected that it might even go up later from there. So
just be careful, be cautious. On the other side of the country in Yosemite Park,
the park is shutting down on Friday and is not expected to reopen until at least
May 3rd, that is also due to snow belt. The wild winter and then spring that
occurred, you're looking at a lot of snow coming down all at one time.
And the Park Service is already saying that even once it opens, hikers need to
be ready. It's gonna be wet, it's gonna be muddy, it's gonna be nasty. And they
They are not ruling out having to extend that closure or have another closure later.
The United States is going to have to come to terms with the fact that extreme weather
is no longer extreme.
It's going to be the norm.
It's going to start being the norm.
We're going to start seeing it more and more often, things that we once considered abnormal.
And it will get worse with climate change.
And that is something that is, it's already happened.
It's going to get worse from where it is.
There's nothing we can do at this point to change that, but we can mitigate in the future.
you start seeing this stuff happen more and more, just understand it's gonna be
the norm. We're going to see it more and it's gonna get worse if we don't do
something. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}