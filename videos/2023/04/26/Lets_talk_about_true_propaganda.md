---
title: Let's talk about true propaganda....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=TGKI27bc9G0) |
| Published | 2023/04/26 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the confusion around the term "propaganda" and its true meaning.
- Defines propaganda as something designed to evoke a specific emotional response, not necessarily false.
- Illustrates how a factual statement can be turned into propaganda through presentation and context.
- Mentions how propaganda can be weaponized to manipulate emotions and spread doubt.
- Gives an example of humorous propaganda involving turret springs for tanks.
- Emphasizes that effective propaganda blurs the lines between truth and lies, creating uncertainty.
- Compares propaganda to the concept of "Flood the zone with stuff" in U.S. politics.

### Quotes

- "The best propaganda is true."
- "It's all true. Even the lies, especially the lies."
- "Creating doubt, uncertainty, and fear."

### Oneliner

Beau breaks down the true meaning of propaganda, showcasing how it manipulates emotions and blurs the lines between truth and lies to achieve specific goals.

### Audience

Analytical thinkers, media consumers.

### On-the-ground actions from transcript

- Analyze media messages for emotional manipulation (implied).
- Educate others on the tactics of propaganda (implied).

### Whats missing in summary

The full transcript provides a deeper understanding of propaganda and its impact on shaping beliefs and perceptions.

### Tags

#Propaganda #Manipulation #Truth #Emotions #Analysis


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about propaganda.
We're gonna talk about what it is.
Because in a recent video, I said something
and it led to a lot of questions.
And I realized there's confusion about something.
So we're gonna kinda go through it.
Like many terms, the word propaganda has lost its real meaning.
In a recent video, I said that the fellas, what they were doing was definitely propaganda,
but it was mostly accurate, and that led to a lot of confusion, because I think most people
today associate the term propaganda with something that is untrue, a misleading
narrative, something like that. It's not the case. It's not the case. Propaganda
is just something designed to make you feel a certain way. It doesn't have
to be false. A lot of propaganda is true. In fact, the best propaganda is true. So
what we're going to do is we're going to turn something from an objective fact
and we are going to turn it into true propaganda and then we're going to take
it a step further and actually weaponize it and show how it would be used if you
were truly a propagandist. Most of the time the fellas stop after that first
transformation. And they do most of this with humor and we'll actually get to
some of the things that they've done. Okay, so FSB estimates say 110,000
Russians have been lost or wounded in Ukraine. That is an objectively true
statement. Okay, the FSB, Russian intelligence, made those estimates. Fact.
That's it. That's true right there. Okay. If you take that exact same wording and
put it onto a meme with a bunch of little crosses or hash marks on it and
say FSB estimates, small letters, 110,000 lost or wounded in Ukraine. If you do
that, especially if you were to, I don't know, translate it to Russian, that's
propaganda. That is designed to make somebody feel a certain way, right? You're
trying to alter their emotional state and I'm being real careful about this
because I have another video on propaganda where I broke it down simply
into I think three or four different types and the PsyOps and civil affairs
guys down in the comments ate me alive because like in their world there's you
know 736,000 different types. Keep in mind this is a YouTube video not AIT. So
putting it on a meme using imagery to evoke an emotion. Objective statement
objective statement on the meme but that's designed to make you feel a
certain way about it. Now let's weaponize it. FSB estimates say 110,000
wounded or lost. The Ministry of Defense, Russian military command, says it's
30,000 or whatever. I don't know the exact number. What else are they lying
to you about? Two objectively true statements and a question. That is
something that could be used, translated, and would be best used
against the civilian populace in Russia and have it targeted towards
them. Seeding that doubt, that lack of willingness to commit to the cause
because they're not even gonna tell you the truth. Do you really want to send
your kid to the grinder? Okay so that's how something that is true can also be
propaganda. Let's talk about something the fellas actually did. A while back I
I got a message from somebody asking, genuinely asking this question.
Basically, they understood that Russia was having a hard time getting parts
for tanks. And somebody, a fellow,
told them the only thing they had a large quantity
of was turret springs. This person who sent the message
wanted to know where those were coming from and how
you know, how you could shut down that supply. If you don't know, turret springs aren't a thing.
That's, it's a joke about the turret flying off the top of a tank, the part that holds the gun
after it's, you know, been visited by St. Javelin.
That is normally the type of propaganda that they use. It's humorous, it's a joke.
And there are a lot of weird jokes that they use, but that demonstrates pretty
clearly how they can use accurate information and still create propaganda
or in the latter case, it's a joke. I mean the answer to the question, who's
supplying the turret springs to Russia? Ukraine. And they're delivering them at
rocket speeds. But that's how you can kind of look at that. So the term
propaganda, it's a lot like the T word. That gets applied to everything now. But
the real definition, what it really is, is the unlawful use of violence or threat
of violence to influence those beyond the immediate area of attack to achieve a
political, religious, ideological, or monetary goal. If it is not all of those
things, it's not the T-word. Propaganda in common usage has now kind of fallen
towards anything false. But that's not what it is. The best propaganda is true.
It's the most effective. And if you are doing it well enough, your opposition has
a hard time telling the truth from the lies. The lies of their own government,
the lies of media, your lies, they don't know.
And it turns into that whole, you know,
how much of it's true?
Well, it's all true.
Even the lies, especially the lies.
And they'll believe whatever they end up being conditioned
to believe by the propaganda that's surrounding them.
And they will pick and choose what's out there.
That's why there's a lot of, to use U.S. politics,
Flood the zone with stuff? It's the same premise. It's creating doubt, uncertainty, and fear.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}