---
title: Let's talk about Trump's plan for the homeless....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=kNJTTHNtgtM) |
| Published | 2023/04/20 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's plan to address homelessness includes making it illegal to be homeless, banning urban camping, and establishing tent cities.
- Trump lacks an understanding of the limits of federal power over local city ordinances.
- Homeless individuals under Trump's plan are given the option to go to jail or a tent city for psychiatric and substance abuse help.
- Beau questions the effectiveness of arresting people for being poor and the logistics of getting to work from a tent city.
- A significant portion of homeless individuals actually have jobs, challenging the stereotype of homelessness being solely due to a lack of character.
- Beau criticizes the punitive approach of turning housing for homeless individuals into tent cities, suggesting it's more about creating a class divide.
- He questions the logic of punishing individuals who may need treatment for mental health issues by placing them in a tent city.
- Trump's focus on punishment rather than genuine help for the homeless population is criticized by Beau.
- While Beau acknowledges the importance of providing housing and social services, he points out the flaws in Trump's approach.
- Beau concludes by expressing skepticism towards Trump's plan and its underlying motives.

### Quotes

- "You ban being homeless. You make it illegal."
- "Being homeless is not a lack of character, it's a lack of money, or it's a lack of being able to find housing."
- "It's not actually about helping. It's about giving their base somebody else to look down on."
- "Why do you want to punish them?"
- "His whole gimmick is finding a group of people to punish."

### Oneliner

Trump's misguided plan to address homelessness focuses on punishment rather than genuine help, showcasing a lack of understanding and compassion for those in need.

### Audience

Advocates, activists, policymakers

### On-the-ground actions from transcript

- Advocate for compassionate and effective solutions for homelessness (implied)
- Support organizations providing social services and housing to the homeless (implied)
- Educate others about the realities of homelessness and challenge stereotypes (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of Trump's plan to address homelessness and criticizes its punitive nature, lack of understanding, and potential harm to vulnerable populations.

### Tags

#Homelessness #Trump #PunitiveApproach #SocialServices #Compassion


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about Trump
and his plan to solve a real issue in the United States
and how that plan actually has
some pretty decent components for once.
But in announcing the plan,
he also announced that even after serving
full term in office, he has no clue what the office of the president is, and what the president
is actually supposed to do, and what they're capable of doing.
Trump announced a plan to deal with homelessness.
And it's interesting for a whole bunch of reasons, but my favorite part is when he just
bans being homeless. That's part of the plan. You ban being homeless. You make it
illegal. Make it arrestable. You ban urban camping where possible. His words,
where possible. That's cool and everything. I mean, I get it. I understand
what he's saying. Does he not understand that that's not federal? I mean, he does.
Somebody told him that. Somebody explained that him as president in
in charge of the federal government has absolutely no control over local city ordinances.
That's why he said where possible. Somebody tried to explain to him
that he wasn't actually a dictator and he just didn't want to accept it.
So, from there, after they get arrested for not having a home, they're given an option.
They can go to jail, or they can go to an internment camp, I'm sorry, a tent city.
A tent city where they will get psychiatric help, his term, substance help, like that,
and they'll help treat them.
Okay, he also said that the tent cities would be on cheap land, you know, away from everything.
So I mean, I have questions, I have questions about that.
I mean, first, I do have to point out the idea of just arresting people because they're
poor doesn't really add up.
I mean, that is definitely the idea of some trust fund billionaire that never had to struggle
a day in his life.
Why don't the poor just buy more money, right?
But beyond that, how are they supposed to get to work?
The people that, you know, choose not to go to jail and go to the tent city, how do they
get to their job?
I know that most of the people who would support a plan like this don't understand this question.
One fact, according to the lowest estimate I could find, one out of four homeless people
have a job.
Being homeless is not a lack of character, it's a lack of money, or it's a lack of being
able to find housing.
They have a job.
How do they get to it?
Once treatment is successful in this internment camp, how do they get a job?
It doesn't make sense.
And keep in mind, one out of four, that's the low end.
I found some that said 40 to 60% had jobs.
I'm all for providing social services, providing psychological help, providing substance help,
But to assume that that's needed when all stats say that that's not actually something
that is present in the entirety of the homeless population, that's a little weird.
The people that don't need treatment, what, they just go to jail and stay there forever?
I don't understand how it works.
But I do like the idea of providing social help.
The other thing I like is the idea of giving them housing.
He's right there.
This plan includes giving homeless people housing.
But rather than make it like housing that could be near their job or their family or
whatever, they want to turn it into a tent city.
They want to turn it into a punishment because it's not actually about helping.
It's about giving their base somebody else to look down on.
That's what it's about.
And I have an additional question.
If you are somebody who actually believes that everybody that is homeless needs treatment,
why do you want to punish them?
Why would you take somebody that has a psychological issue and make them suffer in a tent city?
Not just does the former president not understand the job he had for four years.
Not just does he not understand the way the US government works and how things are divided
under federalism.
His whole gimmick is finding a group of people to punish.
make his base feel like, well, we're doing better than those.
At least we're not in a tense city getting treatment.
He's right in the fact that treatment options should be available, absolutely.
And he's right.
Housing should be provided.
But putting it out in the middle of nowhere?
Making it a punishment?
people that, according to his theory, have mental health issues in a situation in which
they're in a tent city so they can suffer and be a warning to everybody else to make
sure they stay in line and don't become poor.
That doesn't really track for me, it's just more the same stuff from him.
something he can't do just like his wall, promising something he doesn't have the
authority to do, and even the parts that actually make sense, providing housing, providing
social help.
He manages to mess that up too.
Anyway it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}