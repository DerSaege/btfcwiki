---
title: Let's talk about Abbott's pardon problem....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=SXTk1A3nqQM) |
| Published | 2023/04/20 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Texas is experiencing shifting narratives, promises, and new developments regarding a BLM demonstration incident in 2020.
- The shooter in the incident was convicted, and the Texas governor hinted at pardoning him.
- Details from the shooter's phone revealed racist content, posing questions about the governor's stance.
- The framing of the incident paints the victim as a leftist BLM protester killed by a right-wing army sergeant.
- Despite revelations of racist content, Beau suggests that being racist isn't a deal-breaker for many Republicans.
- The victim, Foster, was a right-wing veteran and libertarian, not a leftist as initially portrayed.
- Correcting the narrative to show Foster's true political beliefs might impact the governor's decision on the pardon.
- Foster believed in protecting the first amendment with the second and understood state violence against marginalized people.
- Beau believes it's vital to correct the narrative and acknowledge Foster's true political identity.
- The importance lies in revealing that Foster was a right-wing veteran, challenging the initial portrayal of him as a leftist BLM protester.

### Quotes

- "He was right-wing. He just wasn't a bigot."
- "The framing is that the shooter, an army sergeant, killed a leftist communist socialist Obama-loving BLM protester."
- "Foster wasn't a leftist. He was a right-wing veteran. That's who was killed."

### Oneliner

Texas faces shifting narratives and promises regarding a BLM demonstration incident, where revealing the victim's true political identity challenges the governor's pardon stance.

### Audience

Texans, Activists, BLM Supporters

### On-the-ground actions from transcript

- Correct the narrative about Foster's political beliefs and honor his true identity (implied).
- Advocate for justice for Foster and ensure his story is accurately portrayed (implied).

### Whats missing in summary

Full insight into the nuances of the shifting narratives and the potential impact on the governor's pardon decision.

### Tags

#Texas #BLM #ShiftingNarratives #Pardon #RightWingVeteran


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about Texas and pardons
and promises and new developments
and shifting narratives and how the change in narratives
probably isn't over yet because there's still
a big piece of information that doesn't seem
to have hit the media yet, and it's going to matter in Texas.
OK, so what is going on?
Back in 2020, there was a demonstration, a BLM
demonstration in Texas.
During this, there was an incident
in which a person, Foster, was killed.
The shooter went to trial and was convicted.
The governor of Texas basically, almost immediately, said he was going to pardon him.
Information came out after that regarding the contents of the shooter's phone that
had a whole lot of stuff that I think a lot of people would consider racist.
People are asking, you know, what's Abbott going to do?
kind of painted himself into a corner. Okay, well, I mean, let's look at the framing and
let's look at it from the point of view of Texas. The framing is that the shooter, an
army sergeant, killed a leftist communist socialist Obama-loving BLM protester. That's
That's the framing.
Now the messages have come out, but let's be honest, I'm not saying that all Republicans
are racist.
I am saying that most Republicans decided being racist wasn't a deal breaker a real
long time ago.
So that's not going to hurt him, I don't think.
As it stands right now, I think Abbott will probably still go through with the pardon.
There's another huge piece of information that the media really hasn't latched on to yet,
that I think might change things. Foster, the victim, wasn't a leftist, wasn't a liberal.
In fact, he was also a right-wing veteran. No joke. I know that's not the narrative,
I know that that's not the information that went out initially and it doesn't seem like
it ever got corrected.
But Foster was in the Air Force and he was a libertarian.
He was anti-authoritarian right.
So much so that if I'm not mistaken, I think the Libertarian Party at the time actually
tweeted out mourning his loss.
He was right-wing.
So the image that Abbott is wanting to go for is pardoning somebody who killed a BLM
protestor.
That's what he's angling for.
As the narrative continues to self-correct, and it becomes he's pardoning a person who
killed a right-wing veteran, it may change things.
The reality is Foster was there living a right-wing bumper sticker, using the second to insure
the first.
He was right-wing.
He just wasn't a bigot.
I think it's actually kind of important that that part gets corrected, regardless of what
you think of libertarian politics or anything like that, this guy died for his beliefs.
And the narrative that's out there, it's not right.
He was not a leftist.
He was right-wing.
And he was there because he understood the habit of estate violence, typically being
visited upon marginalized people first. I think as that information comes out it
will make it harder for Abbott to go through with this pardon because it
really changes the dynamics in Texas and that's what this is about. It's about
shoring up support in Texas. My guess is that he's considering running for
president. But yeah, Foster wasn't a leftist. Was not a leftist. He was a
right-wing veteran. That's who was killed. Not some mythical enemy of the people
Anyway, it's just a thought.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}