---
title: Let's talk about why the Dominion settlement matters....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=20-iqziOPCI) |
| Published | 2023/04/20 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Fox News settlement matters and context is key to understanding its significance.
- Upton Sinclair's quote about understanding and salary dependency applies to the situation.
- Money is a significant motivator, as seen in Fox's actions and narratives.
- Bill O'Reilly predicts more problems for Fox due to their pursuit of profit over reporting news.
- The $787 million settlement is a colossal amount that will impact Fox significantly.
- Despite having an audience, Fox could still make money by providing accurate information.
- The financial consequences may lead Fox to reconsider editorial decisions for profitability.
- Beau believes that Fox will prioritize profit over journalistic integrity.
- The monetary penalties may force Fox to adjust their editorial decisions to limit future liability.
- Fox's pursuit of money is a driving force behind their actions and decisions.

### Quotes

- "Cash is a motivator."
- "The nightmare for Fox, it is just beginning."
- "Nobody at Fox is going to look at this as a cost of doing business."
- "They're after the cash."
- "It's more profitable to do so."

### Oneliner

Beau explains the significance of the Fox News settlement, predicting future challenges due to profit-driven narratives and the massive monetary consequences.

### Audience

Media Consumers

### On-the-ground actions from transcript

- Stay informed about media ethics and biases (suggested)
- Support fact-based journalism (implied)

### Whats missing in summary

Insight into the potential long-term effects on Fox News' editorial decisions and reporting practices.

### Tags

#FoxNews #Settlement #Profit #Journalism #MediaBias #UptonSinclair


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about why the Fox News
settlement actually matters.
There are a lot of people expressing different opinions
about it.
And I think some context might really help here
in understanding that this isn't a little thing
and that we probably can expect some changes,
particularly with what I imagine is still to come.
Just at random, here's one of the questions.
I don't understand how this helps repair the damage.
Sure, Dominion now has a lot of money, but how does that help?
Upton Sinclair once said, it is difficult to get a man
to understand something if his salary depends on him not understanding it.
It's a true statement.
The opposite of that's true too.
It is really easy to get somebody to understand something if their paycheck depends on them
understanding it.
Cash is a motivator.
Cash is a motivator.
Don't believe me?
Look at the messages.
Look at those conversations.
Why did Fox push the narratives that they did?
Why did they bring on some of the people that they did?
Look at the conversations about the pillow guy in particular, if you want it crystal
clear.
Money, money.
Now they have to contend with the fact that, well, it didn't work out.
Because that $787 million has to be subtracted from any revenue they made by pushing what
the audience wanted to hear.
An interesting development came from Bill O'Reilly, who, if you don't know, was a Fox
News host for a really, really long time.
And he said that the nightmare is just beginning.
He said that the nightmare for Fox, it is just beginning.
And basically indicated that he thinks that their pursuit of profit and telling the audience
what they want to hear rather than at least somewhat reporting the news is going to continue
to cause problems.
of what they did, there's going to be more fallout.
And the fact that this is Bill O'Reilly saying this, it kind of goes to show something.
Conventional wisdom says that Bill O'Reilly was forced out of Fox because of some settlements,
because of some misconduct claims in the settlements that came from them.
Do you know what they totaled?
I don't know if Hannah, I want to say it's 13 million that Fox had to pay, I think, in
that range.
That's nothing in comparison to 787 million.
There are a lot of people that are treating this as if this isn't a lot of money.
Even for somebody like the Murdochs, $787 million is $787 million.
Nobody at Fox is going to look at this as a cost of doing business.
It's way too much.
They have an audience.
They would still make money if they were putting on more accurate information.
This isn't an all or nothing thing where their options are have their outlet and pay $787
million every once in a while or make no money.
They can make money and not pay $787 million.
I think that now that the cash is at play, I think it might be easier for them to understand
why it's important to do that.
Bill O'Reilly recognized it.
not somebody that can be classified as liberal or a leftist, but even he was
like, you know, when I said that Trump was going to lose in federal court, I lost a
thousand premium subscribers or something along those lines. It's like, so be it.
If Bill O'Reilly gets it, I would imagine that Fox News executives
are going to get it too. And remember, this isn't over. They're going to be
paying out more and more money as time goes on. And this is all from one set
of information that was put out. If they continue doing it, there will be
continual lawsuits. It's not a one-time thing. I do not expect Fox to become
a bastion of journalistic integrity, I do expect them to pursue profit and the
profit isn't in lying. It's not in putting out bad information. Not if
you're gonna have to pay out 787 million here, 500 million here, so on and so
forth, it's too much to be just written off as a cost of doing business. So while
I know people, and myself included, would have much rather seen a trial and all of
that information come out and all of that information get coverage, the
monetary penalties, I would imagine that they would shift editorial decisions at
Fox more than they're going to want to admit. My guess is they start to walk
stuff back slowly to limit their future liability. Not because they care. Not
because they want to, you know, actually have some kind of ethics about this when
it comes to the practice of journalism or anything like that, but simply
because it's more profitable to do so.
You can always count on people like Fox News, entities like Fox News, to pursue the money.
They're after the cash.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}