---
title: Let's talk about Fox on military installations....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=N-Fl_YJprFc) |
| Published | 2023/04/20 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the issue of having Fox News on military installations, particularly in common areas and offices.
- Mentions the First Amendment concerns in relation to military installations.
- Describes how on military installations, there are restrictions on what content can be shown, like cable news in food court areas.
- Talks about the push to remove Fox from bases by groups like Vote Vets due to concerns about content undermining chain of command, readiness, cohesion, and morale.
- Provides scenarios where commanders express controversial views that, if aired on Fox, could lead to their dismissal.
- Points out that comments made on Fox can be detrimental to military readiness and could lead to consequences if expressed by a commander.
- Raises the issue of taxpayers unknowingly funding content that undermines military readiness.
- Concludes by summarizing the importance of pushing to remove Fox News from military bases.

### Quotes

- "US taxpayers are paying to undermine readiness."
- "A lot of the comments that are made on Fox are so detrimental to those concepts of readiness."
- "The reason that there is a push to get Fox off the bases… is because a lot of the content on Fox… undermines the chain of command, undermines readiness, undermines unit cohesion and morale."

### Oneliner

Beau explains the push to remove Fox News from military bases due to concerns about its content undermining readiness and cohesion.

### Audience

Military personnel, activists

### On-the-ground actions from transcript

- Contact military installations to advocate for the removal of Fox News (suggested)
  
### Whats missing in summary

Details on the specific actions individuals can take to support the push to remove Fox News from military bases.

### Tags

#FoxNews #MilitaryBases #FirstAmendment #Readiness #VoteVets


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Fox
and military installations.
And we're gonna go through two scenarios
to help explain the situation.
And we'll kind of cover the perspective of those people
who want Fox out of common areas and offices
and stuff like that on military installations.
I mentioned this issue in a recent video, and it led to a whole bunch of questions,
mostly revolving around the First Amendment.
Okay, so first things first, to all First Amendment concerns, you're talking about
a military installation.
That's not a thing.
That doesn't work the way that you think it does.
phrase at commander's discretion that means a whole lot and there are I'm
trying to figure out how to explain this okay picture a military installation and
on pretty much all of them there's a store that is kind of like a Walmart the
food courts in those areas they have TVs and it was a couple of years ago
that the management of the stores kind of sent out a memo saying yeah stop
with the cable news because it's just too divisive. This is a thing that has
been going on for years. I can remember as a kid in Fort Campbell at the
hospital bear changing the station on the TV and getting in trouble because I
didn't change it to one of the three pre-approved channels it's a thing. The
reason that there is a push to get Fox off the bases and one of the big groups
behind this is a veteran's group called Vote Vets is because a lot of the
content on Fox is stuff that could reasonably be viewed as something that undermines the
chain of command, undermines readiness, undermines unit cohesion and morale.
Couple of scenarios for you.
I want you to picture a division commander and she walks out, gets on the stage, she's
addressing her troops and she says, the Secretary of Defense is a woke, weak
loser who is just not up to the task of getting us ready to fight China. That's
scenario one. Scenario two, I want you to picture a wing commander talking to a
bunch of pilots and he's talking to them and they're about to fly to Europe, carry
a bunch of equipment that is bound for Ukraine and he says this is not our
fight, enjoy your flight, but this is worthless, it's a waste of time, they're
not a NATO member and this is bad policy. The question I have about these
scenarios is do you think that their separation papers would arrive before or
after they left the parade field or the airfield. A lot of the comments that are
made on Fox are so detrimental to those concepts of readiness and stuff like
that, that if a commander said it they'd be gone. So it has created a situation in
which US taxpayers are paying to undermine readiness. So there's a big
push to get Fox off the bases. That's what it's about. Anyway, it's just a
With that, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}