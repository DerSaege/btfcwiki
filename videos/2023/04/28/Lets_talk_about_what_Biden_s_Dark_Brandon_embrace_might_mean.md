---
title: Let's talk about what Biden's Dark Brandon embrace might mean....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=7ggh0Nf2dYY) |
| Published | 2023/04/28 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of "Dark Brandon," a meme portraying an ultra-progressive version of Biden who is ruthless in pursuing progressive goals.
- Suggests that the Biden administration is leaning into the Dark Brandon meme, potentially to connect with younger audiences and claim credit for smaller victories.
- Points out that embracing Dark Brandon could be good PR or a campaign advertisement for the administration.
- Raises the possibility that Biden, despite being viewed as a centrist, might lean into more progressive ideas if he commits to them.
- Speculates on the potential outcomes if Biden were to embrace a more progressive agenda with a mandate, House, and Senate majority.
- Expresses surprise at the idea of Biden pushing for a progressive agenda, given his previous perception as a placeholder during the Trump era.

### Quotes

- "Embracing Dark Brandon, it could just be good PR."
- "If he goes Dark Brandon and actually commits to them, if he has a mandate, if he has the House and the Senate, he's still not going to be Dark Brandon, but he may be far more progressive than people like me."
- "To be transformative, you have to progress."
- "The idea that he might actually turn into somebody who is going to push for a progressive agenda it's it's odd to think about."
- "He might be willing to let other people take the lead on issues and give him advice that pushes him in a more progressive direction."

### Oneliner

Biden administration's potential shift towards a progressive agenda through embracing the "Dark Brandon" meme raises questions about future policy directions and transformative possibilities.

### Audience

Political analysts

### On-the-ground actions from transcript

- Support and advocate for policies that prioritize progressive goals and transformative change (implied).

### Whats missing in summary

Exploration of the implications of Biden's potential move towards a more progressive agenda on broader policy issues and societal impact.

### Tags

#Biden #ProgressiveAgenda #DarkBrandon #Meme #PoliticalAnalysis


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we are going to talk about the Biden administration, the Biden
campaign, leaning into dark Brandon, because it's happening, a meme, they're
going to lean into a meme, and we're going to talk about what that might mean.
because it may mean a little bit more than them just really getting in touch
with internet culture. Okay, if you have absolutely no idea what I'm talking
about which you will totally be forgiven for in this case, Dark Brandon is a meme
that it has a it has kind of a history. I'll put the video from like eight
months ago where I first explained it down below, but short version, it is a
ultra-progressive version of Biden who is just absolutely ruthless in pursuit
of incredibly progressive goals. It was a joke when it first started, but
eight months ago I was like, you know, they might should lean into this because
it can connect with younger people and because of the nature of Dark Brandon being this just
incredible unstoppable force, it gives the Democratic Party and more importantly the
Biden administration a way to claim credit for smaller victories because while the Biden
administration doesn't have like massive headline grabbing stuff, they have a bunch of tiny stuff
that that adds up but people don't know about them and this helps with that.
So on the Biden 2024 website the 404 page is Dark Brandon.
Basically you're lost Jack, let's get you back on the rails.
And there are t-shirts available featuring Dark Brandon.
So let's talk about what it might mean.
The Biden administration is like every other administration.
It wants the same thing that every first term administration wants, a second term.
Generally speaking, if a president wins reelection, they feel unchained.
They are more free to truly pursue their agenda, especially if they win with a mandate.
Biden really did seem to indicate initially that he wanted to have a transformative administration
for the United States.
He wanted a Roosevelt-style administration.
The problem is if you want that, you have to get that stuff through.
And so far, he's gotten some big stuff, but it's not Roosevelt level.
Embracing Dark Brandon, it could just be good PR.
It could just be a campaign advertisement.
Could also mean that he plans on trying to fulfill what he set out to do in his first
term. It is way too early to guess, but right now people just think it's quirky.
It might mean more than that. You know, Biden is a centrist overall, but he is a
centrist who has flirted with some progressive ideas. If he goes dark
Brandon and actually commits to them, if he has a mandate, if he has the House and
the Senate, he's still not going to be Dark Brandon, but he may be far more
progressive than people like me would generally think he would be because he
has always wanted that transformative administration. To be transformative, you
have to progress. If they start leaning into that and a lot of his early
campaign moves seem to be heading that direction, it might be good. Weird to say.
Especially if you're familiar with my whole view on Biden going back to before he even
won the primary.
To me, he's kind of been a placeholder.
Just somebody to help fill the time while Trumpism fades.
The idea that he might actually turn into somebody who is going to push for a progressive
agenda it's it's odd to think about but looking at the moves they're making it's
not impossible and I know a whole lot of people are gonna be like no it's
impossible I mean I understand what you're saying but when you look at the
moves they're making when you look at the hard lines that he's been drawing
the new advertising and you combine with that with the fact that he wanted a
transformative administration. He might be willing to let other people take the
lead on issues and give him advice that pushes him in a more progressive
direction. I mean I wouldn't hold your breath for it but it's a possibility
that I wouldn't discount standing here right now.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}