---
title: Let's talk about Republicans shifting on climate and numbers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=si9JpyzZ2zQ) |
| Published | 2023/04/28 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Senator Ron Johnson of Wisconsin focused on the idea that climate change benefits his state because it is cold, disregarding the global impact.
- Republican Party's stance on climate change has evolved from denial to claiming it is happening but beneficial.
- The expert tried to explain the global impact of climate change to Senator Johnson, who seemed fixated on the benefits to his state.
- Converting excess deaths per 100,000 into real numbers reveals around 6.8 million extra deaths per year globally due to climate change.
- Climate change affects everyone, not just specific regions, and can lead to mass migrations, straining resources.
- Politicians' stance on climate change should be a key consideration for voters.
- Climate change is a pressing global issue that requires immediate action and cannot be ignored based on short-term benefits.
- Both Republican and Democratic Parties need to take significant steps to address climate change urgently.

### Quotes

- "Climate change isn't something that respects state lines or international borders."
- "Climate change is a global game. Everybody has to get involved."
- "Politicians' position on climate and actually doing something to mitigate should probably be a defining characteristic."

### Oneliner

Senator Ron Johnson fixates on Wisconsin's benefit from climate change, ignoring global impacts, as Republican Party's stance on climate evolves to deem it beneficial, disregarding the 6.8 million extra deaths yearly globally due to climate change.

### Audience

Voters

### On-the-ground actions from transcript

- Contact politicians to prioritize climate change mitigation efforts (implied).
- Get involved in local climate action groups or initiatives (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of how politicians' stances on climate change can have significant implications globally and urges immediate action to mitigate its effects.

### Tags

#ClimateChange #RepublicanParty #GlobalImpact #Voters #ClimateAction


## Transcript
Well, howdy there, Internet people, it's Bo again.
So today we are going to talk about a study,
the climate, the Republican Party's
ever-evolving stance when it comes to the climate
and a new development, one that I have to admit
I did not see coming,
and a Senator from Wisconsin, Ron Johnson.
We're going to go through that and then we're going to add some context and turn the rates
into real numbers.
Okay, so Senator Johnson was talking to an expert asking them about their study and it
dealt with the impacts of climate on mortality.
I want to say out to 2100. Johnson from Wisconsin kind of very much honed in on
the idea that because his state is cold as the climate warms it's beneficial to
his state and that he could take comfort. These are actual words that were used
comfort in the study. The expert did attempt to explain to the Senator that
there are 49 other states in the United States. That did not seem to really have
any impact, but we have apparently hit the point where the Republican Party
has has moved from climate change isn't a thing to okay it's real but it's not
happening because of people with there a brief stop I think at yes people have
something to do with it and now yes it's happening but it's good actually the
expert did explain that it's a global thing. Again, not information that Johnson
really seemed to process. He seemed very intent on pointing out that most of the
issues, at least up to 2100, would occur in Africa and that, you know, it was in
red and the blue areas are where things would be okay and he was in the blue.
The expert did everything he could to point out the unequal effects of climate change
and it just did not seem to go anywhere.
rates, when they do studies like this, they talk about extra excess deaths per 100,000.
Sometimes it's super useful to go ahead and convert those into real numbers to get a good
picture of the problem.
On average across the globe, assuming we're not just talking about Wisconsin, which apparently
exist in a vacuum, the rates if you were to calculate the excess deaths with a
population of 8 billion, global population of 8 billion, you're looking
at around 6.8 million extra deaths a year. These are the numbers that are
being shrugged off. Now again people like to think things happen and they don't
impact other things. If you lived in an area that was suddenly hostile to human
life would you stay there or would you move to one of the blue areas? 6.8
million people a year I mean that sounds like something that might cause mass
migration, which would put a strain on infrastructure, resources, everything.
Rest assured, even if you live somewhere that is chilly, climate change is a global game.
Everybody has to get involved.
It is one of the most, if not the most, pressing issues that humanity has to deal with.
It isn't something that respects state lines or international borders.
And it doesn't stop just because the model stops in 2100.
A politician's position on climate and actually doing something to mitigate should probably
be a defining characteristic.
It should be something that is heavily considered when you're talking about giving that person
your vote.
Rest assured, the Republican Party is totally going to embrace the idea that the US is in
blue area for the next hundred years or whatever and therefore this isn't really
something we need to worry about. That's not the way this works, not in the long
term. We don't have time for the Republican Party to catch up here. We
We don't have time for the Democratic Party to acknowledge there's an issue and not take
huge steps to mitigate it.
Anyway it's just a thought.
and I'll have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}