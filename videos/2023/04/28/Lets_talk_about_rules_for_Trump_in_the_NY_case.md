---
title: Let's talk about rules for Trump in the NY case....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=8mfuPi-2VdQ) |
| Published | 2023/04/28 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the ADA's request to limit former President Trump's ability to use evidence obtained during discovery to go after witnesses due to his history of attacking individuals involved in legal proceedings against him.
- The ADA wants a protective order to prevent Trump from using evidence on social media to harass or target individuals involved in the case.
- The protective order aims to restrict Trump from copying, obtaining, or sharing sensitive documents obtained during discovery.
- The judge has not yet ruled on the ADA's request, and a decision may not be made until next week.
- This move by the prosecutor to limit the defendant's ability to talk about evidence they will face is a unique step.
- Beau expresses curiosity about how the judge will rule, considering Trump's history of inciting people.
- The case mentioned is related to falsifying business documents in New York, not another case involving Trump.

### Quotes

- "The ADA wants a protective order that would prohibit him from using evidence obtained during the discovery process on social media to harass or target, intimidate the list of people who will be involved in the case."
- "I have no idea how the judge is going to rule on this, it is incredibly unique, but Trump does have a pretty lengthy history of stirring people up and getting them to do things."

### Oneliner

The assistant district attorney aims to limit Trump's ability to use evidence obtained during discovery due to his history of attacking individuals involved in legal proceedings.

### Audience

Legal observers, concerned citizens.

### On-the-ground actions from transcript

- Contact legal experts for insights on the implications of limiting a defendant's ability to use evidence during legal proceedings (suggested).
- Follow updates on the judge's ruling regarding the ADA's request (implied).

### Whats missing in summary

Insights on the potential impact of limiting a defendant's use of evidence during legal proceedings.

### Tags

#LegalProceedings #ADARequest #Trump #SocialMedia #ProtectiveOrder


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about former president Donald J.
Trump, New York, one of his proceedings in New York and the
assistant district attorney up there and what she is trying to do to, well,
limit Trump's Trumpiness.
The ADA has asked the judge for something and basically what the ADA is
trying to accomplish is limiting Trump's ability to use evidence obtained during
the discovery process to go after witnesses. Normally in a situation like
this, the discovery process occurs, the defendant, in this case Trump, gets the evidence that
the prosecution has.
The ADA says that because defendant Trump has a longstanding and perhaps singular history
of attacking witnesses, investigators, prosecutors, trial jurors, grand jurors, judges, and others
involved in legal proceedings against him that they would feel more comfortable if some
guidelines were put into place because those attacks on social media, they put witnesses,
jurors, the list of people that was named, put them at significant risk.
So the ADA wants a protective order that would prohibit him from using evidence obtained
during the discovery process on social media to harass or target, intimidate the list of
people who will be involved in the case.
That is generally when it comes to sensitive information, the ADA wants Trump to only be
able to review that in the presence of his lawyers and basically put a
restriction on him copying, obtaining, giving the documents to a third person,
transcribing anything when it comes to a select set of documents. My guess would
be this would be this stuff that like you know has somebody's address on it
it or something like that but that's a guess. So this filing has gone in at time of filming the
judge has not ruled on it and kind of indicated they probably wouldn't hear anything about it
until next week. But that is a very unique step for a prosecutor to try
to limit the defendant's ability to talk about the evidence that they will
eventually be confronted with. I'm very curious as to what's going to happen. I
I have no idea how the judge is going to rule on this, it is incredibly unique, but Trump
does have a pretty lengthy history of stirring people up and getting them to do things.
So it is a consideration that the judge definitely has to take seriously.
And for clarification, this is related to the falsifying business documents case up
in New York, not the other one.
So we probably won't hear anything until next week, though.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}