---
title: Let's talk about Jordan falling into a trap....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=3E41VT2KhD4) |
| Published | 2023/04/13 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits
Beau says:

- Beau mentions Jordan and an upcoming hearing in Congress focusing on rates in New York.
- He talks about the perception created by connecting high crime rates with Democrat-run cities.
- Beau explains how setting a minimum population requirement skews lists towards Democrat-run cities.
- He points out that rural areas or medium-sized cities often have higher crime rates than large cities like Chicago or New York.
- Beau hints that Republicans in the hearing may be misled by right-wing outlets' misinformation.
- He advises understanding the methodology behind such lists to counteract misinformation effectively.
- Beau suggests watching a video to comprehend how rural areas often have higher crime rates.
- He mentions that the Republicans may not be aware of this discrepancy and could be in for a surprise during the hearing.

### Quotes
- "It feels like there's going to be some funny moments coming up..."
- "They believe their own propaganda."
- "It's going to be funny."

### Oneliner
Beau dives into the misleading perception of crime rates in Democrat-run cities and rural areas, hinting at potential surprises in an upcoming hearing.

### Audience
Viewers, Congressional staff

### On-the-ground actions from transcript
- Watch the video mentioned to understand how crime rates are misrepresented (suggested).
- Educate others on the methodology behind crime rate lists to combat misinformation (implied).

### What's missing in summary
Beau provides a detailed analysis of how crime rates are perceived and misrepresented, challenging viewers to think critically about data manipulation and misinformation.

### Tags
#CrimeRates #Perception #Misinformation #CongressionalHearing #RuralAreas


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Jordan again,
up there in Congress, and another hearing
that is being put together.
And the likely outcome of it, in the attempt
to draw attention away from other things that
going on in New York looks like Jordan's gonna be putting together a hearing
talking about the rates up there. This is something we have talked about on the
channel before. Jordan may not know it yet but he's about to fall into a trap
but Humpty Dumpty didn't fall he was pushed and in this case Humpty Dumpty
was pushed by an echo chamber that in many ways he helped facilitate. If you
look over social media, if you look into right-wing echo chambers, what you find
is the term Democrat run cities in conjunction with high crime rates. This
is something that has created the perception that large cities have
higher crime rates. What has actually occurred, and I have another video I will
put down below that goes into this in detail and actually goes through lists
and shows how this works. What occurs is the people that put these top 10 most
dangerous cities or whatever these lists together, they set it to where the
minimum population to be included is 200,000 or you know whatever. The higher
it is, the higher the likelihood that all of the cities that would be eligible
would be Democrat-run because large cities trend blue. So it has created this
perception that blue cities lead to high crime. However, if you do it at the county
level or you drop the population requirement, what you find out is that a
lot of rural areas or medium-sized cities have higher crime rates, have
higher violent crime rates than places like Chicago or New York.
Many of those are in Ohio.
I have a feeling that somebody who is actually involved in the criminal justice system, like
people related to the district attorney's office in New York, they might know that.
They might actually understand the basic statistics.
It feels like there's going to be some funny moments coming up where Republicans ask questions
believing they know the answer because of Fox News or some other right-wing outlet and
then realize they're wrong because they don't understand the methodology that was used to
put those lists together.
If you really want to get into how this works, and I strongly suggest it because this practice
is not just limited to crime rates.
So if you understand how they get these charts, you'll kind of inoculate yourself against
it.
But I'll put that video down there.
Definitely watch it, and you will see how rural areas tend to have higher rates than
even those that are villainized the most, the large cities that are villainized the
most, like Chicago.
It does not appear that they're aware of this fact.
It appears that they actually have bought into a... they believe their own propaganda.
Something has been told and has been put out there to fearmonger for so long that those
who probably had a hand in creating that image now believe it themselves.
Those numbers are not going to go the way the Republicans in this hearing plan, because
the second they drop the population requirement, the second they look at it from the county
level, the second they just compare per capita rates, it's going to be funny.
may be one of the first ones I actually tune in to watch. Anyway, it's just a
thought, you'll have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}