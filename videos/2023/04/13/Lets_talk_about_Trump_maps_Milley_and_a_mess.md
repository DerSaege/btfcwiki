---
title: Let's talk about Trump, maps, Milley, and a mess....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=WX0sGZn0VBk) |
| Published | 2023/04/13 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the Trump document case and concerns about Trump's specific interest in General Milley.
- Mentions federal investigators asking witnesses about a map that Trump showed them.
- Speculates on the seriousness of the case based on willful retention of documents.
- Explains the significance of willful retention and the potential implications of intentionally gathering and distributing sensitive information.
- Raises concerns about possible espionage charges if certain questions lead to an indictment.
- Emphasizes that the investigation goes beyond obstruction or willful retention, hinting at more serious charges being considered.
- Points out that the federal government is building a case for something substantial beyond basic security lapses.
- Expresses nervousness about the implications of the investigation's focus on specific interests and unauthorized distribution of documents.
- Notes the shift from a mere security lapse to a potential security breach in the investigation.
- Underlines the gravity of the situation based on the lines of questioning by federal investigators.

### Quotes

- "Willful retention is the least of his concerns."
- "We're talking about gathering them with a specific purpose."
- "We're no longer talking about a security lapse, we're talking about a security breach."
- "Those are very telling."
- "They are looking into something way bigger."

### Oneliner

Federal investigators are delving into Trump's potential interests and unauthorized document distribution, hinting at more serious charges beyond willful retention in the Trump document case.

### Audience

Political analysts, investigators

### On-the-ground actions from transcript

- Stay informed on the developments in the Trump document case (implied).
- Support accountability and transparency in political investigations (implied).

### Whats missing in summary

Insights on the potential legal consequences and broader implications of the investigation.

### Tags

#Trump #DocumentCase #GeneralMilley #Investigation #LegalConsequences


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Trump, and Milley,
and maps, and messes, and how things
might be shifting in the documents case.
And we're going to talk about a video
that I put out a little more than a week ago
that some of y'all said was a little cryptic.
It's about to become a lot less cryptic.
Okay, so I want to say it was 10, 11 days ago, something like that.
Put out a video talking about some reporting that came out, and the reporting said that
federal investigators were now asking witnesses in the Trump document case whether or not
Trump had a specific interest in General Milley.
And I pointed out how that might be concerning and might alter the course of the investigation.
I also said I didn't want to speculate on it because I was hoping it was all one big
coincidence.
There is now reporting about federal investigators asking witnesses if Trump showed them a map,
a map they were not supposed to see.
OK.
So I feel a little bit more comfortable speculating
on this at this time.
Throughout this, you've heard me use the phrase,
willful retention, over and over again.
Because realistically, based on what I knew, what I had heard,
what the reporting was, that was what
was going to get hit with, willful retention, and it is exactly what it sounds like.
You are willfully retaining documents that you're supposed to give back.
Pretty simple.
It's a big deal.
Don't get me wrong.
There are things above that such as things that would relate to gathering national defense
information intentionally, maybe going through documents and picking out things that you
have a specific interest in.
That would be bad.
That would alter what they were looking at.
If somebody has a giant stack of documents and they return all of them except for stuff
that I don't know could disrupt US command and control.
There's a theme to those documents and that immediately forces the investigator to ask
the question of why did you want to keep these?
What specific interest did you have in these?
And then beyond that, you have the question of whether or not any of that information
was distributed, whether or not it was shown to people.
Based on the reporting, that is certainly the lines of inquiry that the federal government
is looking into now.
Short version, if the answers to these questions, did Trump have a specific interest in General
Milley. Did Trump show this map to people who weren't authorized to see it? If the
answers to these questions are yes and it leads to an indictment,
willful retention is the least of his concerns. The lines of questioning
that the federal government is now pursuing, they are way beyond obstruction
or willful retention. They are way beyond that. It appears that they are building a
case for something a wee bit more serious. Now, it is worth noting we don't
know what the answers to these questions were. So if everybody said no, well, I mean
you still have to wonder why they're asking, but there's not enough
enough at this point to jump to Trump's getting hit with real espionage charges.
But there is enough to say it certainly appears that the feds are looking into those charges
and they are asking questions that if it was me, I would be really nervous because we are
We're no longer talking about just simply retaining the documents.
We are talking about gathering them with a specific purpose.
We are talking about distributing them to people who aren't supposed to see them.
It's a whole different game based on those two lines of questioning.
They are looking into something way bigger than him being arrogant and wanting to hold
on to mementos or something like that and simply not following basic security procedures.
We're no longer talking about a security lapse, we're talking about a security breach.
We don't know enough to know how far they're looking into that, but those two lines of
questioning about having a specific interest in General Milley and whether
or not he showed a map to people. Those are very telling. They would not be
concerned about...they wouldn't even be looking into this if all they were
looking at was willful retention. They wouldn't even be questions being asked.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}