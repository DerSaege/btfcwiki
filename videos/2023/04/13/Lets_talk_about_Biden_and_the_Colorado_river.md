---
title: Let's talk about Biden and the Colorado river....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=GoExR0W_m6Y) |
| Published | 2023/04/13 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the issues surrounding the Colorado River, with many states depending on it for water.
- Mentions that despite recent rainfall in the southwest, the problem still persists.
- States have been unsuccessful in developing a plan to address the water usage issue.
- The Biden administration introduced two plans, one impacting California heavily and the other affecting Arizona.
- Biden administration did not specify which plan they will implement, putting pressure on the states to collaborate and find a solution.
- Beau believes it is the right move for the states to make these decisions themselves.
- States are better equipped to understand their needs compared to the federal government.
- States collaborating to address such issues can set a precedent for the future.
- Beau suspects political motives behind the Biden administration's decision, aiming to avoid backlash and negative narratives.
- Biden administration's decision is driven by the desire to avoid being blamed for making cuts that impact water usage.
- Beau acknowledges tough decisions lie ahead, including restrictions on growth for cities and changes for water-intensive farms and businesses.
- Emphasizes that states coming together to make decisions is ideal for everyone involved.
- Despite potential political math behind the decision, Beau believes pressuring states to act is the right move.
- Beau concludes by expressing his thoughts on the situation and wishes everyone a good day.

### Quotes

- "The states have to make these decisions and the precedent needs to be set that states can come together, work out a consensus agreement, and deal with issues like this in the future because there's going to be more and more."
- "It will be better for everybody involved if the states make the decision."
- "This has to do with the votes."
- "There are tough decisions that are going to have to be made. This is going to happen more and more often."
- "Again not the ideal reasoning but putting pressure on the states to get them to make the decision themselves it's the right move."

### Oneliner

Beau explains the Colorado River issues, Biden administration's plans, and why it's vital for states to decide on water usage cuts, despite potential political motives.

### Audience

Environmental activists, Water conservation advocates

### On-the-ground actions from transcript

- Collaborate with local communities and organizations to raise awareness about water conservation and sustainable usage (implied).
- Support initiatives that focus on long-term planning for environmental sustainability in your region (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the Colorado River water usage issue, the Biden administration's approach, and the importance of states making decisions collectively for future water management.

### Tags

#ColoradoRiver #WaterUsage #BidenAdministration #StateDecisions #EnvironmentalSustainability


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about the Colorado River,
water usage, and the states, and the federal government,
and the Biden administration's recent move,
whether or not it's the right move,
and whether or not the move they made
was made for the right reasons.
Because sometimes you can make the wrong move
for the right reasons,
or you can make the right move for the wrong reasons.
And we're going to kind of go through it
and kind of lay out the plan
as I think the Biden administration is trying to implement.
Okay, so if you have no idea what's going on,
the Colorado River is having issues.
And there are a lot of states that depend on that river
for water. They pull water out of it. They're pulling too much. Cuts have to be made. Despite
all of the recent water in the southwest, it didn't actually fix the problem. This
isn't a yearly thing. This is a long trend. Provided a little bit of a reprieve, but not
Really, the cuts still have to be made, a plan for the future still has to be made.
All of the states have been trying to come up with their own plan and they've been unsuccessful
in doing it.
The Biden administration, the feds, put out two plans.
One heavily hits California, the other one hits Arizona.
The interesting part about it is that the feds didn't say which plan that they would
implement.
If they had to do it, if the feds had to step in and make the decision, they're not saying
which one they're going to do.
This has the effect of turning up the pressure on the states to get them to work amongst
themselves to come up with a solution.
Is this the right move?
Absolutely.
The states have to make these decisions and the precedent needs to be set that states
can come together, work out a consensus agreement, and deal with issues like this in the future
because there's going to be more and more.
The states are better equipped.
They have more information about the needs of the states than the feds do.
It will be better for everybody involved if the states make the decision.
It's also good to have these states start to actively think about long-term planning
when it comes to the environment in this way.
Yes, the right move.
Did the Biden administration make that decision and make this move for the reasons I just
said?
Probably not.
My guess is there's a lot of political math going on.
And my guess is that the Biden administration doesn't want the feds to step in and make
the decision.
Not because they think the feds have less information, or they want to set a good precedent,
Or they want this stuff solved at the lowest possible level, nothing like that.
They don't want to step in because no matter where they step in to make cuts, then it will
be turned into, the fed stepped in and took away your water.
And they're trying to avoid that.
It has to do with the votes.
Because make no mistake about it, if the federal government has to step in and force water
cuts, water usage cuts, which they have the power to do.
Somebody will be on Fox that night talking about how they're denying Americans their
rights and this is just massive government overreach and if Americans want to die of
dehydration they have the right to do so.
You know it would happen.
The Biden administration is probably trying to avoid that.
I don't necessarily think that's the right reason to make this decision.
But at the end of the day, the decision is the right one.
The ideal here would be for the states to come together and work out an agreement.
And yes, that means that some cities, they can't grow anymore.
They can't add to their population.
They're not going to be able to put in a bunch of new developments because they're not going
to have the water for them. It means that some farms, especially if you're producing
something that is water intensive, it may need to go. It may mean that businesses that
pull water out and ship it somewhere else, or pull water out to turn it into bottled
water or something like that. They may have to go. There are tough decisions
that are going to have to be made. This is going to happen more and more often.
It will be better for everybody if the states are the ones making the
decisions amongst themselves. But deep down I really feel like this was the
Biden administration looking at the southwestern United States and being
like you know what they can sort this out and we're not gonna take the heat
for it. Again not the ideal reasoning but putting pressure on the states to get
them to make the decision themselves it's the right move. Anyway it's just a
thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}