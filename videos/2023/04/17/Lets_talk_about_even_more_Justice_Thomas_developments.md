---
title: Let's talk about even more Justice Thomas developments....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=PNQRSmJ9QiM) |
| Published | 2023/04/17 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Supreme Court Justice Thomas is in the spotlight due to recent developments.
- Thomas made a rare statement about amending previous filings for accuracy.
- Reports show Thomas received between 50 and 100 grand annually from a firm in Nebraska that ceased to exist in 2006.
- The company that closed was Ginger LTD, while another company, Ginger LLC, was formed.
- The situation may simply be an innocent mistake related to paperwork.
- Calls for further investigation are mounting despite the possibility of a simple explanation.
- Politicians are calling for a code of ethics for the Supreme Court and urging Chief Justice Roberts to open a probe.
- Some are supporting impeachment or calling for resignation.
- Attention on Thomas continues to grow, with the likelihood of more information surfacing.
- Questions remain unanswered regarding private jet use, real estate in Savannah, and vacation expenses.

### Quotes

- "There are still major components to this story that do not have answers."
- "I don't see this getting swept under the rug and disappearing anytime soon."

### Oneliner

Supreme Court Justice Thomas faces scrutiny over financial discrepancies and unanswered questions, sparking calls for ethics reform and further investigation.

### Audience

Politically engaged citizens

### On-the-ground actions from transcript

- Contact your representatives to express support for ethics reforms and calls for further investigation (exemplified)
- Join organizations advocating for transparency and accountability in the Supreme Court (implied)

### Whats missing in summary

More details on the specific allegations and the potential implications of the ongoing investigations.

### Tags

#SupremeCourt #JusticeThomas #EthicsReform #Investigation #Transparency


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk more
about Supreme Court Justice Thomas, yet again,
because there have been more developments.
So we will talk about Thomas, we will talk about Nebraska,
we will talk about paperwork and we'll
talk about the lay of the land and how people are responding
to the continued developments.
So the first piece of news is that in a very, very rare
statement, generally speaking, Supreme Court justices
don't make statements.
Thomas said he was going to go back and amend previous
filings, make sure everything was accurate.
Right about the time that information came out, there was
another development.
Thomas, for years and years, had been reporting, I want to
say somewhere between 50 and 100 grand a year from a firm out of Nebraska. The
problem is that firm ceased to be in 2006 and that is the headline and that's
what people are saying. It looks really bad when that's all you have. This may
actually be just an innocent, honest mistake dealing with paperwork. A cursory
look at it says that the company, which I want to say is Ginger LTD, is the
company that closed in 2006. Another company, Ginger LLC, was formed. This may
be as simple as him copying from the year before and not acknowledging a
reorganization or something like that. This really might just be nothing, but we
don't know yet with everything that's going on. It certainly adds more fuel to
the idea that somebody might want to take a look into all of this. But while
the immediate implications from the headlines, yet those are staggering,
there may be a really simple explanation for that. So what's the lay of the land?
What are politicians saying? What are they suggesting needs to happen? You have
the very lukewarm, you know, response of, we need a code of ethics for the Supreme
Court. The Senate Judiciary Committee is basically looking at Chief Justice
Roberts and they're kind of like, you need to open a probe now. They're a
little bit more insistent in Chief Justice Roberts handling
this quickly. You have some more support for impeachment, not a whole lot, but
there are growing calls for resignation at this point. And given the amount of
attention this is getting and the fact that people are definitely continuing
to dig. There's a high likelihood that other stuff is going to surface. Now, it
may be just simple paperwork stuff like honest mistakes, but if there's enough of
that, there's going to be a continued push. And there's still, as far as I know,
there hasn't been a real explanation of the private jet use or the real
estate in Savannah. That's still kind of up in the air. The vacation thing and all
of that, again I understand how it looks, but that's gonna be waved away. So there
There are a number of components to this growing story that still don't have explanations.
Even when you are giving Justice Thomas the benefit of the doubt, and you're looking at
it saying, okay, well, that could be just minor paperwork stuff.
This could be something he really doesn't know.
There are still major components to this story that do not have answers.
And it seems like those answers would have surfaced by now.
So I would expect this to continue.
I don't see this getting swept under the rug and disappearing anytime soon.
I think there's going to be continued political pressure over this.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}