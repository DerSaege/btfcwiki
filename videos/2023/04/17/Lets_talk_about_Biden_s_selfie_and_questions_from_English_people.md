---
title: Let's talk about Biden's selfie and questions from English people....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=j0RB0SSgA7E) |
| Published | 2023/04/17 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the outrage of American Republicans over a selfie of Biden with Jerry Adams in Ireland.
- American Republicans were told to be upset about the image without knowing the full context.
- Jerry Adams has a controversial past associated with violent activities in the Republican movement.
- Talks about the ambiguity surrounding Adams' past and his rise within the movement.
- Describes Adams as the public face and PR person for the Republican movement in the 80s and 90s.
- Mentions a shift towards peace by Adams after the Good Friday Agreement.
- Points out a significant moment when Adams condemned a violent act unequivocally, showing a visible change.
- Emphasizes Adams' evolution into a mainstream political figure advocating for peaceful means.
- Acknowledges that some may struggle to forgive Adams for his past actions despite his visible transformation.
- Urges American Republicans to understand the context and reconciliation efforts in the UK before criticizing the selfie incident.

### Quotes

- "He became more and more a mainstream political figure and wound up in office."
- "The idea here is that somebody who was sympathetic or was the public face of a violent anti-government movement should be ostracized."
- "If you are an American who is upset about this, please understand there are English people wondering why you're mad."

### Oneliner

American Republicans' outrage over Biden's selfie with Jerry Adams lacks context, ignoring Adams' visible transformation towards peace in Ireland.

### Audience

American Republicans

### On-the-ground actions from transcript

- Contact organizations promoting reconciliation efforts in Ireland (implied)
- Educate oneself on the history and progress of peace in Ireland (implied)

### Whats missing in summary

The full transcript provides a comprehensive background on Jerry Adams' transformation towards peace in Ireland, urging a nuanced understanding of complex political histories for American Republicans. 

### Tags

#Biden #JerryAdams #AmericanRepublicans #UK #Reconciliation


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about a Biden selfie, a selfie that Biden showed
up in when he was in Ireland, because I got a whole bunch of, well, a whole bunch.
I got like four, four messages from people who identified themselves as either
English, British, or from the UK.
And their question was really simple.
They wanted to know why American Republicans were upset about this image,
about this selfie. Short answer is they were told to be mad. So they are. It's
really that simple. They don't know anything about it. They don't know
anything about it. I know that y'all get news about what happens here and it when
When something happens here, it makes the news there.
If I was to say Nashville, odds are you would know what just happened there.
They have no idea what OMOT is or how it changed things.
No clue.
They were told to be mad about it, so they are.
Okay, so for those not in the UK or not in right-wing echo chambers, what's going on?
Biden showed up in a selfie with a man named Jerry Adams.
Jerry Adams, to put it lightly, has a checkered past.
There are a lot of accusations about what he did when he was younger.
They center on him being active with the Republican movement.
By active, I mean violent.
Now there's a lot of ambiguity over what's true and what's not.
Personally, I do not think that he was as active as a lot of the accusations make him
out to be.
That being said, there isn't a logical way for somebody to rise to the level he did without
having their trust, and there's only a few ways to get their trust.
So he didn't end up where he was by singing too loud in the choir.
happy medium between he was a full-blown member and he didn't do anything.
And that's where I think the truth is, but that's my personal opinion.
As time went on, he became the public face of the movement, their PR person, the political
wing.
He would come out after something happened and talk about it and carry forth the Republican
talking points. Remember, American Republican and Republicans over there,
not the same thing. And this went on for a long, long time. Since Americans like
to view this conflict through movies, and that's a good frame of reference, in the
movie Patriot Games, Harrison Ford is shoving his finger in somebody's face
And he's like, I will have you out in the street throwing rocks.
The guy he's talking to, I don't know that he was modeled after Adams.
But if somebody came to me and said, yeah, that's who we based the character on, I'd
believe it.
That's a good reference.
He definitely was the face of the movement.
This is in the 80s and 90s.
Then this weird thing happened.
Peace broke out.
There was a legitimate push for real peace and reconciliation.
And he didn't stand in the way.
Good Friday agreement happened.
View that as the peace treaty.
And then four months later, more or less, I think 13 weeks, a
device went off in Omaha. It was bad. Bad. Dozens lost, hundreds injured. Bad. And
people talk about his ideological movement throughout the years. There was
also a moment, and it was August 15th, 1998. Within hours of that happening, he
came out and he made a statement, and I think he said it was appalling, but the
part that mattered was, I condemn it without any equivocation whatsoever. For
somebody who was a wordsmith, a PR person, that is pretty lukewarm when you're
talking about something that was that bad, but as a wordsmith he knew people
were gonna focus in on one word, condemn. If you look back through all those years
before when a splinter did something, he never used the word condemn, ever. It was a
moment where he changed, where at least it was visible. There are a lot of signs
that show that he shifted over the years, but that moment was a big deal. From that
point, he was very much moving towards peace. In fact, it was only a few short
years later that he called on the volunteers, those are the active
people within the Republican movement, called on them to lay down their arms
and to pursue peaceful means exclusively.
He became more and more a mainstream political figure and wound up in office.
There was an evolution of this person.
And certainly there are people who do not want to forgive him for what he did or what
he might have done back in the day.
I get it, I understand, but that change definitely occurred and it's pretty visible.
And this is the person that was in a selfie with Biden.
If you are an American who is upset about this, please understand there are English
people wondering why you're mad.
There are English people, British people, confused as to why you're upset.
They are working towards reconciliation.
The last thing they need is Americans stirring stuff up.
As far as the idea that, you know, he shouldn't have been legitimized like that, I mean I
understand what you're saying, but if you're going to say that, there's a whole lot of
people in the British government that you need to go talk to.
King Charles, then Prince, shook his hand in Galway publicly, defining moment, very
symbolic, that path towards reconciliation.
And then I think the most important part for American Republicans to really think about,
The idea here is that somebody who was sympathetic, or was the public face of a violent anti-government
movement, should be ostracized.
Okay?
You might want to look into your own party.
And it's worth noting that when Adams got picked up in 2014, I think, he said it was
them trying to influence the election too.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}