---
title: Let's talk about Clarence Thomas, gifts, and disclosure....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Lsr22hwtjo4) |
| Published | 2023/04/06 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces the topic of two friends, Supreme Court Justice Clarence Thomas and Republican mega donor Harlan Crowe.
- Crowe allegedly took Thomas on extravagant vacations worth tens or even hundreds of thousands of dollars, including cruising on a mega yacht and trips to Indonesia.
- The focus is on the appearance of a Supreme Court justice vacationing with a GOP mega donor, which raises concerns about gift disclosures.
- Thomas failed to disclose these gifts properly, which may lead to civil or criminal penalties.
- Despite Crowe being generous with vacations for many people, this does not excuse the lack of disclosure by Thomas.
- The vacations might not be the most significant issue; the use of a jet without Crowe could pose a bigger problem and may trigger an investigation.
- The jet use, potentially costing 20 to 70 grand per trip, could be harder to dismiss compared to the vacations.
- Calls for Thomas to resign have emerged, and there may be an ethics investigation into his actions and the accuracy of his disclosure forms.
- The situation involving Thomas and Crowe appears to be escalating into a scandal that won't easily fade away.
- Beau acknowledges the unprecedented nature of the situation and suggests following the reporting from ProPublica for more details.

### Quotes

- "Vacations that if you were to assign a dollar amount to them would be tens of thousands or hundreds of thousands of dollars."
- "The use of the Jet that's going to be an issue if there is an investigation."
- "This is definitely not a story that's going to go away."

### Oneliner

Beau talks about Supreme Court Justice Clarence Thomas and Republican mega donor Harlan Crowe's extravagant vacations, gift disclosures, and the potential for an investigation and calls for resignation.

### Audience

Journalists, Activists, Citizens

### On-the-ground actions from transcript

- Read and share the reporting from ProPublica to stay informed (suggested)
- Follow any updates on the situation to understand its implications (suggested)

### Whats missing in summary

The full transcript provides detailed insights into the friendship between Clarence Thomas and Harlan Crowe, the controversies surrounding undisclosed gifts, and the potential consequences for Thomas. Viewing the entire transcript helps in grasping the nuances and developments in this unfolding story.

### Tags

#SupremeCourt #ClarenceThomas #HarlanCrowe #GiftDisclosure #Investigation #ProPublica


## Transcript
Well, howdy there internet people, it's Beau again.
So today we're going to talk about two friends.
We're gonna talk about two friends.
One, a Supreme Court Justice, Clarence Thomas,
and another one, a Republican mega donor, Harlan Crowe.
We're going to talk about their friendship,
Crowe's generosity, gifts, and disclosures,
or the lack thereof. We're going to do this because reporting has come out from
ProPublica, an outlet that I have repeatedly recommended even though
they're not incredibly well known, because they do really good going
through the paperwork kind of journalism. That's what's happened here. If you are
not following them, let this be your clue that you should. Okay, so we're going to
to talk about the parts that most people are going to pay attention to, and then we're
going to get to what is probably actually going to matter.
So according to the reporting, Harlan Crowe took the Supreme Court Justice on vacations.
Vacations that if you were to assign a dollar amount to them would be tens of thousands
or hundreds of thousands of dollars.
We're talking about cruising on a mega yacht.
We're talking about going to Indonesia where big vacations.
And this is what people are focusing on because obviously there is an appearance with a Supreme
Court justice chilling on a mega yacht with a GOP mega donor.
I mean, that's not an image you want out there, and this is what people are focusing on, obviously,
because it is, I mean, the appearance is troublesome. Now, under the rules, it certainly appears
that this stuff would fall under the type of gift that Thomas would have to disclose.
and he did not. But I don't actually think that this is going to be the
strongest part, even though that's what people are focusing on right now. Not
disclosing these things properly, I'm pretty, let's see, I know there's civil
penalties, there might be criminal penalties. I didn't look it up. If you
want to know, download the disclosure form for the Supreme Court justices,
scroll down to the bottom where they sign. There's probably going to be an
investigation into this situation. You already have calls for Thomas to resign.
Now, why aren't the vacations the biggest, the strongest part of this? Because my
understanding is that Harlan Crowe is like super generous on vacations with
everybody, with everybody, even people who aren't politically connected. I think to
include like random real estate brokers and stuff like that. This is just who he
is. Now does this mean that it shouldn't be disclosed? No, it doesn't have anything
to do with it, but it provides cover for this part of it, okay? Doesn't actually
change the requirements that Thomas might have faced but there's a there's a
storyline there that says there was nothing improper about this because this
is just how this guy is he does this for a lot of people and my understanding is
that that's actually true doesn't change the requirements though it goes back to
that whole thing of gifts. I think the part that's going to be the hardest for
Thomas to kind of bat away, it's not the vacations. That's actually not
going to be the hard part. The hard part is going to be the jet because it's still
early, but it looks like Thomas used this jet without Harlan Crow. That's, you
can't do that. And that is something that I want to say is actually
explicitly listed as something you can't do without disclosing. And there's
There's not a story that just shows this eccentric rich person who does this for a lot of people
that could provide cover for Thomas.
I think it's actually going to be the Jets, the use of the Jet that's going to be an issue
if there is an investigation.
Because there's kind of no way around that based on what I know so far.
going to be a big problem.
And it's weird because the trip to Indonesia, I think they put a dollar amount on it of
about half a million dollars.
The jets, depending on the trip, 20 to 70 grand.
That's going to be the part that isn't going to be able to be just batted away.
Now, what is going to happen here?
I have no idea.
I have no idea.
Hate to use the word, but kind of unprecedented.
There are calls for Thomas to resign.
There is, based on the reporting,
it certainly looks like something
that might trigger an ethics investigation
and an investigation into whether or not
he was less than accurate on those forms.
And if he was less than accurate,
they will want to know why.
This is definitely not a story that's going to go away,
as if the United States needed another scandal reaching
to incredible heights in the government.
So we'll continue following it here,
and we'll continue talking about it,
I would strongly suggest checking out the reporting
from ProPublica, looking through all of that,
and understanding that when you're talking about stuff
like this, the most sensational parts aren't necessarily
the strongest parts when it comes
to a potential investigation or things
that might prompt somebody to vacate their office early.
Anyway, it's just a thought.
So have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}