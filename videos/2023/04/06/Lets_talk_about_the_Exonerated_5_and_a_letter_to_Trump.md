---
title: Let's talk about the Exonerated 5 and a letter to Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=qjHx3pCYUvg) |
| Published | 2023/04/06 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about an open letter written to Trump regarding the Central Park Five case.
- Mentions Trump's actions in calling for the execution of the Central Park Five.
- Points out that the conviction of the Central Park Five was overturned, and they were renamed the Exonerated Five.
- Describes how Trump influenced public opinion against the Exonerated Five.
- Addresses the issues faced by the community during Trump's time in office.
- Expresses the desire for positive change at the community level.
- Hopes that Trump receives the presumption of innocence, something the Exonerated Five didn't get.
- Stresses the importance of enduring penalties with strength and dignity if found guilty.
- Emphasizes the preservation of civil rights in the open letter.
- Condemns Trump's actions that undermined the system over the years.

### Quotes

- "Money doesn't buy class."
- "The primary concern is not vengeance, it's preservation of civil liberties."
- "Hope that you endure whatever penalties are imposed with the same strength and dignity."

### Oneliner

Beau showcases an open letter addressing Trump's actions against the Exonerated Five and the need for positive change at the community level, stressing the preservation of civil liberties over vengeance.

### Audience

Community members

### On-the-ground actions from transcript

- Work with dedicated community members to build a better future (suggested)
- Advocate for the preservation of civil rights and civil liberties (implied)

### Whats missing in summary

Full emotional impact and depth of analysis on the repercussions of Trump's actions and the importance of community-driven change.

### Tags

#CentralParkFive #Trump #CivilRights #CommunityJustice #PresumptionOfInnocence


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about an open letter that was written to Trump.
Um, and it's, it's long.
I'm not going to read the whole thing, but I'm definitely going to hit the
highlights and kind of showcase what, what class really is.
If you are not familiar with the story of the Central Park Five, definitely something
worth reading up on.
This is the open letter, the opening of it.
On May 1st, 1989, almost 34 years ago, Donald J. Trump spent $85,000 to take out full-page
ads in the New York Times, New York Daily News, New York Post and New York Newsday calling
for the execution of the Central Park Five, an act he has never apologized for even after
someone else confessed to and was convicted of the crime.
The conviction of all five of us were overturned and we were renamed the Exonerated Five.
So this is somebody that is a member of the Exonerated Five.
This is somebody that Trump did everything he could to ensure that the jury pool, that
The public opinion was swayed against them.
The problems our community faced when my name was splashed across the newspapers a generation
ago, inadequate housing, underfunded schools, public safety concerns, and a lack of good
jobs became worse during Donald Trump's time in office.
I am trying to change that by working with so many other dedicated community members
to build a better future for everyone, both here in Harlem and across the country."
And he goes on.
He goes on and he points out a lot of Trump's hypocrisy while at the same time stressing
the need for positive change at the community level.
And he says that he hopes that Trump gets the one thing that they didn't get all those
years ago, the presumption of innocence.
He closes by saying, and if the charges are proven and you are found guilty, I hope that
you endure whatever penalties are imposed with the same strength and dignity that the
The Exonerated Five showed as we served our punishment for a crime we did not commit."
Man, that's wild.
That's wild.
It goes to show that money doesn't buy class.
This is a person who had their civil rights violated, and in this open letter that is
put out, one of the primary concerns is not vengeance, it's preservation of those civil
liberties of those civil rights and to ensure the system. A system that was
certainly attacked and undermined by the former president repeatedly going all
the way back to the 80s.
Anyway, it's just a thought.
I hope you all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}