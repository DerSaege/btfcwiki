---
title: Let's talk about China and who owns America's farmland....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ba_JsDZdbVI) |
| Published | 2023/04/10 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the claims about Chinese ownership of US farmland to debunk common misconceptions and fear-mongering.
- China owns about 400,000 acres of agricultural land in the US, not millions as some memes suggest.
- Putting the numbers into perspective, comparing Chinese ownership to other entities like King Ranch, which is much larger.
- Foreign countries collectively own about 3% of US agricultural acreage, with China owning only 1% of that.
- Most of the farmland China owns came through acquiring a pork producer, not a deliberate land grab.
- Debunks the idea that China owning US farmland could lead to food supply control in the event of a war.
- Emphasizes that the fear-mongering around this issue is not based on reality but rather political propaganda.
- Politicians may use the narrative of foreign ownership of farmland to stoke fear and advance certain agendas.
- Concludes that the situation is not as alarming as portrayed and urges viewers not to be overly concerned.
- Encourages critical thinking and a deeper understanding of the facts behind sensational claims.

### Quotes

- "China owns about 400,000 acres of agricultural land in the US, a little less than."
- "400,000 acres isn't quite as huge as it sounds."
- "They're not even a big investor when it comes to agricultural land in the US."
- "It's fear-mongering, it's the same stuff."
- "Not a big deal."

### Oneliner

Beau debunks fear-mongering claims about Chinese ownership of US farmland, revealing the reality behind the statistics and urging critical thinking over sensationalism.

### Audience

Citizens, Fact-Checker

### On-the-ground actions from transcript

- Verify information before sharing (implied)
- Educate others on the actual statistics and percentages of foreign ownership of US farmland (implied)
- Challenge fear-mongering narratives with facts and perspective (implied)

### Whats missing in summary

Deeper insights into the geopolitical and economic implications of foreign ownership of US farmland.

### Tags

#Farmland #US #China #ForeignInvestment #Debunking #FearMongering #CriticalThinking


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about farmland.
We're going to talk about US farmland and China
and investment and numbers and statistics and acres
and all kinds of things, all to determine who really
owns US farmland.
Dun, dun, dun.
We're going to do this because we got a message.
And it's a good way to talk about one specific set of claims.
And hopefully, if you apply these principles,
you can end up debunking a whole bunch of them on your own.
Since you're going over memes, a big one around me
is that China is buying up all the farmland in the US
and will control our food supply if there's ever a war.
My uncle is convinced the Chinese are
buying the land to starve us.
He says they're buying millions of acres and will own us.
There are tons of unsourced memes about this.
What's the truth?
OK, so you have to start with the stuff that you can verify.
Is China buying millions of acres of farmland,
of agricultural land?
No, no.
They are buying hundreds of thousands of acres, though.
So there is that.
And that's normally the way I've seen this, is not with millions,
but with hundreds of thousands.
OK.
So China, according to the last numbers I saw,
which were last month, China owns about 400,000 acres
of agricultural land in the US, a little less than.
Raw numbers?
That sounds like a whole lot, especially
when it's paired on a meme with something
like China owns 400,000 acres of US farmland.
Disney World is only 30,000 acres or something like that.
I mean, yeah, that's a big difference.
But Disney World is a theme park.
It's not US farmland.
It's a dot when you're talking about the entire map.
It's very much an apples to Florida oranges comparison
there.
So what about an apples to apples comparison?
400,000 acres Chinese investment.
Ever heard of King Ranch?
If you're in the United States, you probably
associate it with a truck.
Because on the side of some trucks, it says King Ranch.
OK, King Ranch is a real place.
It's a ranch in Texas.
That ranch, one ranch, 825,000 acres.
All of a sudden, it doesn't seem like such a big deal.
400,000 acres isn't quite as huge as it sounds.
But to be honest about it, King Ranch is huge.
That is not a normal ranch.
It's massive.
It's so big, you could brand a truck with its name.
So rather than looking at raw numbers, which
is what a lot of memes that are fear-mongering
kind of rely on, look at percentages.
Look at percentages, because it gives you a much clearer
picture.
All foreign agricultural investment in the US,
acres owned, about 3%, a little less than 3%,
of US agricultural acreage is owned by foreign countries.
Of that 3%, China owns 1%.
1% of 3%.
They're not even a big investor when
it comes to agricultural land in the US.
They're really not.
Our biggest foreign countries that
invest in US agricultural land, it's
like Canada, the Netherlands, the UK, France, countries
like that. China isn't anywhere near the top of the list. 400,000 acres is not
actually that much and it's important to understand that they really actually
didn't want all that. Most of it came to them when they bought a big pork
producer and they wound up with it. So is it real? I mean China is buying farmland
in the US, but not a whole lot, certainly not enough to starve us.
And then you can look at the claim in the meme itself.
In the event of a war, China is going to do what with this farmland?
Not produce on it?
Okay, give me a scenario in which the US government doesn't seize it.
It's not real.
It's fear-mongering, it's the same stuff.
This has been around, versions of this have been around for a really long time since the
actual concern wasn't really China, but the UN was coming to take over and all of that
stuff, that theory.
It's not real.
It wasn't real then, it's not real now.
This is just business.
So while it isn't something you should particularly be worried about, just be certain that politicians
are going to lean into this to scare people.
We have a new, you know, near-peer contest thing and they've got to frame it and they've
got to get that propaganda rolling and this is a way that they will certainly try to scare
Americans into thinking that 400,000 acres is like a huge deal.
It's like really concerning.
One percent of three percent.
Not a big deal.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}