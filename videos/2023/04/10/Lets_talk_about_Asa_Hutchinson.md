---
title: Let's talk about Asa Hutchinson....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=PxBlCdsqouo) |
| Published | 2023/04/10 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Asa Hutchinson, seeking the Republican Party's presidential nomination, faces questions about his background and beliefs.
- Hutchinson, a former governor of Arkansas, has extensive executive branch experience and is considered to the right of a normal conservative.
- Despite calling on Trump to drop out, Hutchinson may not stand a chance due to his history of prosecuting far-right groups.
- His past actions against far-right groups may alienate the energized base of the Republican Party.
- Candidates openly opposing Trumpism are starting to emerge, and as Trump's legal problems mount, more may take a stance against him.

### Quotes

- "He's to the right of normal, but still within the normal spectrum."
- "One of the problems that he is definitely going to run into is that while most of the groups that he went up against back then, they're gone."
- "This is one of those candidates that is coming forward and openly opposing Trumpism."

### Oneliner

Asa Hutchinson, seeking the Republican presidential nomination, faces challenges due to his past actions against far-right groups, potentially hindering his chances in a crowded field where candidates are openly opposing Trumpism.

### Audience

Political analysts, voters

### On-the-ground actions from transcript

- Support candidates openly opposing harmful ideologies (implied)

### Whats missing in summary

Insight into how the political landscape may shift as more candidates openly oppose Trumpism and as Trump's legal troubles increase.

### Tags

#AsaHutchinson #RepublicanParty #PresidentialNomination #Trumpism #FarRightGroups


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Asa Hutchinson
and his announcement.
He is seeking the nomination
from the Republican Party for president.
Since that announcement was made,
a whole bunch of questions have come in about him.
And they're pretty general
and most of them are pretty easy to answer.
So we're just gonna kind of run through them all
and then get to the part that really matters.
So, first, who is he, former governor of Arkansas.
Held positions with DEA, I think,
and Department of Homeland Security.
Had a whole bunch of stops along the way.
Lots of executive branch experience.
run for the White House, is the next logical step for him.
Is he a normal Republican?
Yeah.
To me, he would be kind of to the right of what people view as like a normal conservative,
pre-Trump, non-fasci type of conservative.
He's to the right of normal, but still within the normal spectrum.
He's actually called on Trump to drop out, called on Trump to drop out, said he shouldn't
be seeking the nomination at all.
So most of the people who watch this channel will definitely have policy agreements or
disagreements with him.
But you know, he's not super-fashy, which at this point is a win as far as the Republican
Party's concerned.
Does he stand a chance?
Nah, I don't think so.
I don't think so.
And it's actually something in his history that is going to sabotage him, I think.
In the 80s, he was the U.S. attorney for, I want to say the Western District of Arkansas.
Could be wrong on that.
But he kind of made a name for himself going head-to-head with far-right groups, taking
them down, prosecuting them.
And at times became very hands-on in this.
So one of the problems that he is definitely going to run into is that while most of the
groups that he went up against back then, they're gone, a lot of the far-right groups
a day, they kind of draw their lineage from groups that he shut down.
That energized base that the Republican Party has cultivated, they are in many ways sympathetic
to the groups that he shut down that gave him a reputation as being kind of distinguished
in the field of going up against domestic T-words, I think that that's probably going
to derail his campaign.
Because when that group finds out his history when it comes to this, his social media presence
is just going to be constantly disrupted.
They're going to go after him.
I would imagine they would feel like they had to because they'll see him as kind of
a threat to them.
So I don't think he has a chance of really getting ahead in the primary.
It's possible because he has a lot of friends and a lot of influence.
But I have a feeling the MAGA energized base, they're going to shut him out and disrupt
him pretty quickly.
That being said, this is one of those candidates that is coming forward and openly opposing
Trumpism.
So you're starting to see that.
And my guess is as the field kind of crowds and more come out, you'll see more and more
people take a stronger stance when it comes to going against Trump.
You'll start to see candidates endorse other people seeking the nomination.
You'll start to see little things at first.
But as Trump's legal problems mount, he's just going to become less and less appealing
to the political establishment.
That energized base, though, that may stick with him, so we're going to have to wait
and see.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}