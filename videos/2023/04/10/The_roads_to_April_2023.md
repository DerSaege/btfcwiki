---
title: The roads to April 2023....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=5-Ugu-fxz7I) |
| Published | 2023/04/10 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Monthly Q&A session with Beau covering various topics and questions from the audience.
- Addressing skepticism towards Trump's statements by not taking his word seriously due to past experience.
- Dismissing the notion of stopping support for reproductive rights, pointing out the importance of consent.
- Explaining laughter while mentioning the names of arrested Democrats in Florida as a sign of camaraderie and support.
- Acknowledging a viewer's insight on the term "well-spoken" and its implications on people with accents.
- Clarifying the misconceptions surrounding a controversial joke involving kicking a box.
- Explaining the decision-making process behind covering certain news topics over others based on audience interest and coverage.
- Dissecting a misunderstood joke in the context of Iraq and why it faced backlash.
- Teasing a potential future video addressing leaked secret documents with a personal hunch.
- Addressing questions about the YouTube Studio app and sharing positive views on its utility.
- Explaining how the economy grows beyond printing more money by considering factors like velocity and economic activity.
- Responding to preferences for biblical teachings over suggestive content by suggesting attending a Christian school and reading the Bible.

### Quotes

- "I don't immediately assume he's lying. I don't base anything off of what Trump says."
- "Your belief system is based on the fact that the rest of your statement is not true. Just saying."
- "Generally speaking, whatever he's saying is right."
- "The more economic activity, the faster the money moves, the larger the GDP."
- "Then send them to a Christian school."

### Oneliner

Beau addresses a range of topics from Trump's credibility to controversial jokes, showcasing his views on various societal issues with candor and insight.

### Audience

Engaged viewers seeking perspectives on current events and societal issues.

### On-the-ground actions from transcript

- Analyze past statements and actions of public figures before forming opinions (implied).
- Advocate for consent and reproductive rights (implied).
- Educate oneself on the implications of certain terms and stereotypes (implied).
- Stay informed on a variety of news stories to understand differing perspectives (implied).

### Whats missing in summary

Insightful perspectives on current events and societal issues, delivered with clarity and candor.

### Tags

#Q&A #Trump #ReproductiveRights #Controversy #Economy #BiblicalTeachings


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to do a Q&A.
This will be the one for April.
I'm going to try to do one of these every month now.
And they will be either questions like this
that I don't know what they are until I read them
and they were picked up by the team,
or they will be questions that I tell them to put in
with them because I thought they were good,
but they weren't enough for a full video.
And then also coming up on this channel soon will be the community garden one and the relief one both of those videos
are in their final stages.  I know they've been coming for like eight months now.
Okay, so let's dive on in here.
How did you know Trump was lying?
He was talking.
Now, I'm assuming this is about when he said he was going to get picked up and then wasn't.
Generally speaking, when it comes to Trump, I put absolutely no stock in what he says.
One way or the other.
I don't immediately assume he's lying.
I don't immediately assume he's telling the truth.
It means nothing to me.
Nothing.
I don't base anything off of what Trump says.
And that just comes from long experience of having covered him.
Stop being for reproduction rights.
As a Christian, I can tell you the 100% foolproof way to not get pregnant is to not spread your
legs.
Wow.
Okay.
We'll do this in sections.
Stop being for reproduction rights, no.
And as far as the rest of this, you're removing the whole idea of consent, which is something
that should definitely remain part of this conversation.
Aside from that, I would also point out that as a Christian, your belief system is based
on the fact that the rest of your statement is not true.
Just saying.
Why the obvious smirk when you mention the names of the two Democrats arrested?
Okay, this came in a couple of times.
Recently in Florida, there were two high-level Democratic Party members who were picked up
at a demonstration. One of them was Senate Minority Leader Brooke, the other one was the
chair of the party here in Florida, Nikki Fried. When I was covering it, when I got to their names,
I did laugh. That wasn't me smirking in a no-smirking zone. That was camaraderie.
If you don't know, I actually hold, I think that Nikki Fried is exactly what the Democratic
party needs in Florida. And in my interactions with her, she's been a very genuine person.
That wasn't me making fun of them. That was a very supportive laugh.
You helped, but since it came up again, what really helped was somebody in the comment section
talking about black people being called well-spoken. I know what that means for me and you and never
made the connection. That really helped me a lot. That makes sense. So this is a
follow-up. A while back a farmer sent in a message asking for evidence of
systemic racism. I provided anecdotal stuff from their own life, stuff that
they could see. And that topic came up again recently and I referenced it. So
there's that. The well-spoken, yeah that makes sense. That makes sense. If you
don't know people who sound like me, if we can string together a coherent sentence that
is about anything other than like rebuilding a carburetor, we will be called well-spoken
or articulate.
And it means the exact same thing.
It means when people say that about black people.
Because the default is the assumption that if you sound like me, you're not very smart.
And yeah, I can see how that would hit home for him.
Yeah, if you don't know, there are a whole lot of people with accents who, when they
move out of the South or they get a professional job, they go out of their way to learn how
to talk without an accent and speak in a very clear and non-regional manner, so nobody immediately
assumes that we're an extra from deliverance.
I believe that's known as the customer service voice.
We have the exact same thing.
Your school security video over on the other channel,
this channel, was lost on a lot of people.
They don't understand that none of that
has to make it look like a present.
Yeah, I noticed that after the fact.
I noticed that after the fact.
I put out a video over here detailing
how to put in basically ringed security
create a delay for first responders to arrive. All of that sounds really complicated and sounds
like it would make the school look like a fortress. It doesn't. Most, like, high-end hotels and
resorts that you've been to have better security than what's outlined there. It doesn't have to
look that way. Why did you cover the Florida arrested instead of Tennessee? Your entire comment
section was begging for your take on it, not really complaining, it's your channel, cover
what you want, but how do you make decisions on what to cover?
So at the same time as what happened in Florida happened with the Democratic Party members
getting arrested, there was the thing that has now just turned into a massive national
news story in Tennessee.
Generally speaking, when there are two topics that are kind of closely related or lead to
the same point and one of them is getting a bunch of coverage and the other is not,
I will cover the one that isn't getting coverage.
If everybody in the comments section is asking about something, I know they already know
about it.
So there's no urgency to put out a video on it because it's already getting a lot of coverage
where the other thing may end up getting missed if coverage isn't put on it.
So that's something that is pretty common in my decision making process there.
Can you explain and defend the kick the box meme?
That woman is still getting trashed over a common joke that people don't understand.
Or at least explain why people aren't getting it or coming to her defense.
Yeah, I cannot defend that joke.
I can explain it, and as I do, I'm willing to bet you will realize what the difference
is.
If you don't know, a person made a joke, but it's not the same joke.
It's a variation of a joke that was told for a really, really long time, and nobody
got upset.
They changed it just a little bit, and everybody got mad.
include vets, analysts, people who look at rights, people who cover these types of issues.
So the joke, and this is in the context of Iraq, the joke that was told that made everybody
get upset was me trying to convince a local to go kick that box. Now, if you don't know the
boxes, anything interesting on the side of the road, you have to assume that it's something that
might go boom. And variations of this joke were told for a really, really long time.
But, if you're familiar with those jokes, think about the difference.
What are the other ones you know?
Sitting here waiting for EOD, waiting for the people to come clear it, has me ready
to go tell the LT his compass is in that box.
We got an interesting box in the road, go get the new guy.
Sitting here so long, I've decided I'm going to just start lighting up every box we see
on the road.
All of the other jokes are about the frustration of waiting, and they're not talking about
hurting a civilian.
That's the difference.
That's why nobody came to her defense, and that was where the line got crossed.
The joke transferred from being super frustrated standing around to hurting somebody.
That was, and no, there aren't going to be a lot of people that are going to defend that.
Especially, so if you don't know, the whole purpose of this practice, which was kind of
dangerous, you're standing around, the whole purpose of it was to make sure that no US
troops or civilians got hurt.
That's why they were doing it.
That's why that occurred, and I don't know anybody that would be like, ah, that's totally
cool.
It changed the joke from frustration at standing around to hurting an innocent person.
It seems like it's just a slight variation of something that was told for a long time,
it totally changed the joke. Generally speaking, any jokes that are common in wartime, don't
go out of your way to make them edgier. That's not a good idea.
Why are you not talking about the leaked secret documents? That happened like two days ago.
I'm not. I have a hunch on that. I have a hunch on that, and I will let y'all know
what my hunch is in a few weeks. But no, I'm probably not going to talk about that unless
they go ahead and pick somebody up for it. What are the people complaining about talking
about here, this is exactly what I was taught.
It's a screenshot, and it has a video, but I can't actually,
I don't get to watch the video, so I don't know.
Oh, OK, now I see why this was here.
OK, so it looks like the person is demonstrating
what's called a quick peek.
And this is a person on a range demonstrating
how to do something.
And yeah, they're in kind of a weird pose
because they're caught in action.
The important part to note here is that that's Pat McNamara.
Generally speaking, whatever he's saying is right.
So I'm looking at some of the comments that are here below it.
And he wouldn't make it in real life.
Total amateur.
For those who don't know who this is, this guy was like a
Delta weapons instructor.
Funny, but without being able to watch the video, I can't
tell you anything other than the comments or humor is given
who it is.
What do you think of the YouTube Studio app?
I saw somebody say it was bad for productivity and would
hurt your channel.
I don't see it that way at all.
I think it's a really good tool.
I mean, I can see how some people might get sucked into it,
but that provides you a lot of information
at the palm of your hand right there.
I think it's a good idea.
I use it.
How does the economy grow without printing more money?
I always see them say the economy is growing
and the GDP is growing.
Doesn't that just mean they printed more money?
No.
You have money supply, and that plays into it,
but you also have velocity.
A dollar, a single printed dollar,
can change hands multiple times.
The faster it changes hands, the stronger the economy is,
the more it's growing.
That's what that is, because you're adding up
all of the times when you're talking about the GDP,
you're talking about pretty much all the times
that dollar changes hands.
So the more economic activity, the faster,
the money moves, the larger the GDP.
Can you just not accept some people don't want suggestive
things and want biblical teachings instead?
Then send them to a Christian school.
And you really might want to read the Bible.
Actually sit down and read it.
There are quite a few suggestive things in it.
OK, so those are the questions.
Those are the questions that came in.
So I hope everybody kind of enjoyed that.
And there is more coming on this channel soon.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}