---
title: Let's talk about Thomas, vacations, and the Judiciary Committee....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=xJe2gge7rq8) |
| Published | 2023/04/11 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Democrats in the Senate are planning to address Justice Thomas's unique vacation habits that are being examined.
- The Senate Judiciary Committee has sent a strongly worded letter to the Chief Justice, indicating a hearing to restore confidence in the Supreme Court's ethical standards.
- If the issue is not resolved internally, the Committee may take legislative action.
- An internal investigation requested by the Chief Justice may be more favorable for Thomas.
- Democrats in the Senate may propose legislation or conduct hearings if pressure continues to build up.
- Focus may shift from vacations to Thomas's use of a private jet, which could be more problematic.
- Public perception may be influenced by media coverage, potentially normalizing Thomas's vacation activities.
- Investigation or hearings may prioritize Thomas's use of the private jet over other vacation aspects.
- The outcome could involve internal Supreme Court processes or lead to more significant consequences for Thomas.
- The Judiciary Committee's actions are ongoing but not yet definitive.

### Quotes

- "An internal investigation that is done at the request of the Chief Justice would probably have a more favorable outcome for Thomas."
- "The vacations, the yacht, all of that stuff, you've already seen the headline since then."

### Oneliner

Democrats in the Senate address Justice Thomas's vacation habits, signaling potential legislative action or hearings to restore confidence in the Supreme Court's ethical standards.

### Audience

Senators, Judiciary Committee

### On-the-ground actions from transcript

- Contact your Senators to express opinions on ethical standards in the Supreme Court (implied)
- Stay informed about developments and potential actions surrounding Justice Thomas (implied)

### Whats missing in summary

Further details on the potential impacts of media coverage and public perception regarding Justice Thomas's vacation activities.

### Tags

#SupremeCourt #JusticeThomas #SenateJudiciaryCommittee #EthicalStandards #Legislation


## Transcript
Well, howdy there, Internet people, it's Beau again.
So today we are going to talk about the Supreme Court, restoring
confidence in the court and what Democrats in the Senate are planning
to do in regards to vacation habits of a certain Supreme Court
justice and where it may go from here. If you have no idea what I'm talking about,
Justice Thomas has some unique vacation habits that are being examined. I'll
have a video down below in the description to provide a little bit
more insight into it. The Senate Judiciary Committee, the Democrats on the Senate
Judiciary Committee basically sent a very strongly worded letter to the Chief
Justice. It says, in the coming days the Senate Judiciary Committee will hold a
hearing regarding the need to restore confidence in the Supreme Court's ethical
standards. It goes on, if the court does not resolve this issue on its own the
committee will consider legislation to resolve it but you do not need to wait
for Congress to act, to undertake your own investigation into the reported
conduct, and to ensure that it cannot happen again. We urge you to do so." Okay,
so the Democratic Party, the Senate Judiciary Committee, is basically saying
you clean this up or we're going to.
An internal investigation that is done at the request of the Chief Justice would probably
have a more favorable outcome for Thomas.
It might also get to the point when it comes to recommendations on what to do, basically
follow the rules in the future, here are the new ethical guidelines.
That is a likely outcome.
The alternative would be continued pressure and it starts to build up and Democrats in
the Senate feel like they have to do something about it.
Which point?
They propose legislation or they conduct hearings of their own.
If they do that, understand, like we talked about in that first video, it's not really
going to be about the vacations.
The yacht, all of that stuff, you've already seen the headline since then.
Stuff like Supreme Court justices can have friends too.
There's cover for that.
But it looks, by the reporting that Thomas used that private jet at other points in time,
that will become more problematic than anything involving a mega yacht.
It's certainly the way it looks to be shaping up, because there is that cover.
Doesn't mean it's right.
It just means that if the vacations are what gets focused on, most of the public is going
to be like, uh, because they're going to be inundated with articles saying, it's okay.
This happens sometimes and stuff like that.
But that use of the private jet, if it did in fact occur without his host, well that's
something different.
if there is an investigation or a set of hearings, my guess is that the plane
rides are going to be way more important than anything else. Now, the strongly
worded letter, we'll have to wait and see where it goes. It might prompt the Chief
justice to act, it might not. And there's a whole bunch of variables at this point
as far as where it goes from here, but your most likely options are an
investigation that is kind of internal to the Supreme Court, in which case
there's probably not a lot that's going to happen to Thomas. If it becomes a big
deal and there are hearings, it might change. There might be a larger chance of him maybe deciding
it's time to retire or something along those lines or some kind of penalty imposed. But
But there's still a whole lot up in the air and the Judiciary Committee, they're not...
They are acting, which is what they promised, but it's not exactly decisive.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}