---
title: Let's talk about a win in Nashville....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=JCTCe6bvbVc) |
| Published | 2023/04/11 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Nashville Metropolitan Council voted 36-0 to reinstate Justin Jones and another representative unjustly removed by the Tennessee State Legislature.
- Jones and the other representative will be interim representatives until a special election is held.
- The normal election is more than a year away, prompting the need for a special election.
- Both representatives are qualified and intend to run for their seats again in the special election.
- The special election is expected to occur within the next hundred days.
- It is implied that Jones and the other representative will likely win due to their newfound celebrity status.
- Beau urges the people of Tennessee to use the ballot box in 2024 to voice their disapproval of what happened.
- Voters need to make it clear that removing the representatives was unacceptable.
- Those who supported the removal should face challenges in getting re-elected.
- Beau warns that complacency could lead to Tennessee becoming more authoritarian and intrusive.
- Failing to hold the state legislature accountable could result in diminished representation for the people.
- Beau stresses the importance of using the ballot box to uphold representative democracy.
- He cautions that allowing unchecked actions by the legislature will have severe consequences.
- Beau encourages Tennesseans to take action before it's too late and their voices are silenced.

### Quotes

- "You need to take the opportunity to use the ballot box and explain that that isn't how it's supposed to work."
- "Anybody who voted for this, they need to have a really hard time getting re-elected."
- "If you don't, you'll regret it."

### Oneliner

Nashville reinstates unjustly removed reps; Tennesseans urged to use 2024 ballot to hold legislature accountable and defend democracy.

### Audience

Tennesseans

### On-the-ground actions from transcript

- Use the 2024 ballot to express disapproval of the unjust removal of representatives (implied).
- Ensure that those who supported the removal face challenges in getting re-elected (implied).
- Take action now to prevent Tennessee from becoming more authoritarian and intrusive (implied).

### Whats missing in summary

The full transcript provides a detailed explanation of the events in Nashville, the significance of the special election, and the urgency for Tennesseans to assert their voices through voting in 2024.


## Transcript
Well, howdy there, internet people.
Liz Bow again.
So today we are going to talk about some news coming out
of Nashville, Tennessee.
It is news that I think most people watching this channel
are going to be pretty happy about.
Not exactly surprising, but it is nice to see it actually
being carried out.
Then we will talk about where everything goes from here,
what happens next.
And then we'll talk about what the people in Tennessee
need to do to make sure that this doesn't happen again. Okay, so the Nashville Metropolitan
Council voted 36-0 to send Justin Jones back to his rightful seat in the state legislature.
Unanimous decision. He will be the interim representative. There is a high expectation
Pearson will also be returned. So these are the two that were unjustly removed
in an exercise of power by the Tennessee State Legislature.
They will be the interim representatives. Interim because there's going to be a special election.
The reason is basically because the normal election is more than a year away. So there will be a
special election, and my understanding is that not only are both of them qualified
to run for their own seats again, they also intend to. And I think this will
happen sometime in the next hundred days or so. It'll happen pretty quick, and
given their newfound celebrity status, they'll probably win. Okay, so that is
That is the news.
Now what about for people in Tennessee?
This is a good start.
The Tennessee state legislature, intentionally or unintentionally, sent the message that
voters in Tennessee do not matter, that they don't deserve representation, that they are
worthless, that they are owned, and that they should do what they were told.
That was the message that was received by most people, and it should be the message
received by pretty much everybody in Tennessee.
In 2024, it is incredibly important that the people in Tennessee use that ballot box to
say that this was unacceptable.
Anybody who voted for this, they need to have a really hard time getting re-elected.
And I know for a whole lot of people in Tennessee, you're like, well, I'm a Republican.
That's fine.
You could put another Republican in and there's a primary process.
But the people who did this, they showed you who they are and you better believe them.
I know that a whole lot of people in Tennessee are going to be like, well, I don't agree
with the ban.
I don't like what those two guys were saying.
And that's fine, too.
That is fine, too.
But that's how it always works, isn't it?
The eyes of tyranny are always looking at your perceived enemies at first, if you're
in the majority.
how they get everybody to go along with it. And then, while they need a new
perceived enemy, eventually those eyes will turn to you. It has been a long time
since I lived in Tennessee, but when I was there, the idea that strong fences
made good neighbors, that was pretty prevalent. If those in the state
legislature who were behind this, they don't get an electoral message saying
that this was wrong, understand you are saying that you don't care about Tennessee, you
don't care about the country, you don't care about the Republic, you don't care
about representative democracy, you don't care about any of that.
You care about the party and the party owns you and you will do whatever they say.
You better just go ahead and give the state legislature the key to your gate and your
front door because if this goes unchecked, make no mistake about it, Tennessee will become
more and more authoritarian and more and more involved in your private life.
They don't think that you deserve representation.
You don't matter.
Your voice doesn't matter.
They're not your representatives, they're your rulers right now.
And they've made that clear.
You need to take the opportunity to use the ballot box and explain that that isn't how
it's supposed to work.
If you don't, you'll regret it.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}