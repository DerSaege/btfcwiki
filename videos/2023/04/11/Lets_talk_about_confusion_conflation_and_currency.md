---
title: Let's talk about confusion, conflation, and currency....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=puCG21F6oUk) |
| Published | 2023/04/11 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing confusion and conflation around the term "FedNow" due to inaccurate information circulating.
- Explaining that FedNow is not a digital currency and is specifically for interbank communication and quick currency transfers.
- Mentioning that most people won't interact with FedNow unless routinely transferring large sums between banks or during major transactions like buying a house.
- Clarifying that the concept of a digital currency being discussed is separate from FedNow and is still in the exploratory phase.
- Emphasizing that the development and implementation of a digital currency in the US require congressional approval, unlike FedNow.
- Comparing the proposed digital currency ideas to existing payment platforms like PayPal or Cash App, dispelling conspiracies around it.
- Noting that the idea of a digital currency is not as futuristic or alarming as it may seem, given the prevalence of digital transactions currently.

### Quotes

- "It's not a digital currency. It's not even consumer-facing."
- "It's just an additional tool."
- "Most people already use something kind of like it."
- "Most of it's really just reinventing things that already exist."
- "It's not even that big of a step from where we're at."

### Oneliner

Beau clarifies confusion about FedNow not being a digital currency, dispels fears around its implications, and explains the speculative nature of a potential US digital currency.

### Audience

Financially curious individuals

### On-the-ground actions from transcript

- Stay informed about financial matters (implied)
- Educate others on the difference between FedNow and potential digital currency ideas (implied)

### Whats missing in summary

Details on the specific misinformation or conspiracy theories surrounding FedNow and digital currency proposals.

### Tags

#Finance #DigitalCurrency #FedNow #Misinformation #Congress


## Transcript
Well, howdy there internet people, it's Bo again. So today we are going to talk about confusion, conflation, and
currency
because a lot of questions have come in with a certain term and
that people have sent in memes about it and just directly asked and sent in all kinds of stuff and
they are
less than accurate to use that term.
I think some people are genuinely confused, and I think some people are intentionally conflating it to create a new
scary thing, a new boogeyman.  So we're going to talk about FedNow.
If you have seen the memes, you know what I'm talking about, if you haven't, get ready to, because I have a feeling this
isn't going to go away anytime soon.
Fed now is a real thing. It absolutely is. That is a real thing that is going to roll out, I want to say, in July.
It is not a digital currency. It's not even consumer-facing. This isn't something that you or I, under normal
circumstance, will ever interact with.  This is a method for one bank to talk to another bank and to send
currency quickly.
That's it. That's it. It's not a it's not a digital currency. The only people that
actually really need to care about this are people who routinely transfer large
amounts of money between banks or banks themselves. For most people this is only
going to come into play when you're like buying a house or if you get direct
deposit. You might get your money a little bit faster. That's it. It is not a digital
currency. And I know somebody is about to say, well I saw this paper in the study. Yeah,
you probably did. There are actually a bunch of them. It seems like they've focused in
on one. But there are a bunch of think pieces that are put out by financial wizards in this
country talking about it. And some of them were actually commissioned by the US government
create a digital currency. So here are the things to know about that. Number
one, it's not FedNow. It's not the same thing. Number two, there are ideas. They're
looking at it. They haven't even decided that they want it yet. It is certainly
not scheduled to be rolled out. They haven't developed it. They haven't even
and decided that they want to develop it.
Three, I haven't seen a single one of these ideas
where it's actually a replacement for cash.
It's just an additional tool.
Four, if this was to go forward, understand
this isn't like Fed now.
It's not something where the banks can just snap their fingers
and make it happen.
This is something that would go through the House, the Senate,
and be signed by the president.
This would take a literal act of Congress to occur.
And then the last thing that I think possibly
the most important thing is, having read these,
it appears that the brilliant, insightful, groundbreaking,
revolutionary new idea is basically reinventing PayPal or cash app or
whatever it is that you use. None of the ones that I have seen are anywhere
near the the conspiratorial rumors that are out there. It's pretty simple stuff
most of it. In fact, most people already use something kind of like it. Now, I'm sure there
may be some wild study or paper out there that I haven't seen, but most of them it's really just
reinventing things that already exist and making it like a government thing rather than a private
thing. Again, something like that, not something I would necessarily use, but
also not super scary. And then I think the one part of this that I think most
people should remember is that while the idea of a digital currency, it sounds
scary because of decades and decades of of it being used as a plot device in
movies or books or whatever, most people kind of already run on a digital
currency with your debit card and everything else like that. It's not it
isn't as far-fetched as it sounds. It's not even that big of a step from where
we're at. I'm not saying I necessarily like it. I'm just saying you're talking about
the future. I'm pretty sure in the future there will be a digital US currency at some
point. But as of right now, that's not happening. They haven't even decided that they want
to do it and it is not fed now.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}