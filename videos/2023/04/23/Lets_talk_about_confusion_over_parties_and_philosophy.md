---
title: Let's talk about confusion over parties and philosophy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Yq05F5_fz3U) |
| Published | 2023/04/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau Young says:

- Explains the confusion between a political party in the United States and a component of a political ideology with the same name.
- Defines the political spectrum with left and right politically, as well as authoritarian at the top and libertarian at the bottom.
- Mentions the Libertarian Party in the United States being inherently right-wing and pro-capitalism.
- Notes that while the Libertarian Party is right-wing, there are left-leaning libertarians globally.
- Points out that small government conservatives of the Republican Party better align with the Libertarian Party today.
- Suggests that individuals who lean towards small government conservatism but are hesitant to vote for the Democratic Party may find alignment with the Libertarian Party.

### Quotes

- "Libertarian means two different things."
- "Left-leaning libertarians in Europe were like it. That is not true."
- "So you end up with some really weird dynamics at times."
- "They probably represent your ideals a whole lot more than people who want to rule over."
- "Anyway, it's just a thought, y'all have a good day."

### Oneliner

Beau Young clarifies the confusion between the Libertarian Party and libertarian ideology, suggesting small government conservatives to look into the Libertarian Party's platforms.

### Audience

Political enthusiasts

### On-the-ground actions from transcript

- Research and understand the platforms of the Libertarian Party to see if they better represent your ideals (implied)

### Whats missing in summary

Deeper dive into the differences between left and right politically, and understanding the nuances of libertarian ideology and political party in the United States.

### Tags

#Politics #Libertarian #PoliticalIdeology #RepublicanParty #SmallGovernment


## Transcript
Well, howdy there, internet people, it's Bo Young.
So today, we are going to talk about some confusion
that has occurred, and we're going to talk about a party
in the United States, and a component
of a political ideology, and how they share the same name,
and how that gets confusing at times,
because in a recent video, I said something
and people from Europe and Australia and Africa
sent messages basically like, you're wrong.
That is not true.
Yeah, we'll get there.
OK, so for everybody, quick review on something.
There's left and right politically.
Most people know what that is.
That's used pretty much everywhere,
means different things in different places.
But generally, people are aware of what left and right are.
On the political spectrum, there is also a vertical axis, which is authoritarian at the
top and libertarian at the bottom.
On this channel, I generally say anti-authoritarian to avoid, well, this, this exact confusion.
There are left-leaning libertarians all over the world.
you are using that political spectrum.
In the United States, there is also a political party named Libertarian.
You have Republican, Elephant, Democrat, Donkey, Libertarian, Porcupine.
It's a small party.
They are inherently right-wing, and that was the part that had people a little upset.
left-wing libertarians in Europe were like it. That is not true. Not all people
who are ideologically libertarian are right-wing. There are some who might be
incredibly libertarian and be left-wing. I'm certain you know some. In fact, I
would suggest that most people watching this channel probably fall into that.
But in the United States, the political party is inherently right-wing, very pro-capitalism.
The weird part is, they tend to be accepting of a lot of socially progressive positions.
And by that, what I mean is, they may privately condemn whatever it is.
But it is absolutely not the government's business to regulate it.
So you end up with some really weird dynamics at times.
So the Libertarian Party of today, the best way to look at them is that they are the actual
ideological descendants of the Republican Party that was small government conservative.
Small government conservatives of the Republican Party definitely better align with the Libertarian
Party than they do the Republican Party of today, and the Libertarian Party has issues
with getting that message out. Realistically, they should be much bigger than they are.
that a whole lot of them are definitely more ideologically predisposed to the Libertarian
Party than they are to the Republican Party. But they have a messaging issue, like a lot of
small parties do. But that's the root of that confusion. Libertarian means two
different things. It means being against an authoritarian government, but in the
United States it's also a specific political party. So, and if you are one
of the small government conservatives that watch this channel, who just can't bring
yourself to vote for the Democratic Party, you might want to take a look at the Libertarian
Party and at their platforms.
They probably represent your ideals a whole lot more than people who want to rule over
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}