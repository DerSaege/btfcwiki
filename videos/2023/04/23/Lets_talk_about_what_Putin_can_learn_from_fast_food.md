---
title: Let's talk about what Putin can learn from fast food....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=7HVGnAW4x74) |
| Published | 2023/04/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Putin's reliance on reports is resulting in bad decisions as he views external information as propaganda.
- The focus on metrics and measurements in authoritarian countries may not be the root cause of the issue.
- Beau draws a parallel from his experience at a fast food joint where an emphasis on car wait times led to counterproductive actions.
- The pressure on a specific metric caused delays in serving customers efficiently.
- The obsession with numbers led to impractical solutions like creating parking spaces for waiting customers.
- Companies often prioritize reports and numbers over the actual goals they are meant to achieve.
- Russia's military also fell into the trap of prioritizing arbitrary numbers in reports, giving a false impression of efficiency.
- Beau argues that this issue is not exclusive to authoritarian states but is a result of poor leadership and a focus on irrelevant metrics.
- He warns about the dangers of institutionalizing this mindset as it can lead to flawed policy decisions.
- Beau advocates for understanding the purpose behind reports and metrics to avoid losing sight of the actual goals.

### Quotes

- "Focus on a good example here. Focus on street price."
- "When you're looking at reports, always remember why that report was generated and what the goal was."
- "It's a symptom of bad leadership."
- "They became so focused on meeting arbitrary numbers that they just started putting it in the report."
- "The obsession with numbers led to impractical solutions."

### Oneliner

Putin's reliance on reports leads to bad decisions, reflecting a broader issue of prioritizing metrics over goals, seen in fast food and military contexts.

### Audience

Leaders, decision-makers

### On-the-ground actions from transcript

- Question the purpose behind metrics and reports (implied)

### Whats missing in summary

The importance of focusing on goals rather than getting lost in metrics and reports.

### Tags

#Leadership #Metrics #DecisionMaking #Putin #FastFood #Military


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Putin and reports
and measuring things and what Putin can learn
from American fast food restaurants.
There have been a number of articles
that have come out recently.
And they're focused on how Putin is ending up
with bad information.
and he's basing using these reports to make decisions
and it actually just makes things worse.
And because he trusts the reports,
he's viewing everything from outside of Russia is just,
well, that's just propaganda
and it doesn't factor in to his decision-making.
And a lot of the articles are chalking this up to,
well, this is just one of those things that happens
in authoritarian countries.
I mean, OK.
I don't know that that's entirely accurate, though.
It seems more like a symptom of focusing on measurements,
metrics, rather than the reason those metrics were created.
When I was a kid, 15, 16, something like that,
worked at a fast food joint.
And during the period that I was there, they installed a clock.
They installed a clock, and the clock basically measured
how long a car sat at the window.
And the higher-ups, the corporate overlords,
they were really interested in this number.
So much so that even today, I can remember how just adamant
the managers were about the amount of time a car spent at the window.
Why was this measurement being kept?
To speed things up.
To make sure that the customer got their food as quickly as possible.
That's why the measurement exists.
But they put so much pressure on that number, what happened?
You know, ma'am, your fries are going to take a little bit to cook.
Can you do me a favor and just pull up right up there and we've got to keep the lane clear.
We'll have somebody run your food right out to you."
What did that do?
It actually made the person wait longer for their food.
They were so focused on that metric, on that number, on that report that it caused people
to, well, kind of cook the books on it.
They moved the car ahead and then somebody had to run the food out to them, which made
the customer wait longer.
To this day, you will see this.
In fact, you will even see locations that have like parking spaces for people to be
pulled up to, which doesn't actually speed things along for the customer in most cases.
Because what happens is somebody then has to run the food way out to the parking space,
which means that they're not back there bagging up the next order, and it slows things down.
Or the company ends up hiring another person, increasing the labor costs.
just and it all stemmed back to a focus on how long a car sat at the window but
the people further up the chain they're just looking at that report and they're
not really factoring in the reason the number is supposed to be 45 seconds or
whatever it was and then they institutionalized getting around it
when, if they had focused on the goal, which is getting the customer the food
faster, what would they have done? They would have put a door at the drive-through
so people could walk out the door, employees could walk out the door and
hand it to a customer that is one car back or two cars back. So their food goes
as quickly as possible, they pull out of line and go, and the person up front gets
theirs as quickly as possible.
But rather than thinking of the goal, they were focused on the numbers on the report.
It happens everywhere.
It's an institutional memory thing.
But that is definitely something that happened in Russia with their military.
They became so focused on meeting arbitrary numbers that they just started putting it
in the report, you know, how many of, I'm making these up, but you know, how many of
your troops qualified and can hit the tin ring, you know, four out of five times?
Ninety-five percent.
They may not have even gone to the range, but they put that in there and then it went
up and the next commander up may have bumped it up and said ninety-six percent by the
time Putin gets it, everything looks perfect. The Russian military looks like
a well-oiled machine when in reality a lot of their machines hadn't been oiled
in 10 years. It's not a symptom of an authoritarian state. It's a symptom of bad
leadership. It's a symptom of focusing on arbitrary metrics rather than the end
goal. And I think it's important to remember that because if it becomes
institutionalized thought that this is only something that occurs in, you know,
countries like Russia, those other countries, the United States could end up
falling prey to it when it happens all the time in the U.S. Focus on a good example here.
Focus on street price.
That's not a good metric because that doesn't necessarily relate to enforcement.
The U.S. does it too, and they base policy decisions on it.
When you're looking at reports, always remember why that report was generated and what the
goal was, because a lot of times it's meaning gets lost when you pay too much attention
to the numbers on the paper rather than what those numbers are supposed to reflect.
this is important in a lot of different endeavors. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}