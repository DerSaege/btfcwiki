---
title: Let's talk about taking the win with Fox....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=w0DwlABAeVs) |
| Published | 2023/04/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the significance of Fox taking a financial hit due to recent events.
- Breaks down the financial implications for Fox, despite potential tax benefits.
- Points out the substantial loss Fox faced and how it could impact their future.
- Mentions the pressure on Fox to change their business practices.
- Addresses the idea of societal change not being immediate but a gradual process.
- Stresses the importance of recognizing the win in this situation and its implications.
- Emphasizes the need for progressive individuals to acknowledge victories, even small ones.
- Talks about the impact of this financial blow on Fox's credibility and viewer trust.
- Acknowledges that while Fox may not drastically change, there could be alterations in their presentation.
- Encourages taking victories, regardless of scale, and the effect on maintaining hope.

### Quotes

- "Take the win on this. Take the win on the little things."
- "This was a win. This was a huge cost to Fox."
- "They have to change. They have to change their business model."
- "Moving towards a much better world where everybody's getting a fair shake."
- "It's a marathon, there will be more when it comes to Fox."

### Oneliner

Beau explains the financial hit Fox took and the potential for change, urging recognition of victories, no matter how small, for progressive hope.

### Audience

Progressive individuals

### On-the-ground actions from transcript

- Monitor Fox News coverage and hold them accountable for accuracy (implied).
- Support trustworthy news sources and responsible journalism (implied).
- Stay informed and engaged in media literacy efforts in your community (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of Fox News's financial implications and the potential for change in their business practices, offering insight into progressive perspectives and the significance of acknowledging victories.

### Tags

#FoxNews #Progressive #FinancialImplications #MediaLiteracy #Victories


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we're gonna talk about taking the win.
We're gonna talk about taking the win
when it comes to Fox.
This is about Fox in general,
but this can be applied to a whole lot of things.
There are a whole lot of people right now
that are working overtime trying to figure out
how this was actually Fox winning.
They're trying to find the ray of sunshine for Fox and all of this.
Some of it I get.
Like I do.
I think there should have been an on-air apology.
I think that would have been important, but it didn't happen.
Some people talk about how the settlement wasn't high enough.
I get it.
I understand what you're saying.
And then the new one, Fox is going to get a tax break over this.
I mean, yeah, they took a $787 million loss.
They're going to get a tax break.
All they have to do is show that the settlement occurred
during the normal course of their business.
And they'll be able to write it off.
That's true.
But I would point out that if you're calculating
the tax benefits of your loss, you lost.
You lost.
go through the numbers, I think that might help. In 2022, Fox's net income, about 1.2
billion, 787 million of that disappeared with the stroke of a pen. Disappeared with the
stroke of a pen. It's like two-thirds. Factor in what they'll be able to recoup via the
tax write-off. That'll be about 200 million in change. They still lost half a billion dollars.
That's huge. Put it into a different frame of reference. Fox's 2022 midterm coverage,
when they kept telling everybody there's gonna be a red wave. At least I think they did.
it's hard to keep track. Yeah, they didn't make a penny on it. All that profit, gone.
Gone. Right around the corner is Smartmatic and that suit, which is widely seen as kind
of a duplicate suit, and the expectation is that the payout is going to be even higher.
So you could realistically, between that and the smaller follow-on suits that are
sure to come, you can realistically say that Fox lost a year's worth of revenue.
That's not sustainable. They can't do that. They'll go out of business. I know what
people wanted was something that was just going to end Fox. You're talking
about societal change. It's not a sprint. It's a marathon. This is a win. Even
figuring in the tax break, you're talking about half a billion dollars
erasing months worth of profit and putting them in a situation where they
have to change. They have to change their business model. This isn't sustainable.
What do you think happens if they continue with what they were doing? More
suits. And every suit that comes through, it's easier for the people who are saying
they're lying to win because there's this pattern that's developing. Even if a
judge says you can't mention it. The public knows. People like to point to
Murdoch. He doesn't care about the money. Think about what you're saying. He's a
billionaire. You ever met a billionaire that didn't care about money? They didn't
get to be billionaires because they don't care about money. People look at
ideology and they say well he does this for that. That's why he runs Fox because he truly believes
that. Nah, he runs Fox the way he does because it makes 1.2 billion dollars a year.
He's not running it at a loss if it was ideologically driven. I mean that that might
have some sway but all evidence suggests he's running it as a business. Losing half a billion
dollars, that's not good business. You know, I'm not Warren Buffett but I'm willing to bet that's
not a great idea. It's a win. The American left has a problem with doing this. They want everything
to be perfect. I get it. I get it. That's the goal, right? Moving towards a much
better world where everybody's getting a fair shake. I understand that. But it
doesn't happen overnight. It doesn't happen with a single event. It's a
marathon, there will be more when it comes to Fox. There will be more. Either
they change, they change how they handle stuff like this, or eventually they'll go
out of business, they'll run out of money. You can't keep taking losses like that.
that and people are looking at this as if it's a this is it this is their
entire penalty it's not it's not there's more suits coming and anytime they do
this in the future they'll get hit again and they lost the trust of their
viewers and I know that's another thing people are like well Fox News viewers
don't care. Some don't. Some are hyper-partisan and they believe whatever Fox tells them.
Some are hyper-partisan and they know that Fox has been less than accurate in the past
and they don't care. Some are Republicans who were led astray, misinformed, and they're
They're not happy about it.
You can look at the polling about this from Republicans and see that.
I understand that people wanted more, and that's fine.
That's fine.
I get that, but this was a win.
There's no way that you can reframe a company that has a net income of $1.2 billion a year
there, having to fork out $787 million, there's no way you can turn that into a win for the
company, even with their tax write-off.
That's still half a billion dollars.
Take the win on this.
Take the win on the little things.
If you wonder why there are a lot of people that are of a progressive mindset that kind
of lose hope, it's because they're constantly told that they're always losing.
This was a win.
This was a huge cost to Fox.
I would be incredibly surprised if this doesn't lead to changes in the way they present things.
They're still going to be inflammatory, I'm sure.
They're not going to become a bastion of journalistic ethics.
But I'm willing to bet that it is going to alter the way they present stuff.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}