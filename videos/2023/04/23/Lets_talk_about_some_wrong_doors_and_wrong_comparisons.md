---
title: Let's talk about some wrong doors and wrong comparisons....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=bsb3KIZ9DmY) |
| Published | 2023/04/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Received a message about two instances where people went to the wrong house and ended up getting shot, sparking comparisons.
- Discovered that many people were making comparisons between the two situations.
- Two incidents: one involving a black kid who survived being shot, and another where a white woman was killed.
- Differences in the way the two cases were treated raised questions of bias.
- The black kid received more attention, support, and coverage compared to the white woman.
- Detailed the specific events of each incident and the aftermath.
- Pointed out disparities in media coverage, GoFundMe amounts, and reactions from authorities.
- Raised skepticism about the validity of some comparisons made between the two cases.
- Criticized the search for grievances and false equivalencies in trying to compare these vastly different situations.
- Encouraged reevaluating priorities when focusing on skin color in such tragedies.

### Quotes

- "The eternal question of why seems important."
- "If your first reaction to people accidentally going to the wrong house and getting shot is to look at their skin tone and try to figure out how you, by a similar pigment, were somehow damaged, your priorities are a little mixed up."
- "One of these things listed completely makes sense."
- "You're looking for something to view as oppression, it's not real."
- "It's not real."

### Oneliner

Beau questions bias and false equivalencies in comparing two instances of people being shot after going to the wrong house based on their race.

### Audience

Community members, social justice advocates.

### On-the-ground actions from transcript

- Reexamine priorities when faced with tragic events based on skin tone (implied).

### Whats missing in summary

Deeper insights into the impact of biases and false comparisons on justice and media coverage.

### Tags

#Bias #SocialJustice #RacialEquality #CommunityPolicing #Priorities


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about the eternal question.
Why?
I got a message about the two situations
at the wrong house.
And I initially thought it was just one person
making these comparisons.
So I texted it to a friend who does something kind of similar.
And oftentimes, we'll send stuff back and forth.
It's just kind of like, hey, look at this weird one I got.
It was at that point that I found out
that this wasn't a one-off thing and that a whole bunch of people
were making these comparisons.
So I feel like it's worth going over.
OK.
So if you don't know what I'm talking about,
There were two instances where somebody went to the wrong house and wound up getting shocked,
and some comparisons were made.
I'm asking you because you were the only person I saw that covered both from your side.
You're big on social justice and have often said you don't really see racism against white
people.
Well, here's your moment.
The two wrong house shootings.
The poor black kid that got shot had people come in from out of state to speak for them,
got an invitation to the White House, got tons of coverage, and got millions in their
GoFundMe.
The white woman didn't get any of that and only raised about $100,000.
Tell me there's not a bias.
The eternal question of why seems important, but before we get into that, just to recap,
in one instance, a kid goes and knocks on the door and the homeowner is reported to
have shot through the glass door, the storm door, and hit him above the left eye, just
above the left eye, and he survived. And to answer somebody else's question about
how that happened, the only thing I can do is quote pulp fiction. God himself came
down and stopped that bullet. Generally speaking, a wound to the ocular cavity,
to the head, you don't recover from that. But he has survived. In another instance,
a woman, a white woman, was in a car with her friends in a very rural area. They
pulled into a driveway and the homeowner came out and popped off two rounds. One
of them hit her and killed her. Okay, so let's go over the list of things that
were different, the comparisons that are being made, and ask the question of why.
Why?
Why did people come in from out of state to speak for the kid?
Because the reported shooter was not in custody.
The white woman didn't need that.
Because as soon as it happened, police staged an hour-long standoff to take the person into
custody.
So she didn't need somebody to come in from out of state to speak for her.
He got an invitation to the White House.
It's worth noting I did not fact check this.
I have no idea if he actually got an invitation to the White House.
I would point out that he survived.
She did not.
I don't know what good an invitation to the White House would do for her.
Got tons of coverage.
Why?
Because the shooter wasn't in custody and people came in from out of state to make a
bunch of noise.
Got millions in their GoFundMe and it says she only got $100,000, again I don't know
if that's true.
that would make sense. The GoFundMe for her is for her last expenses. He has a lot of
medical bills, I'm sure. Miracle or not, there was probably some pretty intensive medical
procedures that went along with that and then he has to live with what happened
to him. So yeah, it kind of makes sense that his is going to be higher.
Generally speaking, you know, funerals don't cost millions. So I don't know
that this comparison makes any sense at all. It seems as though somebody's trying
to draw a false equivalency between two different things. Yes, the outcomes are
different because these situations are drastically different. The idea that,
But an invitation to the White House, even made it into this, shows that it's not a
real comparison, it's searching for grievance.
You're looking for something to view as oppression, it's not real.
It's not real.
one of these things that's listed completely makes sense.
If your first reaction to people accidentally going to the wrong house and getting shot
to look at their skin tone and try to figure out how you by a similar pigment
were somehow damaged your priorities are a little mixed up anyway it's just a
With that, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}