---
title: Let's talk about Trump taking credit for failure....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=SvP39EaruuA) |
| Published | 2023/04/30 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former President Trump took credit for overturning Roe v. Wade during a rambling response, setting the stage for it to be used against him in future elections.
- Some Republicans are now realizing they caught the car, but are unsure of how to proceed and are giving non-committal answers.
- Republicans are misinterpreting their own polling data on family planning, leading to very restrictive bans that only seven percent of people support.
- The Republican Party's extreme position on family planning has alienated even former allies, such as Hispanic voters who now oppose a six-week ban.
- Beau suggests that the Democratic Party has a slam-dunk issue on their hands if they push for reproductive rights, as the Republican Party may regret their extreme stance.

### Quotes

- "Former President Trump took credit for overturning Roe v. Wade."
- "Some Republicans are now realizing they caught the car, but are unsure of how to proceed."
- "The Republican Party's extreme position on family planning has alienated even former allies."
- "This is going to be good for several percentage points in national elections."
- "The Republican Party took it too far after catching the car."

### Oneliner

Former President Trump claims credit for overturning Roe v. Wade, leading to potential backlash in future elections, as Republicans struggle with their extreme stance on family planning.

### Audience

Politically active individuals

### On-the-ground actions from transcript

- Contact local representatives to express support for reproductive rights (implied)
- Join organizations advocating for comprehensive family planning policies (implied)
- Organize community events to raise awareness about the importance of reproductive rights (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of how the Republican Party's handling of family planning issues may impact future elections. Viewing the full transcript will give a deeper understanding of the nuances involved in this political issue.

### Tags

#ReproductiveRights #RepublicanParty #Elections #RoevWade #FamilyPlanning


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about
former President Trump saying something
that he probably shouldn't have.
Taking credit for something that he probably shouldn't have,
politically speaking anyway.
And we are going to talk about
how the Republican Party caught the car
and doesn't know how to let go,
even though they realize they have messed up.
And we're gonna talk about how they messed up
way more than they think they did because they are misinterpreting
and misreporting their own polling.
Okay, so what happened? Former President Trump was asked
if he would support Lindsey Graham's 15-week ban if it made it to his desk,
something like that. And in part of the answer, Trump did
the right thing, politically. He didn't give an answer, he
He gave the perfect politician non-answer.
We'll get something done on some level.
We're looking at a lot of options.
Means nothing, right?
That would have been a good answer.
But as is often the case with the former president, he can't stop talking.
He can't keep his answers confined.
He rambles.
While he was rambling, he said, and Roe v. Wade for 50 years, they couldn't get anything
done.
I did it.
He took credit for overturning Roe v. Wade.
Get ready to hear that sound bite over and over and over again come election time.
All of this that's happening, it's Trump's fault and he admits it.
Nobody could get it done but him.
Okay, so you do have some Republicans now noticing that they caught the car.
They're the dog who caught the car, they don't know what to do, and they are
giving very non-committal answers. It's happening all over the place. You even
have some Republicans helping to defeat legislation aimed at curtailing family
planning. And that's because of the polls. According to a Fox News poll,
65 percent of Americans want the medication available. That's a big number. That's way
outside the margin of error and that's not something that's going to change. When you're
talking about it in general, 56 percent of Americans say they want it legal in most or
all cases, 56 percent. 43 percent want it illegal all of the time or illegal except
for certain cases. Okay. And that's how it's being reported. But here's the problem with
that logic. That's not the legislation. That's not where the line is. Republicans all over
the country have pushed six-week bans. Very restrictive bans. Bans that do not
allow for it to be illegal, except in certain cases. Those people, they're now
on the other side. They're opposed to how the Republican Party is handling it.
If you really want to know where the line is, you have to look at just those
people who want it illegal in all cases. Seven. Seven percent. The bans they have
proposed are so restrictive it limited the amount of people who actually
support what they're doing down to seven percent. If you show somebody one of the
bans that went through, somebody who was part of that group who wanted it illegal
except in certain cases. And you show them one of those six week bans, they're
not going to be happy because it doesn't allow for those in certain cases because
in some cases it's not even written in as an exemption and in any of the six
week ones it's too early so it amounts to a total ban. Now it's easy for me to
get on YouTube and say this, do I have any evidence to back it up? Yeah, I do
actually. Okay, so there is a demographic that the Republican Party has been,
they've been working overtime trying to court this demographic because they view
them as conservative, natural allies, and they had done a pretty good job, but
now they're starting to see an abrupt U-turn. Okay, so among Hispanic voters
their term, not mine. Now they oppose a six-week ban by three points. A year ago
they were in favor of a six-week ban by 17 points. It's almost like seeing the
outcomes change people's minds. It's almost like this was really horrible
policy that they put through to appease seven percent and they alienated a whole
bunch of people that they had worked really hard to get. Family planning,
reproductive rights, it is going to be on the ballot and the Republican Party has
taken such an extreme position that even people that were on their side a year
go aren't now. And I would like to remind everybody that former President Trump
took credit for it. You know there's that joke that you know he never left the
Democratic Party. You know for those who don't know he was a member of the
Democratic Party at one time and that really he just ran as a Republican to
destroy the Republican Party. Man, I know it's a joke, but I mean, yeah. So at the
end of this, this issue, it's a slam dunk for the Democratic Party if they
push, if they fight, if they do, if they do what is just even the bare minimum
that is expected of them. This is a slam-dunk issue. This is going to be good
for several percentage points in national elections and probably several
percentage points in most state elections the Republican Party took it
too far after catching the car they bit down even harder and I have a feeling
they're gonna regret that. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}