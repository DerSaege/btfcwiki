---
date: 2023-08-06 06:02:24.944000+00:00
dateCreated: 2023-05-19 17:32:13.298000+00:00
description: null
editor: markdown
published: true
tags: null
title: Let's talk about Brandon, books, budgets, and Biden....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=h14jumbHP-0) |
| Published | 2023/04/30 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden made an appearance at the White House Correspondents' Dinner, possibly considering using memes and jokes as part of his campaign strategy.
- The White House Correspondents' Dinner is essentially a roast where Biden joked about "Dark Brandon" and put on aviators.
- Biden must embrace "Dark Brandon" to reach younger voters effectively by showing up in ways that appeal to them.
- Three things Biden must do to embrace "Dark Brandon": fix his labor record, support the LGBTQ community, and call out Republican nonsense.
- Biden needs to make strong public statements in support of labor and LGBTQ rights to embody "Dark Brandon" effectively.
- Biden should not let the White House press office clean up any controversial remarks he makes to lean into the persona of "Dark Brandon."
- Beau suggests that Biden needs to show a different side of himself, embracing the darker aspects to connect with a younger audience.
- Biden should not take seriously the budget proposed by McCarthy because it comes from unserious people who didn't expect it to be taken seriously.
- Beau recommends that Biden needs to lean into this strategy, call out Republicans, fix his labor record, and support the LGBTQ community to make an impact as "Dark Brandon."

### Quotes

- "He has to be Dark Brandon from time to time."
- "Aviators on, walk away. No cleanup from the press office."
- "He has to lean into it. He's got to call out Republicans, he's got to fix his labor record, and he has to come out hard in favor of LGBTQ people."
- "He has to be Dark Brandon every once in a while."

### Oneliner

Biden must embrace "Dark Brandon" by fixing his labor record, supporting the LGBTQ community, and calling out Republican nonsense, as recommended by Beau.

### Audience

Campaign strategists

### On-the-ground actions from transcript

- Call out Republican nonsense, fix labor record, support LGBTQ community (suggested)
- Lean into the strategy, embrace the persona of "Dark Brandon" (suggested)

### Whats missing in summary

The full transcript provides additional context on how Biden can connect with younger voters effectively through embracing a darker persona named "Dark Brandon."

### Tags

#Biden #CampaignStrategy #DarkBrandon #WhiteHouseCorrespondentsDinner #YoungVoters


## Transcript
Well, howdy there, internet people. It's Beau again.

So today, we are going to talk about Biden, books, and budgets, and Brandon. Because Brandon made an appearance at the White House Correspondents' Dinner. And it's another sign that suggests the White House may be considering leaning in to a meme, to a joke, and using it as part of their campaign strategy.

If you're not familiar with the White House Correspondents Dinner, basically it's a roast.

And Biden's up there, and he's just, he's like, "You know, I can take your jokes, but I don't know if Dark Brandon can." and he puts on his aviators.

I get it. It's the perfect spot to test it out. Anything done at the White House Correspondents Dinner can easily be written off and forgotten about as a joke. And if it's a singular reference to a pop culture thing, that's fine.

But if the administration plans on using this to reach younger voters, it'll work, but Biden can't just reference Dark Brandon. He has to be him from time to time. Dark Brandon has to show up in person and in ways that young people care about.

There are three things that Biden has to do if he really wants to embrace Dark Brandon. The first is he has to fix his labor record, and I know long-time Democrats, those people who really pay attention to people's historical positions, you're like, Biden is super pro-union.

Yeah, Dark Brandon, it's like the mob. You're only as good as your last envelope, and Biden's last envelope was breaking a strike, he has to come out hard in favor of labor, big, and he has to do something, has to be public.

He has to come out hard in favor of the LGBTQ community, and he has to do it in a dark Brandon way.

You know, Republican states all over this country they're banning books, taking books out of libraries. We have a federal system states are free to do whatever they want but they're doing this at the same time as passing legislation that might lead to their kids being inspected, and while federalism exists, I would point out that there's also a federal Department of Education, and I get the reports, I know the statistics, and let's be honest, in most of these states, somebody needs to read to these kids.

Aviators on, walk away. No clarification as to what he meant. Nothing. Don't let the White House press office clean it up.

There would probably be some concern about that, because Biden is not the greatest speaker. But you know when he is good? When he's telling a joke. It plays to his strengths to do it this way. So you have Labor, the LGBTQ community, and then he has to call out Republican nonsense as Dark Brandon. Biden's nice. Like by all accounts, people who have dealt with him said that he's a really nice guy. He's very willing to compromise and work together and stuff like that.

The younger crop, they're not really that interested in that, and he has to show it, and he has to show it at the appropriate times when he can really make a point.

"Mr. President, do you plan on taking McCarthy's budget seriously?"
"Is that a serious question? Why would I even do that? Why should I take it seriously when the people who voted for it ran back to their constituents and told them not to take it seriously? It doesn't even makes sense. Why should I treat it like a serious bill when it's coming from unserious people? Nah, Kev, that's not how this works."

Aviators on, walk away. No cleanup from the press office. If he's going to embrace this he has to lean into it. He's got to call out Republicans, he's got to fix his labor record, and he has to come out hard in favor of LGBTQ people. It's that simple.

Those three things have to be done for Dark Brandon to have an impact. And I there's a whole bunch of people watching this who know the origin of Dark Brandon and they're like no we want him to go way further than that. No real life Dark Brandon would go further than that. This isn't real life, it's politics. It's a way to connect.

I think it would work. I think it would work if they really leaned into it and they allowed Biden to be Brandon every once in a while. But if they don't do that it's just a meme that he referenced which is cool. I mean it's a nice move but it'll be forgotten about. He has to be dark Brandon every once in a while and the putting on the aviators and ending it that's that's the signature move.

Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs ~~on Shelf~~ in the video
Avatar Glasses