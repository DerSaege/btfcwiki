---
title: Let's talk about Bragg vs Jordan....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=FS8ItWaoPu0) |
| Published | 2023/04/12 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explaining the back and forth between the local and federal government regarding Trump's New York case.
- Mentioning key figures involved in the situation: Bragg in the DA's office and Jordan and Republicans in Congress.
- Describing Jordan's interest in investigating the charging decision, which Bragg opposes.
- Summarizing the main arguments: Congress's power to intervene based on federal money used by the local prosecutor.
- Pointing out the potential consequences of allowing federal intervention in local matters.
- Speculating on the outcome and the potential impact on various local offices that have received federal funding.
- Noting the possibility of oversight expanding to other areas if Jordan's actions set a precedent.
- Mentioning the protection Congress has under the Speech and Debate Clause.
- Predicting that Republicans may not achieve their desired outcome in this situation.

### Quotes

- "Let's say Jordan is successful in this, and that gets established. That's how things are going to go now."
- "If Jordan gets his way, there's going to be a lot of really upset Republican governors."
- "There are some people who have asked, you know, well, why can't the DA go after Congress?"
- "No matter how they [Republicans] feel about Bragg, Bragg didn't indict Trump. The grand jury did."
- "Expect this to be going on for the next few weeks and turn into a giant thing."

### Oneliner

Exploring the implications of federal intervention in local matters through the Trump New York case, a potential domino effect on oversight emerges.

### Audience

Legal observers, political analysts

### On-the-ground actions from transcript

- Monitor the developments in the Trump New York case and stay informed about the implications of federal intervention (implied).
- Advocate for preserving the balance between local and federal government powers in legal matters (implied).

### Whats missing in summary

The detailed legal intricacies and potential consequences of allowing federal intervention in local prosecution cases like the Trump New York matter.

### Tags

#Trump #Federalism #Oversight #LegalIssues #LocalGovernment #PoliticalImplications


## Transcript
Well, howdy there, internet people, it's Bill again.
So today we are going to talk about Trump's New York case,
but we're not really gonna talk much about the case.
We're going to talk about the back and forth
between the local and federal government that is going on,
because you have Bragg in the DA's office
and you have Jordan and Republicans up in Congress.
Jordan has indicated that he would like to investigate the charging decision.
Bragg is obviously not not in favor of this.
So legal channels all over YouTube are going to go through the filing line by line by line I'm sure.
Short version, there's a subpoena that Congress has put out.
The local DA doesn't really want it enforced, BRAG doesn't want it enforced, and filed
suit to stop that and get some other relief.
What are the main arguments here?
Congress, from Jordan's standpoint, at some point during some part of the investigation,
the local prosecutor used some federal money, therefore, Congress has the power to interject
themselves into the charging decision.
That's generally what it is.
I know somebody's going to say, well, it's about oversight.
Now, it certainly appears intended to affect the outcome.
I would point out that even in response to the suit, Jordan said, first, they indict
a president for no crime.
It's designed to influence the outcome.
Now, from Bragg's position, this is, I mean, this is not how our country set up, it violates
basic premises of federalism.
The federal government can't interject into local matters like this, and there's two reasons
for that.
is, again, the way the country is set up.
The second is they literally can't.
They can't, meaning they don't have the ability to do this.
You can't micromanage the United States to this level.
It's huge.
And then I'm sure that Bragg is aware that the hearings are designed to influence the
outcome.
So BRAG is trying to stop it.
Now what's going to happen?
I don't know.
I don't know.
Those are your two arguments.
Now my understanding is that yes, the DA's office did use federal money.
Does that give Congress enough juice to actually interject to this level?
I don't think so, but I don't know what a judge is going to decide.
So that's what's going on, and it's probably going to get very, very messy.
There's another type of law that I think is important to bring up at this point, and it's
the law of unintended consequences.
Let's say Jordan is successful in this, and that gets established.
That's how things are going to go now.
If the federal government gave money and then some portion of that money got used in some
way that somebody in the federal government doesn't like, they can haul people into D.C.
and stage a giant show, I'm sorry, have hearings about it.
If somebody offered that, if I was sitting at a table and somebody was like, House Republicans
who are notorious for being incapable of putting on a good hearing,
are
going to investigate BRAG
in the DA's office.
But, it opens the door
for oversight
of any local office
that has at any point in time taken federal money.
Would you take the deal?
Absolutely.
Set this up.
Let's do this.
Because I have a feeling that if this is pushed through
and that this is how things are going to run from now on,
I want you to think about all the things around you
that have federal funding that have issues right now.
That school up the road from you,
do you think any federal funding was used in the planning,
running, or construction of that school at any point in time?
I bet it was.
So I would imagine that school board members could be brought in to talk about books they
banned or students that they are treating less than equally.
If Jordan gets his way, there's going to be a lot of really upset Republican governors.
I don't know how this is going to play out, but this is one of those things that will
start dominoes falling.
So be aware, there's a subpoena.
Bragg's trying to fight the subpoena.
And what happens and how this moves forward is going to determine a lot.
There are some people who have asked, you know, well, why can't the DA go after Congress?
Those who are trying to influence the outcome.
Generally speaking, Congress is protected under something called the Speech and Debate
Clause.
That protects speech and debate.
It really isn't established how much it protects actions.
There might be other things that come of this as well on that ground.
But expect this to be going on for the next few weeks and turn into a giant thing.
I would suggest that by the end of it, no matter how it turns out, I don't think Republicans
are going to get what they want.
No matter how they feel about Bragg, Bragg didn't indict Trump.
The grand jury did, and that's something that has to be remembered.
This uh, the political posturing may just draw a whole bunch of attention to something
that would be detrimental to the Republican Party.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}