---
title: Let's talk about Missouri libraries....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Ko18Du0dJaw) |
| Published | 2023/04/12 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Missouri Republicans have decided to defund all public libraries in the state, reflecting an authoritarian desire to control and suppress education and knowledge.
- This decision is about ensuring the people of Missouri follow orders, stay ignorant, and are easily manipulated by those in power.
- The move to defund public libraries goes against the Missouri state constitution, which clearly states support for the establishment and development of free public libraries.
- The Missouri House will need a constitutional amendment to proceed with defunding the libraries, as the constitution mandates support for public libraries.
- Some members of the Missouri Senate oppose this move and have indicated that the funding will likely be reinstated, showing a divide between the House and Senate.
- The attempt to defund libraries reveals a deeper agenda of keeping constituents ignorant and easily controlled, even if it means going against the state constitution.

### Quotes

- "Education is power. Knowledge is power. Denying people education and denying people knowledge is denying them power."
- "That's what this is about. It's about controlling people."
- "In order to do what they want to do here, they have to get a constitutional amendment to the state constitution."
- "Not just do they want you in your place, not just do they want you and your kids as ignorant as possible so they can be manipulated, they will do it in direct contradiction to the Constitution."
- "Y'all have a good day."

### Oneliner

Missouri Republicans aim to defund public libraries to control education and knowledge, going against the state constitution, revealing a deeper desire for manipulation and ignorance.

### Audience

Missouri residents

### On-the-ground actions from transcript

- Contact your Missouri state legislators to express support for public libraries and urge them to uphold the state constitution (suggested)
- Join local library advocacy groups to help protect public libraries in Missouri (implied)

### Whats missing in summary

The full transcript provides more context on the political dynamics in Missouri and the potential implications of defunding public libraries, offering a comprehensive understanding of the situation.

### Tags

#Missouri #PublicLibraries #Education #Knowledge #StateConstitution


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're going to talk about Missouri and libraries
and different ways a certain situation might be described
or people might try to counter it or explain
what's happening.
So if you don't know, the Republicans
in the House in Missouri have decided
to defund all of the state's public libraries.
I mean, that tracks, doesn't it?
I mean, that goes along with pretty much everything
that we've been talking about.
The authoritarian nature of it.
The desire by Republicans to rule rather than represent.
The desire to tell people to sit down, shut up,
and stay in their place, because that's what this is about.
Education is power.
Knowledge is power.
Denying people education and denying people knowledge
is denying them power.
That's what it's about.
It's about making sure the people of Missouri
do what they're told and obey.
These are all arguments that you could expect to hear.
And there are arguments you would definitely expect me to make.
Basically, they're the type I normally do.
Because they're true.
That's what this is about.
It's about controlling people.
It's about making sure that the next generation of voters
is too ignorant to know what to do.
Their job at this point, apparently,
When you are looking at the political elites in Missouri,
their goal is to keep their constituents
as ignorant as possible.
So they're easy to control, easy to manipulate,
easy to trick, and easy to command.
But rather than making those arguments,
I just want to read something.
I think it might be more important.
It is hereby declared to be the policy of the state
promote the establishment and development of free public libraries, and to accept the
obligation of their support by the state and its subdivisions and municipalities in such
a manner as may be provided by law.
When any such subdivision or municipality supports a free public library, the General
Assembly shall grant aid to such public library."
Shall, means you're going to do it.
I mean that's cute and everything, defunding the libraries, but the Missouri state constitution,
that's what I just read, it says no, you're going to cut the check.
In order to do what they want to do here, they have to get a constitutional amendment
to the state constitution.
That's what has to occur.
They shall, not may, shall, words have definitions, that means you're going to do it.
They have to provide the funding.
Now it is worth noting that there are people in the Missouri Senate that are basically
like, there's no way this money is not going back into the budget.
They've made it very clear that they are not of the same mindset as those in the House.
In fact, the fact that this was attempted needs to tell the people of Missouri something.
Not just do they want you in your place, not just do they want you and your kids as ignorant
as possible so they can be manipulated, they will do it in direct contradiction to the
Constitution.
Anyway, it's just a thought.
Y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}