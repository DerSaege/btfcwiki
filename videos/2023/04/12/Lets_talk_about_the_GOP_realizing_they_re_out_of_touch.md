---
title: Let's talk about the GOP realizing they're out of touch....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=DZIjZZ136oQ) |
| Published | 2023/04/12 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Kellyanne Conway acknowledges the Republican Party's struggle to attract younger voters and singles waiting to get married to become conservative.
- Younger voters are turning out in increasing numbers and leaning towards the Democratic Party due to GOP's stance on family planning, climate change, and guns.
- Republican strategists are out of touch as they believe they have won policy arguments on the economy and education.
- The GOP angered many by denying relief for educational loans, making the American dream unattainable for young Americans.
- Conway's statement about waiting for people to get older or married shows a misconception that people become more conservative with age.
- Values and opinions are formed early in life and evolve with society; people don't suddenly become conservative as they age.
- The Republican Party needs to alter its policy platform as younger voters will not become more conservative over time.
- Conservatives are slow to adapt to change and are being left behind by younger generations moving towards progressive ideals.
- Republicans need to become more progressive to appeal to younger voters who will continue trending towards the Democratic Party.
- The belief that people become more conservative with age goes against the principles of representative democracy and the republic.

### Quotes

- "They didn't become more conservative. Society moved forward and therefore they're more conservative in comparison to the more progressive standards of the day."
- "The Republican Party has to become more progressive. There's no way around that."
- "It is against the ideas of representative democracy. It is against the ideas of the republic."

### Oneliner

Kellyanne Conway acknowledges the Republican Party's struggle to attract younger voters as societal progress moves forward, showing the need for the GOP to become more progressive.

### Audience

Young voters

### On-the-ground actions from transcript

- Advocate for progressive policies within the Republican Party (exemplified)
- Engage younger voters in political discourse and encourage participation (exemplified)
- Support candidates who represent progressive values (exemplified)

### Whats missing in summary

The full transcript provides additional context on the evolving political landscape and the necessity for political parties to adapt to changing societal values and beliefs. Watching the full transcript can offer a deeper understanding of the Republican Party's challenges with attracting younger voters.

### Tags

#RepublicanParty #YoungVoters #ProgressivePolicies #SocietalChange #Adaptation


## Transcript
Well, howdy there, Internet people, it's Bill again.
So today we're going to talk about
the Republican Party's Principal Skinner moment
and how it's occurring in real time,
and it is kind of funny.
Of all people, the first real acknowledgement
came from Kellyanne Conway.
She was having a conversation and she said,
look, the Republican Party cannot wait for the young to turn old and the single to get married
to find new voters, that's for sure. And what she's talking about is the trend of younger voters
turning out in just increasing numbers and them trending hard towards the Democratic Party or even
further left.
And she makes some real observations talking about how the GOP's stance on family planning
and climate change and guns, they're just not lining up with younger voters and that
they're going to lose them over these issues.
I mean, yeah, that's pretty obvious, but the moment where you realize how out of touch
Republican strategists have become is when she talks about what she thinks they're doing
well at.
Because it's that moment where you see her standing there, and she becomes Principal
Skinner, and she's like, am I so out of touch?
it's the children. She says, I think we've already won the policy arguments on the
economy and on education. Yeah, I mean that totally tracks as the Republican
Party angered millions upon millions of people by denying relief for educational
loans, where you have people constantly talking about how younger Americans cannot get the
American dream because it's priced out of their reach, while the donors to the Republican
Party see it ever increasing profits.
They don't even understand how out of touch they are.
More importantly, the statement about waiting for them to get older or the single to get
married shows that they have bought into that line, that people get older and become more
conservative.
That's not true.
That's not what happens.
When you are born, you immediately, you start getting values.
As soon as you can process information, you start forming opinions.
And they are the, generally speaking, they are the opinions that are around you all the
time. And society moves forward. It progresses over time. As you get older,
you still have those values from when you were born, from your early childhood.
And a lot of people don't evolve with society. They don't change with society.
They didn't become more conservative. Society moved forward and therefore
they're more conservative in comparison to the more progressive standards of the day.
Somebody who is incredibly progressive at 17 right now in 30 years, if they keep the
same values they have today, they will be conservative.
They will not, people will not suddenly embrace bans on family planning and embrace authoritarian
ideas about education or say that climate change isn't an issue if they
believe it is when they're younger. They're not going to to alter their
position to fall in line with the Republican Party. That's not how it
works. Again, they're demonstrating that they want to rule, not represent. The
The Republican party should be a reflection of the people that vote for it.
They shouldn't be trying to guide them to a more conservative position.
This is one of the first public acknowledgments that I've seen from the Republican party
saying, yeah, we have messed up and we are out of touch and, I mean, our real voter base
is aging out of voting.
We have to do something.
And the only option is to alter the policy platform.
People aren't actually going to become more conservative.
That's not what's going to occur.
The Republican Party has to catch up.
We've talked about it throughout the years on this channel.
Conservatives get drug kicking and screaming into the future.
They dug their heels in for a number of years and did not move.
And now they're paying the price for that.
It will continue, and younger people will trend more and more towards the Democratic
Party until the Republican Party changes its platform.
The Republican Party has to become more progressive.
There's no way around that.
It's not that people get a ring on their finger and suddenly become more conservative.
That doesn't happen.
That is a belief born out of the idea that Republicans are rulers, not representatives.
That you can force people to go back on their beliefs.
It is against the ideas of representative democracy.
It is against the ideas of the republic.
This being this entrenched shows you how authoritarian the Republican Party has become.
And that authoritarian nature is going to cost them with younger voters.
Even when those younger voters turn 30 or 40, they're still not going to like the Republican
Party unless the Republican Party changes its policy platforms.
And just a free piece of political advice, the Republican Party has absolutely not connected
with young people on the economy or on education.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}