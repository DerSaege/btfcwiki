---
title: Let's talk about Schumer and political games....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Y3peIyQBTh8) |
| Published | 2023/04/12 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Democrats in the Senate are not introducing a resolution to condemn defunding the police.
- The situation developed after Trump's actions led to a vote on a resolution condemning defunding the Department of Justice.
- Republicans are put in an uncomfortable position with this resolution, regardless of how they vote.
- The resolution doesn't actually do anything; it's a political game to make Republicans cast a vote they'll have to answer for later.
- Senators are not determining if they support defunding the Department of Justice; they're voting on potential attack ads against them.
- The Democratic Party can use these votes to drive a wedge between senators and their voters.
- Most people may not understand that defunding the Department of Justice also impacts local police due to grants and connections.
- The vote is purely political maneuvering and means nothing in terms of actual action.
- Defunding the Department of Justice in many ways means defunding the police, but this connection might not be clear to everyone.
- Republicans will have to navigate this situation strategically to avoid backlash.

### Quotes

- "Democrats in the Senate are not introducing a resolution to condemn defunding the police."
- "The resolution doesn't actually do anything; it's just a game to put Republicans in an uncomfortable position."
- "Most people may not understand that defunding the Department of Justice also impacts local police."

### Oneliner

Democrats in the Senate play a political game with a resolution condemning defunding the Department of Justice, putting Republicans in a tough spot and potentially driving a wedge between them and their voters.

### Audience

Political observers, voters

### On-the-ground actions from transcript

- Contact your representatives to express your understanding of the political maneuvers happening in the Senate (suggested).
- Stay informed about how political games like these impact policies and decisions affecting your community (implied).

### Whats missing in summary

Further details on the potential consequences of this political maneuvering and how it could affect future legislative actions.

### Tags

#Senate #Democrats #Republicans #DepartmentOfJustice #PoliticalGames


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about the United States
Senate and Schumer and a resolution
and a little bit of confusion and political games.
Because one is about to be played in the Senate,
and it has led to some questions.
And those questions, generally speaking,
have had the tone of, you know, what is happening?
Why on earth are Democrats in the Senate
introducing a resolution to condemn defunding the police?
That's not what's happening.
I know that's what it looks like,
but that's not actually what's occurring.
OK, so this is how this situation developed.
After Trump was indicted, he ranted and raved, and had his little tirades, and threw his
little temper tantrums on social media.
During this process, he instructed his supporters in Congress to defund the Department of Justice,
and I think the FBI.
Schumer is basically like, okay, so my understanding is there's going to be a vote on a resolution
that is basically condemning the call to defund the Department of Justice.
Republicans will vote on this and it doesn't matter how they vote, they lose.
If they vote to condemn defunding the Department of Justice, well then they have upset the
MAGA loyalist, the Trump loyalist, within their own voting base. If they
don't, they've upset those who see themselves as law and order. You have two
competing factions within the Republican Party right now. There is no way for a
Republican to cast a vote on this resolution and not upset one of them.
That's what's happening. This vote, the resolution, it doesn't actually do
anything. It's just a game to put Republicans in an uncomfortable position
and make them cast a vote that they'll have to answer for later. Republicans are
not determining whether or not they actually support defunding the
Department of Justice with this. What they're really voting on is what kind of
attack ads, they would like to have ran against them during their next election.
That's really what they're voting for.
Because regardless of how they vote, it can be capitalized on.
The Democratic Party can run ads to drive a wedge between that senator and their voters.
If they vote against condemning Trump, then they will run ads and say, you know, with
all the crime in your district, you know, blah, blah, blah.
If they vote to condemn Trump, they'll blast him as a rhino or whatever.
This is all political maneuvering.
It absolutely means nothing.
And I don't think that most people actually understand that defunding the Department of
Justice is defunding the local police, too, because of the amount of grants and everything.
Those who are really concerned about criminal justice reform, y'all understand that.
And that's why this kind of came as a shock.
But the average person probably doesn't understand that defunding DOJ is, in a lot
of ways defunding the police. That connection probably isn't even being made by most people.
But rest assured, the vote actually does nothing. It's just a resolution. So it's all a game,
and I'm interested to see how Republicans attempt to squirm out of it. Anyway, it's
It's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}