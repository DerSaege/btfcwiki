---
title: Let's talk about T-55s....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Vq3lLodyAhs) |
| Published | 2023/04/16 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing rumors about Russia sending T-55 tanks to Ukraine.
- Strong indications suggest the rumors are true based on images and markings on the tanks.
- The T-55 is an old tank that became obsolete 50 years ago, post-World War II.
- In a bizarre scenario in 2003, Challenger 2 tanks wiped out T-55 tanks.
- The T-55 has significant technological disadvantages compared to modern tanks.
- Russia may be using the T-55 more as self-propelled howitzers or mobile gun emplacements rather than traditional tanks.
- Russia appears to be emptying out old stock by sending these outdated tanks.
- Despite its age, the T-55 is still dangerous due to its cannon.
- This move doesn't indicate Russia is done militarily but rather suggests they are facing challenges.

### Quotes

- "Despite all limitations, it is still dangerous."
- "They're just doing something very, very strange."

### Oneliner

Beau addresses Russia sending outdated T-55 tanks to Ukraine, facing challenges with their military capabilities, suggesting a strange move that isn't indicative of being done.

### Audience

Military analysts, policymakers, concerned citizens

### On-the-ground actions from transcript

- Monitor and raise awareness about Russia's military actions (implied)
- Advocate for diplomatic solutions to conflicts (implied)

### Whats missing in summary

In-depth analysis of the implications of using outdated tanks and the potential risks involved.

### Tags

#Russia #Ukraine #Tanks #Military #Conflict #Challenges


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Ukraine and rumors
and riding a tank at the general's flank and all
of that stuff, because there is a rumor that is circulating.
And for once, there are strong indications
that it's actually true.
OK, so what's the question?
Is Russia actually sending T-55s to Ukraine?
Looks like it.
Looks like it.
There are strong indications that that is actually occurring.
There are images.
The reason, the only reason I don't say for sure yes
is because it's just absurd to me.
For normal people, the T-55 is a tank.
It is a very old tank.
It was obsolete about 50 years ago.
It was obsolete at that point.
This tank is part of, it is the first generation
of tank post World War II.
So you have the tanks from World War II.
This is the next generation of tank.
It's wild to me, but there have been images that have surfaced that have the markings on them.
There's a lot that indicates, yes, they are actually being sent.
How does this thing compare to what Ukraine is likely to have? It doesn't.
It doesn't. In fact, I want to say in 2003, there was actually a bizarre situation in which Challenger 2s went up
against T55s.  The outcome is exactly what you would imagine. They were wiped out. The T55s were wiped out.
out. So there's that. As far as normal comparisons, things that you would want
to like measure against, they are so far apart in their development that you
really don't have comparable technology. I think to hammer the point home, a T55,
if you want it to be accurate, it has to stop moving. It can't drive and shoot at
the same time. That is a massive disadvantage over even obsolete tanks
that are normally used today. Okay, why are they doing it? The simple answer is
they don't have the tanks they need. Interestingly enough, it also looks like
like they are running out of tank crews.
At this point, there aren't a whole lot of people around
that are proficient on a T-55, because they all retired.
Anybody that knew how to drive one was retired 30 years ago.
Now, all of that being said, it doesn't
look like they're actually being used as tanks.
tanks.
It appears that Russia plans on using them more like self-propelled howitzers or mobile
gun emplacements.
Which I mean is fine, I guess, but it's kind of like that scene from Pentagon Wars.
Do you want to paint this is not a tank on the side of it?
On the battlefield it will be treated like a tank.
So there's that.
I expect them to have a very high attrition rate.
I don't expect a whole lot of them to make it.
It seems clear at this point that Russia is emptying out a bunch of old stock.
Now it is worth remembering that they actually still do have some semi-modern tanks scattered
around the country for defense.
This doesn't mean that they're out of everything else.
They do have other stuff.
They're just breaking out stuff that is more fitting for it to be in a museum.
To put this in American terms, this is part of the generation of tank that when you drive
by like an American Legion or a VFW or something like that and they have that demilitarized
tank sitting out front, it's from that generation.
In the US, something comparable would have already been taken out of service and turned
into a monument.
That's how old these things are.
But at the end of the day, it still has cannon.
Despite all limitations, it is still dangerous.
It just can't realistically be used as a tank.
So it's definitely a sign that Russia is having to scrape the bottom of the barrel,
It isn't... I wouldn't read this as a sign that Russia is done. They're just doing something
very, very strange.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}