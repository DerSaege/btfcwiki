---
title: Let's talk about schools and a question from a conservative.....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=gEclRxoalwk) |
| Published | 2023/04/16 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing parenting and a challenge from a conservative regarding government involvement in certain situations.
- Expresses a private disapproval of "the whole trans and drag thing," but believes government involvement is unnecessary in 99% of cases.
- Suggests parents should be notified if their child at school is using different pronouns or dressing differently.
- Disagrees that the first question parents should have is why the school didn't inform them about their child being trans.
- Emphasizes that the first question should be why the child didn't confide in their parents.
- Explains the Southern perspective on involving pastors in family matters as a form of family therapy.
- Argues that policies mandating school notification regarding a child's gender identity could lead to tragic outcomes.
- Asserts that kids who can hide their identity from parents can also keep it from the school, rendering the policy ineffective.
- Challenges the notion that involving schools will solve the issue of children concealing their gender identity.
- Believes that while some parents may discover their children's identities through school involvement, the worst-case scenarios are concerning.

### Quotes

- "I think the parents should be notified."
- "It's going to go very bad at some point in time."
- "But let's be real, when it comes to the name, I bet you do your homework, help the kids with their homework, right?"
- "And rest assured, it will happen."
- "Definitely not the first question I'd be asking."

### Oneliner

Beau challenges the necessity of government involvement in parenting, stressing the importance of communication within families over external notifications.

### Audience

Parents, educators, policymakers

### On-the-ground actions from transcript

- Communicate openly with your children about gender identity and create a supportive environment (implied)
- Prioritize listening and understanding your child's perspective (implied)
- Seek guidance from family therapists or trusted individuals if needed (implied)

### Whats missing in summary

The full transcript provides a detailed exploration of the potential consequences and ineffectiveness of policies mandating school notification of a child's gender identity.

### Tags

#Parenting #Communication #GenderIdentity #ConservativePerspective #SchoolNotification


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we are going to talk about parenting.
And we're going to go over another question
from a conservative who has not a question,
but a challenge for me.
Wants me to answer something honestly.
And we're going to talk about best case and worst case
scenarios and just kind of run through the one place
where he thinks it's okay for the government
to get involved in something.
Okay, since you're taking questions related to parenting
and from conservatives, I've got one for you.
It's not really a question, but more of a statement,
and I challenge you to honestly tell me I'm wrong.
As an introduction, I'm 46 and an OTR trucker,
long haul trucker.
I'm exactly what you described
as a Southern small government conservative.
I privately think the whole trans and drag thing is weird,
and I don't approve of it.
But it's not my business,
so I'd never publicly, in parentheses,
trash somebody for it,
and I don't think there's any way
the government should get involved in 99% of it.
The only place it makes sense to me
is making sure the parents know.
If some kid in high school
using a different name or pronouns or dressing the other way, in parentheses. I
have no idea how this happens without the parents knowing, but like you say,
let's just say that it's true. I think the parents should be notified. From
there, it should be between the parents, the pastor, or shrink, or doc, if the
parents are supportive, or whoever else. But the government doesn't really seem
to have a role. Tell me the truth. If one of your kids was trans at school for six
months and then you found out, tell me the first question across your mind
wouldn't be, why didn't the school tell me? I think we both know it would be.
Now, it absolutely would not be. That would not be the first question across my mind.
It wouldn't even be in the top ten. My first question would be, why didn't my
kid tell me?" That's what I would want to know. But it's in your message. You know
why, right? If the parents are supportive. You said it. It's in there. It looks to me,
you say, pastor, and it is worth noting for people from outside the South, pastor
doesn't necessarily mean go pray the gay away. It could mean go to somebody who
knows the family and talk about it. In the South, sometimes pastor substitutes
for like family therapy. And since the other options are shrink or dock, I'm
imagining that's where his head is at. Could be wrong, but that's what I'm
guessing. But yeah, you're looking at it from the perspective of best-case
scenarios and it looks like you laid out what you would do, your chain of events.
Pastor, shrink, doc, right? Best-case scenario. But what's the worst-case
scenario. 46, Southern, small government conservative. Even though you're driving,
probably only listening, I'm willing to bet you know what that sound was. That's
the other side to it. And let's be clear, if the kid isn't going to tell the
parents, it's either they're not sure if they'll be supportive or they know they
won't be. Mark my words, these policies, they will end in tragedy. It's going to
go very bad at some point in time.
The idea that it's somehow going to inform parents doesn't make any sense either.
Not really.
Because if the kids know that the school is going to tell the parents, well, if they can
keep it from their parents, they can keep it from the school.
This isn't a, it's not even a solution to the problem that they think exists.
Because the kids just won't let the school know.
If they can hide it from their parents, they can hide it from the school.
The reality here is you're right.
There are not a whole lot of kids that are sneaky enough to be able to hide it from parents
who are involved.
And that's what you're counting on when you say, I have no idea how they'd do this.
How they'd get away with wearing other clothes.
That's just mind-boggling.
My guess is you're pretty involved with your kids.
But let's be real, when it comes to the name, I bet you do your homework, help the kids
with their homework, right?
What name do they put on it?
I don't think that this is a problem that would really be solved by the schools getting
involved.
I think best case scenario, yeah, you have some kids that find out their parents are
supportive when they weren't sure if they would be.
But the worst case scenario, it's going to go bad.
And rest assured, it will happen.
It's going to happen.
I personally don't think it's worth it.
But definitely not the first question I'd be asking.
be asking what I did wrong to put my kid in a situation where they didn't think they could
talk to me.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}