---
title: Let's talk about Republicans biting the hand that feeds them....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=LlX20qBC59E) |
| Published | 2023/04/16 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses the topic that everyone wanted him to talk about, a boycott involving Bud Light, from both conservatives and liberals.
- He stayed quiet because revealing his open secret was necessary to talk about it.
- Conservatives critiqued him for not discussing their successful boycott, while liberals wondered why he didn't make fun of them for not knowing the parent company behind the brand they boycotted.
- The people at the center of the issue reached out to Beau, questioning his silence on the matter.
- Beau explains that he refrains from interrupting his opposition when they're making a mistake, which is why he didn't initially address the situation.
- Bud Light faced backlash for a semi-inclusive marketing campaign that some perceived as going "woke," leading conservatives to initiate a boycott.
- Beau notes that the boycott by conservatives against Bud Light was somewhat successful, causing damage to the company but not as much as claimed.
- He mentions a website called OpenSecrets that reveals the political contributions made by companies like Anheuser-Busch, Bud Light's parent company.
- Beau elaborates on the political donations made by Anheuser-Busch in 2022, showing a significant portion going to Republican committees and candidates.
- The transcript ends with Beau talking about Trump's child calling for an end to the boycott of the conservative-leaning company.

### Quotes

- "I don't like to interrupt my opposition when they're making a mistake."
- "Y'all boycotted one. Y'all boycotted a big donor is what this boiled down to."
- "Maybe they decide to make amends and give an even bigger donation to the people who trashed them."

### Oneliner

Beau addresses the Bud Light boycott, revealing political donations and potential consequences.

### Audience

Consumers, activists, voters

### On-the-ground actions from transcript

- Contact your representatives to inquire about their campaign contributions and affiliations (suggested)
- Stay informed about the political affiliations of companies and their donations (exemplified)

### Whats missing in summary

Analysis of the potential impact of political donations on corporate decisions.

### Tags

#Boycott #PoliticalDonations #Conservatives #Liberals #OpenSecrets


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about something
that pretty much everybody asked me
why I wasn't talking about it.
From all over the political spectrum,
everybody wanted me to talk about this, and I did not.
I stayed quiet because there is no way
I could talk about this without revealing my open secret.
So conservatives sent me messages saying, hey,
you make fun of us every time we do a boycott.
You're not talking about the one that we did successfully.
I mean, success is a scale.
And then there were liberals that sent messages asking
why I wasn't making fun of them for not knowing
what the parent company was, and boycotting
one of their brands, and going and buying
other stuff made by that same company.
And then the people that were at the center of this sent
messages, a couple of them were like, hey, you always kind of
stick up for us and you're being real quiet on this one.
Yes.
I don't like to interrupt my opposition when they're making
a mistake.
OK.
So if you have no idea what I'm talking about, Bud Light,
They did a semi-inclusive marketing thing,
so they were accused of going woke.
Conservatives started a boycott.
And because this is actually a product that they do buy,
they were somewhat successful, not as much
as they're making it seem.
But they did get somewhere with this.
And they did cause a little bit of damage to the company,
I guess.
I mean, again, not as much as they're making it seem, but I mean, they definitely got their
attention this time.
They did have some issues with figuring out all of the brands that the parent company
makes.
That was entertaining.
But the reason I didn't bring it up is because there's a website called OpenSecrets.
And if you ever want to know who your representative actually represents, you can go there.
It tells you who your representative got money from.
It tells you what committees gave them money.
And you can go the other way with it.
You could type in, say, the name of a parent company, Anheuser-Busch, and find out who
they gave money to.
I would like to read a list.
What they do and how they track it is basically
they take the company, individuals immediately
linked to the company, their immediate families,
so you get a very clear picture of what the company supports.
And if you really want to know what the company supports,
you need to look at the committees that they give to,
not the individual congresspeople.
Because the individual congresspeople, big businesses,
they give to both.
So you start with the committees to get a clear picture
and the PACs and stuff like that.
So starting at the top, this is where Anheuser-Busch put
their money in 2022.
Number one, National Republican Senatorial Committee,
more than half a million dollars.
Number two, National Republican Congressional Committee,
that's the House, $464,000.
Save Missouri Values, conservative, $250,000.
National Republican National Committee, $200,000.
GO-PAC, which is a PAC that, if I'm not mistaken, is neutral.
It's not affiliated with a party.
However, I want to say out of the donations they made,
I think 100% went to Republicans.
Anheuser-Busch gave them $129,500.
Conservative Louisiana, $100,000.
Show Me Values pack, $100,000.
Hope you get the picture.
If you were to look at their congressional candidates
and who they gave the most money to,
62.2% of the money they gave, almost 2 thirds,
went to Republicans.
Congratulations! Y'all boycotted one. Y'all boycotted a big donor is what this
boiled down to. The only reason I'm talking about it now is because Trump's
kid came out and asked for the boycott to stop of this conservative leaning
company. It is. And I want to say there was a a thing that occurred between
the RNCC and the company. I think the RNCC put out some merchandise or
something like that. They've removed it since, you know, the company gave them
almost half a million dollars. Yeah, so there's that. I mean, I'm not sure if
if Anheuser-Busch will open their wallet quite the way they have in the past for Republicans
after this.
But we'll see.
Maybe they decide to make amends and give an even bigger donation to the people who
trashed them.
I mean, it could happen.
Anyway, it's just a thought.
Y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}