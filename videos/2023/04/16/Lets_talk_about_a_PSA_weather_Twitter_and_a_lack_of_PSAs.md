---
title: Let's talk about a PSA, weather, Twitter, and a lack of PSAs....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Bnmen-Ro_ZE) |
| Published | 2023/04/16 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Elon Musk targets bots and automated tweeting on Twitter, impacting National Weather Service accounts that provide life-saving information about disasters like tornadoes and hurricanes.
- Changes on Twitter are hindering the National Weather Service from disseminating critical information promptly, potentially jeopardizing lives.
- Public transportation updates and service delays communicated via Twitter in major metropolitan areas may also be affected by these changes.
- Private entities relying on Twitter for information dissemination are likely to face challenges as well, although not as critical as life-saving alerts.
- Beau suggests that Elon Musk could offer free access to certain services impacted by the Twitter changes to incentivize saving lives, possibly even receiving a tax break for doing so.
- Various public services, including the National Weather Service and public transportation systems like BART, are facing obstacles in delivering timely information to the public due to Twitter modifications.
- The rollout of these changes is ongoing, and many service providers may not be fully aware of how their systems are being affected.
- Beau advises individuals who rely on Twitter for critical updates to download alternative apps that provide location-based weather alerts to ensure they receive necessary information promptly.

### Quotes

- "If the people don't make it, they won't be scrolling."
- "It might be worth reminding Elon that if the people don't make it, they won't be scrolling."
- "If you are somebody who typically gets this information from Twitter, it's time to download a new app."

### Oneliner

Elon Musk's Twitter changes hinder critical information dissemination, impacting public services and potentially risking lives.

### Audience

Twitter users

### On-the-ground actions from transcript

- Download a weather app that uses your location for alerts (suggested)
- Stay informed about alternative platforms for receiving critical updates (suggested)

### Whats missing in summary

Importance of exploring alternative platforms and staying updated on changes affecting critical information dissemination.

### Tags

#ElonMusk #Twitter #NationalWeatherService #PublicService #InformationDissemination


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to do a little public service
announcement about the potential lack of future public service
announcements.
So Elon has decided that one of his main goals, one
of the things that he has set out to do with Twitter,
is Reddit of bots, automated tweeting.
This is something that he's very opposed to.
It appears that he has been successful,
but with the wrong stuff.
The National Weather Service has Twitter accounts.
And these Twitter accounts use an automated system
to tweet out, you know, life-saving information
about tornadoes, hurricanes, tsunamis, stuff like that.
It appears that they are being severely hindered now
due to changes on Twitter.
This is one of those topics where
you want people to get the information as soon
as possible, because it could save their life.
It might be worth reminding Elon that if the people don't make
it, they won't be scrolling.
This same problem when it comes to cutting off access
to this particular feature, this is also
going to impact things that are important for a lot of people,
but a little bit less severe than not knowing your house is
going to be destroyed.
If you live in a major metropolitan area
and you get your updates on public transportation,
service delays, stuff like that via Twitter, not anymore.
It doesn't look like they will be able to do that anymore.
There are undoubtedly private entities that
going to have issues as well, but again that seems a little less important than
the National Weather Service not being able to get information to people. I
would imagine that you know giving them access or giving them free access or
something like that if Musk has decided he wants to charge for the ability to
to use bots, which would of course be my guess, that if he was to give it to him for free,
he could probably get a tax break for it.
I mean, maybe that would help incentivize helping to save people's lives.
But at this moment, there are a number of public services that are complaining that
they are being hindered when it comes to their attempts to get information out to
the public in a timely fashion. The National Weather Service, I know BART
has said something about it, quite a few public transportation places. I would
imagine there's more that may not have realized this has happened yet. My
understanding is that the rollout is is occurring like now and they're not all
aware of how their system is being impacted. So if you are somebody who
typically gets this information from Twitter, it's time to download a new app.
Get a weather app that uses your location and sends you alerts, something
like that, because you may not get the information you need from that app.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}