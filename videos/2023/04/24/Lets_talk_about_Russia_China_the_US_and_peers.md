---
title: Let's talk about Russia, China, the US, and peers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=JcotpeupPvM) |
| Published | 2023/04/24 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing China and Russia in comparison to the United States in football terms.
- Describing China as a college team, potentially a championship team due to lack of information on Chinese military operations.
- Noting the uncertainty surrounding the Chinese military's actual combat capabilities and logistics.
- Comparing Russian and Chinese intelligence, stating that Chinese intelligence is better.
- Speculating that if China significantly invested in defense, they could become a peer competitor in about a decade.
- Emphasizing that China's current focus does not seem to be on military expansion.
- Suggesting that all war games involving China inflicting damage on the US do not result in China obtaining Taiwan.
- Pointing out that China's ability to project troops globally like the US takes time and may not be their national interest.
- Speculating that China does not aspire to be a global policeman like the US.

### Quotes

- "China is a college team, maybe a championship college team."
- "Their intelligence, definitely better than Russia's."
- "If they were to dump a bunch of cash into defense, they could be a peer within a decade."
- "It doesn't really seem to be in their national interest and their national character to become expansionist."
- "I don't think they want to be a second world's policeman."

### Oneliner

Beau analyzes China and Russia's military capabilities vis-a-vis the US through football analogies, suggesting that while China could become a peer competitor, their focus seems not on military expansion.

### Audience

Military analysts, policymakers

### On-the-ground actions from transcript

- Study and understand the military capabilities and strategies of China and Russia (implied).
- Advocate for diplomatic solutions and international cooperation to prevent conflicts (implied).

### Whats missing in summary

Insights into the nuances of military strategies and potential future scenarios between the US, China, and Russia.

### Tags

#China #Russia #MilitaryCapabilities #NationalSecurity #USForeignPolicy


## Transcript
Well, howdy there, internet people.
It's Bob again.
So today we are going to talk about China and Russia
in the United States and football.
Number of questions came in about whether or not
China was more of a near peer than Russia.
One in particular asked whether or not
they were better than Russia, and if they were truly a peer.
So we're going to go through that.
Now keep in mind, this is conventional toe-to-toe match-ups.
The United States, and I'm going to use this analogy
because I scrolled your profile.
The United States, Super Bowl champs.
Okay. Russia has proven itself to be a community college team. China is a college team, maybe
a championship college team. We don't know because there's a lot of stuff we don't know
about the Chinese military. You have the stats, you have the equipment, you have the numbers,
of that stuff. But we don't know how the Chinese military actually fights, how
well they integrate. We don't know this because the Chinese military doesn't
engage in a lot of, you know, military foreign interventions. Good for them. Kind
wish the US was a little bit more like that to be honest. But because of not
really seeing it in action, don't know. There's a lot of question marks. Their
equipment is good. Estimates suggest it is definitely better maintained than the
Russian equipment. They have a good manufacturing base. They have a lot of
people. There are questions about their logistics. I remember with Russia, logistics really played
a role in the early failure. China, we don't have a clear picture of that. There aren't a lot of
estimates that provide a clear picture of Chinese military logistics as far as getting stuff to
where it needs to be and stuff like that.
Personally, it's a lot like the beginning of Ukraine.
Conventional wisdom says that Russia was just going to move in.
I was like, yeah, I don't know about that.
With China, the conventional wisdom is because they haven't done it a lot, they're not going
to be really good at it? Yeah, I don't know about that. And this is based pretty
much entirely on their trade. You don't have a trading empire like China has
without logistics. And because of the way their government works and a lot of
overlap. The military should be learning from the civilian side of things. So I
think their logistics is better than what a lot of people give them credit
for. So their intelligence, definitely better than Russia's. Definitely better
than Russia's. And it's worth remembering there was a point in time when Russian
intelligence was was top-tier. Not so much that they've shown that repeatedly.
Chinese intelligence is pretty good. So they are a near peer. They are not a
peer. They are a country that if the bulk of the estimates about their
capabilities are correct. If they were to dump a bunch of cash into defense, they
could be a peer within a decade. They really could ramp things up. I don't
really see them doing that. Historically, they don't like using military force.
good for them. I really hope that they stick to that but they do have the
capability if they pushed and tried to. I want to reiterate I don't think they're
going to try to so this is not grounds to you know ramp up an already bloated US
defense budget, but it's there.
I think the important thing to remember when you're talking about conventional matchups
like that, all of the simulations, all of the war gaming when it comes to Taiwan, China
China inflicts massive damage on the US, but in most scenarios, they don't get Taiwan.
It's worth remembering that in those war games, that's pitting everything China has that it
can use, because you have to keep some for defense and some in reserve.
It's pitting basically everything they've got against about an eighth of the US military.
It's one of those things where if China decided to become a peer, they'd have to ramp up
a whole lot and they would have to start small.
When it comes to their ability to have troops anywhere the way the US does, that takes time.
The United States, it took the US, what, 80 years to get to where it's at, and I don't
think China actually wants to do that, I think they understand the giant pitfalls that come
with that.
It doesn't really seem to be in their national interest and their national character to become
expansionist.
Not to the degree that a lot of people seem to think or seem to want because they want
that enemy.
So near peer could be a peer if they put the effort into it and money, primarily money.
But at the same time, I don't think they want to.
I don't think they want to be a second world's policeman.
Let's be real.
It hasn't worked out so great for the US in the grand scheme.
I don't really foresee them doing that.
They're the next largest competitor.
All of the US propaganda is going to be aimed at them, but I don't really foresee a situation
outside of Taiwan where the US and China would ever come to blows.
That doesn't seem likely at all.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}