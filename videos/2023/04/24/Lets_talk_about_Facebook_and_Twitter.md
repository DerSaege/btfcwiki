---
title: Let's talk about Facebook and Twitter....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=T4XKDbWH-1c) |
| Published | 2023/04/24 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Facebook might owe money to US users between May 24th, 2007, and December 22nd, 2022, due to privacy claims about data sharing.
- Facebook agreed to create a fund of $725 million, but users must file a claim by August 25, 2023, at Facebook user privacy settlement.com.
- Claims may not yield significant payments due to fund allocation for administration and number of claimants.
- Twitter recently removed verification marks (blue checks) from all users, aiming to make it part of Twitter Blue.
- Elon Musk jokingly gifted verification back to certain celebrities who were critical of his service.
- Clicking on the blue checks now reveals that the person subscribed to Twitter Blue, leading to backlash from non-subscribers.
- Some celebrities denounced Twitter Blue publicly, possibly harming its marketing strategy.
- Concerns have been raised about potential legal issues regarding using celebrity likeness for endorsements.
- Some major Twitter users are changing names to manipulate the blue checks, raising marketing concerns.
- Beau left Twitter but finds the social media drama entertaining while staying off the platform.

### Quotes

- "You actually have celebrities on Twitter who have realized if they change their name the blue check goes away."
- "It's the exact opposite of an endorsement."
- "Twitter Blue. You had a number come out and say that they wouldn't pay for it."
- "Everything's fine. No, it's a mess."
- "I don't want to be on it."

### Oneliner

Facebook may owe US users money due to privacy claims, while Twitter's verification mark changes lead to backlash and marketing concerns.

### Audience

Social media users

### On-the-ground actions from transcript

- File a claim at Facebook user privacy settlement.com by August 25, 2023 (suggested)
- Monitor developments regarding Twitter's verification changes and potential legal issues (suggested)

### Whats missing in summary

Details on the specifics of the potential legal issues surrounding the use of celebrity likeness for endorsements.

### Tags

#Facebook #Twitter #Privacy #Verification #SocialMedia


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about social media
and the ups and downs of two social media giants.
We have news about both Facebook and Twitter.
We'll start with Facebook.
If you were a Facebook user in the United States,
I'm pretty sure it's just limited to the US,
between, I want to say May 24th, 2007 and December 22nd, 2022.
Facebook might owe you money.
There were some privacy claims and dealing
with how data was shared and stuff like that.
Facebook has not admitted wrongdoing,
but they do appear to have agreed to create a fund with like $725 million in it.
Users can file a claim to get their portion. That claim has to be filed by August 25,
2023 and you can do that at Facebook user privacy settlement.com. I don't know
how much the payments will be. Realistically most times when a settlement
like this occurs yeah the fund is 725 million dollars but that that money also
goes towards covering the cost of administration of the fund. It goes to
basically everything and obviously it will depend on how many people make the
claim. So if you are in that group of people you want to find out if you're
part of this settlement or have a claim to this settlement that's where you go.
It looks like a pretty simple form.
OK, so moving on.
Twitter.
A few days ago, Twitter decided to pull the verification
marks from everybody, the little blue checks.
Because now, rather than that being a symbol of you having
been verified and being noteworthy, newsworthy,
whatever, it's now part of Twitter blue.
So they yanked the blue checks from all of the celebrities.
The idea behind that, I would assume, was to encourage people
to get the blue check and pay to use Twitter.
It does not appear to have gone the way they planned.
My understanding is that subscriptions went up
like a starship.
So we can only assume that the next part was intended
to be a joke by Musk.
And he put, he gifted verification, the blue check,
back to certain celebrities. Particularly, it seemed that it was those who were very
adamant about not using his service. It's a joke. I would assume that's what that was
intended to be. But there are a number of problems with it. When you click on the little
blue check now, it said something to the effect of, you know, this person is verified because
they subscribed to Twitter Blue or maybe provided a phone number, something along
those lines. People who did not want to be associated with subscribing to
Twitter Blue weren't happy about that. So the end result of the joke, if it was in
fact a joke, was to have people with millions of followers come out and
denounce his product. Twitter blue. You had a number come out and say that they
would never subscribe to it, they wouldn't pay for it, so on and so forth.
I never worked on Madison Avenue but that doesn't seem like a good marketing
strategy, not from where I'm sitting. And then there are some that have suggested
that he might have run afoul of laws regarding using a celebrity likeness for an endorsement.
I've seen people talk about it.
I haven't seen a definitive answer on that yet, but I would imagine that's going to,
that's probably going to come up again.
So yeah, social media is, they're doing great.
Everything's, everything's fine.
No, it's a mess.
You actually have celebrities on Twitter who have realized if they change their
name the blue check goes away and it appears that some some major Twitter
users are doing this and then somebody from Twitter is giving them the check
mark back and they're changing their name again and really from a marketing
standpoint, while we're talking about it, it doesn't really seem like a good idea.
It's the exact opposite of an endorsement. Maybe I'm wrong about that.
Maybe this is some, you know, 12d marketing strategy that I just don't
understand but so that's occurring yeah I I don't know that I'll continue
following all of this for those that don't know I basically left Twitter with
the exception of checking my inbox and yeah while I am enjoying watching the
train wreck. I don't want to be on it. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}