---
title: Let's talk about Democratic primary debates....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=mn5nvl5BJm4) |
| Published | 2023/04/24 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Reporting suggests the Democratic Party won't have debates for the presidential primary, meaning Biden won't debate challengers.
- One-term presidents historically don't have to debate challengers due to serious challenges from within their party leading to the incumbent party losing.
- Bush Sr. faced a challenge from Buchanan, Carter from Ted Kennedy, Ford from Reagan, leading to new presidents.
- The trend of serious challenges from within the party goes back to 1968 and continues with Trump, though he didn't have serious primary challengers.
- The Democratic Party might fear being associated with certain views post-global public health issues if having debates with challengers like Williamson.
- Beau acknowledges the historical trend but believes open debates within the party are better for representative democracy.
- Despite understanding the decision, Beau expresses a desire for more options and open debate within the Democratic Party.

### Quotes

- "I think the country would be better served by having a lot of open discussion between people within the Democratic Party."
- "The trend is there, I don't like it, I'm just going to go ahead and be honest about it."
- "I understand it. I get where they're coming from."

### Oneliner

Beau explains why the Democratic Party might not have primary debates, citing historical trends of serious challenges leading to incumbent party losses.

### Audience

Political enthusiasts

### On-the-ground actions from transcript

- Advocate for more open debates within political parties (implied)

### Whats missing in summary

The full transcript provides a detailed historical analysis of one-term presidents and their challenges from within the party, leading to the Democratic Party's potential decision not to have primary debates.

### Tags

#DemocraticParty #PresidentialDebates #PoliticalTrends #OneTermPresidents #Challengers


## Transcript
Well, howdy there, internet people, Lidsbo again.
So today we are going to talk about the Democratic Party
and presidential primary debates among incumbent parties.
Reporting has surfaced that suggests the Democratic Party
is not going to have debates for the presidential primary.
So Biden will not have to debate the challengers.
This has led to a lot of people wondering why.
What we're going to do is we're going to go through the explanation that you're likely to get
and the thought process behind the Democratic Party's decision.
Okay, so this is what I want you to do.
In your mind, think of the last few one-term presidents,
because that has something to do with it.
Generally speaking, tradition-wise, the incumbent,
well, they don't have to debate.
They don't have to do this stuff, and there's a reason.
Okay, one-term presidents.
You got the last guy, right?
Trump.
Trump is kind of an anomaly when it comes to this,
but not really, and we'll come back to him.
Before that, what do you have?
Bush Sr., right?
Before that, you got Jimmy Carter.
Before that, you've got Ford.
And you could even go back further with this if you wanted,
and we'll touch on one as we go back, but...
So, what happened with Bush Sr.?
I mean, other than he lost to Clinton.
He had a real challenge from inside his own party
from Buchanan.
What happened to Carter?
Real challenge from inside his own party
from Ted Kennedy. That gave us Reagan.
What happened to Ford?
real challenge from inside his own party from Reagan, it gave us Carter.
If you want to take it back further, you can look to the dynamics between Eugene McCarthy
and Johnson, which led to Humphrey, and we wound up with Nixon.
Now let's come back to Trump.
Serious challenge from inside their own party.
That's the trend, right?
Trump didn't have serious primary challengers, not really.
But he had serious opposition from within his own party.
There aren't never Bidens, but there were never Trumps, right?
That has a lot to do with it.
a trend that goes back to 1968. If the incumbent has a significant challenger from inside their
own party, the other party wins. That's why. That's going to be their answer. Now, you
You could say we're in a new age, we are in a social media age, and the debates would
provide content which will fuel discussion, which might promote the incumbent party.
And there's an argument to be made there, but they need more than Williamson.
The Democratic Party, for that to be successful, they would need more than Williamson, and
I know somebody will say, well, what about, yeah, I'm going to suggest that the last thing
the Democratic Party wants is to be associated with somebody that has those very public views
right after a global public health issue.
My guess is that even if they were entertaining it, the second that became an option and having
him up there as a potential Democratic nominee, any thought of it was over.
So I don't see a high likelihood of that occurring.
And that's going to be their reason.
Historically, I mean, the trend is there, the trend is there.
I don't like it, I'm just going to go ahead and be honest about it.
I think the country would be better served by having a lot of open discussion between
people within the Democratic Party that may not necessarily align. But I can't
argue with the historical trend. Like that's, it's there and it's prevalent. So
it's one of those things even though I don't like it, I understand it. I get
where they're coming from. I just don't necessarily think it's a great thing for
the concept of representative democracy. I think there should probably be more
options on the table, but that doesn't seem to be the likely outcome. Anyway,
It's just a thought y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}