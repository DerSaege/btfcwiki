---
title: Let's talk about hope....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=qthe5AGEdto) |
| Published | 2023/04/24 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Shares a story about growing up in the South and encountering racism and homophobia.
- Recounts incidents from his childhood where interracial relationships and LGBTQ+ individuals faced discrimination.
- Describes encountering a gay classmate from high school at a bar as an adult.
- Talks about the progress made in acceptance and equality over his lifetime.
- Acknowledges the ongoing fight for equality and the challenges faced by marginalized communities.
- Expresses optimism about the world changing for the better in the long run.
- Emphasizes the link between hope and motivation, believing in the possibility of a better future.
- Encourages individuals to continue fighting for progress, knowing that future generations are relying on them.

### Quotes

- "Hope is dangerous. It's a cliche but it's true because if you have hope for a better world that means you can imagine it."
- "No matter what group you're a part of, there will be somebody like you after you. And they're counting on you."

### Oneliner

Beau shares personal experiences of discrimination, progress, and the interplay between hope and motivation in fighting for a better future.

### Audience

Activists, marginalized communities

### On-the-ground actions from transcript

- Support LGBTQ+ youth in high schools by creating safe spaces and standing up against bullying (implied)
- Advocate for racial equality and challenge discriminatory behavior in public spaces (implied)
- Educate others about the importance of acceptance and equality to create a more inclusive society (implied)

### Whats missing in summary

Beau's personal anecdotes and reflections provide a powerful reminder of the progress made in the fight for equality and the importance of maintaining hope amidst ongoing challenges.

### Tags

#Equality #Hope #Motivation #Activism #Progress


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about hope and maintaining hope
and how I've always answered this question wrong, I guess,
because it's a question I get.
How do you maintain hope with everything that's going on?
And over the years, I've put out a couple of videos on it.
And I had somebody tell me that I was doing it wrong,
because I always combined hope with motivation.
Trying to motivate people, not necessarily give them hope
when they feel like everything's empty, everything's gone,
that it's just not worth continuing.
And that does change the question a little bit.
So how do I maintain hope?
I've told this story on the channel before.
But short version, when I was a kid,
and I mean young enough to where her dad had
to drive us to our date, I dated a girl
who, when her father found out that we
saw the bodyguard, he lost it, lost his mind, so mad, because of the implication of a white
man and a black woman.
We moved, and the first high school that I went to in Florida, there was a gay guy on
it.
football team basically just picked on this kid till he didn't want to be here anymore.
We moved and we moved, I don't know, forty miles from what was pretty much still a sundown
town. And there was a gay kid that went to that high school too. He actually wound up
falling in with my circle of friends. There is a point to all this.
And one of my friends in high school, they did somebody that wasn't white.
Man, it was a huge thing, huge thing.
We had to fight so many people, non-stop, non-stop.
Years later, I'm in central Florida and the girls that we're friends with at the time,
they want to go dancing and they just want to be left alone.
So they're going to a gay club, but they don't want to go out alone.
So me and one of my friends, we go with them.
And we're sitting there at the bar, just having a grand old time, and this guy comes up and
sits down and he looks at me and he's like, you know why I'm here, I don't know why you're
here.
And it took me a second to place him.
It was the gay kid from high school, all grown up.
And he said that he really appreciated, like, us accepting him.
And I told him, I was like, you know, being completely honest, it's not that we were
like super enlightened, he was like, it's because all of my friends were the hot girls.
I'm like, yeah, I mean, that's something to do with it.
And he laughed, and he's like, yeah, I know that.
But up until that point, you know, I didn't go to parties, I didn't go to bonfires.
I was afraid they'd kill me.
So I probably wouldn't kill you, at least not on purpose.
After that, I was driving back to the area that I consider home, and I'm telling my
friend who is black, who's riding with me, all about this great barbecue place.
And when we walk in, after hours on the road, the person behind the counter says, he brought
an N-word in here.
I was ready to go.
He didn't want to.
He wanted to get out of there.
I mean, I'd get it today.
At the time, when I was younger, I was like, no.
You know, it was going to turn into a situation.
I said all of that to point out that, to this day, when I
drive past that place, all I can think is, yeah, that's a
nice wood building you got there, it'd be a shame if something happened to it. Which
doesn't make any sense because the people that owned it, they're dead and
gone. The people who made that town a sundown town, dead and gone. The dad who
couldn't deal with his daughter watching a movie that implied an interracial
relationship then gone. It might be easier for me to have hope because I
grew up in the South. All of this was within my lifetime. Yeah, there is still a
fight today, no doubt, but it's a different fight. It's a new phase. Gay kids
in high school, they're not dealing with that. They're fighting to be accepted openly for
who they are, not terrified that somebody's going to find out. An interracial couple today,
that's not even like something people talk about, but it was a big deal when I was growing
up. Things have changed for the better and they will continue to. Yeah, the fight
right now, it's something else and in a lot of ways I see it we're
going backward. I get it and I understand how how easy it might be to lose hope
especially if you're one of the people that is
actively harmed by this. I get it.
But on a long enough timeline, we win.
I've seen it my entire life. The world is changing for the better.
and it's just impossible for me to separate hope from motivation. I can't
do it. It's not something that's in my nature because hope is dangerous. It
really is. It's a cliche but it's true because if you have hope for a better
world that means you can imagine it. If you can imagine it you can achieve it.
And if you want motivation, just remember, no matter what group you're a part of, there
will be somebody like you after you.
And they're counting on you.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}