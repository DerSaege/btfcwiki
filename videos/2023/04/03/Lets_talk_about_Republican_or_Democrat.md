---
title: Let's talk about Republican or Democrat....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=kdjCAEBLHjY) |
| Published | 2023/04/03 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Billy Young says:

- Emphasizes the importance of tailoring arguments to reach people effectively, beyond using the strongest arguments.
- Talks about the narrow perception of party loyalty versus ideological consistency when identifying as Republican or Democrat.
- Advocates for supporting policy over party or personality to encourage moving beyond party lines.
- Suggests expanding the Overton window to encourage critical thinking and moving beyond talking points.
- Recommends adjusting messaging to appeal to the person you're speaking with, like promoting a cooperative society over competition.
- Shares an example of using anecdotal evidence effectively to challenge preconceived notions, especially for those who rely on personal observations.
- Stresses the need to customize arguments based on the individual to shift thoughts effectively.
- Encourages tailoring arguments to resonate with the listener for successful communication and thought-shifting.

### Quotes

- "Support policy over party or personality."
- "You have to tailor your argument to the person that you're talking to."
- "If you're trying to use those conversations to shift thought, you have to tailor your argument to the person that you're talking to."

### Oneliner

Billy Young stresses the importance of tailoring arguments to individuals for effective communication and thought-shifting.

### Audience

Communicators, Activists, Advocates

### On-the-ground actions from transcript

- Tailor your arguments to individuals to effectively communicate and shift thoughts (suggested).
- Use anecdotal evidence when appropriate to challenge preconceived notions (implied).

### Whats missing in summary

The full transcript provides in-depth insights on the significance of personalizing arguments to effectively communicate and shift perspectives.

### Tags

#Communication #TailoringArguments #Persuasion #ThoughtShifting #CriticalThinking


## Transcript
Well, howdy there, internet people, it's Billy Young.
So today, we are going to talk about the different parties,
Republican and Democratic parties.
We're gonna talk about them.
We are going to talk about messaging
and tailoring your argument to the person
that you're trying to reach.
Because a lot of times, people will put out
what they think is the absolute best argument to prove a point, but that view
is based on their own perception. It might be the argument that worked on
them, the thing that that made them realize something, but that's not always
the best one to put forward. Sometimes putting forth a weaker argument
actually helps move people more than putting putting out the strongest most
detailed argument. We're gonna do this because I got this message. Been watching
you for a long time and have similar views. What do you say when someone asks
if you're a Republican or Democrat? At best it ends up with a 45-minute
explanation breaking down how corporate media manipulates people. Okay, so if
you're trying to sway somebody, you have to know a little bit about them, right?
You have to be able to tailor that message to them so it resonates with
them. Me, when somebody asks me that question, I tend to try to expand the
Overton window because if they're phrasing it, are you a Republican or a
a Democrat. They're looking more at party loyalty than ideological consistency. So
this is somebody that is firmly locked in in most cases, not all, but in most
cases is somebody that is firmly locked in to the idea that the Overton window
is very narrow. So I try to expand it because if you can get people to think
beyond the talking points, well the media manipulation doesn't matter as much. So
if this is somebody that is hyper partisan and you know that and you know
they ask that question, Republican or Democrat, my general answer is that I
support policy over party or personality. Well what does that mean? And generally
most times when I say that I get pushed into a situation where I'm agreeing, yeah
the Democratic Party lines up more with what I like but to me the ideas are
older and they don't they don't go far enough and that's where you can stop
there. Just get them to move beyond the idea that what the parties say are the
only options. Expand the Overton window. Now if this is somebody that is, you can
tell is a little bit more open to actually having a conversation, try to
figure out what can really appeal to them. Somebody who is generally more in
line with the way that I think, I might say something to the effect of, you know,
we need to move towards a society that is more cooperative than competitive, you
know, both the Democratic and Republican Party, they're platforms, they're older,
they need to be modified, right? So again, the idea is to push people towards a
more progressive, forward-thinking viewpoint. But if you start with that,
especially if you're talking about, for me, most people want to talk
about foreign policy with me. So saying you're moving, you want to move to a more
cooperative than competitive system, that's great. If it's somebody who roughly
aligns, you know, putting out the idea that nationalism is politics for basic
people and all of that stuff. At the same time, if somebody starts talking
to me at a diner and their truck is a is a mural of an American flag, that's not
necessarily going to resonate with them and that's something that happens to me.
So in that case, you can actually make the same points but frame it in
something that resonates with them. You know, we need new ideas, we need to move
forward, we need to change our system and our way of doing things so we can
be more cooperative and everybody gets a leg up and you know we have to embrace
these new ideas. If we don't the United States is going to fall behind. It's the
same point but it's tailored to the person that you're talking to. The goal
is to move people to a more forward-thinking idea. The way to do that
is going to be different for every person. And you can see that in the way
that I approach different things answering these messages. You know, I had a
message from somebody and I made a video about it and this is a person who is
asking for an example of systemic racism. Now, the obvious way to do that for most
people is to give them the statistics. Just start giving them the stats on how
everything works and most people will see that. But in this message, this person
let me know that they were a rural farmer or rancher. I can't remember which.
Generally speaking, people who are in that community, A, they're stubborn, B, they trust
their own observations more than anything.
They trust what they can see.
And part of the issue that was in that message was he was like, you know, every black person
I've ever met has been, you know, a good, hardworking, like great person.
I don't understand where this idea comes from.
And pointing out that his observations say one thing, but every movie he's ever seen,
all the jokes that are told are in direct contradiction to that.
That's more effective than the statistics.
Now generally speaking, if you are somebody who likes to argue, likes to debate, you don't
want to use anecdotal evidence because that's not great.
But if you're talking to somebody who bases their worldview on their own observations
and what they can see and what they can touch, it's the best way to reach them.
So I don't have a standard answer when somebody asks me that question.
And I don't know that people should.
If you are actually trying to use these conversations to move people to a better place, to move
people to be more forward thinking, I don't know that a template's the right move because
everybody's different.
And what is going to resonate with them will be different.
you kind of have to tailor your argument. Now if you're just somebody who likes to
debate, that's an entirely different story. You know, if you're just looking
for verbal sparring, use whatever you want. But if you're trying to use those
conversations to shift thought, you have to tailor your argument to the person
that you're talking to. You have to tailor your points to something that
resonates with them, and it's different.
There is no set answer to that.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}