---
title: Let's talk about Nashville, queens, and 2 stories....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=rbUOk3K6_RU) |
| Published | 2023/04/03 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Two separate stories from Nashville, usually for two videos, but both covered together due to a busy news week.
- First story: a federal judge temporarily blocked the enforcement of a ban on cabaret, called the drag ban, due to First Amendment concerns.
- Concerns raised about nationwide legislation trying to force marginalized groups from public life.
- Despite predictability of court decision, questions remain on Supreme Court interpretation and ongoing legislation impact.
- Second story: Reba McIntyre, known for avoiding political statements, expressed disappointment over the ban, urging focus on more pressing issues like feeding homeless children.
- McIntyre's rare political statement significant given her usual avoidance of such topics.
- Shift in country music royalty's stance towards acceptance and support for marginalized communities.

### Quotes

- "This isn't something that I would suggest saying, 'well we told you it was going to go this way?' Yeah, unconstitutionally vague."
- "I wish they would spend that much time and energy and money on feeding the homeless children."
- "So there is definitely a shift that has occurred, as we have talked about repeatedly, it is in New South."

### Oneliner

Two stories from Nashville: federal judge blocks drag ban enforcement, Reba McIntyre speaks out against ban and advocates for focusing on real issues.

### Audience

Activists, Music Fans

### On-the-ground actions from transcript

- Support marginalized communities by attending drag shows and cabaret performances (implied)
- Volunteer at or donate to organizations supporting homeless children (implied)

### Whats missing in summary

The full transcript provides more context on the impact of legislation on marginalized communities and the significance of McIntyre's statement in the country music world.

### Tags

#Nashville #DragBan #CountryMusic #MarginalizedCommunities #Activism


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about a couple of different
pieces of news coming out of Nashville.
Two separate stories under normal circumstances.
This will probably be two different videos.
But I have a feeling like it's going to be a busy news week,
so we're just going to cover them both at once.
So the first bit of news is dealing
with a piece of legislation.
There was a ban set to take place.
Yes, past tense, was set to take place.
A ban on what they called cabaret, the drag ban.
A federal judge said no and temporarily
blocked its enforcement. Now, for those who understand the First Amendment and
particularly how these things play out in federal court, yeah, we kind of knew
this was gonna go this way. That being said, don't take a victory lap. Don't
trivialize the concern. We know how this is supposed to play out, but I have some
questions about how the Supreme Court would interpret it. Aside from that, for
the people who are impacted by this, please keep in mind this is part of a
nationwide sweep of legislation just trying to force them from public life.
This isn't something that I would suggest saying, well we told you it was
going to go this way?" Yeah, unconstitutionally vague. I think pretty
much everybody saw that, saw that coming even from a Trump-appointed judge who
made this decision. But there are still questions and this isn't the end of it.
And the legislation is feeding a lot of animosity, so they have reason to be
concerned. The other piece of news coming out of Nashville, recently I talked about
some country music royalty and their positions on stuff like this. And if
you're being honest when it comes to country music royalty there are really
two queens. You have the blonde one and you have the redhead. You have Dolly who
is very open about her being an incredibly accepting person. And then you have Reba,
my job is to entertain you, not tell you how to live McIntyre. Getting a statement from Reba
McIntyre on anything political, I mean that's just not going to happen. Or so pretty much everybody
thought. Apparently she is viewing this as so just outside the scope of normal
politics that she doesn't have a problem talking about it. She referred to it as
disappointing, the ban. She said the ban was disappointing, wasn't surprised by it
but was disappointed by it. She went on to say, I wish they would spend that much
time and energy and money on feeding the homeless children. And she had a whole
lot more to say and basically the general tone was that we have actual
problems in this country this shouldn't even be on the radar. And she also said
and God bless them to wear those high heels. I feel for them, but let's center
our attention on something that really needs attention. I know for many people
that is gonna seem really lukewarm, but if you are not familiar with this genre
of music, understand she has been around a long time. I have no clue what her
politics are. She does not, she does not interject herself into anything like this. So for her to
make a statement like this, it's a big deal. So there is definitely a shift that has occurred,
as we have talked about repeatedly, it is in New South, and apparently the queens of country music
have aligned with the Queens of Cabaret. Anyway, it's just a thought. We all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}