---
title: Let's talk about Trump, documents, Milley, and questions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ylO-RzkQ4Bs) |
| Published | 2023/04/03 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump personally selected documents to retain after being informed to return them.
- Investigators are keen on staffers surrounding Trump at the club.
- Often, those with the most information are not in the public eye.
- Staffers like assistants, housekeepers, and security personnel hold valuable information.
- Their job involves a lot of written communication, potentially leading to future revelations.
- Investigators are questioning individuals about Trump's interest in General Milley.
- The reason behind this line of questioning could significantly impact the investigation.
- Speculation about potential reasons for Trump's interest in Milley is discouraged.
- The investigation into the documents case is ongoing, with new lines of inquiry opening up.
- The situation bears watching as it continues to develop.

### Quotes

- "Trump personally went through the documents to choose things that he wanted to hold on to."
- "There are still developments. There are definitely new lines of inquiry being opened."
- "I hope for everybody's sake. It's simply because a lot of the documents that were recovered from Trump's office or something like that had to do with him, and it's all a coincidence."

### Oneliner

Trump's personal document selection and investigators' interest in staffers signal ongoing developments in the case, with potential implications for the investigation.

### Audience

Investigators, concerned citizens

### On-the-ground actions from transcript

- Stay informed on developments in the Trump document case (implied).
- Be vigilant about potential implications of new lines of inquiry (implied).

### Whats missing in summary

Insights into potential outcomes and ramifications of the investigation.

### Tags

#Trump #Documents #Investigation #GeneralMilley #Obstruction #Staffers #Communication


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we are going to talk about the Trump document case,
staffers, some new information and reporting,
and answer a whole bunch of questions
that came in about General Milley,
because all of that's connected.
OK, so the reporting suggests that after,
After Trump and his team were informed
that they needed to give the documents back,
after the subpoena was received, that Trump personally
went through the documents to choose things
that he wanted to hold on to.
Another way of phrasing that would
be that he went through the documents
and selected some to willfully retain.
Now, according to the reporting, this
is information that the investigators have gathered
over the last couple of months.
They are, most of the reporting is framing it as obstruction,
as how that might proceed.
That may or may not be the case.
The other bit of reporting is that the investigators are displaying a keen interest in a lot of
staffers that were around him at the club. We have talked about this before during the January 6th
hearings. Oftentimes, the people that are most in the know are people that you don't know their names.
The people who have the most information are not the people who are in the PR photos.
They're the people who are just outside of the frame.
The assistants, the housekeepers, the bellmen, the security staff, the people who just run
comms, stuff like that.
And that turned out to be the case with the January 6th hearing.
The thing about a lot of those people is that their entire job is communicating with others.
Because of that, there's a lot of written communication.
We will probably see information about that in the future, would be my guess.
So those are the pieces of the reporting that are being focused on.
A whole bunch of y'all caught on to one little tidbit in the reporting and are very curious
about it.
According to the reporting, the investigators are asking people if Trump had any specific
interest in General Milley.
Yeah that's a very curious line of questioning.
And a lot of the questions that came in were, do I have any thoughts about that, tons.
There are only a number of reasons, a few reasons, that they would be asking that kind
of question.
I hope for everybody's sake.
It's simply because a lot of the documents that were recovered from Trump's office or
something like that had to do with him, and it's all a coincidence. As far as
anything beyond that, any other reason would be bad. Bad enough that I don't
want to speculate about. If it's another reason, it will probably change the
course of the investigation. So that's the news and the documents case is
moving along. There are still developments. There are definitely new
lines of inquiry being opened. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}