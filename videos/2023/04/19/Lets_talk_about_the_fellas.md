---
title: Let's talk about the fellas....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Mu3b1FsLDqo) |
| Published | 2023/04/19 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of "the fellas" on Twitter, a group that counters Russian propaganda and supports Ukraine.
- Responds to a message claiming that the fellas are not real people but a botnet controlled by US intelligence.
- Clarifies that the fellas are real human beings, not a botnet, and that while Western intelligence may amplify their message, the core group is genuine.
- Disputes rumors linking the fellas to CENTCOM or CIA control, providing insights into the geographic nature of US commands and debunking the Virginia headquarters claim.
- Shares the humorous origin of the CIA rumor, where some changed their location on Twitter to Langley as a joke.
- Encourages critical examination of rumors, especially regarding jargon like CENTCOM, which is often mistakenly associated with various global events.
- Asserts that the majority of information shared by the fellas is organic and accurate, supporting Ukraine without significant disinformation.

### Quotes

- "They are real human beings."
- "That's just not true."
- "It is definitely more akin to the USO than some super secret operation."

### Oneliner

Beau explains the authenticity of "the fellas" countering Russian propaganda on Twitter and debunks claims of them being a botnet controlled by US intelligence, affirming their real human existence and organic information dissemination.

### Audience

Social media users

### On-the-ground actions from transcript

- Support the efforts of groups countering disinformation by sharing accurate information and debunking false claims (implied).
- Engage in critical thinking when evaluating rumors and online information (implied).

### What's missing in summary

Beau provides valuable insights into how to discern rumors and misinformation online and showcases the authentic efforts of individuals countering propaganda with accurate information.

### Tags

#Twitter #RussianPropaganda #Ukraine #Disinformation #USIntelligence


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about the fellas on Twitter.
We're going to talk about what they are, what they are not.
We're going to go through a message that somebody received.
And we're just going to kind of fact check it.
And this is one of those times where
you can learn how to look at a rumor
discern whether or not the source claimed in the rumor actually knows what
they're talking about, and that can tell you how much stock to put in it. Okay, so
here's the message. If you don't know what the fellas are, I'll explain in a
second. A guy I argue with all the time on Twitter said that the fellas weren't
real people and that they were a botnet. Just wondering if there's any truth to
this, here's exactly what he said. Quote, the fellas aren't real. They're a product
of US intelligence. A friend in the military told me they were all controlled
by United States Central Command in Virginia. Anybody with half a brain would
see it's clearly CENTCOM at work to spread disinformation. Okay, so if you
don't know what the fellas are, they're a group. I know they're really active on
Twitter, they're probably in other places too, other social media platforms.
They're identified by a little dog image, and generally what they do is they counter
Russian propaganda, they put out Ukrainian propaganda, they fact check, they are kind
of an organic information operation.
are on the Ukrainian side. So you will see them a lot when the Russian government
like puts out a statement that needs context, let's just say, they will
show up and be like, yeah, this is where you're wrong, and they'll provide a bunch
of information. Are they a botnet? No, no. I know some people that are fellows. They
are real human beings. That being said, would Western intelligence maybe help
amplify them or maybe create additional fellows? Yeah, I mean that's in bounds.
It's possible, but the idea that they're not real people, that's just not true. In
In fact, you can go to most of the accounts and see, if you scroll back far enough, you'll
see when they became a fella and decided to get involved, and prior to that, they were
just normal accounts.
And most times, you will find that they're real people, sometimes very high profile people,
who are just more than looking at it as like some spooky intelligence operation.
Think of it more like the USO, because that's kind of a better analogy.
But at the same time, the idea that Western intelligence is somehow helping, I mean that's
in bounds.
That part isn't ridiculous.
I would guess that that's actually happening.
I don't have any proof of that, but it would make sense.
And I know that there are former intel who are fellas.
But again, showing that they're human beings.
But the rumor is they're a botnet, they're all fake, and it's based off of the friend
in the military.
And there are versions of this, but all of them share it being controlled by CENTCOM
or the CIA, which we'll come back to that because that actually has a funny origin.
Okay, so first thing to know, United States Central Command.
That's not in Virginia.
They're headquartered out of Tampa.
Second off is when people hear United States Central Command, they think that that's like
the main place, that's the center.
That's not how US commands work.
U.S. commands are geographic in nature. Every command has an area that they cover. CENTCOM
covers, it's actually more than the Middle East, but as a general frame of reference,
they cover the Middle East and other countries nearby.
CENTCOM doesn't care what's happening in Europe.
They're not running the fellas out of Virginia when they're out of Tampa, and they are far
more concerned with what's going on in Syria than they are anything happening in Ukraine.
The idea that somebody with half a brain would obviously see this, that's false.
It's providing that fake credibility.
My friend in the military told me this.
It doesn't even make sense.
It's literally the wrong command because people hear central command and they think
high command or something like that.
That's not how it works.
The other rumor is that they are all based out of Virginia and they're run by the CIA.
That's the other version of this.
There's a funny origin to that though.
This definitely came about in an organic manner.
And early on, the people who were favorable to Russia started saying they were all CIA.
And it was really just kind of like somebody saying, oh, you're obviously working for
the Kremlin.
As a joke, all of the fellas, at least a large portion of them, changed their location on
Twitter to Langley.
And that's kind of what reinforced it for people who didn't know how it started.
So when you are examining a rumor that comes across, any time there's jargon, see what
the jargon means because often times people will insert jargon and they may not know what
it means and CENTCOM actually gets blamed for a whole lot of stuff like all over the
world that literally has nothing to do with them, and they could care less about.
So one little tool for examining the rumors.
At the end of the day, honestly, my best guess is that like 90% of what you see from the
fellas is very organic.
They're real people.
They support Ukraine, and they're putting out information to support their side, so
to speak.
Generally, you don't see a lot of disinformation from them.
That's another term that's in here.
You don't really see a lot of that.
You see a lot of debunking, and you see a lot of stuff that is propaganda, don't get
me wrong, but it's mostly accurate information.
It's just showcasing positive news for the Ukrainian side.
But again, it is definitely more akin to the USO than some super secret operation ran by
CENTCOM in Virginia because they got bored, I guess, with the Middle East.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}