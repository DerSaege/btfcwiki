---
title: Let's talk about Oklahoma updates....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=z6Bja1mxv0Q) |
| Published | 2023/04/19 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Updating on the events in McCurtain County, Oklahoma since a recording surfaced involving county officials having disturbing and inappropriate conversations.
- Topics in the recording included off-color commentary, dealing with a burn victim, talk about removing a journalist permanently, and expressing desires to beat and hang black people.
- Governor has called for resignations, with local people staging demonstrations for the same.
- Surprisingly, the Sheriff's Association in Oklahoma voted to expel the members involved in the recording, showing a departure from the usual closing of ranks to protect law enforcement officials.
- People on the recording have not publicly addressed its content, claiming it was obtained illegally.
- Media will likely continue coverage due to the perceived threat against a journalist.
- Expectations of resignations in the future due to continued coverage, local outcry, lack of law enforcement solidarity, and the governor's stance.
- Real investigation initiated at the state level.
- Beau will monitor and provide updates on any new developments.

### Quotes

- "The governor has called for their resignations but beyond that there have been local people staging demonstrations calling for their resignations."
- "I think they did that unanimously, I'm not sure."
- "There will continue to be coverage."
- "I will continue to monitor this and keep y'all updated on any new developments."
- "Y'all have a good day."

### Oneliner

Beau provides an update on the disturbing recording involving Oklahoma county officials, with calls for resignations, a surprising lack of law enforcement support, and ongoing media coverage indicating potential future resignations.

### Audience

Local Residents, Activists

### On-the-ground actions from transcript

- Join local demonstrations calling for resignations (exemplified)
- Stay informed through media coverage and continue to demand accountability (implied)

### Whats missing in summary

Full context and depth of the events and reactions in McCurtain County, Oklahoma.


## Transcript
Well, howdy there, internet people.
Let's go again.
So today we are going to talk about Oklahoma again,
provide a little bit of an update
on what has happened in McCurtain County,
Oklahoma since the recording has surfaced
and what's occurred since the last video,
go over the developments.
And there have been a few.
We will also talk about a claim regarding the origin
of the recording. If you have no idea what I'm talking about, a recording
surfaced that is alleged to feature a number of county officials having a
conversation that you do not want county officials having. Topics ranged from some
off-color commentary, dealing with a burn victim, to a conversation about possibly
removing a journalist permanently, to what appears to be a desire to still be
able to beat and hang black people. Really not a good conversation. Now the
governor has called for their resignations but beyond that there have
been local people staging demonstrations calling for their resignations. Generally
in a situation like this you could expect law enforcement to close ranks to
protect the sheriff or the other law enforcement officials that might have
been involved in this conversation. That apparently is not happening. The Sheriff's
Association in Oklahoma voted to expel the people that were members of their
Association, and I think they did that unanimously, I'm not sure. The people that
are alleged to be on the recording, at time of filming they still haven't made
a public statement about the content of the recording. However, it does seem as
though the sheriff is saying that the recording was obtained illegally.
Um, generally speaking, journalists know what they can record and what they can't in most
situations.
And my understanding is that this recording originated with a journalist that had a number
of years under their belt.
My guess is that that aspect of it will hinge entirely on whether or not the people on the
recording had a reasonable expectation of privacy, a term that most law
enforcement officials are familiar with. That will probably come into play. Now,
because of the perceived threat against a journalist, the media is not going to
let this go. There will continue to be coverage. Between the continued coverage,
the local outcry asking for resignations, the local and state law enforcement
structure not closing ranks, and the governor calling for resignations, even
though at this point it appears that they are trying to ride it out, I would
imagine that by the end of this we're gonna see some resignations. It does
also appear that there is a real investigation that has started from the
state level. But I will continue to monitor this and keep y'all updated on
any new developments. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}