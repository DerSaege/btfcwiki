---
title: Let's talk about Fox, 787 million, and what's next....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=SPfjcCdhMFA) |
| Published | 2023/04/19 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Fox and Dominion reached a settlement of $787 million right before trial, sparking emotional reactions as people wanted a full-blown trial.
- The settlement doesn't signify the end as Smartmatic, another company, is gearing up for a suit asking for more money.
- Fox's willingness to settle may have opened the floodgates for additional lawsuits due to concerns about public revelations.
- The settlement amount, though significant, may not be substantial considering Fox's annual revenue.
- The settlement impacts Fox's reputation and could provide ammunition for those aiming to remove Fox News from certain areas like military installations.
- Fox's decision to settle for $787 million appears unusual as they were already invested in the legal process.
- Legal analysts suggest Fox was likely to lose the trial, prompting them to settle.
- Dominion agreed to the settlement as receiving $787 million quickly outweighed the risks and delays of a trial where a larger sum might be awarded.
- Beau questions Fox viewers to critically analyze the situation and evidence, suggesting that they were misled by Fox.
- Beau anticipates more repercussions for Fox in the future regarding their reporting practices.

### Quotes

- "Fox and Dominion reached a settlement of $787 million right before trial, sparking emotional reactions as people wanted a full-blown trial."
- "Fox's decision to settle for $787 million appears unusual as they were already invested in the legal process."
- "Dominion agreed to the settlement as receiving $787 million quickly outweighed the risks and delays of a trial where a larger sum might be awarded."
- "Beau questions Fox viewers to critically analyze the situation and evidence, suggesting that they were misled by Fox."
- "I think that there is much more to come when it comes to Fox having to answer for a lot of their reporting."

### Oneliner

Fox and Dominion's $787 million settlement before trial sparks emotional reactions, with implications for Fox's future reputation and potential legal challenges.

### Audience

Viewers, Fox News fans

### On-the-ground actions from transcript

- Analyze the evidence and situation independently (suggested)
- Stay informed about the ongoing developments related to Fox News (implied)

### Whats missing in summary

The full transcript provides in-depth analysis and context on the legal settlement between Fox and Dominion, encouraging viewers to question their trust in Fox News.

### Tags

#Fox News #Legal Settlement #Media Accountability #Dominion #Viewership Trust #Future Implications


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Fox
and $787 million and the settlement.
And what that settlement means for the future,
whether or not it was a good or a bad idea,
why it might've been agreed to, all of that stuff,
because there's a lot of information coming out about it
And it's very emotionally charged right now,
because most people did not get what they wanted, which
was a full blown trial.
Now, if you don't know what happened, right before trial,
I mean right before trial, Fox and Dominion
reached a settlement to the tune of $787 million,
little bit more than. So, first thing to know, this isn't over. This isn't over.
It's important to remind everybody that there is another suit coming from Smart
Matic, another company, right around the corner, and they're actually asking for
more money. The other thing is that Fox may have unintentionally opened the
floodgates here. They have indicated their willingness to settle probably
because of a desire to not have what's in those messages and all of that evidence
become public. They may see other suits surface. I want you to imagine a suit
from the Jan 6th defendants. I want you to imagine a suit from the cops, the
staffers that were at the Capitol that day. I know that sounds wild, but keep in
mind there was a another right-wing figure who just what hundreds of
millions of dollars because of bad information aired that then prompted
people to go to other people's homes. It's not that far out of the realm of
possibility. Imagine states wanting to recoup the losses of managing protests
or paying for recounts, stuff like that. Fox may have set a chain of
events in motion here. Another thing that is being brought up is that this isn't a
a whole lot of money for Fox because they make $70 gajillion a year or whatever.
Almost every single figure that I have seen thrown out, that's their revenue, not their
profit.
That's the amount of money they have coming in before they pay anybody, pay rent, pay
for equipment, any of that.
Those numbers are very, very different.
It is hard.
I wasn't readily able to find good numbers on Fox's profits for recent years, but based
on estimates or previous stuff that I could find, Dominion took out a few months to up
to a year of the profit.
That's not a little thing, it's a big deal.
And that is before the damage to Fox's reputation.
Because while there are some hardcore viewers of Fox that will never be swayed, there are
some who are going to be like, wow, that's kind of a big deal.
It also puts them in a position where people who have been trying to get Fox removed from
certain areas now have a whole lot more ammo.
There has been a widespread movement to get Fox News banned from military installations
in common areas.
Now because of this, I would imagine that eventually DOD is going to have questions
about any commander that allows it, because there's a morale issue that may come into
play now, and that would be a huge blow to Fox.
There are a lot of downstream consequences from what has happened.
Now, you obviously have people that are going to want to spend this and what's
the defense going to be? Lots of companies settle. Yeah, that's true.
That's true. Lots of companies do settle even when they didn't do anything wrong.
That's a fact. That is a fact. In fact, let's say Dominion, as an example, let's
say some subcontractor working for them drops a box of machine parts on their
foot. Files sued for $5,000. Even if Dominion can show they're not at fault, even if they
could show that the person who dropped the box was under the influence, they still might
settle because the $5,000 is less than paying the cost for the attorney to go through the
entire process, discovery and all of that stuff. The thing with Fox is they'd
already paid for all that. The discovery process, all of the procedural stuff
before, that's all paid for. The jury was already seated when this settlement went
through. One thing is not like the others. Fox paid seven hundred eighty seven
million dollars to avoid a trial. Now it could be because they were pretty
certain they were gonna lose and they thought the judge or the jury would
award more than 787 million or it could be worth 787 million dollars to that
company to not have their hosts be on the stand. It could be worth 787 million
dollars to not have their viewers be aware of the content of some of the
evidence. So that doesn't fly. This isn't like a lot of situations where a company
will settle early to avoid all of the cost incurred with lawsuits. This is very,
very different. Most legal analysts basically said it was it was over. Fox
was going to lose. It wasn't a matter of whether or not they were going to lose.
It was a matter of how much they were going to have to pay. So with that in
mind, why did Dominion agree to this? That's another question that has come up.
And the answer is simple. The settlement, $787 million, well Fox has to pay that.
that if they had gone to trial and been awarded let's just say 1.5 billion
dollars okay that sounds like more money but Dominion actually gets the 787
million pretty quickly the 1.5 billion Fox isn't gonna want to pay that appeal
after a pill, after a pill. I want to say Dominion is only worth like 80 million,
roughly. This is huge for them, huge. So they took the check because it's actual
money that they can get, like right now. I know that most people wanted a trial. I
wanted a trial, but as we talked about, I want to say yesterday, Fox has the
ability to put a whole bunch of zeros on a check to make it go away, and that's
what they did. But this isn't over, and I don't think necessarily that this is
just going to make everything go away, and their viewers are going to
necessarily accept it. At this point, if you are a Fox viewer, you have to
understand they lied to you. They lied to you. They put information on
knowing it wasn't true or at least having serious doubts about it. And there
There is a lot of evidence that came out that can help you come to that conclusion on your
own.
You don't have to listen to me.
Don't listen to me.
Go look at the messages that came out.
Check it for yourself.
Come to your own conclusion on it.
We report, you decide.
That would be my advice if you're a Fox News viewer.
You have to question whether or not you can really trust them at all after this.
That's a determination that you as an individual have to make because I want you to think about
how serious these allegations were.
The stuff that Fox broadcast, think about how serious it was, how many problems it caused.
And if they were willing to intentionally err it or were incapable of stopping falsehoods
from being erred about something that serious, can you even trust them with the little stuff?
These were massive, massive allegations.
Short version, $787 million, Dominion got a payday.
There's not clear reporting at time of filming about an apology.
I don't think this is over.
I think that there is much more to come when it comes to Fox having to answer for a lot
of their reporting.
And the just inevitable, lots of companies settle type of thing, one thing is not like
the others.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}