---
title: Let's talk about today's SCOTUS ruling and the future....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=H-kyGky0KUE) |
| Published | 2023/04/22 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A judge in Texas attempted to order a nationwide ban on mephepristone, commonly used for reproductive rights.
- The Supreme Court issued a stay, keeping the medication widely available for now.
- Justices Alito and Thomas dissented, with further legal battles expected in the Fifth Circuit and then back to the Supreme Court.
- Beau questions the enforcement of a nationwide ban on methepristone due to its widespread use.
- The future legal battles could result in consequential rulings, making it a roller coaster ride until the final Supreme Court decision.

### Quotes

- "A judge in Texas decided to attempt to ban something nationwide."
- "There's more litigation to come."
- "Take a breather, relax, and just know that for the time being it's widely available."

### Oneliner

A judge in Texas attempted a nationwide ban on mephepristone, but the Supreme Court's stay keeps it available for now amidst ongoing legal battles, suggesting a roller coaster of events ahead.

### Audience

Reproductive rights advocates

### On-the-ground actions from transcript

- Stay informed about ongoing legal battles and decisions (implied)
- Support organizations advocating for reproductive rights (implied)

### Whats missing in summary

Insights into the potential implications of the Supreme Court's final decision on reproductive rights. 

### Tags

#ReproductiveRights #SupremeCourt #LegalBattles #Mephepristone #Enforcement


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about the Supreme Court.
We're going to talk about what happened today
and what is likely to happen later.
OK, so if you have no idea what's going on
or what the decision today was about,
we'll just kind of do a brief overview,
because while there was a decision, this isn't over.
Okay, so short version, short recap here.
A judge in Texas decided to attempt
to ban something nationwide.
Another judge was like, God, no,
and set up a situation where the whole thing
got fast-tracked to the Supreme Court pretty quickly.
The judge in Texas was attempting to kind of order a ban
of mephepristone nationwide.
And this is a medication commonly used when people are exercising their reproductive rights.
And it's also used in other ways as well, making the enforcement of a nationwide ban
problematic.
The Supreme Court has issued a stay, meaning for the time being, the medication will remain
widely available.
But it's not over.
Alito and Thomas dissented, shock, right?
But it now goes back to, I want to say, the Fifth Circuit, which is widely expected to
issue some outlandish ruling, which will then send everything back to the Supreme Court again.
But we'll have to wait and see how that plays out. There were a lot of people justifiably
concerned about this. You have a breather. It's not over, because there is more litigation to come.
There's going to be continued legal battles.
This is one of those things that I don't really understand because of the widespread
use of methepristone and its companion medication.
I don't actually understand how they intend to execute a ban nationwide.
I understand that they're saying, oh, it can't be used for this.
But I don't know how they plan on enforcing that.
As far as the future, it's going to go back.
There will probably be some wild ruling, and then it'll go back to the Supreme Court.
What happens there will be pretty consequential.
I would imagine between now and then it's going to be a roller coaster, so be ready
for it, but also understand that any setbacks that occur between now and it getting to the
Supreme Court are temporary.
What the Supreme Court does, that's going to matter, but at this point it's kind of
hard to say.
I know there's a lot of commentators who say that the Supreme Court is going to say,
it's banned. But if you look back two weeks ago, they were also saying the
Supreme Court was going to allow the ban to stay in place now, and they didn't.
I don't really have an opinion on how it's going to play out. I am very
curious. I am cautiously hopeful, but we'll have to wait and see. Take a
a breather, relax, and just know that for the time being it's widely available but
there's there's more to come. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}