---
title: Let's talk about Trump's aide being questioned....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=4ReiTnWGTI8) |
| Published | 2023/04/22 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Providing update on Trump's legal entanglements in DC, separate from New York and Georgia issues.
- Trump's top aide, Boris Epstein, questioned by Department of Justice over two days about January 6th and interactions with Rudy and Eastman.
- Epstein is one of nine Trump aides who had their phones taken by the feds.
- Epstein questioned with Smith present, who just observed without asking any questions.
- The length of questioning over two days hints at significant content being discussed.
- Department of Justice may be struggling to get accurate answers or feeling there is more to uncover.
- Epstein's involvement brings the investigation closer to Trump's inner circle.
- This phase of interviews with direct contacts of Trump suggests a nearing end of the investigation.
- DOJ likely seeking information on what was shared with Trump.
- Indicates a significant development that may impact future timelines and events.

### Quotes

- "It seems like they are at the stage where they are talking to people who had a lot of direct contact with Trump."
- "This is probably nearing the end."

### Oneliner

Beau provides an update on Trump's legal entanglements in DC, focusing on questioning of top aide Boris Epstein and the proximity of the investigation to Trump's inner circle, signaling a potential conclusion.

### Audience

Political analysts, news followers

### On-the-ground actions from transcript

- Stay informed about developments in legal investigations involving public figures (implied)

### Whats missing in summary

Insights on the potential implications of the investigation's progress and the broader context of similar legal proceedings. 

### Tags

#Trump #LegalEntanglements #DepartmentOfJustice #Investigation #BorisEpstein


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we are going to provide an update on the
situation with Trump.
We're going to go over one of his legal entanglements and
some of the developments there.
This is the stuff in DC.
So not the New York stuff, not the Georgia stuff, not the
document stuff, not the civil stuff.
This is the DC stuff.
It was talked about over Thursday and Friday while it was happening, but it didn't really
seem to get the emphasis that it should.
So to put a little bit of context on it, one of Trump's top aides for years was questioned
the Department of Justice over Thursday and Friday, two days, Boris Epstein.
The reporting suggests that this was all focused on January 6th stuff, and he was questioned
about his interactions or relationship with Rudy and Eastman and all of this stuff.
So it's playing into the paperwork plot and all of that stuff.
It is worth remembering that this is somebody who had their phone taken, you know, I want
to say it's nine Trump aides had their phones taken by the feds.
He's one of them.
the questioning over these two days, it's reported that Smith himself came in and
sat in on the questioning. That's how it's being reported and framed. It was
said that he didn't ask any questions. He didn't lob any questions. He was just in
the room. Didn't ask any questions. Okay, keep your secrets. I'm gonna say that
there may be more to that part. It seems odd that he would just come in
and hang out. It seems like a bizarre set of circumstances. There may be more to
that we don't know at this point. But that is the reporting. Smith coming in
himself, that's kind of a big deal, even if it was just to sit there and watch. We
don't know the content of the conversation, but given the fact that it
spanned Thursday and Friday, there was probably a decent amount there, and
The length of the conversation could mean one of two things, either the Department of
Justice was not getting answers, or answers that they felt were accurate, or they thought
there was more, or it could mean they were getting answers.
in a situation where it's probably going to be hard to protect Trump because of
his phone being taken. It's a unique situation and it's probably a major
development. The conversation and what took place is probably something that's
going to matter later, like when a timeline of important events is put
together, this is probably going to be on it. We don't know why it's going to be on
it yet, and odds are we won't know for a bit, but this is now moving into Trump's
immediate circle. People that he interacted with, or still interacts with I
think in this case. So we've talked about how these investigations are
conducted and how they start in rings and the rings get smaller and smaller
and smaller. This is kind of final ring. It seems like it anyway. This seems
like they are at the stage where they are talking to people who had a lot of
direct contact with Trump. Not their aid, not their aid's assistant, not somebody
further down the line who heard something, not somebody in another state, or talking
about somebody who had direct contact.
Generally, these kinds of interviews, and there may be more to be clear, they kind of
signal movement towards the end of an investigation.
When you're at that point and you're ready to talk to those people for a day or two,
generally already have the information, what you're wanting is that person to
tell you how much of that information was shared with the the principal person
being investigated. So this is probably nearing the end. Anyway, it's just a
thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}