---
title: Let's talk about 2 Tennessee questions and a joke....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=mqQ8nPHTreU) |
| Published | 2023/04/22 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Overview of recent events in Tennessee, including the resignation of Scotty Campbell, vice chair of the House Republican Caucus, amid allegations of inappropriate behavior or harassment.
- Speculation on the possibility of more resignations in light of increased media coverage and scrutiny of ethics within the legislative body.
- Responds to a question about comedian Drew Morgan, mentioning his use of the word "fascist" to describe Tennessee's government in a recent act.
- Beau shares his perspective on Drew's comedic approach, noting that different tactics work for different audiences, and he appreciates Drew's ability to energize progressive individuals through comedy.
- Beau contrasts his own cautious use of strong language on YouTube with Drew's more liberal use, based on their different approaches and target audiences.
- Emphasizes the importance of recognizing diverse tactics in reaching varied audiences, acknowledging that what works for one person may not work for another.
- Beau concludes by posing the question of whether Drew's approach is wrong, leaving room for individual interpretation and reflection.

### Quotes

- "I don't think the media is going to let this go."
- "People aren't the same. They won't be reached by using the same tactics."
- "It's a diversity of tactics thing because everybody's different."
- "The real question you have to ask is, is he wrong?"
- "Anyway, it's just a thought, y'all have a good day."

### Oneliner

Beau delves into recent Tennessee events, potential resignations, and comedian Drew Morgan's approach, advocating for diverse tactics in reaching varied audiences.

### Audience

Progressive individuals

### On-the-ground actions from transcript

- Watch Drew Morgan's comedy clips about Tennessee to understand his approach and messaging (suggested).
- Embrace diverse tactics in communication and advocacy to reach different audiences (implied).

### Whats missing in summary

Further insights on the importance of tailoring communication strategies to diverse audiences based on their preferences and receptivity.

### Tags

#Tennessee #Resignations #DrewMorgan #Comedy #Progressive


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about Tennessee
and resignations and a comedian named Drew Morgan
in Diversity of Tactics.
We're gonna do this because one person
sent in two questions, and they're both about Tennessee,
and they're both worth covering.
The first one, do you think there will be more resignations?
Probably, probably. If you don't know what happened, the vice chair of the House Republican Caucus,
Scotty Campbell, resigned. The reason for that isn't entirely clear. The reporting isn't very
in-depth at the moment. It has been described in various ways, from he made inappropriate comments,
to harassment. What exactly occurred I don't really know. He describes it as,
I had consensual adult conversations with two adults off property and went on
to say, if I choose to talk to any intern in the future it will be recorded. Shortly
after that he resigned, like within hours. Do I think there will be more? Yeah. You had a
legislative body that is not exactly world-renowned for its ethics, grandstanding on ethics to the
point of expelling two people. When something like that happens, generally speaking, the media starts
looking at everybody's glass houses. So I would imagine there's going to be more media coverage,
which will lead to more things that are perceived as scandals,
which will lead to more resignations.
Or they may just decide to, you know, try to write it all out.
But I wouldn't expect the coverage to end anytime soon.
I don't think the media is going to let this go.
The next question.
I was wondering if you know Drew Morgan.
And if so, if you saw his most recent clips about Tennessee, he used the f-word a lot
in his act to describe Tennessee's government.
You're always very careful with language and was wondering if you thought it was too
far.
You think every southern progressive person knows each other?
That's a little bit stereotypical, don't you think?
Yeah, I know.
In fact, I was trying to figure out how to reference and kind of plug his new act, and
I was trying to work it into this commentary about police, but it just seemed very hamfisted
and it wasn't coming through right, so I'm really glad you asked this question.
Drew's a good dude.
People on this channel will know about the striking miners in Alabama.
He helped with that.
He's a good guy.
Now, he used the F word a lot.
For the sake of clarity, the F word in this case is fascist.
He used the word fascist.
That being said, if you choose to watch the clip down below, probably not something you
want to watch with your kids around or in an office where people can hear you.
Do I think he went too far?
Yeah, I'm careful with language, but I have a different job.
I don't do the same thing.
I use that word very sparingly because YouTube Analytics tells me that the people that I'm
trying to reach who may be on that road, they tune out when they hear it.
So I only use it when it is just ridiculously obvious or I can be like, hey, you support
this guy and here are 14 characteristics about him, by the way.
are the 14 characteristics of fascism. It's a push-and-pull that I use. He has a
different job. He energizes progressive people via comedy and he tries, I assume,
tries to reach people via that comedy and he is very disarming. Earlier in the
act. He's like, I'm not woke, I am toxic. It's very disarming. And then later to hear that same
person be like, oh yeah, Tennessee's government, they're fascists. It works. It's a push-and-pull
thing. I don't even know that he does that intentionally. I think it's just him up there
telling jokes, and it works very well. In fact, I think if he planned it, it probably wouldn't work.
work. Something that happens a lot is people critiquing political commentators, or in this
case a comedian, and they're trying to find a one-size-fits-all approach. It's not how
works. It's not how it works. Somebody who would respond and be
swayed by Drew's act, they would find me just incredibly boring. Somebody who
likes things a little bit more of a slower pace, they might be just
completely put off by some of the language in his act.
There's not a one-size-fits-all approach.
It's a diversity of tactics thing, because everybody's
different.
They'll be reached in different ways.
There are a lot of creators on YouTube, or people who stream,
or whatever, who say things all the time that, when I'm
watching them, I'm like, dude, I can't believe you said
that. But I've also gotten messages saying that that person, you know, got them off of
a bad road or something like that. I do not argue with results. People aren't the same.
They won't be reached by using the same tactics. So, no, I don't think he went too far. And
The real question you have to ask is, is he wrong?
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}