---
title: Let's talk about Texas, the Commandments, and what's next....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=wh1NYvlRSSQ) |
| Published | 2023/04/22 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Texas proposed Senate Bill 1515 mandating the presence of the Ten Commandments in classrooms.
- Under a normal Supreme Court, this mandate violates the First Amendment and is likely to be struck down.
- The current Supreme Court's composition raises concerns about how they may interpret such a law.
- If passed, unintended consequences may lead to the inclusion of various religious texts and beliefs in classrooms.
- The motives behind the proposed law could be to either include diverse religious texts or manipulate the base.
- Beau questions if the legislature believes the law will stand or if it's a ploy to manipulate their base.
- The likelihood of this law standing, given the ignorance of the base, is questioned by Beau.
- The proposed law may be an attempt to motivate a base perceived as ignorant.
- Beau wonders about the outcome if classrooms were filled with guidelines from different religions.
- Despite doubts, Beau doesn't see this law progressing.

### Quotes

- "It's one of those two. I can't think of another option."
- "Your most likely answer as to the motivation behind it, it's just to trick the ignorant people of Texas that don't know no better."
- "Even with this Supreme Court, that's just ridiculously unlikely."
- "You don't understand the Constitution, you'll fall for this, you don't have any understanding of civics, you are ignorant, and we own you."
- "Let's go ahead and just wallpaper one side of every classroom with all kinds of guidelines from different religions."

### Oneliner

Texas proposed a law mandating the Ten Commandments in classrooms, raising concerns about unintended consequences and potential manipulation of constituents' ignorance.

### Audience

Texans, Educators

### On-the-ground actions from transcript

- Question the motives behind proposed laws (implied)
- Stay informed and engaged in legislative matters (implied)

### Whats missing in summary

The full transcript provides a deeper analysis of the potential implications of the proposed law and the dynamics between the legislature and constituents in Texas.

### Tags

#Texas #ProposedLaw #ReligiousFreedom #SupremeCourt #Constituents


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we will talk about a proposed law in Texas.
We will talk about likelihoods, if it becomes law.
And then we will talk about the law of unintended consequences
that will quickly follow some of those options.
The, I think it's Senate Bill 1515, and it has definitely sparked some conversation.
If it becomes law, it will basically mandate that a copy of the Ten Commandments is in
pretty much every classroom.
Under a normal Supreme Court, it would be a certainty that this would be struck down.
Under any normal Supreme Court, that's what would happen.
If it became law, if this was passed, the Supreme Court would just be like, no, stop,
bad, bad Texas.
The fact that the First Commandment, you know, you can't have any gods before me, that thing,
that definitely runs afoul of the whole exercise and establishment thing.
But given the fact that we have people sitting on the Supreme Court who could not explain
the First Amendment during their confirmation hearings, we don't have a normal Supreme Court.
So there is an outside chance that this stands in some fashion.
If that occurs, the law of unintended consequences comes into play because, rest assured, no
matter how the Supreme Court interprets it, it will not only allow the Ten Commandments.
If this happens, right next to the Ten Commandments will be maybe something about the pillars
of faith from Islam.
There will be Satanist rules.
There will be all kinds of things.
There is no way that it will stand just allowing the Ten Commandments and only the Ten Commandments.
That's not going to be a thing.
Even with this Supreme Court, that's just ridiculously unlikely.
I would almost say impossible.
So given the fact that the people in Texas, the legislature there, they have to know this.
isn't a new idea. Why are they putting it forward? You have two options. One is
that secretly they want, you know, satanic literature in a classroom. That's
one option. The other option is that they know it won't stand and they're just
doing something to manipulate their base who they think are stupid.
Those are the options.
Again, you have a Republican dominated legislature looking at their constituents and saying,
you don't understand the Constitution, you'll fall for this, you don't have any understanding
of civics, you are ignorant, and we own you.
We can do anything and say anything and you'll fall for it.
It's one of those two.
I can't think of another option.
I don't believe that a majority of the people in the legislature there in Texas actually
think this would stand.
It's just a move to motivate a base that they think is too ignorant to see through it.
You know, there's actually a part of me that's like, you know what, do it.
Let's go ahead and just wallpaper one side of every classroom with all kinds of guidelines
from different religions.
Let's see how it plays out.
I don't think the Supreme Court is actually going to do that.
I think in all likelihood this will be struck down, even under this Supreme Court.
Because it is this Supreme Court, you do have to entertain the other options, but I don't
see this actually going anywhere.
So your most likely answer as to the motivation behind it, it's just to trick the ignorant
people of Texas that don't know no better.
That's what the Legislature there is trying to do.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}