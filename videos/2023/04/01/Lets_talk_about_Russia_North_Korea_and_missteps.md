---
title: Let's talk about Russia, North Korea, and missteps...
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=BleMDsj_0yc) |
| Published | 2023/04/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about foreign policy and its impact on countries like North Korea and Russia.
- Mentions a previous situation where North Korea faced a food shortage.
- Criticizes the lack of tangible efforts from the West to help alleviate the food shortage in North Korea.
- Raises concerns about Russia offering food to North Korea in exchange for weapons to be used in Ukraine.
- Points out the potential consequences of North Korea accepting the deal with Russia.
- Emphasizes that providing food is a cheaper and more humane option than dealing with military weapons.
- Criticizes the competitive nature of foreign policies that prioritize interests over humanitarian aid.
- Expresses disappointment in the missteps of Western foreign policy and the lack of genuine humanitarian aid.
- Speculates on the likelihood of North Korea accepting the deal with Russia due to matching equipment.
- Condemns the prioritization of competitive interests over basic humanitarian needs.
- Urges for a shift towards more cooperative and humanitarian foreign policies.

### Quotes

- "It's cheaper to be a good person. It causes less suffering."
- "Beyond America's borders do not live a lesser people."
- "Countries don't have morality, they don't have friends, they have interests."

### Oneliner

Beau explains the repercussions of prioritizing competitive foreign policies over humanitarian aid, urging for a more cooperative approach to prevent crises like North Korea's food shortage.

### Audience

Global citizens

### On-the-ground actions from transcript

- Provide real aid without strings attached to countries facing crises like food shortages (exemplified).
- Advocate for foreign policies that prioritize humanitarian aid over competitive interests (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of how foreign policy decisions can exacerbate humanitarian crises, specifically focusing on North Korea's food shortage and the implications of Russia's offer for food in exchange for weapons.

### Tags

#ForeignPolicy #HumanitarianAid #NorthKorea #Russia #Cooperation #CrisisManagement


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about foreign policy and Russia and North Korea and a
situation we talked about a few weeks ago and how things could be different and a
good illustration of why foreign policy and the way different countries pursue it
should change. So a few weeks ago we talked about how North Korea was in a
situation where there were a whole lot of signals that it was not going to
have enough food, that it wasn't going to be able to feed its people. And I put
out a video saying, hey you know I understand the foreign policy concerns
and how this might be used as leverage by the West to get them back to the
table and all of this stuff. But at the end of the day what needs to be
remembered is that these are these are the farmer in the field, the worker in
the factory, these are the people who aren't going to have food. They don't have
anything to do with the decisions being made in those countries. Now the West did
not, at least to my knowledge, I haven't seen anything saying that they tried to
use it as leverage. It looks like they didn't do anything. I can't really see
anything tangible in which they tried to alleviate the situation. But now
reporting is coming out that Russia is willing to give them food in exchange
for weapons to be used in Ukraine. So what does this mean? What's going to
happen now? Let's assume that North Korea takes this deal. So now weapons transfer
from North Korea to Russia to Ukraine. What does the West have to do now? It has
to defeat those weapons. What's more expensive? Food or all of those expensive
products used by the military? It's cheaper to be a good person. It causes
less suffering. If the world community had stepped up when the the signals
first started showing and provided food, this deal may not be happening. So now
you are going to have prolonged problems because instead of acting in a more
cooperative fashion for the sake of humanity, countries attempted to pursue
their own foreign policy interests or just didn't care, one of the two. I would
suggest it's probably a high likelihood that North Korea takes this deal. They
They have a lot of older equipment that matches what Russia is using today in Ukraine.
And I know some people are going to say, well, it's older stuff.
It doesn't matter and all of that stuff.
It's not like it's guided or anything like that.
Yeah, it doesn't matter if it's falling on your town.
This was a misstep.
This was a misstep of Western foreign policy.
And these types of situations happen more often than you might imagine.
This one just happened to occur about a topic I had talked about.
And then within short order, you can see the consequences of not remembering that beyond
America's borders do not live a lesser people.
With aid, real aid, not something that had strings attached to it, but real aid had been
offered, this situation may not be happening.
This was a misstep.
And this is why our foreign policy, and this is most countries, it's not just the U.S.
I do tend to hammer U.S. foreign policy more than most because I live here.
Most foreign policy is designed to be very competitive.
Countries don't have morality, they don't have friends, they have interests.
And when I say stuff like that, that's not me saying that's a good thing.
It's showing how people end up in a situation where they don't have enough food when it's
not necessary.
Because of an unwillingness to engage in actual humanitarian aid, the West is going to spend
far more to defeat weapons that may not have been there.
Had they done what most people would think is the right move?
At this point, the deal, as far as I know, the deal hasn't been agreed to yet, but it
seems really likely.
North Korea has a whole lot of stuff that Russia needs, and even though it is quote
obsolete. It's better than Russia not having anything from their perspective.
And Russia can provide the food and North Korea needs the food. It seems like
it's going to happen. There has also been talk about China perhaps providing
weapons to Russia. That information went out but I haven't seen anything
following up saying that they actually decided to do that. As far as the North
Korean situation, I was really hoping that China was gonna step up and just
give them the food. But that's where we're at. Expect that to hit the
headlines. Expect it to just be used to villainize North Korea. And regardless of
your opinion of North Korea, maybe you believe they should be villainized, but
understand it was an action that that led to this anyway it's just a thought
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}