---
title: Let's talk about schools, legislation, and security....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=iG52e8XqpGE) |
| Published | 2023/04/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Differentiates between prevention, security, and legislation in addressing pressing issues in the country.
- Explains the limitations of focusing solely on legislation for gun control.
- Illustrates the potential roadblocks in passing ideal gun legislation through the Supreme Court.
- Emphasizes the importance of focusing on practical measures like physical security and prevention.
- Argues that advocating for an AR ban may not be the most effective use of political capital at the moment.
- Stresses the need for realistic expectations regarding gun control measures.
- Advocates for implementing practical solutions that can have a tangible impact, such as enhancing physical security measures.
- Urges viewers to prioritize actions that can make a difference in the current landscape.
- Encourages a shift towards initiatives that can be implemented regardless of the political climate.
- Suggests that creating delays through physical security measures can be an effective approach.

### Quotes

- "Prevention and security versus legislation."
- "Calling for an AR ban is the same as saying that you send thoughts and prayers."
- "It's something, and it's something that can be done, and it's something that's going to have an impact."
- "Be very realistic in your expectations."
- "Do something. This is something."

### Oneliner

Beau explains the limitations of gun legislation and advocates for practical security measures to make a tangible impact, urging realistic expectations and immediate action.

### Audience

Advocates for Gun Reform

### On-the-ground actions from transcript

- Implement practical security measures to create delays and prevent harm (implied).
- Prioritize actions with tangible outcomes that can be implemented regardless of political obstacles (implied).
- Advocate for realistic expectations and focus on initiatives that can make a difference in the current landscape (implied).

### Whats missing in summary

The full video provides a detailed analysis of the limitations of gun legislation and the importance of prioritizing practical security measures to address pressing issues effectively.

### Tags

#GunControl #Legislation #Prevention #Security #PracticalMeasures


## Transcript
Well, howdy there internet people, it's Bo again.
So today we are going to talk about prevention
and security versus legislation.
In a recent video, I was talking about where
I thought the focus needed to be to solve a very pressing issue
in this country.
And I talked about prevention and security.
And it led to a whole bunch of commentary,
some people wondering why I didn't mention
the idea of legislation.
And some people who understood security
and understood what I meant.
But at that moment, they didn't make the connection
into what security is designed to do
and how it helps in this situation.
Talking about physical security.
OK.
So when it comes to legislation, this
is where I end up turning a lot of people's dreams
into nightmares. I want you to picture whatever your ideal gun legislation is,
okay? Whatever it is. I have a bunch of videos detailing what I think will work
and what I think won't and what I ideologically agree with and what I
don't. We don't need to go into all of that. Picture what you want, what you
think will work, what you think the best solution is. Keep that in mind. Okay,
magically, power dynamics shift. Even though the Republicans are in control of
the House, something happened and it gets through. Then it goes to the Senate and
for whatever reason, a whole bunch of people cross over in the Senate and it
If it goes to Biden and he signs it, your ideal gun control legislation is enacted.
What's the next thing that occurs?
It goes to the Supreme Court.
And it's over.
It's over.
The current Supreme Court is very pro-gun.
Whatever it is that you're picturing, it's not going to make it through it.
If I was going to direct a bunch of energy towards something, burn a bunch of political
capital, I would want to make sure that it was something that would actually get implemented.
Right now, calling for an AR ban, it's the same as saying that you send thoughts and
prayers.
It's not going to happen.
It's not going to make a difference right now.
In fact, in many ways, I think the Democratic Party is making a huge error.
Because they're burning a bunch of political capital, they are creating a bunch of single-issue
voters for something that even if by some miracle they were successful, the
Supreme Court would strike it down. And depending on what they enact, the Supreme
Court might even create a ruling that expands on what is currently on the
books, meaning they could rely on an interpretation that says the purpose of
the Second Amendment is to grant parity with military forces because that's an
interpretation that's out there and this Supreme Court is very pro-gun. I don't
think that that's, I don't think it's a worthwhile use right now. Even if you
agree with it, whatever your ideal is, it's probably not going to go anywhere.
Not right now. Not until the makeup of the Supreme Court changes. So, prevention
and security. Education about methods of prevention and physical security. When I
said security, most people interpreted that as like armed guards. You don't
need that. That's definitely not what I'm talking about. I agree with the idea
that schools should not become militarized. Talking about physical
security, and people who understand physical security, a lot of them made
the comment, and it's true. It's a true statement, but they didn't apply it to
this current situation. In most situations, corporate and private security, the goal of
security is not to stop somebody because you can't. Physical security doesn't actually
stop people. Given enough will, resources, and time, any security is something that can
be defeated. The real goal in most cases is to send them down the road. But that's not
case with this. Will, resources, and time. Time. They need time. Nashville PD just
set the standard. 13 to 14 minutes. You're trying to buy 15 minutes. Physical
security can accomplish that. And that is something that can be enacted. That's
something that you'll actually get lobbying groups helping out with because
they're gonna make money on it. You're just trying to create that delay and
you're trying to do it before the first person gets hurt. Over on the other
channel I put out a video that that goes into this in detail and it's long that's
Twine's over there, if you're interested in what can be done and can be implemented,
go take a look at it.
I'll have the link down below.
It's something that can actually go somewhere.
I don't want to wait until the makeup of the Supreme Court changes to implement things
that can have an outcome, a positive outcome.
The whole movement of people right now, when they're talking about this, it's do something,
right?
I mean, that's the phrasing on it.
Do something.
This is something.
It's something, and it's something that can be done, and it's something that's going to
have an impact.
I understand those who want to continue to advocate for it.
It's something you believe in ideologically, morally, whatever the reason is.
I get it.
don't advocate for it. I'm just saying be very realistic in your expectations of
this. Even if you win, the Supreme Court is going to say you didn't. Anyway, it's
It's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}