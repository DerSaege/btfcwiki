---
title: Let's talk about Republican solutions to Nashville....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Tclw-2jrrHI) |
| Published | 2023/04/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republicans proposed two solutions in response to recent events in Nashville.
- One solution was to label the incident as a hate crime, which Beau questions due to the lack of a known motive.
- Beau questions the effectiveness of labeling it as a hate crime since it changes charges but won't lead to a trial in this case.
- He criticizes this solution as reactive and not aimed at preventing future incidents.
- The other solution proposed is federal legislation to make such incidents a capital offense.
- Beau doubts the effectiveness of imposing the death penalty as a deterrent since many perpetrators don't expect to survive.
- He stresses the need for solutions that focus on prevention rather than post-incident reactions.
- Beau calls for solutions that can actually stop such incidents from happening, not just serve as talking points.

### Quotes

- "Investigating it in that manner does nothing. Absolutely nothing."
- "The goal should be to stop it."
- "Please make it something that would actually stop it from happening."

### Oneliner

Republicans propose reactive solutions to recent events, but Beau calls for preventative measures to stop such incidents from occurring.

### Audience

Legislators, policymakers, activists

### On-the-ground actions from transcript

- Advocate for preventative measures to address hate crimes (implied)
- Push for solutions that focus on stopping incidents before they occur (implied)

### Whats missing in summary

The full transcript provides additional context on the ineffectiveness of reactive solutions and the importance of focusing on prevention to address hate crimes effectively.

### Tags

#HateCrimes #Prevention #Legislation #PoliticalSolutions #CommunitySafety


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we are going to talk about solutions and Republicans
and the more pressing issue that is facing the country right
now.
So Republicans have put forth two main concepts, two
solutions that they think might impact everything
that's going on.
And we're going to take a look at them and see if they're
effective, if they're in good faith, if they matter and we'll just kind of weigh
it out. In the days since what happened in Nashville occurred, two solutions. One
of them is to investigate it in a certain way. You have a number of
prominent Republicans saying that what happened in Nashville should be
investigated as a hate crime. Okay, I would point out that at this point in
time, at time of filming, there really hasn't been a public indication of motive
yet. Okay, there's that. So calling for that as soon as it happened, that I mean
that's kind of suspect, but beyond that I would like to know what it would
accomplish. What is the point of investigating something in that way? What
does it do? It changes the charges and increases the sentence, right? Take as
long with that as you need to. There's not going to be a trial in this one and
investigating it in that manner does nothing. Absolutely nothing. It
accomplishes nothing. Beyond that, I would like to point out that it's reactive. It
doesn't set the stage to stop it in the future. That's something that would only
come into play after it occurs. That's not a solution. That's a sensationalized
talking point for people who haven't thought about it at all. What's the other
solution? Federal legislation to make these types of incidents a capital
offense, to impose the ultimate penalty. Again, not going to come into play here.
It won't come into play in most of them.
It's worth remembering that about half of all of the people involved in these, of all
of the perpetrators, don't make it through the incident.
So imposing that penalty doesn't really matter now, does it?
Especially when you look at the notes and papers and everything that are left by the
remaining half, most of them didn't expect to make it through. So if somebody
is already ready to go, saying that you'll face the death penalty really
doesn't do much. It's not a deterrent and once again it's reactive. It only comes
into play once it has happened. The goal should be to stop it. Again, the idea of
putting out some, you know, get tough approach that does nothing is just a
sensationalized talking point. These solutions, they don't accomplish
anything. They don't change the dynamics at all. They're all after the fact. If you
are in a position of power in this country and you're going to put forth a
solution, please make it something that would actually stop it from happening.
Not something that's just going to rile up your base. Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}