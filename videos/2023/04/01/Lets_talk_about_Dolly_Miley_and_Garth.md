---
title: Let's talk about Dolly, Miley, and Garth....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=b_iglNZKSoI) |
| Published | 2023/04/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- School in Wisconsin stops students from performing "Rainbowland" duet by Dolly Parton and Miley Cyrus, citing potential controversy around acceptance and being a good person.
- Initially, the school also planned to stop the performance of "Rainbow Connection" by the Muppets for similar reasons but later reversed that decision.
- Beau questions the attempt to cancel performances by Dolly Parton and the Muppets, suggesting a political angle.
- The Academy of Country Music Awards will be hosted by Dolly Parton and Garth Brooks, known for their acceptance and support, especially in controversial areas like marriage equality.
- Garth Brooks, in the early '90s, expressed that traditional family values include encouraging children to be the best they can be, regardless of their parents' race or sexual orientation.
- Both Dolly Parton and Garth Brooks have been allies before it became mainstream, openly supporting causes like marriage equality.
- Beau anticipates a message being sent by selecting Dolly Parton and Garth Brooks as hosts for the Academy of Country Music Awards.
- Garth Brooks has a history of advocating for marriage equality, even when it was a controversial topic.
- The selection of Dolly Parton and Garth Brooks as hosts is seen as a significant move in the country music scene, indicating a message about acceptance and equality.
- Beau wonders if the choice of hosts for the awards show and the school's decision to halt performances are related or just a coincidence.

### Quotes

- "Traditional family values was encouraging children to be the best they can be. If your parents are black and white, if your parents are the same sex, that's still traditional family values to me." - Garth Brooks
- "Both of them were allies before most of us knew what an ally was, and they have been very open about their support." - Beau

### Oneliner

A school stops students from performing songs by Dolly Parton and Miley Cyrus, while the Academy of Country Music Awards feature hosts known for their acceptance and support, sending a message of inclusivity.

### Audience

Country music fans

### On-the-ground actions from transcript

- Support and amplify artists who advocate for acceptance and equality (implied).

### Whats missing in summary

The importance of recognizing and celebrating artists who use their platform to advocate for acceptance and equality.

### Tags

#DollyParton #MileyCyrus #GarthBrooks #Acceptance #Equality #CountryMusic


## Transcript
Well, howdy there internet people, it's Bo again.
So today we are going to talk about Dolly and Miley and Garth.
Yeah, Dolly Parton, Miley Cyrus, and Garth Brooks.
We're gonna do this because a school up in Wisconsin
had some students that were going to perform a song
that was originally a duet between Dolly Parton
and Miley Cyrus.
And it's called Rainbowland.
The school stopped the performance
because it was potentially controversial.
Because it's all about acceptance and being a good
person and all that stuff.
And I guess that's just not something we can have today.
Initially, the school was also going to stop the performance
of Rainbow Connection by the Muppets for the same reason,
it being potentially controversial.
They reversed their decision on that one, apparently.
So I was looking into this and kind of doing just a little bit of research on what happened
because, I mean, yeah, Republicans trying to cancel Dolly Parton and the Muppets seemed
like it might be worth bringing up.
In the process, I ran across some other information dealing with the Academy of Country Music
Awards. They're coming up soon and their hosts for this year it's gonna be Dolly
Parton and Garth Brooks. People who are not familiar with country music probably
blank stare a big deal you know type of thing right now. People familiar with
these two performers and their views are probably raising their eyebrows. Yeah
Isn't that interesting? For those that don't know, I think most people know
about Dolly Parton and her views and how accepting she is and just her status as
an icon when it comes to being a good person. Garth Brooks, a little less so.
Most people don't know about that so I have a quote here for you. Traditional
family values was encouraging children to be the best they can be. If your
parents are black and white, if your parents are the same sex, that's still
traditional family values to me. That was Garth Brooks in the early 90s.
Both of them were allies before most of us knew what an ally was, and they
have been very open about their support. And those are going to be the two hosts
in this climate. I have never watched one of these things before. I will be
watching this one. I'm not saying that there's going to be some overt message
from these two. I don't know if there will be or not. I would give just about
anything to see them perform a duet of We Shall Be Free, but I'm gonna suggest
that selecting those two as the hosts, that is a message. That is a message.
You really aren't going to find a pairing in country music that's quite
that outspoken about this. I mean, Garth Brooks was talking about this so long
ago, talking about marriage equality so long ago. It was paired with interracial
marriage because interracial marriage was still controversial. I just don't
believe that that's an accident. I feel like there is a message being sent
here and I just hope that people receive it. So yes, there is a school in Wisconsin
that has apparently just lost it not just not just Dolly and Miley but the
Muppets were apparently at risk as well and the Academy of Country Music
definitely seems like it's setting up to send a message I don't know that it
could just be a coincidence, but that's
a pretty wild coincidence.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}