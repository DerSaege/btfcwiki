---
title: Let's talk about Trump and New York going first....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=QJ8_-qcSJoo) |
| Published | 2023/04/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Commentators are concerned about the impact of the New York case being the first on perceptions of other cases against Trump.
- Coordinating cases against Trump across the country could give the appearance of a witch hunt, even if it's not.
- There is speculation that the New York case is the weakest based on publicly available information, but the evidence is under seal.
- Prosecutors in different cases against Trump are likely to move forward at their own pace rather than taking turns.
- Social media echo chambers may amplify concerns about the New York case's outcome influencing perceptions of other cases.
- Most Americans are not likely to view the New York case's outcome as indicative of all other cases against Trump.
- Trump loyalists are expected to maintain their support regardless of the case outcomes, while undecided individuals are the ones who matter in forming opinions.
- Beau believes that the American electorate is capable of understanding that outcomes in different cases are not directly linked.

### Quotes

- "Should they have coordinated? Absolutely not."
- "Just because New York started first doesn't mean they're going to end first."
- "The American electorate in general is a little bit smarter than that."

### Oneliner

Commentators worry about New York going first in the Trump case, but outcomes won't dictate all cases; American electorate understands better.

### Audience

American voters

### On-the-ground actions from transcript

- Trust the American electorate to understand complex legal proceedings (implied)
- Focus on engaging undecided voters rather than catering to entrenched positions (implied)

### Whats missing in summary

Full details and depth of Beau's analysis on the potential impact of the New York case on public perceptions of legal proceedings against Trump.

### Tags

#Trump #LegalProceedings #NewYorkCase #AmericanElectorate #PublicPerceptions


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about the New York case
and it going first and the impacts that's going to have
on people's perceptions of the various cases
because there are a lot of commentators
who are putting out the idea that it's bad
that New York went first.
And that is, for the most part, based on the idea
that their case isn't the strongest and the concern that is being expressed is
that because maybe this one doesn't go the way people want it to, that it will
undermine some of the other cases. So the message that prompted this, what do you
think about what is being said? The idea that Americans will see all the cases as
witch hunts if the New York case fails? Should they have coordinated? First, let's
answer that last question. Should they have coordinated? Absolutely not. If there
was evidence that various prosecutors around the country actually coordinated
their cases against Trump, that would actually look like a witch hunt, whether
or not it actually was. The idea that they should have like lined up or
whatever, that's not actually a good idea. As far as the idea that New York going
first is bad, it's based on a couple of a couple of theories that people have.
First, the New York case is the weakest. And when it comes to that, I mean I've
expressed that based on what's publicly available, I think it's the weakest.
However, we don't actually know what the evidence is. It's under seal. We don't
know that it's the weakest. Based on what's publicly available, it looks like
the weakest. However, what is publicly available is like 1% of what they have.
So, we don't actually know. Now, the other part of this is the idea that they
shouldn't have gone first, okay? That, I mean, that's the whole basis of the
statement. That's based on the idea that the prosecutors in these cases are going
take turns. It is far more likely that this case in New York actually takes
some time and during that period the other cases also move forward. It's not
like they're gonna hold off on indicting Trump in let's say Georgia or you know
the Fed's doing it or whatever because he's looking at a case in New York.
They're not going to take turns like that. They're each moving ahead at their
own pace. It is entirely likely that before the New York case even goes to
trial that there are more indictments and those cases may even move faster.
Just because New York started first doesn't mean they're going to end first.
And then the third reason people believe this is social media echo chambers.
They're looking at those people who support Trump no matter what, and they're looking at the noise
they're making online and saying, you know, if this doesn't work, it's not going to convince those
people. Yeah, it's not going to convince them anyway. The majority of Americans are not
going to say just because what New York did didn't pan out, and again this is assuming
that it doesn't pan out, that all of the other ones won't either. They're not going to look
at it as a totality. They're not going to look at it and say that one case has something
to do with the other. Most Americans are far more understanding of how that works than
the commentators are giving them credit for. I think there's a severe underestimation of
American electorate here. I don't believe that Americans are going to say that
something that happened on the other side of the country is somehow
undermined by what goes on in New York. I don't see that as a realistic concern. I
think that's based off of the response from Trump loyalists and understand even
if Trump is convicted in every single case that that he's being investigated
for, his loyalists are still going to support him. A lot of them will. Some, as
more evidence comes out, it might influence their loyalty, but for the
most part, those who at this point are still out there waving Trump flags and
saying, no, it's them who are out to get them, I don't feel like much is going to
change their mind and I don't think that anything should be catered to try to
shift them. The people that matter here are those who are undecided, those who
don't have a strong opinion. Those are the people that matter. And I feel like
those who have withheld judgment are not going to base their opinion of the
Georgia case or the documents case on what happens in New York. I think it's a
bad, it's a bad train of thought that originates with a pretty elitist attitude
and assuming that those, you know, us commoners can't understand that things
aren't related. I think that the American electorate in general is a little bit
smarter than that.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}