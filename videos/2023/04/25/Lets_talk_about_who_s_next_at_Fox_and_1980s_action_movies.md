---
title: Let's talk about who's next at Fox and 1980s action movies....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=gxGi9YuAEbU) |
| Published | 2023/04/25 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Speculation is rife about who's next at Fox, with Tucker Carlson's departure sparking questions and predictions.
- Rupert Murdoch's move to assert control over his "pet project" by removing those who may not agree with planned changes.
- The recent developments at Fox may serve as a warning to others at the network.
- Expectations for changes at Fox include potentially halting the promotion of blatantly false narratives.
- Despite potential changes, Fox is not likely to transform into a beacon of journalistic ethics.
- Divisions within Fox between the news and entertainment sides may impact the handling of factually inaccurate information.
- Tucker Carlson's future post-Fox is uncertain, with potential challenges due to his rhetoric and past legal issues.
- Carlson's next steps could involve smaller outlets or even international offers like RT.
- Carlson's departure may not significantly impact his loyal audience deeply entrenched in an echo chamber.

### Quotes

- "Who goes next depends on who doesn't listen to the new boss, same as the old boss."
- "Okay, don't expect it to become some bastion of journalistic ethics."
- "Don't go away mad, just go away."
- "Those people who would follow him, they're already so far down that echo chamber, it doesn't matter."
- "Anyway, it's just a thought."

### Oneliner

Speculation abounds at Fox after Tucker Carlson's exit, signaling potential changes and maintaining skepticism about the network's ethical standards and future narrative.

### Audience

Media Consumers

### On-the-ground actions from transcript

- Stay informed about developments at Fox and hold the network accountable for promoting truthful narratives (implied).

### Whats missing in summary

Insights on the dynamics of media influence and audience loyalty in the face of network changes.

### Tags

#Fox #TuckerCarlson #Media #Journalism #Narrative


## Transcript
Well howdy there internet people, it's Bo again. So today we are going to talk
about Fox and who's next and we're gonna talk about what we can all learn from
1980s action films. That's the big question right now. Who is next to go at
Fox? It probably makes up 40% of my inbox and there are people all over social
media, there are political commentators, everybody's making their predictions.
I have a different view of this.
There are old movies and there's a cliche in it.
The bad guy walks in, who's in charge here?
Somebody says, I am, raises their hand, bang, who's in charge here?
You are.
That's what Rupert Murdoch just did.
Who goes next depends on who doesn't listen to the new boss, same as the old boss.
A lot of people rightfully pointed out that Tucker seemed to view himself as Fox.
He was the most important thing.
Everything was about him.
is the view a lot of people were kind of casting when it comes to the way he
behaved. I don't know that personally, I don't know the guy, but that's the image
that most people portrayed of him. If you are somebody who is wanting to reassert
control of your pet project and you know one person in particular is viewed as
the de facto boss and that person may not agree with all of the changes you
have planned, what do you do? You make sure they're not there anymore and you
do it in a surprising way. So what just happened? Who I think next is whoever
crosses Murdoch next? I think this might have been a little bit more of a warning
to other people in the way that it was done. Now as information has come out of
course it had nothing to do with the massive lawsuit. It had to do with
something else. Sure, whatever. I believe that. That could be true. I mean, it is Fox.
It's not like they have a reputation for being less than accurate or anything, but
I do think that even if that was true in the way that it was done, it was done to
send a message. It was done to send a message to every personality on Fox
that people are saying is next. That was telling Hannity, hey there's gonna be
some changes and you're gonna be okay with them. And then that's probably
another 10% of the inbox. What kind of changes to expect at Fox? Okay, don't
expect it to become some bastion of journalistic ethics. That's not gonna
to happen. Best case scenario in my opinion is that they absolutely stop
promoting blatantly false narratives. That's what I'm hoping for. I don't
think the rhetoric is going to be... I don't think that's going to decrease. It will
probably be just as inflammatory, but they will put in more checks to make
certain that there won't be any blatantly false information. There may be
cases where they still go for plausible deniability. Oh, we didn't know. We were
doing the best that we could. But I think that the quote news side of the
organization, if you don't know, Fox kind of has themselves self-divided into the
news side and the entertainment side I guess. I think the news side might have a
little bit more influence over being able to say hey this is factually
inaccurate but I think it would have to be pretty blatant stuff. Stuff like you
know, who won an election. I wouldn't expect, you know, real fact-checking to
suddenly begin or anything like that. It's still going to be Fox. Another thing
that has come up is what about Tucker himself? What is he going to do? I don't
care. I don't. Don't go away mad, just go away. I don't care where he goes. You know,
I'm sure that there is some golden parachute that he's getting out of this, but at the
end of it. He's not on Fox anymore. The rhetoric that he put out, it's not going
to be there coming from him. People are going to saying that, well, he's just
going to go to some other outlet. I mean, okay, sure, do that. It's going to be
smaller. They probably do not have the funds to insure him the way that Fox did.
He could go out on his own, but then you run into that same insurance issue. A lot
of personalities at Fox, they're gonna have a hard time finding a job somewhere
else. Because the companies that insure other companies for false statements,
bringing somebody that was, you know, maybe part of a $787 million settlement,
that might raise rates a little bit. And sure, for some smaller companies, they
may, they may take the risk and maybe they'll be fine. But he won't have the
audience, he won't have the sway. Those people who would follow him, they're
already so far down that echo chamber, it doesn't matter. As far as I know, the
only public offer that he's gotten so far came from RT. That's not a joke, they
actually tweeted something to the effect of, hey you can still come here and
challenge the story or something like that. Maybe they were just inviting him
a show. Maybe they weren't offering to hire them, but I bet they would. Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}