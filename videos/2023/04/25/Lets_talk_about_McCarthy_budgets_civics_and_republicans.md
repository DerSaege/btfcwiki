---
title: Let's talk about McCarthy, budgets, civics, and republicans....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=SoR0TqN3n24) |
| Published | 2023/04/25 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- McCarthy's budget is up for a vote this week, but some Republicans have raised concerns and may not vote for it.
- The issue isn't about how the budget cuts will impact Republican supporters but the lack of plans to balance the budget within 10 years.
- Beau points out that the idea of balancing the budget within a certain timeframe in the current Congress does not hold weight for future Congresses.
- He expresses concern that some Republican lawmakers may lack a basic understanding of civics and the powers of Congress.
- Beau stresses the importance of electing officials who understand their roles and limitations within the government.
- He calls for the Republican Party to address these fundamental gaps in knowledge among its members.
- Beau underlines the need for voters to be aware of these issues and not be misled by false promises or claims in budgets.
- He uses the example of writing in caps on future spending, stating that such provisions are imaginary and hold no real power.
- Beau urges for a better understanding of how government functions and the limitations on current Congress to bind future Congresses.
- He concludes by calling for the Republican Party to improve on various levels, including the need for a better grasp of governmental functions.

### Quotes

- "The Republican Party has to get its act together."
- "You are electing people that literally could not write a job description for what they're up there to do."
- "There is nothing that this Congress can do to limit the actions of a future Congress in that way."
- "The Republican party needs to get its act together on so many levels."
- "They're going to say, oh, well, we capped future spending. No, they didn't."

### Oneliner

Beau stresses the critical need for Republican lawmakers to understand basic civics and the limitations of current Congress on future actions.

### Audience

Voters, Republican Party members

### On-the-ground actions from transcript

- Educate yourself and others on the functions and limitations of Congress (implied)
- Hold lawmakers accountable for their understanding of basic civics (implied)
- Ensure that elected officials have a comprehensive grasp of their roles and responsibilities (implied)

### Whats missing in summary

The full transcript provides a detailed insight into the lack of understanding among some Republican lawmakers regarding basic civics and governmental functions.

### Tags

#RepublicanParty #Budget #Civics #Congress #Government


## Transcript
Well, howdy there, internet people.
It's Bill again.
So today we are going to talk about McCarthy,
the budget, the debt ceiling, other Republicans,
basic civics, and an interesting dynamic.
A dynamic that has developed and it is interesting,
but to be honest, it makes me a little bit uncomfortable
and it has definitely altered my perception of some things.
So what's going on?
The Republican Speaker of the House,
McCarthy has said his budget, it's going for a vote this week.
Other Republicans have indicated they have issues, questions, comments, concerns with
this budget, some going as far as suggesting they may not vote for it.
By my count, at time of filming, he's going to need congressional approval and he doesn't
have the votes.
Now he is Speaker of the House, so he can put some pressure, make some backroom deals,
make a lot of promises, and he can probably shore up the votes.
It's not a huge gap.
It's embarrassing for him to be in that situation as Speaker, but that's not the dynamic I want
to talk about.
I want to talk about why some Republicans have questions, comments, and concerns.
You would think that it's because they understand that the budget cuts that are actually in
it, they impact their voters, their base, their supporters, their constituents.
This budget will actively harm Republican supporters.
You would think that would be the reason they have issues with it.
But for some of them, that's not it.
That's not the question.
The real issue, apparently, is that there's nothing in this budget that suggests they
plan on basically balancing the budget within 10 years.
If you are a staffer for a congressperson that holds this position, please listen closely.
That's not how this works.
That's not a thing.
It doesn't matter.
That isn't a clause that carries any weight whatsoever.
I mentioned this in another video recently, and we'll come back to it, but there is no
law that a current Congress can enact that is so heavy a future Congress cannot lift.
That's not a thing.
Sure, you can put the words in there.
We're going to put a cap and we're going to balance the budget within 10 years.
great if the next congress decides they want to spend 12 years worth of federal budget in six
months. All they have to do when they approve it is remove the language you included. This year that's
it. It's not real. There are multiple things in McCarthy's budget that are like this, like the caps
on future spending and stuff like that. It's imaginary. It doesn't carry any weight whatsoever.
All it does is make a future Congress write a few extra words in their bill. It doesn't matter.
I was operating under the assumption that Republican lawmakers knew this. They understood this.
And this was just them banking on the fact that they're, you know, super patriotic,
pro-America, pro-constitution base was too ignorant of civics to know that it
didn't matter. After seeing Republicans literally talk
about withholding their vote over this, it makes me think that there are
Republican lawmakers who do not know basic civics, who do not
know the function of the Congress that they serve in, do not know the
power of the Congress that they serve in. That is unnerving.
The Republican Party has to get its act together. Set aside all rhetoric, all
policy, everything like that. You are electing people that literally
could not write a job description for what they're up there to do.
They have no idea what their powers or limitations
are. You cannot say, oh, I support the Republic.
When the people you elect have no clue how the government works, if you hear your representative
talking about, well, there's nothing about a balanced budget within 10 years, just assume
that they have no clue how the government functions because there is nothing that this
This Congress can do to limit the actions of a future Congress in that way.
That's not a thing.
You need a constitutional amendment limiting the power of Congress to do this.
It's not something they can include in a budget.
But apparently, there are some in the Republican Party who don't understand that.
I'm assuming there are also a lot of normal rank-and-file Republicans who don't know that.
You need to know that because part of the budget is banking on you not knowing that.
They're going to say, oh, well, we capped future spending.
No, they didn't.
They didn't.
They wrote it in, but it means nothing.
They have, I mean, it's like them writing in, it can no longer rain in DC.
They have no power over that.
The Republican party needs to get its act together on so many levels, and this is one
of them.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}