---
date: 2023-05-03 03:48:58.957000+00:00
dateCreated: 2023-04-25 03:07:42.632000+00:00
description: Recent reports of Tucker Carlson's departure from Fox News and reflects on the implications and changes happening at the network. It emphasizes the importance of not succumbing to despair and maintaining hope for change.
editor: markdown
published: true
tags: news, fox news, tucker carlson, media, hope, despair, accountability, election, dominion, gerrymandering, single video easter egg, change
title: Let's talk about Tucker and what we can learn....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=mcaeNhQZqFM) |
| Published | 2023/04/25 |
| Theme     | Change and Hope |
| Status    | article incomplete |

## Human Summary
Beau talks about the recent reports of Tucker Carlson's departure from Fox News and reflects on the implications and changes happening at the network. It emphasizes the importance of not succumbing to despair and maintaining hope for change.

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Tucker Carlson is reportedly no longer at Fox, signaling changes happening at the network.
- The wealthy in the country have a track record of avoiding accountability, leading to skepticism about consequences for Fox.
- Even after settling for $787 million, comments dismissed the impact, believing there wouldn't be changes at Fox.
- Despite uncertainties about the reasons behind Tucker's departure, changes are evident at Fox.
- Tucker was seen as untouchable, but recent events have proven otherwise.
- Right-wing outlets thrive on fear-mongering, while despair is a common tactic for those seeking change.
- Despair-mongering can be self-defeating, as it discourages action and perpetuates a sense of hopelessness.
- Hope is emphasized as a driving force for engagement, motivation, and instigating change.
- The lesson from recent events is that change is inevitable and can be sparked by even small entities like Dominion challenging Fox.
- Despite uncertainties about future changes at Fox, hope for positive transformation is encouraged.

### Quotes

- "Hope is what keeps people engaged. Hope is what keeps people motivated. It's what keeps them in the fight. It is what causes change."
- "Despair is a good business model but it's not good for any long-term movement."
- "Whether you think you can or you think you can't, if you're talking about the majority of people in this country, if the majority of people think that nothing can be done, they're right."
- "Putting out the kind of content that says we can't win, it is literally self-defeating."
- "Change is coming. It's the one thing you can't stop."

### Oneliner

Tucker Carlson reportedly leaving Fox signifies potential shifts at the network; beware of despair-mongering and embrace hope for driving change.

### Audience

Media consumers

### On-the-ground actions from transcript

- Support media outlets that prioritize hope and motivate positive change (suggested)
- Engage in constructive discourse and actions that foster hope and drive positive transformations (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the recent developments at Fox News surrounding Tucker Carlson's departure and encourages viewers to maintain hope for driving positive change despite challenges.

### Tags

#Media #Hope #Despair #Change #Accountability


## Transcript
Well, howdy there Internet people, Beau again.

So today we're gonna talk about Tucker Carlson and what we can learn from it. What has happened and what we can learn from the chain of events.

If you have missed it, reporting suggests that Tucker Carlson is no longer at Fox. I think the implications of that are pretty clear. I don't think we need to spend a whole lot of time going over that, but there is a something that can be learned.

It wasn't too long ago when a video would go out about any attempt to hold Fox accountable, a really common comment underneath in the comment section was, "and then nothing happened." And you can kind of be forgiven for having that opinion. The wealthier people in this country, the track record of them avoiding accountability, it's pretty big.

But that comment "and then nothing happened." and then as time went on, once suits got filed and stuff like that, it became "Fox is gonna win." and then it became "Fox doesn't care." and then it became "Fox has unlimited amount of money." and then even when it got to the point of them settling for $787 million, even factoring in any potential tax break, they're out half a billion dollars. The common comment was "it doesn't matter, that there wouldn't be changes."

Okay. I feel like there have been because I would like to point out that Dan is also out over there. I don't know that that one is related though, but there are definitely changes happening at Fox. Do we know that they're linked to the suits? No, we don't. And in fact, even if they were, Fox is probably gonna do everything within their power to make sure that that never comes to light. But the changes are
happening.

I don't think that any people would have given much support to the hope that Tucker would become the fallen one[^1], would fall from grace over at Fox. He seemed untouchable. And a whole lot of liberal and some left leaning outlets really leaned into that.

We often talk about right-wing outlets and how their coverage is fear, fear, fear, fear, fear. It's fear-mongering, constantly trying to terrify people to get them on their side. It's a good business model for them. For those who want change, if you were trying to appeal to them, a good business model is despair, despair, despair, despair. Because the people who want change, they want coverage of it and they want somebody sympathetic to them. And despair is a whole lot easier to sell than the alternative.

See, if you convince people to just give up, that's an easy sell because then they don't have to do anything. They don't have to get involved. They don't have to get involved. You have absolved them of that. "Nothing's gonna matter and then nothing changed." It's a good business model but it's not good for any long-term movement. It's actually a type of propaganda that militaries put out during wartime to their opposition. "You can't win."

Normally this occurs in liberal media. It does happen sometimes in left media, if we're going to draw that distinction. Generally, in the left, it is more... it comes mostly from people who are heavily focused on climate change. That I get. That I actually understand because we're not stopping it. We're mitigating it. And there are people who say that even that can't be done. I get it. I get what's being said. But when it comes to that kind of despair-mongering, just remember whether you think you can or
you think you can't, if you're talking about the majority of people in this country, if the majority of people think that nothing can be done, they're right.

And if the majority of people think that something can be done, they're right.

Putting out the kind of content that says we can't win, it is literally self-defeating. As much as we caution people about falling into fear-mongering right-wing echo chambers, do not fall into despair. 

Hope is what keeps people engaged. Hope is what keeps people motivated. It's what keeps them in the fight. It is what causes change.

If the companies who are seeking to hold Fox accountable, if they had listened to the commentators pundits who as recently as yesterday were saying there's no way they could win even after they got a 787 million dollar settlement they might never have filed. If they believed that they might never even tried.

There are a whole lot of people who have fallen victim to this and you see it in a bunch of different ways. When you hear people talk about gerrymandering, yeah well it doesn't do me any good to vote. I live in a solidly red state. In some cases that may actually be true, but in most cases gerrymandered districts, it's only a couple of points. You just have to create unlikely voters. You have to increase voter turnout and you can overcome that gerrymandering in a whole lot of districts. Yes there are some that that can't be done in but the overall point here the overall lesson is that Dominion a pretty tiny company has dealt a major blow to Fox and it appears because again we don't have confirmation as to why Fox is out and my guess is it will when the statements come out it will be anything other than something to do with the election because that's that's more press that Fox just does not need and it would benefit Tucker so they would probably both agree just not to mention that part.

There are changes at Fox and this is a big one. It's a big one. Now again, what kind of changes? We don't know yet. I mean they could replace Fox or Fox could replace Tucker with somebody even worse as far as rhetoric. Maybe. Don't know. We'll have to wait and see. But change is coming. It's the one thing you can't stop. And if you are somebody who wants that change, don't lose hope that it can happen because it can.

Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
Grey with dark capital lettering in a Trivia Slab Bold like font on two rows:
```
MORE HOPE
MORE HUMANITY
```
## Easter Eggs on Shelf
FALL1N1 California yellow on black plate referencing Tucker Carlson's departure from Fox. It only appeared on this video.
{{EasterEgg}}

[^1]: Speech reference to FALL1N1 CA plate.