---
title: Let's talk about evil in a religious context....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=uPbxWyg1Q_c) |
| Published | 2023/04/21 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Receives a message criticizing his support for the LGBTQ community, suggesting he find Jesus Christ and stop "deceiving people" by supporting evil.
- Acknowledges the religious aspect of the criticism, stating he usually doesn't share his spiritual beliefs on the channel.
- Explores the concept of evil in a Christian context as the opposite of love, which is described as following the two greatest commandments of loving God and loving your neighbor as yourself.
- Responds to the accusation that the LGBTQ community has divided the country and hurt families by sharing his perspective that they are simply trying to be true to themselves.
- Suggests that divisions occur because some Christians fail to follow the principle of loving their neighbor, leading to rejection and alienation.
- Addresses the criticism that any negative comments towards the LGBTQ community are labeled as hate, pointing out that genuine love and acceptance should be the focus.
- Questions why individuals claim the LGBTQ community forces their views when in reality, they are just trying to be authentic.
- Challenges the notion of a "gay agenda," arguing that people should be allowed to live their lives without judgment or hate.
- Encourages embracing love over hate, especially for those who have chosen a Christian path.

### Quotes

- "Love is good. Hate is evil."
- "I see it as people who should be trying to love their neighbor, pushing them away, dividing from them."
- "It's hate. It's hate."
- "You chose to be a Christian. That means you gotta let the hate go."
- "It's easier to sell judgment. It's easier to sell hate. Because it's easy. It is much harder to love."

### Oneliner

Beau tackles criticism of his support for the LGBTQ community, delving into the concept of love, divisions, and the importance of embracing love over hate, especially within Christian beliefs.

### Audience

Christians, LGBTQ allies

### On-the-ground actions from transcript

- Challenge yourself to actively show love and acceptance towards all individuals in your community (exemplified).
- Refrain from passing judgment and instead focus on understanding and supporting others (implied).

### Whats missing in summary

A deeper dive into the dynamics of religious beliefs, acceptance, and the challenges of embracing love over hate in today's society.

### Tags

#Religion #LoveOverHate #LGBTQSupport #Christianity #CommunityRelations


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about something we normally
don't talk about on this channel.
But we are going to talk about that.
We're going to talk about the two biggest rules.
We're going to talk about what it takes to break those rules.
We're going to talk about love.
Talk about a lot of things.
And we're going to do this because I've got a message, two
messages by the end of it.
But as soon as I saw the first one, I was like,
that's a video.
Even though it's not something we normally talk about,
that's a video.
And the whole video just played in my mind.
And it was so complete, I actually
responded to the person saying, thanks for the video topic.
And they responded to it, which is good.
It made it better, because I'm not straw manning now.
I'm not guessing at their view that they wrote it out.
So we're going to go through that.
Fair warning for those who don't want to hear any of it,
there will be discussions of religion in this one, which
is not normal for this channel.
But I think this is important.
The message came in regarding the LGBTQ community and my support of them.
Bo, you make good videos and you're spot on in a lot of your takes, but I really hope
you find Jesus Christ and stop all caps deceiving people and pretending like evil is good.
Evil.
strong word. I don't talk about my religious or spiritual beliefs on this
channel. I don't think it's appropriate. The channel is not about me, it's about
ideas and events, but evil. I have read most religious texts, have a pretty good
understanding of them. My grandfather was a deacon. I've been in a church before.
The idea of evil, you want to talk about evil in a religious context in particular?
Oh, I'm your guy.
What is evil in a Christian context?
It's the opposite of good, right?
It's the opposite of good.
What's good?
two biggest rules, two greatest commandments. If you don't know, it's Matthew 22, 36, I think.
Just say 30, you're supposed to read above and below. Jesus is asked what the greatest
commandment is, and he says, you know, loving God. And the second greatest commandment is loving
loving your neighbor as yourself.
Following that, that's good.
In a Christian context, the opposite of that would be evil.
Love is good.
Hate is evil.
And I was going to go on from here, you know, the rest of this video was going to occur,
And it wouldn't have been as good because I didn't have the reply.
After saying, thanks for the video topic, they said, you're welcome.
You should also research how that community has divided the country and hurt so many families,
yet that community is never held accountable.
Any type of criticism that's aimed at them is always viewed as hate.
Why is that?
Yeah, why is that?
That's a good question.
Why is that community allowed to hate when they're always forcing their views on other
people?
Yet it's wrong when people hate them.
There is an obvious agenda and double standard there.
Even you have to admit that.
Big fan by the way.
We'll see how the rest of that last sentence holds up.
There's a lot in this and I'm going to try to go through all of it.
That community has divided the country and hurt so many families.
Have they though?
Because that's not the way I see it.
That has not been my experience.
My experience has generally been something else.
You know, when people become Christian, most of them remember it.
Most people can remember the moment they became saved, right?
A lot of people had a baptism and they remember that.
It's a big day.
What is it?
And I'm not talking about the mechanics of being dunked under the water.
I'm not talking about the symbolism of the watering, anything like that.
What is it really?
When you're describing the event, person being baptized up there, making a public
declaration in front of their friends and family of who they are, who they have become.
And that's what being saved is, right?
becoming who you are supposed to be as a Christian. People in that community, they
have something really similar. They have something really similar. Where they come
out and they say who they have become. They announce who they have become. And
And do you know what their biggest fear is in the time leading up to it?
It's that their family, their community won't love and accept them.
I would suggest that the divisions occur because Christians aren't following the two greatest
commandments.
I'm willing to bet that if you were going to sever ties with your kid because they're
gay, that you're probably not loving your neighbor either.
I don't see it as them dividing the country.
I see it as people who should be trying to love their neighbor, pushing them away, dividing
from them.
The message goes on, any criticism that's aimed at them is always viewed as hate.
Why is that?
It's certainly not love.
And when you're talking about the criticism that comes in about them, how is it described?
Like I know when people are talking directly to them, most of them are trying to church
it up.
They're trying to be nice in a lot of cases.
Some people aren't.
And that's obviously hateful.
And even those people who question why it's viewed as hate, why that criticism is viewed
as hate, have no problem sending a message that says stop deceiving people and pretending
like evil is good, evil.
That's probably why it's viewed as hate.
Because even when it's cloaked, most times it's pretty clear that it's hate.
Now obviously this is not all Christians, but the people who tend to bring this up the
most, it's pretty hateful most times.
Greatest commandments.
Why is that community allowed to hate when they're always forcing their views on other
people?
Stop there before we finish with the rest of that sentence.
Are they though?
You know, I've never in my entire life gotten a message saying, hey, I wish you were gay.
I hope you become gay so you're not evil anymore.
I have gotten some that say, I really hope you find Jesus Christ and stop deceiving people
pretending like evil was a good thing.
A lot of these.
A lot of these.
I don't think they are forcing their views.
I think they're just trying to be them.
Now, I understand that from a Christian context,
you're going to say that what they're doing is a sin.
I got that.
I would first point out that that's you forcing your views.
But I would also point out is that in Christianity, aren't we all sinners?
Isn't that like a major component?
And believe me, I know somebody's going to say, you know, love the sinner and hate the
Yeah, I have heard all of the religious platitudes, but the reality is that it's
not aimed at some... at some behavior. It's aimed at them as people. It's hate. It's
hate. There are Christian pastors who are very open to that community, who are very
open to that community. They manage to follow the greatest commandments and
not be hateful. It doesn't require it. You can still be loving. And then it goes
on, so the whole sentence, why is that community allowed to hate when they're
always forcing their views on other people, yet it's wrong when people hate
them. Again, I want to point out that at this point, you're saying it's hate.
You're acknowledging that. People hate them. Not their sin. Them. Breaking the
greatest commandments. But more importantly, why is it wrong for you as
a Christian to hate them. Because you're a Christian. Those are self-imposed rules.
You chose to be a Christian. That means you gotta let the hate go. That's what the book
says. I know that there are religious leaders that present things in a different way. I'm
pretty sure that the book actually talks about that too. You should read the book. You should
actually kind of go through and pay attention to what Jesus said was
important. There's an obvious agenda and double standard here. Even you have to
admit that? The gay agenda? I don't think there is a gay agenda. I think it's people
trying to live their lives. There is a double standard. There is a double standard, though.
Because you chose to be a Christian. You should be embracing love because of that. That's
That's the standard you chose, you set for yourself.
Not everybody is.
And they're not all going to live up to that standard because they're not Christian.
And you can't impose your views on other people, right?
So I'll admit to that last part, there's a double standard, but it's not for the reason
you think. One of the big problems with Christianity, in my opinion, and why those
pews are less and less full, and why you have less and less people identifying
that way is because Christianity in general, there's a whole lot of churches
that do not go by the book, and maybe they should. It's easier to sell judgment.
it's easier to sell hate. Because it's easy. It is much harder to love. But
that's what you're supposed to do. So when it comes to this particular topic, the
religious arguments, they tend not to go very far with me, especially when the aim
of it seems to be to get me to stop showing love towards a group of people,
towards my neighbors. That doesn't sit well with me. Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}