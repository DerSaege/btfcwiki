---
title: Let's talk about Chief Justice Robert being invited to the Senate....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=4IJMcwHSFSw) |
| Published | 2023/04/21 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Chief Justice Roberts invited to talk to Senate Judiciary Committee about ethics at the Supreme Court.
- Democratic Party pushing for an investigation into Justice Thomas's gifts and financial arrangements.
- Senate Judiciary Committee Chair invited Roberts to talk, but lacks votes for a subpoena due to Senator Feinstein's absence.
- Republican Party showing disinterest in pursuing a subpoena for ethics investigation.
- Calls for transparency in ethics standards for the highest court in the land.
- Republicans not showing interest in getting to the bottom of the situation raises concerns.
- Suggestions that Republicans should clarify the ethics issues surrounding Justice Thomas.
- Lack of Republican support for transparency raises doubts among voters.
- Pressure building on Chief Justice Roberts to address legitimacy concerns at the Supreme Court.
- Hopeful for Chief Justice Roberts to address the ethics issues facing the Supreme Court.

### Quotes

- "Republicans apparently don't care or aren't willing to simply ask and get to the bottom of it."
- "The fact that the Republican Party doesn't seem interested in this should be a warning sign to a whole lot of people."
- "The fact that they're apparently going to assist in stonewalling, that should say a lot to undecided voters."
- "I am hopeful that the Chief Justice understands the legitimacy issues that the Supreme Court is facing."
- "It's just a thought, y'all have a good day."

### Oneliner

Chief Justice Roberts invited to address ethics at Supreme Court amid Republican disinterest, raising concerns about transparency and legitimacy.

### Audience

Concerned citizens, activists.

### On-the-ground actions from transcript

- Contact your representatives to demand transparency and accountability in the Supreme Court (suggested).
- Organize or participate in local campaigns advocating for ethical standards in the judiciary (implied).

### Whats missing in summary

The full transcript provides a nuanced analysis of the political dynamics surrounding calls for transparency and accountability within the Supreme Court.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Chief Justice Roberts and an invitation to come
talk to the Senate Judiciary Committee about ethics at the Supreme Court and what's likely
to happen there.
So as we have talked about before, the Democratic Party has basically been like, you need to
look into this or we will when talking to Chief Justice Roberts.
And it's been about the situation with Justice Thomas and the gifts and financial arrangements
that have been discovered and reported on.
So it's an invitation, that's the thing, it's not a subpoena.
The Senate Judiciary Committee Chair Dick Durbin has invited, asked for him to come
talk but they don't have the votes for a subpoena because of Senator Feinstein's absence.
Republican Party has indicated that they're not really interested in helping
get a subpoena to find out about this. In fact, Lindsey Graham kind of said that if
he doesn't want to come, well, I would respect his decision or something along
those lines. That seems like it should be a talking point. I think that most
Americans, particularly those who are not hyper-partisan, would like to know what
the ethics standards for the highest court in the land are. And I think that
they might be interested in knowing that Republicans apparently don't care or
aren't willing to simply ask and get to the bottom of it. That seems like a
That seems like a point that the Democratic Party should be repeatedly
making because without Republican help or Feinstein's return, they don't have
the votes. They can't get the subpoena. It's literally just asking, hey will you
come talk to us? The fact that the Republican Party doesn't seem interested
in this should be a warning sign to a whole lot of people.
With the Supreme Court having the legitimacy concerns that it does, it seems like it would
be very beneficial for Republicans to set the record straight and say, no, he was advised
that the vacations were OK, and the private jet,
and the whole property situation,
there are reasonable explanations for that.
It seems like it would be high on their agenda.
The fact that they're apparently going
to assist in stonewalling, that should say a lot
to undecided voters.
That should say a lot to the American people in general.
But at this moment, the pressure is still building.
However, without the votes for subpoena,
the Judiciary Committee doesn't have any teeth.
So we're going to have to wait and see what Roberts decides.
I'm hopeful.
I don't know, I would go as far as to say optimistic,
but I am hopeful that the Chief Justice understands
the legitimacy issues that the Supreme Court is facing
and is willing to come talk about it.
Seems kind of like a long shot, but there is a chance.
So we'll have to wait and see what the Chief Justice decides
and then see how the American people react
the Republican Party apparently not caring about ethical considerations with the Supreme
Court.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}