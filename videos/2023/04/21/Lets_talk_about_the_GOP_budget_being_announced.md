---
title: Let's talk about the GOP budget being announced....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Rjahp7N42B0) |
| Published | 2023/04/21 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing the Republican budget plan coming out of the House and deconstructs its components.
- The budget includes items for negotiation, unlikely proposals, and cuts that may happen.
- Some elements, like canceling Biden's key legislation and student debt relief, are unlikely to make it into the final package.
- Two interesting proposals involve returning discretionary spending to 2022 levels and capping budget growth at 1% for the next decade, which Beau deems as insignificant.
- Beau criticizes the budget for not addressing significant areas like defense or Social Security.
- The plan includes reclaiming unspent COVID relief funds and cutting the 2024 budget for a total reduction of $221 billion.
- Beau sees the budget show as a facade, with proposed cuts being minimal and easily reversible.
- The deal raises the debt ceiling by $1.5 trillion, rendering any actual cuts temporary and insignificant.
- Beau predicts Biden will likely concede to the budget plan as the key elements are not binding on future Congresses.
- Despite the budget's flaws, Beau acknowledges that there is still room for negotiation and adjustments.

### Quotes

- "It has been shown to be a show and nothing more."
- "It's words that are put on the paper, but no future Congress actually has to abide by them."
- "Even the stuff that is an actual cut, it's not really either because it can be redone in a very short period of time."

### Oneliner

Beau dismantles the Republican budget plan, revealing its superficial promises and insignificant cuts, ultimately deeming it a political show.

### Audience

Budget analysts, political activists

### On-the-ground actions from transcript

- Contact local representatives to push for more substantial budget cuts and allocation adjustments (implied)
- Organize community dialogues on budget priorities and advocate for proper funding in critical areas (implied)

### Whats missing in summary

Insights on how constituents can hold lawmakers accountable for budget decisions.

### Tags

#RepublicanBudget #PoliticalAnalysis #BudgetCuts #GovernmentSpending #DebtCeiling


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about
the Republican budget plan coming out of the House
because they put it out finally.
Since this whole show started, I have called it a show
and talked about the promises they made
and how they weren't gonna be able to keep them
and so on and so forth.
did not have high hopes for the Republican budget.
Yeah, so what's in it?
That's what people want to know.
There are a number of things that
are put in there to be given away at negotiations.
There are some things that just are not going to happen.
There are some things that literally do not matter.
And then there actually are some cuts.
And some of this may actually happen.
But it is nowhere near what they promised,
even though they may be able to make it look like they achieved
what they promised.
OK.
So the stuff that just is not going to happen
is stuff like getting rid of Biden's key legislation when
it comes to like green energy, the cuts to the IRS,
stuff like that. That's not, no. Ridiculously unlikely that that makes it into any final
package. There's a bit about canceling Biden's student debt relief thing. Doesn't matter.
That's being decided by the courts. Does not matter. That's not a real thing either. It's
It's just something that they put in there to appeal to their base.
There are two things that are really interesting, because their base is probably going to fall
for this, but it literally doesn't matter.
One of them is returning discretionary spending to 2022 levels, I think, and the other one
is capping the budget growth at 1% for like the next 10 years.
I mean, that sounds great.
That sounds great, except it literally doesn't matter.
Because here's the thing.
When the next budget is approved, these will just be taken away.
They're not real.
actually something that a future Congress has to to honor. They pass the
legislation so they just remove it as they move forward. But their base will
probably buy this because they put it into the legislation that oh well we're
gonna cap the budget. It doesn't matter the current Congress doesn't get to
determine the budget eight years from now. But I honestly believe that large
portions of their base are not going to realize that the House of
Representatives is literally banking on them not understanding how the government
works. Republicans in the House are looking at their constituents right now
and saying there's no way y'all are gonna know, you know, basic civics. That's
what's going on with that. And I think it'll work. And I would imagine that Democrats would leave
this in. They would give Republicans this because it is that irrelevant in real life.
As far as the actual cuts, they plan on taking some money back that was not spent from COVID relief.
like $91 billion, and then they're cutting the 2024 budget as well for a grand total
reduction of $221 billion in change.
That's a drop in the bucket for the federal government.
It's not a significant cut.
They're not going after defense.
They're not going after Social Security.
They're not going at any of this stuff.
I think there was something in there about work requirements for some kind of aid.
That's something that gets thrown up every once in a while.
I don't see that going anywhere either.
As long as they can get their imaginary cuts in the future to lie to their base about,
They'll probably be happy.
And then the actual cuts, they're not significant.
So the whole show that they have put on about being concerned about the debt and all of
that stuff, it has been shown to be a show and nothing more.
Now this deal would raise the debt limit by, I want to say, or the debt ceiling, by $1.5
trillion, I think, which is a year more or less.
So even the stuff that is an actual cut, it's not really either because it can be redone
in a very short period of time.
Yeah, it's a joke.
My guess is that Biden will give them their $220 billion
and let them keep their imaginary stuff for the future.
And that's probably going to be the end of it.
Because the two key pieces in this
are the discretionary spending back to 2022
and the cap on what future budgets are.
Neither of those are real.
It's words that are put on the paper,
but no future Congress actually has to abide by them.
So I would guess that Biden would give them that
Unless he has something else up his sleeve, and he's gonna try to play hardball with it or expose it to their base.
I mean, I don't know that I'd fight over that. It's literally irrelevant.
But so that's the plan. It's a general overview of it.
it, there's room to work here.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}