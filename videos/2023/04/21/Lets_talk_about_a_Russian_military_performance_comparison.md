---
title: Let's talk about a Russian military performance comparison....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=R-ZPe2RkgoU) |
| Published | 2023/04/21 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the discrepancy between the expectations of Russia's military performance and the reality, using the example of the T-55 video and the messaging around it.
- Points out that Russia's military bloggers are praising the T-55 tank due to its ammunition load, referencing a statement from a retired Lieutenant General with experience.
- Contrasts the casualties in Afghanistan between the US (23,000 over 20 years) and Russia (official Russian estimate of 110,000 in one year), illustrating the immense losses Russia is facing.
- Attributes the inflated expectations of Russia's military capability to US defense industry propaganda that portrayed Russia as a near peer to the US to secure defense funding.
- States that Russia is not on par with the US militarily, citing the inability to match US military technology and capabilities demonstrated in past conflicts like Desert Storm.
- Analyzes the geopolitical implications of Russia's actions in Ukraine, asserting that regardless of the outcome on the battlefield, Russia has already lost geopolitically.
- Emphasizes that the conflict in Ukraine is unwinnable for Russia from a geopolitical perspective, even if they achieve military victory.
- Acknowledges that while Russia is using outdated equipment in the conflict, they are strategically holding modern resources in reserve.
- Asserts that no conventional force worldwide can compete with the military might of the United States, not as a claim of exceptionalism but a factual statement.
- Concludes by underscoring the massive costs, both in terms of casualties and geopolitical consequences, that Russia is incurring due to the conflict in Ukraine.

### Quotes

- "Russia is taking roughly 100 years worth every year."
- "There is not a conventional force on the planet that can hold its own against the United States."
- "Geopolitically, it's over. Russia lost."

### Oneliner

Beau clarifies the mismatch between expectations and reality in Russia's military performance, exposing the impact of propaganda and geopolitical consequences.

### Audience

Military analysts, policymakers

### On-the-ground actions from transcript

- Analyze and challenge defense industry propaganda narratives (implied)
- Support efforts to de-escalate conflicts and prioritize diplomatic solutions (implied)

### Whats missing in summary

Insights into the potential long-term implications of Russia's military actions in Ukraine. 

### Tags

#Russia #MilitaryPerformance #Geopolitics #Propaganda #USMilitary


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about Russia and their
performance, why they are not meeting the expectation that
some people have for them and their performance.
And we're going to talk about where that expectation came
from and why it needs to be adjusted.
And this all stems from the T-55 video.
There were a number of messages about that.
And there is now a talking point among those
who are favorable to Russia that maybe we should go over.
I've seen it a few times.
And I've seen it repeated by people
who I'm fairly certain are just repeating it and they don't know that this is more
than likely something that's engineered propaganda.
Okay, so here's one of them.
Your T-55 video is just NATO propaganda said for YouTube.
I hope you have made yourself proud.
You expect viewers to believe Russia is probably having this shortage already when the US had
no shortage all those years in Afghanistan.
Yeah, okay.
So it's worth noting that since that video went out, it is now confirmed they are using
T-55s.
It's also worth noting that this message was sent before that confirmation came out as
well.
You even now have Russian military bloggers talking about how great the T-55 is because
of its ammunition load, which all stems from a statement made by a retired, of course,
Lieutenant General, retired because he actually had experience with the thing.
So we're going to use Afghanistan.
They chose it.
We'll use it as the comparison.
But we're also going to talk about why people believe Russia was on par with the United
States, because that's a comparison being made here, and the answer may surprise you.
Okay, so Afghanistan.
The U.S. took roughly 23,000 killed and wounded.
In Ukraine, if you use Russian estimates from the FSB, okay, now keep in mind these are
the lower end of estimates that anybody believes, but these are official Russian estimates,
not Western estimates, 110,000.
That alone, that much loss alone would be enough to cause a shortage, but there's another
key factor at play.
The 23,000 number is from 20 years of US operations in Afghanistan.
The FSB number, 110,000, that's for one year.
That's from February.
So to do the math, Russia is taking roughly 100 years worth every year.
100 years worth of U.S. operations in Afghanistan.
Those are the KIAWI numbers, 100 years worth every year.
That's how bad it is.
Now, why did people have an expectation of Russia performing better than it is?
Oddly enough, it's US propaganda.
It's really defense industry propaganda, but it's the same thing.
If for the last 20 or 30 years defense lobbyists were honest when they walked into Capitol
Hill and they told congressional leaders that realistically in a toe-to-toe conventional
fight the United States would stomp Russia into the ground, we don't even need NATO,
would help, a whole lot would reduce casualties, but we could do it without them.
they told them that, Congress probably wouldn't buy more stuff.
So the defense industry kind of made Russia seem a little bit better than it really was.
So there were expectations that Russia was on par with the United States, which was never
the case.
That wasn't a realistic estimate from anybody.
It's easy to say this after the fact, right?
There's a video, let's talk about the futures, plural, of Russia and Ukraine.
Something like that.
It's from before anything started.
It's actually from the period where I still believed that Putin wasn't dumb enough to
invade.
Before any of the shooting started.
In it, I'm laying out the scenarios of how the opening assault would take place from
the Russian side.
Incidentally, it's exactly what happened.
But I said they were going to try to duplicate what the US did during Desert Storm.
I also said that they're a near pier, not a pier, so it's not going to look exactly
like Desert Storm.
There's going to be a lot more artillery because they don't have the air game and so on and
so forth. Put that into perspective. Russia was not a peer to the US military
of 30 years ago. They don't have the technology and capability to pull off
what the US pulled off 30 years ago. Nobody has ever called Russia a peer
nation. They were near peer. They were never expected to perform as well as the
US military. That isn't a realistic comparison. Russia is taking heavy, heavy
losses, ridiculous losses. When you're talking about the war itself,
Geopolitically, it's over. Russia lost. From a geopolitical standpoint, the war in Ukraine is
unwinnable. There is no way that Russia leaves that situation better than when they entered on
the geopolitical stage. Even if they were to win the fighting, the fighting isn't what the war's about.
It's not NATO propaganda that Russia is having this much trouble. It's just
what's happening. They're having to use really old equipment. Again, as I said in
that video, they still have some modern stuff that they're holding in reserve,
which is a smart move. It's one of the smartest moves they've made during this
whole thing, but you cannot compare the Russian military to the US military.
There is not a conventional force on the planet that can hold its own against the United States.
That's not American exceptionalism, it's statement of fact.
America is not doing well.
The actual costs long-term from this conflict, they're huge.
They're huge, a hundred years worth in a year.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}