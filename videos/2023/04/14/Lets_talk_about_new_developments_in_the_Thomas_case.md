---
title: Let's talk about new developments in the Thomas case....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=lZKW4std-RA) |
| Published | 2023/04/14 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Supreme Court Justice Thomas and Republican mega-donor Harlan Crow are involved in a situation with new developments related to real estate transactions.
- Crow bought properties owned by Thomas in Savannah in 2014 without disclosure from Thomas.
- Crow claims he bought the properties to preserve them to illustrate the life of Thomas as a Supreme Court Justice.
- Improvements were made to Thomas's mother's house, where he grew up, after Crow purchased it, and Thomas's mother still lives there post-purchase.
- Reports suggest connections between Thomas and Crow are becoming more concerning and less justifiable politically.
- Impeachment is being considered by some representatives regarding Thomas.
- ProPublica's investigation revealed this information, praising them for their thorough journalism.
- The situation involves a long-term undisclosed relationship that raises eyebrows, especially if Thomas's mother still lives in a property bought by Crow in 2014.
- The Chief Justice may be concerned, particularly if this information was not disclosed to him initially.

### Quotes

- "The plot has thickened through real estate plots."
- "Thomas's mother still lives there, even though Crowe has purchased property in 2014."
- "The vacations, honestly, they would have been waved away."

### Oneliner

Supreme Court Justice Thomas faces scrutiny over undisclosed real estate transactions with Republican mega-donor Crow, raising concerns of a long-term relationship. Impeachment is considered.

### Audience

Legal scholars, activists

### On-the-ground actions from transcript

- Contact representatives to express support for reform or impeachment (implied)
- Follow and support ProPublica for thorough investigative journalism (exemplified)

### Whats missing in summary

Detailed explanation of the specific real estate transactions and their implications.

### Tags

#SupremeCourt #Thomas #RealEstate #Impeachment #ProPublica


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are calling to talk about the new developments
in the situation involving Supreme Court Justice Thomas
and Republican mega-donor Harlan Crow,
because there have definitely been some developments,
pun intended.
And I guess another way to say it
would be that, well, the plot has thickened
through real estate plots.
All right.
In 2014, a company owned by Crow engaged in some real estate
transactions, bought up some real estate in Savannah.
The properties were owned by Thomas.
Thomas did not disclose these transactions that year.
Now, that is an issue in and of itself.
Crow basically says the reason that he wanted them
was to preserve them so that later they
could be used to illustrate the life of a great American Supreme
Court justice.
After the purchases, one of the houses,
which was Thomas's mother's house, where he grew up as a child, there were a bunch
of improvements made to it, according to the reporting.
And here's where it really goes sideways.
According to the reporting, Thomas's mother still lives there, even though Crowe has purchased
property in 2014.
That's concerning.
That's something that certainly will move this situation forward.
It's no longer something that there is political cover for.
connections are becoming a little bit more frequent and a little bit more
outside of what can be explained or waved away. This kind of falls in line
with the use of the jet. These are things that are going to be really more of the
focus than the vacations, even though the vacations are the part that everybody's
going to be talking about because of the luxury aspect of them.
It is worth noting that certain representatives have already started looking at impeachment
as a real possibility now.
There are still people in Congress who are talking about reform and legislating a code
of ethics and stuff like that.
You do have some who have already moved beyond that and are more interested in removing Thomas.
It is worth noting that this information, again, came from an investigation by ProPublica.
Just one more reason to remind everybody, if you're not following them, you really
should.
They really are great, old-fashioned, going through all the paperwork journalists.
There's a lot of interesting reporting that comes out of there that doesn't always make
headlines, but provides a lot of background and context.
Where are we at on this by the time it's all said and done?
The vacations, honestly, they would have been waved away.
I don't think that would have gone anywhere by itself.
The private jet might have caught some attention.
Now you're talking about a very long-term relationship
of some kind that has yet to really be explained.
If the reporting is accurate
and this property was purchased in 2014
and Thomas's mother is still living there,
that's an ongoing relationship
and people are going to want to know about it.
Not just the public, but I think this might be something
that actually raises eyebrows in Congress.
And I would imagine that the Chief Justice
is probably pretty concerned about this too,
especially if this wasn't disclosed
when the initial round of information came out.
information came out. Because I would imagine that once the first story broke
the Chief Justice is like, what's going on? And there was probably some
explanation. If this comes up and this part of this transaction and this part
of this information was not included in the explanation to the Chief Justice that
might be something that prompts further action. Anyway, it's just a thought. Y'all
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}