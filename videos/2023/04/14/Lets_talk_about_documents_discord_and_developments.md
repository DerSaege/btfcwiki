---
title: Let's talk about documents, discord, and developments....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=H63iXMutkho) |
| Published | 2023/04/14 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the topic of documents, discord, and developments in a news story.
- Expresses reluctance to talk without evidence but hints at a hunch regarding potential developments in the story.
- Mentions a person named Jake Tashara being arrested for unlawful removal of national defense information.
- Describes the arrest of Jake Tashara involving helicopters, armored cars, and SWAT teams, showcasing the seriousness of the situation.
- Reports suggest Tashara held far-right extreme beliefs, including anti-Semitic and racist beliefs.
- Points out the caution exercised by the US military in assessing individuals with extreme beliefs.
- Speculates on the potential reasons behind the release of classified documents, including leaking due to emotional discord.
- Mentions an ongoing counterintelligence assessment to determine the damage caused by the released documents.
- Indicates that the situation does not seem to be a whistleblower case but rather irrational behavior.
- Mentions doubts surrounding the accuracy of the information released, with some viewing it as intentional disinformation.
- Notes that allied and adversarial nations are cautious about the information's accuracy and potential impact.
- Compares the current situation to the Trump documents case, suggesting Trump's possession of more damaging information.
- Concludes by mentioning the potential illustration provided by the current events in understanding the possible impact of Trump's activities.

### Quotes

- "This looks like irrational behavior."
- "It's going to provide a nice illustration for most people in the United States."
- "I think that this occurring at the same time as the Trump documents case is going to be very..."

### Oneliner

Beau introduces a story involving documents, discord, and developments, discussing the arrest of Jake Tashara for the unlawful removal of national defense information and addressing doubts about information accuracy and potential impact.

### Audience

Citizens, News Consumers

### On-the-ground actions from transcript

- Monitor updates on the story to understand potential implications (suggested)
- Stay informed about national security developments and the accuracy of information being circulated (suggested)

### Whats missing in summary

The full transcript provides additional context and depth to understand the implications of a recent arrest related to national defense information and the potential impact on foreign relations and information accuracy.

### Tags

#NationalSecurity #InformationSecurity #USMilitary #ForeignRelations #TrumpDocuments


## Transcript
Well, howdy there internet people it's Beau again. So today we are going to talk about documents and discord and
developments in  a story that has been
Kind of going through the news cycle that I haven't talked about
Over on the other channel. I said I had a hunch about it and that if somebody got picked up
Well, I would talk about my hunch
I
I have a lot of questions about this, a bunch.
The thing is, I'm not in the habit of coming on here and just asking questions.
I like to have evidence, and I don't.
What I will say is that I would be totally unsurprised if there was a development in
this story that altered the way people perceive some of it, up to and including the point
in time in which US counterintelligence detected the suspect.
But that's a hunch.
What do we know?
What we know is that a person, I believe his name is Jake Tashara, was picked up for the
unlawful removal, retention, and transmission of national defense information.
He was arrested.
at this point in time he has been indicted. If you are wondering what it
looks like if you are, I don't know, not a former president of the United States
and are suspected of something along these lines, go ahead and take a look at
how they picked him up. Helicopters, armored cars, SWAT teams. When people were
talking about that early on and said if this was you or I we'd already be in
cuffs, this is what they meant. As far as him himself, reporting suggests that he
was basically a bag full of red flags dumped into an Air Force National Guard
uniform. There is reporting that suggests a lot of far-right extreme
beliefs, anti-Semitic beliefs, racist beliefs, so on and so forth. If you want to
know why the US military is very cautious about people who have these
beliefs, this is why. If you want to know why the US military is developing
policies that might encourage their troops to become more alert to injustice,
this is why. If you are somebody who is susceptible to emotional, inaccurate,
garbage rhetoric you are somebody who is susceptible to emotion and you might be
provoked into I don't know leaking a bunch of classified documents because of
a discord argument which is one of the theories behind why this happened. So what
happens from here? I believe the counterintelligence assessment is already
underway determining how much damage might have been caused by the release of
these documents. That will in some ways determine the severity of what the
suspect is looking at. At the same time, due to just the massive scope, the
starting place for this is going to be pretty high. Kind of a big deal. At this
point in time, it does not look like this was a whistleblower type thing. This
looks like irrational behavior. It looks like irrational behavior. Now from here
the FBI is going to move from where they're at, which again I believe he was
charged to be a complaint. So there will be an indictment process, that's when
we'll find out a whole lot more information.
In theory, they could have, I guess, already indicted
and it be sealed.
But once the indictment comes out,
we'll have a lot more to go on.
As far as the damage that was caused on the foreign policy
scene, because this is so bizarre,
There are signs that not a whole lot of people
actually trust the information that was released.
This bears a lot of the hallmarks
of an intentional disinformation operation.
But I would point out that based on what has been released,
that's not what it is.
based on the reporting, this was actually just irrational behavior and trying to show who's
the biggest on the internet. But because of all of the hallmarks,
there's a lot of doubt as to whether or not the information is accurate. So both allied and
adversarial nations are kind of, they're taking all of it with a grain of salt, and it's hard
to gauge whether or not this is going to have a large impact for real.
I know there's a lot of reporting talking about how this is going to disrupt this or
that or this or that, and that's fine, but all of that reporting is based on the idea
that the other nations believe it's true and we've seen signs that quite a few
nations and some that are most important don't believe it's true, don't believe
the information in the documents is accurate. So we'll have to wait and see
on that. I think that this occurring at the same time as the Trump documents
case is going to be very... it's going to provide a nice illustration for most
people in the United States as far as what the damage could have been or might
have been from Trump's activities. Understand that the reporting suggests
that what Trump had in his possession is far, far more damaging and wide in scope.
So if you want to get a gauge, here it is playing out in real time.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}