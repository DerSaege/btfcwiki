---
title: Let's talk about US troops in Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=1eYPDqpr3wU) |
| Published | 2023/04/14 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Reveals the presence of US personnel in Ukraine has recently surfaced, surprising many.
- Describes a hypothetical scenario where concerns about US equipment being misused in Ukraine led to the deployment of personnel attached to the US embassy.
- Mentions a sarcastic video made in November 2022 discussing the presence of these personnel as a response to prior outrage.
- Notes that the personnel are focused on ensuring the proper use of equipment and are not involved in combat.
- Suggests that while officially their role is accountability, they may also be providing advice.
- Speculates about the presence of former US military members working for contracting companies in Ukraine, despite lack of evidence.
- Emphasizes the role of manufactured outrage in shaping narratives and policies.
- Points out a US Congress member justifying the leak about the troops, potentially involved in creating the situation.
- Indicates that the deployment of personnel was likely already happening but became public due to manufactured outrage.
- Encourages critical thinking when evaluating reporting, suggesting that if a topic falls within someone's expertise, they may have covered it before.

### Quotes

- "I want you to picture a world where, let's say five or six months ago, Russia and people who are of like minds in the U.S. Congress."
- "When you create an outrage of the day, like they did, that's the only possible response."
- "The people who are super mad about this, in fact, there's one person who sits in the US Congress, who's kind of justifying the leak."
- "It's just a thought, y'all have a good day."

### Oneliner

Beau reveals the presence of US personnel in Ukraine amidst manufactured outrage, shedding light on accountability and potential covert operations.

### Audience

Journalists, policymakers, concerned citizens

### On-the-ground actions from transcript

- Contact journalists or news outlets to demand further investigation into the presence of US personnel in Ukraine (suggested).
- Reach out to policymakers to question the narrative and accountability surrounding the deployment of personnel (suggested).
- Engage in critical thinking and fact-checking to understand the nuances behind manufactured outrage and its impact on policies (exemplified).

### Whats missing in summary

Detailed analysis of the geopolitical implications and potential consequences of the revealed US personnel presence in Ukraine.

### Tags

#USpersonnel #Ukraine #ManufacturedOutrage #Geopolitics #Accountability


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about the apparent news
that there is US personnel in Ukraine.
This is information that just surfaced
and it has surprised a lot of people.
It has led to a number of messages sent to me
saying that any self-respecting journalist, any commentator who didn't
talk about that fact first wasn't worth anything and how me not
talking about it was obviously playing in to you know
NATO and just supporting you know rampant militarism
and all of that stuff.
So we will talk about those troops today, again.
I want you to picture a world where, let's say five or six months ago, Russia and people
who are of like minds in the U.S. Congress.
They created an outrage of the day where they were super worried that U.S. equipment being
provided to Ukraine wasn't going to be used on the battlefield, but was going to be siphoned
off and end up in some shady gray market.
They were super concerned about this.
They got really mad.
And that in response to that, even though there was no evidence at the time, somebody
made a video, let's say hypothetically speaking on November 1st, 2022, that said there would
be a special group of people who would know how the weapons worked and how to train special
forces and they would be attached to the US embassy in Ukraine and they would be working
for the Defense Attaché's office there.
They would be nowhere near the fighting though, but they would be there to make sure that
stuff was used properly.
And in that video that is incredibly sarcastic in this fictional world that totally didn't
happen. In that video, the person who made it went out of their way to say,
when this is discovered years from now, because I didn't, I mean the person who
made the video didn't expect a leak, it's important to remember that the people
who are going to complain about it are the people who made it happen. I haven't
covered it because I already covered it more than five months ago put out a
video detailing exactly what was revealed it's worth noting that the last
comment on that video is this is the absolute worst commentary on this I
wouldn't listen to this dude for his take on anything four weeks later I'll
put the video down below but the name of it is let's talk about US
personnel in Ukraine. Now, just to clarify the situation, in the leaks there's
evidence of special forces working in Ukraine. Let's hear what NSC spokesperson
Kirby has to say about it. I won't talk to the specifics of numbers and that kind
thing, but to get to your exact question, there is a small U.S. military presence at
the embassy in conjunction with the Defense Attaché's office to help us work on accountability
of the material that is going in and out of Ukraine.
So they're attached to that embassy and to that Defense Attaché.
They're nowhere near the battlefield.
I already covered this, yeah, it was in a super sarcastic manner because there wasn't
any evidence of it at the time.
But when you create an outrage of the day, like they did, that's the only possible response.
Now, are these people who were sent there, are they only checking the equipment and making
sure that everything's done?
Officially, yes, but I want you to think about the type of people who would volunteer and
go on an assignment like that.
They're probably providing some advice along with it.
They're not fighting.
Not this group, but as I have talked about in other videos, undoubtedly there are people
who recently separated from the US military.
They're no longer US troops and they're working for a contracting company there.
That is absolutely happening.
Again, there's no evidence of that, but it's occurring.
As soon as the international legion kind of formed up, it provided perfect cover for that.
It's absolutely occurring.
But again, you're probably not going to find any evidence of that.
Without this leak, the video from November 1st, 2022, it wouldn't have made any sense
until after the war was over.
It just would have seemed like an incredibly sarcastic piece of commentary that was absolutely
the worst commentary on it.
It's important to remember that there are a whole lot of people who are in positions
of authority who can help manufacture a narrative, like being surprised about this, who don't
know how any of this stuff works and don't know how their statements, their public statements,
end up shaping policy.
The people who are super mad about this, in fact, there's one person who sits in the
US Congress, who's kind of justifying the leak, saying, well, they told us about this.
They told us about these troops, and we needed to know that they were one of the people who
helped manufacture the outrage and created this situation to begin with.
Now realistically, my guess is that this was already happening before the manufactured
Like in real life, it was actually occurring before then, but it was very secret.
But once the outrage occurred, well, they had no choice.
I think I said it was an inevitability that it was going to happen, and it did.
When you're looking for reporting, if somebody isn't necessarily talking about something,
necessarily talking about something, and it is right up their battle, their area, maybe
they already talked about it.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}