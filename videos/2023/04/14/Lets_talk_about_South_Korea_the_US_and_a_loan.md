---
title: Let's talk about South Korea, the US, and a loan....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=f9BPeuv115c) |
| Published | 2023/04/14 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Representative Green's statement about borrowing ammunition from South Korea for Ukraine sparks fact-checking and analysis.
- The statement by Representative Green is criticized for inaccuracies and premature conclusions regarding the ammunition deal between the US and South Korea.
- The deal discussed is not finalized but in negotiations, involving a loan of 500,000 rounds of howitzer ammunition, not 50,000 as mentioned.
- South Korea's usual stance against providing lethal aid is part of the context for understanding the agreement.
- The US is ramping up production of ammunition, indicating that the request for assistance from South Korea is not due to a shortfall.
- The core issue is finding ways to support Ukraine without violating South Korea's principles against lethal aid.
- The real story lies in the potential shift in South Korea's stance towards aiding Ukraine in a non-lethal manner.
- The importance of accurate sources for understanding foreign policy is emphasized over relying on social media for news.

### Quotes

- "Don't get your news from Facebook memes."
- "It's not correctly framed by this representative shock, I know, right?"
- "A country that is notorious for not providing lethal aid is, in theory, maybe, and maybe still, looking for ways to work around that long-standing principle because the situation in Ukraine is that important."

### Oneliner

Representative Green's premature and inaccurate statements on US-South Korea ammunition deal for Ukraine prompt fact-checking, revealing the significance of non-lethal aid support and the importance of reliable news sources.

### Audience

Policy enthusiasts

### On-the-ground actions from transcript

- Contact your representatives to advocate for accurate and informed foreign policy decisions (implied).
- Support reliable news sources and fact-check information before forming opinions (implied).

### Whats missing in summary

The full transcript provides a detailed breakdown of the US-South Korea ammunition deal for Ukraine, shedding light on the importance of non-lethal aid support amidst international dynamics.

### Tags

#US #SouthKorea #Ukraine #AmmunitionDeal #ForeignPolicy


## Transcript
Well, howdy there, internet people, it's Bill again.
So today we are going to talk about South Korea
and the United States and lending and Ukraine
and a statement from a certain US representative.
We will go through the statement
and then we will fact check the statement
because of course, then we will talk about
what's actually happening and why it's happening.
Then we'll talk about what the real news story
here is, assuming this is true, and then we'll introduce a new rule that I think
everybody should live by. Okay, so the statement is from Representative Green,
and here it is. This is unforgivable that we have given so much all-caps ammunition
to Ukraine that we have to borrow all caps. 50,000 rounds from South Korea.
Pathetic all caps three exclamation points. The entire Biden admin is a failure all caps.
Okay so we'll start with the fact check because while caps lock is cruise control for cool,
you still have to drive and she didn't. Alright so the first thing to note is that this is not a deal.
What's being discussed, it's not a deal. It's not finalized. It's in negotiations.
And as somebody who is part of the federal government, she probably
shouldn't be acting as though it is finalized and commenting on it in public
since it's not. In a recent video I talked about how you might have
newly minted people in Congress talking about foreign policy and causing issues.
It would look something like this if that was to happen. The next piece to
be fact-checked, it's not 50,000 rounds, it's 500,000 rounds. And then we get to
the word borrow. Okay, so she's talking about the possible deal between the US
and South Korea where South Korea is going to loan 500,000 rounds of 155
millimeter howitzer ammunition. Okay, loan is that's definitely in quotation
marks. Generally speaking when you're talking about howitzer ammo you don't
get it back when you loan it to somebody. It is really hard to go out there and pick up all the
pieces and super glue it back together. It takes a lot of time. It's easier just to build new ones.
The fact that the word loan was used to anybody who has an understanding of foreign policy
would realize there's something else going on. It's not the U.S. borrowing ammo. So, what
What is actually happening?
First thing you need to know, 155mm Howitzer ammo is being used a lot over there.
For those who are wondering, is this the same ammo that we were talking about seven months
ago?
Yes.
Same stuff.
It's being used a lot in Ukraine.
A whole lot.
For a couple of reasons.
One is doctrinal.
They use it more than most countries would.
Part of that is because there's not a lot of air support.
Since there's not a lot of air support, they have to use artillery to fill that role.
So they're burning through a lot of this ammo, this particular type.
Okay, so South Korea is a country that does not provide lethal aid.
Generally speaking, they don't do that.
They'll provide tons of humanitarian aid, but they do not provide lethal aid.
It's extremely rare.
So remember at the beginning of the conflict, the beginning when it all started in Ukraine,
you had a lot of countries that didn't want to commit lethal aid and so what they would
do is they would send Poland 50 lethal widgets and then Poland would give Ukraine 50 lethal
widgets of their own manufacture.
They would just kind of swap them out.
If this is occurring, that's what's happening.
South Korea at that international poker game where everybody's cheating, South Korea is
sliding the US a card.
The United States is then going to slide a different card made in the USA to Ukraine.
That way, South Korean ammo never enters the battlefield.
way they didn't provide lethal aid because it's a principle that they don't want to violate.
That's what's happening.
It's not because of a massive shortfall.
In fact, if somebody was, I don't know, in Congress, they might know that the United
States is currently ramping up production of 155, I want to say 600 percent, and it's
It's supposed to be doubled by the end of the month where they're going to be pumping
out almost a thousand of these things a day.
It's not correctly framed by this representative shock, I know, right?
So it isn't a situation where the US is begging South Korea to loan them ammo for their own
uses.
It's a situation where the US is trying to get all of its allies to provide aid to Ukraine,
and South Korea doesn't provide lethal aid.
So this way, they get a workaround.
That's what's going on.
What's the real story here?
A country that is notorious for not providing lethal aid is, in theory, maybe, and maybe
still, looking for ways to work around that long-standing principle because the situation
in Ukraine is that important.
That's the actual story.
Now, new rule, something I've said repeatedly over the last couple of weeks is don't get
your news from Facebook memes.
get your foreign policy stories from the space laser lady. Anyway, it's just a
thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}