---
title: The Roads to the new shop and a Q&A
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=eY3EKOipXjI) |
| Published | 2023/04/29 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau shares the journey from filming in his actual workshop to constructing a dedicated space for his channel due to limitations.
- The new shop is fully equipped for demonstrations, with better lighting and soundproofing.
- Beau explains the improvement in video quality as a result of upgraded lighting, not a new camera.
- He addresses delays in legal proceedings in Georgia and refrains from guessing without sufficient information.
- Beau advises on staying safe in rural areas to avoid getting shot accidentally, especially when lost.
- He recalls a funny anecdote involving his kid's response to what his parents do at school.
- Beau declines to comment on a feud between prominent right-wing figures, deeming it not his business.
- He attributes his channel's success to luck, contrary to advice from YouTube expert Roberto Blake.
- Beau talks about approaching collaborations with individuals on social media, particularly women, with respect and genuine interest.
- He acknowledges the potential challenges and increased attention required post-Tucker's departure from right-wing media.
- Beau expresses hesitation about covering certain sensitive topics, like assault cases, due to personal reasons.
- The new studio will enable Beau to create videos on various topics like water purification and tools for mutual aid.
- He shares thoughts on creators who push boundaries in their content and the impact of their actions.

### Quotes

- "The problem with filming in my actual workshop that you know occasionally I had actual work to do and that got in the way."
- "There's going to be so many people jockeying for his job, and for a while it's going going to be just, I'm going to have to, I'm going to have to pay way more attention."
- "So if you're waiting or you're getting ready for hurricane season or whatever, yeah."

### Oneliner

Beau shares the journey from filming in a limited space to constructing a dedicated shop for demonstrations and future content creation, addressing safety in rural areas, collaborations, and content creator responsibilities.

### Audience

Content creators

### On-the-ground actions from transcript

- Collaborate with other content creators respectfully and genuinely (exemplified)
- Educate yourself on safety measures in rural areas to avoid accidental harm (exemplified)
- Use luck as a starting point but be willing to put in the effort for success (implied)

### Whats missing in summary

The full transcript captures Beau's journey from humble beginnings to a dedicated space for his channel, touching on various aspects like safety, content creation, collaborations, and personal boundaries.

### Tags

#ContentCreation #Safety #Collaboration #YouTube #CommunityBuilding


## Transcript
Well, howdy there, internet people.
It's Bo again.
And today, we will talk about the roads to the new shop,
because as some of you have figured out, it's done.
And we're now using it.
So what we're going to do is we're going to go through the
whole chain of events that led us to here for those who
don't know.
And then we'll jump into a normal Q&A.
If you don't know how this channel started,
it started as a joke.
A whole lot of people did not know I was from the South,
and when they found out, they encouraged me to make videos.
It started with me and my cell phone that was broke,
and sound only came out of one side
when y'all would listen to it,
and I was filming in my actual workshop.
And it was fun, it was great.
The problem with filming in my actual workshop
that you know occasionally I had actual work to do and that got in the way. It
also had a tin roof which meant if it was raining I couldn't record and even
after it rained there would be little drops of water that would hit the roof
and it sounded remarkably like pants in a dryer. If somebody was running a
a tractor, too much noise, couldn't record.
So eventually, we got the idea to build a little soundproof
set to record in.
And we made it look, as much as we could,
like the original shop.
And I'd been recording in that.
The problem with that is that it was tiny.
And by tiny, I mean, I could touch both sides of the walls.
It was very small, which meant we
didn't have a lot of room to do some of the stuff
that I wanted to do, the demonstration type stuff.
We needed a workbench.
So eventually, as time went on and the channel
continued to grow, we started converting a steel building
into a place dedicated to do the channel.
And we once again kept the same look from the original shop.
And that is where we are at now.
It is a shop that will function.
We'll have tools, and it can do all of the demonstration stuff
that y'all have asked about.
And it has lighting, and there's soundproofing,
and all of that stuff.
There have been questions about whether I got a new camera.
Nope, same one.
This is all lighting.
The change in clarity is all due to having much better lighting.
I'm still not 100% happy with it, so there will still be fine tuning on that.
I feel like there's still an echo, so we'll probably be changing the sound a little bit
too as time goes on, as we figure all of it out.
So that's what's happening.
And you will see a lot more content go out over on the roads with Bo, which is probably
where this video is.
But it will hopefully really help workflow.
We'll see how it works out.
There is still a whole lot to do outside of that door because we plan on putting in like
an office and kind of upgrading our information flow as well, but we'll get to that as we
get to it.
Okay, so let's jump into the Q&A.
Your video on basic civics shows you don't understand basic civics.
All I have to do is make it illegal to propose a budget that isn't balanced, problem solved.
Your budget doesn't balance, you go to jail, period.
Our founders never would have tolerated this kind of spending.
Our founders also would not have tolerated your solution.
That's why they put the speech and debate clause into the Constitution.
You can't put a senator or representative in jail because they proposed something.
Yeah okay, you didn't even guess at why there was the delay in the imminent charges in
Georgia.
Why?
I'm assuming this is about, well, the delay.
The prosecutor there made it sound repeatedly like charges were coming any minute and now
we're looking at summer before we're going to find anything out.
I didn't guess because I don't like to guess.
I like to have really good information because I like being right.
If you're going to force me to guess, the best I can do is there was an offer that was
supposed to be conveyed to some of the witnesses and basically an immunity deal.
Reporting suggests that that offer didn't get conveyed.
If that offer didn't get conveyed, the prosecutor may actually want more time to get them to
accept that deal so they have more witnesses so they have a stronger case.
But that's a guess, not like my normal guesses that are very well researched.
How do I avoid getting shot in rural areas if I get lost?
Yeah, I've actually gotten this question more than once.
So the big thing to know in rural areas, and this is because of a couple of incidents out
rural areas where people have pulled into the wrong driveway, and they've taken fire,
or in one case, a woman was killed.
Before we get into this, I want to say, I don't think that the people that were shot
at did anything wrong.
My understanding is that in both of those cases, the person who shot at them was an
anomaly.
of the norm. It wasn't a case where somebody who didn't already have issues
thought something was amiss. But if you want to kind of hedge your bets, one
thing to pay attention to is the fence line. Rural people, they don't put their
fence right at their property line, especially along normal
roads. That's pretty rare. You normally have a car length or
two that is outside of the fence, but still part of their
property. Generally speaking, out there, nobody cares about.
The problem is a lot of people in rural areas also leave their
gate open. So even if the gates open, don't cross the fence line. That would be
rule one right there. And again, I don't know that anybody involved in any of
these incidents did that, but that's a big one. Also be conscious of the car
you're driving. There are some areas that if you pull into and you're driving of
a late model Yukon or Suburban or a Malibu, they probably don't want you
there. So be aware of what vehicles law enforcement entities in your area use
and if you are in one of those be extra careful because the whole idea of law
don't come around here, that's still a thing. Be extra careful around rural
properties that have lights facing the fence or facing outward rather than the
lights facing the house. That's a clear indication that those people want to be
left alone. If you run across a property that has two gates in one spot, they have
a gate and then you could drive a little bit and then there's another gate, just
don't even risk it. Those are people that are very serious about being left alone.
Again, in the incidents that I'm aware of, though, they didn't do anything wrong.
Since you left Twitter, I've missed stories about the animals and kids.
Tell me one.
Let's see, off the top of my head, I don't know that there's been anything incredibly
interesting lately. One of my kids, they had something where they were talking about what
their parents do at school and he said I was on YouTube and his teacher didn't believe
him and said, aren't you sure that, or don't your parents actually have a ranch? I think
I've seen your mom at the feed store."
And my son responded by saying, those horses don't make any money.
Which is funny because it's very much a kid thing to say and also because they don't.
So there's that.
What do you think of the feud between...
Yeah.
Okay, so there is a feud going on between two prominent right-wing people, and it apparently
stemmed from one of their family situations changing.
What do I think of it?
I think it is totally not my business.
I have no comment on that whatsoever.
Are you in the new studio?
It seems different.
Yes.
Roberto Blake said it's never luck and that people only say that to seem humble.
You've always said it's luck.
Any thoughts?
Okay if you don't know who Roberto Blake is.
He gives advice to people who are building YouTube channels and he is really, really,
he's really well informed.
His advice is really good.
If you have a YouTube channel or you're trying to start one, yeah, look into him.
He is very good at explaining what the analytics, all the information that Google gives you,
explaining what it really means and how you can use it.
And I'm guessing that he's, I'm guessing he said something like, it's not luck, it's putting
out this type of video that appeals to this audience and stuff like that because he does
that a lot.
In my case, it actually was luck.
The very first video that I put on YouTube blew up.
That never happens.
That's not a thing.
And it gave me a starting audience.
And I mean, like I could show you in the analytics where it's the very first video where my subscribers
spike.
and he gave me that base audience to start from.
Now from there, yeah, everything that he says
about, you know, just being willing to go all in on it,
it's all true. My personal belief is that you have to do
everything that he's saying and get lucky,
because you need that one video to pop to to get you that base audience.
And whether it occurs on your first video or your 50th, you're not going to get a giant
channel until that happens.
It's not going to start growing until you get that one video that clicks.
And it's hard to try to craft that video using the analytics because when you first start
out you don't have an audience, you don't have any analytics, not that they can tell
you anything.
But yeah, I am the exception that proves his rule.
I don't know anybody else that picked up tens of thousands of subscribers on their first
video. And it was a joke. It was a joke making fun of people burning
Nikes. Let's see. You work and talk with a list of people on YouTube or Twitter.
Why do they say yes to you?
Person A is incredibly fine, not his word.
And person B is the hottest woman on the internet.
I will make sure to tell her you said that.
She doesn't work with anybody.
What do you do differently?
I've been trying to get them on my podcast, and
neither have responded.
The entire list of people is women and the two, you know what, both of them do yoga,
so I can say this without being too identifying, yeah I know both of them.
One of them has actually done yoga in my living room and I'm telling you, I was sitting there
on my couch just staring at my computer working.
Both of these people, they are not on the platforms they're on to be known as the hottest
woman on the Internet or incredibly fine.
My guess would be that the phrasing on some of your social media leads them not to be
incredibly interested.
Yeah, they are both attractive women, but that's not what their platforms are.
not why they they make content so I would I would guess there is something
in your approach that that suggests you're interested in them for their
looks let's see I know you always try to be positive but admit it your job is now
so much easier that Tucker's gone? Probably not. I don't know yet. It's too early to tell.
It's probably going to be harder, to be honest, because to know what the right-wing talking
point outrage of the day is, all I had to do was keep up with him and a couple of other people.
With him gone, there's going to be so many people jockeying for his job, and for a while it's going
going to be just, I'm going to have to, I'm going to have to pay way more attention to
right-wing media than I want to.
You should have been a preacher.
You don't like talking about religion, but those are some of your best videos.
I don't know that I have the appropriate level of faith to be a preacher.
When you promote smaller channels like Drew or Miss Context, how do you pick them?
Generally, it's something that I think most of y'all would like and isn't really being
offered in that way somewhere else.
There are a lot of progressive southern comedians, but there aren't a lot that have the edge
that he does.
And I know there were a lot of people who commented about his edge and were like, yeah,
that's too much, which I understand.
It's not for everybody.
But I think for those people who would like to see themselves represented, I think that's
really important.
context, that channel, like I understand the goal of it and they're still early on.
I mark my words that's gonna be a good channel.
Why aren't you covering the New York Civil Case?
So years ago when I think I was working for a little Canadian outlet,
it. I covered a case that involved the same sort of transgression. This is, of course,
about the E. Jean Carroll case in New York, the civil case which started today. I covered
a case like that once and went through all of the evidence. I got a little too close
to it. It really bothered me and I never covered a case like that again. I don't
cover those particular types of assault. I don't. I don't respond well if they
don't go the way I think they should. We'll just leave it at that. New shop.
Does this mean we'll finally get videos on water purification, heart tools, wiring, etc.?
Yes, that's exactly what it means.
We can start doing those out or putting those out.
So for those who don't know, one of the reasons we wanted this was to put together videos
that allowed for hands-on demonstration.
I'd actually forgot about the HARTools thing.
But I mean, after all this time, so if you're waiting or
you're getting ready for hurricane season or whatever,
yeah.
Overall, they're like any tool brand.
Some are great.
Some are not.
But I'll put that together pretty quickly, because I
know it's coming up time to get stuff for mutual aid but yeah we will put
together these videos so people can have those skills and or have a reference to
them and that is that's a big part of why we wanted this. Do you think creators
that go too far in what they say are net good even if they reinforce bad things
sometimes. That is really broad. The answer to that is it depends, you know.
Sometimes reinforcing a bad thing to get rid of a much worse thing, I think it's
okay. Sometimes I think it's not. But what you have to look at is their results.
You know, there was that guy who, he was a violent racist, like that's who he was.
I wish I could remember this guy's name.
But he saw the light, you know, he changed, and he changed on a fundamental deep level.
Complete, completely different.
But for a couple of years after he had made the change, he continued to be adjacent to that community and his whole goal
was to kind of guide them from being violent racists to being like peaceful separatists.  I mean, yeah,
they're still bigots, but they don't want to hurt anybody.  body.
It depends on the stakes.
It depends on what they're doing.
In a case like that, had people found out what he was doing, he was putting his life
on the line for that.
I think there's a whole lot of factors that go into that.
There are some times when I think that some creators are just going too far to be edgy.
And sometimes I think they are doing it in a very intentional way to get a certain audience
that would be bound for really bad things and steer them somewhere else.
So it depends on the stakes, it depends on whether or not it works, it depends on the
person's intent when they're doing it? I mean, there's a whole lot to that question.
The shop looks much better. Your carpentry skills have improved. Oh, that's painful.
So the original shop, I did not build. And the one behind me, Cletus did all the shelving.
So my carpentry skills have not improved. He improved on the design. I'm just going
to leave it at that and say you know we were trying to stick more to the original
shop when we put the other one together. Hurt my feelings. No. So those are the
questions. We'll do another one of these relatively soon and we will get I'll get
the heart thing out as soon as possible because I know that there are people who
are going to need that soon. So that's what's going on. We're in the new shop. As
we get everything situated and the lighting tuned, we may even do like a
full tour and show everybody where everything is and all that kind of stuff.
But I guess that's it. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}