---
title: Let's talk about McCarthy, conflicting statements, and nerve....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=fUrr4XgmgCM) |
| Published | 2023/04/29 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republicans in the House presented a budget as a victory, but discrepancies are arising regarding McCarthy's messages to different factions.
- McCarthy urged colleagues to ignore the substance of the bill and focus on passing any legislation to show seriousness about spending cuts.
- McCarthy promised conservatives that the bill they voted on was just a starting point, not the final version.
- McCarthy's messages to moderates and hardliners were contradictory, suggesting different levels of severity in the bill.
- There are concerns about McCarthy's leadership and potential backlash from factions within the Republican Party.
- Some Republicans may have only voted for the bill after being assured it wouldn't be the final version, raising doubts about the bill's actual support.
- The lack of transparency from Republicans about the bill's contents and implications is worrying.
- Republicans in swing states who supported the bill may face challenges explaining their vote to constituents in the future.
- Inter-factional tensions and arguments within the Republican Party are expected to arise due to the conflicting messages and actions.
- The situation in the House regarding the budget is uncertain and may lead to further revelations.

### Quotes

- "Even the Republicans don't actually want this."
- "There might be some arguments that they kind of spring up."
- "Things in the house are going great."

### Oneliner

Republicans present budget victory, but mixed messages and doubts emerge, revealing internal party tensions.

### Audience

Politically active individuals

### On-the-ground actions from transcript

- Reach out to Republican representatives to express concerns and seek clarity on their stance (suggested)
- Stay informed about the budget process and any updates regarding Republican positions (suggested)

### Whats missing in summary

Insights into the potential long-term implications of the budget decisions made by Republicans. 

### Tags

#Budget #RepublicanParty #McCarthy #InternalTensions #PoliticalActions


## Transcript
Well, howdy there internet people, Lidsbo again.
So today we are going to talk a little bit
about that budget that the House sent up,
Republicans in the House sent up
and acted like it was a great big huge victory
and some contradictory messages
and nerves of steel,
because apparently McCarthy has them.
Okay, so the ink isn't even dry
on this budget yet, and reporting has started to highlight some discrepancies in what McCarthy
is said to have told the various factions within the Republican Party, because remember
they can't agree on anything, they all want something different.
So according to the New York Times, while he was, quote,
beseeching his colleagues privately to back the bill,
Mr. McCarthy repeatedly told them
to ignore the substance of the measure, which would never
become law, and instead focus on the symbolic victory
of passing any legislation to show Mr. Biden they
were serious about their demand for spending cuts.
I mean, just saying that in and of itself is something else, but there's more.
There's another piece of reporting that says that McCarthy promised conservatives late
Tuesday night that the debt ceiling bill they voted on Wednesday or on yesterday was a floor,
not a ceiling.
McCarthy, he said, told them he would personally oppose and fight against any debt ceiling
agreement that doesn't include all of the red meat provisions in the House
bill. So the short version here is that McCarthy went to the moderates and was
like this is never gonna become law. Don't even worry about what's in it. It's
just a starting place. It won't be this drastic. And then he went to the
hardliners and was like hey this is just the beginning. We're gonna do more. I mean
that's something else. These people work in the same building.
I hope they don't talk to each other or read these reports.
They might get pretty upset and given the fact that McCarthy significantly
lowered
what it required to remove him as speaker,
he better hope that none of them get in their feelings about this.
I would imagine that
other factions of the Republican Party were probably told other things as well.
If this reporting is accurate, New York Times and Politico, if it's accurate, then there's
probably going to be more to come out. But the important part for everybody watching to remember
is even the Republicans don't actually want this. In order to vote for it, some of them had to be
reassured that this wasn't going to be the actual bill. That doesn't provoke a
lot of confidence. Of course I would suggest given the fact that their main
messaging is, hey we passed a bill now you have to negotiate, says the same
thing. And given the fact that they're not actually going out and telling
people everything that they cut, that probably says the same thing as well.
those Republicans who are in swing states who voted for this, they might be
really upset because they have taken a very risky vote now that they may have
to answer for later. So my guess is that there will be conversations between the
different factions and there might be some arguments that they kind of spring
up. Yeah, things in the house are going great. Anyway, it's just a thought. Y'all have a
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}