---
title: Let's talk about Manchin and progressives getting a wish granted....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=IIx0arGO63A) |
| Published | 2023/04/29 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau warns about progressives in West Virginia potentially getting their wish in the next election but being unhappy about it.
- He points out that Senator Manchin, though a Democrat, is not progressive and has been safe in West Virginia because of it.
- Governor Jim Justice has announced his candidacy for Senate in West Virginia, posing a significant challenge to Manchin.
- Justice is well-liked in West Virginia, being a Republican and potentially replacing Manchin with someone worse through a progressive lens.
- Beau acknowledges that Justice is likely to have a strong campaign with financial backing and public favor, making his victory seem almost certain unless a scandal emerges.
- He speculates that Manchin may have doubts about winning in the face of Justice's popularity and resources.

### Quotes

- "Being careful what you wish for."
- "Manchin's name gonna be at the top of that list."
- "Justice may give progressives their wish, but Manchin may be gone."
- "Just a thought, y'all have a good day."

### Oneliner

In the next election, progressives in West Virginia might be unhappy as Governor Jim Justice's candidacy poses a serious challenge to Senator Manchin, potentially replacing him with someone worse through a progressive lens.

### Audience

Progressives in West Virginia

### On-the-ground actions from transcript

- Support progressive candidates in West Virginia (exemplified)
- Stay informed about the upcoming election and candidates (implied)

### Whats missing in summary

The nuances of Senator Manchin's political stance and potential implications for progressives in West Virginia.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about West Virginia
and being careful what you wish for.
Because I have a feeling that in the next election,
a whole lot of progressives are going to get their wish,
and they're going to be really unhappy about it.
If you were to ask most progressives in this country
which Democrat they disliked the most.
I'd be willing to bet that Manchin's name
gonna be at the top of that list.
Manchin is a senator from West Virginia.
And he's been pretty safe.
He's decently liked, even though he's a Democrat.
But it's because he doesn't really act
like a Democrat most times.
He is not progressive at all, and that has kept him safe in West Virginia.
He is not a person that I agree with on much of anything.
But him having a D after his name, that counts towards the Democratic majority in the Senate.
has been safe for a very, very long time. However, Governor Jim Justice has
announced his candidacy. He intends to run for Senate in West Virginia assuming
he survives his primary battle, which he probably will. Manchin is now in a
campaign that he's likely to not win.
Justice is incredibly well-liked in West Virginia. Very well-liked.
He's one of the few politicians that has like a truly favorable approval
thing. He is a cold boss.
It's West Virginia. I mean, basically
all politicians are pretty much boss hog come to life,
but that's
what you're dealing with. He's a Republican and
he very well may give progressives their wish
and Manchin may be gone.
But he is probably going to be replaced with somebody worse.
Not just in the fact that he would, that his seat would count towards a Republican majority,
but policy-wise, probably going to be worse through the progressive lens.
And realistically, he's going to have money for the campaign.
He's a coal boss in West Virginia.
going to have money. He's very well liked, definitely better liked in West Virginia
than Manchin. Unless there is something in his past, unless there is something,
some kind of scandal or something, I don't really see a way for him to lose.
something has to go horribly wrong with his campaign for him to lose so we'll
have to wait and see it's still really early but at time of announcement I would
imagine that Manchin probably had doubts about even running I'm sure publicly
he's going to continue to say, I'll win any race I can enter because that's what he says.
But I would imagine there's a little bit of doubt there and it's probably pretty well founded.
Anyway, it's just a thought.
Just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}