---
title: Let's talk about understanding term polling....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=lI9cn4iRIpQ) |
| Published | 2023/04/29 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Polling helps gauge the popularity of different entities and provides insights into political currents in the country.
- Most terms and entities in the United States do not have majority support, with the real metric being how many people view them positively versus negatively.
- Black Lives Matter (BLM) is viewed positively by 38% of Americans, while MAGA is viewed positively by 24%.
- BLM is two points underwater, while MAGA is 21 points underwater, meaning more people view MAGA negatively.
- Attacks against BLM and similar movements do not have the intended effect and can drive negative voter turnout.
- When candidates identify as MAGA, they are eliminating 45% of the population who view it negatively.
- The Republican Party may drive their own negative voter turnout by associating with MAGA.
- Campaigning against movements like BLM can lead to more people voting against the candidate.

### Quotes

- "Black Lives Matter is very, very much more popular than MAGA."
- "There are far more people who view MAGA in a negative light than view it in a positive light."
- "The Republican Party is going to drive their own negative voter turnout."

### Oneliner

Polling reveals that while Black Lives Matter is more popular than MAGA, attacks against movements like BLM may drive negative voter turnout.

### Audience

Political analysts, campaign strategists, voters

### On-the-ground actions from transcript

- Analyze polling data on different entities and movements to gauge public sentiment (implied)
- Be mindful of how attacks on certain movements can affect voter turnout (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of polling data and the potential impact of attacks on movements like BLM on voter behavior.

### Tags

#Polling #PoliticalCurrents #PublicSentiment #NegativeVoterTurnout #CampaignStrategy


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about polling
in different entities and why measuring the popularity,
or unpopularity, of different entities
can actually kind of help clue you in to the general currents
in the country, politically speaking.
in a recent video I went over some polling and I talked about how most terms
and entities in the United States do not actually have a majority of people
supporting them and the real metric because of that is the number of points
they're underwater, meaning the difference between how many people view
them positively and how many people view them negatively, the the percentages
there. During this, there were two entities that seemed weird. BLM and MAGA
were both mentioned in the polls. The poll collected data on how people
felt about these groups, these terms. Lots of questions about why this matters.
Okay, so to go over those numbers again, for BLM, for Black Lives Matter, 38% of
Americans view it positively, view it in a positive light. For MAGA, it is 24%. BLM,
Black Lives Matter, is very, very much more popular than MAGA. But that's not
the whole story, you have to look at who views them negatively as well. Okay, for
BLM it's 40%, for MAGA it's 45%. So what this means is Black Lives Matter is
two points underwater, MAGA is 21 points underwater. There are more people, there's
a wider gap of people who dislike MAGA more, so basically what it boils down to.
So why is this included in polling about the election? Every time one of the
Trump-lite MAGA people comes out and references MAGA, they are not winning
moderates. They're upsetting people. The people who support MAGA, they
already support MAGA. They're not going to be winning anybody new with this. More
importantly, when they launch attacks against BLM, and you can safely assume
that like movements have the same, roughly, the same level of favorability.
So when somebody says, I'm a MAGA Trump supporter and if you don't elect me, BLM's
gonna get you, okay? It doesn't have the intended effect. What it means is they're
standing there saying, I'm part of a group that you don't like and if you
if you don't vote for me a group that you like more than me is going to have
more power. That's why looking at those numbers and how terms are viewed can
help you guess what's going to happen. Based on those numbers 45% of Americans
when they hear, you know, when they hear I'm MAGA, they already don't like that
person. 45%. Only 24% are like, yay, go team go. It's a gap of 21 points. There
are far more people who view MAGA in a negative light, then view it in a
positive light. When it comes to BLM which for when you're looking at
terminology it's basically just become social justice movements in general. 38
percent view it positively and 40 percent view it negatively, meaning only
two percent, two points. As the MAGA movement campaigns and they talk about
BLM. They talk about all of these different entities or grievances. The
majority of Americans have a more favorable view of their grievances than
they do of them. Their campaigning is going to drive negative voter turnout.
It's going to send more people not to vote for their opponent, but to vote
against the MAGA candidate. It's a dynamic we're going to see as primaries
start heating up again, as people start going through making all those those
speeches, using the rhetoric they feel like they have to do to win a Republican
primary, all of that, because this time it's an election year, all of that will
by election year I mean a presidential election year, all of that will enter the
mainstream. It'll get a whole lot more coverage, which means those candidates
who use that rhetoric in the primary when it comes to the general, people are
going to remember it. As they identify themselves as MAGA they are eliminating
45% of the population. 45% off top view it as negative and only 25% view it or
24% view it as positive. The Republican Party is going to drive their own
negative voter turnout. It's gonna be wild. Anyway, it's just a thought. Y'all
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}