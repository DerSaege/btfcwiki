---
title: Let's talk about whether NY is still the weakest case....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=X0QoHBzQzXY) |
| Published | 2023/04/05 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Identifies New York as the weakest case among the legal entanglements facing the former president.
- Initially believed demonstrating allegations in the New York case might be challenging.
- Acknowledges that if even half of the claims in the indictment can be proven, it could lead to charges sticking.
- Notes that the method used to elevate charges from misdemeanor to felony in the New York case remains unproven.
- Speculates that the sentencing outcome might not significantly differ whether one or multiple charges stick.
- Mentions LawTube potentially dissecting and explaining the indictment process.
- Expresses certainty that LawTube will provide more insights into the legal aspects of the case.
- Considers the delay in the legal process as a result of the slow-moving justice system.
- Suggests that further developments in other cases may arise before significant progress in the New York case.
- Indicates a shift in focus towards determining what charges to indict the former president for in another case involving classified documents.

### Quotes

- "Initially, I thought they were going to have a hard time demonstrating some of what they were alleging."
- "Realistically, one is almost as good as thirty."
- "I think our justice system is broken."
- "There might have been another motive."
- "Anyway, it's just a thought."

### Oneliner

Beau believes New York remains the weakest legal case against the former president, citing unproven methods and potential delays, alongside a focus on another case involving classified documents.

### Audience

Legal analysts and concerned citizens.

### On-the-ground actions from transcript

- Stay informed about legal developments and analyses from reliable sources (exemplified).
- Monitor updates on various legal cases involving the former president (exemplified).

### Whats missing in summary

Beau's detailed analysis and insights on the legal entanglements facing the former president can best be understood by watching the full transcript.

### Tags

#LegalAnalysis #FormerPresident #NewYorkCase #JusticeSystem #DocumentCase


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about whether or not
I still think New York is the weakest case.
Something I have said for a while
is that out of the cases and the various legal entanglements
that the former president finds himself in,
that New York seemed to be the weakest case.
As soon as all of the information came out
and it was available for review,
questions started coming in, do I still think that? I still think it's the
weakest case, but I no longer think it's a weak case, if that makes any sense.
Initially, I thought they were going to have a hard time demonstrating some of
what they were alleging. Now, if they can demonstrate half of what they are
claiming they can in that indictment, I would imagine that something, one, of those charges
is going to stick.
And when it comes to sentencing, realistically, one is almost as good as thirty.
It probably wouldn't make a huge difference in the sentence.
So it's no longer about them actually being able to demonstrate what they're saying.
I think the weak part in that case is the mechanism used to turn it from misdemeanor
to felony.
And I don't necessarily know that it's weak, more that it's unproven, at least by what
I can find out.
I am certain that once they're done explaining the indictment language, the arraignment process
and everything, LawTube is going to be all over that.
And they will go through it.
Because as near as I can tell, the mechanism they're using to do this, it kind of relies
on allegations that aren't really proven or it relies on crimes that are more the
Fed's business and I it doesn't seem like it's been done before so it's
unproven. Now again the Law Tube people I'm certain they're gonna be all over
this and I will yield to whatever their determination is they they know a whole
lot more about this than I do. So I still don't think it is the strongest case. I
think out of the four main ones that we're looking at, it's probably the
weakest. But at this point it seems more weak based on procedure stuff than them
actually being able to demonstrate it. Now, one of the other questions that has
come in is why is it going to take so long to get to the next part of this?
The wills of justice turn slowly and all of that stuff. Say with me, our justice
system is broken. Now, at the same time, I wouldn't worry about it too much. I think
there are going to be more developments in other cases
between now and then.
I see that as incredibly likely.
When it comes to the documents case,
the more the information comes out about that,
the more little tidbits I hear, the more I
become convinced that that is probably the strongest case,
to the point where, right now, I don't think they're determining whether or not
they have a case or whether or not to indict.
I think they're trying to determine what to indict him for
and how hard they really want to hit him with charges.
The shift in some of the questioning
that we have heard about, I can tell you this.
If I was in a situation where somehow I wound up
with a bunch of wild classified documents
and didn't return them, and all of a sudden the questioning
was less about what did I know and when did I know it,
And it started to turn to, well, did
I have a specific interest in this topic?
I would be super concerned.
Because that moves it from something
that might just be the behavior of somebody
who is entitled to somebody who might have been up
to something that was bad.
beyond just endangering something through keeping
documents in the way that he was,
there might have been another motive.
So that case is definitely proceeding.
And I feel like there's a pretty good likelihood
that between now and the next major set of developments
in the New York case, there will probably
be movement on the documents case.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}