---
title: Let's talk about clarifying Trump NY coverage....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=G3pyLarZnSs) |
| Published | 2023/04/05 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former President Donald J. Trump went through the booking process, entered a plea of not guilty, and faced a lot of media coverage.
- Trump is not facing a conspiracy charge, despite initial incorrect reporting from some outlets.
- Reports claiming Trump could face 134 years in prison are inaccurate; this is a low-level felony with sentences likely to run concurrently rather than consecutively.
- Despite some Trump supporters showing up, the numbers were not significant, with only about 300 in attendance.
- One Congress member compared Trump to Jesus and Mandela, leading to opposition from the crowd.
- There was controversy over inflammatory rhetoric used by Trump and a tweet from his son featuring the judge's daughter, raising questions about conflicts of interest.
- This historic event marks a former President of the United States facing criminal charges, a total of 34 counts.

### Quotes

- "Trump is not facing a conspiracy charge."
- "Trump is not looking at 134 years."
- "What exactly is the conflict with the judge?"
- "A former President of the United States is facing criminal charges, 34 of them."
- "It's just a thought, y'all have a good day."

### Oneliner

Former President Donald J. Trump faces criminal charges amidst inaccurate media coverage and controversy over conflicts of interest, marking an unprecedented historic event.

### Audience

Journalists, Activists, Legal Experts

### On-the-ground actions from transcript

- Research and verify information before reporting (exemplified)
- Stand up against inaccurate media coverage by sharing corrected information (exemplified)

### Whats missing in summary

In-depth analysis and detailed breakdown of the inaccurate media coverage and potential conflicts of interest in the case.

### Tags

#DonaldJTrump #MediaCoverage #ConflictsOfInterest #HistoricEvent #CriminalCharges


## Transcript
Well, howdy there, internet people, let's bow again.
So the former president of the United States, Donald J. Trump,
went through the process today, was booked,
and entered a plea and not guilty.
During this process, there was a whole lot of coverage.
We are going to clear up some of that coverage
because a lot of outlets were more concerned with being first
than they were with being right.
So we're gonna clear up a whole lot of things
that got reported that are less than accurate.
First and foremost, Trump is not facing a conspiracy charge.
That's not there.
That's not there.
What it looks like happened is that the prosecutor
or somebody on the prosecutorial team
used that term during court.
And in an effort to get ahead of everybody, one outlet reported that that was one of the charges.
It is not. It is not.
This is a big material difference because a conspiracy charge definitely ups the possible penalties.
Didn't happen. But once one outlet reported it, a whole bunch of other outlets said that it was a charge
and cited the initial reporting without checking it themselves.
themselves.
So the next part, speaking of sentences, Trump is not looking at 134 years.
That's not going to happen.
That is being widely reported.
For that to occur, Trump would have to be convicted on all counts and be given the maximum
on all counts, and the judge decide that all sentences would run consecutively for a pretty
low-level crime.
This is a low-level felony.
This is not happening.
That's not real.
Generally speaking, sentences run concurrently.
You see this in a lot of reporting where the person is looking at, you know, 140 years
if convicted on all counts.
The U.S. justice system tends to run sentences concurrently, meaning if you are sentenced
to 10 years for one and five years for another, you do 10 years.
And the first five years counts for both sentences.
The former president of the United States would certainly be extended that courtesy.
The odds of Trump, no matter how you look at it, this would kind of be like a first
offense type of thing.
The odds that the judge would run those sentences consecutively is zero, and certainly not sentence
them to the max and then run them consecutively.
That is not going to occur in any world.
The reason this is important is because a lot of people view Trump as their savior.
And realistically, Trump might do a year suggesting that whatever the outcome would be, it would
amount to a life sentence might provoke people.
That's bad reporting.
That is bad coverage.
That's not realistic.
Now let's talk about Trump supporters that showed up.
Depending on the outlet that you're looking at, there were none, there weren't that many,
there were a whole bunch, the whole city turned out, really what happened?
There were about 300 that were in support of him.
To put that into perspective, when you are talking about high-profile cases, Johnny
Depp had more people show up for his civil case.
Those aren't big numbers.
There were a couple of people in Congress who showed up, one being Santos and one being
I don't remember her name, even though everybody keeps giving her free press for some reason,
the space laser lady.
She showed up.
She compared Trump to Jesus and Mandela.
I'm not joking.
She was eventually heckled by the people who showed up who were opposed to Trump.
There were probably 150 of those.
judge did talk, let's not say warn, talk about some of the inflammatory rhetoric
used by Trump. At roughly the same time, Trump's kid tweeted out an article
featuring an image of the judge's daughter. The article attempted to, it
looked like it was trying to imply some conflict because the judge's daughter
campaigned for Harris. I would like to know what the specific conflict is and
there's gonna be a whole video on this later because this is important. I would
like to know what the specific conflict is. When you are hearing about this, what
What exactly is the conflict with the judge?
Is it now an assumption that every judge and their family members have to be apolitical
for them to sit on a case?
Because I'm fairly certain I remember cases dealing with Trump going before Trump appointed
judges.
That's not actually a standard.
When you are hearing about that, try to discern what the specific allegation of a conflict
of interest is.
So those were the larger pieces that were not reported to my satisfaction.
A lot of this we'll get separate videos later and we'll talk about it more in depth, but
given as quickly as everything is moving, it seemed important to get this information
out.
This is now an unprecedented historic event.
A former President of the United States is facing criminal charges, 34 of them.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}