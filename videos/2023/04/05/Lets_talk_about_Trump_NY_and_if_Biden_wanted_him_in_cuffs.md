---
title: Let's talk about Trump, NY, and if Biden wanted him in cuffs....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=s2R3WAokXp0) |
| Published | 2023/04/05 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the Trump New York case, judges, and impartiality, with a focus on recent information surfacing.
- The judge in the case commented on Trump's rhetoric and social media statements and how they may be perceived.
- Trump's sons tweeted an article featuring the judge's daughter, insinuating a conflict of interest due to her involvement with the Harris campaign.
- Questions arise about the specific conflict of interest being suggested and whether political views of family members affect judicial decisions.
- Beau points out the inconsistency in implying judges must be apolitical, especially given cases involving Trump appointed judges.
- The essence of a judge lies in their ability to set personal beliefs aside, despite attempts to imply conspiracy.
- Beau dissects a far-fetched theory involving Biden orchestrating Trump's arrest, ridiculing the idea as implausible and insulting to intelligence.
- He argues that Biden, if he truly wanted Trump detained, could simply request a risk assessment from the intelligence community.
- Beau criticizes the leniency shown by Biden's Department of Justice towards Trump, contrasting it with how others in similar situations are treated.
- Despite the conspiracy theories, Beau stresses that the federal government is not actively pursuing Trump, evident in the leniency shown.

### Quotes

- "Those spreading this kind of information, they're just assuming that their base is not smart enough to figure it out."
- "The fact that Trump isn't awaiting the outcome of this inside of a room with bars is proof positive that the federal government, that Biden's DOJ, is not actually out to get him."
- "There is no man behind the curtain."

### Oneliner

Beau dives into Trump's case, dissecting judicial impartiality and debunking conspiracy theories, showcasing federal leniency towards Trump.

### Audience

Activists, Political Observers

### On-the-ground actions from transcript

- Reach out to the intelligence community for a risk assessment on individuals of concern (suggested).
- Stay informed about political proceedings and hold officials accountable (implied).

### Whats missing in summary

Insights on the importance of critical thinking and staying informed in political discourse.

### Tags

#Trump #JudicialImpartiality #ConspiracyTheories #FederalGovernment #BidenAdministration


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about the Trump New York case again, and we're going to talk
about judges and impartiality and some pieces of information that have surfaced and whether
or not they should weigh on the proceedings.
If you missed it, the judge in the case was talking about some of Trump's rhetoric and
some of the statements on social media and how they could be interpreted.
At pretty much the same time, his sons were tweeting out an article that contained an
image of the judge's daughter.
The implication in the article was, I don't even know if it was in the article, but the
implication that those spreading it were trying to create was that the judge's daughter, because
she had volunteered or worked with the Harris campaign, somehow that makes the judge incapable
of being on this case, and the judge should recuse himself.
Okay, but what is the specific conflict of interest that's being suggested?
Certainly the Republican Party is not saying that all judges are apolitical, or that they
should be, or that they could only sit on cases that don't involve politicians or politics.
It doesn't even make sense.
And I'm certain they do not want to go down the road of saying a family member's political
views would impact the judge's decisions.
Trump had some of his cases go before Trump appointed judges.
That seems like a much larger conflict to me.
But the idea behind a judge is that they can set that stuff aside.
And for the most part, they do.
But since we know that that isn't what they're trying to imply, they're not trying to say
that all Democratic or Republican Party judges should be off the bench, I mean, obviously
that's not what they're saying.
They're trying to imply some form of conspiracy.
In fact, some of them flat out said that, basically saying that it was all engineered.
So let's run through that real quick, and then we're going to get to the reality of
what would happen if Biden actually wanted Trump in cuffs, detained in a cell.
Okay, so Biden makes that decision according to this theory.
So Biden reaches out to Harris, who reaches out to a low-level campaign worker, who reaches
out to her dad, who's a judge, who then reaches out to a DA, who then decided to campaign
on that beforehand, but that's just magic.
And then the DA reaches out to Cohen to get some evidence and to other people to get evidence,
then takes it before the grand jury.
And somehow all of them play along with this, probably they have a cousin who once voted
for Kennedy or something, and that's how this kangaroo court, this witch hunt came to be.
You understand how looney tunes that sounds, right?
It's ridiculous.
Those spreading this kind of information, they're just assuming that their base is not
smart enough to figure it out.
their base will parrot whatever they're told and that they can't think for themselves.
Let's talk about what would actually happen if Biden made the decision to put Trump in
a cell.
That's what he really wanted to do.
Would he have to go through all that?
No, absolutely not.
All he has to do is reach out to the IC, to the intelligence community, and ask for a
product.
an assessment of the risk of somebody who constantly meets with foreign
nationals, has millions of dollars at their disposal, their own plane, and is
known to have retained classified material even after they were asked to
give it back. Ask for a risk assessment of that. That product would then be
shared with FBI counterintelligence.
Let's be super clear on something.
The federal government, Biden's DOJ, has been ridiculously lenient when it comes to
the former president.
Any other person who did what he is accused of doing, anybody that was accused of that,
It had that quantity of classified material and was retaining it even after they were
told they needed to give it back.
There was a subpoena, so on and so forth.
After all of that, anybody who did that would be in a cell and they would sit there and
wait until the case was determined, especially if they were known for meeting with foreign
nationals had millions of dollars in a plane. The fact that Trump isn't awaiting
the outcome of this inside of a room with bars is proof positive that the
federal government, that Biden's DOJ, is not actually out to get him. They are
extending him every courtesy, far beyond what can reasonably be expected.
There is no man behind the curtain.
The world is just a chaotic, scary place.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}