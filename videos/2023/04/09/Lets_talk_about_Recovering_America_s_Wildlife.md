---
title: Let's talk about Recovering America's Wildlife....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=faE9BC4MKzE) |
| Published | 2023/04/09 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the Recovering America's Wildlife Act, which has been reintroduced with bipartisan support.
- About one-third of America's wildlife faces an elevated risk of extinction, impacting the environment and other species.
- State-level plans exist to assist in wildlife recovery, but federal funding of $65 million/year is insufficient.
- The Act aims to provide $1.4 billion annually to states and tribal governments to address wildlife conservation.
- The funding will help mitigate disease among species, reestablish migration routes, and control invasive species.
- Urges action now to prevent irreversible damage, drawing a parallel to the water crisis out west.
- Emphasizes the importance of acting before the situation worsens and becomes irreversible.
- Points out the successful conservation efforts in the US when adequate funding and resources are present.
- Stresses the need for collective will to address wildlife conservation effectively.
- Concludes with a call to seize the current moment and work towards a sustainable future.

### Quotes

- "If we don't take the opportunity, if we don't start to address this now, it will be regretted later."
- "The warning signs are there and we have the opportunity to do something about it before it is out of control."
- "For years and years and years, you had people talking about how bad the water situation was going to be out west, and nobody wanted to do anything about it."
- "The US has actually proven itself capable of doing it if the funding is there, if the resources are there, if the will is there."
- "Do we have the will to actually work to solve it before it becomes a headline-grabbing issue?"

### Oneliner

Beau presents the urgent need for the Recovering America's Wildlife Act, stressing the critical role of adequate funding in preventing irreversible wildlife loss and environmental impact.

### Audience

Conservationists, Wildlife Enthusiasts

### On-the-ground actions from transcript

- Advocate for the passing of the Recovering America's Wildlife Act by contacting your senators and representatives (suggested).
- Join local conservation efforts or wildlife preservation organizations to contribute to positive change in your community (implied).

### Whats missing in summary

The full transcript provides detailed insights into the importance of the Recovering America's Wildlife Act and the critical need for immediate action to prevent irreversible wildlife loss and environmental damage.

### Tags

#WildlifeConservation #RecoveringAmericasWildlifeAct #Funding #EnvironmentalImpact #UrgentAction


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about the Recovering America's
Wildlife Act.
And I know some people are already like, yeah,
it's been reintroduced.
It has bipartisan support.
It has been reintroduced.
For those that don't know, this has come up before,
and it didn't make it.
But there's another chance here.
So what we're going to do is we're going to talk about what
it is, what it would do, and why we need it. We'll start with why we need it.
According to the National Wildlife Foundation, about a third of all of
America's wildlife has an elevated risk of extinction. About a third. That's huge.
Keep in mind, when species go, it changes the environment. When species go, they
often impact other species because there's a lack of food or something and
get a domino effect. The warning signs are there and we have the opportunity to
do something about it before it is out of control, but we have to take the
opportunity. There are state-level plans for for assisting in bringing wildlife
back. The problem is the federal government offers funding for this
through a grant program that totals 65 million dollars a year, roughly. It's
nowhere near enough. It is not enough money and this is one of those
situations where you know people say you can't solve everything by throwing
money at it. In this case the problem is a lack of funding. Yes throwing money at
it would be super helpful. So what does what does the Act do? The main thing it
does is that it creates that funding. $1.4 billion a year to go out to the
various states and tribal governments and everything. It creates the source of
funding to work to address the issue. And as far as what the funding would go to,
it would go to helping mitigate disease among species, it would help reestablish
migration routes, it would help control invasive species, that's what the money's for.
If you want Native American wildlife to survive, these are things you kind of have to do.
And it's there, and this is one of those moments.
For years and years and years, you had people talking about how bad the water situation
was going to be out west, and nobody wanted to do anything about it.
And now it's really visible, but in many cases, for a lot of the area, it's almost too late
to really do anything.
You have to wait for the environment to correct itself.
And when it does that, it doesn't care about where the cities are.
This is a similar situation.
have the people telling you this is going to be an issue, it's gonna be a
problem, we have to start working now to stop it. The question is do we have the
will? Do we have the will to actually work to solve it before it becomes a
headline-grabbing issue? I hope that we do. The US actually has a pretty good
record when it comes to, we're going to work to bring this species back, we're
going to work to fix this particular conservation issue. They can do it. The
US has actually proven itself capable of doing it if the funding is there, if the
resources are there, if the will is there. In fact, I'll put a little cartoon down
below that talks about how it was done with the Mexican gray wolf in the
United States. But this is a moment, this is an opportunity where it's back in, it's
back in the Senate. If we don't take the opportunity, if we don't start to address
this now, it will be regretted later. Anyway, it's just a thought. Y'all have a
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}