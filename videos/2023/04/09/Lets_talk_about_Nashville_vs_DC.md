---
title: Let's talk about Nashville vs DC....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=KwEYufJwbKs) |
| Published | 2023/04/09 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the similarities between the events in Nashville and at the Capitol was initially resisted but later acknowledged as wise.
- There was no property damage in Tennessee, unlike at the Capitol.
- Contrary to expectations, there were very few arrests in Tennessee, with the Highway Patrol describing the protests as peaceful.
- Despite assumptions, there were no injuries reported in Tennessee.
- Neither weapons nor improvised items were found with the crowd in Tennessee.
- Allegations of rifles being stashed at a nearby hotel or violent chat logs were not present in Tennessee.
- Unlike the Capitol incident, no one was hit with an American flag in Tennessee.
- The crowd in Tennessee did not chant to harm anyone.
- The National Guard was not required in Nashville, unlike at the Capitol.
- The purpose of the Nashville gathering was to petition for redress of grievances and peaceably assemble, not to undermine the Constitution.

### Quotes

- "One was to undermine the Constitution. The other was to exercise rights explicitly protected in it."
- "Seems like they're not at all similar."
- "They're not the same."

### Oneliner

Addressing supposed similarities between Nashville and the Capitol reveals stark differences in intentions and actions, debunking any false equivalence.

### Audience

Online commentators

### On-the-ground actions from transcript

- Question false equivalencies and challenge misleading narratives (suggested)
- Promote accurate understanding and critical thinking among online communities (suggested)

### Whats missing in summary

The nuances and detailed analysis provided by Beau in the full transcript are missing in this summary.

### Tags

#Nashville #Capitol #FalseEquivalency #CriticalThinking #CommunityPolicing


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about Nashville
and the Capitol.
And we're going to look for similarities.
We're going to do this because in that recent video
about Nashville, in the comments had a bunch of people saying,
you can't talk about what happened in Nashville
without talking about what happened at the Capitol.
You have to address them both.
And there were a couple of comments like that,
and most of them got less than diplomatic answers to me.
For that, I apologize.
But at the time, I was thinking that I just really
didn't need to address some manufactured talking
point and false equivalency.
But the more I thought about it, the more I think they're right.
I think it is a good idea to show the similarities
between the two.
I think that's wise.
And I apologize for not seeing that sooner.
OK, so let's look for similarities
for those that are wondering, yeah, it's a monkey paw video.
OK, how much property damage occurred in Tennessee?
None, none.
According to the Tennessee Highway Patrol, none.
I mean, so I guess that's not a similarity.
That's not the same.
That's different.
But certainly, there were a lot of arrests, right?
No.
No, there weren't.
In fact, Highway Patrol said they were peaceful.
But even with the Highway Patrol saying that, I mean,
obviously, like, people got injured.
They had to go to the hospital because they're similar, right?
No, nope, that's not the same either, weird.
But even though nobody got hurt, certainly the crowd,
they had weapons or improvised stuff
that they could use as one, right?
No?
Fascinating.
Well then, obviously, because they're so similar,
There's allegations of rifles being stashed at a nearby hotel or something, just in case.
No?
Didn't happen either, huh?
Yeah.
Okay.
What about chat logs?
There's chat logs of people discussing possible acts of violence, right?
That would make it similar.
But those don't exist either.
Man.
Certainly somebody got hit with an American flag.
That happened.
No? Wild.
I don't know. What else you got?
The crowd? Did the crowd chant to hang anybody or anything?
No? I guess not.
I mean, because that would have made it similar. A little bit.
What about when the National Guard showed up? Did they have a hard time getting them out?
That's right. In Nashville, the National Guard wasn't required. Wait. Sorry. Sorry.
Okay.
All right. Well, what about its purpose? Certainly, the purpose was the same, right?
The people that showed up in Nashville,
they had a similar goal.
They were there to, you know, force
the wrong person
to retain control of the federal government, or in that case,
the state government, and engage in a self-coup?
Was that part of it?
No.
They weren't there to undermine the Constitution at all.
That wasn't part of the plan of the people behind it, huh?
Instead, they were there petitioning
for a redress of grievances, peaceably assembling.
Because remember, despite what the GOP politicians said, the state troopers said it was peaceful.
And they were engaging in free speech.
So I'm not sure what the similarities are, to be honest.
Ten things, and none of them are the same, wild, defining characteristics there.
Seems like they're not at all similar.
like that's something they just made up to trick people, a false equivalency.
I mean, let's be real.
The crowd in Tennessee, they went into a building yelling.
I mean, I guess it's probably more similar to like a college championship game than what
happened on the 6th.
One was to undermine the Constitution.
The other was to exercise rights explicitly protected in it.
They're not the same.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}