---
title: Let's talk about doomsday planes and Facebook memes....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=iyc1WGJ0Fss) |
| Published | 2023/04/09 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concern about a meme on Facebook regarding the Navy building a new doomsday plane for nuclear war with China.
- Describes doomsday planes as airborne command and control centers paired with the US strategic arsenal.
- Mentions the Navy is indeed acquiring new doomsday planes, each costing around $250 million.
- Clarifies that the purchase of these planes is not a sign of imminent nuclear war with China.
- States that the current Navy planes in this role are old and need to be replaced with newer airframes.
- Compares the replacement of the airframe to upgrading an old food truck with a new truck to ensure mission success.
- Notes that the Navy plans to have three test planes first before acquiring nine more in the future.
- Emphasizes that this modernization is a normal process as equipment ages, not a preparation for immediate conflict.
- Addresses the accuracy of the Facebook meme regarding the cost and acquisition of the doomsday planes.
- Concludes by advising against getting news from Facebook and suggesting it's just a routine equipment upgrade.

### Quotes

- "Don't get your news from Facebook."
- "That part [about doomsday planes] would be scary."
- "It's just normal modernization as time goes along."

### Oneliner

Beau explains the truth behind a Facebook meme about the Navy acquiring doomsday planes, clarifying it's routine modernization rather than preparation for war with China.

### Audience

Internet users

### On-the-ground actions from transcript

- Verify information from reliable sources before believing or sharing content online (implied)
- Stay informed through credible news outlets rather than social media platforms (implied)

### Whats missing in summary

The detailed nuances and explanations provided by Beau in the full transcript can offer a deeper understanding of military modernization processes and dispel misinformation online.

### Tags

#Navy #DoomsdayPlanes #FacebookMemes #MilitaryModernization #Misinformation


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about Facebook memes
and whether or not you should be concerned about something
that the Navy is buying.
And just kind of run through everything,
because if it has already started and news about this
just really kind of came out, I would imagine, eventually,
memes like this will be everywhere.
So we'll just go ahead and kind of go through it now.
So here's the message.
My dad showed me a meme from Facebook.
Before sending you this, I checked online and found out a little bit, but not enough
to know if the meme was true.
It said the Navy was building a new doomsday plane to get ready for nuclear war with China.
It said they were spending $250 million on the plane and that this was proof things were
much worse than they, whoever they are, are letting us know.
wondering if you know anything about this. Yes, yeah. So let's start with
what's a doomsday plane because that sounds really scary. They are airborne
command and control centers. They're communications platforms really, but
they're designed to be paired with the US strategic arsenal, NUKEs. So there is a
basis for this meme. There's a hint of reality in it. These are not new things. They've been
around a long time. I think the first one was like the EC-135. They always have really
creepy nicknames like Looking Glass or Night Watch. I think there's one called Judgment
So, they exist.
Their whole gig is to just manage the unthinkable if it occurs.
The Navy is, in fact, getting new ones.
And the number is pretty close, $250 million, I want to say the Navy asked for $213 million.
So I mean even that for a Facebook meme, that's about as accurate as you're going to get.
It's not for one plane, it's for three, and they're test planes.
Is this because nuclear war with China is imminent?
No, no.
The current Navy planes that fill this role, they're older.
The airframe is, it's old, I want to say it's 25 years old, something like that.
My understanding is that they aren't even putting a whole lot of new tech into these.
They're just switching out the airframe.
I don't really know how to explain that.
Picture a food truck.
The kitchen layout in the back of the food truck, it's fine.
The truck itself, it's old.
They need a new truck.
That's basically what's going on.
They are expensive, but it's because these are, this is kind of a can't fail mission.
If it ever has to be used, it has to work.
So they are expensive aircraft.
I think by the time it's all said and done, the Navy wants the three test planes to work
everything out and then they will get nine at some point in the future. This
isn't them preparing for something that is going to happen immediately given the
fact that they're not using new tech and it's just about replacing the airframe.
If this was something that was imminent they would just order new versions of
the type they already have. Like they would just order new production of them.
They're taking the time to use a different airframe and I want to say
it's like a souped-up C-130 if you care about that kind of stuff. So the meme
itself, I mean as far as Facebook memes go, that's pretty accurate. I mean the
The number is close, it is actually happening as far as it being linked to imminent war
with China or something like that, no.
It's just normal modernization as time goes along and the planes get older.
This has happened numerous times before.
I think the only scary part about this would be for those people who had no idea these
planes existed, and you're just finding out that there are aircraft whose entire job is
to basically coordinate the destruction of the world.
That part would be scary, but as far as this purchase, there's nothing abnormal about
it.
It's just normal getting rid of older equipment and getting newer stuff.
Don't get your news from Facebook.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}