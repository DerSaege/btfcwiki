---
title: Let's talk about 3 unrelated but similar incidents....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=aV1hLVLZMyc) |
| Published | 2023/04/18 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Three separate events in Kansas City, New York, and New Mexico share a common element.
- In Kansas City, an 84-year-old white man shot a 16-year-old black kid who knocked on his door by mistake.
- The shooter in Kansas City has been charged with assault and armed criminal action, with questions raised about the charges.
- In New York, a homeowner shot at a group of friends who mistakenly pulled into their driveway, killing a 20-year-old woman.
- The shooter in New York has been charged with second-degree murder after an hour-long standoff with law enforcement.
- In Farmington, New Mexico, law enforcement went to the wrong house, resulting in the homeowner being shot and killed by officers.
- Law enforcement in Farmington did not force entry or use deceit to enter the property, leading to questions about potential charges.
- The incident in Farmington may be deemed a harmless error, allowing for a justified shoot based on the mistaken presence of law enforcement.
- Constant fear-mongering and keeping people on edge can lead to innocent lives being lost due to heightened tensions.
- Beau questions the impact of creating an environment where individuals constantly live in fear, likening it to a self-fulfilling prophecy.

### Quotes

- "Maybe it's not a good idea to constantly fear monger and keep people on edge."
- "You have a lot of innocent people being lost because people feel like they live in a combat zone."
- "It's becoming a self-fulfilling prophecy."

### Oneliner

Three separate incidents across the country reveal the dangers of fear-mongering and living in constant fear, leading to tragic outcomes.

### Audience

Communities, Activists, Bystanders

### On-the-ground actions from transcript

- Advocate for community policing and de-escalation training (implied)
- Support initiatives that address racial biases in law enforcement (implied)
- Join local organizations working towards police accountability (implied)

### Whats missing in summary

The full transcript provides a deeper insight into the consequences of fear-mongering and the need for community-driven solutions to prevent tragic incidents.

### Tags

#Crime #RacialJustice #CommunityPolicing #FearMongering #Accountability


## Transcript
Well, howdy there, internet people.
It's Bell again.
So today we are going to talk about three separate events,
three seemingly unrelated stories, one in Kansas City,
one in New Mexico, one in New York.
And although these incidents are unconnected,
they share a common element.
And there's a similarity that when people started
sending messages about the different events,
it got confusing because they are very similar.
We'll start with the one in Kansas City.
A 16-year-old black kid is going to pick up their siblings,
goes to the wrong house, knocks on the door,
rings the doorbell. The homeowner, an 84 year old white man, opened the door and
fired through the storm door, the glass door, striking the kid once in the arm, I
think, and once in the head. The kid is alive, which is nothing short of a
miracle. The officials there, law enforcement there, says they do believe
there is a racial component to it. The shooter has been charged with assault
and armed criminal action. There have been questions about the charges and why
those charges were leveled instead of other things. One, the laws there are
really weird. And two, I don't think this is necessarily going to be the final
charges. I think there's a pretty good possibility that the charges change as
time goes on on this one. The next event is in New York. A group of friends are
driving around in a very rural area, they get lost pulling to a driveway and they're
turning around, they're leaving, and the homeowner comes out and pops off two
rounds. One of them strikes the car and kills a 20-year-old woman. The car
flees, the people flee. Law enforcement shows up, takes about an hour to get the
shooter out of the house, has been charged with second-degree murder. Then
there is a situation in Farmington, New Mexico where law enforcement went to the
wrong house and they knock on the door, ring the doorbell, they don't get an
answer. They're standing there for a little bit and right about the time that
they start to realize they may not be in the right spot, the homeowner opens the
door and is holding a gun and law enforcement opens fire and kills him.
They also exchange fire with the wife before it is discovered what is
happening. People have asked about that and about potential charges. I've looked
at the footage. I don't know. I don't know. This is very different than other
situations where law enforcement goes to the wrong house. In this case, they
didn't force entry and, you know, they didn't lie to get a warrant or anything
like that. It looks like a mistake, in which case it will probably be deemed a
harmless error, even though it was definitely not harmless. Which means
they're allowed to be there. Them being there wasn't illegal. Therefore, when the
homeowner had a weapon, the shoot would be deemed justified. That's your most
likely scenario. I'd be very surprised if they filed charges on that one, but we'll
have to wait and see. Three incidents all over the country. Maybe it's not a good
idea to, maybe it's not a good idea to constantly fear monger and keep people
on edge. You have a lot of innocent people being lost because people feel like they live
in a combat zone and it's becoming a self-fulfilling prophecy. Anyway, it's just a thought. Y'all
So have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}