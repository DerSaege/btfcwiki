---
title: Let's talk about Republicans pursuing losing policies....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=-G6f3gQlAvw) |
| Published | 2023/04/18 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the question of why the Republican Party is pursuing losing positions, particularly focusing on reproductive rights.
- Federal-level Republicans are notably quiet on pushing through policies because they understand these are losing positions.
- The doubling down on losing positions is happening at the state level, in red states or gerrymandered states where these positions are electorally favorable.
- State-level Republicans, aiming to secure their positions, are putting the federal-level Republican Party in jeopardy by pursuing these losing positions.
- The risk lies in negative voter turnout driven by pursuing unpopular policies, potentially leading to gerrymandered districts flipping.
- The situation arises as Republicans focus on winning locally while potentially endangering the party as a whole.
- Federal Republicans may be calling on state reps to stop pursuing losing positions, but state reps are entrenched in their local echo chambers.
- The state reps prioritize staying in office and use what they perceive as a surefire strategy based on vocal support in their districts.
- Despite potential risks, Republicans continue to pursue these losing positions due to the immediate electoral benefits in their districts.
- The disconnect between state and federal Republicans may ultimately lead to consequences for the party as a whole.

### Quotes

- "Republicans are doubling down on losing positions, but which Republicans?"
- "State-level Republicans are putting the federal-level Republican Party in jeopardy."
- "They may actually end up driving so much negative voter turnout."
- "Federal Republicans may be calling home to state reps, y'all have to stop."
- "They're in their own social media echo chamber."

### Oneliner

Beau explains why the Republican Party continues to pursue losing positions, jeopardizing the party as a whole by prioritizing local wins over national strategy.

### Audience

Political observers

### On-the-ground actions from transcript

- Contact local representatives to express concerns about pursuing losing positions (implied).

### Whats missing in summary

Beau's analysis provides insight into the dynamics of the Republican Party's strategy and potential risks of pursuing losing positions.

### Tags

#RepublicanParty #Politics #ElectoralStrategy #StatevsFederal #Gerrymandering


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about a question that
came in a bunch of different times about different issues.
But at its heart, it's the same question.
And it has a very, very simple answer.
It's just counterintuitive when you're first looking at it.
So we'll go through that today.
And the question at its heart is why is the Republican Party pursuing losing positions?
Like why are they taking a losing position on issues and just doubling down on it?
And the question came in in relationship to a whole bunch of different things.
The most common was reproductive rights.
So we'll just roll with that.
But rest assured, if you sent in a message that has that core question, why are Republicans
doubling down on this horrible position that polls poorly and they've already suffered
backlash from, this applies to you too.
It applies to your question.
Republicans are doubling down on a bunch of losing positions, but which Republicans?
Is it McCarthy and McConnell?
No, right?
In fact, federal-level Republicans,
they've been super quiet.
Even with the position the House is in,
they're not pushing through anything.
It's weird.
It's because they know they're losing positions.
They don't want to double down on them.
So they're just kind of riding things out,
trying not to make too many waves,
engaging in grievance stuff and culture war stuff,
but not pursuing policy because they
they have a losing policy. They don't want to drive negative voter turnout,
which is where people show up not to vote for the Democratic candidate, but to
vote against them. And if they pursue more restrictions on reproductive rights
at the federal level, they will generate that, and they know it. That's how they
wound up with unlikely voters during the midterms.
So they're being really quiet.
It's not all of the Republican Party that's doing this.
It's the state level.
And which states?
It's either states that are genuinely red, like they are just red states, where people
would kind of support this.
Or they are states that are successfully gerrymandered, one of the two.
That's where this is happening.
That's where you see Republicans doubling down on losing positions.
Because in their district, it's not a losing position.
It'll get them reelected.
And they're just kind of like, well, I can secure my spot at the state level.
Those federal reps, they can worry about themselves.
And that's what's happening.
The state-level Republican Party is putting the federal-level Republican Party in jeopardy
by continuing to push this stuff.
And generally, they think they're safe.
The state-level reps, they believe they're safe.
I don't know that that's true, though.
In the red areas, yeah, those that are legitimately red, sure.
In the areas that are gerrymandered, not so much.
Because when those districts get drawn up, they're drawn up based on normal voter turnout.
If negative voter turnout is driven and a whole bunch of people show up just with the
goal of ousting Republicans, particularly ousting them at the federal level because
that's what draws most people in, they're not going to vote for the Republican at the
state level.
So they may actually end up paying for this themselves, even though right now they don't
think they will.
But generally speaking, when you are looking at this weird situation that is developing
where polling is just very lopsided, it is landslide territory, and the Republicans are
still pursuing something that they know is a losing position, that's why in their very
narrow district or in their particular state, it's a winning position there.
So they're pursuing it and putting the rest of the Republican Party in jeopardy.
I think if they continue, they actually will end up driving so much negative voter turnout
that you will see districts that are successfully gerrymandered flip because it is when those
Those districts are drawn up.
They are based on normal voter turnout.
If 10% extra of the Democratic Party shows up, it's not gerrymandered anymore because
they're normally designed to look somewhat competitive.
Look somewhat competitive.
They're really not, but they're designed to look that way.
If the Democratic Party has higher than expected turnout, and the Republican Party doesn't
because, well, they feel pretty safe, those unlikely voters, it's a good possibility
that those gerrymandered districts get flipped, at least a few of them.
So that's why this is happening.
It isn't a, it's not a party-wide thing.
In fact, I'm sure those at the federal level are probably calling home to state reps into
the state party, like, y'all have to stop.
But they're not going to listen to them because they are in their own social media echo chamber.
They're in that situation.
The state reps are just listening to the people in their districts.
the most vocal in their districts and they just see a a method that they
perceive to be a surefire way to stay in office and you know all of them want
what every first term administration wants. Another term. Anyway it's just a
thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}