---
title: Let's talk about McCurtain County Oklahoma and a recording....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=MqFHR1lN6Tg) |
| Published | 2023/04/18 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduction to discussing an incident in McCurtain County, Oklahoma.
- Mention of a recording that surfaced involving county officials with inappropriate dark humor.
- Focus on the main concern: county officials expressing upset over not being allowed to beat or hang black people.
- Mention of the official reactions, including calls for resignations of specific county officials.
- Involvement of state police force, Oklahoma State Bureau of Investigation, and the attorney general's office in looking into the matter.
- The FBI now has a copy of the recording, prompting questions about their interest in it.
- Speculation on the Civil Rights Division of the FBI potentially investigating the County Sheriff's Department due to the disturbing content of the recording.
- Clarification that the FBI possessing the recording doesn't confirm an active investigation.
- Mention of the possibility of civil rights violations against black Americans in that location.
- Uncertainty about whether the Civil Rights Division will launch an investigation based on the recording's content.

### Quotes

- "Hearing top law enforcement officials in a county say that they're upset that they can't
  engage in extrajudicial killing, that might be something that the Civil Rights Division
  views as an indicator."
- "It is appalling. It is appalling. It is not something you want from public officials."

### Oneliner

Beau delves into a recording from McCurtain County, Oklahoma, revealing disturbing sentiments from county officials and potential civil rights violations being investigated by the FBI.

### Audience

Community members

### On-the-ground actions from transcript

- Contact local advocacy organizations to support potential civil rights investigations by federal agencies (implied).
- Stay updated on developments and spread awareness within your community about the importance of accountability and transparency in law enforcement (exemplified).

### Whats missing in summary

The full transcript provides a detailed account of the concerning recording involving county officials in McCurtain County, Oklahoma, shedding light on potential civil rights violations and federal investigations. 

### Tags

#McCurtainCounty #Oklahoma #CountyOfficials #FBI #CivilRights #Investigation


## Transcript
Well, howdy there, internet people.
Let's go again.
So today we are going to talk about McCurtain County,
Oklahoma.
And we're going to talk about a recording that
has surfaced there and what's on it.
And we'll go over the reactions from various officials
to the recording.
And then we'll get into what appears
be the main question that most people have, and it involves one specific organization
having interest in it.
And we'll talk about where that might lead if they do, in fact, have interest in it.
Okay, so what happened?
A recording surfaced of county officials in McCurtain County, and there is a lot of dark
humor on it, stuff that's not really appropriate from people in public office.
But that's not what most people are focused on.
During this conversation, it appears to turn to the fact that at least some of them are
upset that they are no longer allowed to beat or hang black people.
some of these county officials are involved in law enforcement.
Yeah this is obviously a problem.
So what have been the official reactions?
The governor came out and was pretty clear on this one and well I'll just read it.
In light of these events, I am calling for the immediate resignation of McCurtain County
Sheriff, Kevin Clardy, District 2 Commissioner Mark Jennings, Investigator Alicia Manning,
and Jail Administrator Larry Hendricks.
On top of that part, there also appears to be a discussion about the possibility of taking
out a local reporter, a local journalist as well.
Okay, so resignations have been called for.
It appears that the state police force there, I think it's called the Oklahoma State Bureau
of Investigation, they are looking into it.
The attorney general's office in Oklahoma is looking into it.
And the part that most people are interested in, apparently the FBI has a copy of the recording
now.
people have asked why the FBI would be looking into it. Because, at least in the
parts I've heard, what's on the tape, it's appalling. It is appalling. It is not
something you want from public officials. But there's nothing illegal on it, at
least in the parts that I've heard. That being said, the Civil Rights Division of
the FBI, that they've been really active lately in trying to send messages.
Hearing top law enforcement officials in a county say that they're upset that they can't
engage in extrajudicial killing, that might be something that the Civil Rights Division
views as an indicator, and they use that to start looking into the County Sheriff's Department
as a whole. They need to find out how pervasive that attitude is and whether or not it was
ever acted upon in the way that was described. And then, even if it wasn't, if that attitude
is present in that department, then it seems likely that there were civil rights violations
of black Americans in that location, even if it didn't go to that extreme. So they're going
to probably look into that. But it's worth noting that just because the FBI has a copy of the
recording doesn't mean that they're actually investigating. The reporter could have sent it
to them or the governor could have just been like, send this to anybody who might care.
And normally you don't hear much from the Civil Rights Division until they're ready
to do something.
So we don't actually know that they're going to launch an investigation, but this is definitely
the type of recording that might prompt them to.
So that's why they might be interested in it.
I'll keep following this and keep y'all updated on how it develops.
Anyway, it's just a thought y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}