---
title: Let's talk about helping change along....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=9KqJUqxKI5Q) |
| Published | 2023/04/18 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the importance of change, jokes, memes, and how change is a gradual process.
- Sharing a scenario where a family member sends bigoted jokes about the LGBTQ community.
- Explaining how to approach and help someone who is showing interest in learning and changing.
- Suggesting tactics like making the person explain the jokes they share to understand their impact.
- Emphasizing the recycled nature of offensive jokes as a signal of belonging to a specific group.
- Encouraging starting with educating family members about the hurtful nature of such jokes.
- Advising on taking baby steps in educating and understanding complex topics like gender identity.
- Stating the significance of preserving family values and using that as a starting point for change.
- Recommending exposing the person to family members from marginalized communities to reduce fear and prejudice.
- Stressing the gradual nature of the process and the importance of patience and persistence in fostering change.

### Quotes

- "Change takes time. Baby steps."
- "Exposure kills fear. Fear is the root of most of this."
- "Your family is your family and nothing comes between that type of stuff."
- "Give it time to digest before you give them something new."
- "Start by framing it around you want to keep your family."

### Oneliner

Beau provides tactics to help someone change their bigoted views by starting with family education, understanding offensive jokes, and gradually introducing new perspectives, stressing the importance of patience and persistence.

### Audience

All community members

### On-the-ground actions from transcript

- Start educating family members about the impact of offensive jokes (implied)
- Encourage the person to explain offensive jokes to understand their impact (implied)
- Slowly introduce new perspectives to the person, starting with small government conservatism (implied)

### Whats missing in summary

The full transcript provides detailed insights on initiating difficult but necessary dialogues to foster understanding and change in individuals holding prejudiced views.

### Tags

#Change #Education #Family #Understanding #Patience


## Transcript
Well, howdy there, internet people.
It's Bob again.
So today, we are going to talk about change, and jokes,
and memes, and how change takes time,
and how to help if somebody in your life
indicates that they're interested in changing,
even if it's just a tiny, tiny sign that they're
interested in learning more and we'll go through some tactics and some steps that
I think might be helpful and we're gonna do this because I got a message. There's
a lot of details in this and I'm not gonna go through all of it but generally
speaking you have a relative. There's a family and there is a relative in this
family who sends memes and text jokes and stuff like that that are, well, they're bigoted.
They're about the LGBTQ community.
And that's part of it.
And the person is a family member that they're trying to kind of break free, says, started
talking to him about it, and he replied with, yeah, I don't know how to feel about it.
Met my niece's wife when I went to visit sister, I'm going to need some help understanding
all of that stuff.
the Redneck Whisperer, can you help me explain this to him? He is, last I knew, a
Trumper as well as a Christian. How can we help him to become a better person?
First, don't undervalue, I'm gonna need some help understanding all that stuff.
He's literally asking. So on some level he understands that there's an
education gap. That he understands there's things that he does not know. So
you have an opening. Now, starting from the beginning, the memes and texts and
stuff like that. The jokes. You don't get it. Whatever the joke is, you don't get it.
I know I've given this advice before multiple times on the channel. It's
because it works. It works. Make them explain the joke. Whatever it is. And
let's be real, when it comes to jokes about this community, there's only like
three. They're just recycled. Make them explain it. I don't get it. I don't
understand. Why is that funny? I don't get it. The reason you do
this is that a couple of different reasons but these jokes they're not funny
right? It is. It's the same recycled stuff over and over again because it's not
actually a joke. It's not meant to be funny to the people in the in-group. It's
just a signal. It's a virtue signal. I'm part of your club. I make this joke.
That's why the same thing can be recycled over and over again and they
pretend that it's something original because it's not actually about being
funny. It's about identifying yourself as part of their little club. When they have
to explain that joke to somebody outside of that in group, they realize a number
of things. One, it's not funny. Two, they realize it's the same joke because they
have to explain it to you multiple times because when they tell you different
variations of it, you don't get any of them. Even though they've already
explained it to you every single time. I don't get it. I don't understand. Drag it
out of them. They also tend to realize that it is a signal. They start to realize
that those outside of that echo chamber, outside of that end group, don't find it
funny and in fact it's offensive. So this part is kind of separate from your
main question here but I'm gonna need some help understanding all of that
stuff. I would start with family. I would start there. I know like your your
instinct in a situation like this is give them all the information they could possibly
want. Don't do that. Change takes time. Baby steps. Baby steps. I would start with, you
know, you have family members that are part of this community and these jokes that you're
telling. I mean, they're told by people who hate your family. If somebody is a
Christian and somebody is, you know, right-wing and is a Trumper, whether or
not they actually care about their family, that's up for debate. But that
signal about caring about your family, that's huge. Tap into it and use it. Your
Your family is your family and nothing comes between that type of stuff.
These jokes are told by people who want to hurt your family in some cases.
It helps.
Start there.
Get him to acknowledge that he wants to preserve his family and that's the most important thing.
Should be easy.
That should be an easy sell.
far as educating and helping understand all that stuff.
Some of it can be done with the jokes.
Get the family part laid out first.
But then when you're asking them to explain it, you know, the joke, the helicopter joke
or whatever, I don't get it, I don't understand.
would you identify as a helicopter? That doesn't make any sense. Well, it's because people
can identify as whatever they want now. Is that how it works? Because I kind of thought
that it was really people only identifying like, I mean, for most people, it's only like
one of three options that people are picking. I don't, I don't really get it. No, well,
yeah, I mean, they're saying that, but that's not what they are. And it gives you that opportunity
to explain the difference between gender and sex.
Some of the jokes will provide an opportunity
to provide a little bit of education at a time.
Don't overload them.
Little bits and pieces.
Get them to acknowledge one piece at a time.
Give it time to digest.
This is not going to be a quick process.
And your key element here, once this person is in a situation where they're
not going to be offensive, maybe get them to spend some time around their family
members who are part of that community. Exposure kills fear. Fear is the root of
most of this. So if it was me and you have this opportunity handed to you, you
know, I'm gonna need some help understanding. Yeah, definitely take them
up on it, but do it slowly and start by framing it around you want to keep your
family. Your family is the most important thing in the world and start
there. It's it's gonna be a long thing though and I mean if you're in this
position you don't really have a choice. I mean it sounds like you got picked, not
the other way around. So you have to commit to those conversations. Maybe one
week. Just little bits of information at a time. And if it was me, I would start by
trying to move them from where they're at to what we've been kind of
talking about all week. Those small government conservatives who their
Your general point of view is not my business.
I'm cool with you because even if I don't like it, this isn't my business.
You're not hurting anybody.
It's certainly not the government's place.
Start there and then you can move them further.
try to take them from far right to far left. Baby steps. If it works, you never know what
you end up creating. If you can actually, to use your term, make him be a better person,
he might do the same thing with other people and it just snowballs. It's worth the effort
Just understand it's going to be a lot of effort,
and it's going to take time.
But the key parts, established family is most important.
I don't get the joke.
And then little bits of information at a time.
Give it time to digest before you give them something new.
Anyway, it's just a thought.
You all have a good day.
Thanks for watching!
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}