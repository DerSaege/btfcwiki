---
title: Let's talk about Texas, books, and a decision....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Zdswj1rIImM) |
| Published | 2023/04/04 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- News out of Texas: a Texas county board decided to remove a bunch of books from a library, leading to a lawsuit by patrons.
- Federal judge ruled that removal of books from libraries based on viewpoint content discrimination violates the First Amendment.
- Many jurisdictions across the country are using libraries to remove LGBTQ-friendly content, regardless of the viewpoint.
- People making these decisions often lack understanding of how the Constitution and First Amendment work.
- Judge ordered the books to be returned to the shelves within 24 hours, showing certainty in the outcome.
- Ongoing culture war involves unconstitutional actions by individuals.
- Supreme Court likely to uphold decisions against content discrimination to avoid favoring one side of the country.
- Overall, the issue revolves around the unconstitutional removal of books from libraries based on viewpoint discrimination.

### Quotes

- "The federal judge decided that the plaintiffs are likely to succeed on their viewpoint discrimination claim."
- "In most cases, people making these decisions do not understand how the Constitution works."
- "Supreme Court understands that if content discrimination is allowed, it is unlikely that the conservative side comes out ahead."
- "It's an ongoing thing."
- "Judge ordered the books to be returned to the shelves within 24 hours."

### Oneliner

A federal judge ruled against viewpoint discrimination in removing books from libraries, showcasing the ongoing unconstitutional culture war on LGBTQ-friendly content.

### Audience

Librarians, Book Lovers, Advocates

### On-the-ground actions from transcript

- Contact local libraries and offer support in defending against viewpoint discrimination (suggested).
- Join community advocacy groups working to protect LGBTQ-friendly content in libraries (suggested).

### Whats missing in summary

Importance of supporting libraries in upholding the First Amendment and protecting diverse content.

### Tags

#Libraries #FirstAmendment #LGBTQ #ViewpointDiscrimination #SupremeCourt


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about some news out of Texas.
And we're going to talk about books and content
and what libraries can do and what they cannot.
Because this was a subject of some debate in a Texas county
where the board made the decision
to remove a bunch of books.
And that library and the board were
sued by some of the patrons of the library.
This is something that is happening all over the country.
And you're likely to see a very similar outcome in most places.
The federal judge that heard the case decided that the plaintiffs have made a clear showing
that they are likely to succeed on their viewpoint discrimination claim.
Although libraries are afforded great discretion for their selection and acquisition decisions,
the First Amendment prohibits the removal of books from libraries based on either viewpoint
content discrimination. That decision, and I imagine that eventually it's going
to be boilerplate in these decisions. There are a lot of jurisdictions around
the country that are using a, that are using libraries in their attempt to
scapegoat a group, and they are focusing on removing content that is LGBTQ friendly, or
not even necessarily friendly if it just mentions them, or perhaps the author.
It's an ongoing thing.
These decisions have been made by people that in most cases do not understand how the Constitution
works.
More importantly, they don't understand how the First Amendment works.
The decision you just heard is going to be repeated.
In this case, the judge seemed incredibly certain that the plaintiffs were going to
win, mainly because in this case, they actually said that it was due to the content is why
they were removing them.
And the judge was so certain of the outcome of this he ordered the books to be returned
to the shelves within 24 hours.
So odds are by the time you watch this they're already back on the shelves.
Again there's a lot of culture war stuff that is going on right now, most of it being perpetrated
by people who have, you know, we the people profile pictures that is just blatantly unconstitutional.
And in this case, you're likely to see this outcome upheld and repeated over and over
and over again, and even once it gets to the Supreme Court, because those on the Supreme
court understand that if content discrimination is allowed, it is unlikely that the conservative
side of the country comes out ahead in that exchange.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}