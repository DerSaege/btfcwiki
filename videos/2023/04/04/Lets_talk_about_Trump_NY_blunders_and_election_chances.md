---
title: Let's talk about Trump, NY, blunders, and election chances....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=gggADDbTe-A) |
| Published | 2023/04/04 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing Trump's chances of re-election post-indictment in New York from an electoral perspective, not a legal one.
- Trump lost in 2020 due to failed leadership and his leadership style, not just post-election actions.
- Polling shows 60% of Americans approve of Trump's indictment in New York, while 62% of independents support it.
- 79% of Republicans disapprove of Trump's indictment in New York, leaving 21% who don't disapprove.
- Unlikely that Trump, lacking majority support from Americans, independents, and even a significant portion of Republicans, can win re-election.
- Authors of opinion pieces often write based on desired outcomes rather than realistic predictions.
- Even if Trump secures the Republican primary, the lack of broad support makes a successful 2024 run unlikely.
- Trump's influence remains significant, but the prospect of him returning to the White House seems improbable.
- People's memory of Trump's actions, especially around the January 6th events, will likely impact his political future.
- Despite fears, Beau believes Trump's chances in 2024 are slim, barring unforeseen circumstances.

### Quotes

- "It is incredibly unlikely that Trump ever sleeps in that White House again."
- "Even if he wins the primary, that's not winning numbers. Those are horrible numbers."
- "People will always remember his attempt at altering the outcome of the election."
- "I don't think he stands a chance in 2024. Not a realistic one."
- "It's just a thought, y'all have a good day."

### Oneliner

Analyzing Trump's slim chances of re-election post-indictment in New York through polling data and electoral perspectives, it appears highly unlikely that he could secure another term in the White House.

### Audience

Voters, political analysts

### On-the-ground actions from transcript

- Educate others on the polling data surrounding Trump's indictment and its potential impact on his political future (implied).

### Whats missing in summary

The detailed breakdown of polling data and public opinion regarding Trump's indictment in New York and its potential implications for his political future. 

### Tags

#Trump #Election2024 #PollingData #RepublicanParty #PoliticalFuture


## Transcript
Well, howdy there, internet people.
Let's vote again.
So today we are going to talk about New York,
and Trump, and blunders, and mistakes,
and campaign donations.
And we're going to go over the reality
of a lot of the opinion pieces
that have been coming out lately,
and talk about this strictly from an electoral view.
because a number of questions have come and we have mainly been talking about
the the legal aspects of all of this. But there's the other concern can you please
talk about whether Trump being indicted in New York increased his chances of
election. I've seen so many articles saying it will and calling the arrest a
blunder a mistake or a campaign donation to Trump. I'm not talking about the legal
side of things. I'm talking about people's political perception. I think a
lot of us are scared this will lead to him getting re-elected and him being
even worse than before. Okay, there are a whole lot of people who write opinion
pieces based on what they want to happen rather than what they think will happen
happen, and even less on what data says will happen.
Let's start with this because this is a reminder that I think people need every once in a while
as a very vocal but small group within the Republican Party continues to push Trump.
He lost in 2020, partially because of his own failed leadership during the public health
issue and losing a whole bunch of his own voters, and partially because a whole lot
of people saw his leadership style.
This loss occurred before his attempt to subvert, change, undermine, whatever term you want
to use, contest the election.
This loss occurred before January 6th.
This loss occurred before everything he's done since.
He couldn't win before.
He's not going to be able to win now.
Not with everything else that's going on.
Not with everything else that has happened since.
He was President Reject before the things that most people view as the worst thing that
he did.
Okay.
for me to say that and that that's a historical point that you can look at but
let's look at this another way. Let's look at the polling on whether or not
people approve of his indictment because that's happened already. Polling has
already occurred as to Americans reactions to him being indicted in New
York. 60% of Americans agree with it. They approve of his indictment in New York. 60%.
If 60% of Americans approve of your indictment, it is unlikely that you will
win the White House because this doesn't include people who disapprove of the
indictment but wouldn't vote for him because that that's another demographic
that is contained within those who disapprove of the indictment are people
who don't want him to go to jail but also don't want him to be president but
you can safely assume those who believe he should be indicted aren't gonna vote
for him. But that's 60% across the board. What about the independents? Because
that's what he needs. If he wants to win, he has to get all of the Republican
Party and a majority of the independents. When you are just measuring
independents, 62% support his indictment in New York. They approve of it. And
And again, there's going to be a whole lot of the remaining 38 percent who disapprove
of the indictment who wouldn't vote for him.
And now, perhaps the most telling part.
What about Republicans?
What do Republicans think of it?
79 percent disapprove.
Okay, 79% disapprove of him being indicted in New York.
What about the other 21%?
One out of five Republicans, people in his own party, they don't disapprove of him being
indicted in New York.
It seems incredibly unlikely that somebody that doesn't have the full support of their
party doesn't have a majority of independents and doesn't have a majority of Americans
in general is going to win re-election.
Why do people keep putting out these articles?
Because people read them.
Because it is scary.
There's a whole lot of demographics that are terrified of another Trump term.
So playing on that fear, they can put out these opinion pieces and people will read
them.
That's from the publisher's perspective.
Most of the authors are writing what they want to happen rather than what they actually
think will.
It is incredibly unlikely that Trump ever sleeps in that White House again.
That doesn't seem like something that's going to happen.
Does that mean that he's no longer a concern?
No, no, because he still wields a whole lot of control and influence over the base and
over specific Republican politicians.
importantly, he set that tone, that leadership style, that many Republican
candidates may seek to emulate in the future. So he's still a concern, but the
odds of him ever being in the White House again, that doesn't seem
likely. One out of five Republicans could not bring themselves to say that they
disapprove of his indictment in New York. Even if he wins the primary, that's
that's not a... those are not winning numbers. Those are horrible numbers. Yeah,
he can fundraise off of it, but to be honest he's not doing great at that. Um,
and he can mobilize and energize that ever-shrinking base that he has, but it's
It's not enough for electoral success, not for the White House.
If he couldn't win before all of this stuff happened, it seems really unlikely that he
could pull it off at some point in the future.
People will always remember his attempt at altering the outcome of the election, of all
of the claims that went out, things that undermined basic institutions in this country.
People will always remember the footage of the 6th.
And whether or not people think he should be held criminally responsible for what happened
on the 6th, I think most people definitely lay that event at his feet.
Even if they don't think what he did was illegal, they think he set the stage for it.
I can't really envision a scenario in which Trump mounts a successful 2024 run right now.
Not a realistic one.
I can come up with wild cards, but not anything that really stands a chance at actually happening.
I get the fear, especially if you're part of one of the groups that he frequently scapegoats
and targets.
I get it.
But I have a feeling by the time the indictments are through, he's going to be a footnote.
I don't think he stands a chance in 2024.
Not a realistic one.
It would take some wild change of events.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}