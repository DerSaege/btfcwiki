---
title: Let's talk about top Florida Dems getting arrested....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Yj9N5kETK1w) |
| Published | 2023/04/04 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Americans anticipate proceedings involving prominent Republican and Democratic figures today, with all eyes on New York.
- In Tallahassee, Florida, a peaceful assembly in support of reproductive rights clashed with law enforcement over First Amendment rights.
- Florida officials instructed demonstrators to disperse by sundown, leading to a sit-in protest.
- Demonstrators, including prominent political figures, were arrested for trespassing after defying orders to leave.
- Despite overshadowing headlines, other ongoing fights across the country impact people's rights significantly.
- Beau urges viewers to not treat unfolding events as a spectator sport but to actively participate and be aware of broader issues affecting rights.
- He predicts more politicians may face minor offenses due to authoritarian approaches to constitutionally protected rights.
- Beau notes the significance of not overlooking less flashy news that impacts lives directly.
- The transcript concludes with Beau recommending watching the footage and commenting on a political figure's nonchalant reaction to being arrested.
- Beau underlines the importance of staying engaged and active in current events.

### Quotes

- "You really couldn't have scripted this better."
- "We can't forget that there are other things going on that have a very tangible impact on people's lives."
- "At this point it's not a spectator sport. You have to get involved."
- "I am not sure I have ever seen anybody as apathetic to getting cuffed as Ms. Freed was in that."
- "It's just a thought."

### Oneliner

Americans watch prominent figures in New York, while Florida's clash over reproductive rights underscores the need for active engagement beyond headline events.

### Audience

Political activists and engaged citizens

### On-the-ground actions from transcript

- Support peaceful assemblies for causes you believe in (suggested)
- Stay informed and engaged in ongoing fights impacting rights (implied)

### Whats missing in summary

The emotional nuances and subtleties of Beau's delivery and the impact of direct engagement with current events.

### Tags

#USPolitics #ReproductiveRights #FirstAmendment #Activism #Engagement


## Transcript
Well, howdy there, internet people, it's Beau again.
So today is the day, right?
Today's the day a whole lot of Americans
have been waiting for, and I would imagine
that most of the nation's eyes are turned towards New York,
waiting for proceedings involving
a prominent Republican figure.
However, on the other end of the country,
it was Democratic Party officials being picked up.
In Florida, in Tallahassee, Florida, there was an assembly, and by all accounts, it was a peaceful assembly.
It was a demonstration in support of reproductive rights because the state of Florida is attempting to push through a
six-week ban.
At some point, and for some reason, the government officials in the area, I guess they determined that those involved
with the demonstration  had used up all of their First Amendment tokens.
Law enforcement instructed them that they
needed to be gone by sundown, like it was some old Western.
Those involved with the demonstration
did not agree with that interpretation
of the First Amendment and remained.
They basically engaged in a sit-in.
They sat in a circle, sang songs, so on and so forth.
Shortly after sundown, law enforcement
showed up to issue the final warning.
At this time, the circle was singing, Lean On Me.
You really couldn't have scripted this better.
Law enforcement issued their command to leave,
and the circle was holding hands.
They raised their hands and began to sing even louder,
at which point law enforcement initiated
the process of removing them.
Among those picked up were Florida Senate Minority Leader Book and Florida Democratic
Party Chair Nikki Freed.
My understanding is that those involved were transported to Leon County Jail on trespassing.
It's an incredibly minor charge in Florida for those who are concerned.
Now, while what's happening in New York is certain to get a whole lot more press, because
it has a greater capacity to get clicks, we need to keep in mind that there are other
fights going on all over this country.
And these fights, the outcome determines people's rights when it comes to the First Amendment,
the right to privacy, to their right to autonomy, all kinds of things.
What is likely to happen with all of the Trump stuff is that it is going to dominate headlines.
We can't forget that there are other things going on that have a very tangible impact
on people's lives.
So while what's going to happen in New York is important and it's something that needs
to be watched and paid attention to, remember that at this point it's not a spectator sport.
You have to get involved.
And I would suggest it's probably a, there's probably a high likelihood that we see more
and more politicians end up with minor offenses on their records because in many areas there
is a more authoritarian approach to people who are exercising their constitutionally
protected rights.
Now that being said, that's the news.
If you happen to get a chance to watch the footage, it's worth it.
I am not sure I have ever seen anybody as apathetic to getting cuffed as Ms. Freed was
in that.
It just looked like pure annoyance.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}