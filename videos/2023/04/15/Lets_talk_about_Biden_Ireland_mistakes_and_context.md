---
title: Let's talk about Biden, Ireland, mistakes, and context....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Z7j2hY3CSMc) |
| Published | 2023/04/15 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden made a mistake while referencing a sports team during his visit to Ireland, calling them "the black and tans," causing embarrassment due to the historical context.
- The Black and Tans were a paramilitary police force used by the British to suppress Ireland, known for brutality and creating enduring repercussions on Irish history.
- Mentioning the Black and Tans to Irish people is sensitive due to the wounds of history that are still fresh in Ireland.
- Although the diplomatic spat caused by Biden's mistake won't lead to major tensions between the US and the UK, it was an inappropriate slip-up that should have been avoided.
- Beau compares Biden's blunder to a hypothetical scenario where the British Prime Minister references "wounded knee" while visiting Native American reservations in the US, illustrating the cringeworthy nature of such remarks.
- It's vital to be mindful of historical sensitivities when visiting other countries and to acknowledge that events depicted in movies and literature have real-life impacts on communities.

### Quotes

- "Mentioning the Black and Tans to Irish people is sensitive due to the wounds of history that are still fresh in Ireland."
- "Although the diplomatic spat caused by Biden's mistake won't lead to major tensions between the US and the UK, it was an inappropriate slip-up that should have been avoided."

### Oneliner

Biden's misstep referencing the Black and Tans in Ireland showcases the importance of historical sensitivity in international interactions.

### Audience

Travelers, diplomats

### On-the-ground actions from transcript

- Be mindful of historical sensitivities when visiting other countries (implied)
- Acknowledge the real-life impacts of historical events on communities (implied)

### Whats missing in summary

The full transcript provides a detailed explanation of why referencing the Black and Tans in Ireland is highly sensitive and showcases the importance of historical awareness in diplomatic interactions.

### Tags

#Biden #Ireland #HistoricalSensitivity #Diplomacy #TravelAwareness


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Biden and Ireland
and mistakes and context and comparisons
because a number of Americans have asked
why this was such a big deal.
So if you have no idea what I'm talking about,
the president of the United States recently went to Ireland
And while they were there, Biden said something
that he shouldn't have.
It was a mistake, he misspoke,
but the term that came out was one that I'm certain
the British government would rather not have been spoken.
Okay, so what happened?
Biden was meaning to reference a sports team.
I think he was talking about how they lost, and instead of saying the name of the team,
he said the black and tans.
People familiar with Ireland, if you didn't know this occurred, you're either laughing
or cringing right now, one of the two, right?
On a personal level, I do have to admit that my favorite dark Brandon moments are when
he unintentionally outs himself as an Irish nationalist. And this has happened a number
of times, but normally when he does it, it's in a private setting. It's not something
that is something that causes a diplomatic tiff.
So what are the black and tans, right?
They were a paramilitary police force used by the British in an attempt to subdue Ireland.
They were called that because they had mix-matched uniforms.
And the media, when they're talking about this, they're throwing out the word brutality
a lot.
That doesn't cover it.
were bad, really bad. So it's obviously something our British allies would
prefer not be mentioned, especially to a bunch of Irish people. And even if you
were to take the moral aspects of it just out of it, and forget about that for
a second, the security clampdown that the Black and Tans imposed, it created a
dynamic, and it in many ways generated a type of fighting that you still see used today.
There are many who would draw a straight line from the dynamic that arose in
Ireland at the time of the Black and Tans to pretty much every non-state
actor today. From organization to structure to tactics, just there are a
a lot of similarities and there were a lot of downstream effects from the introduction
of the black and tans there.
So again, not something the British government would want brought up.
The other thing to remember is that while the black and tans have been gone for a very
long time, the trouble in Ireland, it's still kind of fresh.
It's not the scars of history.
They're still wounds.
They're closed wounds, but they're still wounds.
And visitors to Ireland should be sensitive to that.
So that's what occurred.
Now the diplomatic spat that arose from this, is it a big deal?
Is it going to cause major tensions between the US and the United Kingdom?
No, no, it's not.
But it's one of those things that shouldn't have happened, but he misspoke.
It did happen.
To put it into a context that Americans would be more familiar with for comparison, I want
you to picture the British Prime Minister coming here and they decide they want to tour
the United States and see real America.
And along the way they have a speaking engagement at a reservation.
And when they're getting off the bus, well, they trip.
And as they get on stage, they're just like, man, my wounded knee.
Cringing, right?
Same thing.
Same basic premise.
Something that the United States would certainly prefer not be spoken about by a foreign official
that's visiting.
That's what occurred.
And it's kind of on that level of just it being very cringeworthy.
So while it isn't a huge deal diplomatically, it is one of those moments, if you plan on
visiting Ireland, just to remember that what is often featured in movies or plot devices
in books here, it occurred in their homes.
It occurred in their communities.
And it's not always well received to reference it.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}