---
title: Let's talk about rights, grinding to a halt, and a republican question....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=n23xpo5C9GI) |
| Published | 2023/04/15 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the viewpoint of a small government conservative who believes in strong fences making good neighbors.
- Advises listeners to listen for the phrase "It's not my business" as a cue that they might be dealing with a small government conservative.
- Points out that the Democratic Party is not the one introducing legislation on social issues but the Republican Party.
- Challenges Republicans to scrutinize if the legislation introduced by their party makes their lives better or just makes someone else's life worse.
- Suggests that cultural war legislation introduced by the Republican Party is a distraction from their lack of policy ideas.
- Encourages individuals to question why the Republican Party is focusing on legislation that hurts others rather than helping the American people.

### Quotes

- "They're not your friend. They're not the Republican party that they used to be."
- "Go through Republican legislation and with each piece ask yourself does this make your life better or does it just make somebody else's life worse?"
- "All of this culture war nonsense is just something to distract you from the fact that the Republican Party doesn't have any policy ideas anymore."

### Oneliner

Beau challenges Republicans to question if the legislation introduced by their party truly benefits them or just harms others, pointing out the distraction of cultural war from policy issues.

### Audience

Conservative Republicans

### On-the-ground actions from transcript

- Examine Republican legislation to see if it truly benefits the American people (suggested).
- Question why certain legislation is being introduced by representatives and how it impacts others (implied).

### Whats missing in summary

A deeper understanding of the motivations behind Republican legislative actions and the importance of questioning the impact of such legislation on society.

### Tags

#Republican #SocialIssues #Legislation #PoliticalAnalysis #Challenges


## Transcript
Well, howdy there, Internet people.
Let's bow again.
So today we are going to go over a question
from a Republican.
And it deals with why the Democratic Party
is grinding everything to a halt
over a topic that they just don't get.
They don't understand why it's such a big deal.
And I think that this is a genuine,
small government conservative, they still exist, a few of them anyway, and I think it's in good
faith. So we're going to go through it, I'm going to answer the question, and I'm going to provide
a challenge to all Republicans. Okay, I'm one of those Republicans who watch your channel for
explanations of what Libs are thinking. I have a genuine question I hope you answer. Before that,
But I want to say, I don't mind trans people.
What people do with their body isn't my business, but they're a very small population percentage
wise.
Can you explain why liberals are so dead set on fighting Republicans on this?
It seems there are more important things.
I honestly don't give a care what bathroom people use or what pronoun they use.
But I also don't see the point in grinding everything to a halt over it."
See that phrase?
It's not my business.
Do you ever want to know if you're dealing with somebody who is really a small government
conservative?
Listen for that phrase.
If you're talking about social issues and you hear that phrase, that's a cue to tune
in because they very well might be a small government conservative.
What this means is they may absolutely despise whatever the activity is, but that's not
the government's place.
Whatever it is that's being done, you're doing that on your side of the fence and strong
fences make good neighbors.
So it's not my business, I don't want to get involved in it.
speaking these are people when it comes to social issues they keep their opinion to themselves.
These are very much judge not lest ye be judged type of people.
Okay, can you explain why liberals are so dead set on fighting Republicans on this?
It's a small population percentage wise.
right. You're right. Which means they can't fight for themselves. They don't have the
numbers, right? In an electoral setting, they don't have the numbers. So somebody
has to stand up for their right to do what they want to do on their side of
the fence. You've said it yourself. You don't care what bathroom people use. You
don't care what pronouns they use. It's not your business, right? You're just out
they're trying to live your life and be polite. Just guessing. So the democratic
opposition to this legislation is based on the idea that people have the right
to live their life as they choose. That's it. That's why they're doing it. But here's
the question. Is it the Democratic Party that's introducing legislation about
this. It's not, is it? It's the Republican Party. It's the Republican Party that is
introducing legislation to curtail people's rights, to curtail people's
ability to do things on their side of the fence. You have Republican legislation
trying to micromanage what books can go into a library now. I mean, I would
But imagine that if your five or six year old or ten year old or whatever is getting
books from the library and as a parent you don't know what they are, that's on you.
That is on you.
There's a bunch of stuff in a library children shouldn't access.
It's up to the parent to make that decision and I'm sure you would agree with that because
you don't want the government involved in parenting, right?
It's the same thing. It isn't the Democratic Party grinding everything to a halt on this.
It's the Republican Party trying to curtail people and trying to inject themselves into
somebody else's fenced-in area and tell them what they can do. You don't want it done
to you. I'm certain from this message you don't want it done to you. Neither do most
libs. So they're standing up for that small percentage of people because they
don't have the voting power to do it on their own. Here's a challenge I have for
you because you're right there are things that should be more pressing. This
shouldn't be a topic. People should just be allowed to live their lives. I think
you would agree with that and there are topics that the government really should
be focusing on. So here's the challenge. Go through Republican legislation and
with each piece ask yourself does this make your life better or does it just
make somebody else's life worse? Ask yourself that question and if you answer
it honestly you're gonna find out that all of this culture war nonsense is just
something to distract you from the fact that the Republican Party doesn't have
any policy ideas anymore. Those big issues, they don't have a clue as to how
to address them. They complain, they talk about them, but they don't do anything
about it. But they can't be seen to be doing nothing, so they introduce cultural
war legislation to make other people's lives worse in the hopes that their base
will look down on them and say, well, I'm in a better place than them.
It's not the Democratic Party that you're mad at here.
If you go through their legislation and you actually look at it, none of it's designed
to help you.
The person who is stereotypically a small government conservative, none of it's to
help you.
It's to hurt somebody else and make you think they're doing something.
They're not your friend.
They're not the Republican party that they used to be.
I know there are a whole lot of people that are going to be like, Republicans are always
bad.
And I mean, okay, but I would imagine there was a point in time that for small government
conservatives, they at least did something for you.
They don't do anything for you anymore because they're not small government that they want
to inject themselves into everything, they don't want to represent, they want to rule.
This is a perfect example of it.
Take me up on that challenge, go through the legislation that your representative has introduced
or sponsored, co-sponsored, look at your state legislature and ask yourself, does this stuff
help you or does it just hurt someone else?
And then ask why they're grinding everything to a halt to hurt their fellow American.
Anyway it's just a thought.
have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}