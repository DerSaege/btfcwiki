---
title: Let's talk about the RNC, Milwaukee, trouble, and loyalty....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=X5qEs3HYLRE) |
| Published | 2023/04/15 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Republican National Committee chose Milwaukee for the first primary debate for the Republican presidential nominee.
- Team Trump got upset over the Young Americas Foundation's involvement in the debate, seeing them as pro-Pence.
- Loyalty and division issues have arisen between Trump loyalists and the rest of the Republican Party.
- The RNC's loyalty pledge, requiring support for the nominee, is seen as ineffective given the internal party divisions.
- Trump's behavior towards other nominees and potential winners is questioned, doubting his adherence to the loyalty pledge.
- Massive divisions and animosity within the Republican Party are already evident, with factions power-hungry and ready to attack each other constantly.

### Quotes

- "They are turning most of their anger towards the RNC."
- "The RNC has decided to move ahead with their loyalty pledge, which is not going to be worth the paper it's written on."
- "Every time they give a concession or they try to team up, it will anger others."
- "There is now so much animosity between the factions."
- "Y'all have a good day."

### Oneliner

The Republican Party faces internal strife over loyalty, with divisions and animosity escalating, potentially harming unity efforts.

### Audience

Political observers, Republican Party members

### On-the-ground actions from transcript

- Monitor and actively participate in local Republican Party events and meetings (implied)

### Whats missing in summary

Insight into potential strategies for mitigating internal conflicts within the Republican Party.

### Tags

#RepublicanParty #InternalStrife #LoyaltyPledge #Division #PoliticalObservations


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about the first Republican debate, Milwaukee, loyalty, and
disruptions early on.
Okay, so the Republican National Committee has decided the first primary debate for the
presidential nominee for the Republican Party will be in Milwaukee in August.
Almost immediately there was an issue because Team Trump, Trump World, got super mad that
one of the partners in this debate will be the Young Americas Foundation.
Trumpworld sees this group as being heavily aligned with Pence.
For once, it's not all like make-believe.
I would say that, yeah, they do lean towards Pence.
But at the same time, it's a debate.
You're supposed to get up there and argue.
So I don't really know why it's such a big deal, but it has turned into one and it has
already created a grievance and a problem between those who are loyal to Trump and the
rest of the Republican Party.
They are turning most of their anger towards the RNC.
They're trying to showcase it as the Republican establishment, trying to sandbag Trump and
just make sure that he just can't win no matter what.
And I mean, I think that's a little overblown, but the interesting part is the divisions
have already started.
And my guess is they're going to get worse and they will probably get really nasty.
In direct contradiction to that, the RNC has decided to move ahead with their loyalty pledge,
which is not going to be worth the paper it's written on.
Basically, anybody who wants to run as a Republican and try to get the nomination, they have to
pledge to support whoever wins the nomination.
If they don't do that, they won't be platformed.
If they don't say, hey, whoever wins, I'm going to support them.
Okay, first, let's be real for a second.
You do have multiple factions within the Republican Party and they are not friends.
Do you honestly see Trump supporting anybody else if they were to win?
No, of course not, right?
He's going to make fun of them and do everything he can in the Trumpiest way possible to talk
about their failing numbers and how the Republican Party would be better off if he was the nominee.
And you have people who are openly opposed to Trump who are running.
So sure, they're going to take the loyalty pledge.
And then whoever loses, I would imagine it would be almost immediate that they find something
that the new nominee did that they just, they can't get behind and they're sorry.
They've had a change of heart and they can't support them.
If Trump was to get the nomination, they could literally just wait until the next time he
He sends out one of his little off-brand tweets.
There will be something in it that they could find appalling and that would end their support.
With Trump, I don't even think he's going to pretend that he cared about the pledge.
He's just going to be like, yep, you're all losers now, because that's going to be totally
on brand for Trump.
It hasn't started.
It hasn't started yet and you are already seeing massive divisions start to occur and
you are seeing the RNC do everything that they think is the right move to try to bridge
those gaps between the factions and in all honesty, it's probably going to make it worse
as time moves on, because every time they give a concession or they try to team up and
allow one of the potential nominees to, I don't know, pick a partner or have influence
over a partner or make a statement, it will anger the others.
And there is now so much animosity between the factions and they are all so power hungry
they're going to be taking shots at each other constantly. You can expect that to
start immediately and escalate from now until the end of the primary. Anyway, it's
It's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}