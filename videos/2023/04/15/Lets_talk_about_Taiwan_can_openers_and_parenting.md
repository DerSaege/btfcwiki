---
title: Let's talk about Taiwan, can openers, and parenting....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=4TQS8vMFnUw) |
| Published | 2023/04/15 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the connection between Taiwan, can openers, a perceived shortage, and parenting.
- Responds to a viewer's question about the implications of going to war over Taiwan.
- Shares a personal story about asking his dad, a gunnery sergeant, about the potential conflict.
- Mentions that China's invasion of Taiwan in war games usually fails.
- Describes the high cost projected for both Taiwan and the United States in the event of conflict.
- Points out the difficulty of China invading Taiwan due to its challenging nature.
- Emphasizes that the US strategy in such a scenario focuses on air and sea defense.
- Notes that the US doctrine involves being able to fight against two nearest competitors simultaneously.
- Concludes that a perceived shortage of a specific type of ammunition wouldn't change the overall strategy.

### Quotes

- "If your kids ask a question, even adult children, take the time to answer, if you can."
- "The reason China hasn't invaded and just taken Taiwan back is because it's really hard."
- "US doctrine is to be able to fight our two nearest competitors at the same time."

### Oneliner

Beau explains the implications of a conflict over Taiwan and the US strategy, focusing on air and sea defense.

### Audience

Viewers interested in understanding the potential implications of conflicts involving Taiwan and US military strategies.

### On-the-ground actions from transcript

- Educate yourself on international relations and military strategies (implied).

### Whats missing in summary

In-depth analysis and additional context on the geopolitical implications of potential conflicts involving Taiwan and the US military strategy. 

### Tags

#Taiwan #USMilitary #Conflict #Parenting #Geopolitics


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about Taiwan and can openers
and a perceived shortage and parenting
and what all of these things have to do with each other.
The actual question that came in on the message
is pretty simple to answer.
There's some additional information
that came in with the question.
And I'm going to read it because I
imagine that there are people who might need to hear it. Okay, after watching your video about the
ammo and MTG space laser lady, I started wondering what would happen if we had to go to war over
Taiwan and thought about how that might become a problem then, even if it's not one now. I asked
my dad, who is a gunnery sergeant in the Marines, he was deployed the whole time I was growing up
and he is super macho and doesn't know how to relate to women, so we don't have the
greatest relationship to begin with. I thought showing interest in this might spark a conversation.
Wrong. He said, quote, that's a stupid question. It wouldn't matter. Well, thanks dad. The first
time I ever saw a video of yours, you taught me how to use a can opener.
So Daddy Bo, teach me to use a can opener here. Why is it a stupid question?
Man, those can opener references are brutal. I will put the video down below.
Short version, the parenting part. Let's get that out of the way.
If your kids ask a question, even adult children, take the time to answer, if you can.
Okay, so why is it a stupid question? It's only a stupid question if you're a gunny and you know a
whole lot about this. Okay, so the prospect of China and the U.S. coming to blows over Taiwan
has been war-gamed over and over and over again. I want to say, I want to say
there was just a study done this year where it was war-gamed 24 times already.
The outcome? Pretty much always the same. There's not a lot of outliers here. China's
invasion of Taiwan fails. However, it comes at an incredibly high cost for
both Taiwan and the United States.
Taiwan is devastated.
The United States, I want to say most projections say the U.S. is going to lose thousands of
people and a whole bunch of ships and two carriers, two carriers.
For historical comparison, the last time the U.S. lost a carrier was during World War II.
happen very often, losing two, it's a big deal. Two modern carriers, that's
massive. But that's the expectation. So what does this have to do with anything?
Well, it's an air and sea game. It is an air and sea game. The reason China
hasn't invaded and just taken Taiwan back is because it's really hard. Like in
And this isn't one of those things where China's just not up to the task.
No, this would be hard for any military.
It is a difficult prospect.
So that's why they've never attempted it.
Defending it, the whole idea behind it would be to interrupt the invasion in the air or
on the sea.
If the invasion actually, if they got troops on the ground and got like a head start and
started occupying, it is incredibly unlikely that the US would try to take it back.
So the whole goal would be to stop it before it happened, air and sea.
Why would the US not want to try to take it back?
For the same reason China hasn't attempted to take it.
It would be really, really hard.
It would be very costly.
So what does this have to do with 155 millimeter shells?
They're not used.
Air and sea, they're not really used.
So the U.S. part of that, it wouldn't factor into it.
That's why it wouldn't matter because they're not really used.
I know somebody's going to bring up the AGS.
Yeah, those aren't even the same kind.
I know they're 155 but I don't think they're actually compatible and if I'm not mistaken
I actually stopped using it.
But there, just because I know somebody's going to break it up.
So the reason a perceived shortage of this type of ammo wouldn't be an issue is because
it wouldn't be used.
It wouldn't be something that was really in play.
The real issue would be, well, primarily it would be weapons used on aircraft.
That's what the U.S. would need a lot of for that.
It wouldn't be 155 rounds.
So his answer to him, I am sure that the way he's looking at it was, that's just, it doesn't
even make sense because it wouldn't be a ground war.
Even if that fight was to occur, understand, the US is not going to attempt to invade mainland
China because we value our troops.
same reason China isn't going to attempt to invade the United States.
So there wouldn't be a lot of call for that particular ammunition.
So that's the reason.
US doctrine is to be able to fight our two nearest competitors at the same time.
A perceived shortage of 155 ammo doesn't change that.
It would be a different type of fight, using very different weapons.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}