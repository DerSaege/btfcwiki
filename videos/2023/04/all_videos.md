# All videos from April, 2023
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2023-04-30: Let's talk about Tuberville and promotions.... (<a href="https://youtube.com/watch?v=eeoY1eXlDfw">watch</a> || <a href="/videos/2023/04/30/Lets_talk_about_Tuberville_and_promotions">transcript &amp; editable summary</a>)

Senator Tuberville's actions delay military promotions for family planning policy changes, impacting readiness and revealing Republican Party struggles post-Roe v. Wade.

</summary>

"It's not a desire to represent. It's not a desire to enact the will of the American people. It is a desire to rule them."
"The Republican Party is the dog who caught the car."
"The Republican Party will do everything they can to try to downplay it, but they can't."

### AI summary (High error rate! Edit errors on video page)

Senator Tuberville is holding up 184 promotions for high-ranking people in the military until the Department of Defense stops providing leave for family planning services in red states.
This action is causing a significant impact on readiness within the military.
Even Republicans, like Susan Collins, are uncomfortable with Tuberville's actions, as they are holding up promotions for apolitical professionals.
McConnell may step in soon as there is pressure building up against Tuberville's actions.
Changing the policy as Tuberville desires could result in a loss of recruits for the military, affecting readiness even further.
Holding up promotions not only delays pay raises and promotions but also impacts individuals' ability to go to their next postings, further affecting readiness.
One of the affected individuals is the incoming commander of cyber command, which could have serious implications.
Tuberville's actions showcase a desire to rule rather than represent the will of the American people.
Despite public or private disagreements within the Republican Party, there is no significant effort to stop Tuberville's actions.
The Republican Party is facing a dilemma post-Roe v. Wade, struggling to navigate the extremist base they have created.
The issue of military readiness and family planning policies is likely to be a significant factor in the 2024 election.
The Republican Party may struggle to find a tenable position that appeals to both extremists and the majority of Americans who support family planning services.
The legitimacy of the Supreme Court is coming into question, adding another layer of uncertainty for the Republican Party's stance on family planning issues.
The intentional weakening of the U.S. military's readiness by the Republican Party contradicts their narrative of a strong military.

Actions:

for voters, activists, military personnel,
Contact local representatives to express support for military readiness and access to family planning services (suggested)
Join advocacy groups working to protect military professionals and their rights (exemplified)
Organize community events or campaigns to raise awareness about the impact of political interference on military operations (implied)
</details>
<details>
<summary>
2023-04-30: Let's talk about Trump taking credit for failure.... (<a href="https://youtube.com/watch?v=SvP39EaruuA">watch</a> || <a href="/videos/2023/04/30/Lets_talk_about_Trump_taking_credit_for_failure">transcript &amp; editable summary</a>)

Former President Trump claims credit for overturning Roe v. Wade, leading to potential backlash in future elections, as Republicans struggle with their extreme stance on family planning.

</summary>

"Former President Trump took credit for overturning Roe v. Wade."
"Some Republicans are now realizing they caught the car, but are unsure of how to proceed."
"The Republican Party's extreme position on family planning has alienated even former allies."
"This is going to be good for several percentage points in national elections."
"The Republican Party took it too far after catching the car."

### AI summary (High error rate! Edit errors on video page)

Former President Trump took credit for overturning Roe v. Wade during a rambling response, setting the stage for it to be used against him in future elections.
Some Republicans are now realizing they caught the car, but are unsure of how to proceed and are giving non-committal answers.
Republicans are misinterpreting their own polling data on family planning, leading to very restrictive bans that only seven percent of people support.
The Republican Party's extreme position on family planning has alienated even former allies, such as Hispanic voters who now oppose a six-week ban.
Beau suggests that the Democratic Party has a slam-dunk issue on their hands if they push for reproductive rights, as the Republican Party may regret their extreme stance.

Actions:

for politically active individuals,
Contact local representatives to express support for reproductive rights (implied)
Join organizations advocating for comprehensive family planning policies (implied)
Organize community events to raise awareness about the importance of reproductive rights (implied)
</details>
<details>
<summary>
2023-04-30: Let's talk about Colorado, tractors, and farmers.... (<a href="https://youtube.com/watch?v=tDUnQOZ4Llc">watch</a> || <a href="/videos/2023/04/30/Lets_talk_about_Colorado_tractors_and_farmers">transcript &amp; editable summary</a>)

Legislation in Colorado addresses farmers' machinery repair struggles, impacting consumers' wallets, leading to a potential nationwide "right to repair" movement.

</summary>

"Right to repair."
"Help may be coming."
"Legislation aims to fix that."

### AI summary (High error rate! Edit errors on video page)

Legislation signed in Colorado addresses farmers' struggles with machinery maintenance, impacting consumers' wallets.
Manufacturers make machinery intentionally complex, making it difficult for farmers to repair on their own.
Farmers dependent on machinery face issues when approved providers can't fix equipment promptly during critical times like harvest season.
The new legislation mandates manufacturers to provide tools, parts, manuals, and software for repairs.
The legislation was primarily supported by the Democratic Party but had some Republican backing as well.
Manufacturers are unhappy with the legislation and may challenge it in court, citing concerns like emission standards and proprietary software.
The movement is called "right to repair," aiming to give farmers the ability to repair their own equipment.
Companies may need to compromise on certain aspects, like proprietary software, in the future.
Colorado is the first state to pass such legislation, with potential for similar laws in other states and even at the federal level.

Actions:

for farmers, consumers,
Reach out to local representatives to advocate for similar right-to-repair legislation in your state (suggested).
Stay informed about potential federal-level legislation regarding machinery repair rights (implied).
</details>
<details>
<summary>
2023-04-30: Let's talk about Brandon, books, budgets, and Biden.... (<a href="https://youtube.com/watch?v=h14jumbHP-0">watch</a> || <a href="/videos/2023/04/30/Lets_talk_about_Brandon_books_budgets_and_Biden">transcript &amp; editable summary</a>)

Biden must embrace "Dark Brandon" by fixing his labor record, supporting the LGBTQ community, and calling out Republican nonsense, as recommended by Beau.

</summary>

"He has to be Dark Brandon from time to time."
"Aviators on, walk away. No cleanup from the press office."
"He has to lean into it. He's got to call out Republicans, he's got to fix his labor record, and he has to come out hard in favor of LGBTQ people."
"He has to be Dark Brandon every once in a while."

### AI summary (High error rate! Edit errors on video page)

Biden made an appearance at the White House Correspondents' Dinner, possibly considering using memes and jokes as part of his campaign strategy.
The White House Correspondents' Dinner is essentially a roast where Biden joked about "Dark Brandon" and put on aviators.
Biden must embrace "Dark Brandon" to reach younger voters effectively by showing up in ways that appeal to them.
Three things Biden must do to embrace "Dark Brandon": fix his labor record, support the LGBTQ community, and call out Republican nonsense.
Biden needs to make strong public statements in support of labor and LGBTQ rights to embody "Dark Brandon" effectively.
Biden should not let the White House press office clean up any controversial remarks he makes to lean into the persona of "Dark Brandon."
Beau suggests that Biden needs to show a different side of himself, embracing the darker aspects to connect with a younger audience.
Biden should not take seriously the budget proposed by McCarthy because it comes from unserious people who didn't expect it to be taken seriously.
Beau recommends that Biden needs to lean into this strategy, call out Republicans, fix his labor record, and support the LGBTQ community to make an impact as "Dark Brandon."

Actions:

for campaign strategists,
Call out Republican nonsense, fix labor record, support LGBTQ community (suggested)
Lean into the strategy, embrace the persona of "Dark Brandon" (suggested)
</details>
<details>
<summary>
2023-04-29: The Roads to the new shop and a Q&A (<a href="https://youtube.com/watch?v=eY3EKOipXjI">watch</a> || <a href="/videos/2023/04/29/The_Roads_to_the_new_shop_and_a_Q_A">transcript &amp; editable summary</a>)

Beau shares the journey from filming in a limited space to constructing a dedicated shop for demonstrations and future content creation, addressing safety in rural areas, collaborations, and content creator responsibilities.

</summary>

"The problem with filming in my actual workshop that you know occasionally I had actual work to do and that got in the way."
"There's going to be so many people jockeying for his job, and for a while it's going going to be just, I'm going to have to, I'm going to have to pay way more attention."
"So if you're waiting or you're getting ready for hurricane season or whatever, yeah."

### AI summary (High error rate! Edit errors on video page)

Beau shares the journey from filming in his actual workshop to constructing a dedicated space for his channel due to limitations.
The new shop is fully equipped for demonstrations, with better lighting and soundproofing.
Beau explains the improvement in video quality as a result of upgraded lighting, not a new camera.
He addresses delays in legal proceedings in Georgia and refrains from guessing without sufficient information.
Beau advises on staying safe in rural areas to avoid getting shot accidentally, especially when lost.
He recalls a funny anecdote involving his kid's response to what his parents do at school.
Beau declines to comment on a feud between prominent right-wing figures, deeming it not his business.
He attributes his channel's success to luck, contrary to advice from YouTube expert Roberto Blake.
Beau talks about approaching collaborations with individuals on social media, particularly women, with respect and genuine interest.
He acknowledges the potential challenges and increased attention required post-Tucker's departure from right-wing media.
Beau expresses hesitation about covering certain sensitive topics, like assault cases, due to personal reasons.
The new studio will enable Beau to create videos on various topics like water purification and tools for mutual aid.
He shares thoughts on creators who push boundaries in their content and the impact of their actions.

Actions:

for content creators,
Collaborate with other content creators respectfully and genuinely (exemplified)
Educate yourself on safety measures in rural areas to avoid accidental harm (exemplified)
Use luck as a starting point but be willing to put in the effort for success (implied)
</details>
<details>
<summary>
2023-04-29: Let's talk about understanding term polling.... (<a href="https://youtube.com/watch?v=lI9cn4iRIpQ">watch</a> || <a href="/videos/2023/04/29/Lets_talk_about_understanding_term_polling">transcript &amp; editable summary</a>)

Polling reveals that while Black Lives Matter is more popular than MAGA, attacks against movements like BLM may drive negative voter turnout.

</summary>

"Black Lives Matter is very, very much more popular than MAGA."
"There are far more people who view MAGA in a negative light than view it in a positive light."
"The Republican Party is going to drive their own negative voter turnout."

### AI summary (High error rate! Edit errors on video page)

Polling helps gauge the popularity of different entities and provides insights into political currents in the country.
Most terms and entities in the United States do not have majority support, with the real metric being how many people view them positively versus negatively.
Black Lives Matter (BLM) is viewed positively by 38% of Americans, while MAGA is viewed positively by 24%.
BLM is two points underwater, while MAGA is 21 points underwater, meaning more people view MAGA negatively.
Attacks against BLM and similar movements do not have the intended effect and can drive negative voter turnout.
When candidates identify as MAGA, they are eliminating 45% of the population who view it negatively.
The Republican Party may drive their own negative voter turnout by associating with MAGA.
Campaigning against movements like BLM can lead to more people voting against the candidate.

Actions:

for political analysts, campaign strategists, voters,
Analyze polling data on different entities and movements to gauge public sentiment (implied)
Be mindful of how attacks on certain movements can affect voter turnout (implied)
</details>
<details>
<summary>
2023-04-29: Let's talk about McCarthy, conflicting statements, and nerve.... (<a href="https://youtube.com/watch?v=fUrr4XgmgCM">watch</a> || <a href="/videos/2023/04/29/Lets_talk_about_McCarthy_conflicting_statements_and_nerve">transcript &amp; editable summary</a>)

Republicans present budget victory, but mixed messages and doubts emerge, revealing internal party tensions.

</summary>

"Even the Republicans don't actually want this."
"There might be some arguments that they kind of spring up."
"Things in the house are going great."

### AI summary (High error rate! Edit errors on video page)

Republicans in the House presented a budget as a victory, but discrepancies are arising regarding McCarthy's messages to different factions.
McCarthy urged colleagues to ignore the substance of the bill and focus on passing any legislation to show seriousness about spending cuts.
McCarthy promised conservatives that the bill they voted on was just a starting point, not the final version.
McCarthy's messages to moderates and hardliners were contradictory, suggesting different levels of severity in the bill.
There are concerns about McCarthy's leadership and potential backlash from factions within the Republican Party.
Some Republicans may have only voted for the bill after being assured it wouldn't be the final version, raising doubts about the bill's actual support.
The lack of transparency from Republicans about the bill's contents and implications is worrying.
Republicans in swing states who supported the bill may face challenges explaining their vote to constituents in the future.
Inter-factional tensions and arguments within the Republican Party are expected to arise due to the conflicting messages and actions.
The situation in the House regarding the budget is uncertain and may lead to further revelations.

Actions:

for politically active individuals,
Reach out to Republican representatives to express concerns and seek clarity on their stance (suggested)
Stay informed about the budget process and any updates regarding Republican positions (suggested)
</details>
<details>
<summary>
2023-04-29: Let's talk about Manchin and progressives getting a wish granted.... (<a href="https://youtube.com/watch?v=IIx0arGO63A">watch</a> || <a href="/videos/2023/04/29/Lets_talk_about_Manchin_and_progressives_getting_a_wish_granted">transcript &amp; editable summary</a>)

In the next election, progressives in West Virginia might be unhappy as Governor Jim Justice's candidacy poses a serious challenge to Senator Manchin, potentially replacing him with someone worse through a progressive lens.

</summary>

"Being careful what you wish for."
"Manchin's name gonna be at the top of that list."
"Justice may give progressives their wish, but Manchin may be gone."
"Just a thought, y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Beau warns about progressives in West Virginia potentially getting their wish in the next election but being unhappy about it.
He points out that Senator Manchin, though a Democrat, is not progressive and has been safe in West Virginia because of it.
Governor Jim Justice has announced his candidacy for Senate in West Virginia, posing a significant challenge to Manchin.
Justice is well-liked in West Virginia, being a Republican and potentially replacing Manchin with someone worse through a progressive lens.
Beau acknowledges that Justice is likely to have a strong campaign with financial backing and public favor, making his victory seem almost certain unless a scandal emerges.
He speculates that Manchin may have doubts about winning in the face of Justice's popularity and resources.

Actions:

for progressives in west virginia,
Support progressive candidates in West Virginia (exemplified)
Stay informed about the upcoming election and candidates (implied)
</details>
<details>
<summary>
2023-04-28: Let's talk about what Biden's Dark Brandon embrace might mean.... (<a href="https://youtube.com/watch?v=7ggh0Nf2dYY">watch</a> || <a href="/videos/2023/04/28/Lets_talk_about_what_Biden_s_Dark_Brandon_embrace_might_mean">transcript &amp; editable summary</a>)

Biden administration's potential shift towards a progressive agenda through embracing the "Dark Brandon" meme raises questions about future policy directions and transformative possibilities.

</summary>

"Embracing Dark Brandon, it could just be good PR."
"If he goes Dark Brandon and actually commits to them, if he has a mandate, if he has the House and the Senate, he's still not going to be Dark Brandon, but he may be far more progressive than people like me."
"To be transformative, you have to progress."
"The idea that he might actually turn into somebody who is going to push for a progressive agenda it's it's odd to think about."
"He might be willing to let other people take the lead on issues and give him advice that pushes him in a more progressive direction."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of "Dark Brandon," a meme portraying an ultra-progressive version of Biden who is ruthless in pursuing progressive goals.
Suggests that the Biden administration is leaning into the Dark Brandon meme, potentially to connect with younger audiences and claim credit for smaller victories.
Points out that embracing Dark Brandon could be good PR or a campaign advertisement for the administration.
Raises the possibility that Biden, despite being viewed as a centrist, might lean into more progressive ideas if he commits to them.
Speculates on the potential outcomes if Biden were to embrace a more progressive agenda with a mandate, House, and Senate majority.
Expresses surprise at the idea of Biden pushing for a progressive agenda, given his previous perception as a placeholder during the Trump era.

Actions:

for political analysts,
Support and advocate for policies that prioritize progressive goals and transformative change (implied).
</details>
<details>
<summary>
2023-04-28: Let's talk about rules for Trump in the NY case.... (<a href="https://youtube.com/watch?v=8mfuPi-2VdQ">watch</a> || <a href="/videos/2023/04/28/Lets_talk_about_rules_for_Trump_in_the_NY_case">transcript &amp; editable summary</a>)

The assistant district attorney aims to limit Trump's ability to use evidence obtained during discovery due to his history of attacking individuals involved in legal proceedings.

</summary>

"The ADA wants a protective order that would prohibit him from using evidence obtained during the discovery process on social media to harass or target, intimidate the list of people who will be involved in the case."
"I have no idea how the judge is going to rule on this, it is incredibly unique, but Trump does have a pretty lengthy history of stirring people up and getting them to do things."

### AI summary (High error rate! Edit errors on video page)

Explains the ADA's request to limit former President Trump's ability to use evidence obtained during discovery to go after witnesses due to his history of attacking individuals involved in legal proceedings against him.
The ADA wants a protective order to prevent Trump from using evidence on social media to harass or target individuals involved in the case.
The protective order aims to restrict Trump from copying, obtaining, or sharing sensitive documents obtained during discovery.
The judge has not yet ruled on the ADA's request, and a decision may not be made until next week.
This move by the prosecutor to limit the defendant's ability to talk about evidence they will face is a unique step.
Beau expresses curiosity about how the judge will rule, considering Trump's history of inciting people.
The case mentioned is related to falsifying business documents in New York, not another case involving Trump.

Actions:

for legal observers, concerned citizens.,
Contact legal experts for insights on the implications of limiting a defendant's ability to use evidence during legal proceedings (suggested).
Follow updates on the judge's ruling regarding the ADA's request (implied).
</details>
<details>
<summary>
2023-04-28: Let's talk about Republicans shifting on climate and numbers.... (<a href="https://youtube.com/watch?v=si9JpyzZ2zQ">watch</a> || <a href="/videos/2023/04/28/Lets_talk_about_Republicans_shifting_on_climate_and_numbers">transcript &amp; editable summary</a>)

Senator Ron Johnson fixates on Wisconsin's benefit from climate change, ignoring global impacts, as Republican Party's stance on climate evolves to deem it beneficial, disregarding the 6.8 million extra deaths yearly globally due to climate change.

</summary>

"Climate change isn't something that respects state lines or international borders."
"Climate change is a global game. Everybody has to get involved."
"Politicians' position on climate and actually doing something to mitigate should probably be a defining characteristic."

### AI summary (High error rate! Edit errors on video page)

Senator Ron Johnson of Wisconsin focused on the idea that climate change benefits his state because it is cold, disregarding the global impact.
Republican Party's stance on climate change has evolved from denial to claiming it is happening but beneficial.
The expert tried to explain the global impact of climate change to Senator Johnson, who seemed fixated on the benefits to his state.
Converting excess deaths per 100,000 into real numbers reveals around 6.8 million extra deaths per year globally due to climate change.
Climate change affects everyone, not just specific regions, and can lead to mass migrations, straining resources.
Politicians' stance on climate change should be a key consideration for voters.
Climate change is a pressing global issue that requires immediate action and cannot be ignored based on short-term benefits.
Both Republican and Democratic Parties need to take significant steps to address climate change urgently.

Actions:

for voters,
Contact politicians to prioritize climate change mitigation efforts (implied).
Get involved in local climate action groups or initiatives (implied).
</details>
<details>
<summary>
2023-04-28: Let's talk about Pence talking and what it means for Trump.... (<a href="https://youtube.com/watch?v=Sffvos2qcEY">watch</a> || <a href="/videos/2023/04/28/Lets_talk_about_Pence_talking_and_what_it_means_for_Trump">transcript &amp; editable summary</a>)

Pence's prolonged testimony to a federal grand jury hints at impending indictments, revealing the Republican Party's slow pivot away from Trump.

</summary>

"You don't bring a former vice president in to talk to a federal grand jury unless you plan on indicting."
"Pence wants to be president and right now Trump's in his way."
"The Republican Party has a Trump problem as well. They need him gone."

### AI summary (High error rate! Edit errors on video page)

Pence spoke to a federal grand jury for five hours, indicating a significant movement in the investigation.
Bringing in a former vice president to talk to a grand jury usually signals an intent to indict.
Pence's potential indictment may be due to his direct interactions with Trump on January 6th.
Pence's political aspirations for the presidency require Trump to step aside.
The Republican Party is facing a dilemma with Trump, needing him out of the primary but not wanting to alienate the MAGA base.
Republican figures may start distancing themselves from Trump to pave the way for other candidates.
Pence could have a better chance in a general election than Trump due to his perceived stance against Trump's actions on January 6th.
The slow shift away from Trump within the Republican Party is becoming more apparent.
Republican donors' attitudes may be influencing the party's changing stance regarding Trump.
Overall, there is a gradual movement away from Trump within the Republican Party.

Actions:

for political observers and activists,
Contact political representatives to express support for accountability within the Republican Party (implied)
Join or support organizations advocating for ethical political practices (implied)
</details>
<details>
<summary>
2023-04-27: Let's talk about the House GOP's huge win.... (<a href="https://youtube.com/watch?v=lheawN_uCYE">watch</a> || <a href="/videos/2023/04/27/Lets_talk_about_the_House_GOP_s_huge_win">transcript &amp; editable summary</a>)

The Republican Party's House budget win lacks substance and won't make it past the Senate, revealing disorganization and social media pandering.

</summary>

"It is a major victory for them to get through a budget in a House where they only need their votes and they almost missed it."
"Legislation that doesn't stand any chance of getting signed is not a victory."
"No matter how much they talk about it as they win on social media, they're still begging Biden to talk to them because it doesn't matter."

### AI summary (High error rate! Edit errors on video page)

The Republican Party is celebrating a major win in passing a budget through the House.
Beau created his own budget that focuses on Congress paying debts and promoting general welfare, which he and his dogs voted yes on.
Despite the Republican Party's victory, the budget will likely not pass in the Senate.
Republicans claiming victory shows their disorganization and catering to social media rather than actual voters.
Beau criticizes the Republicans for thinking their budget will force Biden to negotiate when it has no chance of being signed.
Beau points out that Biden, being a former Senator, understands the process and won't be swayed by the House's budget struggles.

Actions:

for political observers, voters,
Contact local representatives to express opinions on budget priorities (suggested)
Engage in community dialogues about legislative impact (implied)
</details>
<details>
<summary>
2023-04-27: Let's talk about a House resolution on Ukraine.... (<a href="https://youtube.com/watch?v=mcaowdpTBOU">watch</a> || <a href="/videos/2023/04/27/Lets_talk_about_a_House_resolution_on_Ukraine">transcript &amp; editable summary</a>)

The US should not set victory conditions for Ukraine, respecting their autonomy and decision-making in conflict resolution.

</summary>

"They get to make the decisions on what victory is."
"It is not the place of the United States to encourage them to fight for, again, this is something that there are a lot of military analysts who will tell you it is impossible."
"The United States doesn't get to make that decision."

### AI summary (High error rate! Edit errors on video page)

The US House is considering a resolution regarding Ukraine, which is expected to garner significant attention.
Congress frequently uses resolutions to establish US policy, but sometimes they go beyond their scope.
A current resolution aims to prevent a repeat of the mistake made on September 1st, 1939, when Germany invaded Poland.
The resolution asserts the US policy is to support Ukraine in regaining its 1991 borders, including Crimea.
While some see this goal as impossible, others believe it is difficult but achievable.
The decision on victory conditions should rest with Ukraine, not dictated by the US.
Beau suggests that the resolution should stop at affirming US support for Ukraine's victory.
The US should not dictate Ukraine's actions or push them to continue fighting based on specific conditions set by the resolution.
Beau urges the House Foreign Affairs Committee to revise the resolution and remove the clause defining victory conditions for Ukraine.
Ultimately, Beau stresses that it is not the place of the US to dictate another country's decisions or define their victory conditions.

Actions:

for us house foreign affairs committee,
Revise the resolution to remove the clause defining victory conditions for Ukraine (suggested)
Advocate for respecting Ukraine's autonomy in determining their victory conditions (implied)
</details>
<details>
<summary>
2023-04-27: Let's talk about SCOTUS, Feinstein, invitations, and resignations.... (<a href="https://youtube.com/watch?v=DA5nTUA0-MI">watch</a> || <a href="/videos/2023/04/27/Lets_talk_about_SCOTUS_Feinstein_invitations_and_resignations">transcript &amp; editable summary</a>)

Chief Justice Roberts declined to talk, Senate Judiciary Committee faces hurdles due to Feinstein's absence; pressure for resignation may increase, hindering Democratic Party actions.

</summary>

"They don't have the votes to get a subpoena."
"There may be renewed pressure on Feinstein to actually resign."
"It kind of stops the Democratic Party from doing one of the few things they can actually do without the House."
"I kind of do expect there to be a little bit more pressure for her to just resign."
"That to me just kind of boggles the mind that those Republicans on the Senate Judiciary Committee are comfortable, which is being like, yeah, we don't care."

### AI summary (High error rate! Edit errors on video page)

Chief Justice Roberts declined the Senate Judiciary Committee's request to talk, prompting questions about why the Committee couldn't subpoena him.
The Senate Judiciary Committee is facing obstacles in addressing concerns about ethics at the Supreme Court due to the "Feinstein situation."
Senator Feinstein's absence and the Republican Party's block on a temporary replacement prevent the Committee from issuing a subpoena.
The only way to get someone new on the Committee is for Feinstein to resign, and pressure for her resignation may increase.
Without Feinstein's replacement or return, the Committee lacks the votes needed to issue a subpoena.
Congressional subpoenas can be difficult to enforce and may be especially challenging for a Supreme Court Justice.
Feinstein's absence hinders the Democratic Party's ability to push judges through without the House.
With the combination of slow judge approvals and lack of subpoena power, there may be mounting pressure for Feinstein to resign.
The Senate Judiciary Committee is currently unable to take significant action due to the lack of votes and Republican opposition.
Beau expresses disbelief at the lack of interest from some Republicans on the Committee in addressing ethics concerns at the Supreme Court.

Actions:

for political activists,
Contact Governor Newsom to urge him to appoint an interim replacement for Senator Feinstein (suggested).
Reach out to Senator Feinstein's office to express support for her resignation (implied).
</details>
<details>
<summary>
2023-04-27: Let's talk about 2024 as another "vote against" election.... (<a href="https://youtube.com/watch?v=cifoBJnwdVA">watch</a> || <a href="/videos/2023/04/27/Lets_talk_about_2024_as_another_vote_against_election">transcript &amp; editable summary</a>)

The 2024 election is likely to be driven by negative voter turnout, with Trump's unfavorability potentially playing a significant role in the outcome.

</summary>

"You can't win a primary without Trump. You can't win a general with him."
"Black Lives Matter is much more favorable than anything else."
"A majority of Americans view Trump unfavorably."

### AI summary (High error rate! Edit errors on video page)

The 2024 election is shaping up to be driven by negative voter turnout, meaning people voting against somebody rather than for somebody.
Biden is the presumptive nominee for the Democratic side, and Trump for the Republican side.
A poll by NBC News shows favorability ratings: BLM 38%, Biden 38%, Democratic Party 36%, Republican Party 33%, MAGA 24%, Trump 34%.
Unfavorability ratings: BLM 40%, Biden 48%, Democratic Party 46%, Republican Party 43%, MAGA 45%, Trump 53%.
The gap between favorability and unfavorability is critical; BLM is 2% underwater, Biden, Democratic Party, and Republican Party are 10% underwater, MAGA 21%, Trump 19%.
The Democratic Party has an edge assuming normal voter turnout.
The question is who will become more likable over time: Trump and MAGA or Biden and BLM.
Black Lives Matter is more favorable than the other entities, with a smaller gap between favorability and unfavorability.
There's a clear divide in the Republican Party between normal Republicans and the MAGA movement.
Winning the primary without Trump is impossible, but winning the general with him is challenging.

Actions:

for voters,
Analyze and understand the favorability and unfavorability ratings of different entities in the upcoming election (suggested).
Stay informed about how public opinion shifts and impacts the political landscape (suggested).
</details>
<details>
<summary>
2023-04-26: Let's talk about true propaganda.... (<a href="https://youtube.com/watch?v=TGKI27bc9G0">watch</a> || <a href="/videos/2023/04/26/Lets_talk_about_true_propaganda">transcript &amp; editable summary</a>)

Beau breaks down the true meaning of propaganda, showcasing how it manipulates emotions and blurs the lines between truth and lies to achieve specific goals.

</summary>

"The best propaganda is true."
"It's all true. Even the lies, especially the lies."
"Creating doubt, uncertainty, and fear."

### AI summary (High error rate! Edit errors on video page)

Explains the confusion around the term "propaganda" and its true meaning.
Defines propaganda as something designed to evoke a specific emotional response, not necessarily false.
Illustrates how a factual statement can be turned into propaganda through presentation and context.
Mentions how propaganda can be weaponized to manipulate emotions and spread doubt.
Gives an example of humorous propaganda involving turret springs for tanks.
Emphasizes that effective propaganda blurs the lines between truth and lies, creating uncertainty.
Compares propaganda to the concept of "Flood the zone with stuff" in U.S. politics.

Actions:

for analytical thinkers, media consumers.,
Analyze media messages for emotional manipulation (implied).
Educate others on the tactics of propaganda (implied).
</details>
<details>
<summary>
2023-04-26: Let's talk about flooding in the US.... (<a href="https://youtube.com/watch?v=R3KNCH4F5dg">watch</a> || <a href="/videos/2023/04/26/Lets_talk_about_flooding_in_the_US">transcript &amp; editable summary</a>)

Beau addresses internet people about the increasing frequency and severity of extreme weather events, urging preparedness and mitigation for the future.

</summary>

"The United States is going to have to come to terms with the fact that extreme weather is no longer extreme."
"We're going to start seeing it more and it's gonna get worse if we don't do something."
"It's gonna be wet, it's gonna be muddy, it's gonna be nasty."
"We can mitigate in the future."
"Just understand it's gonna be the norm."

### AI summary (High error rate! Edit errors on video page)

Addressing internet people about snow melt and flooding in California and along the Mississippi.
Snowmelt from the Northern part heading down towards the rivers, causing flooding warnings.
20 gauges along the river already in major flood stage, with more expected.
Urging people to have their flood plans ready and be cautious.
La Crosse in Minnesota expecting a crest at 16.1 feet, with a possibility of further rise.
Yosemite Park closing until at least May 3rd due to snowmelt, with warnings for hikers about the wet and muddy conditions.
Extreme weather becoming the norm and increasing with climate change.
Mitigation is necessary for the future as extreme weather events will worsen.
Alerting to the need for action to prevent worsening situations in the future.
Urging awareness and preparation for the increasing frequency and severity of extreme weather events.

Actions:

for residents in flood-prone areas,
Prepare a flood plan and ensure readiness for potential flooding (implied)
Stay cautious and aware of flood warnings in your area (implied)
Stay informed about the weather conditions in regions prone to extreme weather events (implied)
Support and advocate for climate change mitigation efforts in your community (implied)
</details>
<details>
<summary>
2023-04-26: Let's talk about Trump refusing the debates.... (<a href="https://youtube.com/watch?v=CIn4zvtmtEI">watch</a> || <a href="/videos/2023/04/26/Lets_talk_about_Trump_refusing_the_debates">transcript &amp; editable summary</a>)

Beau dives into Trump's reluctance to debate, exploring his stated and potential real reasons, and the realization that Trump's political brand might not be a winning one.

</summary>

"You can't win a primary without Trump and you can't win a general with him."
"He cannot win a debate by the standards that matter, meaning convincing more people to vote for him."
"Trump has realized that Trump is not a great political brand."
"He may not be up to it anymore."
"He needs to win."

### AI summary (High error rate! Edit errors on video page)

Analyzing Trump's reluctance to participate in Republican primary debates.
Trump's stated reasons for not wanting to debate, including lack of approval and hostile networks.
Trump's potential real reasons for avoiding debates, beyond his stated justifications.
Speculation that Trump may have learned something about himself and his political brand.
Trump's possible fear of debating due to legal entanglements, election claims, and Jan 6 events.
The idea that Trump's debate skills are not as effective as they used to be, especially in light of his previous term in office.
Trump's challenge in balancing his primary base and the need to appeal to moderates for a general election.
Trump's realization that he can't win a primary with his current rhetoric and also can't win a general election with it.
The delicate balancing act Trump faces in shifting his base while trying to attract more moderate voters.
The suggestion that Trump's decision to bow out of debates may be a strategic move given his no-win situation.

Actions:

for political analysts, voters,
Analyze Trump's political strategy and messaging (implied)
Stay informed about political developments and candidate behaviors (implied)
Engage in critical thinking about political figures and their motives (implied)
</details>
<details>
<summary>
2023-04-26: Let's talk about Cruz, a recording, and a commission.... (<a href="https://youtube.com/watch?v=mgyvYWqqB6E">watch</a> || <a href="/videos/2023/04/26/Lets_talk_about_Cruz_a_recording_and_a_commission">transcript &amp; editable summary</a>)

Beau addresses a controversial recording involving Senator Cruz, hinting at potential legal nuances and downplaying its immediate significance, anticipating more revealing recordings in the future.

</summary>

"It's worth noting that a lot of what's on this recording that people find so shocking today, he said in public."
"I think this might be more of a taste test of what's to come than anything else."
"I don't really see this as the blockbuster story that it's being made out to be."

### AI summary (High error rate! Edit errors on video page)

Beau addresses Senator Cruz and a recording that has been publicized regarding a commission to address perceived issues with the 2020 election.
He mentions that some view this commission as potentially furthering a coup, though he questions if there's anything criminal in the recording.
The recording features ideas Senator Cruz presented publicly before, possibly offering legal cover due to his position as a senator.
Beau suggests that despite being election fodder and problematic, the recording may not be as significant as portrayed.
There are reports indicating that the special counsel's office will acquire these recordings, hinting at more candid recordings to surface.
Beau anticipates more revealing recordings to emerge, potentially causing real issues, but he doesn't see the current one as a major story.
While he acknowledges the importance of the recording, Beau cautions against overreacting based on social media commentary and urges a measured response.

Actions:

for political analysts, concerned citizens,
Stay informed on political developments and implications (exemplified)
Monitor for further revelations and recordings (exemplified)
</details>
<details>
<summary>
2023-04-25: Let's talk about who's next at Fox and 1980s action movies.... (<a href="https://youtube.com/watch?v=gxGi9YuAEbU">watch</a> || <a href="/videos/2023/04/25/Lets_talk_about_who_s_next_at_Fox_and_1980s_action_movies">transcript &amp; editable summary</a>)

Speculation abounds at Fox after Tucker Carlson's exit, signaling potential changes and maintaining skepticism about the network's ethical standards and future narrative.

</summary>

"Who goes next depends on who doesn't listen to the new boss, same as the old boss."
"Okay, don't expect it to become some bastion of journalistic ethics."
"Don't go away mad, just go away."
"Those people who would follow him, they're already so far down that echo chamber, it doesn't matter."
"Anyway, it's just a thought."

### AI summary (High error rate! Edit errors on video page)

Speculation is rife about who's next at Fox, with Tucker Carlson's departure sparking questions and predictions.
Rupert Murdoch's move to assert control over his "pet project" by removing those who may not agree with planned changes.
The recent developments at Fox may serve as a warning to others at the network.
Expectations for changes at Fox include potentially halting the promotion of blatantly false narratives.
Despite potential changes, Fox is not likely to transform into a beacon of journalistic ethics.
Divisions within Fox between the news and entertainment sides may impact the handling of factually inaccurate information.
Tucker Carlson's future post-Fox is uncertain, with potential challenges due to his rhetoric and past legal issues.
Carlson's next steps could involve smaller outlets or even international offers like RT.
Carlson's departure may not significantly impact his loyal audience deeply entrenched in an echo chamber.

Actions:

for media consumers,
Stay informed about developments at Fox and hold the network accountable for promoting truthful narratives (implied).
</details>
<details>
<summary>
2023-04-25: Let's talk about Tucker and what we can learn.... (<a href="https://youtube.com/watch?v=mcaeNhQZqFM">watch</a> || <a href="/videos/2023/04/25/Lets_talk_about_Tucker_and_what_we_can_learn">transcript &amp; editable summary</a>)

Tucker Carlson reportedly leaving Fox signifies potential shifts at the network; beware of despair-mongering and embrace hope for driving change.

</summary>

"Hope is what keeps people engaged. Hope is what keeps people motivated. It's what keeps them in the fight. It is what causes change."
"Despair is a good business model but it's not good for any long-term movement."
"Whether you think you can or you think you can't, if you're talking about the majority of people in this country, if the majority of people think that nothing can be done, they're right."
"Putting out the kind of content that says we can't win, it is literally self-defeating."
"Change is coming. It's the one thing you can't stop."

### AI summary (High error rate! Edit errors on video page)

Tucker Carlson is reportedly no longer at Fox, signaling changes happening at the network.
The wealthy in the country have a track record of avoiding accountability, leading to skepticism about consequences for Fox.
Even after settling for $787 million, comments dismissed the impact, believing there wouldn't be changes at Fox.
Despite uncertainties about the reasons behind Tucker's departure, changes are evident at Fox.
Tucker was seen as untouchable, but recent events have proven otherwise.
Right-wing outlets thrive on fear-mongering, while despair is a common tactic for those seeking change.
Despair-mongering can be self-defeating, as it discourages action and perpetuates a sense of hopelessness.
Hope is emphasized as a driving force for engagement, motivation, and instigating change.
The lesson from recent events is that change is inevitable and can be sparked by even small entities like Dominion challenging Fox.
Despite uncertainties about future changes at Fox, hope for positive transformation is encouraged.

Actions:

for media consumers,
Support media outlets that prioritize hope and motivate positive change (suggested)
Engage in constructive discourse and actions that foster hope and drive positive transformations (implied)
</details>
<details>
<summary>
2023-04-25: Let's talk about Trump, timetables, and Georgia.... (<a href="https://youtube.com/watch?v=pON8zbf1TnE">watch</a> || <a href="/videos/2023/04/25/Lets_talk_about_Trump_timetables_and_Georgia">transcript &amp; editable summary</a>)

Officials in Georgia have settled on a timetable for future announcements regarding potential proceedings against former President Trump and his allies, with indictments expected between July and September.

</summary>

"There is a high likelihood that between now and the beginning of July, we're not going to hear much else."
"The anticipation is that at least some of Trump's allies will be indicted."
"The Georgia case seems to be moving towards charging."

### AI summary (High error rate! Edit errors on video page)

Officials in Georgia have settled on a timetable for future announcements regarding potential proceedings against former President Trump and his allies.
The prosecutor in Georgia, Willis, has a list of allegations ranging from election interference to involvement with fake electors, possibly leading to larger conspiracy charges.
Notifications were sent out to law enforcement in the area between July 11th and September 1st, indicating a high likelihood of indictments against some of Trump's allies during that period.
There may not be much information trickling out to the press until the beginning of July, as they are in a phase where details are not likely to be released.
The expectation is that significant public interest in the prosecutor's decision may lead to demonstrations requiring law enforcement readiness.
The Georgia case seems to be moving towards charging, with a wide timeframe signaling closure in the final stages.
Despite developments in other cases, such as federal and New York cases, the focus is currently on the Georgia case, with limited updates expected until summer.

Actions:

for legal analysts, political enthusiasts,
Stay informed about developments in the legal proceedings against former President Trump and his allies (implied).
Be prepared for potential demonstrations or public interest events related to the prosecutor's decisions (implied).
</details>
<details>
<summary>
2023-04-25: Let's talk about McCarthy, budgets, civics, and republicans.... (<a href="https://youtube.com/watch?v=SoR0TqN3n24">watch</a> || <a href="/videos/2023/04/25/Lets_talk_about_McCarthy_budgets_civics_and_republicans">transcript &amp; editable summary</a>)

Beau stresses the critical need for Republican lawmakers to understand basic civics and the limitations of current Congress on future actions.

</summary>

"The Republican Party has to get its act together."
"You are electing people that literally could not write a job description for what they're up there to do."
"There is nothing that this Congress can do to limit the actions of a future Congress in that way."
"The Republican party needs to get its act together on so many levels."
"They're going to say, oh, well, we capped future spending. No, they didn't."

### AI summary (High error rate! Edit errors on video page)

McCarthy's budget is up for a vote this week, but some Republicans have raised concerns and may not vote for it.
The issue isn't about how the budget cuts will impact Republican supporters but the lack of plans to balance the budget within 10 years.
Beau points out that the idea of balancing the budget within a certain timeframe in the current Congress does not hold weight for future Congresses.
He expresses concern that some Republican lawmakers may lack a basic understanding of civics and the powers of Congress.
Beau stresses the importance of electing officials who understand their roles and limitations within the government.
He calls for the Republican Party to address these fundamental gaps in knowledge among its members.
Beau underlines the need for voters to be aware of these issues and not be misled by false promises or claims in budgets.
He uses the example of writing in caps on future spending, stating that such provisions are imaginary and hold no real power.
Beau urges for a better understanding of how government functions and the limitations on current Congress to bind future Congresses.
He concludes by calling for the Republican Party to improve on various levels, including the need for a better grasp of governmental functions.

Actions:

for voters, republican party members,
Educate yourself and others on the functions and limitations of Congress (implied)
Hold lawmakers accountable for their understanding of basic civics (implied)
Ensure that elected officials have a comprehensive grasp of their roles and responsibilities (implied)
</details>
<details>
<summary>
2023-04-24: Let's talk about hope.... (<a href="https://youtube.com/watch?v=qthe5AGEdto">watch</a> || <a href="/videos/2023/04/24/Lets_talk_about_hope">transcript &amp; editable summary</a>)

Beau shares personal experiences of discrimination, progress, and the interplay between hope and motivation in fighting for a better future.

</summary>

"Hope is dangerous. It's a cliche but it's true because if you have hope for a better world that means you can imagine it."
"No matter what group you're a part of, there will be somebody like you after you. And they're counting on you."

### AI summary (High error rate! Edit errors on video page)

Shares a story about growing up in the South and encountering racism and homophobia.
Recounts incidents from his childhood where interracial relationships and LGBTQ+ individuals faced discrimination.
Describes encountering a gay classmate from high school at a bar as an adult.
Talks about the progress made in acceptance and equality over his lifetime.
Acknowledges the ongoing fight for equality and the challenges faced by marginalized communities.
Expresses optimism about the world changing for the better in the long run.
Emphasizes the link between hope and motivation, believing in the possibility of a better future.
Encourages individuals to continue fighting for progress, knowing that future generations are relying on them.

Actions:

for activists, marginalized communities,
Support LGBTQ+ youth in high schools by creating safe spaces and standing up against bullying (implied)
Advocate for racial equality and challenge discriminatory behavior in public spaces (implied)
Educate others about the importance of acceptance and equality to create a more inclusive society (implied)
</details>
<details>
<summary>
2023-04-24: Let's talk about Russia, China, the US, and peers.... (<a href="https://youtube.com/watch?v=JcotpeupPvM">watch</a> || <a href="/videos/2023/04/24/Lets_talk_about_Russia_China_the_US_and_peers">transcript &amp; editable summary</a>)

Beau analyzes China and Russia's military capabilities vis-a-vis the US through football analogies, suggesting that while China could become a peer competitor, their focus seems not on military expansion.

</summary>

"China is a college team, maybe a championship college team."
"Their intelligence, definitely better than Russia's."
"If they were to dump a bunch of cash into defense, they could be a peer within a decade."
"It doesn't really seem to be in their national interest and their national character to become expansionist."
"I don't think they want to be a second world's policeman."

### AI summary (High error rate! Edit errors on video page)

Analyzing China and Russia in comparison to the United States in football terms.
Describing China as a college team, potentially a championship team due to lack of information on Chinese military operations.
Noting the uncertainty surrounding the Chinese military's actual combat capabilities and logistics.
Comparing Russian and Chinese intelligence, stating that Chinese intelligence is better.
Speculating that if China significantly invested in defense, they could become a peer competitor in about a decade.
Emphasizing that China's current focus does not seem to be on military expansion.
Suggesting that all war games involving China inflicting damage on the US do not result in China obtaining Taiwan.
Pointing out that China's ability to project troops globally like the US takes time and may not be their national interest.
Speculating that China does not aspire to be a global policeman like the US.

Actions:

for military analysts, policymakers,
Study and understand the military capabilities and strategies of China and Russia (implied).
Advocate for diplomatic solutions and international cooperation to prevent conflicts (implied).
</details>
<details>
<summary>
2023-04-24: Let's talk about Facebook and Twitter.... (<a href="https://youtube.com/watch?v=T4XKDbWH-1c">watch</a> || <a href="/videos/2023/04/24/Lets_talk_about_Facebook_and_Twitter">transcript &amp; editable summary</a>)

Facebook may owe US users money due to privacy claims, while Twitter's verification mark changes lead to backlash and marketing concerns.

</summary>

"You actually have celebrities on Twitter who have realized if they change their name the blue check goes away."
"It's the exact opposite of an endorsement."
"Twitter Blue. You had a number come out and say that they wouldn't pay for it."
"Everything's fine. No, it's a mess."
"I don't want to be on it."

### AI summary (High error rate! Edit errors on video page)

Facebook might owe money to US users between May 24th, 2007, and December 22nd, 2022, due to privacy claims about data sharing.
Facebook agreed to create a fund of $725 million, but users must file a claim by August 25, 2023, at Facebook user privacy settlement.com.
Claims may not yield significant payments due to fund allocation for administration and number of claimants.
Twitter recently removed verification marks (blue checks) from all users, aiming to make it part of Twitter Blue.
Elon Musk jokingly gifted verification back to certain celebrities who were critical of his service.
Clicking on the blue checks now reveals that the person subscribed to Twitter Blue, leading to backlash from non-subscribers.
Some celebrities denounced Twitter Blue publicly, possibly harming its marketing strategy.
Concerns have been raised about potential legal issues regarding using celebrity likeness for endorsements.
Some major Twitter users are changing names to manipulate the blue checks, raising marketing concerns.
Beau left Twitter but finds the social media drama entertaining while staying off the platform.

Actions:

for social media users,
File a claim at Facebook user privacy settlement.com by August 25, 2023 (suggested)
Monitor developments regarding Twitter's verification changes and potential legal issues (suggested)
</details>
<details>
<summary>
2023-04-24: Let's talk about Democratic primary debates.... (<a href="https://youtube.com/watch?v=mn5nvl5BJm4">watch</a> || <a href="/videos/2023/04/24/Lets_talk_about_Democratic_primary_debates">transcript &amp; editable summary</a>)

Beau explains why the Democratic Party might not have primary debates, citing historical trends of serious challenges leading to incumbent party losses.

</summary>

"I think the country would be better served by having a lot of open discussion between people within the Democratic Party."
"The trend is there, I don't like it, I'm just going to go ahead and be honest about it."
"I understand it. I get where they're coming from."

### AI summary (High error rate! Edit errors on video page)

Reporting suggests the Democratic Party won't have debates for the presidential primary, meaning Biden won't debate challengers.
One-term presidents historically don't have to debate challengers due to serious challenges from within their party leading to the incumbent party losing.
Bush Sr. faced a challenge from Buchanan, Carter from Ted Kennedy, Ford from Reagan, leading to new presidents.
The trend of serious challenges from within the party goes back to 1968 and continues with Trump, though he didn't have serious primary challengers.
The Democratic Party might fear being associated with certain views post-global public health issues if having debates with challengers like Williamson.
Beau acknowledges the historical trend but believes open debates within the party are better for representative democracy.
Despite understanding the decision, Beau expresses a desire for more options and open debate within the Democratic Party.

Actions:

for political enthusiasts,
Advocate for more open debates within political parties (implied)
</details>
<details>
<summary>
2023-04-23: Let's talk about what Putin can learn from fast food.... (<a href="https://youtube.com/watch?v=7HVGnAW4x74">watch</a> || <a href="/videos/2023/04/23/Lets_talk_about_what_Putin_can_learn_from_fast_food">transcript &amp; editable summary</a>)

Putin's reliance on reports leads to bad decisions, reflecting a broader issue of prioritizing metrics over goals, seen in fast food and military contexts.

</summary>

"Focus on a good example here. Focus on street price."
"When you're looking at reports, always remember why that report was generated and what the goal was."
"It's a symptom of bad leadership."
"They became so focused on meeting arbitrary numbers that they just started putting it in the report."
"The obsession with numbers led to impractical solutions."

### AI summary (High error rate! Edit errors on video page)

Putin's reliance on reports is resulting in bad decisions as he views external information as propaganda.
The focus on metrics and measurements in authoritarian countries may not be the root cause of the issue.
Beau draws a parallel from his experience at a fast food joint where an emphasis on car wait times led to counterproductive actions.
The pressure on a specific metric caused delays in serving customers efficiently.
The obsession with numbers led to impractical solutions like creating parking spaces for waiting customers.
Companies often prioritize reports and numbers over the actual goals they are meant to achieve.
Russia's military also fell into the trap of prioritizing arbitrary numbers in reports, giving a false impression of efficiency.
Beau argues that this issue is not exclusive to authoritarian states but is a result of poor leadership and a focus on irrelevant metrics.
He warns about the dangers of institutionalizing this mindset as it can lead to flawed policy decisions.
Beau advocates for understanding the purpose behind reports and metrics to avoid losing sight of the actual goals.

Actions:

for leaders, decision-makers,
Question the purpose behind metrics and reports (implied)
</details>
<details>
<summary>
2023-04-23: Let's talk about taking the win with Fox.... (<a href="https://youtube.com/watch?v=w0DwlABAeVs">watch</a> || <a href="/videos/2023/04/23/Lets_talk_about_taking_the_win_with_Fox">transcript &amp; editable summary</a>)

Beau explains the financial hit Fox took and the potential for change, urging recognition of victories, no matter how small, for progressive hope.

</summary>

"Take the win on this. Take the win on the little things."
"This was a win. This was a huge cost to Fox."
"They have to change. They have to change their business model."
"Moving towards a much better world where everybody's getting a fair shake."
"It's a marathon, there will be more when it comes to Fox."

### AI summary (High error rate! Edit errors on video page)

Explains the significance of Fox taking a financial hit due to recent events.
Breaks down the financial implications for Fox, despite potential tax benefits.
Points out the substantial loss Fox faced and how it could impact their future.
Mentions the pressure on Fox to change their business practices.
Addresses the idea of societal change not being immediate but a gradual process.
Stresses the importance of recognizing the win in this situation and its implications.
Emphasizes the need for progressive individuals to acknowledge victories, even small ones.
Talks about the impact of this financial blow on Fox's credibility and viewer trust.
Acknowledges that while Fox may not drastically change, there could be alterations in their presentation.
Encourages taking victories, regardless of scale, and the effect on maintaining hope.

Actions:

for progressive individuals,
Monitor Fox News coverage and hold them accountable for accuracy (implied).
Support trustworthy news sources and responsible journalism (implied).
Stay informed and engaged in media literacy efforts in your community (implied).
</details>
<details>
<summary>
2023-04-23: Let's talk about some wrong doors and wrong comparisons.... (<a href="https://youtube.com/watch?v=bsb3KIZ9DmY">watch</a> || <a href="/videos/2023/04/23/Lets_talk_about_some_wrong_doors_and_wrong_comparisons">transcript &amp; editable summary</a>)

Beau questions bias and false equivalencies in comparing two instances of people being shot after going to the wrong house based on their race.

</summary>

"The eternal question of why seems important."
"If your first reaction to people accidentally going to the wrong house and getting shot is to look at their skin tone and try to figure out how you, by a similar pigment, were somehow damaged, your priorities are a little mixed up."
"One of these things listed completely makes sense."
"You're looking for something to view as oppression, it's not real."
"It's not real."

### AI summary (High error rate! Edit errors on video page)

Received a message about two instances where people went to the wrong house and ended up getting shot, sparking comparisons.
Discovered that many people were making comparisons between the two situations.
Two incidents: one involving a black kid who survived being shot, and another where a white woman was killed.
Differences in the way the two cases were treated raised questions of bias.
The black kid received more attention, support, and coverage compared to the white woman.
Detailed the specific events of each incident and the aftermath.
Pointed out disparities in media coverage, GoFundMe amounts, and reactions from authorities.
Raised skepticism about the validity of some comparisons made between the two cases.
Criticized the search for grievances and false equivalencies in trying to compare these vastly different situations.
Encouraged reevaluating priorities when focusing on skin color in such tragedies.

Actions:

for community members, social justice advocates.,
Reexamine priorities when faced with tragic events based on skin tone (implied).
</details>
<details>
<summary>
2023-04-23: Let's talk about confusion over parties and philosophy.... (<a href="https://youtube.com/watch?v=Yq05F5_fz3U">watch</a> || <a href="/videos/2023/04/23/Lets_talk_about_confusion_over_parties_and_philosophy">transcript &amp; editable summary</a>)

Beau Young clarifies the confusion between the Libertarian Party and libertarian ideology, suggesting small government conservatives to look into the Libertarian Party's platforms.

</summary>

"Libertarian means two different things."
"Left-leaning libertarians in Europe were like it. That is not true."
"So you end up with some really weird dynamics at times."
"They probably represent your ideals a whole lot more than people who want to rule over."
"Anyway, it's just a thought, y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains the confusion between a political party in the United States and a component of a political ideology with the same name.
Defines the political spectrum with left and right politically, as well as authoritarian at the top and libertarian at the bottom.
Mentions the Libertarian Party in the United States being inherently right-wing and pro-capitalism.
Notes that while the Libertarian Party is right-wing, there are left-leaning libertarians globally.
Points out that small government conservatives of the Republican Party better align with the Libertarian Party today.
Suggests that individuals who lean towards small government conservatism but are hesitant to vote for the Democratic Party may find alignment with the Libertarian Party.

Actions:

for political enthusiasts,
Research and understand the platforms of the Libertarian Party to see if they better represent your ideals (implied)
</details>
<details>
<summary>
2023-04-22: Let's talk about today's SCOTUS ruling and the future.... (<a href="https://youtube.com/watch?v=H-kyGky0KUE">watch</a> || <a href="/videos/2023/04/22/Lets_talk_about_today_s_SCOTUS_ruling_and_the_future">transcript &amp; editable summary</a>)

A judge in Texas attempted a nationwide ban on mephepristone, but the Supreme Court's stay keeps it available for now amidst ongoing legal battles, suggesting a roller coaster of events ahead.

</summary>

"A judge in Texas decided to attempt to ban something nationwide."
"There's more litigation to come."
"Take a breather, relax, and just know that for the time being it's widely available."

### AI summary (High error rate! Edit errors on video page)

A judge in Texas attempted to order a nationwide ban on mephepristone, commonly used for reproductive rights.
The Supreme Court issued a stay, keeping the medication widely available for now.
Justices Alito and Thomas dissented, with further legal battles expected in the Fifth Circuit and then back to the Supreme Court.
Beau questions the enforcement of a nationwide ban on methepristone due to its widespread use.
The future legal battles could result in consequential rulings, making it a roller coaster ride until the final Supreme Court decision.

Actions:

for reproductive rights advocates,
Stay informed about ongoing legal battles and decisions (implied)
Support organizations advocating for reproductive rights (implied)
</details>
<details>
<summary>
2023-04-22: Let's talk about Trump's aide being questioned.... (<a href="https://youtube.com/watch?v=4ReiTnWGTI8">watch</a> || <a href="/videos/2023/04/22/Lets_talk_about_Trump_s_aide_being_questioned">transcript &amp; editable summary</a>)

Beau provides an update on Trump's legal entanglements in DC, focusing on questioning of top aide Boris Epstein and the proximity of the investigation to Trump's inner circle, signaling a potential conclusion.

</summary>

"It seems like they are at the stage where they are talking to people who had a lot of direct contact with Trump."
"This is probably nearing the end."

### AI summary (High error rate! Edit errors on video page)

Providing update on Trump's legal entanglements in DC, separate from New York and Georgia issues.
Trump's top aide, Boris Epstein, questioned by Department of Justice over two days about January 6th and interactions with Rudy and Eastman.
Epstein is one of nine Trump aides who had their phones taken by the feds.
Epstein questioned with Smith present, who just observed without asking any questions.
The length of questioning over two days hints at significant content being discussed.
Department of Justice may be struggling to get accurate answers or feeling there is more to uncover.
Epstein's involvement brings the investigation closer to Trump's inner circle.
This phase of interviews with direct contacts of Trump suggests a nearing end of the investigation.
DOJ likely seeking information on what was shared with Trump.
Indicates a significant development that may impact future timelines and events.

Actions:

for political analysts, news followers,
Stay informed about developments in legal investigations involving public figures (implied)
</details>
<details>
<summary>
2023-04-22: Let's talk about Texas, the Commandments, and what's next.... (<a href="https://youtube.com/watch?v=wh1NYvlRSSQ">watch</a> || <a href="/videos/2023/04/22/Lets_talk_about_Texas_the_Commandments_and_what_s_next">transcript &amp; editable summary</a>)

Texas proposed a law mandating the Ten Commandments in classrooms, raising concerns about unintended consequences and potential manipulation of constituents' ignorance.

</summary>

"It's one of those two. I can't think of another option."
"Your most likely answer as to the motivation behind it, it's just to trick the ignorant people of Texas that don't know no better."
"Even with this Supreme Court, that's just ridiculously unlikely."
"You don't understand the Constitution, you'll fall for this, you don't have any understanding of civics, you are ignorant, and we own you."
"Let's go ahead and just wallpaper one side of every classroom with all kinds of guidelines from different religions."

### AI summary (High error rate! Edit errors on video page)

Texas proposed Senate Bill 1515 mandating the presence of the Ten Commandments in classrooms.
Under a normal Supreme Court, this mandate violates the First Amendment and is likely to be struck down.
The current Supreme Court's composition raises concerns about how they may interpret such a law.
If passed, unintended consequences may lead to the inclusion of various religious texts and beliefs in classrooms.
The motives behind the proposed law could be to either include diverse religious texts or manipulate the base.
Beau questions if the legislature believes the law will stand or if it's a ploy to manipulate their base.
The likelihood of this law standing, given the ignorance of the base, is questioned by Beau.
The proposed law may be an attempt to motivate a base perceived as ignorant.
Beau wonders about the outcome if classrooms were filled with guidelines from different religions.
Despite doubts, Beau doesn't see this law progressing.

Actions:

for texans, educators,
Question the motives behind proposed laws (implied)
Stay informed and engaged in legislative matters (implied)
</details>
<details>
<summary>
2023-04-22: Let's talk about 2 Tennessee questions and a joke.... (<a href="https://youtube.com/watch?v=mqQ8nPHTreU">watch</a> || <a href="/videos/2023/04/22/Lets_talk_about_2_Tennessee_questions_and_a_joke">transcript &amp; editable summary</a>)

Beau delves into recent Tennessee events, potential resignations, and comedian Drew Morgan's approach, advocating for diverse tactics in reaching varied audiences.

</summary>

"I don't think the media is going to let this go."
"People aren't the same. They won't be reached by using the same tactics."
"It's a diversity of tactics thing because everybody's different."
"The real question you have to ask is, is he wrong?"
"Anyway, it's just a thought, y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Overview of recent events in Tennessee, including the resignation of Scotty Campbell, vice chair of the House Republican Caucus, amid allegations of inappropriate behavior or harassment.
Speculation on the possibility of more resignations in light of increased media coverage and scrutiny of ethics within the legislative body.
Responds to a question about comedian Drew Morgan, mentioning his use of the word "fascist" to describe Tennessee's government in a recent act.
Beau shares his perspective on Drew's comedic approach, noting that different tactics work for different audiences, and he appreciates Drew's ability to energize progressive individuals through comedy.
Beau contrasts his own cautious use of strong language on YouTube with Drew's more liberal use, based on their different approaches and target audiences.
Emphasizes the importance of recognizing diverse tactics in reaching varied audiences, acknowledging that what works for one person may not work for another.
Beau concludes by posing the question of whether Drew's approach is wrong, leaving room for individual interpretation and reflection.

Actions:

for progressive individuals,
Watch Drew Morgan's comedy clips about Tennessee to understand his approach and messaging (suggested).
Embrace diverse tactics in communication and advocacy to reach different audiences (implied).
</details>
<details>
<summary>
2023-04-21: Let's talk about the GOP budget being announced.... (<a href="https://youtube.com/watch?v=Rjahp7N42B0">watch</a> || <a href="/videos/2023/04/21/Lets_talk_about_the_GOP_budget_being_announced">transcript &amp; editable summary</a>)

Beau dismantles the Republican budget plan, revealing its superficial promises and insignificant cuts, ultimately deeming it a political show.

</summary>

"It has been shown to be a show and nothing more."
"It's words that are put on the paper, but no future Congress actually has to abide by them."
"Even the stuff that is an actual cut, it's not really either because it can be redone in a very short period of time."

### AI summary (High error rate! Edit errors on video page)

Analyzing the Republican budget plan coming out of the House and deconstructs its components.
The budget includes items for negotiation, unlikely proposals, and cuts that may happen.
Some elements, like canceling Biden's key legislation and student debt relief, are unlikely to make it into the final package.
Two interesting proposals involve returning discretionary spending to 2022 levels and capping budget growth at 1% for the next decade, which Beau deems as insignificant.
Beau criticizes the budget for not addressing significant areas like defense or Social Security.
The plan includes reclaiming unspent COVID relief funds and cutting the 2024 budget for a total reduction of $221 billion.
Beau sees the budget show as a facade, with proposed cuts being minimal and easily reversible.
The deal raises the debt ceiling by $1.5 trillion, rendering any actual cuts temporary and insignificant.
Beau predicts Biden will likely concede to the budget plan as the key elements are not binding on future Congresses.
Despite the budget's flaws, Beau acknowledges that there is still room for negotiation and adjustments.

Actions:

for budget analysts, political activists,
Contact local representatives to push for more substantial budget cuts and allocation adjustments (implied)
Organize community dialogues on budget priorities and advocate for proper funding in critical areas (implied)
</details>
<details>
<summary>
2023-04-21: Let's talk about evil in a religious context.... (<a href="https://youtube.com/watch?v=uPbxWyg1Q_c">watch</a> || <a href="/videos/2023/04/21/Lets_talk_about_evil_in_a_religious_context">transcript &amp; editable summary</a>)

Beau tackles criticism of his support for the LGBTQ community, delving into the concept of love, divisions, and the importance of embracing love over hate, especially within Christian beliefs.

</summary>

"Love is good. Hate is evil."
"I see it as people who should be trying to love their neighbor, pushing them away, dividing from them."
"It's hate. It's hate."
"You chose to be a Christian. That means you gotta let the hate go."
"It's easier to sell judgment. It's easier to sell hate. Because it's easy. It is much harder to love."

### AI summary (High error rate! Edit errors on video page)

Receives a message criticizing his support for the LGBTQ community, suggesting he find Jesus Christ and stop "deceiving people" by supporting evil.
Acknowledges the religious aspect of the criticism, stating he usually doesn't share his spiritual beliefs on the channel.
Explores the concept of evil in a Christian context as the opposite of love, which is described as following the two greatest commandments of loving God and loving your neighbor as yourself.
Responds to the accusation that the LGBTQ community has divided the country and hurt families by sharing his perspective that they are simply trying to be true to themselves.
Suggests that divisions occur because some Christians fail to follow the principle of loving their neighbor, leading to rejection and alienation.
Addresses the criticism that any negative comments towards the LGBTQ community are labeled as hate, pointing out that genuine love and acceptance should be the focus.
Questions why individuals claim the LGBTQ community forces their views when in reality, they are just trying to be authentic.
Challenges the notion of a "gay agenda," arguing that people should be allowed to live their lives without judgment or hate.
Encourages embracing love over hate, especially for those who have chosen a Christian path.

Actions:

for christians, lgbtq allies,
Challenge yourself to actively show love and acceptance towards all individuals in your community (exemplified).
Refrain from passing judgment and instead focus on understanding and supporting others (implied).
</details>
<details>
<summary>
2023-04-21: Let's talk about a Russian military performance comparison.... (<a href="https://youtube.com/watch?v=R-ZPe2RkgoU">watch</a> || <a href="/videos/2023/04/21/Lets_talk_about_a_Russian_military_performance_comparison">transcript &amp; editable summary</a>)

Beau clarifies the mismatch between expectations and reality in Russia's military performance, exposing the impact of propaganda and geopolitical consequences.

</summary>

"Russia is taking roughly 100 years worth every year."
"There is not a conventional force on the planet that can hold its own against the United States."
"Geopolitically, it's over. Russia lost."

### AI summary (High error rate! Edit errors on video page)

Explains the discrepancy between the expectations of Russia's military performance and the reality, using the example of the T-55 video and the messaging around it.
Points out that Russia's military bloggers are praising the T-55 tank due to its ammunition load, referencing a statement from a retired Lieutenant General with experience.
Contrasts the casualties in Afghanistan between the US (23,000 over 20 years) and Russia (official Russian estimate of 110,000 in one year), illustrating the immense losses Russia is facing.
Attributes the inflated expectations of Russia's military capability to US defense industry propaganda that portrayed Russia as a near peer to the US to secure defense funding.
States that Russia is not on par with the US militarily, citing the inability to match US military technology and capabilities demonstrated in past conflicts like Desert Storm.
Analyzes the geopolitical implications of Russia's actions in Ukraine, asserting that regardless of the outcome on the battlefield, Russia has already lost geopolitically.
Emphasizes that the conflict in Ukraine is unwinnable for Russia from a geopolitical perspective, even if they achieve military victory.
Acknowledges that while Russia is using outdated equipment in the conflict, they are strategically holding modern resources in reserve.
Asserts that no conventional force worldwide can compete with the military might of the United States, not as a claim of exceptionalism but a factual statement.
Concludes by underscoring the massive costs, both in terms of casualties and geopolitical consequences, that Russia is incurring due to the conflict in Ukraine.

Actions:

for military analysts, policymakers,
Analyze and challenge defense industry propaganda narratives (implied)
Support efforts to de-escalate conflicts and prioritize diplomatic solutions (implied)
</details>
<details>
<summary>
2023-04-21: Let's talk about Chief Justice Robert being invited to the Senate.... (<a href="https://youtube.com/watch?v=4IJMcwHSFSw">watch</a> || <a href="/videos/2023/04/21/Lets_talk_about_Chief_Justice_Robert_being_invited_to_the_Senate">transcript &amp; editable summary</a>)

Chief Justice Roberts invited to address ethics at Supreme Court amid Republican disinterest, raising concerns about transparency and legitimacy.

</summary>

"Republicans apparently don't care or aren't willing to simply ask and get to the bottom of it."
"The fact that the Republican Party doesn't seem interested in this should be a warning sign to a whole lot of people."
"The fact that they're apparently going to assist in stonewalling, that should say a lot to undecided voters."
"I am hopeful that the Chief Justice understands the legitimacy issues that the Supreme Court is facing."
"It's just a thought, y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Chief Justice Roberts invited to talk to Senate Judiciary Committee about ethics at the Supreme Court.
Democratic Party pushing for an investigation into Justice Thomas's gifts and financial arrangements.
Senate Judiciary Committee Chair invited Roberts to talk, but lacks votes for a subpoena due to Senator Feinstein's absence.
Republican Party showing disinterest in pursuing a subpoena for ethics investigation.
Calls for transparency in ethics standards for the highest court in the land.
Republicans not showing interest in getting to the bottom of the situation raises concerns.
Suggestions that Republicans should clarify the ethics issues surrounding Justice Thomas.
Lack of Republican support for transparency raises doubts among voters.
Pressure building on Chief Justice Roberts to address legitimacy concerns at the Supreme Court.
Hopeful for Chief Justice Roberts to address the ethics issues facing the Supreme Court.

Actions:

for concerned citizens, activists.,
Contact your representatives to demand transparency and accountability in the Supreme Court (suggested).
Organize or participate in local campaigns advocating for ethical standards in the judiciary (implied).
</details>
<details>
<summary>
2023-04-20: Let's talk about why the Dominion settlement matters.... (<a href="https://youtube.com/watch?v=20-iqziOPCI">watch</a> || <a href="/videos/2023/04/20/Lets_talk_about_why_the_Dominion_settlement_matters">transcript &amp; editable summary</a>)

Beau explains the significance of the Fox News settlement, predicting future challenges due to profit-driven narratives and the massive monetary consequences.

</summary>

"Cash is a motivator."
"The nightmare for Fox, it is just beginning."
"Nobody at Fox is going to look at this as a cost of doing business."
"They're after the cash."
"It's more profitable to do so."

### AI summary (High error rate! Edit errors on video page)

The Fox News settlement matters and context is key to understanding its significance.
Upton Sinclair's quote about understanding and salary dependency applies to the situation.
Money is a significant motivator, as seen in Fox's actions and narratives.
Bill O'Reilly predicts more problems for Fox due to their pursuit of profit over reporting news.
The $787 million settlement is a colossal amount that will impact Fox significantly.
Despite having an audience, Fox could still make money by providing accurate information.
The financial consequences may lead Fox to reconsider editorial decisions for profitability.
Beau believes that Fox will prioritize profit over journalistic integrity.
The monetary penalties may force Fox to adjust their editorial decisions to limit future liability.
Fox's pursuit of money is a driving force behind their actions and decisions.

Actions:

for media consumers,
Stay informed about media ethics and biases (suggested)
Support fact-based journalism (implied)
</details>
<details>
<summary>
2023-04-20: Let's talk about Trump's plan for the homeless.... (<a href="https://youtube.com/watch?v=kNJTTHNtgtM">watch</a> || <a href="/videos/2023/04/20/Lets_talk_about_Trump_s_plan_for_the_homeless">transcript &amp; editable summary</a>)

Trump's misguided plan to address homelessness focuses on punishment rather than genuine help, showcasing a lack of understanding and compassion for those in need.

</summary>

"You ban being homeless. You make it illegal."
"Being homeless is not a lack of character, it's a lack of money, or it's a lack of being able to find housing."
"It's not actually about helping. It's about giving their base somebody else to look down on."
"Why do you want to punish them?"
"His whole gimmick is finding a group of people to punish."

### AI summary (High error rate! Edit errors on video page)

Trump's plan to address homelessness includes making it illegal to be homeless, banning urban camping, and establishing tent cities.
Trump lacks an understanding of the limits of federal power over local city ordinances.
Homeless individuals under Trump's plan are given the option to go to jail or a tent city for psychiatric and substance abuse help.
Beau questions the effectiveness of arresting people for being poor and the logistics of getting to work from a tent city.
A significant portion of homeless individuals actually have jobs, challenging the stereotype of homelessness being solely due to a lack of character.
Beau criticizes the punitive approach of turning housing for homeless individuals into tent cities, suggesting it's more about creating a class divide.
He questions the logic of punishing individuals who may need treatment for mental health issues by placing them in a tent city.
Trump's focus on punishment rather than genuine help for the homeless population is criticized by Beau.
While Beau acknowledges the importance of providing housing and social services, he points out the flaws in Trump's approach.
Beau concludes by expressing skepticism towards Trump's plan and its underlying motives.

Actions:

for advocates, activists, policymakers,
Advocate for compassionate and effective solutions for homelessness (implied)
Support organizations providing social services and housing to the homeless (implied)
Educate others about the realities of homelessness and challenge stereotypes (implied)
</details>
<details>
<summary>
2023-04-20: Let's talk about Fox on military installations.... (<a href="https://youtube.com/watch?v=N-Fl_YJprFc">watch</a> || <a href="/videos/2023/04/20/Lets_talk_about_Fox_on_military_installations">transcript &amp; editable summary</a>)

Beau explains the push to remove Fox News from military bases due to concerns about its content undermining readiness and cohesion.

</summary>

"US taxpayers are paying to undermine readiness."
"A lot of the comments that are made on Fox are so detrimental to those concepts of readiness."
"The reason that there is a push to get Fox off the bases… is because a lot of the content on Fox… undermines the chain of command, undermines readiness, undermines unit cohesion and morale."

### AI summary (High error rate! Edit errors on video page)

Explains the issue of having Fox News on military installations, particularly in common areas and offices.
Mentions the First Amendment concerns in relation to military installations.
Describes how on military installations, there are restrictions on what content can be shown, like cable news in food court areas.
Talks about the push to remove Fox from bases by groups like Vote Vets due to concerns about content undermining chain of command, readiness, cohesion, and morale.
Provides scenarios where commanders express controversial views that, if aired on Fox, could lead to their dismissal.
Points out that comments made on Fox can be detrimental to military readiness and could lead to consequences if expressed by a commander.
Raises the issue of taxpayers unknowingly funding content that undermines military readiness.
Concludes by summarizing the importance of pushing to remove Fox News from military bases.

Actions:

for military personnel, activists,
Contact military installations to advocate for the removal of Fox News (suggested)
</details>
<details>
<summary>
2023-04-20: Let's talk about Abbott's pardon problem.... (<a href="https://youtube.com/watch?v=SXTk1A3nqQM">watch</a> || <a href="/videos/2023/04/20/Lets_talk_about_Abbott_s_pardon_problem">transcript &amp; editable summary</a>)

Texas faces shifting narratives and promises regarding a BLM demonstration incident, where revealing the victim's true political identity challenges the governor's pardon stance.

</summary>

"He was right-wing. He just wasn't a bigot."
"The framing is that the shooter, an army sergeant, killed a leftist communist socialist Obama-loving BLM protester."
"Foster wasn't a leftist. He was a right-wing veteran. That's who was killed."

### AI summary (High error rate! Edit errors on video page)

Texas is experiencing shifting narratives, promises, and new developments regarding a BLM demonstration incident in 2020.
The shooter in the incident was convicted, and the Texas governor hinted at pardoning him.
Details from the shooter's phone revealed racist content, posing questions about the governor's stance.
The framing of the incident paints the victim as a leftist BLM protester killed by a right-wing army sergeant.
Despite revelations of racist content, Beau suggests that being racist isn't a deal-breaker for many Republicans.
The victim, Foster, was a right-wing veteran and libertarian, not a leftist as initially portrayed.
Correcting the narrative to show Foster's true political beliefs might impact the governor's decision on the pardon.
Foster believed in protecting the first amendment with the second and understood state violence against marginalized people.
Beau believes it's vital to correct the narrative and acknowledge Foster's true political identity.
The importance lies in revealing that Foster was a right-wing veteran, challenging the initial portrayal of him as a leftist BLM protester.

Actions:

for texans, activists, blm supporters,
Correct the narrative about Foster's political beliefs and honor his true identity (implied).
Advocate for justice for Foster and ensure his story is accurately portrayed (implied).
</details>
<details>
<summary>
2023-04-19: Let's talk about the fellas.... (<a href="https://youtube.com/watch?v=Mu3b1FsLDqo">watch</a> || <a href="/videos/2023/04/19/Lets_talk_about_the_fellas">transcript &amp; editable summary</a>)

Beau explains the authenticity of "the fellas" countering Russian propaganda on Twitter and debunks claims of them being a botnet controlled by US intelligence, affirming their real human existence and organic information dissemination.

</summary>

"They are real human beings."
"That's just not true."
"It is definitely more akin to the USO than some super secret operation."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of "the fellas" on Twitter, a group that counters Russian propaganda and supports Ukraine.
Responds to a message claiming that the fellas are not real people but a botnet controlled by US intelligence.
Clarifies that the fellas are real human beings, not a botnet, and that while Western intelligence may amplify their message, the core group is genuine.
Disputes rumors linking the fellas to CENTCOM or CIA control, providing insights into the geographic nature of US commands and debunking the Virginia headquarters claim.
Shares the humorous origin of the CIA rumor, where some changed their location on Twitter to Langley as a joke.
Encourages critical examination of rumors, especially regarding jargon like CENTCOM, which is often mistakenly associated with various global events.
Asserts that the majority of information shared by the fellas is organic and accurate, supporting Ukraine without significant disinformation.

Actions:

for social media users,
Support the efforts of groups countering disinformation by sharing accurate information and debunking false claims (implied).
Engage in critical thinking when evaluating rumors and online information (implied).
</details>
<details>
<summary>
2023-04-19: Let's talk about delays and developments in Georgia.... (<a href="https://youtube.com/watch?v=Up7iGKabf6Q">watch</a> || <a href="/videos/2023/04/19/Lets_talk_about_delays_and_developments_in_Georgia">transcript &amp; editable summary</a>)

News from Georgia suggests a looming conspiracy charge, as fake electors turn on each other, potentially causing delays and building a massive case.

</summary>

"The prosecution in Georgia is looking at a giant conspiracy charge."
"The prosecution there is trying to build a huge conspiracy case."
"Putting those together and then presenting it, that takes time."

### AI summary (High error rate! Edit errors on video page)

News from Georgia regarding electors could spell bad news for Trump and cause delays.
A filing aimed to disqualify a defense attorney is making headlines.
People involved in the fake electors scheme are pointing fingers at each other.
Some implicated individuals were unaware of a potential immunity deal from July.
Ethical concerns arise as one attorney represents those pointing fingers and those being accused.
Prosecution in Georgia is speaking to individuals at the bottom of the scheme, hinting at a conspiracy charge.
The prosecution seems focused on building a massive conspiracy case.
Despite the entertaining drama, the key point is the prosecution's efforts to gather information on criminal activities.
Indictments might be delayed due to the time needed to build a strong case.
Prosecution appears to be methodically working from top to bottom in the case.

Actions:

for georgia residents, legal observers,
Stay informed about the developments in the legal proceedings in Georgia (suggested).
Support efforts towards transparency and accountability in electoral processes (implied).
</details>
<details>
<summary>
2023-04-19: Let's talk about Oklahoma updates.... (<a href="https://youtube.com/watch?v=z6Bja1mxv0Q">watch</a> || <a href="/videos/2023/04/19/Lets_talk_about_Oklahoma_updates">transcript &amp; editable summary</a>)

Beau provides an update on the disturbing recording involving Oklahoma county officials, with calls for resignations, a surprising lack of law enforcement support, and ongoing media coverage indicating potential future resignations.

</summary>

"The governor has called for their resignations but beyond that there have been local people staging demonstrations calling for their resignations."
"I think they did that unanimously, I'm not sure."
"There will continue to be coverage."
"I will continue to monitor this and keep y'all updated on any new developments."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Updating on the events in McCurtain County, Oklahoma since a recording surfaced involving county officials having disturbing and inappropriate conversations.
Topics in the recording included off-color commentary, dealing with a burn victim, talk about removing a journalist permanently, and expressing desires to beat and hang black people.
Governor has called for resignations, with local people staging demonstrations for the same.
Surprisingly, the Sheriff's Association in Oklahoma voted to expel the members involved in the recording, showing a departure from the usual closing of ranks to protect law enforcement officials.
People on the recording have not publicly addressed its content, claiming it was obtained illegally.
Media will likely continue coverage due to the perceived threat against a journalist.
Expectations of resignations in the future due to continued coverage, local outcry, lack of law enforcement solidarity, and the governor's stance.
Real investigation initiated at the state level.
Beau will monitor and provide updates on any new developments.

Actions:

for local residents, activists,
Join local demonstrations calling for resignations (exemplified)
Stay informed through media coverage and continue to demand accountability (implied)
</details>
<details>
<summary>
2023-04-19: Let's talk about Fox, 787 million, and what's next.... (<a href="https://youtube.com/watch?v=SPfjcCdhMFA">watch</a> || <a href="/videos/2023/04/19/Lets_talk_about_Fox_787_million_and_what_s_next">transcript &amp; editable summary</a>)

Fox and Dominion's $787 million settlement before trial sparks emotional reactions, with implications for Fox's future reputation and potential legal challenges.

</summary>

"Fox and Dominion reached a settlement of $787 million right before trial, sparking emotional reactions as people wanted a full-blown trial."
"Fox's decision to settle for $787 million appears unusual as they were already invested in the legal process."
"Dominion agreed to the settlement as receiving $787 million quickly outweighed the risks and delays of a trial where a larger sum might be awarded."
"Beau questions Fox viewers to critically analyze the situation and evidence, suggesting that they were misled by Fox."
"I think that there is much more to come when it comes to Fox having to answer for a lot of their reporting."

### AI summary (High error rate! Edit errors on video page)

Fox and Dominion reached a settlement of $787 million right before trial, sparking emotional reactions as people wanted a full-blown trial.
The settlement doesn't signify the end as Smartmatic, another company, is gearing up for a suit asking for more money.
Fox's willingness to settle may have opened the floodgates for additional lawsuits due to concerns about public revelations.
The settlement amount, though significant, may not be substantial considering Fox's annual revenue.
The settlement impacts Fox's reputation and could provide ammunition for those aiming to remove Fox News from certain areas like military installations.
Fox's decision to settle for $787 million appears unusual as they were already invested in the legal process.
Legal analysts suggest Fox was likely to lose the trial, prompting them to settle.
Dominion agreed to the settlement as receiving $787 million quickly outweighed the risks and delays of a trial where a larger sum might be awarded.
Beau questions Fox viewers to critically analyze the situation and evidence, suggesting that they were misled by Fox.
Beau anticipates more repercussions for Fox in the future regarding their reporting practices.

Actions:

for viewers, fox news fans,
Analyze the evidence and situation independently (suggested)
Stay informed about the ongoing developments related to Fox News (implied)
</details>
<details>
<summary>
2023-04-18: Let's talk about helping change along.... (<a href="https://youtube.com/watch?v=9KqJUqxKI5Q">watch</a> || <a href="/videos/2023/04/18/Lets_talk_about_helping_change_along">transcript &amp; editable summary</a>)

Beau provides tactics to help someone change their bigoted views by starting with family education, understanding offensive jokes, and gradually introducing new perspectives, stressing the importance of patience and persistence.

</summary>

"Change takes time. Baby steps."
"Exposure kills fear. Fear is the root of most of this."
"Your family is your family and nothing comes between that type of stuff."
"Give it time to digest before you give them something new."
"Start by framing it around you want to keep your family."

### AI summary (High error rate! Edit errors on video page)

Addressing the importance of change, jokes, memes, and how change is a gradual process.
Sharing a scenario where a family member sends bigoted jokes about the LGBTQ community.
Explaining how to approach and help someone who is showing interest in learning and changing.
Suggesting tactics like making the person explain the jokes they share to understand their impact.
Emphasizing the recycled nature of offensive jokes as a signal of belonging to a specific group.
Encouraging starting with educating family members about the hurtful nature of such jokes.
Advising on taking baby steps in educating and understanding complex topics like gender identity.
Stating the significance of preserving family values and using that as a starting point for change.
Recommending exposing the person to family members from marginalized communities to reduce fear and prejudice.
Stressing the gradual nature of the process and the importance of patience and persistence in fostering change.

Actions:

for all community members,
Start educating family members about the impact of offensive jokes (implied)
Encourage the person to explain offensive jokes to understand their impact (implied)
Slowly introduce new perspectives to the person, starting with small government conservatism (implied)
</details>
<details>
<summary>
2023-04-18: Let's talk about Republicans pursuing losing policies.... (<a href="https://youtube.com/watch?v=-G6f3gQlAvw">watch</a> || <a href="/videos/2023/04/18/Lets_talk_about_Republicans_pursuing_losing_policies">transcript &amp; editable summary</a>)

Beau explains why the Republican Party continues to pursue losing positions, jeopardizing the party as a whole by prioritizing local wins over national strategy.

</summary>

"Republicans are doubling down on losing positions, but which Republicans?"
"State-level Republicans are putting the federal-level Republican Party in jeopardy."
"They may actually end up driving so much negative voter turnout."
"Federal Republicans may be calling home to state reps, y'all have to stop."
"They're in their own social media echo chamber."

### AI summary (High error rate! Edit errors on video page)

Addresses the question of why the Republican Party is pursuing losing positions, particularly focusing on reproductive rights.
Federal-level Republicans are notably quiet on pushing through policies because they understand these are losing positions.
The doubling down on losing positions is happening at the state level, in red states or gerrymandered states where these positions are electorally favorable.
State-level Republicans, aiming to secure their positions, are putting the federal-level Republican Party in jeopardy by pursuing these losing positions.
The risk lies in negative voter turnout driven by pursuing unpopular policies, potentially leading to gerrymandered districts flipping.
The situation arises as Republicans focus on winning locally while potentially endangering the party as a whole.
Federal Republicans may be calling on state reps to stop pursuing losing positions, but state reps are entrenched in their local echo chambers.
The state reps prioritize staying in office and use what they perceive as a surefire strategy based on vocal support in their districts.
Despite potential risks, Republicans continue to pursue these losing positions due to the immediate electoral benefits in their districts.
The disconnect between state and federal Republicans may ultimately lead to consequences for the party as a whole.

Actions:

for political observers,
Contact local representatives to express concerns about pursuing losing positions (implied).
</details>
<details>
<summary>
2023-04-18: Let's talk about McCurtain County Oklahoma and a recording.... (<a href="https://youtube.com/watch?v=MqFHR1lN6Tg">watch</a> || <a href="/videos/2023/04/18/Lets_talk_about_McCurtain_County_Oklahoma_and_a_recording">transcript &amp; editable summary</a>)

Beau delves into a recording from McCurtain County, Oklahoma, revealing disturbing sentiments from county officials and potential civil rights violations being investigated by the FBI.

</summary>

"Hearing top law enforcement officials in a county say that they're upset that they can't
engage in extrajudicial killing, that might be something that the Civil Rights Division
views as an indicator."
"It is appalling. It is appalling. It is not something you want from public officials."

### AI summary (High error rate! Edit errors on video page)

Introduction to discussing an incident in McCurtain County, Oklahoma.
Mention of a recording that surfaced involving county officials with inappropriate dark humor.
Focus on the main concern: county officials expressing upset over not being allowed to beat or hang black people.
Mention of the official reactions, including calls for resignations of specific county officials.
Involvement of state police force, Oklahoma State Bureau of Investigation, and the attorney general's office in looking into the matter.
The FBI now has a copy of the recording, prompting questions about their interest in it.
Speculation on the Civil Rights Division of the FBI potentially investigating the County Sheriff's Department due to the disturbing content of the recording.
Clarification that the FBI possessing the recording doesn't confirm an active investigation.
Mention of the possibility of civil rights violations against black Americans in that location.
Uncertainty about whether the Civil Rights Division will launch an investigation based on the recording's content.

Actions:

for community members,
Contact local advocacy organizations to support potential civil rights investigations by federal agencies (implied).
Stay updated on developments and spread awareness within your community about the importance of accountability and transparency in law enforcement (exemplified).
</details>
<details>
<summary>
2023-04-18: Let's talk about 3 unrelated but similar incidents.... (<a href="https://youtube.com/watch?v=aV1hLVLZMyc">watch</a> || <a href="/videos/2023/04/18/Lets_talk_about_3_unrelated_but_similar_incidents">transcript &amp; editable summary</a>)

Three separate incidents across the country reveal the dangers of fear-mongering and living in constant fear, leading to tragic outcomes.

</summary>

"Maybe it's not a good idea to constantly fear monger and keep people on edge."
"You have a lot of innocent people being lost because people feel like they live in a combat zone."
"It's becoming a self-fulfilling prophecy."

### AI summary (High error rate! Edit errors on video page)

Three separate events in Kansas City, New York, and New Mexico share a common element.
In Kansas City, an 84-year-old white man shot a 16-year-old black kid who knocked on his door by mistake.
The shooter in Kansas City has been charged with assault and armed criminal action, with questions raised about the charges.
In New York, a homeowner shot at a group of friends who mistakenly pulled into their driveway, killing a 20-year-old woman.
The shooter in New York has been charged with second-degree murder after an hour-long standoff with law enforcement.
In Farmington, New Mexico, law enforcement went to the wrong house, resulting in the homeowner being shot and killed by officers.
Law enforcement in Farmington did not force entry or use deceit to enter the property, leading to questions about potential charges.
The incident in Farmington may be deemed a harmless error, allowing for a justified shoot based on the mistaken presence of law enforcement.
Constant fear-mongering and keeping people on edge can lead to innocent lives being lost due to heightened tensions.
Beau questions the impact of creating an environment where individuals constantly live in fear, likening it to a self-fulfilling prophecy.

Actions:

for communities, activists, bystanders,
Advocate for community policing and de-escalation training (implied)
Support initiatives that address racial biases in law enforcement (implied)
Join local organizations working towards police accountability (implied)
</details>
<details>
<summary>
2023-04-17: Let's talk about even more Justice Thomas developments.... (<a href="https://youtube.com/watch?v=PNQRSmJ9QiM">watch</a> || <a href="/videos/2023/04/17/Lets_talk_about_even_more_Justice_Thomas_developments">transcript &amp; editable summary</a>)

Supreme Court Justice Thomas faces scrutiny over financial discrepancies and unanswered questions, sparking calls for ethics reform and further investigation.

</summary>

"There are still major components to this story that do not have answers."
"I don't see this getting swept under the rug and disappearing anytime soon."

### AI summary (High error rate! Edit errors on video page)

Supreme Court Justice Thomas is in the spotlight due to recent developments.
Thomas made a rare statement about amending previous filings for accuracy.
Reports show Thomas received between 50 and 100 grand annually from a firm in Nebraska that ceased to exist in 2006.
The company that closed was Ginger LTD, while another company, Ginger LLC, was formed.
The situation may simply be an innocent mistake related to paperwork.
Calls for further investigation are mounting despite the possibility of a simple explanation.
Politicians are calling for a code of ethics for the Supreme Court and urging Chief Justice Roberts to open a probe.
Some are supporting impeachment or calling for resignation.
Attention on Thomas continues to grow, with the likelihood of more information surfacing.
Questions remain unanswered regarding private jet use, real estate in Savannah, and vacation expenses.

Actions:

for politically engaged citizens,
Contact your representatives to express support for ethics reforms and calls for further investigation (exemplified)
Join organizations advocating for transparency and accountability in the Supreme Court (implied)
</details>
<details>
<summary>
2023-04-17: Let's talk about confusion over Russian statements.... (<a href="https://youtube.com/watch?v=RDXe0QU4srA">watch</a> || <a href="/videos/2023/04/17/Lets_talk_about_confusion_over_Russian_statements">transcript &amp; editable summary</a>)

Reports and social media confusion surround Russian leadership's sarcastic call for Putin to declare victory, hinting at deeper whispers of urging an end to the conflict.

</summary>

"He's being sarcastic."
"There are whispers among people that are high up in the Russian machine."
"He is being deadpan in his delivery."

### AI summary (High error rate! Edit errors on video page)

Reports and social media posts about Russian leadership led to confusion.
Main leader suggested Putin should declare victory and go home.
The post was sarcastic, not a call for Russia to leave.
The boss of Wagner emphasized the need for Russia to commit fully to achieve victory or face a rebirth of nationalism.
Despite misleading portrayals, scholars believe the leader's statement was sarcastic and not meant to be taken seriously.
The social media posts suggest high-ranking Russian officials may be urging Putin to concede victory.
There are whispers among Russian elites that the conflict should end.
Scholars interpret the leader's statements as deadpan and hint at deeper meanings.
The boss of Wagner's post implies that some oligarchs are pushing for Putin to claim victory and end the conflict.
The essence behind the posts is a call for Putin to end the conflict, declare victory, and potentially retain some territory.

Actions:

for world citizens,
Reach out to organizations advocating for peace and diplomatic resolutions (implied)
Stay informed about international conflicts and their implications (implied)
</details>
<details>
<summary>
2023-04-17: Let's talk about a delay in the Fox news case.... (<a href="https://youtube.com/watch?v=9LUsvq2peaQ">watch</a> || <a href="/videos/2023/04/17/Lets_talk_about_a_delay_in_the_Fox_news_case">transcript &amp; editable summary</a>)

Beau delves into the Fox News case, discussing potential settlement and the implications for Fox's reputation and finances amidst a possible trial delay.

</summary>

"Y'all have a good day."
"We've got another 24 hours before anybody should really start popping their popcorn."
"Conventional wisdom right now is that Fox is basically on the phone or in the room with Dominion's lawyers."
"It certainly stands to reason that this is a last-ditch attempt by Fox to get a settlement offer accepted."
"Not just because of the exposure, but because they're looking at a massive suit with punitive damages on top of that."

### AI summary (High error rate! Edit errors on video page)

Beau delves into the Fox News case, discussing the delays and potential settlement.
The trial was expected to start at 9 AM, but there might be a one-day delay.
Fox seems inclined to settle with Dominion, possibly due to legal and PR concerns.
Going to trial could expose Fox to public scrutiny and potential damage to their brand.
Dominion appears focused on showing Fox's inaccuracies rather than settling.
Fox has substantial financial resources, including cash and insurance, to handle the case.
Settling might be the prudent move for Fox considering the potential punitive damages and follow-on suits.
The situation remains uncertain, with a possible last-minute settlement attempt by Fox.
The outcome will determine whether the case proceeds to trial or ends in settlement.
Beau concludes with a reminder that the situation is still unfolding.

Actions:

for legal observers,
Stay informed about the developments in the Fox News case (implied).
Engage in critical analysis of media coverage surrounding legal disputes (implied).
</details>
<details>
<summary>
2023-04-17: Let's talk about Biden's selfie and questions from English people.... (<a href="https://youtube.com/watch?v=j0RB0SSgA7E">watch</a> || <a href="/videos/2023/04/17/Lets_talk_about_Biden_s_selfie_and_questions_from_English_people">transcript &amp; editable summary</a>)

American Republicans' outrage over Biden's selfie with Jerry Adams lacks context, ignoring Adams' visible transformation towards peace in Ireland.

</summary>

"He became more and more a mainstream political figure and wound up in office."
"The idea here is that somebody who was sympathetic or was the public face of a violent anti-government movement should be ostracized."
"If you are an American who is upset about this, please understand there are English people wondering why you're mad."

### AI summary (High error rate! Edit errors on video page)

Explains the outrage of American Republicans over a selfie of Biden with Jerry Adams in Ireland.
American Republicans were told to be upset about the image without knowing the full context.
Jerry Adams has a controversial past associated with violent activities in the Republican movement.
Talks about the ambiguity surrounding Adams' past and his rise within the movement.
Describes Adams as the public face and PR person for the Republican movement in the 80s and 90s.
Mentions a shift towards peace by Adams after the Good Friday Agreement.
Points out a significant moment when Adams condemned a violent act unequivocally, showing a visible change.
Emphasizes Adams' evolution into a mainstream political figure advocating for peaceful means.
Acknowledges that some may struggle to forgive Adams for his past actions despite his visible transformation.
Urges American Republicans to understand the context and reconciliation efforts in the UK before criticizing the selfie incident.

Actions:

for american republicans,
Contact organizations promoting reconciliation efforts in Ireland (implied)
Educate oneself on the history and progress of peace in Ireland (implied)
</details>
<details>
<summary>
2023-04-16: Let's talk about schools and a question from a conservative..... (<a href="https://youtube.com/watch?v=gEclRxoalwk">watch</a> || <a href="/videos/2023/04/16/Lets_talk_about_schools_and_a_question_from_a_conservative">transcript &amp; editable summary</a>)

Beau challenges the necessity of government involvement in parenting, stressing the importance of communication within families over external notifications.

</summary>

"I think the parents should be notified."
"It's going to go very bad at some point in time."
"But let's be real, when it comes to the name, I bet you do your homework, help the kids with their homework, right?"
"And rest assured, it will happen."
"Definitely not the first question I'd be asking."

### AI summary (High error rate! Edit errors on video page)

Addressing parenting and a challenge from a conservative regarding government involvement in certain situations.
Expresses a private disapproval of "the whole trans and drag thing," but believes government involvement is unnecessary in 99% of cases.
Suggests parents should be notified if their child at school is using different pronouns or dressing differently.
Disagrees that the first question parents should have is why the school didn't inform them about their child being trans.
Emphasizes that the first question should be why the child didn't confide in their parents.
Explains the Southern perspective on involving pastors in family matters as a form of family therapy.
Argues that policies mandating school notification regarding a child's gender identity could lead to tragic outcomes.
Asserts that kids who can hide their identity from parents can also keep it from the school, rendering the policy ineffective.
Challenges the notion that involving schools will solve the issue of children concealing their gender identity.
Believes that while some parents may discover their children's identities through school involvement, the worst-case scenarios are concerning.

Actions:

for parents, educators, policymakers,
Communicate openly with your children about gender identity and create a supportive environment (implied)
Prioritize listening and understanding your child's perspective (implied)
Seek guidance from family therapists or trusted individuals if needed (implied)
</details>
<details>
<summary>
2023-04-16: Let's talk about a PSA, weather, Twitter, and a lack of PSAs.... (<a href="https://youtube.com/watch?v=Bnmen-Ro_ZE">watch</a> || <a href="/videos/2023/04/16/Lets_talk_about_a_PSA_weather_Twitter_and_a_lack_of_PSAs">transcript &amp; editable summary</a>)

Elon Musk's Twitter changes hinder critical information dissemination, impacting public services and potentially risking lives.

</summary>

"If the people don't make it, they won't be scrolling."
"It might be worth reminding Elon that if the people don't make it, they won't be scrolling."
"If you are somebody who typically gets this information from Twitter, it's time to download a new app."

### AI summary (High error rate! Edit errors on video page)

Elon Musk targets bots and automated tweeting on Twitter, impacting National Weather Service accounts that provide life-saving information about disasters like tornadoes and hurricanes.
Changes on Twitter are hindering the National Weather Service from disseminating critical information promptly, potentially jeopardizing lives.
Public transportation updates and service delays communicated via Twitter in major metropolitan areas may also be affected by these changes.
Private entities relying on Twitter for information dissemination are likely to face challenges as well, although not as critical as life-saving alerts.
Beau suggests that Elon Musk could offer free access to certain services impacted by the Twitter changes to incentivize saving lives, possibly even receiving a tax break for doing so.
Various public services, including the National Weather Service and public transportation systems like BART, are facing obstacles in delivering timely information to the public due to Twitter modifications.
The rollout of these changes is ongoing, and many service providers may not be fully aware of how their systems are being affected.
Beau advises individuals who rely on Twitter for critical updates to download alternative apps that provide location-based weather alerts to ensure they receive necessary information promptly.

Actions:

for twitter users,
Download a weather app that uses your location for alerts (suggested)
Stay informed about alternative platforms for receiving critical updates (suggested)
</details>
<details>
<summary>
2023-04-16: Let's talk about T-55s.... (<a href="https://youtube.com/watch?v=Vq3lLodyAhs">watch</a> || <a href="/videos/2023/04/16/Lets_talk_about_T-55s">transcript &amp; editable summary</a>)

Beau addresses Russia sending outdated T-55 tanks to Ukraine, facing challenges with their military capabilities, suggesting a strange move that isn't indicative of being done.

</summary>

"Despite all limitations, it is still dangerous."
"They're just doing something very, very strange."

### AI summary (High error rate! Edit errors on video page)

Addressing rumors about Russia sending T-55 tanks to Ukraine.
Strong indications suggest the rumors are true based on images and markings on the tanks.
The T-55 is an old tank that became obsolete 50 years ago, post-World War II.
In a bizarre scenario in 2003, Challenger 2 tanks wiped out T-55 tanks.
The T-55 has significant technological disadvantages compared to modern tanks.
Russia may be using the T-55 more as self-propelled howitzers or mobile gun emplacements rather than traditional tanks.
Russia appears to be emptying out old stock by sending these outdated tanks.
Despite its age, the T-55 is still dangerous due to its cannon.
This move doesn't indicate Russia is done militarily but rather suggests they are facing challenges.

Actions:

for military analysts, policymakers, concerned citizens,
Monitor and raise awareness about Russia's military actions (implied)
Advocate for diplomatic solutions to conflicts (implied)
</details>
<details>
<summary>
2023-04-16: Let's talk about Republicans biting the hand that feeds them.... (<a href="https://youtube.com/watch?v=LlX20qBC59E">watch</a> || <a href="/videos/2023/04/16/Lets_talk_about_Republicans_biting_the_hand_that_feeds_them">transcript &amp; editable summary</a>)

Beau addresses the Bud Light boycott, revealing political donations and potential consequences.

</summary>

"I don't like to interrupt my opposition when they're making a mistake."
"Y'all boycotted one. Y'all boycotted a big donor is what this boiled down to."
"Maybe they decide to make amends and give an even bigger donation to the people who trashed them."

### AI summary (High error rate! Edit errors on video page)

Beau addresses the topic that everyone wanted him to talk about, a boycott involving Bud Light, from both conservatives and liberals.
He stayed quiet because revealing his open secret was necessary to talk about it.
Conservatives critiqued him for not discussing their successful boycott, while liberals wondered why he didn't make fun of them for not knowing the parent company behind the brand they boycotted.
The people at the center of the issue reached out to Beau, questioning his silence on the matter.
Beau explains that he refrains from interrupting his opposition when they're making a mistake, which is why he didn't initially address the situation.
Bud Light faced backlash for a semi-inclusive marketing campaign that some perceived as going "woke," leading conservatives to initiate a boycott.
Beau notes that the boycott by conservatives against Bud Light was somewhat successful, causing damage to the company but not as much as claimed.
He mentions a website called OpenSecrets that reveals the political contributions made by companies like Anheuser-Busch, Bud Light's parent company.
Beau elaborates on the political donations made by Anheuser-Busch in 2022, showing a significant portion going to Republican committees and candidates.
The transcript ends with Beau talking about Trump's child calling for an end to the boycott of the conservative-leaning company.

Actions:

for consumers, activists, voters,
Contact your representatives to inquire about their campaign contributions and affiliations (suggested)
Stay informed about the political affiliations of companies and their donations (exemplified)
</details>
<details>
<summary>
2023-04-15: Let's talk about the RNC, Milwaukee, trouble, and loyalty.... (<a href="https://youtube.com/watch?v=X5qEs3HYLRE">watch</a> || <a href="/videos/2023/04/15/Lets_talk_about_the_RNC_Milwaukee_trouble_and_loyalty">transcript &amp; editable summary</a>)

The Republican Party faces internal strife over loyalty, with divisions and animosity escalating, potentially harming unity efforts.

</summary>

"They are turning most of their anger towards the RNC."
"The RNC has decided to move ahead with their loyalty pledge, which is not going to be worth the paper it's written on."
"Every time they give a concession or they try to team up, it will anger others."
"There is now so much animosity between the factions."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

The Republican National Committee chose Milwaukee for the first primary debate for the Republican presidential nominee.
Team Trump got upset over the Young Americas Foundation's involvement in the debate, seeing them as pro-Pence.
Loyalty and division issues have arisen between Trump loyalists and the rest of the Republican Party.
The RNC's loyalty pledge, requiring support for the nominee, is seen as ineffective given the internal party divisions.
Trump's behavior towards other nominees and potential winners is questioned, doubting his adherence to the loyalty pledge.
Massive divisions and animosity within the Republican Party are already evident, with factions power-hungry and ready to attack each other constantly.

Actions:

for political observers, republican party members,
Monitor and actively participate in local Republican Party events and meetings (implied)
</details>
<details>
<summary>
2023-04-15: Let's talk about rights, grinding to a halt, and a republican question.... (<a href="https://youtube.com/watch?v=n23xpo5C9GI">watch</a> || <a href="/videos/2023/04/15/Lets_talk_about_rights_grinding_to_a_halt_and_a_republican_question">transcript &amp; editable summary</a>)

Beau challenges Republicans to question if the legislation introduced by their party truly benefits them or just harms others, pointing out the distraction of cultural war from policy issues.

</summary>

"They're not your friend. They're not the Republican party that they used to be."
"Go through Republican legislation and with each piece ask yourself does this make your life better or does it just make somebody else's life worse?"
"All of this culture war nonsense is just something to distract you from the fact that the Republican Party doesn't have any policy ideas anymore."

### AI summary (High error rate! Edit errors on video page)

Explains the viewpoint of a small government conservative who believes in strong fences making good neighbors.
Advises listeners to listen for the phrase "It's not my business" as a cue that they might be dealing with a small government conservative.
Points out that the Democratic Party is not the one introducing legislation on social issues but the Republican Party.
Challenges Republicans to scrutinize if the legislation introduced by their party makes their lives better or just makes someone else's life worse.
Suggests that cultural war legislation introduced by the Republican Party is a distraction from their lack of policy ideas.
Encourages individuals to question why the Republican Party is focusing on legislation that hurts others rather than helping the American people.

Actions:

for conservative republicans,
Examine Republican legislation to see if it truly benefits the American people (suggested).
Question why certain legislation is being introduced by representatives and how it impacts others (implied).
</details>
<details>
<summary>
2023-04-15: Let's talk about Taiwan, can openers, and parenting.... (<a href="https://youtube.com/watch?v=4TQS8vMFnUw">watch</a> || <a href="/videos/2023/04/15/Lets_talk_about_Taiwan_can_openers_and_parenting">transcript &amp; editable summary</a>)

Beau explains the implications of a conflict over Taiwan and the US strategy, focusing on air and sea defense.

</summary>

"If your kids ask a question, even adult children, take the time to answer, if you can."
"The reason China hasn't invaded and just taken Taiwan back is because it's really hard."
"US doctrine is to be able to fight our two nearest competitors at the same time."

### AI summary (High error rate! Edit errors on video page)

Explains the connection between Taiwan, can openers, a perceived shortage, and parenting.
Responds to a viewer's question about the implications of going to war over Taiwan.
Shares a personal story about asking his dad, a gunnery sergeant, about the potential conflict.
Mentions that China's invasion of Taiwan in war games usually fails.
Describes the high cost projected for both Taiwan and the United States in the event of conflict.
Points out the difficulty of China invading Taiwan due to its challenging nature.
Emphasizes that the US strategy in such a scenario focuses on air and sea defense.
Notes that the US doctrine involves being able to fight against two nearest competitors simultaneously.
Concludes that a perceived shortage of a specific type of ammunition wouldn't change the overall strategy.

Actions:

for viewers interested in understanding the potential implications of conflicts involving taiwan and us military strategies.,
Educate yourself on international relations and military strategies (implied).
</details>
<details>
<summary>
2023-04-15: Let's talk about Biden, Ireland, mistakes, and context.... (<a href="https://youtube.com/watch?v=Z7j2hY3CSMc">watch</a> || <a href="/videos/2023/04/15/Lets_talk_about_Biden_Ireland_mistakes_and_context">transcript &amp; editable summary</a>)

Biden's misstep referencing the Black and Tans in Ireland showcases the importance of historical sensitivity in international interactions.

</summary>

"Mentioning the Black and Tans to Irish people is sensitive due to the wounds of history that are still fresh in Ireland."
"Although the diplomatic spat caused by Biden's mistake won't lead to major tensions between the US and the UK, it was an inappropriate slip-up that should have been avoided."

### AI summary (High error rate! Edit errors on video page)

Biden made a mistake while referencing a sports team during his visit to Ireland, calling them "the black and tans," causing embarrassment due to the historical context.
The Black and Tans were a paramilitary police force used by the British to suppress Ireland, known for brutality and creating enduring repercussions on Irish history.
Mentioning the Black and Tans to Irish people is sensitive due to the wounds of history that are still fresh in Ireland.
Although the diplomatic spat caused by Biden's mistake won't lead to major tensions between the US and the UK, it was an inappropriate slip-up that should have been avoided.
Beau compares Biden's blunder to a hypothetical scenario where the British Prime Minister references "wounded knee" while visiting Native American reservations in the US, illustrating the cringeworthy nature of such remarks.
It's vital to be mindful of historical sensitivities when visiting other countries and to acknowledge that events depicted in movies and literature have real-life impacts on communities.

Actions:

for travelers, diplomats,
Be mindful of historical sensitivities when visiting other countries (implied)
Acknowledge the real-life impacts of historical events on communities (implied)
</details>
<details>
<summary>
2023-04-14: Let's talk about new developments in the Thomas case.... (<a href="https://youtube.com/watch?v=lZKW4std-RA">watch</a> || <a href="/videos/2023/04/14/Lets_talk_about_new_developments_in_the_Thomas_case">transcript &amp; editable summary</a>)

Supreme Court Justice Thomas faces scrutiny over undisclosed real estate transactions with Republican mega-donor Crow, raising concerns of a long-term relationship. Impeachment is considered.

</summary>

"The plot has thickened through real estate plots."
"Thomas's mother still lives there, even though Crowe has purchased property in 2014."
"The vacations, honestly, they would have been waved away."

### AI summary (High error rate! Edit errors on video page)

Supreme Court Justice Thomas and Republican mega-donor Harlan Crow are involved in a situation with new developments related to real estate transactions.
Crow bought properties owned by Thomas in Savannah in 2014 without disclosure from Thomas.
Crow claims he bought the properties to preserve them to illustrate the life of Thomas as a Supreme Court Justice.
Improvements were made to Thomas's mother's house, where he grew up, after Crow purchased it, and Thomas's mother still lives there post-purchase.
Reports suggest connections between Thomas and Crow are becoming more concerning and less justifiable politically.
Impeachment is being considered by some representatives regarding Thomas.
ProPublica's investigation revealed this information, praising them for their thorough journalism.
The situation involves a long-term undisclosed relationship that raises eyebrows, especially if Thomas's mother still lives in a property bought by Crow in 2014.
The Chief Justice may be concerned, particularly if this information was not disclosed to him initially.

Actions:

for legal scholars, activists,
Contact representatives to express support for reform or impeachment (implied)
Follow and support ProPublica for thorough investigative journalism (exemplified)
</details>
<details>
<summary>
2023-04-14: Let's talk about documents, discord, and developments.... (<a href="https://youtube.com/watch?v=H63iXMutkho">watch</a> || <a href="/videos/2023/04/14/Lets_talk_about_documents_discord_and_developments">transcript &amp; editable summary</a>)

Beau introduces a story involving documents, discord, and developments, discussing the arrest of Jake Tashara for the unlawful removal of national defense information and addressing doubts about information accuracy and potential impact.

</summary>

"This looks like irrational behavior."
"It's going to provide a nice illustration for most people in the United States."
"I think that this occurring at the same time as the Trump documents case is going to be very..."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic of documents, discord, and developments in a news story.
Expresses reluctance to talk without evidence but hints at a hunch regarding potential developments in the story.
Mentions a person named Jake Tashara being arrested for unlawful removal of national defense information.
Describes the arrest of Jake Tashara involving helicopters, armored cars, and SWAT teams, showcasing the seriousness of the situation.
Reports suggest Tashara held far-right extreme beliefs, including anti-Semitic and racist beliefs.
Points out the caution exercised by the US military in assessing individuals with extreme beliefs.
Speculates on the potential reasons behind the release of classified documents, including leaking due to emotional discord.
Mentions an ongoing counterintelligence assessment to determine the damage caused by the released documents.
Indicates that the situation does not seem to be a whistleblower case but rather irrational behavior.
Mentions doubts surrounding the accuracy of the information released, with some viewing it as intentional disinformation.
Notes that allied and adversarial nations are cautious about the information's accuracy and potential impact.
Compares the current situation to the Trump documents case, suggesting Trump's possession of more damaging information.
Concludes by mentioning the potential illustration provided by the current events in understanding the possible impact of Trump's activities.

Actions:

for citizens, news consumers,
Monitor updates on the story to understand potential implications (suggested)
Stay informed about national security developments and the accuracy of information being circulated (suggested)
</details>
<details>
<summary>
2023-04-14: Let's talk about US troops in Ukraine.... (<a href="https://youtube.com/watch?v=1eYPDqpr3wU">watch</a> || <a href="/videos/2023/04/14/Lets_talk_about_US_troops_in_Ukraine">transcript &amp; editable summary</a>)

Beau reveals the presence of US personnel in Ukraine amidst manufactured outrage, shedding light on accountability and potential covert operations.

</summary>

"I want you to picture a world where, let's say five or six months ago, Russia and people who are of like minds in the U.S. Congress."
"When you create an outrage of the day, like they did, that's the only possible response."
"The people who are super mad about this, in fact, there's one person who sits in the US Congress, who's kind of justifying the leak."
"It's just a thought, y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Reveals the presence of US personnel in Ukraine has recently surfaced, surprising many.
Describes a hypothetical scenario where concerns about US equipment being misused in Ukraine led to the deployment of personnel attached to the US embassy.
Mentions a sarcastic video made in November 2022 discussing the presence of these personnel as a response to prior outrage.
Notes that the personnel are focused on ensuring the proper use of equipment and are not involved in combat.
Suggests that while officially their role is accountability, they may also be providing advice.
Speculates about the presence of former US military members working for contracting companies in Ukraine, despite lack of evidence.
Emphasizes the role of manufactured outrage in shaping narratives and policies.
Points out a US Congress member justifying the leak about the troops, potentially involved in creating the situation.
Indicates that the deployment of personnel was likely already happening but became public due to manufactured outrage.
Encourages critical thinking when evaluating reporting, suggesting that if a topic falls within someone's expertise, they may have covered it before.

Actions:

for journalists, policymakers, concerned citizens,
Contact journalists or news outlets to demand further investigation into the presence of US personnel in Ukraine (suggested).
Reach out to policymakers to question the narrative and accountability surrounding the deployment of personnel (suggested).
Engage in critical thinking and fact-checking to understand the nuances behind manufactured outrage and its impact on policies (exemplified).
</details>
<details>
<summary>
2023-04-14: Let's talk about South Korea, the US, and a loan.... (<a href="https://youtube.com/watch?v=f9BPeuv115c">watch</a> || <a href="/videos/2023/04/14/Lets_talk_about_South_Korea_the_US_and_a_loan">transcript &amp; editable summary</a>)

Representative Green's premature and inaccurate statements on US-South Korea ammunition deal for Ukraine prompt fact-checking, revealing the significance of non-lethal aid support and the importance of reliable news sources.

</summary>

"Don't get your news from Facebook memes."
"It's not correctly framed by this representative shock, I know, right?"
"A country that is notorious for not providing lethal aid is, in theory, maybe, and maybe still, looking for ways to work around that long-standing principle because the situation in Ukraine is that important."

### AI summary (High error rate! Edit errors on video page)

Representative Green's statement about borrowing ammunition from South Korea for Ukraine sparks fact-checking and analysis.
The statement by Representative Green is criticized for inaccuracies and premature conclusions regarding the ammunition deal between the US and South Korea.
The deal discussed is not finalized but in negotiations, involving a loan of 500,000 rounds of howitzer ammunition, not 50,000 as mentioned.
South Korea's usual stance against providing lethal aid is part of the context for understanding the agreement.
The US is ramping up production of ammunition, indicating that the request for assistance from South Korea is not due to a shortfall.
The core issue is finding ways to support Ukraine without violating South Korea's principles against lethal aid.
The real story lies in the potential shift in South Korea's stance towards aiding Ukraine in a non-lethal manner.
The importance of accurate sources for understanding foreign policy is emphasized over relying on social media for news.

Actions:

for policy enthusiasts,
Contact your representatives to advocate for accurate and informed foreign policy decisions (implied).
Support reliable news sources and fact-check information before forming opinions (implied).
</details>
<details>
<summary>
2023-04-13: Let's talk about a special master on the Fox case.... (<a href="https://youtube.com/watch?v=RfFmaVQ_BqQ">watch</a> || <a href="/videos/2023/04/13/Lets_talk_about_a_special_master_on_the_Fox_case">transcript &amp; editable summary</a>)

New developments in the Fox News case reveal potential misconduct by Fox, causing significant concerns for the upcoming trial and possibly impacting Fox's reputation.

</summary>

"The judge plans to appoint a special master to probe possible misconduct by Fox."
"All of the information that has come out lately, well, it's a really bad sign for Fox."
"While trials like this, the outcome is never certain."

### AI summary (High error rate! Edit errors on video page)

Fox News case has new developments with surfaced audio recordings involving people associated with the Trump campaign discussing lack of evidence to support claims.
The judge is unhappy with Fox News due to the recordings and other concerns regarding possible misconduct.
The judge plans to appoint a special master to probe possible misconduct by Fox, including whether they misled the court or withheld evidence.
Dominion may conduct additional depositions based on the new evidence.
Fox News claims they turned over the information as soon as they knew about it, but the judge is concerned.
The upcoming trial is likely to be delayed due to these developments, but the judge may not be willing to postpone it.
The case is expected to be a major news story with constant updates once the trial starts, potentially impacting Fox News significantly.

Actions:

for media consumers, legal observers,
Stay informed about the developments in the Fox News case and follow updates on the trial (suggested).
Support accountability in media by demanding transparency and ethical conduct (implied).
</details>
<details>
<summary>
2023-04-13: Let's talk about Trump, maps, Milley, and a mess.... (<a href="https://youtube.com/watch?v=WX0sGZn0VBk">watch</a> || <a href="/videos/2023/04/13/Lets_talk_about_Trump_maps_Milley_and_a_mess">transcript &amp; editable summary</a>)

Federal investigators are delving into Trump's potential interests and unauthorized document distribution, hinting at more serious charges beyond willful retention in the Trump document case.

</summary>

"Willful retention is the least of his concerns."
"We're talking about gathering them with a specific purpose."
"We're no longer talking about a security lapse, we're talking about a security breach."
"Those are very telling."
"They are looking into something way bigger."

### AI summary (High error rate! Edit errors on video page)

Talks about the Trump document case and concerns about Trump's specific interest in General Milley.
Mentions federal investigators asking witnesses about a map that Trump showed them.
Speculates on the seriousness of the case based on willful retention of documents.
Explains the significance of willful retention and the potential implications of intentionally gathering and distributing sensitive information.
Raises concerns about possible espionage charges if certain questions lead to an indictment.
Emphasizes that the investigation goes beyond obstruction or willful retention, hinting at more serious charges being considered.
Points out that the federal government is building a case for something substantial beyond basic security lapses.
Expresses nervousness about the implications of the investigation's focus on specific interests and unauthorized distribution of documents.
Notes the shift from a mere security lapse to a potential security breach in the investigation.
Underlines the gravity of the situation based on the lines of questioning by federal investigators.

Actions:

for political analysts, investigators,
Stay informed on the developments in the Trump document case (implied).
Support accountability and transparency in political investigations (implied).
</details>
<details>
<summary>
2023-04-13: Let's talk about Jordan falling into a trap.... (<a href="https://youtube.com/watch?v=3E41VT2KhD4">watch</a> || <a href="/videos/2023/04/13/Lets_talk_about_Jordan_falling_into_a_trap">transcript &amp; editable summary</a>)

Beau dives into the misleading perception of crime rates in Democrat-run cities and rural areas, hinting at potential surprises in an upcoming hearing.

</summary>

"It feels like there's going to be some funny moments coming up..."
"They believe their own propaganda."
"It's going to be funny."

### AI summary (High error rate! Edit errors on video page)

Beau mentions Jordan and an upcoming hearing in Congress focusing on rates in New York.
He talks about the perception created by connecting high crime rates with Democrat-run cities.
Beau explains how setting a minimum population requirement skews lists towards Democrat-run cities.
He points out that rural areas or medium-sized cities often have higher crime rates than large cities like Chicago or New York.
Beau hints that Republicans in the hearing may be misled by right-wing outlets' misinformation.
He advises understanding the methodology behind such lists to counteract misinformation effectively.
Beau suggests watching a video to comprehend how rural areas often have higher crime rates.
He mentions that the Republicans may not be aware of this discrepancy and could be in for a surprise during the hearing.

Actions:

for viewers, congressional staff,
Watch the video mentioned to understand how crime rates are misrepresented (suggested).
Educate others on the methodology behind crime rate lists to combat misinformation (implied).
</details>
<details>
<summary>
2023-04-13: Let's talk about Biden and the Colorado river.... (<a href="https://youtube.com/watch?v=GoExR0W_m6Y">watch</a> || <a href="/videos/2023/04/13/Lets_talk_about_Biden_and_the_Colorado_river">transcript &amp; editable summary</a>)

Beau explains the Colorado River issues, Biden administration's plans, and why it's vital for states to decide on water usage cuts, despite potential political motives.

</summary>

"The states have to make these decisions and the precedent needs to be set that states can come together, work out a consensus agreement, and deal with issues like this in the future because there's going to be more and more."
"It will be better for everybody involved if the states make the decision."
"This has to do with the votes."
"There are tough decisions that are going to have to be made. This is going to happen more and more often."
"Again not the ideal reasoning but putting pressure on the states to get them to make the decision themselves it's the right move."

### AI summary (High error rate! Edit errors on video page)

Explains the issues surrounding the Colorado River, with many states depending on it for water.
Mentions that despite recent rainfall in the southwest, the problem still persists.
States have been unsuccessful in developing a plan to address the water usage issue.
The Biden administration introduced two plans, one impacting California heavily and the other affecting Arizona.
Biden administration did not specify which plan they will implement, putting pressure on the states to collaborate and find a solution.
Beau believes it is the right move for the states to make these decisions themselves.
States are better equipped to understand their needs compared to the federal government.
States collaborating to address such issues can set a precedent for the future.
Beau suspects political motives behind the Biden administration's decision, aiming to avoid backlash and negative narratives.
Biden administration's decision is driven by the desire to avoid being blamed for making cuts that impact water usage.
Beau acknowledges tough decisions lie ahead, including restrictions on growth for cities and changes for water-intensive farms and businesses.
Emphasizes that states coming together to make decisions is ideal for everyone involved.
Despite potential political math behind the decision, Beau believes pressuring states to act is the right move.
Beau concludes by expressing his thoughts on the situation and wishes everyone a good day.

Actions:

for environmental activists, water conservation advocates,
Collaborate with local communities and organizations to raise awareness about water conservation and sustainable usage (implied).
Support initiatives that focus on long-term planning for environmental sustainability in your region (implied).
</details>
<details>
<summary>
2023-04-12: Let's talk about the GOP realizing they're out of touch.... (<a href="https://youtube.com/watch?v=DZIjZZ136oQ">watch</a> || <a href="/videos/2023/04/12/Lets_talk_about_the_GOP_realizing_they_re_out_of_touch">transcript &amp; editable summary</a>)

Kellyanne Conway acknowledges the Republican Party's struggle to attract younger voters as societal progress moves forward, showing the need for the GOP to become more progressive.

</summary>

"They didn't become more conservative. Society moved forward and therefore they're more conservative in comparison to the more progressive standards of the day."
"The Republican Party has to become more progressive. There's no way around that."
"It is against the ideas of representative democracy. It is against the ideas of the republic."

### AI summary (High error rate! Edit errors on video page)

Kellyanne Conway acknowledges the Republican Party's struggle to attract younger voters and singles waiting to get married to become conservative.
Younger voters are turning out in increasing numbers and leaning towards the Democratic Party due to GOP's stance on family planning, climate change, and guns.
Republican strategists are out of touch as they believe they have won policy arguments on the economy and education.
The GOP angered many by denying relief for educational loans, making the American dream unattainable for young Americans.
Conway's statement about waiting for people to get older or married shows a misconception that people become more conservative with age.
Values and opinions are formed early in life and evolve with society; people don't suddenly become conservative as they age.
The Republican Party needs to alter its policy platform as younger voters will not become more conservative over time.
Conservatives are slow to adapt to change and are being left behind by younger generations moving towards progressive ideals.
Republicans need to become more progressive to appeal to younger voters who will continue trending towards the Democratic Party.
The belief that people become more conservative with age goes against the principles of representative democracy and the republic.

Actions:

for young voters,
Advocate for progressive policies within the Republican Party (exemplified)
Engage younger voters in political discourse and encourage participation (exemplified)
Support candidates who represent progressive values (exemplified)
</details>
<details>
<summary>
2023-04-12: Let's talk about Schumer and political games.... (<a href="https://youtube.com/watch?v=Y3peIyQBTh8">watch</a> || <a href="/videos/2023/04/12/Lets_talk_about_Schumer_and_political_games">transcript &amp; editable summary</a>)

Democrats in the Senate play a political game with a resolution condemning defunding the Department of Justice, putting Republicans in a tough spot and potentially driving a wedge between them and their voters.

</summary>

"Democrats in the Senate are not introducing a resolution to condemn defunding the police."
"The resolution doesn't actually do anything; it's just a game to put Republicans in an uncomfortable position."
"Most people may not understand that defunding the Department of Justice also impacts local police."

### AI summary (High error rate! Edit errors on video page)

Democrats in the Senate are not introducing a resolution to condemn defunding the police.
The situation developed after Trump's actions led to a vote on a resolution condemning defunding the Department of Justice.
Republicans are put in an uncomfortable position with this resolution, regardless of how they vote.
The resolution doesn't actually do anything; it's a political game to make Republicans cast a vote they'll have to answer for later.
Senators are not determining if they support defunding the Department of Justice; they're voting on potential attack ads against them.
The Democratic Party can use these votes to drive a wedge between senators and their voters.
Most people may not understand that defunding the Department of Justice also impacts local police due to grants and connections.
The vote is purely political maneuvering and means nothing in terms of actual action.
Defunding the Department of Justice in many ways means defunding the police, but this connection might not be clear to everyone.
Republicans will have to navigate this situation strategically to avoid backlash.

Actions:

for political observers, voters,
Contact your representatives to express your understanding of the political maneuvers happening in the Senate (suggested).
Stay informed about how political games like these impact policies and decisions affecting your community (implied).
</details>
<details>
<summary>
2023-04-12: Let's talk about Missouri libraries.... (<a href="https://youtube.com/watch?v=Ko18Du0dJaw">watch</a> || <a href="/videos/2023/04/12/Lets_talk_about_Missouri_libraries">transcript &amp; editable summary</a>)

Missouri Republicans aim to defund public libraries to control education and knowledge, going against the state constitution, revealing a deeper desire for manipulation and ignorance.

</summary>

"Education is power. Knowledge is power. Denying people education and denying people knowledge is denying them power."
"That's what this is about. It's about controlling people."
"In order to do what they want to do here, they have to get a constitutional amendment to the state constitution."
"Not just do they want you in your place, not just do they want you and your kids as ignorant as possible so they can be manipulated, they will do it in direct contradiction to the Constitution."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Missouri Republicans have decided to defund all public libraries in the state, reflecting an authoritarian desire to control and suppress education and knowledge.
This decision is about ensuring the people of Missouri follow orders, stay ignorant, and are easily manipulated by those in power.
The move to defund public libraries goes against the Missouri state constitution, which clearly states support for the establishment and development of free public libraries.
The Missouri House will need a constitutional amendment to proceed with defunding the libraries, as the constitution mandates support for public libraries.
Some members of the Missouri Senate oppose this move and have indicated that the funding will likely be reinstated, showing a divide between the House and Senate.
The attempt to defund libraries reveals a deeper agenda of keeping constituents ignorant and easily controlled, even if it means going against the state constitution.

Actions:

for missouri residents,
Contact your Missouri state legislators to express support for public libraries and urge them to uphold the state constitution (suggested)
Join local library advocacy groups to help protect public libraries in Missouri (implied)
</details>
<details>
<summary>
2023-04-12: Let's talk about Bragg vs Jordan.... (<a href="https://youtube.com/watch?v=FS8ItWaoPu0">watch</a> || <a href="/videos/2023/04/12/Lets_talk_about_Bragg_vs_Jordan">transcript &amp; editable summary</a>)

Exploring the implications of federal intervention in local matters through the Trump New York case, a potential domino effect on oversight emerges.

</summary>

"Let's say Jordan is successful in this, and that gets established. That's how things are going to go now."
"If Jordan gets his way, there's going to be a lot of really upset Republican governors."
"There are some people who have asked, you know, well, why can't the DA go after Congress?"
"No matter how they [Republicans] feel about Bragg, Bragg didn't indict Trump. The grand jury did."
"Expect this to be going on for the next few weeks and turn into a giant thing."

### AI summary (High error rate! Edit errors on video page)

Explaining the back and forth between the local and federal government regarding Trump's New York case.
Mentioning key figures involved in the situation: Bragg in the DA's office and Jordan and Republicans in Congress.
Describing Jordan's interest in investigating the charging decision, which Bragg opposes.
Summarizing the main arguments: Congress's power to intervene based on federal money used by the local prosecutor.
Pointing out the potential consequences of allowing federal intervention in local matters.
Speculating on the outcome and the potential impact on various local offices that have received federal funding.
Noting the possibility of oversight expanding to other areas if Jordan's actions set a precedent.
Mentioning the protection Congress has under the Speech and Debate Clause.
Predicting that Republicans may not achieve their desired outcome in this situation.

Actions:

for legal observers, political analysts,
Monitor the developments in the Trump New York case and stay informed about the implications of federal intervention (implied).
Advocate for preserving the balance between local and federal government powers in legal matters (implied).
</details>
<details>
<summary>
2023-04-11: Let's talk about confusion, conflation, and currency.... (<a href="https://youtube.com/watch?v=puCG21F6oUk">watch</a> || <a href="/videos/2023/04/11/Lets_talk_about_confusion_conflation_and_currency">transcript &amp; editable summary</a>)

Beau clarifies confusion about FedNow not being a digital currency, dispels fears around its implications, and explains the speculative nature of a potential US digital currency.

</summary>

"It's not a digital currency. It's not even consumer-facing."
"It's just an additional tool."
"Most people already use something kind of like it."
"Most of it's really just reinventing things that already exist."
"It's not even that big of a step from where we're at."

### AI summary (High error rate! Edit errors on video page)

Addressing confusion and conflation around the term "FedNow" due to inaccurate information circulating.
Explaining that FedNow is not a digital currency and is specifically for interbank communication and quick currency transfers.
Mentioning that most people won't interact with FedNow unless routinely transferring large sums between banks or during major transactions like buying a house.
Clarifying that the concept of a digital currency being discussed is separate from FedNow and is still in the exploratory phase.
Emphasizing that the development and implementation of a digital currency in the US require congressional approval, unlike FedNow.
Comparing the proposed digital currency ideas to existing payment platforms like PayPal or Cash App, dispelling conspiracies around it.
Noting that the idea of a digital currency is not as futuristic or alarming as it may seem, given the prevalence of digital transactions currently.

Actions:

for financially curious individuals,
Stay informed about financial matters (implied)
Educate others on the difference between FedNow and potential digital currency ideas (implied)
</details>
<details>
<summary>
2023-04-11: Let's talk about a win in Nashville.... (<a href="https://youtube.com/watch?v=JCTCe6bvbVc">watch</a> || <a href="/videos/2023/04/11/Lets_talk_about_a_win_in_Nashville">transcript &amp; editable summary</a>)

Nashville reinstates unjustly removed reps; Tennesseans urged to use 2024 ballot to hold legislature accountable and defend democracy.

</summary>

"You need to take the opportunity to use the ballot box and explain that that isn't how it's supposed to work."
"Anybody who voted for this, they need to have a really hard time getting re-elected."
"If you don't, you'll regret it."

### AI summary (High error rate! Edit errors on video page)

Nashville Metropolitan Council voted 36-0 to reinstate Justin Jones and another representative unjustly removed by the Tennessee State Legislature.
Jones and the other representative will be interim representatives until a special election is held.
The normal election is more than a year away, prompting the need for a special election.
Both representatives are qualified and intend to run for their seats again in the special election.
The special election is expected to occur within the next hundred days.
It is implied that Jones and the other representative will likely win due to their newfound celebrity status.
Beau urges the people of Tennessee to use the ballot box in 2024 to voice their disapproval of what happened.
Voters need to make it clear that removing the representatives was unacceptable.
Those who supported the removal should face challenges in getting re-elected.
Beau warns that complacency could lead to Tennessee becoming more authoritarian and intrusive.
Failing to hold the state legislature accountable could result in diminished representation for the people.
Beau stresses the importance of using the ballot box to uphold representative democracy.
He cautions that allowing unchecked actions by the legislature will have severe consequences.
Beau encourages Tennesseans to take action before it's too late and their voices are silenced.

Actions:

for tennesseans,
Use the 2024 ballot to express disapproval of the unjust removal of representatives (implied).
Ensure that those who supported the removal face challenges in getting re-elected (implied).
Take action now to prevent Tennessee from becoming more authoritarian and intrusive (implied).
</details>
<details>
<summary>
2023-04-11: Let's talk about Trump appealing and a theory.... (<a href="https://youtube.com/watch?v=QjfaFBHxjmQ">watch</a> || <a href="/videos/2023/04/11/Lets_talk_about_Trump_appealing_and_a_theory">transcript &amp; editable summary</a>)

Pence won't appeal, Trump likely to lose quickly, Democrats aiding Trump theory, GOP must reject Trump for change, high voter turnout key.

</summary>

"It has to be the Republican Party themselves rejecting them."
"Keeping Trumpism alive, I don't see that as a good strategy for the country."
"This country has to get out and vote on a level that it might not."
"Democrats stand a better shot of beating Trump than just about anybody else."
"Campaigns change things."

### AI summary (High error rate! Edit errors on video page)

Pence won't appeal a decision to talk to a grand jury, but Trump will.
Trump's appeal is likely to lose quickly, possibly without causing a delay.
Trump's behavior is becoming erratic, possibly due to rumors circulating about him.
Speculation suggests the Democratic Party may help Trump win the primaries.
Both Republicans and Democrats have entertained the idea of Democrats aiding Trump.
The Democratic Party has a history of elevating weak candidates to run against.
Despite his rhetoric, Trump was a losing candidate before the election overturn attempts.
Trump may have a good chance of winning the primary but likely not the general election.
Some Democratic strategists may prefer Trump to win the primary for strategic reasons.
It is vital for Republicans to reject Trump and his ideology for the country's sake.
Continuing to enable Trumpism is not a positive strategy for the nation.
The GOP needs to experience a significant loss with Trump for a strategy change.
Democratic Party beating Trump by a large margin could force a shift in GOP strategy.
There's a risk in letting Trump run in the general election due to potential outcomes.
The country needs high voter turnout to prevent Trump from winning again.
Democrats may stand a better chance against Trump compared to other Republicans.
Campaign dynamics can change, potentially favoring a different Republican candidate.
While Trump seems weak now, unforeseen circumstances could alter the campaign landscape.

Actions:

for politically engaged citizens,
Mobilize voter registration and turnout campaigns (suggested)
Advocate for rejecting Trumpism within the Republican Party (suggested)
</details>
<details>
<summary>
2023-04-11: Let's talk about Thomas, vacations, and the Judiciary Committee.... (<a href="https://youtube.com/watch?v=xJe2gge7rq8">watch</a> || <a href="/videos/2023/04/11/Lets_talk_about_Thomas_vacations_and_the_Judiciary_Committee">transcript &amp; editable summary</a>)

Democrats in the Senate address Justice Thomas's vacation habits, signaling potential legislative action or hearings to restore confidence in the Supreme Court's ethical standards.

</summary>

"An internal investigation that is done at the request of the Chief Justice would probably have a more favorable outcome for Thomas."
"The vacations, the yacht, all of that stuff, you've already seen the headline since then."

### AI summary (High error rate! Edit errors on video page)

Democrats in the Senate are planning to address Justice Thomas's unique vacation habits that are being examined.
The Senate Judiciary Committee has sent a strongly worded letter to the Chief Justice, indicating a hearing to restore confidence in the Supreme Court's ethical standards.
If the issue is not resolved internally, the Committee may take legislative action.
An internal investigation requested by the Chief Justice may be more favorable for Thomas.
Democrats in the Senate may propose legislation or conduct hearings if pressure continues to build up.
Focus may shift from vacations to Thomas's use of a private jet, which could be more problematic.
Public perception may be influenced by media coverage, potentially normalizing Thomas's vacation activities.
Investigation or hearings may prioritize Thomas's use of the private jet over other vacation aspects.
The outcome could involve internal Supreme Court processes or lead to more significant consequences for Thomas.
The Judiciary Committee's actions are ongoing but not yet definitive.

Actions:

for senators, judiciary committee,
Contact your Senators to express opinions on ethical standards in the Supreme Court (implied)
Stay informed about developments and potential actions surrounding Justice Thomas (implied)
</details>
<details>
<summary>
2023-04-10: The roads to April 2023.... (<a href="https://youtube.com/watch?v=5-Ugu-fxz7I">watch</a> || <a href="/videos/2023/04/10/The_roads_to_April_2023">transcript &amp; editable summary</a>)

Beau addresses a range of topics from Trump's credibility to controversial jokes, showcasing his views on various societal issues with candor and insight.

</summary>

"I don't immediately assume he's lying. I don't base anything off of what Trump says."
"Your belief system is based on the fact that the rest of your statement is not true. Just saying."
"Generally speaking, whatever he's saying is right."
"The more economic activity, the faster the money moves, the larger the GDP."
"Then send them to a Christian school."

### AI summary (High error rate! Edit errors on video page)

Monthly Q&A session with Beau covering various topics and questions from the audience.
Addressing skepticism towards Trump's statements by not taking his word seriously due to past experience.
Dismissing the notion of stopping support for reproductive rights, pointing out the importance of consent.
Explaining laughter while mentioning the names of arrested Democrats in Florida as a sign of camaraderie and support.
Acknowledging a viewer's insight on the term "well-spoken" and its implications on people with accents.
Clarifying the misconceptions surrounding a controversial joke involving kicking a box.
Explaining the decision-making process behind covering certain news topics over others based on audience interest and coverage.
Dissecting a misunderstood joke in the context of Iraq and why it faced backlash.
Teasing a potential future video addressing leaked secret documents with a personal hunch.
Addressing questions about the YouTube Studio app and sharing positive views on its utility.
Explaining how the economy grows beyond printing more money by considering factors like velocity and economic activity.
Responding to preferences for biblical teachings over suggestive content by suggesting attending a Christian school and reading the Bible.

Actions:

for engaged viewers seeking perspectives on current events and societal issues.,
Analyze past statements and actions of public figures before forming opinions (implied).
Advocate for consent and reproductive rights (implied).
Educate oneself on the implications of certain terms and stereotypes (implied).
Stay informed on a variety of news stories to understand differing perspectives (implied).
</details>
<details>
<summary>
2023-04-10: Let's talk about the Wizard of Oz, books, movies, and chaos.... (<a href="https://youtube.com/watch?v=22zUbK0A3wA">watch</a> || <a href="/videos/2023/04/10/Lets_talk_about_the_Wizard_of_Oz_books_movies_and_chaos">transcript &amp; editable summary</a>)

Beau responds to hate mail, clarifying misconceptions about "The Wizard of Oz" while challenging the allure of conspiracy theories in seeking order within chaos.

</summary>

"You're everything that's wrong with America."
"Human beings like patterns, their brains love patterns, and they try to find them."
"But the idea that there is some grand conspiracy, It's kind of like Dorothy's dream."
"Interestingly enough, had you actually read the book, you would kind of have a point."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Responds to hate mail referencing a comment on "The Wizard of Oz" and the chaos in the world.
Hate mail accuses Beau of being uneducated and catering to a "woke agenda."
Hate mail criticizes Beau for claiming there is no man behind the curtain in Wizard of Oz.
Hate mail implies that watching the movie and not reading the book influenced Beau's views.
Beau clarifies that he watched the movie and read the book, correcting misconceptions from the hate mail.
Points out differences between the book and the film, such as the color of the slippers and the presence of a curtain.
Explains how patterns and conspiracy theories arise from human brains seeking order in chaos.
Challenges the idea of a grand conspiracy, likening it to Dorothy's dream in The Wizard of Oz.
Notes that the dream sequence is not present in the book, questioning the hate mail's familiarity with the source material.
Concludes with a reflection on interpreting chaos and patterns in the world.

Actions:

for internet users,
Challenge conspiracy theories and seek evidence-based explanations (implied)
Read source materials before forming strong opinions (implied)
</details>
<details>
<summary>
2023-04-10: Let's talk about China and who owns America's farmland.... (<a href="https://youtube.com/watch?v=ba_JsDZdbVI">watch</a> || <a href="/videos/2023/04/10/Lets_talk_about_China_and_who_owns_America_s_farmland">transcript &amp; editable summary</a>)

Beau debunks fear-mongering claims about Chinese ownership of US farmland, revealing the reality behind the statistics and urging critical thinking over sensationalism.

</summary>

"China owns about 400,000 acres of agricultural land in the US, a little less than."
"400,000 acres isn't quite as huge as it sounds."
"They're not even a big investor when it comes to agricultural land in the US."
"It's fear-mongering, it's the same stuff."
"Not a big deal."

### AI summary (High error rate! Edit errors on video page)

Exploring the claims about Chinese ownership of US farmland to debunk common misconceptions and fear-mongering.
China owns about 400,000 acres of agricultural land in the US, not millions as some memes suggest.
Putting the numbers into perspective, comparing Chinese ownership to other entities like King Ranch, which is much larger.
Foreign countries collectively own about 3% of US agricultural acreage, with China owning only 1% of that.
Most of the farmland China owns came through acquiring a pork producer, not a deliberate land grab.
Debunks the idea that China owning US farmland could lead to food supply control in the event of a war.
Emphasizes that the fear-mongering around this issue is not based on reality but rather political propaganda.
Politicians may use the narrative of foreign ownership of farmland to stoke fear and advance certain agendas.
Concludes that the situation is not as alarming as portrayed and urges viewers not to be overly concerned.
Encourages critical thinking and a deeper understanding of the facts behind sensational claims.

Actions:

for citizens, fact-checker,
Verify information before sharing (implied)
Educate others on the actual statistics and percentages of foreign ownership of US farmland (implied)
Challenge fear-mongering narratives with facts and perspective (implied)
</details>
<details>
<summary>
2023-04-10: Let's talk about Asa Hutchinson.... (<a href="https://youtube.com/watch?v=PxBlCdsqouo">watch</a> || <a href="/videos/2023/04/10/Lets_talk_about_Asa_Hutchinson">transcript &amp; editable summary</a>)

Asa Hutchinson, seeking the Republican presidential nomination, faces challenges due to his past actions against far-right groups, potentially hindering his chances in a crowded field where candidates are openly opposing Trumpism.

</summary>

"He's to the right of normal, but still within the normal spectrum."
"One of the problems that he is definitely going to run into is that while most of the groups that he went up against back then, they're gone."
"This is one of those candidates that is coming forward and openly opposing Trumpism."

### AI summary (High error rate! Edit errors on video page)

Asa Hutchinson, seeking the Republican Party's presidential nomination, faces questions about his background and beliefs.
Hutchinson, a former governor of Arkansas, has extensive executive branch experience and is considered to the right of a normal conservative.
Despite calling on Trump to drop out, Hutchinson may not stand a chance due to his history of prosecuting far-right groups.
His past actions against far-right groups may alienate the energized base of the Republican Party.
Candidates openly opposing Trumpism are starting to emerge, and as Trump's legal problems mount, more may take a stance against him.

Actions:

for political analysts, voters,
Support candidates openly opposing harmful ideologies (implied)
</details>
<details>
<summary>
2023-04-09: Let's talk about doomsday planes and Facebook memes.... (<a href="https://youtube.com/watch?v=iyc1WGJ0Fss">watch</a> || <a href="/videos/2023/04/09/Lets_talk_about_doomsday_planes_and_Facebook_memes">transcript &amp; editable summary</a>)

Beau explains the truth behind a Facebook meme about the Navy acquiring doomsday planes, clarifying it's routine modernization rather than preparation for war with China.

</summary>

"Don't get your news from Facebook."
"That part [about doomsday planes] would be scary."
"It's just normal modernization as time goes along."

### AI summary (High error rate! Edit errors on video page)

Explains the concern about a meme on Facebook regarding the Navy building a new doomsday plane for nuclear war with China.
Describes doomsday planes as airborne command and control centers paired with the US strategic arsenal.
Mentions the Navy is indeed acquiring new doomsday planes, each costing around $250 million.
Clarifies that the purchase of these planes is not a sign of imminent nuclear war with China.
States that the current Navy planes in this role are old and need to be replaced with newer airframes.
Compares the replacement of the airframe to upgrading an old food truck with a new truck to ensure mission success.
Notes that the Navy plans to have three test planes first before acquiring nine more in the future.
Emphasizes that this modernization is a normal process as equipment ages, not a preparation for immediate conflict.
Addresses the accuracy of the Facebook meme regarding the cost and acquisition of the doomsday planes.
Concludes by advising against getting news from Facebook and suggesting it's just a routine equipment upgrade.

Actions:

for internet users,
Verify information from reliable sources before believing or sharing content online (implied)
Stay informed through credible news outlets rather than social media platforms (implied)
</details>
<details>
<summary>
2023-04-09: Let's talk about Recovering America's Wildlife.... (<a href="https://youtube.com/watch?v=faE9BC4MKzE">watch</a> || <a href="/videos/2023/04/09/Lets_talk_about_Recovering_America_s_Wildlife">transcript &amp; editable summary</a>)

Beau presents the urgent need for the Recovering America's Wildlife Act, stressing the critical role of adequate funding in preventing irreversible wildlife loss and environmental impact.

</summary>

"If we don't take the opportunity, if we don't start to address this now, it will be regretted later."
"The warning signs are there and we have the opportunity to do something about it before it is out of control."
"For years and years and years, you had people talking about how bad the water situation was going to be out west, and nobody wanted to do anything about it."
"The US has actually proven itself capable of doing it if the funding is there, if the resources are there, if the will is there."
"Do we have the will to actually work to solve it before it becomes a headline-grabbing issue?"

### AI summary (High error rate! Edit errors on video page)

Introduces the Recovering America's Wildlife Act, which has been reintroduced with bipartisan support.
About one-third of America's wildlife faces an elevated risk of extinction, impacting the environment and other species.
State-level plans exist to assist in wildlife recovery, but federal funding of $65 million/year is insufficient.
The Act aims to provide $1.4 billion annually to states and tribal governments to address wildlife conservation.
The funding will help mitigate disease among species, reestablish migration routes, and control invasive species.
Urges action now to prevent irreversible damage, drawing a parallel to the water crisis out west.
Emphasizes the importance of acting before the situation worsens and becomes irreversible.
Points out the successful conservation efforts in the US when adequate funding and resources are present.
Stresses the need for collective will to address wildlife conservation effectively.
Concludes with a call to seize the current moment and work towards a sustainable future.

Actions:

for conservationists, wildlife enthusiasts,
Advocate for the passing of the Recovering America's Wildlife Act by contacting your senators and representatives (suggested).
Join local conservation efforts or wildlife preservation organizations to contribute to positive change in your community (implied).
</details>
<details>
<summary>
2023-04-09: Let's talk about Nashville vs DC.... (<a href="https://youtube.com/watch?v=KwEYufJwbKs">watch</a> || <a href="/videos/2023/04/09/Lets_talk_about_Nashville_vs_DC">transcript &amp; editable summary</a>)

Addressing supposed similarities between Nashville and the Capitol reveals stark differences in intentions and actions, debunking any false equivalence.

</summary>

"One was to undermine the Constitution. The other was to exercise rights explicitly protected in it."
"Seems like they're not at all similar."
"They're not the same."

### AI summary (High error rate! Edit errors on video page)

Addressing the similarities between the events in Nashville and at the Capitol was initially resisted but later acknowledged as wise.
There was no property damage in Tennessee, unlike at the Capitol.
Contrary to expectations, there were very few arrests in Tennessee, with the Highway Patrol describing the protests as peaceful.
Despite assumptions, there were no injuries reported in Tennessee.
Neither weapons nor improvised items were found with the crowd in Tennessee.
Allegations of rifles being stashed at a nearby hotel or violent chat logs were not present in Tennessee.
Unlike the Capitol incident, no one was hit with an American flag in Tennessee.
The crowd in Tennessee did not chant to harm anyone.
The National Guard was not required in Nashville, unlike at the Capitol.
The purpose of the Nashville gathering was to petition for redress of grievances and peaceably assemble, not to undermine the Constitution.

Actions:

for online commentators,
Question false equivalencies and challenge misleading narratives (suggested)
Promote accurate understanding and critical thinking among online communities (suggested)
</details>
<details>
<summary>
2023-04-09: Let's talk about Finland and Putin's paranoia.... (<a href="https://youtube.com/watch?v=b3mQDyGHd34">watch</a> || <a href="/videos/2023/04/09/Lets_talk_about_Finland_and_Putin_s_paranoia">transcript &amp; editable summary</a>)

Beau explains the shifting international dynamics post Ukraine invasion, with Finland and Sweden moving towards NATO, increasing Putin's paranoia and potentially tying up Russian troops near borders.

</summary>

"Countries that were traditionally non-aligned, they decided that they wanted to be part of an alliance."
"He has become more and more paranoid, Putin has, to the point where there are now reports of loyalty tests."
"More troops up there, not fighting, not in Ukraine, I mean that's a win."

### AI summary (High error rate! Edit errors on video page)

Talks about Finland, Putin, Sweden, Turkey, and Russia in an international context.
Mentions how countries have shifted alignment post Putin's invasion of Ukraine.
Explains Finland and Sweden's decision to move closer to NATO.
Describes Russia's concerns with NATO countries bordering them.
Points out Putin's increasing paranoia and loyalty tests for officials.
Speculates on Russia's potential troop buildup near its borders.
Notes the drain on resources from stationing troops closer to borders.
Emphasizes Finland's contribution to tying up Russian troops near their border.
Comments on the security measures in Russia to protect Putin from internal threats.
Concludes with the potential impact of Russia stationing troops near borders.

Actions:

for international observers,
Monitor international relations and developments (implied)
Stay informed about geopolitical shifts (implied)
</details>
<details>
<summary>
2023-04-08: Let's talk about the Wisconsin GOP talking about impeachments.... (<a href="https://youtube.com/watch?v=5ZMjc-YTsJ0">watch</a> || <a href="/videos/2023/04/08/Lets_talk_about_the_Wisconsin_GOP_talking_about_impeachments">transcript &amp; editable summary</a>)

Beau warns Wisconsin Republicans of potential backlash in upcoming elections if they disregard voters' will and undermine democracy by impeaching a judge.

</summary>

"If they go against the will of those voters and they try to engage in authoritarian measures from the legislature, they'll pay for it in 2024."
"You should probably just leave that judge alone."
"They will have angered voters. They will have canceled out the will of voters who voted for them and crossed party lines to vote for her."
"I'm fairly certain that you'll end up handing the Democratic party a trifecta."
"Her massive win means that people that voted for them crossed party lines to vote for her."

### AI summary (High error rate! Edit errors on video page)

Talks about the recent events in Wisconsin, particularly focusing on the Republican-controlled legislature.
Mentions the Supreme Court election in Wisconsin where a progressive judge won by a significant margin.
Points out the possibility of the legislature overriding a governor's veto or impeaching a judge due to the seats they picked up.
Raises concerns about Republicans potentially disregarding the will of voters and undermining democracy.
Emphasizes the repercussions for Republicans in the upcoming elections if they take authoritarian measures.
Suggests that going against the voters' will may lead to significant losses for the Republican Party in 2024.
Urges Republicans to reconsider their actions and the impact on voters who crossed party lines in the previous election.
Warns that upsetting voters now could result in a tougher race for Republicans later on.
Expresses a belief that such actions could ultimately benefit the Democratic party.
Advises leaving the judge alone to avoid potential electoral consequences.

Actions:

for wisconsin voters,
Reassess actions and decisions considering the impact on voters who crossed party lines (implied)
</details>
<details>
<summary>
2023-04-08: Let's talk about new Fox decisions and the pillow guy.... (<a href="https://youtube.com/watch?v=OkmoOSYJq48">watch</a> || <a href="/videos/2023/04/08/Lets_talk_about_new_Fox_decisions_and_the_pillow_guy">transcript &amp; editable summary</a>)

Beau explains Fox's struggles, potential Murdochs testimony, and financial influences on air content while urging attention to these critical issues.

</summary>

"Some texts and the idea that decisions related to the accuracy of what might be said on the air on Fox were heavily influenced by financial considerations, that seems like something that should be talked about."
"It keeps people focused on the matter at hand."
"This is about whether or not Fox defamed Dominion."

### AI summary (High error rate! Edit errors on video page)

Explaining about Fox, rulings, and the Murdochs, and related topics.
Fox isn't doing well in the early stages before the trial.
The recent ruling suggests the Murdochs may testify.
The Fox legal team wanted to avoid the Murdochs testifying.
Surprisingly, the case won't involve discussing January 6th events.
The case focuses on whether Fox defamed Dominion.
Texts and comments from Tucker's team surfaced regarding the pillow guy.
There were concerns about what the pillow guy might say on air.
Financial considerations influenced decisions about what to air on Fox.
Beau hopes this information doesn't get buried amidst other news.

Actions:

for news consumers, activists, watchdogs,
Share and raise awareness about how financial considerations impact media content (implied).
Support unbiased media outlets that prioritize truth over financial gain (implied).
</details>
<details>
<summary>
2023-04-08: Let's talk about Russia not being able to deny it anymore.... (<a href="https://youtube.com/watch?v=csikoLiV11E">watch</a> || <a href="/videos/2023/04/08/Lets_talk_about_Russia_not_being_able_to_deny_it_anymore">transcript &amp; editable summary</a>)

Russia can no longer deny covert operations following a cafe bombing, revealing internal power struggles and confirming suspicions.

</summary>

"Russia can no longer deny recent events."
"We no longer have to just repeat the official statements with sarcasm."
"They have to acknowledge it, which means it's going to become public inside Russia."

### AI summary (High error rate! Edit errors on video page)

Russia can no longer deny recent events after downplaying or denying them for months.
An investigation into an incident at a cafe revealed the bombing of a military blogger critical to Russia's information operations.
The blogger was believed to be giving a class on writing to further Russian narratives before the explosion.
Russia has been blaming recent unfortunate events on anything but acknowledging them as operations.
Possible perpetrators behind the attacks include Ukrainian special services, a rebel group, or an internal power struggle within the Russian elite.
The recent cafe bombing indicates an internal power struggle or potentially a rebel group's involvement.
Speculations include the bombing as a message to a high-ranking figure associated with the cafe.
The individual in custody may have been manipulated into carrying out the attack, as suggested by her behavior.
The incident will likely result in increased security measures in Russia and divert resources from the war effort.
This event confirms suspicions of covert operations in Russia, leading to a shift in public acknowledgment of such incidents.

Actions:

for international observers,
Monitor developments in Russia and stay informed on emerging details (implied).
Support efforts to increase transparency and accountability in Russia (implied).
</details>
<details>
<summary>
2023-04-08: Let's talk about China, Taiwan, and shipping.... (<a href="https://youtube.com/watch?v=uxWBnEM4w0w">watch</a> || <a href="/videos/2023/04/08/Lets_talk_about_China_Taiwan_and_shipping">transcript &amp; editable summary</a>)

China's plans to inspect shipping in the Strait of Taiwan raise tensions with the U.S., testing American willingness to confront China over Taiwan.

</summary>

"A Taiwanese ship being sunk would likely be interpreted as an act of aggression."
"The situation is delicate..."
"This is the first instance where there's something that really can spiral out of control."

### AI summary (High error rate! Edit errors on video page)

China plans to inspect shipping in the Strait of Taiwan, escalating tensions with Taiwan and the United States.
Taiwan instructs ships to call if they encounter a patrol, defying China's inspection plans.
The United States historically supports Taiwan, but the situation could quickly escalate into a confrontation.
Potential naval vessel encounters and ship inspections could lead to volatile situations.
The U.S. must decide how to respond early to prevent spiraling events.
The Biden administration and Congress face a test in determining if the U.S. is willing to back its rhetoric on defending Taiwan.
American people must question if they are ready to risk a confrontation with China over Taiwan.
There's a delicate balance between defending Taiwan and maintaining relationships with allies in the region.
The situation is complex, with potential consequences for U.S. diplomacy and international relations.
Missteps or inflammatory statements could escalate tensions unnecessarily.

Actions:

for foreign policy analysts, policymakers,
Monitor the situation in the Taiwan Strait and stay informed (implied)
Avoid inflammatory statements or actions that could escalate tensions (implied)
</details>
<details>
<summary>
2023-04-07: Let's talk about the report on leaving over there.... (<a href="https://youtube.com/watch?v=XoI6omJGp1M">watch</a> || <a href="/videos/2023/04/07/Lets_talk_about_the_report_on_leaving_over_there">transcript &amp; editable summary</a>)

Beau provides a critical analysis of the Afghanistan withdrawal report, focusing on accountability, intelligence failures, and the need to prevent similar events in the future.

</summary>

"Nobody gets to complain, nobody gets to whine if their team shoulders blame on this."
"Heads of state and negotiators cannot tell the opposition that they want to leave and establish a timeline."
"The estimates are supposed to be prepared objectively, not averaged out among everybody."
"You shouldn't be worrying about politics at this point. You should be worrying about making sure this doesn't happen again."
"But I'm sure because the Republican Party wants to like two hearings on this, this report will come up again."

### AI summary (High error rate! Edit errors on video page)

Analyzing the report and summary of the Afghanistan withdrawal to ensure it doesn't happen again.
Criticizing the attention given to critical issues in the withdrawal process.
Emphasizing the mistake of establishing a timeline for withdrawal publicly.
Pointing out the flaw in intelligence estimates due to groupthink.
Holding the Biden administration accountable for not addressing potential outcomes.
Stressing the need to focus on preventing unnecessary suffering in future events.
Mentioning that Trump's foreign policy decisions heavily impacted the Afghanistan situation.
Noting that blame for the situation in Afghanistan extends beyond Trump to previous administrations.
Suggesting that strategies employed, rather than individuals on the ground, contributed to strengthening the opposition.
Urging focus on preventing similar events rather than playing politics.

Actions:

for policymakers, analysts, activists.,
Contact policymakers to ensure accountability in future decision-making (implied).
Organize community forums to raise awareness about the implications of intelligence failures (implied).
</details>
<details>
<summary>
2023-04-07: Let's talk about reactions to the Justice Thomas news.... (<a href="https://youtube.com/watch?v=Nu37vBfhTGM">watch</a> || <a href="/videos/2023/04/07/Lets_talk_about_reactions_to_the_Justice_Thomas_news">transcript &amp; editable summary</a>)

Public officials are navigating calls for action in response to allegations against Clarence Thomas, with uncertainty surrounding the concrete steps to be taken.

</summary>

"They know it's wrong, but they don't know exactly how to respond politically yet."
"We will act. But what act means, well, that's really open to interpretation there, isn't it?"
"It is worth noting that according to the reporting that the Supreme Court, the highest court in the land, actually has the lowest ethical standards..."

### AI summary (High error rate! Edit errors on video page)

Public officials' reactions to the allegations against Clarence Thomas from ProPublica are being discussed.
Democrats like Elizabeth Warren and Sheldon Whitehouse have made vague statements about the situation.
AOC is more specific, calling for Thomas to be impeached.
Dick Durbin, a key figure on the Senate Judiciary Committee, has mentioned that action will be taken, but what that entails is unclear.
There is uncertainty about how public officials will respond and what actions they will take.
Calls for Thomas to be impeached or resign are circulating, but the actual course of action remains unknown.
The Supreme Court is noted for having relatively loose ethical standards compared to other institutions.
Expectations are that substantial developments may not occur until Monday, as officials strategize their response.
It is suggested that officials may be taking time to plan a course of action in light of the allegations.

Actions:

for politically engaged individuals,
Contact your representatives to express your views on how they should respond to the allegations (suggested)
Stay informed about any developments in the situation and be ready to advocate for ethical accountability in government (implied)
</details>
<details>
<summary>
2023-04-07: Let's talk about messages and Tennessee.... (<a href="https://youtube.com/watch?v=XiFxHAU453M">watch</a> || <a href="/videos/2023/04/07/Lets_talk_about_messages_and_Tennessee">transcript &amp; editable summary</a>)

Beau shines a light on Tennessee's legislature, exposing their messages of control and urging defiance through voting to reclaim power and representation.

</summary>

"You better stay in your place, do what you're told, listen to your betters, and just accept your fate because you don't get a voice."
"They're telling you, you will obey the party, period."
"The Republican Party in Tennessee succeeded in creating tens of thousands of lifelong Democrats when they did this."
"The Tennessee State Legislature showed its hand. They're not representatives, they're rulers."
"If you put them back in office, it's going to send the message that it's unacceptable."

### AI summary (High error rate! Edit errors on video page)

Recounts a personal experience from middle school where he was scared by high schoolers in the woods, reflecting on a cave incident.
Explains his familiarity with Tennessee, mentioning attending the same yearly field trip to the Hermitage as others.
Criticizes the Tennessee state legislature for expelling three targeted individuals, sending clear messages with their actions.
Points out the racial bias in the legislature's actions, sparing a white woman while booting two black men, implying skin color played a role.
Asserts that the legislature's message isn't just about racism but also about asserting power and control over all Tennesseans.
Analyzes the underlying message as one that diminishes the importance of individual voices and representation in favor of obedience to authority.
Debunks the legislature's framing of their actions as defending the Second Amendment, revealing it as a tactic to distract and control the population.
Calls out the manipulation around the topic of gun control, used to divert attention and generate fear to ensure compliance.
Emphasizes the goal of the legislature: to enforce obedience and loyalty to the party above all else, disregarding democracy and individual rights.
Encourages Tennesseans, especially young people, to resist this control by voting and sending a clear message of defiance against authoritarian tactics.

Actions:

for tennessee residents,
Vote to re-elect the expelled individuals if they run again (suggested)
Use your power at the ballot box to reinstate those who were removed from office (exemplified)
Ensure that the voices of constituents are not silenced by voting against authoritarian tactics (implied)
</details>
<details>
<summary>
2023-04-07: Let's talk about Trump's mouth, his supporters, and a judge.... (<a href="https://youtube.com/watch?v=U0dHReNyLTs">watch</a> || <a href="/videos/2023/04/07/Lets_talk_about_Trump_s_mouth_his_supporters_and_a_judge">transcript &amp; editable summary</a>)

Beau explains the situation between a judge, Trump, and Trump supporters, warning that it's the supporters' actions that could land Trump in trouble.

</summary>

"It's not Trump that will end up creating the situation necessarily, because Trump could get out there and be pretty mellow and just say normal Trumpy things, it's the reaction from his supporters and what his supporters say and do that will end up getting Trump in trouble."
"The judge has more power here than I think people realize."

### AI summary (High error rate! Edit errors on video page)

Explains the developing situation in New York with a judge and Trump's involvement.
Details how the judge politely warned Trump not to stir the pot during the arraignment.
Mentions Trump's negative comments about the judge and the threats the judge received from Trump supporters.
Outlines the judge's options, which include a stern talking to, a gag order, or even putting Trump in a cell if his behavior doesn't change.
Emphasizes that it's not Trump's direct actions but the reactions of his supporters that may land him in trouble.
Points out that the timeline of potential consequences depends on how extreme Trump's statements become.

Actions:

for legal observers, political analysts,
Contact legal experts for insights on the judge's potential actions (suggested)
Stay informed on the developments in New York's legal situation (implied)
</details>
<details>
<summary>
2023-04-06: Let's talk about the GOP in North Carolina and Tennessee.... (<a href="https://youtube.com/watch?v=UP8W8MooDn8">watch</a> || <a href="/videos/2023/04/06/Lets_talk_about_the_GOP_in_North_Carolina_and_Tennessee">transcript &amp; editable summary</a>)

Tricia Cawthon's party switch in North Carolina gives Republicans a veto-proof supermajority, paving the way for extreme legislation and potential testing grounds for the GOP's strategies nationwide.

</summary>

"North Carolina and Tennessee may become testing grounds for the Republican Party's extreme and authoritarian projects."
"The Republican Party is likely to continue down a failing strategy path, catering to a narrowing group rather than addressing broader issues."

### AI summary (High error rate! Edit errors on video page)

Tricia Cawthon, elected as a Democrat in a deep blue district in North Carolina, switched her party affiliation to Republican, giving Republicans a veto-proof supermajority.
This shift enables Republicans in North Carolina to push through legislation without fear of veto.
North Carolina and Tennessee may become testing grounds for the Republican Party's extreme and authoritarian projects.
The focus could be on pushing through legislation that may negatively impact various aspects like politics, economics, education, and public safety.
Republicans seem more concerned with maintaining a specific voter base rather than solving actual problems.
The party's strategy appears to center around cultural wars rather than improving lives, hoping to energize their base.
The Republican Party is likely to continue down a failing strategy path, catering to a narrowing group rather than addressing broader issues.
There is a fear of more extreme and authoritarian legislation being passed in North Carolina and Tennessee.
These states might become synonymous with Florida and Texas in terms of controversial legislation.
The Republican Party's approach seems to involve scapegoating and reducing rights of certain groups.

Actions:

for north carolinians, political observers,
Organize community meetings to stay informed and engaged with local politics (implied).
Mobilize voter education efforts to ensure awareness of legislative changes and impacts (implied).
</details>
<details>
<summary>
2023-04-06: Let's talk about the Exonerated 5 and a letter to Trump.... (<a href="https://youtube.com/watch?v=qjHx3pCYUvg">watch</a> || <a href="/videos/2023/04/06/Lets_talk_about_the_Exonerated_5_and_a_letter_to_Trump">transcript &amp; editable summary</a>)

Beau showcases an open letter addressing Trump's actions against the Exonerated Five and the need for positive change at the community level, stressing the preservation of civil liberties over vengeance.

</summary>

"Money doesn't buy class."
"The primary concern is not vengeance, it's preservation of civil liberties."
"Hope that you endure whatever penalties are imposed with the same strength and dignity."

### AI summary (High error rate! Edit errors on video page)

Talks about an open letter written to Trump regarding the Central Park Five case.
Mentions Trump's actions in calling for the execution of the Central Park Five.
Points out that the conviction of the Central Park Five was overturned, and they were renamed the Exonerated Five.
Describes how Trump influenced public opinion against the Exonerated Five.
Addresses the issues faced by the community during Trump's time in office.
Expresses the desire for positive change at the community level.
Hopes that Trump receives the presumption of innocence, something the Exonerated Five didn't get.
Stresses the importance of enduring penalties with strength and dignity if found guilty.
Emphasizes the preservation of civil rights in the open letter.
Condemns Trump's actions that undermined the system over the years.

Actions:

for community members,
Work with dedicated community members to build a better future (suggested)
Advocate for the preservation of civil rights and civil liberties (implied)
</details>
<details>
<summary>
2023-04-06: Let's talk about Pence's pragmatic political power play.... (<a href="https://youtube.com/watch?v=LB5BJ2F5-hc">watch</a> || <a href="/videos/2023/04/06/Lets_talk_about_Pence_s_pragmatic_political_power_play">transcript &amp; editable summary</a>)

Pence's decision not to appeal benefits himself and the Republican Party, aiming for a swift legal process before the primaries to potentially capitalize on Trump's troubles.

</summary>

"I fought him on that subpoena. I didn't walk in there to DOJ and rat Trump out. I did what I could."
"The Republican Party at this point has turned into an episode of The Sopranos."
"You're only as good as your last envelope and Trump's envelopes aren't really big anymore."

### AI summary (High error rate! Edit errors on video page)

Pence decided not to appeal a ruling, sparking questions about his decision.
Previously, Pence was informed he had to testify to the grand jury, with some exceptions.
Opting not to appeal appears to benefit Pence and the Republican Party.
Appeals could have caused delays for Trump, but dragging the process out doesn't help Pence's presidential aspirations.
With Trump already indicted in New York, it's in Pence's best interest for the legal process to move swiftly before the primaries.
If Trump is knocked out of the race, it may benefit Pence politically.
Delaying the process could lead to Trump losing supporters within the Republican Party.
The current political landscape resembles an episode of The Sopranos, with alliances shifting based on self-interest.
Those seeking political influence may distance themselves from Trump if his support wanes.
Pence's decision not to appeal is seen as the smart move for the Republican Party and himself.

Actions:

for political observers,
Support community leaders (implied)
</details>
<details>
<summary>
2023-04-06: Let's talk about Clarence Thomas, gifts, and disclosure.... (<a href="https://youtube.com/watch?v=Lsr22hwtjo4">watch</a> || <a href="/videos/2023/04/06/Lets_talk_about_Clarence_Thomas_gifts_and_disclosure">transcript &amp; editable summary</a>)

Beau talks about Supreme Court Justice Clarence Thomas and Republican mega donor Harlan Crowe's extravagant vacations, gift disclosures, and the potential for an investigation and calls for resignation.

</summary>

"Vacations that if you were to assign a dollar amount to them would be tens of thousands or hundreds of thousands of dollars."
"The use of the Jet that's going to be an issue if there is an investigation."
"This is definitely not a story that's going to go away."

### AI summary (High error rate! Edit errors on video page)

Beau introduces the topic of two friends, Supreme Court Justice Clarence Thomas and Republican mega donor Harlan Crowe.
Crowe allegedly took Thomas on extravagant vacations worth tens or even hundreds of thousands of dollars, including cruising on a mega yacht and trips to Indonesia.
The focus is on the appearance of a Supreme Court justice vacationing with a GOP mega donor, which raises concerns about gift disclosures.
Thomas failed to disclose these gifts properly, which may lead to civil or criminal penalties.
Despite Crowe being generous with vacations for many people, this does not excuse the lack of disclosure by Thomas.
The vacations might not be the most significant issue; the use of a jet without Crowe could pose a bigger problem and may trigger an investigation.
The jet use, potentially costing 20 to 70 grand per trip, could be harder to dismiss compared to the vacations.
Calls for Thomas to resign have emerged, and there may be an ethics investigation into his actions and the accuracy of his disclosure forms.
The situation involving Thomas and Crowe appears to be escalating into a scandal that won't easily fade away.
Beau acknowledges the unprecedented nature of the situation and suggests following the reporting from ProPublica for more details.

Actions:

for journalists, activists, citizens,
Read and share the reporting from ProPublica to stay informed (suggested)
Follow any updates on the situation to understand its implications (suggested)
</details>
<details>
<summary>
2023-04-05: Let's talk about whether NY is still the weakest case.... (<a href="https://youtube.com/watch?v=X0QoHBzQzXY">watch</a> || <a href="/videos/2023/04/05/Lets_talk_about_whether_NY_is_still_the_weakest_case">transcript &amp; editable summary</a>)

Beau believes New York remains the weakest legal case against the former president, citing unproven methods and potential delays, alongside a focus on another case involving classified documents.

</summary>

"Initially, I thought they were going to have a hard time demonstrating some of what they were alleging."
"Realistically, one is almost as good as thirty."
"I think our justice system is broken."
"There might have been another motive."
"Anyway, it's just a thought."

### AI summary (High error rate! Edit errors on video page)

Identifies New York as the weakest case among the legal entanglements facing the former president.
Initially believed demonstrating allegations in the New York case might be challenging.
Acknowledges that if even half of the claims in the indictment can be proven, it could lead to charges sticking.
Notes that the method used to elevate charges from misdemeanor to felony in the New York case remains unproven.
Speculates that the sentencing outcome might not significantly differ whether one or multiple charges stick.
Mentions LawTube potentially dissecting and explaining the indictment process.
Expresses certainty that LawTube will provide more insights into the legal aspects of the case.
Considers the delay in the legal process as a result of the slow-moving justice system.
Suggests that further developments in other cases may arise before significant progress in the New York case.
Indicates a shift in focus towards determining what charges to indict the former president for in another case involving classified documents.

Actions:

for legal analysts and concerned citizens.,
Stay informed about legal developments and analyses from reliable sources (exemplified).
Monitor updates on various legal cases involving the former president (exemplified).
</details>
<details>
<summary>
2023-04-05: Let's talk about notes for Republicans and Wisconsin.... (<a href="https://youtube.com/watch?v=Q7Zb9ey8rv8">watch</a> || <a href="/videos/2023/04/05/Lets_talk_about_notes_for_Republicans_and_Wisconsin">transcript &amp; editable summary</a>)

Beau discussed the Wisconsin election, urging the Republican Party to change course after a surprising victory by the progressive judge, signaling potential issues for the party in 2024.

</summary>

"The voters in Wisconsin understood the assignment."
"The Republican Party has to change course and do it quick."
"That election was a sign."

### AI summary (High error rate! Edit errors on video page)

Discussed the importance of a recent election in Wisconsin for the Supreme Court and its national implications.
Noted the significance of Wisconsin as a swing state in potential election challenges.
Mentioned the impact on redistricting and reproductive rights.
Described the unexpected landslide victory of the progressive judge by about 10 points.
Pointed out the lack of concession from the Republican candidate due to the wide margin of loss.
Suggested that the Republican Party may need to change course after the election results.
Speculated on the implications for the party's future in 2024.
Criticized the Republican Party's current strategies and underperformance in recent elections.
Warned about potential severe issues for the Republican Party in 2024 if they continue on the same path.
Referenced Lindsey Graham's remarks on the party's direction.

Actions:

for political strategists,
Smart Republican strategists should re-evaluate their approach now (implied).
</details>
<details>
<summary>
2023-04-05: Let's talk about clarifying Trump NY coverage.... (<a href="https://youtube.com/watch?v=G3pyLarZnSs">watch</a> || <a href="/videos/2023/04/05/Lets_talk_about_clarifying_Trump_NY_coverage">transcript &amp; editable summary</a>)

Former President Donald J. Trump faces criminal charges amidst inaccurate media coverage and controversy over conflicts of interest, marking an unprecedented historic event.

</summary>

"Trump is not facing a conspiracy charge."
"Trump is not looking at 134 years."
"What exactly is the conflict with the judge?"
"A former President of the United States is facing criminal charges, 34 of them."
"It's just a thought, y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Former President Donald J. Trump went through the booking process, entered a plea of not guilty, and faced a lot of media coverage.
Trump is not facing a conspiracy charge, despite initial incorrect reporting from some outlets.
Reports claiming Trump could face 134 years in prison are inaccurate; this is a low-level felony with sentences likely to run concurrently rather than consecutively.
Despite some Trump supporters showing up, the numbers were not significant, with only about 300 in attendance.
One Congress member compared Trump to Jesus and Mandela, leading to opposition from the crowd.
There was controversy over inflammatory rhetoric used by Trump and a tweet from his son featuring the judge's daughter, raising questions about conflicts of interest.
This historic event marks a former President of the United States facing criminal charges, a total of 34 counts.

Actions:

for journalists, activists, legal experts,
Research and verify information before reporting (exemplified)
Stand up against inaccurate media coverage by sharing corrected information (exemplified)
</details>
<details>
<summary>
2023-04-05: Let's talk about Trump, NY, and if Biden wanted him in cuffs.... (<a href="https://youtube.com/watch?v=s2R3WAokXp0">watch</a> || <a href="/videos/2023/04/05/Lets_talk_about_Trump_NY_and_if_Biden_wanted_him_in_cuffs">transcript &amp; editable summary</a>)

Beau dives into Trump's case, dissecting judicial impartiality and debunking conspiracy theories, showcasing federal leniency towards Trump.

</summary>

"Those spreading this kind of information, they're just assuming that their base is not smart enough to figure it out."
"The fact that Trump isn't awaiting the outcome of this inside of a room with bars is proof positive that the federal government, that Biden's DOJ, is not actually out to get him."
"There is no man behind the curtain."

### AI summary (High error rate! Edit errors on video page)

Exploring the Trump New York case, judges, and impartiality, with a focus on recent information surfacing.
The judge in the case commented on Trump's rhetoric and social media statements and how they may be perceived.
Trump's sons tweeted an article featuring the judge's daughter, insinuating a conflict of interest due to her involvement with the Harris campaign.
Questions arise about the specific conflict of interest being suggested and whether political views of family members affect judicial decisions.
Beau points out the inconsistency in implying judges must be apolitical, especially given cases involving Trump appointed judges.
The essence of a judge lies in their ability to set personal beliefs aside, despite attempts to imply conspiracy.
Beau dissects a far-fetched theory involving Biden orchestrating Trump's arrest, ridiculing the idea as implausible and insulting to intelligence.
He argues that Biden, if he truly wanted Trump detained, could simply request a risk assessment from the intelligence community.
Beau criticizes the leniency shown by Biden's Department of Justice towards Trump, contrasting it with how others in similar situations are treated.
Despite the conspiracy theories, Beau stresses that the federal government is not actively pursuing Trump, evident in the leniency shown.

Actions:

for activists, political observers,
Reach out to the intelligence community for a risk assessment on individuals of concern (suggested).
Stay informed about political proceedings and hold officials accountable (implied).
</details>
<details>
<summary>
2023-04-04: Let's talk about top Florida Dems getting arrested.... (<a href="https://youtube.com/watch?v=Yj9N5kETK1w">watch</a> || <a href="/videos/2023/04/04/Lets_talk_about_top_Florida_Dems_getting_arrested">transcript &amp; editable summary</a>)

Americans watch prominent figures in New York, while Florida's clash over reproductive rights underscores the need for active engagement beyond headline events.

</summary>

"You really couldn't have scripted this better."
"We can't forget that there are other things going on that have a very tangible impact on people's lives."
"At this point it's not a spectator sport. You have to get involved."
"I am not sure I have ever seen anybody as apathetic to getting cuffed as Ms. Freed was in that."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Americans anticipate proceedings involving prominent Republican and Democratic figures today, with all eyes on New York.
In Tallahassee, Florida, a peaceful assembly in support of reproductive rights clashed with law enforcement over First Amendment rights.
Florida officials instructed demonstrators to disperse by sundown, leading to a sit-in protest.
Demonstrators, including prominent political figures, were arrested for trespassing after defying orders to leave.
Despite overshadowing headlines, other ongoing fights across the country impact people's rights significantly.
Beau urges viewers to not treat unfolding events as a spectator sport but to actively participate and be aware of broader issues affecting rights.
He predicts more politicians may face minor offenses due to authoritarian approaches to constitutionally protected rights.
Beau notes the significance of not overlooking less flashy news that impacts lives directly.
The transcript concludes with Beau recommending watching the footage and commenting on a political figure's nonchalant reaction to being arrested.
Beau underlines the importance of staying engaged and active in current events.

Actions:

for political activists and engaged citizens,
Support peaceful assemblies for causes you believe in (suggested)
Stay informed and engaged in ongoing fights impacting rights (implied)
</details>
<details>
<summary>
2023-04-04: Let's talk about Trump, NY, blunders, and election chances.... (<a href="https://youtube.com/watch?v=gggADDbTe-A">watch</a> || <a href="/videos/2023/04/04/Lets_talk_about_Trump_NY_blunders_and_election_chances">transcript &amp; editable summary</a>)

Analyzing Trump's slim chances of re-election post-indictment in New York through polling data and electoral perspectives, it appears highly unlikely that he could secure another term in the White House.

</summary>

"It is incredibly unlikely that Trump ever sleeps in that White House again."
"Even if he wins the primary, that's not winning numbers. Those are horrible numbers."
"People will always remember his attempt at altering the outcome of the election."
"I don't think he stands a chance in 2024. Not a realistic one."
"It's just a thought, y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Analyzing Trump's chances of re-election post-indictment in New York from an electoral perspective, not a legal one.
Trump lost in 2020 due to failed leadership and his leadership style, not just post-election actions.
Polling shows 60% of Americans approve of Trump's indictment in New York, while 62% of independents support it.
79% of Republicans disapprove of Trump's indictment in New York, leaving 21% who don't disapprove.
Unlikely that Trump, lacking majority support from Americans, independents, and even a significant portion of Republicans, can win re-election.
Authors of opinion pieces often write based on desired outcomes rather than realistic predictions.
Even if Trump secures the Republican primary, the lack of broad support makes a successful 2024 run unlikely.
Trump's influence remains significant, but the prospect of him returning to the White House seems improbable.
People's memory of Trump's actions, especially around the January 6th events, will likely impact his political future.
Despite fears, Beau believes Trump's chances in 2024 are slim, barring unforeseen circumstances.

Actions:

for voters, political analysts,
Educate others on the polling data surrounding Trump's indictment and its potential impact on his political future (implied).
</details>
<details>
<summary>
2023-04-04: Let's talk about Trump and gag orders.... (<a href="https://youtube.com/watch?v=P4eyVPJKnho">watch</a> || <a href="/videos/2023/04/04/Lets_talk_about_Trump_and_gag_orders">transcript &amp; editable summary</a>)

Speculation surrounds whether the judge will issue a gag order to Trump, with experts uncertain and all eyes on the evolving situation.

</summary>

"It's really what it boils down to."
"The judge probably wants to try the case within the courtroom, not in the court of public opinion."
"No matter what happens, it's likely to change."
"It's up to the judge."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Speculation swirls around whether the judge will issue a gag order to Trump.
Divided opinions exist, with some expecting a gag order and others not.
Beau predicts a cautious approach from the judge, potentially leading to a narrow order.
Concerns center around Trump's influence through public statements on the case.
Balancing the First Amendment with potential jury influence creates a delicate situation.
Experts are uncertain about the judge's decision in this matter.
The final decision rests with the judge, amid intense speculation.
Any decision made today is likely to evolve and change over time.
Trump's actions, whether following or violating an order, could impact the case dynamics.
The unfolding events will require patience and observation.

Actions:

for legal observers,
Stay informed about the developments in the legal proceedings (implied).
Monitor how Trump's public statements could impact the case (implied).
Advocate for transparency and fairness in legal processes (implied).
</details>
<details>
<summary>
2023-04-04: Let's talk about Texas, books, and a decision.... (<a href="https://youtube.com/watch?v=Zdswj1rIImM">watch</a> || <a href="/videos/2023/04/04/Lets_talk_about_Texas_books_and_a_decision">transcript &amp; editable summary</a>)

A federal judge ruled against viewpoint discrimination in removing books from libraries, showcasing the ongoing unconstitutional culture war on LGBTQ-friendly content.

</summary>

"The federal judge decided that the plaintiffs are likely to succeed on their viewpoint discrimination claim."
"In most cases, people making these decisions do not understand how the Constitution works."
"Supreme Court understands that if content discrimination is allowed, it is unlikely that the conservative side comes out ahead."
"It's an ongoing thing."
"Judge ordered the books to be returned to the shelves within 24 hours."

### AI summary (High error rate! Edit errors on video page)

News out of Texas: a Texas county board decided to remove a bunch of books from a library, leading to a lawsuit by patrons.
Federal judge ruled that removal of books from libraries based on viewpoint content discrimination violates the First Amendment.
Many jurisdictions across the country are using libraries to remove LGBTQ-friendly content, regardless of the viewpoint.
People making these decisions often lack understanding of how the Constitution and First Amendment work.
Judge ordered the books to be returned to the shelves within 24 hours, showing certainty in the outcome.
Ongoing culture war involves unconstitutional actions by individuals.
Supreme Court likely to uphold decisions against content discrimination to avoid favoring one side of the country.
Overall, the issue revolves around the unconstitutional removal of books from libraries based on viewpoint discrimination.

Actions:

for librarians, book lovers, advocates,
Contact local libraries and offer support in defending against viewpoint discrimination (suggested).
Join community advocacy groups working to protect LGBTQ-friendly content in libraries (suggested).
</details>
<details>
<summary>
2023-04-03: Let's talk about Trump, documents, Milley, and questions.... (<a href="https://youtube.com/watch?v=ylO-RzkQ4Bs">watch</a> || <a href="/videos/2023/04/03/Lets_talk_about_Trump_documents_Milley_and_questions">transcript &amp; editable summary</a>)

Trump's personal document selection and investigators' interest in staffers signal ongoing developments in the case, with potential implications for the investigation.

</summary>

"Trump personally went through the documents to choose things that he wanted to hold on to."
"There are still developments. There are definitely new lines of inquiry being opened."
"I hope for everybody's sake. It's simply because a lot of the documents that were recovered from Trump's office or something like that had to do with him, and it's all a coincidence."

### AI summary (High error rate! Edit errors on video page)

Trump personally selected documents to retain after being informed to return them.
Investigators are keen on staffers surrounding Trump at the club.
Often, those with the most information are not in the public eye.
Staffers like assistants, housekeepers, and security personnel hold valuable information.
Their job involves a lot of written communication, potentially leading to future revelations.
Investigators are questioning individuals about Trump's interest in General Milley.
The reason behind this line of questioning could significantly impact the investigation.
Speculation about potential reasons for Trump's interest in Milley is discouraged.
The investigation into the documents case is ongoing, with new lines of inquiry opening up.
The situation bears watching as it continues to develop.

Actions:

for investigators, concerned citizens,
Stay informed on developments in the Trump document case (implied).
Be vigilant about potential implications of new lines of inquiry (implied).
</details>
<details>
<summary>
2023-04-03: Let's talk about Republican or Democrat.... (<a href="https://youtube.com/watch?v=kdjCAEBLHjY">watch</a> || <a href="/videos/2023/04/03/Lets_talk_about_Republican_or_Democrat">transcript &amp; editable summary</a>)

Billy Young stresses the importance of tailoring arguments to individuals for effective communication and thought-shifting.

</summary>

"Support policy over party or personality."
"You have to tailor your argument to the person that you're talking to."
"If you're trying to use those conversations to shift thought, you have to tailor your argument to the person that you're talking to."

### AI summary (High error rate! Edit errors on video page)

Emphasizes the importance of tailoring arguments to reach people effectively, beyond using the strongest arguments.
Talks about the narrow perception of party loyalty versus ideological consistency when identifying as Republican or Democrat.
Advocates for supporting policy over party or personality to encourage moving beyond party lines.
Suggests expanding the Overton window to encourage critical thinking and moving beyond talking points.
Recommends adjusting messaging to appeal to the person you're speaking with, like promoting a cooperative society over competition.
Shares an example of using anecdotal evidence effectively to challenge preconceived notions, especially for those who rely on personal observations.
Stresses the need to customize arguments based on the individual to shift thoughts effectively.
Encourages tailoring arguments to resonate with the listener for successful communication and thought-shifting.

Actions:

for communicators, activists, advocates,
Tailor your arguments to individuals to effectively communicate and shift thoughts (suggested).
Use anecdotal evidence when appropriate to challenge preconceived notions (implied).
</details>
<details>
<summary>
2023-04-03: Let's talk about Nashville, queens, and 2 stories.... (<a href="https://youtube.com/watch?v=rbUOk3K6_RU">watch</a> || <a href="/videos/2023/04/03/Lets_talk_about_Nashville_queens_and_2_stories">transcript &amp; editable summary</a>)

Two stories from Nashville: federal judge blocks drag ban enforcement, Reba McIntyre speaks out against ban and advocates for focusing on real issues.

</summary>

"This isn't something that I would suggest saying, 'well we told you it was going to go this way?' Yeah, unconstitutionally vague."
"I wish they would spend that much time and energy and money on feeding the homeless children."
"So there is definitely a shift that has occurred, as we have talked about repeatedly, it is in New South."

### AI summary (High error rate! Edit errors on video page)

Two separate stories from Nashville, usually for two videos, but both covered together due to a busy news week.
First story: a federal judge temporarily blocked the enforcement of a ban on cabaret, called the drag ban, due to First Amendment concerns.
Concerns raised about nationwide legislation trying to force marginalized groups from public life.
Despite predictability of court decision, questions remain on Supreme Court interpretation and ongoing legislation impact.
Second story: Reba McIntyre, known for avoiding political statements, expressed disappointment over the ban, urging focus on more pressing issues like feeding homeless children.
McIntyre's rare political statement significant given her usual avoidance of such topics.
Shift in country music royalty's stance towards acceptance and support for marginalized communities.

Actions:

for activists, music fans,
Support marginalized communities by attending drag shows and cabaret performances (implied)
Volunteer at or donate to organizations supporting homeless children (implied)
</details>
<details>
<summary>
2023-04-02: Let's talk about developments in the Fox case.... (<a href="https://youtube.com/watch?v=QuRS7uOoRyw">watch</a> || <a href="/videos/2023/04/02/Lets_talk_about_developments_in_the_Fox_case">transcript &amp; editable summary</a>)

The judge's decision on the Fox case underscores the importance of facts over opinions, with upcoming trials potentially reshaping perceptions post-2020 election.

</summary>

"Facts are facts."
"These cases are likely to help push things back to the idea that facts do matter."

### AI summary (High error rate! Edit errors on video page)

Following the developments of the Fox case and the judge's devastating decision.
The judge ruled that statements about Dominion in the 2020 election are false.
The case is scheduled to go to jury trial starting on April 17th.
Stressing the importance of remembering the impact of misinformation.
Emphasizing that beliefs in false information can lead to harmful consequences.
Urging attention to cases related to Fox's coverage post-2020 election.
Hoping that these cases will help people realize the importance of facts over opinions.
Asserting the critical need for a baseline of reality in today's era of alternative facts.
Advocating for the significance of acknowledging and valuing facts.
Encouraging a shift back to valuing reality to address the current climate effectively.

Actions:

for advocates, activists, concerned citizens,
Share information about the Fox case and its implications (suggested)
Pay attention to upcoming trials related to misinformation (implied)
Advocate for the importance of facts in public discourse (exemplified)
</details>
<details>
<summary>
2023-04-02: Let's talk about cops, reasonable expectations, and training.... (<a href="https://youtube.com/watch?v=zKflxAAKm1A">watch</a> || <a href="/videos/2023/04/02/Lets_talk_about_cops_reasonable_expectations_and_training">transcript &amp; editable summary</a>)

Beau Young explains why cops couldn't meet unrealistic top-tier expectations and why it's impractical for them to strive for perfection in every situation.

</summary>

"Unless you've gone through a door yourself, shut the hole under your nose."
"The goal of departments should be to get them good enough to do it and complete the mission."
"It's not that people are opposed to them training. It's that getting them to the level that those critiques were based on is going to prohibit them from being first on the scene."
"The level of training it takes to get to the level that people apparently wanted them to be at, it prohibits them from also being patrol officers."
"I think that's kind of unreasonable."

### AI summary (High error rate! Edit errors on video page)

Explains why cops couldn't perform at a top tier level in a specific situation in Nashville.
Mentions the unrealistic comparison made between cops and top tier U.S. military.
Emphasizes the extensive training required to reach a certain skill level.
Points out that constant training and mindset changes might affect cops' roles.
Argues that pushing cops to meet unrealistic standards may hinder their primary duties.
Suggests that departments should focus on getting cops good enough to complete missions.
States that aiming for perfection in every cop may not be practical or beneficial.
Raises concerns about the impact of intense training on cops' roles as patrol officers.
Acknowledges the importance of training but questions the need for perfection in all situations.
Concludes by suggesting that the unrealistic expectations placed on cops may be unreasonable.

Actions:

for police reform advocates,
Contact local police departments for community policing initiatives (implied)
Attend community meetings to address concerns about police training and expectations (implied)
</details>
<details>
<summary>
2023-04-02: Let's talk about SECDEF vs Republicans.... (<a href="https://youtube.com/watch?v=ofH1CYyyAo4">watch</a> || <a href="/videos/2023/04/02/Lets_talk_about_SECDEF_vs_Republicans">transcript &amp; editable summary</a>)

The Secretary of Defense appeals to Republicans to stop blocking military promotions, citing impacts on readiness and retirements, revealing Republican focus on controlling women rather than prioritizing national security.

</summary>

"It is about, once again, the Republican Party wanting to control women."
"Republicans attempting to grandstand and score political points while jeopardizing U.S. readiness."
"The legal mechanism that they used to do this is holding up."

### AI summary (High error rate! Edit errors on video page)

The Secretary of Defense appealed to Republicans in Congress to stop blocking promotions for higher ranking members of the US military, impacting around 160 officers.
Republicans are blocking promotions because they are upset that the Department of Defense is supporting women in the military with leave time for family planning.
Women make up about 20% of the military force, and the DOD wants to ensure their readiness.
The holdup in promotions is causing ripple effects and downstream issues within the military.
Troops moving around installations and promotions being associated with different commands are being hindered by the promotion block.
The promotion block affects retirements, readiness, and the ability of the US military to respond to challenges.
Republicans are being critiqued for focusing on controlling women rather than considering the broader implications of their actions.
The Secretary of Defense emphasized how the promotion block is impeding readiness as the US faces complex geopolitical situations.
The impact on retirement for high-ranking officers will worsen if the promotion block continues.
The Republican Party's actions are seen as grandstanding and prioritizing political points over military readiness and personnel well-being.

Actions:

for congressional constituents,
Contact your Congressional representatives to express support for ending the promotion block (suggested)
Join advocacy groups supporting gender equality in the military (implied)
Organize community dialogues on the importance of military readiness and gender equality in the armed forces (implied)
</details>
<details>
<summary>
2023-04-02: Let's talk about McCarthy, budgets, and the debt ceiling.... (<a href="https://youtube.com/watch?v=cTmqxF4tbOc">watch</a> || <a href="/videos/2023/04/02/Lets_talk_about_McCarthy_budgets_and_the_debt_ceiling">transcript &amp; editable summary</a>)

Republicans are facing challenges with the budget and debt ceiling negotiations, risking the US economy's stability under McCarthy's leadership.

</summary>

"Republicans have to act. They have to get the debt ceiling raised. They have to put forth a budget of their own or accept Biden's."
"The Republican Party with McCarthy at the helm is headed towards steering the US economy into devastation."

### AI summary (High error rate! Edit errors on video page)

Republicans made promises about the budget and the debt ceiling, wanting to tie them together.
The debt ceiling is artificial and manufactured by Congress to create political tension.
Biden has put forth a suggested budget, but Republicans have not released theirs.
McCarthy and Republicans are engaging in social media sensationalism instead of negotiating.
Republicans may not want to put forth a budget plan and prefer negotiating Biden's budget.
Another reason for the lack of a Republican budget could be over-promising and fear of backlash from their base.
McCarthy is facing challenges within the Republican Party due to various factions not agreeing on a budget.
Republicans need to have a starting point to negotiate with Biden but are running out of time.
If McCarthy can't rally Republicans behind a budget, he may be speaker in name only.
Failure to raise the debt ceiling and a possible default could have devastating consequences on the US economy.

Actions:

for politically engaged citizens,
Contact your representatives to urge them to prioritize raising the debt ceiling and coming up with a budget plan (suggested)
Join local political organizations or movements advocating for responsible fiscal policies (implied)
</details>
<details>
<summary>
2023-04-01: Let's talk about schools, legislation, and security.... (<a href="https://youtube.com/watch?v=iG52e8XqpGE">watch</a> || <a href="/videos/2023/04/01/Lets_talk_about_schools_legislation_and_security">transcript &amp; editable summary</a>)

Beau explains the limitations of gun legislation and advocates for practical security measures to make a tangible impact, urging realistic expectations and immediate action.

</summary>

"Prevention and security versus legislation."
"Calling for an AR ban is the same as saying that you send thoughts and prayers."
"It's something, and it's something that can be done, and it's something that's going to have an impact."
"Be very realistic in your expectations."
"Do something. This is something."

### AI summary (High error rate! Edit errors on video page)

Differentiates between prevention, security, and legislation in addressing pressing issues in the country.
Explains the limitations of focusing solely on legislation for gun control.
Illustrates the potential roadblocks in passing ideal gun legislation through the Supreme Court.
Emphasizes the importance of focusing on practical measures like physical security and prevention.
Argues that advocating for an AR ban may not be the most effective use of political capital at the moment.
Stresses the need for realistic expectations regarding gun control measures.
Advocates for implementing practical solutions that can have a tangible impact, such as enhancing physical security measures.
Urges viewers to prioritize actions that can make a difference in the current landscape.
Encourages a shift towards initiatives that can be implemented regardless of the political climate.
Suggests that creating delays through physical security measures can be an effective approach.

Actions:

for advocates for gun reform,
Implement practical security measures to create delays and prevent harm (implied).
Prioritize actions with tangible outcomes that can be implemented regardless of political obstacles (implied).
Advocate for realistic expectations and focus on initiatives that can make a difference in the current landscape (implied).
</details>
<details>
<summary>
2023-04-01: Let's talk about Trump and New York going first.... (<a href="https://youtube.com/watch?v=QJ8_-qcSJoo">watch</a> || <a href="/videos/2023/04/01/Lets_talk_about_Trump_and_New_York_going_first">transcript &amp; editable summary</a>)

Commentators worry about New York going first in the Trump case, but outcomes won't dictate all cases; American electorate understands better.

</summary>

"Should they have coordinated? Absolutely not."
"Just because New York started first doesn't mean they're going to end first."
"The American electorate in general is a little bit smarter than that."

### AI summary (High error rate! Edit errors on video page)

Commentators are concerned about the impact of the New York case being the first on perceptions of other cases against Trump.
Coordinating cases against Trump across the country could give the appearance of a witch hunt, even if it's not.
There is speculation that the New York case is the weakest based on publicly available information, but the evidence is under seal.
Prosecutors in different cases against Trump are likely to move forward at their own pace rather than taking turns.
Social media echo chambers may amplify concerns about the New York case's outcome influencing perceptions of other cases.
Most Americans are not likely to view the New York case's outcome as indicative of all other cases against Trump.
Trump loyalists are expected to maintain their support regardless of the case outcomes, while undecided individuals are the ones who matter in forming opinions.
Beau believes that the American electorate is capable of understanding that outcomes in different cases are not directly linked.

Actions:

for american voters,
Trust the American electorate to understand complex legal proceedings (implied)
Focus on engaging undecided voters rather than catering to entrenched positions (implied)
</details>
<details>
<summary>
2023-04-01: Let's talk about Russia, North Korea, and missteps... (<a href="https://youtube.com/watch?v=BleMDsj_0yc">watch</a> || <a href="/videos/2023/04/01/Lets_talk_about_Russia_North_Korea_and_missteps">transcript &amp; editable summary</a>)

Beau explains the repercussions of prioritizing competitive foreign policies over humanitarian aid, urging for a more cooperative approach to prevent crises like North Korea's food shortage.

</summary>

"It's cheaper to be a good person. It causes less suffering."
"Beyond America's borders do not live a lesser people."
"Countries don't have morality, they don't have friends, they have interests."

### AI summary (High error rate! Edit errors on video page)

Talks about foreign policy and its impact on countries like North Korea and Russia.
Mentions a previous situation where North Korea faced a food shortage.
Criticizes the lack of tangible efforts from the West to help alleviate the food shortage in North Korea.
Raises concerns about Russia offering food to North Korea in exchange for weapons to be used in Ukraine.
Points out the potential consequences of North Korea accepting the deal with Russia.
Emphasizes that providing food is a cheaper and more humane option than dealing with military weapons.
Criticizes the competitive nature of foreign policies that prioritize interests over humanitarian aid.
Expresses disappointment in the missteps of Western foreign policy and the lack of genuine humanitarian aid.
Speculates on the likelihood of North Korea accepting the deal with Russia due to matching equipment.
Condemns the prioritization of competitive interests over basic humanitarian needs.
Urges for a shift towards more cooperative and humanitarian foreign policies.

Actions:

for global citizens,
Provide real aid without strings attached to countries facing crises like food shortages (exemplified).
Advocate for foreign policies that prioritize humanitarian aid over competitive interests (implied).
</details>
<details>
<summary>
2023-04-01: Let's talk about Republican solutions to Nashville.... (<a href="https://youtube.com/watch?v=Tclw-2jrrHI">watch</a> || <a href="/videos/2023/04/01/Lets_talk_about_Republican_solutions_to_Nashville">transcript &amp; editable summary</a>)

Republicans propose reactive solutions to recent events, but Beau calls for preventative measures to stop such incidents from occurring.

</summary>

"Investigating it in that manner does nothing. Absolutely nothing."
"The goal should be to stop it."
"Please make it something that would actually stop it from happening."

### AI summary (High error rate! Edit errors on video page)

Republicans proposed two solutions in response to recent events in Nashville.
One solution was to label the incident as a hate crime, which Beau questions due to the lack of a known motive.
Beau questions the effectiveness of labeling it as a hate crime since it changes charges but won't lead to a trial in this case.
He criticizes this solution as reactive and not aimed at preventing future incidents.
The other solution proposed is federal legislation to make such incidents a capital offense.
Beau doubts the effectiveness of imposing the death penalty as a deterrent since many perpetrators don't expect to survive.
He stresses the need for solutions that focus on prevention rather than post-incident reactions.
Beau calls for solutions that can actually stop such incidents from happening, not just serve as talking points.

Actions:

for legislators, policymakers, activists,
Advocate for preventative measures to address hate crimes (implied)
Push for solutions that focus on stopping incidents before they occur (implied)
</details>
<details>
<summary>
2023-04-01: Let's talk about Dolly, Miley, and Garth.... (<a href="https://youtube.com/watch?v=b_iglNZKSoI">watch</a> || <a href="/videos/2023/04/01/Lets_talk_about_Dolly_Miley_and_Garth">transcript &amp; editable summary</a>)

A school stops students from performing songs by Dolly Parton and Miley Cyrus, while the Academy of Country Music Awards feature hosts known for their acceptance and support, sending a message of inclusivity.

</summary>

"Traditional family values was encouraging children to be the best they can be. If your parents are black and white, if your parents are the same sex, that's still traditional family values to me." - Garth Brooks
"Both of them were allies before most of us knew what an ally was, and they have been very open about their support." - Beau

### AI summary (High error rate! Edit errors on video page)

School in Wisconsin stops students from performing "Rainbowland" duet by Dolly Parton and Miley Cyrus, citing potential controversy around acceptance and being a good person.
Initially, the school also planned to stop the performance of "Rainbow Connection" by the Muppets for similar reasons but later reversed that decision.
Beau questions the attempt to cancel performances by Dolly Parton and the Muppets, suggesting a political angle.
The Academy of Country Music Awards will be hosted by Dolly Parton and Garth Brooks, known for their acceptance and support, especially in controversial areas like marriage equality.
Garth Brooks, in the early '90s, expressed that traditional family values include encouraging children to be the best they can be, regardless of their parents' race or sexual orientation.
Both Dolly Parton and Garth Brooks have been allies before it became mainstream, openly supporting causes like marriage equality.
Beau anticipates a message being sent by selecting Dolly Parton and Garth Brooks as hosts for the Academy of Country Music Awards.
Garth Brooks has a history of advocating for marriage equality, even when it was a controversial topic.
The selection of Dolly Parton and Garth Brooks as hosts is seen as a significant move in the country music scene, indicating a message about acceptance and equality.
Beau wonders if the choice of hosts for the awards show and the school's decision to halt performances are related or just a coincidence.

Actions:

for country music fans,
Support and amplify artists who advocate for acceptance and equality (implied).
</details>
