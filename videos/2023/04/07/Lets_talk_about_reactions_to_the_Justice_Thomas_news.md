---
title: Let's talk about reactions to the Justice Thomas news....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Nu37vBfhTGM) |
| Published | 2023/04/07 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Public officials' reactions to the allegations against Clarence Thomas from ProPublica are being discussed.
- Democrats like Elizabeth Warren and Sheldon Whitehouse have made vague statements about the situation.
- AOC is more specific, calling for Thomas to be impeached.
- Dick Durbin, a key figure on the Senate Judiciary Committee, has mentioned that action will be taken, but what that entails is unclear.
- There is uncertainty about how public officials will respond and what actions they will take.
- Calls for Thomas to be impeached or resign are circulating, but the actual course of action remains unknown.
- The Supreme Court is noted for having relatively loose ethical standards compared to other institutions.
- Expectations are that substantial developments may not occur until Monday, as officials strategize their response.
- It is suggested that officials may be taking time to plan a course of action in light of the allegations.

### Quotes

- "They know it's wrong, but they don't know exactly how to respond politically yet."
- "We will act. But what act means, well, that's really open to interpretation there, isn't it?"
- "It is worth noting that according to the reporting that the Supreme Court, the highest court in the land, actually has the lowest ethical standards..."

### Oneliner

Public officials are navigating calls for action in response to allegations against Clarence Thomas, with uncertainty surrounding the concrete steps to be taken.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Contact your representatives to express your views on how they should respond to the allegations (suggested)
- Stay informed about any developments in the situation and be ready to advocate for ethical accountability in government (implied)

### Whats missing in summary

Additional context and details on the specific allegations against Clarence Thomas and the implications for Supreme Court ethics. 

### Tags

#ClarenceThomas #ProPublica #PublicOfficials #SupremeCourt #Ethics


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about reactions
from public officials when it comes to the
reporting that has come out from ProPublica
about Clarence Thomas and the very unique
travel arrangements he is alleged to have participated in.
Okay, so the allegations came out.
I said, if there was an investigation, a lot of people were like, do you not expect one?
I'm not sure.
It's unprecedented, you don't really know what they're going to do here.
There have been some statements from public officials, all Democrats, which is completely
unsurprising.
So what do you have?
You have Elizabeth Warren came out, basically do something very vague as to what the something
is.
You have Sheldon Whitehouse, who is a Democratic senator, a long-time critic of the ethics
standards at the Supreme Court.
Definitely wanting action, but again, not real specific.
AOC came out.
She was pretty specific.
She wants him impeached.
Then you have the one person that matters.
Dick Durbin, who is the top person on the Senate Judiciary Committee.
This is a person that has a lot of sway over what is going to happen, if anything, when
it comes to this situation.
And basically, to sum it up, he said, well, we will act.
What does act mean?
No clue.
It could be that they actually really look into it and move with AOC's idea or come
up with some other method of holding him accountable.
It could mean establishing a stricter code of ethics for the Supreme Court and letting
it go.
We don't know.
Right now, I think there's a lot of people who are just processing.
They know it's wrong, but they don't know exactly how to respond politically yet.
I can tell from the comments about it, I think most people watching this channel would like
to see him impeached or resign, which that may actually be on the table as something
that he might consider, depending on how widespread this practice is.
But right now, we have no clue what they're really going to do.
You have a vow from the person that matters saying, we will act.
But what act means, well, that's really open to interpretation there, isn't it?
So that's really all there is to know at this point.
There will probably not be substantial developments.
I wouldn't expect to hear more until like Monday.
I could be wrong about that, but my guess is those with the influence to actually do
something about this, they're going to take their time in plotting out a course of action.
They were probably kind of blindsided by it.
That's pretty much it.
It is worth noting that according to the reporting that the Supreme Court, the highest court
in the land, actually has the lowest ethical standards, the loosest ethical standards that
they have to comply with.
That might need to be adjusted.
So anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}