---
title: Let's talk about messages and Tennessee....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=XiFxHAU453M) |
| Published | 2023/04/07 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recounts a personal experience from middle school where he was scared by high schoolers in the woods, reflecting on a cave incident.
- Explains his familiarity with Tennessee, mentioning attending the same yearly field trip to the Hermitage as others.
- Criticizes the Tennessee state legislature for expelling three targeted individuals, sending clear messages with their actions.
- Points out the racial bias in the legislature's actions, sparing a white woman while booting two black men, implying skin color played a role.
- Asserts that the legislature's message isn't just about racism but also about asserting power and control over all Tennesseans.
- Analyzes the underlying message as one that diminishes the importance of individual voices and representation in favor of obedience to authority.
- Debunks the legislature's framing of their actions as defending the Second Amendment, revealing it as a tactic to distract and control the population.
- Calls out the manipulation around the topic of gun control, used to divert attention and generate fear to ensure compliance.
- Emphasizes the goal of the legislature: to enforce obedience and loyalty to the party above all else, disregarding democracy and individual rights.
- Encourages Tennesseans, especially young people, to resist this control by voting and sending a clear message of defiance against authoritarian tactics.

### Quotes

- "You better stay in your place, do what you're told, listen to your betters, and just accept your fate because you don't get a voice."
- "They're telling you, you will obey the party, period."
- "The Republican Party in Tennessee succeeded in creating tens of thousands of lifelong Democrats when they did this."
- "The Tennessee State Legislature showed its hand. They're not representatives, they're rulers."
- "If you put them back in office, it's going to send the message that it's unacceptable."

### Oneliner

Beau shines a light on Tennessee's legislature, exposing their messages of control and urging defiance through voting to reclaim power and representation.

### Audience

Tennessee residents

### On-the-ground actions from transcript

- Vote to re-elect the expelled individuals if they run again (suggested)
- Use your power at the ballot box to reinstate those who were removed from office (exemplified)
- Ensure that the voices of constituents are not silenced by voting against authoritarian tactics (implied)

### Whats missing in summary

Full context and emotional depth of Beau's impassioned plea for Tennesseans to reclaim their power through voting and resist authoritarian control.

### Tags

#Tennessee #StateLegislature #Authoritarianism #Voting #Representation #Power


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we're gonna talk about Tennessee and messages.
Messages plural, because multiple messages went out today
and I wanna make sure that everybody is aware
of the message that was intended for them.
But before we do that,
I'll tell you some things you may not know about me.
When I was in middle school,
some high schoolers took me and a friend
to a place they called the Bell Witch Cave. To this day, I have no idea whether or not there was
a cave even there. What they really did was take us out in the woods, scare us, and then leave us.
We wound up out there for a couple of days. Today, this would probably be a national news story,
but it was a different time. I learned how to ride a horse and shoe a horse just outside Dover.
Saying this because I'm about to say some things probably makes people in Tennessee mad.
I just want to clear, I'm not a person unfamiliar with the state.
I went on the same yearly field trip to the Hermitage that you did."
So the state legislature booted three of two people that they had targeted, three of two
people that they had decided to make an example of to send a message with.
What were those messages?
The first is obvious.
You better not get too uppity.
You better stay in your place.
That's hard to miss.
That is hard to miss.
The two black men got booted, the white woman, wow, she got spared, right?
And even she says that she thinks her skin tone had something to do with it.
There's that message.
legislature sent that message loud and clear to a large chunk of Tennessee.
But here's the thing, the racism, it's real, it's thick.
Not surprising, it is disappointing.
But that stay in your place part, that was meant for you too.
That was meant for everybody in Tennessee.
What did they really do today?
What did they really do?
I know they're trying to frame it as them standing up for the Second Amendment and all
of that, but what did they actually do?
They sent out the message that you don't matter, that you don't deserve a voice.
You don't need representation because you have rulers.
You have people who are going to tell you what to do and you better like it.
That's the message.
And that went out to everybody, whether you agree with what they did, whether you agree
with the demonstration, it doesn't matter.
That message is for you.
Your betters will decide.
You sit down.
You be quiet.
You don't deserve representation.
There's no way they can argue this because you're talking tens of thousands, hundreds
of thousands of people in Tennessee no longer have representation.
That was the whole point of what they did today.
They're telling you that it's not the volunteer state anymore.
It's not a state of free thinkers.
It's a state of people who need to sit down, shut up, and do what they're told like obedient
lackeys.
That is their message for you.
That message is for everybody in Tennessee.
They're trying to frame this in a certain way, and they're trying to use that framing,
frankly because they think you're dumb.
They think you can't count.
What two words are in almost every headline and in almost every soundbite about why this
is happening?
Gun control, right?
The reason they're doing that is because gun control isn't really popular in Tennessee.
It's something that they can use to tie these representatives to, these legislators to.
They can tie them to that, to that idea of gun control.
And then you're like, well, they're not on my side, don't worry about them.
That's exactly what they want you to think.
But again, they're only using this framing because they think you can't count.
It's not a joke.
They think you're that uneducated.
They think you're a hick.
Let's be real for a second.
The Democratic Party, they don't have the votes to stop one of their own members from
getting expelled.
You think they got the juice to push through gun control legislation in Tennessee?
They don't have the votes.
But the state legislature, they know that's something that will keep you preoccupied.
They can use that to scare you.
And scared people?
Well, they're obedient.
They do what they're told.
That's the goal.
You don't need a voice.
You don't need representation.
You need to obey.
That's their goal.
That's their message for you.
Don't forget it.
That's the whole point.
You better stay in your place, do what you're told, listen to your betters, and just accept
your fate because you don't get a voice.
They want to frame it about the Second Amendment, but it's not.
It's about the First Amendment.
And then they're trying to use the facade of decorum, decorum.
It's the Tennessee legislature.
Please take a look at some of the scandals up there and tell me you think decorum is
something that exists in that house.
It's not about that.
It's about control.
They're trading on your loyalty.
They're trading on you assuming they're like Republicans of the past.
They're not.
It's about control.
It's about making you obey and sending the message that you will obey the party.
Your loyalty is to the party.
Not to the country, not to the republic, not to the constitution, not to the idea of representative
democracy.
Nothing like that.
Just do what you're told.
Another group that got a message today was young people in Tennessee.
They got their own message.
For a lot of them, that was their first real involvement in politics.
And what were they told?
They don't matter.
Their concerns, their voice, we're not going to talk about that.
Sit down, shut up, and do what you're told.
Make no mistake about it, they're treating the entire state of Tennessee the way they're
treating the young people.
They're just doing it a little bit differently because they know if they can divide it, well,
people who are divided, they're easier to control.
That's what it's about, don't forget that.
Doesn't matter where you stand on these issues.
They're telling you, you will obey the party, period.
So what can you do?
They sent you a message, you should send them one back.
Here's the thing, yeah, they can bounce these people, they can boot them out, and you can
put them right back in there. You can. If they choose, if those men choose, they
could run again and you could put them back in there. If you want to send a
message that's what you need to do. You need to get out there. You need to vote.
Even if you don't even like these people, even if you disagree with everything
that they stand for, you need to put them back in office. You need to use the power
that ballot box and put them right back there, put them back in their seats.
Because the message that was sent, it doesn't stop.
They will become more and more controlling, more and more authoritarian, and the only
real message you can send back is that you're not going to listen to it.
You're going to vote these people right back in.
The Republican Party in Tennessee succeeded in creating tens of thousands of lifelong
Democrats when they did this.
There's going to be a shift in Tennessee politics.
It is now inevitable.
Your part in that, if you want to be on the right side of history, is to make sure that
those men who stood up for their constituents, who gave them the voice that the state legislature
wants to take away from tens of thousands or hundreds of thousands of Tennesseeans,
you put them right back in office.
If they're willing to serve again.
The Tennessee State Legislature showed its hand.
They're not representatives, they're rulers.
You don't get a voice.
you think doesn't matter, you're going to do what you're told.
Whether you accept that or not is up to you.
You have an option.
If you put them back in office, it's going to send the message that it's unacceptable.
Would be nice to tie that to not voting for the people who are trying to rule over you.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}