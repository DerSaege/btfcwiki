---
title: Let's talk about Trump's mouth, his supporters, and a judge....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=U0dHReNyLTs) |
| Published | 2023/04/07 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the developing situation in New York with a judge and Trump's involvement.
- Details how the judge politely warned Trump not to stir the pot during the arraignment.
- Mentions Trump's negative comments about the judge and the threats the judge received from Trump supporters.
- Outlines the judge's options, which include a stern talking to, a gag order, or even putting Trump in a cell if his behavior doesn't change.
- Emphasizes that it's not Trump's direct actions but the reactions of his supporters that may land him in trouble.
- Points out that the timeline of potential consequences depends on how extreme Trump's statements become.

### Quotes

- "It's not Trump that will end up creating the situation necessarily, because Trump could get out there and be pretty mellow and just say normal Trumpy things, it's the reaction from his supporters and what his supporters say and do that will end up getting Trump in trouble."
- "The judge has more power here than I think people realize."

### Oneliner

Beau explains the situation between a judge, Trump, and Trump supporters, warning that it's the supporters' actions that could land Trump in trouble.

### Audience

Legal observers, political analysts

### On-the-ground actions from transcript

- Contact legal experts for insights on the judge's potential actions (suggested)
- Stay informed on the developments in New York's legal situation (implied)

### Whats missing in summary

Insight into the specific statements made by Trump that led to the judge's warnings and threats received by the judge.

### Tags

#LegalSystem #Trump #Supporters #Judge #Threats


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about the situation
that is developing in New York with the judge up there.
And we'll talk about Trump and his mouth
and what Trump was told by the judge.
And we'll talk about Trump supporters and their mouths
and how Trump supporters' mouths may end up getting Trump
somewhere he doesn't want to be.
OK, so during the arraignment, during that whole process
up there, the judge politely, very politely,
told Trump, basically, don't stir the pot.
Did it in a relatively informal manner.
It was pretty cool about the whole thing.
Trump didn't really heed that, didn't go as far as I thought he would.
But he did, you know, say some less than favorable things about the judge and the judge's family
and so on and so forth.
The judge has received dozens of threats from Trump supporters.
This has led a whole lot of people to ask, well, what happened to now?
It's up to the judge.
The judge could look at this and be like, you know, all of this is kind of unsubstantiated,
unfounded stuff.
No big deal.
Or the judge could take it to the next level.
The judge actually has the power to bring Trump back up there and talk to him, put a
gag order on him, for real.
And eventually, if Trump does not stop, the judge has the power to basically put Trump
in a wait room, meaning go in that cell and wait.
This is one of those things that I'm not sure that Trump's lawyers have done a good job
under of explaining to him. I don't know that he understands this. There's a
process to it. It doesn't just jump from, you know, Trump saying one bad thing to
him sitting in a cell. But at the end of the chain of events, if it doesn't stop,
that's where Trump goes. It would probably be calling him up there having
a stern talking to one time, then putting a gag order on him, then maybe a
second chance on that, and then, well, okay, you obviously don't understand.
So I know one place where you can't cause a problem.
That's within the judge's power.
How long that process takes, if it goes that far, depends on how wild Trump's statements
get.
I mean, this is something that theoretically could happen in like two weeks if Trump really
got out of line, or it could drag on for a while.
But the unique part about this is that it's not Trump, really.
It's not Trump that will end up creating the situation necessarily, because Trump could
get out there and be pretty mellow and just say normal Trumpy things, it's the
reaction from his supporters and what his supporters say and do that will end
up getting Trump in trouble. You know, it's a unique situation because for a
long time it was Trump saying things, riling people up, and them getting in
trouble. Now, if he riles them up, it's gonna be him that ends up somewhere he
doesn't want to be. If that happens, I don't know, but for those who were
wondering what options the judge has, the judge has more power here than I
I think people realize.
Anyway, it's just a thought.
I have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}