---
title: Let's talk about the report on leaving over there....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=XoI6omJGp1M) |
| Published | 2023/04/07 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing the report and summary of the Afghanistan withdrawal to ensure it doesn't happen again.
- Criticizing the attention given to critical issues in the withdrawal process.
- Emphasizing the mistake of establishing a timeline for withdrawal publicly.
- Pointing out the flaw in intelligence estimates due to groupthink.
- Holding the Biden administration accountable for not addressing potential outcomes.
- Stressing the need to focus on preventing unnecessary suffering in future events.
- Mentioning that Trump's foreign policy decisions heavily impacted the Afghanistan situation.
- Noting that blame for the situation in Afghanistan extends beyond Trump to previous administrations.
- Suggesting that strategies employed, rather than individuals on the ground, contributed to strengthening the opposition.
- Urging focus on preventing similar events rather than playing politics.

### Quotes

- "Nobody gets to complain, nobody gets to whine if their team shoulders blame on this."
- "Heads of state and negotiators cannot tell the opposition that they want to leave and establish a timeline."
- "The estimates are supposed to be prepared objectively, not averaged out among everybody."
- "You shouldn't be worrying about politics at this point. You should be worrying about making sure this doesn't happen again."
- "But I'm sure because the Republican Party wants to like two hearings on this, this report will come up again."

### Oneliner

Beau provides a critical analysis of the Afghanistan withdrawal report, focusing on accountability, intelligence failures, and the need to prevent similar events in the future.

### Audience

Policymakers, analysts, activists.

### On-the-ground actions from transcript

- Contact policymakers to ensure accountability in future decision-making (implied).
- Organize community forums to raise awareness about the implications of intelligence failures (implied).

### Whats missing in summary

Detailed examination of the impact of strategies employed over time on the situation in Afghanistan.

### Tags

#Afghanistan #Withdrawal #IntelligenceFailures #Prevention #Accountability


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we're going to talk about the hot wash.
We're going to talk about the report and summary
about the withdrawal.
And whether or not it was fair, accurate,
if I feel like there are things that are missing,
as soon as it came out, people started asking.
So we'll go through it, provide a brief overview
what I think is missing. Now the first thing to understand is that this is not
something designed to hold anybody accountable. This is something to make
sure it doesn't happen again. So you have to look at it through that lens. Nobody
gets to complain, nobody gets to whine if their team shoulders blame on this.
That's not what this is about. This is about making sure we don't have another
chaotic withdrawal like that in the future. And for those who have no idea
what I'm talking about, there was a report released detailing the chain of
events that led up to the withdrawal in Afghanistan. And they're trying to figure
out why it didn't go perfectly. Okay, so I haven't read the full report yet,
obviously, read the summary and went through it. Yeah, it seems fair. It seems
fair. There are two things that I take exception to, not because they're wrong
but because they did not get the attention that they deserve. And in, from
my point of view, they're the two biggest problems. If these two things didn't go
the wrong way, it would have looked entirely different. Okay, so the first
thing that isn't really hammered home the way it should have been was that heads
of state and negotiators cannot tell the opposition that they want to leave and
establish a timeline. That cannot happen. If you do that, you get what happened in
Afghanistan. This isn't Monday morning quarterbacking. Go back and watch the
videos from when Trump announced his deal. It was called then. As soon as he did
what he did and said, well we want to be out by this time, like it's over.
Afghanistan is gonna fall. And I wasn't the only person who called that. You
You can't do that.
If you do that, you establish a situation where the U.S. has the clocks counting down
the time, but the opposition actually has the time.
They know that as part of the deal, they're not going to be engaging the U.S., so they
can redistribute their resources to move to take the country as soon as the U.S. is gone,
is exactly what happened.
This is basic stuff and maybe that's the reason it wasn't hammered in is because they don't
think any other future head of state would make the same mistake.
I don't have that level of faith.
This event set everything else in motion.
If Trump had just treated it like foreign policy instead of a real estate deal, none
of this would have happened.
The other thing that I don't really like is that in the summary, and this could be
detailed greater in the report, but in the summary, it makes it seem like the Biden administration
is totally absolved because they went off of the estimates provided by the
intelligence community in the military. Okay, I get it from the standpoint of
this is what you were told, but the reality is you had dozens of people
saying that wasn't how it was going to go. When it comes to these assessments,
When it comes to estimates like this, what happens a lot of times, and I'm certain when
it's all said and done, it will turn out that this is what happened here.
You get groupthink.
You have a bunch of people developing their own estimates and then they talk to each other.
Nobody wants to be an outlier, so they all adjust their estimates to kind of at least
be close to each other.
That's bad.
That's what led us into a rock.
It's bad.
It's all bad.
The estimates are supposed to be prepared objectively, not averaged out among everybody.
If there was an outlier saying, hey, because they knew we were leaving, that they probably
got everything together and have been stationed for a while just waiting for the right moment,
it could have changed the outcome.
Now is that really a problem with the Biden administration?
that part. That is a problem within the intelligence community and the military.
However, the Biden administration, they should have known. Yeah, yes, I am holding
them to a higher standard than the Trump administration, absolutely. But the Biden
administration actually has a role for a policy team. I feel like they should have
seen that coming. Would that have changed things? It would have changed the end
outcome, but they might have been able to mitigate more. They might have been able
to get put some more urgency, but behind it. Those are the two things that were
listed off in the summary. The summary is only, I don't know, 15 pages or so, that I
don't think got the attention that it should have. And it, if you're trying to
stop it from happening again, those are the two things that need to be addressed the most.
Because one put the whole chain of events in motion.
The other was an opportunity because of a change of administration to re-evaluate and
mitigate and yeah, even if the Biden administration had done that perfectly, Afghanistan was still
going to fall.
It was still going to go to the opposition, it would still be very much the same scenario
that's going on now.
But it would have been a little bit better.
And if you're trying to stop unnecessary suffering, which is what the goal of hot washes
like this should be, that needs to be considered.
So the report does put a lot of focus on how bad Trump's foreign policy was.
And yeah, it was bad.
It's a pretty fair read.
But the part to me that mattered, the part that let everybody know what was going to
to happen to Afghanistan, it really didn't get included.
Saying that the Trump administration, their decisions put a bunch of constraints on the
Biden administration.
Yeah, sure, that's true, it is, but I think it needs to be more direct than that.
If Trump had treated it like foreign policy instead of a real estate deal, it would have
looked entirely different.
In fact, if he hadn't have done that, there's a slim, very slim chance that the national
government could have held on.
And what's happening there wouldn't be happening.
But to be clear, before anybody starts, you know, laying everything that's occurring
there at the feet of Trump, it's a very slim chance that they would have been able
to hold on anyway. The real faults for this go all the way back to Bush. They
really do. There were a lot of decisions that were made along the way as far as
the use of unmanned aircraft, as far as the strategies employed, that just
fed the opposition, that strengthened the opposition. It wasn't the people on the
ground, really. It was the strategies that were employed. And that that's not
mentioned in the report either. It's a little outside the scope of it, but I
feel like maybe there should be some background on that. But I'm sure because
the Republican Party wants to like two hearings on this, this report will come
up again. The summary in particular will come up again because they are not going
to like what it says because most of the blame falls on Trump. But newsflash, in
real life, most of the blame falls on Trump. But you shouldn't be worrying
about politics at this point. You should be worrying about making sure this
doesn't happen again. Anyway, it's just a thought. You'll have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}