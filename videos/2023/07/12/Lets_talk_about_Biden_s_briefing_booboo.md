---
title: Let's talk about Biden's briefing booboo....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=8EJA5qmrwUk) |
| Published | 2023/07/12 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Biden's use of profanity and shouting has become a topic of interest, particularly among right-wing media.
- Fox News claims Biden's profanity undermines his "well curated image," despite instances of him using profanity publicly in the past.
- Biden's go-to phrases when frustrated with his staffers include "how the heck did you not know that, man?" and "don't hecking BS me."
- Beau defends Biden's demanding approach to accurate information, suggesting it's a positive change from the previous administration's penchant for inaccurate information.
- Biden prefers briefings in plain, everyday language, and he requests translations of technical jargon into understandable terms.
- Beau humorously points out the absence of reported ketchup incidents in the White House amidst all the focus on Biden's use of profanity.

### Quotes

- "How the heck did you not know that, man?"
- "Don't hecking BS me."
- "Welcome to a White House that expresses urgency about things other than golf tee times, taking documents and cooing."
- "I personally think this is a nice change."
- "Y'all have a good heckin' day."

### Oneliner

President Biden's use of profanity sparks controversy, but demanding accurate information and translating technical jargon show a refreshing approach in the White House.

### Audience

Media consumers

### On-the-ground actions from transcript

- Translate technical jargon into everyday language (exemplified)

### What's missing in summary

The full transcript provides a detailed and humorous analysis of the controversy surrounding President Biden's use of profanity, showcasing a contrasting approach to demanding accurate information in the White House.

### Tags

#PresidentBiden #Profanity #Accuracy #Translation #WhiteHouse


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about some very important breaking news involving
President Joe Biden and his language.
This is going to be very groundbreaking.
It's very important.
I read about it over on Fox, so you know that it's something that is truly
valuable to the national discussion and not something that, you know, that they've just latched onto.
Coming fresh off the hills of his major scandal involving wearing no-show socks,
it has been discovered that President Biden shouts and uses profanity. I know
you're shocked. I'm shocked. Right-wing media is kind of having fun with this
right now. They're saying that it's very important because it undermines Biden's
quote, well curated image. Well curated. That was a term used by Fox. Before we go
any further, I would like to object to the idea that Biden has a well curated
image. He is a walking hot Mike. Apparently Fox forgot about the time
Biden called one of their reporters a stupid son of a gun on camera. This isn't
a surprise. You're talking about a man who has dropped the F-bomb in public on
camera multiple occasions. The idea that he cusses in the White House is not
really that shocking. But it does get entertaining when you start looking at
the actual quotes in his quote, go to quotes, yeah, tell me you've never given a briefing
without telling me you've never given a briefing.
So apparently the two phrases Biden shouts at his staffers the most are how the heck,
he did not say heck, how the heck did you not know that man?
And don't hecking BS me.
Yeah.
I can see how the people who have flags out in front of their house that say heck Biden,
I can see why they are clutching their pearls over this.
So let me tell you without even going into it, let me tell you how this happens.
You are a policy advisor.
You are providing information to the policy maker.
The policy maker asks you a question and you don't know the answer.
Let me tell you the next words you're going to hear.
How the heck do you not know that man?
Your entire job is to hecking know that man.
Don't hecking BS me.
Get the heck out of my office until you can come back with a hecking answer and it had
better be in the next three hecking minutes.
And then once you leave, that continues because the person's like, heck, can you believe this
hecking stuff?
How can I do my hecking job if I don't have the information?
That's how it plays out.
Welcome to a White House that expresses urgency about things other than golf tee times, taking
documents and cooing. In most White
houses this is any given Monday. This is
what high-pressure briefings are like
anywhere from the White House to any
other briefing room where the outcome
really matters. I would suggest that a
policymaker demanding accurate information isn't actually a bad thing.
I would suggest that's a good thing. Now, sure, there's probably a nicer way to
ask for it, but from experience that's generally not what happens. And again,
Again, given the last administration's apparent consistent demands for inaccurate information,
I mean, I personally think this is a nice change.
And then when you dig further into it, what you also find out is that Biden likes to get
his briefings in normal language.
And he asks questions to translate GovSpeak, Milspeak, Palspeak into normal everyday language.
If this is becoming a problem, it might be a good idea to have somebody in the room who
can translate the the policy speak
into normal, everyday
language. Some people have actually built
kind of a career out of doing that. I'm sure that there are a bunch that would be
available.
Now, I combed through some of the reporting.
One thing that I was not able to find
was ketchup on the wall.
Anyway,
it's just a thought.
Y'all have a good heckin' day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}