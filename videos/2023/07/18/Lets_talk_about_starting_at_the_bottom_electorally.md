---
title: Let's talk about starting at the bottom electorally....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=1S66zqiiJsI) |
| Published | 2023/07/18 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing electoral politics and the need for work to achieve change.
- Starting at the bottom is key to creating real progress in the political system.
- Progressives moving up through the Democratic Party can lead to a more progressive establishment.
- Emphasizing the importance of starting at the bottom rather than jumping straight to the top.
- The power lies in Congress, making Senate and House positions vital for progressive change.
- Advocating for a strategic approach to implementing change through political careers at local and state levels.
- Acknowledging the need for reforms in the US electoral system.
- Stating that building one's power structure is necessary to combat existing political machines.
- Explaining the significance of building a power structure outside of traditional political parties.
- Stressing the importance of more than just voting for a third-party candidate to bring about systemic change.

### Quotes

- "Starting at the bottom is key to creating real progress in the political system."
- "You have to build your own power structure."
- "A dream without a plan, it's never going to happen."
- "Building a power structure outside of a political party [...] That's the way forward."
- "It requires a lot more work."

### Oneliner

Beau explains the importance of starting at the bottom in electoral politics to create real change and build a power structure outside traditional parties.

### Audience

Activists, Political Organizers

### On-the-ground actions from transcript

- Join organizations working towards systemic change (implied)
- Build a power structure outside traditional political parties (implied)

### Whats missing in summary

Beau's detailed insights on the necessity of starting at the bottom in politics are best experienced in the full transcript.

### Tags

#ElectoralPolitics #ProgressiveChange #PowerStructure #SystemicChange #PoliticalActivism


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we're going to talk a little bit more
about electoral politics, additional parties,
and how to get there, because after that last video,
there were a whole bunch of people saying,
well, this just sounds hopeless.
No, it's not hopeless.
That was a long video.
I felt like there might be people
that didn't get to the end,
But how to accomplish that is in the video.
It's not hopeless.
It's just not something that happens without work.
You have to set the goal.
If your goal is have a third party candidate president,
that's not a goal.
That's a wish.
You have to have a plan to get there.
the plan starts at the bottom. And when you look at the counter examples that
were brought up, you know, they're like, well, it didn't do any good with the
primary with Bernie. Well, yeah, that's starting at the top. You have to start at
the bottom. If you put progressives at the bottom and they move up through the
Democratic Party, the Democratic establishment, quote, becomes more progressive because that's
who's making it up.
They would be more inclined to support a more progressive presidential candidate.
It isn't really something that can really be argued.
That's how it works.
A lot of people brought up Ross Perot.
He did okay.
he had a really strong showing for a third-party candidate and I mean
relatively close to 20% of the vote. How many electoral votes did he get? When
you look at it through the lens that matters, didn't even come close, starting
at the top. You have to start at the bottom. You have to start at the bottom
And there's a lot of people who, you know, like the House and the Senate, I don't understand how people in a time of
senators like Manchin  can doubt the power of just a couple of very closely aligned senators.
If you had a bunch of very progressive senators, and by a bunch I mean a handful, less than a handful, three, in a
closely divided Senate,  Senate you would be able to move more legislation forward of a progressive
bent than you would ever get to move with a third-party progressive president.
The president is the head of state but in the United States the power is in
the halls of Congress. It is incredibly important and how do people get to the
Senate? Normally through the House of Representatives. So you have to go there
first and most times this is kind of being upended right now because there are
a lot of people who are just jumping straight to the House but normally those
people have a political career at the state or local level first. So that's
I mean that's the route it's not it's not saying that it's hopeless it's
saying that you have to have a plan and just saying well this is what I want it
that's just a wish and then there were a lot of people who brought up reforms in
the US electoral system that are desperately needed 100% correct on all
them. They're things that would definitely help make the United States
more responsive to the citizens of the country, but that means the people in
power lose power, right? So how are you going to get them to vote for it? You
have to put people in who already believe that's what they need to do,
that those policies need to be implemented. How do they get there? From
the bottom, right? It's just the route. It's how US politics works and
trying to ignore that is not a recipe for victory. It doesn't matter how
morally correct you are, how correct in your position you are. It doesn't matter
how much you believe, it doesn't matter how energized you are when you are in
fact facing two very, very strong political machines. They have the power.
You have to build your own power structure. And one thing that I
didn't really consider when I was talking about it was people said, well
you got a president from a third party elected, well it would show that their
positions have a mandate and therefore you know the people in Congress would
would move to support them. I want you to look at the polling for Medicare for All,
universal health care, look at the polling for gun control, look at the
polling for reproductive rights. Those are landslide wins but that's not the
way the legislation is headed because it's not really about the power
structure in that way. They don't look at the polling and say this is what I'm
going to do. They look at the polling that matters in their district at that
time and what they can distract from and what they're getting donations to
support. It isn't there. You have to get the seats of power. They're in
Congress. It's not the White House. I understand that it is more difficult to
to go this route, and it may seem more daunting, but it's what has to happen.
There's no other way. I'm not trying to disrupt your dream, but a dream without a
plan, it's never going to happen. If you look at the successful movements, those
that were truly capable of changing party direction and party platform, they
started at the bottom. It's the only way to do it. You're right. There's a monetary
thing. The people at the top have all the money, but at the bottom you don't need as
much. So it's much easier to move ahead that way. It's the reality of American
politics. Building a power structure outside of a political party that is
geographic to a city or a county, and then using that as an electoral tool
rather than as a part of a party. That's the way forward, that's the way
forward. There are entire playlists on how to build those networks, so I'm not
trying to dissuade you. I'm trying to say if you want to go that route, if you
want to use electoral politics to get that deep systemic change, you have to do
way more than cast a vote for a third-party presidential candidate.
It requires a lot more work and there are organizations in there, there's some
mentioned in the comments under that video. There are some organizations that
are putting in the work to do that. It's not impossible. It just takes time. Anyway,
It's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}