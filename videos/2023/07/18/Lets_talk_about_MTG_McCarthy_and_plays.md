---
title: Let's talk about MTG, McCarthy, and plays....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=At2xtpajUII) |
| Published | 2023/07/18 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Marjorie Taylor Greene's rewards and participation in the conference committee for the NDAA are discussed.
- Greene was expelled from the far-right Freedom Caucus, and McCarthy's reward reduces their influence.
- McCarthy's move to include Greene in the committee is seen as a way to control her and boost his power.
- The shift appears to be part of a broader effort in the Republican Party to sideline the far right and regain control.
- The outcome of these political maneuvers remains to be seen.

### Quotes

- "There's probably a lot of truth to that, but I think there's more to it."
- "It certainly appears like McCarthy is bringing her under his wing and at the same time increasing his own power."
- "It seems like they're trying to get their party back."

### Oneliner

Marjorie Taylor Greene's rewards and role in the NDAA committee signal broader political maneuvers within the Republican Party to sideline the far right and regain control.

### Audience

Political observers

### On-the-ground actions from transcript

- Monitor political developments and shifts within the Republican Party (implied).
- Stay informed about the actions and motivations of political figures (implied).

### Whats missing in summary

Insights on the potential implications of these political shifts within the Republican Party.

### Tags

#MarjorieTaylorGreene #RepublicanParty #PoliticalManeuvers #NDAA #McCarthy


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about rewards
for Marjorie Taylor Greene, and why she's getting them,
and how she might either be a participant or completely
unaware of what's going on.
OK, so let's start with this.
Marjorie Taylor Greene will be on the conference committee that will hash out the final details
when it comes to the NDAA between the Senate version and the House version.
She is not exactly the kind of congressperson you would expect to be in that room.
So how'd she get there?
The simple answer is that, well, she said she wasn't going to vote for it and McCarthy
gave her that as a way to get her to vote for it.
I mean, and that's probably true.
There's probably a lot of truth to that, but I think there's more to it.
Marjorie Taylor Greene was expelled from the Freedom Caucus, that far-right group.
She's no longer part of them.
McCarthy rewarding her for no longer being a part of them and for playing ball with the
Republican establishment further reduces their influence.
Now is it because McCarthy doesn't like the far right or has some kind of moral objection?
No, of course not.
It has to do with them holding up his speaker position.
all those votes welcome to the the revenge portion of the show. Now who wins
in this? Okay obviously Green McCarthy because as she is separated more and
more from her far-right base she becomes more controllable. I have no idea if she
she's even aware of what might be happening and why these moves may be occurring.
But it certainly appears like McCarthy is bringing her under his wing and at the same
time increasing his own power.
So he wins a little bit, she wins a little bit, who loses?
the Freedom Caucus in the United States. Green has no business being in that room when the
two versions get hashed out. It will probably cause issues later. But right now McCarthy
is certainly cutting the influence of the Freedom Caucus. And it's happening in such
a way that I mean sure it could just be it could be an accident it could be all
coincidental but generally speaking in politics you got to work really hard to
make that many coincidences happen so it appears as though there is an active
effort to kind of push the far right into a more irrelevant position, much like it appears there
is a concerted effort among a lot of Republicans to kind of push Trump and those like him into
an irrelevant position. It seems like they're trying to get their party back. Don't know how
successful it's going to be, but it does look like it's underway in multiple, multiple ways.
We'll have to wait and see how it plays out.
Anyway, it's just a thought.
have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}