---
title: Let's talk about Trump's Georgia Supreme Court petition....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=a5aZtuQU8Es) |
| Published | 2023/07/18 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump filed a petition in Georgia to shut down an investigation, aiming to prevent evidence use in future criminal or civil proceedings.
- The petition was dismissed unanimously by all nine Georgia Supreme Court justices, despite eight being Republican appointees.
- The intervention Trump requested required extraordinary circumstances absent in this case.
- The grand jury in Georgia, capable of indicting the former president, has been sworn in.
- Speculation arises about potential new indictments related to a controversial phone call.
- The ruling against Trump's petition is seen as a win for the prosecution, removing a possible roadblock.
- The outcome of this case is expected to gain more attention in the news as time progresses.
- Trump's chances of success in this petition were low, and the ruling further signals challenges for Team Trump in Georgia.
- Despite expectations, the dismissal of Trump's petition is significant in the legal proceedings.

### Quotes

- "Trump filed a petition in Georgia to shut down an investigation."
- "The petition was dismissed unanimously by all nine Georgia Supreme Court justices."
- "The ruling against Trump's petition is seen as a win for the prosecution."

### Oneliner

Trump's failed petition in Georgia to halt an investigation carries implications for future legal proceedings and potential indictments, despite unanimous dismissal by the Supreme Court.

### Audience

Legal observers, political analysts

### On-the-ground actions from transcript

- Monitor news updates on the legal proceedings in Georgia (suggested)
- Stay informed about the developments surrounding the former president's case (suggested)

### Whats missing in summary

The detailed legal intricacies and potential implications of the dismissed petition can be better understood by watching the full transcript.

### Tags

#Trump #Georgia #LegalProceedings #Indictment #Justice


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump and Georgia,
and we're going to go over something that has happened
that we really haven't been covering much on the channel
because the outcome that has occurred
was pretty much the predestined outcome from the beginning,
but it's worth bringing up now
because it's certainly going to come up later.
Okay, so in Georgia, in the state case in Georgia, Trump filed a petition that in effect
shut down the investigation.
It was designed to toss evidence and the DA and make it to where that evidence could never
be used again in the future in a criminal or civil proceeding.
the DA from any further involvement in any of it. It was giant. It was a
giant request. Now, Trump probably thought that there was a chance of this going
somewhere because eight of the nine justices on the Georgia Supreme Court
were appointed by Republicans. Nine of the nine justices decided unanimously to
dismiss his petition. There was, there wasn't grounds for this. I mean
realistically it just wasn't there in any way. The type of intervention that he
was asking for is something that requires just absolutely extraordinary
circumstances that are not present in this case.
So it has been, the petition has been dismissed, Trump did not get his way, the Republican
judges in Georgia did not behave in a partisan manner.
All of this was pretty much expected.
Realistically, this didn't have a chance of moving forward.
So it wasn't at the top of the list to cover.
Now that the grand jury that could theoretically indict the former president in Georgia has
been sworn in, and this has been ruled on and shot down, it's probably going to become
a little bit more pressing.
This case will show up more and more in the news as we move closer and closer to what
many people expect will be yet another indictment for the former president.
This one is primarily focused on the perfect phone call asking for all the votes to find
the votes.
At the same time, there were a lot of signs that they were looking into things beyond
that that maybe we just don't have information on.
And we'll have to wait and find out what is determined about that when and if an indictment
comes down.
But that's where we're at.
ruling. I'm sure that many places are going to treat it as like a huge win for the prosecution.
It is in the sense that if it had gone the other way it really could have just destroyed their case,
but there wasn't a high likelihood of Trump really succeeding here. I mean there wasn't a
low likelihood of him succeeding here to be honest. So that is now out of the way.
It removes one more roadblock and yeah it's just another bad sign for Team
Trump in Georgia.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}