---
title: Let's talk about two minutes on Fox....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=mLTEcND18Sw) |
| Published | 2023/07/18 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Jesse Waters takes over Tucker Carlson's time slot on Fox News, and on his first show, his mother calls in live.
- Initially, the call starts with a proud mother congratulating her son, but it takes a surprising turn.
- Jesse's mother advises him to avoid conspiracy theories, do no harm, be kind, use his voice responsibly, stop Biden-bashing, and let Trump go back to TV.
- The advice seems like a subtle dig at other Fox News hosts.
- Jesse's mother's unexpected and candid advice makes the moment glorious and entertaining.
- Despite the unexpected turn, Jesse tries to maintain his composure during the call.
- Beau finds the interaction humorous and chuckles at the exchange.
- Beau questions how much foreknowledge Fox had about the topics discussed during the call.
- The call may indicate a shift in tone for the time slot, potentially signaling a change in Fox's approach.
- Beau hints at the possibility of Fox orchestrating the call to set a specific tone for the show.

### Quotes

- "How much foreknowledge did Fox have about the topics being discussed?"
- "Jesse's mother advises him to avoid conspiracy theories, do no harm, be kind, use his voice responsibly, stop Biden-bashing, and let Trump go back to TV."

### Oneliner

Jesse Waters' mother's candid advice on Fox News sets a surprising tone, urging kindness and steering away from conspiracy theories and political bashing.

### Audience

Media consumers

### On-the-ground actions from transcript
- Watch the clip of Jesse Waters' mother's call (implied)

### Whats missing in summary

The full transcript captures a lighthearted yet thought-provoking moment on Fox News, showcasing a mother's candid advice to her son amidst a potentially changing tone on the network.

### Tags

#FoxNews #JesseWaters #Media #CandidAdvice #ConspiracyTheories


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about the most amazing two
minutes to ever occur on Fox News.
OK, so if you somehow missed it, Tucker,
you know, no longer with the network.
Jesse Waters is taking over his slot, his time slot, and on his first show his
mother called, his mother called in, and he took the call live and it started out
the way you would expect. A proud mother congratulating her son and everything
you know that you would picture as you know some kind of feel-good thing that
they were gonna put on a on a show to kind of open it up. And then she
explained how he could best keep his job by not you know going down conspiracy
rabbit holes and she said that he he shouldn't go down conspiracy rabbit
because they don't want to lose him, and they don't want any lawsuits.
An obvious shot at other Fox News hosts.
She then went on to say that he needed to do no harm and be kind, which, I mean, that's great advice.
Said that he needed to use his voice responsibly, and then said to stop the Biden-bashing
bashing and forget about the laptop and stop worrying about other people's
bodies. It was glorious. She closed by advising him to tell Trump to give up on
politics and go back to TV. I want to see a show with Jesse's mom. That's
what I would like to see. I would watch that. It was very entertaining. The host
did his best to not appear bothered, although he did seem to mumble at one
point that I knew this was a bad idea. I will try to find a clip and put it down
below because it was it was pretty humorous. I got a big chuckle out of it.
Now here's the the question we have to ask. How much foreknowledge did
Fox have about the topics being discussed? This is a time slot that has
gotten them in a lot of trouble and they may be trying to shake it up and this
This may be them trying to set the tone.
If she was improvising what she said, that's not the case, but if this was a little bit
more structured, this might be a signal from Fox.
We have to wait and see, but yeah, I'll try to find a clip of it.
Anyway, it's just a thought.
have a good day and be kind.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}