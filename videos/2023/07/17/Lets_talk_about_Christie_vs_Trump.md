---
title: Let's talk about Christie vs Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=IEnm3vi7H2k) |
| Published | 2023/07/17 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau delves into the unfolding rivalry between Chris Christie and Trump, hinting at potential consequences.
- Christie appears to be running in the Republican primary not to win but to make Trump lose or damage him.
- Recent actions by Christie include critiquing Trump's foreign policy record, which many Republicans mistakenly view positively.
- Christie called out Trump's conduct related to classified national secrets, contrasting Trump's portrayal of himself as a savior.
- The impact of Christie's criticisms is expected to resonate more on the debate stage than in early sparring through news soundbites.
- Beau notes that Trump's appeal lies in his personality of owning the libs rather than policy, principle, or party allegiance.
- Christie's strategy of attacking Trump recklessly could potentially benefit him or at least harm Trump during the debate.
- Christie has met the donor threshold to join the debate stage with Trump on August 23rd.
- Trump's reluctance to face Christie on stage may stem from the fear of being embarrassed, as Christie's aggressive approach could undermine Trump's facade.
- There's a possibility that Christie, despite not aiming to win, might inadvertently succeed if he damages Trump significantly and attracts vindictive voters.

### Quotes

- "They don't care about policy. They really don't. They don't care about policy. They don't care about principle. They don't care about party."
- "Trump is doing so well because he can own the libs. He can embarrass people."
- "Christie, whether or not he does when the time comes, he certainly has the potential to embarrass Trump and hurt that facade that he has put up."
- "The more I think about it, the more I think it might be possible, but it'll be a complete accident."
- "It depends on how hard Christie swings at Trump."

### Oneliner

Beau analyzes the brewing conflict between Chris Christie and Trump, with Christie seemingly aiming to damage Trump's image rather than win, potentially impacting the Republican primary dynamics.

### Audience

Political observers

### On-the-ground actions from transcript

- Watch the debate between Chris Christie and Trump on August 23rd (implied)
- Stay informed about the evolving dynamics in the Republican primary (implied)

### Whats missing in summary

Insights on the potential ripple effects of Christie's actions beyond the primary race.

### Tags

#ChrisChristie #DonaldTrump #RepublicanPrimary #PoliticalAnalysis #Debate #Election2022


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about Chris Christie versus Trump, because that is something
that is shaping up and it may end up turning into something that really matters.
And I have a feeling come August, it's going to get pretty entertaining.
So for those who don't know what's going on, Chris Christie is running in the Republican
primary for president.
It certainly appears to me that he has absolutely no intention of winning.
His goal appears to be just make Trump lose or just damage Trump.
He's gone about this in a number of ways.
Recently, he went after Trump's foreign policy record.
And this is important because a lot of Republicans falsely believe that he has a good foreign
policy record.
He doesn't.
Chris Christie called it out and now I still think Trump reaching out to North
Korea was actually a good move. He didn't do it right, but I'm convinced the
theory is sound. Other than that, Christie's critiques, they were pretty
spot-on and the Republican audience is now hearing these critiques for the
first time from Republicans. In addition to that, recently Trump said, I'm being
indicted for you, talking to the crowd. Basically, again, trying to cast himself
as some kind of savior, build that following of people who view him as
almost godlike. Christie responded with, he's indicted because of his outrageous
conduct. There's no other of the 200 million Americans he spoke about who
illegally retained classified national secrets after being asked politely,
quietly, and professionally for 18 months to voluntarily turn them back over after
he left the White House. I mean, it's hard to argue with that. These hits, they're not
going to matter a whole lot in this phase of things, in the early political sparring
via news soundbites. That's not where it's going to come into play. Where it's going
to come into play is on that debate stage because Republicans, particularly
those who have been drawn to Trump, they don't care about policy. They really
don't. They don't care about policy. They don't care about principle. They don't
care about party. Generally, they care about personality and why do they like
that personality? Because he can own the libs. He can embarrass people. That's why
you know, Trump is doing so well. If Christie gets up there and moves with the
absolutely reckless abandon that he has been in going after Trump, he's gonna
come out looking really good. At bare minimum, he's going to hurt Trump and
Christie knows this. He has repeatedly pointed out that he has the 45,000
donors. So he's met the threshold to be on the debate stage with Trump and he
definitely appears to intend on showing up. That will be August 23rd. It's worth
noting that I'm not afraid of anybody Trump has suddenly kind of teased the
idea that he's gonna skip it. Yeah, I mean I would too because Christie's gonna
embarrass them. That's the part that Trump had going for him in previous
primary. He was the one that wasn't playing into civility politics. He was
the one that wasn't breaking that 11th commandment of never speak ill of
another Republican, and he was the only one doing it. With Christie coming out
swinging, it's gonna hurt Trump. Trump knows that. And to be honest, I have a
feeling that Trump may not be up to the task of trying to handle Christie on
stage. There's a pretty high likelihood that Trump, I mean for lack of a better
word, chickens out because he doesn't want to be embarrassed. And Christie,
whether or not he does when the time comes, he certainly has the potential to
embarrass Trump and hurt that facade that he has put up. And I was asked if I
thought that there was a possibility of Christie actually going somewhere, like
actually making it through the primaries and winning. The more I think about it,
the more I think it might be possible, but it'll be a complete accident. Again,
I don't think Christie's running to win. I think that he is running to make sure
Trump loses. However, if he damages Trump enough on the debate stage, he may swing
the vindictive voters, those people who Trump gave permission to to be their
worst, they may actually end up supporting Christie. It depends on how
How hard Christie swings at Trump.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}