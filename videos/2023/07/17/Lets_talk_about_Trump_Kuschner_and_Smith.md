---
title: Let's talk about Trump, Kuschner, and Smith....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=T6M_sB6W-fk) |
| Published | 2023/07/17 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring Kushner and Trump's connection in relation to election results.
- Kushner reportedly testified that Trump genuinely believed he won the election.
- Despite this testimony, there are contradicting statements from others like the comms director, Griffin, and Milley.
- Bannon was recorded discussing similar sentiments along with other evidence.
- The case against Trump won't likely crumble due to conflicting testimonies.
- Prior to the election, seeds were planted to support claims of election interference.
- Special counsel's office is focusing on obstruction and willful retention in addition to documents.
- Defending Trump on matters where there is ample contradictory evidence may not be effective.
- Multiple individuals are actively cooperating regarding election interference.
- Smith, the special counsel, is likely prepared for testimonies claiming Trump's ignorance.

### Quotes

- "It's worth remembering he was planting the seeds before the election."
- "There are clear indications that there are multiple people who are actively cooperating."
- "Smith, the special counsel, is pretty far ahead of where legal analysts even think he is."

### Oneliner

Exploring Kushner's testimony on Trump's belief in election win amidst contradicting statements, the case against Trump remains strong with active cooperation from multiple individuals.

### Audience

Legal analysts

### On-the-ground actions from transcript

- Stay informed and updated on the ongoing legal proceedings surrounding Trump and Kushner (implied)
- Support transparency and accountability in legal investigations (implied)

### Whats missing in summary

In-depth analysis of legal implications and potential outcomes beyond just testimonies and conflicting statements.

### Tags

#Kushner #Trump #ElectionInterference #LegalProceedings #Cooperation #Obstruction


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Kushner and Trump and whether or not the
reporting about what Kushner said is somehow something that is going to make
all of this go away for the former president.
Um, so reporting suggests that Kushner, when testifying before the grand jury that
He said that Trump truly believed that he won the election.
I mean, okay, I get it.
Smith, the special counsel's office, would likely want to show that Trump knew that he had lost.
Kushner saying that he really believed that the election had been taken from him, I mean,
that's all fine and good, but it's the son-in-law.
It's worth remembering that the comms director, I think Griffin said otherwise, Milley said
otherwise.
I think there's a recording of Bannon saying that he was going to do it as well as a whole
bunch of other evidence.
It's not quite something that undermines the whole case to have one witness say that.
And it's worth remembering he was planting the seeds before the election.
So I don't think that that's going to be insurmountable.
It's also worth remembering that the special counsel's office, in the documents case, yeah,
willful retention, all of that, but also obstruction, I don't think that those who are going to
take a route of trying to defend the former president, particularly about
things that there's a bunch of counter testimony to. I don't think
that they're going to just walk away from it.
There's a whole lot of evidence as far as the election interference stuff and
there are clear indications that there are multiple people who are actively
cooperating and I'm not sure that it's a great idea to
to jump to Trump's defense.
I don't think that this is something that is going to undermine Smith's case.
I think it, I think on some level, he was probably prepared for that.
He probably had a list of people that he expected to say that Trump knew nothing.
And he probably had that list prepared before they ever talked to the grand jury.
If there is one thing that we have learned about Smith thus far is he's pretty far ahead of where
Where legal analysts even think he is at any given time and he's certainly thinking
further ahead than Trump.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}