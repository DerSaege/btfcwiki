---
title: Let's talk about Putin's signals and plans....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=jpguIX6Xx4A) |
| Published | 2023/07/16 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Speculation surrounds Putin's plans following recent events involving Wagner.
- Putin's treatment of individuals hints at a possible political purge within military structures.
- Traditional Russian officers are being questioned and relieved of command, signaling a significant realignment.
- Loyalty seems to outweigh skill in the selection of replacements, causing concern for Russian troops.
- Wagner PMC, according to Putin, does not legally exist in Russia.
- Putin's actions suggest attempts to maintain control over Wagner while potentially reorganizing its structure.
- Putin's maneuvers indicate a desire to keep Wagner intact for his own viability.
- Putin is dealing with these challenges tactfully but may be driven by fear of potential threats to his power.
- The uncertainty surrounding Putin's actions stems from his perceived vulnerability and fear of being deposed.

### Quotes

- "Loyalty, not skill."
- "You do realize I can outlaw your company at any moment."
- "He's scared, which makes him a little bit more unpredictable."
- "He's scared he's going to be deposed."
- "It's just a thought."

### Oneliner

Speculation abounds on Putin's plans amid military restructuring, hinting at loyalty shifts and fear of upheaval.

### Audience

Political analysts

### On-the-ground actions from transcript

- Monitor developments in Russian military structures for potential shifts and implications (implied).
- Stay informed about Putin's actions and political maneuvers within the military (implied).
  
### Whats missing in summary

Insights on the potential impact of Putin's actions on Russian military dynamics and geopolitical stability.

### Tags

#Putin #Wagner #RussianMilitary #PoliticalAnalysis #PowerStruggles


## Transcript
Well, howdy there internet people, it's Bo again.
So today we are going to talk a little bit more
about Putin and his plans as things move forward.
Over the last couple of weeks,
ever since the dust-up with Wagner there's been a lot
of speculation and not a lot of information
what Putin is planning and how he intends on trying to handle those elements
within the military structures there that are questioning his command
capabilities. We're finally starting to see little bits and pieces in public
from Putin that give us a hint because the people that he's been dealing with
and how he's been treating them he's been pretty polite but possibly purgy
and we're getting little bits of information that start to come out and
you can finally start to get a picture of what he might actually be trying to
Okay, so let's start with the removal of traditional Russian officers and where that's going.
We've been talking about some that have been questioned, I think is the polite term, detained,
and we've kind of focused on three that have had this happen or they've been relieved of
command, something like that. It looks like that number may be closing in on 20 at this point.
There's a dozen more that are out there that have been pretty well confirmed to have been relieved
of command. For your average Russian troop, this is really bad news because this is starting to
become much more widespread. It's definitely a political realignment of the traditional
military command structure. The reason that's bad news for the troops is that, generally speaking,
when this starts to occur, the main criteria to take over a vacant command is loyalty, not skill.
Well, that may be really bad news for a lot of Russian troops.
So that's what's going on with the traditional Ministry of Defense side.
Now let's talk about Wagner.
Putin said Wagner PMC does not exist.
And that's the quote that's being used.
There's more to it.
He didn't dismantle it.
That is a...
That's him kind of signaling, sending out a message.
The reality is that there is not a law in Russia that authorizes a military structure
like Wagner.
A company like that, under their laws, really can't exist.
So there's two pieces to this.
Part of it is kind of reminding everybody, hey, you do realize I can outlaw your company
at any moment, but I'm not doing that yet because I'm still trying to be friendly.
It's the carrot and the stick thing.
So he wasn't saying that this is a done deal, that Wagner has been scattered to the ends
of the earth.
he was saying was a factually correct statement. Legally, in Russia, Wagner PMC
does not exist because no law would authorize an organization like that to
exist. It's a veiled method of intimidation a little bit. We
We also have a reporting that says he tried to get the current boss of Wagner out and
put the troops from Wagner under the command of a battlefield commander that they all kind
of respect.
But apparently that didn't go the way he planned.
I have no idea why.
still kind of out there. But it appears that Putin's plan here is to keep Wagner.
I mean, he kind of has to. He needs them. He needs the experienced core of that
company to even pretend that he can remain even remotely viable. And he has
to find a way to keep them loyal to him. I feel like his intention here is going
to be to kind of kick the old boss to the curb and leave Wagner intact but it
might, maybe it comes under control of the Ministry of Defense and it's a
special operations command, an official one, or maybe the company gets
restructured under a new owner but the commanders stay the same. That appears to
be his route and that you know Wagner PMC does not exist that was just
reminding everybody that you're gonna play ball or I'm gonna take the ball and
go home because the only reason your company exists is because I allow it to.
So, he is coming back to his strong man roots, dealing with them.
It seems pretty tactful so far.
However, when you combine that with the traditional commanders that had been relieved of duty,
he's scared.
He's scared, which makes him a little bit more unpredictable.
I think he realizes that maybe the boss of Wagner has more contacts within the upper
echelons of Russian society, those with a lot of money, those with a lot of power if
they were to pool it, and he might be a real threat to him.
So he's trying to maintain control peacefully for the moment.
But a lot of his actions definitely indicate he's scared.
He's scared he's going to be deposed.
And that may not be an irrational fear for him.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}