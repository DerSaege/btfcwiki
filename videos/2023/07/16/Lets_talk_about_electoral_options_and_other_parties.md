---
title: Let's talk about electoral options and other parties....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=-KX8xddKfeM) |
| Published | 2023/07/16 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the role of third-party candidates in the presidential election process.
- Stresses that third-party candidates are mainly about messaging and not actually winning.
- Envisions a scenario where a third-party candidate miraculously wins the presidency and poses the question of what happens next.
- Analyzes the limitations and challenges a third-party president might face within the existing government structure.
- Argues that real change comes from building power structures outside of traditional political parties at the local level.
- Advocates for getting involved in the Democratic Party at the grassroots level to push for progressive change.
- Points out the success of movements like the Tea Party in influencing party dynamics over time.
- Emphasizes the need for long-term, grassroots efforts to bring about meaningful systemic change.
- Encourages focusing on community networks to influence electoral outcomes effectively.
- Concludes by stressing the importance of building sustainable power structures rather than relying on a presidential candidate to bring about significant change.

### Quotes

- "A savior is not coming to fix the United States."
- "The real solution is to build your own power structure that isn't linked to a political party."
- "You will not usher in some glorious revolution, you will not get deep systemic change by recreating the exact same system that brought us here."
- "The system that exists wants to maintain the system that exists because it benefits them."
- "You have to start at the bottom and you have to build it up."

### Oneliner

Beau explains the limitations of third-party presidential candidates, advocating for grassroots power-building outside traditional political structures for meaningful change.

### Audience

Activists, Political Reformers

### On-the-ground actions from transcript

- Start from local levels: County Commission, City Council, School Board, State Representatives, House of Representatives, Senate (suggested)
- Build community networks to influence electoral outcomes (implied)
- Get involved in Democratic Party at local levels to push for progressive change (exemplified)

### Whats missing in summary

Beau's detailed breakdown and analysis of the limitations and challenges faced by third-party presidential candidates, urging for a focus on grassroots power-building for impactful change.

### Tags

#ThirdParty #PresidentialElection #Grassroots #PoliticalReform #CommunityBuilding


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about the electoral process
in the United States, how some people would like to change it,
and I'm going to answer like 300 messages all at once.
Because I have gotten just tons and tons of messages
asking me to talk about a specific candidate who
running with a party that is not Republican or not Democrat. Okay, a bunch
of different candidates, but it's all basically talk about my candidate who is
running for president on a third-party ticket. I talk about third-party
candidates closer to the elections, generally speaking. When it comes to a
third-party candidate for president, I hope that most people know that it's
messaging. That's what it is. It's trying to carry an idea forward. It's not
actually about winning. And I know that there are a lot of people who say, you
know, the only reason that's true, the only reason a third-party candidate
can't win is because people like you say that they can't win so nobody votes for
them. We're gonna skip over the likelihood of a third-party candidate
winning the presidency. We're just gonna jump over the fact that it's less than a
1% chance. We're going to say your candidate, whoever your candidate is, they
won the presidency. How? Doesn't matter. Magic. Not just did they win, they
won in a landslide. Now what? Now what? What are they going to do? The
The United States government is set up with three allegedly co-equal branches of government.
The president from a third party, your candidate, they are going to be able to sign or veto
legislation proposed by the two other parties.
what they're gonna be able to do. And most people who don't view third-party
candidates as just a messenger for an idea to get an idea attention, but really
you want them to win, they do so not out of philosophical reasons but out of
cynical ones. You know, these are very much people who oftentimes say things
like, you know, two wings of the same bird, two sides of the same coin, type of
thing. And this is what drove them to look at third-party candidates to begin
with. If that assumption is true, what do you think the people in Congress are
going to do? Let's say your candidate vetoes something. You'll see a bipartisan
effort to overturn it, right? They can't do anything as president. Sure, they can
maybe change US foreign policy a little bit. What would really happen? Boomerang
effect, just like with Trump trying to undermine NATO. What happened as soon as
the next person got in? NATO is stronger than it has ever been. They can use
executive orders. Yeah, I mean, they can, the same as any other president, but it's
not going to be transformative. The courts, which are full of people appointed
by the two other parties, will strike it down. I mean, let's be clear, the Supreme
Court wouldn't let student debt relief stand. Your candidate is not going to
executive order in the revolution. It's not gonna happen. They're gonna coalition
build and get support in Congress. No, they're not. A third party is a threat to
the power structures that exist, the Democratic and Republican parties they're
not going to help them succeed. You end up with a four-year lame duck. That's the
real problem with people taking it seriously. Now those people who are, who
support third-party candidates to message, that's an entirely different
thing, okay? Or if there is that rare candidate that actually philosophically
aligns with you. I mean, those two I can kind of understand. But thinking that a third party
candidate running for president is going to fundamentally alter the way the United States
works or they're going to be able to accomplish much of anything at all, it's just denial
of reality. It's just not how the country is set up. A savior is not coming to fix the United States.
It's not going to happen. So, if you really don't like, for most people watching this channel,
the Democratic Party, what might you do? If you want to run a president on a
third-party candidate, you have to start from the bottom. County Commission, City
Council, school board, state representatives, House of Representatives,
Senate. If your candidate does not have allies, and a lot of them, up on Capitol
Hill, your presidential candidate doesn't mean anything because even if they were
to win they can't get anything done. You have to start at the bottom and you had
to build it up. Yeah, take a really long time, decades, decades, but that's if you
want deep systemic change you have to build it. The system that exists is not
going to allow it. The system that exists wants to maintain the system that
exists because it benefits them. So you have that as an option. The other option
would be to get involved with the Democratic Party at the local level.
County Commission, City Council, School Board, State Reps, use the primary system
and work your way up. When people say this and say you know you can push more
progressive candidates. The response is often that, well, the Democratic
establishment won't allow it. And that's what people say. But what I hear is
people saying the people with the Tea Party, they were like way better than us.
The Tea Party did it. They started at the bottom. And yeah, the Tea Party in name
didn't really get anywhere, but the Tea Party gave us MAGA. The Tea Party gave us
MAGA, and that's how long it takes to shift a major political party like
that. MAGA has its roots in the Tea Party, in that movement, in that desire to
shift of the Republican Party. It led directly to Trump and all of that stuff.
It is completely possible. The Democratic Party is actually a little bit more
flexible than the Republican Party. Not much, but a little bit. So you have that
as an option and I know because I said that somebody's gonna say look here he
is saying to go back to the establishment. No, I'm gonna say the same
thing I've been saying since all of this started. The real solution is to build a
power structure outside of political parties at the local geographic level.
That's where it starts because if you do that, if you build a community network
like that, you can use it to swing a Democratic primary because you have a
voting base. You can use it to swing a Democratic primary to a more progressive
candidate and then the people in your network, they could vote for the third
party. You have more power that way. You will not usher in some glorious
revolution, you will not get deep systemic change by recreating the exact
same system that brought us here. It takes work and you have to build it.
There's no savior. A presidential candidate for a third party, even in the
super unlikely event that they win, they're not going to be able to do what
you want them to do. The real solution is to build your own power structure that
isn't linked to a political party that's really about helping those people in
that community. That will help determine any electoral politics that get
applied because the people in that network or from that community they know
the issues that that community is facing and they know who would best address them.
The reason I don't talk a lot about third parties outside of their
general message or in the rare occasions where you find people who
literally cannot vote for either party like last time you had a whole bunch of
people who in the Republican Party who are like I cannot vote for Trump again
and I will never vote for a Democrat well let me introduce you to the
Libertarian Party. That's when it gets brought up. We'll talk about the
message and stuff like that as it gets closer as that message gets out. But when
you're looking at a third party and you're trying to evaluate it, if it's
not running, if it's not running senatorial candidates, if it's not trying
to put people in the House of Representatives.
If you don't have somebody locally as part of that party on the ballot somewhere, even
in the really unlikely event that they win, they're not going to have any power.
At the end of the day, the occupant of the White House, kind of their entire job, is
to decide whether or not to let Congress have their way without a fight.
You have to have the allies up on Capitol Hill, or the agenda that your third party
candidate is saying they would have. It'll never make it out of the folder.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}