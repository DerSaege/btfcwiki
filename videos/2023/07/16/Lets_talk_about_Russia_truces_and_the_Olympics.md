---
title: Let's talk about Russia, truces, and the Olympics....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=EUPNbsD2ZVs) |
| Published | 2023/07/16 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Olympic Committee decided Russia and Belarus won't be invited to the next year's games in Paris, but athletes can participate under a neutral flag.
- Ukraine believes Russian and Belarusian athletes shouldn't participate at all, even under a neutral flag.
- A tradition of truce during the Olympics dates back to the 700s BC to ensure the safety of the hosting city and travelers.
- The truce was revived in the early 90s, and it has been violated only three times since then.
- The tradition of sending out invitations about a year in advance for the Olympics makes it unlikely for changes to occur.
- The decision not to invite Russian and Belarusian teams is a consequence of their recent actions, particularly Russia's invasion of Ukraine.
- This move signifies international political consequences and is meant as a punishment to Russia and Belarus.
- Such actions are reminiscent of Cold War tactics and indicate a shift towards a more multipolar world.
- Expect more international political posturing as global power dynamics evolve.
- The decision serves as both a statement and a form of punishment.

### Quotes

- "Ukraine believes Russian and Belarusian athletes shouldn't participate at all, even under a neutral flag."
- "The decision not to invite Russian and Belarusian teams is a consequence of their recent actions."
- "Expect more international political posturing as we move towards a world that is a little bit more multipolar."

### Oneliner

The Olympic Committee excludes Russia and Belarus from the games, allowing athletes to compete under a neutral flag, sparking international political consequences.

### Audience

Sports officials

### On-the-ground actions from transcript

- Contact sports organizations to express support for fair play and sportsmanship (suggested)
- Join movements advocating for clean sports and ethical competition (implied)

### Whats missing in summary

Insight into the potential implications on future international sporting events. 

### Tags

#Olympics #Russia #Belarus #InternationalRelations #Sportsmanship


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Russia and Belarus
and the Olympics and a truce.
So news has come out that the Olympic Committee has basically
decided that Russia and Belarus will not be welcome at next
year's games in Paris.
They won't be invited.
Now, the athletes themselves, they can come participate,
but they have to do it under a neutral flag.
Russia and Belarus will not be invited officially.
Obviously, Russia is upset about this.
Ukraine is too, because they don't even
think that the athletes should be able to participate
under a neutral flag.
They just feel like they shouldn't be allowed to participate at all.
There's an interesting tradition, and it goes back to the 700s, BC.
It was revived in the early 90s, a truce while the Olympics are going on, the period surrounding
the Olympics.
Now back in the 700s, I want to say 770-ish BC, the reason the truce existed then was
to make sure that the city sponsoring and hosting the games wasn't attacked and people
could travel back and forth and there were just less problems, all in the spirit of the
games.
It was brought back in the 90s, early 90s, and since then, countries have pretty much
helped to it.
In fact, it's only been violated three times.
In 2008, with the Russo-Georgian War, in 2014, when Russia went into Crimea, and then in
2022 when Russia invaded Ukraine most recently. Now there is there's a lot of
time between now and then. The the games don't start until July 26th and things
in theory could change, but traditionally the invitations go out
about a year in advance and there aren't a lot of changes once that happens. So it
seems unlikely that we will see a Russian team or one from Belarus. So this
This is something that was very common during the Cold War, this type of, say, international
political consequences for actions like this.
It is, it's meant to be an insult.
It's meant to punish the Russian government and Belarus for siding with them.
As we re-engage with great powers and near peers, expect to see more of this kind of
international political posturing as we move towards a world that is a little bit more
multipolar.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}