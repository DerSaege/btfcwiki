---
title: Let's talk about warming waters in Florida....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=rYmRiLpekvk) |
| Published | 2023/07/16 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- South Florida and the East Coast are experiencing water temperatures in the 90s, leading to coral bleaching.
- Human-caused climate change is a major contributor to the warming waters and coral damage.
- Coral plays a critical role in the ecosystem by providing food for fish, impacting the entire food chain.
- The loss of coral not only devastates the ecosystem but also has economic consequences.
- The effects of climate change are not a distant future issue; they are happening right now.
- Despite warnings for decades, some continue to deny the impact of climate change and advocate for harmful practices like drilling.
- The damage to coral reefs will force boats reliant on tourism to change their profession.
- The impacts of climate change are already visible and will continue to worsen over time.
- Mitigating climate change now won't prevent all future damage due to the inertia of the process.
- Ignoring the signs of climate change is dangerous, as the long-term effects will be severe and irreversible.

### Quotes

- "It's a freight train and it has been barreling along, caution lights flashing and nobody paying attention."
- "Rather than the boats taking people to snorkel and look at all the fish, it's going to shift and become eco-disaster tourism."
- "It is going to affect us all."

### Oneliner

South Florida and the East Coast face immediate coral damage from warming waters due to climate change, impacting ecosystems and economies while signaling broader future disasters we must address now.

### Audience

Climate activists, environmentalists

### On-the-ground actions from transcript

- Contact local environmental organizations to volunteer for coral reef restoration efforts (suggested)
- Support policies and politicians advocating for strong climate change mitigation measures (suggested)

### Whats missing in summary

The full transcript provides a detailed explanation of how coral bleaching due to warm waters is a clear indication of the immediate impacts of climate change, urging urgent action to prevent irreversible damage to ecosystems and economies.

### Tags

#ClimateChange #CoralBleaching #Ecosystem #SouthFlorida #EnvironmentalActivism


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we are going to talk about coral,
Florida temperatures and being under the sea.
OK, so if you missed it, down in South Florida,
and I think along the East Coast as well,
the surface temperature of the water
in the 90s. The water is so warm that it is that it's bleaching the coral. It's hurting
it and if this happens for an extended period of time it causes huge issues, huge. This
This is definitely something that human-caused climate change is contributing to.
The coral produces food for the little fish.
Big fish eat the little fish, so on and so forth.
The coral going away is a major issue.
Not just for the ecosystem, I mean it's just absolutely devastating to that, but for the
economy as well, since a whole lot of people seem to care more about that, we'll talk
about that too.
The reality is this is happening now, right now, this minute.
It's not happening in 20 years or 30 years, not some Star Wars date.
occurring now. And people have been saying it's gonna happen for decades.
And another group of people have been saying no, no it won't. Drill, baby, drill.
It's not a big deal. If the temperatures stay like this and it damages the coral,
There's going to be a whole lot of snorkel boats, scuba boats, fishing boats that are
going to have to change their profession.
It's a big deal.
We are starting to see some of the lesser impacts of what we can expect from climate
change.
Now, today, not 20 years from now, it's happening today.
The stuff 20 years from now, it's going to happen though.
Even if we started mitigating right now, it's still going to happen.
It's not a car.
Doesn't stop on a dime.
It's a freight train and it has been barreling along, caution lights flashing and nobody
paying attention.
This is something that people should probably pay attention to.
We may get lucky this time, but eventually it's the temperatures are going to stay high
enough for long enough to cause permanent damage.
And rather than the boats taking people to snorkel and look at all the fish, it's going
to shift and become eco-disaster tourism or something.
It's not good.
This is just one sign.
There's a whole bunch coming.
It is going to affect us all.
Even down where it's wetter.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}