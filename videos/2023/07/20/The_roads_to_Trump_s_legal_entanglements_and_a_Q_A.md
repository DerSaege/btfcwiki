---
title: The roads to Trump's legal entanglements and a Q&A....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=EdIazr9Xauo) |
| Published | 2023/07/20 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Provides a deep dive and update into Trump's legal entanglements, discussing various cases and answering questions related to them.
- Talks about the election interference case, where Trump received a target letter and an indictment is expected relatively quickly.
- Mentions the New York criminal case, where Trump's attempt to move it to federal court was denied.
- Addresses the Eging-Carroll case, where Trump lost an attempt to get a new trial.
- Talks about the Documents case, where Trump is already indicted, and his team is fighting over a trial date.
- Mentions the Georgia case, where an indictment is expected, raising questions about the breadth of charges.
- Indicates ongoing investigations by the feds and states into financial irregularities involving Trump.
- Mentions potential legal exposure in states investigating fake elector activities.
- Talks about the possibility of Trump facing indictment from the Feds with regards to the Florida case.
- Addresses the Secret Service's potential actions regarding Trump's legal situation, including scenarios like home confinement.
- Comments on Rudy Giuliani's lawyer's statement about not flipping and the potential evidence he holds.
- Talks about the Senate's addition of NATO-related measures to the NDAA and its implications.
- Mentions Georgia Governor Kemp's role as the GOP's hope in re-centering the party.
- Speculates on potential outcomes for Trump, including home confinement and implications for others like him.
- Comments on Marjorie Taylor Greene's stance on accountability for crimes and its potential application to Trump.
- Humorously mentions future video topics post-Trump era, like gardening and relief vehicle projects.

### Quotes

- "I know there are people worried about the Florida case [...] but right now, it does look like the Feds are gonna indict him again."
- "There's a whole bunch of other people who are flirting with Trump's style of leadership, that brand of authoritarianism."
- "His ability to do that is why he's kind of the GOP's only hope at this point."
- "I'd probably feel a lot better if I didn't have to talk about him all the time."
- "Y'all have a good day. Thank you."

### Oneliner

Beau provides a detailed update on Trump's legal challenges, addressing various cases and potential implications, while humorously hinting at future video topics beyond the Trump era.

### Audience

Political enthusiasts, legal observers

### On-the-ground actions from transcript

- Reach out to local organizations supporting accountability and transparency in government investigations (implied)

### Whats missing in summary

Insight into Beau's engaging presentation style and detailed analysis beyond the quick overview.

### Tags

#Trump #LegalEntanglements #ElectionInterference #Indictments #GOPHope


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to kind of do a deep dive
and more of an update into all of Trump's
various legal entanglements.
So we'll go through the cases that are being widely covered,
talk about where they're at at the moment,
and then we're gonna answer your questions
in relation to this.
This format is something we may do as Trump's proceedings proceed and move along at a faster pace.
If this is something y'all like, we may do it more and more on this channel rather than basically have two videos a day
about Trump over on the other one.  But we'll just have to wait and see how it all goes.
Okay, so, starting off, his various cases.
The one that everybody is talking about now, the one that is getting the most coverage,
is the election interference one, the one he got the target letter for.
What do we know about that?
He got the target letter for it.
Generally speaking, there is not a lot of time between receiving a target letter and
an indictment.
So we can expect that to happen relatively quickly.
And once that occurs, we'll know more.
Right now there's a whole lot of speculation going on, but we're going to have to wait
and see what actually comes out of it.
There is the New York criminal case, the one brought by the state up there.
He was indicted on that.
He recently tried to have it moved to federal court.
The judge was like, no.
He might end up being very thankful for that.
I know that they had their view of how things would play out if they were able to get it
moved to federal court.
I'm not sure that their read on how it would have progressed was accurate, but it doesn't
matter because the judge said it's staying at the state level.
You have the Eging-Carroll case.
Trump wanted to get a new trial.
This is the one that he lost.
He attempted to get a new trial.
He lost there too.
The judge is like, no, cut the check.
That's not the official ruling, but that was the general tone.
You have the Documents case, the federal one that Trump has already been indicted
on. Right now they're fighting over a trial date. Trump's team wants it, you know, sometime after
the election when he's free and doesn't have a dinner date or something. Smith wanted it in the
middle of December. The judge did not really seem open to having an indefinite date for the trial.
trial, definitely seemed like, at least at this moment, she wanted some kind of trial
date.
At the same time, she kind of indicated that she thought December might be too soon.
So my guess is Smith and Team Trump are going to try to work something out, but by the end
that will probably end up going back before the judge because Trump's legal
strategy seems to be delay, delay, delay, delay. Okay, then we have the Georgia
case. There is an indictment expected there. For the most part, the big
question there is how expansive is it going to be? The investigation was very broad in scope.
Theoretically, they could be indicting a whole lot of people on a whole lot of counts or they could
try to narrow it down and just get the big fish. We don't know. We'll find out more here soon.
The expectation is we will hear about that sometime in the next 40 days.
So those are the ones that everybody knows about and those are the ones that are getting
all the coverage.
Now at the same time, it's worth remembering that while there's not been official word,
there are pretty clear indications that the feds are looking into, well the feds and the
state actually, in a couple of states, are looking into, let's just call them financial
irregularities dealing with Trump.
And that may come as a surprise to people later, because there hasn't been anything
official about that, but that doesn't mean it hasn't been going on in the background.
And then it's worth remembering that there are a couple of states who are still looking
into the whole fake elector thing and that, depending on the laws in the state and what
the people involved in that say, that could also end up bringing Trump into it.
And then there was a development in New Jersey, which also has the potential to open up a
whole different case.
Yeah, Trump's legal exposure is pretty wide.
He has a whole lot of it.
And at this point, I don't think there's some magic wand that is going to make all
of this go away.
I know there are people worried about the Florida case and saying, well, you know, he
can push it back and push it back because the judge seems to be friendly to him.
Maybe he can get it all the way back until after the election.
Sure.
I mean, that's a possibility, but at the same time,
right now, it does look like the Feds
are gonna indict him again.
That would be in a different venue
in front of a different judge,
who may not be as willing to show any kind of,
I don't wanna say favoritism, but sympathy,
or respect for the former office,
however it gets framed. Okay so that's a brief update on where everything is at
time of filming. Now let's get to your questions. Why are some outlets saying
Trump's letter says witness tampering and others say obstruction of a
congressional proceeding? Okay so witness tampering and obstruction of an
official proceeding are both under the same, let's just call it a heading.
They're both kind of like subsections of it.
And the target letter may not have been specific and the reporting is kind of filling in the
blanks.
The thing is, realistically with Trump and his past behavior, either one of those charges
makes sense.
If it is witness tampering, as I talked about on the other channel, that's really, really
bad for him.
That's not a good sign.
The obstruction thing, yeah, it tracks, it makes sense, it matches the conduct, the alleged
conduct, but at the same time, it seems like some of the other charges would cover that.
So we don't, we'll have to wait until the indictment comes out to know exactly what
it is, because both of those make sense and both of those are under the same heading.
It's worth remembering, Smith does not have to stick to that letter.
The grand jury could return an indictment on all of those, none of those, and something
else, some of those and other charges, it doesn't have to match that letter at all.
Why didn't others get target letters?
So you don't have to get a target letter.
Generally that serves more like an invitation to come talk to the grand jury and present
your side of things.
It's also kind of worth remembering that they may not be disclosing who has had target
letters and those people who have received them if they let's say they got a target letter and they
didn't like being a target maybe they called Smith's office and worked something out.
So, I wouldn't put a whole lot of emphasis on who got a letter and who didn't.
After all this time does it even matter? Yeah, of course it does. Look, the reality is there's a
whole bunch of other people who are flirting with Trump's style of leadership, that brand
of authoritarianism, and I think it would be incredibly valuable for those who might
be tempted to mimic him to see where it ends.
It absolutely still matters.
Will the Secret Service go to prison with Trump?
I have no idea how they're going to handle that.
They might remove Secret Service protection from him.
They might send him to a special secure installation and he serves it there.
Or he could just be given home confinement in lieu of prison.
It is a special case with special considerations, so we don't know.
Rudy's lawyer says he didn't flip.
Then who did?
Yeah I saw that.
Rudy's lawyer said that he didn't flip and that it wouldn't matter if he did because
all of the evidence that he has points to Trump's innocence.
Do you believe the last part of that sentence?
I wouldn't believe the first part if he didn't believe the last part.
His attorney is doing his job, representing the interests of his client.
And that's not to say that he has flipped.
That definition, generally, that really means you have signed a plea and cooperation agreement.
Rudy may not have done that.
I think by the time it's all said and done, there are going to be a whole lot of people
who absolutely did not flip on Trump, who wound up giving evidence against him.
But we'll have to see.
There are, I have my suspicions about who has already come to terms with what's going
on and come to an agreement with the feds but okay did the Senate add that
NATO thing because they're worried about Trump getting re-elected that doesn't
have anything to do with the legal stuff but okay fair enough I have a video
coming out on this on the other channel if you don't know the Senate is trying
to insert something into the NDAA that basically makes it really really hard to
withdraw from NATO. It requires congressional approval so on and so
forth. Is that because they're worried that Trump might get re-elected? I mean
maybe that has something to do with it for some of them but I think I think
I think they realized how fragile that system really is, and they're just shoring it up.
That was something that needed to be done anyway, just to provide a level of stability.
I don't think that many people understand how quickly the landscape internationally
would change if the United States suddenly just decided to withdraw from NATO.
And Congress is just making sure that there's a check on executive authority, which is literally
their job.
Okay.
You said Kemp was the GOP's hope.
He's talking about how Trump might lose Georgia, and he seemed sad.
Yeah, he did seem that way.
That's why he's the GOP's hope.
Kemp is one of the few people that could actually really help the GOP re-center.
He really is.
And it's because he can sit there and say, you know, if Trump keeps talking about 2020,
he's going to lose Georgia.
He knows Trump's going to lose Georgia anyway.
But he's putting it in a way to encourage the former president to behave.
And he's saying it in a tone that wouldn't anger MAGA, which might eventually be able
to get them to return to center.
His ability to do that is why he's kind of the GOP's only hope at this point.
I assume Trump will get home confinement.
Is that really a deterrent for others like him?
I mean, yeah, I think so, because I think that if they are going to go that route and
make that accommodation for him, I think that he would be on home confinement for a much
longer period than he would if he was actually going to be incarcerated.
And I do think that it would be a deterrent.
M.T.G. Marjorie Taylor Greene, the space laser lady, said people should be held accountable
for their crimes no matter who they are.
Does that not apply to Trump?
I mean, I get what you're saying.
Yeah.
I mean, it's, she did.
She went through the whole spiel and all of that stuff talking about it.
And I mean, obviously, when she was saying that, she did not count on it being applied
to Trump.
I mean, she has been accused of many things.
Being consistent isn't one of them.
So yeah, I mean, that's just, it's normal.
I want to, what's that saying, you know, for my friends understanding for my enemies, the
law.
That's that's what she's going with here.
If Trump goes to prison, what are you going to make videos about?
Oh, I get it, because all my videos are about Trump.
Gardening video, a gardening video, I'd like to finish the gardening video.
That would be nice.
So the turning the Jeep into the relief vehicle, I mean that thing's actually been used in
a hurricane and we still don't have the video out on it, but that will make the video better
because you'll get to see it.
Yeah I would actually like to not have to cover Trump all the time, that would be fantastic,
it would be fine by me.
There are plenty of things wrong in the world that I can cover.
I don't have to, I don't need Trump for that.
I'd probably feel a lot better
if I didn't have to talk about him all the time.
Okay, so there's the updates on the various cases,
answers to your questions.
If you want to send in a question,
question for Bo, F-O-R, question for Bo at gmo.
Gmail.
And I think that's it.
Anyway, it's just a thought.
Y'all have a good day. Thank you.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}