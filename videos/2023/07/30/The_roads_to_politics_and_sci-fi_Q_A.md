---
title: The roads to politics and sci-fi Q&A....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=MN2zVoquIfY) |
| Published | 2023/07/30 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau is doing another Rhodes Q&A, which will now likely occur weekly due to the influx of questions.
- Questions are being organized thematically, with a mix of political and science fiction-related queries.
- Topics discussed include frozen roundworms coming back to life, Twitter's rebranding, the idea of a monument to Emmett Till, and the parallels between Star Trek episodes and current events.
- Beau shares insights on Ukraine's troop strength, Twitter rebranding, and the potential for a general strike.
- Beau also addresses the ICC's limitations in arresting political figures like George W. Bush or Dick Cheney.
- Beau expresses surprise at Trump's 2016 win, discussing voter turnout and the impact of negative sentiments towards Hillary Clinton.
- Beau touches on the challenges of off-grid living in Colorado based on a tragic incident involving a group's failed attempt.
- Beau shares thoughts on Mitch McConnell's handling of Trump within the Republican Party and Hulu's staggered release of Futurama episodes.

### Quotes

- "I think that a monument to Till would do better at reminding people of the history rather than the mythology."
- "Stuff on YouTube is edited. When you're talking about anything that is permanent, you have to actually try this stuff in a more controlled setting."
- "If he can't resolve this problem before he finishes his time in office, his legacy is going to be that of the Senate majority leader who enabled Trump."

### Oneliner

Beau delves into various topics, from frozen roundworms to Trump's presidency, providing insights on current events and community networks.

### Audience

Viewers interested in diverse perspectives on current events and community engagement.

### On-the-ground actions from transcript

- Research and support local community networks (suggested)
- Advocate for accurate and comprehensive survival education (implied)
- Stay informed about political figures' actions and their implications (implied)

### Whats missing in summary

Insights into Beau's perspectives on Trump's presidency and Mitch McConnell's handling of the Republican Party.

### Tags

#CommunityEngagement #CurrentEvents #YouTube #PoliticalInsights #SurvivalEducation


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to do another Rhodes Q&A.
These are definitely going to happen more than once a month,
which was what the original plan was.
Ha, ha, ha.
There's just the number of questions coming in.
So they'll probably be once a week.
We haven't really decided yet.
They're trying to organize the questions into some kind of theme
as they come in.
It's hit or miss.
this was told, I was told that this was mostly political questions, but with a
science fiction theme. Don't really know what that means, but we're about to find
out. Okay, so what are the questions? What about the frozen roundworms 46,000
years? Yeah, that's something else. That's probably something that
will turn into a full video over on the other channel. For those who are just
hearing about this, it was in the permafrost. And basically, they found these small organisms
that had been frozen for tens of thousands of years. And they brought them back to life.
And there are a lot of people who are, let's just say, concerned. You know, a lot of science
fiction movies start off that way. I guess I shouldn't say brought them back to life.
I guess they were cryogenically frozen, kind of.
They weren't really dead.
When they were thawed out, they came back to life after a very long period of time.
It raises some questions, and the irony in it to me is those people who are objecting
to it the loudest. They are typically the people who don't really believe in climate
change. I've got some bad news about what's gonna happen when the temperature warms and
things start thawing. This is gonna happen naturally. We'll dig into that a little bit
later this week I think. What do you think of X instead of Twitter? Yeah, that's a...
I have never seen anybody remove so much brand value so quickly before in my entire life.
For those that don't know, Twitter is rebranding. Yes, the company that was so good at branding
that the name of the company became a verb, tweet.
You know, even on clone platforms,
like nobody actually says, well, I re-truthed that.
They re-tweeted it.
You can't buy that kind of advertising.
And then the selection of X, from my understanding,
has already led to a giant list of issues,
The most entertaining being something that happened in Indonesia, I guess.
I didn't really look into it, but it appears that due to the normal connotation of the
letter X on the internet, that some countries are reluctant to allow the platform.
Okay.
What do you think of the Till monument idea?
I think it's a great idea.
I think it is a fantastic idea.
Because it takes a conservative talking point and it turns it on its head.
You know, all of this time they were saying, you know, we have to keep these statues up
so we remember our history.
I think that a monument to Till would do better at reminding people of the history rather
than the mythology than, you know, a statue honoring a Confederate general.
I think it is a really good idea.
I'm sure it's going to come with its own backlash, but I think it's a good idea.
I've started watching Star Trek, and have to say it's amazing how accurately the shows
from the 90s and before depict our current timeline.
Irish unification, massive homelessness, etc.
Just a ray of hope.
I think it might make a good, hopeful video.
Ooh.
Okay, so right now the the like the huge Star Trek fans, they're all
Kind of cringing
Let's hope it doesn't stay on
The Star Trek timeline because if I'm not mistaken
mistaken. Sometime in the next five years by that timeline, I think there's a nuclear
war. Yeah, there's a lot of bad before we get to, you know, the fully automated society
there. 2026, I think I could be wrong. But yeah, that's I would like to think we could
get through it without going through the full timeline.
Just watching your July Q&A, the question, what do you know about us?
And as a regular viewer from Vienna, but not Virginia, the OG Vienna, I would like to know
how global is your viewership?
About one in four.
About one in four.
I think it's 77% are in the U.S.
The bigger countries, as far as viewership, they're mainly the countries you would expect.
First English-speaking countries, and then Japan, Korea, Germany, countries where there's
a U.S. military installation.
Those are the biggest, but 1 in 4 overseas is more than I expected when I first saw the
number and that's been pretty consistent since probably 300,000 subscribers, somewhere
around there.
A coworker and I were talking about whether or not Ukraine has the troops to sustain their
their efforts, conflicting info from different sources.
I shared with him a YouTube channel
that covers frontline movement.
Coworker said it's useful info.
Oh, but asked what the source material was for the maps.
Okay, so this question was actually used
on the other channel, at least part of it,
the part about the maps and everything.
Okay, so as far as whether or not Ukraine has the troops, that's a talking point right
now that's coming from both sides, but each saying the other country is going to run out
of troops.
Generally speaking, during wars, countries do not actually run out of troops.
They run out of resolve.
I want to say Ukraine has 11 million that are available for military service, that means
within the proper age range, and I want to say 7 million of those were fit for duty.
So the amount of loss it would take to actually get to the point where a country doesn't have
the proper amount of troops, those numbers would be in the millions.
That's really unlikely, and that goes for both sides.
Russia is having a harder time with troops right now, but it's because Putin knows that
a full mobilization, like a real draft there, might put Swan Lake on the air and be the
end of his end of his rule and then as far as the maps and movements there's
that there's a whole video over there now explaining how that's done generally
speaking Institute for the study of war has the best maps if you really want to
get into where everybody is at the moment. Bo I noticed you haven't been
organizing your videos into playlists lately? Do you have plans to maybe start doing it
... doing so again? It gets difficult sometimes to go back and see which videos are about
Ukraine, for example. Okay. So we're still putting them in playlists, but oddly enough
that is going to be redone the way we do it on both channels. If you're looking for stuff
on Ukraine, on the main channel, look under the foreign policy heading.
That's where you'll find them.
I saw a meme of you saying to ignore people stealing from grocery stores.
Just thought you would like to know that people are using your image for that.
I haven't seen that meme.
If you could make a quantum leap time travel within your own lifetime, what event would
you try to prevent?
Yeah, I don't know.
I'm a real big believer in the butterfly effect.
Like a tiny change might create something far, far worse.
Yeah, I don't know.
I do not have an answer to that question really.
My question is, did you think Trump would win in 2016?
I'm an independent voter in a rural area who thought we were smarter than that as a country.
back, I see Trump as the bigot's backlash for having the audacity to put a liberal black
man in the White House.
So the question, did I think Trump was going to win in 2016?
No.
I grossly underestimated the unlikely voter.
I did not realize how much negative voter turnout Clinton was going to cause.
There were a whole lot of people who showed up not to vote for Trump, but to vote against
her.
And then there were a lot of people on the Democratic side of the aisle who didn't vote,
would normally vote because of the Bernie thing with the primary and all of that.
It really was kind of a unique storm, but no, I didn't see that one coming.
I was very concerned about him getting elected, but in the back of my mind, I didn't really...
I recognized his rhetoric early on, I just didn't, not to be a walking
cliche, I didn't think it could happen here.
I didn't think he'd get elected.
Could Hunter Biden make a legal case against the space laser lady for sharing
those images, I have to alter what was written here, sharing the images of him that she did
on the floor.
So what she did on the floor is probably covered by the speech and debate clause.
That being said, my understanding is that she also distributed those images via other
means.
He might have a case there.
I don't know.
an aspect of of law I really know I mean anything about to be honest and then the
follow-up is you know should he if he could I don't know I don't know that it
would do any good she would probably she would probably claim you know some kind
of that it was unfair and an infringement on her free speech and
something like that, and right now she's not doing well.
I don't know that I would want to hand her a tool she can use to fundraise off of.
As the Teamsters start to join in the Hollywood strike, and so many Americans are showing
their support, do you think this could snowball into a general strike?
I have a video on this topic over on the other channel.
Let's talk about strikes generally, is the name of it.
It is, it's a huge undertaking.
And in the United States, it is a massive undertaking.
That's one of those things that you would have to
have a real network of people organizing
for an extended period to even get it into people's mindset
that it's something that could be done.
Now, could it be done on a localized level, like say in Hollywood?
Maybe, maybe.
But as far as that just happening organically, I don't think so.
Any idea if the ICC ever considered an attempt to arrest George W. or Cheney for their crimes
or is there a statute of limitations?
Okay, so when it comes to the ICC, the one thing to remember is that it's foreign policy.
Foreign policy is not about morality, it's not about right and wrong, it is about power.
Nobody's going to arrest a U.S. out of state.
That is one of those things that it's just so unlikely.
It's like they wouldn't, they would not swear out a warrant.
They wouldn't issue a warrant for a Chinese head of state either.
The fact that they did issue a warrant for a Russian head of state, you can read a little
into that.
America is not the superpower that it once was, and the very fact that the ICC is doing
what it's doing is evidence of that.
I thought the deal was whoever got the nomination that the person would get the support of the
other people.
Wouldn't this be reason enough for him to refuse to do the debate?
That's obviously about Trump.
Yeah I don't know.
I don't think that's why he would refuse to do the debate because he doesn't have a problem
going back on what he says.
I mean nobody would actually expect Trump to honor his word on something like that.
If Trump believes what he needs to do to be able to continue to claim his prosecution
is about interfering with the election, he will absolutely run third party.
It doesn't matter what he says.
I've been watching your videos for a few years now and I've noticed a paradox.
You're a big advocate of community networks and mutual aid, but you're also a self-described
hermit.
Yeah I am.
If I could do things my way, I would pretty much never leave this place.
That being said, when I do, it's normally for a community networking event.
In fact, since you're on this channel, on this channel, you can see that most of the
organizing and logistics and stuff like that, it occurs here, like for my network.
I don't even have to leave for meetings, they come here.
But at the same time, it's also worth remembering that you can be a person who doesn't people
well and still be involved.
You just have to find your specific category where your skill set fits in and you can do
stuff from your house.
You can do stuff just among people that you're comfortable around.
You don't have to go out and get involved in the community to benefit the community
and be part of a community network.
One of the examples that I've used in the past is when we were doing relief for Hurricane
Michael, we had all of these orange buckets that showed up.
They were like Home Depot buckets and they were packed.
They were pre-packed with all kinds of stuff people would need to do to do cleanup.
Like if your house had been damaged, everything you needed was in that bucket.
And you know, yeah, we took them out and delivered them to people.
And I did, you know, leave the property for that because when it is stuff like that, I
don't mind doing it.
I would rather, I would rather me do it than some of the other people in our network.
Yeah, just a different range of life experiences.
But those orange buckets, somebody packed those.
Somebody packed those.
Those came off a truck, I want to say from Tennessee, and they were pre-packed.
Somebody that far away did something with their network, and those were probably packed
in somebody's house.
Did you hear or have any thoughts about the off-grid group in Colorado?
That is so sad.
If you haven't heard about it yet, a group of people, I want to say it was two, I think
it's two sisters and one of their sons.
They decided to go off the grid in Colorado and try to live off the grid in Colorado and
apparently their entire research was YouTube and they just went out and tried to do it.
They did not make it a year.
Please remember and this goes for any videos on this channel that show up about homesteading
or survival or something like that.
It is not designed, nothing on YouTube is designed to be comprehensive.
You have to actually try this stuff in a more controlled setting than going out in extreme
weather.
It's one of those things where when you're talking about Colorado, making a mistake with
your shelter, there's no second take.
You're done if you don't do it right.
It's like being here and not knowing how to cool off.
If you don't have those skills mastered, you will not make it.
This is a lot like what happened when YouTube was just like overrun with the van life videos
where people were doing that and it was really cool.
And then a lot of people who weren't YouTubers, who didn't have a production budget, who
couldn't do a second take, went out and tried it and the experience was very different.
Just remember, stuff on YouTube is edited.
It is edited.
Where we did the survival video at on this channel, where that nasty green water that
we pulled out, there are definitely gators in there.
There are definitely wolves and coyotes in that area.
There are issues that people can run into, and generally speaking, when somebody is on
on YouTube putting together a video like that,
they're doing it in an area, a climate
that they are incredibly comfortable in.
And a mistake there, it could be really bad.
In fact, there was something that was obviously,
and now kind of looking back,
I wish I'd put it in the video,
there was obviously an area
where there were a bunch of venomous snakes right at the edge of the water.
We know what we're looking at and we didn't go near it because we like our blood of the
proper consistency, but if somebody wasn't familiar with that, they might have stood
or I buy it to get water.
Just remember there's a lot more danger in being outdoors, especially when you're talking
about anything that is permanent or you are a long way from help or you don't have a ready
means to get help.
And that's the other thing, is at least with my circle, 80% have a first responder or better
under their belt.
I mean, it is dangerous.
And don't let YouTube let you think otherwise, because it may not end up well.
I know you always say never to underestimate Mitch McConnell, but after three years into
the Biden's presidency, do you think McConnell overplayed his hand with Trump?
Personally, it's very easy to see that the Republican Party would be in a much stronger
position if they had cut Trump out sooner.
I think he underplayed it.
I think he actually had him and could have gotten him out of the party early on, had
he led the senators that way.
I think he was expecting all of these indictments to come a lot sooner, and that way it was
the Democrats who took out Trump and not him.
I would imagine that's going to be a big regret, because if he can't resolve this
problem before he finishes his time in office, his legacy is going to be that of the Senate
majority leader who enabled Trump, regardless of anything he might have done behind the
scenes to try to lessen the impact.
At the end of the day, if he had tried a little bit harder and if he had approached Trump
the way he typically approaches any Democratic president, the country would be a lot better
off.
I think he was worried about ending up like Liz Cheney.
But at the end of the day, when the history books are written, her name's fine.
His name?
Especially if his health issues catch up to him before he can do something?
It's not going to be a good legacy.
Why no Futurama video yet?
I need a review.
Because Hulu is doing like the cruelest thing in the world.
releasing one episode at a time. And that's just it's mean and hurtful. For
those that don't know, Futurama is a show that started a real long time ago. It got
canceled over and over again and kept being brought back. It has come back to
Hulu. The first episode is out at time of filming. I have watched it. It is just
like the first episodes. It's like the rest of the series. They're not
doing it like a fan it's not like a fan service thing they're just continuing on
with the series it's pretty cool but I would really I will probably wait to see
a few more episodes before I make a video about it because right now the
there's only the one episode and I haven't been able to binge on it and of
Of course, ironically, the first episode is about binging.
It's a take on the habit of binging content.
And they only released one episode, because they're evil.
No, overall, from what I've seen, it looks pretty good.
And I've seen the titles of the episodes
that they're going to be releasing.
going to be the same type of satire
that a whole lot of people are going to miss.
And they're not going to get, I think.
Which is good, because that's what the entire show has
been this time.
So it's nice of them to be consistent in that regard,
I guess.
OK.
So those are all of the questions for this week.
I'm going to pretend that we're going to do this every week.
We don't know yet, but these are definitely
going to be coming out more often
than we initially thought.
I think we may try to just do it every Sunday
and get one out every Sunday on that channel,
and then the other stuff can fall in between somehow.
So that's where we're at.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}