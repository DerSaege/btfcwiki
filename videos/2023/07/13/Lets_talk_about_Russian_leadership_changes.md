---
title: Let's talk about Russian leadership changes....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=XK4Li-hsjPw) |
| Published | 2023/07/13 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Providing an overview of Russia's leadership changes and happenings.
- Speculating on the private deal between Putin and the boss of Wagner.
- Mentioning a naval officer getting shot while jogging, likely unrelated to command structure changes.
- Addressing the detainment of Sir Avikin and the ambiguity around his situation.
- Explaining the relief of duty for General Ivan Popov due to criticism of the Ministry of Defense.
- Debating whether the ongoing changes in leadership constitute a purge or a political realignment.
- Noting Russian security services' increased micromanagement and removal of commanders stepping out of line.
- Suggesting that the current actions may not yet qualify as a purge but could develop into one in the future.
- Encouraging continued observation of the situation for further developments.

### Quotes

- "Is this the P word? Is this a purge?"
- "Russian security services loyal to Putin have started the micromanagement."
- "But give it a month. We'll see what happens."

### Oneliner

Beau analyzes Russia's leadership changes, speculates on private deals, and debates whether the shifts amount to a purge or a political realignment.

### Audience

Political analysts, Russia watchers

### On-the-ground actions from transcript

- Monitor Russian leadership changes for future developments (implied).

### Whats missing in summary

Insights into the potential implications of Russia's leadership changes and micromanagement by security services.

### Tags

#Russia #LeadershipChanges #Putin #SecurityServices #PoliticalRealignment


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about Russia and generals
and command changes and the structure
of how things are shaping up over there.
And we're gonna answer a question
about whether or not something has started.
And we'll just kind of go through some of the happenings
with the leadership over there
and some of the little bits of information
have come out. Okay, so we'll start at the top. The boss of Wagner, what's going on?
He is apparently now telling people that, well, he just went crazy. He just went
crazy. He is being treated remarkably well, all things considered. We don't
know how much of this is part of the private deal that he struck with Putin
because when they stood down there was the public facing deal and then the
private deal. We don't know what the private deal was. We know that one
happened based on everything that's occurred since but we don't know any of
the terms. So, it could be that Putin is just living up to his end of the deal, or it could
be that Putin is too scared to act, or it could be that Putin is just remembering that
it's a dish best served cold. But right now, he really doesn't seem like he has a care
of the world. The boss of Wagner, he's kind of just chilling. Now, one that a whole bunch of people
have asked about, there was a naval officer who was jogging, who caught, I want to say six rounds while
he was jogging. That happened, according to all reports. I don't think that's related to
this, I think that is a separate thing that's going on. I think that has to do
with something else entirely. It has nothing to do with the command structure
changes. You have Sir Avikin, who was the one who was detained, reportedly, and
gently and politely questioned. Recently, those linked with the Kremlin have
indicated that he is quote resting at a resort in the mountains but he is not
available. I mean maybe that's true. As we talked about at the time, being
detained means different things. That could mean anything from you need to go
home and chill out to you're in a basement somewhere. So maybe that's true,
Maybe it's not and we'll never see him again. We don't know yet. You have General
Ivan Popov. He was commander of the 58th Combined Arms. He basically criticized
the Ministry of Defense. He has been relieved of command. He had complaints
It's pretty similar to what the boss of Wagner said.
Wasn't getting support, no counter-artillery.
It wasn't going well.
He felt it was due to mismanagement at the Ministry of Defense.
He was quickly relieved of duty.
looks like the end of it. It doesn't look like they're taking extreme punishments
at the moment. Okay, so the question that keeps coming in with all of these
changes, is this the P word? Is this a purge? I don't think so. At the moment
And this is a political realignment of sorts.
I don't consider it a purge until they start aligning the generals horizontally.
At the moment, it really does seem like they're yanking people and restructuring.
They're doing it in a very direct fashion, but so far it doesn't look like their new
command is in a cemetery somewhere. So while that's still on the table when we
talked about it before, this type of thing would go in a cycle and this is
this is kind of what the beginning of one could look like, but we're not there
yet. We're not at the point to say yes it's happening, but it is definitely
certain at this point that Russian security services loyal to Putin have
started the micromanagement, have started really paying attention to what the
field commanders are doing and if they step out of the line right now they're
just being relieved of duty. The way they get relieved of duty may change in the
future if it starts to spread. So it's something to continue watching, but at
this moment I wouldn't I wouldn't call it a purge. But give it a month. We'll see
what happens. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}