---
title: Let's talk about an update on the pillow world....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=pnVDGslWMSg) |
| Published | 2023/07/13 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau provides an update on the aftermath of the 2020 election, focusing on the MyPillow CEO, Mike Lindell, and his claims.
- MyPillow's financial situation seems dire as they are subleasing manufacturing space and selling surplus equipment.
- Major retailers dropped MyPillow products post-January 6th, impacting the company's revenue.
- Lindell believes that retailers like Bed Bath and Beyond made decisions based on not carrying MyPillow products.
- Lindell remains confident in the lawsuits against Dominion and Smartmatic, claiming he has evidence that will vindicate him.
- He compares his situation to the movie "My Cousin Vinny" in terms of evidence presentation.
- Beau suggests keeping track of this community as they might reenter the news cycle due to Lindell's financial situation.
- Voting machine companies might be willing to settle for less due to Lindell's financial struggles.
- Despite overwhelming evidence against the claims, a community continues to believe in the election fraud allegations.
- Beau hints at a potential resurgence of this topic in the news cycle soon.

### Quotes

- "He compares his situation to the movie 'My Cousin Vinny.'"
- "There's still a pretty active community looking into the voting claims."
- "It's the kind of stuff that when you read through it just makes your mind melt."

### Oneliner

Beau gives an update on MyPillow's financial struggles post-election, Lindell's confidence in lawsuits, and the persistent community still promoting election fraud claims.

### Audience

News consumers

### On-the-ground actions from transcript

- Stay informed on the developments in the aftermath of the 2020 election (implied).
- Be critical of misinformation and false claims regarding election fraud (implied).

### Whats missing in summary

Detailed analysis and context on the aftermath of the 2020 election and its impact on MyPillow and Mike Lindell.

### Tags

#ElectionFraud #MyPillow #MikeLindell #NewsUpdate #CommunityBeliefs


## Transcript
Well, howdy there internet people, it's Bo again. So today we are going to
take a look at the wonderful world of
the the pillow guy and kind of check in there see how things are
going in that space
Remember that there is still a lot of
unfinished news that will be coming out of
that
world soon. If you have no idea what I'm talking about, in the aftermath of the
2020 election, one of the main personalities behind the famed MyPillow
made a lot of claims. Mike Lindell made a lot of claims about the election. Many
of them were similar to the claims that were the subject of the recent Fox
lawsuit. Dominion and Smartmatic do appear to be pursuing the same type of
suit against Lindell. Now, as far as MyPillow, how is that doing? Not well, not
well. It appears they are subleasing some of their manufacturing space. It also
appears that they are auctioning or selling some manufacturing equipment
that Lindell describes as surplus. The company is having a hard time making
ends meet. In the aftermath of January 6th, a whole lot of major retailers
decided that maybe it wasn't such a good idea to carry those products. That
impacted Lindell's bottom line in a big way. So the resources really aren't there.
Now, it is worth noting that Lindell believes, at least appears to believe, you
know his quotes are chaotic. It appears that he believes Bed Bath and Beyond
went the way they went because they stopped carrying my pillow. I'm not sure
if that's actually the case but he certainly appears to believe that. He
also believes that in the suits with the machine companies with Smartmatic and
Dominion that he will ultimately triumph. In a recent interview he said that he
had evidence that of course nobody has seen yet but once that evidence is shown
at trial, he believes he will be vindicated. He made comparisons to the
movie My Cousin Vinny. Yeah, so there's that. It's important to keep track of
this community because it will reenter the news cycle and it's going to
explain why it seems likely to me that the voting machine companies, Dominion and
Smartmatic, they might be willing to settle for less than they initially
asked for, simply so it's a number that can actually be collected. We don't know
that that's going to happen but if the dollar amounts drop it might have
something to do with Lindell's own financial situation. Not a sudden
softening from the companies that are filing suit. It's always a unique
dive anytime you look into that world. It's not getting a lot of coverage right
now so it might be surprising to some people to find out that there's still a
pretty active community who is looking into the voting claims despite all of
the evidence that has come out since then. They are still reviewing those
claims and of course believe they have evidence that somehow should convince people that something
happened despite all of the available evidence to the contrary.
There's a reason we don't touch on this world very often in the coverage because it's the
kind of stuff that when you read through it just makes your mind melt. But I have
a feeling all of this will be coming back up in the news pretty soon. So just
to give you an update. That's what's going on in that world. Anyway, it's just
a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}